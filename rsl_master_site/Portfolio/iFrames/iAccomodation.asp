<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim lst_heating, lst_parking, lst_garden, lst_furnishing, lst_security
	Dim lst_kitchen, lst_scheme, lst_ss, lst_ads,l_address1
	
	Dim FLOORAREA ,MAXPEOPLE, BEDROOMS, BEDROOMSITTINGROOM, KITCHEN, KITCHENDINER, LIVINGROOM, LIVINGROOMDINER, DININGROOM, WC,_
		BATHROOM, BATHROOMWC, SHOWER, SHOWERWC, BOXROOMUTILITYROOM, SHAREDKITCHENLIVING, SHAREDWC, SHAREDSHOWERWC, HEATING,	PARKING,_
		GARDEN, FURNISHING, PETS, SECURITY, ACTION
	
	Dim TitleCellWidth,DataCellWidth
	
	
	
	
	PropertyID = Request("PropertyID")
	if (PropertyID = "") then PropertyID = -1 end if
	
	l_address1 = Request("strAddress")
	if (l_address1 = "") then l_address1 = "[None Given]" 

	OpenDB()
	
	ACTION = "NEW"
	
	SQL = 	"SELECT PA.* , "&_
				" ISNULL( PH.DESCRIPTION , '[None Selected]') AS HEATINGDESC ,"&_
				" ISNULL( PG.DESCRIPTION , '[None Selected]') AS GARDENDESC ,"&_
				" ISNULL( PF.DESCRIPTION , '[None Selected]') AS FURNISHINGDESC,"&_
				" ISNULL( PS.DESCRIPTION , '[None Selected]') AS SECURITYDESC, "&_
				" PARKING = CASE PARKING WHEN 1 THEN 'Yes' ELSE 'No' END,"&_ 
				" PETS = CASE PETS WHEN 1 THEN 'Yes' ELSE 'No' END"&_ 
			" FROM P_ATTRIBUTES PA "&_	
				" LEFT JOIN P_HEATING PH ON PH.HEATINGID = PA.HEATING "&_
				" LEFT JOIN P_GARDEN PG ON PG.GARDENID = PA.GARDEN "&_
				" LEFT JOIN P_FURNISHING PF ON PF.FURNISHINGID = PA.FURNISHING "&_
				" LEFT JOIN P_SECURITY PS ON PS.SECURITYID = PA.SECURITY "&_
			"WHERE PROPERTYID = '" & PropertyID & "'"
	
	
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then
		FLOORAREA = rsLoader("FLOORAREA")
		MAXPEOPLE = rsLoader("MAXPEOPLE")
		BEDROOMS = rsLoader("BEDROOMS")
		BEDROOMSITTINGROOM = rsLoader("BEDROOMSITTINGROOM")
		KITCHEN = rsLoader("KITCHEN")
		KITCHENDINER = rsLoader("KITCHENDINER")
		LIVINGROOM = rsLoader("LIVINGROOM")
		LIVINGROOMDINER = rsLoader("LIVINGROOMDINER")
		DININGROOM = rsLoader("DININGROOM")
		WC = rsLoader("WC")
		BATHROOM = rsLoader("BATHROOM")
		BATHROOMWC = rsLoader("BATHROOMWC")
		SHOWER = rsLoader("SHOWER")
		SHOWERWC = rsLoader("SHOWERWC")
		BOXROOMUTILITYROOM = rsLoader("BOXROOMUTILITYROOM")
		SHAREDKITCHENLIVING = rsLoader("SHAREDKITCHENLIVING")
		SHAREDWC = rsLoader("SHAREDWC")
		SHAREDSHOWERWC = rsLoader("SHAREDSHOWERWC")
		HEATING = rsLoader("HEATINGDESC")
		PARKING = rsLoader("PARKING")
		GARDEN = rsLoader("GARDENDESC")
		FURNISHING = rsLoader("FURNISHINGDESC")																										
		PETS = rsLoader("PETS")
		SECURITY = rsLoader("SECURITYDESC")
		ACTION = "AMEND"				
	end if
																		
	// build page top select boxs
	
	
	
	
	Call BuildSelect(lst_heating, "sel_HEATING", "P_HEATING", "HEATINGID, DESCRIPTION", "DESCRIPTION", "Please Select", HEATING, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_garden, "sel_GARDEN", "P_GARDEN", "GARDENID, DESCRIPTION", "DESCRIPTION", "Please Select", GARDEN, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_furnishing, "sel_FURNISHING", "P_FURNISHING", "FURNISHINGID, DESCRIPTION", "DESCRIPTION", "Please Select", FURNISHING, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_security, "sel_SECURITY", "P_SECURITY", "SECURITYID, DESCRIPTION", "DESCRIPTION", "Please Select", SECURITY, "width:150px", "textbox200", NULL)

	TitleCellWidth = 137.5
	DataCellWidth = 50

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<style TYPE="text/css">
	<!--
		.TITLE {  background-color: beige; font-size: 10px; font-weight: bold}
	-->
</STYLE>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">

<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD ROWSPAN=2 WIDTH=70% HEIGHT=100%>
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLSPACING=1 cellpadding=1 border=1 CLASS="TAB_TABLE">
				<tr>
					
					<td colspan='8' rowspan='2' >
						<TABLE HEIGHT='100%' WIDTH='100%' STYLE="BORDER-COLLAPSE:COLLAPSE;border:none" CELLPADDING=0 CELLSPACING=0 border=0 >
							<tr>
								<td nowrap CLASS='TITLE' STYLE='Width:138px;BORDER-BOTTOM:1PX INSET #E0E0E0;BORDER-RIGHT:1PX INSET #E0E0E0' >Furnishing:</td>
								<td STYLE='Width:230px;BORDER-BOTTOM:1PX INSET #E0E0E0;BORDER-RIGHT:1PX INSET #E0E0E0' nowrap>&nbsp;<%=FURNISHING%></td>
								<td nowrap CLASS='TITLE' STYLE='BORDER-BOTTOM:1PX INSET #E0E0E0;BORDER-RIGHT:1PX INSET #E0E0E0'>Security:</td>
								<td align=left nowrap STYLE='BORDER-BOTTOM:1PX INSET #E0E0E0;'>&nbsp;<%=SECURITY%></td>
							</tr>
							<tr>
								<td nowrap CLASS='TITLE'  STYLE='BORDER-RIGHT:1PX INSET #E0E0E0;'>Heating:</td>
								<td nowrap STYLE='BORDER-RIGHT:1PX INSET #E0E0E0;'>&nbsp;<%=HEATING%></td>
								<td nowrap CLASS='TITLE'  STYLE='BORDER-RIGHT:1PX INSET #E0E0E0;'>Garden:</td>
								<td align=left nowrap>&nbsp;<%=GARDEN%></td>
							</tr>
					
						</table>
					</td>
				</tr>
				<tr>
					
					
				</tr>
				<TR>
					<td nowrap CLASS='TITLE'   style='width:<%=TitleCellWidth%>px'>Bedrooms:</td>
					<td style='width:<%=DataCellWidth%>px' align='center'><%=BEDROOMS%></td>
					<td  CLASS='TITLE' align='left'  style='width:<%=TitleCellWidth%>px' nowrap>Bedroom/Sitting&nbsp;Room:</td>
					<td style='width:<%=DataCellWidth%>px' align='center'><%=BEDROOMSITTINGROOM%></td>
					<td nowrap CLASS='TITLE'    style='width:<%=TitleCellWidth%>px'>Kitchen:</td>
					<td style='width:<%=DataCellWidth%>px' align='center'><%=KITCHEN%></td>
					<td nowrap CLASS='TITLE'  style='width:<%=TitleCellWidth%>px' >Kitchen/Diner:</td>
					<td style='width:<%=DataCellWidth%>px' align='center'><%=KITCHENDINER%></td>
				</tr>
				<TR>
					<td nowrap CLASS='TITLE'  >Living Room:</td>
					<td align='center'><%=LIVINGROOM%></td>
					
           			<td nowrap CLASS='TITLE'    align='left'>Living&nbsp;Room/Diner:</td>
					<td align='center'><%=LIVINGROOMDINER%></td>
					<td nowrap CLASS='TITLE'  >Dining Room:</td>
					<td align='center'><%=DININGROOM%></td>
					<td nowrap CLASS='TITLE'  >WC:</td>
					<td align='center'><%=WC%></td>
				</tr>
				<TR>
					<td nowrap CLASS='TITLE'  >Bathroom:</td>
					<td align='center'><%=BATHROOM%></td>
					<td nowrap CLASS='TITLE'  >Bathroom/WC:</td>
					<td align='center'><%=BATHROOMWC%></td>
					<td nowrap CLASS='TITLE'  >Shower:</td>
					<td align='center'><%=SHOWER%></td>
					<td nowrap CLASS='TITLE'  >Shower/WC:</td>
					<td align='center'><%=SHOWERWC%></td>
				</tr>
				<TR>
					
					<td nowrap CLASS='TITLE'   height="27"  align='left'>Box/Utility&nbsp;Room:</td>
							
					<td align='center'><%=BOXROOMUTILITYROOM%></td>
							
					<td nowrap CLASS='TITLE'    align='left' height="27">Shared&nbsp;Kitchen/Living:</td>
							
					<td align='center'><%=SHAREDKITCHENLIVING%></td>
							
					<td nowrap CLASS='TITLE'   height="27">Shared 
					  WC:</td>
							
					<td align='center'><%=SHAREDWC%></td>
							
					<td nowrap CLASS='TITLE'   height="27"  align='left'>Shared&nbsp;Shower/WC:</td>
							
					<td align='center'><%=SHAREDSHOWERWC%></td>
				</tr>
				<tr>
					<td nowrap CLASS='TITLE'  >Parking:</td>
					<td align='left'><%=PARKING%></td>
					<td nowrap CLASS='TITLE'  >Pets:</td>
					<td align='left'><%=PETS%></td>
					
           			<td nowrap CLASS='TITLE'   title='Square Metres'>Floor&nbsp;Area&nbsp;(sq&nbsp;m):</td>
					<td align='center'><%=FLOORAREA%></td>
					<td nowrap CLASS='TITLE'  >Max People:</td>
					<td align='center'><%=MAXPEOPLE%></td>
				</tr>
			</TABLE>
		</TD>
	</TR>
</form>
</table>
</BODY>
</HTML>	
	
	
	
	
	