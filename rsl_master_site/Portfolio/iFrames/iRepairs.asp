<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, title, path, fullname, property_id, TooExpensive
	Dim lst_location, lst_item, lstContractors, supplier, DisableAll, EmployeeLimit, patch
	Dim repair_text, EstimatedSeconds, DateStamp, DelDate
	Dim VAT, VATTYPE, GROSSCOST, RepairCost, OrderID
	
	property_id = Request("propertyid")
	ForDatabase_PropertyID = "'" & Request("propertyid") & "'"
	OpenDB()
	
	'CHECKS WEHTHER THIS PROPERTY IS UNDER A DEFECTS PERIOD
	SQL = "SELECT DEFECTSPERIOD FROM P__PROPERTY P, P_DEVELOPMENT D " &_
			" WHERE P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			  " AND DEFECTSPERIOD >= GETDATE() AND P.PROPERTYID = '" & propertyid & "'"
	Call OpenRs(rsDefect, SQL)
	if (NOT rsDefect.EOF) then
		CloseRs(rsDefect)
		Response.Redirect "iDefects.asp?customerid=" & Request("customerid") & "&tenancyid=" & Request("tenancyid") & "&Text=The tenants property is still under its defect period"
	end if
	CloseRs(rsDefect)	
	
	path = request.form("hid_go")
	if path  = "" then path = 0 end if
	
	If path Then
		// write new record
		new_record()
	Else
		entry()
	End If
	
	CLoseDB()
	Function entry()
	
		'NEED TO FIND THE EMPLOYEE LIMIT FOR THE USER
		SQL = "SELECT ISNULL(EMPLOYEELIMIT,0) AS EMPLOYEELIMIT FROM E_JOBDETAILS WHERE EMPLOYEEID = " & SESSION("USERID")
		Call OpenRs(rsEmLimit, SQL)
		if (NOT rsEmLimit.EOF) then
			EmployeeLimit = rsEmLimit("EMPLOYEELIMIT")
			DisableAll = ""			
		else
			DisableAll = " disabled"
			EmployeeLimit = -1
		end if
		CloseRs(rsEmLimit)
		get_querystring()
		get_patch()
		CALL BuildSelect(lstContractors, "sel_CONTRACTOR", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' " & DisableAll & "")
		CALL BuildSelect(lst_location, "sel_LOCATION", "R_ZONE", "ZONEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange='select_change(1)' style='width:300px' " & DisableAll & " ")

	End Function

	Function get_patch()
		//THIS SECTION GETS THE PATCH FOR DISPLAY
		str_sql = "SELECT P.PROPERTYID, E.LOCATION FROM P__PROPERTY P LEFT JOIN E_PATCH E ON E.PATCHID = P.PATCH WHERE P.PROPERTYID = '" & Request("propertyid") & "'"
		Call OpenRs(rsSet, str_sql)
		If not rsSet.EOF Then
			patch = rsSet("LOCATION")
		else	
			patch = "UNKNOWN"
		end if
		CloseRs(rsSet)
	End Function

	
	// retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		supplier = Request.Form("sel_CONTRACTOR")
		if (supplier = "") then
			supplier = "NULL"
		end if
		RepairCost = Request.Form("txt_REPAIRCOST")
		if (RepairCost = "" OR NOT isNumeric(RepairCost)) then RepairCost = 0
		RepairCost = Cdbl(RepairCost)
		TooExpensive = CInt(Request.Form("hid_TOOEXPENSIVE"))		
	End Function
	
	Function new_record()
	
		Dim strSQL, journal_id
		get_querystring()

		iAction = 1 'logged
		iStatus = 1 'pending
		//IF ASSIGNING TO A CONTRACTOR THEN CREATE AN APPROPRIATE PURCHASE ORDER
		if (supplier <> "NULL") then
			'if the item is too expensive to assign to a contractor do exactly the same except ceate an inactive purchase order and set the
			'action of the Repair to 12, which is Queued
			if (TooExpensive = 1) then
				iAction = 12 ' queued
				iStatus = 12 ' queued
				PurchaseStatus = 0 'inactive
			else
				'SET THE ACTION TO ASSIGN TO CONTRACTOR 
				iAction = 2 ' assign to contractor
				iStatus = 2 ' assigned
				PurchaseStatus = 1 'active				
			end if

			//GET THE STANDARD INFORMATION
			CALL GetStandardRepairInfo(Request.Form("sel_REPAIR"), RepairCost)

			//CREATE THE PURCHASE ORDER
			Call CreatePurchaseOrder(Request.Form("sel_REPAIR"), get_repair_name(itemnature_id) , repair_text, DateStamp, DelDate, NULL, VATTYPE, RepairCost, VAT, GROSSCOST, 3, 2, Session("USERID"), Supplier, PurchaseStatus)
		else
			'just get the repair text for the main journal entry
			CALL GetRepairText(Request.Form("sel_REPAIR"))
		end if
		//END OF CREATE PURCHASE ORDER

		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (NULL, NULL, " & ForDatabase_PropertyID & ",  " & item_id & ", " & itemnature_id & ", " & iStatus & ", '" & repair_text & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		
		strSQL = 	"INSERT INTO C_REPAIR " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
					"VALUES (" & journal_id & ", " & iStatus & " , " & iAction & ", " & Session("userid") & ", " & Request.Form("sel_REPAIR") &_
					 ", " & supplier & ", '" & title & "', '" & Replace(Request.Form("txt_NOTES"),"'", "''") & "')"
		Conn.Execute(strSQL)
		
		
		//FINALLY CREATE A ROW IN THE C_REPAIRPURCHASE TABLE TO LINK THE REPAIR AND PURCHASE ORDER (IF APPLICABLE)
		if (supplier <> "NULL") then
			SQL = "INSERT INTO C_REPAIRPURCHASE (ORDERID, JOURNALID) VALUES (" & OrderID & ", " & journal_id & ")"
			Conn.Execute(SQL)
		END IF
		//END OF STUFF
		Response.Redirect "iPropertyJournal.asp?propertyid=" & property_id & "&SyncTabs=1"		
	End Function
	
	// GET THE NAME OF THE NATURE OF THE REPAIR, TO BE USED IN THE PURCHASE ORDER 
	Function get_repair_name(int_repair_id)
	
		SQL = "SELECT UPPER(DESCRIPTION) FROM C_NATURE WHERE ITEMNATUREID = " & int_repair_id
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then
			get_repair_name = rsSet(0)
		else
			get_repair_name = "NOT FOUND"
		end if
		
		CloseRs(rsSet)		
		exit function			
			
	End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio -- > New Repair</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "sel_LOCATION|Location|SELECT|Y"
	FormFields[1] = "sel_ITEM|Item|SELECT|Y"
	FormFields[2] = "sel_ELEMENT|Element|SELECT|Y"
	FormFields[3] = "sel_REPAIR|Repair Details|SELECT|Y"
	FormFields[4] = "sel_CONTRACTOR|Contractor|SELECT|N"
	FormFields[5] = "txt_NOTES|Notes|TEXT|N"
	FormFields[6] = "txt_REPAIRCOST|Net Cost(�)|CURRENCY|Y"
	
	function save_form(){
		FormFields[5] = "txt_NOTES|Notes|TEXT|N"
		if (!checkForm()) return false;

		//check wether the net cost has been changed in comparison to the database value,
		//if so require the notes field to be filled in
		NetCost = document.getElementById("txt_REPAIRCOST").value	
		DataCost = document.getElementById("DatabaseCost").value	
		if (parseFloat(NetCost) != parseFloat(DataCost)) FormFields[5] = "txt_NOTES|Notes|TEXT|Y"

		//rerun the check form validation, incase the notes are required
		if (!checkForm()) return false;
		//check the employee is allowed to assign this to a contractor depending on their limit
		EmployeeLimit = "<%=EmployeeLimit%>";
		if (NetCost == "") NetCost = 0
		document.getElementById("hid_TOOEXPENSIVE").value = 0
		if ((parseFloat(EmployeeLimit) < parseFloat(NetCost)) && document.getElementById("sel_CONTRACTOR").value != ""){
			document.getElementById("hid_TOOEXPENSIVE").value = 1
			result = confirm("The repair net cost (�" + FormatCurrency(NetCost) + ") is more than your employee repair limit.\nTherefore this item will be put in a queue for authorisation by a manager.\nIf you wish to continue click on 'OK'.\nOtherwise click on 'CANCEL'.");
			if (!result) return false;
			}

		RSLFORM.hid_go.value = 1;
		RSLFORM.target = "PRM_BOTTOM_FRAME";
		RSLFORM.action = "IREPAIRS.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&propertyid=<%=property_id%>";
		document.getElementById("TheSaveButton").disabled = true
		RSLFORM.submit();
	}

	// source = 1 --> 'LOCATION changed'  = 2 --> 'ITEM changed', 3 --> ELEMENT CHANGED, 4 --> GET PRIORITY
	function select_change(int_source){
	
		RSLFORM.target = "frm_prm_repairs";
		RSLFORM.action = "../serverside/selects_srv.asp?source="+int_source+"&natureid=<%=itemnature_id%>";
		RSLFORM.submit();
	
	}
	
</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0">
<% IF (EmployeeLimit = -1) THEN %>
ERROR: you are not allowed to create repairs as you have no employee limit set. Contact a supervisor immediately.
<% else %>
	<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
<FORM NAME=RSLFORM METHOD=POST>		
		<TR>
		<TD>Location :</td><TD><%=lst_location%></TD>
		<TD><image src="/js/FVS.gif" name="img_LOCATION" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD>Item :</td><TD>
			<select class="textbox200" name="sel_ITEM"  style='width:300px' disabled>
				<OPTION VALUE="">Select Location</option>
			</select></TD>
		<TD><image src="/js/FVS.gif" name="img_ITEM" width="15px" height="15px" border="0"></TD>	
		</TR>
		<TR>
		<TD>Element :</td><TD>
			<select class="textbox200" name="sel_ELEMENT" style='width:300px' disabled>
				<OPTION VALUE="">Select Item</option>
			</select>
		</TD>
		<TD><image src="/js/FVS.gif" name="img_ELEMENT" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD nowrap>Repair Details :</td><TD>
			<select class="textbox200" name="sel_REPAIR" style='width:300px' disabled>
				<OPTION VALUE="">Select Element</option>
			</select></TD>
		<TD><image src="/js/FVS.gif" name="img_REPAIR" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD>Patch :</td><TD><input tabindex=-1 type="text" class="textbox200" name="txt_PATCH" style='width:117px;background-color:beige' readonly value="<%=patch%>">
		Net Cost (�):
		<input type="text" class="textbox200" name="txt_REPAIRCOST" style='width:100px;'>
        <input type="hidden" name="DatabaseCost">
		</TD>
		<TD><image src="/js/FVS.gif" name="img_REPAIRCOST" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD>Contractor</td><TD>
			<%=lstContractors%>
		</TD>
		<TD><image src="/js/FVS.gif" name="img_CONTRACTOR" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD VALIGN=TOP>Notes :
		<!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
		</TD><TD><textarea style='OVERFLOW:HIDDEN;width:300px' class='textbox200' rows=3 name="txt_NOTES" <%=DisableAll%>></textarea>
		</TD>	
		
      <TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
      </TD>
		</TR>
		<TR>
		<TD>Priority :</td><TD><input type="text" tabindex=-1 class="textbox200" name="txt_PRIORITY" style='width:244px;background-color:beige' readonly>
        <input type="button" value=" Save " class="RSLButton"  onClick="save_form()" name="TheSaveButton" <%=DisableAll%>>
        <input type="hidden" name="hid_go" value=0>
        <input type="hidden" name="hid_TOOEXPENSIVE" value="0">				
        <input type="hidden" name="hid_PROPERTYID" value="<%=Request("propertyid")%>">		
      </TD>
		</TR>
	</FORM>
   	</TABLE>
<% end if %>	
<IFRAME src="/secureframe.asp" NAME=frm_prm_repairs WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>

</BODY>
</HTML>	
	
	
	
	
	