<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'THIS PAGE BUILDS THE WORK ORDER JOURNAL FOR TYHE SUPPLIER CRM

	'LAST MODIFIED BY ZANFAR ALI 15 JUNE 2004 AT 17:15 HOURS
	'WORK DONE: TOTAL REWRITE TO TAKE INTO ACCOUNT NEW WORK ORDER FUNCTIONALITY.

	Dim cnt, customer_id, str_journal_table

	'HOLDS THE ID OF THE SELECTED SUPPLIER
	PropertyID = Request("PropertyID")
	nature_id = 47 'Nature Id of Gas Servicing

	SQL = "SELECT PP.DEVELOPMENTID, PP.BLOCKID,ISNULL(PP.PATCH,PD.PATCHID) AS PATCHID FROM P__PROPERTY PP LEFT JOIN P_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID WHERE PP.PROPERTYID = '" & PropertyID & "'"
	Call OpenDB()
	Call OpenRs(rsDEV, SQL)
	    DEVID = rsDEV("DEVELOPMENTID")
	    BLOCKID = rsDEV("BLOCKID")
	    PATCHID= rsDEV("PATCHID")
	If (isNULL(BLOCKID) OR BLOCKID = "") Then BLOCKID = -99
	Call CloseRs(rsDEV)

	If nature_id <> "" Then
		nature_sql = " AND WO.BIRTH_NATURE = " & nature_id  & " "
	Else
		nature_sql = ""
	End If

	'STORES THE CURRENT OPEN WORK ORDER VALUE, WHICHIS CONTINAULLY ROTATED WITH THE MASTER HOLDING CRM PAGE
	CurrentWorkOrder = Request("CWO")

	Call build_work_order()
	Call CloseDB()

	Function build_work_order()
		'THIS PART BUILDS THE MAIN WORK ORDER ROWS
		'IT JOINS THE P_WORKORDERENTITY AND P_WORKORDERMODULE TABLES TO GET RESPECTIVE IMAGES
		'THE FOLLOWING ARE RULES WHICH DEFINE HOW THIS SHOULD WORK.
		'IF THE DEVELOPMENTID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS SCHEME WIDE 
		'IF THE BLOCKID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS BLOCK WIDE
		'IF THE PROPERTYID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS PROPERTY SPECIFIC
		cnt = 0
		strSQL =" SET CONCAT_NULL_YIELDS_NULL OFF; " &_
                " SELECT TOTALCOUNT,BIRTH_MODULE_IMAGE,BIRTH_MODULE_DESCRIPTION,NATURE,WOID,TITLE,WOSTATUSNAME, "&_
	            "        CONVERT(VARCHAR,CREATEDATE,103) AS CREATIONDATE,FULLADDRESS,ACTION,JOURNALID,REDIR,ITEMNATUREID,APPOINTMENTDATE,APPOINTMENT_STATUS "&_
                " FROM  "&_
                " (  "&_ 
				" SELECT JE2.THECOUNT AS TOTALCOUNT, WOM.BIRTH_MODULE_IMAGE, WOM.BIRTH_MODULE_DESCRIPTION,  " &_
				" N.DESCRIPTION AS NATURE, WO.WOID, WO.TITLE, WS.DESCRIPTION AS WOSTATUSNAME, " &_
				" WO.CREATIONDATE AS CREATEDATE, " &_
				" FULLADDRESS = CASE " &_
				" WHEN B.BLOCKID IS NOT NULL THEN 'Block: ' + B.BLOCKNAME " &_
				" WHEN D.DEVELOPMENTID IS NOT NULL THEN ' Development: ' + D.DEVELOPMENTNAME " &_
				" WHEN EP.PATCHID IS NOT NULL THEN ' Patch: ' + EP.LOCATION "&_
				" ELSE P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE " &_
				" END,WOA.ACTION,WOA.JOURNALID,'iServicingDetail.asp' AS REDIR,N.ITEMNATUREID,WOA.APPOINTMENTDATE,WOA.APPOINTMENT_STATUS AS APPOINTMENT_STATUS   " &_
				" FROM P_WORKORDER WO " &_
				" LEFT JOIN P_WORKORDERTOPATCH WOP ON WOP.WOID=WO.WOID "&_
				" LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
				" LEFT JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_
				" LEFT JOIN E_PATCH EP ON EP.PATCHID=WOP.PATCHID "&_
				" LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
				" LEFT JOIN (SELECT COUNT(J.JOURNALID) AS THECOUNT, WTR.WOID FROM C_JOURNAL J INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID WHERE PROPERTYID = '" & PropertyID & "' GROUP BY WTR.WOID) JE ON JE.WOID = WO.WOID " &_
				" LEFT JOIN (SELECT COUNT(J.JOURNALID) AS THECOUNT, WTR.WOID FROM C_JOURNAL J INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID GROUP BY WTR.WOID) JE2 ON JE2.WOID = WO.WOID " &_
                " LEFT JOIN ( "&_
			    "            SELECT CA.DESCRIPTION AS ACTION, "&_
				"	         CASE "&_
				"       		WHEN CRG.LETTERID IS NOT NULL THEN CSL.LETTERDESC "&_
				"       		WHEN CRG.APPOINTMENTID IS NOT NULL THEN CG.DESCRIPTION "&_
				"       		ELSE NULL	"&_
				"        	 END  AS APPOINTMENT_STATUS,W.WOID,CR.JOURNALID,  "&_
				"	         CRG.APPOINTMENTDATE AS APPOINTMENTDATE "&_
			    "           FROM C_REPAIR CR "&_
				"               INNER JOIN P_WOTOREPAIR W ON CR.JOURNALID=W.JOURNALID "&_
				"               INNER JOIN C_ACTION CA ON CA.ITEMACTIONID=CR.ITEMACTIONID "&_
				"               INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
				"               LEFT JOIN C_REPAIRTOGASSERVICING CRG ON CRG.REPAIRHISTORYID=CR.REPAIRHISTORYID "&_
				"               LEFT JOIN C_GASSERVICINGAPPOINTMENT CG ON CG.APPOINTMENTID=CRG.APPOINTMENTID "&_
				"               LEFT JOIN C_LETTERACTION CLA ON CLA.ACTIONID=CRG.LETTERACTIONID "&_
				"               LEFT JOIN C_STANDARDLETTERS CSL ON CSL.LETTERID=CRG.LETTERID "&_
			    "           WHERE CR.REPAIRHISTORYID=( SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID=W.JOURNALID )  "&_
			    "           AND CJ.PROPERTYID =  '" & PropertyID & "' "&_
			    "               GROUP BY CRG.APPOINTMENTDATE ,CA.DESCRIPTION,W.WOID,CR.JOURNALID, "&_
			    "                    CASE  "&_
			    "	    			    WHEN CRG.LETTERID IS NOT NULL THEN CSL.LETTERDESC "&_
				"   			    	WHEN CRG.APPOINTMENTID IS NOT NULL THEN CG.DESCRIPTION   "&_
				"		        		ELSE NULL	 "&_
				"	    		     END "&_
			    "           ) WOA ON WOA.WOID=WO.WOID "&_
				"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS " &_
				"INNER JOIN P_WORKORDERENTITY WOE ON WOE.BIRTH_ENTITY_ID = WO.BIRTH_ENTITY " &_
				"INNER JOIN P_WORKORDERMODULE WOM ON WOM.BIRTH_MODULE_ID = WO.BIRTH_MODULE " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = WO.BIRTH_NATURE " &_
				"WHERE (WO.PROPERTYID = '" & PropertyID & "' OR (WO.DEVELOPMENTID = " & DEVID & " AND JE.THECOUNT > 0) OR (WO.BLOCKID = " & BLOCKID & " AND JE.THECOUNT > 0) OR (WOP.PATCHID = " & PATCHID & " AND JE.THECOUNT > 0)) " & nature_sql &_
                " UNION "&_
                " SELECT 0 AS TOTALCOUNT,'ic_gas' AS BIRTH_MODULE_IMAGE,'Gas Servicing' AS BIRTH_MODULE_DESCRIPTION,'Annual Gas Servicing' AS NATURE,0000 AS WOID,'Annual Gas Servcing' AS TITLE, "&_
	            "        CS.DESCRIPTION AS  WOSTATUSNAME,GJ.CREATIONDATE  AS CREATEDATE, "&_
	            "        P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE AS FULLADDRESS,'' AS ACTION,GJ.JOURNALID,'iServicingDetailNew.asp' AS REDIR, "&_
	            "        GJ.ITEMNATUREID AS ITEMNATUREID,GJH.APPOINTMENTDATE AS APPOINTMENTDATE,  "&_
                "        CASE "&_
			    "             WHEN  GJH.LETTERID IS NOT NULL THEN CSL.LETTERDESC  "&_
                "             WHEN GJH.LETTERACTIONID IS NOT NULL THEN CLA.DESCRIPTION "&_
			    "            ELSE 'Appointment TBC'   "&_
		        "        END AS APPOINTMENT_STATUS "&_
                " FROM P_GASJOURNAL GJ "&_
	            "      INNER JOIN P_GASJOURNALHISTORY GJH ON GJ.JOURNALID=GJH.JOURNALID "&_
	            "      INNER JOIN dbo.C_STATUS CS ON CS.ITEMSTATUSID=GJ.CURRENTITEMSTATUSID "&_
	            "      INNER JOIN dbo.P__PROPERTY P ON P.PROPERTYID=GJ.PROPERTYID "&_
	            "      LEFT JOIN C_GASSERVICINGAPPOINTMENT CG ON CG.APPOINTMENTID=GJH.APPOINTMENTID  "&_
                "      LEFT JOIN C_STANDARDLETTERS CSL ON CSL.LETTERID=GJH.LETTERID "&_
                "      LEFT JOIN C_LETTERACTION CLA ON CLA.ACTIONID=GJH.LETTERACTIONID  "&_
                " WHERE GJH.GASHISTORYID=(SELECT MAX(GASHISTORYID)  FROM P_GASJOURNALHISTORY WHERE JOURNALID=GJ.JOURNALID) "&_
                " AND GJ.PROPERTYID= '" & PropertyID & "' " &_
                ") GS "&_
                " ORDER BY GS.CREATEDATE DESC "

		Call OpenRs (rsSet, strSQL)
		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			WOID = rsSet("WOID")

			'THE IMAGE TAG IS BUILT HERE, WILL ALWAYS CONTAIN THREEE IMAGES
			ImageTD = "<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("BIRTH_MODULE_IMAGE") & ".gif' title='" & rsSet("BIRTH_MODULE_DESCRIPTION") & "' width='16' height='18' border=0></td>" 

			AddressTitle = ""
			THEADDRESS = rsSet("FULLADDRESS")
			If (Len(THEADDRESS) > 40) Then
				AddressTitle = " title=""" & THEADDRESS & """"
				THEADDRESS = Left(THEADDRESS, 40) & "..."
			End If

			'HERE WE HAVE CHECK IF THE ITEM HAS BEEN SELECTED IN WHICH CASE WE SHOW THE RESPECTIVE REPAIRS FOR THE ITEM
			'THIS PART BUILDS THE STANDARD WORK ORDER LINE WHICH IS CLOSED...
				str_journal_table = str_journal_table & 	"<tr onclick='open_me(" & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",""" & rsSet("REDIR") & """)' style='cursor:pointer;'>" &_
				                                                ImageTD &_
																"<td><b>" & rsSet("CREATIONDATE") & "</b></td>" &_
																"<td><b>" & GSWorkNumber(rsSet("WOID")) & "</b></td>" &_
																"<td><b>" & rsSet("NATURE") & "</b></td>" &_
																"<td><b>" & rsSet("APPOINTMENTDATE") & "</b></td>" &_ 
																"<td><b>" & rsSet("APPOINTMENT_STATUS") &"</b></td>" &_
																"<td><b>" & rsSet("WOSTATUSNAME")  & "</b></td>" &_
																"<td><b>" & rsSet("ACTION")  & "</b></td>" &_
															"<tr>"

			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""8"" align=""center"">No repair work order entries exist.</td></tr></tfoot>"
		End If

	End Function

	Function GSWorkNumber(strWord)
		GSWorkNumber = "<b>GO</b> " & characterPad(strWord,"0", "l", 7)
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
    body
    {
        background-color: White;
        margin: 0px 0px 0px 0px;
    }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
    //THIS FUNCTION OPENS UP THE RESPECTIVE REPAIRS JOURNAL
    function open_me(int_nature, int_journal_id, str_redir) {
        parent.swap_div(15, "BOTTOM")
        location.href = str_redir + "?journalid=" + int_journal_id + "&natureid=" + int_nature;
    }
</script>
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM');">
    <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(../../Includes/Tables/tablehl.htc);
        border-collapse: collapse" slcolor='' border="0" hlcolor="#4682b4">
        <thead>
            <tr valign="top">
                <td style="border-bottom: 1px solid" width="34" nowrap="nowrap">
                </td>
                <td style="border-bottom: 1px solid" width="80" nowrap="nowrap">
                    <b><font color="blue">Date</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="80" nowrap="nowrap">
                    <b><font color="blue">WO Number</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="130" nowrap="nowrap">
                    <b><font color="blue">Nature</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="89" nowrap="nowrap">
                    <b><font color="blue">Appointment</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">Action</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="89" nowrap="nowrap">
                    <b><font color="blue">Status</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b><font color="blue">Sub Status</font></b>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="6">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
