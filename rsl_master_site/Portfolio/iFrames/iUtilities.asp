<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWaterName,strCouncDesc,strElecDesc,strGasDesc,intTaxPay,intWaterMeter	
	PropertyID = Request("PropertyID")
	if (PropertyID = "") then PropertyID = -1 end if
	
	OpenDB()
	
	ACTION = "NEW"										
	SQL = 	"	SELECT isnull(COUNCILTAXPAYABLE,0) as F_COUNCILTAXPAYABLE, WATERMETERNUMBER , "&_
			" 		ISNULL( SO.NAME , '[None Selected]') AS WATERNAME ,"&_
			" 		ISNULL( PC.DESCRIPTION , '[None Selected]') AS COUNCILDESC ,"&_
			" 		ISNULL( SOE.NAME , '[None Selected]') AS ELECDESC,"&_
			" 		ISNULL( SOG.NAME , '[None Selected]') AS GASDESC "&_
			"	FROM P_UTILITIES PU "&_
			"		LEFT JOIN S_ORGANISATION SO ON SO.ORGID = PU.WATERAUTHORITY "&_
			"		LEFT JOIN P_COUNCILTAXBAND PC ON PC.CID = PU.COUNCILTAXBAND "&_
			"		LEFT JOIN S_ORGANISATION SOE ON SOE.ORGID = PU.ELECTRICITYSUPPLIER"&_
			"		LEFT JOIN S_ORGANISATION SOG ON SOG.ORGID = PU.GASSUPPLIER "&_
			"	WHERE PROPERTYID = '" & PropertyID & "'"
	//Response.Write SQL
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then
		strWaterName 	= rsLoader("WATERNAME")
		strCouncDesc 	= rsLoader("COUNCILDESC")
		strElecDesc 	= rsLoader("ELECDESC")
		strGasDesc 		= rsLoader("GASDESC")
		
		intTaxPay 		= FormatNumber(rsLoader("F_COUNCILTAXPAYABLE"),2,-1,0,0)
		intWaterMeter 	= rsLoader("WATERMETERNUMBER")
									
	end if

	CloseRs(rsLoader)

	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">

<TABLE WIDTH='750px' HEIGHT='180px' STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD valign='top'>
			<TABLE  width='100%' height='100%' STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLSPACING=1 cellpadding=1 border=1 CLASS="TAB_TABLE">
				<!-- CONTENTS -->
				<tr> 
				 	<td Class='TITLE' nowrap  style='width:200px'>Water Authority:</td>
				  	<td  ><%=strWaterName%></td>
				</tr>
	  			<tr>
					<td Class='TITLE' nowrap >Electrical&nbsp;Supplier:</td>
					<td  ><%=strElecDesc%></td>
					
	  			</tr>
	  			<tr> 
					<td Class='TITLE' nowrap >Council Tax Band:</td>
     				<td><%=strCouncDesc%></td>
				</tr>
	  			<tr>
					<td Class='TITLE' nowrap >Gas Supplier:</td>
					<td><%=strGasDesc%></td>
				</tr>
	  			<tr>
					
            <td Class='TITLE' nowrap >Council&nbsp;Tax&nbsp;Payable&nbsp;:</td>
			 		<td><%=formatcurrency(intTaxPay)%></td>
				</tr>
	  			<tr>
					<td Class='TITLE' nowrap>Water Meter No:</td>
					<td><%=intWaterMeter%></td>
				</tr>
				<tr>
					
            <td height='35px' colspan=2>&nbsp;</td>
				</tr>
				
				
	 			<!-- \/\/\/\/\ -->
			</TABLE>
		</TD>
	</TR>
</form>
</table>
</BODY>
</HTML>
	
	
	
	
	