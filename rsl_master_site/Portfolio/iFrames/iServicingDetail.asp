<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id, Repair_Title, last_status, isDisabled, ButtonValue
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		
		strSQL = "  SELECT ISNULL(CONVERT(NVARCHAR, CR.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), CR.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, "&_
				 "      CASE  "&_
				 "	        WHEN CRG.LETTERACTIONID IS NOT NULL THEN CLA.DESCRIPTION "&_
				 "	        WHEN CRG.APPOINTMENTID IS NOT NULL THEN CG.DESCRIPTION  "&_
				 "	        ELSE CA.DESCRIPTION "&_
				 "      END AS STAGE, "&_
				 "      CASE "&_
				 "	        WHEN  CRG.LETTERID IS NOT NULL THEN CSL.LETTERDESC "&_
				 "          WHEN CRG.APPOINTMENTID=9 THEN CR.TITLE "&_
				 "	        ELSE 'Appointment TBC'   "&_
				 "      END AS DOCUMENT_DESC,ISNULL(CR.NOTES,'N/A') AS NOTES,SO.NAME AS CONTRACTOR,"&_ 
				 "	    CRG.APPOINTMENTDATE AS APPOINTMENTDATE, "&_
				 "      CS.DESCRIPTION AS STATUS,CA.ITEMACTIONID,CR.REPAIRHISTORYID, "&_
				 "      ISNULL(CRG.LETTERID,0) AS LETTERID "&_
                 "  FROM C_REPAIR CR "&_
	             "      LEFT JOIN C_REPAIRTOGASSERVICING CRG ON CRG.REPAIRHISTORYID=CR.REPAIRHISTORYID "&_
	             "      LEFT JOIN C_GASSERVICINGAPPOINTMENT CG ON CG.APPOINTMENTID=CRG.APPOINTMENTID "&_
	             "      LEFT JOIN C_LETTERACTION CLA ON CLA.ACTIONID=CRG.LETTERACTIONID "&_
	             "      LEFT JOIN C_STANDARDLETTERS CSL ON CSL.LETTERID=CRG.LETTERID  "&_
	             "      LEFT JOIN C_ACTION CA ON CA.ITEMACTIONID=CR.ITEMACTIONID "&_
	             "      LEFT JOIN S_SCOPE S ON S.SCOPEID=CR.SCOPEID "&_
	             "      LEFT JOIN S_ORGANISATION SO ON SO.ORGID=S.ORGID "&_	
	             "     	LEFT JOIN C_STATUS CS ON CR.ITEMSTATUSID = CS.ITEMSTATUSID  " &_    
                 "  WHERE JOURNALID=" & journal_id & " "&_
                 "  ORDER BY CR.REPAIRHISTORYID DESC " 
		        			
		
		'response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		last_action = -1
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY' VALIGN=TOP>"
			else
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				Repair_Title = "Annual Gas Servicing"
				repair_history_id = rsSet("REPAIRHISTORYID")
				last_status = rsSet("STATUS")
				last_action = rsSet("ITEMACTIONID")
			End If
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("STAGE") & "</TD>" &_
															"<TD>" & rsSet("DOCUMENT_DESC") & "</TD>" &_
															"<TD>" & rsSet("APPOINTMENTDATE")  & "</TD>" &_
															"<TD>" & rsSet("NOTES")  & "</TD>" &_
															"<TD>" & rsSet("CONTRACTOR")  & "</TD>" 

            	If rsSet("LETTERID") <> 0 Then 
					str_journal_table = str_journal_table & "<TD><img src='../Images/attach_letter.gif' style='cursor:hand' title='Open Letter' onclick='open_letter("& rsSet("REPAIRHISTORYID") & "," & rsSet("LETTERID") &")'></TD>"
				Else
					str_journal_table = str_journal_table & "<TD> </TD>"
				End If														
				str_journal_table = str_journal_table &  "<TR>"										
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
		ButtonValue = " Update "
		isDisabled = ""
		SQL = "SELECT * FROM C_ACTION WHERE ITEMACTIONID = " & last_action
		Call OpenRs(rsCheckOption,SQL)
		if (NOT rsCheckOption.EOF) then
'			if (rsCheckOption("EMPLOYEEONLY") = "" OR ISNULL(rsCheckOption("EMPLOYEEONLY"))) then
			if ( (rsCheckOption("EMPLOYEEONLY") = "" OR ISNULL(rsCheckOption("EMPLOYEEONLY"))) AND (rsCheckOption("CONTRACTORONLY") = "" OR ISNULL(rsCheckOption("CONTRACTORONLY"))) ) then
				isDisabled = " disabled"
				ButtonValue = " No Options "
			end if
		end if
		CloseRs(rsCheckOption)
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function update_repair(int_repair_history_id){
		var str_win
		
		str_win = "../PopUps/pGasServicing.asp?repairhistoryid="+int_repair_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=320, left=200,top=200") ;
	}
   function open_letter(repairhistoryid,letter_id){
		
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/pView_letter_preview.asp?repairhistoryid="+ repairhistoryid + "&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
		
	}	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="parent.synchronize_tabs(14,'BOTTOM');parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=6><table cellspacing=0 cellpadding=1 width=100%><tr valign=top><td width=100%><b>Repair Information:</b>&nbsp;<%=Repair_Title%></TD>
	<TD nowrap>Current Status:&nbsp;<font color=red><%=last_status%></font>&nbsp;</td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonValue%>" style="background-color:beige" onclick='update_repair(<%=repair_history_id%>)' <%=isDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Date</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Stage</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Document</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Appointment Date</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Notes</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID">Contractor</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	