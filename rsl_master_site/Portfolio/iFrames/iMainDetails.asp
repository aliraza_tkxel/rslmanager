<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_DIMS = "width=""750"" height=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim pd_propertyref
	Dim pd_devname
	Dim pd_schemename
	Dim pd_patch
	Dim pd_add1
	Dim pd_add2
	Dim pd_add3
	Dim pd_towncity
	Dim pd_county
	Dim pd_postcode
	Dim pd_status
	Dim pd_substatus
	Dim pd_avdate
	Dim pd_propertytype
	Dim pd_assettype
	Dim pd_datebuilt
	Dim pd_righttobuy
	Dim pd_int_status
	Dim str_tenant
    Dim str_tenantId
	Dim pd_ownership, pd_fueltype

    property_id = Request("propertyid")

	Call OpenDB()
	Call get_main_details()
	' CREATE QUICK LINK TO TENANT DETAILS ONLY IF PROPERTY STATUS IS LET
	If pd_int_status = 2 Then get_tenant_details() End If
	Call CloseDB()

	Function get_tenant_details()

		Dim strSQL
		strSQL = 	"SELECT DISTINCT LTRIM(ISNULL(TIT.DESCRIPTION, '') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS FULLNAME, " &_
					"		C.CUSTOMERID, T.TENANCYID " &_
					"FROM 	P__PROPERTY P " &_
					"		LEFT JOIN C_TENANCY T ON P.PROPERTYID = T.PROPERTYID " &_
					"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"		LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
					"		LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " &_
					"		LEFT JOIN C_ADDITIONALASSET A ON A.TENANCYID = T.TENANCYID AND (A.ENDDATE > GETDATE() OR A.ENDDATE IS NULL) " &_
					"WHERE 	(P.PROPERTYID = '" & property_id & "' OR A.PROPERTYID = '" & property_id & "') " &_
					"		   AND (  (CT.ENDDATE > GETDATE() OR CT.ENDDATE IS NULL ) " &_
					"			 AND (T.ENDDATE > GETDATE() OR T.ENDDATE IS NULL)  ) "

		Call OpenRs (rsSet,strSQL)
		str_tenant = "("
		While Not rsSet.EOF
			str_tenant = str_tenant & "<span style=""cursor:pointer"" title=""Take me there"" onclick=""parent.location.href='../../Customer/CRM.asp?CustomerID=" & rsSet("CUSTOMERID") & "'"" >" & rsSet("FULLNAME") & "</span>"
			rsSet.Movenext()
			IF NOT rsSet.EOF Then str_tenant = str_tenant & " " end if
		Wend
		str_tenant = str_tenant & " )"
		Call CloseRs(rsSet)

	End Function

	' get employee details
	Function get_main_details()

		Dim strSQL
		strSQL = 	"SELECT	ISNULL(P.PROPERTYID, '') AS PROPERTYREF, " &_
					"		ISNULL(P.STATUS, '') AS INTSTATUS, " &_
					"		ISNULL(D.DEVELOPMENTNAME, '') AS DEVELOPMENTNAME, " &_
					"		ISNULL(D.SCHEMENAME, '') AS SCHEMENAME, " &_
					"		ISNULL(PA.LOCATION, '') AS PATCH, " &_
					"		CASE WHEN HOUSENUMBER IS NULL THEN  " &_
					"		   LTRIM(ISNULL(FLATNUMBER,'')	+ ' ' + ISNULL(P.ADDRESS1,'')) " &_
					"	 	ELSE " &_
					"		   LTRIM(ISNULL(HOUSENUMBER,'')	+ ' ' + ISNULL(P.ADDRESS1,'')) " &_
					"		END AS ADD1, " &_
					"		ISNULL(P.ADDRESS2 ,'') AS ADD2, " &_
					"		ISNULL(ADDRESS3 ,'') AS ADD3, " &_
					"		ISNULL(TOWNCITY ,'') AS TOWNCITY, " &_
					"		ISNULL(P.COUNTY ,'') AS COUNTY, " &_
					"		ISNULL(P.POSTCODE ,'') AS POSTCODE, " &_
					"		ISNULL(S.DESCRIPTION ,'') AS STATUS, " &_
					"		ISNULL(SS.DESCRIPTION ,'') AS SUBSTATUS, " &_
					"		ISNULL(OWN.DESCRIPTION,'') AS OWNERSHIP, " &_
					"		ISNULL(CONVERT(NVARCHAR, P.AVDATE, 103) ,'') AS AVDATE, " &_
					"		ISNULL(PT.DESCRIPTION ,'') AS PROPERTYTYPE, " &_
					"		ISNULL(A.DESCRIPTION ,'') AS ASSETTYPE, " &_
					"		ISNULL(CONVERT(NVARCHAR, P.DATEBUILT, 103) ,'') AS DATEBUILT, " &_
					"		CASE WHEN RIGHTTOBUY = 1 THEN 'Yes' ELSE 'No' END AS RIGHTTOBUY,F.FUELTYPE as FT " &_
					"FROM 	P__PROPERTY P " &_
					"		LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
					"		LEFT JOIN E_PATCH PA ON D.PATCHID = PA.PATCHID " &_
					"		LEFT JOIN P_STATUS S ON P.STATUS = S.STATUSID " &_
					"		LEFT JOIN P_SUBSTATUS SS ON SS.SUBSTATUSID = P.SUBSTATUS " &_
					"		LEFT JOIN P_PROPERTYTYPE PT ON P.PROPERTYTYPE = PT.PROPERTYTYPEID " &_
					"		LEFT JOIN P_ASSETTYPE A ON P.ASSETTYPE = A.ASSETTYPEID " &_
					"		LEFT JOIN P_OWNERSHIP OWN ON P.OWNERSHIP = OWN.OWNERSHIPID " &_
                    "		LEFT JOIN P_FUELTYPE F ON F.FUELTYPEID = P.FUELTYPE " &_
					"WHERE 	PROPERTYID = '" & property_id & "'"

		Call OpenRs (rsSet,strSQL)

		If not rsSet.EOF Then
			' get property details
			pd_propertyref	= rsSet("PROPERTYREF")
			pd_devname		= rsSet("DEVELOPMENTNAME")
			pd_schemename	= rsSet("SCHEMENAME")
			pd_patch		= rsSet("PATCH")
			pd_add1			= rsSet("ADD1")
			pd_add2			= rsSet("ADD2")
			pd_add3			= rsSet("ADD3")
			pd_towncity		= rsSet("TOWNCITY")
			pd_county		= rsSet("COUNTY")
			pd_postcode		= rsSet("POSTCODE")
			pd_status		= rsSet("STATUS")
			pd_substatus	= rsSet("SUBSTATUS")
			pd_avdate		= rsSet("AVDATE")
			pd_propertytype	= rsSet("PROPERTYTYPE")
			pd_assettype	= rsSet("ASSETTYPE")
			pd_datebuilt	= rsSet("DATEBUILT")
			pd_righttobuy	= rsSet("RIGHTTOBUY")
			pd_int_status	= rsSet("INTSTATUS")
			pd_ownership	= rsSet("OWNERSHIP")
            pd_fueltype     = rsSet("FT")
		Else
			pd_propertyref = "<b>Property Not found !!!!</b>"
		End If

		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <title>RSLmanager > Portfolio Module</title>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/Loading.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP');" class="TA">
    <table width="750" height="180" style="border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td rowspan="2" width="70%" height="100%">
                <table height="100%" width="100%" style="border: solid 1px #133e71; border-collapse: collapse"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td class="TITLE" nowrap="nowrap">
                            Property Ref No
                        </td>
                        <td width="220" nowrap="nowrap">
                            <%=pd_propertyref%>
                            ( <a href="http://www.upmystreet.com/l/<%=pd_postcode%>.html" target="_blank">Up My
                                Street</a> )
                        </td>
                        <td class="TITLE" nowrap="nowrap">
                            Address
                        </td>
                        <td width="300" title="<%=pd_ethnicity%>">
                            <%=pd_add1%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            Dev. Name
                        </td>
                        <td>
                            <%=pd_devname%>
                        </td>
                        <td class="TITLE">
                        </td>
                        <td>
                            <%=pd_add2%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            Scheme Name
                        </td>
                        <td>
                            <%=pd_schemename%>
                        </td>
                        <td class="TITLE">
                        </td>
                        <td>
                            <%=pd_add3%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Status
                        </td>
                        <td width="160">
                            <%=pd_status%>&nbsp;&nbsp<font style='color: BLUE'><%=str_tenant%></font>
                        </td>
                        <td class='TITLE'>
                        </td>
                        <td>
                            <%=pd_towncity%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Sub Status
                        </td>
                        <td>
                            <%=pd_substatus%>
                        </td>
                        <td class='TITLE'>
                        </td>
                        <td>
                            <%=pd_county%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Property Type
                        </td>
                        <td>
                            <%=pd_propertytype%>
                        </td>
                        <td class='TITLE'>
                        </td>
                        <td>
                            <%=pd_postcode%>
                        </td>
                    </tr>
                    <tr>
                        <td class='TITLE'>
                            Asset Type
                        </td>
                        <td>
                            <%=pd_assettype%>
                        </td>
                        <td class='TITLE'>
                            Right To Acquire
                        </td>
                        <td>
                            <%=pd_righttobuy%>
                        </td>
                    </tr>
                    <tr>
                       <!-- <td class='TITLE'>
                            Fuel Type
                        </td>
                        <td>
                            <%'=pd_fueltype%>
                        </td> -->
                        <td class='TITLE'>
                            Confirmation of Ownership
                        </td>
                        <td>
                            <%=pd_ownership%>
                        </td>
						 <td class='TITLE'>
                            
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
