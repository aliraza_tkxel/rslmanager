<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	//THIS PAGE BUILDS THE WORK ORDER JOURNAL FOR TYHE SUPPLIER CRM
	
	//LAST MODIFIED BY ZANFAR ALI 15 JUNE 2004 AT 17:15 HOURS
	//WORK DONE: TOTAL REWRITE TO TAKE INTO ACCOUNT NEW WORK ORDER FUNCTIONALITY.
	
	Dim cnt, customer_id, str_journal_table
	
	//HOLDS THE ID OF THE SELECTED SUPPLIER
	DEVID = Request("DEVID")
	nature_id = Request("natureid")
	
	if nature_id <> "" Then 
		nature_sql = " AND WO.BIRTH_NATURE = " & nature_id  & " " 
			else
				nature_sql = "" 
					end if
	
	//STORES THE CURRENT OPEN WORK ORDER VALUE, WHICHIS CONTINAULLY ROTATED WITH THE MASTER HOLDING CRM PAGE
	CurrentWorkOrder = Request("CWO")

	OpenDB()
	build_work_order()
	CloseDB()

	Function build_work_order()
		//THIS PART BUILDS THE MAIN WORK ORDER ROWS
		//IT JOINS THE P_WORKORDERENTITY AND P_WORKORDERMODULE TABLES TO GET RESPECTIVE IMAGES
		//THE FOLLOWING ARE RULES WHICH DEFINE HOW THIS SHOULD WORK.
		//IF THE DEVELOPMENTID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS SCHEME WIDE 
		//IF THE BLOCKID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS BLOCK WIDE
		//IF THE PROPERTYID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS PROPERTY SPECIFIC
		cnt = 0
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WOM.BIRTH_MODULE_IMAGE, WOM.BIRTH_MODULE_DESCRIPTION, WOE.BIRTH_ENTITY_IMAGE, WOE.BIRTH_ENTITY_DESCRIPTION, " &_
				"N.DISPLAYIMAGE, WO.ORDERID, WO.WOID, WO.TITLE, WS.DESCRIPTION AS WOSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, WO.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), WO.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
				"FULLADDRESS = CASE " &_
				"WHEN B.BLOCKID IS NOT NULL THEN 'Block: ' + B.BLOCKNAME " &_
				"WHEN D.DEVELOPMENTID IS NOT NULL THEN ' Development: ' + D.DEVELOPMENTNAME " &_
				"ELSE P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE " &_
				"END " &_
				"FROM P_WORKORDER WO " &_
				"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_									
				"LEFT JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_									
				"LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_													
				"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS " &_
				"INNER JOIN P_WORKORDERENTITY WOE ON WOE.BIRTH_ENTITY_ID = WO.BIRTH_ENTITY " &_				
				"INNER JOIN P_WORKORDERMODULE WOM ON WOM.BIRTH_MODULE_ID = WO.BIRTH_MODULE " &_
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = WO.BIRTH_NATURE " &_												
				"WHERE WO.DEVELOPMENTID = '" & DEVID & "' " & nature_sql &_
				"ORDER BY WO.WOID DESC"
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			WOID = rsSet("WOID")
			
			'THE IMAGE TAG IS BUILT HERE, WILL ALWAYS CONTAIN THREEE IMAGES
			ImageTD = "<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("BIRTH_MODULE_IMAGE") & ".gif' title='" & rsSet("BIRTH_MODULE_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
						"<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("BIRTH_ENTITY_IMAGE") & ".gif' title='" & rsSet("BIRTH_ENTITY_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
						"<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("DISPLAYIMAGE") & ".gif' title=""" & rsSet("TITLE") & """ width='29' height='18' border=0></TD>"						
			
			AddressTitle = ""
			THEADDRESS = rsSet("FULLADDRESS")
			if (Len(THEADDRESS) > 40) THEN
				AddressTitle = " title=""" & THEADDRESS & """"
				THEADDRESS = Left(THEADDRESS, 40) & "..."
			end if

			//HERE WE HAVE CHECK IF THE ITEM HAS BEEN SELECTED IN WHICH CASE WE SHOW THE RESPECTIVE REPAIRS FOR THE ITEM
			if ( (CStr(CurrentWorkOrder) = CStr(WOID)) ) then
				//ON THE STRING THAT APPEARS NEXT YOU WILL SEE THAT AN <A> TAG HAS BEEN ENTERED THIS IS SO THAT THE FRAME
				//CAN BE SCROLLED TO THIS WORK ORDER ON LOAD....
				str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND;'><A NAME=""WOID"">" &_
																"<TD style='background-color:white'><img src='/js/tree/img/minus.gif' width=18 height=18 border=0></TD>" &_
																ImageTD &_
																"<TD><b>" & rsSet("CREATIONDATE") & "</b></TD>" &_
																"<TD><b>" & WorkNumber(rsSet("WOID")) & "</b></TD>" &_
																"<TD " & AddressTitle & "><b>" & THEADDRESS & "</b></TD>" &_			
																"<TD><b>" & rsSet("WOSTATUSNAME")  & "</b></TD>" &_
															"<TR>"
				//CHECK WHEHTER THE ITEM HAS BEEN CLICKED ON AGAIN IN WHICH CASE WE CLOSE THE ITEM
				if (Request("CloseWO") <> "1") then
					BuildRepairList(WOID)	//SHOW THE APPROPRIATE REPAIR ROWS IF APPLICABLE
				else
				//RESET THE CURRENTWORKORDER VARIABLE AS IT HAS BEEN CLOSED
					CurrentWorkOrder = ""
				end if
			else
				//THIS PART BUILDS THE STANDARD WORK ORDER LINE WHICH IS CLOSED...
				str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND;'>" &_
																"<TD style='background-color:white'><img src='/js/tree/img/plus.gif' width=18 height=18 border=0></TD>" &_
																ImageTD &_
																"<TD><b>" & rsSet("CREATIONDATE") & "</b></TD>" &_
																"<TD><b>" & WorkNumber(rsSet("WOID")) & "</b></TD>" &_
																"<TD " & AddressTitle & "><b>" & THEADDRESS & "</b></TD>" &_																			
																"<TD><b>" & rsSet("WOSTATUSNAME")  & "</b></TD>" &_
															"<TR>"
			end if
			
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=8 ALIGN=CENTER>No repair work order entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function

	//THIS FUNCTION BUILDS THE ACTUAL REPAIR ITEMS FOR THE SELECTED WORK ORDER
	Function BuildRepairList(WOID)
		cnt = 0
		//HUMONGOUS SQL WHICH DOES SOMETHING.....
		//INCLUDING GETTING THE REDIRECT PAGE FOR EACH SELECTED ITEM....
		
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF;" &_
					"SELECT 	J.JOURNALID, J.CURRENTITEMSTATUSID, " &_
					"			CASE WHEN J.ITEMNATUREID = 1 THEN 'iDefectsDetail.asp' " &_
					"			WHEN J.ITEMNATUREID IN (2,20,21,22,34,35,36,37,38,39,40) THEN 'iRepairDetailDev.asp'  " &_		
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE AS FULLADDRESS, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN J.PROPERTYID IS NULL AND LEN(J.TITLE) > 80 THEN LEFT(J.TITLE,80) + '...' " &_
					"			WHEN J.PROPERTYID IS NOT NULL AND LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, J.PROPERTYID, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			J.ITEMNATUREID " &_
					"FROM	 	C_JOURNAL J " &_
					"			INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_
					"			INNER JOIN P_WORKORDER WO ON WTR.WOID = WO.WOID " &_
					"			LEFT JOIN P__PROPERTY P ON J.PROPERTYID = P.PROPERTYID " &_																			
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"WHERE	 	J.ITEMNATUREID IN (1,2,20,21,22,34,35,36,37,38,39,40) " &_
					"			AND	WO.WOID = " & WOID & " " &_					
					"ORDER BY WTR.ORDERITEMID DESC"
		//response.write strSQL
		Call OpenRs (rsSet2, strSQL) 
		
		While Not rsSet2.EOF
			
			cnt = cnt + 1

			TRSTYLE = ""
			if (rsSet2("CURRENTITEMSTATUSID") = 5) then
				TRSTYLE = ";color:red;text-decoration:line-through"
			end if			
			
			//WE CHECK IF THE EACH REPAIR JOURNAL HAS AN ADDRESS IN WHICH CASE
			//WE DISPLAY THE ADDRESS FOR EACH INDIVIDUAL REPAIR
			if (rsSet2("PROPERTYID") <> "") THEN

				//THIS PART GETS THE ADDRESS AND CHECKS WHETHER IT IS TOO LONG IN WHICH CASE IT IS REDUCED IN LENGTH AND A TITLE ADDED
				//TO THE RESPECTIVE TD
				AddressTitle = ""
				THEADDRESS = rsSet2("FULLADDRESS")
				if (Len(THEADDRESS) > 35) THEN
					AddressTitle = " title=""" & THEADDRESS & """"
					THEADDRESS = Left(THEADDRESS, 35) & "..."
				end if
							
				str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet2("ITEMNATUREID") & "," & rsSet2("JOURNALID") & ",""" & rsSet2("REDIR") & """)' STYLE='CURSOR:HAND" & TRSTYLE & "'>" &_
															"<TD colspan=4 style='background-color:white'><img src='/js/tree/img/joinlong.gif' width=84 height=18 border=0></TD>" &_
															"<TD colspan=2 " & AddressTitle & ">&nbsp;&nbsp;" & THEADDRESS & "</TD>" &_
															"<TD TITLE=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE")  & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</TD>" &_
														"<TR>"
			else			

				str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet2("ITEMNATUREID") & "," & rsSet2("JOURNALID") & ",""" & rsSet2("REDIR") & """)' STYLE='CURSOR:HAND" & TRSTYLE & "'>" &_
															"<TD colspan=4 style='background-color:white'><img src='/js/tree/img/joinlong.gif' width=84 height=18 border=0></TD>" &_
															"<TD colspan=3 TITLE=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE")  & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</TD>" &_
														"<TR>"
			end if											
			rsSet2.movenext()
			
		Wend
		CloseRs(rsSet2)
		
		IF cnt = 0 then
			str_journal_table = str_journal_table & "<TR><TD COLSPAN=9 ALIGN=CENTER>No repair item entries exist.</TD></TR>"
		End If	

	End Function	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iRepairJournalDev.asp?DEVID=<%=DEVID%>&CWO=" + WOID + "&natureid=<%=nature_id%>"
	}

	//THIS FUNCTION OPENS UP THE RESPECTIVE REPAIRS JOURNAL
	function open_me(int_nature, int_journal_id, str_redir){
		parent.swap_div(70, "BOTTOM")
		location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
	}

	//THIS FUNCTION UPDATES THE CURRENTPURCHASEORDER VARIABLE IN THE PARENT HOLDING CRM
	//IT THEN MAKES THE FRAME SCROLL TO THE TOP OF THE CURRENT SELECTED WORK ORDER...		
	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1") 
			parent.synchronize_tabs(70,"BOTTOM")
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		location.href = "#WOID"		
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM');">

	<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=20></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=34 NOWRAP></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap><B><FONT COLOR='BLUE'>Date</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap><B><FONT COLOR='BLUE'>Code / Nature</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=270 nowrap><B><FONT COLOR='BLUE'>Address</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap><B><FONT COLOR='BLUE'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</BODY>
</HTML>	
	
	
	
	
	