<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, property_id, str_journal_table, nature_id, nature_sql

	property_id = Request("propertyid")
	nature_id = Request("natureid")

	If nature_id <> "" Then 
		nature_sql = " AND J.ITEMNATUREID = " & nature_id  & " "
	Else
		nature_sql = ""
	End If

    Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		Dim surveyjournal

		cnt = 0
	    surveyjournal=""

		strSQL = 	"SELECT 	J.JOURNALID, " &_
					"			CASE J.ITEMNATUREID WHEN 2 THEN 'iRepairDetail.asp' " &_
					"			WHEN 1 THEN 'iDefectsDetail.asp'  " &_
					"			WHEN 27 THEN 'iTerminationDetail.asp'  " &_
					"			WHEN 47 THEN 'iRepairDetail.asp'  " &_
					"			WHEN 62 THEN 'iRepairDetail.asp'  " &_
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			J.ITEMNATUREID " &_
					"FROM	 	C_JOURNAL J " &_
					"			LEFT JOIN C_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"WHERE	    J.ITEMNATUREID NOT IN (2,20,21,22,34,35,36,37,38,39,40,47,60) AND J.PROPERTYID = '" & property_id & "'" & nature_sql &_
					"ORDER		BY J.JOURNALID DESC"

		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",""" & rsSet("REDIR") & """)' STYLE='CURSOR:HAND'>" &_
															"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ITEM") & "</TD>" &_
															"<TD>" & rsSet("NATURE") & "</TD>" &_
															"<TD>" & rsSet("TITLE")  & "</TD>" &_
															"<TD>" & rsSet("STATUS")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		'Begin update for Property Survey 
		'Updated by Umair on 29/04/2008
		'Calling the function to get the Data for Property Survey		
		surveyjournal=GetSurveyItem()
		
		if(surveyjournal<>"") then  
		    str_journal_table=str_journal_table &  surveyjournal
		elseif cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		'End update for the Property Survey
		
	End Function
	
	'Begin update for Property Survey 
	'Updated by Umair on 29/04/2008
	'This function gets the information about the survey against a propertyid using stored procedure SP_PDA_SURVEY_PROPERTYJOURNAL
	 Function GetSurveyItem()
	    
	    dim dbrs,dbcmd,str_survey_journal_table
	    
	    set dbrs=server.createobject("ADODB.Recordset")
        set dbcmd= server.createobject("ADODB.Command")
        
        str_survey_journal_table=""
    
        dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    	dbcmd.CommandType = 4
    	dbcmd.CommandText = "SP_PDA_SURVEY_PROPERTYJOURNAL"
    	dbcmd.Parameters.Refresh
	
	    dbcmd.Parameters(1) = property_id
	
		set dbrs=dbcmd.execute
	    
	    While Not dbrs.eof
	       str_survey_journal_table = str_survey_journal_table & 	"<TR ONCLICK='open_survey_detail(" & dbrs("SID") & "," & dbrs("SDID") & ",""" & property_id & """)' STYLE='CURSOR:HAND'>" &_
															            "<TD>" & dbrs("DATE") & "</TD>" &_
															            "<TD>" & dbrs("ITEM") & "</TD>" &_
															            "<TD>" & dbrs("NATURE") & "</TD>" &_
															            "<TD>" & dbrs("TITLE")  & "</TD>" &_
															            "<TD>" & dbrs("STATUS")  & "</TD>" &_
														            "<TR>" 
           dbrs.movenext()
	    Wend
	    
		CloseRs(dbrs)
		GetSurveyItem = str_survey_journal_table
    End Function
    'End Update for the Property Survey	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
	function open_me(int_nature, int_journal_id, str_redir){
		location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
		}

	//Begin udpate for Property Survey
	//Updated By Umair on 29/04/2008
	//Calls the Survey journal page i.e iPropertySurveyDetail.asp as an Iframe
    	function open_survey_detail(int_sid,int_sdid,char_propertyid){
		location.href = "iPropertySurveyDetail.asp?sid="+int_sid+"&sdid="+int_sdid+"&pid="+char_propertyid;
		}
	//End Update for Property Survey
	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(10, "BOTTOM")
		}		
    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="behavior: url(../../Includes/Tables/tablehl.htc);
        border-collapse: collapse" slcolor='' border="0" hlcolor="#4682b4">
        <thead>
            <tr valign="top">
                <td style="border-bottom: 1px solid" width="90">
                    <b><font color="blue">Date</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="100">
                    <b><font color="blue">Item</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="150">
                    <b><font color="blue">Nature</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="200">
                    <b><font color="blue">Title</font></b>
                </td>
                <td style="border-bottom: 1px solid" width="100">
                    <b><font color="blue">Status</font></b>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="6">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%>
        </tbody>
    </table>
</body>
</html>
