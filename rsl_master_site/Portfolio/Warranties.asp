<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstwarrantytype, lstcontractor, warrantystring, defectsperiod, lstareaitem

	ACTION = "NEW"
	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then PropertyID = -1 End If

	Call OpenDB()
	    '--Retrieve First Line of Address
		SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
		Call OpenRs(rsAddress, SQL)
		If (NOT rsAddress.EOF) Then
			l_address1 = rsAddress("ADDRESS1")
		Else
			l_address1 = "[None Given]"
		End If
		l_housenumber = rsAddress("HOUSENUMBER")
		l_propertyid = UCASE(rsAddress("PROPERTYID"))
		l_propertystatus = l_propertyid
		Call CloseRs(rsAddress)
	    '--Retrieve First Line of Address

	' get defects period from ssociaited development
	SQL = "SELECT D.DEFECTSPERIOD FROM P__PROPERTY P " &_
			" LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	    If (NOT rsLoader.EOF) Then defectsperiod = rsLoader(0) Else defectsperiod = "Non Attached" End If
	Call CloseRs(rsLoader)

	SQL = "SELECT W.*, T.DESCRIPTION, S.NAME, R.DESCRIPTION AS AREAITEM FROM P_WARRANTY W " &_
			"LEFT JOIN P_WARRANTYTYPE T ON W.WARRANTYTYPE = T.WARRANTYTYPEID " &_
			  " LEFT JOIN S_ORGANISATION S ON W.CONTRACTOR = S.ORGID " &_
			    " LEFT JOIN R_AREAITEM R ON W.AREAITEM = R.AREAITEMID " &_
			" WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		WHILE NOT rsLoader.EOF
			warrantystring = warrantystring & "<tr title=""" & rsLoader("NOTES") & """ style=""cursor:pointer"" onclick=""LoadForm(" & rsLoader("WARRANTYID") & ")"">" &_
						"<td width=""140"" nowrap=""nowrap"">" & rsLoader("DESCRIPTION") & "</td>" &_
						"<td width=""240"" nowrap=""nowrap"">" & rsLoader("AREAITEM") & "</td>" &_
						"<td width=""220"" nowrap=""nowrap"">" & rsLoader("NAME") & "</td>" &_
						"<td width=""90"" nowrap=""nowrap"">" & rsLoader("EXPIRYDATE") & "</td>" &_
						"<td bgcolor=""white"" width=""500"" align=""center"" onclick=""DeleteMe(" & rsLoader("WARRANTYID") & ",event)""><font color=""red"">DEL</font></td></tr>"
			rsLoader.MoveNext
		wend
	Else
		warrantystring = warrantystring & "<tfoot><tr><td colspan=""5"" align=""center"">No warranties exist for this property</td></tr></tfoot>"
	End If
	Call CloseRs(rsLoader)

	Call BuildSelect(lstwarrantytype, "sel_WARRANTYTYPE", "P_WARRANTYTYPE", "WARRANTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", w_warrantytype, NULL, "textbox200", " tabindex='1' " )
	Call BuildSelect(lstcontractor, "sel_CONTRACTOR", "S_ORGANISATION ", "ORGID, NAME", "NAME", "Please Select", w_contractor, NULL, "textbox200", " tabindex='3' " )
	Call BuildSelect(lstareaitem, "sel_AREAITEM", "R_AREAITEM", "AREAITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", w_contractor, NULL, "textbox200", " tabindex='2' " )
    Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio --> Warranty Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_NOTES|NOTES|TEXT|N"
        FormFields[1] = "sel_AREAITEM|WARRANTY FOR|TEXT|Y"
        FormFields[2] = "txt_EXPIRYDATE|EXPIRY DATE|DATE|Y"
        FormFields[3] = "sel_WARRANTYTYPE|WARRANTY TYPE|SELECT|Y"
        FormFields[4] = "sel_CONTRACTOR|Contractor|SELECT|N"

        function LoadForm(ID) {
            document.getElementById("hid_Action").value = "LOAD"
            document.getElementById("hid_WARRANTYID").value = ID
            document.RSLFORM.action = "ServerSide/warranty_svr.asp"
            document.RSLFORM.target = "frm_war"
            document.RSLFORM.submit()
        }


        function SaveForm() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/warranty_svr.asp"
            document.RSLFORM.target = "frm_war"
            document.RSLFORM.submit()
        }

        function SetButtons(Code) {
            if (Code == "AMEND") {
                document.getElementById("AddButton").style.display = "none"
                document.getElementById("AmendButton").style.display = "block"
            }
            else {
                document.getElementById("AddButton").style.display = "block"
                document.getElementById("AmendButton").style.display = "none"
            }
        }

        function ResetForm() {
            document.getElementById("sel_WARRANTYTYPE").value = ""
            document.getElementById("sel_AREAITEM").value = ""
            document.getElementById("sel_CONTRACTOR").value = ""
            document.getElementById("txt_EXPIRYDATE").value = ""
            document.getElementById("hid_WARRANTYID").value = ""
            document.getElementById("hid_Action.value").value = "NEW"
            SetButtons('NEW')
        }

        function AmendForm() {
            document.getElementById("hid_Action").value = "AMEND"
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide/warranty_svr.asp"
            document.RSLFORM.target = "frm_war"
            document.RSLFORM.submit()
        }

        function DeleteMe(WarID, evt) {
            var e = (evt) ? evt : window.event;
            if (window.event) {
                e.cancelBubble = true;
            } else {
                e.stopPropagation();
            }
            answer = window.confirm("Are you sure you wish to delete this warranty?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
            if (answer) {
                document.getElementById("hid_Action").value = "DELETE"
                document.getElementById("hid_WARRANTYID").value = WarID
                document.RSLFORM.action = "ServerSide/warranty_svr.asp"
                document.RSLFORM.target = "frm_war"
                document.RSLFORM.submit()
            }
        }
    </script>
</head>
<body onload="initSwipeMenu(2);" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_main_details" src="images/tab_main_details.gif" width="97" height="20"
                        border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0"
                        alt="" /></a></td>
            <td rowspan="2">
                <img name="tab_warranties" src="images/tab_warranties-over.gif" width="89" height="20"
                    border="0" alt="" /></td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_utilities" src="images/tab_utilities-tab_warrantie.gif" width="68"
                        height="20" border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_marketing" src="images/tab_marketing.gif" width="76" height="20" border="0"
                        alt="" /></a></td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_hands" src="images/tab_healthsafety_1.gif" width="110" height="20"
                        border="0" alt="" /></a></td>
            <td>
                <img alt="" src="/myImages/spacer.gif" width="151" height="19" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img alt="" src="images/spacer.gif" width="151" height="1" /></td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Warranty Type:
            </td>
            <td>
                <%=lstwarrantytype%>
                <img src="/js/FVS.gif" name="img_WARRANTYTYPE" id="img_WARRANTYTYPE" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Expiry Date:
            </td>
            <td>
                <input type="text" name="txt_EXPIRYDATE" id="txt_EXPIRYDATE" class="textbox200" value="<%=w_expirydate%>" />
                <img src="/js/FVS.gif" name="img_EXPIRYDATE" id="img_EXPIRYDATE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                For:
            </td>
            <td>
                <%=lstareaitem%>
                <img src="/js/FVS.gif" name="img_AREAITEM" id="img_AREAITEM" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Notes:
            </td>
            <td rowspan="4" valign="top">
                <textarea style="overflow: hidden; width: 200px" class="textbox200" rows="6" cols="10" name="txt_NOTES"
                    id="txt_NOTES"></textarea>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Org/Contractor:
            </td>
            <td>
                <%=lstcontractor%>
                <img src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" valign="top">
                <input type="button" class="RSLButton" style="width: 70px; cursor:pointer" value=" RESET " title=" RESET " onclick="ResetForm()"
                    name="BtnRESET" id="BtnRESET" />
            </td>
            <td valign="top">
                <input type="button" class="RSLButton" style="width: 70px; cursor:pointer" value=" ADD " title=" ADD " onclick="SaveForm()"
                    name="AddButton" id="AddButton" />
                <input type="button" class="RSLButton" value=" AMEND " title=" AMEND " onclick="AmendForm()" name="AmendButton"
                    id="AmendButton" style="display: none; width: 70px; cursor:pointer" />
            </td>
        </tr>
    </table>
    <br />
    <table cellspacing="0" cellpadding="0" border="1" style='height:230px; border-collapse: collapse;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71'
        width="750">
        <tr bgcolor='#133E71' style='color: #FFFFFF'>
            <td width="140" nowrap="nowrap" height="20">
                <b>&nbsp;Type</b>
            </td>
            <td width="240" nowrap="nowrap">
                <b>&nbsp;For</b>
            </td>
            <td width="220" nowrap="nowrap">
                <b>&nbsp;Org/Contractor</b>
            </td>
            <td width="90" nowrap="nowrap">
                <b>&nbsp;Expiry Date</b>
            </td>
            <td width="500">
                <b>&nbsp;</b>
            </td>
        </tr>
        <tr>
            <td valign="top" height="100%" colspan="5">
                <div style='height: 230; width: 750; overflow: auto' class='TA'>
                    <table cellspacing="0" cellpadding="3" border="1" style='border-collapse: collapse;
                        behavior: url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='#4682b4' width="100%">
                        <%=warrantystring%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" name="hid_PropertyID" id="hid_PropertyID" value="<%=PropertyID%>" />
    <input type="hidden" name="hid_WARRANTYID" id="hid_WARRANTYID" value="<%=warrantyID%>" />
    <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_war" id="frm_war" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>