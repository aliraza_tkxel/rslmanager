<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 17

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match
	Dim TDFunc         (6)	 'stores any functions that will be applied
	Dim SubQuery			 'used to get the name of the parent object
	Dim SubQueryTitle		 'the title of the parent object

	ColData (0) = "Ref No:|PROPERTYID|80"
	SortASC (0)	= "P.PROPERTYID ASC"
	SortDESC(0) = "P.PROPERTYID DESC"
	TDSTUFF (0) = ""
	TDFunc  (0) = ""

	ColData (1) = "Status|STATUS|110"
	SortASC (1)	= "STATUS ASC"
	SortDESC(1) = "STATUS DESC"
	TDSTUFF (1) = " nowrap=""nowrap"" "
	TDFunc  (1) = ""

	ColData (2) = "Property Type|PROPERTYTYPE|140"
	SortASC (2)	= "PROPERTYTYPE ASC"
	SortDESC(2) = "PROPERTYTYPE DESC"
	TDSTUFF (2) = ""
	TDFunc  (2) = ""

	ColData (3) = "Street|ADDRESS10|200"
	SortASC (3)	= "HOUSENUMBER, ADDRESS10"
	SortDESC(3) = "HOUSENUMBER DESC, ADDRESS10 DESC"
	TDSTUFF (3) = " nowrap=""nowrap"" "
	TDFunc  (3) = ""

	ColData (4) = "Town /City|TOWNCITY|120"
	SortASC (4)	= "TOWNCITY ASC"
	SortDESC(4) = "TOWNCITY DESC"
	TDSTUFF (4) = ""
	TDFunc  (4) = ""

	ColData (5) = "Beds|BEDROOMS|40"
	SortASC (5)	= "BEDROOMS ASC"
	SortDESC(5) = "BEDROOMS DESC"
	TDSTUFF (5) = ""
	TDFunc  (5) = ""

	ColData (6) = "TEXT|<img src=""/MYIMAGES/INFO.GIF"" border=""0"" alt=""Information"" title=""Information"">|20"
	SortASC (6)	= ""
	SortDESC(6) = ""
	TDSTUFF (6) = " "" style=""""background-color:#FFFFFF"""" onclick=""""DisplayProperty('"" & rsSet(""PROPERTYID"") & ""')"""" """	
	TDFunc  (6) = ""

	PageName = "PropertyList.asp"
	EmptyText = "No properties found for the selected scheme!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""onclick=""""load_me('"" & rsSet(""PROPERTYID"") & ""')"""" """ 
	SubQuery = "DEVELOPMENTNAME"
	SubQueryTitle = "Scheme&nbsp;:&nbsp;"

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	If Request.QueryString("DevelopmentID") = "" Then
		DevelopmentID = -1
	Else
		If (IsNumeric(Request.QueryString("DevelopmentID"))) Then
			DevelopmentID = CInt(Request.QueryString("DevelopmentID"))
		Else
			DevelopmentID = -1
		End If
	End If

	SQLCODE = "SELECT P.PROPERTYID, PS.SCHEMENAME DEVELOPMENTNAME, S.DESCRIPTION AS STATUS, PT.DESCRIPTION AS PROPERTYTYPE, "   &_
			  "LTRIM(P.HOUSENUMBER + ' ' + ISNULL(P.FLATNUMBER,'') + ' ' + ISNULL(P.ADDRESS1, P.ADDRESS2)) AS ADDRESS10, "  &_ 
			  "ISNULL(P.ADDRESS1, P.ADDRESS2) AS ADDRESS1, P.TOWNCITY, ISNULL(A.PARAMETERVALUE,0) AS BEDROOMS "  &_ 
              "FROM P__PROPERTY P  "   &_
			  "LEFT JOIN P_SCHEME PS ON P.SCHEMEID = PS.SCHEMEID  "  &_
			  "LEFT JOIN P_STATUS S ON P.STATUS = S.STATUSID "  &_
			  "LEFT JOIN P_PROPERTYTYPE PT ON PT.PROPERTYTYPEID = P.PROPERTYTYPE  "  &_
			  "LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P.PROPERTYID AND A.ITEMPARAMID = ( "  &_
			  "						 SELECT ItemParamID from PA_ITEM_PARAMETER "  &_
		      "					 INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID "  &_
			  "						 Where ParameterName='Bedrooms') "  &_
			  " WHERE PS.SCHEMEID = " & DevelopmentID & " " &_
			  " ORDER BY " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

	Call Create_Table()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Portfolio --> Tools --> Property List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

        function load_me(PropID) {
            location.href = "Property.asp?PropertyID=" + PropID;
        }

        function DisplayProperty(PropID) {
            event.cancelBubble = true
            window.open("/Portfolio/PopUps/Property_Details.asp?PropertyID=" + PropID, "PropertyDetails", "width=400,height=300")
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="javascript:location.href='DevelopmentList.asp?CC_Sort=<%=Request("DevSort")%>&page=<%=Request("DevPage")%>'">
                    <img name="tab_Scheme" id="tab_Scheme" title="Scheme List" src="images/scheme_closed.gif" width="102"
                        height="20" border="0" alt="Scheme List" /></a></td>
            <td rowspan="2">
                <img name="tab_PropertyList" id="tab_PropertyList" title="Property List" src="images/tab_prop_list-over.gif"
                    width="111" height="20" border="0" alt="" /></td>
            <td>
                <img src="images/spacer.gif" width="535" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="506" height="1" alt="" /></td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
