	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 21

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)
	Dim searchSQL,searchName
	
	
	searchName = Replace(Request("name"),"'","''")
	searchSQL = ""
	if searchName <> "" Then
		searchSQL = " Where (SCHEMENAME LIKE '%" & searchName & "%' ) "
	End If
	
	ColData(0)  = "Scheme Name|SCHEMENAME|270"
	SortASC(0) 	= "SCHEMENAME ASC"
	SortDESC(0) = "SCHEMENAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Type|DEVELOPMENTTYPE|120"
	SortASC(1) 	= "DEVELOPMENTTYPE ASC"
	SortDESC(1) = "DEVELOPMENTTYPE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Start|STARTDATE2|75"
	SortASC(2) 	= "STARTDATE ASC"
	SortDESC(2) = "STARTDATE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "End|TARGETDATE2|75"
	SortASC(3) 	= "TARGETDATE ASC"
	SortDESC(3) = "TARGETDATE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Props|TOTALPROPERTIES|80"
	SortASC(4) 	= "TOTALPROPERTIES ASC"
	SortDESC(4) = "TOTALPROPERTIES DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "Available|AVAILABLE|100"
	SortASC(5) 	= "AVAILABLE ASC"
	SortDESC(5) = "AVAILABLE DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""		

	ColData(6)  = "Un-avail...|UNAVAILABLE|110"
	SortASC(6) 	= "UNAVAILABLE ASC"
	SortDESC(6) = "UNAVAILABLE DESC"	
	TDSTUFF(6)  = ""
	TDFunc(6) = ""		

	PageName = "EstateMaintenance.asp"
	EmptyText = "No developments found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""DEVELOPMENTID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT D.DEVELOPMENTID, SCHEMENAME, DT.DESCRIPTION AS DEVELOPMENTTYPE, " &_
			"STARTDATE2 = ISNULL(CONVERT(VARCHAR, STARTDATE, 103),'N/A') , " &_
			"TARGETDATE2 = ISNULL(CONVERT(VARCHAR, TARGETDATE, 103),'N/A'), " &_
			"ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') AS FULLNAME, " &_
			"ISNULL(TP.TOTALPROPERTIES,0) AS TOTALPROPERTIES, " &_
			"ISNULL(AV.AVAILABLE,0) AS AVAILABLE,  " &_
			"ISNULL(TP.TOTALPROPERTIES,0) - ISNULL(AV.AVAILABLE,0) AS UNAVAILABLE " &_
			"FROM P_DEVELOPMENT D " &_
			"	LEFT JOIN  " &_
			"		(SELECT COUNT(*) AS AVAILABLE, DEVELOPMENTID  " &_
			"			FROM P__PROPERTY WHERE STATUS IN (1,5) GROUP BY DEVELOPMENTID) AV  " &_
			"	ON D.DEVELOPMENTID = AV.DEVELOPMENTID " &_
			"	LEFT JOIN  " &_
			"		(SELECT COUNT(*) AS TOTALPROPERTIES, DEVELOPMENTID  " &_
			"			FROM P__PROPERTY GROUP BY DEVELOPMENTID) TP " &_
			"	ON D.DEVELOPMENTID = TP.DEVELOPMENTID " &_	
			"	LEFT JOIN P_DEVELOPMENTTYPE DT ON D.DEVELOPMENTTYPE = DT.DEVELOPMENTTYPEID " &_
			"	LEFT JOIN E__EMPLOYEE E ON D.LEADDEVOFFICER = E.EMPLOYEEID " &_
			" " &  searchSQL & " "&_
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --&gt; Estate Maintenance --&gt; Development List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(dev_id){
	location.href = "BlockList.asp?DEVID=" + dev_id + "&DevPage=<%=Request("page")%>&DevSort=<%=Request("CC_Sort")%>&name=<%=Request("name")%>";	
	//location.href = "PropertyList.asp?DevelopmentID=" + employee_id + "&DevPage=<%=Request("page")%>&DevSort=<%=Request("CC_Sort")%>";
	}

function quick_find(){
	location.href = "EstateMaintenance.asp?name="+thisForm.findemp.value;
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
	 <TD ROWSPAN=2><IMG NAME="tab_employees" TITLE='Scheme List' SRC="images/schemelist.gif" WIDTH=102 HEIGHT=20 BORDER=0></TD>
		<TD><IMG SRC="images/spacer.gif" WIDTH=347 HEIGHT=19></TD>
		<td nowrap><INPUT TYPE='BUTTON' name='btn_FIND' title='Search for matches using scheme Name.' CLASS='RSLBUTTON' VALUE='Quick Find' ONCLICK='quick_find()'>
					&nbsp;<input type=text  size=18 name='findemp' class='textbox200' value="<%=searchName%>">
		</td>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71 colspan=2><IMG SRC="images/spacer.gif" WIDTH=318 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:NONE'></iframe>
</BODY>
</HTML>