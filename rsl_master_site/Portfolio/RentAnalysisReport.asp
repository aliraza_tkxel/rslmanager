<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include virtual="Connections/db_connection.asp" -->

<%
	dim data_table,developmentid,localauthority,processdate,postcode,dbrs,dbcmd,norecords,count,formpost
	dim patchid
	
	formpost=Request.Item("form_post")
    
	
	if(Request.Item("sel_Patch")<> "") then
	    patchid=Request.Item("sel_Patch")
	    l_Patch=patchid 
	    patchdescription=Request.Item("patchdescription")
	else
	    patchid=null
	end if    
	
	if(Request.Item("sel_Development")<> "") then
	    developmentid=Request.Item("sel_Development")
	    l_DEVELOPMENT=developmentid 
	    developmentdescription=Request.Item("developmentdescription")
	else
	    developmentid=null
	end if    
	
	if(Request.Item("sel_LocalAuthority")<>"") then
	    localauthority=Request.Item("sel_LocalAuthority")
	    l_LOCALAREA=localauthority
	    localauthoritydescrption=Request.Item("localauthoritydescrption")
	else
	    localauthority=null
	end if
	
	
	if(Request.Item("processdate")<>"") then
	    processdate=Request.Item("processdate")
	else
	    processdate=date()
	end if
	
	if(Request.Item("post_code")<>"") then
	    postcode=Request.Item("post_code")
	else
	    postcode=null
	end if
	
	if(formpost="1" and localauthority<>"") then
	    l_DEVELOPMENT=""
	    SchemeFilter=" WHERE LOCALAUTHORITY=" & localauthority
	    
    elseif(formpost="2" and patchid<>"") then  
        l_DEVELOPMENT="" 
	    SchemeFilter=" WHERE PATCHID=" & patchid    
	else
	    SchemeFilter=""
	end if
	
    OpenDB()
    Call BuildSelect(lstPatch, "sel_Patch", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", l_Patch, NULL, "textbox200", " STYLE='margin-left:53px;' TABINDEX=11 OnChange=""PatchChange();"" ")	    
	Call BuildSelect(lstDEVELOPEMNT, "sel_Development", "P_DEVELOPMENT "&SchemeFilter, "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_DEVELOPMENT,null, "textbox100", "style='width:200px;margin-left:42px;'" )
	Call BuildSelect(lstLOCALAREA, "sel_LocalAuthority", "G_LOCALAUTHORITY ", "LOCALAUTHORITYID, DESCRIPTION", "DESCRIPTION", "Please Select", l_LOCALAREA,null, "textbox100", "style='width:200px;margin-left:38px;' OnChange=""LAChange();""" )
	CloseDB()
	
	if(formpost="3") then
	    set dbrs=server.createobject("ADODB.Recordset")
        set dbcmd= server.createobject("ADODB.Command")
    
        dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    	dbcmd.CommandType = 4
    	dbcmd.CommandText = "P_PORTFOLIOANALYSIS_REPORT"
    	dbcmd.Parameters.Refresh
	
	    dbcmd.Parameters(1) = processdate
	    dbcmd.Parameters(2) = localauthority
	    dbcmd.Parameters(3) = developmentid
	    dbcmd.Parameters(4) = postcode
	    dbcmd.Parameters(5) = patchid
	
		set dbrs=dbcmd.execute
	    
	    if(not dbrs.eof) then           
	        data_table= "<div   id=""inner_content"" ><table id=""PortfolioDataTable"" border=""0"" cellpadding=""0""  class=""data_table"" cellspacing=""0""  summary=""Portfolio Stock Summary"">"
	        data_table=data_table & "<tr class=""filter"">"
	        data_table=data_table & "   <td><span class=""printbold""> Date: </span>" & processdate & "</td>"
	        data_table=data_table & "</tr>" 
	        

            if(patchdescription<>"") then 
	            data_table=data_table & "<tr class=""filter"">"
	            data_table=data_table & "   <td><span class=""printbold""> Patch: </span>" & patchdescription & "</td>"
	            data_table=data_table & "</tr>"
	        end if


	        if(localauthoritydescrption<>"") then
	            data_table=data_table & "<tr class=""filter"">"
	            data_table=data_table & "   <td><span class=""printbold""> L. A. Area: </span>" & localauthoritydescrption & "</td>"
	            data_table=data_table & "</tr>"
	        end if
	        
	        if(developmentdescription<>"") then 
	            data_table=data_table & "<tr class=""filter"">"
	            data_table=data_table & "   <td><span class=""printbold""> Scheme: </span>" & developmentdescription & "</td>"
	            data_table=data_table & "</tr>"
	        end if
	        
	        if(postcode<>"") then 
	            data_table=data_table & "<tr class=""filter"">"
	            data_table=data_table & "   <td><span class=""printbold""> Postcode:</span>" & postcode & "</td>"
	            data_table=data_table & "</tr>"
	        end if
	        
	        data_table=data_table & "<tr>"
	        data_table=data_table & "   <th>Asset Type</th>"
	        data_table=data_table & "   <th>Total Count</th>"
	        data_table=data_table & "   <th>Let</th>"
	        data_table=data_table & "   <th>Available</th>"
	        data_table=data_table & "   <th>Unavailable</th>"
	        data_table=data_table & "</tr>"
    	    
	        norecords =dbrs.recordcount
	            	    
	        count=0
	        while not dbrs.eof 
	            count=count+1
	            if(count=1 or count<=norecords or dbrs("description")="TOTAL") then
	                data_table=data_table & "<tr>"
	                data_table=data_table & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
	                data_table=data_table & "</tr>" 
	                data_table=data_table & "<tr>" 
	                
	                if(count=1) then
	                    data_table=data_table & "   <td><span class=""fontweightbold"">" & dbrs("description") & "</span></td>"
	                    data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','0','')"">" & dbrs("total") & "</span></td>"
	                    data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','2','Let')"">" & dbrs("let") & "</span></td>"
	                    data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','1','Available')"">" & dbrs("available") & "</span></td>"
	                    data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','4','Unavailable')"">" & dbrs("unavailable") & "</span></td>"
	                else
	                    data_table=data_table & "   <td class=""printbold""><span class=""fontweightbold"">" & dbrs("description") & "</span></td>"
	                    data_table=data_table & "   <td class=""printbold"">" & dbrs("total") & "</td>"     
	                    data_table=data_table & "   <td class=""printbold""><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','2','Let')"">" & dbrs("let") & "</span></td>"
	                    data_table=data_table & "   <td class=""printbold""><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','1','Available')"">" & dbrs("available") & "</span></td>"
	                    data_table=data_table & "   <td class=""printbold""><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','4','Unavailable')"">" & dbrs("unavailable") & "</span></td>"
	                    
	                end if
	                
	                
	                
	                data_table=data_table & "</tr>"
	            else
	                data_table=data_table & "<tr>" 
	                data_table=data_table & "   <td class=""fontweightbold"">" & dbrs("description") & "</td>"
	                data_table=data_table & "   <td ><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','0','')"">" & dbrs("total") & "</span></td>"
                    data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','2','Let')"">" & dbrs("let") & "</span></td>"
	                data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','1','Available')"">" & dbrs("available") & "</span></td>"
	                data_table=data_table & "   <td><span class=""linkstyle"" onclick=""assettypeandstatus('" & dbrs("assettypeid") & "','" & dbrs("description") & "','4','Unavailable')"">" & dbrs("unavailable") & "</span></td>"
	                data_table=data_table & "</tr>"
	            end if    
	            dbrs.movenext()
	        wend    
            data_table=data_table & "</table></div>"
            data_table=data_table & "<div><p class=""button""><input type=""button""  name=""btn_print"" value=""Print"" class=""RSLButton"" style="" width:60px;margin-left:119px;margin-top:5px;"" onclick=""print_data();"" /> </p></div>"
	    end if
	    set dbrs=nothing
        set dbcmd= nothing
        
	end if
%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Customers --> Rent Analysis </title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style  type="text/css" media="screen">
    #filter {background-color: #FFFFFF; border: #133e71 solid; border-width: 1px 1px 1px 1px;width:320px;margin-left:5px;padding-top:10px;padding-bottom:5px;float:left;margin-right:3px;}
    #content{border: #FFFFFF solid;border-width:1px;height:1px;}
    #inner_content {background-color: #FFFFFF; border: #133e71 solid; border-width: 1px 1px 1px 1px;width:100%;margin-bottom:0px;padding-bottom:0px;padding-right:1px;} 
     h1 {font-size:14px;margin:5px 10px 10px 10px;font-weight: bold;}
    .date_filter{padding:0px 0px 0px 8px;margin:0 0 10px 0;}
    .date_filter a{	text-decoration : none; margin:0 0 0 0; padding:0 0 0 0;}
    .date_filter img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .lable {font-weight: bold;}
    .thickline{border-style: solid;border-color:#133e71;border-width:0 0 2px 0; padding:0 0 0 0; margin:0 0 0 0;}
    .thinline{border-style: solid;border-color:#gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}
    .border_bottom{border-style: solid;border-color:#133e71;border-bottom:1px; padding:0 0 0 0; margin:0 0 0 0;}
    .other_filter{margin-left:8;padding-top:0;padding-bottom:0;margin-top:6px;margin-bottom:6px;}
    .button{padding-left:245px;margin:0 0 0 0;}
    .data_table {margin-left:5px;padding-bottom:10px;width:410px;}
    .data_table th{font-size:10px;font-weight:bold;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-left:10px;padding-right:10px;padding-top:5px;margin-bottom:0px;padding-bottom:0px;}
    .data_table td{padding-top:0px;padding-left:13px;font-size:10;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-right:4px;}
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;}
    .fontweightbold{font-weight:bold;}
    .filter{display:none;}
    /*#Menu1{display:none;}*/
</style>
<style type="text/css" media="print">
    #filter {display:none;}
    .button {display:none;}
    img { display: none } 
    #Menu1hMenu1 {display:none;}
    #Menu2hMenu1 {display:none;}
    #Menu3hMenu1 {display:none;}
    #Menu4hMenu1 {display:none;}
    #Menu5hMenu1 {display:none;}
    h1{font-size:14px;}
    title{display:none;}
    th{font-size:11px;padding-right:40px;}
    td{padding-right:40px;padding-top:10px;}
   .printbold{font-weight:bold;}
   .filter{padding-bottom:10px;}
</style>
</head>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
  function filter()
  {
    //alert(thisForm.sel_LocalAuthority.options[thisForm.sel_LocalAuthority.selectedIndex].text);
    thisForm.localauthoritydescrption.value=thisForm.sel_LocalAuthority.options[thisForm.sel_LocalAuthority.selectedIndex].text;
    thisForm.developmentdescription.value=thisForm.sel_Development.options[thisForm.sel_Development.selectedIndex].text;
    thisForm.patchdescription.value=thisForm.sel_Patch.options[thisForm.sel_Patch.selectedIndex].text;
   
    thisForm.form_post.value="3";
    thisForm.submit();
  }
  function print_data()
  {
    document.getElementById("content").focus();
    window.print();
  }
  function assettypeandstatus(assetid,assetdescription,status,statusdescription)
  {
     var QueryString
    
    QueryString="?assetid="+assetid+"&assetdescription="+assetdescription;
    
    if(thisForm.sel_LocalAuthority.value!=undefined) 
    {
        QueryString=QueryString+"&localauthority="+thisForm.sel_LocalAuthority.value;
        QueryString=QueryString+"&localauthoritydescription="+ thisForm.sel_LocalAuthority.options[thisForm.sel_LocalAuthority.selectedIndex].text;
   }
    
    if(thisForm.sel_Development.value!=undefined) 
    {
        QueryString=QueryString+"&developmentid="+thisForm.sel_Development.value;
        QueryString=QueryString+"&developmentdescription="+thisForm.sel_Development.options[thisForm.sel_Development.selectedIndex].text;
    }
    
    if(thisForm.sel_Patch.value!=undefined)
    {
        QueryString=QueryString+"&patch="+thisForm.sel_Patch.value;
        QueryString=QueryString+"&patchdescription="+thisForm.sel_Patch.options[thisForm.sel_Patch.selectedIndex].text;
    }
    
    if(thisForm.processdate.value!='') 
    {
        QueryString=QueryString+"&processdate="+thisForm.processdate.value;
    }
    
    if(thisForm.post_code.value!='') 
    {
        QueryString=QueryString+"&postcode="+thisForm.post_code.value;
    }
    
    if(status!='0') 
    {
        QueryString=QueryString+"&status="+status;
    }
    
    if(statusdescription!='0') 
    {
        QueryString=QueryString+"&statusdescription="+statusdescription;
    }
     
    //alert(QueryString);
    window.open("PopUps/pAssetType_RentReport.asp"+QueryString+"", "display","width=1000px,height=440px,left=10,top=200") ;
  }
  function LAChange()
  {
    thisForm.form_post.value="1";
    thisForm.patchdescription.value="";
    thisForm.sel_Patch.value="";
    thisForm.submit();
  }
  function PatchChange()
  {
    thisForm.form_post.value="2";
    thisForm.localauthoritydescrption.value="";
    thisForm.sel_LocalAuthority.value="";
    thisForm.submit();
  }
  
 
 </script>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages()" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop_portfolioreport.asp" -->
<form name = "thisForm" method="post" action="">
    <h1>Rent Analysis </h1>
    <div  id="filter">
        <p class="date_filter">
            <label class="lable">Date (as at):</label>
            <input name="processdate" type="text" value="<%=processdate%>" class="textbox100" style="width:180px;margin-left:10px;" />
            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('processdate',360,150,'de','#FFFFFF','#133E71','YY_calendar1')" tabindex="1"><img alt="calender_image" width="18px" height="19px"  src="images/cal.gif"/></a>
        </p>
        <p class="thickline"></p>
        <p class="other_filter">
            <label class="lable"> Patch:</label>
            <%=lstPatch %>
        </p>
        <p class="other_filter">
            <label class="lable"> L A Area:</label>
            <%=lstLOCALAREA%>
        </p>
        <p class="other_filter">
            <label class="lable"> Scheme:</label>
            <%=lstDEVELOPEMNT%>
        </p>
        <p class="other_filter">
            <label class="lable"> Post Code:</label>
            <input name="post_code" type="text" class="textbox200" value="<%=postcode %>" style="margin-left:30px;" />
        </p>
        <p class="button"><input type="button"  name="btn_filter" value="Enter" class="RSLButton" style=" margin-bottom:3px;width:60px;" onClick="filter()" /> </p>
        <input type="hidden" name="form_post" value="0" />
    </div>
    <div id="content">
           <%=data_table%>
    </div>
    <input type="hidden" value="<%=localauthoritydescrption%>" name="localauthoritydescrption" />
    <input type="hidden" value="<%=developmentdescription%>" name="developmentdescription" />
    <input type="hidden" value="<%=patchdescription%>" name="patchdescription" />
    
 </form>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:109px; z-index:20; visibility: hidden'></div>
<!--#include virtual="Includes/Bottoms/BodyBottom_portfolioreport.asp" -->
</body>
</html>