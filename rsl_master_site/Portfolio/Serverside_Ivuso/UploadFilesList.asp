<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'DECLARE VARIABLES
	Dim Title, FileDate, By
	Dim RentIncreaseFileData

	'GET ALL THE VARIABLES POSTED FROM MAIN PAGE WHEN SEARCHING FOR FILES
	Function GetPostedData()

		'This function will get all the posted data
		Title = Request.Item("txt_Title")
		FileDate =  replace(Request.Item("txt_Date"),"DD/MM/YYYY","")
		By =  Request.Item("txt_UploadedBy")

	End Function

	Call openDB()
	Call GetPostedData()
	GetFileListLines = GetFileList()
	Call closeDB()

    Function GetFileList()

		Dim Filefilter, RentIncreaseFileData

		'filter by title
		if (Title  <> "") then
			Filefilter= Filefilter & " and RF.TITLE LIKE '%" & TITLE & "%'"
		end if

		' filter by the date
		if (FileDate <> "") then
			Filefilter= Filefilter & " and RF.UPLOADDATE = '" & FileDate & "'"
		end if

		' filter by the person who uploaded the file
		if (By  <> "") then
			Filefilter= Filefilter & " and E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & By & "%'"
		end if

		SQL= " SELECT FILEID,FILENAME,RF.TITLE,E.FIRSTNAME + ' ' + E.LASTNAME AS UPLOADEDBY,UPLOADDATE " &_
			" FROM P_RENTINCREASE_FILES RF " &_
			"	INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = RF.UPLOADEDBY " &_
			" 	where FILEPASSED = 1 " & Filefilter &_
			" ORDER BY UPLOADDATE DESC" 

		Call OpenRs(rsSet, SQL)

		'// CREATE THE LIST OF ALL THE FILES THAT HAVE BEEN UPLOADED
			RentIncreaseFileData="<div style=""height:200px;overflow:scroll;width:100%;""><table style='border-collapse:collapse;width:100%'  cellspacing=4 cellpadding=2>"
			RentIncreaseFileData=RentIncreaseFileData & "<tr class=""heading2"">"
			RentIncreaseFileData=RentIncreaseFileData & "<td> Title</td>"
			RentIncreaseFileData=RentIncreaseFileData & "<td> File Name</td>"
			RentIncreaseFileData=RentIncreaseFileData & "<td> Upload Date</td>"
			RentIncreaseFileData=RentIncreaseFileData & "<td> Uploaded By</td>"
			RentIncreaseFileData=RentIncreaseFileData & "<td></td>"
			RentIncreaseFileData=RentIncreaseFileData & "</tr>" 
			RentIncreaseFileData=RentIncreaseFileData & "<tr><td class=""dottedline"" colspan=""6""></td></tr>"
			If(Not rsSet.eof) Then
			    While(not rsSet.eof)
				    RentIncreaseFileData=RentIncreaseFileData & "<tr style=""color:#133e71;"">"
				    RentIncreaseFileData=RentIncreaseFileData & "<td>" & rsSet("Title") &"</td>"
				    RentIncreaseFileData=RentIncreaseFileData & "<td><a href='/RentIncreaseUploadFile/" & rsSet("FileName") & "' target=""_blank"">" & rsSet("FileName") &"</td>"
				    RentIncreaseFileData=RentIncreaseFileData & "<td>" & rsSet("UploadDate") &"</td>"
				    RentIncreaseFileData=RentIncreaseFileData & "<td>" & rsSet("UploadedBy") &"</td>"
				    RentIncreaseFileData=RentIncreaseFileData & "<td><a href='#' onclick='Open_window(" & rsSet("FILEID")  & ")'>View Report</a> </td>"
				    RentIncreaseFileData=RentIncreaseFileData & "<td></td>"
				    RentIncreaseFileData=RentIncreaseFileData & "</tr>"
				    rsSet.movenext()
			    wend
			Else
				RentIncreaseFileData=RentIncreaseFileData & "<tr>"
				RentIncreaseFileData=RentIncreaseFileData & "<td colspan=""6"">No Files Exist (If you have used the search facility please try changing your criterea)</td>"
				RentIncreaseFileData=RentIncreaseFileData & "</tr>"
			End If
			 RentIncreaseFileData=RentIncreaseFileData & "<tr><td class=""dottedline"" colspan=""6""></td></tr>"
			 RentIncreaseFileData= RentIncreaseFileData & "</table></div>"

             Call CloseRs(rsSet)

             GetFileList = RentIncreaseFileData

    End Function
    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Rent File List</title>
    <script language="javascript" type="text/javascript">
        function ReturnData() {
            parent.document.getElementById("content_body2").innerHTML = document.getElementById("FileListDetail").innerHTML;
            return true;
        }
    </script>
</head>
<body onload="ReturnData()">
    <form name="thisForm" method="post" action="">
    <div id="Heading">
        <%=Heading%></div>
    <div style="text-align: center;" id="FileListDetail">
        <%=GetFileListLines%></div>
    </form>
</body>
</html>
<% response.end()%>