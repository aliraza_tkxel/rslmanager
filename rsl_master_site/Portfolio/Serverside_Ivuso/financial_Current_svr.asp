<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	FirstArraySize = 12
 
	ReDim DataFields   (FirstArraySize)
	ReDim DataTypes    (FirstArraySize)
	ReDim ElementTypes (FirstArraySize)
	ReDim FormValues   (FirstArraySize) 
	ReDim FormFields   (FirstArraySize)
	
	UpdateID	  = "hid_PropertyID"

	FormFields(0) = "txt_DATERENTSET|DATE"
	FormFields(1) = "txt_RENTEFFECTIVE|DATE"
	FormFields(2) = "txt_TOTALRENT|MONEY"
	FormFields(3) = "txt_RENT|Rent|MONEY"
	FormFields(4) = "txt_SERVICES|MONEY"
	FormFields(5) = "txt_COUNCILTAX|MONEY"
	FormFields(6) = "txt_WATERRATES|MONEY"
	FormFields(7) = "txt_INELIGSERV|MONEY"
	FormFields(8) = "txt_SUPPORTEDSERVICES|MONEY"
	FormFields(9) = "sel_RENTTYPE|NUMBER"
	FormFields(10) = "hid_PROPERTYID|TEXT"
	FormFields(11) = "txt_GARAGE|MONEY"
	FormFields(12) = "hid_PFUSERID|NUMBER"
	
	Function NewRecord ()
		ID = Request.Form(UpdateID)		
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO P_FINANCIAL " & strSQL & ""
		Response.Write SQL& "<br><br>"
		Conn.Execute SQL, recaffected
		SQL = "INSERT INTO P_FINANCIAL_HISTORY " & strSQL & ""
		Response.Write SQL& "<br><br>"
		Conn.Execute SQL, recaffected	


		GO()
	End Function
	
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		' Check the property exists in p_financial/ if not ass new record
		CheckExists = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & ID  & "'"
		Call OpenRs(rsCheck, CheckExists) 
		if rsCheck.eof then
			NewRecord ()
			Exit Function
		end if
		call CloseRs(rsCheck)
		
		HISTORYSQL = "INSERT INTO P_FINANCIAL_HISTORY (PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue) " &_
			  		 " SELECT PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"

		Conn.Execute HISTORYSQL, recaffected
		rw HISTORYSQL & "<br><br>"
		SQL = "UPDATE P_FINANCIAL " & strSQL & ",PFTIMESTAMP=GETDATE() WHERE PROPERTYID = '" & ID & "'"
		Response.Write SQL
		Conn.Execute SQL, recaffected
		
		GO()
	End Function
	
	Function DeleteRecord(Id, theID)
		ID = Request.Form(UpdateID)	
		
		SQL = "DELETE FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected
		rw sql& "<br><br>"
	End Function
	
	Function GO()
		' RUN SCRIPTS TO ENTER PART MONTH VOID ENTRIES
		Conn.Execute ("NL_LETTING_DATE_MONITORING")

		Response.Redirect "../iFrames_iVuso/iFinancial_CurrentUpdate.asp?PropertyID=" & ID

	End Function
	
	TheAction = Request("hid_Action")
		
	OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	CloseDB()
%>