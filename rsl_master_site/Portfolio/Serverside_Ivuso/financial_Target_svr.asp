<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	FirstArraySize = 10
 
	ReDim DataFields   (FirstArraySize)
	ReDim DataTypes    (FirstArraySize)
	ReDim ElementTypes (FirstArraySize)
	ReDim FormValues   (FirstArraySize) 
	ReDim FormFields   (FirstArraySize)
	
	UpdateID	  = "hid_PropertyID"

	FormFields(0) = "sel_RENTTYPE|NUMBER"

	FormFields(1) = "txt_RENT|Rent|MONEY"
	FormFields(2) = "txt_SERVICES|MONEY"
	FormFields(3) = "txt_COUNCILTAX|MONEY"
	FormFields(4) = "txt_WATERRATES|MONEY"
	FormFields(5) = "txt_INELIGSERV|MONEY"
	FormFields(6) = "txt_SUPPORTEDSERVICES|MONEY"
	FormFields(7) = "txt_GARAGE|MONEY"
	FormFields(8) = "txt_TOTALRENT|MONEY"

	FormFields(9) = "hid_PROPERTYID|TEXT"
	FormFields(10) = "hid_PFUSERID|NUMBER"
		
	Function NewRecord ()
		ID = Request.Form(UpdateID)		
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO P_FINANCIAL_TARGET " & strSQL & ""
		Response.Write SQL& "<br><br>"
		Conn.Execute SQL, recaffected
		'SQL = "INSERT INTO P_FINANCIAL_HISTORY " & strSQL & ""
		'Response.Write SQL& "<br><br>"
		'Conn.Execute SQL, recaffected	
		GO()
	End Function
	
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		' Check the property exists in p_financial/ if not ass new record
		CheckExists = "SELECT * FROM P_FINANCIAL_target WHERE PROPERTYID = '" & ID  & "'"
		Call OpenRs(rsCheck, CheckExists) 
		if rsCheck.eof then
			NewRecord ()
			Exit Function
		end if
		call CloseRs(rsCheck)
		
		HISTORYSQL = "INSERT INTO P_FINANCIAL_HISTORY (PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue) " &_
			  		 " SELECT PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"

		'Conn.Execute HISTORYSQL, recaffected
		rw HISTORYSQL & "<br><br>"
		SQL = "UPDATE P_FINANCIAL_target " & strSQL & " WHERE PROPERTYID = '" & ID & "'"
		Response.Write SQL
		Conn.Execute SQL, recaffected
		
		GO()
	End Function
	
	
	Function GO()
		
		Response.Redirect "../iframes_iVuso/iFinancial_TargetUpdate.asp?PropertyID=" & ID

	End Function
	
	TheAction = Request("hid_Action")
		
	OpenDB()
	Select Case TheAction
		Case "NEW"	NewRecord()
		Case "AMEND"	AmendRecord()
		Case "LOAD"	LoadRecord()
	End Select
	CloseDB()
%>