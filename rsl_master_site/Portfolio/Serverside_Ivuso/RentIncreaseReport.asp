<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	'\\ DECLARE VARIABLES
	Dim 		Patch,Scheme ,VName ,VStatus 	,Asat 
	Dim RentIncreaseData
	
	'\\ GET ALL THE VARIABLES POSTED FROM MAIN PAGE WHEN SEARCHING FOR FILES	
	Function GetPostedData()
	
		//This function will get all the posted data
		
		Patch 	=  Request.Item("sel_PATCH")
		Scheme 	=  Request.Item("sel_DEVELOPMENT")
		VName 	=  Request.Item("txt_NAME")
		VStatus =  Request.Item("sel_STATUS")
		Asat 	=  Request.Item("sel_ASAT")
		
	End Function
		
	openDB()
	GetPostedData()
	GetFileListLines  =     GetFileList()
	closeDB()
	
    Function GetFileList()
	    
		Dim Filefilter   

		'filter by PATCH
		if (PATCH <> "") then
			Filefilter= Filefilter & " and  P.PATCH = " & PATCH & ""
		end if
		
		'filter by SCHEME
		if (SCHEME <> "") then
			Filefilter= Filefilter & " and  P.DEVELOPMENTID = " & SCHEME & ""
		end if
		 
		'filter by NAME
		if (VNAME <> "") then
			Filefilter= Filefilter & " and  V.LIST LIKE '%" & replace(VNAME,"'","''") & "%'"
		end if
		
		'filter by STATUS
		if (VSTATUS <> "") then
			Filefilter= Filefilter & " and  P.STATUS = " & VSTATUS & ""
		end if
		
		'filter by STATUS
		if (ASAT <> "") then
			Filefilter= Filefilter & " and  T.REASSESMENTDATE = '" & ASAT & "'"
		end if
		rw Filefilter
		SQL= "SELECT 	 P.PROPERTYID, " &_
			"	(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') ) AS ADDRESS, " &_
			"	V.LIST AS VNAME, " &_
			"	S.DESCRIPTION AS STATUS ," &_
			"	(ISNULL(OLD_RENT,0) + ISNULL(OLD_SERVICES,0)+  " &_
			"	ISNULL(OLD_COUNCILTAX,0)+ ISNULL(OLD_WATERRATES,0)+   " &_
			"	ISNULL(OLD_INELIGSERV,0)+ ISNULL(OLD_SUPPORTEDSERVICES,0)) AS UPLOAD_RENT,  " &_
			"	(ISNULL(T.ACTUAL_INCREASE_AT_APRIL1ST,0)) AS ACTUAL_RENT,  " &_
			"	0 AS TARGET_RENT  " &_
			"FROM	P_TARGETRENT T  " &_
			"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  " &_
			"	INNER JOIN P_STATUS S ON S.STATUSID = P.STATUS " &_
			"	LEFT JOIN C_TENANCY TN ON TN.PROPERTYID = P.PROPERTYID AND TN.ENDDATE IS NULL  " &_
			"	LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW V ON V.I = TN.TENANCYID   " &_
			"WHERE ACTIVE = 1 "  & Filefilter &_
			" order by P.ADDRESS1,P.HOUSENUMBER"
			rw  SQL
'response.end()
		
		Call OpenRs(rsSet, SQL)
		
		'// CREATE THE LIST OF ALL THE FILES THAT HAVE BEEN UPLOADED
		
			RentIncreaseData="<div style=""height:280px;overflow:scroll;width:100%;""><table style='border-collapse:collapse;width:100%'  cellspacing=4 cellpadding=2>"
			RentIncreaseData=RentIncreaseData & "<tr class=""heading2"">"
			RentIncreaseData=RentIncreaseData & "<td> Address </td>"
			RentIncreaseData=RentIncreaseData & "<td> Customer </td>"
			RentIncreaseData=RentIncreaseData & "<td> Status </td>"
			RentIncreaseData=RentIncreaseData & "<td> New Value</td>"
			RentIncreaseData=RentIncreaseData & "<td> at 1st April</td>"
			'RentIncreaseData=RentIncreaseData & "<td> Target </td>"
			RentIncreaseData=RentIncreaseData & "</tr >" 
			RentIncreaseData=RentIncreaseData & "<tr><td class=""dottedline"" colspan=6 ></td></tr>" 
														
			if(not rsSet.eof) then


			
			While(not rsSet.eof)    
			
			Old_Rent_Total = Old_Rent_Total  + rsSet("UPLOAD_RENT")
			NEW_Rent_Total = NEW_Rent_Total  + rsSet("ACTUAL_RENT")
			TARGET_Rent_Total = TARGET_Rent_Total  + rsSet("TARGET_RENT")

				RentIncreaseData=RentIncreaseData & "<tr style='color:#133e71;'>"
				RentIncreaseData=RentIncreaseData & "<td><a href='/portfolio/prm.asp?propertyid=" & rsSet("propertyid") & "' target='_blank'>" & rsSet("ADDRESS") &"</a></td>"
				RentIncreaseData=RentIncreaseData & "<td>" & rsSet("VNAME") &"</td>"
				RentIncreaseData=RentIncreaseData & "<td>" & rsSet("STATUS") &"</td>"
				RentIncreaseData=RentIncreaseData & "<td>" & FormatCurrency(rsSet("UPLOAD_RENT"),2) &"</td>"
				RentIncreaseData=RentIncreaseData & "<td>" & FormatCurrency(rsSet("ACTUAL_RENT"),2) &"</td>"
				'RentIncreaseData=RentIncreaseData & "<td>" & FormatCurrency(rsSet("TARGET_RENT"),2) &"</td>"
				RentIncreaseData=RentIncreaseData & "<td></td>"
				RentIncreaseData=RentIncreaseData & "</tr >"
				rsSet.movenext()
			wend      
			else
				RentIncreaseData=RentIncreaseData & "<tr  >"
				RentIncreaseData=RentIncreaseData & "<td colspan=5>No records exist</td>"
				RentIncreaseData=RentIncreaseData & "</tr >"
			end if
			
			RentIncreaseData=RentIncreaseData & "<tr><td class=""dottedline"" colspan=6 ></td></tr>" 
			' TOTAL ROW	
				RentIncreaseData=RentIncreaseData & "<tr style='color:#133e71;'>"
				RentIncreaseData=RentIncreaseData & "<td>TOTALS</td>"
				RentIncreaseData=RentIncreaseData & "<td></td>"
				RentIncreaseData=RentIncreaseData & "<td></td>"
				RentIncreaseData=RentIncreaseData & "<td><B>" & FormatCurrency(OLD_RENT_TOTAL,2) &"</B></td>"
				RentIncreaseData=RentIncreaseData & "<td><B>"  & FormatCurrency(NEW_RENT_TOTAL,2) &"</B></td>"
				'RentIncreaseData=RentIncreaseData & "<td><B>"  & FormatCurrency(TARGET_RENT_TOTAL,2) &"</B></td>"
				RentIncreaseData=RentIncreaseData & "<td></td>"
				RentIncreaseData=RentIncreaseData & "</tr >"
			 RentIncreaseData=RentIncreaseData & "<tr><td class=""dottedline"" colspan=5 ></td></tr>" 
			 RentIncreaseData= RentIncreaseData & "</table></div>"
			 CloseRs(rsSet)
			 GetFileList = RentIncreaseData
    End Function
    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Rent File List</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
   // parent.document.getElementById("MonitoringSubHead").innerHTML = Heading.innerHTML;
    parent.document.getElementById("content_body2").innerHTML= FileListDetail.innerHTML;
return true;
}
</script>
<body onLoad="ReturnData()">
<form name = "thisForm" method="post" action="">
<div id="Heading"><%=Heading%></div>
<div  style=" width:100%; height:100px; overflow:auto; text-align:center;" id="FileListDetail"><%=GetFileListLines%></div>
</form>
</body>
</html>
<% response.end()%>