<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	FirstArraySize = 10
	SecondtArraySize = 28
	ReDim DataFields   (FirstArraySize)
	ReDim DataTypes    (FirstArraySize)
	ReDim ElementTypes (FirstArraySize)
	ReDim FormValues   (FirstArraySize)
	ReDim FormFields   (FirstArraySize)

	UpdateID	  = "hid_PropertyID"
	FormFields(0) = "txt_NOMINATINGBODY|TEXT"
	FormFields(1) = "sel_FUNDINGAUTHORITY|NUMBER"
	FormFields(2) = "txt_OMV|MONEY"
	FormFields(3) = "txt_OMVST|MONEY"
	FormFields(4) = "txt_INSURANCEVALUE|MONEY"
	FormFields(5) = "hid_PROPERTYID|TEXT"
	FormFields(6) = "hid_PFUSERID|NUMBER"
	FormFields(7) = "txt_EUV|MONEY"
	FormFields(8) = "sel_CHARGE|NUMBER"
	FormFields(9) = "txt_CHARGEVALUE|MONEY"
	FormFields(10) = "sel_RENTFREQUENCY|NUMBER"

	Function NewRecord ()
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)
		SQL = "INSERT INTO P_FINANCIAL " & strSQL & ""
		Conn.Execute SQL, recaffected
		SQL = "INSERT INTO P_FINANCIAL_HISTORY " & strSQL & ""
		Conn.Execute SQL, recaffected
		Call GO()
	End Function

	Function AmendRecord()
		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)

		' Check the property exists in p_financial/ if not ass new record
		CheckExists = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & ID  & "'"
		Call OpenRs(rsCheck, CheckExists)
		If rsCheck.eof Then
			Call NewRecord()
			Exit Function
		End If
		Call CloseRs(rsCheck)

		HISTORYSQL = "INSERT INTO P_FINANCIAL_HISTORY (PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue) " &_
			  		 " SELECT PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"
		'Conn.Execute HISTORYSQL, recaffected
		SQL = "UPDATE P_FINANCIAL " & strSQL & " WHERE PROPERTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected
		Call GO()
	End Function

	Function DeleteRecord(Id, theID)
		ID = Request.Form(UpdateID)
		SQL = "DELETE FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected
	End Function

	Function GO()
		' RUN SCRIPTS TO ENTER PART MONTH VOID ENTRIES
		Conn.Execute ("NL_LETTING_DATE_MONITORING")
		Response.Redirect "../Financial.asp?PropertyID=" & ID
	End Function

	TheAction = Request("hid_Action")

	Call OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	Call CloseDB()
%>