<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim CONST_PAGESIZE

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance
	Dim viewItemSelectedAll, viewItemSelected1, viewItemSelected2, viewItemSelected3, StrSrch
	Dim TotGross, TotBALANCE, TotPay
	Dim SOFilter
	    SOFilter = ""
	Dim wid,contractor
	Dim BollFlag
	    BollFlag = false
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	Dim filterOption, optAll, optRR1

	filterOption = Request.QueryString("wid")
	If filterOption = "" Then
		filterOption = 1
		optAll="selected"
	End If
	If filterOption = 2 Then
		filterOption = 2
		optRR1 = "selected"
	End If

	PageName = "FairRentReport.asp"
	MaxRowSpan = 6

    Call OpenDB()
	Call get_tenant_balance()
	Call get_Sum()
	Call CloseDB()

	function get_sum()
		Dim rsSet
		set rsSet = Server.CreateObject("ADODB.Recordset")
		    rsSet.ActiveConnection = RSL_CONNECTION_STRING
			If filterOption=2 Then
			    fil = " AND P_FINANCIAL.RENTEFFECTIVE < dateAdd(m,3,getdate()) "
			End If
		strSQL=" SELECT IsNull(Sum(P_FINANCIAL.TOTALRENT),0) As TotGross"&_
			   " FROM P__PROPERTY INNER JOIN "&_
			   " C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID INNER JOIN "&_
			   " P_FINANCIAL ON P__PROPERTY.PROPERTYID = P_FINANCIAL.PROPERTYID INNER JOIN "&_
			   " C_CUSTOMER_NAMES_GROUPED_VIEW ON C_TENANCY.TENANCYID = C_CUSTOMER_NAMES_GROUPED_VIEW.I INNER JOIN "&_
			   " C_TENANCYTYPE ON P_FINANCIAL.RENTTYPE = C_TENANCYTYPE.TENANCYTYPEID "&_
			   " WHERE (C_TENANCYTYPE.TENANCYTYPEID = 10) AND C_TENANCY.ENDDATE IS NULL " & FIL

		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.CursorLocation = 3
		rsSet.Open()
		If Not rsSet.eof Then
			TotGross=rsSet("TotGross")
		End If
		rsSet.close()
		Set rsSet = Nothing

	End Function

	Function get_tenant_balance()

			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount
			Dim Address,strCustomerid,CustomerNameStr

			intRecord = 0
			str_data = ""
			If filterOption = 2 Then
				fil=" AND F.RENTEFFECTIVE < dateAdd(m,3,getdate()) "
			End If
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			strSQL =  	" SELECT CNG.CLIST CUSTOMERID,T.TENANCYID TENANCYID,P.PROPERTYID PROPERTYID,  CNG.LIST AS CNAME, " &_
						" 		P.HOUSENUMBER, ISNULL(P.FLATNUMBER,'')FLATNUMBER, " &_ 
						" 		P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.COUNTY, P.POSTCODE, " &_
						"		isnull(F.TOTALRENT,0) as totalrent, T.STARTDATE, " &_
						"		convert(varchar(10),F.RENTEFFECTIVE,103) FREffectDate,CASE WHEN datediff(D,GETDATE(),RENTEFFECTIVE) <=0 THEN 'RED' WHEN datediff(D,GETDATE(),RENTEFFECTIVE) >0 THEN 'BLACK' END COL " &_
						" FROM  P__PROPERTY P " &_
						"		INNER JOIN C_TENANCY T ON P.PROPERTYID = T.PROPERTYID  " &_
						"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID " &_
						"		INNER JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNG ON T.TENANCYID = CNG.I "&_
						"		INNER JOIN C_TENANCYTYPE TT ON F.RENTTYPE = TT.TENANCYTYPEID " &_
						" WHERE  (TT.TENANCYTYPEID = 10) AND T.ENDDATE IS NULL "  & fil  &_
						" ORDER BY RENTEFFECTIVE"

			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()

			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount
			intRecordCount = rsSet.RecordCount

			' Sort pages
			intpage = CInt(Request("page"))

			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If

			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If

			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If

			' Make sure that the recordset is not empty.  If it is not, then set the
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End If
			End If

			count = 0

			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<tbody class=""CAPS"">"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize

					'///////////////////////////////////////////////////////////
					    ' Here we break up the array which is used to have multiple customers for link
					    'if rsSet("CUSTOMERID") <> "" then
						'		strCustomerid = Split(rsSet("CUSTOMERID"), ",")
						'
						'		if rsSet("CNAME") <> "" then
						'				strCustomerName = Split(rsSet("CNAME"), ",")
						'		else 
						'				strCustomerName = rsSet("CNAME")
						'		end if
						'
						'		iold = 0
						'		For i = 0 to Ubound(strCustomerid)
						'			if i > iold then customerNameStr  = CustomerNameStr & " , " end if
						'			CustomerNameStr  = CustomerNameStr & strCustomerName(i)
						'			iold = i
						'		Next
						'else
						'		CustomerNameStr = CustomerNameStr & "<font color=""blue"" onclick=""LoadCustomer(" & rsSet("CUSTOMERID") & ")"">" & rsSet("FULLNAME")& "</font> "
						'end if
                        
                        Address="'" & cstr(rsSet("HOUSENUMBER")) & " " & rsSet("ADDRESS1") & " " & rsSet("ADDRESS2")& " " & rsSet("ADDRESS3") & " " & rsSet("TOWNCITY")& " " & rsSet("COUNTY")& " " & rsSet("POSTCODE") & "'"
                        strStart =  "<tr style=""cursor:pointer"" onclick='loadPopup(""" & rsSet("PROPERTYID") & """)'><td style='color:" & rsSet("COL") &"'>" & TenancyReference(rsSet("TENANCYID")) & "</td>" 
						strEnd =	"<td style='color:" & rsSet("COL") &"' title="  & Address & " >" &  rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & " " & rsSet("TOWNCITY") & "</td>" &_
									"<td style='color:" & rsSet("COL") &"' align=""right""> " & rsSet("FREffectDate") & "</td>" &_
									"<td style='color:" & rsSet("COL") &"' align=""right"">" & FORMATNUMBER(rsSet("TOTALRENT"),2) & "</td></tr>" 		
						str_data = str_data & strStart & "<td>" & rsSet("CNAME") & "</td>" & strEnd

						count = count + 1
						CustomerNameStr = ""
						rsSet.movenext()
						If rsSet.EOF Then Exit for 

					Next
					str_data = str_data & "</tbody>"
	
					'ensure table height is consistent with any amount of records
					Call fill_gaps()
					
					' links
					str_data = str_data &_
					"<tfoot>" & vbcrlf &_
                    "<tr>" & vbcrlf &_
                    "<td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" & vbcrlf &_
					"<table cellspacing=""0"" cellpadding=""0"" width=""100%"">" & vbcrlf &_
                    "<thead>" & vbcrlf &_
                    "<tr>" & vbcrlf &_
                    "<td width=""100""></td>" & vbcrlf &_
                    "<td align=""center"">"  &_
					"<a href='" & PageName & "?page=1&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&wid=" & REQUEST("wid") & "'><b><font color=""blue"">First</font></b></a> "  &_
					"<a href='" & PageName & "?page=" & prevpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&wid=" & REQUEST("wid") & "'><b><font color=""blue"">Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <a href='" & PageName & "?page=" & nextpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&wid=" & REQUEST("wid") & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
					" <a href='" & PageName & "?page=" & intPageCount & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&wid=" & REQUEST("wid") & "'><b><font color=""blue"">Last</font></b></a>"  &_
					"</td>" & vbcrlf &_
                    "<td align=""right"" width=""100"">" & vbcrlf &_
                    "Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value='' size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"" />&nbsp;"  &_
					"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"" />" & vbcrlf &_
					"</td>" & vbcrlf &_
                    "</tr>" & vbcrlf &_
                    "</thead>" & vbcrlf &_
                    "</table>" & vbcrlf &_
                    "</td>" & vbcrlf &_
                    "</tr>" & vbcrlf &_
                    "</tfoot>"
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<tr>" & vbcrlf &_
                "<td colspan=""" & MaxRowSpan & """ align=""center"">No records found</td>" & vbcrlf &_
                "</tr>" & vbcrlf
				count = 1
				Call fill_gaps()
			End If

			rsSet.close()
			Set rsSet = Nothing

		End function

		' pads table out to keep the height consistent
		Function fill_gaps()

			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<tr>" & vbcrlf &_
                "<td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td>" & vbcrlf &_
                "</tr>" & vbcrlf
				cnt = cnt + 1
			wend

		End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --&gt; Supplier Balances</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/vbscript" language="vbscript">
function checkDate()
	
	checkDate=DateDiff("d",Date,document.getElementById("txt_jobdate").value)
	
end function

function setJobDatevb()

		if cint(Weekday( DateAdd("d",1,document.getElementById("txt_deliverydate").value)) ) <>1 then
			document.getElementById("txt_jobdate").value=DateAdd("d",1,document.getElementById("txt_deliverydate").value)
		else
			document.getElementById("txt_jobdate").value=DateAdd("d",2,document.getElementById("txt_deliverydate").value)
		end if
		
end function
    </script>
    <script type="text/javascript" language="JavaScript">

    function LoadProperty(PropertyId)
    {
	    parent.location.href = "../Portfolio/PRM.asp?PropertyID=" + PropertyId +"&top_iframe=ifinancial"
	}                       
	
	function test()
	{
		alert("sdf")
	}

	function loadPopup(PROPERTYID)
	{
		if (window.showModelessDialog) {        // Internet Explorer
		    window.showModalDialog("Popups/pRentUpdate.asp?PROPERTYID=" + PROPERTYID + "&rand=" + new Date(), document, "dialogHeight: 405px; dialogWidth: 380px; status: No; resizable: No;");
		  } 
        else {
            window.open ("Popups/pRentUpdate.asp?PROPERTYID=" + PROPERTYID + "&rand=" + new Date(), "","width=375px, height=340px, alwaysRaised=yes, scrollbars=no");
        }
        window.location.reload(true);    
	}

	var FormFields = new Array()
    	FormFields[0] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function setJobDate()
	{		
		FormFields[0] = "txt_deliverydate|Delivery Date|DATE|Y"
		if (!checkForm()) return false;
		setJobDatevb()
	}
	
	function JumpPage(){		
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "FairRentReport.asp?page="+iPage+"&PAGESIZE="+<%=CONST_PAGESIZE%>+"&wid="+document.getElementById("ddFilter").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function click_go(){
		FormFields[1] = "sel_Warehouse|Warehouse|SELECT|Y"
		if (!checkForm()) return false;
		location.href = "FairRentReport.asp?page=1&PAGESIZE="+<%=CONST_PAGESIZE%>+"&wid="+document.getElementById("ddFilter").value
		}
		
	function Save_Form()
	{
		var totlCheck=0
		var frm = document.Form1;
		
		for(i=0;i< frm.length;i++)
		{
			e=frm.elements[i];
			if ( e.type=='checkbox' && e.checked)
			totlCheck++
		}
		
		FormFields[0] = "sel_contractor|Contractor|SELECT|Y"
		FormFields[1] = "txt_jobdate|Jobdate|DATE|Y"
		if (!checkForm()) return false;
		if(checkDate()<0)
		{
			alert("Job Date must always be future a date")
			return false;
		}
		if(totlCheck==0)
		{
			alert("Select Atleast One Sale")
			return false;
		}		
	}

	function Loaddata()
	{
		location.href="FairRentReport.asp?wid=" + document.getElementById("ddFilter").value
	}

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" id="Form1" action="">
    <table>
        <tr>
            <td>
                Filter: &nbsp;
            </td>
            <td>
                <select id="ddFilter" class="textbox" style="width: 70px" onchange="Loaddata()">
                    <option <%=optAll%> value="1">ALL</option>
                    <option <%=optRR1%> value="2">RR1</option>
                </select>
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="1" style="border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="#4682b4" border="1"
        id="Table1">
        <thead>
            <tr>
                <td width="50">
                    &nbsp;<b>Tenancy</b>
                </td>
                <td width="350px">
                    &nbsp;<b>Customer</b>
                </td>
                <td width="300px">
                    &nbsp;<b>Address</b>
                </td>
                <td width="100px" align="right">
                    &nbsp;<b>Date Expires</b>&nbsp;
                </td>
                <td width="100px" align="right">
                    &nbsp;<b>Rent</b>&nbsp;
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="10" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
        <tr style="background-color: white">
            <td style="background-color: white; color: black" colspan="4" align="right">
                <b>Total : </b>
            </td>
            <td style="background-color: white; color: black" align="right">
                <b>
                    <%=formatcurrency(TotGross)%>
                </b>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe name="frm_team" id="frm_team" width="4px" height="4px" style="display: none"
        src="/SecureFrame.asp"></iframe>
</body>
</html>
