<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstDevelopments, lstStatus, lstSubStatus, lstAssetType, lstPropertyType, lstLevel, lstBlocks

	Call OpenDB()
	Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", l_Patch, NULL, "textbox200","onChange=GetSchemes()")	
	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "P_development", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", NULL, NULL, "textbox200", " onchange=""GetBlocks()""")
	Call BuildSelect(lstStatus, "sel_STATUS", "P_STATUS", "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " onchange=""GetSubStatus()""")
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_assettype, NULL, "textbox200", NULL)
	Call BuildSelect(lstPropertyType, "sel_PROPERTYTYPE", "P_PROPERTYTYPE", "PROPERTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lstLevel, "sel_PROPERTYLEVEL", "P_LEVEL", "LEVELID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lstAdapatationCat, "sel_ADAPTATIONCAT", "P_ADAPTATIONCAT", "ADAPTATIONCATID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200",  " onchange=""GetAdaptations()""")
	Call BuildSelect(lstUnAdapatationCat, "sel_UNADAPTATIONCAT", "P_ADAPTATIONCAT", "ADAPTATIONCATID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio--&gt; Main Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|N"
        FormFields[1] = "sel_STATUS|Status|SELECT|N"
        FormFields[2] = "sel_SUBSTATUS|Sub - Status|SELECT|N"
        FormFields[3] = "sel_DEVELOPMENTID|Development|SELECT|N"
        FormFields[4] = "sel_BLOCKID|Block|SELECT|N"
        FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|N"
        FormFields[6] = "sel_ADAPTATIONCAT|Adaptation Cat|SELECT|N"
        FormFields[7] = "sel_UNADAPTATIONCAT|Unavailable Cat|SELECT|N"
        FormFields[8] = "txt_PROPERTYREF|Property Ref|TEXT|N"
        FormFields[9] = "sel_PATCH|Patch|SELECT|N"


        function SearchNow() {
            if (!checkForm()) return false
            if (document.getElementById("sel_ADAPTATIONCAT").value != "" && document.getElementById("sel_UNADAPTATIONCAT").value != "") {
                ManualError("img_ADAPTATIONCAT", "Please select only one from 'Adaptation Cat' and 'Unavailable Cat'", 0)
                ManualError("img_UNADAPTATIONCAT", "Please select only one from 'Adaptation Cat' and 'Unavailable Cat'", 0)
                return false
            }
            document.RSLFORM.action = "ServerSide/PropertySearch_svr.asp"
            document.RSLFORM.target = "SearchResults"
            document.RSLFORM.submit()
            return false
        }

        function GetSchemes() {
            document.getElementById("sel_DEVELOPMENTID").disabled = true
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Schemes.asp"
            document.RSLFORM.submit()
        }

        function GetBlocks() {
            document.getElementById("sel_BLOCKID").disabled = true
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Blocks.asp"
            document.RSLFORM.submit()
        }

        function GetSubStatus() {
            document.getElementById("sel_SUBSTATUS").disabled = true
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_SubStatus.asp"
            document.RSLFORM.submit()
        }

        function GetAddressData() {
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Address.asp"
            document.RSLFORM.submit()
        }

        function GetAdaptations() {
            document.getElementById("sel_ADAPTATIONS").disabled = true
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Adaptations.asp"
            document.RSLFORM.submit()
        }

        function AdvancedSearch() {
            advref = document.getElementsByName("ADV")
            if (advref.length) {
                for (i = 0; i < advref.length; i++) {
                    advref[i].style.display = "block";
                    advref[i].style.display = 'table-row';
                }
            }
            document.getElementById("ADVANCED").value = 1
            document.getElementById("tab_standardsearch").src = "images/tab_find_property.gif"
            document.getElementById("tab_advancedsearch").src = "images/tab_advance_search-over.gif"
        }

        function StandardSearch() {
            advref = document.getElementsByName("ADV")
            if (advref.length) {
                for (i = 0; i < advref.length; i++)
                    advref[i].style.display = "none"
            }
            document.getElementById("ADVANCED").value = 0
            document.getElementById("tab_standardsearch").src = "images/tab_find_property-over.gif"
            document.getElementById("tab_advancedsearch").src = "images/tab_advance_search-tab_find.gif"
        }

        function ResetRest() {
            document.getElementById("dvADAPTATIONS").innerHTML = "<select name='sel_ADAPTATIONS' id='sel_ADAPTATIONS' class='textbox200' disabled='disabled'><option value=''>Please Select an Adapt' Cat'</option></select>";
            document.getElementById("sel_ADAPTATIONS").disabled = true
            document.getElementById("dvSUBSTATUS").innerHTML = "<select name='sel_SUBSTATUS' id='sel_SUBSTATUS' class='textbox200' disabled='disabled'><option value=''>Not Applicable</option></select>";
            document.getElementById("sel_SUBSTATUS").disabled = true
            document.getElementById("dvBLOCKID").innerHTML = "<select name='sel_BLOCKID' id='sel_BLOCKID' class='textbox200' disabled='disabled'><option value=''>Please Select</option></select>";
            document.getElementById("sel_BLOCKID").disabled = true
            StandardSearch()
        }

    </script>
</head>
<body onload="initSwipeMenu(4);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table cellspacing="0" cellpadding="0" style="height: 100%">
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" style="cursor: pointer">
                            <img name="tab_standardsearch" id="tab_standardsearch" src="images/tab_find_property-over.gif"
                                width="117" height="20" border="0" onclick="StandardSearch()" title="Find a Property"
                                alt="Find a Property" style="cursor: pointer" /></td>
                        <td rowspan="2" style="cursor: pointer">
                            <img name="tab_advancedsearch" id="tab_advancedsearch" src="images/tab_advance_search-tab_find.gif"
                                width="139" height="20" border="0" onclick="AdvancedSearch()" title="Advanced Search"
                                alt="Advanced Search" style="cursor: pointer" /></td>
                        <td>
                            <img src="images/spacer.gif" width="74" height="19" alt="" /></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133e71">
                            <img src="images/spacer.gif" width="74" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="images/spacer.gif" width="53" height="6" alt="" /></td>
                    </tr>
                </table>
                <table width="233" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
                    border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            Property Ref:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_PROPERTYREF" id="txt_PROPERTYREF" class="textbox200"
                                maxlength="50" style="text-transform: uppercase" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_PROPERTYREF" id="img_PROPERTYREF" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            Status:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstStatus%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Sub - Status:
                        </td>
                        <td nowrap="nowrap">
                            <div id="dvSUBSTATUS">
                                <select name="sel_SUBSTATUS" id="sel_SUBSTATUS" class="textbox200" disabled="disabled">
                                    <option value=''>Not Applicable</option>
                                </select></div>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_SUBSTATUS" id="img_SUBSTATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            Property Type:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstPropertyType%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_PROPERTYTYPE" id="img_PROPERTYTYPE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Patch:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstPatch%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_PATCH" id="img_PATCH" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Scheme:
                        </td>
                        <td nowrap="nowrap">
                            <div id="dvDEVELOPMENTID">
                                <%=lstDevelopments%></div>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_DEVELOPMENTID" id="img_DEVELOPMENTID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Scheme Name:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_SCHEMENAME" id="txt_SCHEMENAME" class="textbox200" readonly="readonly"
                                maxlength="50" style="border: 1px dotted #133e71" />
                        </td>
                        <td nowrap="nowrap">
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Block:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <div id="dvBLOCKID">
                                <select name="sel_BLOCKID" id="sel_BLOCKID" class="textbox200" disabled="disabled">
                                    <option value=''>Please Select</option>
                                </select></div>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_BLOCKID" id="img_BLOCKID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Street:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_ADDRESS1" id="txt_ADDRESS1" class="textbox200" maxlength="100" />
                            <input type="hidden" name="txt_ADDRESS2" id="txt_ADDRESS2" />
                            <input type="hidden" name="txt_ADDRESS3" id="txt_ADDRESS3" />
                            <input type="hidden" name="txt_TOWNCITY" id="txt_TOWNCITY" />
                            <input type="hidden" name="txt_COUNTY" id="txt_COUNTY" />
                            <input type="hidden" name="DONTDOIT" id="DONTDOIT" value="1" />
                            <input type="hidden" name="ADVANCED" id="ADVANCED" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Postcode:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_POSTCODE" id="txt_POSTCODE" class="textbox200" maxlength="10" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Adaptation Cat:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <div id="dvADAPTATIONS_">
                                <%=lstAdapatationCat%></div>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADAPTATIONCAT" id="img_ADAPTATIONCAT" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Adaptation Type:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <div id="dvADAPTATIONS">
                                <select name="sel_ADAPTATIONS" id="sel_ADAPTATIONS" class="textbox200" disabled="disabled">
                                    <option value="">Please Select an Adapt' Cat'</option>
                                </select></div>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADAPTATIONS" id="img_ADAPTATIONS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Unavailable Cat:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <%=lstUnAdapatationCat%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_UNADAPTATIONCAT" id="img_UNADAPTATIONCAT" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr name="ADV" style="display: none">
                        <td nowrap="nowrap">
                            <i><font color="#133e71">Asset Type:</font></i>
                        </td>
                        <td nowrap="nowrap">
                            <%=lstAssetType%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ASSETTYPE" id="img_ASSETTYPE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="2" align="right">
                            <input type="reset" onclick="ResetRest()" value=" RESET " title=" RESET " class="RSLButton"
                                style="cursor: pointer" />
                            <input type="button" onclick="SearchNow()" value=" SEARCH " title=" SEARCH " class="RSLButton"
                                style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td valign="top" height="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <img name="tab_main_details" src="images/tab_search_results.gif" width="122" height="20"
                                border="0" alt="Search Results" /></td>
                        <td>
                            <img src="images/spacer.gif" width="303" height="19" alt="" /></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133e71">
                            <img src="images/spacer.gif" width="303" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="images/spacer.gif" width="53" height="6" alt="" /></td>
                    </tr>
                </table>
                <table width="425" border="0" cellpadding="2" cellspacing="0" style="height: 90%;
                    border-right: 1px solid #133E71; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
                    <tr>
                        <td nowrap="nowrap" height="100%" valign="top">
                            <iframe src="/secureframe.asp" name="SearchResults" id="SearchResults" height="400"
                                width="100%" frameborder="0"></iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Property_Server" id="Property_Server" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
