<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_DIMS = "width=""750"" height=""200"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim property_id, underconstruction
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev, disa_bled
	Dim defectperiodend,defectperiodflag
    Dim str_tenantId
    str_tenantId = 0

	property_id = Request("propertyid")
	Call OpenDB()

	' CHECK PROPERTY STATUS TO DISABLE WORK ORDER CREATION IF STILL UNDER CONSTRUCTION
	SQL = "SELECT ISNULL(STATUS,-1) AS STATUS, ISNULL(SUBSTATUS,-1) AS SUBSTATUS,ISNULL(DEFECTSPERIODEND,getdate()-1) AS DEFECTSPERIODEND FROM P__PROPERTY WHERE PROPERTYID = '" & property_id & "'"
	Call OpenRs(rsSet, SQL)
	If Not rsSet.EOF Then
        If (Cint(rsSet("STATUS")) = 4 And Cint(rsSet("SUBSTATUS")) = 1)	Then
			underconstruction = "YES"
		End If
		defectperiodend = rsSet("DEFECTSPERIODEND")
		If(defectperiodend <> "" And datediff("d",Date(),defectperiodend)>0 ) Then
		    defectperiodflag = 1
		Else
		    defectperiodflag = 0
		End If
	End If
	
    strSQL = ""
    strSQL = 	"SELECT DISTINCT LTRIM(ISNULL(TIT.DESCRIPTION, '') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS FULLNAME, " &_
					"		C.CUSTOMERID, T.TENANCYID " &_
					"FROM 	P__PROPERTY P " &_
					"		LEFT JOIN C_TENANCY T ON P.PROPERTYID = T.PROPERTYID " &_
					"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"		LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
					"		LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " &_
					"		LEFT JOIN C_ADDITIONALASSET A ON A.TENANCYID = T.TENANCYID AND (A.ENDDATE > GETDATE() OR A.ENDDATE IS NULL) " &_
					"WHERE 	(P.PROPERTYID = '" & property_id & "' OR A.PROPERTYID = '" & property_id & "') " &_
					"		   AND (  (CT.ENDDATE > GETDATE() OR CT.ENDDATE IS NULL ) " &_
					"			 AND (T.ENDDATE > GETDATE() OR T.ENDDATE IS NULL)  ) "

	Call OpenRs (rsSet,strSQL)
        
    If Not rsSet.EOF Then
        str_tenantId = rsSet("TENANCYID")
    End If
    
    Call CloseRs(rsSet)
	Call BuildSelect(lst_nature, "sel_NATURE", " C_NATURE WHERE ITEMID = 1 AND ITEMNATUREID IN (2,20,21,22)", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "All", NULL, "width:110", "textbox100", " onchange=""javascript:STARTLOADER('BOTTOM');PRM_BOTTOM_FRAME.location.href='iFrames/iRepairJournal.asp?propertyid=" & property_id & "&natureid='+this.value+'&SyncTabs=1'"" " )
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Portfolio -- &gt; Portfolio Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/Loading.js"></script>
    <script type="text/javascript" language="JavaScript">

        var MASTER_OPEN_WORKORDER
        MASTER_OPEN_WORKORDER = ""

        var iFrameArray = new Array("empty",
								"imaindetails", "ifinancial", "iaccomodation", "iwarranties", "iutilities", "iattributes", "icyclicalrepairs", "ipropertyhistory", "iPlanMaintenance",
								"iPropertyJournal", "inewitem", "iRepairJournal", "iPlannedWorks", "iServicingJournal", "", "")
        var MAIN_OPEN_BOTTOM_FRAME = 10
        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        // where is either 'top' or 'bottom' relating to the tab area of the page
        // FULL RELOAD FUNCTIONALITY NEEDS TO BE IMPLEMETED TO RELOAD ALL DYNAMIC DIVS -- PDP
        function swap_div(int_which_one, str_where) {

            var divid, imgid, upper

            if (str_where == 'top') {
                upper = 9; lower = 1;
                STARTLOADER('TOP')
                PRM_TOP_FRAME.location.href = "/portfolio/iframes/" + iFrameArray[int_which_one] + ".asp?propertyid=<%=property_id%>";
            }
            else {
                upper = 15; lower = 10;
                STARTLOADER('BOTTOM')
                MAIN_OPEN_BOTTOM_FRAME = int_which_one
                PRM_BOTTOM_FRAME.location.href = "/portfolio/iframes/" + iFrameArray[int_which_one] + ".asp?propertyid=<%=property_id%>&CWO=" + MASTER_OPEN_WORKORDER + "&natureid=" + RSLFORM.sel_NATURE.value;
            }
            imgid = "img" + int_which_one.toString();

            // manipulate images
            for (j = lower; j <= upper; j++)
                document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"

            // unless last image in row
            if (int_which_one != 16)
                if (int_which_one != upper)
                    document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
                document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
            }

            function synchronize_tabs(int_which_one, str_where) {

                var divid, imgid, upper
                if (str_where == 'top') { upper = 7; lower = 1; }
                else { upper = 14; lower = 10; }

                imgid = "img" + int_which_one.toString();
                for (j = lower; j <= upper; j++) {
                    document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
                }

                if (int_which_one != 16)
                    if (int_which_one != upper)
                        document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"

                    //document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
                }

                // receives the url of the page to open plus the required width and the height of the popup
                function update_record(str_redir, wid, hig) {
                    window.open(str_redir, "display", "width=" + wid + ",height=" + hig + ",left=100,top=200");
                }

                function work_order() {
                    var w = 790;
                    var h = 640;
                    var left = (window.screen.width / 2) - (w / 2);
                    var top = (window.screen.height / 2) - (h / 2);
                    if ("<%=defectperiodflag%>" == 1) {
                        alert("The defect period for the property end at: " + "<%=defectperiodend%>")
                    }
                    window.open("popups/WorkOrder.asp?propertyid=<%=property_id%>", "_blank", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=790, height=640, top=' + top + ', left=' + left);
                }

                function load_inspection() {
                    window.showModelessDialog("popups/pNewInspection.asp?propertyid=<%=property_id%>", "", "dialogHeight: 600px; dialogWidth: 590px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: Yes");
                }

                function open_Reactive_Repair() {
                    window.open("../../faultlocator/secure/ReactiveRepair/property_reactive_repair.aspx?propertyid=<%=property_id%>", "", "width=900,height=600,left=100,top=100,scrollbars=1,resizable=1");
                }

                function open_PropertyJournal() {
                    window.open("../../RSLApplianceServicing/Bridge.aspx?cmd=rsl&pid=<%=property_id%>", "", "width=1100,height=700,left=100,top=100,scrollbars=1,resizable=1");
                }

                function post_cash() {

                    if(<%=str_tenantId%> > 0 ){
                        var strcontent, popupWidth, popupHeight, popupLeft, popupTop;

                        strcontent = navigator.userAgent;
                        strcontent = strcontent.toLowerCase();

                        popupWidth = 440;
                        popupHeight = 340;
                        popupLeft = (screen.width / 2) - (popupWidth / 2);
                        popupTop = (screen.height / 2) - (popupHeight / 2);

                        if (strcontent.search("chrome") > 0) {
                            window.open("../Finance/SalesInvoice/popups/cashposting.asp?AccountType=1&AccountID=<%=str_tenantId%>", "SalesPosting", "height=" + popupHeight + "px; width=" + popupWidth + "px; left=" + popupLeft + "px; top=" + popupTop + "px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
                            return;
                        }
                        else {
                            window.showModalDialog("../Finance/SalesInvoice/popups/cashposting.asp?AccountType=1&AccountID=<%=str_tenantId%>", "SalesPosting", "dialogheight: " + popupHeight + "px; dialogwidth: " + popupWidth + "px; dialogleft: " + popupLeft + "px; dialogtop: " + popupTop + "px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
                            return;
                        }
                    }
                    else
                        alert("There is no tenant in current property, Unable to post cash invoice,")
                }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();" onunload="macGo()" class="ta">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="img1" id="img1" title='Main Details' src="images/1-open.gif" width="97"
                    height="20" border="0" onclick="swap_div(1, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img2" id="img2" title='Financial Details' src="images/2-previous.gif"
                    width="79" height="20" border="0" onclick="swap_div(2, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img3" id="img3" title='Accomodation' src="images/3-closed.gif" width="84"
                    height="20" border="0" onclick="swap_div(3, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img4" id="img4" title='Warranties' src="images/4-closed.gif" width="89"
                    height="20" border="0" onclick="swap_div(4, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img5" id="img5" title='Utilities' src="images/5-closed.gif" width="68"
                    height="20" border="0" onclick="swap_div(5, 'top')" style="cursor: pointer; display: none"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img7" id="img7" title='Cyclical Repairs' src="images/7-closed.gif" width="124"
                    height="20" border="0" onclick="swap_div(7, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img8" id="img8" title='Property History' src="images/8-closed.gif" width="75"
                    height="20" border="0" onclick="swap_div(8, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img9" id="img9" title='Plan Maintenance' src="images/9-closed.gif" width="74"
                    height="20" border="0" onclick="swap_div(9, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img6" id="img6" title='Attributes' src="images/6-closed.gif" width="92"
                    height="20" border="0" onclick="swap_div(6, 'top')" style="cursor: pointer" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#004376" valign="bottom">
                            <img src="images/spacer.gif" width="35" height="1" border="0" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="TOP_DIV" style="display: block; overflow: hidden">
        <iframe name="PRM_TOP_FRAME" <%=TABLE_DIMS%> src="iframes/iMainDetails.asp?propertyid=<%=property_id%>"
            style="overflow: hidden" frameborder="0"></iframe>
    </div>
    <div id="TOP_DIV_LOADER" style="display: none; overflow: hidden; width: 750; height: 200"
        <%=TABLE_DIMS%>>
        <table width="750" height="180" style="border-right: solid 1px #133e71" cellpadding="1"
            cellspacing="2" border="0" class="TAB_TABLE">
            <tr>
                <td rowspan="2" width="70%" height="100%">
                    <table height="100%" width="100%" style="border: solid 1px #133e71; border-collapse: collapse"
                        cellpadding="3" cellspacing="0" border="0" class="TAB_TABLE">
                        <tr>
                            <td width="17px">
                            </td>
                            <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                                <div id="LOADINGTEXT_TOP">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom" width="54">
                <img name="img10" id="img10" src="images/10-open.gif" width="54" height="20" border="0"
                    onclick="swap_div(10, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom" width="82">
                <img name="img11" id="img11" src="images/11-previous.gif" width="82" height="20"
                    border="0" onclick="swap_div(11, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom" width="120">
                <img name="img12" id="img12" src="images/12-closed.gif" width="120" height="20" border="0"
                    onclick="swap_div(12, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img13" id="img13" src="images/13-closed.gif" width="74" height="20" border="0"
                    onclick="swap_div(13, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img14" id="img14" src="images/14-closed.gif" width="80" height="20" border="0"
                    onclick="swap_div(14, 'bottom')" style="cursor: pointer" alt="" /><img name="img15"
                        id="img15" src="images/15-closed.gif" width="80" height="20" border="0" onclick="swap_div(15, 'bottom')"
                        style="cursor: pointer" alt="" />
            </td>
            <td width="62" align="center" valign="middle">
                <img alt="" name="cpost" src="images/cpost.gif" width="27" height="22" title='Post cash/chq to this tenancy'
                    style='cursor: hand' onclick='post_cash()' <%=disa_bled%> />
                <%If underconstruction = "YES" Then %>
                <img src="/myImages/Repairs/sbasket.gif" title="Create new work order" width="27"
                    height="22" style="cursor: pointer" onclick="alert('Works Order is not available to properties still under contstruction.')"
                    alt="" />
                <% Else %>
                <img src="/myImages/Repairs/sbasket.gif" title="Create new work order" width="27"
                    height="22" style="cursor: pointer" onclick="work_order()" alt="" />
                <% End If %>
                <!-- <img src="images/inspection.gif" width="27" height="22" style="cursor:pointer" onclick="load_inspection()" alt="" /> -->
            </td>
            <td height="19" align="right" valign="top" width="140">
                <table width="140" cellpadding="2" height="50%" cellspacing="0" border="0">
                    <tr>
                        <td align="right">
                            <%=lst_nature%>
                        </td>
                        <td align="right">
                            <b><a href="#" onclick="javascript:open_Reactive_Repair()"><font color="blue">Repairs</font></a></b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#004174">
                <img src="images/spacer.gif" width="260" height="1" alt="" />
            </td>
        </tr>
    </table>
    <div id="BOTTOM_DIV" style="display: block; overflow: hidden">
        <table width="750" height="210" style="border-right: solid 1px #133e71" cellpadding="1"
            cellspacing="0" border="0" class="TAB_TABLE">
            <tr>
                <td>
                    <iframe name="PRM_BOTTOM_FRAME" src="iFrames/iPropertyJournal.asp?propertyid=<%=property_id%>"
                        width="100%" height="100%" frameborder="0" style="border: none"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="BOTTOM_DIV_LOADER" style="display: none; overflow: hidden; width: 750; height: 210">
        <table width="750" height="210" style="border-right: solid 1px #133e71" cellpadding="10"
            cellspacing="10" border="0" class="TAB_TABLE">
            <tr>
                <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                    <div id="LOADINGTEXT_BOTTOM">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" name="null" id="null" />
    <script type="text/javascript" language="JavaScript">
<!--
	<% 	gs_journalid = request("gs_journalid")
		if gs_journalid <> ""  then %>
		PRM_BOTTOM_FRAME.location.href = "iframes/iservicingDetail.asp?journalid=<%=gs_journalid%>&natureid=47"
	<% end if %>
// -->
    </script>
    </form>
    <iframe src="/secureframe.asp" name="frm_prm_srv" id="frm_prm_srv" width="400px"
        height="400px" style="display: none"></iframe>
    <!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
</body>
</html>
