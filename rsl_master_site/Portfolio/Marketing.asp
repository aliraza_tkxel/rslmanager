<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim l_address1

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then PropertyID = -1 End If

	'--Retrieve First Line of Address
	SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenDB()
	Call OpenRs(rsAddress, SQL)
	If (NOT rsAddress.EOF) Then
		l_address1 = rsAddress("ADDRESS1")
	Else
		l_address1 = "[None Given]"
	End If
	l_housenumber = rsAddress("HOUSENUMBER")
	l_propertyid = UCASE(rsAddress("PROPERTYID"))
	l_propertystatus = l_propertyid
	Call CloseRs(rsAddress)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio --&gt; marketing</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_VIEWINGDETAILS|NOMINATING BODY|TEXT|Y"
        FormFields[1] = "txt_AVAILABLEFROM|FUNDING AUTHORITY|TEXT|N"
        FormFields[2] = "sel_MOSTSUITABLE|MONTHLY RENT|DATE|N"
        FormFields[3] = "txt_NRSCHOOL|RENT TYPE|DATE|N"
        FormFields[4] = "txt_NURSERY|DATE RENT SET|DATE|N"
        FormFields[5] = "txt_HCENTRE|RENT EFFECT|DATE|N"
        FormFields[6] = "txt_LIBRARY|TARGET RENT|TEXT|N"
        FormFields[7] = "txt_POFFICE|YIELD|TEXT|N"
        FormFields[8] = "txt_POLICE|CAPITAL VALUE|TEXT|N"
        FormFields[9] = "txt_SPECIALFEAT|INSURANCE VALUE|TEXT|N"

        function limitText(textArea, length) {
            if (textArea.value.length > length) {
                textArea.value = textArea.value.substr(0, length);
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(0);" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_main_details" src="images/tab_main_details.gif" width="97" height="20"
                        border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0"
                        alt="" /></a></td>
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_warranties" src="images/tab_warranties.gif" width="89" height="20"
                        border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_utilities" src="images/tab_utilities.gif" width="68" height="20" border="0"
                        alt="" /></a></td>
            <td rowspan="2">
                <img name="tab_marketing" src="images/6-open.gif" width="83" height="20" border="0" alt="" /></td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_hands" src="images/tab_healthsafety_marketing-over.gif" width="110"
                        height="20" border="0" alt="" /></a></td>
            <td>
                <img src="/myImages/spacer.gif" width="144" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="images/spacer.gif" width="144" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style="height: 7px">
            <td width="23%">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top">
                Viewing Details:
            </td>
            <td>
                <textarea class="textbox200" name="txt_VIEWINGDETAILS" id="txt_VIEWINGDETAILS" rows="4"
                    cols="10" onkeypress="limitText(this,499);" tabindex="4"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_VIEWINGDETAILS" id="img_VIEWINGDETAILS" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap" valign="top">
                Special Features:
            </td>
            <td>
                <textarea class="textbox200" name="txt_SPECIALFEAT" id="txt_SPECIALFEAT" rows="4"
                    cols="10" onkeypress="limitText(this,499);" tabindex="4"></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SPECIALFEAT" id="img_SPECIALFEAT" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Available From:
            </td>
            <td>
                <input type="text" name="txt_AVAILABLEFROM" id="txt_AVAILABLEFROM" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_AVAILABLEFROM" id="img_AVAILABLEFROM" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td>
                Image:
            </td>
            <td>
                <input type="text" name="textfield835" id="textfield835" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_textfield835" id="img_textfield835" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Most Suitable for:
            </td>
            <td>
                <select name="sel_MOSTSUITABLE" id="sel_MOSTSUITABLE" class="textbox200">
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOSTSUITABLE" id="img_MOSTSUITABLE" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td align="right">
                <input type="button" name="Submit32" id="Submit32" value="Add" title="Add" class="RSLBUTTON"
                    style="cursor: pointer" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st School:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_NRSCHOOL" id="txt_NRSCHOOL" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NRSCHOOL" id="img_NRSCHOOL" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <input type="text" name="textfield836" id="textfield836" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_textfield836" id="img_textfield836" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st Nursery:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_NURSERY" id="txt_NURSERY" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NURSERY" id="img_NURSERY" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td align="right">
                <input type="button" name="Submit322" id="Submit322" value="Add" title="Add" class="RSLBUTTON"
                    style="cursor: pointer" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st Health Centre:
            </td>
            <td>
                <input type="text" name="txt_HCENTRE" id="txt_HCENTRE" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HCENTRE" id="img_HCENTRE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td rowspan="4" valign="bottom">
                <img src="images/large_blank.gif" height="80px" width="100px" border="1" align="bottom"
                    alt="" />
                <img src="images/large_blank.gif" height="40px" width="50px" border="1" align="bottom"
                    alt="" />
                <input type="button" name="btn_SAVE" id="btn_SAVE" value="Save" class="RSLBUTTON"
                    style="cursor: pointer" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st Library:
            </td>
            <td>
                <input type="text" name="txt_LIBRARY" id="txt_LIBRARY" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LIBRARY" id="img_LIBRARY" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td nowrap="nowrap">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st Post Office:
            </td>
            <td>
                <input type="text" name="txt_POFFICE" id="txt_POFFICE" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POFFICE" id="img_POFFICE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Nr'st Police St:
            </td>
            <td>
                <input type="text" name="txt_POLICE" id="txt_POLICE" class="textbox200" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POLICE" id="img_POLICE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4"
        style="display: none"></iframe>
</body>
</html>
