<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%

   Dim FileName ,UploadDate,UploadName ,UploadedBy ,TotalProperties 
   
   ' Passed from pRentIncreaseList.asp when user clicks to see details
   FileID = Request("FileID")
  
   OpenDB()
	    
	    SQL=  "SELECT		FILENAME, " &_
		"		UPLOADDATE, " &_
		"		RF.TITLE AS UPLOADNAME, " &_
		"		FIRSTNAME + ' ' + LASTNAME AS UPLOADEDBY, " &_
		"		PT_CNT.CNT AS TOTALPROPERTIES " &_
		"FROM P_RENTINCREASE_FILES RF " &_
		"	INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = RF.UPLOADEDBY " &_
		"	LEFT JOIN ( SELECT COUNT(1) AS CNT, FILEID " &_
		"				FROM	P_PRE_TARGETRENT_TBL PT " &_
		"				GROUP BY FILEID )PT_CNT ON PT_CNT.FILEID = RF.FILEID " &_
		" where RF.FILEID = " & FileID


		Call OpenRs(rsSet, SQL)
			 
		FileName = rsSet("FILENAME")
		UploadDate =  rsSet("UPLOADDATE")
		UploadName =  rsSet("UPLOADNAME")
		UploadedBy =  rsSet("UPLOADEDBY")
		TotalProperties =  rsSet("TOTALPROPERTIES")
			 
		 CloseRs(rsSet)


		sql  = "SELECT TN.TENANCYID, (ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'')) AS ADDRESS,(ISNULL(F.RENT,0) + ISNULL(F.SERVICES,0) + ISNULL(F.COUNCILTAX,0) + ISNULL(F.WATERRATES,0) + ISNULL(F.INELIGSERV,0)+ ISNULL(F.SUPPORTEDSERVICES,0)) as oldrent,  " &_
			"	(ISNULL(T.RENT,0) + ISNULL(T.SERVICES,0) + ISNULL(T.COUNCILTAX,0) + ISNULL(T.WATERRATES,0) + ISNULL(T.INELIGSERV,0)+ ISNULL(T.SUPPORTEDSERVICES,0)) as newrent	" &_
			"FROM P_PRE_TARGETRENT_TBL T " &_
			"	inner join c_tenancy tn on tn.propertyid = t.propertyid	and tn.enddate is null " &_
			"	INNER JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID " &_
			"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = F.PROPERTYID " &_
			" WHERE (ISNULL(F.RENT,0) + ISNULL(F.SERVICES,0) + ISNULL(F.COUNCILTAX,0) + ISNULL(F.WATERRATES,0) + ISNULL(F.INELIGSERV,0)+ ISNULL(F.SUPPORTEDSERVICES,0))> " &_
			"(ISNULL(T.RENT,0) + ISNULL(T.SERVICES,0) + ISNULL(T.COUNCILTAX,0) + ISNULL(T.WATERRATES,0) + ISNULL(T.INELIGSERV,0)+ ISNULL(T.SUPPORTEDSERVICES,0))  " &_
			" and FILEID = " & FILEID

		OverRentCnt = 0
		Call OpenRs(rsSet, SQL)

		PropertiesHTML = PropertiesHTML  & "<table width='100%'>"	
		PropertiesHTML = PropertiesHTML  & "<TR><TD><b>Tenancy Ref:</b></TD><TD><b>Address:</b></TD><TD><b>Rent:</b></TD><TD><b>New Rent:</B></TD></TR>"
			

		while not rsset.eof 
		OverRentCnt = OverRentCnt + 1
		
			OldRent = rsSet("oldrent")
			NewRent = rsSet("newrent")
			TenancyID = rsSet("TenancyID")
			Address = rsSet("Address")

			
			PropertiesHTML = PropertiesHTML  & "<TR><TD>" & TenancyID & "</TD><TD>" & Address & "</TD><TD>" & OldRent & "</TD><TD>" & NewRent & "</TD></TR>"	

		rsset.MoveNext()
		wend
		PropertiesHTML = PropertiesHTML  & "</table>"	
		CloseRs(rsSet)
   CloseDB()

%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSLManager_greenBrand.css" type="text/css">
<style  type="text/css" media="all">
    #main{width:100%;}
    /*CSS for filter*/
    #filter {background-color: #FFFFFF;width:25%;margin-left:5px;padding-top:10px;float:left;margin-right:3px;}
    .heading {border:1px #133e71 solid;width:160px;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    #filter_head{width:291px;}
    #filter_headline{width:131px;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .filter_headlinediv{width:131px;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    #filter_body{width:291px;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-bottom:18px;margin-bottom:10px;}
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;font-size:13px;}
    .linkstylesmall{text-decoration: underline;color:blue;cursor:hand;font-size:10px;padding:0 5 0 5;}
    .boxstyle{border:1px solid #133e71;width:12px;height:1px;margin:5 5 0 10;}
    .dottedline{border-bottom:1px dotted #133e71; padding:0 0 0 0; margin:0 9 0 9;}
    .line{border-bottom:1px solid #133e71; padding:0 0 0 0; margin:0 9 0 2;}
    .dottedbox{border:1px dotted #133e71;}
    .dottedbox img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .elementlabel{font-size:10px;margin-left:10px;font-weight:bold;color:#133e71}
    .elementlabeldisabled{font-size:10px;margin-left:10px;font-weight:bold;color:gray}
    .margintop{margin:10 0 0 0;}
    /*CSS for content*/
    .content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;float:left;}
    .content_head{width:99%;}
    .heading_content {border:1px #133e71 solid;width:24%;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    .content_headline{width:76%;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .content_headlinediv{width:100%;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    .content_body{width:99%;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-left:10px;float:left;height:100%;}
    .heading2{font-weight:bold;color:#133e71;}
    .heading3{font-weight:bold;color:#133e71;font-size:12px;}
    .fonttwelevewithcolor{font-size:12;color:#133e71;} 
</style>
</head>
<script type="text/javascript" language="javascript">

function ShowProperties(){

			document.getElementById("ViewProperties").style.display = "none"
			document.getElementById("HideProperties").style.display = "block"
			PropertiesRow.style.display = "Block"
			window.resizeTo(400,400);
			return true;

}
function HideProperties(){

			document.getElementById("ViewProperties").style.display = "block"
			document.getElementById("HideProperties").style.display = "none"
			PropertiesRow.style.display = "none"
			window.resizeTo(400,350);
			return true;

}
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=3 onLoad="window.focus()">
   <div id="maincontent_popup" style=" float:left;">
<table width=100% border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>

  <tr>
  	  <td height=40  valign=middle style=';border-bottom:1px solid #669900;padding-left:10px;'>

		<table>	
		<TR>
			<TD>	
				<b>Upload Report for the File Name :</b> <br>
				<b style="color:blue">'<%=FileName%>'<b>
			</TD>
			
		</TR>
		</table>	
	 </td>
	
  </tr>

  <tr>
  	  <td height=130  valign=top style='border-bottom:1px solid #669900;padding-left:10px;'>
	<table>
		<TR><TD height=5></TD></TR>
		<TR>
			<TD><b>Upload Name</b></TD>
			<TD><%=UploadName%></TD>
			<TD></TD>
			
		</TR>
		<TR><TD height=5></TD></TR>
		<TR>
			<TD><b>Upload Date</b></TD>
			<TD><%=UploadDate %></TD>
			<TD></TD>
		</TR>
		<TR><TD height=5></TD></TR>
 		<TR>
			<TD><b>Uploaded By</b></TD>
			<TD><%=UploadedBy %></TD>
			<TD></TD>
		</TR>
		<TR><TD height=5></TD></TR>
 		<TR>
			<TD><b>Total Properties</b></TD>
			<TD><%=TotalProperties%></TD>
			<TD></TD>
		</TR>
 		<TR>
			<TD><b>Current Rent Higher</b></TD>
			<TD><%=OverRentCnt %></TD>
			<TD></TD>
		</TR>
       
	</table>	  
	   
      </td>


  </tr>

  <tr id="PropertiesRow" name="PropertiesRow" style="display:none">
  	  <td valign=top style='border-bottom:1px solid #669900;padding-left:10px;'><%=PropertiesHTML%> </td>
  </tr>

  <tr> 

	  <td height=40  align=right valign=middle style='padding-left:10px;'>
 	<table>
		<TR>
			<TD><input name="ViewProperties" id="ViewProperties" type="button" class="RSLButton" style="display:block" onClick="ShowProperties()" value="View Properties"/ ></TD>
			<TD><input name="HideProperties" id="HideProperties" type="button" class="RSLButton" style="display:none" onClick="HideProperties()" value="Hide Properties"/ ></TD>
			<TD><input name="button" type="button" class="RSLButton" style="" onClick="window.close()" value="Close"/></TD>
		</TR>
       
	</table>
		

		
	&nbsp;
		</td>
	
  </tr>
</table>

</div>
 <iframe name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

