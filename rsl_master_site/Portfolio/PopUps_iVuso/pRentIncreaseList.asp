<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/ExcelToXML.asp" -->
<%
Dim sv_UserID 
sv_UserID = Session("UserID")
Dim GP_FullFileName
%>
<%
Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'Get the boundary
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    Response.Write "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br>"
    Response.Write "Please correct the form attributes and try again."
    Response.End
  end if
  'Check ADO Version
	set checkADOConn = Server.CreateObject("ADODB.Connection")
	adoVersion = CSng(checkADOConn.Version)
	set checkADOConn = Nothing
	if adoVersion < 2.5 then
    Response.Write "<b>You don't have ADO 2.5 installed on the server.</b><br>"
    Response.Write "The File Upload extension needs ADO 2.5 or greater to run properly.<br>"
    Response.Write "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br>"
    Response.End
	end if		
  'Check content length if needed
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      Response.Write "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'Get all data inside the boundaries
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'Members variable of objects are put in a dictionary object
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'Get an object name
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'Test if object is of file type
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'Get Filename, content-type and content of file
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'Add filename to dictionary object
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'Add content-type to dictionary object
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'Get content of object
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'Get content of object
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'Add content to dictionary object
    UploadControl.Add "Value" , Value	
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen	
    'Add dictionary object to main dictionary
    UploadRequest.Add name, UploadControl	
    'Loop to next object
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'Save all uploaded files
    if UploadRequest.Item(GP_curKey).Item("FileName") <> "" then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      if GP_valueLen = 0 then
        Response.Write "<B>An error has occured saving uploaded file!</B><br><br>"
        Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br>"
        Response.Write "File does not exists or is empty.<br>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
	  	  response.End
	    end if
      
      'Create a Stream instance
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")
      
      'Open the stream
      GP_strm1.Open
      GP_strm1.Type = 1 'Binary
      GP_strm2.Open
      GP_strm2.Type = 1 'Binary
        
      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen
    
      'Create and Write to a File
      GP_curPath = Request.ServerVariables("PATH_INFO")
      'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	  GP_curPath = UploadFormRequest("hid_FOLDERPATH")
	  GP_curPath = GP_curPath & "\"
	  
      GP_CurFileName = REPLACE(UploadRequest.Item(GP_curKey).Item("FileName"),"'","")
	  
	  SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	  Call OpenDB()
	  Call OpenRs(rsDEFAULTS, SQL)
		doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	  Call CloseRs(rsDEFAULTS)
	  Call CloseDB() 
      GP_FilePath = doc_DEFAULTPATH & "\Portfolio\rentIncreaseUploadFiles"
    
	 ' GP_FilePath = "C:\Inetpub\wwwroot\RslManager\BHA rslManager\NEWS\DOCUMENTMANAGER\DOCUMENTS"
	 ' GP_FilePath = "BHA rslManager\NEWS\DOCUMENTMANAGER\DOCUMENTS\"
	 ' GP_FilePath = "D:\inetpub\wwwroot\rmebsys\BHA rslManager\NEWS\DOCUMENTMANAGER\DOCUMENTS\"

	  GP_FullFileName =  GP_FilePath & GP_curPath & GP_CurFileName
      'rw "GP_FullFileName :: " & (GP_FullFileName) & "<br/>"
      'Check if the file already exist
      GP_FileExist = false
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = true
      End If    
	 
      if nameConflict = "error" and GP_FileExist then
        Response.Write "<b>File already exists!</b><br/><br/>"
        Response.Write "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
				GP_strm1.Close
				GP_strm2.Close
	  	  response.End
      end if
      if ((nameConflict = "over" or nameConflict = "uniq") and GP_FileExist) or (NOT GP_FileExist) then
        if nameConflict = "uniq" and GP_FileExist then
          Begin_Name_Num = 0
          while GP_FileExist    
            Begin_Name_Num = Begin_Name_Num + 1
            'GP_FullFileName = GP_FilePath & GP_curPath & "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
			'rw(GP_FullFileName) & "<br/>"
            GP_FullFileName = GP_FilePath & "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            'rw "GP_FullFileName::" & (GP_FullFileName) & "<br/>"
            'Response.End()
            GP_FileExist = fso.FileExists(GP_FullFileName)
            rw(GP_FileExist)
            'Response.End()
          wend  
          UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
					UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
        end if
        on error resume next
		
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
		  'RW GP_FullFileName & "<br><br>" 
          Response.Write "<b>An error has occured saving uploaded file!</b><br/><br/>"
          Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
          Response.Write "Maybe the destination directory does not exist, or you don't have write permission.<br/>"
          Response.Write "Please correct and <a href=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    end if
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			end if
        on error goto 0
      end if
    end if
  next

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString =""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1))) 
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  end if  
End Function

If (CStr(Request.QueryString("GP_upload")) <> "") Then

  GP_redirectPage = "pRentIncreaseList.asp"
  If (GP_redirectPage = "") Then
    GP_redirectPage = CStr(Request.ServerVariables("URL"))
  end if
  
  RequestBin = Request.BinaryRead(Request.TotalBytes)

  Dim UploadRequest
  Set UploadRequest = CreateObject("Scripting.Dictionary") 
  
  ' NEED TO CHANGE THIS BELOW TO GET THE CORRECT FILE PATH 
  BuildUploadRequest RequestBin, GP_curPath, "file", "", "uniq"
  
  '*** GP NO REDIRECT
end if  
if UploadQueryString <> "" then
  UploadQueryString = UploadQueryString & "&GP_upload=true"
else  
  UploadQueryString = "GP_upload=true"
end if
%>
<%
' *** Edit Operations: (Modified for File Upload) declare variables

MM_editAction = CStr(Request.ServerVariables("URL")) 'MM_editAction = CStr(Request("URL"))
If (UploadQueryString <> "") Then
  MM_editAction = MM_editAction & "?" & UploadQueryString
End If

' boolean to abort record edit
MM_abortEdit = false

' query string to execute
MM_editQuery = ""
%>
<%
' *** Insert Record: (Modified for File Upload) set variables

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  MM_editConnection = RSL_CONNECTION_STRING
  MM_editTable = "dbo.P_RENTINCREASE_FILES"
  MM_editRedirectUrl = ""
  MM_fieldsStr  = "txt_DocTitle|value|hid_UserID|value"
  MM_columnsStr = "TITLE|',none,''|UPLOADEDBY|',none,''"

  ' create the MM_fields and MM_columns arrays
  MM_fields = Split(MM_fieldsStr, "|")
  MM_columns = Split(MM_columnsStr, "|")

  ' set the form values
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    MM_fields(i+1) = CStr(UploadFormRequest(MM_fields(i)))
	'rw MM_fields(i) & "<BR>"
  Next

  ' append the query string to the redirect URL
  If (MM_editRedirectUrl <> "" And UploadQueryString <> "") Then
    If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0 And UploadQueryString <> "") Then
      MM_editRedirectUrl = MM_editRedirectUrl & "?" & UploadQueryString
    Else
      MM_editRedirectUrl = MM_editRedirectUrl & "&" & UploadQueryString
    End If
  End If

End If
%>
<%
' *** Insert Record: (Modified for File Upload) construct a sql insert statement and execute it

If (CStr(UploadFormRequest("MM_insert")) <> "") Then

  ' create the sql insert statement
  MM_tableValues = ""
  MM_dbValues = ""
  For i = LBound(MM_fields) To UBound(MM_fields) Step 2
    FormVal = MM_fields(i+1)
    MM_typeArray = Split(MM_columns(i+1),",")
    Delim = MM_typeArray(0)
    If (Delim = "none") Then Delim = ""
    AltVal = MM_typeArray(1)
    If (AltVal = "none") Then AltVal = ""
    EmptyVal = MM_typeArray(2)
    If (EmptyVal = "none") Then EmptyVal = ""
    If (FormVal = "") Then
      FormVal = EmptyVal
    Else
      If (AltVal <> "") Then
        FormVal = AltVal
      ElseIf (Delim = "'") Then  ' escape quotes
        FormVal = "'" & Replace(FormVal,"'","''") & "'"
      Else
        FormVal = Delim + FormVal + Delim
      End If
    End If
    If (i <> LBound(MM_fields)) Then
      MM_tableValues = MM_tableValues & ","
      MM_dbValues = MM_dbValues & ","
    End If
    MM_tableValues = MM_tableValues & MM_columns(i)
    MM_dbValues = MM_dbValues & FormVal
  Next

    ' this part adds an amend item  for the FILE NAME so that if the filename is changed it is shown in the DB
  If UploadFormRequest("txt_FilePath") <> "" Then
	  If AmendedFileName <> "" Then 
		AdditionalAdd_COL = ",FILENAME"
		AdditionalAdd_FIELD = ",'" & AmendedFileName & "'"
	  Else
		AdditionalAdd_COL = ",FILENAME"
		AdditionalAdd_FIELD = ",'" & UploadFormRequest("txt_FilePath") & "'"
	  End If
  End If

  MM_editQuery = "SET NOCOUNT ON;insert into " & MM_editTable & " (" & MM_tableValues & AdditionalAdd_COL & ",FilePassed) values (" & MM_dbValues & AdditionalAdd_FIELD & ",1); SELECT SCOPE_IDENTITY() AS FILEID;"
  'Response.Write(MM_editQuery)
  'Response.End()
  If (Not MM_abortEdit) Then
    ' execute the insert
	Call OpenDB()

    set MM_editCmd = Conn.Execute(MM_editQuery)
	NewFILEID = MM_editCmd.fields("FILEID").value
	'/************************Added to get XML from XLS file in order to inset into database***********************************/
    Dim XMLString
    Dim xmlnsstart, xmlnsend,xmlnssrting
    Dim xmlstart,xmlend,xmltopstring

    XMLString= GetXMLfromXLSFile(replace(GP_FullFileName,"/",""),"[Sheet1$]","*")
 
    '/***********************************************************/

    CheckFileSQL = "EXEC [P_RENTINCREASE_FILECHECK] " & NewFILEID &",'" & XMLString & "'"
	On Error Resume Next
	Conn.Execute(CheckFileSQL)
	On Error Resume Next

	SQL = "SELECT FILEPASSED FROM P_RENTINCREASE_FILES WHERE FILEID = " & NewFILEID
	Call OpenRs(RSFILECHECK,SQL)

	If RSFILECHECK("FILEPASSED") = 1 Then
		PASSMESSAGE = "true"
		SQL = " EXEC P_RENTINCREASE_STOREDATA " & NewFILEID
		Conn.Execute(SQL)
	Else
		PASSMESSAGE = "false"
	End If
	Call OpenRs(RSFILECHECK)

	Response.Redirect("pRentIncreaseList.asp?Up_Pass=" & PASSMESSAGE & "")
	Call CloseDB()

  End If

End If
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager --> Rent Increase </title>
    <link rel="stylesheet" type="text/css" href="/css/RSLManager_greenBrand.css" />
    <style type="text/css" media="all">
        #main
        {
            width: 100%;
        }
        /*CSS for filter*/
        #filter
        {
            background-color: #FFFFFF;
            width: 25%;
            margin-left: 5px;
            padding-top: 10px;
            float: left;
            margin-right: 3px;
        }
        .heading
        {
            border: 1px #133e71 solid;
            width: 160px;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        #filter_head
        {
            width: 291px;
        }
        #filter_headline
        {
            width: 131px;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .filter_headlinediv
        {
            width: 131px;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        #filter_body
        {
            width: 291px;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-bottom: 18px;
            margin-bottom: 10px;
        }
        .linkstyle
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 13px;
        }
        .linkstylesmall
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 10px;
            padding: 0 5 0 5;
        }
        .boxstyle
        {
            border: 1px solid #133e71;
            width: 12px;
            height: 1px;
            margin: 5 5 0 10;
        }
        .dottedline
        {
            border-bottom: 1px dotted #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 9;
        }
        .line
        {
            border-bottom: 1px solid #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 2;
        }
        .dottedbox
        {
            border: 1px dotted #133e71;
        }
        .dottedbox img
        {
            border: none;
            margin: 0 0 0 1;
            padding: 0 0 0 0;
        }
        .elementlabel
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: #133e71;
        }
        .elementlabeldisabled
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: gray;
        }
        .margintop
        {
            margin: 10 0 0 0;
        }
        /*CSS for content*/
        .content
        {
            border: #FFFFFF solid;
            border-width: 1px;
            height: 1px;
            padding-top: 10px;
            float: left;
        }
        .content_head
        {
            width: 99%;
        }
        .heading_content
        {
            border: 1px #133e71 solid;
            width: 24%;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        .content_headline
        {
            width: 76%;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .content_headlinediv
        {
            width: 100%;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        .content_body
        {
            width: 99%;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-left: 10px;
            float: left;
            height: 100%;
        }
        .heading2
        {
            font-weight: bold;
            color: #133e71;
        }
        .heading3
        {
            font-weight: bold;
            color: #133e71;
            font-size: 12px;
        }
        .fonttwelevewithcolor
        {
            font-size: 12;
            color: #133e71;
        }
    </style>
    <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="javascript" src="/js/general.js"></script>
    <script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

          
          <% if request("Up_Pass") = "false" then %>
          	alert("File failed : please check the file is set up correctly")
          <% elseIF request("Up_Pass") = "true" then %>
          	alert("The File has Uploaded and Rent Increase Letters are ready to print.")
          <% end if %>

        var FormFields = new Array()


        // This form submition is to bring back the list of previous uploaded files 
        function GetDetail() {
            // declare all the feilds that need to be part of the form valifation
            FormFields[0] = "txt_Title|Search - Title|TEXT|N"
            FormFields[1] = "txt_Date|Search - Date|DATE|N"
            FormFields[2] = "txt_UploadedBy|Search - By|TEXT|N"
            // check all feilds pass validation
            if (!checkForm()) return false;
            // if pass validation then submit the form
            document.RSLFORM_FILTER.target = "ServerFrame"
            document.RSLFORM_FILTER.action = "../ServerSide_Ivuso/UploadFilesList.asp"
            document.RSLFORM_FILTER.submit()
            return true;
        }

        // this form submition is to upload the file  
        function UploadFile() {
            // check if the user has eneterd a file to upload
            if (document.RSLFORM.txt_FilePath.value == "") {
                alert("Please select a file to upload.")
                return false
            }
            // lets get the last 3 characters which will be the file extension
            var ext = document.RSLFORM.txt_FilePath.value
            ext = ext.substring(ext.length - 3, ext.length);
            ext = ext.toLowerCase();
            // check if the file extenstion matches the accepted ones / one,
            // if not alert that its invalid and cancel actions
            if (!(ext == "xls"|| ext == "lsx")) {
                alert("Only .XLS files can be uploaded. Please upload the correct file format.")
                return false
            }
            // set validation feilds 
            FormFields[0] = "txt_DocTitle|Upload Title|TEXT|Y"
            // validate feilds
           // if (!checkForm()) return false;
            if (document.getElementById("txt_DocTitle").value == "") {
            alert("Upload Title may not be blank");
            return false
            }
            // submit forom if validation passes	 
            document.RSLFORM.target = ""
            document.RSLFORM.action = "<%=MM_editAction%>";
            document.RSLFORM.submit();
        }

        // When the user clicks to see the details this opens a window to desiplay a pop up
        function Open_window(FileID) {
            window.open("pRentIncreaseList_detail.asp?FileID=" + FileID, "display", "scrollbars=1,resizable=1,width=400,height=260");
        }


        // This function is called when user clicks the red X and deletes the file and the records stored
        function DeleteFile(FileID) {
            // make sure the user understand the consiquences of deleting the file
            if (!confirm("Please confirm that you would like to remove this file and any associated data that may have been recorded. The rent increase will not take place using any information from this file once deleted.")) {
                return false;
            }
            document.RSLFORM_FILTER.hid_FileID.value = FileID
            document.RSLFORM_FILTER.target = "ServerFrame"
            document.RSLFORM_FILTER.action = "../ServerSide_Ivuso/DeleteRentUploadFile.asp"
            document.RSLFORM_FILTER.submit()
        }
 
    </script>
</head>
<body onload="GetDetail()">
    <div id="maincontent" style="float: left;">
        <div id="topbar">
            <div id="topmenu">
                <div id="system_logo">
                </div>
                <div style="float: right; margin-right: 4px; margin-top: 10px;">
                    <a class="lnklogout" href="#">Help</a> <a class="lnklogout" href="#">Whiteboard</a>
                    <a class="lnklogout" href="#" onclick="window.close()">Log Out</a></div>
                <div id="PageHeading" style="clear: right; float: right; margin-right: 4px; margin-top: 30px;">
                    Rent Increase Control
                    </div><br />
            </div>
        </div>
        <div id="stripe">
        </div>
        <br />
        <div id="main" style="float: left; height: 410px;">
            <form name="RSLFORM_FILTER" method="post" action="<%=MM_editAction%>">
            <div id="filter">
                <div id="filter_head">
                    <div id="filter_heading" class="heading" style="float: left;">
                        Control</div>
                    <div id="filter_headline">
                        <div class="filter_headlinediv" style="border-bottom: 1px solid #133e71; float: left;
                            width: 100%;">
                        </div>
                    </div>
                </div>
                <div id="filter_body" style="float: left;">
                    <p style="margin-top: 15px;">
                    </p>
                    <p style="margin-bottom: 5px;">
                        <span class="elementlabel">Select:</span>
                        <select id="sel_reportlist" name="sel_reportlist" class="textbox200" style="width: 150"
                            onchange="window.location=document.RSLFORM_FILTER.sel_reportlist.options[document.RSLFORM_FILTER.sel_reportlist.selectedIndex].value">
                            <option value="">Please Select</option>
                            <option value="pRentIncreaseList.asp">Upload Rent Increase</option>
                            <option value="pRentIncreaseReport.asp">Rent Review Report</option>
                            <option value="">-------------------------</option>
                            <option value=""></option>
                            <option value="pRentIncreaseBuildLetter.asp">Rent Letter Admin</option>
                        </select>
                    </p>
                    <p class="dottedline" style="margin-bottom: 10px;">
                        &nbsp;</p>
                    <p style="font-size: 14px; font-weight: bold; margin-left: 10px; margin-top: 0px;
                        margin-bottom: 0px;">
                        Search</p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_Title" id="lbl_Title">
                            Title:</label>
                        <input type="text" value="" id="txt_Title" name="txt_Title" class="textbox200" style="margin-left: 15px;" />
                        <img src="/js/FVS.gif" name="img_Title" id="img_Title" width="15px" height="15px"
                            border="0" alt="" />
                    </p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_Date" id="lbl_Date">
                            Date:</label>
                        <input type="text" value="" id="txt_Date" name="txt_Date" class="textbox200" style="margin-left: 11px;
                            width: 120;" />
                        <img src="/js/FVS.gif" name="img_Date" id="img_Date" width="15px" height="15px" border="0"
                            alt="" />
                    </p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_UploadedBy" id="lbl_UploadedBy">
                            By:</label>
                        <input type="text" value="" id="txt_UploadedBy" name="txt_UploadedBy" class="textbox200"
                            style="margin-left: 23px;" />
                        <img src="/js/FVS.gif" name="img_UploadedBy" id="img_UploadedBy" width="15px" height="15px"
                            border="0" alt="" />
                    </p>
                    <p class="margintop">
                        <input name="button" type="button" class="RSLButton" style="margin-left: 203px;"
                            onclick="GetDetail();" value="Search" />
                    </p>
                    <p class="dottedline">
                        &nbsp;</p>
                </div>
            </div>
            <input type="hidden" name="hid_FileID" id="hid_FileID" value="" />
            </form>
            <form name="RSLFORM" method="post" action="<%=MM_editAction%>" enctype="multipart/form-data"
            style="float: left; width: 70%;">
            <div class="content" id="content" style="float: left; height: 100px; width: 100%">
                <div id="content_head" class="content_head">
                    <div id="content_heading" class="heading_content">
                        Upload Rent Increase</div>
                    <div id="content_headline" class="content_headline">
                        <div class="content_headlinediv" style="border-bottom: 1px solid #133e71; width: 101.3%">
                        </div>
                    </div>
                </div>
                <div id="content_body" class="content_body">
                    <p class="margintop">
                        <label class="elementlabel" for="txt_FilePath" id="lbl_FilePath">
                            Document:</label>
                        <input type="file" id="txt_FilePath" name="txt_FilePath" class="textbox200" style="margin-left: 21px;" />
                        <img src="/js/FVS.gif" name="img_FilePath" id="img_FilePath" width="15px" height="15px"
                            border="0" alt="" />
                    </p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_DocTitle" id="lbl_DocTitle">
                            Upload Title:</label>
                        <input type="text" value="" id="txt_DocTitle" name="txt_DocTitle" class="textbox200"
                            style="margin-left: 15px;" />
                        <img src="/js/FVS.gif" name="img_DocTitle" id="img_DocTitle" width="15px" height="15px"
                            border="0" alt="" />
                    </p>
                    <p class="margintop">
                        <label class="elementlabel">
                            <%
                            ' this is the time frame the bfiles can be uploaded
                            startDate = DateDiff("D", "1/1/" &  year(now()) , now())
                            endDate = DateDiff("D", "23/2/" &  year(now()) , now())
                            'Response.Write(startDate)
                            'Response.Write(endDate)
                            'If (startDate > 0 And enddate < 0) Then
                            %>
                            <input name="button" type="button" class="RSLButton" style="margin-left: 203px;"
                                onclick="UploadFile();" value="Upload" />
                            <% 'End If %>
                        </label>
                    </p>
                </div>
            </div>
            <div class="content" id="content2" style="float: left; margin-top: 5px;">
                <div id="content_head2" class="content_head" style="float: left; width: 100%; margin-top: 25px;">
                    <div id="content_heading2" class="heading_content" style="float: left;">
                        Upload List</div>
                    <div id="content_headline2" class="content_headline">
                        <div class="content_headlinediv" style="border-right: 1px solid #133e71;">
                        </div>
                    </div>
                </div>
                <div id="content_body2" class="content_body" style="float: left; border: 1px solid #133e71;
                    height: 200px; padding-right: 3px;">
                    <iframe name="ServerFrame" id="ServerFrame" style="display: none; float: left;" height="200px"
                        width="100%"></iframe>
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                </div>
            </div>
            <input type="hidden" id="hid_UserID" name="hid_UserID" value="<%=sv_UserID%>" />
            <input type="hidden" name="MM_insert" id="MM_insert" value="true" /></form>
        </div>
    </div>
    <br />
    <div id="strapline" style="float: left;">
        RSLmanager is a <a href="http://www.reidmark.com" target="_blank">Reidmark</a> ebusiness
        system � All rights reserved 2007</div>
</body>
</html>
