<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="/CUSTOMER/iFrames/iHB.asp" -->
<%

	'// DECLARE VARIABLES THAT WILL BE SET IN FUNCTIONS BUT USED OUTSIDE OF THEM
		Dim SATISFACTION_LETTER_HTML
		Dim insert_list  
		Dim sel_split
		Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color
		Dim ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor,LetterBody, is_a_DD_Tennant
		Dim PrintAction
		Dim targetrentid
		dim JournalTitle
	
	'// set all the dates and format them
		JournalTitle=mid(year(now),3) & " / " &  mid(year(now)+1,3) & " Increase"
		CurrentDate = CDate(NOW) 
		TheCurrentMonth = DatePart("m", CurrentDate)
		TheCurrentYear = DatePart("yyyy", CurrentDate)
		TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
		
		PreviousDate = DateAdd("m", -1, CurrentDate)
		ThePreviousMonth = DatePart("m", PreviousDate)
		ThePreviousYear = DatePart("yyyy", PreviousDate)
		ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
		TheNextYearDate = TheCurrentYear + 1
		
	'//	 call the main function below
		OpenDB()
		Display_Letter()
		CloseDB()
	 	
	Function Display_Letter()
YEARNUMBER = TheCurrentYear 
TheNextYear=Mid(TheNextYearDate, 3)
				'========================================================================================
				' Build Address detail
				'========================================================================================
	
					' now combine the name and address to give a post label
					FullAddress = "<TR><TD nowrap style='width:75%;font:12PT ARIAL;padding-left:20mm;'>Mr Smith</TD><td nowrap style='width:20%;padding-right:25mm; text-align:right; '><span style='width:150px;font:12PT ARIAL'>Your Tenancy Ref: <b>" & TenancyReference(tenancyref) & "</b></span></td></TR>" &_
								"<TR><TD nowrap style='font:12PT ARIAL;padding-left:20mm;'>1 Test Road<BR>Norwich</TD><td style='text-align:right;padding-right:25mm;font:12PT ARIAL'>Date: 20 February " & YEARNUMBER & "</td></TR>" &_
								RemainingAddressString 
				'========================================================================================
				'	End building name and address info
				'========================================================================================

			
					DoPageBreak2 = "style='page-break-after: always'"
					
					LetterBody = LetterBody &  "<p " & DoPageBreak &  "><table width=100% border=0 style='height:21cm'>"
					LetterBody = LetterBody &  "<TBODY ID='LETTERDIV'><tr><td align=right valign=top style='font:12PT ARIAL' height='99'><TABLE border=0 id='return_address' width='100%'    style='display:block' >&nbsp;<tr><td style='font:14PT ARIAL; width:80%;'>Your Rent Reassessment </td><td align=right valign=top style='font:12PT ARIAL;width:100%;' height='99'>&nbsp; <img src='/myImages/LOGOLETTER.gif' style='text-align:right; width:138px; height:117px; ' /></td></tr></TABLE></td>"
					LetterBody = LetterBody &  "</tr><tr><td nowrap valign=top><table width='100%'><tr><td nowrap style='text-align:right; '><span style='width:150px;font:12PT ARIAL'><b></b></span></td></tr><tr><td style='text-align:right;font:12PT ARIAL'></td></tr>" & FullAddress 
					LetterBody = LetterBody &  "</table></td></tr><tr><td>&nbsp;</td></tr><tr><td> </td></tr>"
					' Dear Line
					LetterBody = LetterBody &"<tr><td style='font:12PT ARIAL' ><div style='width:60%; border-right:2px solid black;float:left'><table width='100%'> "
					LetterBody = LetterBody &  "<tr><td style='font:12PT ARIAL' >Dear Mr Smith</td></tr>"
					'explanation text
					LetterBody = LetterBody & "<tr style='height:10px'><td > </td></tr><tr id='LetterContent1' style='display:block'><td style='font:12PT ARIAL' valign='top' class='RSLBlack><div id=letterText>"
					LetterBody = LetterBody & "<span style='font:12PT ARIAL'>"
					
					
'// Increase Date -- needs to come from database

					IncreaseDate_HTML = YEARNUMBER
					
'// RENT FIGURES TABLE - PLACE WHERE USER HAS ENTERED [R]


'// RENT FIGURES TABLE - PLACE WHERE USER HAS ENTERED [R]
					RentAccountInfo_HTML = ""


					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<table border='0' cellspacing='0' cellpadding='0'>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td style='width:345'></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='18' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top'><b>Existing Rent</b></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='13' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='110' valign='top' align='right'><b>New Rent wef 01/04/200" &  YEARNUMBER & "</b></td>"					
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "</table>"

					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table style='width:90%'>" 
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td style='width:100%' colspan='5'>"
	
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"					
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top' style='font:12PT ARIAL; text-align:center; border:1px solid black;'><b>Existing</b></td>"
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top' style='font:12PT ARIAL;text-align:center;border:1px solid black;'><b>New Rent </td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "</tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Rent</span></td> <td align='Right' style='font:12PT ARIAL;text-align:Right;'>" &FormatCurrency(0,2)& "</td> <td align='Right' style='text-align:Right;font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td><span style='font:12PT ARIAL'>Support Services </span></td><td align='Right' style='font:12PT ARIAL;text-align:Right;'>" & FormatCurrency(0,2)& "</td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & " <tr><td><span style='font:12PT ARIAL'>Ineligible Services </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Services Ineligible for HB</span></td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td> <td align='Right' style='text-align:Right;font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "  <tr><td><span style='font:12PT ARIAL'>Other Services </span></td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Council Tax </span></td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td><td align='Right' style='text-align:Right;font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Water Rates </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td><td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Garage/Car Space </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(0,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>&nbsp;</span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td style='padding: 2px;border: 1px solid black;border-right: none;'><span style='font:12PT ARIAL; '>Total Rent Payable</span></td><td align='Right' style='padding: 2px;font:12PT ARIAL;border: 1px solid black;border-right: none;border-left: none;'>" & FormatCurrency(0,2) & "</td><td align='Right' style='padding: 2px;font:12PT ARIAL ;border: 1px solid black;border-left: none;'><b>" & FormatCurrency(0,2) & "</b></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         "</table>"
					
				
										
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & 	"</td></tr></table>"


' PAGE BREAK			
		
					PageBreak_HTML = PageBreak_HTML &	"<p style='page-break-after: always'></p>"
					
	
' DD ACCOUNT HTML

					' SHOW DD PAY AGREEMENT IF TENANT PAYS THAT WAY
					DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
						
                    DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br/>If you pay your rent via Direct Debit, we will make any necessary adjustments and you will be notified separately. " 
				
					'/////////////////
					
					
					sql = "select * from P_RENTINCREASE_LETTERTEXT"
					' this bit puts in the letter
					Call OpenRS(rsLetterBody, SQL)
					MainBodyFromDB ="<div style='width:100%; float:left; font:12PT ARIAL !important;'>" & rsLetterBody("LETTERTEXT") &"</div>"
					Call CloseRS(rsLetterBody)
					
					'REPLACE LETTER BUILDER BOLD TAGS AND REPLACE WITH HTML BOLD TAGS
					MainBodyFromDB = replace(MainBodyFromDB,"[B]","<B>")
					MainBodyFromDB = replace(MainBodyFromDB,"[/B]","</B>")

					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[D]",IncreaseDate_HTML)
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[N]",TheNextYear)
					
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[R]",RentAccountInfo_HTML)

					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[DD]",DDAccountInfo_HTML)
					
					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[PB]",PageBreak_HTML)
					
					LetterBody = LetterBody & MainBodyFromDB
					
					
					'////////////////					

					LetterBody = LetterBody &	 " </td></tr><tr><td> </td></tr><tr style='height:10px'><td style='font:12PT ARIAL'>&nbsp;</td></tr> "
					LetterBody = LetterBody &	 " <tr><td style='font:12PT ARIAL'><span font:12PT ARIAL>Yours sincerely</span></td></tr> "
					LetterBody = LetterBody &	 "<tr> "
					LetterBody = LetterBody &	 "  <td align=left id='footer' style='visiblity:hidden'><img src='../Images/Ivan_Johnson.gif' width='113' height='85'></td> "
					LetterBody = LetterBody &	 "  </tr> "
					LetterBody = LetterBody &	 "<tr style='height:20px' valign='top'> "
					LetterBody = LetterBody &	 "  <td style='font:12PT ARIAL'><span font:12PT ARIAL>Ivan Johnson<BR>Executive Housing Director</span></td> "
					LetterBody = LetterBody &	 " </tr> "					
					LetterBody = LetterBody &	 "<tr> "
					LetterBody = LetterBody &	 "    <td align=right id='footer' style='visiblity:hidden'>&nbsp; </td> "
					LetterBody = LetterBody &	   "</table></div><div style='width:37%;font:12PT ARIAL !important; vertical-align:top;float:left'> <div style='width:100%;'> <div style='width:100%; float: left;'> <div style='margin: 2px;float: left;color: white;background-image: url(/myimages/arrow-iconGrey.png);width: 110px;height: 22px;padding-top: 10px;padding-left: 14px;font:12PX ARIAL;'>Total Current Rent </div> <div style='margin: 2px; float: left; padding: 7px;'><span style='border:2px solid black; font-weight:bold; padding: 5px; font:12PT ARIAL; '>" & FormatCurrency(0,2) & "</span></div> </div> <div style='width:100%; float: left;'> <div style='margin: 2px; float: left;color: white;background-image: url(/myimages/arrow-iconGreen.png);width: 110px;height: 22px;padding-top: 10px;padding-left: 14px;font:12PX ARIAL;'>New Total Rent </div> <div style='margin: 2px; float: left; padding: 7px;'><span style='border:2px solid black; font-weight:bold; padding: 5px; font:12PT ARIAL; '>" & FormatCurrency(0,2) & "</span></div> </div> <div style='width:100%; float: left; text-align: center;margin-top:3px; margin-left: 5px;'><span style='font:14PT ARIAL; '>How to get in touch:</span></div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconlogin.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Login to Tenants Online<br/></span><span style='font:12PT ARIAL; '>www.broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconTel.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Call us<br />0303 303 0003</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconMail.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Email us<br />rent@broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconPC.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Visit us online <br />www.broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconFB.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Find us on Facebook <br />@Broadland</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconTwt.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Find us on Twitter <br />@BroadlandHsg</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconEdit.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Write to us at<br />Broadland Housing Group<br />NCFC<br />Carrow Road<br />Norwich<br />NR1 1HU</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px; line-height:20px; font:12PT ARIAL;'><span style='font:12PT ARIAL; font-weight:bold;'> Ways to pay us</span><br /><br /><b>Login to Tenants Online:</b><br /><span>www.broadlandgroup.org</span><br /><b>Direct Debit:</b> Call 0303 303 0003<br /><b>Standing Order:</b><br/> Sort code: 60-15-31<br />Account: 21481229<br />Quote Ref: <b>123456</b><br />For alternative payment methods <br />please contact us.<br/><br/><br/> </div> </div><div style='width:98%;float: left;margin-left:-5px; height:45mm;background: white;'>&nbsp;</div> <div id='newRent' style='width:98%;float: left; background-color:#eaf2ff;padding:5px; margin: 10px; text-align:left; font:12PT ARIAL;'>Your new total rent shown is payable from 01/04/2018. If you have any problems paying your rent please contact your Income Officer, <b>Joe Bloggs</b> by calling <b>01603 750000</b> or email <b>joe.bloggs@broadlandgroup.org </b></div><div id='bottom' style='width:99%;margin-top:550px; float: left; margin-right: 10px; text-align:right; font:12px ARIAL;'>Part of the Broadland Housing Group,<br />incorporating Broadland Housing<br />Association Limited, Broadland Merdian,<br />Broadland St Benedicts Limited and<br />Broadland Development Services.<br /><br />Broadland Housing Association Limited:<br />Registered address NCFC, Carrow Road,<br />Norwich, NR1 1HU. Registered in England<br />and Wales as a Registered Society under<br />the Co-operative and Community Benefit<br />Societies Act 2014 as a non profit making<br />housing association with charitable status.<br />Registered Society No. 16274R. HCA<br />Registration No. L0026<br /></div> <div style='width:100%; float: left; margin-left: 10px; text-align:right; font:10PT ARIAL;'><img src='/myimages/CapturePrint.JPG' width='200px' height='40px'</div></div> "
					LetterBody = LetterBody &	 "</td></tr></table></p>"
									

	End Function
		
%>
<html>
<head>  <meta http-equiv="X-UA-Compatible" content="IE=10" /></head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/print.js"></SCRIPT>

<script language="Javascript">

	function PrintMe(){
		//document.getElementById("PrintLetter").style.display = "none"
		  document.getElementById("PrintLetter").setAttribute("style", "display:none");
		  //  document.getElementById("bottom").setAttribute("style", "marginTop:675px");
		
		 var dvTarget = document.getElementById("printContent");
            var printWindow;
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if (isChrome) {
			//document.getElementById('bottom').style.marginTop = '440px';
                printWindow = window.open('chrome://test/content/scanWindow.xul', 'Scan', '**chrome,width=850,height=150,centerscreen**');
            } else {
                printWindow = window.open('', '', 'left=0,top=0,toolbar=0,sta­tus=0,scrollbars=1');
            }

            printWindow.document.write(dvTarget.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
		}
		
</script>
<style type="text/css">
.thisHere{
	position:relative;
	top:-10px;
	-webkit-print-color-adjust:exact;
}
@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 5mm 5mm 5mm 5mm;  
}
br{line-height:1px;} 
</style>

<body class="thisHere">
<form name="RSLFORM" method="get"><input name="PrintLetter" id="PrintLetter" style="display:block" value="Print Letter" type="button" onClick="PrintMe()"></form>
<DIV id="printContent">
<%=LetterBody%>
</DIV>
</body>
</html>
