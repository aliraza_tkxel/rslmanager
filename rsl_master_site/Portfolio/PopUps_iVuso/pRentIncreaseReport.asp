<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<%
Dim sv_UserID 
sv_UserID = Session("UserID")

OpenDB()
Call BuildSelect(lst_patch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All Patches", "", "width:150px;margin-left:17px;", "textbox200", "onChange='SchemesByPatch()'")

Call BuildSelect(lst_scheme, "sel_DEVELOPMENT", "P_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "All Schemes", "", "width:150px;margin-left:5px;", "textbox200", NULL)

Call BuildSelect(lst_status, "sel_STATUS", "P_STATUS", "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", "", "width:150px;margin-left:14px;", "textbox200", NULL)

Call BuildSelect(lst_ASAT, "sel_ASAT", "F_FISCALYEARS WHERE YRANGE > 9", "YSTART, YSTART", "YSTART", "Please Select", "", "width:150px;margin-left:19px;", "textbox200", NULL)




CloseDB()	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager --> Rent Increase </title>
    <link rel="stylesheet" type="text/css" href="/css/RSLManager_greenBrand.css" />
    <style type="text/css" media="all">
        #main
        {
            width: 100%;
        }
        /*CSS for filter*/
        #filter
        {
            background-color: #FFFFFF;
            width: 25%;
            margin-left: 5px;
            padding-top: 10px;
            float: left;
            margin-right: 3px;
        }
        .heading
        {
            border: 1px #133e71 solid;
            width: 160px;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        #filter_head
        {
            width: 291px;
        }
        #filter_headline
        {
            width: 131px;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .filter_headlinediv
        {
            width: 131px;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        #filter_body
        {
            width: 291px;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-bottom: 18px;
            margin-bottom: 10px;
        }
        .linkstyle
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 13px;
        }
        .linkstylesmall
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 10px;
            padding: 0 5 0 5;
        }
        .boxstyle
        {
            border: 1px solid #133e71;
            width: 12px;
            height: 1px;
            margin: 5 5 0 10;
        }
        .dottedline
        {
            border-bottom: 1px dotted #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 9;
        }
        .line
        {
            border-bottom: 1px solid #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 2;
        }
        .dottedbox
        {
            border: 1px dotted #133e71;
        }
        .dottedbox img
        {
            border: none;
            margin: 0 0 0 1;
            padding: 0 0 0 0;
        }
        .elementlabel
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: #133e71;
        }
        .elementlabeldisabled
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: gray;
        }
        .margintop
        {
            margin: 10 0 0 0;
        }
        /*CSS for content*/
        .content
        {
            border: #FFFFFF solid;
            border-width: 1px;
            height: 1px;
            padding-top: 10px;
            float: left;
        }
        .content_head
        {
            width: 99%;
        }
        .heading_content
        {
            border: 1px #133e71 solid;
            width: 24%;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        .content_headline
        {
            width: 76%;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .content_headlinediv
        {
            width: 100%;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        .content_body
        {
            width: 99%;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-left: 10px;
            float: left;
            height: 100%;
        }
        .heading2
        {
            font-weight: bold;
            color: #133e71;
        }
        .heading3
        {
            font-weight: bold;
            color: #133e71;
            font-size: 12px;
        }
        .fonttwelevewithcolor
        {
            font-size: 12;
            color: #133e71;
        }
    </style>
</head>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">

    var FormFields = new Array()


    // This form submition is to bring back the list of previous uploaded files

    function GetDetail() {

        // declare all the feilds that need to be part of the form valifation
        FormFields[0] = "sel_PATCH|Search - Patch|INT|N"
        FormFields[1] = "sel_DEVELOPMENT|Search - Scheme|INT|N"
        FormFields[2] = "txt_NAME|Search - Name|TEXT|N"
        FormFields[3] = "sel_STATUS|Search - Status|INT|N"
        FormFields[4] = "sel_ASAT|Search - As At|INT|Y"

        // check all feilds pass validation
        if (!checkForm()) return false;

        // if pass validation then submit the form
        RSLFORM_FILTER.target = "ServerFrame"
        RSLFORM_FILTER.action = "../ServerSide_Ivuso/RentIncreaseReport.asp"
        RSLFORM_FILTER.submit()
        return true;
    }


    function SchemesByPatch() {

        RSLFORM_FILTER.target = "ServerFrame"
        RSLFORM_FILTER.action = "../ServerSide_Ivuso/SchemesByPatch.asp"
        RSLFORM_FILTER.submit()
        return true;
    }
</script>
<body onload="GetDetail()">
    <div id="maincontent" style="float: left; width:100%;">
        <div id="topbar">
            <div id="topmenu">
                <div id="system_logo">
                </div>
                <div style="float: right; margin-right: 4px; margin-top: 10px;">
                    <a class="lnklogout" href="#">Help</a> <a class="lnklogout" href="#">Whiteboard</a>
                    <a class="lnklogout" href="#" onclick="window.close()">Log Out</a></div>
                <div id="PageHeading" style="clear: right; float: right; margin-right: 4px; margin-top: 30px;">
                    Rent Increase Control</div>
            </div>
        </div>
        <div id="stripe">
        </div>
        <br />
        <div id="main">
            <form name="RSLFORM_FILTER" method="post">
            <div id="filter">
                <div id="filter_head">
                    <div id="filter_heading" class="heading" style="float: left;">
                        Control</div>
                    <div id="filter_headline">
                        <div class="filter_headlinediv" style="border-bottom: 1px solid #133e71; float: left;
                            width: 100%;">
                        </div>
                    </div>
                </div>
                <div id="filter_body" style="float: left;">
                    <p style="margin-top: 15px;">
                    </p>
                    <p style="margin-bottom: 5px;">
                        <span class="elementlabel">Select:</span>
                        <select id="sel_reportlist" name="sel_reportlist" class="textbox200" style="width: 150"
                            onchange="window.location=document.RSLFORM_FILTER.sel_reportlist.options[document.RSLFORM_FILTER.sel_reportlist.selectedIndex].value">
                            <option value="">Please Select</option>
                            <option value="pRentIncreaseList.asp">Upload Rent Increase</option>
                            <option value="pRentIncreaseReport.asp">Rent Review Report</option>
                            <option value="">-------------------------</option>
                            <option value=""></option>
                            <option value="pRentIncreaseBuildLetter.asp">Rent Letter Admin</option>
                        </select>
                    </p>
                    <p class="dottedline" style="margin-bottom: 10px;">
                        &nbsp;</p>
                    <p style="font-size: 14px; font-weight: bold; margin-left: 10px; margin-top: 0px;
                        margin-bottom: 0px;">
                        Search</p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_PATCH" id="lbl_Patch">
                            Patch:</label>
                        <%=lst_patch%>
                        <image src="/js/FVS.gif" name="img_PATCH" width="15px" height="15px" border="0">
                    </p>
                    <div id="div_scheme">
                        <p class="margintop">
                            <label class="elementlabel" for="txt_DEVELOPMENT" id="lbl_Scheme">
                                Scheme:</label>
                            <%=lst_scheme%>
                            <image src="/js/FVS.gif" name="img_DEVELOPMENT" width="15px" height="15px" border="0">
                        </p>
                    </div>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_NAME" id="lbl_Name">
                            Name:</label>
                        <input type="text" value="" id="txt_NAME" name="txt_NAME" class="textbox200" style="margin-left: 17px;" />
                        <image src="/js/FVS.gif" name="img_NAME" width="15px" height="15px" border="0">
                    </p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_STATUS" id="lbl_Status">
                            Status:</label>
                        <%=lst_sTATUS%>
                        <image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0">
                    </p>
                    <p class="margintop">
                        <label class="elementlabel" for="txt_ASAT" id="lbl_Asat">
                            As At:</label>
                        <%=lst_ASAT%>
                        <image src="/js/FVS.gif" name="img_ASAT" width="15px" height="15px" border="0">
                    </p>
                    <p class="margintop">
                        <input name="button" type="button" class="RSLButton" style="margin-left: 203px;"
                            onclick="GetDetail();" value="Search" />
                    </p>
                    <p class="dottedline">
                        &nbsp;</p>
                </div>
            </div>
            <input type="hidden" name="hid_FileID" value="">
            </form>
            <form name="RSLFORM" method="post" action="<%=MM_editAction%>">
            <div class="content" id="content2" style="float: left; width: 70%">
              <div id="content_head" class="content_head">
                    <div id="content_heading" class="heading_content">
                        Rent Review Report</div>
                    
                </div>
                    <div id="content_body2" class="content_body" style="float: left; border: 1px solid #133e71;
                    height: 300px; padding-right: 3px;">
                    <iframe name="ServerFrame" style='display: none'></iframe>
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                </div>
            </div>
        </div>
        <input type="hidden" id="hid_UserID" name="hid_UserID" value="<%=sv_UserID%>" />
        <input type="hidden" name="MM_insert" value="true">
    </div>
    <br />
    <div id="strapline" style="float: left;">
        RSLmanager is a <a href="http://www.reidmark.com" target="_blank">Reidmark</a> ebusiness
        system � All rights reserved 2007</div>
    </form>
</body>
</html>
