<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<%
' LETS GET THE EXISTING RENT LETTER STORED
Dim sv_UserID 
sv_UserID = Session("UserID")

	OpenDB()
	
	' Get the letter text to fill the text box
	SQL = "select LETTERTEXT from P_RENTINCREASE_LETTERTEXT "
		
	call OpenRs(RSLETTER,SQL)
	
	' get the text with html tags in
	LETTERTEXT = RSLETTER("LETTERTEXT")
	' replace the tags and replace to provide breaks
	LETTERTEXT = Replace(LETTERTEXT, "<BR>", Chr(13))

	call closeRs(RSLETTER)
	

	CloseDB()


%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager --> Rent Increase </title>
    <link rel="stylesheet" type="text/css" href="/css/RSLManager_greenBrand.css" />
    <style type="text/css" media="all">
        #main
        {
            width: 100%;
        }
        /*CSS for filter*/
        #filter
        {
            background-color: #FFFFFF;
            width: 25%;
            margin-left: 5px;
            padding-top: 10px;
            float: left;
            margin-right: 3px;
        }
        .heading
        {
            border: 1px #133e71 solid;
            width: 160px;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        #filter_head
        {
            width: 291px;
        }
        #filter_headline
        {
            width: 131px;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .filter_headlinediv
        {
            width: 131px;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        #filter_body
        {
            width: 291px;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-bottom: 18px;
            margin-bottom: 10px;
        }
        .linkstyle
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 13px;
        }
        .linkstylesmall
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 10px;
            padding: 0 5 0 5;
        }
        .boxstyle
        {
            border: 1px solid #133e71;
            width: 12px;
            height: 1px;
            margin: 5 5 0 10;
        }
        .dottedline
        {
            border-bottom: 1px dotted #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 9;
        }
        .line
        {
            border-bottom: 1px solid #133e71;
            padding: 0 0 0 0;
            margin: 0 9 0 2;
        }
        .dottedbox
        {
            border: 1px dotted #133e71;
        }
        .dottedbox img
        {
            border: none;
            margin: 0 0 0 1;
            padding: 0 0 0 0;
        }
        .elementlabel
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: #133e71;
        }
        .elementlabeldisabled
        {
            font-size: 10px;
            margin-left: 10px;
            font-weight: bold;
            color: gray;
        }
        .margintop
        {
            margin: 10 0 0 0;
        }
        /*CSS for content*/
        .content
        {
            border: #FFFFFF solid;
            border-width: 1px;
            height: 1px;
            padding-top: 10px;
            float: left;
        }
        .content_head
        {
            width: 99%;
        }
        .heading_content
        {
            border: 1px #133e71 solid;
            width: 24%;
            height: 28px;
            margin: 0 0 0 0;
            padding: 5 0 0 6;
            display: inline;
            font-size: 12px;
            font-weight: bold;
            color: #133e71;
        }
        .content_headline
        {
            width: 76%;
            height: 20px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            display: inline;
        }
        .content_headlinediv
        {
            width: 100%;
            height: 1px;
            margin: 0 0 0 0;
            padding: 0 0 0 0;
        }
        .content_body
        {
            width: 99%;
            border: solid #133e71;
            border-width: 0px 1px 1px 1px;
            padding-left: 10px;
            float: left;
            height: 100%;
        }
        .heading2
        {
            font-weight: bold;
            color: #133e71;
        }
        .heading3
        {
            font-weight: bold;
            color: #133e71;
            font-size: 12px;
        }
        .fonttwelevewithcolor
        {
            font-size: 12;
            color: #133e71;
        }
    </style>
</head>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">


    // when letter is saved calls this function
    function BuildLetter() {

        // we dont store old letters so we alert the user of this.
        if (!confirm("ALERT! The changes you have made will replace the current Rent Increase letter and will be stored and used in the Annual Rent Increase process from this point. Are you sure you want to save changes.")) {
            return false;
        }

        // if the user okays this we save the letter 
        RSLFORM.target = "ServerFrame"
        RSLFORM.action = "../ServerSide_Ivuso/BuldLetter_Save.asp"
        RSLFORM.submit()
        return true;
    }

    // the preview will only show the latests saved letter
    function Preview_Letter() {
        window.open("RentLetter_Preview.asp", "", "width=" + 700 + ",height=" + 500 + ",scrollbars=yes,left=100,top=200");
    }
  
</script>
<body>
    <form name="RSLFORM" method="post" action="<%=MM_editAction%>">
    <div id="maincontent" style="float: left; width:100%;">
        <div id="topbar">
            <div id="topmenu">
                <div id="system_logo">
                </div>
                <div style="float: right; margin-right: 4px; margin-top: 10px;">
                    <a class="lnklogout" href="#">Help</a> <a class="lnklogout" href="#">Whiteboard</a>
                    <a class="lnklogout" href="#" onclick="window.close()">Log Out</a></div>
                <div id="PageHeading" style="clear: right; float: right; margin-right: 4px; margin-top: 30px;">
                    Rent Increase Control</div>
            </div>
        </div>
        <div id="stripe">
        </div>
        <br />
        <div id="main">
            <div id="filter">
                <div id="filter_head">
                    <div id="filter_heading" class="heading" style="float: left;">
                        Control</div>
                    <div id="filter_headline">
                        <div class="filter_headlinediv" style="border-bottom: 1px solid #133e71; float: left;
                            width: 100%;">
                        </div>
                    </div>
                </div>
                <div id="filter_body" style="float: left;">
                    <p style="margin-top: 15px;">
                    </p>
                    <p style="margin-bottom: 5px;">
                        <span class="elementlabel">Select:</span>
                        <select id="sel_reportlist" name="sel_reportlist" class="textbox200" style="width: 150"
                            onchange="window.location=document.RSLFORM.sel_reportlist.options[document.RSLFORM.sel_reportlist.selectedIndex].value">
                            <option value="">Please Select</option>
                            <option value="pRentIncreaseList.asp">Upload Rent Increase</option>
                            <option value="pRentIncreaseReport.asp">Rent Review Report</option>
                            <option value="">-------------------------</option>
                            <option value=""></option>
                            <option value="pRentIncreaseBuildLetter.asp">Rent Letter Admin</option>
                        </select>
                    </p>
                    <p class="dottedline" style="margin-bottom: 10px;">
                        &nbsp;</p>
                    <p style="font-size: 14px; font-weight: bold; margin-left: 10px; margin-top: 0px;
                        margin-bottom: 0px;">
                        Editor</p>
                    <p class="margintop" style="margin-left: 10px;">
                        <label class="elementlabel" for="txt_Title" id="lbl_Title">
                            <table>
                                <tr>
                                    <td valign="top" width="25%">
                                        Bold Text
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[B]</b> before and <b>[/B]</b> after the text you would like to apear in
                                        bold.
                                        <br>
                                        Eg. [B] Title [/B] (to display as <b>Title</b>)
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="25%">
                                        Date From
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[D]</b> where you would like the date the increase will be applicable to
                                        appear.<br>
                                        <br>
                                    </td>
                                </tr>
								<tr>
                                    <td valign="top" width="25%">
                                        Date To
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[N]</b> where you would like the fiscal end date (e.g. 2018/19 ) the increase will be applicable to
                                        appear.<br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="25%">
                                        Rent A/C
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[R]</b> where you would like the rent account table to appear. Click Here
                                        to view an example.
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="25%">
                                        D.Debit
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[DD]</b> where you would like the Direct Debit account table to appear.
                                        Click Here to view an example.
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="25%">
                                        Page Break
                                    </td>
                                    <td valign="top" width="75%">
                                        Type <b>[PB]</b> where you would like text to run onto a new page when printing.
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                        </label>
                    </p>
                    <p class="dottedline">
                        &nbsp;</p>
                </div>
            </div>
            <div class="content" id="content" style="float: left; width: 70%">
                <div id="content_head" class="content_head">
                    <div id="content_heading" class="heading_content">
                        Rent Letter Admin:</div>
                    <div id="content_headline" class="content_headline">
                        <div class="content_headlinediv" style="border-bottom: 1px solid #133e71; width: 101.3%">
                        </div>
                       
                    </div>
                </div>
                <div id="content_body" class="content_body" style=" height:389px;">
                    <p class="margintop">
                        <textarea name="txt_RentLetter" id="txt_RentLetter" tabindex="1" style="width: 95%;
                            height: 300"><%=LETTERTEXT%></textarea>
                    </p>
                    <p class="margintop">
                        <input name="" type="button" value="Preview Saved Letter" class="RSLButton" onclick="Preview_Letter()">
                        <input name="" type="button" value="Save Letter" class="RSLButton" onclick="BuildLetter()">
                        <br>
                        <br>
                        <div id="MessageDiv" name="MessageDiv">
                        <iframe name="ServerFrame" style='display: none'></iframe>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div id="strapline" style=" float:left;">
        RSLmanager is a <a href="http://www.reidmark.com" target="_blank">Reidmark</a> ebusiness
        system � All rights reserved 2007</div>
    </form>
    
</body>
</html>