<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/Calendar.asp" -->
<%

	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
	
	OpenDB()
	Call BuildSelect(lst_Development, "sel_DEVELOPMENT", "P_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "All Developments", Null, "width:176px", "textbox", " onchange=""GetBlocks()"" ")	
	Call BuildSelect(lst_Block, "sel_BLOCK", "P_BLOCK WHERE BLOCKID = -1", "BLOCKID, BLOCKNAME", "BLOCKNAME", "Select Development", Null, "width:176px", "textbox", " ")	
	CloseDB()	

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Cyclical Repair Forecasting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function GetCalendar(Tdate){
		RSLFORM.target = "RepairFrame<%=TIMESTAMP%>";
		RSLFORM.action = "ServerSide/Calendar_svr.asp?Date=" + Tdate
		RSLFORM.submit();
		}

	
	function NewDay(rDate){
		RSLFORM.hid_Date.value= rDate
		dDate.innerHTML = rDate	
		UpdateForecast()
		}
	
	function UpdateForecast(){
		MainDiv.innerHTML = "<br><BR><BR><center style='font-size:22px;font-weight:bold'>Please Wait...</center>"
		RSLFORM.target = "RepairFrame<%=TIMESTAMP%>";
		RSLFORM.action = "serverside/Forecast_svr.asp"
		RSLFORM.submit();		
		}
		
	function GetBlocks(){
		RSLFORM.target = "RepairFrame<%=TIMESTAMP%>";
		RSLFORM.action = "serverside/Blocks_svr.asp"
		RSLFORM.submit();
		}
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name=RSLFORM method=post>
<table width=100% cellspacing=0 cellpadding=2>
    <tr> 
      <td> 
<div id="CalendarHolder">
<!--#include file="ServerSide/Calendar_Create.asp" -->
</div>

</td>
<td rowspan=4 width=100% valign=top style='padding-left:8px'>
<DIV ID=MainDiv>fdsf</DIV>
</td>
</tr>
<tr><td>

	<TABLE width=182px BORDER=0 STYLE='BORDER:1PX SOLID SILVER;BACKGROUND-COLOR:BEIGE' CELLPADDING=2 CELLSPACING=0 height=51>
	  <TR bgcolor=#133e71>
            <TD colspan=2 style='color:white' height=20 align=center><b><div id="dDate" style='font-weight:bold;font-size:22px'><%=FormatDateTime(Date,2)%></div></b></TD>
	  </TR>
	</TABLE>
	<input type=hidden name="hid_Date" value="<%=FormatDateTime(Date,2)%>">
</TD>
</TR>
<tr><td>

	<TABLE width=180px BORDER=0 STYLE='BORDER:1PX SOLID SILVER;BACKGROUND-COLOR:BEIGE' CELLPADDING=2 CELLSPACING=0 height=131>
	  <TR bgcolor=#133e71>
            <TD colspan=2 style='color:white' height=20 align=center><b>Filters</b></TD>
	  </TR>
	  <TR><TD>Developments</TD></TR>
	  <TR><TD><%=lst_Development%></TD></TR>
          <tr><td>Blocks:</td></tr>
	  <TR><TD><%=lst_Block%></TD></TR>
	  <tr><td align=right><input type=button class="RslButton" value="Forecast" onClick="UpdateForecast()"></td></tr>
	<tr><td height=100%>&nbsp;</td></tr>	
	</TABLE>
	
</TD></TR>
<TR><TD HEIGHT=100%>&nbsp;<br><BR><BR><BR><BR><BR></TD></TR>
</TABLE>
		
<BR>


</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name="RepairFrame<%=TIMESTAMP%>" width=400px height=400px style='display:none' SRC="serverside/ForeCast_svr.asp?hid_Date=<%=FormatDateTime(Date,2)%>"></iframe> 
</BODY>
</HTML>

