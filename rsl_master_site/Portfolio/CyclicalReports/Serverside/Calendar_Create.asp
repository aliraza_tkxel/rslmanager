<%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function

	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 0 'Sets the top position
	MyCalendar.Left = 0 'Sets the left position
	MyCalendar.Position = "relative" 'Relative or Absolute positioning
	MyCalendar.Height = "161" 'Sets the height
	MyCalendar.Width = "180" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = False 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='DDCSV.asp?pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		EndOfMonthDate = LastDayOfMonth(CDate("1" & "/" & TheCurrentMonth & "/" & TheCurrentYear)) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear					
	End If

	StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear


	Select Case Month(MyCalendar.GetDate())
		Case TheCurrentMonth

			OpenDB()
			

'			Call OpenRs(rsDays, ACTUALSQL)
'			while (NOT rsDays.EOF)
'				Difference = FormatNumber(rsDays("IDIFF"),2,-1,0,0)
'				'Response.Write rsDays("THEDAY") & " - " & Difference & "<BR>"
'				'isNull(Difference) OR 
'				TotalCount = CInt(rsDays("TOTALCOUNT"))
'				if (TotalCount = 0) then
'					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='No items have been validated against this day' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "orange"
'				elseif (Difference = 0 AND TotalCount <> 0) then
'					if (TotalCount = 1) then
'						TotalText = "1 item has"
'					else
'						TotalText = TotalCount & " items have"
'					end if
'					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='" & TotalText & " been validated and the closing balance matches.' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "green"
'				else
'					if (TotalCount = 1) then
'						TotalText = "1 item has"
'					else
'						TotalText = TotalCount & " items have"
'					end if
'					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' title='" & TotalText & " been validated and the difference is &pound;" & FormatNumber(Difference,2) & ".' href=""javascript:LoadDay(" & rsDays("BSID") & ")"" ><font color=white><small><b>" & rsDays("THEDAY") & " </b></small></font></a>", "red"				
'				end if
'				
'				rsDays.moveNext
'			wend
'			CloseRs(rsDays)									
		
			CloseDB()
			
	End Select
	
	MyCalendar.Draw()
%>