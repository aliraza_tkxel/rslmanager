<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()
	
	if Request("sel_DEVELOPMENT") = "" then

			OptionString = "<select name='sel_BLOCK' style='width:176px' class='textbox'>" &_
							"<option value=''>Select Development</option>" &_
							"</select>"
	
	else
		
		SQL = "SELECT BLOCKID, BLOCKNAME FROM P_BLOCK WHERE DEVELOPMENTID = " & Request("sel_DEVELOPMENT")
		Call OpenRs(rsB, SQL)
		if NOT rsB.EOF then
		
			OptionString = "<select name='sel_BLOCK' style='width:176px' class='textbox'>" &_
							"<option value=''>All Blocks</option>"
			
			while NOT rsB.EOF
				OptionString = OptionString & "<option value=""" & rsB("BLOCKID") & """>" & rsB("BLOCKNAME") & "</option>"
				rsB.moveNext
			wend	
			OptionString = OptionString & "</select>"
		
		else
	
			OptionString = "<select name='sel_BLOCK' style='width:176px' class='textbox'>" &_
							"<option value=''>No Blocks Found</option>" &_
							"</select>"
		
		end if
		Call CloseRs(rsB)
	
		Call CloseDB()

	end if

%>
<SCRIPT LANGUAGE="JAVASCRIPT">
function ReturnData(){
	parent.RSLFORM.sel_BLOCK.outerHTML = BlockData.innerHTML				
	}
</SCRIPT>
<BODY onLoad="ReturnData()">
<DIV id="BlockData">
<%=OptionString%>
</DIV>
</BODY>
</HTML>