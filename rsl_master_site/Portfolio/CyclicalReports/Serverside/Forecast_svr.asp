<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Function CylicalNumber (strWord)
		CylicalNumber = "<b>CYC</b> " & characterPad(strWord,"0", "l", 6)
	End Function

	Call OpenDB()
	
	StartDate = Request("hid_Date")
	
	Development = Request("sel_DEVELOPMENT")
	if (Development = "") then Development = "NULL"
	
	Block = Request("sel_BLOCK")
	if (Block = "") then Block = "NULL"
	
	Counter = 0
	TotalNetCost = 0
	
	SQL = "EXEC CYC_REPAIRS_FORECAST '" & FormatDateTime(Startdate,1) & "', " & Development & ", " & Block
	Call OpenRs(rsLoader, SQL)
	if NOT rsLoader.EOF then
		
		WHILE NOT rsLoader.EOF
				NetCost = rsLoader("REPAIRNETCOST")
				BlockName = rsLoader("BLOCKNAME")
				if NOT isNull(BlockName) AND BlockName <> "" then 
					BlockName = " - [<font color=green>" & BlockName & "</font>]"
				else
					BlockName = ""
				end if
				if NOT isNull(NetCost) then TotalNetCost = TotalNetCost + NetCost
				pmstring = pmstring &_
						"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "' style='cursor:hand')>" &_
							"<TD width=90 nowrap>" & rsLoader("STARTDATE") & "</TD>" &_
							"<TD width=445 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br><b>" & rsLoader("DEVELOPMENTNAME") & "</b> " & BlockName & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("SUPPLIER") &_
							"<div align=right><B>Started On</B> " & rsLoader("CSTART") & " <B>Last cycle on</B> " & rsLoader("CLAST") & "</DIV>" &_
							"<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
						 "</TR>"						
				Counter = Counter + 1
			rsLoader.MoveNext
		WEND
	
	else
		pmstring = pmstring & "<TR><TD colspan=6 align=center>No Cyclical Repairs exist for the selected criteria</TD></TR>" 
	end if
	
	Call CloseRs(rsLoader)
	Call CloseDB()
	
%>
<SCRIPT LANGUAGE="JAVASCRIPT">
function ReturnData(){
	parent.MainDiv.innerHTML = ForecastData.innerHTML				
	}
</SCRIPT>
<BODY onLoad="ReturnData()">
<DIV id="ForecastData">
	<strong>Please Note :</strong> The date is the date that will be attached to the cyclical works order/purchase order when it is automatically generated. This is usually a month before the shown date.<br><br>
	<TABLE cellspacing=0 cellpadding=0 border=0 style='border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=555 height="100%">
	  <TR bgcolor='#133E71' style='color:#FFFFFF'> 
		<TD height=23 width=90 nowrap><b>&nbsp;Date</b></TD>
		<TD width=445 nowrap><b>&nbsp;Repair</b></TD>
		<TD width=500></TD>
	  </TR>
	  <TR> 
		<td valign=top height=100% colspan=6> 
		  <div id="CYCDATA" name="CYCDATA" style='height:340PX;width:555;overflow:auto' class='TA'> 
			<table cellspacing=0 cellpadding=3 border=1 STYLE='behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' hlcolor='STEELBLUE' width=100%>
			  <%=pmstring%> 
			</table>
		  </div>
		</td>
	  </tr>
	  <tr><td colspan=3 align=right height=25px style='border-top:1px solid #133e71'> Total Cyclical Repairs : <strong><%=Counter%></strong> Total Net Value : <strong><%=FormatCurrency(TotalNetCost)%></strong>&nbsp;&nbsp;</td></tr>
	</TABLE>
</DIV>
</BODY>
</HTML>