<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
    'variable declaration
    dim top_data,bottom_data,asset_type_data,norecords,count,localauthority,developmentid,patch,postcode,processdate,displaydate
    
    CONST CONST_PAGESIZE = 7
    
    'Start of posted form values 
    if(Request.Item("localauthority")<>"") then
        localauthority=Request.Item("localauthority")
    else
        localauthority=null
    end if
    
    if(Request.Item("developmentid")<>"") then
        developmentid=Request.Item("developmentid") 
    else
        developmentid=null
    end if
    
    if(Request.Item("patch")<>"") then
        patch=Request.Item("patch")
    else
        patch=null
    end if
    
    if(Request.Item("processdate")<>"") then
        processdate=Request.Item("processdate")
    else
        processdate=null   
    end if    
    
    if(Request.Item("postcode")<>"") then 
        postcode=Request.Item("postcode")
    else
        postcode=null
    end if
    
    if(processdate<>null) then
        displaydate=processdate
    else
        displaydate=date()
    end if
        
    if(Request.Item("form_post")<>"") then
        form_post=Request.Item("form_post")
    else
        form_post=0
    end if
    
    if(Request.Item("propertytypeid")<>"") then
        propertytypeid=Request.Item("propertytypeid")
    else
        propertytypeid=null
    end if
    
    if(Request.Item("propertytypedescription")<>"") then
        propertytypedescription=Request.Item("propertytypedescription")
    else
        propertytypedescription=""
    end if
    
    if(Request.Item("bed")<>"") then
        bed=Request.Item("bed")
    else
        bed=""
    end if
    
    if(Request.Item("person")<>"") then
        person=Request.Item("person")
    else
        person=""
    end if
    
    If Request.Item("page") = "" Then
		page = 1
    Else
		page = Request.Item("page")
    End If
    
    If Request.Item("status")<> "" Then
		status = Request.Item("status")
    Else
		status = null
    End If
  
    If Request.Item("statusdescription")<> "" Then
		statusdescription =  "->" & Request.Item("statusdescription")
    Else
		statusdescription = ""
    End If
    
    If(Request.Item("assetid")<>"") Then
      assetid=Request.Item("assetid")
    Else
      assetid=null  
    End If 
    
    If (Request.Item("assetdescription")<>"TOTAL") Then
          assetdescription="->"& Request.Item("assetdescription")
    Else
        assetdescription=""
    End if
    
    If(Request.Item("personbedsdescritpion")<>"") Then
        personbedsdescritpion=Request.Item("personbedsdescritpion")
    Else
        personbedsdescritpion=""
    End if
    
    If(Request.Item("oldstatus")<>"") Then
        oldstatus=Request.Item("oldstatus")
     Else
        oldstatus=""
     End if
    
    'End of posted form value    
    
    'top data variable make the data for the box at the top
    top_data="<p class=""top_trail"">Portfolio Stock Report " & statusdescription & assetdescription  & ""
    
    'Altering the form width depending on data of window and posted form
    if(form_post=3 or form_post=4) then 
        asset_type_data= "<table id=""PortfolioPropertyDataTable"" border=""0"" cellpadding=""0""  class=""property_data_table"" cellspacing=""0"" style=""width:530px;""  summary=""Portfolio Property Stock Summary"">"
	else
	    asset_type_data= "<div style=""overflow:auto;height:380px;"" ><table id=""PortfolioPropertyDataTable"" border=""0"" cellpadding=""0""  class=""property_data_table"" cellspacing=""0""  summary=""Portfolio Property Stock Summary"">"
	end if
	
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
    
    dbrs.CursorType = 3
	dbrs.CursorLocation = 3
        
    dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    dbcmd.CommandType = 4
	
	'this form shows the window 
	if(form_post=1) then
	    'response.Write ("processdate: " & processdate & " localauthority: " & localauthority & " developmentid: " & developmentid & " postcode: " & postcode & " housingofficer: " & housingofficer & " assetid: " & assetid & " propertytypeid: " & propertytypeid & " status: " & status )
	    top_data=top_data & "->" & propertytypedescription
	    checkbox="<span style=""padding-left:40px;"">&nbsp;</span><label for=""includeoccupancy"">Include Occupancy</label><input  type=""checkbox"" id=""includeoccupancy"" name=""includeoccupancy"" onClick=""occupancy();"" checked /><span style=""margin-left:103px""></span>"
	    Printtext="PRINT"
	    printbtnwidth=40
	    printtype=1
	    
	    asset_type_data=asset_type_data & "<tr>"
	    asset_type_data=asset_type_data & "   <th style=""padding-right:20px;"">Pers/Bed</th>"
	    asset_type_data=asset_type_data & "   <th>Total Count</th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftfivepx"">Let</span></th>"
	    asset_type_data=asset_type_data & "   <th>Available</th>"
	    asset_type_data=asset_type_data & "   <th>Unavailable</th>"
	    asset_type_data=asset_type_data & "</tr>"

        dbcmd.CommandText = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE_NUMB_BEDROOMS_WITH_OCCUPANCY"
        dbcmd.Parameters.Refresh
        
        dbcmd.Parameters(1)=processdate
        dbcmd.Parameters(2)=localauthority
        dbcmd.Parameters(3)=developmentid
        dbcmd.Parameters(4)=postcode
        dbcmd.Parameters(5)=patch
        dbcmd.Parameters(6)=assetid
        dbcmd.Parameters(7)=propertytypeid
        dbcmd.Parameters(8)=status
        
        'Response.Write ("processdate: " & processdate & "<br/> localauthority:" & localauthority & "<br/> developmentid:" & developmentid & "<br/> postcode:" & postcode & "<br/> housingofficer:" & housingofficer & "<br/> assetid:" & assetid & "<br/> propertytypeid:" & propertytypeid)
        
        set dbrs=dbcmd.execute
    
        if(not dbrs.eof) then	
                norecords =dbrs.recordcount
	            'response.write norecords
	    end if
	
	    count=0
	    
	    do while not dbrs.eof 
	        count=count+1
	        if(count=1 or count<=norecords or dbrs("bed")="TOTAL") then
	            asset_type_data=asset_type_data & "<tr>"
	            asset_type_data=asset_type_data & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
	            asset_type_data=asset_type_data & "</tr>" 
	            asset_type_data=asset_type_data & "<tr>" 
	            
	            if(count=1) then
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("occupancy")& dbrs("bed") & "</td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','')"">" & dbrs("total") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printpaddinglefttenpx""><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','2')"">" & dbrs("let") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','1')"">" & dbrs("available") & "</span></td>"
    	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','4')"">" & dbrs("unavailable") & "</span></td>"
	            else
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold""><span class=""printbold"">" & dbrs("occupancy")& dbrs("bed") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold"">" & dbrs("total") & "</td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""printpaddinglefttenpxlinkstyle"" onclick=""personandnoofbeds('','','2')"">" & dbrs("let") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""personandnoofbeds('','','1')"">" & dbrs("available") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""personandnoofbeds('','','4')"">" & dbrs("unavailable") & "</span></td>"
	            end if
	            
	            asset_type_data=asset_type_data & "</tr>"
	        else
	            asset_type_data=asset_type_data & "<tr>" 
	            asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("occupancy")& dbrs("bed") & "</td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")&"','" & dbrs("bed")& "','')  "">" & dbrs("total") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td class=""printpaddinglefttenpx""><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','2')"">" & dbrs("let") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','1')"">" & dbrs("available") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""personandnoofbeds('" & dbrs("occupancy")& "','" & dbrs("bed")& "','4')"">" & dbrs("unavailable") & "</span></td>"
	            asset_type_data=asset_type_data & "</tr>"
	        end if    
	        dbrs.movenext()
	    loop 
	elseif(form_post=2) then
	    top_data=top_data & "->" & propertytypedescription
	    checkbox="<span style=""padding-left:40px;"">&nbsp;</span><label for=""includeoccupancy"">Include Occupancy</label><input  type=""checkbox"" id=""includeoccupancy"" name=""includeoccupancy"" onClick=""occupancy();"" /><span style=""margin-left:103px""></span>"
	    Printtext="PRINT"
	    printbtnwidth=40
	    printtype=1
	    
	    'response.Write ("processdate: " & processdate & " localauthority: " & localauthority & " developmentid: " & developmentid & " postcode: " & postcode & " housingofficer: " & housingofficer & " assetid: " & assetid & " propertytypeid: " & propertytypeid & " status: " & status )
	    
	    asset_type_data=asset_type_data & "<tr>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingrighttwelevepx"">Bed</span></th>"
	    asset_type_data=asset_type_data & "   <th>Total Count&nbsp;</th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftrighteightpx"">Let</span></th>"
	    asset_type_data=asset_type_data & "   <th>Available</th>"
	    asset_type_data=asset_type_data & "   <th>Unavailable&nbsp;&nbsp;</th>"
	    asset_type_data=asset_type_data & "</tr>"

        dbcmd.CommandText = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE_NUMB_BEDROOMS"
        dbcmd.Parameters.Refresh
        dbcmd.Parameters(1)=processdate
        dbcmd.Parameters(2)=localauthority
        dbcmd.Parameters(3)=developmentid
        dbcmd.Parameters(4)=postcode
        dbcmd.Parameters(5)=patch
        dbcmd.Parameters(6)=assetid
        dbcmd.Parameters(7)=propertytypeid
        dbcmd.Parameters(8)=status
        
        
        
        set dbrs=dbcmd.execute
        
            
        if(not dbrs.eof) then	
                norecords =dbrs.recordcount
	            'response.write norecords
	    end if
	
	    count=0
	    
	    do while not dbrs.eof 
	        count=count+1
	        if(count=1 or count<=norecords or dbrs("bed")="TOTAL") then
	            asset_type_data=asset_type_data & "<tr>"
	            asset_type_data=asset_type_data & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
	            asset_type_data=asset_type_data & "</tr>" 
	            asset_type_data=asset_type_data & "<tr>" 
	            
	            if(count=1) then
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("bed") & "</td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','')"">" & dbrs("total") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','2')"">" & dbrs("let") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','1')"">" & dbrs("available") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','4')"">" & dbrs("unavailable") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	            else
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold""><span class=""printbold"">" & dbrs("bed") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""printbold"">" & dbrs("total") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""noofbeds('','2')"">" & dbrs("let") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""noofbeds('','1')"">" & dbrs("available") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""noofbeds('','4')"">" & dbrs("unavailable") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	            end if

	        else
	            asset_type_data=asset_type_data & "<tr>" 
	            asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("bed") & "</td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','')"">" & dbrs("total") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','2')"">" & dbrs("let") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','1')"">" & dbrs("available") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""noofbeds('" & dbrs("bed") & "','4')"">" & dbrs("unavailable") & "</span></td>"
	            asset_type_data=asset_type_data & "</tr>"
	        end if    
	        dbrs.movenext()
	    loop
	
	elseif(form_post=3 or form_post=4) then   
	    checkbox="<span style=""margin-left:340px""></span>"
	    Printtext="PRINT ALL"
	    printbtnwidth=60
	    printtype=2
	    
	    heading=heading & "<tr>"
	    heading=heading & "   <th><span class=""paddingleftrightelevenpx"">Property Ref</span></th>"
	    heading=heading & "   <th><span style=""margin-right:50px;padding-left:50px;"">Address</span></th>"
	    heading=heading & "   <th>Status</th>"
	    heading=heading & "   <th>Substatus</th>"
	    heading=heading & "</tr>"
	    
	    asset_type_data= asset_type_data & heading
	    
	    'Response.Write ("processdate: " & processdate & "<br/> localauthority:" & localauthority & "<br/> developmentid:" & developmentid & "<br/> postcode:" & postcode & "<br/> housingofficer:" & housingofficer & "<br/> assetid:" & assetid & "<br/> propertytypeid:" & propertytypeid & "<br/> bed" & bed & "<br/> person" & person)	           
        dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
		data_source = "P_PORTFOLIOANALYSIS_REPORT_DETAIL @REPORTDATE='" & processdate & "'"
		
		if(localauthority<>"") then
		    data_source=data_source & ",@LOCALAUTHORITY='" & localauthority & "'"
		end if
		
		if(developmentid<>"") then    
		    data_source=data_source & ",@SCHEME='" & developmentid & "'"
		end if
		
		if(postcode<>"") then    
		    data_source=data_source & ",@POSTCODE='" & postcode & "'"
		end if
		
		if(patch<>"") then    
		    data_source=data_source & ",@patch='" & patch & "'"
		end if    
		
		if(assetid<>"") then
		    data_source=data_source & ",@ASSETTYPE=" & assetid & ""
		end if
		
		if(propertytypeid<>"") then
		    data_source=data_source &  ",@PROPERTYTYPE=" & propertytypeid & ""
		end if
		
		if(status<>"") then
		    data_source=data_source &  ",@STATUS=" & status & ""
		end if
		
		if(bed<>"") then
		    data_source=data_source & ",@BEDS='" & bed &"'"
		end if
		
		if(person<>"") then 
	            data_source= data_source & ",@OCCUPANCY='" & person & "'"
	     end if 
		
		'Response.Write data_source 
		
		dbrs.Open data_source
		
		top_data=top_data & "->" & propertytypedescription & "->" & personbedsdescritpion
	   	    
	    
			
		
		print_all_data= print_all_data & "<table id=""print_all_table"">" & heading
		do while not dbrs.eof
		  if dbrs("address")<>"TOTAL" then  
		        print_all_data=print_all_data & "<tr>" 
                print_all_data=print_all_data & "   <td class=""fontweightbold"">" & dbrs("propertyid") & "</td>"
                print_all_data=print_all_data & "   <td >" & dbrs("address") & "</td>"
                print_all_data=print_all_data & "   <td >" & dbrs("statusdescription") & "</td>"
                print_all_data=print_all_data & "   <td >" & dbrs("substatusdescription") & "</td>"
                print_all_data=print_all_data & "</tr>"
            end if
		    dbrs.movenext()
	    loop  
		print_all_data=print_all_data & "</table>" 
		
		dbrs.PageSize = CONST_PAGESIZE
		'my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		dbrs.CacheSize = CONST_PAGESIZE
		intPageCount = dbrs.PageCount 
		intRecordCount = dbrs.RecordCount 
       'response.Write  "<br/>" & intRecordCount	    
		
		' Sort pages
		intpage = Cint(page)
		
    	If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
		
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If
        
        ' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If
        
        ' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			dbrs.AbsolutePage = intPage
			intStart = dbrs.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (dbrs.PageSize - 1)
			End if
		End If
        
       if intRecordCount > 0 Then
	         ' Display the record that you are starting on and the record
	         ' that you are finishing on for this page by writing out the
	         ' values in the intStart and the intFinish variables.
	         ' Iterate through the recordset until we reach the end of the page
	         ' or the last record in the recordset.
	         
	   	     for intRecord = 1 to dbrs.PageSize
            
                if (intRecord =1) then
                    asset_type_data=asset_type_data & "<tr>"
	                asset_type_data=asset_type_data & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
	                asset_type_data=asset_type_data & "</tr>" 
                end if
                
                if(dbrs("address")<>"TOTAL") then
                
                    asset_type_data=asset_type_data & "<tr style='cursor:hand' onmouseover=""this.style.backgroundColor='#117DFF';"" onmouseout=""this.style.backgroundColor='white';"" onclick=""propertyredirect('" & dbrs("propertyid") & "');"">" 
                    asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("propertyid") & "</td>"
                    asset_type_data=asset_type_data & "   <td >" & dbrs("address") & "</td>"
                    asset_type_data=asset_type_data & "   <td >" & dbrs("statusdescription") & "</td>"
                    asset_type_data=asset_type_data & "   <td >" & dbrs("substatusdescription") & "</td>"
                    asset_type_data=asset_type_data & "</tr>"
                    
                end if
                dbrs.movenext()
    		    if dbrs.EOF then exit for
    		    'if dbrs("address")="TOTAL" then exit for			
			  next   
     	end if
 	    
   	    asset_type_data=asset_type_data & "<tr>"
        asset_type_data=asset_type_data & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
        asset_type_data=asset_type_data & "</tr>" 
        
        if((intPage)*dbrs.pagesize+count > intRecordCount-1) then
            numbofrecordonpage=intRecordCount-1
        else
            numbofrecordonpage=(intPage)*dbrs.pagesize+count
        end if 
          
        asset_type_data=asset_type_data & "<tr><td colspan=""4"" style=""padding-top:10px;"">"
        asset_type_data=asset_type_data & "<span style=""margin-left:80px;text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('1');"">FIRST</span>&nbsp;&nbsp;"    
        asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & prevpage & "');"">PREV</span>&nbsp;&nbsp;" 
        asset_type_data=asset_type_data & "<span> Page " & intpage & " of " & intPageCount & ". Records: " & dbrs.PageSize * (intpage - 1) + 1 & "  to " & numbofrecordonpage & " of " & intRecordCount-1 & "</span>"  
        asset_type_data=asset_type_data & "&nbsp;&nbsp;<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & nextpage & "');"">NEXT</span>&nbsp;&nbsp;"
        asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & intPageCount & "');"">LAST</span>"    
        asset_type_data=asset_type_data & "</td></tr>" 
        
	else
	    'response.Write ("processdate: " & processdate & " localauthority: " & localauthority & " developmentid: " & developmentid & " postcode: " & postcode & " housingofficer: " & housingofficer & " assetid: " & assetid & " propertytypeid: " & propertytypeid & " status: " & status )
	    checkbox="<span style=""margin-left:270px""></span>"
	    Printtext="PRINT"
	    printbtnwidth=40
	    printtype=1
	    asset_type_data=asset_type_data & "<tr>"
	    asset_type_data=asset_type_data & "   <th style=""padding-right:40px;"">Property Type</th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftfivepx"">Total Count</span></th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftninepx"">Let<span></th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftfivepx"">Available</span></th>"
	    asset_type_data=asset_type_data & "   <th><span class=""paddingleftfivepx"">Unavailable</span></th>"
	    asset_type_data=asset_type_data & "</tr>"

        dbcmd.CommandText = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE"
        dbcmd.Parameters.Refresh
        dbcmd.Parameters(1)=processdate
        dbcmd.Parameters(2)=localauthority
        dbcmd.Parameters(3)=developmentid
        dbcmd.Parameters(4)=postcode
        dbcmd.Parameters(5)=patch
        dbcmd.Parameters(6) =assetid
        dbcmd.Parameters(7) =status
        set dbrs=dbcmd.execute
    
        if(not dbrs.eof) then	
                norecords =dbrs.recordcount
	            'response.write norecords
	    end if
	
	    count=0
	    
	    do while not dbrs.eof 
	        count=count+1
	        if(count=1 or count<=norecords or dbrs("description")="TOTAL") then
	            asset_type_data=asset_type_data & "<tr>"
	            asset_type_data=asset_type_data & "<td colspan=""5"" class=""thinline"">&nbsp;</td>"    
	            asset_type_data=asset_type_data & "</tr>" 
	            asset_type_data=asset_type_data & "<tr>" 
	            
	            if(count=1) then
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("description") & "</td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','0')"">" & dbrs("total") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printpaddinglefttenpx""><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','2')""> " & dbrs("let") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','1')"">" & dbrs("available") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','4')"">" & dbrs("unavailable") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	            else
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold""><span class=""printbold"">" & dbrs("description") & "<span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold"">" & dbrs("total") & "</td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""printpaddinglefttenpxlinkstyle"" onclick=""propertytype('0','All Property Type','2')"">" & dbrs("let") & "<span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""propertytype('0','All Property Type','1')"">" & dbrs("available") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""linkstyle"" onclick=""propertytype('0','All Property Type','4')"">" & dbrs("unavailable") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	            end if
	            
	        else
	            asset_type_data=asset_type_data & "<tr>" 
	            asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("description") & "</td>"
	            asset_type_data=asset_type_data & "   <td ><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','0')"">" & dbrs("total") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td class=""printpaddinglefttenpx""><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','2')"">" & dbrs("let") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','1')"">" & dbrs("available") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertytype('" & dbrs("propertytypeid") & "','" & dbrs("description") & "','4')"">" & dbrs("unavailable") & "</span></td>"
	            asset_type_data=asset_type_data & "</tr>"
	        end if    
	        dbrs.movenext()
	    loop
   end if 
   if(form_post=3 or form_post=4) then 
        asset_type_data=asset_type_data & "</table>" & print_all_data
   else
        asset_type_data=asset_type_data & "</table></div>" & print_all_data
   end if
   
   top_data=top_data & "</p><p class=""top_processdate""><span class=""printbold"">Date:</span>" & displaydate & ""
   top_data=top_data & checkbox
   top_data=top_data & "<input type=""button""  id=""back_button"" name=""btn_back"" value=""<<Back"" class=""RSLButton"" style="" text-align:right;width:50px;margin-top:3px;"" onclick=""back();"" />"
   top_data=top_data & "<input type=""button""  id=""print_button"" name=""btn_print"" value="""& Printtext & """ class=""RSLButton"" style="" margin-left:10px;width:" & printbtnwidth &"px;margin-top:3px;"" onclick=""print_data('" & printtype & "');"" /></p>" 
   
   bottom_data=asset_type_data
   
   'dbrs.Close()
   'dbcmd.Cancel()
%>
<html>
    <head>
        <title>Stock Analysis</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
        <style type="text/css" media="screen">
           #top_content{border-style: solid;border-color:#133e71;border-width:1px;padding-top:0;padding-bottom:5px;margin-bottom:4px;} 
           #bottom_content{border-style: solid;border-color:#133e71;border-width:1px 1px 0px 1px;padding-top:0;padding-bottom:0px;margin-bottom:0px;height:200px} 
           .border_left{height:67px;margin-right:67px;border-bottom:solid 1px;border-left:solid 1px ;border-color:#133e71;}
           .bottom_right{padding-top:0px;margin:0;margin-left:0px;margin-right:0px; height:67px;}
           .property_data_table { background-color: #FFFFFF; margin-left:10px;padding-bottom:10px;width:450px;margin-bottom:3px;}
           .property_data_table th{font-size:10px;font-weight:bold;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-left:0px;padding-right:15px;padding-top:5px;margin-bottom:0px;padding-bottom:0px;}
           .property_data_table td{padding-top:0px;padding-left:13px;font-size:10;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-right:4px;}
           .thinline{border-style: solid;border-color:#gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}  
           .linkstyle{text-decoration: underline;color:blue;cursor:hand;}
            .fontweightbold{font-weight:bold;}
            #print_all_table{display:none;}
            .top_trail{margin-left:5px;margin-bottom:0px;margin-top:2;}
            .top_processdate{margin-top:0px;margin-left:5px;padding-bottom:0px;margin-bottom:0px;}
            .paddingleftfivepx{padding-left:5px;}
            .paddingleftninepx{padding-left:9px;}
            .paddingrighttwelevepx{padding-right:12px;}
            .paddingleftrighteightpx{padding-left:8px;}
            .paddingleftrightelevenpx{padding-left:11px;}
            .paddingleftrightfifteenpx{padding-right:15px;}
            .printpaddinglefttenpxlinkstyle{text-decoration: underline;color:blue;cursor:hand;}
        </style>
        <style type="text/css" media="print">
            #top_content{border:none;}
            #bottom_content{border:none;}
            h1{font-size:14px;}
            th{font-size:12px;padding-right:10px;margin:0px;padding-left:0px;margin-left:0px;}
            td{padding-right:40px;padding-top:10px;}
            #back_button{display:none;}
            #print_button{display:none;}
            #includeoccupancy{display:none;}
            label{display:none;}
            .top_processdate{padding-top:10px;padding-bottom:10px;}
            .top_trail{font-size:14px;font-weight:bold;}
            .printbold{font-weight:bold;}
            .printpaddingrighttenpx{padding-right:10px;}
            .printpaddinglefttenpx{padding-left:20px}
            .printpaddinglefttenpxlinkstyle{padding-left:20px}
            .paddingleftrighteightpx{padding-right:9px;}
            .paddingrightprint{padding-right:20px;}
            .paddingrighttwelevepx{padding-right:17px;margin-right:17px;margin-left:0px;padding-left:0px;}
            .paddingleftrightelevenpx{padding-right:10px;}
          </style>
    </head>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript">	
    function propertytype(propertytypeid,propertytypedescription,propertystatus)
    {
        rslform.form_post.value=1;
        if(propertytypeid!='0') 
        {
            rslform.propertytypeid.value=propertytypeid;
        }
        rslform.propertytypedescription.value=propertytypedescription;
        if(propertystatus!='0') 
        {
            rslform.status.value=propertystatus;
        }
        //alert( propertytypedescription);
        rslform.submit();
    }	
    function back()
    {
        if(rslform.form_post.value==1 || rslform.form_post.value==2) 
        {
          rslform.form_post.value=0; 
          rslform.status.value='';
          rslform.propertytypeid.value='';
          window.resizeTo(550,600);
          rslform.submit();
        }
        else 
            if(rslform.form_post.value==3)
            {
                
                window.resizeTo(507,490);
                rslform.form_post.value=1; 
                rslform.bed.value='';
                rslform.person.value='';    
                rslform.status.value=rslform.oldstatus.value;
                rslform.submit();
            }
            else 
                if(rslform.form_post.value==4)
                {
                    window.resizeTo(507,490);
                    rslform.form_post.value=2;
                    rslform.bed.value='';
                    rslform.person.value='';
                    rslform.status.value=rslform.oldstatus.value; 
                    rslform.submit();    
                }
                else 
                {
                    window.close();
                }  
    }
    function occupancy()
    {
        if(rslform.includeoccupancy.checked==1) 
        {
           rslform.form_post.value=1;
           rslform.submit();
        }
        else
        {
            rslform.form_post.value=2;
            rslform.submit();
        }
    }
    function noofbeds(beds,propertystatus)
    {
        window.resizeTo(600,480);
        rslform.form_post.value=4;
        rslform.bed.value=beds;
        if(beds=='' && propertystatus!='')
        {
            if(propertystatus=='2')
            {
                rslform.personbedsdescritpion.value='Let'
            }
            else
            {
                if(propertystatus=='1')
                {
                    rslform.personbedsdescritpion.value='Available'
                }
                else
                {
                    rslform.personbedsdescritpion.value='Unavailable'
                }
                
            }
        }
        else
        {
             rslform.personbedsdescritpion.value=beds
        }
        rslform.oldstatus.value=rslform.status.value;
        
        if(propertystatus!='')
        {
            rslform.status.value=propertystatus;
        }
        rslform.submit();
    }
    
    function personandnoofbeds(persons,beds,propertystatus)
    {
        window.resizeTo(600,480);
        rslform.form_post.value=3;
        rslform.person.value=persons;
        rslform.bed.value=beds;
        if(beds=='' && persons=='' && propertystatus!='')
        {
            if(propertystatus=='2')
            {
                rslform.personbedsdescritpion.value='Let'
            }
            else
            {
                if(propertystatus=='1')
                {
                    rslform.personbedsdescritpion.value='Available'
                }
                else
                {
                    rslform.personbedsdescritpion.value='Unavailable'
                }
                
            }
        }
        else
        {
             rslform.personbedsdescritpion.value= persons + beds
        }
        rslform.oldstatus.value=rslform.status.value;
        if(propertystatus!='')
        {
            rslform.status.value=propertystatus;
        }
        rslform.submit();
    }
    function movetopage(pageno)
    {
        rslform.page.value=pageno;
        rslform.submit();
    }
    function propertyredirect(propertyid)
    {
        
        window.opener.location="../PRM.asp?PropertyID="+propertyid;
        window.close();
   }
  function print_data(printtype)
  {
       
    if(printtype==1)
    {
       document.getElementById("PortfolioPropertyDataTable").style.display="block";
       window.print();
    }
    else
    {
        document.getElementById("PortfolioPropertyDataTable").style.display="none";
        window.print();
        document.getElementById("PortfolioPropertyDataTable").style.display="block";
    }   
    
    
  }
  </script>	

    <body bgcolor="#FFFFFF" onload="window.focus()" >
        <form name="rslform" method="post"  action="" >
            <div id="top_content">
                <%=top_data %>
            </div>
            <div >
                <div id="bottom_content" ><%=bottom_data %></div> 
                <div id="bottom_right" style="background: url(../../myImages/corner_pink_white_big.gif) 100% 100% no-repeat;">
                    <div class="border_left"></div>
                </div>
            </div>
            <input type="hidden" name="form_post" value="<%=form_post %>" />
            <input type="hidden" name="localauthority" value="<%=localauthority %>" />
            <input type="hidden" name="developmentid" value="<%=developmentid%>" />
            <input type="hidden" name="patch" value="<%=patch %>" />
            <input type="hidden" name="processdate" value="<%=processdate%>" />
            <input type="hidden" name="postcode" value="<%=postcode%>" />
            <input type="hidden" name="assetid" value="<%=assetid %>" />
            <input type="hidden" name="assetdescription" value="<%=assetdescription %>" />
            <input type="hidden" name="propertytypeid" value="<%=propertytypeid %>" />
            <input type="hidden" name="propertytypedescription" value="<%=propertytypedescription %>" />
            <input type="hidden" name="bed" value="<%=bed %>" />
            <input type="hidden" name="person" value="<%=person %>" />
            <input type="hidden" name="page" value="<%=page%>" />
            <input type="hidden" name="status" value="<%=status%>" />
            <input type="hidden" name="oldstatus" value="<%=oldstatus%>" />
            <input type="hidden" name="personbedsdescritpion" value="<%=personbedsdescritpion%>" />
         </form>
        
    </body>
   
</html>

