<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%

	Dim BLOCK_ID, patch
	
	Dim DefaultOrg
    DefaultOrg = 1270 
    
	patch= "MULTIPLE PATCHES "
	BLOCK_ID = REQUEST("BLOCKID")

	OpenDB()

	SQL = "SELECT BLOCKNAME FROM P_BLOCK WHERE BLOCKID = " & BLOCK_ID
	Call OpenRs(rsDEV,SQL)
	THE_DEV_NAME = rsDEV("BLOCKNAME")
	Call CloseRs(rsDEV)
	
	'NEED TO FIND THE EMPLOYEE LIMIT FOR THE USER
	SQL = "SELECT ISNULL(EMPLOYEELIMIT,0) AS EMPLOYEELIMIT FROM E_JOBDETAILS WHERE EMPLOYEEID = " & SESSION("USERID")
	Call OpenRs(rsEmLimit, SQL)
	if (NOT rsEmLimit.EOF) then
		EmployeeLimit = rsEmLimit("EMPLOYEELIMIT")
		DisableAll = ""			
	else
		DisableAll = " disabled"
		EmployeeLimit = -1
	end if
	CloseRs(rsEmLimit)
	
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", DefaultOrg, NULL, "textbox200", " style='width:300px' TABINDEX=1 " & DisableAll & "")
	Call BuildSelect(lst_location, "sel_LOCATION", "R_ZONE WHERE ZONEID NOT IN (13,14)", "ZONEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", " tabindex=2 onchange='select_change(1)' style='width:300px' " & DisableAll & " ")
	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:117' tabindex=3")
	Call BuildSelect(lstNatures, "sel_NATURE", "C_NATURE WHERE ITEMID = 1 AND ITEMNATUREID in (2,21,34,35,36,37,38,39,40)", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " STYLE='WIDTH:300' tabindex=1 onchange='SetTitle();ResetData()' ")	


	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD 15"
	Call OpenRs(rsTAXDATE, SQL)	

	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	CloseDB()	

%>
<html>
<head>
<title>Work Order</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();

	function SetChecking(Type){
		FormFields.length = 0
		if (Type == 1) {
			FormFields[0] = "sel_LOCATION|Location|SELECT|Y"
			FormFields[1] = "sel_ITEM|Item|SELECT|Y"
			FormFields[2] = "sel_ELEMENT|Element|SELECT|Y"
			FormFields[3] = "sel_REPAIR|Repair Details|SELECT|Y"
			FormFields[4] = "txt_NOTES|Notes|TEXT|N"
			FormFields[5] = "txt_NETCOST|Net Cost(�)|CURRENCY|Y"	
			FormFields[6] = "txt_VAT|VAT (�)|CURRENCY|Y"	
			FormFields[7] = "txt_GROSSCOST|Gross Cost(�)|CURRENCY|Y"			
			}
		else if (Type == 2){
			FormFields[0] = "sel_SUPPLIER|Supplier|SELECT|Y"
			FormFields[1] = "sel_NATURE|Repair Type|SELECT|Y"			
			FormFields[2] = "txt_PODATE|Order Date|DATE|Y"			
			}
		else if (Type == 3) {
			FormFields[0] = "sel_LOCATION|Location|SELECT|Y"
			FormFields[1] = "sel_ITEM|Item|SELECT|Y"
			FormFields[2] = "sel_ELEMENT|Element|SELECT|Y"
			FormFields[3] = "sel_REPAIR|Repair Details|SELECT|Y"
			FormFields[4] = "txt_NOTES|Notes|TEXT|N"
			FormFields[5] = "txt_NETCOST|Net Cost(�)|CURRENCY|Y"	
			FormFields[6] = "txt_VAT|VAT (�)|CURRENCY|Y"	
			FormFields[7] = "txt_GROSSCOST|Gross Cost(�)|CURRENCY|Y"
			}
		}
	
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
	
	function SetTitle(){
		document.getElementById("hid_NATURETITLE").value = document.getElementById("sel_NATURE").options(document.getElementById("sel_NATURE").selectedIndex).text
		}
				
	function SaveOrder(){
		SetChecking(2)
		if (!checkForm()) return false
		if (parseFloat(document.getElementById("hiGC").value) <= 0) {
			alert("Please enter some repairs before creating a Repair Work Order.")
			return false;
			}		
		RSLFORM.target = "BLO_BOTTOM_FRAME";
		RSLFORM.action = "../ServerSide/WorkOrderBlo_svr.asp?BLOCKID=<%=BLOCK_ID%>";

// lockdown code    
	    var YStart = new Date("<%=YearStartDate%>")
	    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    var YEnd = new Date("<%=YearendDate%>")
	    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
	    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>") 

	    if (!checkForm()) return false;
	    if  ( real_date(document.getElementById("txt_PODATE").value) < YStart ) {
	        // outside current financial year
	        if ( TodayDate <= YEndPlusTime ) {
	            // inside grace period
	            if ( real_date(document.getElementById("txt_PODATE").value) < YStartPlusTime) {
	                // outside of last and this financial year
                    alert("Please enter a tax date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
                    return false;
	            } else {
	                // date must be ok?
	            }
	        } else {
	            // we're outside the grace period and also outside this financial year
                alert("Please enter an Order Date no earlier than '<%=YearStartDate%>'");
                return false;                
		    }
	    }

		document.getElementById("SaveButton").disabled = true
		RSLFORM.submit();
		window.close()
		}

	// source = 1 --> 'LOCATION changed'  = 2 --> 'ITEM changed', 3 --> ELEMENT CHANGED, 4 --> GET PRIORITY
	function select_change(int_source){
		if (RSLFORM.sel_NATURE.value == "") {
			alert("Please select the nature of the work order before you continue.\nThis will filter the repair list to the appropriate items.")
			return false;
			}
		RSLFORM.target = "iRepairsFrame";
		RSLFORM.action = "../ServerSide/Selects_Srv.asp?source="+int_source+"&natureid=" + RSLFORM.sel_NATURE.value;
		RSLFORM.submit();
		}

	function SetRecharge(){
		if (document.getElementById("chk_RECHARGEABLE").checked) {
			alert("You have set this repair to be  rechargeable to the tenant.\nThis will insert the cost of the repair (plus vat) onto the Tenants Rent Account.")
			}
		}

	RowCounter = 0
	var ExpenditureArray = new Array()
	var ExpValueArray = new Array()
	var CCArray = new Array()	
		
	function AddRow(insertPos){
		SetChecking(1)			
		if (!checkForm()) return false

		GrossCost = document.getElementById("txt_GROSSCOST").value
		isQueued = 0

		//check wether the net cost has been changed in comparison to the database value,
		//if so then require the notes field to be filled in
		DataCost = document.getElementById("DatabaseCost").value	
		if (parseFloat(GrossCost) != parseFloat(DataCost)) FormFields[4] = "txt_NOTES|Notes|TEXT|Y"
		//rerun the check form validation, incase the notes are required
		if (!checkForm()) return false;

		//check the costcentrer will not go over...
		//note: have to remove the total of any other entries aswell.
		CCLeft = document.getElementById("hid_TOTALCCLEFT").value
		if ((parseFloat(CCLeft) < parseFloat(GrossCost))){
			alert("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the Cost Centre budget (�" + FormatCurrencyComma(CCLeft) + ") for the selected repair.\nTherefore this item cannot be entered onto the system.");			
			return false;
			}
			
		//check the expenditure will not go over...
		ExpLeft = document.getElementById("hid_TOTALEXPLEFT").value
		if ((parseFloat(ExpLeft) < parseFloat(GrossCost))){
//			alert("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget (�" + FormatCurrencyComma(ExpLeft) + ") for the selected repair.\nTherefore this item cannot be entered onto the system.");
//			return false;
			answer = confirm("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget (�" + FormatCurrencyComma(ExpLeft) + ") for the selected repair.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");			
			if (!answer) return false
			}	
			
		//check the employee is allowed to assign this to a contractor depending on their limit				
		EmployeeLimit = document.getElementById("hid_EMPLOYEELIMIT").value
		if ((parseFloat(EmployeeLimit) < parseFloat(GrossCost))){
			result = confirm("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than your employee repair limit (�" + FormatCurrencyComma(EmployeeLimit) + ") for the selected repair.\nTherefore this item will be put in a queue for authorisation by a manager.\nIf you wish to continue click on 'OK'.\nOtherwise click on 'CANCEL'.");
			if (!result) return false;
			isQueued = 1
			}				
		
		Rechargeable = 0
		if (document.getElementById("chk_RECHARGEABLE").checked == true)
			Rechargeable = 1

		if (RSLFORM.sel_NATURE.value == 22) {
			var str_request = "../POPUPS/ISOK3.ASP?BLOCK=<%=BLOCK_ID%>&REPAIR="+RSLFORM.sel_REPAIR.value
			var answer = window.showModalDialog(str_request, "", "dialogHeight:600px; dialogWidth:600px");
			if (answer == "FAILED") return false
			AnswerArray = answer.split("||****||")
			AnswerCount = AnswerArray[1]
			AnswerExcludes = AnswerArray[0]
			}
		else {
			AnswerExcludes = ""
			AnswerCount = 1
			}
					
		RowCounter++
		RepairID = document.getElementById("sel_REPAIR").value			
		RepairNotes = document.getElementById("txt_NOTES").value			
		RepairName = document.getElementById("sel_REPAIR").options(document.getElementById("sel_REPAIR").selectedIndex).text					
		NetCost = document.getElementById("txt_NETCOST").value			
		VAT = document.getElementById("txt_VAT").value											
		VatTypeID = document.getElementById("sel_VATTYPE").value			
		VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text					
		VatTypeCode = VatTypeName.substring(0,1).toUpperCase()					

		oTable = document.getElementById("RepairTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "EMPTYLINE") {
				oTable.deleteRow(i)
				break;
				}
			}

		oRow = oTable.insertRow(insertPos)
		oRow.id = "TR" + RowCounter
		oRow.style.cursor = "hand"
		
		DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + RepairID + "||<>||" + RepairNotes + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + isQueued + "||<>||" + Rechargeable + "||<>||" + AnswerExcludes + "||<>||" + RSLFORM.sel_NATURE.value + "\">"

		//THIS PART ADDS THE EXPENDITURE ID AND VALUE TO ARRAYS, INCASE ANOTHER REPAIR OF THE SAME TYPE IS SELECTED. 
		//SO A TRUE COMPARISON TO THE EXPENDITURE LEFT CAN BE MADE.
		ExpenditureArray[RowCounter] = document.getElementById("hid_EXPENDITUREID").value
		ExpValueArray[RowCounter] = GrossCost
		CCArray[RowCounter] = document.getElementById("hid_COSTCENTREID").value		
			
		AddCell (oRow, RepairName + DATA, RepairNotes, "", "")
		AddCell (oRow, VatTypeCode, VatTypeName + " Rate", "center", "")		
		AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "")
		AddCell (oRow, AnswerCount, "", "right", "")
		AddCell (oRow, FormatCurrencyComma(GrossCost * AnswerCount), "", "right", "")
		DelImage = "<img title='Clicking here will remove this repair from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + GrossCost + "," + AnswerCount + "," + (GrossCost * AnswerCount) + ")\">"
		AddCell (oRow, DelImage, "", "center", "#FFFFFF")

		SetTotals (GrossCost, AnswerCount, GrossCost * AnswerCount)
		ResetData()
		}	

	function ResetData(){
		SetChecking(3)
		checkForm()
		RSLFORM.sel_ITEM.outerHTML = "<select class='textbox200' name='sel_ITEM' style='width:300px' disabled><OPTION VALUE=''>Select Location</option></select>";
		RSLFORM.sel_ELEMENT.outerHTML = "<select class='textbox200' name='sel_ELEMENT' style='width:300px' disabled><OPTION VALUE=''>Select Item</option></select>";
		RSLFORM.sel_REPAIR.outerHTML = "<select class='textbox200' name='sel_REPAIR' style='width:300px' disabled><OPTION VALUE=''>Select Element</option></select>";		
		document.getElementById("txt_NOTES").value = ""
		document.getElementById("txt_PRIORITY").value = ""
		document.getElementById("txt_NETCOST").value = "0.00"
		document.getElementById("txt_GROSSCOST").value = "0.00"
		document.getElementById("chk_RECHARGEABLE").checked = false				
		document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		document.getElementById("sel_LOCATION").selectedIndex = 0;							
		document.getElementById("txt_VAT").value = "0.00";
		document.getElementById("txt_EMPLOYEELIMIT").value = "0.00";	
		document.getElementById("txt_TOTALEXPLEFT").value = "0.00";					
		document.getElementById("hid_EMPLOYEELIMIT").value = "0.00";	
		document.getElementById("hid_EXPENDITUREID").value = "0.00";			
		document.getElementById("hid_TOTALEXPLEFT").value = "0.00";				
		}

	function SetTotals(iNE, iVA, iGC) {
		totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
		totalVAT = parseInt(document.getElementById("hiVA").value) + parseInt(iVA)
		totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

		document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		document.getElementById("hiVA").value = totalVAT
		document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
		
		document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		document.getElementById("iVA").innerHTML = totalVAT
		document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)						
		}

	function SetLimits(ExpLimit, CCLimit){
		Ref = document.getElementsByName("ID_ROW")
		//this part removes the expenditure bits
		ExpID = document.getElementById("hid_EXPENDITUREID").value
		ExpLimit = parseFloat(ExpLimit)
		if (Ref.length) {
			for (i=0; i<Ref.length; i++){
				if (ExpenditureArray[Ref[i].value] == ExpID)
					ExpLimit -= parseFloat(ExpValueArray[Ref[i].value])
				}
			}
		//this part removes the costcentre part.
		CCID = document.getElementById("hid_COSTCENTREID").value
		CCLimit = parseFloat(CCLimit)
		if (Ref.length) {
			for (i=0; i<Ref.length; i++){
				if (CCArray[Ref[i].value] == CCID)
					CCLimit -= parseFloat(ExpValueArray[Ref[i].value])
				}
			}

		document.getElementById("txt_TOTALEXPLEFT").value = FormatCurrencyComma(ExpLimit)
		document.getElementById("hid_TOTALEXPLEFT").value = FormatCurrency(ExpLimit)
		document.getElementById("hid_TOTALCCLEFT").value = FormatCurrency(CCLimit)
		}
				
	function AddCell(iRow, iData, iTitle, iAlign, iColor) {
		oCell = iRow.insertCell()
		oCell.innerHTML = iData
		if (iTitle != "") oCell.title = iTitle
		if (iAlign != "") oCell.style.textAlign = iAlign
		if (iColor != "") oCell.style.backgroundColor = iColor
		}

	function DeleteRow(RowID,NE,VA,GR){
		oTable = document.getElementById("RepairTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "TR" + RowID) {
				oTable.deleteRow(i)
				SetTotals (-NE, -VA, -GR)				
				break;
				}
			}
		if (oTable.rows.length == 1) {
			oRow = oTable.insertRow()
			oRow.id = "EMPTYLINE"
			oCell = oRow.insertCell()
			oCell.colSpan = 7
			oCell.innerHTML = "Please enter a Repair from above"
			oCell.style.textAlign = "center"
			}
		ResetData()
		}								
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onLoad="window.focus()">
<TABLE><TR><TD WIDTH=10></TD><TD>
<table width=744 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
  <tr>
	  <td  height=10 width=92 nowrap><img src="/myImages/Repairs/tab_work_order.gif" width="117" height="20" alt="" border=0 /></td>
      <td  height=10 width=8 nowrap style='border-bottom:1px solid #133e71;'>&nbsp;</td>	  
	  <td width=100% style='border-bottom:1px solid #133e71;' align=right>Block : <b><%=THE_DEV_NAME%></b></td>
  </tr>
</table>
<table width=744 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<FORM NAME=RSLFORM METHOD=POST>		
  <tr>
  	  <td height=370 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71;;border-bottom:1px solid #133e71'>
<div style='overflow:auto;padding:5px;' class='TA'>
<TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0 width=730 STYLE='BORDER:1PX SOLID BLACK;border-bottom:none'>
		<TR bgcolor=#133E71 style='color:white'> 
			<TD colspan=6><b>WORK ORDER INFORMATION</b></TD>
		</TR>
		<TR>
		<TD width=100px nowrap>Repair Type : </td><TD>
			<%=lstNatures%>
		</TD>
		<TD width=50><image src="/js/FVS.gif" name="img_NATURE" width="15px" height="15px" border="0"></TD>
		<TD width=100>Order Date : </TD><TD><input type="text" name="txt_PODATE" class="textbox"  style='width:117px;text-align:left' value="<%=FormatDateTime(now(),2)%>"></TD>
		<TD width=50><image src="/js/FVS.gif" name="img_PODATE" width="15px" height="15px" border="0"></TD>		
		</TR>

		<TR>
		<TD>Supplier : </td><TD>
			<%=lstSuppliers%>
		</TD>
		<TD width=50><image src="/js/FVS.gif" name="img_SUPPLIER" width="15px" height="15px" border="0"></TD>
		<TD title='Expenditure Left'>Your Limit : </TD><TD>  <input type="text" name="txt_EMPLOYEELIMIT" class="textbox"  style='width:117px;text-align:right' readonly value="0.00"></TD>
		</TR>
		<TR>
		<TD></td><TD>
			
		</TD>
		<TD width=50></TD>
		<TD title='Expenditure Left'>Expend. Left : </TD><TD> <input type="text" name="txt_TOTALEXPLEFT" class="textbox"  style='width:117px;text-align:right' readonly value="0.00"></TD>
		</TR>
	</TABLE>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' CELLPADDING=2 CELLSPACING=0 BORDER=0 width=730>	
		<TR bgcolor=#133E71 style='color:white'> 
			<TD colspan=6><b>REPAIR ITEM</b></TD>
		</TR>
		<TR>
		<TD width=100>Location :</td><TD><%=lst_location%></TD>
		<TD width=50><image src="/js/FVS.gif" name="img_LOCATION" width="15px" height="15px" border="0"></TD>
		<TD>Rechargeable :</TD><TD><input type="checkbox" name="chk_RECHARGEABLE" onClick="SetRecharge()" value="1" disabled></TD>
		</TR>
		<TR>
		<TD>Item :</td><TD>
			<select class="textbox200" name="sel_ITEM"  style='width:300px' disabled tabindex=2>
				<OPTION VALUE="">Select Location</option>
			</select></TD>
		<TD><image src="/js/FVS.gif" name="img_ITEM" width="15px" height="15px" border="0"></TD>
		<TD width=100>Patch :</td><TD><input tabindex=-1 type="text" class="textbox200" name="txt_PATCH" style='width:117px;background-color:beige' readonly VALUE="<%=patch%>"></TD>			
		</TR>
		<TR>
		<TD>Element :</td><TD>
			<select class="textbox200" name="sel_ELEMENT" style='width:300px' disabled tabindex=2>
				<OPTION VALUE="">Select Item</option>
			</select>
		</TD>
		<TD><image src="/js/FVS.gif" name="img_ELEMENT" width="15px" height="15px" border="0"></TD>
        <TD>Net Cost � : </TD>
		<TD><input type="text" class="textbox200" name="txt_NETCOST" style='width:117px;text-align:right' VALUE="0.00" onBlur="SetChecking();TotalBoth();ResetVAT()" tabindex=3></TD>
		<TD width=50><image src="/js/FVS.gif" name="img_NETCOST" width="15px" height="15px" border="0"></TD>		
		</TR>
		<TR>
		<TD nowrap>Repair Details :</td><TD>
			<select class="textbox200" name="sel_REPAIR" style='width:300px' disabled tabindex=2>
				<OPTION VALUE="">Select Element</option>
			</select></TD>
		<TD><image src="/js/FVS.gif" name="img_REPAIR" width="15px" height="15px" border="0"></TD>
        <TD>VAT Type : </TD>
		<TD><%=lstVAT%></TD>
		<TD><image src="/js/FVS.gif" name="img_VATTYPE" width="15px" height="15px" border="0"></TD>		
		</TR>
		<TR>
		<TD>Notes :
		</TD><TD rowspan=2><textarea style='OVERFLOW:HIDDEN;width:300px' class='textbox200' rows=3 name="txt_NOTES" <%=DisableAll%> tabindex=2></textarea>
		</TD>	
        <TD rowspan=2><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"></TD>
        <TD>VAT � : </TD>
		<TD><input type="text" class="textbox200" name="txt_VAT" style='width:117px;text-align:right' VALUE="0.00" READONLY tabindex=-1></TD>
		<TD><image src="/js/FVS.gif" name="img_VAT" width="15px" height="15px" border="0"></TD>		
		</TR>
		<TR>
		<TD><!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" --></TD>
        <TD>Total � : </TD>
		<TD valign=top><input type="text" class="textbox200" name="txt_GROSSCOST" style='width:117px;text-align:right' VALUE="0.00" READONLY tabindex=-1></TD>
		<TD valign=top><image src="/js/FVS.gif" name="img_GROSSCOST" width="15px" height="15px" border="0"></TD>		
		</TR>		
		<TR>
		<TD>Priority :</td><TD><input type="text" tabindex=-1 class="textbox200" name="txt_PRIORITY" style='width:300px;background-color:beige' readonly></TD>
		<TD align=right colspan=3>
        <input type="button" value=" RESET " class="RSLButton"  onClick="ResetData()" tabindex=4 name="TheCancelButton" <%=DisableAll%>>
        <input type="button" value=" ADD " class="RSLButton"  onClick="AddRow(-1)" name="TheSaveButton" <%=DisableAll%> tabindex=3>
        <input type="hidden" name="hid_NATURETITLE">
        <input type="hidden" name="DatabaseCost">
		<input type="hidden" name="hid_EMPLOYEELIMIT" value="">		
		<input type="hidden" name="hid_TOTALEXPLEFT" value="">
		<input type="hidden" name="hid_TOTALCCLEFT" value="">
		<input type="hidden" name="hid_COSTCENTREID" value="">												
		<input type="hidden" name="hid_EXPENDITUREID" value="">		
        <input type="hidden" name="hid_BLOCKID" value="<%=BLOCK_ID%>">		
      </TD>
		</TR>
   	</TABLE>
</div>
<div style='overflow:auto;height:200;padding:5px;' class='TA'>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=730 id="RepairTable" slcolor='' hlcolor="steelblue">
	<THEAD>
	<TR bgcolor=#133E71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=left width=415 NOWRAP><b>Repair:</b></TD>
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=90 nowrap><b>Gross Cost (�):</b></TD>
	  <TD width=75 nowrap><b>Quantity:</b></TD>
	  <TD width=80 nowrap><b>Total (�):</b></TD>
	  <TD width=29 nowrap>&nbsp;</TD>	  
	</TR>
	</THEAD>
	<TBODY>
	<TR ID="EMPTYLINE"><TD colspan=7 align="center">Please enter a Repair from above</TD></TR>
	</TBODY>
  </TABLE>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=730>
	<TR bgcolor=#133E71 ALIGN=RIGHT> 
	  <TD height=20 width=455 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=90 nowrap bgcolor=white><b><input type="hidden" id="hiNC" value="0"><div id="iNC">0.00</div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><input type="hidden" id="hiVA" value="0"><div id="iVA">0</div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><input type="hidden" id="hiGC" value="0"><div id="iGC">0.00</div></b></TD>
	  <TD width=29 nowrap bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
</div>
<div style='overflow:auto;padding-left:5px;padding-right:5px;padding-bottom:5px;' class='TA'>	
<TABLE cellspacing=0 cellpadding=2 width=730>
<tr><td align=right><input type=button name="SaveButton" value=" SAVE ORDER " class="RSLButton" onClick="SaveOrder()"  tabindex=4><BR></TD></TR>
</TABLE>
</div>
      </td>
  </tr>
</FORM>
</table>
</td></tr></TABLE>
<IFRAME src="/secureframe.asp" NAME=iRepairsFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>
</BODY>
</HTML>

