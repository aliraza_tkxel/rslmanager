<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
OpenDB()

dev_id = REQUEST("DEVID")
SQL = "SELECT DEVELOPMENTNAME FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & dev_id
Call OpenRs(rsDEV,SQL)
THE_DEV_NAME = rsDEV("DEVELOPMENTNAME")
Call CloseRs(rsDEV)

'OLD SQL PRE 13/2/2005	
'DISTINCT_SQL = "F_COSTCENTRE CC " &_
'		"INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
'		"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
'		"WHERE (CC.ACTIVE = 1) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1)" &_
'		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND " &_
'		"AND CC.COSTCENTREID IN (11,13,20,22,23)"

Call GetCurrentYear()
FY = GetCurrent_YRange

Dim lstApprovedBy
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (13,22,23) " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"


Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:70' tabindex=5")

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)
	

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD_NODEFAULT"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)
	
CloseDB()
	
TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")		
%>
<html>
<head>
    <title>Development Purchase</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<script type="text/javascript" src="/js/preloader.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" src="/js/financial.js"></script>
<script type="text/javascript">
	var FormFields = new Array()
	
	function SetChecking(Type){
		FormFields.length = 0
		if (Type == 1) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
			FormFields[1] = "sel_HEAD|Head|SELECT|Y"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
			FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
			FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
			}
		else if (Type == 2){
			FormFields[0] = "txt_PONAME|Name|TEXT|Y"
			FormFields[1] = "sel_SUPPLIER|Supplier|SELECT|Y"
			FormFields[2] = "txt_PODATE|Order Date|DATE|Y"
			FormFields[3] = "txt_DELDATE|Expected Date|DATE|N"
			FormFields[4] = "txt_PONOTES|Purchase Notes|TEXT|N"
			}
		else if (Type == 3) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|N"
			FormFields[1] = "sel_HEAD|Head|SELECT|N"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|N"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|N"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|N"
			FormFields[6] = "txt_VAT|VAT|CURRENCY|N"
			FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|N"
			}
		}
			
	function PopulateListBox(values, thetext, which){
		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");
		if (which == 2)
			var selectlist = document.forms.THISFORM.sel_HEAD;
		else 
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE;	
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");

			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
	
	function Select_OnchangeFund(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		THISFORM.IACTION.value = "gethead";
		THISFORM.action = "/Finance/ServerSide/GetHeads.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}
	
	function Select_OnchangeHead(){
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		document.getElementById("txt_EMLIMITS").value = "0.00";								
		PopulateListBox("", "Waiting for Data...", 1);	
		THISFORM.IACTION.value = "change";
		THISFORM.action = "/Finance/ServerSide/GetExpenditures.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}

	function SetPurchaseLimits(){
		MatchRow = -1
		if (document.getElementById("AmendButton").style.display == "block"){
			RowData = document.getElementById("UPDATEID").value
			RowArray = RowData.split("||<>||")
			MatchRow = RowArray[0]
			}
				
		eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
		eEMLIMITS = eEMLIMITS.split(";;");

		Ref = document.getElementsByName("ID_ROW")
		//this part removes the expenditure bits
		ExpID = document.getElementById("sel_EXPENDITURE").value
		EXPDeduct = 0
		if (Ref.length) {
			for (i=0; i<Ref.length; i++){
				if (ExpenditureArray[Ref[i].value] == ExpID && Ref[i].value != MatchRow)
					EXPDeduct += parseFloat(ExpValueArray[Ref[i].value])
				}
			}
		document.getElementById("txt_EXPBALANCE").value = FormatCurrency(parseFloat(eAmounts[eIndex])-parseFloat(EXPDeduct));
		document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	

		//this part removes the costcentre part.
		CCID = document.getElementById("sel_COSTCENTRE").value
		CCDeduct = 0
		if (Ref.length) {
			for (i=0; i<Ref.length; i++){
				if (CCArray[Ref[i].value] == CCID && Ref[i].value != MatchRow)
					CCDeduct += parseFloat(ExpValueArray[Ref[i].value])
				}
			}
		document.getElementById("hid_CCBALANCE").value = FormatCurrency(parseFloat(document.getElementById("hid_TOTALCCLEFT").value)-parseFloat(CCDeduct));				
		}	
	var RowCounter = 0
	var ExpenditureArray = new Array()
	var ExpValueArray = new Array()
	var CCArray = new Array()	
	
	function AddRow(insertPos){
		SetChecking(1)			
		if (!checkForm()) return false
		GrossCost = document.getElementById("txt_GROSSCOST").value
		EmployeeLimit = document.getElementById("txt_EMLIMITS").value
		ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value
		SaveStatus = 1

		//MAKE SURE PEOPLE DONT ENTER A NEGATIVE FIGURE...
		if (parseFloat(GrossCost) < 0) {
			alert("You have entered an item with a negative value. Please correct this to continue.")
			return false;
			}
			
		//check the costcentre will not go over...
		//note: have to remove the total of any other entries aswell.
		CCLeft = document.getElementById("hid_CCBALANCE").value
		if ((parseFloat(CCLeft) < parseFloat(GrossCost) )){
			alert("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the Cost Centre budget remaining (�" + FormatCurrencyComma(CCLeft) + ") for the selected item.\nTherefore this item cannot be entered onto the system.");			
			return false;
			}		
			
		//check the expenditure will not go over...
		ExpLeft = document.getElementById("txt_EXPBALANCE").value
		if ((parseFloat(ExpLeft) < parseFloat(GrossCost) )){
			answer = confirm("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget remaining (�" + FormatCurrencyComma(ExpLeft) + ") for the selected item.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");			
			if (!answer) return false
			}

		//check the employee limit						
		if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)){
			SaveStatus = 0
			result = confirm("The item cost is greater than your employee limit (�" + FormatCurrencyComma(EmployeeLimit) + ") for the selected item budget.\nIf you continue then the item will be placed in a queue to be authorised\nby a user who has appropriate limits.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
			if (!result) return false
			}		

		//this part deletes the previous row for amends
		//amends come in with a variable of zero (0) for insertPos
		if (insertPos != -1) {
			sTable = document.getElementById("ItemTable")
			RowData = document.getElementById("UPDATEID").value
			RowArray = RowData.split("||<>||")
			MatchRow = RowArray[0]
			for (i=0; i<sTable.rows.length; i++){
				if (sTable.rows[i].id == "TR" + MatchRow) {
					sTable.deleteRow(i)
					insertPos = i
					SetTotals (-RowArray[1], -RowArray[2], -RowArray[3])				
					break;
					}
				}
			}
			
		RowCounter++
		ItemRef = document.getElementById("txt_ITEMREF").value			
		ItemDesc = document.getElementById("txt_ITEMDESC").value			
		ExpenditureID = document.getElementById("sel_EXPENDITURE").value
		ExpenditureName = document.getElementById("sel_EXPENDITURE").options[document.getElementById("sel_EXPENDITURE").selectedIndex].text					
		NetCost = document.getElementById("txt_NETCOST").value			
		VAT = document.getElementById("txt_VAT").value											
		VatTypeID = document.getElementById("sel_VATTYPE").value			
		VatTypeName = document.getElementById("sel_VATTYPE").options[document.getElementById("sel_VATTYPE").selectedIndex].text					
		VatTypeCode = VatTypeName.substring(0,1).toUpperCase()					

		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows[i].id == "EMPTYLINE") {
				oTable.deleteRow(i)
				break;
				}
			}

		oRow = oTable.insertRow(insertPos)
		oRow.id = "TR" + RowCounter
		oRow.onclick = AmendRow
		oRow.style.cursor = "pointer"
		
		DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" id=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + ExpenditureID + "||<>||" + SaveStatus + "\">"

		//THIS PART ADDS THE EXPENDITURE ID AND VALUE TO ARRAYS, INCASE ANOTHER REPAIR OF THE SAME TYPE IS SELECTED. 
		//SO A TRUE COMPARISON TO THE EXPENDITURE LEFT CAN BE MADE.
		ExpenditureArray[RowCounter] = document.getElementById("sel_EXPENDITURE").value
		ExpValueArray[RowCounter] = GrossCost
		CCArray[RowCounter] = document.getElementById("sel_COSTCENTRE").value
		RefNo ="0"+RowCounter
		if (RowCounter<=9) {
		 RefNo ="0"+RefNo
		} 	
		AddCell (oRow, RefNo, "", "", "",0)
        AddCell (oRow, ItemRef + DATA, ItemDesc, "", "",1)
		AddCell (oRow, ExpenditureName, "", "", "",2)		
		AddCell (oRow, VatTypeCode, VatTypeName + " Rate", "center", "",3)	
		AddCell (oRow, FormatCurrencyComma(NetCost), "", "right", "",4)
		AddCell (oRow, FormatCurrencyComma(VAT), "", "right", "",5)
		AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "",6)
		DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
		AddCell (oRow, DelImage, "", "center", "#FFFFFF",7)

		SetTotals (NetCost, VAT, GrossCost)
		ResetData()
		return true;
		}
	
	function ResetData(){
		SetChecking(3)
		checkForm()
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Please Select a Cost Centre...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		ResetArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_COSTCENTRE")
		for (i=0; i<ResetArray.length; i++)
			document.getElementById(ResetArray[i]).value = ""
		document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		document.getElementById("txt_VAT").value = "0.00";	
		document.getElementById("AmendButton").style.display = "none"		
		document.getElementById("AddButton").style.display = "block"
		}
		
	function AmendRow(){
		event.cancelBubble = true
		Ref = this.id
		RowNumber = Ref.substring(2,Ref.length)
		StoredData = document.getElementById("iDATA" + RowNumber).value
		StoredArray = StoredData.split("||<>||")
		
		ReturnArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST")
		for (i=0; i<ReturnArray.length; i++)
			document.getElementById(ReturnArray[i]).value = StoredArray[i]
		document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
		document.getElementById("AddButton").style.display = "none"
		document.getElementById("AmendButton").style.display = "block"		
	
		THISFORM.IACTION.value = "LOADPREVIOUS";
		THISFORM.action = "/Finance/ServerSide/GetExpenditures.asp?EXPID=" + StoredArray[6];
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}
		
	function SetTotals(iNE, iVA, iGC) {
		totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
		totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
		totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

		document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		document.getElementById("hiVA").value = FormatCurrency(totalVAT)
		document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
		
		document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
		document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)						
		}
		
	function AddCell(iRow, iData, iTitle, iAlign, iColor, iposition)
    {
		oCell = iRow.insertCell(iposition)
		oCell.innerHTML = iData
		if (iTitle != "") oCell.title = iTitle
		if (iAlign != "") oCell.style.textAlign = iAlign
		if (iColor != "") oCell.style.backgroundColor = iColor
		}
	
	function DeleteRow(RowID,NE,VA,GR){
		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "TR" + RowID) {
				oTable.deleteRow(i)
				SetTotals (-NE, -VA, -GR)				
				break;
				}
			}
		if (oTable.rows.length == 1) {
			oRow = oTable.insertRow()
			oRow.id = "EMPTYLINE"
			oCell = oRow.insertCell()
			oCell.colSpan = 7
			oCell.innerHTML = "Please enter an item from above"
			oCell.style.textAlign = "center"
			}
		ResetData()
		}

	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
	
	function SavePurchaseOrder(){
	
            this.disabled = true;
		    var YStart = new Date("<%=YearStartDate%>")
		    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
		    var YEnd = new Date("<%=YearendDate%>")
		    var YStartPlusTime = new Date('<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>')
		    var YEndPlusTime = new Date('<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>')
		   
		    if  ( real_date(THISFORM.txt_PODATE.value) < YStart || real_date(THISFORM.txt_PODATE.value) > YEnd ) 
			{
			    alert("Please enter an Order Date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
			    this.disabled = false;
                return false;                
			}		
		ResetData()
		SetChecking(2)			
		SetChecking(2)			
		if (!checkForm()) { 
		    this.disabled = false;
		    return false;	
        }

		if (parseFloat(document.getElementById("hiGC").value) <= 0) {
		    alert("Please enter some items before creating a purchase order.")
		    this.disabled = false;
			return false;
		}
		
		// MAKE SURE ORDER DATE IS IN CURRENT FISCAL YEAR
		<% if LockdownStatus = "ON" then %>
		    var YStart = new Date("<%=YearStartDate%>")
		    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
		    var YEnd = new Date("<%=YearendDate%>")
		    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
		    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>") 

		    if (!checkForm()) return false;
		    if  ( real_date(THISFORM.txt_PODATE.value) < YStart || real_date(THISFORM.txt_PODATE.value) > YEnd ) {
		        // outside current financial year
		        if ( TodayDate <= YEndPlusTime ) {
		            // inside grace period
		            if ( real_date(THISFORM.txt_PODATE.value) < YStartPlusTime || real_date(THISFORM.txt_PODATE.value) > YEnd) {
		                // outside of last and this financial year
                        alert("Please enter a tax date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
                        return false;
		            } else {
		                // date must be ok?
		            }
		        } else {
		            // we're outside the grace period and also outside this financial year
                    alert("Please enter a tax date for this financial year\n(i.e. between '<%=YearStartDate%>' and '<%=YearendDate%>')");
                    return false;                
			    }
		    }	
		<% end if %>
	
	
		THISFORM.target = "DEV_BOTTOM_FRAME"
		THISFORM.method = "POST"
		THISFORM.action = "/Finance/ServerSide/CreatePurchaseOrder_svr.asp?REDIRECT=1"
		THISFORM.submit()
		window.close()
		}		
</script>
<body bgcolor="#FFFFFF" margintop="0" marginheight="0" topmargin="6" onload="window.focus()"
    style='background: url()'>
    <table>
        <tr>
            <td width="10">
            </td>
            <td>
                <table width="768" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
                    <tr>
                        <td height="10" width="92" nowrap>
                            <img src="/Portfolio/images/tab_development%2Bpurchase.gif" width="167" height="20"
                                alt="" border="0" />
                        </td>
                        <td height="10" width="8" nowrap style='border-bottom: 1px solid #133e71;'>
                            &nbsp;
                        </td>
                        <td width="100%" style='border-bottom: 1px solid #133e71;' align="right">
                            Development : <b>
                                <%=THE_DEV_NAME%></b>
                        </td>
                    </tr>
                </table>
                <form name="THISFORM" id="THISFORM" method="POST">
                <table width="744" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
                    <tr>
                        <td height="370" colspan="3" valign="top" style="border-left: 1px solid #133e71;
                            border-right: 1px solid #133e71; border-bottom: 1px solid #133e71">
                            <div style='overflow: auto; padding: 5px;' class='TA'>
                                <table style='border: 1PX SOLID BLACK; border-bottom: none' cellspacing="0" cellpadding="3"
                                    width="755">
                                    <tr bgcolor="steelblue" style='color: white'>
                                        <td colspan="6">
                                            <b>PURCHASE ORDER INFORMATION</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Order Name:
                                        </td>
                                        <td>
                                            <input name="txt_PONAME" id="txt_PONAME" type="text" class="TEXTBOX200" size="50" maxlength="50"
                                                tabindex="1">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_PONAME" id="img_PONAME">
                                        </td>
                                        <td rowspan="3" valign="top" nowrap>
                                            &nbsp;Purchase Notes :
                                            <br>
                                            <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                                        </td>
                                        <td rowspan="3">
                                            <textarea name="txt_PONOTES" id="txt_PONOTES" cols="55" rows="5" class="TEXTBOX200" tabindex="2" style='width: 305;
                                                overflow: hidden; border: 1px solid #133E71'></textarea>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_PONOTES">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Supplier :
                                        </td>
                                        <td>
                                            <%=lstSuppliers%>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_SUPPLIER">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Order Date :
                                        </td>
                                        <td>
                                            <input name="txt_PODATE" id="txt_PODATE" type="text" class="TEXTBOX200" tabindex="1" size="50" maxlength="10"
                                                value="<%=FormatDateTime(Date,2)%>">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_PODATE">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;Expected Date :
                                        </td>
                                        <td>
                                            <input name="txt_DELDATE" id="txt_DELDATE" type="text" class="TEXTBOX200" tabindex="2" style='width: 305;'
                                                maxlength="10" size="50">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_DELDATE">
                                        </td>
                                    </tr>
                                </table>
                                <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="3" width="755">
                                    <tr bgcolor="steelblue" style='color: white'>
                                        <td colspan="9">
                                            <b>NEW ITEM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Item :
                                        </td>
                                        <td>
                                            <input name="txt_ITEMREF" id="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex="3">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMREF">
                                        </td>
                                        <td>
                &nbsp;Cost Centre :
            </td>
            <td>
                <%=lstCostCentres%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRE" id="img_COSTCENTRE" alt="" />
            </td>
                                        <td>
                &nbsp;Net Cost :
            </td>
            <td>
                <input style="text-align: right" name="txt_NETCOST" id="txt_NETCOST" type="text" class="textbox" maxlength="20"
                    size="11" onblur="TotalBoth();ResetVAT()" onfocus="alignLeft()" tabindex="5" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_NETCOST" id="img_NETCOST" alt="" />
            </td>
        </tr>
        <tr>
            <td rowspan="3" valign="top">
                Notes :
            </td>
            <td rowspan="4">
                <textarea tabindex="3" name="txt_ITEMDESC" id="txt_ITEMDESC" rows="7" cols="10" class="TEXTBOX200" style="overflow: hidden;
                    border: 1px solid #133E71"></textarea>
            </td>
            <td rowspan="3">
                <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMDESC" id="img_ITEMDESC" alt="" />
            </td>
            <td>
                &nbsp;Head :
            </td>
            <td>
                <select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()"
                    tabindex="4">
                    <option value="">Please Select a Cost Centre...</option>
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_HEAD" id="img_HEAD" alt="" />
            </td>
            <td>
                &nbsp;Vat Type :
            </td>
            <td>
                <%=lstVAT%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_VATTYPE" id="img_VATTYPE" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Expenditure :
            </td>
            <td>
                <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()"
                    tabindex="4">
                    <option value="">Please select a Head...</option>
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_EXPENDITURE" id="img_EXPENDITURE" alt="" />
            </td>
            <td>
                &nbsp;VAT :
            </td>
            <td>
                <input style="text-align: right" name="txt_VAT" id="txt_VAT" type="text" class="textbox" maxlength="20"
                    size="11" onblur="TotalBoth()" onfocus="alignLeft()" value="0.00" tabindex="5" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_VAT" id="img_VAT" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Exp Balance :
            </td>
            <td>
                <input type="text" name="txt_EXPBALANCE" id="txt_EXPBALANCE" class="textbox200" tabindex="-1" readonly="readonly" />
            </td>
            <td>
            </td>
            <td>
                &nbsp;Total :
            </td>
            <td>
                <input style="text-align: right" name="txt_GROSSCOST" id="txt_GROSSCOST" type="text" class="textbox"
                    maxlength="20" size="11" readonly="readonly" tabindex="-1" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST" id="img_GROSSCOST" alt="" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                &nbsp;Spend Limit :
            </td>
            <td>
                <input type="text" name="txt_EMLIMITS" id="txt_EMLIMITS" class="textbox200" readonly="readonly" tabindex="-1" />
            </td>
            <td align="right" colspan="3">
                <input type="hidden" name="IACTION" id="IACTION" />
                <input type="hidden" name="UPDATEID" id="UPDATEID" />
                <input type="hidden" name="EXPENDITURE_LEFT_LIST" id="EXPENDITURE_LEFT_LIST" />
                <input type="hidden" name="EMPLOYEE_LIMIT_LIST" id="EMPLOYEE_LIMIT_LIST" />
                <input type="hidden" name="hid_TOTALCCLEFT" id="hid_TOTALCCLEFT" />
                <input type="hidden" name="hid_CCBALANCE" id="hid_CCBALANCE" />
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td nowrap="nowrap">
                            <input type="button" name="ResetButton" id="ResetButton" value=" RESET " title="RESET" class="RSLButton" onclick="ResetData()"
                                tabindex="6" />&nbsp;
                        </td>
                        <td>
                            <input type="button" name="AddButton" id="AddButton" value=" ADD " title="ADD" class="RSLButton" onclick="AddRow(-1)"
                                tabindex="6" />
                        </td>
                        <td>
                            <input type="button" name="AmendButton" id="AmendButton" value=" AMEND " title="AMEND" class="RSLButton" onclick="AddRow(0)"
                                style="display: none" tabindex="6" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
	<div  style='max-height:120px !important; overflow:auto;' class='TA'>
    <table style="border: 1px solid black; behavior: url(/Includes/Tables/tablehl.htc); "
        cellspacing="0" cellpadding="3" width="755" id="ItemTable" slcolor='' hlcolor="silver" >
        <thead>
            <tr bgcolor="#4682b4" align="right" style='color: white'>
                 <td height="20" align="left" width="20" nowrap="nowrap">
                    <b>Ref#:</b>
                </td>
                <td height="20" align="center" width="205" nowrap="nowrap">
                    <b>Item Name:</b>
                </td>
                <td width="200" nowrap="nowrap" align="left">
                    <b>Expenditure:</b>
                </td>
                <td width="40" nowrap="nowrap">
                    <b>Code:</b>
                </td>
                <td width="80" nowrap="nowrap">
                    <b>Net (�):</b>
                </td>
                <td width="75" nowrap="nowrap">
                    <b>VAT (�):</b>
                </td>
                <td width="80" nowrap="nowrap">
                    <b>Gross (�):</b>
                </td>
                <td width="29" nowrap="nowrap">
                    &nbsp;
                </td>
            </tr>
        </thead>
		
        <tbody >
            <tr id="EMPTYLINE" >
                <td colspan="8" align="center">
                    Please enter an item from above
                </td>
            </tr>
        </tbody>
	
    </table>
	</div>
    <table style="border: 1px solid black; border-top: none" cellspacing="0" cellpadding="2"
        width="755">
        <tr bgcolor="#4682b4" align="right">
            <td height="20" width="490" nowrap="nowrap" style="border: none; color: white">
                <b>TOTAL : &nbsp;</b>
            </td>
            <td width="80" nowrap="nowrap" bgcolor="white">
                
                    <input type="hidden" id="hiNC" value="0" /><div id="iNC" style="font-weight:bold">
                        0.00</div>
                
            </td>
            <td width="75" nowrap="nowrap" bgcolor="white">
                
                    <input type="hidden" id="hiVA" value="0" /><div id="iVA" style="font-weight:bold">
                        0.00</div>
                
            </td>
            <td width="80" nowrap="nowrap" bgcolor="white">
               
                    <input type="hidden" id="hiGC" name="hiGC" value="0" /><div id="iGC" style="font-weight:bold">
                        0.00</div>
                
            </td>
            <td width="29" nowrap="nowrap" bgcolor="white">
                &nbsp;
            </td>
        </tr>
    </table>
  
   <br />
    <table cellspacing="0" cellpadding="2" width="755">
        <tr align="right">
            <td width="754" align="right" nowrap="nowrap">
                &nbsp;<input type="button" value=" CREATE ORDER " name="MainSave" id="MainSave" class="RSLButton"
                    onclick="SavePurchaseOrder()" />
            </td>
        </tr>
    </table>
    </form>
            </td>
        </tr>
    </table>
    <iframe src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" id="PURCHASEFRAME<%=TIMESTAMP%>"
        style='display: NONE'></iframe>
</body>
</html>
