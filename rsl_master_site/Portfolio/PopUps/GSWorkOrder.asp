<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	
	Dim topdata,orgid,patchid,developmentid,contractor,WorkOrderNum,WorkOrder,OrderDetailData,TotalOrder,UserId
	
	Function GetQueryString()
	    //This function will get all the Query String variable
	    orgid=Request.QueryString.Item("orgid")
	    patchid=Request.QueryString.Item("patchid")
	    developmentid=Request.QueryString.Item("developmentid")
	    UserId=Session("USERID")
	End Function
	
	Function GetContractor()
	    //This function will get detail of the contractor
		SQL = "SELECT NAME,ADDRESS1,ADDRESS2,ADDRESS3,TOWNCITY,POSTCODE FROM S_ORGANISATION WHERE ORGID=" & orgid & ""
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then
		    
		    contractor="<p class='heading' style='display:inline;'>Contractor:</p>"
		
			contractor = contractor & "<p style='display:inline;padding-left:19px;color:#133e71;'>" & rsSet("NAME") & ",</p>"
			
			if(rsSet("ADDRESS1")<>"") then
			    contractor=contractor & "<p class='contractoraddress'>" & rsSet("ADDRESS1") & "</p>"
			end if
			if(rsSet("ADDRESS2")<>"") then
			    contractor=contractor & "<p class='contractoraddress'>" & rsSet("ADDRESS2") & "</p>"
			end if
			if(rsSet("ADDRESS3")<>"") then
			    contractor=contractor & "<p class='contractoraddress'> " &  rsSet("ADDRESS2") & "</p>"
			end if
			
			contractor=contractor & "<p class='contractoraddress'>" & rsSet("TOWNCITY") & "</p>" 
			contractor=contractor & "<p class='contractoraddress'>" & rsSet("POSTCODE") & "</p>"    
			
		else	
			contractor = "UNKNOWN"
		end if
		CloseRs(rsSet)
	End Function
	
	Function CreateWorkOrder()
	    Dim  dbrec,dbcmd   
	    set dbrec=server.createobject("ADODB.Recordset")
        set dbcmd= server.createobject("ADODB.Command")
        
        dbcmd.CommandTimeout = 120
           
        
        dbrec.CursorType = 3
	    dbrec.CursorLocation = 3
        
        dbcmd.ActiveConnection=RSL_CONNECTION_STRING
        dbcmd.CommandType = 4
	
	    dbcmd.CommandText = "GS_WORKORDER"
        dbcmd.Parameters.Refresh
        
        dbcmd.Parameters(1)=orgid
        dbcmd.Parameters(2)=UserId
        if(patchid="") then
            dbcmd.Parameters(3)=0
        else
            dbcmd.Parameters(3)=patchid
        end if    
        dbcmd.Parameters(4)=developmentid
              
        
        set dbrec=dbcmd.execute
    
        if(not dbrec.eof) then	
            WorkOrderNum="G0000" & dbrec("WORKORDER")
        end if
        
        dbrec.Close()
        dbcmd.Cancel()
        
        Set dbrec = Nothing    
	End Function
	
Function GetContractDetail()
	    
     Dim filter   
	 if (patchid = "") then
	    filter=""
	 else
	    filter=",@patchid=" &  patchid 
	 end if
	
	 if(developmentid<>"") then
	    filter=filter & ",@developmentid=" & developmentid	   
	 end if
	
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
             
	dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
	sql="GS_CONTRACT_DETAIL @ORGID=" & orgid & filter
	
	dbrs.Source = sql
	dbrs.CursorType = 3
	dbrs.CursorLocation = 3
	dbrs.Open()
    
    NoofRecords=dbrs.RecordCount
    
   
    if(not dbrs.eof) then
        OrderDetailData="<table style='border-collapse:collapse;padding:0 0 0 0;margin:0 0 0 0;' cellpadding=2>"
        OrderDetailData= OrderDetailData & "<tr class='heading' style='text-align:center;'>"   
        OrderDetailData= OrderDetailData & "<td class='thinline'>&nbsp;</td>"
        OrderDetailData= OrderDetailData & "<td class='border_left_right_bottom'>Properties</td>"
        OrderDetailData= OrderDetailData & "<td class='thinline'>Fire</td>"
        OrderDetailData= OrderDetailData & "<td class='border_right_bottom'>Water Heater/<br/>Multipoint</td>"
        OrderDetailData= OrderDetailData & "<td class='thinline'>Cooker <br/> Hob</td>"
        OrderDetailData= OrderDetailData & "<td class='border_right_bottom'>Traditional <br/> Boiler</td>"
        OrderDetailData= OrderDetailData & "<td class='thinline'>Combi <br/> Boiler</td>"
        OrderDetailData= OrderDetailData & "<td class='border_right_bottom'>Oil <br/> Boiler</td>"
        OrderDetailData= OrderDetailData & "<td class='border_right_bottom'>Wall <br/> Heater</td>"
        OrderDetailData= OrderDetailData & "<td class='thinline'>Contract Value</td>"
        OrderDetailData= OrderDetailData & "</tr>"
         For intRecord = 1 to dbrs.RecordCount
           
           'IF WE HAVE CHANGED PATCHES WE NEED TO ADD IN THE PATCH SUMMARY LINE
           RecordCount = dbrs.RecordCount
           
           Scheme_PatchID  		= dbrs("PATCHID")
           Scheme_Location		= dbrs("LOCATION")
           Scheme_DevelopmentName	= dbrs("DEVELOPMENTNAME")
           Scheme_PropertyCount 	= dbrs("PROPERTIES")
           Scheme_TraditionalBoilerCount 	= dbrs("NOOFTRADITIONALBOLIER")
           Scheme_TraditionalBoilerCost = dbrs("COSTPERTRADITIONALBOILER")
           Scheme_FireCount	= dbrs("NOOFFIRE")
           Scheme_FireCost 	= dbrs("COSTPERFIRE") 
           Scheme_WaterHeaterMultipointCount 	= dbrs("NOOFWATERHEATER_MULTIPOINT")
           Scheme_WaterHeaterMultipointCost 	= dbrs("COSTPERWATERHEATER_MULTIPOINT")    
           Scheme_CookHobCount 		= dbrs("NOOFCOOKER_HOB")
           Scheme_CookHobCost 		= dbrs("COSTPERCOOKER_HOB")
           Scheme_CombiBoilerCount 	= dbrs("NOOFCOMBIBOILER")
           Scheme_CombiBoilerCost 	= dbrs("COSTPERCOMBIBOILER")
           Scheme_OilBoilerCount 	= dbrs("NOOFOILBOILERS")
           Scheme_OilBoilerCost 	= dbrs("COSTPEROILBOILERS")
           Scheme_WallHeaterCount 	= dbrs("NOOFWALLHEATER") 
           Scheme_WallHeaterCost = dbrs("COSTPERWALLHEATER") 
           Scheme_ContractCost 	= dbrs("CONTRACTVALUE")
             
          
             
                      
           OrderDetailDataDev= OrderDetailDataDev & "<tr style='color:#133e71;padding:10 0 10 0;'>" 
           OrderDetailDataDev= OrderDetailDataDev & "<td class='heading' style='padding-left:14px;'>" &  Scheme_DevelopmentName & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td class='border_left_right' style='text-align:right;'>" &  Scheme_PropertyCount & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td style='text-align:right;'>" &  Scheme_FireCount & " (" & formatcurrency(Scheme_FireCost) & ")" & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td class='border_right' style='text-align:right;'>" & Scheme_WaterHeaterMultipointCount & " (" & formatcurrency(Scheme_WaterHeaterMultipointCost) & ")"  & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td style='text-align:right;'>" &  Scheme_CookHobCount & " (" & formatcurrency(Scheme_CookHobCost) & ")" & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td class='border_right' style='text-align:right;'>" &  Scheme_TraditionalBoilerCount & " (" & formatcurrency(Scheme_TraditionalBoilerCost) &  ")" &  "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td style='text-align:right;'>" &  Scheme_CombiBoilerCount & " (" & formatcurrency(Scheme_CombiBoilerCost) & ")" & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td class='border_right' style='text-align:right;'>" &  Scheme_OilBoilerCount & " (" & formatcurrency(Scheme_OilBoilerCost) & ")" & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td class='border_right' style='text-align:right;'>" &  Scheme_WallHeaterCount & " (" & formatCurrency(Scheme_WallHeaterCost) & ")"  & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "<td style='text-align:right;'>" &  formatcurrency(Scheme_ContractCost) & "</td>"
           OrderDetailDataDev= OrderDetailDataDev & "</tr>"
                    
           if ((cInt(Scheme_PatchID) <> cInt(dbrs("PATCHID")) and Scheme_PatchID <> "") or (intRecord   + 1 > dbrs.RecordCount)) then		  
               
                    OrderDetailData= OrderDetailData & "<tr style='color:#133e71;padding:10 0 10 0;'>" 
                    OrderDetailData= OrderDetailData & "<td class='heading' style='padding-left:4px;'>" &  Scheme_Location & "</td>"
                    OrderDetailData= OrderDetailData & "<td class='border_left_right' style='text-align:right;'>" &  Patch_PropertyCount + Cint(Scheme_PropertyCount) & "</td>"
                    OrderDetailData= OrderDetailData & "<td style='text-align:right;'>" &  Patch_FireCount + Scheme_FireCount & " (" & formatcurrency(Patch_FireCost +	Scheme_FireCost) & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td class='border_right' style='text-align:right;'>" &  Patch_WaterHeaterMultipointCount  + Scheme_WaterHeaterMultipointCount & " (" & formatcurrency(Patch_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost ) & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td style='text-align:right;'>" &  Patch_CookHobCount + Scheme_CookHobCount & " (" & formatcurrency(Patch_CookHobCost + Scheme_CookHobCost) & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td class='border_right' style='text-align:right;'>" &  Patch_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount & " (" & formatcurrency(Patch_TraditionalBoilerCost + Scheme_TraditionalBoilerCost)  & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td style='text-align:right;'>" &  Patch_CombiBoilerCount  + Scheme_CombiBoilerCount & " (" & formatcurrency(Patch_CombiBoilerCost + Scheme_CombiBoilerCost) & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td class='border_right' style='text-align:right;'>" &  Patch_OilBoilerCount + Scheme_OilBoilerCount & " (" & formatCurrency(Patch_OilBoilerCost + Scheme_OilBoilerCost) & ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td class='border_right' style='text-align:right;'>" &  Patch_WallHeaterCount  + Scheme_WallHeaterCount & " (" & formatCurrency(Patch_WallHeaterCost + Scheme_WallHeaterCost) &  ")" & "</td>"
                    OrderDetailData= OrderDetailData & "<td style='text-align:right;'>" &  formatcurrency(Patch_ContractCost + Scheme_ContractCost ) & "</td>"
                    OrderDetailData= OrderDetailData & "</tr>"
                    OrderDetailData= OrderDetailData & OrderDetailDataDev
                    
                     ' now set the dev rows empty for next patch
                     OrderDetailDataDev = ""

                     Patch_PropertyCount 	= 0
                     Patch_TraditionalBoilerCount = 0
                     Patch_TraditionalBoilerCost = 0
                     Patch_FireCount 		= 0
                     Patch_FireCost 		= 0
                     Patch_WaterHeaterMultipointCount = 0
                     Patch_WaterHeaterMultipointCost = 0
                     Patch_CookHobCount		= 0
                     Patch_CookHobCost =0
                     Patch_CombiBoilerCount  	= 0
                     Patch_CombiBoilerCost = 0
                     Patch_OilBoilerCount  	= 0
                     Patch_OilBoilerCost =0	
                     Patch_WallHeaterCount = 0
                     Patch_WallHeaterCost =0
                     Patch_ContractCost 	= 0
                        
            end if
             
            Patch_PropertyCount 	= cInt(Patch_PropertyCount) + Cint(Scheme_PropertyCount)
            Patch_TraditionalBoilerCount  = Patch_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount 
            Patch_TraditionalBoilerCost = Patch_TraditionalBoilerCost + Scheme_TraditionalBoilerCost
            Patch_FireCount	= Patch_FireCount  + Scheme_FireCount
            Patch_FireCost = Patch_FireCost +	Scheme_FireCost
            Patch_WaterHeaterMultipointCount 		= Patch_WaterHeaterMultipointCount + Scheme_WaterHeaterMultipointCount
            Patch_WaterHeaterMultipointCost = Patch_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost 
            Patch_CookHobCount 	= Patch_CookHobCount + Scheme_CookHobCount
            Patch_CookHobCost = Patch_CookHobCost + Scheme_CookHobCost
            Patch_CombiBoilerCount	= Patch_CombiBoilerCount +  Scheme_CombiBoilerCount
            Patch_CombiBoilerCost = Patch_CombiBoilerCost + Scheme_CombiBoilerCost
            Patch_OilBoilerCount = Patch_OilBoilerCount + Scheme_OilBoilerCount
            Patch_OilBoilerCost = Patch_OilBoilerCost + Scheme_OilBoilerCost 			
            Patch_WallHeaterCount	= Patch_WallHeaterCount + Scheme_WallHeaterCount
            Patch_WallHeaterCost = Patch_WallHeaterCost + Scheme_WallHeaterCost
            Patch_ContractCost 	= Patch_ContractCost + Scheme_ContractCost   
            
            Total_PropertyCount	= Total_PropertyCount + Cint(Scheme_PropertyCount)
            Total_TraditionalBoilerCount  = Total_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount 
            Total_TraditionalBoilerCost = Total_TraditionalBoilerCost + Scheme_TraditionalBoilerCost
            Total_FireCount	= Total_FireCount  + Scheme_FireCount
            Total_FireCost = Total_FireCost +	Scheme_FireCost
            Total_WaterHeaterMultipointCount = Total_WaterHeaterMultipointCount + Scheme_WaterHeaterMultipointCount
            Total_WaterHeaterMultipointCost = Total_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost 
            Total_CookHobCount 	= Total_CookHobCount + Scheme_CookHobCount
            Total_CookHobCost = Total_CookHobCost + Scheme_CookHobCost
            Total_CombiBoilerCount	= Total_CombiBoilerCount +  Scheme_CombiBoilerCount
            Total_CombiBoilerCost = Total_CombiBoilerCost + Scheme_CombiBoilerCost
            Total_OilBoilerCount = Total_OilBoilerCount + SchemeOilBoilerCount
            Total_OilBoilerCost = Total_OilBoilerCost + Scheme_OilBoilerCost 			
            Total_WallHeaterCount	= Total_WallHeaterCount + Scheme_WallHeaterCount
            Total_WallHeaterCost = Total_WallHeaterCost + Scheme_WallHeaterCost
            Total_ContractCost 	= Total_ContractCost + Scheme_ContractCost   
             
             
             counter=counter+1       
            dbrs.movenext()
	        If dbrs.EOF Then Exit for
    	Next
        
        OrderDetailData= OrderDetailData & "<tr style='color:#133e71;'>" 
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:center;'>Total</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:right;'>" &  Total_PropertyCount & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_bottom' style='text-align:right;'>" &  Total_FireCount & " (" & formatcurrency(Total_FireCost) & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:right;'>" &  Total_WaterHeaterMultipointCount  & " (" & formatcurrency(Total_WaterHeaterMultipointCost) & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_bottom' style='text-align:right;'>" &  Total_CookHobCount  & " (" & formatcurrency(Total_CookHobCost + Patch_CookHobCost) & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:right;'>" &  Total_TraditionalBoilerCount   & " (" & formatcurrency(Total_TraditionalBoilerCost)  & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_bottom' style='text-align:right;'>" &  Total_CombiBoilerCount   & " (" & formatcurrency(Total_CombiBoilerCost) & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:right;'>" &  Total_OilBoilerCount  & " (" & formatCurrency(Total_OilBoilerCost) & ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_right_bottom' style='text-align:right;'>" &  Total_WallHeaterCount  & " (" & formatCurrency(Total_WallHeaterCost) &  ")" & "</td>"
        OrderDetailData= OrderDetailData & "<td class='border_top_bottom' style='text-align:right;'>" &  formatcurrency(Total_ContractCost ) & "</td>"
        OrderDetailData= OrderDetailData & "</tr>"
        OrderDetailData=OrderDetailData & "</table>"
	    
    else
	    OrderDetailData="No data exist for this contractor"  
    end if
    dbrs.close()
    Set dbrs = Nothing
End Function
	
	OpenDB()
	
	GetQueryString()
	
	GetContractor()
	
	CreateWorkOrder()
	
	GetContractDetail()
	
	WorkOrder="<p class='heading'>WO Number:"
	WorkOrder=WorkOrder & "<span style='padding-left:12px;'>" & WorkOrderNum & "</span></p>"
	
	topdata=WorkOrder & contractor
	
	CloseDB()
	
%>
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Work Order</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL_Non_Logo.css" type="text/css" />
    <style type="text/css" media="screen">
      #top{width:100%;}
      #bottom{width:98%; border:solid #133e71 1px; border-top-width:0px; height:470; vertical-align:top;}  
      #order{width:100%;border:solid #133e71 1px;border-left-width:0px;border-right-width:0px;margin-top:10px;padding:20 0 0 20;}
      #orderdetail{width:100%;border-top:solid #133e71 1px;margin-top:10px;height:300px;overflow:auto;margin-right:0px;}
      .displayInline{display:inline;}
      .tabline{border-bottom:1px solid #133e71; text-align:right;display:inline;width:85%;}
      .contractoraddress{margin:2 0 2 0;padding:0 0 0 86;color:#133e71;}
      .printbutton{margin:0 20 10 0;text-align:right;}
      .heading{font-weight:bold;color:#133e71;}
      .thinline{border-style: solid;border-color:#133e71;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}
      .border_left_right_bottom{border:1px solid #133e71;border-top-width:0px;}
      .border_left_right{border:1px solid #133e71;border-top-width:0px;border-bottom-width:0px;}
      .border_right_bottom{border:1px solid #133e71;border-top-width:0px;border-left-width:0px;}
      .border_right{border-right:1px solid #133e71;}
      .border_top_bottom{border-top:1px solid #133e71;border-bottom:1px solid #133e71;}
      .border_top_right_bottom{border:1px solid #133e71;border-left-width:0px;}
    </style>
    <style type="text/css" media="print">
    #top{display:none;}
      #bottom{width:744;vertical-align:top;height:200px;}  
      #order{display:none;}
      #orderdetail{width:742;border:solid #133e71 1px;border-bottom-width:0px;}
      .displayInline{display:inline;}
      .tabline{border-bottom:1px solid #133e71; text-align:right;display:inline;width:627px;}
      .contractoraddress{margin:2 0 2 0;padding:0 0 0 86;color:#133e71;}
      .printbutton{display:none;}
      .heading{font-weight:bold;color:#133e71;}
      .thinline{border-style: solid;border-color:#133e71;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}
      .border_left_right_bottom{border:1px solid #133e71;border-top-width:0px;}
      .border_left_right{border:1px solid #133e71;border-top-width:0px;border-bottom-width:0px;}
      .border_right_bottom{border:1px solid #133e71;border-top-width:0px;border-left-width:0px;}
      .border_right{border-right:1px solid #133e71;}
      .border_top_bottom{border-top:1px solid #133e71;border-bottom:1px solid #133e71;}
      .border_top_right_bottom{border:1px solid #133e71;border-left-width:0px;}
    </style>    
</head>
<script language="javascript" src="/js/preloader.js"  type="text/javascript"></script>
<script language="javascript" src="/js/general.js" type="text/javascript"></script>
<script language="javascript" src="/js/menu.js" type="text/javascript"></script>
<script language="javascript" src="/js/formvalidation.js" type="text/javascript"></script>
<script language="javascript" src="/js/financial.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function PrintOrder()
{
     window.print();
}

</script>
<body onload="window.focus()" >
<div id="top">
    <p class="displayInline">
    <img src="/myImages/Repairs/tab_work_order.gif" width="117" height="20" alt="" 
    style="margin:0 0 0 0;padding:0 0 0 0;"/></p>
    <p class="tabline">&nbsp;</p>
</div>
<div id="bottom">
    <div id="order">
        <%=topdata%>
        <p class="printbutton"><input type="button" value="PRINT ORDER" class="RSLButton" onclick="PrintOrder();" /></p>
    </div>
    <div id="orderdetail">
        <%=OrderDetailData %>
    </div>
</div>

</body>
</html>

