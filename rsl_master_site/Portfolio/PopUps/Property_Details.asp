<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
PropertyID = Request("PropertyID")
if (PropertyID = "") then
	PropertyID = -1
end if

OpenDB()
SQL = "SELECT  P.PROPERTYID, flatNUMBER, HOUSENUMBER, ADDRESS1, ADDRESS2, ADDRESS3, TOWNCITY, COUNTY, POSTCODE, S.DESCRIPTION AS STATUS, " &_
	"ISNULL(SS.DESCRIPTION, 'N/A') AS SUBSTATUS, ISNULL(A.PARAMETERVALUE,0) AS MAXPEOPLE, " &_
		"ISNULL(MSF.DESCRIPTION, 'N/A') AS SUITABLEFOR " &_
			"FROM P__PROPERTY P " &_
				"LEFT JOIN P_STATUS S ON P.STATUS = S.STATUSID " &_
				"LEFT JOIN P_SUBSTATUS SS ON P.SUBSTATUS = SS.SUBSTATUSID " &_				
                "Left outer Join PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = P.PROPERTYID AND A.ITEMPARAMID = (" &_
									" SELECT ItemParamID from PA_ITEM_PARAMETER" &_
									" INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID" &_
									" Where ParameterName='MAXPEOPLE')" &_
				" LEFT JOIN P_MARKETING M ON M.PROPERTYID = P.PROPERTYID " &_
				" LEFT JOIN P_MOSTSUITABLEFOR MSF ON M.SUITABLEFOR = MSF.MOSTSUITABLEFORID " &_
				" WHERE P.PROPERTYID = '" & PropertyID & "'  order by P.PROPERTYID"
Call OpenRs(rsLoader, SQL)
if (not rsLoader.EOF) then
	propertyid = rsLoader("PROPERTYID")
	housenumber = rsLoader("HOUSENUMBER")
	'FLATNUMBER = rsLoader("FLATNUMBER")
	'IF (FLATNUMBER <> "" AND NOT ISNULL(FLATNUMBER)) THEN
	'	housenumber = FLATNUMBER & ", " & housenumber
	'end if	
	address1 = rsLoader("ADDRESS1")
	address2 = rsLoader("ADDRESS2")
	address3 = rsLoader("ADDRESS3")
	towncity = rsLoader("TOWNCITY")
	county = rsLoader("COUNTY")
	postcode = rsLoader("POSTCODE")
	istatus = rsLoader("STATUS")								
	substatus = rsLoader("SUBSTATUS")									
	maxpeople = rsLoader("MAXPEOPLE")								
	suitablefor = rsLoader("SUITABLEFOR")
end if										
CloseRs(rsLoader)
CloseDB()
%>
<HTML>
<HEAD>
<TITLE>Property Info:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_small_long.gif" width="10" height="22" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71' bgcolor=#133e71 align=center class='RSLWhite'><b>Property Information</b></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71' bgcolor=#133e71><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
<tr>
    <td><b>Property Ref:</b></td>
    <td><%=propertyid%></td></tr>
<tr>
            <td><b>House/Flat No:</b></td>
    <td><%=housenumber%></td></tr>
<tr>
    <td><b>Address 1:</b></td>
    <td><%=address1%></td></tr>
<tr>
    <td><b>Address 2:</b></td>
    <td><%=address2%></td></tr>
<tr>
    <td><b>Address 3:</b></td>
    <td><%=address3%></td></tr>
<tr>
    <td><b>Town / City:</b></td>
    <td><%=towncity%></td></tr>
<tr>
    <td><b>County:</b></td>
    <td><%=county%></td></tr>
<tr>
    <td><b>Postcode:</b></td>
    <td><%=postcode%></td></tr>
<tr>
    <td nowrap><b>Max Occupancy:&nbsp;</b></td>
    <td><%=maxpeople%></td></tr>
<tr>
    <td><b>Suitable For:</b></td>
    <td><%=suitablefor%></td></tr>
<tr>
    <td><b>Status:</b></td>
    <td><%=istatus%></td></tr>
<tr>
    <td><b>Sub - Status:</b></td>
    <td><%=substatus%></td></tr>

</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

