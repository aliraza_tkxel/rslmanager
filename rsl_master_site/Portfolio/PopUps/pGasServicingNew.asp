<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<%
	Dim gas_history_id		' the historical id of the gas record used to get the latest version of this record QUERYSTRING
	Dim nature_id			' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim lst_action			' action of repair reocrd from REQUEST.FORM
	Dim  ContractorName, ContractorID, tenancyid, orgid, username, journalid, appointmentdate, appointmentdatetime, Propertyid, appliancetypeid

	path = request.form("hid_go")
	gas_history_id = Request("gashistoryid")
	nature_id = Request("natureid")
    Propertyid = Request("propertyid")
    appliancetypeid = Request("appliancetypeid")

	' begin processing
	Call OpenDB()
    Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE=" & nature_id ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:198px' onchange='action_change()' ")
	Call get_detail()
	Call CLoseDB()

	Function get_detail()
	    SQL=" SELECT GJ.JOURNALID,-1 AS ORGID,'BRS' AS CONTRACTORNAME,CT.TENANCYID,GJH.APPOINTMENTDATE "&_
            " FROM P_GASJOURNALHISTORY GJH "&_
	        "    INNER JOIN P_GASJOURNAL GJ ON GJ.JOURNALID=GJH.JOURNALID "&_
	        "     LEFT JOIN dbo.C_TENANCY CT ON CT.PROPERTYID=GJ.PROPERTYID AND CT.ENDDATE IS NULL "&_
            " WHERE GJH.GASHISTORYID= " & gas_history_id
 
	    Call OpenRs(rsSet, SQL)
	        ContractorName= rsSet("CONTRACTORNAME")
	        tenancyid=rsSet("TENANCYID")
	        orgid=rsSet("ORGID")
	        appointmentdatetime=rsSet("APPOINTMENTDATE")
            journalid = rsSet("JOURNALID")

	        If(appointmentdatetime = "" Or isnull(appointmentdatetime) = true) Then
	            appointmentdatetime = GetAppointmentDateTime(journalid)
	        End If
            If(appointmentdatetime <> "") Then
	            appointmentdate=FormatDateTime(appointmentdatetime,2)
	        End If

	    Call CloseRs(rsSet)

	End Function

    Function GetAppointmentDateTime(journalid)

   SQL= " SELECT TOP 1 APPOINTMENTDATE "&_
        " FROM P_GASJOURNALHISTORY "&_
        " WHERE JOURNALID= " & journalid &_
        "    AND APPOINTMENTDATE IS NOT NULL "&_
        " ORDER BY GASHISTORYID DESC "

			Call OpenRs(rsSet, SQL)
			If(Not rsSet.eof) Then
		        GetAppointmentDateTime = rsSet("APPOINTMENTDATE")
		    Else
		        GetAppointmentDateTime = ""
			End If
			Call CloseRs(rsSet)

	End Function

	username = Session("FirstName") & "," & Session("LastName")
%>
<html>
<head>
    <title>Update Repair</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .elementlabeldisabled
        {
            color: gray;
        }
        .elementlabel
        {
            color: black;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">
    <!--
        var FormFields = new Array();
	    FormFields[0] = "sel_ITEMACTIONID|Action|SELECT|Y"
	    FormFields[1] = "sel_LETTER|LETTER|SELECT|N"
        FormFields[0] = "txt_CP12NUMBER|CP12 Number|TEXT|N"
	    FormFields[2] = "txt_ISSUEDATE|Issue Date|DATE|N"

        function action_change() {

            document.getElementById("col_document").style.display=''; 
            document.getElementById("col_date").style.display=''; 
            document.getElementById("col_time").style.display=''; 
            document.getElementById("col_actionby").style.display=''; 
            document.getElementById("col_cp12number").style.display='none'
            document.getElementById("col_issuedate").style.display='none'
            document.getElementById("col_cp12file").style.display='none'

	        if(RSLFORM.sel_ITEMACTIONID.value == "108") {
		        document.RSLFORM.txt_APPOINTMENTDATE.disabled=true;
		        document.RSLFORM.sel_HOUR.disabled=true;
		        document.RSLFORM.sel_MINUTES.disabled=true;
		        document.RSLFORM.sel_MARADIAM.disabled=true;
		        document.RSLFORM.btn_view_letter.disabled=true;
		        document.getElementById("lbl_APPOINTMENTDATE").className ="elementlabeldisabled";
		        document.getElementById("lbl_time").className ="elementlabeldisabled";
		        document.RSLFORM.txt_NOTES.disabled=true;
		        document.getElementById("lbl_NOTES").className ="elementlabeldisabled";	
	        }
            else if(RSLFORM.sel_ITEMACTIONID.value == "109") {
                document.getElementById("col_document").style.display='none'; 
                document.getElementById("col_date").style.display='none'; 
                document.getElementById("col_time").style.display='none'; 
                document.getElementById("col_actionby").style.display='none'; 
                document.RSLFORM.btn_view_letter.disabled=true;        
                document.getElementById("col_cp12number").style.display=''
                document.getElementById("col_issuedate").style.display=''
                document.getElementById("col_cp12file").style.display=''              
            }
            else if(RSLFORM.sel_ITEMACTIONID.value == "105" || RSLFORM.sel_ITEMACTIONID.value == "106") {
                document.RSLFORM.txt_APPOINTMENTDATE.disabled=true;
		        document.RSLFORM.sel_HOUR.disabled=true;
		        document.RSLFORM.sel_MINUTES.disabled=true;
		        document.RSLFORM.sel_MARADIAM.disabled=true;
		        document.getElementById("lbl_APPOINTMENTDATE").className ="elementlabeldisabled";
		        document.getElementById("lbl_time").className ="elementlabeldisabled";
            }
	        else {
		        document.RSLFORM.txt_APPOINTMENTDATE.disabled=false;
		        document.RSLFORM.sel_HOUR.disabled=false;
		        document.RSLFORM.sel_MINUTES.disabled=false;
		        document.RSLFORM.sel_MARADIAM.disabled=false;
		        document.RSLFORM.btn_view_letter.disabled=false;
		        document.getElementById("lbl_APPOINTMENTDATE").className ="elementlabel";
		        document.getElementById("lbl_time").className ="elementlabel";
	        }	
	        document.RSLFORM.action = "../Serverside/GetLetterAction.asp"
	        document.RSLFORM.target = "ServerFrame"
	        document.RSLFORM.submit()
	        return;
        }   
	
        function save_form()
        {
	        if(RSLFORM.txt_APPOINTMENTDATE.value == "" && (RSLFORM.sel_ITEMACTIONID.value == "107" || RSLFORM.sel_ITEMACTIONID.value == "104"))
		    {
			    alert("Please enter the appointment date.");
			    return false;
		    } 
            else if(RSLFORM.sel_ITEMACTIONID.value == "109" && RSLFORM.txt_ISSUEDATE.value=="")
            {
                alert("Please enter the issue date.");
			    return false;
            }
               
	        if(RSLFORM.sel_ITEMACTIONID.value != "108" && RSLFORM.sel_ITEMACTIONID.value != "109") 
	        {
	            FormFields[1] = "sel_LETTER|LETTER|SELECT|Y"	
	        }				
	        else
	        {   
	            FormFields[1] = "sel_LETTER|LETTER|SELECT|N"		
	        }	
	        if (!checkForm()) return false;
	
	        var AppointmentTime = RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;        
	        document.RSLFORM.btn_submit.disabled=true;
	        document.RSLFORM.btn_view_letter.disabled=true;        
	        document.RSLFORM.hid_APPOINTMENTTIME.value=AppointmentTime;
	        document.RSLFORM.target = "ServerFrame";
	        document.RSLFORM.action = "../Serverside/SaveLettersNew.asp";
	        document.RSLFORM.submit();
        }
	
	    function open_letter()
        {	
	        var AppointmentTime = RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;

    	    if (RSLFORM.sel_LETTER.value == "") 
	        {
    		    alert("You must first select a letter to view");
		        return false;
	        }
			
	        var tenancy_id = "<%=tenancyid%>"
	        window.open("pView_letter.asp?tenancyid="+tenancy_id+"&letterid="+RSLFORM.sel_LETTER.value+"&DATE1="+ RSLFORM.txt_APPOINTMENTDATE.value+" "+AppointmentTime+"&DATE2=<%=appointmentdatetime%>", "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
        }
	
        function refreshParent() 
        {
            window.opener.location.href = window.opener.location.href;
            if (window.opener.progressWindow)
	        {
	            window.opener.parent.open_me(47,<%=Journalid%>,"iServicingDetail.asp")
	            window.opener.reload();
            }
            window.close();
        }
 
       function Add_CP12()
       {
	        var doctype
		    document.forms[0].encoding = "multipart/form-data";      
		    document.RSLFORM.target = "_self";
		    document.RSLFORM.action = "/DBFileUpload/uploader.aspx";
            document.RSLFORM.keycriteria.value="PROPERTYID='<%=Propertyid%>'";
		    doctype=RSLFORM.rslfile.value;

		    if(doctype.length>0) {
                RSLFORM.updatefields.value=" DOCUMENTTYPE='"+ doctype.substring(doctype.length-3)+"'"; 
		    }
            else {
                alert("You must first selected the CP12 document to upload");
		        return false;
            }
	        document.RSLFORM.submit();	
	        return;
        }	
     //-->   	
    </script>
</head>
<body onload="window.focus()">
    <div id='Calendar1' style='background-color: white; position: absolute; margin-right: 100px;
        width: 200px; height: 109px; z-index: 20; visibility: hidden'>
    </div>
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td width="10" height="10">
                <img src="/Customer/Images/tab_update.gif" width="74" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="3" valign="top" style='border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71; padding-left: 10px;'>
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contractor
                        </td>
                        <td>
                            <input type="text" class="textbox200" readonly="readonly" name="PreviousContractor"
                                id="PreviousContractor" value="<%=ContractorName%>" />
                            <input type="hidden" name="PreviousContractorID" id="PreviousContractorID" value="<%=ContractorID%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action
                        </td>
                        <td>
                            <%=lstAction%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_document">
                        <td>
                            <label for="sel_LETTER" style="vertical-align: top;">
                                Document:</label>
                        </td>
                        <td>
                            <div id="dvLetter">
                                <select name="sel_LETTER" id="sel_LETTER" class="textbox200">
                                    <option value="">Please select action</option>
                                </select></div>
                        </td>
                        <td>
                            <img alt="" src="/js/FVS.gif" name="img_LETTER" id="img_LETTER" width="15px" height="15px"
                                border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_date">
                        <td>
                            <label for="txt_APPOINTMENTDATE" id="lbl_APPOINTMENTDATE">
                                Appointment:</label>
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_APPOINTMENTDATE" id="txt_APPOINTMENTDATE"
                                value="" />
                        </td>
                        <td>
                            <span style="margin: 5 0 0 0;"><a href="javascript:YY_Calendar('txt_APPOINTMENTDATE',190,90,'de','#FFFFFF','#133E71','YY_calendar1');">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" style="border-width: 0;" /></a>
                            </span>
                            <img alt="" src="/js/FVS.gif" name="img_APPOINTMENTDATE" id="img_APPOINTMENTDATE"
                                width="15px" height="15px" border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_time">
                        <td>
                            <label for="sel_HOUR" id="lbl_time">
                                Time:</label>
                        </td>
                        <td>
                            <select name="sel_HOUR" id="sel_HOUR" class="textbox100" style="width: 50px;">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8" selected="selected">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <select name="sel_MINUTES" id="sel_MINUTES" class="textbox100" style="width: 50px;">
                                <option value="15">00</option>
                                <option value="15">15</option>
                                <option value="15">30</option>
                                <option value="15">45</option>
                            </select>
                            <select name="sel_MARADIAM" id="sel_MARADIAM" class="textbox100" style="width: 50px;">
                                <option value="1">AM</option>
                                <option value="2">PM</option>
                            </select>
                        </td>
                    </tr>
                    <tr id="col_actionby">
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=username%>"
                                readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_cp12number" style="display: none;">
                        <td nowrap="nowrap">
                            <label for="txt_CP12NUMBER" style="vertical-align: top;">
                                LGSR Number:</label>
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_CP12NUMBER" id="txt_CP12NUMBER" value="" />
                        </td>
                        <td>
                            <img alt="" src="/js/FVS.gif" name="img_CP12NUMBER" id="img_CP12NUMBER" width="15px"
                                height="15px" border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_issuedate" style="display: none;">
                        <td>
                            <label for="txt_ISSUEDATE" style="vertical-align: top;">
                                Issue Date:</label>
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_ISSUEDATE" id="txt_ISSUEDATE" value="" />
                        </td>
                        <td>
                            <span style="margin: 5 0 0 0;"><a href="javascript:YY_Calendar('txt_ISSUEDATE',190,90,'de','#FFFFFF','#133E71','YY_calendar1');"
                                class="iagManagerSmallBlue" tabindex="1">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" style="border-width: 0;" /></a>
                            </span>
                            <img alt="" src="/js/FVS.gif" name="img_ISSUEDATE" id="img_ISSUEDATE" width="15px"
                                height="15px" border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="col_cp12file" style="display: none;">
                        <td>
                            <label for="rslfile" style="vertical-align: top;">
                                Certificate:</label>
                        </td>
                        <td colspan="2">
                            <input type="file" class="textbox200" style="width: 250px;" name="rslfile" id="rslfile" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <label for="txt_NOTES" id="lbl_NOTES">
                                Notes:</label>
                        </td>
                        <td>
                            <textarea style="overflow: hidden" class="textbox200" name="txt_NOTES" id="txt_NOTES"
                                rows="4" cols="1"></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_GASHISTORYID" id="hid_GASHISTORYID" value="<%=gas_history_id %>" />
                            <input type="hidden" name="hid_ORGID" id="hid_ORGID" value="<%=orgid%>" />
                            <input type="hidden" name="hid_APPOINTMENTTIME" id="hid_APPOINTMENTTIME" value="" />
                            <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                            <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="border-bottom: 1px solid #133e71; border-left: 1px solid #133e71">
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="button" name="btn_view_letter" id="btn_view_letter" onclick="open_letter()"
                    value="View Letter" class="RSLButton" />
                <input type="button" name="btn_close" id="btn_close" onclick="refreshParent()" value=" Close "
                    class="RSLButton" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    class="RSLButton" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133E71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" name="error_message" id="error_message" />
    <input type="hidden" name="hid_Propertyid" id="hid_Propertyid" value="<%=Propertyid %>" />
    <input type="hidden" name="keycriteria" id="keycriteria" value="" />
    <input type="hidden" name="updatefields" id="updatefields" value="" />
    <input type="hidden" name="filefield" id="filefield" value="CP12DOCUMENT" />
    <input type="hidden" name="tablename" id="tablename" value="GS_PROPERTY_APPLIANCE" />
    <input type="hidden" name="connectionstring" id="connectionstring" value="connRSL" />
    <input type="hidden" name="hid_appliancetypeid" id="hid_appliancetypeid" value="" />
    </form>
    <iframe name="ServerFrame" id="ServerFrame" src="/dummy.asp" style="display: none">
    </iframe>
</body>
</html>
