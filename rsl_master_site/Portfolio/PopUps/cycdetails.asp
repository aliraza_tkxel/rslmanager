<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim blockid, repairid, wheresql, developmentid, repairsql, commence_date, start_date, isblock, ispropertyspecific
	Dim str_what, cnt, str_table
	
	blockid 		= Request("BLOCK")
	developmentid 	= Request("DEVELOPMENT")
	repairid 		= Request("REPAIR")
	commence_date	= Request("COMMENCE")	
	start_date		= Request("START")
	
	'response.write "<BR> BLOCKID : " & blockid
	'response.write "<BR> DEVELPMENTID : " & developmentid
	'response.write "<BR> REPAIRID : " & repairid
	'response.write "<BR> COMMENCE : " & commence_date
	'response.write "<BR> START : " & start_date
	
	if block = "" Then 
		wheresql = " WHERE P.DEVELOPMENTID = " & developmentid & " AND P.STATUS NOT IN (6,8,9) "
	else
		isblock = 1
		wheresql = " WHERE P.BLOCKID = " & blockid & " AND P.STATUS NOT IN (6,8,9) "
	end if
	
	OpenDB()
	
	// FIND OUT WHETHER OR NOT REPAIR IS PROPERTY SPECIFIC, IF SO THEN INCLUDE IN SQL
	SQL = "SELECT * FROM R_ITEMDETAIL WHERE ISPROPERTYSPECIFIC = 1 AND ITEMDETAILID = " & repairid
	Call OpenRs(rsSet, SQL)
	if not rsSet.EOF Then
		ispropertyspecific = 1
		repairsql = "INNER JOIN P_ANNUALSERVICINGS A ON A.PROPERTYID = P.PROPERTYID AND REPAIRID = " & repairid
	else 	
		repairsql = ""
	end if
	CloseRs(rsSet)
	
	SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & repairsql & wheresql & " ORDER BY P.HOUSENUMBER "
	
	'response.write "<BR>" & SQL
	Call OpenRs(rsSet, SQL)
	
	cnt = 0
	str_table = "<TR>"
	While Not rsSet.EOF 
	
		str_table = str_table & "<TD WIDTH=100PX>" & rsSet("PROPERTYID") & "</TD><TD>" & rsSet("ADDRESS") & "</TD></TR>"
		cnt = cnt + 1
		rsSet.movenext()
	
	Wend
	CloseRs(rsSet)
	CloseDB()
	
	str_what = build_decsriptive_string()
	
	// GET INFORMATION STRING
	Function build_decsriptive_string()
		
		Dim this_string
		if isblock = 1 then 
			this_string = "block wide"
		else 
			this_string = "scheme wide"
		end if	
		if ispropertyspecific = 1 then
			this_string = this_string & " and property specific"
		end if
		
		build_decsriptive_string = this_string
		Exit Function 
			
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio --> Check Cyclical Repairs</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function return_focus(bool){
	
		window.returnValue = bool
		window.close()
	
	}

</SCRIPT>

<BODY class='ta' BGCOLOR=#FFFFFF onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
		<tr>
			<td colspan=2>The repair you have chosen is <b><%=str_what%></b> and will be attached to the following properties.
				The repairs will appear on the system on <B><%=commence_date%></b> and are scheduled to begin on <b><%=start_date%></b>
				. Please check that this information is correct. If so then press submit, if the information is not correct then
				click cancel and amend the appropriate data. 
			</td>
		</tr>
	</table>
	<BR>
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr><td WIDTH=100PX><b>Property Ref</b></td><td><b>Address</b></td><tr>
		<%=str_table%>
	</table>
	<BR>
	<table width=95% align=center>
	<tr>
		<td colspan=2 align=right>
		<input type=button class="RSLButton" style='width:70px' title='Cancel this cyclical repair' value=" Cancel " onclick="return_focus(false)">
		<input type=button class="RSLButton" style='width:70px' title='Submit this cyclical repair' value=" Submit " onclick="return_focus(true)">
		</td>
	<tr>
	</table>

<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_cyclrep width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

