<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim DevID,FY,searchsubSQL,rsSetYR
DevID 	 = 	Request("DevID")
DevName	 = 	Request("DevName")
ListType =  Request("ListType")
FY       =  Request("FY")

    searchsubSQL=""

    OpenDB()
    if FY<>"" Then
    	Call OpenRS(rsSetYR, "SELECT CONVERT(VARCHAR, YSTART, 103) AS YSTART, CONVERT(VARCHAR, YEND, 103) AS YEND FROM F_FISCALYEARS WHERE YRANGE=" & FY)
        searchsubSQL=" AND ISNULL(ISNULL(PIT.PAIDON,PIT.PIDATE),PIT.PICREATED) >= CONVERT(SMALLDATETIME,'" & rsSetYR("YSTART") & "')"
        searchsubSQL=searchsubSQL & " AND ISNULL(ISNULL(PIT.PAIDON,PIT.PIDATE),PIT.PICREATED) <= CONVERT(SMALLDATETIME,'" & rsSetYR("YEND") & "')"
    end if
    
 
	Select Case ListType
	
			case "DEV_REPAIR"
			' SQL FOR DEV REPAIRS		
			strSQL = "SELECT 	PO.ORDERID  as theorder,  " &_
					"	PIT.ITEMNAME AS THEITEM,  " &_
					"	SUM(PIT.GROSSCOST)AS THECOST,  " &_
					"	POS.POSTATUSNAME AS THESTATUS,  " &_
					"	PIT.PAIDON,  " &_
					"	ORG.NAME , D.SCHEMENAME AS SCHEME   " &_
					"FROM F_PURCHASEITEM  PIT  " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID  " &_
					"	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID   " &_
					"	INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID    " &_
					"	INNER JOIN P_WORKORDER WO ON PIT.ORDERID = WO.ORDERID  " &_
					"	INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID  " &_
					"	INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PIT.PISTATUS  " &_
					"	INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID  " &_
					"WHERE 	WO.DEVELOPMENTID = " & DevID & " AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) AND WO.PROPERTYID is null " & searchsubSQL &_
					"GROUP   BY PO.ORDERID , PIT.ITEMNAME, POS.POSTATUSNAME, PIT.PAIDON, ORG.NAME, D.SCHEMENAME  " &_
					"ORDER 	BY PO.ORDERID ASC , PIT.PAIDON DESC, PIT.ITEMNAME ASC  " 			
			theTypeTitle = "Results for Development: Repairs "
			case "DEV_NONREPAIR"
			' SQL for dev non repairs
			strSQL = "SELECT 	PO.ORDERID  as theorder,  " &_
					"	PIT.ITEMNAME AS THEITEM,  " &_
					"	SUM(PIT.GROSSCOST)AS THECOST,  " &_
					"	POS.POSTATUSNAME AS THESTATUS,  " &_
					"	PIT.PAIDON,  " &_
					"	ORG.NAME  , D.SCHEMENAME AS SCHEME  " &_
					"FROM F_PURCHASEITEM  PIT  " &_
					"	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID   " &_
					"	INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID    " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID  " &_
					"	INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID  " &_
					"	INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PIT.PISTATUS  " &_
					"	INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID  " &_
					"WHERE 	PO.DEVELOPMENTID = " & DevID & " AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) AND  PO.POTYPE = 1  " & searchsubSQL &_
					"GROUP   BY PO.ORDERID , PIT.ITEMNAME, POS.POSTATUSNAME, PIT.PAIDON, ORG.NAME, D.SCHEMENAME  " &_
					"ORDER 	BY PO.ORDERID ASC , PIT.PAIDON DESC, PIT.ITEMNAME ASC  " 
					theTypeTitle = "Results for Development: Non - Repairs "
			case "BLOCK_REPAIR"
			' SQL FOR BLOCK REPAIRS
			strSQL = "SELECT 	PO.ORDERID  as theorder,  " &_
					"	PIT.ITEMNAME AS THEITEM, " &_
					"	SUM(PIT.GROSSCOST)AS THECOST, " &_
					"	POS.POSTATUSNAME AS THESTATUS, " &_
					"	PIT.PAIDON, " &_
					"	ORG.NAME , D.SCHEMENAME AS SCHEME  " &_
					"FROM F_PURCHASEITEM  PIT " &_
					"	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
					"	INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID  " &_ 
					"	INNER JOIN P_WORKORDER WO ON PIT.ORDERID = WO.ORDERID " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
					"	INNER JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
					"	INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
					"	INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PIT.PISTATUS " &_
					"	INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID " &_
					"WHERE 	B.DEVELOPMENTID = " & DevID & " AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
					"GROUP   BY PO.ORDERID , PIT.ITEMNAME, POS.POSTATUSNAME, PIT.PAIDON, ORG.NAME , D.SCHEMENAME " &_
					"ORDER 	BY PO.ORDERID ASC , PIT.PAIDON DESC, PIT.ITEMNAME ASC " 
					theTypeTitle = "Results for Block: Repairs "
			case "BLOCK_NONREPAIR"
			' SQL BLOCK NON REPAIRS
			strSQL = "SELECT 	PO.ORDERID  as theorder,  " &_
					"	PIT.ITEMNAME AS THEITEM, " &_
					"	SUM(PIT.GROSSCOST)AS THECOST, " &_
					"	POS.POSTATUSNAME AS THESTATUS, " &_
					"	PIT.PAIDON, " &_
					"	ORG.NAME , D.SCHEMENAME AS SCHEME  " &_
					"FROM F_PURCHASEITEM  PIT " &_
					"	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
					"	INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID " &_
					"	INNER JOIN P_BLOCK B ON B.BLOCKID = PO.BLOCKID " &_
					"	INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
					"	INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PIT.PISTATUS " &_
					"	INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID " &_
					"WHERE 	B.DEVELOPMENTID = " & DevID & " AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) AND  PO.POTYPE = 1 " & searchsubSQL &_
					"GROUP   BY PO.ORDERID , PIT.ITEMNAME, POS.POSTATUSNAME, PIT.PAIDON, ORG.NAME, D.SCHEMENAME " &_
					"ORDER 	BY PO.ORDERID ASC , PIT.PAIDON DESC, PIT.ITEMNAME ASC " 
					theTypeTitle = "Results for Block: Non-Repairs "
			case "PROP_REPAIR"
			'PROPERTY REPAIRS
			strSQL = "SELECT 	PO.ORDERID as theorder ,  " &_
					"	PIT.ITEMNAME AS THEITEM,  " &_
					"	SUM(PIT.GROSSCOST)AS THECOST,  " &_
					"	POS.POSTATUSNAME AS THESTATUS,  " &_
					"	PIT.PAIDON,  " &_
					"	ORG.NAME, D.SCHEMENAME AS SCHEME  " &_
					"FROM F_PURCHASEITEM  PIT  " &_
					"	INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID   " &_
					"	INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID    " &_
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID  " &_
					"	INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID  " &_
					"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID  " &_
					"	INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID  " &_
					"	INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PIT.PISTATUS  " &_
					"	INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID   " &_
					"WHERE 	P.DEVELOPMENTID = " & DevID & " AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22)  " & searchsubSQL &_
					"GROUP   BY PO.ORDERID , PIT.ITEMNAME, POS.POSTATUSNAME, PIT.PAIDON, ORG.NAME, D.SCHEMENAME  " &_
					"ORDER 	BY PO.ORDERID ASC , PIT.PAIDON DESC, PIT.ITEMNAME ASC "
					theTypeTitle = "Results for Property: Repairs "
			End Select
' Open original recordset

IF STRSQL <> "" THEN
	Call OpenRs(sprocScheme, strSQL)
	if not sprocScheme.eof then 
		while not sprocScheme.eof 
		
		THEORDER 		= sprocScheme("THEORDER")
		THEITEM 		= sprocScheme("THEITEM")
		THEGROSS 		= sprocScheme("THECOST")
		thescheme 		= sprocScheme("SCHEME")
		THESTATUS 		= sprocScheme("THESTATUS")
		PAIDON 			= sprocScheme("PAIDON")
		Supplier		= sprocScheme("NAME")
		
		STRrow  =  	STRrow & "<tr>" &_
							"  <td align='left' bgcolor='#F5F5F5'>" & PurchaseNumber(THEORDER) & "</td>" &_
							"  <td align='left' >" & THEITEM & "</td>" &_
							"  <td align='right' >" & FormatCurrency(THEGROSS,2) & "</td>" &_
							"  <td align='right'>" & THESTATUS & "</td>" &_
							"  <td align='right' >" & PAIDON & "</td>" &_
							"  <td align='right' >" & Supplier & "</td>" &_
							"</tr>"
							
		bottom_THEGROSS	= bottom_THEGROSS + THEGROSS
		sprocScheme.MoveNext()
		Wend
		if 	STRrow = "" then 
		STRrow  =  	STRrow & "<tr>" &_
							"  <td align='left' colspan=6 bgcolor='#F5F5F5'>No records for this option</td>" &_
							"</tr>"
		END IF
		ELSE
		if 	STRrow = "" then 
		STRrow  =  	STRrow & "<tr>" &_
							"  <td align='left' colspan=6 bgcolor='#F5F5F5'>No records for this choice</td>" &_
							"</tr>"
		END IF
	End If
	CloseRs(sprocScheme)
ELSE
		if 	STRrow = "" then 
		STRrow  =  	STRrow & "<tr>" &_
							"  <td align='left' colspan=6 bgcolor='#F5F5F5'>Please select an option at the top of the page</td>" &_
							"</tr>"
		END IF
		
eND iF
CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Finance - Fund Report</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--	
.style1 {
	color: #FFFFFF;
	font-weight: bold;
}
body {
	margin-left: 0px;
	margin-top: 0px;
}
.style2 {
	color: #008000;
	font-weight: bold;
}
.style3 {
	color: #000099;
	font-weight: bold;
}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<script language="javascript" >

function SubmitOption(theValue){
		RSLFORM.ListType.value = theValue
		RSLFORM.DevName.value = "<%=DevName%>"
		RSLFORM.DevID.value = "<%=DevID%>"
		RSLFORM.FY.value = "<%=FY%>"
		RSLFORM.action = ""
		RSLFORM.submit ()
}
</script>
<BODY BGCOLOR=#FFFFFF >
<form name="RSLFORM" method="post">
      <table width="605" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td><table width="600" border="0" align=center cellpadding="0" cellspacing="0">
            <tr class="RSLBlack">
              <td colspan="2" valign="top">&nbsp;</td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top">Scheme : <strong><%=DevName%></strong></td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top">&nbsp;</td>
            </tr>
            <tr valign="middle" class="RSLBlack">
              <td width="3%"><img src="../../myImages/Repairs/ic_scheme.gif" width="20" height="18"> </td>
              <td width="97%">To view the <span class="style2">repair orders</span> that have been assign <strong>to the scheme</strong> <a class="style2" style="cursor:hand; text-decoration:underline " onClick="SubmitOption('DEV_REPAIR')" >click here</a> <input type="hidden" name="ListType">
              <input type="hidden" name="DevName"> <input type="hidden" name="DevID"><input type="hidden" name="FY"></td>
            </tr>
            <tr valign="middle" class="RSLBlack">
              <td><img src="../../myImages/Repairs/ic_scheme.gif" width="20" height="18"> </td>
              <td>To view the <span class="style3">non-repair</span> orders that have been assign<strong> to the scheme</strong> <a class="style3"  style="cursor:hand; text-decoration:underline " onClick="SubmitOption('DEV_NONREPAIR')">click here</a></td>
            </tr>
            <tr valign="middle" class="RSLBlack">
              <td><img src="../../myImages/Repairs/ic_block.gif" width="20" height="18"></td>
              <td> To view the <span class="style2">repair orders</span> that have been assign<strong> to blocks</strong> in this scheme <a class="style2"  style="cursor:hand; text-decoration:underline " onClick="SubmitOption('BLOCK_REPAIR')">click here</a></td>
            </tr>
            <tr valign="middle" class="RSLBlack">
              <td><img src="../../myImages/Repairs/ic_block.gif" width="20" height="18"> </td>
              <td>To view the<span class="style3"> non-repair</span> orders that have been assign <strong>to blocks</strong> in this scheme <a class="style3"  style="cursor:hand; text-decoration:underline " onClick="SubmitOption('BLOCK_NONREPAIR')">click here</a></td>
            </tr>
            <tr valign="middle" class="RSLBlack">
              <td><img src="../../myImages/Repairs/ic_house.gif" width="20" height="18"> </td>
              <td>To view the <span class="style2">repair orders</span> that have been assign to <strong>individual properties</strong> in this scheme <a class="style2"  style="cursor:hand; text-decoration:underline " onClick="SubmitOption('PROP_REPAIR')">click here</a> </td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top">&nbsp;</td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top"><%=theTypeTitle%></td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top">&nbsp;</td>
            </tr>
            <tr class="RSLBlack">
              <td colspan="2" valign="top"><table width="600"  border=1 cellpadding="2" cellspacing="0" style='border-collapse:collapse'>
                  <tr bgcolor="green">
                    <td width="14%"><div align="right" class="style1">PO No. </div></td>
                    <td width="24%" bgcolor="green"><div align="right" class="style1">Order Item </div></td>
                    <td width="9%" bgcolor="green"><div align="right" class="style1">Cost</div></td>
                    <td width="15%" bgcolor="green"><div align="right" class="style1">Status</div></td>
                    <td width="10%" bgcolor="green"><div align="right" class="style1">Pay Date </div></td>
                    <td width="28%" bgcolor="green"><div align="right" class="style1">Supplier</div></td>
                  </tr>
                  <%=STRrow%>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <br>
      <table class="RSLBlack" border=1 style='border-collapse:collapse' cellspacing=0 align=center>
      </table></form>
</BODY>
</HTML>
