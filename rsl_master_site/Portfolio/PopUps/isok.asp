<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim blockid, repairid, wheresql, developmentid, repairsql, commence_date, start_date, isblock, ispropertyspecific
	Dim str_what, cnt, str_table, nett_cost, cost, desc
	
	' GET REQUIRED VARIABLES
	blockid 		= Request("BLOCK")
	developmentid 	= Request("DEVELOPMENT")
	repairid 		= Request("REPAIR")
	commence_date	= Request("COMMENCE")	
	start_date		= Request("START")
	ispropertyspecific = 0
	'response.write "<BR> BLOCKID : " & blockid
	'response.write "<BR> DEVELPMENTID : " & developmentid
	'response.write "<BR> REPAIRID : " & repairid
	'response.write "<BR> COMMENCE : " & commence_date
	'response.write "<BR> START : " & start_date
	
	if block = "" Then 
		wheresql = " WHERE P.DEVELOPMENTID = " & developmentid & " AND P.STATUS NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	else
		isblock = 1
		wheresql = " WHERE P.BLOCKID = " & blockid & " AND P.STATUS NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	end if
	
	OpenDB()
	
	// FIND OUT WHETHER OR NOT REPAIR IS PROPERTY SPECIFIC, IF SO THEN INCLUDE IN SQL
	if repairid = "9999" Then ispropertyspecific = 1 end if
	
	// DETERMINE WHICH TABLE TO BUILD
	if ispropertyspecific = 1 Then
		build_multiple()
	Else
		build_single()
	End if
	
		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & repairsql & wheresql & " ORDER BY P.PROPERTYID "
	
	str_what = build_decsriptive_string()
	
	CloseDB()
	
	// USE THIS FUNCTION IF WE HAVE A GAS REQUEST
	Function build_multiple()
	
		SQL = 	"SELECT P.PROPERTYID, R.DESCRIPTION, COST, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          		"FROM 	P__PROPERTY P " &_ 
				"		INNER JOIN P_ANNUALSERVICINGS A ON A.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN R_ITEMDETAIL R ON A.REPAIRID = R.ITEMDETAILID " & wheresql &_
				"ORDER	BY P.PROPERTYID "

		'response.write "<BR>" & SQL
		Call OpenRs(rsSet, SQL)
		
		cnt = 0
		nett_cost = 0
		str_table = "<TR>"
		While Not rsSet.EOF 
		
			str_table = str_table & "<TD WIDTH=100PX>" & rsSet("PROPERTYID") &_
									"</TD><TD nowrap>" & rsSet("ADDRESS") &_
									"</TD><TD nowrap>" & rsSet("DESCRIPTION") &_
									"</TD><TD>" & FormatCurrency(rsSet("COST")) & "</TD></TR>"
			cnt = cnt + 1
			nett_cost = nett_cost + rsSet("COST")
			rsSet.movenext()
		
		Wend
		CloseRs(rsSet)
	
	End Function
	
	// USE THIS FUNCTION IF WE HAVE A NORMAL SINGULAR REQUEST
	Function build_single()
	
		SQL = "SELECT COST, DESCRIPTION FROM R_ITEMDETAIL WHERE ITEMDETAILID = " & repairid
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then 
			cost = rsSet(0) 
			desc = rsSet(1) 
		end if
		CloseRs(rsSet)		
		
		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & wheresql & " ORDER BY P.PROPERTYID "

		'response.write "<BR>" & SQL
		Call OpenRs(rsSet, SQL)
		
		cnt = 0
		str_table = "<TR>"
		While Not rsSet.EOF 
		
			str_table = str_table & "<TD WIDTH=100PX>" & rsSet("PROPERTYID") &_
									"</TD><TD>" & rsSet("ADDRESS") &_
									"</TD></TR>"
			cnt = cnt + 1
			rsSet.movenext()
		
		Wend
		CloseRs(rsSet)
		nett_cost = cnt * cost
		
	End Function
	
	// GET INFORMATION STRING
	Function build_decsriptive_string()
		
		Dim this_string
		if isblock = 1 then 
			this_string = "block wide"
		else 
			this_string = "scheme wide"
		end if	
		if ispropertyspecific = 1 then
			this_string = this_string & " and property specific"
		end if
		
		build_decsriptive_string = this_string
		Exit Function 
			
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio --> Check Cyclical Repairs</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function return_focus(bool){
	
		window.returnValue = bool
		window.close()
	
	}
	
	function do_resize(){
		
		alert(<%=ispropertyspecific%>)
		if (<%=ispropertyspecific%> == 0)
			window.resizeTo(500,400);
	}

</SCRIPT>

<BODY class='ta' BGCOLOR=#FFFFFF onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" onload="do_resize()">
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
		<tr>
			<td colspan=2>The repair you have chosen is <b><%=str_what%></b> and will be attached to the following properties.
				The repairs will appear on the system on <B><%=commence_date%></b> and are scheduled to begin on <b><%=start_date%></b>
				. Please check that this information is correct. If so then press submit, if the information is not correct then
				click cancel and amend the appropriate data. 
			</td>
		</tr>
	</table>
	<BR>
<% If ispropertyspecific = 1 Then %>
	<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td WIDTH=100PX><b>Property Ref</b></td>
		<td><b>Address</b></td>
		<td><b>Description</b></td>
		<td><b>Cost</b></td>
	<tr>
		<%=str_table%>
	</table>
<% Else %>
<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td align=right><b>Item:</b> <%=desc%> <b>Cost:</b> <%=FormatCurrency(cost)%></td>
	<tr>
</table>
<BR>
<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td WIDTH=100PX><b>Property Ref</b></td>
		<td><b>Address</b></td>
	<tr>
		<%=str_table%>
	</table>
<% End If %>		
	
	<BR>
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr><td colspan=2 align=right>
		<font style='font-size:13'><b>Nett Cost: <%=FormatCurrency(nett_cost)%></b></font>
		</td>
	</tr>
	</table>
	<br>
	<table width=95% align=center>
	<tr>
		<td colspan=2 align=right>
		<input type=button class="RSLButton" style='width:70px' title='Cancel this cyclical repair' value=" Cancel " onclick="return_focus(false)">
		<input type=button class="RSLButton" style='width:70px' title='Submit this cyclical repair' value=" Submit " onclick="return_focus(true)">
		</td>
	<tr>
	</table>

<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_cyclrep width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

