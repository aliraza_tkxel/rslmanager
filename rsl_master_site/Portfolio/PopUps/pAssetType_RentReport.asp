<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
    dim top_data,bottom_data,asset_type_data,norecords,count,localauthority,developmentid,patch,postcode,processdate,displaydate
    dim pagesize
     
    
    if(Request.Item("localauthority")<>"") then
        localauthority=Request.Item("localauthority")
    else
        localauthority=null
    end if
    
    if(Request.Item("localauthoritydescription")<>"") then
        localauthoritydescription=Request.Item("localauthoritydescription")
    else
        localauthoritydescription=null
    end if
    
    if(Request.Item("developmentid")<>"") then
        developmentid=Request.Item("developmentid") 
    else
        developmentid=null
    end if
    
     if(Request.Item("developmentdescription")<>"") then
        developmentdescription=Request.Item("developmentdescription") 
    else
        developmentdescription=null
    end if
    
    if(Request.Item("patch")<>"") then
        patch=Request.Item("patch")
    else
        patch=null
    end if
    
    if(Request.Item("patchdescription")<>"") then
        patchdescription=Request.Item("patchdescription")
    else
        patchdescription=null
    end if
    
    if(Request.Item("processdate")<>"") then
        processdate=Request.Item("processdate")
    else
        processdate=null   
    end if    
    
    if(Request.Item("postcode")<>"") then 
        postcode=Request.Item("postcode")
    else
        postcode=null
    end if
    
    if(processdate<>"") then
        displaydate=processdate
    else
        displaydate=null
    end if
        
    if(Request.Item("form_post")<>"") then
        form_post=Request.Item("form_post")
    else
        form_post=0
    end if
    
    if(Request.Item("propertytypeid")<>"") then
        propertytypeid=Request.Item("propertytypeid")
    else
        propertytypeid=null
    end if
    
    if(Request.Item("propertytypedescription")<>"") then
        propertytypedescription=Request.Item("propertytypedescription")
    else
        propertytypedescription=""
    end if
 
    If Request.Item("page") = "" Then
		page = 1
    Else
		page = Request.Item("page")
    End If
    
    If Request.Item("status")<> "" Then
		status = Request.Item("status")
    Else
		status = null
    End If
    
    If Request.Item("statusdescription")<> "" Then
		statusdescription =  "->" & Request.Item("statusdescription")
    Else
		statusdescription = ""
    End If
    
    If(Request.Item("assetid")<>"") Then
      assetid=Request.Item("assetid")
    Else
      assetid=null  
    End If 
    
    If Request.Item("assetdescription")<>"TOTAL" Then
          assetdescription="->"& Request.Item("assetdescription")
    Else
        assetdescription=""
    End if
    
    If(Request.Item("pagesize")<> "") then
        pagesize = Request.Item("pagesize")
    else
        pagesize = 7
    end  if   
    
    
    top_data="<p class=""top_trail"">Portfolio Rent Analysis Report" & statusdescription & assetdescription & "</p>"
    
    asset_type_data= "<div style=""overflow:auto;height:230px;"" ><table id=""PortfolioPropertyDataTable"" border=""0"" cellpadding=""0"" cellspacing=""0""  class=""detail_data_table"" summary=""Portfolio Property Stock Summary"">"

	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
    
    dbrs.CursorType = 3
	dbrs.CursorLocation = 3
        
    dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    dbcmd.CommandType = 4
       
	
    paddingspace="<span style=""margin-left:590px"">Please print as a landscape. </span>"
    Printtext="PRINT ALL"
    printbtnwidth=60
    'printtype=1
    heading=heading & "<tr>"
    heading=heading & "   <th >Scheme</th>"
    heading=heading & "   <th >Property Ref</th>"
    heading=heading & "   <th>Property Address</th>"
    heading=heading & "   <th>Rent</th>"
    heading=heading & "   <th>Services</th>"
    heading=heading & "   <th>Inelig <br/> Serv</th>"
    heading=heading & "   <th>Supp Serv</th>"
    heading=heading & "   <th>Water</th>"
    heading=heading & "   <th>C/Tax</th>"
    heading=heading & "   <th>Garage</th>"
    heading=heading & "   <th>TOTAL</th>"
    heading=heading & "</tr>"
    
	 asset_type_data= asset_type_data & heading
	
    dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
	data_source = "P_PORTFOLIOANALYSIS_REPORT_DETAIL @REPORTDATE='" & processdate & "'"
	
   Response.Write("<p class=""top_trail_print"">Portfolio Rent Analysis Report" & statusdescription & assetdescription & "</p>")
   Response.Write("<p class=""top_processdate_print""><span class=""printbold"">Date:</span>" & displaydate & "</p>")
   
	
	if(patch<>"") then    
	    data_source=data_source & ",@PATCH='" & patch & "'"
	    Response.Write("<p class=""screen_invisible""><span class=""printbold"">Patch:</span>" & patchdescription & "</p>")
	end if 
	
	if(localauthority<>"") then
	    data_source=data_source & ",@LOCALAUTHORITY='" & localauthority & "'"
	    Response.Write("<p class=""screen_invisible""><span class=""printbold"">Local Authority:</span>" & localauthoritydescription & "</p>")
	end if
	
	if(developmentid<>"") then    
	    data_source=data_source & ",@SCHEME='" & developmentid & "'"
	    Response.Write("<p class=""screen_invisible""><span class=""printbold"">Scheme:</span>" & developmentdescription & "</p>")
	end if
	
	if(postcode<>"") then    
	    data_source=data_source & ",@POSTCODE='" & postcode & "'"
	    Response.Write("<p class=""screen_invisible""><span class=""printbold"">Post Code:</span>" & postcode & "</p>")
	end if

	if(assetid<>"") then
	    data_source=data_source & ",@ASSETTYPE=" & assetid & ""
	end if
	
	if(status<>"") then
	    data_source=data_source &  ",@STATUS=" & status & ""
	end if
	

	
	'Response.Write data_source 
	
    dbrs.Open data_source
    

   
   Response.Write("<table id=""print_all_table"">" & heading)
   
   do while not dbrs.eof
      if dbrs("address")="TOTAL" then 
            
            Response.Write("<tr>")
            Response.Write("    <td>&nbsp;</td>")
            Response.Write("    <td>&nbsp;</td>")
            Response.Write("    <td class=""printbold"">" & dbrs("address") & "</td>")    
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("rent")) & "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("services")) & "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("ineligserv")) & "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("supportedservices")) & "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("waterrates")) & "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("counciltax"))& "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("garage"))& "</td>")
            Response.Write("    <td class=""printbold"">" & FormatCurrency(dbrs("totalrent"))& "</td>")
            Response.Write("</tr>")
        else
            Response.Write("<tr>")
            Response.Write("    <td>" & dbrs("scheme") & "</td>")
            Response.Write("    <td>" & dbrs("propertyid") & "</td>")
            Response.Write("    <td>" & dbrs("address") & "</td>")    
            Response.Write("    <td>" & FormatCurrency(dbrs("rent")) & "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("services")) & "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("ineligserv")) & "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("supportedservices")) & "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("waterrates")) & "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("counciltax"))& "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("garage"))& "</td>")
            Response.Write("    <td>" & FormatCurrency(dbrs("totalrent"))& "</td>")
            Response.Write("</tr>")
         end if 
   dbrs.movenext()
   loop
   Response.Write("</table>")   
   
   dbrs.PageSize = pagesize'CONST_PAGESIZE
   'my_page_size = CONST_PAGESIZE
   ' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
   dbrs.CacheSize = pagesize'CONST_PAGESIZE
   intPageCount = dbrs.PageCount 
   intRecordCount = dbrs.RecordCount 
   'response.Write  "<br/>" & intRecordCount	    
	
	' Sort pages
	intpage = Cint(page)
		
	If intpage = 0 Then intpage = 1 End If
	' Just in case we have a bad request
	If intpage > intPageCount Then intpage = intPageCount End If
	If intpage < 1 Then intpage = 1 End If
	
	nextPage = intpage + 1
	If nextPage > intPageCount Then nextPage = intPageCount	End If
	prevPage = intpage - 1
	If prevPage <= 0 Then prevPage = 1 End If
    
    ' double check to make sure that you are not before the start
	' or beyond end of the recordset.  If you are beyond the end, set 
	' the current page equal to the last page of the recordset.  If you are
	' before the start, set the current page equal to the start of the recordset.
	If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
	If CInt(intPage) <= 0 Then intPage = 1 End If
    
    ' Make sure that the recordset is not empty.  If it is not, then set the 
	' AbsolutePage property and populate the intStart and the intFinish variables.
	If intRecordCount > 0 Then
		dbrs.AbsolutePage = intPage
		intStart = dbrs.AbsolutePosition
		If CInt(intPage) = CInt(intPageCount) Then
			intFinish = intRecordCount
		Else
			intFinish = intStart + (dbrs.PageSize - 1)
		End if
	End If
    
   if intRecordCount > 0 Then
         ' Display the record that you are starting on and the record
         ' that you are finishing on for this page by writing out the
         ' values in the intStart and the intFinish variables.
         ' Iterate through the recordset until we reach the end of the page
         ' or the last record in the recordset.
         
   	     for intRecord = 1 to dbrs.PageSize
        
            if (intRecord =1) then
                asset_type_data=asset_type_data & "<tr>"
                asset_type_data=asset_type_data & "<td colspan=""11"" class=""thinline"">&nbsp;</td>"    
                asset_type_data=asset_type_data & "</tr>" 
            end if
            if dbrs("address")="TOTAL" then
                asset_type_data=asset_type_data & "<tr>"
                asset_type_data=asset_type_data & "<td colspan=""11"" class=""thickline"">&nbsp;</td>"    
                asset_type_data=asset_type_data & "</tr>" 
                asset_type_data=asset_type_data & "<tr>" 
                asset_type_data=asset_type_data & "   <td >" & dbrs("scheme") & "</td>"
                asset_type_data=asset_type_data & "   <td >" & dbrs("propertyid") & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("address") & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("rent")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("services")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("ineligserv")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("supportedservices")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("waterrates")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("counciltax")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("garage")) & "</td>"
                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("totalrent")) & "</td>"
                asset_type_data=asset_type_data & "</tr>"
            else
                asset_type_data=asset_type_data & "<tr style='cursor:hand' onmouseover=""this.style.backgroundColor='#117DFF';"" onmouseout=""this.style.backgroundColor='white';"" onclick=""propertyredirect('" & dbrs("propertyid") & "');"">" 
                asset_type_data=asset_type_data & "   <td>" & dbrs("scheme") & "</td>"
                asset_type_data=asset_type_data & "   <td>" & dbrs("propertyid") & "</td>"
                asset_type_data=asset_type_data & "   <td>" & dbrs("address") & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("rent")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("services")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("ineligserv")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("supportedservices")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("waterrates")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("counciltax")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("garage")) & "</td>"
                asset_type_data=asset_type_data & "   <td>" & FormatCurrency(dbrs("totalrent")) & "</td>"
                asset_type_data=asset_type_data & "</tr>"
            end if
            
            dbrs.movenext()
		    if dbrs.EOF then exit for
		    'if dbrs("address")="TOTAL" then exit for			
		  next   
 	end if
    
  
    
    if((intPage)*dbrs.pagesize+count > intRecordCount-1) then
         numbofrecordonpage=intRecordCount-1
    else
         numbofrecordonpage=(intPage)*dbrs.pagesize+count
    end if  
    
    asset_type_data=asset_type_data &  "</table></div><table class=""detail_data_table"">"
    asset_type_data=asset_type_data & "<tr>"
    asset_type_data=asset_type_data & "<td colspan=""11"" class=""thinline"">&nbsp;</td>"    
    asset_type_data=asset_type_data & "</tr>"
    asset_type_data=asset_type_data & "<tr><td colspan=""11"" style=""padding-top:10px;padding-left:210px;"">"
    asset_type_data=asset_type_data & "<span style=""margin-left:80px;text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('1');"">FIRST</span>&nbsp;&nbsp;"    
    asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & prevpage & "');"">PREV</span>&nbsp;&nbsp;" 
    asset_type_data=asset_type_data & "<span> Page " & intpage & " of " & intPageCount & ". Records: " & dbrs.PageSize * (intpage - 1) + 1 & "  to " & numbofrecordonpage & " of " & intRecordCount-1 & "</span>"  
    asset_type_data=asset_type_data & "&nbsp;&nbsp;<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & nextpage & "');"">NEXT</span>&nbsp;&nbsp;"
    asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & intPageCount & "');"">LAST</span>"    
    asset_type_data=asset_type_data & "<span style=""padding-left:15px;""><input type=""text"" id=""page_size"" name=""page_size"" value=""" & pagesize & """ class=""textbox"" maxlength=""3"" size=""3""/></span><span style=""padding-left:10px;""><input type=""button"" name=""resize_button"" id=""resize_button"" value=""resize"" onclick=""resize_page();""/> </span></td></tr>" 
	    
     
   asset_type_data=asset_type_data & "</table>" '& print_all_data
   
   top_data=top_data & "<p class=""top_processdate"">Date:" & displaydate & ""
   top_data=top_data & paddingspace
   top_data=top_data & "<input type=""button""  id=""back_button"" name=""btn_back"" value=""<<Back"" class=""RSLButton"" style="" text-align:right;width:50px;margin-top:3px;"" onclick=""back();"" />"
   top_data=top_data & "<input type=""button""  id=""print_button"" name=""btn_print"" value="""& Printtext & """ class=""RSLButton"" style="" margin-left:10px;width:" & printbtnwidth &"px;margin-top:3px;"" onclick=""print_data();"" /></p>" 
   
   bottom_data=asset_type_data
  
%>
<html>
    <head>
        <title>Rent Analysis</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="/css/RSL_Non_Logo.css" type="text/css" />
        <style type="text/css" media="screen">
           #top_content{border-style: solid;border-color:#133e71;border-width:1px;padding-top:0;padding-bottom:5px;margin-bottom:4px;} 
           #bottom_content{border-style: solid;border-color:#133e71;border-width:1px 1px 0px 1px;padding-top:0;padding-bottom:0px;margin-bottom:0px;height:200px;} 
           .border_left{height:67px;margin-right:67px;border-bottom:solid 1px;border-left:solid 1px ;border-color:#133e71;}
           .bottom_right{padding-top:0px;margin:0;margin-left:0px;margin-right:0px; height:67px;}
           .detail_data_table{margin-left:10px;padding-bottom:0px;margin-bottom:3px;width:950px;margin-right:0px;}
           .detail_data_table th{font-size:10px;font-weight:bold;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-left:0px;padding-right:15px;padding-top:5px;margin-bottom:0px;padding-bottom:0px;}
           .detail_data_table td{padding-top:5px;padding-left:13px;font-size:10;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-right:4px;}
           /*.innertable {width:100%;}
           .innertable  td{padding-bottom:10px;}*/
           .thinline{border-style: solid;border-color:gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}  
           .thickline{border-style: solid;border-color:light blue;border-width:0 0 2px 0; padding:0 0 0 0; margin:0 0 0 0;}  
           .linkstyle{text-decoration: underline;color:blue;cursor:hand;}
           .fontweightbold{font-weight:bold;}
           #print_all_table{display:none;}
           .top_trail{margin-left:5px;margin-bottom:0px;margin-top:2;margin-right:0px;}
           .top_trail_print{display:none;}
           .top_processdate{margin-top:0px;margin-left:5px;padding-bottom:0px;margin-bottom:0px;}
           .top_processdate_print{display:none;}
           #scrolldata{overflow: scroll;width:100%;height:100%;overflow-x:hidden; height:200px;padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;}
            .tenpixelpaddingleftandright{padding-right:10px;padding-left:10px;}
            .tenpixelwidth{width:10px;}
            .screen_invisible{display:none;}
            #resize_button{font-size: 9px; color: #133e71; font-family: verdana, arial, helvetica; background-color: #ffffff; border: 1px #133e71 solid;cursor: hand}
        </style>
        <style type="text/css" media="print">
            #top_content{border:none;}
            #bottom_content{border:none;}
            h1{font-size:14px;}
            th{font-size:12px;padding-right:10px;}
            td{padding-right:10px;}
            #back_button{display:none;}
            #print_button{display:none;}
            label{display:none;}
            .top_processdate_print{padding-top:10px;padding-bottom:10px;display:none;}
            .top_processdate{display:none}
            .top_trail_print{font-size:14px;font-weight:bold;}
            .top_trail{display:none;}
            .printbold{font-weight:bold;}
            .detail_data_table{display:none;}
            .screen_invisible{display:none;}
      
        </style>
    </head>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript">	
    
    function back()
    {
     window.close();
    }
    

  function print_data()
  {
    window.print();
  }
  
  function propertyredirect(propertyid)
    {
        
        window.opener.location="../PRM.asp?PropertyID="+propertyid;
        window.close();
   }
   
   function movetopage(pageno)
   {
        rslform.page.value=pageno;
        rslform.submit();
   }
   function resize_page()
   {
        rslform.pagesize.value=rslform.page_size.value;
        rslform.submit();
   }
  </script>	

    <body bgcolor="#FFFFFF" onload="window.focus()" >
        <form name="rslform" method="post"  action="" >
            <div id="top_content">
                <%=top_data %>
            </div>
            <div >
                <div id="bottom_content" ><%=bottom_data %></div> 
                <div id="bottom_right" style="background: url(../../myImages/corner_pink_white_big.gif) 100% 100% no-repeat;">
                    <div class="border_left"></div>
                </div>
            </div>
            <input type="hidden" name="form_post" value="<%=form_post %>" />
            <input type="hidden" name="localauthority" value="<%=localauthority %>" />
            <input type="hidden" name="developmentid" value="<%=developmentid%>" />
            <input type="hidden" name="patch" value="<%=patch %>" />
            <input type="hidden" name="processdate" value="<%=processdate%>" />
            <input type="hidden" name="postcode" value="<%=postcode%>" />
            <input type="hidden" name="assetid" value="<%=assetid %>" />
            <input type="hidden" name="assetdescription" value="<%=assetdescription %>" />
            <input type="hidden" name="page" value="<%=page%>" />
            <input type="hidden" name="pagesize" value="<%=pagesize%>" />

         </form>
        
    </body>
   
</html>

