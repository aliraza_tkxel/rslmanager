<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
	Dim repair_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim  ContractorName, ContractorID,tenancyid,orgid,username,journalid,appointmentdate,appointmentdatetime 
	
	path = request.form("hid_go")
	repair_history_id 	= Request("repairhistoryid")
	nature_id 			= Request("natureid")

	' begin processing
	Call OpenDB()
        Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE=" & nature_id ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:198px' onchange='action_change()' ")
	    Call get_detail()
	Call CLoseDB()
	
	Function get_detail()
	    SQL=" SELECT CJ.JOURNALID, O.ORGID AS ORGID,O.NAME AS CONTRACTORNAME,CT.TENANCYID,CRG.APPOINTMENTDATE FROM C_REPAIR CR "&_
	        "    LEFT JOIN S_SCOPE S ON S.SCOPEID=CR.SCOPEID "&_
	        "    LEFT JOIN S_ORGANISATION O ON O.ORGID=S.ORGID "&_
	        "    INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
	        "    LEFT JOIN C_TENANCY CT ON CT.PROPERTYID=CJ.PROPERTYID AND CT.ENDDATE IS NULL "&_
	        "    LEFT JOIN C_REPAIRTOGASSERVICING CRG ON CRG.REPAIRHISTORYID=CR.REPAIRHISTORYID  "&_
	        " WHERE CR.REPAIRHISTORYID= " & repair_history_id    
 
	    Call OpenRs(rsSet, SQL)
	          ContractorName= rsSet("CONTRACTORNAME")
	          tenancyid=rsSet("TENANCYID")
	          orgid=rsSet("ORGID")
	          appointmentdatetime=rsSet("APPOINTMENTDATE")
	          if(appointmentdatetime="" or isnull(appointmentdatetime)=true) then
	                appointmentdatetime=GetAppointmentDateTime(repair_history_id)
	          end if
	          if(appointmentdatetime<>"") then
	            appointmentdate=FormatDateTime(appointmentdatetime,2)
	          end if
		journalid = rsSet("JOURNALID")
	    CloseRs(rsSet)
	End Function
	
    Function GetAppointmentDateTime(ID)
	    
	    SQL= " SELECT CRG.APPOINTMENTDATE "&_
            "  FROM C_REPAIRTOGASSERVICING CRG "&_
	        "   INNER JOIN 	C_REPAIR CR ON CR.REPAIRHISTORYID=CRG.REPAIRHISTORYID "&_
	        "   INNER JOIN ( "&_
	        "                SELECT J.JOURNALID "&_
			"            	 FROM C_REPAIR R "&_
			"           		INNER JOIN C_JOURNAL J ON J.JOURNALID=R.JOURNALID "&_
			"           	 WHERE R.REPAIRHISTORYID="& ID &_
			"           	)S ON S.JOURNALID=CR.JOURNALID 	 "&_
            " WHERE cr.REPAIRHISTORYID=( "&_
            "                           SELECT MAX(KR.REPAIRHISTORYID) "&_
			"                           FROM C_REPAIRTOGASSERVICING KRG "&_
			"                   			INNER JOIN C_REPAIR KR ON KRG.REPAIRHISTORYID=KR.REPAIRHISTORYID "&_
			"               			WHERE KR.JOURNALID=CR.JOURNALID AND KRG.APPOINTMENTDATE IS NOT NULL "&_
			"                           )	"
			 

			 Call OpenRs(rsSet, SQL)
			 
			 if(not rsSet.eof) then
		         GetAppointmentDateTime=rsSet("APPOINTMENTDATE")
		     else
		        GetAppointmentDateTime=""       	       
			 end if
			 
			 CloseRs(rsSet)
	End Function
	
	username=Session("FirstName") & "," & Session("LastName")
%>
<html>
<head>
    <title>Update Repair</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .elementlabeldisabled
        {
            color: gray;
        }
        .elementlabel
        {
            color: black;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript">
<!--
var FormFields = new Array();
	FormFields[0] = "sel_ITEMACTIONID|Action|SELECT|Y"
	FormFields[1] = "sel_LETTER|LETTER|SELECT|N"

function action_change() {

	if(RSLFORM.sel_ITEMACTIONID.value=="108") {
		document.RSLFORM.txt_APPOINTMENTDATE.disabled=true;
		document.RSLFORM.sel_HOUR.disabled=true;
		document.RSLFORM.sel_MINUTES.disabled=true;
		document.RSLFORM.sel_MARADIAM.disabled=true;
		document.RSLFORM.btn_view_letter.disabled=true;
		document.getElementById("lbl_APPOINTMENTDATE").className ="elementlabeldisabled";
		document.getElementById("lbl_time").className ="elementlabeldisabled";
		document.RSLFORM.txt_NOTES.disabled=true;
		document.getElementById("lbl_NOTES").className ="elementlabeldisabled";	
	}
	else {
		document.RSLFORM.txt_APPOINTMENTDATE.disabled=false;
		document.RSLFORM.sel_HOUR.disabled=false;
		document.RSLFORM.sel_MINUTES.disabled=false;
		document.RSLFORM.sel_MARADIAM.disabled=false;
		document.RSLFORM.btn_view_letter.disabled=false;
		document.getElementById("lbl_APPOINTMENTDATE").className ="elementlabel";
		document.getElementById("lbl_time").className ="elementlabel";
	}	
	document.RSLFORM.action = "../Serverside/GetLetterAction.asp"
	document.RSLFORM.target = "ServerFrame"
	document.RSLFORM.submit()
	return;
}   
	
	
function save_form()
{
	if(RSLFORM.txt_APPOINTMENTDATE.value == "" && RSLFORM.sel_ITEMACTIONID.value != "108")
		{
			alert("Please enter the appointment date.")
			return;
		}    
	if(RSLFORM.sel_ITEMACTIONID.value != "108") 
	{
	  FormFields[1] = "sel_LETTER|LETTER|SELECT|Y"	
	}				
	else
	{
	  FormFields[1] = "sel_LETTER|LETTER|SELECT|N"		
	}	
	if (!checkForm()) return false;
	
	var AppointmentTime = RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;        
	document.RSLFORM.btn_submit.disabled=true;
	document.RSLFORM.btn_view_letter.disabled=true;        
	document.RSLFORM.hid_APPOINTMENTTIME.value=AppointmentTime;
	document.RSLFORM.target = "ServerFrame";
	document.RSLFORM.action = "../Serverside/SaveLetters.asp";
	document.RSLFORM.submit();
}
	
	
    function open_letter()
    {
	    var	AppointmentTime = RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;

	    if (RSLFORM.sel_LETTER.value == "") 
	    {
		    alert("You must first select a letter to view");
		    return false;
	    }

	    var tenancy_id = "<%=tenancyid%>"
	    window.open("pView_letter.asp?tenancyid="+tenancy_id+"&letterid="+RSLFORM.sel_LETTER.value+"&DATE1="+ RSLFORM.txt_APPOINTMENTDATE.value+" "+AppointmentTime+"&DATE2=<%=appointmentdatetime%>", "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
    }

    function refreshParent()
    {
        window.opener.location.href = window.opener.location.href;
        if (window.opener.progressWindow)
	    {
	        window.opener.parent.open_me(47,<%=Journalid%>,"iServicingDetail.asp")
	        window.opener.reload();
        }
        window.close();
    }
 	//--> 
    </script>
</head>
<body onload="window.focus()">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td width="10" height="10">
                <img src="/Customer/Images/tab_update.gif" width="74" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="3" valign="top" style='border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71; padding-left: 10px;'>
                <table>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contractor
                        </td>
                        <td>
                            <input type="text" class="textbox200" readonly="readonly" name="PreviousContractor"
                                id="PreviousContractor" value="<%=ContractorName%>" />
                            <input type="hidden" name="PreviousContractorID" id="PreviousContractorID" value="<%=ContractorID%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action
                        </td>
                        <td>
                            <%=lstAction%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="sel_LETTER" style="vertical-align: top;">
                                Document:</label>
                        </td>
                        <td>
                            <div id="dvLetter">
                                <select name="sel_LETTER" id="sel_LETTER" class="textbox200">
                                    <option value="">Please select action</option>
                                </select></div>
                        </td>
                        <td>
                            <img alt="" src="/js/FVS.gif" name="img_LETTER" id="img_LETTER" width="15px" height="15px"
                                border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="txt_APPOINTMENTDATE" id="lbl_APPOINTMENTDATE">
                                Appointment:</label>
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_APPOINTMENTDATE" id="txt_APPOINTMENTDATE"
                                value="" />
                        </td>
                        <td>
                            <img alt="" src="/js/FVS.gif" name="img_APPOINTMENTDATE" id="img_APPOINTMENTDATE"
                                width="15px" height="15px" border="0" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="sel_HOUR" id="lbl_time">
                                Time:</label>
                        </td>
                        <td>
                            <select name="sel_HOUR" id="sel_HOUR" class="textbox100" style="width: 50px;">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8" selected="selected">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <select name="sel_MINUTES" id="sel_MINUTES" class="textbox100" style="width: 50px;">
                                <option value="15">00</option>
                                <option value="15">15</option>
                                <option value="15">30</option>
                                <option value="15">45</option>
                            </select>
                            <select name="sel_MARADIAM" id="sel_MARADIAM" class="textbox100" style="width: 50px;">
                                <option value="1">AM</option>
                                <option value="2">PM</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_RECORDEDBY" id="txt_RECORDEDBY" value="<%=username%>"
                                readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <label for="txt_NOTES" id="lbl_NOTES">
                                Notes:</label>
                        </td>
                        <td>
                            <textarea style="overflow: hidden" class="textbox200" name="txt_NOTES" id="txt_NOTES"
                                rows="4" cols="1"></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_REPAIRHISTORYID" id="hid_REPAIRHISTORYID" value="<%=repair_history_id %>" />
                            <input type="hidden" name="hid_ORGID" id="hid_ORGID" value="<%=orgid%>" />
                            <input type="hidden" name="hid_APPOINTMENTTIME" id="hid_APPOINTMENTTIME" value="" />
                            <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                            <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="border-bottom: 1px solid #133e71; border-left: 1px solid #133e71">
                <span style='visibility: hidden'>
                    <%=RepairPriority%></span>
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="button" name="btn_view_letter" id="btn_view_letter" onclick="open_letter()"
                    value="View Letter" class="RSLButton" />
                <input type="button" name="btn_close" id="btn_close" onclick="refreshParent()" value=" Close "
                    class="RSLButton" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    class="RSLButton" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133E71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe name="ServerFrame" id="ServerFrame" src="/dummy.asp" style="display: none">
    </iframe>
</body>
</html>
