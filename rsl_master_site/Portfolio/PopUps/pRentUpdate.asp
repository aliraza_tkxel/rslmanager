<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PropertyID = Request("PropertyID")
	f_renteffective = "31/03/2005"
	f_daterentset = "01/04/2004"

    SQL = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & PropertyID & "'"

	Call OpenDB()
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		f_targetrentset = rsLoader("TARGETRENTSET")  'This is when a property becomes available, to keep in line with rent targets
		F_nominatingbody = rsLoader("NOMINATINGBODY")
		f_renttype = rsLoader("RENTTYPE")
		f_renteffective = rsLoader("RENTEFFECTIVE")
		If isNull(f_renteffective) or f_renteffective = "" Then f_renteffective = "31/03/2005" End If
		f_targetrent = rsLoader("TARGETRENT")
		If isNull(f_targetrent) Then f_targetrent = FormatNumber(0.00) End If
		f_daterentset = rsLoader("DATERENTSET")
		If isNull(f_daterentset) Then f_daterentset = "01/04/2004" End If	
		f_fundingauthority = rsLoader("FUNDINGAUTHORITY")				
		f_charge = rsLoader("CHARGE")		
		f_chargevalue = rsLoader("CHARGEVALUE")	
		If isNull(f_chargevalue) Then f_chargevalue = "0.00" End If			
		f_rent = rsLoader("RENT")						
		If isNull(f_rent) Then f_rent = "0.00" End If			
		f_counciltax = rsLoader("COUNCILTAX")
		If isNull(f_counciltax) Then f_counciltax = "0.00" End If											
		f_services = rsLoader("SERVICES")
		If isNull(f_services) Then f_services = "0.00" End If											
		f_water = rsLoader("WATERRATES")
		If isNull(f_water) Then f_water = "0.00" End If											
		f_support = rsLoader("SUPPORTEDSERVICES")
		If isNull(f_support) Then f_support = "0.00" End If					
		f_inelig = rsLoader("INELIGSERV")
		If isNull(f_inelig) Then f_inelig = "0.00" End If											
		f_totalrent = rsLoader("TOTALRENT")
		If isNull(f_totalrent) Then f_totalrent = "0.00" End If											
		f_garage = rsLoader("GARAGE")
		If isNull(f_garage) Then f_garage = "0.00" End If		
	End If
	
	Call BuildSelect(lstrenttype, "sel_RENTTYPE", "C_TENANCYTYPE where TENANCYTYPEID=10", "TENANCYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", f_renttype, NULL, "textbox", "STYLE='WIDTH:200PX'  TABINDEX=2")
	Call CloseDB()
%>
<html>
<head>
    <title>Update Repair</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .style3
        {
            color: #FFFFFF;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_DATERENTSET|Date Rent Set|DATE|Y"
        FormFields[1] = "txt_RENTEFFECTIVE|Rent Effective From|DATE|Y"
        FormFields[2] = "txt_TOTALRENT|Total Rent|TEXT|Y"
        FormFields[3] = "txt_RENT|Rent|CURRENCY|Y"
        FormFields[4] = "txt_SERVICES|Services|CURRENCY|Y"
        FormFields[5] = "txt_COUNCILTAX|Council Tax|CURRENCY|Y"
        FormFields[6] = "txt_WATERRATES|Water Rates|CURRENCY|Y"
        FormFields[7] = "txt_INELIGSERV|Ineligible Services|CURRENCY|Y"
        FormFields[8] = "txt_SUPPORTEDSERVICES|Supported Services|CURRENCY|Y"
        FormFields[9] = "sel_RENTTYPE|Rent type|SELECT|Y"
        FormFields[10] = "txt_TARGETRENT|Target Rent|CURRENCY|Y"
        FormFields[11] = "txt_GARAGE|Garage|FLOAT|Y|2"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.method = "POST"
            document.RSLFORM.target = "frm_direct";
            document.RSLFORM.action = "../ServerSide/UpdateRentfinancial_svr.asp"
            document.RSLFORM.submit()
        }

        function calc_rent() {
            if (!checkForm(true)) return false;
            var rent = parseFloat(document.getElementById("txt_RENT").value);
            var services = parseFloat(document.getElementById("txt_SERVICES").value);
            var counciltax = parseFloat(document.getElementById("txt_COUNCILTAX").value);
            var water = parseFloat(document.getElementById("txt_WATERRATES").value);
            var inelgserv = parseFloat(document.getElementById("txt_INELIGSERV").value);
            var supp = parseFloat(document.getElementById("txt_SUPPORTEDSERVICES").value);
            var garage = parseFloat(document.getElementById("txt_GARAGE").value);
            var totalrent = rent + services + counciltax + water + inelgserv + supp + garage
            document.getElementById("txt_TOTALRENT").value = FormatCurrency(totalrent);
        }
	
    </script>
</head>
<body bgcolor="#FFFFFF">
    <form name="RSLFORM" action="" style="margin:0px; padding:0px">
    <table id="table1" style="border: 1px solid #133e71" width="360">
        <tr>
            <td colspan="4">
                <table width="353">
                    <tr>
                        <td>
                            Date Rent Set:
                        </td>
                        <td colspan="3">
                            <input type="text" name="txt_DATERENTSET" id="txt_DATERENTSET" tabindex="3" class="textbox200" value="<%=f_daterentset%>" />
                            <img src="/js/FVS.gif" name="img_DATERENTSET" id="img_DATERENTSET" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rent Effective to :
                        </td>
                        <td colspan="3">
                            <input type="text" name="txt_RENTEFFECTIVE" id="txt_RENTEFFECTIVE" tabindex="4" class="textbox200" value="<%=f_renteffective%>" />
                            <img src="/js/FVS.gif" name="img_RENTEFFECTIVE" id="img_RENTEFFECTIVE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rent Type
                        </td>
                        <td colspan="3">
                            <%=lstrenttype%>
                            <img src="/js/FVS.gif" name="img_RENTTYPE" id="img_RENTTYPE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="101">
                            Rent
                        </td>
                        <td width="87">
                            <input onblur="calc_rent()" tabindex="9" type="text" name="txt_RENT" id="txt_RENT" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_rent)%>" />
                            <img src="/js/FVS.gif" name="img_RENT" id="img_RENT" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td width="65">
                            Services
                        </td>
                        <td width="80">
                            <input onblur="calc_rent()" type="text" tabindex="10" name="txt_SERVICES" id="txt_SERVICES" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_services)%>" />
                            <img src="/js/FVS.gif" name="img_SERVICES" id="img_SERVICES" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Council Tax
                        </td>
                        <td>
                            <input onblur="calc_rent()" type="text" name="txt_COUNCILTAX" id="txt_COUNCILTAX" tabindex="11" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_counciltax)%>" />
                            <img src="/js/FVS.gif" name="img_COUNCILTAX" id="img_COUNCILTAX" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            Water
                        </td>
                        <td>
                            <input onblur="calc_rent()" type="text" name="txt_WATERRATES" id="txt_WATERRATES" tabindex="12" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_water)%>" />
                            <img src="/js/FVS.gif" name="img_WATERRATES" id="img_WATERRATES" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Inelig.
                        </td>
                        <td>
                            <input onblur="calc_rent()" type="text" name="txt_INELIGSERV" id="txt_INELIGSERV" tabindex="13" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_inelig)%>" />
                            <img src="/js/FVS.gif" name="img_INELIGSERV" id="img_INELIGSERV" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            Supp.
                        </td>
                        <td>
                            <input onblur="calc_rent()" type="text" name="txt_SUPPORTEDSERVICES" id="txt_SUPPORTEDSERVICES" tabindex="14"
                                class="textbox100" style="width: 60px" value="<%=FormatNumber(f_support)%>" />
                            <img src="/js/FVS.gif" name="img_SUPPORTEDSERVICES" id="img_SUPPORTEDSERVICES" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Garage.
                        </td>
                        <td>
                            <input onblur="calc_rent()" type="text" name="txt_GARAGE" id="txt_GARAGE" tabindex="13" class="textbox100"
                                style="width: 60px" value="<%=FormatNumber(f_garage)%>" />
                            <img src="/js/FVS.gif" name="img_GARAGE" id="img_GARAGE" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr bgcolor="#133E71">
                        <td colspan="4">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr bgcolor="#040455">
                                    <td width="105" bgcolor="#133E71">
                                        <span class="style3">Total Rent</span>
                                    </td>
                                    <td bgcolor="#133E71">
                                        <input type="text" name="txt_TOTALRENT" id="txt_TOTALRENT" class="textbox100" style="width: 60px" readonly="readonly"
                                            value="<%=FormatNumber(f_totalrent)%>" />
                                        <img src="/js/FVS.gif" name="img_TOTALRENT" id="img_TOTALRENT" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
            <td width="119">
                &nbsp;
            </td>
            <td width="82">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
            <td>
                Target Rent:
            </td>
            <td>
                <input onblur="calc_rent()" type="text" name="txt_TARGETRENT" id="txt_TARGETRENT" tabindex="14" class="textbox100"
                    style="width: 60px" value="<%=FormatNumber(f_targetrent)%>" />
                <img src="/js/FVS.gif" name="img_TARGETRENT" id="img_TARGETRENT" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="108">
            </td>
            <td width="32">
                &nbsp;
            </td>
            <td nowrap="nowrap">
                Target Set
                <%  
			If NOT f_targetrentset <> "1" Then isChecked = "checked" End If %>
            </td>
            <td nowrap="nowrap">
                <input type="checkbox" name="chk_targetrentset" id="chk_targetrentset" value="1" <%=isChecked%> />
            </td>
        </tr>
        <tr style="display: none">
            <td colspan="4">
                <table width='100%'>
                    <tr>
                        <td>
                            App. Date
                        </td>
                        <td>
                            <input type="text" name="txt_ApplicationDate" class="textbox100" style="width: 60px"
                                readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_ApplicationDate" id="img_ApplicationDate" width="45px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            Reg. Date
                        </td>
                        <td>
                            <input type="text" name="txt_RegistrationDate" id="txt_RegistrationDate" class="textbox100" style="width: 60px"
                                readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_RegistrationDate" id="img_RegistrationDate" width="45px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Effect. Date
                        </td>
                        <td>
                        </td>
                        <td style="cursor: pointer" title="(1st month following Effective Date)">
                            Start Date
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Reg. Number
                        </td>
                        <td>
                        </td>
                        <td style="cursor: pointer" title="(2 years from Effective Date)">
                            Reg. Next Due
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="hid_PropertyID" id="hid_PropertyID" value="<%=Request("PROPERTYID")%>" />
                <input type="button" class="RSLButton" value=" SAVE " title="SAVE" onclick="SaveForm()" tabindex="15"
                    name="button" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="frm_direct" id="frm_direct" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
