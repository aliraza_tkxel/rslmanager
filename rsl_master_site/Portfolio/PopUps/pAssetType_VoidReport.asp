<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%

    'variable declaration
    dim top_data,bottom_data,asset_type_data,norecords,count,localauthority,developmentid,housingofficer,postcode,processdate,displaydate
    
    CONST CONST_PAGESIZE = 7
    
    'Start of posted form values 
    if(Request.Item("localauthority")<>"") then
        localauthority=Request.Item("localauthority")
    else
        localauthority=null
    end if
    
    if(Request.Item("developmentid")<>"") then
        developmentid=Request.Item("developmentid") 
    else
        developmentid=null
    end if
    
    if(Request.Item("patch")<>"") then
        patch=Request.Item("patch")
    else
        patch=null
    end if
    
    if(Request.Item("processdate")<>"") then
        processdate=Request.Item("processdate")
    else
        processdate=null   
    end if    
    
    if(Request.Item("postcode")<>"") then 
        postcode=Request.Item("postcode")
    else
        postcode=null
    end if
    
    if(processdate<>null) then
        displaydate=processdate
    else
        displaydate=date()
    end if
        
    if(Request.Item("form_post")<>"") then
        form_post=Request.Item("form_post")
    else
        form_post=0
    end if
  
   if(Request.Item("period")<>"") then
        period=Request.Item("period")
    else
        period=0
    end if
    
    if(Request.Item("perioddescription")<>"") then
        perioddescription=Request.Item("perioddescription")
    else
        perioddescription=""
    end if
   
    If Request.Item("page") = "" Then
		page = 1
    Else
		page = Request.Item("page")
    End If
    
    If Request.Item("status")<> "" Then
		status = Request.Item("status")
    Else
		status = null
    End If
    
    If Request.Item("statusdescription")<> "" Then
		statusdescription =  "->" & Request.Item("statusdescription")
    Else
		statusdescription = ""
    End If
    
    If Request.Item("assettypeid")<>"" Then
       assettypeid=Request.Item("assettypeid")
    Else
      assettypeid=null 
    End If 
    
    If Request.Item("assetdescription")<>"TOTAL" Then
          assetdescription="->"& Request.Item("assetdescription")
    Elseif(Request.Item("assetdescriptiond2"))<>"" Then
        assetdescription="->"& Request.Item("assetdescriptiond2")
    else
        assetdescription=""
    End if
    
    'response.Write("assetdescritpion:" & Request.Item("assetdescription"))
   'End of posted form value 
  
    
    'top data variable make the data for the box at the top
    top_data="<p class=""top_trail"">Portfolio Void Management Report" & statusdescription & assetdescription
    
    'Altering the form width depending on data of window and posted form
    if(form_post=1) then 
        asset_type_data= "<div style=""overflow:auto;height:360px;""><table id=""PortfolioPropertyDataTable"" border=""0"" cellpadding=""0""  class=""property_data_table"" cellspacing=""0"" style=""width:700px;""  summary=""Portfolio Property Stock Summary"">"
	else
	    asset_type_data= "<table id=""PortfolioPropertyDataTable"" border=""0"" cellpadding=""0""  class=""property_data_table"" cellspacing=""0""  summary=""Portfolio Property Stock Summary"">"
	end if
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
    
    dbrs.CursorType = 3
	dbrs.CursorLocation = 3
        
    dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    dbcmd.CommandType = 4
	
	if(form_post=1) then   
	    checkbox="<span style=""margin-left:480px""></span>"
	    Printtext="PRINT ALL"
	    printbtnwidth=60
	    printtype=2
	    top_data=top_data & "->" & perioddescription
	    
	    heading=heading & "<tr>"
	    heading=heading & "   <th style=""padding-right:10px;"">Scheme</th>"
	    heading=heading & "   <th style=""text-align=center;"">Property<br/> Ref</th>"
	    heading=heading & "   <th  >Property<br/> Address</th>"
	    heading=heading & "   <th style=""text-align=right;"">Void From</th>"
	    heading=heading & "   <th>No of <br/> Days <br/>Void</th>"
	    heading=heading & "   <th>Total Rent</th>"
	    heading=heading & "   <th>Accum <br/> Rent Loss</th>"
	    heading=heading & "</tr>"
	    
	    asset_type_data= asset_type_data & heading
	    
	    dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
		
		parameter=" @REPORTDATE='" & processdate & "',@STATUS=" & status & ""
		
		if(localauthority<>"") then
		    parameter=parameter & ",@LOCALAUTHORITY=" & localauthority & ""
		end if
		
		if(developmentid<>"") then
		    parameter=parameter & ",@SCHEME=" & developmentid & ""
		end if
		
		if(postcode<>"") then
		   parameter=parameter & ",@POSTCODE='" & postcode & "'" 
		end if
		
		if(patch<>"") then 
		    parameter=parameter & ",@PATCH=" & patch & ""
		end if
		 
		if(assettypeid <> "") then
		    parameter=parameter & ",@ASSETTYPE=" & assettypeid & ""
		elseif(Request.Item("assettypeid2")<>"") then
		    parameter=parameter & ",@ASSETTYPE=" & Request.Item("assettypeid2") & ""
		end if
		
		if(period<>"") then
		    parameter= parameter & ",@PERIOD=" & period & ""
		end if 
		
		data_source = "P_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL " & parameter
	    
	    'response.Write 	"P_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL" & parameter
		
		dbrs.Open data_source
		
		'response.End()
		print_all_data= print_all_data & "<table id=""print_all_table"">" & heading
		do while not dbrs.eof
		    if(dbrs("schemename")<>"TOTAL") then
		        print_all_data=print_all_data & "<tr>" 
                print_all_data=print_all_data & "   <td>" & dbrs("schemename") & "</td>"
                print_all_data=print_all_data & "   <td style=""padding-left:20px;"">" & dbrs("propertyid") & "</td>"
                print_all_data=print_all_data & "   <td style=""padding-left:50px;"">" & dbrs("address") & "</td>"
                print_all_data=print_all_data & "   <td style=""width:30px;"">" & FormatDateTime(dbrs("enddate")) & "</td>"
                print_all_data=print_all_data & "   <td >" & FormatNumber(dbrs("numbofdaysvoid"),0) & "</td>"
                print_all_data=print_all_data & "   <td >" & FormatCurrency(dbrs("totalrent")) & "</td>"
                print_all_data=print_all_data & "   <td >" & FormatCurrency(dbrs("accumulatedrent")) & "</td>"
                print_all_data=print_all_data & "</tr>"
             else
                print_all_data=print_all_data & "<tr>" 
                print_all_data=print_all_data & "   <td>&nbsp;</td>"
                print_all_data=print_all_data & "   <td>&nbsp;</td>"
                print_all_data=print_all_data & "   <td class=""printbold"">" & dbrs("schemename") & "</td>"
                print_all_data=print_all_data & "   <td >&nbsp; </td>"
                print_all_data=print_all_data & "   <td class=""printbold"">" & FormatNumber(dbrs("numbofdaysvoid"),0) & "</td>"
                print_all_data=print_all_data & "   <td class=""printbold"">" & FormatCurrency(dbrs("totalrent")) & "</td>"
                print_all_data=print_all_data & "   <td class=""printbold"">" & FormatCurrency(dbrs("accumulatedrent")) & "</td>"
                print_all_data=print_all_data & "</tr>"   
             end if
		    dbrs.movenext()
	    loop  
		print_all_data=print_all_data & "</table>" 
		
		dbrs.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		dbrs.CacheSize = dbrs.PageSize
		intPageCount = dbrs.PageCount 
		intRecordCount = dbrs.RecordCount 
       'response.Write  "<br/>" & intRecordCount	    
		
		' Sort pages
		intpage = Cint(page)
		
    	If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
		
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If
        
        ' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If
        
        ' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			dbrs.AbsolutePage = intPage
			intStart = dbrs.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (dbrs.PageSize - 1)
			End if
		End If
        
       if intRecordCount > 0 Then
	         ' Display the record that you are starting on and the record
	         ' that you are finishing on for this page by writing out the
	         ' values in the intStart and the intFinish variables.
	         ' Iterate through the recordset until we reach the end of the page
	         ' or the last record in the recordset.
	         
	   	     for intRecord = 1 to dbrs.PageSize
            
                if (intRecord =1) then
                    asset_type_data=asset_type_data & "<tr>"
	                asset_type_data=asset_type_data & "<td colspan=""7"" class=""thinline"">&nbsp;</td>"    
	                asset_type_data=asset_type_data & "</tr>" 
                end if
                if(dbrs("schemename") <>"TOTAL") then
                    float_div= float_div & "<div id='" & dbrs("propertyid") & "div" & "' style=""display:none;""><div id=""float_div"">"
                    float_div=float_div &  "        <p style=""padding-bottom:0px;margin-bottom:0px;margin-top:5px;padding-left:10px;""><img src=""../images/Im_i_grey.gif"" alt=""information""  width=""15px"" height=""16px"" />Prop Ref:<span style=""margin-left:15px;"">" & dbrs("propertyid") & "</span></p>"
                    float_div=float_div &  "        <p class=""thinline"" style=""width:150px;margin-left:10px;margin-right:10px;margin-top:0px;padding-top:0px;margin-bttom:0px;""> </p>"
                    float_div=float_div &  "        <p style=""margin-top:10px;padding-left:15px;margin-bottom:3px;"">Rent:<span style=""padding-left:72px;"">" & FormatCurrency(dbrs("rent")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">Services:<span style=""margin-left:52px;"">" & FormatCurrency(dbrs("services")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">Inelig Services:<span style=""margin-left:15px;"">" & FormatCurrency(dbrs("ineligserv")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">Supp Services:<span style=""margin-left:19px;"">" & FormatCurrency(dbrs("supportedservices")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">Water:<span style=""margin-left:63px;"">" & FormatCurrency(dbrs("waterrates")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">C/Tax:<span style=""margin-left:61px;"">" & FormatCurrency(dbrs("counciltax")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:0px;margin-bottom:3px;padding-left:15px;"">Garage:<span style=""margin-left:55px;"">" & FormatCurrency(dbrs("garage")) &"</span></p>"
                    float_div=float_div &  "        <p style=""margin-top:10px;margin-bottom:0px;padding-left:15px;padding-bottom:0px;"">TOTAL:<span style=""margin-left:58px;"">" & FormatCurrency(dbrs("totalrent")) &"</span></p>"
                    float_div=float_div &  "        <p class=""thinline"" style=""width:150px;margin-left:10px;margin-right:10px;margin-top:0px;padding-top:0px;margin-bttom:3px;""> </p>"
                    float_div=float_div &  "<span class=""image_link"" style=""padding-left:70px;padding-top:10px;"" onclick=""open_info_div('" & dbrs("propertyid") & "div" & "','0')"">Close Window&nbsp;<img alt=""close"" src=""../../myImages/My48.gif"" height=""11px"" width=""11px"" /></span>"
                    float_div=float_div &  "</div></div>"     
                    
                    'asset_type_data=asset_type_data & "<tr style='cursor:hand' onmouseover=""this.style.backgroundColor='#117DFF';"" onmouseout=""this.style.backgroundColor='white';"" onclick=""propertyredirect('" & dbrs("propertyid") & "');"">" 
                    asset_type_data=asset_type_data & "<tr>" 
                    asset_type_data=asset_type_data & "   <td colspan=5 style=""padding-left:0;margin-left:0px;text-align:left;"">"
                    asset_type_data=asset_type_data & "         <table class=""property_data_table_inner""  style=""padding-left:0px;margin-left:0px;"">"
                    asset_type_data=asset_type_data & "             <tr style='cursor:hand' onmouseover=""this.style.backgroundColor='#117DFF';"" onmouseout=""this.style.backgroundColor='white';"" onclick=""propertyredirect('" & dbrs("propertyid") & "');""> "
                    asset_type_data=asset_type_data & "                     <td style=""width:120px;"">" & dbrs("schemename") & "</td>"
                    asset_type_data=asset_type_data & "                     <td>" & dbrs("propertyid") & "</td>"
                    asset_type_data=asset_type_data & "                     <td style=""width:120px;"">" & dbrs("address") & "</td>"
                    asset_type_data=asset_type_data & "                     <td>" & dbrs("enddate") & "</td>"
                    asset_type_data=asset_type_data & "                     <td style=""text-align:right;padding-left:15px;"">" & FormatNumber(dbrs("numbofdaysvoid"),0) & "</td>"
                    asset_type_data=asset_type_data & "              </tr>"
                    asset_type_data=asset_type_data & "           </table>"
                    asset_type_data=asset_type_data & "   </td>"
                    asset_type_data=asset_type_data & "   <td style=""text-align:right;"">" 
                        asset_type_data=asset_type_data & FormatCurrency(dbrs("totalrent")) & "<span class=""image_link"" onclick=""open_info_div('" & dbrs("propertyid") & "div" & "','1')"">&nbsp;<img src=""../../myImages/info.gif"" alt=""information""  width=""15px"" height=""16px"" /></span></td>"
                    asset_type_data=asset_type_data & "   <td >" & FormatCurrency(dbrs("accumulatedrent")) & "</td>"
                    asset_type_data=asset_type_data & "</tr>"
                else
                    asset_type_data=asset_type_data & "<tr>"
                    asset_type_data=asset_type_data & "<td colspan=""7"" class=""thickline"">&nbsp;</td>"    
                    asset_type_data=asset_type_data & "</tr>" 
                    asset_type_data=asset_type_data & "<tr>" 
                    asset_type_data=asset_type_data & "   <td class=""fontweightbold"" style=""width:50px;"">&nbsp;</td>"
                    asset_type_data=asset_type_data & "   <td >&nbsp;</td>"
                    asset_type_data=asset_type_data & "   <td >&nbsp;</td>"
                    asset_type_data=asset_type_data & "   <td class=""fontweightbold"">"& dbrs("schemename") & "</td>"
                    asset_type_data=asset_type_data & "   <td class=""fontweightbold"" style=""padding-left:15px;"">" & FormatNumber(dbrs("numbofdaysvoid"),0) & "</td>"
                    asset_type_data=asset_type_data & "   <td style=""text-align:right;font-weight:bold;padding-right:12px;width:5px;"">" & FormatCurrency(dbrs("totalrent")) & "</td>"
                    asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & FormatCurrency(dbrs("accumulatedrent")) & "</td>"
                    asset_type_data=asset_type_data & "</tr>"
                end if
                dbrs.movenext()
    		    if dbrs.EOF then exit for
    				
			  next   
     	end if
 	    
   	    asset_type_data=asset_type_data & "<tr>"
        asset_type_data=asset_type_data & "<td colspan=""7"" class=""thinline"">&nbsp;</td>"    
        asset_type_data=asset_type_data & "</tr>" 
        
        if((intPage)*dbrs.pagesize+count > intRecordCount-1) then
            numbofrecordonpage=intRecordCount-1
        else
            numbofrecordonpage=(intPage)*dbrs.pagesize+count
        end if 
        
        asset_type_data=asset_type_data &  "</table></div><table class=""detail_data_table"" id=""paging"">"  
        asset_type_data=asset_type_data & "<tr><td colspan=""6"" style=""padding-top:10px;"">"
        asset_type_data=asset_type_data & "<span style=""margin-left:170px;text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('1');"">FIRST</span>&nbsp;&nbsp;"    
        asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & prevpage & "');"">PREV</span>&nbsp;&nbsp;" 
        asset_type_data=asset_type_data & "<span> Page " & intpage & " of " & intPageCount & ". Records: " & dbrs.PageSize * (intpage - 1) + 1 & "  to " & numbofrecordonpage & " of " & intRecordCount-1 & "</span>"  
        asset_type_data=asset_type_data & "&nbsp;&nbsp;<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & nextpage & "');"">NEXT</span>&nbsp;&nbsp;"
        asset_type_data=asset_type_data & "<span style=""text-decoration: underline;color:blue;cursor:hand;"" onclick=""movetopage('" & intPageCount & "');"">LAST</span>"    
        asset_type_data=asset_type_data & "</td></tr>" 
    else
        'default form is used  when the window is displayed first time with form_post=0
	    checkbox="<span style=""margin-left:535px""></span>"
	    Printtext="PRINT"
	    printbtnwidth=40
	    printtype=1
	    asset_type_data=asset_type_data & "<tr>"
	    asset_type_data=asset_type_data & "   <th style=""padding-right:20px;"">Asset Type</th>"
	    asset_type_data=asset_type_data & "   <th>Total Property Count</th>"
	    asset_type_data=asset_type_data & "   <th><3 weeks</th>"
	    asset_type_data=asset_type_data & "   <th>3 weeks  to <br/> <6 weeks</th>"
	    asset_type_data=asset_type_data & "   <th>6 weeks  to <br/> <6 months</th>"
	    asset_type_data=asset_type_data & "   <th>6 months  to <br/> <1 year</th>"
	    asset_type_data=asset_type_data & "   <th><span>1 year +</span></th>"
	    asset_type_data=asset_type_data & "   <th>Pending Termination</th>"
	    asset_type_data=asset_type_data & "</tr>"
        
        'response.Write("fprocessdate: " & processdate & "assetid: " & assetid & "status: " & status & "housingofficer: " & housingofficer & "postcode: " & postcode & "developmentid: " & developmentid & "localauthority: " & localauthority)
        
        dbcmd.CommandText = "[P_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS]"
        dbcmd.Parameters.Refresh
        dbcmd.Parameters(1)=processdate
        dbcmd.Parameters(2)=localauthority
        dbcmd.Parameters(3)=developmentid
        dbcmd.Parameters(4)=postcode
        dbcmd.Parameters(5)=patch
        dbcmd.Parameters(6) =assettypeid
        dbcmd.Parameters(7) =status
        set dbrs=dbcmd.execute
           
        if(not dbrs.eof) then	
                norecords =dbrs.recordcount
	            'response.write norecords
	    end if
	    
	        
	    count=0
	    
	    do while not dbrs.eof 
	        count=count+1
	        if(count=1 or count<=norecords or dbrs("description")="TOTAL") then
	            asset_type_data=asset_type_data & "<tr>"
	            asset_type_data=asset_type_data & "<td colspan=""8"" class=""thinline"">&nbsp;</td>"    
	            asset_type_data=asset_type_data & "</tr>"
	                
	            if(count=1) then 
	                asset_type_data=asset_type_data & "<tr>" 
	                asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("description") & "</td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:40px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','0','Total Property Count','" & dbrs("description")&"')"">" & dbrs("totalpropertycount") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertydetail('" & dbrs("assettypeid") & "','2','<3 weeks','" & dbrs("description") & "')"">" & dbrs("under3weeks") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','3','3 weeks  to <6 weeks','" & dbrs("description") & "')"">" & dbrs("morethan3weekslessthan6weeks") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','4','6 weeks  to <6 months','" & dbrs("description") & "')"">" & dbrs("morethan6weekslessthan6months") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','5','6 months  to <1 year','" & dbrs("description") & "')"">" & dbrs("morethan6monthslessthan1year") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','6','1 year +','" & dbrs("description") & "')"">" & dbrs("morethan1year") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td><span class=""linkstylependingtermination"" style=""padding-left:40px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','1','Pending Termination','" & dbrs("description") & "')"">" & dbrs("pendingterminations") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	             else
	                asset_type_data=asset_type_data & "<tr>" 
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""fontweightbold"">" & dbrs("description") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddingleftfortypx"">" & dbrs("totalpropertycount") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold"">" & dbrs("under3weeks") & "</td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddinglefttenpx"">" & dbrs("morethan3weekslessthan6weeks") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddinglefttenpx"">" & dbrs("morethan6weekslessthan6months") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddinglefttenpx"">" & dbrs("morethan6monthslessthan1year") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddinglefttenpx"">" & dbrs("morethan1year") & "</span></td>"
	                asset_type_data=asset_type_data & "   <td class=""printbold""><span class=""paddingleftfortypx"">" & dbrs("pendingterminations") & "</span></td>"
	                asset_type_data=asset_type_data & "</tr>"
	             end if
	        else
	            asset_type_data=asset_type_data & "<tr>" 
	            asset_type_data=asset_type_data & "   <td class=""fontweightbold"">" & dbrs("description") & "</td>"
                asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:40px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','0','Total Property Count','" & dbrs("description") & "')"">" & dbrs("totalpropertycount") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" onclick=""propertydetail('" & dbrs("assettypeid") & "','2','<3 weeks','" & dbrs("description") & "')"">" & dbrs("under3weeks") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','3','3 weeks  to <6 weeks','" & dbrs("description") & "')"">" & dbrs("morethan3weekslessthan6weeks") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','4','6 weeks  to <6 months','" & dbrs("description") & "')"">" & dbrs("morethan6weekslessthan6months") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','5','6 months  to <1 year','" & dbrs("description") & "')"">" & dbrs("morethan6monthslessthan1year") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td><span class=""linkstyle"" style=""padding-left:10px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','6','1 year +','" & dbrs("description") & "')"">" & dbrs("morethan1year") & "</span></td>"
	            asset_type_data=asset_type_data & "   <td ><span class=""linkstylependingtermination"" style=""padding-left:40px;"" onclick=""propertydetail('" & dbrs("assettypeid") & "','1','Pending Termination','" & dbrs("description") & "')"">" & dbrs("pendingterminations") & "</span></td>"
	            asset_type_data=asset_type_data & "</tr>"
	        end if    
	        dbrs.movenext()
	    loop
   end if 

   asset_type_data=asset_type_data & "</table>" & print_all_data
  
   
   top_data=top_data & "</p><p class=""top_processdate""><span class=""printbold"">Date:</span>" & displaydate & ""
   top_data=top_data & checkbox
   top_data=top_data & "<input type=""button""  id=""back_button"" name=""btn_back"" value=""<<Back"" class=""RSLButton"" style="" text-align:right;width:50px;margin-top:3px;"" onclick=""back();"" />"
   top_data=top_data & "<input type=""button""  id=""print_button"" name=""btn_print"" value="""& Printtext & """ class=""RSLButton"" style="" margin-left:10px;width:" & printbtnwidth &"px;margin-top:3px;"" onclick=""print_data('" & printtype & "');"" /></p>" 
   
   bottom_data=asset_type_data
   
   'dbrs.Close()
   'dbcmd.Cancel()
%>
<html>
    <head>
        <title>Void Analysis</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
        <style type="text/css" media="screen">
           /*CSS for screen display*/
           #top_content{border-style: solid;border-color:#133e71;border-width:1px;padding-top:0;padding-bottom:5px;margin-bottom:4px;z-index:1;} 
           #bottom_content{border-style: solid;border-color:#133e71;border-width:1px 1px 0px 1px;padding-top:0;padding-bottom:0px;margin-bottom:0px;height:200px;z-index:1;} 
           .border_left{height:67px;margin-right:67px;border-bottom:solid 1px;border-left:solid 1px ;border-color:#133e71;}
           .bottom_right{padding-top:0px;margin:0;margin-left:0px;margin-right:0px; height:67px;}
           .property_data_table { background-color: #FFFFFF; margin-left:10px;padding-bottom:10px;width:730px;margin-bottom:3px; z-index:1}
           .property_data_table th{padding-left:13px;font-size:10px;font-weight:bold;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-top:5px;margin-bottom:0px;padding-bottom:0px;}
           .property_data_table td{padding-top:0px;padding-left:13px;font-size:10;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-right:4px;}
           .property_data_table img{padding-left:3px;}
           .property_data_table_inner{background-color: #FFFFFF; z-index:1}
           .property_data_table_inner td{margin-top:0px;padding-top:0px;padding-left:18px;font-size:10;font-family : verdana, Geneva, Arial, Helvetica, sans-serif;padding-right:4px;}
           .thinline{border-style: solid;border-color:#gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}  
           .thickline{border-style: solid;border-color:light blue;border-width:0 0 2px 0; padding:0 0 0 0; margin:0 0 0 0;}  
           .linkstyle{text-decoration: underline;color:blue;cursor:hand;}
           .linkstylependingtermination{text-decoration: underline;color:darkblue;cursor:hand;}
           .image_link{cursor:hand;}
            .fontweightbold{font-weight:bold;}
            #print_all_table{display:none;}
            .top_trail{margin-left:5px;margin-bottom:0px;margin-top:2;}
            .top_processdate{margin-top:0px;margin-left:5px;padding-bottom:0px;margin-bottom:0px;}
            #float_div{position:absolute;top:100px; left:100px; z-index:10;background-color: #D3D3D3;width:180px;margin:0px 0px 0px 0px;padding-right:3px;}
            .paddinglefttenpx{padding-left:10px;}
            .paddingleftfortypx{padding-left:40px;}
         </style>
        <style type="text/css" media="print">
            /*CSS for printing page*/
            #top_content{border:none;}
            #bottom_content{border:none;}
            #paging{display:none;}
            h1{font-size:14px;}
            th{font-size:12px;padding:0px;margin:0px;}
            td{padding-left:20px;padding-top:10px;}
            #back_button{display:none;}
            #print_button{display:none;}
            #includeoccupancy{display:none;}
            label{display:none;}
            .top_processdate{padding-top:10px;padding-bottom:10px;}
             #float_div{display:none;}
            .top_trail{font-size:14px;font-weight:bold;}
            .printbold{font-weight:bold;}
            .paddinglefttenpx{padding-left:10px;}
            .paddingleftfortypx{padding-left:40px;}
        </style>
    </head>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript">	
    //function for back button
    function back()
    {
        if(rslform.form_post.value==1) 
        {
          window.resizeTo(780,500);
          rslform.form_post.value=0; 
          rslform.submit();
        }
        
        else 
        {
           window.close();
        }  
    }
    
    function movetopage(pageno)
    {
        rslform.page.value=pageno;
        rslform.submit();
    }
  //function for print data
  function print_data(printtype)
  {
       
    if(printtype==1)
    {
       document.getElementById("PortfolioPropertyDataTable").style.display="block";
       window.print();
    }
    else
    {
        document.getElementById("PortfolioPropertyDataTable").style.display="none";
        window.print();
        document.getElementById("PortfolioPropertyDataTable").style.display="block";
    }   
    
    
  }
  /*function to switch to second form*/
  function propertydetail(assetid,period,perioddescription,assetdescription)
  {
    //alert(assetid);
    //alert(assetdescription);
    window.resizeTo(780,650);
    rslform.form_post.value=1;
    if(rslform.assettypeid.value=="")
    {
        rslform.assettypeid2.value=assetid;
        rslform.assetdescriptiond2.value=assetdescription;
        
    }
    rslform.period.value=period;
    rslform.perioddescription.value=perioddescription;
    rslform.submit();
   }
  /*function for information window*/
  function open_info_div(divname,status)
  {
    if(status==1)
    {
        document.getElementById(divname).style.display="block";
    }    
    else
    {   
       document.getElementById(divname).style.display="none";
    }
  }
 function propertyredirect(propertyid)
    {
        
        window.opener.location="../PRM.asp?PropertyID="+propertyid;
        window.close();
   }
  </script>	

    <body bgcolor="#FFFFFF" onload="window.focus()" >
        <form name="rslform" method="post"  action="" >
           <div id="top_content">
                <%=top_data %>
            </div>
            <%=float_div%>
            <div >
                <div id="bottom_content" ><%=bottom_data %></div> 
                <div id="bottom_right" style="background: url(../../myImages/corner_pink_white_big.gif) 100% 100% no-repeat;">
                    <div class="border_left"></div>
                </div>
            </div>
            <input type="hidden" name="form_post" value="<%=form_post %>" />
            <input type="hidden" name="localauthority" value="<%=localauthority %>" />
            <input type="hidden" name="developmentid" value="<%=developmentid%>" />
            <input type="hidden" name="patch" value="<%=patch %>" />
            <input type="hidden" name="processdate" value="<%=processdate%>" />
            <input type="hidden" name="postcode" value="<%=postcode%>" />
            <input type="hidden" name="statusid" value="<%=statusid %>" />
            <input type="hidden" name="statusdescription" value="<%=statusdescription %>" />
            <input type="hidden" name="assettypeid" value="<%=assettypeid%>" />
            <input type="hidden" name="assetdescription" value="<%=assetdescription%>" />
            <input type="hidden" name="assettypeid2" value="" />
            <input type="hidden" name="assetdescriptiond2" value="" />
            <input type="hidden" name="period" value="<%=period %>" />
            <input type="hidden" name="perioddescription" value="<%=perioddescription %>" />
            <input type="hidden" name="page" value="<%=page%>" />
          </form>
        
    </body>
   
</html>

