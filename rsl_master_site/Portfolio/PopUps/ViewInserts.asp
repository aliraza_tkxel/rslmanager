<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
supplier = Request("supplier")
repair = Request("repair")
frequency = Request("txt_FREQUENCY")
cycle = Request("txt_CYCLE")
startdate = FormatDateTime(Request("txt_STARTDATE"),1)

Call OpenDB()


DateString = ""
SQL = "EXEC CYC_REPAIRS_INSERT_CHECK '" & startdate & "'," & cycle & "," & frequency
Response.Write SQL
Counter = 0
Call OpenRs(rsC, SQL)
while NOT rsC.EOF 
	DateString = DateString & "<li>" & rsC("STARTDATE")
	Counter = Counter + 1
	rsC.moveNext
wend

Call CloseRs(rsC)
Call CloseDB()
%>
<html>
<head>
    <title>RSL Portfolio --> Check Cyclical Repairs</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type="text/javascript">
        function ReturnData() {
            parent.FinalWarning.innerHTML = WarningData.innerHTML
        }
    </script>
</head>
<body bgcolor="#FFFFFF" onload="ReturnData()" marginheight="0" leftmargin="10" topmargin="10"
    marginwidth="0">
    <div id="WarningData">
        <table align="center">
            <tr>
                <td>
                    <br>
                    <table cellpadding="4" cellspacing="1">
                        <tr>
                            <td bgcolor="#133e71" style='color: white; font-weight: bold'>
                                Repair :
                            </td>
                            <td>
                                <%=Repair%>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#133e71" style='color: white; font-weight: bold'>
                                Supplier :
                            </td>
                            <td>
                                <%=Supplier%>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#133e71" style='color: white; font-weight: bold'>
                                Cycle/Frequency :
                            </td>
                            <td>
                                <b>Every</b>
                                <%=cycle%>
                                <b>month(s) for</b>
                                <%=frequency%>
                                <b>occurrence(s).</b>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#133e71" style='color: white; font-weight: bold'>
                                Start Date :
                            </td>
                            <td>
                                <%=startdate%>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <% if Counter > 0 then %>
                    The following initial cycles will be entered into the system for the cyclical repair:<br>
                    <ul>
                        <b>
                            <%=DateString%>
                        </b>
                    </ul>
                    <b>Note:</b> One cyclical work order will be entered for each date above.
                    <% else %>
                    No initial cycles will be entered for the cyclical repair.
                    <% end if %>
                    <br>
                    If all items are correct then click 'SAVE' to save the cyclical repair, otherwise
                    click 'CANCEL'.
                    <br>
                    <table width="500px">
                        <tr>
                            <td>
                                <input type="button" onclick="CancelSave()" value=" CANCEL " class="RSLButton">
                                <input type="button" onclick="SaveForm()" value=" SAVE " class="RSLButton">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
