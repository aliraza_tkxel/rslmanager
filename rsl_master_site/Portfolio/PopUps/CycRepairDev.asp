<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Function CylicalNumber (strWord)
	CylicalNumber = "<b>CYC</b> " & characterPad(strWord,"0", "l", 6)
End Function

OpenDB()

BLOCK_ID = ""
DevelopmentID = Request("DEVID")
SQL = "SELECT DEVELOPMENTNAME FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & DevelopmentID
Call OpenRs(rsDEV,SQL)
THE_DEV_NAME = rsDEV("DEVELOPMENTNAME")
Call CloseRs(rsDEV)
%>
<%
	Dim lstwarrantytype, lstcontractor, pmstring, defectsperiod, lstareaitem, lst_block, str_selbox

	ACTION = "NEW"
	
	OpenDB()
	
		'THIS SQL GETS THE CURRENT CYCLICAL REPAIR FOR THE DEVELOPMENT
		'IT ALSO GETS THE CURRENTSTATUS OF EACH CYCLICAL REPAIR AND THE NEXT ANTICIPATED CYCLE.
		SQL = "SELECT  p.PMID, ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, PM_ACTIVE, " &_
					"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
					"ISNULL(P.CYCLE,'') AS CCYCLE, P.FREQUENCY, " &_
					"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_
					"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
					"ISNULL(P.NOTES ,'') AS CNOTES, " &_
					"ISNULL(S.NAME,'') AS CONTRACTOR, " &_
					"R.DESCRIPTION, " &_
					"CURRENTSTATUS = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'INACTIVE' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'COMPLETED' " &_					
					"ELSE 'ACTIVE' " &_
					"END, " &_
					"NEXTCYCLE = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'N/A' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'N/A' " &_					
					"WHEN LASTEXECUTED = '1 JULY 1970' THEN 'NOT STARTED' " &_					
					"ELSE CONVERT(NVARCHAR, DATEADD(M, P.CYCLE, LASTEXECUTED), 103) " &_					
					"END, " &_
					"(SELECT ISNULL(SUM(REPAIRNETCOST),0) AS REPAIRNETCOST FROM P_PLANNEDINCLUDES PINC WHERE PINC.PMID = P.PMID) AS REPAIRNETCOST " &_
			  	"FROM 	P_PLANNEDMAINTENANCE P " &_
			  		"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
					"INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
					"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
				"WHERE 	P.BLOCK IS NULL AND P.DEVELOPMENTID = " & DevelopmentID & " " &_
				"ORDER BY CURRENTSTATUS ASC, P.PMID DESC "	
	'response.write SQL
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		WHILE NOT rsLoader.EOF
			CURRENTSTATUS = rsLoader("CURRENTSTATUS")
			//DEPENDING ON WHAT STATUS THE CYCLCIAL REPAIR IS , DICTATES WHAT ACTIONS CAN BE APPLIED TO EACH CYCLICAL REPAIR.
			IF (CURRENTSTATUS = "INACTIVE") THEN
				pmstring = pmstring &_
							"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "' style='text-decoration:line-through;color:red'>" &_
								"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
								"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
								"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
								"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
								"<TD bgcolor=white TITLE=""Inactive Cyclical Repair"" width=500 align=center><font color=red>INA</font></TD></TR>"						
			ELSEIF (CURRENTSTATUS = "COMPLETED") THEN
				pmstring = pmstring &_
							"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "' style='color:gray'>" &_
								"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
								"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
								"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
								"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
								"<TD bgcolor=white TITLE=""Completed Cyclical Repair"" width=500 align=center><font color=blue>CO</font></TD></TR>"
			ELSE
				pmstring = pmstring &_
							"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "' style='cursor:hand'>" &_
								"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
								"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
								"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
								"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
								"<TD bgcolor=white width=500 TITLE=""Delete/De-activate Cyclical Repair"" align=center onclick=""DeleteMe(" & rsLoader("PMID") & ")""><font color=red>DEL</font></TD></TR>"						
			
			END IF
			rsLoader.MoveNext
		wend
		
	else
		pmstring = pmstring & "<tfoot><Tr><TD colspan=6 align=center>No Cyclical Repairs exist for this development</TD></TR></tfoot	>" 
	end if
	CloseRs(rsLoader)
	
	CALL BuildSelect(lstContractors, "sel_CONTRACTOR", "S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select", NULL, NULL, "textbox200", " tabindex=1 style='width:300px' " & DisableAll & "")
	get_repairs()
	CloseDB()

	'THIS FUNCTION BUILDS A LIST OF CYCLICAL REPAIRS
	Function get_repairs()
		
		SQL = "SELECT * FROM R_ITEMDETAIL WHERE MAINTENANCECODE LIKE 'C%' AND AREAITEMID NOT IN(38,822) ORDER BY DESCRIPTION"		
		Call OpenRs(rsSet, SQL)
		' BEWARE, THE CODE 822 IS HARD CODED HERE AND IS A DUMMY ROW IN THE TABLE
		str_selbox = " <select class='textbox200' tabindex=1 name='sel_REPAIR' id='sel_REPAIR'  style='width:300px'><OPTION VALUE=''>Select Item</option>"
		While not rsSet.EOf 
			str_selbox = str_selbox & "<OPTION VALUE='" & rsSet("ITEMDETAILID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.movenext()
		Wend 
		str_selbox = str_selbox & "</SELECT>"
	End Function

	
%>
<html>
<head>
    <title>Development Cyclical Repairs</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<script type="text/javascript" src="/js/preloader.js"></script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type="text/javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript">

    var FormFields = new Array();

    FormFields[0] = "txt_STARTDATE|STARTDATE|DATE|Y"
    FormFields[1] = "txt_CYCLE|CYCLES|INTEGER|Y"
    FormFields[2] = "sel_REPAIR|REPAIR|SELECT|Y"
    FormFields[3] = "sel_CONTRACTOR|CONTRACTOR|SELECT|Y"
    FormFields[4] = "txt_FREQUENCY|FREQUENCY|INTEGER|Y"
    FormFields[5] = "txt_CONTRACTSTARTDATE|Contract Start Date|DATE|Y"

    var THEPRICELIST = 0

    //THIS FUNCTION OPENS UP THE POPUP IS_OK2, WHICH GETS A LIST OF ALL PROPERTIES THAT THE CYCLICAL REPAIR CAN 
    //BE APPLIED TO. THE USER WILL THEN SELECT 
    function check_is_ok() {

        if (!checkForm()) return false;
        var str_request = "ISOK2.ASP?COMMENCE=" + RSLFORM.txt_CONTRACTSTARTDATE.value + "&START=" + RSLFORM.txt_STARTDATE.value + "&BLOCK=<%=BLOCK_ID%>&DEVELOPMENT=<%=DevelopmentID%>&REPAIR=" + RSLFORM.sel_REPAIR.value
        var answer = window.showModalDialog(str_request, document, "dialogHeight:600px; dialogWidth:600px");
        //window.open(str_request, "display","width=600,height=600,left=100,top=200");

        if (answer != "FAILED") {
            tArray = answer.split("|SSSSS|")
            document.getElementById("IncludeProperties").value = tArray[0]
            document.getElementById("hid_REPAIRPRICES").value = tArray[1]

            //show final warnings
            RSLFORM.sel_CONTRACTOR.style.visibility = "hidden"
            RSLFORM.sel_REPAIR.style.visibility = "hidden"
            FinalWarning.style.display = "block"
            FinalWarning.innerHTML = "<BR><BR><BR><CENTER style='font-size:22px;font-weight:bold'>Checking Cyclical Repair<br>Please Wait...</CENTER>"

            rSupplier = RSLFORM.sel_CONTRACTOR.options[RSLFORM.sel_CONTRACTOR.selectedIndex].text
            rRepair = RSLFORM.sel_REPAIR.options[RSLFORM.sel_REPAIR.selectedIndex].text

            RSLFORM.action = "ViewInserts.asp?supplier=" + rSupplier + "&repair=" + rRepair
            RSLFORM.target = "frm_cyclrep";
            RSLFORM.submit();


            RSLFORM.hid_Action.value = "NEW"
            //SaveForm();
        }
    }

    function SaveForm() {
        FinalWarning.innerHTML = "<BR><BR><BR><CENTER style='font-size:22px;font-weight:bold'>Saving New Cyclical Repair<br>Please Wait...</CENTER>"
        RSLFORM.target = "frm_cyclrep";
        RSLFORM.action = "../serverside/cyclerep_srv.asp";
        RSLFORM.submit();

    }

    function CancelSave() {

        RSLFORM.sel_CONTRACTOR.style.visibility = "visible"
        RSLFORM.sel_REPAIR.style.visibility = "visible"
        FinalWarning.style.display = "none"
        FinalWarning.innerHTML = "<BR><BR><BR><CENTER>Please Wait...</CENTER>"

    }

    //DELETES THE SELECTED CYCLICAL REPAIR	
    function DeleteMe(WarID) {

        event.cancelBubble = true;
        answer = window.confirm("Are you sure you wish to delete this repair cycle?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
        if (answer) {
            RSLFORM.hid_Action.value = "DELETE"
            RSLFORM.hid_PMID.value = WarID
            RSLFORM.action = "../serverside/cyclerep_srv.asp";
            RSLFORM.target = "frm_cyclrep";
            RSLFORM.submit()
        }
    }

    function LoadForm(ID) {

        var str_request = "POPUPS/cycdetails.asp?rmid=" + ID;
        window.showModalDialog(str_request, "display", "width=300,height=600,left=100,top=200");

    }

    function SetButtons(Code) {

        if (Code == "AMEND") {
            document.getElementById("AddButton").style.display = "none"
            document.getElementById("AmendButton").style.display = "block"
        }
        else {
            document.getElementById("AddButton").style.display = "block"
            document.getElementById("AmendButton").style.display = "none"
        }
    }

    function AmendForm() {

        RSLFORM.hid_Action.value = "AMEND"
        if (!checkForm()) return false;
        RSLFORM.action = "../serverside/cyclerep_srv.asp";
        RSLFORM.target = "frm_cyclrep";
        RSLFORM.submit()
    }

</script>
<body bgcolor="#FFFFFF" margintop="0" marginheight="0" topmargin="6" onload="window.focus()"
    style='background: url()'>
    <div id="FinalWarning" style='display: none; background-color: white; position: absolute;
        width: 700px; height: 410px; left: 50px; top: 45px; border: 1px solid red; overflow: auto'>
    </div>
    <table>
        <tr>
            <td width="10">
            </td>
            <td>
                <table width="764" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
                    <tr>
                        <td height="10" width="92" nowrap>
                            <img src="/Portfolio/images/cyc_open.gif" width="126" height="20" alt="" border="0" />
                        </td>
                        <td height="10" width="8" nowrap style='border-bottom: 1px solid #133e71;'>
                            &nbsp;
                        </td>
                        <td width="100%" style='border-bottom: 1px solid #133e71;' align="right">
                            Development : <b>
                                <%=THE_DEV_NAME%></b>
                        </td>
                    </tr>
                </table>
                <form name="RSLFORM" id="RSLFORM" method="post">
                <table width="744" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
                    <tbody>
                        <tr>
                            <td height="370" colspan="3" valign="top" style="border-left: 1px solid #133e71;
                                border-right: 1px solid #133e71; border-bottom: 1px solid #133e71">
                                <div style='overflow: auto; padding: 5px;' class='TA'>
                                    <table width="750" border="0" cellpadding="2" cellspacing="0" style="border: 1px solid #133E71;">
                                        <tr style='height: 7px'>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Repair Details :
                                            </td>
                                            <td>
                                                <%=str_selbox%><img src="/js/FVS.gif" name="img_REPAIR" id="img_REPAIR" width="15px"
                                                    height="15px" border="0">
                                            </td>
                                            <td>
                                                Occurences:
                                            </td>
                                            <td>
                                                <input type="TEXT" name="txt_FREQUENCY" id="txt_FREQUENCY" class='TEXTBOX' style='width: 180px'
                                                    maxlength="10" tabindex="2">
                                                <image src="/js/FVS.gif" name="img_FREQUENCY" id="img_FREQUENCY" width="15px" height="15px"
                                                    border="0">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Contractor :
                                            </td>
                                            <td>
                                                <%=lstContractors%><image src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR"
                                                    width="15px" height="15px" border="0">
                                            </td>
                                            <td>
                                                Occurs every:
                                            </td>
                                            <td>
                                                <input type="TEXT" name="txt_CYCLE" id="txt_CYCLE" class='TEXTBOX' style='width: 90px'
                                                    maxlength="10" tabindex="2">
                                                &nbsp; <b>MONTH(s)</b>
                                                <img src="/js/FVS.gif" name="img_CYCLE" id="img_CYCLE" width="15px" height="15px"
                                                    border="0">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Contract Start Date :
                                            </td>
                                            <td>
                                                <input type="text" name="txt_CONTRACTSTARTDATE" id="txt_CONTRACTSTARTDATE" class='TEXTBOX'
                                                    style='width: 180px' maxlength="10" tabindex="1">
                                                <img src="/js/FVS.gif" name="img_CONTRACTSTARTDATE" id="img_CONTRACTSTARTDATE" width="15px"
                                                    height="15px" border="0">
                                            </td>
                                            <td>
                                                Notes:
                                            </td>
                                            <td rowspan="3" valign="top">
                                                <textarea style='overflow: HIDDEN; width: 180px' class='textbox200' rows="4" name="txt_NOTES"
                                                    id="txt_NOTES" tabindex="2"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1st Occurence Date :
                                            </td>
                                            <td>
                                                <input type="TEXT" name="txt_STARTDATE" id="txt_STARTDATE" class='TEXTBOX' style='width: 180px'
                                                    maxlength="10" tabindex="1">
                                                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                                    border="0">
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right" valign="top" nowrap>
                                                <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                                                <input type="hidden" name="sel_BLOCK" id="sel_BLOCK" value="<%=BLOCK_ID%>">
                                                <input type="hidden" name="IncludeProperties" id="IncludeProperties">
                                                <input type="button" class="RSLButton" style='width: 70px' value=" RESET " onclick="RSLFORM.reset()">
                                                <input type="button" class="RSLButton" style='width: 70px' value=" SAVE. " onclick="check_is_ok()"
                                                    name="AddButton" id="AddButton">
                                                <input type="button" class="RSLButton" value=" AMEND " onclick="AmendForm()" name="AmendButton"
                                                    id="AmendButton" disabled style='display: none; width: 70px'>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style='overflow: auto; padding: 5px;' class='TA'>
                                    <table cellspacing="0" cellpadding="0" border="0" style='border-collapse: collapse;
                                        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71'
                                        width="750" height="300">
                                        <tr bgcolor='#133E71' style='color: #FFFFFF'>
                                            <td height="23" width="90" nowrap>
                                                <b>&nbsp;Start Date</b>
                                            </td>
                                            <td width="410" nowrap>
                                                <b>&nbsp;Repair</b>
                                            </td>
                                            <td width="100" nowrap>
                                                <b>&nbsp;Next Cycle</b>
                                            </td>
                                            <td width="100" nowrap>
                                                <b>&nbsp;Last Cycle</b>
                                            </td>
                                            <td width="500">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" height="100%" colspan="6">
                                                <div id="CYCDATA" name="CYCDATA" style='height: 260; width: 750; overflow: auto'
                                                    class='TA'>
                                                    <table cellspacing="0" cellpadding="3" border="1" style='behavior: url(../../Includes/Tables/tablehl.htc);
                                                        border-collapse: collapse' slcolor='' hlcolor='STEELBLUE' width="100%">
                                                        <%=pmstring%>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <input type="hidden" name="hid_PMID" id="hid_PMID" value="<%=PMID%>">
                                <input type="hidden" name="hid_DEVELOPMENTID" id="hid_DEVELOPMENTID" value="<%=DevelopmentID%>">
                                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>">
                                <input type="hidden" name="hid_USERID" id="hid_USERID" value="<%=Session("USERID")%>">
                                <input type="hidden" name="hid_REPAIRPRICES" id="hid_REPAIRPRICES" value="">
                            </td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </td>
        </tr>
    </table>
    <iframe src="/secureframe.asp" name="frm_cyclrep" id="frm_cyclrep" width="400px"
        height="400px" style='display: none;'></iframe>
</body>
</html>
