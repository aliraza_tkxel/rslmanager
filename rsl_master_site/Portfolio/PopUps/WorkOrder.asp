<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
    Dim DefaultOrg, asbestosString
    DefaultOrg = 2129

	Function get_property()
		'THIS SECTION GETS THE PROPERTY FOR INSERT, AND ALSO THE PATCH NAME FOR ENTRY
		str_sql = "SELECT P.PROPERTYID, E.LOCATION FROM P__PROPERTY P LEFT JOIN E_PATCH E ON E.PATCHID = P.PATCH WHERE P.PROPERTYID = '" & property_id & "'"
		Call OpenRs(rsSet, str_sql)
		If Not rsSet.EOF Then
			patch = rsSet("LOCATION")
		Else
			property_id = -1
			patch = "UNKNOWN"
		End If
		Call CloseRs(rsSet)
	End Function

    Function get_property_asbestos()
        str_sql =  " SELECT PL.ASBRISKLEVELID AS RISKLEVELID, PL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL, " &_
                   " P.ASBESTOSID AS ELEMENTID, P.RISKDESCRIPTION AS ELEMENTDISCRIPTION ,PRI.ASBRISKID AS RISK, " &_
                   " CASE P.OTHER WHEN 0 THEN NULL ELSE P.RISKDESCRIPTION END AS OTHER " &_
                   " FROM P_ASBRISK PRI, P_PROPERTY_ASBESTOS_RISKLEVEL PR " &_
                   " LEFT JOIN  P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID = PAR.PROPASBLEVELID " &_
                   " INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID   " &_
                   " INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID  " &_
                   " WHERE PAR.ASBRISKID = PRI.ASBRISKID AND PR.PROPERTYID = '" & property_id & "'" &_
                   " ORDER BY PL.ASBRISKLEVELID, P.ASBESTOSID"
        Call OpenRs(rsSet, str_sql)
        If not rsSet.eof then
        asbestosString = "<table cellpadding='2' cellspacing='0' border='0' width='730' style='border:1px solid black; border-bottom:none'>"
        asbestosString = asbestosString & "<tr bgcolor='#133E71' style='color:white'><td colspan='6'><b>ASBESTOS INFORMATION</b></td></tr>"
	    asbestosString = asbestosString & "<tr><td colspan=""2"">Risk Level : <b>" & rsSet("RISKLEVEL") & "</b></td></tr>"
        asbestosString = asbestosString & "<tr><td colspan=""2""><u>Risk - Element</u></td></tr>"
        asbestosString = asbestosString & "<tr><td colspan=""2""><div style='overflow: auto; height: 90; padding: 5px;' class='TA'><table>"
        counter = 1
			While not rsSet.eof
                If counter Mod 2 = 1 Then
                    asbestosString = asbestosString & "<tr>"
                End If
                asbestosString = asbestosString & "<td>" & rsSet("RISK") & " - " &  rsSet("ELEMENTDISCRIPTION") & "</td>"
 				If counter Mod 2 = 0 Then
                    asbestosString = asbestosString & "</tr>"	
                End If
                counter = counter + 1
				rsSet.movenext
			Wend
        asbestosString = asbestosString & "</table></tr></td></div>"
		asbestosString = asbestosString & "</table>"
        End If
        Call CloseRs(rsSet)

    End Function

	Dim property_id, patch

	property_id = Request("propertyid")

	Call OpenDB()

	'NEED TO FIND THE EMPLOYEE LIMIT FOR THE USER
	SQL = "SELECT ISNULL(EMPLOYEELIMIT,0) AS EMPLOYEELIMIT FROM E_JOBDETAILS WHERE EMPLOYEEID = " & SESSION("USERID")
	Call OpenRs(rsEmLimit, SQL)
	If (NOT rsEmLimit.EOF) Then
		EmployeeLimit = rsEmLimit("EMPLOYEELIMIT")
		DisableAll = ""
	Else
		DisableAll = " disabled='disabled' "
		EmployeeLimit = -1
	End If
	Call CloseRs(rsEmLimit)

	Call get_property()
    Call get_property_asbestos()

	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select", DefaultOrg, NULL, "textbox200", " style='width:300px' tabindex=1 " & DisableAll & "")
	Call BuildSelect(lst_location, "sel_LOCATION", "R_ZONE WHERE ACTIVE = 1 AND ZONEID NOT IN (11,12)", "ZONEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", " tabindex=1 onchange='select_change(1)' style='width:300px' " & DisableAll & " ")
	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:117' tabindex=3")
	Call BuildSelect(lstNatures, "sel_NATURE", "C_NATURE WHERE ITEMID = 1 AND ITEMNATUREID in (20,21,34,35,36,37,38,39,40)", "ITEMNATUREID, DESCRIPTION", "ITEMNATUREID", "Please Select", NULL, NULL, "textbox200", " style='width:300' tabindex='1' onchange='SetTitle();ResetData()' ")	
%>
<html>
<head>
    <title>Work Order</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();

        function SetChecking(Type) {
            FormFields.length = 0
            if (Type == 1) {
                FormFields[0] = "sel_LOCATION|Location|SELECT|Y"
                FormFields[1] = "sel_ITEM|Item|SELECT|Y"
                FormFields[2] = "sel_ELEMENT|Element|SELECT|Y"
                FormFields[3] = "sel_REPAIR|Repair Details|SELECT|Y"
                FormFields[4] = "txt_NOTES|Notes|TEXT|N"
                FormFields[5] = "txt_NETCOST|Net Cost(�)|CURRENCY|Y"
                FormFields[6] = "txt_VAT|VAT (�)|CURRENCY|Y"
                FormFields[7] = "txt_GROSSCOST|Gross Cost(�)|CURRENCY|Y"
            }
            else if (Type == 2) {
                FormFields[0] = "sel_SUPPLIER|Supplier|SELECT|Y"
                FormFields[1] = "sel_NATURE|Repair Type|SELECT|Y"
            }
            else if (Type == 3) {
                FormFields[0] = "sel_LOCATION|Location|SELECT|Y"
                FormFields[1] = "sel_ITEM|Item|SELECT|Y"
                FormFields[2] = "sel_ELEMENT|Element|SELECT|Y"
                FormFields[3] = "sel_REPAIR|Repair Details|SELECT|Y"
                FormFields[4] = "txt_NOTES|Notes|TEXT|N"
                FormFields[5] = "txt_NETCOST|Net Cost(�)|CURRENCY|Y"
                FormFields[6] = "txt_VAT|VAT (�)|CURRENCY|Y"
                FormFields[7] = "txt_GROSSCOST|Gross Cost(�)|CURRENCY|Y"
            }
        }

        function SetTitle() {
            document.getElementById("hid_NATURETITLE").value = document.getElementById("sel_NATURE").options(document.getElementById("sel_NATURE").selectedIndex).text
        }

        function SaveOrder() {
            SetChecking(2)
            if (!checkForm()) return false
            if (parseFloat(document.getElementById("hiGC").value) < 0) {
                alert("Please enter some repairs before creating a Repair Work Order.");
                return false;
            }
         
            RSLFORM.target = "PRM_BOTTOM_FRAME";
            RSLFORM.action = "../ServerSide/WorkOrder_svr.asp?propertyid=<%=property_id%>";
            document.getElementById("SaveButton").disabled = true;
            RSLFORM.submit();
            window.close()
        }

        // source = 1 --> 'LOCATION changed'  = 2 --> 'ITEM changed', 3 --> ELEMENT CHANGED, 4 --> GET PRIORITY
        function select_change(int_source) {
            if (RSLFORM.sel_NATURE.value == "") {
                alert("Please select the nature of the work order before you continue.\nThis will filter the repair list to the appropriate items.");
                return false;
            }
            RSLFORM.target = "iRepairsFrame";
            RSLFORM.action = "../ServerSide/Selects_Srv.asp?source=" + int_source + "&natureid=" + RSLFORM.sel_NATURE.value;
            RSLFORM.submit();
        }

        function SetRecharge() {
            if (document.getElementById("chk_RECHARGEABLE").checked) {
                alert("You have set this repair to be  rechargeable to the tenant.\nThis will insert the cost of the repair (plus vat) onto the Tenants Rent Account.");
            }
        }

        RowCounter = 0
        var ExpenditureArray = new Array()
        var ExpValueArray = new Array()
        var CCArray = new Array()

        function AddRow(insertPos) {
            SetChecking(1)
            if (!checkForm()) return false
            GrossCost = document.getElementById("txt_GROSSCOST").value
            isQueued = 0

            //check wether the net cost has been changed in comparison to the database value,
            //if so then require the notes field to be filled in
            DataCost = document.getElementById("DatabaseCost").value
            if (parseFloat(GrossCost) != parseFloat(DataCost)) FormFields[4] = "txt_NOTES|Notes|TEXT|Y"
            //rerun the check form validation, incase the notes are required
            if (!checkForm()) return false;

            //check the costcentrer will not go over...
            //note: have to remove the total of any other entries aswell.
            CCLeft = document.getElementById("hid_TOTALCCLEFT").value
            if ((parseFloat(CCLeft) < parseFloat(GrossCost))) {
                alert("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the Cost Centre budget (�" + FormatCurrencyComma(CCLeft) + ") for the selected repair.\nTherefore this item cannot be entered onto the system.");
                return false;
            }

            //check the expenditure will not go over...
            ExpLeft = document.getElementById("hid_TOTALEXPLEFT").value
            if ((parseFloat(ExpLeft) < parseFloat(GrossCost))) {
                //			alert("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget (�" + FormatCurrencyComma(ExpLeft) + ") for the selected repair.\nTherefore this item cannot be entered onto the system.");
                //			return false;
                answer = confirm("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget (�" + FormatCurrencyComma(ExpLeft) + ") for the selected repair.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");
                if (!answer) return false
            }

            //check the employee is allowed to assign this to a contractor depending on their limit				
            EmployeeLimit = document.getElementById("hid_EMPLOYEELIMIT").value
            if ((parseFloat(EmployeeLimit) < parseFloat(GrossCost))) {
                result = confirm("The repair cost (�" + FormatCurrencyComma(GrossCost) + ") is more than your employee repair limit (�" + FormatCurrencyComma(EmployeeLimit) + ") for the selected repair.\nTherefore this item will be put in a queue for authorisation by a manager.\nIf you wish to continue click on 'OK'.\nOtherwise click on 'CANCEL'.");
                if (!result) return false;
                isQueued = 1
            }

            Rechargeable = 0
            if (document.getElementById("chk_RECHARGEABLE").checked == true)
                Rechargeable = 1

            RowCounter++
            RepairID = document.getElementById("sel_REPAIR").value
            RepairNotes = document.getElementById("txt_NOTES").value
            RepairName = document.getElementById("sel_REPAIR").options(document.getElementById("sel_REPAIR").selectedIndex).text
            NetCost = document.getElementById("txt_NETCOST").value
            VAT = document.getElementById("txt_VAT").value
            VatTypeID = document.getElementById("sel_VATTYPE").value
            VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text
            VatTypeCode = VatTypeName.substring(0, 1).toUpperCase()

            oTable = document.getElementById("RepairTable")
            for (i = 0; i < oTable.rows.length; i++) {
                if (oTable.rows(i).id == "EMPTYLINE") {
                    oTable.deleteRow(i)
                    break;
                }
            }

            oRow = oTable.insertRow(insertPos)
            oRow.id = "TR" + RowCounter
            oRow.style.cursor = "hand"

            DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + RepairID + "||<>||" + RepairNotes + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + isQueued + "||<>||" + Rechargeable + "\">"

            //THIS PART ADDS THE EXPENDITURE ID AND VALUE TO ARRAYS, INCASE ANOTHER REPAIR OF THE SAME TYPE IS SELECTED. 
            //SO A TRUE COMPARISON TO THE EXPENDITURE LEFT CAN BE MADE.
            ExpenditureArray[RowCounter] = document.getElementById("hid_EXPENDITUREID").value
            ExpValueArray[RowCounter] = GrossCost
            CCArray[RowCounter] = document.getElementById("hid_COSTCENTREID").value

            AddCell(oRow, RepairName + DATA, RepairNotes, "", "")
            AddCell(oRow, VatTypeCode, VatTypeName + " Rate", "center", "")
            AddCell(oRow, FormatCurrencyComma(NetCost), "", "right", "")
            AddCell(oRow, FormatCurrencyComma(VAT), "", "right", "")
            AddCell(oRow, FormatCurrencyComma(GrossCost), "", "right", "")
            DelImage = "<img title='Clicking here will remove this repair from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
            AddCell(oRow, DelImage, "", "center", "#FFFFFF")

            SetTotals(NetCost, VAT, GrossCost)
            ResetData()
        }

        function ResetData() {
            SetChecking(3)
            checkForm()
            RSLFORM.sel_ITEM.outerHTML = "<select class='textbox200' name='sel_ITEM' style='width:300px' disabled><OPTION VALUE=''>Select Location</option></select>";
            RSLFORM.sel_ELEMENT.outerHTML = "<select class='textbox200' name='sel_ELEMENT' style='width:300px' disabled><OPTION VALUE=''>Select Item</option></select>";
            RSLFORM.sel_REPAIR.outerHTML = "<select class='textbox200' name='sel_REPAIR' style='width:300px' disabled><OPTION VALUE=''>Select Element</option></select>";
            document.getElementById("txt_NOTES").value = ""
            document.getElementById("txt_PRIORITY").value = ""
            document.getElementById("txt_NETCOST").value = "0.00"
            document.getElementById("txt_GROSSCOST").value = "0.00"
            document.getElementById("chk_RECHARGEABLE").checked = false
            document.getElementById("sel_VATTYPE").selectedIndex = 0;
            document.getElementById("sel_LOCATION").selectedIndex = 0;
            document.getElementById("txt_VAT").value = "0.00";
            document.getElementById("txt_EMPLOYEELIMIT").value = "0.00";
            document.getElementById("txt_TOTALEXPLEFT").value = "0.00";
            document.getElementById("hid_EMPLOYEELIMIT").value = "0.00";
            document.getElementById("hid_EXPENDITUREID").value = "0.00";
            document.getElementById("hid_TOTALEXPLEFT").value = "0.00";
        }

        function SetTotals(iNE, iVA, iGC) {
            totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
            totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
            totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

            document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
            document.getElementById("hiVA").value = FormatCurrency(totalVAT)
            document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)

            document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
            document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
            document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)
        }

        function SetLimits(ExpLimit, CCLimit) {
            Ref = document.getElementsByName("ID_ROW")
            //this part removes the expenditure bits
            ExpID = document.getElementById("hid_EXPENDITUREID").value
            ExpLimit = parseFloat(ExpLimit)
            if (Ref.length) {
                for (i = 0; i < Ref.length; i++) {
                    if (ExpenditureArray[Ref[i].value] == ExpID)
                        ExpLimit -= parseFloat(ExpValueArray[Ref[i].value])
                }
            }
            //this part removes the costcentre part.
            CCID = document.getElementById("hid_COSTCENTREID").value
            CCLimit = parseFloat(CCLimit)
            if (Ref.length) {
                for (i = 0; i < Ref.length; i++) {
                    if (CCArray[Ref[i].value] == CCID)
                        CCLimit -= parseFloat(ExpValueArray[Ref[i].value])
                }
            }

            document.getElementById("txt_TOTALEXPLEFT").value = FormatCurrencyComma(ExpLimit)
            document.getElementById("hid_TOTALEXPLEFT").value = FormatCurrency(ExpLimit)
            document.getElementById("hid_TOTALCCLEFT").value = FormatCurrency(CCLimit)
        }

        function AddCell(iRow, iData, iTitle, iAlign, iColor) {
            oCell = iRow.insertCell()
            oCell.innerHTML = iData
            if (iTitle != "") oCell.title = iTitle
            if (iAlign != "") oCell.style.textAlign = iAlign
            if (iColor != "") oCell.style.backgroundColor = iColor
        }

        function DeleteRow(RowID, NE, VA, GR) {
            oTable = document.getElementById("RepairTable")
            for (i = 0; i < oTable.rows.length; i++) {
                if (oTable.rows(i).id == "TR" + RowID) {
                    oTable.deleteRow(i)
                    SetTotals(-NE, -VA, -GR)
                    break;
                }
            }

            if (oTable.rows.length == 1) {
                oRow = oTable.insertRow()
                oRow.id = "EMPTYLINE"
                oCell = oRow.insertCell()
                oCell.colSpan = 7
                oCell.innerHTML = "Please enter a Repair from above"
                oCell.style.textAlign = "center"
            }
            ResetData()
        }								
    </script>
</head>
<body onload="window.focus()">
    <table>
        <tr>
            <td>
                <form name="RSLFORM" method="post" action="">
                <table width="744" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
                    <tr>
                        <td height="10" width="92" nowrap="nowrap">
                            <img src="/myImages/Repairs/tab_work_order.gif" width="117" height="20" alt="" border="0" /></td>
                        <td height="10" width="8" nowrap="nowrap" style='border-bottom: 1px solid #133e71;'>
                            &nbsp;</td>
                        <td width="100%" style='border-bottom: 1px solid #133e71;' align="right">
                            Property Reference : <b>
                                <%=property_id%></b></td>
                    </tr>
                </table>
                <table width="744" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <td height="370" colspan="3" valign="top" style='border-left: 1px solid #133e71;
                            border-right: 1px solid #133e71; border-bottom: 1px solid #133e71'>
                            <div style='overflow: auto; padding: 5px' class='TA'>
                                <%=asbestosString %>
                                <table cellpadding="2" cellspacing="0" border="0" width="730" style='border: 1PX SOLID BLACK;
                                    border-bottom: none'>
                                    <tr bgcolor="#133E71" style='color: white'>
                                        <td colspan="6">
                                            <b>WORK ORDER INFORMATION</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px" nowrap="nowrap">
                                            Repair Type :
                                        </td>
                                        <td>
                                            <%=lstNatures%>
                                        </td>
                                        <td width="50">
                                            <img src="/js/FVS.gif" name="img_NATURE" id="img_NATURE" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td width="100">
                                            Your Limit :
                                        </td>
                                        <td>
                                            <input type="text" name="txt_EMPLOYEELIMIT" id="txt_EMPLOYEELIMIT" class="textbox"
                                                style="width: 117px; text-align: right" readonly="readonly" value="0.00" />
                                        </td>
                                        <td width="50">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Supplier :
                                        </td>
                                        <td>
                                            <%=lstSuppliers%>
                                        </td>
                                        <td width="50">
                                            <img src="/js/FVS.gif" name="img_SUPPLIER" id="img_SUPPLIER" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td title='Expenditure Left'>
                                            Expend. Left :
                                        </td>
                                        <td>
                                            <input type="text" name="txt_TOTALEXPLEFT" id="txt_TOTALEXPLEFT" class="textbox"
                                                style='width: 117px; text-align: right' readonly="readonly" value="0.00" />
                                        </td>
                                    </tr>
                                </table>
                                <table style='border: 1PX SOLID BLACK' cellpadding="2" cellspacing="0" border="0"
                                    width="730">
                                    <tr bgcolor="#133E71" style='color: white'>
                                        <td colspan="6">
                                            <b>REPAIR ITEM</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100">
                                            Location :
                                        </td>
                                        <td>
                                            <%=lst_location%>
                                        </td>
                                        <td width="50">
                                            <img src="/js/FVS.gif" name="img_LOCATION" id="img_LOCATION" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td width="100">
                                            Rechargeable :
                                        </td>
                                        <td>
                                            <input type="checkbox" name="chk_RECHARGEABLE" id="chk_RECHARGEABLE" onclick="SetRecharge()"
                                                value="1" disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Item :
                                        </td>
                                        <td>
                                            <select class="textbox200" name="sel_ITEM" id="sel_ITEM" style='width: 300px' disabled="disabled"
                                                tabindex="1">
                                                <option value="">Select Location</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" name="img_ITEM" id="img_ITEM" width="15px" height="15px" border="0"
                                                alt="" />
                                        </td>
                                        <td width="100">
                                            Patch :
                                        </td>
                                        <td>
                                            <input tabindex="-1" type="text" class="textbox200" name="txt_PATCH" id="txt_PATCH"
                                                style="width: 117px; background-color: #f5f5dc" readonly="readonly" value="<%=patch%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Element :
                                        </td>
                                        <td>
                                            <select class="textbox200" name="sel_ELEMENT" id="sel_ELEMENT" style="width: 300px"
                                                disabled="disabled" tabindex="1">
                                                <option value="">Select Item</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" name="img_ELEMENT" id="img_ELEMENT" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td>
                                            Net Cost � :
                                        </td>
                                        <td>
                                            <input tabindex="2" type="text" class="textbox200" name="txt_NETCOST" id="txt_NETCOST"
                                                style='width: 117px; text-align: right' value="0.00" onblur="SetChecking();TotalBoth();ResetVAT()" />
                                        </td>
                                        <td width="50">
                                            <img src="/js/FVS.gif" name="img_NETCOST" id="img_NETCOST" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap">
                                            Repair Details :
                                        </td>
                                        <td>
                                            <select class="textbox200" name="sel_REPAIR" style='width: 300px' disabled="disabled"
                                                tabindex="1">
                                                <option value="">Select Element</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" name="img_REPAIR" id="img_REPAIR" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td>
                                            VAT Type :
                                        </td>
                                        <td>
                                            <%=lstVAT%>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" name="img_VATTYPE" id="img_VATTYPE" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Notes :
                                        </td>
                                        <td rowspan="2">
                                            <textarea tabindex="1" style="overflow: hidden; width: 300px" class='textbox200'
                                                rows="3" cols="6" name="txt_NOTES" id="txt_NOTES" <%=DisableAll%>></textarea>
                                        </td>
                                        <td rowspan="2">
                                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                        <td>
                                            VAT � :
                                        </td>
                                        <td>
                                            <input tabindex="-1" type="text" class="textbox200" name="txt_VAT" id="txt_VAT" style='width: 117px;
                                                text-align: right' value="0.00" readonly="readonly" />
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" name="img_VAT" id="img_VAT" width="15px" height="15px" border="0"
                                                alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                                        </td>
                                        <td>
                                            Total � :
                                        </td>
                                        <td valign="top">
                                            <input tabindex="-1" type="text" class="textbox200" name="txt_GROSSCOST" id="txt_GROSSCOST"
                                                style='width: 117px; text-align: right' value="0.00" readonly="readonly" />
                                        </td>
                                        <td valign="top">
                                            <img src="/js/FVS.gif" name="img_GROSSCOST" id="img_GROSSCOST" width="15px" height="15px"
                                                border="0" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Priority :
                                        </td>
                                        <td>
                                            <input type="text" tabindex="-1" class="textbox200" name="txt_PRIORITY" id="txt_PRIORITY"
                                                style="width: 300px; background-color: #f5f5dc" readonly="readonly" />
                                        </td>
                                        <td align="right" colspan="3">
                                            <input type="button" value=" RESET " class="RSLButton" onclick="ResetData()" tabindex="4"
                                                name="TheCancelButton" id="TheCancelButton" <%=DisableAll%> />
                                            <input type="button" value=" ADD " class="RSLButton" onclick="AddRow(-1)" tabindex="4"
                                                name="TheSaveButton" id="TheSaveButton" <%=DisableAll%> />
                                            <input type="hidden" name="hid_NATURETITLE" id="hid_NATURETITLE" />
                                            <input type="hidden" name="DatabaseCost" id="DatabaseCost" />
                                            <input type="hidden" name="hid_EMPLOYEELIMIT" value="" id="hid_EMPLOYEELIMIT" />
                                            <input type="hidden" name="hid_TOTALEXPLEFT" value="" id="hid_TOTALEXPLEFT" />
                                            <input type="hidden" name="hid_TOTALCCLEFT" value="" id="hid_TOTALCCLEFT" />
                                            <input type="hidden" name="hid_COSTCENTREID" value="" id="hid_COSTCENTREID" />
                                            <input type="hidden" name="hid_EXPENDITUREID" value="" id="hid_EXPENDITUREID" />
                                            <input type="hidden" name="hid_PROPERTYID" value="<%=property_id%>" id="hid_PROPERTYID" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style='overflow: auto; height: 200; padding: 5px;' class='TA'>
                                <table style='border: 1PX SOLID BLACK; behavior: url(/Includes/Tables/tablehl.htc)'
                                    cellspacing="0" cellpadding="3" width="730" id="RepairTable" slcolor='' hlcolor="steelblue">
                                    <thead>
                                        <tr bgcolor="#133E71" align="right" style='color: white'>
                                            <td height="20" align="left" width="425" nowrap="nowrap">
                                                <b>Repair:</b>
                                            </td>
                                            <td width="40" nowrap="nowrap">
                                                <b>Code:</b>
                                            </td>
                                            <td width="80" nowrap="nowrap">
                                                <b>Net (�):</b>
                                            </td>
                                            <td width="75" nowrap="nowrap">
                                                <b>VAT (�):</b>
                                            </td>
                                            <td width="80" nowrap="nowrap">
                                                <b>Gross (�):</b>
                                            </td>
                                            <td width="29" nowrap="nowrap">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="EMPTYLINE">
                                            <td colspan="7" align="center">
                                                Please enter a Repair from above
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style='border: 1PX SOLID BLACK; border-top: none' cellspacing="0" cellpadding="2"
                                    width="730">
                                    <tr bgcolor="#133E71" align="right">
                                        <td height="20" width="465" nowrap="nowrap" style='border: none; color: white'>
                                            <b>TOTAL : &nbsp;</b>
                                        </td>
                                        <td width="80" nowrap="nowrap" bgcolor="white">
                                            <input type="hidden" id="hiNC" name="hiNC" value="0" /><div id="iNC" style="font-weight: bold">
                                                0.00</div>
                                        </td>
                                        <td width="75" nowrap="nowrap" bgcolor="white">
                                            <input type="hidden" id="hiVA" name="hiVA" value="0" /><div id="iVA" style="font-weight: bold">
                                                0.00</div>
                                        </td>
                                        <td width="80" nowrap="nowrap" bgcolor="white">
                                            <input type="hidden" id="hiGC" name="hiGC" value="0" /><div id="iGC" style="font-weight: bold">
                                                0.00</div>
                                        </td>
                                        <td width="29" nowrap="nowrap" bgcolor="white">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style='overflow: auto; padding-left: 5px; padding-right: 5px; padding-bottom: 5px;'
                                class='TA'>
                                <table cellspacing="0" cellpadding="2" width="730">
                                    <tr>
                                        <td align="right">
                                            <input type="button" name="SaveButton" id="SaveButton" value=" SAVE ORDER " class="RSLButton"
                                                onclick="SaveOrder()" tabindex="4" /><br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
    <iframe name="iRepairsFrame" width="400" height="100" style="display: none"></iframe>
</body>
</html>
