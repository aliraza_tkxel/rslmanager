<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'=====================================================================================================
	' DECLERATION OF GLOBAL VARIABLES STARTS HERE HERE
	'=====================================================================================================

		CONST LAaddress = 5
		CONST BROADLAND = 2
		CONST TENANT 	= 1

		Dim tenancy_id, lett_id, FullAddress, str_letter, StringFullName
		Dim arrInserts, textFieldCounter, formFieldsArray, signature, d_cnt, i_cnt, t_cnt
		Dim intTo, intFrom, str_recipient_address, str_return_address, str_Dear, str_Footer,AmendCount, PROPERTYID
		Dim Generate_Forwarding_Address,strSig_BOX
		Dim lstTeams,lstUsers,Housing_Officer,Overide_date

	'=====================================================================================================
	' DECLERATION OF GLOBAL VARIABLES ENDS HERE HERE
	'=====================================================================================================

	OpenDB()

	'=====================================================================================================
	' REQUEST AND INITIALISE VARIABLES STARTS HERE
	'=====================================================================================================

		tenancy_id = Request("tenancyid")
		letter_id = Request("letterid")
        D1=Request.Item("Date1")
        D2=Request.Item("Date2")

		textFieldCounter = 0
		formFieldsArray = ""


		If Not tenancy_id <> "" Then tenancy_id = 0 End If
		If Not letter_id <> "" Then letter_id = 0 End If
		str_letter = build_letter_string()
	    Call entry()

	'=====================================================================================================
	' REQUEST AND INITIALISE VARIABLES ENDS HERE
	'=====================================================================================================


	'=====================================================================================================
	'  DETERMINE WHICH RECIPIENT ADDRESS TO USE STARTS HERE
	'=====================================================================================================

		If intTo = TENANT Then
			str_recipient_address = get_tenant_address()
			If Generate_Forwarding_Address = "true" Then
				str_recipient_address = GenerateForwardingAddress()
			End If
		Elseif intTo = LAaddress Then
			str_recipient_address = get_LA_address(intTo)
			str_Dear = "<br />Dear Sir/Madam"
		Else
			str_recipient_address = get_org_address(intTo)
			str_Dear = "<br />Dear Sir/Madam"
		End If

	'=====================================================================================================
	'  DETERMINE WHICH RECIPIENT ADDRESS TO USE ENDS HERE
	'=====================================================================================================

	'=====================================================================================================
	'  DETERMINE WHICH RETURN ADDRESS TO USE STARTS HERE
	'=====================================================================================================

		If intFrom = BROADLAND Then

			str_return_address = "<tr><td valign='top' bgcolor='white' style='height:65px'><img name='LogoImage' id='LogoImage' src='/Customer/Images/BHALOGOLETTER.gif' width='145' height='113' style='visibility:visible' alt='' /></td></tr>"
			str_Footer = 			"<table border='0' cellspacing='2' cellpadding='1'>" &_
										"<tr><td style='font:8;color:#133e71'>Broadland Housing Association</td></tr>" &_
										"<tr><td style='font:8;color:#133e71'>NCFC Jarrold Stand</td></tr>" &_
										"<tr><td style='font:8;color:#133e71'>Carrow Road</td></tr>" &_
										"<tr><td style='font:8;color:#133e71'>Norwich NR1 1HU</td></tr>" &_
										"<tr><td style='font:8;color:#04A859'>tel 01603 750200</td></tr>" &_
										"<tr><td style='font:8;color:#04A859'>fax 01603 750222</td></tr>" &_
										"<tr><td style='font:8;color:#133e71'>www.broadlandhousing.org</td></tr>" &_
										"<tr><td style='font:7;color:#133e71'>" &_
											"Reqistered under the Industrial and<br />Provident Societies Act 1965 as a non<br />" &_
											"profit making housing association with<br />charitable status. Reg No. 16274R " &_
											"<br />Housing Corporation Reg. No. L0026</td></tr>" &_
									 "</table>"

		ElseIf intFrom = TENANT Then
			str_return_address = get_tenant_address()
			If Generate_Forwarding_Address = "true" Then
				str_return_address = GenerateForwardingAddress()
			End If
		Elseif intFrom = LAaddress Then
			str_return_address = get_LA_address(intFrom)
		Else
			str_return_address = get_org_address(intFrom)
		End If

	'=====================================================================================================
	'  DETERMINE WHICH RETURN ADDRESS TO USE STARTS HERE
	'=====================================================================================================
	If (Request.Item("txt_SIGNATURE") <> "") Then
        Signature = "<div>" & Request("txt_SIGNATURE") & "</div>"
	Else
	    Signature = ""
	End If

	If Not Signature <> "" Then
		Housing_Officer_detail = get_housing_officer()
		Signature="<div id='SigNote'><b>Note:</b> There is currently no signature selected."
		Signature=Signature & "<br />You must select a signature before printing <br /> the letter."
		Signature=Signature & "Click Amend Content to select a <br />signature.</div>"
		Response.Write(Housing_Officer)
	End If

	strSig_BOX = Replace(Signature,"<br />", Chr(13) )
	'=====================================================================================================
	'  FORMAT ALL INPUT FIELDS, I.E DATE, MONETRY VALUES STARTS HERE
	'=====================================================================================================

		' CHANGED COS AS OF TODAY 1/JUNE/04 ONE SQL CAN RELATE TO ALL LETTERS
		str_letter = Replace(str_letter, "[1]", sql_call(1))
		str_letter = Replace(str_letter, "[2]", sql_call(2))

		' INSERT INPIUT BOXES -1 IS A DATE, -2 IS INT AND -3 IS FREE TEXT (ALL MUST BE INSERTED, DATE AND INT VALIDATED)
		' MUST ONLY REPLACE IF EXPRESSION IS FOUND IN STRING

		Call RegExpTest("'D\d'", str_Letter, 1)
		Call RegExpTest("'I\d'", str_Letter, 2)
		Call RegExpTest("'T\d'", str_Letter, 3)

	'=====================================================================================================
	'  FORMAT ALL INPUT FIELDS, I.E DATE, MONETRY VALUES ENDS HERE
	'=====================================================================================================

	'=====================================================================================================
	'  START FUNCTIONS
	'=====================================================================================================
        Function entry()

		    SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		    Call OpenRs(rsTeam, SQL)
		    If (NOT rsTeam.EOF) Then
			    TeamID = rsTeam("TEAMID")
		    Else
			    TeamID = -1
		    End If
		    Call CloseRs(rsTeam)

		    Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.TEAMID <> 1 AND T.ACTIVE=1", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", Null, NULL, "textbox200", " onchange='GetEmployees()' ")

		    SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				    " WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		    Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", null, NULL, "textbox200",null)

	    End Function

		Function get_housing_officer()

			Dim strSig

			SQL = "SELECT 	E.FIRSTNAME + ' ' + E.LASTNAME as FullName, ISNULL(CO.WORKDD,'') AS DD, ISNULL(CO.WORKEMAIL,'') AS EMAIL FROM 	C_TENANCY T  " &_
					" INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					" INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					" INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
					" LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					" INNER JOIN E_CONTACT CO ON CO.EMPLOYEEID = E.EMPLOYEEID " &_
					" WHERE T.TENANCYID = " & tenancy_id
			Call OpenRs(rsSet,SQL)

			If not rsSet.EOF Then
				strSig = rsSet(0) & "<br />Housing Officer." & "<br />Direct Dial " & rsSet(1) & "<br />Email " & rsSet(2)
			Else
				strSig = "Housing Officer."
			End If

			Call CloseRs(rsSet)

			get_housing_officer = strSig

		End Function


		Function RegExpTest(patrn, strng, param)

			Dim regEx, retVal, cnt              ' Create variable.
			cnt = 1
			Set regEx = New RegExp              ' Create regular expression.
			    regEx.Global = True			    ' Search entire string
			    regEx.Pattern = patrn           ' Set pattern.
			    regEx.IgnoreCase = False        ' Set case sensitivity.
			Set retVal = regEx.Execute(strng)   ' Execute the search test.
			For Each obj In retVal
				str_letter = Replace(str_letter, obj, input_call(param))
				cnt = cnt + 1
			Next

		End Function

		' LOOKS IN TABLE 'C_LETTERSQL' TO GET SQL WHICH WILL RETURN VALUE TO INSERT ONTO PAGE
		Function sql_call(int_sqlid)

			Dim item_sql, result
			SQL = "SELECT SQL FROM C_LETTERSQL WHERE SQLID = " & int_sqlid
			Call OpenRs(rsSet, SQL)
			If Not rsSet.EOF Then
				item_sql = rsset(0)
			Else
				sql_call = "**** Unrecognised sql id ****"
				Call CloseRs(rsSet)
				Exit Function
			End If

			SQL = item_sql & tenancy_id
			Call OpenRs(rsSet,SQL)
			If Not rsSet.EOF Then
				sql_call = rsSet(0)
				Call CloseRs(rsSet)
				Exit Function
			ELse
				sql_call = "**** No result set **** "
				CloseRs(rsSet)
				Exit Function
			End If

		End Function

		' OR ALTERNATIVELY PROVIDES INPUT BOX FOR USER INPUT
		Function input_call(int_wot)

			Dim strReturn
			If int_wot = 1 Then
				formFieldsArray = formFieldsArray & "FormFields["&textFieldCounter&"] = ""txt_INPUT"&textFieldCounter&"|Tenancy Type|DATE|Y"" " & VbCrLf
			ElseIf int_wot = 2 Then
				formFieldsArray = formFieldsArray & "FormFields["&textFieldCounter&"] = ""txt_INPUT"&textFieldCounter&"|Tenancy Type|CURRENCY|Y"" " & VbCrLf
			Else
				formFieldsArray = formFieldsArray & "FormFields["&textFieldCounter&"] = ""txt_INPUT"&textFieldCounter&"|Tenancy Type|TEXT|Y"" " & VbCrLf
			End If

			strReturn = "<span id='SPAN" & textFieldCounter & "'><input type='text' align='center' class='textbox200' name='txt_INPUT" & textFieldCounter & "' id='txt_INPUT" & textFieldCounter & "' style='width:75px; border:none; border-bottom:dashed 1px; font-size:10px' maxlength='10'></span>"
			strReturn = strReturn & "<image src='/js/FVS.gif' name='img_INPUT" & textFieldCounter & "' id='img_INPUT" & textFieldCounter & "' width='15px' height='15px' border='0' />"

            textFieldCounter = textFieldCounter + 1
			input_call = strReturn

		End Function


		' GET ALL ASSOCIATED LETTER TEXT FROM TABLE C_STANDARLETTERS
		Function build_letter_string()

			SQL = "SELECT LETTERTEXT,  FROMID, TOID, ISNULL(LETTERSIG,'FALSE') AS LETTERSIG FROM C_STANDARDLETTERS WHERE LETTERID = " & letter_id
			Call OpenRs(rsLetter, SQL)
			If Not rsLetter.Eof Then
				If request("hid_action") = "LOADNEWLETTER" Then
					AmendCount = "2"
					build_letter_string = Replace(request("txt_LETTERTEXT"), Chr(13), "<br />")
				Else
					build_letter_string = Replace(rsLetter(0), Chr(13), "<br />")
				End If
				' if an overide date has been sent to this page we replace all the date feilds with this date
				If D1 <> "" Then
				    build_letter_string = Replace(build_letter_string ,"D1",D1)
				End If
				if D2 <> "" Then
				    build_letter_string = Replace(build_letter_string ,"D2",D2)
				End If
				intFrom = rsLetter(1)
				intTo = rsLetter(2)
				Signature = rsLetter(3)
			Else
				build_letter_string = "The system is unable to find the associated letter. Please contact System Support."
			End If

		End Function


		' BUILD THE ADDRESS
		Function get_tenant_address()

			SQL = "SELECT 	C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, " &_
					"		C.FIRSTNAME, C.LASTNAME, A.HOUSENUMBER, A.HOUSENUMBER, " &_
					"		A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY , T.PROPERTYID " &_
					"FROM 	C_TENANCY T " &_
					"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
					"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID" &_
					"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & tenancy_id & " AND A.ISDEFAULT = 1 "

			Call OpenRS(rsCust, SQL)

			If rsCust.EOF Then
				' If a former tenant build the fowarding address of the former tenant
				Generate_Forwarding_Address = "true"
			Else
				' If not a former tenant build the correspondence address
				count = 1
				while NOT rsCust.EOF
					StringName = ""
					' Add Title to string ie MR, MISS
					Title = rsCust("TITLE")
					If (Title <> "" AND NOT isNull(Title)) Then
						StringName = StringName & Title & " "
						ShortName = ShortName & Title & " "
					End If

					' Add First name to string
					'FirstName = rsCust("FIRSTNAME")
					'If (FirstName <> "" AND NOT isNull(FirstName)) Then
					'	StringName = StringName & FirstName & " "
					'End If

					' Add Last name to string
					LastName =rsCust("LASTNAME")
					If (LastName <> "" AND NOT isNull(LastName)) Then
						StringName = StringName & LastName & " "
						ShortName = ShortName & LastName
					End If

					' Build Above onto the dear string
					If (count = 1) Then
						StringFullName = StringName
						str_Dear = "Dear " & ShortName
						count = 2
					Else
						StringFullName = StringFullName & " and " & StringName
						str_Dear = str_Dear & " and " & StringName
					End If

					rsCust.moveNext
				wend

				If (count = 2) Then

					rsCust.moveFirst()

					housenumber = rsCust("housenumber")
					If (housenumber = "" or isNull(housenumber)) Then
						FirstLineOfAddress = rsCust("Address1")
					Else
						FirstLineOfAddress = housenumber & " " & rsCust("Address1")
					End If

					RemainingAddressString = ""
					AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")

					' Loop through array of values that you have included in the select statement
					For i=0 to Ubound(AddressArray)
						temp = rsCust(AddressArray(i))
						If (temp <> "" and not isNull(temp)) Then
							RemainingAddressString = RemainingAddressString &  "<br />" & temp 
						End If
					Next
					tenancyref = rsCust("TENANCYID")
					If Not tenancyref <> "" Then tenancyref = 0 End If

				End If

			End If

			' Build the address html with what has been collected above
			get_tenant_address =	"<tr><td nowrap='nowrap' style='height:65px; font:12pt arial'>"& StringFullName & "" &_
									"<br />" & FirstLineOfAddress & RemainingAddressString & "</td></tr>" 

		End Function


		' IF FORMER TENANT BUILD THE FORWARDING ADDRESS
		Function GenerateForwardingAddress()

			SQL = "SELECT 	C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, " &_
					"		C.FIRSTNAME, C.LASTNAME, A.HOUSENUMBER, A.HOUSENUMBER, " &_
					"		A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY , T.PROPERTYID " &_
					"FROM 	C_TENANCY T " &_
					"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
					"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID" &_
					"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					"WHERE  T.TENANCYID = " & tenancy_id & " AND A.ISDEFAULT = 1 "

			Call OpenRS(rsCust, SQL)

				' If not a former tenant build the correspondence address
				count = 1
				while NOT rsCust.EOF

					StringName = ""

					' Add Title to string ie MR, MISS
					Title = rsCust("TITLE")
					If (Title <> "" AND NOT isNull(Title)) Then
						StringName = StringName & Title & " "
						ShortName = ShortName & Title & " "
					End If

					' Add First name to string
					FirstName = rsCust("FIRSTNAME")
					If (FirstName <> "" AND NOT isNull(FirstName)) Then
						StringName = StringName & FirstName & " "
					End If

					' Add Last name to string
					LastName =rsCust("LASTNAME")
					If (LastName <> "" AND NOT isNull(LastName)) Then
						StringName = StringName & LastName & " "
						ShortName = ShortName & LastName
					End If

					' Build Above onto the dear string
					If (count = 1) Then
						StringFullName = StringName
						str_Dear = "Dear " & ShortName
						count = 2
					Else
						StringFullName = StringFullName & " and " & StringName
						str_Dear = str_Dear & " and " & StringName
					End If

					rsCust.moveNext
				wend

				If (count = 2) Then

					rsCust.moveFirst()

					housenumber = rsCust("housenumber")
					If (housenumber = "" or isNull(housenumber)) Then
						FirstLineOfAddress = rsCust("Address1")
					Else
						FirstLineOfAddress = housenumber & " " & rsCust("Address1")
					End If

					RemainingAddressString = ""
					AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
					' Loop through array of values that you have included in the select statement
					For i=0 to Ubound(AddressArray)
						temp = rsCust(AddressArray(i))
						If (temp <> "" And Not isNull(temp)) Then
							RemainingAddressString = RemainingAddressString &  "<br />" & temp 
						End If
					Next
					tenancyref = rsCust("TENANCYID")
				end if

				GenerateForwardingAddress =	"<tr><td nowrap='nowrap' style='height:65px; font:12pt arial'>"& StringFullName & "" &_
										"<br />" & FirstLineOfAddress & RemainingAddressString & "</td></tr>"

		End Function

		Function get_org_address(int_addressid)

			Dim str_address

			SQL = "SELECT 	ISNULL(ADDRESS1,'') AS ADD1, " &_
					"		ISNULL(ADDRESS2,'') AS ADD2, " &_
					"		ISNULL(ADDRESS3,'') AS ADD3, " &_
					"		ISNULL(TOWNCITY,'') AS ADD4, " &_
					"		ISNULL(COUNTY,'') AS ADD5, " &_
					"		ISNULL(POSTCODE,'') AS ADD6 " &_
					"FROM 	C_STANDARDLETTERADDRESSES WHERE ADDRESSID = " & int_addressid
			Call OpenRS(rsAdd, SQL)
			count = 1
			while NOT rsAdd.EOF
				AddressArray = Array("ADD1", "ADD2", "ADD3", "ADD4", "ADD5", "ADD6")
				For i=0 To Ubound(AddressArray)
					temp = rsAdd(AddressArray(i))
					If (Not isNull(temp)) Then
						If (replace(temp," ", "") <> "") Then
							RemainingAddressString = RemainingAddressString & "<tr><td nowrap='nowrap' style='font:12pt arial'>" & temp & "</td></tr>"
						End If
					End If
				Next
			rsAdd.movenext
			Wend

			Call CloseRS(rsAdd)
			get_org_address = RemainingAddressString

		End Function

		Function get_LA_address(int_addressid)

			Dim str_address
			If tenancy_id <> "" Then

			SQL =	" SELECT 	LA.DESCRIPTION AS LANAME, " &_
					"			ORG.ADDRESS1,ORG.ADDRESS2,ORG.ADDRESS3, ORG.TOWNCITY, ORG.POSTCODE " &_
					" FROM 		P__PROPERTY P " &_
					"			INNER JOIN C_TENANCY T ON T.PROPERTYID = P.PROPERTYID " &_
					"			INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID " &_
					"			INNER JOIN G_LOCALAUTHORITY LA ON DEV.LOCALAUTHORITY = LA.LOCALAUTHORITYID " &_
					"			INNER JOIN S_ORGANISATION ORG ON ORG.ORGID = LA.LINKTOSUPPLIER " &_
					" WHERE		T.TENANCYID = " & tenancy_id &_
					" GROUP 	BY LA.DESCRIPTION, ORG.ADDRESS1,ORG.ADDRESS2,ORG.ADDRESS3, ORG.TOWNCITY, ORG.POSTCODE "
			Call OpenRS(rsAdd, SQL)

			count = 1
			while NOT rsAdd.EOF
				AddressArray = Array("LANAME","ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE")
				For i=0 to Ubound(AddressArray)
					temp = rsAdd(AddressArray(i))
					If (not isNull(temp)) Then
						If (replace(temp," ", "") <> "") Then
							RemainingAddressString = RemainingAddressString & "<tr><td nowrap='nowrap' style='font:12pt arial'>" & temp & "</td></tr>"
						End If
					End If
				Next
			rsAdd.movenext
			Wend

			Call CloseRS(rsAdd)
			get_LA_address = RemainingAddressString

			End If

		End Function

	'=====================================================================================================
	'  END FUNCTIONS
	'=====================================================================================================

	Call CloseDB()
%>
<html>
<head>
    <title>Customer Info:</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style media="all" type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        #SigNote
        {
            border: 1px solid #133E71;
            font: 12pt ARIAL;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">
	
	// USED TO ENSURE THAT PAGE IS FORMATTED ONLY PRIOR TO FIRST PRINT
	var print_count = 0
	var int_from = <%=intFrom%>
	
	function format_page(){
		var numinputs = RSLFORM.hid_NUMINPUTS.value;
		print_count ++;
		if (print_count == 1)
			for (i = 0 ; i < numinputs ; i++){
				document.getElementById("img_INPUT" + i + "").style.display = "none";
				document.getElementById("SPAN" + i + "").innerHTML = eval("RSLFORM.txt_INPUT" + i + ".value");
			}
	}

	function AttachMe(){
	// IF FIRST PRINT THEN FORMAT PAGE
		if (!checkForm()) return false
		answer = window.confirm("Please check the letter is correct as you will not be able to change them at a later date.")
		if (!answer) return false
				document.getElementById("AttachButton").style.visibility = "hidden";
				save_letter();
		close()
	}

	function PrintMe(){

		// IF FIRST PRINT THEN FORMAT PAGE
		if (print_count == 0){
			if (!checkForm()) return false
					format_page();
					hide_borders();
		}

		// HIDE BROADLAND LOGO AND FOOTER DUE TO HEADED PAPER
		if (int_from == 2){
		    document.getElementById("return_address").style.display = "none";
		    document.getElementById("footer").style.display = "none";
		}

        document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("AttachButton").style.visibility = "hidden";
		document.getElementById("CloseButton").style.visibility = "hidden";	
		document.getElementById("AmendContent").style.visibility = "hidden";

		print()

		document.getElementById("PrintButton").style.visibility = "visible";
		document.getElementById("CloseButton").style.visibility = "visible";
		document.getElementById("AmendContent").style.visibility = "visible";
		document.getElementById("AttachButton").style.visibility = "visible";
		document.getElementById("CloseButton").style.border = "1px solid";
		document.getElementById("PrintButton").style.border = "1px solid";
		document.getElementById("AmendContent").style.border = "1px solid";
		document.getElementById("AttachButton").style.border = "1px solid";

		// HIDE BROADLAND LOGO AND FOOTER DUE TO HEADED PAPER
		if (int_from == 2) {
			document.getElementById("return_address").style.display = "block";
			document.getElementById("footer").style.display = "block";
		}

	}

	// SAVE LETTER STRING AND ASSOCIATE WITH THIS LEVEL OF ARREARS PROCESS
	function save_letter() {
		opener.RSLFORM.hid_LETTERTEXT.value = document.getElementById("LETTERDIV").innerHTML;
		parent.document.getElementsByName("hid_SIGNATURE").value = document.getElementById("SigText").innerHTML;
	}

	var FormFields = new Array()
	<%=formFieldsArray%>

	function hide_borders(){	
		var coll = document.all.tags("INPUT");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	coll[i].style.border = "none"
	}				
	
	function AmendMe(amendtype){

	if (amendtype == "showamend"){	    
		    document.getElementById("LetterContent1").style.display = "none";
		    document.getElementById("LetterContent2").style.display = "block";
		    document.getElementById("SigContent1").style.display = "none";
		    document.getElementById("SigContent2").style.display = "block";
		    document.getElementById("AmendContent").style.display = "none";
		    document.getElementById("PrintButton").disabled = true;	
		    document.RSLFORM.target = "frm_letter_srv"
		    document.RSLFORM.hid_ACTION.value = "LOAD"
		    document.RSLFORM.action = "../serverside/amend_letter_svr.asp"
		    document.RSLFORM.submit();
		    if (document.getElementById("txt_SIGNATURE").value == "") {
		        document.getElementById("txt_SIGNATURE").value="<%=Housing_Officer%>";
		    }
		}
	else if (amendtype == "cancel"){
		document.getElementById("LetterContent1").style.display = "block";
		document.getElementById("LetterContent2").style.display = "none";
		document.getElementById("SigContent1").style.display = "block";
		document.getElementById("SigContent2").style.display = "none";	
		document.getElementById("AmendContent").style.display = "block";
		document.getElementById("PrintButton").disabled = false;		
		}
	else {
		document.RSLFORM.target = "_self"
		document.RSLFORM.hid_AmendCount.value = "2"
		document.RSLFORM.hid_ACTION.value = "LOADNEWLETTER"
		document.RSLFORM.hid_Team.value=RSLFORM.sel_TEAMS.options[RSLFORM.sel_TEAMS.selectedIndex].text;
		document.RSLFORM.hid_Employee.value=RSLFORM.sel_USERS.options[RSLFORM.sel_USERS.selectedIndex].text;
		document.RSLFORM.action = "pView_letter.asp"
		document.RSLFORM.submit();
		}
	}
	
    function GetEmployees(){
		if (document.RSLFORM.sel_TEAMS.value != "") {
			document.RSLFORM.action = "../Serverside/GetEmployees.asp"
			document.RSLFORM.target = "frm_letter_srv"
			document.RSLFORM.submit()
			}
		else {
			document.RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200'><option value=''>Please select a team</option></select>"
			}
		}

	function GetEmployee_detail(){
	    
		if (document.RSLFORM.sel_USERS.value != "") {
			document.RSLFORM.action = "../Serverside/GetEmployeeDetail.asp"
			document.RSLFORM.target = "frm_letter_srv"
			document.RSLFORM.submit()
			}
		//else {
		//	RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' class='textbox200'><option value=''>Please select a team</option></select>"
		//	}
		}
	function GetEmployees_ByPatch(){
			document.RSLFORM.action = "../Serverside/GetEmployees_ByPatch.asp"
			document.RSLFORM.target = "frm_letter_srv"
			document.RSLFORM.submit()
		}	

    </script>
</head>
<body class="TA">
    <form name="RSLFORM" method="post" action="">
    <table width="100%" border="0" style="height: 24cm">
        <tbody id="LETTERDIV">
            <tr>
                <td align="right" valign="top" style="font: 12pt arial" height="65">
                    <table border="0" id="return_address" style="display: block">
                        <%=str_return_address%>
                    </table>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">
                    <table>
                        <tr>
                            <td nowrap="nowrap" style="border: 1px solid #133E71; font: 12pt aria">
                                Tenancy Ref: <b>
                                    <%=TenancyReference(tenancy_id)%></b>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="font: 12pt arial">
                                <%=formatdatetime(date,1) & "<br /><br />" %>
                            </td>
                        </tr>
                        <%=str_recipient_address%>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td style="font: 12pt arial">
                    <%=str_Dear%>
                </td>
            </tr>
            <tr style="height: 10px">
                <td>
                </td>
            </tr>
            <tr id="LetterContent1" style="display: block">
                <td style="font: 12pt arial">
                    <div id="letterText">
                        <%=str_letter%></div>
                </td>
            </tr>
            <tr id="LetterContent2" style="display: none">
                <td style="font: 12pt arial">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <textarea name="txt_LETTERTEXT" id="txt_LETTERTEXT" cols="10" rows="10" style="height: 400;
                                    width: 400"><%= Request("txt_LETTERTEXT")%></textarea>
                            </td>
                            <td width="5px">
                                &nbsp;
                            </td>
                            <td bgcolor="beige" valign="top" width="28%">
                                <b><u>Key</u></b><br />
                                <br />
                                <b>'D*'</b> -- Date Field<br />
                                <b>'I*'</b> -- Number Field<br />
                                <b>'T*'</b> -- Text Field<br />
                                <br />
                                <b>[1]</b> -- Current Balance<br />
                                <b>[2]</b> -- Monthly Rent<br />
                                <br />
                                * Incremental integer
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" value="Amend" name="DoAmend" id="DoAmend" onclick="AmendMe('doamend')"
                                    title="Amend Letter Content" class="RSLButton" />
                                <input type="button" value="Cancel" name="Cancel" id="Cancel" onclick="AmendMe('cancel')"
                                    title="Cancel Amend Letter Content" class="RSLButton" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td style="font: 12pt arial; padding-top: 20px; padding-bottom: 90px;">
                    Yours sincerely
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td nowrap="nowrap" style="font: 12pt arial; display: none" valign="top" id="SigContent2">
                                <table>
                                    <tr>
                                        <td colspan="2" style="font-size: 11px; padding-bottom: 5px;">
                                            <b>Select Signature:</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Team:
                                        </td>
                                        <td>
                                            <%=lstTeams%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Employee:
                                        </td>
                                        <td>
                                            <div id="dvUsers">
                                                <%=lstUsers %></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <textarea name="txt_SIGNATURE" id="txt_SIGNATURE" cols="6" rows="4" style="width: 200;
                                                height: 170"><%= Signature%></textarea>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td nowrap="nowrap" style="font: 12pt arial; display: block" valign="top" id="SigContent1">
                                <div id="SigText">
                                    <%=signature%></div>
                            </td>
                            <td align="right" valign="top" width="200">
                                <input type="button" value="Print" onclick="PrintMe()" title="Print Me" class="RSLButton"
                                    name="PrintButton" id="PrintButton" /><br />
                                <br />
                                <input type="button" value="Attach" onclick="AttachMe()" title="Attach Me" class="RSLButton"
                                    name="AttachButton" id="AttachButton" style="display: block" /><br />
                                <input type="button" value="Close" onclick="window.close()" title="Close Me" class="RSLButton"
                                    name="CloseButton" id="CloseButton" />
                                <br />
                                <br />
                                <input type="button" value="Amend Content" onclick="AmendMe('showamend')" title="Amend Letter Content"
                                    class="RSLButton" name="AmendContent" id="AmendContent" style="display: block" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="100%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="right" id="footer" style="visibility: hidden">
                    <%=str_Footer%>
                </td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" name="hid_AmendedText" id="hid_AmendedText" value="" />
    <input type="hidden" name="hid_AmendCount" id="hid_AmendCount" value="<%=AmendCount%>" />
    <input type="hidden" name="tenancyid" id="tenancyid" value="<%=tenancy_id%>" />
    <input type="hidden" name="letterid" id="letterid" value="<%=letter_id%>" />
    <input type="hidden" name="hid_ACTION" id="hid_ACTION" />
    <input type="hidden" name="hid_LETTER" id="hid_LETTER" />
    <input type="hidden" name="hid_LETTERID" id="hid_LETTERID" value="<%=letter_id%>" />
    <input type="hidden" name="hid_NUMINPUTS" id="hid_NUMINPUTS" value="<%=textFieldCounter%>" />
    <input type="hidden" name="small" id="small" value="2" />
    <input type="hidden" name="hid_Team" id="hid_Team" />
    <input type="hidden" name="hid_Employee" id="hid_Employee" />
    <input type="hidden" name="hid_Housing_Officer_detail" id="hid_Housing_Officer_detail"
        value="<%=Housing_Officer_detail %>" />
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="frm_letter_srv" id="frm_letter_srv" width="400"
        height="400" style="display: none"></iframe>
</body>
</html>
