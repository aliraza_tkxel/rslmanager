<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim blockid, repairid, wheresql, developmentid, repairsql, commence_date, start_date, isblock, ispropertyspecific
	Dim str_what, cnt, str_table, nett_cost, cost, desc, CostString, JscriptRef, JScriptVal

	' GET REQUIRED VARIABLES
	blockid 		= Request("BLOCK")
	developmentid 	= Request("DEVELOPMENT")
	repairid 		= Request("REPAIR")
	commence_date	= Request("COMMENCE")
	start_date		= Request("START")
	ispropertyspecific = 0

	If blockid = "" Then
		wheresql = " WHERE P.DEVELOPMENTID = " & developmentid & " AND P.PROPERTYTYPE NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	Else
		isblock = 1
		wheresql = " WHERE P.BLOCKID = " & blockid & " AND P.PROPERTYTYPE NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	End If

	Call OpenDB()

	' FIND OUT WHETHER OR NOT REPAIR IS PROPERTY SPECIFIC, IF SO THEN INCLUDE IN SQL
	If repairid = "822" Then ispropertyspecific = 1 End If

	' DETERMINE WHICH TABLE TO BUILD
	If ispropertyspecific = 1 Then
		build_multiple()
	Else
		build_single()
	End if

		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & repairsql & wheresql & " ORDER BY P.PROPERTYID "
	'response.write "<br />" & SQL
	str_what = build_decsriptive_string()

	Call CloseDB()

	' USE THIS FUNCTION IF WE HAVE A GAS REQUEST
	Function build_multiple()

		SQL = 	"SELECT R.ITEMDETAILID, P.PROPERTYID, R.DESCRIPTION, ISNULL(COST,0) AS COST, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          		"FROM 	P__PROPERTY P " &_
				"		INNER JOIN P_ANNUALSERVICINGS A ON A.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN R_ITEMDETAIL R ON A.REPAIRID = R.ITEMDETAILID " & wheresql &_
				"ORDER	BY P.PROPERTYID "

		'response.write "<br />" & SQL
		Call OpenRs(rsSet, SQL)

		cnt = 0
		nett_cost = 0
		str_table = "<tr>"
		While Not rsSet.EOF

			str_table = str_table & "<td width=""100px"">" & rsSet("PROPERTYID") &_
									"</td><td nowrap=""nowrap"">" & rsSet("ADDRESS") &_
									"</td><td nowrap=""nowrap"">" & rsSet("DESCRIPTION") &_
									"</td><td><input type=""checkbox"" checked=""checked"" name=""Include"" value=""" & rsSet("ITEMDETAILID") & "|" & rsSet("PROPERTYID") & """ onclick=""UpdateTotal()"" /></td></tr>"
			cnt = cnt + 1
			nett_cost = nett_cost + rsSet("COST")
			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		SQL = 	"SELECT DISTINCT R.ITEMDETAILID, R.DESCRIPTION, ISNULL(COST,0) AS COST " &_
          		"FROM 	P__PROPERTY P " &_
				"		INNER JOIN P_ANNUALSERVICINGS A ON A.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN R_ITEMDETAIL R ON A.REPAIRID = R.ITEMDETAILID " & wheresql
		'response.write "<br />" & SQL
        Call Build_Costs(SQL)

	End Function

	'THIS FUNCTION BUILDS THE REPAIR ITEM COST ROWS. THESE COSTS CAN NOW BE MODIFIED.
	'IT ALSO CREATES A LIST OF REFERENCES WHICH IS USED BY THE CLIENT SIDE JAVASCRIPT.
	Function Build_Costs(iSQL)
		CostString = ""
		JscriptRef = ""
		iCounter = 0
		Call OpenRs(rsDiff, iSQL)
		while NOT rsDiff.EOF
			CostString = CostString & "<tr bgcolor=""beige""><td align=""right""><b>Item:</b> " & rsDiff("DESCRIPTION") &_
						"&nbsp;<b>Cost:</b> �<input type=""text"" style=""text-align:right"" class=""textbox"" id=""txt_NC" & rsDiff("ITEMDETAILID") & """ name=""txt_NC" & rsDiff("ITEMDETAILID") & """ " &_
						" value=""" & FormatNumber(rsDiff("COST"),2,-1,0,0) & """ onblur=""UpdateTotal()"" />" &_
						"<img src=""/js/FVS.gif"" width=""15"" height=""15"" name=""img_NC" & rsDiff("ITEMDETAILID") & """ id=""img_NC" & rsDiff("ITEMDETAILID") & """ alt="""" />" &_
						"</td></tr>"
			JscriptRef = JScriptRef & "," & rsDiff("ITEMDETAILID")
			JScriptVal = JScriptVal & "FormFields[" & iCounter & "] = ""txt_NC" &  rsDiff("ITEMDETAILID") & "|Repair Cost|CURRENCY|Y""" & chr(13)
			iCounter = iCounter + 1
			rsDiff.moveNext
		wend
	End Function

	' USE THIS FUNCTION IF WE HAVE A NORMAL SINGULAR REQUEST
	Function build_single()

		SQL = "SELECT ITEMDETAILID, COST, DESCRIPTION FROM R_ITEMDETAIL WHERE ITEMDETAILID = " & repairid
		'response.write "<br />" & SQL
		Call Build_Costs(SQL)

		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			cost = rsSet("Cost")
		End If
		Call CloseRs(rsSet)

		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & wheresql & " ORDER BY P.PROPERTYID "
		'response.write "<br />" & SQL
		Call OpenRs(rsSet, SQL)

		cnt = 0
		str_table = "<tr>"
		While Not rsSet.EOF

			str_table = str_table & "<td width=""100px"">" & rsSet("PROPERTYID") &_
									"</td><td>" & rsSet("ADDRESS") &_
									"</td><td width=""20""><input type=""checkbox"" checked=""checked"" name=""Include"" value=""" & repairid & "|" & rsSet("PROPERTYID") & """ onclick=""UpdateTotal()"" /></td></tr>"
			cnt = cnt + 1
			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)
		nett_cost = cnt * cost

	End Function

	' GET INFORMATION STRING
	Function build_decsriptive_string()

		Dim this_string
		If isblock = 1 Then
			this_string = "block wide"
		Else 
			this_string = "scheme wide"
		End If
		If ispropertyspecific = 1 Then
			this_string = this_string & " and property specific"
		End If

		build_decsriptive_string = this_string
		Exit Function

	End Function

	' check if not items exist in the list in which case set the submit button to disabled
	button_disabled = ""
	if cnt = 0 then button_disabled = " disabled"
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio --> Check Cyclical Repairs</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" src="/js/preloader.js"></script>
    <script type="text/javascript" src="/js/general.js"></script>
    <script type="text/javascript" src="/js/menu.js"></script>
    <script type="text/javascript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" >
	var FormFields = new Array()
	<%=JscriptVal%>
	
	window.returnValue = "FAILED"
	
	var RepairCosts = new Array(0<%=JscriptRef%>)
	
	//RETURNS THE CORRESPONDINGLY CHECKED ITEMS WHICH WILL BE SAVED IN THE PLANNED INCLUDES TABLE
	function return_focus(bool){
		if (!checkForm()) return false;	
		if (bool) {
			ref = document.getElementsByName("Include")
			var iDataArray = new Array()
			if (ref.length) {
				for (i=0; i<ref.length; i++){
					if (ref[i].checked){
						iDataArray[iDataArray.length] = ref[i].value
						}
					}
				}
			eData = iDataArray.join("||<>||")
			if (eData == "") eData = "-1"
			
			CostData = ""
			for (k=1; k<RepairCosts.length; k++){
				if (k > 1) CostData += "|DDD|"
				CostData += RepairCosts[k] + "||" + document.getElementById("txt_NC" + RepairCosts[k]).value
				}
			//THIS PART CONTAINS A LIST OF ALL PROPERTY IDS WITH THE CORRESPONDING REPAIR ITEM.				
			window.returnValue = eData + "|SSSSS|" + CostData
			window.close()
			}
		else {
			window.returnValue = "FAILED"
			window.close()
			}
		}

	function FormatCurrrenyComma(Figure){
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}
	
	//THIS FUNCTION UPDATES THE CURRENT NET COST TOTAL.	
	function UpdateTotal(){
		if (!checkForm()) return false;
		ref = document.getElementsByName("Include")
		NewNet = 0
		if (ref.length) {
			JCOUNTER = 0
			for (i=0; i<ref.length; i++){
				if (ref[i].checked){
					JCOUNTER++
					iData = ref[i].value
					iDataArray = iData.split("|")
					NewNet += parseFloat(document.getElementById("txt_NC" + iDataArray[0]).value)
					}
				}
			}
		if (JCOUNTER > 0) 
			document.getElementById("SUBBUT").disabled = false
		else
			document.getElementById("SUBBUT").disabled = true		
		document.getElementById("TOT").innerHTML = "Net Cost: �" + FormatCurrrenyComma(NewNet) 
		}

        function checkAll(field)
        {
            for (i = 0; i < field.length; i++)
            {
	            field[i].checked = true ;
            }
            UpdateTotal()
        }

        function uncheckAll(field)
        {
            for (i = 0; i < field.length; i++)
            {
	            field[i].checked = false ;
            }
            UpdateTotal()
        }
    </script>
</head>
<body class="ta" onunload="macGo()">
<form name="myform" id="myform" action="">
    <table border="1" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <tr>
            <td colspan="2">
                The repair you have chosen is <b>
                    <%=str_what%></b> and will be attached to the following properties. The repair
                contract starts on <b>
                    <%=commence_date%></b> and the repairs are scheduled to begin on <b>
                        <%=start_date%></b> . Please check that this information is correct. If
                so then press submit, if the information is not correct then click cancel and amend
                the appropriate data.
            </td>
        </tr>
    </table>
    <br />
    <table border="1" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <tr>
            <td colspan="2">
                If you wish to <b>exclude</b> properties (or specific annual servicings) just click
                on the check box next to each respective item so that it <b>does not</b> contain
                a tick in it.
            </td>
        </tr>
    </table>
    <br />
    <% If ispropertyspecific = 1 Then %>
    <table border="1" cellspacing="2" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <%=CostString%>
    </table>
    <br />
    <table border="1" cellspacing="2" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <tr bgcolor='beige'>
            <td width="100PX">
                <b>Property Ref</b>
            </td>
            <td>
                <b>Address</b>
            </td>
            <td>
                <b>Description</b>
            </td>
            <td>
                <b>INC</b>
            </td>
        </tr>
        <%=str_table%>
    </table>
    <% Else %>
    <table border="1" cellspacing="2" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <%=CostString%>
    </table>
    <br />
    <table border="1" cellspacing="2" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <tr bgcolor='beige'>
            <td width="100PX">
                <b>Property Ref</b>
            </td>
            <td>
                <b>Address</b>
            </td>
            <td>
                <b>INC</b>
            </td>
            </tr>
                <%=str_table%>
    </table>
    <% End If %>
    <br />
    <table border="1" cellpadding="2" width="95%" align="center" style='border: 1px solid #133e71;
        border-collapse: collapse'>
        <tr>
            <td colspan="2" align="right">
                    <div id="TOT" style='font-size: 13; font-weight:bold'>
                        Net Cost:
                        <%=FormatCurrency(nett_cost)%></div>
             </td>
        </tr>
    </table>
    <br />
    <table width="95%" align="center">
        <tr>
            <td colspan="2" align="right">
                <input style="cursor: pointer" type="button" class="RSLButton" value="Select All" onclick="checkAll(document.myform.Include)" />
                <input style="cursor: pointer" type="button" class="RSLButton" value="Deselect All" onclick="uncheckAll(document.myform.Include)" />
                <input type="button" class="RSLButton" style="width: 70px" title="Cancel this cyclical repair"
                    value=" Cancel " onclick="return_focus(false)" />
                <input type="button" class="RSLButton" style="width: 70px" name="SUBBUT" id="SUBBUT" title="Submit this cyclical repair"
                    value=" Submit " onclick="return_focus(true)" <%=button_disabled%> />
            </td>
            </tr>
    </table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name="frm_cyclrep" id="frm_cyclrep" width="4px" height="4px" style="display: none"></iframe>
</body>
</html>
