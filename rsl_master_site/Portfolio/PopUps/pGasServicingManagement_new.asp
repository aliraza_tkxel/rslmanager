<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<%
	Call OpenDB()
	Call BuildSelect(lstOrganisation, "sel_ORGANISATION", "S_ORGANISATION WHERE ORGID IN (SELECT DISTINCT ORGID FROM S_SCOPE WHERE AREAOFWORK IN(5,6))", "ORGID, NAME", "NAME", "Please Select", Null,Null , "textbox200", " style=""margin-left:2px;"" ")	 'disabled=disabled    
	Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", Null, Null, "textbox200", " STYLE='margin-left:29px;' TABINDEX=11 OnChange=""GetSchemes();"" ")	    
	Call BuildSelect(lstDevelopemnt, "sel_SCHEME", "P_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "All", Null,Null, "textbox200", "style=""margin-left:17px;"" " )	
    Call BuildSelect(lstFuel, "sel_FUEL", "P_FUELTYPE", "FUELTYPEID, FUELTYPE", "FUELTYPE", "All", null,null, "textbox200", "style=""margin-left:8px;"" " )
	Call CloseDB()
%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager --> Gas Servicing </title>
<link rel="stylesheet" type="text/css" href="../../css/RSLManager_greenBrand.css" />
<style  type="text/css" media="all">
    #main{width:100%;}
    /*CSS for filter*/
    #filter {background-color: #FFFFFF;width:25%;margin-left:5px;padding-top:10px;float:left;margin-right:3px;}
    .heading {border:1px #133e71 solid;width:160px;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    #filter_head{width:291px;}
    #filter_headline{width:131px;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .filter_headlinediv{width:131px;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    #filter_body{width:291px;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-bottom:18px;margin-bottom:10px;}
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;font-size:13px;}
    .linkstylesmall{text-decoration: underline;color:blue;cursor:hand;font-size:10px;padding:0 5 0 5;}
    .boxstyle{border:1px solid #133e71;width:12px;height:1px;margin:5 5 0 10;}
    .dottedline{border-bottom:1px dotted #133e71; padding:0 0 0 0; margin:0 9 0 9;}
    .line{border-bottom:1px solid #133e71; padding:0 0 0 0; margin:0 9 0 2;}
    .dottedbox{border:1px dotted #133e71;}
    .dottedbox img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .elementlabel{font-size:10px;margin-left:10px;font-weight:bold;color:#133e71}
    .elementlabeldisabled{font-size:10px;margin-left:10px;font-weight:bold;color:gray}
    .margintop{margin:10 0 0 0;}
    /*CSS for content*/
    .content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;float:left;}
    .content_head{width:99%;}
    .heading_content {border:1px #133e71 solid;width:24%;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    .content_headline{width:76%;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .content_headlinediv{width:100%;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    .content_body{width:99%;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-left:10px;float:left;height:100%;}
    .heading2{font-weight:bold;color:#133e71;}
    .heading3{font-weight:bold;color:#133e71;font-size:12px;}
    .fonttwelevewithcolor{font-size:12;color:#133e71;} 
</style>
</head>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
  
  function chkObject(theVal)
  {
    if (document.getElementById(theVal) != null) 
            return true;
        else 
            return false;

  }
  

 function GetSchemes()
  {
    
	RSLFORM.sel_SCHEME.disabled = true;
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "../ServerSide/GetSchemes_GasServicing.asp"
	RSLFORM.submit()
 }
  
  function ResetFilter()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;
    var parent,child

    switch (selectedoption) 
    {
        
        default:
                if( chkObject("mon_menu_data")==true)
                {
                    parent=document.getElementById("content"); 
                    child=document.getElementById("mon_menu_data"); 
                    parent.removeChild(child);
                } 
				document.getElementById("content_head").style.display="none";
				document.getElementById("content_body").style.display="none";
				//RSLFORM.sel_RENEWAL.disabled=true;
				//RSLFORM.txt_street.value="";
				//RSLFORM.txt_street.disabled=true;
				//RSLFORM.txt_postcode.value="";
				//RSLFORM.txt_postcode.disabled=true;
				//document.getElementById("lbl_renwal").className ="elementlabeldisabled";
				//document.getElementById("lbl_street").className ="elementlabeldisabled";
				//document.getElementById("lbl_postcode").className ="elementlabeldisabled"; 
    }
    return;
  }
  
  function GetMonitoring()
  {
 //   if(RSLFORM.sel_ORGANISATION.value=="") 
 //   {
 //       alert("Please select the organisation");
 //       return;
 //   }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "../ServerSide/GetMonitoring_new.asp"
	RSLFORM.submit()
	return;
  }

   function GetMonitoringDetail()
  {
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "../ServerSide/GetMonitoringDetail_new.asp"
	RSLFORM.submit()
	return;
  }
  function FilterRecords()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;

    switch (selectedoption) 
    {
        default: GetMonitoring();
    }
  }
  
  function swap()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;

    switch (selectedoption) 
    {
        
      default:   RSLFORM.hid_mon_menu.value="1";
                 GetMonitoring();
    }
    return true;
  }
  
  function ChangeMenu()
	{
    RSLFORM.hid_mon_menu.value=RSLFORM.sel_Monitoring.value;

	if(RSLFORM.sel_Monitoring.value==1) 
	{
		
	}
	 
	GetMonitoring();
	return;
	}


	function GetDetailMonitoring(Status)
	{
		RSLFORM.mon_status.value=Status;
		GetMonitoringDetail();
	} 
 

  function PropertyUpdate(PropertyId)
  {
    window.opener.location.href="/Portfolio/Attributes.asp?PropertyID="+PropertyId;
    window.close();
  }
 </script>

<body onload="GetMonitoring();"> <!--  //-->

<form name="RSLFORM" method="post" action="">

<div id="maincontent">
	<div id="topbar">
		<div id="topmenu">
			<div id="system_logo"></div>
			<div style="float:right;margin-right:4px;margin-top:10px;"><a class="lnklogout" href="#">Help</a> <a class="lnklogout"  href="#">Whiteboard</a> <a class="lnklogout" href="#" onClick="window.close()">Log Out</a></div>
			
			<div id="PageHeading" style="clear:right;float:right;margin-right:4px;margin-top:40px;">Fuel Certificate Monitoring</div>
			
		</div>
		
	</div>
	<div id="stripe"></div> 
	<br />



   <div id="main">
    <div  id="filter">
        <div id="filter_head">
            <div id="filter_heading" class="heading">Filters</div>
            <div id="filter_headline">
                <div class="filter_headlinediv" style="border-bottom:1px solid #133e71;"></div>
                <div class="filter_headlinediv" style="border-right:1px solid #133e71;"></div>
            </div>
         </div>
         <div id="filter_body">
			<br/>
			<!--     
			<p style="margin-top:5px;">
				<span class="boxstyle"></span>
				<span>Summary</span>
			</p>
			//-->

            <p style="margin-bottom:10px;">
                <span class="elementlabel">Action:</span>
                <select id="sel_reportlist" name="sel_reportlist" class="textbox200" style="width:150" onChange="swap();" >
                    <option value="">Please Select</option> 
                     <option value="4" selected="selected">Monitoring</option>   
                </select>
            </p>
            <p class="dottedline" style="margin-bottom:10px;">&nbsp;</p>
            <p style="font-size:14px;font-weight:bold;margin-left:10px;margin-top:0px;margin-bottom:0px;">Search</p>
            <!--<p style="margin-bottom:0px;" class="margintop">
                 <span id="lbl_contractor" class="elementlabeldisabled">Contractor:</span>
                 <%=lstOrganisation%>
            </p>-->
			<!--
            <p  class="margintop">
                <label class="elementlabeldisabled" for="sel_RENEWAL" id="lbl_renwal"> 
                    <span style="display:block;">CP12</span> <span style="margin-left:10px;">Renewal:</span>
                </label>
                <select id="sel_RENEWAL" name="sel_RENEWAL" class="textbox200" style="width:200;margin-left:15px;" disabled="true">
                     <option value="">Select</option>  
                     <option value="0-6">0-6 days</option>  
                     <option value="7-12">7-12 days</option> 
                     <option value="1">1 month</option>                        
                     <option value="1+">1 month+</option>   
                 </select>
            </p>
            <p class="margintop">
                <label class="elementlabeldisabled" for="sel_APPOINTMENTSTAGE" id="lbl_appointmentstage">Stage:</label>
                <select id="sel_APPOINTMENTSTAGE" name="sel_APPOINTMENTSTAGE" class="textbox200" style="width:200;margin-left:29px;" disabled="disabled">
                     <option value="">Select</option>  
                     <option value="1">1st &nbsp;Appointment</option>  
                     <option value="2">2nd Appointment</option> 
                     <option value="3">3rd &nbsp;Appointment</option>                        
                     <option value="4">4th &nbsp;Appointment</option>
                     <option value="5">5th &nbsp;Appointment</option>
                     <option value="6">6th &nbsp;Appointment</option>
                     <option value="0">No-Entry</option>   
                 </select>
            </p>
			//-->
            <p class="margintop">
                <span class="elementlabel">Fuel Type:</span>
                <%=lstFuel %>
            </p>
            <p class="margintop">
                <span class="elementlabel">Patch:</span>
                <%=lstPatch %>
            </p>
            <p class="margintop">
                <span class="elementlabel">Scheme:</span>
                <%=lstDevelopemnt %>
            </p>
             <p class="margintop">
              <label class="elementlabel" for="txt_uprn" id="lbl_uprn">UPRN:</label>  
              <input type="text" maxlength="10" value="" id="txt_UPRN" name="txt_UPRN" class="textbox100" style="margin-left:29px;"/>
            </p>
			<!--
            <p class="margintop">
              <label class="elementlabeldisabled" for="txt_street" id="lbl_street">Address:</label>  
              <input type="text" value="" id="txt_street" name="txt_street" class="textbox200" style="margin-left:15px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="txt_postcode" id="lbl_postcode">PostCode:</label>  
              <input type="text" value="" id="txt_postcode" name="txt_postcode" class="textbox200" style="margin-left:8px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <span class="elementlabeldisabled">Customer:</span>  
              <input type="text" value="" id="cust_name" name="cust_name" class="textbox200" style="margin-left:7px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <span class="elementlabeldisabled">Cust No:</span>  
              <input type="text" value="" id="cust_no" name="cust_no" class="textbox200" style="margin-left:19px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <span class="elementlabeldisabled">Appointment date:</span>  
              <input type="text" value="DD/MM/YYYY" class="textbox200" style="margin-left:38px;width:120;" disabled="disabled" />
            </p>
			//-->
            <p class="margintop">
               <input type="button" value="Search" class="RSLButton" onClick="FilterRecords();" style="margin-left:227px;"/>
            </p>
            <p class="dottedline">&nbsp;</p>
         </div>
     </div>
     <div class="content" id="content">
        <div id="content_head" class="content_head">
            <div id="content_heading" class="heading_content"></div>
            <div id="content_headline" class="content_headline">
                <div class="content_headlinediv" style="border-bottom:1px solid #133e71;"></div>
                <div class="content_headlinediv" style="border-right:1px solid #133e71;"></div>
            </div>
         </div>
         <div id="content_body" class="content_body">
         </div>
     </div>
 </div>
 <input type="hidden" id="hid_select_journalid" name="hid_select_repairhistoryid" value="" />
 <input type="hidden" id="hid_select_scopeid" name="hid_select_scopeid" value="" />
 <input type="hidden" id="hid_mon_menu" name="hid_mon_menu" value="1"/>
 <input type="hidden" id="mon_status" name="mon_status" value="0"/>
 <input type="hidden" id="hid_appointmenttime" name="hid_appointmenttime" value="" />
</div>
<br /> 
</form>
<iframe src="/secureframe.asp" name="ServerFrame" height="600" width="600" style="display:none"></iframe>
</body>
</html>