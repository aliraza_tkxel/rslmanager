<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim blockid, repairid, wheresql, developmentid, repairsql, commence_date, start_date, isblock, ispropertyspecific
	Dim str_what, cnt, str_table, nett_cost, cost, desc
	
	' GET REQUIRED VARIABLES
	blockid 		= Request("BLOCK")
	developmentid 	= Request("DEVELOPMENT")
	repairid 		= Request("REPAIR")
	ispropertyspecific = 0
	
	if blockid = "" Then 
		wheresql = " WHERE P.DEVELOPMENTID = " & developmentid & " AND P.PROPERTYTYPE NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	else
		isblock = 1
		wheresql = " WHERE P.BLOCKID = " & blockid & " AND P.PROPERTYTYPE NOT IN (6,8,9) " ' EXCLUDE GARAGES ETC
	end if
	
	OpenDB()
	
	// FIND OUT WHETHER OR NOT REPAIR IS PROPERTY SPECIFIC, IF SO THEN INCLUDE IN SQL
	if repairid = "822" Then ispropertyspecific = 1 end if
	
	// DETERMINE WHICH TABLE TO BUILD
	if ispropertyspecific = 1 Then
		build_multiple()
	Else
		build_single()
	End if
	
		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & repairsql & wheresql & " ORDER BY P.PROPERTYID "

	str_what = build_decsriptive_string()
	
	CloseDB()
	
	// USE THIS FUNCTION IF WE HAVE A GAS REQUEST
	Function build_multiple()
	
		SQL = 	"SELECT R.ITEMDETAILID, P.PROPERTYID, R.DESCRIPTION, ISNULL(COST,0) AS COST, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          		"FROM 	P__PROPERTY P " &_ 
				"		INNER JOIN P_ANNUALSERVICINGS A ON A.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN R_ITEMDETAIL R ON A.REPAIRID = R.ITEMDETAILID " & wheresql &_
				"ORDER	BY P.PROPERTYID "

		'response.write "<BR>" & SQL
		Call OpenRs(rsSet, SQL)
		
		cnt = 0
		nett_cost = 0
		str_table = "<TR>"
		While Not rsSet.EOF 
		
			str_table = str_table & "<TD WIDTH=100PX>" & rsSet("PROPERTYID") &_
									"</TD><TD nowrap>" & rsSet("ADDRESS") &_
									"</TD><TD nowrap>" & rsSet("DESCRIPTION") &_
									"</TD><TD>" & FormatCurrency(rsSet("COST")) & "</TD>" &_
									"</TD><TD><input type=""checkbox"" CHECKED name=""Include"" value=""" & rsSet("ITEMDETAILID") & "|" & rsSet("PROPERTYID") & "|" & rsSet("COST") & """ onclick=""UpdateTotal()""></TD></TR>"
			cnt = cnt + 1
			nett_cost = nett_cost + rsSet("COST")
			rsSet.movenext()
		
		Wend
		CloseRs(rsSet)
	
	End Function
	
	// USE THIS FUNCTION IF WE HAVE A NORMAL SINGULAR REQUEST
	Function build_single()
	
		SQL = "SELECT COST, DESCRIPTION FROM R_ITEMDETAIL WHERE ITEMDETAILID = " & repairid
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then 
			cost = rsSet(0) 
			desc = rsSet(1) 
		end if
		CloseRs(rsSet)		
		
		SQL = " SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1) AS ADDRESS " &_
          " FROM P__PROPERTY P " & wheresql & " ORDER BY P.PROPERTYID "

		response.write "<BR>" & SQL
		Call OpenRs(rsSet, SQL)
		
		cnt = 0
		str_table = "<TR>"
		While Not rsSet.EOF 
		
			str_table = str_table & "<TD WIDTH=100PX>" & rsSet("PROPERTYID") &_
									"</TD><TD>" & rsSet("ADDRESS") &_
									"</TD><TD WIDTH=20><input type=""checkbox"" CHECKED name=""Include"" value=""" & repairid & "|" & rsSet("PROPERTYID") & "|" & cost & """ onclick=""UpdateTotal()""></TD></TR>"
			cnt = cnt + 1
			rsSet.movenext()
		
		Wend
		CloseRs(rsSet)
		nett_cost = cnt * cost
		
	End Function
	
	// GET INFORMATION STRING
	Function build_decsriptive_string()
		
		Dim this_string
		if isblock = 1 then 
			this_string = "block wide"
		else 
			this_string = "scheme wide"
		end if	
		if ispropertyspecific = 1 then
			this_string = this_string & " and property specific"
		end if
		
		build_decsriptive_string = this_string
		Exit Function 
			
	End Function
	
	//check if not items exist in the list in which case set the submit button to disabled
	button_disabled = ""
	if cnt = 0 then button_disabled = " disabled"	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio --> Check Cyclical Repairs</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function return_focus(bool){
		if (bool) {
			ref = document.getElementsByName("Include")
			var iDataArray = new Array()
			if (ref.length) {
				counter = 0
				for (i=0; i<ref.length; i++){
					if (ref[i].checked){
						iDataArray[iDataArray.length] = ref[i].value
						counter++
						}
					}
				}
			eData = iDataArray.join("||<**>||")
			if (eData == "") eData = ""
			window.returnValue = eData + "||****||" + counter
			window.close()
			}
		else {
			window.returnValue = "FAILED"
			window.close()
			}
		}

	function FormatCurrrenyComma(Figure){
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}
		
	function UpdateTotal(){
		ref = document.getElementsByName("Include")
		NewNet = 0
		if (ref.length) {
			JCOUNTER = 0
			for (i=0; i<ref.length; i++){
				if (ref[i].checked){
					JCOUNTER++
					iData = ref[i].value
					iDataArray = iData.split("|")
					NewNet += parseFloat(iDataArray[2])
					}
				}
			}
		if (JCOUNTER > 0) 
			document.getElementById("SUBBUT").disabled = false
		else
			document.getElementById("SUBBUT").disabled = true		
		document.getElementById("TOT").innerHTML = "Net Cost: �" + FormatCurrrenyComma(NewNet) 
		}
</SCRIPT>

<BODY class='ta' BGCOLOR=#FFFFFF onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
		<tr>
			<td colspan=2>The repair you have chosen is <b><%=str_what%></b> and will be attached to the following properties.
				The repairs will appear on the system on immediately. Please check that this information is correct. If so then press submit, if the information is not correct then
				click cancel and amend the appropriate data. 
			</td>
		</tr>
	</table>
	<BR>
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
		<tr>
			<td colspan=2>If you wish to <b>exclude</b> properties just click on the check box next to each respective item so that it does <b>not</b> contain a tick in it. 
			</td>
		</tr>
	</table>
	<BR>
<% If ispropertyspecific = 1 Then %>
	<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td WIDTH=100PX><b>Property Ref</b></td>
		<td><b>Address</b></td>
		<td><b>Description</b></td>
		<td><b>Cost</b></td>
		<td><b>INC</b></td>				
	<tr>
		<%=str_table%>
	</table>
<% Else %>
<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td align=right><b>Item:</b> <%=desc%> <b>Cost:</b> <%=FormatCurrency(cost)%></td>
	<tr>
</table>
<BR>
<table border=1 cellspacing=2 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr bgcolor='beige'>
		<td WIDTH=100PX><b>Property Ref</b></td>
		<td><b>Address</b></td>
		<td><b>INC</b></td>		
	<tr>
		<%=str_table%>
	</table>
<% End If %>		
	
	<BR>
	<table border=1 cellpadding=2 width=95% align=center style='border:1px solid #133e71;border-collapse:collapse'>
	<tr><td colspan=2 align=right>
		<font style='font-size:13'><b><div id="TOT">Net Cost: <%=FormatCurrency(nett_cost)%></div></b></font>
		</td>
	</tr>
	</table>
	<br>
	<table width=95% align=center>
	<tr>
		<td colspan=2 align=right>
		<input type=button class="RSLButton" style='width:70px' title='Cancel this cyclical repair' value=" Cancel " onclick="return_focus(false)">
		<input type=button class="RSLButton" style='width:70px' name="SUBBUT" title='Submit this cyclical repair' value=" SAVE " onclick="return_focus(true)" <%=button_disabled%>>
		</td>
	<tr>
	</table>

<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_cyclrep width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

