<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Portfolio --> Planned Maintenance </title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style  type="text/css" media="screen">
    #filter {border: gray solid 1;width:402px;display:inline;margin:0 0 0 10;padding:10 0 10 0;}
    #creatorder{padding:0 0 11 10;width:100px;display:inline;}
    #content{border: gray solid 1px;height:1px;margin:10 10 0 10;padding:5 5 5 5;}
    h1 {font-size:14px;margin:5px 10px 10px 10px;font-weight: bold;}
    .lable {font-weight: bold;}
    .other_filter{margin-left:8;padding-top:0;padding-bottom:0;margin-top:6px;margin-bottom:6px;}
    .button{padding-left:8px;display:inline;}
    .thinline{border-style: solid;border-color:gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}
</style>
<script type="text/javascript" language="javascript">
	function pmopen() {
 	   window.open("/RSLPlannedMaintenance","pmmainapp","status=1,scrollbars=1,resizable=1,left=10,top=20");
	}
	pmopen();
	
	function openURL(url) {
	    window.parent.navigate(url);
	}
</script>

</head>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>

<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages()" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop_portfolioreport.asp" -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="14">&nbsp;</td>
		<td>
		<table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
	 		<tr> 
        	<tr> 
				<td><b>Planned Maintenance</b></td><td>&nbsp;</td>
			</tr>
		  		
          <td width="35%"><img src="../myImages/HoldingGraphics/portfolio.gif" width="295" height="283"></td>
          		<td width="65%" valign="top"> 
            		Planned works module will be loaded in a new window. If not loaded automatically please <a href="javascript:pmopen();">click here</a> to load.
          		</td>
			</tr>
      	</table>
		</td>
	</tr>
	<tr>
		<td width="14">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom_portfolioreport.asp" -->
</body>
</html>