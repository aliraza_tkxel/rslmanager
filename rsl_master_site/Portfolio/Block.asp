<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size

	Call getData()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	' retrives team details from database and builds 'paged' report
	Function getData()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT BLOCKID, BLOCKNAME, DEVELOPMENTNAME, B.ADDRESS1, B.ADDRESS2, B.POSTCODE , COMPLETIONDATE" &_
					"			FROM P_BLOCK B " &_
					"				LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
					"			ORDER BY BLOCKNAME"
		
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<tbody class='CAPS'>"
			For intRecord = 1 to rsSet.PageSize
				team_name = rsSet("DEVELOPMENTNAME")
				str_data = str_data & "<tr style=""cursor:pointer"" onclick=""load_team(" & rsSet("BLOCKID") & ")"">" &_
					"<td>" & rsSet("BLOCKNAME") & "</td>" &_
					"<td>" & rsSet("DEVELOPMENTNAME") & "</td>" &_
					"<td>" & rsSet("ADDRESS1") & "</td>" &_
					"<td>" & rsSet("ADDRESS2") & "</td>" &_
					"<td>" & rsSet("POSTCODE") & "</td>" &_
					"<td>" & rsSet("COMPLETIONDATE") & "</td>" &_
					"<td style=""border-bottom:1px solid silver"" class=""DEL"" title=""DEL"" onclick=""del_team(" & rsSet("BLOCKID") & ")"">Del</td></tr>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""7"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='BLOCK.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='BLOCK.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='BLOCK.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_
			" <a href='BLOCK.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""7"" align=""center""><b>no blocks exist within the system !!</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""7"" align=""center"" style=""background-color:white"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend
	End Function

	Dim lstDevelopments

	Call OpenDB()
	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "P_development", "DEVELOPMENTID, DEVELOPMENTNAME + ' / ' + SCHEMENAME AS DEVNAME", "DEVELOPMENTNAME + ' / ' + SCHEMENAME", "Please Select", NULL, NULL, "textbox200", NULL)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Portfolio --&gt; Tools --&gt; Block Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
    body
    {
        background-color: White;
        margin: 10px 0px 0px 10px;
    }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_BLOCKNAME|Block Name|TEXT|Y"
        FormFields[1] = "txt_ADDRESS1|Address 1|TEXT|Y"
        FormFields[2] = "txt_ADDRESS2|Address 2|TEXT|N"
        FormFields[3] = "txt_TOWNCITY|Town / City|TEXT|N"
        FormFields[4] = "txt_COUNTY|County|TEXT|N"
        FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|N"
        FormFields[6] = "sel_DEVELOPMENTID|Development/Scheme|SELECT|Y"
        FormFields[7] = "txt_COMPLETIONDATE|Completion Date|DATE|N"


        // called by save and amend buttons
        function click_save(str_action) {
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_team";
            document.RSLFORM.hid_ACTION.value = str_action
            document.RSLFORM.action = "serverside/block_srv.asp"
            document.RSLFORM.submit();
        }

        // called by delete button
        function del_team(int_id) {
            event.cancelBubble = true
            var answer
            answer = window.confirm("Are you sure you wish to delete this block")
            // if yes then send to server page to delete
            if (answer) {
                document.RSLFORM.target = "frm_team";
                document.getElementById("hid_ACTION").value = "DELETE"
                document.getElementById("hid_BLOCKID").value = int_id;
                document.RSLFORM.action = "serverside/block_srv.asp"
                document.RSLFORM.submit();
            }
        }

        // called when user clicks on a row
        function load_team(int_id) {
            document.RSLFORM.target = "frm_team";
            document.getElementById("hid_ACTION").value = "LOAD"
            document.getElementById("hid_BLOCKID").value = int_id;
            document.RSLFORM.action = "serverside/block_srv.asp"
            document.RSLFORM.submit();
        }

        // swaps save button for amend button and vice versa
        // called by server side page
        function swap_button(int_which_one) { //   1 = show amend, 2 = show save
            if (int_which_one == 2) {
                document.getElementById("BTN_SAVE").style.display = "block"
                document.getElementById("BTN_AMEND").style.display = "none"
            }
            else {
                document.getElementById("BTN_SAVE").style.display = "none"
                document.getElementById("BTN_AMEND").style.display = "block"
            }
        }

        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        function swap_div(int_which_one) {
            if (int_which_one == 1) {
                document.getElementById("new_div").style.display = "none";
                document.getElementById("table_div").style.display = "block";
                document.getElementById("tab_dev").src = 'images/tab_block-over.gif';
                document.getElementById("tab_dev_team").src = 'images/tab_new_block-tab_block_ove.gif';
                document.getElementById("tab_amend_dev").src = 'images/tab_amend_block.gif';
            }
            else if (int_which_one == 2) {
                clear()
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_dev").src = 'images/tab_block.gif';
                document.getElementById("tab_dev_team").src = 'images/tab_new_block-over.gif';
                document.getElementById("tab_amend_dev").src = 'images/tab_amend_block-tab_new_blo.gif';
                swap_button(2)
            }
            else {
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_dev").src = 'images/tab_block.gif';
                document.getElementById("tab_dev_team").src = 'images/tab_new_block.gif';
                document.getElementById("tab_amend_dev").src = 'images/tab_amend_block-over.gif';
                swap_button(3)
            }
            checkForm()
        }

        // clear form fields
        function clear() {
            document.getElementById("txt_BLOCKNAME").value = ""
            document.getElementById("sel_DEVELOPMENTID").value = ""
            document.getElementById("txt_ADDRESS1").value = ""
            document.getElementById("txt_ADDRESS2").value = ""
            document.getElementById("txt_TOWNCITY").value = ""
            document.getElementById("txt_COUNTY").value = ""
            document.getElementById("txt_POSTCODE").value = ""
            document.getElementById("txt_COMPLETIONDATE").value = ""
        }

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_dev" id="tab_dev" onclick="swap_div(1)" style="cursor: pointer" src="images/tab_block-over.gif"
                    width="54" height="20" border="0" alt="" /></td>
            <td rowspan="2">
                <img name="tab_dev_team" id="tab_dev_team" onclick="swap_button(2);swap_div(2)" style="cursor: pointer"
                    src="images/tab_new_block-tab_block_ove.gif" width="89" height="20" border="0" alt="" /></td>
            <td rowspan="2">
                <img name="tab_amend_dev" id="tab_amend_dev" src="images/tab_amend_block.gif" width="112" height="20"
                    border="0" alt="" /></td>
            <td>
                <img src="images/spacer.gif" width="492" height="19" border="0" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="492" height="1" alt="" /></td>
        </tr>
    </table>
    <!-- End ImageReady Slices -->
    <div id="new_div" style="display: none">
        <table width="750" style="border-right: solid 1px #133e71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr style="height: 3px">
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Block Name:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_BLOCKNAME" id="txt_BLOCKNAME" maxlength="100"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_BLOCKNAME" id="img_BLOCKNAME" width="15px" height="15px"
                        border="0" alt="" />
                </td>
                <td width="100%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Development/Scheme:
                </td>
                <td>
                    <%=lstDevelopments%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DEVELOPMENTID" id="img_DEVELOPMENTID" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Address 1:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1" maxlength="100"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Address 2:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2" maxlength="100"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Town / City:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_TOWNCITY" id="txt_TOWNCITY" maxlength="100"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_TOWNCITY" id="img_TOWNCITY" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    County:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" maxlength="100"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Postcode:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10"
                        size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Completion Date:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_COMPLETIONDATE" id="txt_COMPLETIONDATE"
                        maxlength="10" size="30" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_COMPLETIONDATE" id="img_COMPLETIONDATE" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <input type="button" id="BTN_SAVE" name="BTN_SAVE" value="Save" title="Save" class="rslbutton"
                        onclick="click_save('NEW')" style="display: block; cursor:pointer" />
                    <input type="button" id="BTN_AMEND" name='BTN_AMEND' value="Amend" title="Amend" class="rslbutton"
                        onclick="click_save('AMEND')" style="display: none; cursor:pointer" />
                </td>
            </tr>
        </table>
    </div>
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="behavior: url(/Includes/Tables/tablehl.htc);
            border-collapse: collapse" slcolor='' border="1" hlcolor="#4682b4">
            <thead>
                <tr style="height: 3px; border-bottom: 1px solid #133e71">
                    <td colspan="6" class="TABLE_HEAD">
                    </td>
                </tr>
                <tr>
                    <td width="150px" class="TABLE_HEAD">
                        <b>Block Name</b>
                    </td>
                    <td width="200px" class="TABLE_HEAD">
                        <b>Development Name</b>
                    </td>
                    <td width="150px" class="TABLE_HEAD">
                        <b>Address 1 </b>
                    </td>
                    <td width="150px" class="TABLE_HEAD">
                        <b>Address 2</b>
                    </td>
                    <td width="70px" class="TABLE_HEAD">
                        <b>Postcode</b>
                    </td>
                    <td class="TABLE_HEAD" nowrap="nowrap">
                        <b>Completion Date</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class="TABLE_HEAD" colspan="7" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
    </div>
    <input type="hidden" name="hid_BLOCKID" id="hid_BLOCKID" />
    <input type="hidden" name="hid_ACTION" id="hid_ACTION" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="1px" height="1px" style="display: none">
    </iframe>
</body>
</html>
