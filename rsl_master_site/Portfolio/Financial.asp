<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION, lstrentfrequency, lstfundingauthority, lstcharge, PropertyID, l_address1, f_targetrentset

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then
		PropertyID = -1 
		ACTION = "NEW"
	Else
		ACTION = "AMEND"
	End If

	Call OpenDB()
		'--Retrieve First Line of Address
		SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
		Call OpenRs(rsAddress, SQL)
		If (NOT rsAddress.EOF) Then
			l_address1 = rsAddress("ADDRESS1")
		Else
			l_address1 = "[None Given]"
		End If
		l_housenumber = rsAddress("HOUSENUMBER")
		l_propertyid = UCASE(rsAddress("PROPERTYID"))
		l_propertystatus = l_propertyid
		Call CloseRs(rsAddress)
		'--Retrieve First Line of Address

	f_renteffective = "31/03/2005"
	f_daterentset = "01/04/2004"

	SQL = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		f_targetrentset = rsLoader("TARGETRENTSET")  'This is when a property becomes available, to keep in line with rent targets
		F_nominatingbody = rsLoader("NOMINATINGBODY")
		f_renttype = rsLoader("RENTTYPE")
		f_renteffective = rsLoader("RENTEFFECTIVE")
		If isNull(f_renteffective) or f_renteffective = "" Then f_renteffective = "31/03/2005" End If
		f_targetrent = rsLoader("TARGETRENT")
		If isNull(f_targetrent) Then f_targetrent = FormatNumber(0.00) End If
		f_daterentset = rsLoader("DATERENTSET")
		If isNull(f_daterentset) Then f_daterentset = "01/04/2004" End If
		f_fundingauthority = rsLoader("FUNDINGAUTHORITY")
		f_charge = rsLoader("CHARGE")
		f_chargevalue = rsLoader("CHARGEVALUE")
		If isNull(f_chargevalue) Then f_chargevalue = "0.00" End If
		f_rent = rsLoader("RENT")
		If isNull(f_rent) Then f_rent = "0.00" End If
		f_counciltax = rsLoader("COUNCILTAX")
		If isNull(f_counciltax) Then f_counciltax = "0.00" End If
		f_services = rsLoader("SERVICES")
		If isNull(f_services) Then f_services = "0.00" End If
		f_water = rsLoader("WATERRATES")
		If isNull(f_water) Then f_water = "0.00" End If
		f_support = rsLoader("SUPPORTEDSERVICES")
		If isNull(f_support) Then f_support = "0.00" End If
		f_inelig = rsLoader("INELIGSERV")
		If isNull(f_inelig) Then f_inelig = "0.00" End If
		f_totalrent = rsLoader("TOTALRENT")
		If isNull(f_totalrent) Then f_totalrent = "0.00" End If
		f_garage = rsLoader("GARAGE")
		If isNull(f_garage) Then f_garage = "0.00" End If
		f_rentfrequency = rsLoader("RENTFREQUENCY")
		'valuation boxes
		f_omvst = rsLoader("OMVST")
		If isNull(f_omvst) Then f_omvst = "0.00" End If
		f_euv = rsLoader("EUV")
		If isNull(f_euv) Then f_euv = "0.00" End If
		f_insurancevalue = rsLoader("INSURANCEVALUE")
		If isNull(f_insurancevalue) Then f_insurancevalue = FormatNumber(0.00) End If
		f_omv = rsLoader("OMV")
		If isNull(f_omv) Then f_omv = "0.00" End If
	End If

	Call BuildSelect(lstrentfrequency, "sel_RENTFREQUENCY", "P_RENTFREQUENCY", "FID, DESCRIPTION", "DESCRIPTION", "Please Select", f_rentfrequency, NULL, "textbox", "style='width:200px'  tabindex=1")
	Call BuildSelect(lstfundingauthority, "sel_FUNDINGAUTHORITY", "P_FUNDINGAUTHORITY", "FUNDINGAUTHORITYID, DESCRIPTION", "DESCRIPTION", "Please Select", f_fundingauthority, NULL, "textbox", "  tabindex=8")
	Call BuildSelect(lstcharge, "sel_CHARGE", "P_CHARGE", "CHARGEID, DESCRIPTION", "DESCRIPTION", "Please Select", f_charge, NULL, "textbox", "style='width:200px'  tabindex=2")

	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio--&gt; Financial</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .style3
        {
            color: #FFFFFF;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_NOMINATINGBODY|Nominating Body|TEXT|N"
        FormFields[1] = "sel_FUNDINGAUTHORITY|Funding Authority|SELECT|Y"
        FormFields[2] = "txt_INSURANCEVALUE|Insurance Value|CURRENCY|N"
        FormFields[3] = "sel_CHARGE|Charge|SELECT|N"
        FormFields[4] = "txt_CHARGEVALUE|Charge Value|CURRENCY|N"
        FormFields[5] = "txt_OMVST|OMV St|CURRENCY|N"
        FormFields[6] = "txt_EUV|EUV|CURRENCY|N"
        FormFields[7] = "txt_OMV|OMV|CURRENCY|N"
        FormFields[8] = "sel_RENTFREQUENCY|Rent Frequency|SELECT|Y"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide_ivuso/financial_Main_svr.asp"
            document.RSLFORM.submit()
        }

    </script>
</head>
<body onload="initSwipeMenu(1);document.getElementById('txt_NOMINATINGBODY').focus();"
    onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Main Details">
                    <img name="tab_main_details" src="images/tab_main_details.gif" width="97" height="20"
                        border="0" alt="Main Details" /></a></td>
            <td rowspan="2">
                <img name="tab_financial" src="images/tab_financial-over.gif" width="79" height="20"
                    border="0" alt="Financial" /></td>        
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Warranties">
                    <img name="tab_warranties" src="images/tab_warranties.gif" width="89" height="20"
                        border="0" alt="Warranties" /></a></td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Utilities">
                    <img name="tab_utilities" src="images/tab_utilities.gif" width="68" height="20" border="0"
                        alt="Utilities" /></a></td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Marketing">
                    <img name="tab_marketing" src="images/tab_marketing.gif" width="76" height="20" border="0"
                        alt="Marketing" /></a></td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>"
                    title="Health and Safety">
                    <img name="tab_hands" src="images/tab_healthsafety_1.gif" width="110" height="20"
                        border="0" alt="Health and Safety" /></a></td>
            <td>
                <img src="/myImages/spacer.gif" width="156" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="images/spacer.gif" width="156" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style="height: 7px">
            <td width="19%">
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top" nowrap="nowrap">
                <table width="380" border="0" cellspacing="0" cellpadding="1">
                    <tr>
                        <td>
                            Rent Frequency:
                        </td>
                        <td>
                            <%=lstrentfrequency%><img src="/js/FVS.gif" name="img_RENTFREQUENCY" id="img_RENTFREQUENCY"
                                width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="134">
                            Nominating Body:
                        </td>
                        <td width="242">
                            <input type="text" name="txt_NOMINATINGBODY" id="txt_NOMINATINGBODY" class="textbox200"
                                value="<%=f_nominatingbody%>" tabindex="1" />
                            <img src="/js/FVS.gif" name="img_NOMINATINGBODY" id="img_NOMINATINGBODY" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Funding Authority:
                        </td>
                        <td>
                            <%=lstfundingauthority%><img src="/js/FVS.gif" name="img_FUNDINGAUTHORITY" id="img_FUNDINGAUTHORITY"
                                width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Charge:
                        </td>
                        <td>
                            <%=lstCharge%><img src="/js/FVS.gif" name="img_CHARGE" id="img_CHARGE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Charge Value :
                        </td>
                        <td>
                            <input name="txt_CHARGEVALUE" type="text" class="textbox200" id="txt_CHARGEVALUE"
                                tabindex="7" onblur="checkForm(true)" value="<%=FormatNumber(f_chargevalue)%>" />
                            <img src="/js/FVS.gif" name="img_CHARGEVALUE" id="img_CHARGEVALUE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Valuation</strong>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OMV St.
                        </td>
                        <td>
                            <input type="text" onblur="checkForm(true)" name="txt_OMVST" id="txt_OMVST" class="textbox200"
                                value="<%=FormatNumber(f_omvst)%>" tabindex="5" />
                            <img src="/js/FVS.gif" name="img_OMVST" id="img_OMVST" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            EUV
                        </td>
                        <td>
                            <input type="text" onblur="checkForm(true)" name="txt_EUV" id="txt_EUV" class="textbox200"
                                value="<%=FormatNumber(f_euv)%>" tabindex="5" />
                            <img src="/js/FVS.gif" name="img_EUV" id="img_EUV" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Insurance Value:
                        </td>
                        <td>
                            <input type="text" onblur="checkForm(true)" name="txt_INSURANCEVALUE" id="txt_INSURANCEVALUE"
                                class="textbox200" value="<%=FormatNumber(f_insurancevalue)%>" tabindex="5" />
                            <img src="/js/FVS.gif" name="img_INSURANCEVALUE" id="img_INSURANCEVALUE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OMV
                        </td>
                        <td>
                            <input type="text" onblur="checkForm(true)" name="txt_OMV" id="txt_OMV" class="textbox200"
                                value="<%=FormatNumber(f_omv)%>" tabindex="5" />
                            <img src="/js/FVS.gif" name="img_OMV" id="img_OMV" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button" class="RSLButton" value=" SAVE " title=" SAVE " onclick="SaveForm()"
                                tabindex="15" name="button" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="51%" valign="top" nowrap="nowrap">
                <iframe src="iFrames_iVuso/iFinancial_CurrentUpdate.asp?PropertyID=<%=PropertyID%>"
                    scrolling="no" frameborder="0" style="height: 300px; width: 365px"></iframe>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                &nbsp;
            </td>
            <td colspan="2">
                &nbsp;
            </td>
            <td align="right" nowrap="nowrap">
                <input type="hidden" name="hid_PFUserID" id="hid_PFUserID" value="<%=Session("UserID")%>" />
                <input type="hidden" name="hid_PropertyID" id="hid_PropertyID" value="<%=PropertyID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="5">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe name="frm_team" id="frm_team" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
