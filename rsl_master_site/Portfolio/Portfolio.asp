<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width="14">&nbsp;</td>
    <td> 
      <table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
        <tr> 
        <tr> 
          <td><b>Portfolio</b></td>
          <td>&nbsp;</td>
        </tr>
          <td width="35%"><img src="../myImages/HoldingGraphics/portfolio.gif" width="295" height="283"></td>
          <td width="65%" valign="top"> 
            <p><br>
              The Portfolio Module contains property specific data and channels 
              this information to relevant parts of the system.<br>
              <br>
            </p>
            <ul>
              <li> Add new asset records and immediately publish to Housing Association 
                web sites</li>
              <li> Matches prospective customers with available properties</li>
              <li> Integrates rental data with financial systems</li>
              <li> Responsive repairs logged and profiled ensuring that planned 
                maintenance is cost effective</li>
              <li> Sharing of appropriate information with suppliers, contractors 
                and customers</li>
            </ul>
            <p></p>
			<ul>
              <li>
			To view a list of all cyclical repairs click the following link : <a href="#" onclick="window.open('CyclicalRepairList.asp', 'CYCLIST', 'scrollbars=yes, resizable =yes, width=800, height=600')"><font color=blue>Cyclical Repair List</font></a>
			</ul>
</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width="14">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

