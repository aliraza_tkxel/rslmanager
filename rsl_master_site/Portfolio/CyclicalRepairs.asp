<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstwarrantytype, lstcontractor, pmstring, defectsperiod, lstareaitem, lst_block, str_selbox

	ACTION = "NEW"
	DevelopmentID = Request("DevelopmentID")
	if (PropertyID = "") then PropertyID = -1 end if
	
	OpenDB()
	
	// get defects period from ssociaited development
	SQL = "SELECT D.DEFECTSPERIOD FROM P__PROPERTY P " &_
			" LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then defectsperiod = rsLoader(0) else defectsperiod = "Non Attached" End If
	CloseRs(rsLoader)

		SQL = "SELECT  p.PMID, ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, " &_
					"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
					"ISNULL(P.CYCLE,'') AS CCYCLE, " &_
					"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_
					"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
					"ISNULL(P.NOTES ,'') AS CNOTES, " &_
					"ISNULL(S.NAME,'') AS CONTRACTOR, " &_
					"R.DESCRIPTION " &_
			  	"FROM 	P_PLANNEDMAINTENANCE P " &_
			  		"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
					"INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
					"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
				"WHERE 	P.DEVELOPMENTID = " & developMentid	
	//response.write SQL
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		WHILE NOT rsLoader.EOF
			pmstring = pmstring &_
						"<TR TITLE='" & rsLoader("DESCRIPTION") & "' style='cursor:hand')>" &_
							"<TD width=100 nowrap>" & rsLoader("CBLOCK") & "</TD>" &_
							"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
							"<TD width=240 nowrap>" & rsLoader("CREPAIR") & "</TD>" &_
							"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
							"<TD width=190 nowrap>" & rsLoader("CONTRACTOR") & "</TD>" &_
							"<TD bgcolor=white width=500 align=center onclick=""DeleteMe(" & rsLoader("PMID") & ")""><font color=red>DEL</font></TD></TR>"						
			rsLoader.MoveNext
		wend
		
	else
		pmstring = pmstring & "<tfoot><Tr><TD colspan=6 align=center>No Cyclical Repairs exist for this Development</TD></TR></tfoot	>" 
	end if
	CloseRs(rsLoader)
	
	CALL BuildSelect(lstContractors, "sel_CONTRACTOR", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' " & DisableAll & "")
	CALL BuildSelect(lst_block, "sel_BLOCK", "P_BLOCK B WHERE EXISTS (SELECT BLOCKID FROM P__PROPERTY WHERE BLOCKID = B.BLOCKID) AND DEVELOPMENTID = " & DevelopmentID, "BLOCKID, BLOCKNAME", "BLOCKNAME", "All", 0, NULL, "textbox200", " style='width:300px' ")
	get_repairs()
	CloseDB()

	Function get_repairs()
		
		SQL = "SELECT * FROM R_ITEMDETAIL WHERE MAINTENANCECODE LIKE 'C%' AND AREAITEMID <> 38 ORDER BY DESCRIPTION"		
		Call OpenRs(rsSet, SQL)
		// BEWARE, THE CODE 822 IS HARD CODED HERE AND IS A DUMMY ROW IN THE TABLE
		str_selbox = " <select class='textbox200' name='sel_REPAIR' style='width:300px'><OPTION VALUE=''>Select Item</option><OPTION VALUE='822'>Annual Gas Service</option>"
		While not rsSet.EOf 
			str_selbox = str_selbox & "<OPTION VALUE='" & rsSet("ITEMDETAILID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.movenext()
		Wend 
		str_selbox = str_selbox & "</SELECT>"
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio --> Warranty Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();

	FormFields[0] = "txt_STARTDATE|STARTDATE|DATE|Y"
	FormFields[1] = "txt_CYCLE|CYCLES|INTEGER|Y"
	FormFields[2] = "sel_REPAIR|REPAIR|SELECT|Y"
	FormFields[3] = "sel_CONTRACTOR|CONTRACTOR|SELECT|Y"
	FormFields[4] = "sel_BLOCK|BLOCK|SELECT|N"
	FormFields[5] = "txt_FREQUENCY|FREQUENCY|INTEGER|Y"
	FormFields[6] = "txt_CONTRACTSTARTDATE|Contract Start Date|DATE|Y"
	
	
	function check_is_ok(){
		
		if (!checkForm()) return false;
		var str_request = "POPUPS/ISOK2.ASP?COMMENCE="+RSLFORM.txt_CONTRACTSTARTDATE.value+"&START="+RSLFORM.txt_STARTDATE.value+"&BLOCK="+RSLFORM.sel_BLOCK.value+"&DEVELOPMENT=<%=DevelopmentID%>&REPAIR="+RSLFORM.sel_REPAIR.value
		var answer = window.showModalDialog(str_request, "", "dialogHeight:600px; dialogWidth:600px");
		//window.open(str_request, "display","width=600,height=600,left=100,top=200");

		if (answer == true)
			SaveForm();
	}
	
	function SaveForm(){

		RSLFORM.target = "frm_cyclrep";
		RSLFORM.action = "serverside/cyclerep_srv.asp";		
		RSLFORM.submit();
	
	}
	
	function DeleteMe(WarID){

		event.cancelBubble = true;
		answer = window.confirm("Are you sure you wish to delete this repair cycle?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
		if (answer) {
			RSLFORM.hid_Action.value = "DELETE"
			RSLFORM.hid_PMID.value = WarID			
			RSLFORM.action =  "serverside/cyclerep_srv.asp";
			RSLFORM.target = "frm_cyclrep";
			RSLFORM.submit()
			}
		}

	function LoadForm(ID){
	
		var str_request = "POPUPS/cycdetails.asp?rmid="+ID;
		window.showModalDialog(str_request, "display","width=300,height=600,left=100,top=200");

	}

	function SetButtons(Code){
	
		if (Code == "AMEND") {
			document.getElementById("AddButton").style.display = "none"
			document.getElementById("AmendButton").style.display = "block"		
			}
		else {
			document.getElementById("AddButton").style.display = "block"
			document.getElementById("AmendButton").style.display = "none"		
			}
	}

	function AmendForm(){
	
		RSLFORM.hid_Action.value = "AMEND"
		if (!checkForm()) return false;
		RSLFORM.action = "serverside/cyclerep_srv.asp";
		RSLFORM.target = "frm_cyclrep";
		RSLFORM.submit()
	}

</SCRIPT>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --> 
<form name = RSLFORM method=post>
<!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR> 
	  <TD ROWSPAN=2> <a href="development.asp"><IMG NAME="tab_dev" style='cursor:hand' SRC="images/dev-closed.gif" WIDTH=100 HEIGHT=20 BORDER=0></a></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_dev_team" style='cursor:hand' SRC="images/new-dev-closed.gif" WIDTH=133 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_amend_dev" SRC="images/cyc_open.gif" WIDTH=126 HEIGHT=20 BORDER=0></TD>
	  <TD height=19 align=right> <font color=green>CYM</font> - Cyclical Repairs</TD>
	</TR>
	<TR> 
	  <TD BGCOLOR=#133E71> <IMG SRC="images/spacer.gif" WIDTH=388 HEIGHT=1></TD>
	</TR>
</TABLE>
<!-- End ImageReady Slices -->
  <table width="99%" border="0" cellpadding="2" cellspacing="0"  style=" border-right: 1px solid #133E71; border-LEFT: 1px solid #133E71;  border-BOTTOM: 1px solid #133E71 ">
    <tr style='height:7px'> 
      <td></td>
    </tr>
    <TR> 
      <TD>Scheme/Block :</td>
      <TD> <%=lst_block%> <image src="/js/FVS.gif" name="img_BLOCK" width="15px" height="15px" border="0"> 
      </TD>
      <TD>Occurences: </td>
      <td> 
        <input type=TEXT name=txt_FREQUENCY class='TEXTBOX' style='width:180px' maxlength=10>
        <image src="/js/FVS.gif" name="img_FREQUENCY" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
    <TR> 
      <TD>Repair Details :</td>
      <TD> <%=str_selbox%> <image src="/js/FVS.gif" name="img_REPAIR" width="15px" height="15px" border="0"> 
      </TD>
      <TD>Occurs every: </TD>
      <TD> 
        <input type=TEXT name=txt_CYCLE class='TEXTBOX' style='width:180px' maxlength=10>
        <image src="/js/FVS.gif" name="img_CYCLE" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
    <TR> 
      <TD>Contractor</td>
      <TD><%=lstContractors%><image src="/js/FVS.gif" name="img_CONTRACTOR" width="15px" height="15px" border="0"> 
      </TD>
      <TD>Notes:</TD>
      <TD rowspan="3" valign=top> 
        <textarea style='OVERFLOW:HIDDEN;width:180px' class='textbox200' rows=4 name="txt_NOTES"></textarea>
      </TD>
    </TR>
    <TR> 
      <TD>Contract Start :</td>
      <TD> 
        <INPUT TYPE=TEXT NAME=txt_CONTRACTSTARTDATE CLASS='TEXTBOX' style='width:180px' MAXLENGTH=10>
        <image src="/js/FVS.gif" name="img_CONTRACTSTARTDATE" width="15px" height="15px" border="0"> 
      </TD>
      <TD>&nbsp;</TD>
      <TD> </TD>
    </TR>
    <TR> 
      <TD nowrap>1st Occurence:</td>
      <TD> 
        <INPUT TYPE=TEXT NAME=txt_STARTDATE CLASS='TEXTBOX' style='width:180px' MAXLENGTH=10>
        <image src="/js/FVS.gif" name="img_STARTDATE" width="15px" height="15px" border="0"> 
      </TD>
      <td>&nbsp;</td>
    </TR>
    <TR> 
      <TD colspan=4 align=right valign=top NOWRAP> 
        <input type=button class="RSLButton" style='width:70px' value=" RESET " onclick="RSLFORM.reset()">
        <input type=button class="RSLButton" style='width:70px' value=" SAVE " onclick="check_is_ok()" name="AddButton">
        <input type=button class="RSLButton" value=" AMEND " onclick="AmendForm()" name="AmendButton" DISABLED style='display:none;width:70px'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </TD>
    </TR>
  </TABLE>
  <BR>
	 <TABLE cellspacing=0 cellpadding=0 border=0 style='border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="230">
	<TR bgcolor='#133E71' style='color:#FFFFFF'> 
	  <TD HEIGHT=20PX width=100 nowrap><b>&nbsp;Block</b></TD>
	  <TD width=90 nowrap><b>&nbsp;Start Date</b></TD>
	  <TD width=240 nowrap><b>&nbsp;Repair</b></TD>
	  <TD width=100 nowrap><b>&nbsp;Last Cycle</b></TD>	  
	  <TD width=190 NOWRAP><b>&nbsp;Contractor</b></TD>	  
	  <TD width=500></TD>	  	  
	</TR>
	<TR>
	  <td valign=top height=100% colspan=7> 
		<div style='height:190;width:750;overflow:auto' class='TA'> 
		<table cellspacing=0 cellpadding=3 border=1 style='border-collapse:collapse' slcolor='' hlcolor='STEELBLUE' width=100%>
		<%=pmstring%>
		</table>
		</div>
	  </td>
	</tr>
	</TABLE>
	    <input type=hidden name="hid_PMID" value="<%=PMID%>">
	    <input type=hidden name="hid_DevelopmentID" value="<%=DevelopmentID%>">
	    <input type=hidden name="hid_Action" value="<%=ACTION%>">
	    <input type=hidden name="hid_USERID" value="<%=Session("USERID")%>">
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp"  name=frm_cyclrep width=400px height=400px style='display:BLOCK'></iframe>
</BODY>
</HTML>

