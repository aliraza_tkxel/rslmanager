<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName,searchSQL,FY,lstFY,searchsubSQL
    Dim rsSetYR
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	searchName = Replace(Request("name"),"'","''")
    searchSQL = ""
	if searchName <> "" Then
		searchSQL = " WHERE (DEV.SCHEMENAME LIKE '%" & searchName & "%' ) "
	End If

    OpenDB()
    
    FY = Request("FY")
    searchsubSQL=""
    if FY<>"" Then
    	Call OpenRS(rsSetYR, "SELECT CONVERT(VARCHAR, YSTART, 103) AS YSTART, CONVERT(VARCHAR, YEND, 103) AS YEND FROM F_FISCALYEARS WHERE YRANGE=" & FY)
        searchsubSQL=" AND ISNULL(ISNULL(PIT.PAIDON,PIT.PIDATE),PIT.PICREATED) >= CONVERT(SMALLDATETIME,'" & rsSetYR("YSTART") & "')"
        searchsubSQL=searchsubSQL & " AND ISNULL(ISNULL(PIT.PAIDON,PIT.PIDATE),PIT.PICREATED) <= CONVERT(SMALLDATETIME,'" & rsSetYR("YEND") & "')"
    end if

	
	

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "SchemeReportMain.asp"
	MaxRowSpan = 6
	
	
	
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Call BuildSelect(lstFY, "sel_FY", "F_FISCALYEARS", "YRANGE, CAST(YEAR(YSTART) AS VARCHAR) + ' - ' + CAST(YEAR(YEND) AS VARCHAR) AS FY", "YRANGE", "ALL", FY, NULL, "textbox200", "")		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 

			intRecord = 0
			str_data = ""
			strSQL	=	"SELECT 	(ISNULL(DEV_REPAIRS.FURNTOTAL,0) + ISNULL(DEV_NONREPAIRS.FURNTOTAL,0) + ISNULL(BLOCK_REPAIRS.FURNTOTAL,0) + ISNULL(BLOCK_NONREPAIRS.FURNTOTAL,0) + ISNULL(PROPERTYREPAIRS.FURNTOTAL,0)) AS ALL_FURNTOTAL, " &_
			"	(ISNULL(DEV_REPAIRS.MANAGETOTAL,0) + ISNULL(DEV_NONREPAIRS.MANAGETOTAL,0) + ISNULL(BLOCK_REPAIRS.MANAGETOTAL,0) + ISNULL(BLOCK_NONREPAIRS.MANAGETOTAL,0) + ISNULL(PROPERTYREPAIRS.MANAGETOTAL,0)) AS ALL_MANAGETOTAL, " &_
			"	(ISNULL(DEV_REPAIRS.SERVICESTOTAL,0) + ISNULL(DEV_NONREPAIRS.SERVICESTOTAL,0) + ISNULL(BLOCK_REPAIRS.SERVICESTOTAL,0) + ISNULL(BLOCK_NONREPAIRS.SERVICESTOTAL,0) + ISNULL(PROPERTYREPAIRS.SERVICESTOTAL,0)) AS ALL_SERVICESTOTAL, " &_
			"	(ISNULL(DEV_REPAIRS.MAINTTOTAL,0) + ISNULL(DEV_NONREPAIRS.MAINTTOTAL,0) + ISNULL(BLOCK_REPAIRS.MAINTTOTAL,0) + ISNULL(BLOCK_NONREPAIRS.MAINTTOTAL,0) + ISNULL(PROPERTYREPAIRS.MAINTTOTAL,0) ) AS ALL_MAINTTOTAL, " &_
			"   DEV.DEVELOPMENTID as DEVID, DEV.SCHEMENAME AS SCHEME " &_
			"FROM P_DEVELOPMENT  DEV " &_
			"	LEFT JOIN (  " &_
			"			SELECT 	SUM(CASE H.COSTCENTREID WHEN 22 THEN PIT.GROSSCOST ELSE 0 END) AS FURNTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 20 THEN PIT.GROSSCOST ELSE 0 END) AS MANAGETOTAL, " &_ 
			"				SUM(CASE H.COSTCENTREID WHEN 13 THEN PIT.GROSSCOST ELSE 0 END) AS SERVICESTOTAL, " &_ 
			"				SUM(CASE H.COSTCENTREID WHEN 11 THEN PIT.GROSSCOST ELSE 0 END) AS MAINTTOTAL, " &_
			"				D.DEVELOPMENTID AS DEVELOPMENTID   " &_
			"			FROM F_PURCHASEITEM  PIT " &_
			"				INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
			"				INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
			"				INNER JOIN P_WORKORDER WO ON PIT.ORDERID = WO.ORDERID " &_
			"				INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_
			"			WHERE 	WO.DEVELOPMENTID IS NOT NULL AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
			"			GROUP BY D.DEVELOPMENTID )DEV_REPAIRS ON DEV.DEVELOPMENTID = DEV_REPAIRS.DEVELOPMENTID  " &_
			"	LEFT JOIN (  " &_
			"			SELECT 	SUM(CASE H.COSTCENTREID WHEN 22 THEN PIT.GROSSCOST ELSE 0 END) AS FURNTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 20 THEN PIT.GROSSCOST ELSE 0 END) AS MANAGETOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 13 THEN PIT.GROSSCOST ELSE 0 END) AS SERVICESTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 11 THEN PIT.GROSSCOST ELSE 0 END) AS MAINTTOTAL, " &_
			"				D.DEVELOPMENTID AS DEVELOPMENTID  " &_ 
			"			FROM F_PURCHASEITEM  PIT " &_
			"				INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
			"				INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
			"				INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID " &_
			"				INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID " &_
			"			WHERE 	PO.DEVELOPMENTID IS NOT NULL AND  PO.POTYPE = 1 AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
			"			GROUP BY D.DEVELOPMENTID)DEV_NONREPAIRS ON DEV.DEVELOPMENTID = DEV_NONREPAIRS.DEVELOPMENTID " &_
			"	LEFT JOIN (  " &_
			"			SELECT 	SUM(CASE H.COSTCENTREID WHEN 22 THEN PIT.GROSSCOST ELSE 0 END) AS FURNTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 20 THEN PIT.GROSSCOST ELSE 0 END) AS MANAGETOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 13 THEN PIT.GROSSCOST ELSE 0 END) AS SERVICESTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 11 THEN PIT.GROSSCOST ELSE 0 END) AS MAINTTOTAL, " &_
			"				D.DEVELOPMENTID AS DEVELOPMENTID    " &_
			"			FROM F_PURCHASEITEM  PIT " &_
			"				INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
			"				INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
			"				INNER JOIN P_WORKORDER WO ON PIT.ORDERID = WO.ORDERID " &_
			"				INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
			"				INNER JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
			"				INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
			"			WHERE 	WO.BLOCKID IS NOT NULL AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
			"			GROUP BY D.DEVELOPMENTID )BLOCK_REPAIRS ON DEV.DEVELOPMENTID= BLOCK_REPAIRS.DEVELOPMENTID " &_
			"	LEFT JOIN (  " &_
			"			SELECT 	SUM(CASE H.COSTCENTREID WHEN 22 THEN PIT.GROSSCOST ELSE 0 END) AS FURNTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 20 THEN PIT.GROSSCOST ELSE 0 END) AS MANAGETOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 13 THEN PIT.GROSSCOST ELSE 0 END) AS SERVICESTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 11 THEN PIT.GROSSCOST ELSE 0 END) AS MAINTTOTAL, " &_
			"				D.DEVELOPMENTID AS DEVELOPMENTID   " &_
			"			FROM F_PURCHASEITEM  PIT " &_
			"				INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
			"				INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
			"				INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID " &_
			"				INNER JOIN P_BLOCK B ON B.BLOCKID = PO.BLOCKID " &_
			"				INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
			"			WHERE 	PO.BLOCKID IS NOT NULL AND  PO.POTYPE = 1 AND PIT.ACTIVE = 1 AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
			"			GROUP BY D.DEVELOPMENTID)BLOCK_NONREPAIRS ON DEV.DEVELOPMENTID = BLOCK_NONREPAIRS.DEVELOPMENTID  " &_
			"	LEFT JOIN (  " &_
			"			SELECT 	SUM(CASE H.COSTCENTREID WHEN 22 THEN PIT.GROSSCOST ELSE 0 END) AS FURNTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 20 THEN PIT.GROSSCOST ELSE 0 END) AS MANAGETOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 13 THEN PIT.GROSSCOST ELSE 0 END) AS SERVICESTOTAL,  " &_
			"				SUM(CASE H.COSTCENTREID WHEN 11 THEN PIT.GROSSCOST ELSE 0 END) AS MAINTTOTAL, " &_
			"				D.DEVELOPMENTID AS DEVELOPMENTID   " &_
			"			FROM F_PURCHASEITEM  PIT " &_
			"				INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = PIT.EXPENDITUREID  " &_
			"				INNER JOIN F_HEAD H ON F_EXPENDITURE.HEADID = H.HEADID   " &_
			"				INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PIT.ORDERID " &_
			"				INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID " &_
			"				INNER JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
			"				INNER JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " &_
			"			WHERE 	 PIT.ACTIVE = 1	AND H.COSTCENTREID IN (11,13,20,22) " & searchsubSQL &_
			"			GROUP BY D.DEVELOPMENTID )PROPERTYREPAIRS ON DEV.DEVELOPMENTID = PROPERTYREPAIRS.DEVELOPMENTID     " & searchSQL &_
			" ORDER	BY DEV.SCHEMENAME "

			'rw strSQL & "<BR>"
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
									
				MAINTTOTAL 		= rsSet("ALL_MAINTTOTAL")
				MANAGETOTAL 	= rsSet("ALL_MANAGETOTAL")
				FURNTOTAL 		= rsSet("ALL_FURNTOTAL")
				SERVICESTOTAL 	= rsSet("ALL_SERVICESTOTAL")
				GROSSCOST 		= (MAINTTOTAL + MANAGETOTAL + FURNTOTAL + SERVICESTOTAL)
				
					STRrow  =  STRrow & "<tr style='cursor:hand' onClick=""LoadWorkOrders(" & rsSet("DEVID") & ", '" & replace(rsSet("SCHEME"),"'","") & "')"">" &_
						"  <td align='left' bgcolor='#F5F5F5'>" & rsSet("SCHEME") & "</td>" &_
						"  <td align='right' >" & FormatCurrency(MAINTTOTAL,2) & "</td>" &_
						"  <td align='right' >" & FormatCurrency(MANAGETOTAL,2) & "</td>" &_
						"  <td align='right' >" & FormatCurrency(FURNTOTAL,2) & "</td>" &_
						"  <td align='right'>" & FormatCurrency(SERVICESTOTAL,2) & "</td>" &_
						"  <td align='right' bgcolor='#E0EEFE'>" & FormatCurrency(GROSSCOST,2) & "</td>" &_
						"</tr>"
	
					str_data = STRrow
						bottom_MAINTTOTAL		= bottom_MAINTTOTAL + MAINTTOTAL
						bottom_MANAGETOTAL		= bottom_MANAGETOTAL + MANAGETOTAL
						bottom_FURNTOTAL		= bottom_FURNTOTAL + FURNTOTAL
						bottom_SERVICESTOTAL	= bottom_SERVICESTOTAL + SERVICESTOTAL
						bottom_GROSSCOST		= bottom_GROSSCOST + GROSSCOST
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				str_data = str_data & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript defer>

	function LoadWorkOrders(DevID,DevName){

	    if (thisForm.sel_FY.value != "")
	        window.open("Popups/pSchemeWO.asp?DevID=" + DevID + "&DevName=" + DevName + "&FY=" + thisForm.sel_FY.value, "", "width=635,height=390, left=100,top=100,scrollbars=yes")
	    else
	        window.open("Popups/pSchemeWO.asp?DevID=" + DevID + "&DevName=" + DevName, "", "width=635,height=390, left=100,top=100,scrollbars=yes")
		    //window.showModelessDialog("Popups/pSchemeWO.asp?DevID=" + DevID + "&DevName=" + DevName + "&Random=" + new Date(), "_blank", "dialogHeight: 390; dialogWidth: 630; status: No; resizable: No; scrolling:yes")
	}

	function quick_find() {
	    var querystr
	    querystr=""
	    if (thisForm.sel_FY.value != "") querystr = "&FY=" + thisForm.sel_FY.value

        querystr=
        location.href = "SchemeReportMain.asp?name=" + thisForm.findemp.value + querystr;
	}

</script>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <table border=0 width=750 cellpadding=2 cellspacing=2 height=40PX style='BORDER:1PX SOLID #133E71' bgcolor=beige >
    <tr> 
      <td bgcolor="#FAFAED">
            <p>This scheme report shows how much has been spent against , Estate Maintenance, Estate Management, Tenancy Furnishings and Services, associated with a scheme. The figures are a combination of repairs and non-repairs. If you click a row you can view a more indepth breakdown of the totals.</p>
      </td>
    </tr>
    <tr>
      <td  align="left" valign="top">
        Use the filters below to search<br>
        <br>
           Fiscal Year
           &nbsp;
           <%=lstFY%>
           &nbsp;
           Scheme 
           &nbsp;
        <input type=text  size=18 name='findemp' class='textbox200' value="<%=searchName%>">
        &nbsp;
        <input type='BUTTON' name='btn_FIND' title='Search for matches using scheme Name.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'>
      </td>
    </tr>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=3 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="../Portfolio/images/TABSTEMP/tab_schemereport.gif" WIDTH=120 HEIGHT=20 BORDER=0></TD>
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="93%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=630 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=250><strong>Scheme</strong></TD>
      <TD CLASS='NO-BORDER' WIDTH=110>&nbsp;<B>Estate Maintenance </B></TD>
      <TD CLASS='NO-BORDER' WIDTH=111>&nbsp;<strong>Estate Management</strong>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=97><strong>Tenancy Furnishings </strong>&nbsp;</TD>
      <TD WIDTH=70 align="center" CLASS='NO-BORDER'><strong>Services</strong></TD>
      <TD CLASS='NO-BORDER' WIDTH=72>&nbsp;<b>Total</b>&nbsp;</TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=8 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>