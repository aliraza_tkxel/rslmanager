<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim l_address1

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then PropertyID = -1 End If

	Call OpenDB()

	'-- Retrieve First Line of Address
		SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
		Call OpenRs(rsAddress, SQL)
		If (NOT rsAddress.EOF) Then
			l_address1 = rsAddress("ADDRESS1")
		Else
			l_address1 = "[None Given]"
		End If
		l_housenumber = rsAddress("HOUSENUMBER")
		l_propertyid = UCASE(rsAddress("PROPERTYID"))
		l_propertystatus = l_propertyid
		Call CloseRs(rsAddress)
	'-- Retrieve First Line of Address

	ACTION = "NEW"
	SQL = "SELECT isnull(COUNCILTAXPAYABLE,0) As F_COUNCILTAXPAYABLE, * FROM P_UTILITIES WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		l_water = rsLoader("WATERAUTHORITY")
		l_elec = rsLoader("ELECTRICITYSUPPLIER")
		l_taxband = rsLoader("COUNCILTAXBAND")
		l_taxpay = FormatNumber(rsLoader("F_COUNCILTAXPAYABLE"),2,-1,0,0)
		l_gas = rsLoader("GASSUPPLIER")
		l_watermeter = rsLoader("WATERMETERNUMBER")
		ACTION = "AMEND"
	End If
	Call CloseRs(rsLoader)

	Call BuildSelect(lst_waterauthority, "sel_WATERAUTHORITY", "S_ORGANISATION WHERE ORGACTIVE = 1 AND (TRADE = 8) ", "ORGID, NAME", "NAME", "Please Select", l_water, NULL, "textbox200", NULL)
	Call BuildSelect(lst_counciltaxband, "sel_COUNCILTAXBAND", "P_COUNCILTAXBAND", "CID, DESCRIPTION", "DESCRIPTION", "Please Select", l_taxband, NULL, "textbox200", NULL)
	Call BuildSelect(lst_electricalsupplier, "sel_ELECTRICITYSUPPLIER", "S_ORGANISATION WHERE ORGACTIVE = 1 AND (TRADE = 12 OR TRADE = 14) ", "ORGID, NAME", "NAME", "Please Select", l_elec, NULL, "textbox200", NULL)
	Call BuildSelect(lst_gassupplier, "sel_GASSUPPLIER", "S_ORGANISATION WHERE ORGACTIVE = 1 AND (TRADE = 13 OR TRADE = 14) ", "ORGID, NAME", "NAME", "Please Select", l_gas, NULL, "textbox200", NULL)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Business --> Team Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();

        FormFields[0] = "txt_COUNCILTAXPAYABLE|Council Tax Payable|CURRENCY|Y"
        FormFields[1] = "txt_WATERMETERNUMBER|Water Meter Number|TEXT|Y"
        FormFields[2] = "sel_WATERAUTHORITY|Water Authority|SELECT|Y"
        FormFields[3] = "sel_ELECTRICITYSUPPLIER|Electrical Supplier|SELECT|Y"
        FormFields[4] = "sel_COUNCILTAXBAND|Council Tax Band|SELECT|Y"
        FormFields[5] = "sel_GASSUPPLIER|Gas Supplier|SELECT|Y"

        function SaveForm() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/Utilities_svr.asp"
            document.RSLFORM.submit()
        }
    </script>
</head>
<body onload="initSwipeMenu(4);" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_main_details" src="images/tab_main_details.gif" width="97" height="20"
                        border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_warranties" src="images/tab_warranties.gif" width="89" height="20"
                        border="0" alt="" /></a></td>
            <td rowspan="2">
                <img name="tab_utilities" src="images/tab_utilities-over.gif" width="68" height="20"
                    border="0" alt="" /></td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_marketing" src="images/tab_marketing.gif" width="76" height="20"
                        border="0" alt="" / /></a></td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_hands" src="images/tab_healthsafety_1.gif" width="110" height="20"
                        border="0" alt="" /></a></td>
            <td>
                <img src="/myImages/spacer.gif" width="151" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="images/spacer.gif" width="151" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Water Authority:
            </td>
            <td>
                <%=lst_waterauthority%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WATERAUTHORITY" id="img_WATERAUTHORITY" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Electrical Supplier:
            </td>
            <td>
                <%=lst_electricalsupplier%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ELECTRICITYSUPPLIER" id="img_ELECTRICITYSUPPLIER" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Council Tax Band:
            </td>
            <td>
                <%=lst_counciltaxband%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNCILTAXBAND" id="img_COUNCILTAXBAND" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Gas Supplier:
            </td>
            <td>
                <%=lst_gassupplier%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_GASSUPPLIER" id="img_GASSUPPLIER" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Council Tax Payable:
            </td>
            <td>
                <input type="text" name="txt_COUNCILTAXPAYABLE" id="txt_COUNCILTAXPAYABLE" class="textbox200" value="<%=l_taxpay%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNCILTAXPAYABLE" id="img_COUNCILTAXPAYABLE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Water Meter No:
            </td>
            <td>
                <input type="text" name="txt_WATERMETERNUMBER" id="txt_WATERMETERNUMBER" class="textbox200" maxlength="20"
                    value="<%=l_watermeter%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WATERMETERNUMBER" id="img_WATERMETERNUMBER" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES2" id="img_KEYCOMPETENCIES2" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td colspan="2" align="right">
                <input type="hidden" name="hid_ACTION" id="hid_ACTION" value="<%=ACTION%>" />
                <input type="hidden" name="hid_PROPERTYID" id="hid_PROPERTYID" value="<%=PropertyID%>" />
                <input type="button" name="button" id="button" value=" SAVE " title=" SAVE " class='RSLBUTTON' onclick="SaveForm()" style="cursor:pointer" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
                <br/>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
