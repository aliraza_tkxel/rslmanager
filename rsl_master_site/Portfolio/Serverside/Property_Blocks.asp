<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstBlocks

	Dim developmentid
		developmentid = Request.Form("sel_DEVELOPMENTID")

	If (developmentid = "") Then
		developmentid = -1
	End If

	SQL = "SELECT * FROM P_DEVELOPMENT D WHERE D.DEVELOPMENTID = " & developmentid
	Call OpenDB()
	Call OpenRs(rsBlockData, SQL)
	If (NOT rsBlockData.EOF) Then
		schemename = rsBlockData("SCHEMENAME")
	End If
	Call CloseRs(rsBlockData)

	Call BuildSelect(lstBlocks, "sel_BLOCKID", "P_BLOCK WHERE DEVELOPMENTID = " & developmentid, "BLOCKID, BLOCKNAME ", "BLOCKNAME", "Please Select", NULL, NULL, "textbox200", " onchange=""GetAddressData()""")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Propert Search > Property Blocks</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("txt_SCHEMENAME").value = "<%=schemename%>";
            parent.document.getElementById("dvBLOCKID").innerHTML = document.getElementById("dvBLOCKID").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvBLOCKID">
        <%=lstBlocks%></div>
</body>
</html>
