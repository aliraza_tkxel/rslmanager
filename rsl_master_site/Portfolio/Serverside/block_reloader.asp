<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size

	getData()
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	// retrives team details from database and builds 'paged' report
	Function getData()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT BLOCKID, BLOCKNAME, DEVELOPMENTNAME, ADDRESS1, ADDRESS2, POSTCODE, COMPLETIONDATE " &_
					"			FROM P_BLOCK B " &_
					"				LEFT JOIN P_DEVELOPMENT D ON D.DEVELOPMENTID = B.DEVELOPMENTID " &_
					"			ORDER BY BLOCKNAME"
							
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
				
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		//response.write "<BR>Number of pages : " & intPageCount	& "<BR>" & "Number of records : " & intRecordCount &_
		//				"<BR>Current page : " & intPage & "<br>Page size : " & rsSet.PageSize
		count = 0	
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
			For intRecord = 1 to rsSet.PageSize
				
				team_name = rsSet("DEVELOPMENTNAME")
				str_data = str_data & "<TR STYLE='CURSOR:HAND' ONCLICK=""load_team(" & rsSet("BLOCKID") & ")"">" &_
					"<TD>" & rsSet("BLOCKNAME") & "</TD>" &_
					"<TD>" & rsSet("DEVELOPMENTNAME") & "</TD>" &_
					"<TD>" & rsSet("ADDRESS1") & "</TD>" &_
					"<TD>" & rsSet("ADDRESS2") & "</TD>" &_
					"<TD>" & rsSet("POSTCODE") & "</TD>" &_
					"<TD>" & rsSet("COMPLETIONDATE") & "</TD>" &_										
					"<TD STYLE='BORDER-BOTTOM:1PX SOLID SILVER' CLASS='DEL' ONCLICK='del_team(" & rsSet("BLOCKID") & ")'>Del</TD></TR>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"

			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'BLOCK.asp?page=1'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'BLOCK.asp?page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='BLOCK.asp?page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='BLOCK.asp?page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"
			
		End If
		
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=7 ALIGN=CENTER><B>No BLOCKS exist within the system !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function
%>	
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript>
function ReturnData(){
	parent.table_div.innerHTML = table_div.innerHTML
	parent.swap_div(1)
	}	
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="ReturnData()">
<DIV ID=table_div>
	<TABLE WIDTH=750 CLASS="TAB_TABLE" CELLPADDING=1 CELLSPACING=2 STYLE="behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=1 hlcolor=STEELBLUE>
		<THEAD><TR STYLE="HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71"><TD COLSPAN=6 CLASS='TABLE_HEAD'></TD></TR>
		<TR> 
		  <TD WIDTH=150PX CLASS='TABLE_HEAD'> <B>Block Name</B> </TD>
 		  <TD WIDTH=200PX CLASS='TABLE_HEAD'> <B>Development Name</B> </TD>
		  <TD WIDTH=150PX CLASS='TABLE_HEAD'> <B>Address 1 </B> </TD>
		  <TD WIDTH=150PX CLASS='TABLE_HEAD'> <B>Address 2</B> </TD>
		  <TD WIDTH=70PX CLASS='TABLE_HEAD'> <B>Postcode</B> </TD>
		  <TD  CLASS='TABLE_HEAD' nowrap> <B>Completion Date</B> </TD>		  		  
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=7 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<%=str_data%>
	</TABLE>
</DIV>
</body>
</html>
