<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim blockid
		blockid = Request.Form("sel_BLOCKID")

	If (blockid = "") Then
		blockid = -1
	End If

	SQL = "SELECT * FROM P_BLOCK B WHERE B.BLOCKID = " & blockid
	Call OpenDB()
	Call OpenRs(rsBlockData, SQL)
	If (NOT rsBlockData.EOF) Then
		address1 = rsBlockData("ADDRESS1")
		address2 = rsBlockData("ADDRESS2")
		towncity = rsBlockData("TOWNCITY")
		county = rsBlockData("COUNTY")
		postcode = rsBlockData("POSTCODE")
	End If
	Call CloseRs(rsBlockData)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Propert Search > Property Address</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("txt_ADDRESS1").value = "<%=address1%>";
            parent.document.getElementById("txt_ADDRESS2").value = "<%=address2%>";
            parent.document.getElementById("txt_TOWNCITY").value = "<%=towncity%>";
            parent.document.getElementById("txt_COUNTY").value = "<%=county%>";
            parent.document.getElementById("txt_POSTCODE").value = "<%=postcode%>";
        }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
