<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

block_id = Request("hid_BLOCKID")
if (block_id = "" OR block_id = -1) then block_id = -1

item_id = 1 ' TENANT ITEM
itemnature_id = Request.Form("sel_NATURE")
NatureTitle = Request.Form("hid_NATURETITLE")
Supplier = Request.Form("sel_SUPPLIER")
if (Supplier = "") then
	SupplierID = "NULL"
else
	SupplierID = Supplier
end if

//THIS VARIABLE STORES THE EXPECTED DELIVEY SECONDS THAT WILL BE ADDED TO THE PURCHASE ORDER DATE
MaxSeconds = 0

//THIS IS A FORMATED CURRENT DATE FOR INSERTION INTO THE DATABASE
CurrentDateStamp = Now
CurrentFullDate = FormatDateTime(CurrentDateStamp,1) & " " & FormatDateTime(CurrentDateStamp,3)

//this part will create the purchase order and set it inactive, it will be enabled at a later stage.
SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE, POTYPE, POSTATUS, BLOCKID) VALUES " &_
		"('" & UCASE(NatureTitle) & " WORK ORDER', '" & CurrentFullDate & "', 'This purchase order was created automatically from the Repairs process.', " & Session("USERID") & ", " & SupplierID & ", " &_
		" 1, 2, 3, " & block_id & ");SELECT SCOPE_IDENTITY() AS ORDERID;SET NOCOUNT OFF"
Call OpenRs(rsPO, SQL)
ORDERID = rsPO("ORDERID")
Call CloseRs(rsPO)

//CREATE THE ACTUAL WORK ORDER HERE
SQL = "SET NOCOUNT ON;INSERT INTO P_WORKORDER (ORDERID, TITLE, BLOCKID, WOSTATUS, BIRTH_NATURE, BIRTH_ENTITY, BIRTH_MODULE) VALUES " &_
		"(" & ORDERID & ", '" & NatureTitle & "', " & block_id & ", 2, " & itemnature_id & ", 2, 2); SELECT SCOPE_IDENTITY() AS WOID"

Call OpenRs(rsWO, SQL)
WOID = rsWO("WOID")
Call CloseRs(rsWO)

AnyQueued = false
For each i in Request.Form("ID_ROW")

	iData = Request.Form("iData" & i)
	
	iDataArray = Split(iData, "||<>||")
	RepairID = iDataArray(0)
	RepairNotes = Replace(iDataArray(1), "'", "''")
	VatType = iDataArray(2)
	NetCost = iDataArray(3)
	VAT = iDataArray(4)
	GrossCost = iDataArray(5)
	isQueued = CInt(iDataArray(6))
	isRechargeable = iDataArray(7)
	tempNature = iDataArray(9)

	if (Cint(tempNature) = 22) then
		//THE NEXT PART WILL BUILD A COMMA DELIMETED LIST OF PROPERTY IDS WHICH SHOULD BE EXCLUDED FROM THE REPAIR...
		Excludes = iDataArray(8)
		ExcludesSQL = ""
		if (Excludes <> "") then
			ExcludesArray = Split(Excludes, "||<**>||")
			Redim FinalExcludes (Ubound(ExcludesArray))
			for k = 0 to Ubound(ExcludesArray)
				//this will hold the Repairid | PropertyID | Cost
				MiniArray = Split(ExcludesArray(k), "|")
				FinalExcludes(k) = MiniArray(1)
			next
			//NEXT REJOIN THE ITEMS AS A FOLLOWS 
			ExcludesSQL = " AND P.PROPERTYID IN ('" & JOIN(FinalExcludes, "','") & "') "
		end if
	end if

	if (Supplier = "") then 'if no supplier selected....
		iAction = 1 'logged
		iStatus = 1 'pending
		PurchaseStatus = 3 'Work Ordered
	elseif (isQueued = 1) then 'is any item is queued
		AnyQueued = true
		iAction = 12 ' queued
		iStatus = 12 ' queued
		PurchaseStatus = 0 'Queued
	else  'SET THE ACTION TO ASSIGN TO CONTRACTOR 
		iAction = 2 ' assign to contractor
		iStatus = 2 ' assigned
		PurchaseStatus = 3 'Work Ordered
	end if

	//THIS PART GETS SOME DEFAULT INFORMATION REGARDING THE REPAIR			
	SQL = "SELECT ID.DESCRIPTION, R_EXP_ID, ESTTIME_SEC FROM R_ITEMDETAIL ID " &_
			"LEFT JOIN R_PRIORITY PR ON ID.PRIORITY = PR.PRIORITYID " &_
			"WHERE ITEMDETAILID = " & RepairID
	Call OpenRs(rsDefault, SQL)
	RepairName = Replace(rsDefault("DESCRIPTION"), "'", "''")
	ExpenditureID = rsDefault("R_EXP_ID")
	IF (ExpenditureID = "" OR isNULL(ExpenditureID)) THEN
		SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BACKUPREPAIREXPENDITUREID'"
		Call OpenRs(rsEXP, SQL)
		if NOT rsEXP.EOF then ExpenditureID = rsEXP("DEFAULTVALUE")
		CloseRs(rsEXP)
	END IF
	EstimatedSeconds = rsDefault("ESTTIME_SEC")		
	Call CloseRs(rsDefault)

	'GET THE ESTIMATED WORK COMPLETION DATE FOR EACH ITEM
	if (EstimatedSeconds = "" OR isNull(EstimatedSeconds)) then
		DELDATE = "NULL"
	else
		//THIS PART GETS THE MAXIMUM TIME FROM THE LIST OF ITEMS SO IT CAN BE ADDED TO THE PURCHASE ORDER EXPECTED DELIVERY TIME.
		if (CLng(MaxSeconds) < CLng(EstimatedSeconds)) then
			MaxSeconds = EstimatedSeconds
		end if
		DELDATE = DateAdd("s", EstimatedSeconds, CurrentDateStamp)
		DELDATE = "'" & FormatDateTime(DelDate,1) & " " & FormatDateTime(DelDate, 3) & "'"
	end if

	if (Cint(tempNature) = 22) then
		SQL = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT P.PROPERTYID, LTRIM(ISNULL(P.HOUSENUMBER,'') + ' ' + P.ADDRESS1 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE) AS ADDRESS  FROM P__PROPERTY P WHERE P.BLOCKID = " & block_id & " " & ExcludesSQL
		Call OpenRs(rsPROP, SQL)
		while NOT rsPROP.EOF
			//INSERT A LINE INTO THE JOURNAL TABLE	
			SQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES ('" & rsPROP("PROPERTYID") & "',  " & item_id & ", " & itemnature_id & ", " & iStatus & ", '" & RepairName & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
			Call OpenRs(rsSet, SQL)
			journal_id = rsSet.fields("JOURNALID").value
			Call CloseRs(rsSet)
			
			//NEXT INSERT A LINE INTO THE ACTUAL REPAIR TABLE
			SQL = 	"INSERT INTO C_REPAIR " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
					"VALUES (" & journal_id & ", " & iStatus & " , " & iAction & ", " & Session("USERID") & ", " & RepairID &_
					 ", " & SupplierID & ", '" & RepairName & "', '" & RepairNotes & "')"
			Conn.Execute(SQL)	
		
			//THIS PART INSERTS THE INDIVIDUAL PURCHASE ITEMS
			SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS) VALUES " &_
					"(" & ORDERID & ", " & ExpenditureID & ", '" & RepairName & "', 'FOR " & rsPROP("ADDRESS") & ". " & RepairNotes & "', '" & CurrentFullDate & "', " & DELDATE & ", " &_
					" " & NetCost & ", " & VatType & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 2, " & PurchaseStatus & ");SELECT SCOPE_IDENTITY() AS ORDERITEMID"
			Call OpenRs(rsITEM, SQL)
			ORDERITEMID = rsITEM("ORDERITEMID")
			Call CloseRs(rsITEM)
		
			//FINALLY JOIN ALL THE ITEMS TOGETHER SO THAT THEY ARE LINKED
			SQL = "INSERT INTO P_WOTOREPAIR (WOID, JOURNALID, ORDERITEMID) VALUES (" & WOID & ", " & journal_id & ", " & ORDERITEMID & ")"
			Conn.Execute(SQL)	
			rsPROP.movenext
		wend
	else
		//INSERT A LINE INTO THE JOURNAL TABLE	
		SQL = 	"SET NOCOUNT ON;" &_	
				"INSERT INTO C_JOURNAL (PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
				"VALUES (NULL,  " & item_id & ", " & itemnature_id & ", " & iStatus & ", '" & RepairName & "');" &_
				"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		Call OpenRs(rsSet, SQL)
		journal_id = rsSet.fields("JOURNALID").value
		Call CloseRs(rsSet)
		
		//NEXT INSERT A LINE INTO THE ACTUAL REPAIR TABLE
		SQL = 	"INSERT INTO C_REPAIR " &_
				"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
				"VALUES (" & journal_id & ", " & iStatus & " , " & iAction & ", " & Session("USERID") & ", " & RepairID &_
				 ", " & SupplierID & ", '" & RepairName & "', '" & RepairNotes & "')"
		Conn.Execute(SQL)	
	
		//THIS PART INSERTS THE INDIVIDUAL PURCHASE ITEMS
		SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS) VALUES " &_
				"(" & ORDERID & ", " & ExpenditureID & ", '" & RepairName & "', '" & RepairNotes & "', '" & CurrentFullDate & "', " & DELDATE & ", " &_
				" " & NetCost & ", " & VatType & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 2,  " & PurchaseStatus & ");SELECT SCOPE_IDENTITY() AS ORDERITEMID"
		Call OpenRs(rsITEM, SQL)
		ORDERITEMID = rsITEM("ORDERITEMID")
		Call CloseRs(rsITEM)
	
		//FINALLY JOIN ALL THE ITEMS TOGETHER SO THAT THEY ARE LINKED
		SQL = "INSERT INTO P_WOTOREPAIR (WOID, JOURNALID, ORDERITEMID) VALUES (" & WOID & ", " & journal_id & ", " & ORDERITEMID & ")"
		Conn.Execute(SQL)	
	end if	
next

//FINALLY SET THE PROPER STATUSES FOR THE WORK ORDER AND PURCHASE ORDER TAKING INTO ACCOUNT ANY ITEMS
//THAT MAY HAVE BEEN QUEUED IN THE WORK ORDER
if (AnyQueued = true) then
	SQL = "UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = " & WOID
	Conn.Execute (SQL)
	SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 0 WHERE ORDERID = " & ORDERID
	Conn.Execute (SQL)
elseif 	(Supplier = "") then 'if no supplier selected....
	SQL = "UPDATE P_WORKORDER SET WOSTATUS = 1 WHERE WOID = " & WOID
	Conn.Execute (SQL)
    else
            ' - - -  - - - - - - -  -
            '' AUTO ACCEPT REPAIRS
            ' - ---- - - - -  - - - -
            ' we check to see if the org has an auto accept functionality =  1
            ' if so then we auto accept the repair
            
	        SQL = "EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = " & ORDERID & ", @SUPPLIERID = " & Supplier
		    Conn.Execute (SQL)	
end if

CloseDB()

RESPONSE.REDIRECT "../iFrames/iRepairJournalBlo.asp?BLOCKID=" & block_id & "&WOID=" & WOID & "&SyncTabs=1"
%>