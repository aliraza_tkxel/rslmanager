<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID, LoaderString, NoDelete,check_count
Dim DataFields   (7)
Dim DataTypes    (7)
Dim ElementTypes (7)
Dim FormValues   (7)
Dim FormFields   (7)
UpdateID	  = "hid_BLOCKID"
FormFields(0) = "txt_BLOCKNAME|TEXT"
FormFields(1) = "txt_ADDRESS1|TEXT"
FormFields(2) = "txt_ADDRESS2|TEXT"
FormFields(3) = "txt_TOWNCITY|TEXT"
FormFields(4) = "txt_COUNTY|TEXT"
FormFields(5) = "txt_POSTCODE|TEXT"
FormFields(6) = "sel_DEVELOPMENTID|NUMBER"
FormFields(7) = "txt_COMPLETIONDATE|DATE"



Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO P_BLOCK " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO()		
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE P_BLOCK " & strSQL & " WHERE BLOCKID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO()
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	' First Check that there is no properties associated witht the block, if so dont delete
	SQLcheck = 	"select (count(p.propertyid)+ count(wo.woid) + count(po.orderid)) as check_count "&_ 
				"from p_block b  "&_ 
				"	left join p__property p on p.developmentid = b.developmentid "&_ 
				"	left join p_workorder wo on wo.blockid = b.blockid "&_ 
				"	left join f_purchaseorder po on po.blockid = b.blockid  "&_ 
				"where b.blockid = " & ID
				rw SQLcheck
	Call OpenRs(rsCheckProperties, SQLcheck)
	check_count = rsCheckProperties("check_count")
	if check_count > 0 then
		NoDelete = "true"
	else
		NoDelete = "false"
		SQL = "DELETE FROM P_BLOCK WHERE BLOCKID = " & ID
		Conn.Execute SQL, recaffected
	end if
	
	GO()	
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)
	LoaderString = ""
	Call OpenRs(rsLoader, "SELECT * FROM P_BLOCK WHERE BLOCKID = " & ID)
	if not (rsLoader.EOF) then
		for i=0 to Ubound(FormFields)
			FormArray = Split(FormFields(i), "|")
			TargetItem = FormArray(0)
			LoaderString = LoaderString & "parent.RSLFORM." & TargetItem & ".value = """ & rsLoader(RIGHT(TargetItem, Len(TargetItem)-4)) & """" & VbCrLf
		next
	end if
	CloseRs(rsLoader)
End Function

Function GO()
	CloseDB()
	if NoDelete = "true" then
	rw "stay"	
	else
	rw "go"
	Response.Redirect "block_reloader.asp"
	end if
End Function
TheAction=""
TheAction = Request("hid_Action")
'Response.Write(TheAction) 
OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select

%>
<script language=javascript>

//<% if NoDelete = "false" then %>

//<% else %>
function Return()
{
        <%if TheAction="LOAD" then%>
            <%=LoaderString%>
	        parent.RSLFORM.hid_ACTION.value = "AMEND"
	        parent.checkForm()
	        parent.swap_div(3);
	        parent.checkForm();
	        
        <%else %>
	       <%if TheAction ="DELETE" then%>
	         parent.alert("This block can not be deleted as there are <%=check_count%> properties / purchase orders associated with it.");
	       <%end if %>
	    <%end if%>  
	    return;
}	    	        	
	//
//<% end if %>

</script>
<html>
<body onload="Return();">
</body>
</html>