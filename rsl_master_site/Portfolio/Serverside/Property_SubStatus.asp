<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim STATUSID
		STATUSID = Request.Form("sel_STATUS")

	If (STATUSID = "") Then
		STATUSID = -1
	End If

	SQL = "SELECT * FROM P_SUBSTATUS S WHERE S.STATUSID = " & STATUSID & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsSubStatus, SQL)
		count = 0
		optionstr = ""
		while (NOT rsSubStatus.EOF)
			optionstr = optionstr & "<option value=""" & rsSubStatus("SUBSTATUSID") & """>" & rsSubStatus("DESCRIPTION") & "</option>"
			rsSubStatus.moveNext
			count = 1
		wend
	Call CloseRs(rsSubStatus)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Propert Search > Property Status</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData(){
        <% if count = 0 then %>
	        parent.document.getElementById("dvSUBSTATUS").innerHTML = "<select name='sel_SUBSTATUS' id='sel_SUBSTATUS' class='textbox200'><option value=''>Not Applicable</option></select>";
        <%	if (Request("DONTDOIT") <> 1) then %>
	        parent.FormFields[18] = "sel_SUBSTATUS|SUBSTATUS|SELECT|N"
        <% end if %>
	        parent.document.getElementById("sel_SUBSTATUS").disabled = true
        <% else %>
	        parent.document.getElementById("dvSUBSTATUS").innerHTML = document.getElementById("dvSUBSTATUS").innerHTML;
        <%	if (Request("DONTDOIT") <> 1) then %>
	        parent.FormFields[18] = "sel_SUBSTATUS|SUBSTATUS|SELECT|Y"
        <% end if %>
	        parent.document.getElementById("sel_SUBSTATUS").disabled = false
        <% end if %>
	        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvSUBSTATUS">
        <select name="sel_SUBSTATUS" id="sel_SUBSTATUS" class="textbox200">
            <option value="">Please Select</option>
            <%=optionstr%>
        </select>
    </div>
</body>
</html>
