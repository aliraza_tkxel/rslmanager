<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim dbrs,dbcmd,orgid,patchid,developmentid,TreeViewData,counter,intRecord,filter,checkflag,checkmessage

	orgid=Request.Form("sel_contractor_warranty")
	patchid = Request.Form("sel_PATCH")
	developmentid=Request.Form("sel_SCHEME")
	createworkorder="disable"
	counter = 0
	
	if (patchid <> "") then
	  filter=",@patchid=" & patchid 
	end if
	
	if(developmentid<>"") then
	    filter=filter & ",@developmentid=" & developmentid	
	end if
	
	set dbrs=server.createobject("ADODB.Recordset")
   	set dbcmd= server.createobject("ADODB.Command")
             
	dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
	sql="GS_CONTRACT_DETAIL @ORGID=" & orgid & filter
	
	dbrs.Source = sql
	dbrs.CursorType = 3
	dbrs.CursorLocation = 3
	dbrs.Open()
    
    NoofRecords=dbrs.RecordCount
    
   
    if(not dbrs.eof) then
        TreeViewData="<table style='border-collapse:collapse;width:1000;'>"
        TreeViewData= TreeViewData & "<tr >"   
        TreeViewData= TreeViewData & "<td >&nbsp;</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"">&nbsp;</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style=' text-align:center;width:70px;' bgcolor='beige'>Properties</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;'>Fire</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;' bgcolor='beige'>Water Heater/<br/>Multipoint </td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;'>Cooker <br/> Hob</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;' bgcolor='beige'>Traditional <br/> Boiler</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;'>Combi <br/> Boiler</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;' bgcolor='beige'>Oil <br/> Boiler</td>"
        TreeViewData= TreeViewData & "<td class=""thinline""style='text-align:center;'>Wall <br/> Heater</td>"
        TreeViewData= TreeViewData & "<td class=""thinline"" style='text-align:center;'  bgcolor='beige'>Contract<br/> Value</td>"
        TreeViewData= TreeViewData & "</tr>"
        
        For intRecord = 1 to dbrs.RecordCount
	
        ' IF WE HAVE CHANGED PATCHES WE NEED TO ADD IN THE PATCH SUMMARY LINE
          RecordCount = dbrs.RecordCount

           Scheme_PatchID  		= dbrs("PATCHID")
           Scheme_Location		= dbrs("LOCATION")
           Scheme_DevelopmentName	= dbrs("DEVELOPMENTNAME")
           Scheme_PropertyCount 	= dbrs("PROPERTIES")
           Scheme_TraditionalBoilerCount 	= dbrs("NOOFTRADITIONALBOLIER")
           Scheme_TraditionalBoilerCost = dbrs("COSTPERTRADITIONALBOILER")
           Scheme_FireCount	= dbrs("NOOFFIRE")
           Scheme_FireCost 	= dbrs("COSTPERFIRE") 
           Scheme_WaterHeaterMultipointCount 	= dbrs("NOOFWATERHEATER_MULTIPOINT")
           Scheme_WaterHeaterMultipointCost 	= dbrs("COSTPERWATERHEATER_MULTIPOINT")    
           Scheme_CookHobCount 		= dbrs("NOOFCOOKER_HOB")
           Scheme_CookHobCost 		= dbrs("COSTPERCOOKER_HOB")
           Scheme_CombiBoilerCount 	= dbrs("NOOFCOMBIBOILER")
           Scheme_CombiBoilerCost 	= dbrs("COSTPERCOMBIBOILER")
           Scheme_OilBoilerCount 	= dbrs("NOOFOILBOILERS")
           Scheme_OilBoilerCost 	= dbrs("COSTPEROILBOILERS")
           Scheme_WallHeaterCount 	= dbrs("NOOFWALLHEATER") 
           Scheme_WallHeaterCost = dbrs("COSTPERWALLHEATER") 
           Scheme_ContractCost 	= dbrs("CONTRACTVALUE")

           TreeViewDataDev= TreeViewDataDev & "<tr  style='display:none;border-collapse:collapse;' id=""ChildLine"& Scheme_PatchID   & counter & """>" 
           TreeViewDataDev= TreeViewDataDev & "<td><img src=""images/join.gif"" /></td>"
           TreeViewDataDev= TreeViewDataDev & "<td style=""padding-left:10px;width:160px;"">" &  Scheme_DevelopmentName & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  bgcolor='beige'  style='text-align:center;'>" & Scheme_PropertyCount & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  style='text-align:center;'>" &  Scheme_FireCount & " (" & formatcurrency(Scheme_FireCost) & ")" & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  bgcolor='beige'  style='text-align:center;'>" &  Scheme_WaterHeaterMultipointCount & " (" & formatcurrency(Scheme_WaterHeaterMultipointCost) & ")" & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  style='text-align:center;'>" &  Scheme_CookHobCount & " (" & formatcurrency(Scheme_CookHobCost) & ")" & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  bgcolor='beige'  style='text-align:center;'>" &  Scheme_TraditionalBoilerCount & " (" & formatcurrency(Scheme_TraditionalBoilerCost) &  ")" & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  style='text-align:center;'>" &  Scheme_CombiBoilerCount & " (" & formatcurrency(Scheme_CombiBoilerCost) & ")"  & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  bgcolor='beige'  style='text-align:center;'>" &  Scheme_OilBoilerCount & " (" & formatcurrency(Scheme_OilBoilerCost) & ")" & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  style='text-align:center;'>" & Scheme_WallHeaterCount & " (" & formatCurrency(Scheme_WallHeaterCost) & ")"  & "</td>"
           TreeViewDataDev= TreeViewDataDev & "<td  bgcolor='beige'  style='text-align:right;padding-right:5px;'>" &  formatcurrency(Scheme_ContractCost) & "</td>"
           TreeViewDataDev= TreeViewDataDev & "</tr>"

        	if ((cInt(Scheme_PatchID) <> cInt(dbrs("PATCHID")) and Scheme_PatchID <> "") or (intRecord   + 1 > dbrs.RecordCount)) then		
  
                TreeViewData= TreeViewData & "<tr style='cursor:hand' onClick=""LoadDetail('" & Cint(Scheme_PatchID)  & "')"">" 
                TreeViewData= TreeViewData & "<td ><img src=""images/plus.gif""  id=""Parent"& Scheme_PatchID    &""" / ></td>"
                TreeViewData= TreeViewData & "<td style=""width:170px;"">" &  Scheme_Location  & "</td>"
                TreeViewData= TreeViewData & "<td bgcolor='beige' style='text-align:center;'>" &  Patch_PropertyCount + Cint(Scheme_PropertyCount) & "</td>"
                TreeViewData= TreeViewData & "<td style='text-align:center;'>" &  Patch_FireCount + Scheme_FireCount & " (" & formatcurrency(Patch_FireCost +	Scheme_FireCost) & ")" & "</td>"
                TreeViewData= TreeViewData & "<td bgcolor='beige' style='text-align:center;'>" &  Patch_WaterHeaterMultipointCount  + Scheme_WaterHeaterMultipointCount & " (" & formatcurrency(Patch_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost ) & ")"  & "</td>"
                TreeViewData= TreeViewData & "<td style='text-align:center;'>" &  Patch_CookHobCount + Scheme_CookHobCount & " (" & formatcurrency(Patch_CookHobCost + Scheme_CookHobCost) & ")" & "</td>"
                TreeViewData= TreeViewData & "<td bgcolor='beige' style='text-align:center;'>" &  Patch_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount & " (" & formatcurrency(Patch_TraditionalBoilerCost + Scheme_TraditionalBoilerCost)  & ")" & "</td>"
                TreeViewData= TreeViewData & "<td style='text-align:center;'>" &  Patch_CombiBoilerCount  + Scheme_CombiBoilerCount & " (" & formatcurrency(Patch_CombiBoilerCost + Scheme_CombiBoilerCost) & ")" & "</td>"
                TreeViewData= TreeViewData & "<td bgcolor='beige' style='text-align:center;'>" &  Patch_OilBoilerCount + Scheme_OilBoilerCount & " (" & formatCurrency(Patch_OilBoilerCost + Scheme_OilBoilerCost) & ")" & "</td>"
                TreeViewData= TreeViewData & "<td style='text-align:center;'>" & Patch_WallHeaterCount  + Scheme_WallHeaterCount & " (" & formatCurrency(Patch_WallHeaterCost + Scheme_WallHeaterCost) &  ")" & "</td>"
                TreeViewData= TreeViewData & "<td bgcolor='beige' style='text-align:right;padding-right:5px;'>" &  formatcurrency(Patch_ContractCost + Scheme_ContractCost ) & "</td>"
                TreeViewData=   TreeViewData & "</tr>" 
                TreeViewData= TreeViewData & TreeViewDataDev

                ' now set the dev rows empty for next patch

                TreeViewDataDev = ""

                Patch_PropertyCount 	= 0
                Patch_TraditionalBoilerCount = 0
                Patch_TraditionalBoilerCost = 0
                Patch_FireCount 		= 0
                Patch_FireCost 		= 0
                Patch_WaterHeaterMultipointCount = 0
                Patch_WaterHeaterMultipointCost = 0
                Patch_CookHobCount		= 0
                Patch_CookHobCost =0
                Patch_CombiBoilerCount  	= 0
                Patch_CombiBoilerCost = 0
                Patch_OilBoilerCount  	= 0
                Patch_OilBoilerCost =0	
                Patch_WallHeaterCount = 0
                Patch_WallHeaterCost =0
                Patch_ContractCost 	= 0

            end if

            Patch_PropertyCount 	= cInt(Patch_PropertyCount) + Cint(Scheme_PropertyCount)
            Patch_TraditionalBoilerCount  = Patch_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount 
            Patch_TraditionalBoilerCost = Patch_TraditionalBoilerCost + Scheme_TraditionalBoilerCost
            Patch_FireCount	= Patch_FireCount  + Scheme_FireCount
            Patch_FireCost = Patch_FireCost +	Scheme_FireCost
            Patch_WaterHeaterMultipointCount 		= Patch_WaterHeaterMultipointCount + Scheme_WaterHeaterMultipointCount
            Patch_WaterHeaterMultipointCost = Patch_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost 
            Patch_CookHobCount 	= Patch_CookHobCount + Scheme_CookHobCount
            Patch_CookHobCost = Patch_CookHobCost + Scheme_CookHobCost
            Patch_CombiBoilerCount	= Patch_CombiBoilerCount +  Scheme_CombiBoilerCount
            Patch_CombiBoilerCost = Patch_CombiBoilerCost + Scheme_CombiBoilerCost
            Patch_OilBoilerCount = Patch_OilBoilerCount + Scheme_OilBoilerCount
            Patch_OilBoilerCost = Patch_OilBoilerCost + Scheme_OilBoilerCost 			
            Patch_WallHeaterCount	= Patch_WallHeaterCount + Scheme_WallHeaterCount
            Patch_WallHeaterCost = Patch_WallHeaterCost + Scheme_WallHeaterCost
            Patch_ContractCost 	= Patch_ContractCost + Scheme_ContractCost 

            counter=counter+1  
       
            dbrs.movenext()
            If dbrs.EOF Then Exit for
    	Next
	    TreeViewData=TreeViewData & "</table>"
	    createworkorder="enable" 
	    'OpenDB()      
	    '    CheckWorkorder()
	    ' CloseDB()   
	else
	     TreeViewData="No data exist for this contractor"  
    end if
    
   dbrs.close()
   Set dbrs = Nothing
   
 ' Function CheckWorkorder()
  '  if(developmentid="" and patchid <> "") then
  '      strSQL=" SELECT COUNT(DEVELOPMENTID) AS DEVELOPMENTIDCOUNT "&_
  '          " FROM P_WORKORDER PW "&_
'	        "   INNER JOIN P_WORKORDERTOPATCH PWP ON PWP.WOID=PW.WOID "&_
  '          " WHERE PW.GASSERVICINGYESNO=1 AND PWP.PATCHID=" & patchid
 '      Call OpenRs (rsChk, strSQL)   
   '    if(Not rsChk.EOF) then
    '        if(rsChk("DEVELOPMENTIDCOUNT")>0) then
     '           checkflag=1
      '          checkmessage=" Work Order already created against scheme of the selected patch ."
       '     else
                checkflag=0
        '        checkmessage=""
         '   end if    
             
      ' end if   
      ' CloseRs(rsChk)
     'end if   
   'End Function  -->  

   ' set dbrs=nothing
   ' set dbcmd= nothing    

%>
<html>
<head>
<title>RSLmanager > Protfolio Module > Gas Servicing > Serverside</title>
    <script language="javascript" type="text/javascript">
        function ReturnData() {
            if ("<%=checkflag%>" == "1") {
                parent.alert("<%=checkmessage%>");
            }
            else {
                parent.document.getElementById("content").innerHTML = document.getElementById("content").innerHTML;
                if ("<%=createworkorder%>" == "enable")
                    parent.RSLFORM.btn_createorder.disabled = false;
            }
            return;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="content">
        <%=TreeViewData%></div>
</body>
</html>
