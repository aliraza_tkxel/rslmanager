<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
PMID = Request("PMID")
'if (PMID = "") then
'	PMID = -1
'end if

Response.Write PMID

SQL = "SELECT RUNM.PMID, WO.WOID, WO.ORDERID, pi.NETCOST, EXECUTED FROM P_RUNNINGMAINTENANCE RUNM " &_
			" INNER JOIN P_WORKORDER WO ON WO.WOID = RUNM.WOID " &_
			" INNER JOIN F_PURCHASEORDER PO ON WO.ORDERID = PO.ORDERID " &_
			" INNER JOIN (SELECT SUM(NETCOST) AS NETCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_			
			" WHERE RUNM.PMID = " & PMID & " " &_
			" ORDER BY EXECUTED ASC "
TableString = ""
OpenDB()
Call OpenRs(rsPMID, SQL)
if (NOT rsPMID.EOF) then
	COUNT = 1
	while NOT rsPMID.EOF
		TableString = TableString & "<TR><TD>" & "<b>CYC</b> " & characterPad(rsPMID("PMID"),"0", "l", 6) & "</td><TD>" & COUNT & "</td><TD>" & FormatDateTime(rsPMID("EXECUTED"),2) & "</td><TD>" & WorkNumber(rsPMID("WOID")) & "</TD><TD style='cursor:hand' onclick=""PO(" & rsPMID("ORDERID") & ")""><font color=blue>" & PurchaseNumber(rsPMID("ORDERID")) & "</font></td><td align=right>" & FormatCurrency(rsPMID("NETCOST")) & "</TD></TR>"
		COUNT = COUNT + 1
		rsPMID.moveNext
	wend

else
	TableString = "<TR><TD colspan=6 align=center> No details found for this cyclical repair</TD></TR>"
end if
Call CloseRs(rsPMID)
CloseDB()
%>
<html>
<body>
<div id="TableData">
  <TABLE CELLSPACING=0 CELLPADDING=3 border=1 style='border-collapse:collpase'>
    <TR style='color:white;background-color:#133e71'> 
      <TD width=100><b>Cyclical ID</b></TD>
      <TD width=100><b>Nunber</b></TD>
      <TD width=100><b>Created</b></TD>	  
      <TD width=100><b>Work Order</b></TD>
      <TD width=100><b>Purchase Order</b></TD>
      <TD width=100><b>Net Value</b></TD>
    </TR>
    <%=TableString%> 
  </TABLE>
</div>
<script language=javascript>
parent.TableData.innerHTML = TableData.innerHTML
</script>
</body>
</html>