<%@ language="vbscript" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim data(3)
	Dim labels(3)
	Dim strName
	
	data(0) = Request("qtr1")
	data(1) = Request("qtr2")
	data(2) = Request("qtr3")
	data(3) = Request("qtr4")
	strName = Request("strname")
	
	OpenDB()
	SQL = "SELECT KPINAME FROM P_KPI WHERE KPIID = " & cint(strName)
	Call OpenRs(rsSet, SQL)
	if not rsSet.Eof Then
		strName = rsSet(0)
	End If
	CloseRs(rsSet)
	
	Set cd = CreateObject("ChartDirector.API")
	
	'The data for the pie chart
	'data = Array(25, 18)
	
	'The labels for the pie chart
	For i=1 To 4
		labels(i-1) = "QTR " & i	
	Next 
	'labels = Array("qtr 1", "qtr 2", "qtr 3", "qtr 4")
	
	'Create a PieChart object of size 450 x 240 pixels
	Set c = cd.PieChart(550, 240)
	
	'Set the center of the pie at (150, 100) and the radius to 80 pixels
	Call c.setPieSize(250, 100, 80)
	
	'Add a title at the bottom of the chart using Arial Bold Italic font
	Call c.addTitle2(cd.Top, strName, "arialbi.ttf")
	
	'Draw the pie in 3D
	Call c.set3D()
	Call c.setLabelLayout(cd.SideLayout)
	Call c.setLabelStyle().setBackground(cd.SameAsMainColor, cd.Transparent, 1)
	Call c.setLineColor(cd.SameAsMainColor, &H0)
	Call c.setStartAngle(135)
	
	'Set the pie data and the pie labels
	Call c.setData(data, labels)
	
	'Explode the 1st sector (index = 0)
	Call c.setExplode(0)
	
	'output the chart
	Response.ContentType = "image/png"
	Response.BinaryWrite c.makeChart2(cd.GIF)
	Response.End

%>
