<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
    Server.ScriptTimeout=200
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,fuelid,uprn
	Dim monitoringid,mon_status,MonitoringDetailData

	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function

	Function GetPostedData()
		'This function will get all the posted data
		orgid=nulltest(Request.Item("sel_ORGANISATION"))
		patchid=nulltest(Request.Form.Item("sel_PATCH"))
		developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
        fuelid=nulltest(Request.Form.Item("sel_FUEL"))
        uprn=nulltest(Request.Form.Item("txt_UPRN"))
		monitoringid=Request.Item("hid_mon_menu") 
		mon_status=Request.Form("mon_status")
	End Function


	Function DebugRquests()
		If Request.Form <> "" Then
			For Each Item In Request.Form
				rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
			Next
		 End If	
		 If Request.QueryString <> "" Then
			For Each Item In Request.QueryString
				rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
			Next
		 End If  
	End Function

	'Call DebugRquests()

	Function GetPropertyCP12StatusDetail()

	Dim CP12DetailData   

	Select case mon_status
	
		case "0"
			Heading =  "Expired "  
			filterid = 0   
		case "1"			        
				Heading="Due in 1 week " 
				filterid = 1
		case "2"    
				Heading=  "Due in 1 - 4 weeks " 
				filterid = 2        
		case "3"
				Heading=  "Due in 4 - 8 weeks " 
				filterid = 3
		case "4"
				Heading=  "Due in 8 - 12 weeks " 
				filterid = 4        
		case "5"		        
				Heading =  "Due 12 - 16 weeks "
				filterid = 5  	
        case "6"		        
				Heading =  "Due 16 - 52 weeks "
				filterid = 6  	
		case "7"	
				Heading =  "Due in > 52 weeks "
				filterid = 7
		case "8"			        
				Heading="No Cert. Data " 
				filterid = 8  
				  
	End Select	 

	 set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "P_LGSR_RECORDSET"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
		spUser.Parameters.Append spUser.CreateParameter("@PERIODFILTER", 3, 1,4,filterid)
        spUser.Parameters.Append spUser.CreateParameter("@FUELID", 3, 1,4,fuelid)
        spUser.Parameters.Append spUser.CreateParameter("@UPRN", 200, 1,10,uprn)
	
	set rsSet = spUser.Execute
	
			CP12DetailData="<div style=""height:300px;overflow:scroll;width:100%;""><table style='border-collapse:collapse;'  cellspacing=4 cellpadding=2>"
			CP12DetailData=CP12DetailData & "<tr class=""heading2"">"
			CP12DetailData=CP12DetailData & "<td></td>"
			CP12DetailData=CP12DetailData & "<td>Address</td>"
			CP12DetailData=CP12DetailData & "<td>Expiry Date</td>"
            CP12DetailData=CP12DetailData & "<td>Property Type</td>"
            CP12DetailData=CP12DetailData & "<td>Asset Type</td>"
			CP12DetailData=CP12DetailData & "</tr>" 
			CP12DetailData=CP12DetailData & "<tr><td class=""dottedline"" colspan=5></td></tr>"
			
			If(NOT rsSet.eof) Then
				i = 1
				While(not rsSet.eof) 
					CP12DetailData=CP12DetailData & "<tr style='color:#133e71;'>"
					CP12DetailData=CP12DetailData & "<td>" & i & "</td>"
					CP12DetailData=CP12DetailData & "<td class='linkstylesmall' onclick=""PropertyUpdate('" & rsSet("PROPERTYID") & "');"">" & rsSet("PROPERTYADDRESS") &"</td>"
					CP12DetailData=CP12DetailData & "<td>" & rsSet("CURR_CERT_DATE") &"</td>"
                    CP12DetailData=CP12DetailData & "<td>" & rsSet("PTYPE") &"</td>"
                    CP12DetailData=CP12DetailData & "<td>" & rsSet("ATYPE") &"</td>"
					CP12DetailData=CP12DetailData & "</tr>"
				rsSet.movenext()
				i = i + 1
				wend     
			Else
				CP12DetailData=CP12DetailData & "<tr>"
				CP12DetailData=CP12DetailData & "<td colspan=6>No data exit for selected LGSR status</td>"
				CP12DetailData=CP12DetailData & "</tr>"
			End If
			rsSet.Close
			CP12DetailData=CP12DetailData & "<tr><td class=""dottedline"" colspan=5></td></tr>"   
			CP12DetailData= CP12DetailData & "</table></div>"
			
	set sprocDisplayIagManagerUsers = nothing

 
	GetPropertyCP12StatusDetail = CP12DetailData

	End Function

   
	Call OpenDB()
	Call GetPostedData()
    MonitoringDetailData = GetPropertyCP12StatusDetail()
	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Manage Appointment</title></head>
<script language="javascript" type="text/javascript">
function ReturnData()
{
    parent.document.getElementById("MonitoringSubHead").innerHTML = Heading.innerHTML;
    parent.document.getElementById("subdatabody").innerHTML= MonitoringDetail.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<form name = "thisForm" method="post" action="">
<div id="Heading"><%=Heading%></div>
<div style="text-align:center;" id="MonitoringDetail"><%=MonitoringDetailData%></div>
</form>
</body>
</html>