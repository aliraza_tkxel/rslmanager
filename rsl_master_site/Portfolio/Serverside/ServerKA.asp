<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (24)
Dim DataTypes    (24)
Dim ElementTypes (24)
Dim FormValues   (24)
ReDim FormFields (24)
UpdateID	  = "hid_PropertyID"
Dim TableString
Dim THESELECT
  
Function NewRecord ()
	ID = Request.Form(UpdateID)	
	SQL = "INSERT INTO P_ATTTOKITCHEN (PROPERTYID, KID) VALUES ('" & ID & "', " & Request.Form("sel_KITCHEN") & ")"
	Conn.Execute SQL
	Rebuild()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	
	Redim Preserve FormFields (23)
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE P_ATTRIBUTES " & strSQL & " WHERE PROPERTYID = '" & ID & "'" 
	
	Conn.Execute SQL, recaffected
	GO()
End Function

Function Rebuild()
	SQL = "SELECT KA.DESCRIPTION, KA.KID FROM P_ATTTOKITCHEN AK INNER JOIN P_KITCHENAPPLIANCES KA ON AK.KID = KA.KID WHERE PROPERTYID = '" & ID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString = "<TR>"		
		while NOT rsLoader.EOF 
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString = TableString & "</TR><TR>"				
			TableString = TableString & "<TD width=216>" & rsLoader("DESCRIPTION") & "</TD><TD style='cursor:hand' width=30 onclick=""DeleteKA(" & rsLoader("KID") & ")""><font color=red>DEL</font></TD>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString = TableString & "<TD colspan=4></TD>"
		if (Remainder = 2) then TableString = TableString & "<TD colspan=2></TD>"		
		TableString = TableString & "</TR>"
	else
		TableString = "<TR><TD colspan=6 align=center>No items found</td></tr>"
	end if
	CloseRs(rsLoader)

	Call BuildSelect(THESELECT, "sel_KITCHEN", "P_KITCHENAPPLIANCES KA WHERE NOT EXISTS (SELECT * FROM P_ATTTOKITCHEN AK WHERE AK.KID = KA.KID AND PROPERTYID = '" & ID & "')", "KID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)	
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM P_ATTTOKITCHEN WHERE PROPERTYID = '" & ID & "' AND KID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute (SQL)
	Rebuild()
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head></head>
<script language=javascript>
function sendData(){
	parent.KA.innerHTML = Reloader.innerHTML
	parent.THISFORM.sel_KITCHEN.outerHTML = SelectReloader.innerHTML	
}
</script>
<body onload="sendData()">
<div id="SelectReloader">
<%=THESELECT%>
</div>
<div id="Reloader">
<TABLE CELLSPACING=2 CELLPADDING=1 STYLE='BORDER-COLLAPSE:COLLAPSE' BORDER=1 WIDTH=100% HEIGHT=100%>
<%=TableString%>
<TR><TD HEIGHT=100% COLSPAN=6></TD></TR>
</TABLE>
</div>
</body>
</html>