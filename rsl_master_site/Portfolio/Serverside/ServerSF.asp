<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (24)
Dim DataTypes    (24)
Dim ElementTypes (24)
Dim FormValues   (24)
ReDim FormFields (24)
UpdateID	  = "hid_PropertyID"
Dim TableString
Dim THESELECT
  
Function NewRecord ()
	ID = Request.Form(UpdateID)	
	SQL = "INSERT INTO P_ATTTOSCHEME (PROPERTYID, SID) VALUES ('" & ID & "', " & Request.Form("sel_SCHEME") & ")"
	Conn.Execute SQL
	Rebuild()
End Function

Function Rebuild()
	SQL = "SELECT SF.DESCRIPTION, SF.FID FROM P_ATTTOSCHEME ASS INNER JOIN P_SCHEMEFACILITIES SF ON ASS.SID = SF.FID WHERE PROPERTYID = '" & ID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString = "<TR>"		
		while NOT rsLoader.EOF 
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString = TableString & "</TR><TR>"				
			TableString = TableString & "<TD width=216>" & rsLoader("DESCRIPTION") & "</TD><TD style='cursor:hand' width=30 onclick=""DeleteSF(" & rsLoader("FID") & ")""><font color=red>DEL</font></TD>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString = TableString & "<TD colspan=4></TD>"
		if (Remainder = 2) then TableString = TableString & "<TD colspan=2></TD>"		
		TableString = TableString & "</TR>"
	else
		TableString = "<TR><TD colspan=6 align=center>No items found</td></tr>"
	end if
	CloseRs(rsLoader)

	Call BuildSelect(THESELECT, "sel_SCHEME", "P_SCHEMEFACILITIES SF WHERE NOT EXISTS (SELECT * FROM P_ATTTOSCHEME ASS WHERE ASS.SID = SF.FID AND PROPERTYID = '" & ID & "')", "FID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL,NULL, "textbox200", NULL)
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM P_ATTTOSCHEME WHERE PROPERTYID = '" & ID & "' AND SID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute (SQL)
	Rebuild()
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head></head>
<script language=javascript>
function sendData(){
	parent.SF.innerHTML = Reloader.innerHTML
	parent.THISFORM.sel_SCHEME.outerHTML = SelectReloader.innerHTML	
}
</script>
<body onload="sendData()">
<div id="SelectReloader">
<%=THESELECT%>
</div>
<div id="Reloader">
<TABLE CELLSPACING=2 CELLPADDING=1 STYLE='BORDER-COLLAPSE:COLLAPSE' BORDER=1 WIDTH=100% HEIGHT=100%>
<%=TableString%>
<TR><TD HEIGHT=100% COLSPAN=6></TD></TR>
</TABLE>
</div>
</body>
</html>