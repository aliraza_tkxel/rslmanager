<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	propertyref = Request.Form("txt_PROPERTYREF")
	If (propertyref = "") Then 
		propysql = ""
	Else
		propysql = " AND P.PROPERTYID LIKE '%" & propertyref & "%' "
	End If

	developmentid = Request.Form("sel_DEVELOPMENTID")
	If (developmentid = "") Then 
		devsql = ""
	Else
		devsql = " AND P.DEVELOPMENTID = " & developmentid & " "
	End If

	patchid = Request.Form("sel_PATCH")
	If (patchid = "") Then 
		patsql = ""
	Else
		patsql = " AND D.PATCHID = " & patchid & " "
	End If

	address1 = Replace(Request.Form("txt_ADDRESS1"), "'", "''")
	If (address1 = "") Then 
		addsql = ""
	Else
		addsql = " AND (P.ADDRESS1 LIKE '%" & address1 & "%' OR P.ADDRESS2 LIKE '%" & address1 & "%' OR P.ADDRESS3 LIKE '%" & address1 & "%' OR P.HOUSENUMBER + ' ' + P.ADDRESS1 LIKE '%" + address1 + "%') "
	End If

	istatus = Request.Form("sel_STATUS")
	If (istatus = "") Then 
		stasql = ""
	Else
		stasql = " AND P.STATUS = " & istatus & " "
	End If

	postcode = Replace(Request.Form("txt_POSTCODE"), "'", "''")
	If (postcode = "") Then
		possql = ""
	Else
		possql = " AND P.POSTCODE LIKE '%" & postcode & "%' "
	End If

	propertytpe = Request.Form("sel_PROPERTYTYPE")
	If (propertytpe = "") Then
		prosql = ""
	Else
		prosql = " AND P.PROPERTYTYPE = " & propertytpe & " "
	End If

	substatus = Request.Form("sel_SUBSTATUS")
	If (substatus = "") Then 
		subsql = ""
	Else
		subsql = " AND P.SUBSTATUS = " & substatus & " "
	End If

	If (Request("ADVANCED") = "1") Then

		blockid = Request.Form("sel_BLOCKID")
		If (blockid = "") Then 
			blosql = ""
		Else
			blosql = " AND P.BLOCKID = " & blockid & " "
		End If

		assettype = Request.Form("sel_ASSETTYPE")
		If (assettype = "") Then
			asssql = ""
		Else
			asssql = " AND P.ASSETTYPE = " & assettype & " "
		End If

		adaptationcat = Request.Form("sel_ADAPTATIONCAT")
		If (adaptationcat = "") Then
			adasql = ""
		Else
			adasql = " AND AD.ADAPTATIONCAT = " & adaptationcat & " "
		End If

		adaptation = Request.Form("sel_ADAPTATIONS")
		If (adaptation = "") Then
			adaptsql = ""
		Else
			adaptsql = " AND AD.ADAPTATIONID = " & adaptation & " "
		End If

		unadaptationcat = Request.Form("sel_UNADAPTATIONCAT")
		If (unadaptationcat = "") Then
			unasql = ""
		Else
			unasql = " AND (AD.ADAPTATIONCAT IS NULL OR AD.ADAPTATIONCAT <> (" & unadaptationcat & ")) "
		End If

	End If

	MaxRecords = 200
	SQL = "SELECT DISTINCT TOP " & MaxRecords & " P.PROPERTYID, ISNULL(HOUSENUMBER,FLATNUMBER) AS HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE FROM P__PROPERTY P " &_
			"LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"LEFT JOIN P_PORTFOLIOADAPTATIONS AP ON P.PROPERTYID = AP.PROPERTYID " &_
			"LEFT JOIN P_ADAPTATIONS AD ON AP.ADAPTATIONID = AD.ADAPTATIONID " &_
			"WHERE 1 = 1 " & propysql & patsql & devsql & possql & subsql & stasql & addsql & blosql & prosql & asssql & adasql & adaptsql & unasql & " ORDER BY HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE"
	Call OpenDB()
	Call OpenRs(rsSearch, SQL)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Propert Search > Property Results</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <script type="text/javascript" language="JavaScript">

        function DisplayProperty(PropID) {
            event.cancelBubble = true
            window.open("/Portfolio/PopUps/Property_Details.asp?PropertyID=" + PropID, "PropertyDetails", "width=400,height=300,left=200,top=200")
        }

        function LoadProperty(PropID) {
            parent.location.href = "../PRM.asp?PropertyID=" + PropID
        }

    </script>
</head>
<body>
    <table cellspacing="0" cellpadding="0" height="100%">
        <tr>
            <td valign="top" height="20">
                <table>
                    <tr>
                        <td width="90px" nowrap="nowrap" style="color: #133E71">
                            <b>Property Ref&nbsp;</b>
                        </td>
                        <td width="270px" nowrap="nowrap" style="color: #133E71">
                            <b>Address</b>
                        </td>
                        <td width="20px" nowrap="nowrap" style="color: #133E71">
                            <b>&nbsp;</b>
                        </td>
                    </tr>
                    <tr style="line-height: 2px">
                        <td colspan="3" height="2" style="line-height: 2px">
                            <hr style="height:2px; border: 1px dotted #133E71" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="100%" valign="top">
                <div style="height: 100%; overflow: auto" class="TA">
                    <table style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#133e71" width="393">
                        <%
	                    COUNT = 0
	                    If (not rsSearch.EOF) Then
		                    While NOT rsSearch.EOF
			                    HOUSENUMBER = rsSearch("HOUSENUMBER")
			                    ADDRESS1 = rsSearch("ADDRESS1")
			                    ADDRESS2 = rsSearch("ADDRESS2")
			                    TOWNCITY = rsSearch("TOWNCITY")
			                    POSTCODE = rsSearch("POSTCODE")

			                    If (HOUSENUMBER <> "" AND NOT ISNULL(HOUSENUMBER)) Then
				                    propaddress = HOUSENUMBER & " " & ADDRESS1 & ", " &  ADDRESS2 &  ", " &  TOWNCITY & ", " &  POSTCODE
			                    Else
				                    propaddress = ADDRESS1 & ", " &  ADDRESS2 &  ", " &  TOWNCITY & ", " &  POSTCODE
			                    End If

			                    propaddress = Replace(propaddress, ", , ", ", ")
			                    propid = rsSearch("PROPERTYID")

			                    If (LEN(propaddress) > 40) Then
				                    Response.Write "<tr style=""cursor:pointer"" onclick=""LoadProperty('" & propid & "')"">"
                                        Response.Write "<td width=""90px"">" & rsSearch("PROPERTYID") & "</td>"
                                        Response.Write "<td width=""270px"" title=""" & Server.HTMLEncode(propaddress) & """>" & Left(propaddress, 40) & "..." & "</td>"
                                        Response.Write "<td width=""20"" style=""background-color:#ffffff""><a onclick=""DisplayProperty('" & propid & "')"" title=""info""><img src=""/myImages/info.gif"" border=""0"" alt=""info"" /></a></td>"
                                    Response.Write "</tr>"
			                    Else
				                    Response.Write "<tr style=""cursor:pointer"" onclick=""LoadProperty('" & propid & "')"">"
                                        Response.Write "<td width=""90px"">" & rsSearch("PROPERTYID") & "</td>"
                                        Response.Write "<td width=""270px"">" & propaddress & "</td>"
                                        Response.Write "<td width=""20"" style=""background-color:#ffffff""><a onclick=""DisplayProperty('" & propid & "')"" title=""info""><img src=""/myImages/info.gif"" border=""0"" alt=""info"" /></a></td>"
                                    Response.Write "</tr>"
			                    End If
			                    COUNT = COUNT + 1
			                    rsSearch.moveNext
		                    Wend
	                    Else
		                    Response.Write "<tr>"
                                Response.Write "<td colspan=""3"" align=""center"">No Records Found</td>"
                            Response.Write "</tr>"
	                    End If
	                    If (COUNT = MaxRecords) Then
		                    Response.Write "<tr>"
                                Response.Write "<td colspan=""3"" align=""center"">Maximum number of records returned.<br>To Filter the list apply more filters.</td>"
                            Response.Write "</tr>"
	                    End If
	                    Call CloseRs(rsSearch)
	                    Call CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="393">
                    <tfoot>
                        <tr style='line-height: 2'>
                            <td colspan="3" height="2" style="line-height: 2px">
                                <hr style="height:2px; border: 1px dotted #133E71" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
