<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID
	FirstArraySize = 14
	SecondtArraySize = 28
	ReDim DataFields   (FirstArraySize)
	ReDim DataTypes    (FirstArraySize)
	ReDim ElementTypes (FirstArraySize)
	ReDim FormValues   (FirstArraySize)
	ReDim FormFields   (FirstArraySize)

	UpdateID	  = "hid_PropertyID"

	FormFields(0) = "txt_DATERENTSET|DATE"
	FormFields(1) = "txt_RENTEFFECTIVE|DATE"
	FormFields(2) = "txt_TOTALRENT|MONEY"
	FormFields(3) = "txt_RENT|Rent|MONEY"
	FormFields(4) = "txt_SERVICES|MONEY"
	FormFields(5) = "txt_COUNCILTAX|MONEY"
	FormFields(6) = "txt_WATERRATES|MONEY"
	FormFields(7) = "txt_INELIGSERV|MONEY"
	FormFields(8) = "txt_SUPPORTEDSERVICES|MONEY"
	FormFields(9) = "sel_RENTTYPE|NUMBER"
	FormFields(10) = "hid_PROPERTYID|TEXT"
	FormFields(11) = "txt_TARGETRENT|MONEY"
	FormFields(12) = "txt_GARAGE|MONEY"
	FormFields(13) = "chk_TARGETRENTSET|NUMBER"
	FormFields(14) = "hid_PFUSERID|NUMBER"

	Call OpenDB()
	Call AmendRecord()
	Call CloseDB()

	Sub AmendRecord()

		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)

		' Check the property exists in p_financial/ if not ass new record
		CheckExists = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & ID  & "'"
		Call OpenRs(rsCheck, CheckExists)
		If rsCheck.eof Then
			Call NewRecord()
			Exit sub
		End If
		Call CloseRs(rsCheck)

		HISTORYSQL = "INSERT INTO P_FINANCIAL_HISTORY (PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue) " &_
			  		 " SELECT PROPERTYID,NOMINATINGBODY,FUNDINGAUTHORITY,RENT,SERVICES,COUNCILTAX,WATERRATES,INELIGSERV,SUPPORTEDSERVICES,GARAGE,TOTALRENT,RENTTYPE,OLDTOTAL,RENTTYPEOLD,DATERENTSET,RENTEFFECTIVE,TARGETRENT,YIELD,CAPITALVALUE,INSURANCEVALUE,NORENTCHANGE,TARGETRENTSET,PFUSERID,PFTIMESTAMP,FRApplicationDate,FRRegistrationDate,FRStartDate,FREffectDate,FRRegNum,FRRegNextDue FROM P_FINANCIAL WHERE PROPERTYID = '" & ID & "'"
		Conn.Execute HISTORYSQL, recaffected
		SQL = "UPDATE P_FINANCIAL " & strSQL & " WHERE PROPERTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected

	End Sub

	Sub NewRecord()
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)
		SQL = "INSERT INTO P_FINANCIAL " & strSQL & ""
		Conn.Execute SQL, recaffected
		SQL = "INSERT INTO P_FINANCIAL_HISTORY " & strSQL & ""
		Conn.Execute SQL, recaffected
		return 0
	End Sub
%>
<html>
<head>
    <title>RSLmanager > Portfolio > Fair Rent Report > Rent Update</title>
    <script type="text/javascript" language="javascript">
        alert("Record has been updated successfully!")
        if (window.showModelessDialog) {
            window.parent.location.reload();
        }
        else {
            parent.window.opener.location.reload(true);
        }
        parent.window.close()
    </script>
</head>
<body>
</body>
</html>
