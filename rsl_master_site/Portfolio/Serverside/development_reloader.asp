<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size

	Call getTeams()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	' retrives team details from database and builds 'paged' report
	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT  DEVELOPMENTID, DEVELOPMENTNAME, STARTDATE = ISNULL(CONVERT(VARCHAR, STARTDATE, 103), 'N/A'), " &_
					"		 TARGETDATE = ISNULL(CONVERT(VARCHAR, TARGETDATE, 103),'N/A'), " &_  
					"		 ISNULL(SCHEMENAME, 'N/A') AS SCHEMENAME, ISNULL(D.PROJECTMANAGER,'N/A') AS PROJECTMANAGER, " &_
					"			T.DESCRIPTION AS TYPE " &_
					"			FROM P_DEVELOPMENT D " &_
					"				LEFT JOIN P_DEVELOPMENTTYPE T ON T.DEVELOPMENTTYPEID = D.DEVELOPMENTTYPE " &_
					"			ORDER BY DEVELOPMENTNAME"

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<tbody class=""CAPS"">"
			For intRecord = 1 to rsSet.PageSize

				team_name = rsSet("DEVELOPMENTNAME")
				str_data = str_data & "<tr style=""cursor:pointer"" onclick=""load_team(" & rsSet("DEVELOPMENTID") & ")"">" &_
					"<td>" & rsSet("DEVELOPMENTNAME") & "</td>" &_
					"<td>" & rsSet("SCHEMENAME") & "</td>" &_
					"<td>" & rsSet("TYPE") & "</td>" &_
					"<td>" & rsSet("STARTDATE") & "</td>" &_
					"<td>" & rsSet("TARGETDATE") & "</td>" &_
					"<td style=""border-bottom:1px solid silver"" class=""DEL"" onclick=""del_team(" & rsSet("DEVELOPMENTID") & ")"">DEL</td></tr>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""6"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">" &_			
			"<a href=""DEVELOPMENT.asp?page=1""><b><font color=""blue"">First</font></b></a> " &_
			"<a href=""DEVELOPMENT.asp?page=" & prevpage & """><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href=""DEVELOPMENT.asp?page=" & nextpage & """><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href=""DEVELOPMENT.asp?page=" & intPageCount & """><b><font color=""blue"">Last</font></b></a>" &_
			"</td><td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71; font-size:11px"">&nbsp;"  &_
			"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" title=""GO"" onclick=""JumpPage()"" style=""font-size:10px"">"  &_
			"</td></tr></thead></table></td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""7"" align=""center""><b>No DEVELOPMENTS exist within the system !!</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		While (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""7"" align=""center"" style=""background-color:white"">&nbsp;</td></tr>"
			cnt = cnt + 1
		Wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Developemnt > New Development</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("table_div").innerHTML = document.getElementById("table_div").innerHTML
            parent.swap_div(1)
        }
</script>
</head>
<body onload="ReturnData()">
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="behavior: url(/Includes/Tables/tablehl.htc);
            border-collapse: collapse" slcolor='' border="1" hlcolor="STEELBLUE">
            <thead>
                <tr style="height: 3px; border-bottom: 1px solid #133e71">
                    <td colspan="5" class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td width="200px" class='TABLE_HEAD'>
                        <b>Development Name</b>
                    </td>
                    <td width="290px" class='TABLE_HEAD'>
                        <b>Scheme</b>
                    </td>
                    <td width="110px" class='TABLE_HEAD'>
                        <b>Type</b>
                    </td>
                    <td width="90px" class='TABLE_HEAD'>
                        <b>Start Date</b>
                    </td>
                    <td width="90px" class='TABLE_HEAD'>
                        <b>Target Date1</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD' colspan="7" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
    </div>
</body>
</html>
