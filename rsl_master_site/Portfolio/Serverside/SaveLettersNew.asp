<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim orgid,LetterContent,Journalid,LetterActionId,GasHistoryid,Notes,dbcmd,dbrs,Letterid,AppointmentDate,AppointmentTime,userid,alertmessage
    Dim issuedate,cp12number,cp12issuedby,NewJournalId
	
	
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
    
    dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	
	Function GetPostedData()
	    //This function will get all the posted data
	    orgid=Request.Item("hid_ORGID")
	    GasHistoryid=Request.Item("hid_GASHISTORYID")
	    LetterContent=Request.Item("hid_LETTERTEXT")
	    LetterActionId=Request.Item("sel_ITEMACTIONID")
	    Letterid=Request.Item("sel_LETTER")
	    Notes=Request.Item("txt_NOTES")
	    AppointmentDate=Request.Item("txt_APPOINTMENTDATE")
	    AppointmentTime=Request.Item("hid_APPOINTMENTTIME")
	    userid=Session("USERID")
        cp12number=Request.Item("txt_CP12NUMBER") 
        issuedate=Request.Item("txt_ISSUEDATE") 
    End Function
	
	Function SaveAppointmentsLetter()
        SQLGASREPAIR =" SELECT STATUSID,JOURNALID "&_
                      " FROM P_GASJOURNALHISTORY "&_
                      " WHERE GASHISTORYID= "& GasHistoryid 

        Call OpenRs(rsSet, SQLGASREPAIR)        
        
                If(Not rsSet.eof) Then
                    If (LetterActionId=108 OR LetterActionId=109) Then
    
                    SQL=	" SET NOCOUNT ON; "&_
                            " INSERT INTO P_GASJOURNALHISTORY(JOURNALID,LASTACTIONUSER,LASTACTIONDATE,STATUSID,LETTERACTIONID,NOTES) "&_
		    	            " VALUES(" & rsSet("JOURNALID") & "," & userid & ",GETDATE()," & rsSet("STATUSID") & "," & LetterActionId & ",'" & Notes & "');SELECT @@IDENTITY AS GASHISTORYID; "    

                    ElseIf (LetterActionId=105 OR LetterActionId=106) Then

                    SQL=	" SET NOCOUNT ON; "&_
                            " DECLARE @STATUSID INT "&_
                            " SET @STATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='In Progress') "&_
                            " INSERT INTO P_GASJOURNALHISTORY(JOURNALID,LASTACTIONUSER,LASTACTIONDATE,STATUSID,LETTERID,LETTERACTIONID,LETTERCONTENT,NOTES)  "&_
                            " VALUES(" & rsSet("JOURNALID") & "," & userid & ",GETDATE(), @STATUSID," & Letterid & "," & LetterActionId & ",'" & Replace(LetterContent,"'", "''") & "','" & Notes & "'); "&_
                            " SELECT @@IDENTITY AS GASHISTORYID; "
                    Else
                    SQL=	" SET NOCOUNT ON; "&_
                            " DECLARE @STATUSID INT "&_
                            " SET @STATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='In Progress') "&_
                            " INSERT INTO P_GASJOURNALHISTORY(JOURNALID,LASTACTIONUSER,LASTACTIONDATE,STATUSID,LETTERID,LETTERACTIONID,LETTERCONTENT,APPOINTMENTDATE,NOTES)  "&_
                            " VALUES(" & rsSet("JOURNALID") & "," & userid & ",GETDATE(), @STATUSID," & Letterid & "," & LetterActionId & ",'" & Replace(LetterContent,"'", "''") & "','" & AppointmentDate & " " & AppointmentTime & "','" & Notes & "'); "&_
                            " SELECT @@IDENTITY AS GASHISTORYID; "     
                        

                    End if

        	        Journalid = rsSet("JOURNALID")
                    dbcmd.CommandText=SQL
                    Set dbrs=dbcmd.execute
        
                    If(not dbrs.eof) Then
                	    If (LetterActionId=108)  Then
                               alertmessage="The No Entry has been recorded"
                        ElseIf (LetterActionId=109)  Then

                               'UPDATE THE APPLIANCE RECORDS
                               SQLGS =" UPDATE  GS_PROPERTY_APPLIANCE "&_
                                      " SET ISSUEDATE= '" & issuedate & "',"&_
	                                  "     CP12NUMBER='" & cp12number & "'," &_
	                                  "     CP12ISSUEDBY= "& userid &" "&_
                                      "   WHERE PROPERTYID =(SELECT PROPERTYID FROM P_GASJOURNAL WHERE JOURNALID= " & Journalid & " )"

                               Conn.Execute(SQLGS)

                               'SET THE REPAIR ITEM TO COMPLETETD
                               SQLGS =" UPDATE P_GASJOURNALHISTORY "&_
                                      " SET STATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='Completed') "&_
                                      "  WHERE GASHISTORYID=" & dbrs("GASHISTORYID")

                               Conn.Execute(SQLGS)     

                               'SET JOURNAL ITEM TO COMPLETETED
                               SQLGS =" UPDATE P_GASJOURNAL "&_
                                      " SET CURRENTITEMSTATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='Completed') "&_
                                      " WHERE  JOURNALID= " & rsSet("JOURNALID")
                               Conn.Execute(SQLGS)     

                               'INSERT NEW RECORDS IN JOURNAL FOR NEXT GAS SERVICING
                               
                                SQLGS=  " SET NOCOUNT ON; "&_
                                        " DECLARE @STATUSID INT "&_
                                        " SET @STATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='Logged') "&_
                                        " INSERT INTO P_GASJOURNAL(PROPERTYID,CREATIONDATE,CURRENTITEMSTATUSID,ITEMNATUREID) "&_ 
                                        " SELECT PROPERTYID,GETDATE(),@STATUSID,ITEMNATUREID "&_
                                        " FROM   P_GASJOURNAL " &_
                                        " WHERE JOURNALID= " & Journalid & " "&_
                                        " SELECT @@IDENTITY AS NEWJOURNALID; "    

                                Call OpenRs(rsSetGS, SQLGS)        
        
                                If(Not rsSetGS.eof) Then
                                      NewJournalId=rsSetGS("NEWJOURNALID")
                                      
                                      SQLGH=" INSERT INTO P_GASJOURNALHISTORY(JOURNALID,LETTERID,LETTERACTIONID,LETTERCONTENT,APPOINTMENTID,APPOINTMENTDATE,NOTES,LASTACTIONDATE,STATUSID,LASTACTIONUSER) "&_
                                            " VALUES(" & NewJournalId & ",NULL,NULL,NULL,NULL, NULL,NULL,GETDATE(),1," & userid  & ")"   

                                      Conn.Execute(SQLGH)         
                                End If
                                     
                               alertmessage="LGSR has been Issued" 
                        Else
                            
                               'SET JOURNAL ITEM TO COMPLETETED
                               SQLGS =" UPDATE P_GASJOURNAL "&_
                                      " SET CURRENTITEMSTATUSID=(SELECT ITEMSTATUSID FROM  C_STATUS WHERE ITEMID=1 AND DESCRIPTION='In Progress') "&_
                                      " WHERE  JOURNALID= " & rsSet("JOURNALID")
                               Conn.Execute(SQLGS)       

                               If(LetterActionId=105) then
                                   alertmessage="Pre-Injunction has been recorded"  
                               ElseIf(LetterActionId=106) then
                                   alertmessage="Court Proceedings has been recorded" 
                               Else
                                   alertmessage="The Appointment has been confirmed"
                               End If

                        End If         
                    End If
                End If
        CloseRs(rsSet) 
  End Function
    
  OpenDB()
	 GetPostedData()
     SaveAppointmentsLetter()
  CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Save Appointment Letter</title></head>
<script language="javascript" type="text/javascript">
    function ReturnData() {
    if(<%=LetterActionId%>==109)
    {    
        parent.Add_CP12();
    }
    parent.alert("<%=alertmessage%>");
	//parent.opener.parent.open_me(47,<%=Journalid%>,"iServicingDetail.asp")
	return;
}
</script>
<body onload="ReturnData()">
<p>Save Appointment Letter</p>
</body>
</html>