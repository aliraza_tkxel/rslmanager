<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Const UNAVAILABLE = 4
	COnst UNDERCONSTRUCTION = 1

	Dim ID
	Dim DataFields   (28)
	Dim DataTypes    (28)
	Dim ElementTypes (28)
	Dim FormValues   (28)
	ReDim FormFields (28)
	UpdateID	  = "hid_PropertyID"
	FormFields(0) = "txt_PROPERTYID|TEXT"
	FormFields(1) = "txt_AVDATE|DATE"
	FormFields(2) = "txt_HOUSENUMBER|TEXT"
	FormFields(3) = "txt_ADDRESS1|TEXT"
	FormFields(4) = "txt_ADDRESS2|TEXT"
	FormFields(5) = "txt_DATEBUILT|DATE"
	'FormFields(6) = "txt_ADDRESS3|TEXT"
	FormFields(6) = "txt_VIEWINGDATE|DATE"
	FormFields(7) = "txt_TOWNCITY|TEXT"
	FormFields(8) = "txt_LETTINGDATE|DATE"
	FormFields(9) = "txt_UNITDESC|TEXT"
	FormFields(10) = "rdo_RIGHTTOBUY|NUMBER"
	FormFields(11) = "txt_COUNTY|TEXT"
	FormFields(12) = "txt_POSTCODE|TEXT"
	FormFields(13) = "sel_DEVELOPMENTID|NUMBER"
	FormFields(14) = "sel_BLOCKID|NUMBER"
	FormFields(15) = "sel_PROPERTYTYPE|NUMBER"
	FormFields(16) = "sel_PROPERTYLEVEL|NUMBER"
	'FormFields(17) = "sel_HOUSINGOFFICER|NUMBER"
	FormFields(17) = "txt_DEFECTSPERIODEND|DATE"
	FormFields(18) = "sel_STOCKTYPE|NUMBER"
	FormFields(19) = "sel_ASSETTYPE|NUMBER"
	FormFields(20) = "sel_OWNERSHIP|NUMBER"
	FormFields(21) = "txt_MINPURCHASE|NUMBER"
	FormFields(22) = "txt_PURCHASELEVEL|NUMBER"
	FormFields(23) = "sel_DWELLINGTYPE|NUMBER"
	FormFields(24) = "sel_NROSHASSETTYPEMAIN|NUMBER"
	FormFields(25) = "sel_NROSHASSETTYPESUB|NUMBER"
    FormFields(26) = "sel_FUELTYPE|NUMBER"
	FormFields(27) = "sel_STATUS|NUMBER"
	FormFields(28) = "sel_SUBSTATUS|NUMBER"
	
	
	  
	Function NewRecord ()
	
		'AUTO GENERATE A NEW PROPERTY REFERENCE NUMBER
		SQL = "SET NOCOUNT ON; " &_
				"UPDATE P_PROPERTY_IDENTITY SET P_IDENTITY = (SELECT P_IDENTITY + 1 FROM P_PROPERTY_IDENTITY); " &_
				"SELECT P_IDENTITY FROM P_PROPERTY_IDENTITY"
		Call OpenRs(rsIDENTITY, SQL)
		NewID = "BHA" & characterPad(rsIDENTITY("P_IDENTITY"),"0", "l", 7)
		CloseRs(rsIDENTITY)
	
		FormFields(0) = "cde_PROPERTYID|'" & NewID & "'"
		FormFields(27) = "cde_STATUS|" & UNAVAILABLE & " "
		FormFields(28) = "cde_SUBSTATUS|" & UNDERCONSTRUCTION & " "
		
		Call MakeInsert(strSQL)
		Response.Write strSQL	
	
		SQL = "SET NOCOUNT ON; INSERT INTO P__PROPERTY " & strSQL & ";"
		Response.Write SQL
		ID = NewID
		Conn.Execute(SQL)
		
		' COMMENTED OUT BY PAUL 12 JUL 2005 *** DO NOT REMOVE WITHOUT CONSULTING PAUL PATTERSON ***
		'SQL = " INSERT INTO KPI_STATUSTRACKER (PROPERTYID, ASSETTYPE, STATUS, SUBSTATUS, TSTARTDATE) " &_
		'		  " VALUES ('" & NewID & "', " & Request("sel_ASSETTYPE") & ", " & UNAVAILABLE & ", " & UNDERCONSTRUCTION & ", '" & FormatDateTime(date,2) & "') "
		'Conn.Execute (SQL)
		' *****************************************************************************************
		
		GO()
	End Function
	
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
	
		'this part checks for a current tenant in the property, in which case it disables the user from updating the status of the
		'selected property.
		SQL = "SELECT * FROM C_TENANCY T WHERE (T.ENDDATE IS NULL OR (T.ENDDATE IS NOT NULL AND T.ENDDATE > GETDATE())) AND T.PROPERTYID = '" & ID & "'"
		Response.Write SQL
		Call OpenRs(rsTenant, SQL)
		if (NOT rsTenant.EOF) then
			Redim Preserve FormFields(26)
		else
			SQL = "SELECT * FROM C_ADDITIONALASSET WHERE (ENDDATE IS NULL OR (ENDDATE IS NOT NULL AND ENDDATE > GETDATE())) AND PROPERTYID = '" & ID & "'"			
			Call OpenRs(rsAdTenant, SQL)
			if (NOT rsAdTenant.EOF) then
				Redim Preserve FormFields(26)
			END IF
			CloseRs(rsAdTenant)		
		end if
		CloseRs(rsTenant)
	
		ReDim Preserve FormFields (26)
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE P__PROPERTY " & strSQL & " WHERE PROPERTYID = '" & ID & "'" 
		Response.Write SQL
		Conn.Execute SQL, recaffected
		
		dim l_MANAGINGAGENT
		l_MANAGINGAGENT = Request("txt_MANAGINGAGENT")
		if (l_MANAGINGAGENT <> "") then
    	    SQL = "DELETE FROM P_MANAGINGAGENT WHERE PROPERTYID = '" & ID & "';" &_
		          "INSERT INTO P_MANAGINGAGENT (PROPERTYID, MANAGINGAGENT) VALUES (N'" & ID & "', N'" & l_MANAGINGAGENT & "');" 
		    Response.Write SQL
		    Conn.Execute SQL, recaffected
		end if
		
		GO()
	End Function
	
	Function DeleteRecord(Id, theID)
		ID = Request.Form(UpdateID)	
		
		SQL = "DELETE FROM P__PROPERTY WHERE PROPERTYID = '" & ID & "'" 
		Conn.Execute SQL, recaffected
		
		SQL = "DELETE FROM P_MANAGINGAGENT WHERE PROPERTYID = '" & ID & "'" 
		Conn.Execute SQL, recaffected
	End Function
	
	Function GO()
		' RUN SCRIPTS TO ENTER PART MONTH VOID ENTRIES
		Conn.Execute ("NL_LETTING_DATE_MONITORING")
		CloseDB()
		Response.Redirect "../Property.asp?PropertyID=" & ID
	End Function
	
	TheAction = Request("hid_Action")
	
	OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
		
	' RUN SROC TO CHECK
	CloseDB()
%>