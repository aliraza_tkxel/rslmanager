<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID, LoaderString, FundingListCols, FundingListValues,lstFunding
	Dim DataFields   (31)
	Dim DataTypes    (31)
	Dim ElementTypes (31)
	Dim FormValues   (31)
	Dim FormFields   (31)
	UpdateID	  = "hid_DEVELOPMENTID"
	FormFields(0) = "txt_DEVELOPMENTNAME|TEXT"
	FormFields(1) = "txt_SCHEMENAME|TEXT"
	FormFields(2) = "txt_SCHEMECODE|TEXT"
	FormFields(3) = "txt_STARTDATE|DATE"
	FormFields(4) = "txt_TARGETDATE|DATE"
	FormFields(5) = "txt_INSURANCEVALUE|NUMBER"
	FormFields(6) = "txt_ACTUALDATE|DATE"
	FormFields(7) = "txt_DEFECTSPERIOD|DATE"
	FormFields(8) = "txt_NUMBEROFUNITS|NUMBER"
	FormFields(9) = "sel_LEADDEVOFFICER|NUMBER"
	FormFields(10) = "sel_SUPPORTDEVOFFICER|NUMBER"
	FormFields(11) = "txt_PROJECTMANAGER|TEXT"
	FormFields(12) = "sel_CONTRACTOR|NUMBER"
	FormFields(13) = "sel_DEVELOPMENTTYPE|NUMBER"
	FormFields(14) = "sel_USETYPE|NUMBER"
	FormFields(15) = "sel_SUITABLEFOR|NUMBER"
	FormFields(16) = "sel_LOCALAUTHORITY|NUMBER"
	FormFields(17) = "txt_AQUISITIONCOST|CURRENCY"
	FormFields(18) = "txt_DEVELOPMENTADMIN|CURRENCY"
	FormFields(19) = "txt_CONTRACTORCOST|CURRENCY"
	FormFields(20) = "txt_INTERESTCOST|CURRENCY"
	FormFields(21) = "txt_OTHERCOST|CURRENCY"
	FormFields(22) = "txt_SCHEMECOST|CURRENCY"
	FormFields(23) = "txt_BALANCE|CURRENCY"
	FormFields(24) = "txt_HAG|CURRENCY"
	FormFields(25) = "txt_BORROWINGRATE|CURRENCY"
	FormFields(26) = "txt_MORTGAGE|CURRENCY"
	FormFields(27) = "txt_NETTCOST|CURRENCY"
	FormFields(28) = "sel_PATCHID|NUMBER"
	FormFields(29) = "sel_SUPPORTEDCLIENTS|NUMBER"
	FormFields(30) = "sel_SURVEYOR|NUMBER"
	FormFields(31) = "sel_COMPANYID|NUMBER"

	FundingListCols = Request.Form("txtList")
	Dim dev_has_properties, Purchase_Items_Found, Grants_Found, POS_Found
	Purchase_Items_Found = 0
	dev_has_properties = 0
	Grants_Found = 0
	POS_Found = 0
	
	Function NewRecord ()
		ID = Request.Form(UpdateID)
		
		'RETURN THE DEVELOPMENT ID SO THAT WE CAN CREATE A CORREPSONDING FINANCE DEVELOPMENT WITH IT.
		Call MakeInsert(strSQL)	
		SQL = "SET NOCOUNT ON;INSERT INTO P_DEVELOPMENT " & strSQL & ";SELECT SCOPE_IDENTITY() AS NEWID"
		Call OpenRs(rsINS, SQL)
		DEVID = rsINS("NEWID")
		Call CloseRs(rsINS)

		'NEXT INSERT A HEAD UNDER THE DEVELOPMENT COST CENTRE
		SQL = "SET NOCOUNT ON;INSERT INTO F_HEAD (DESCRIPTION, HEADDATE, USERID, LASTMODIFIED, COSTCENTREID) " &_
				"SELECT DEVELOPMENTNAME, GETDATE(), " & Session("USERID") & ", GETDATE(), (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID') FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & DEVID & ";" &_
				"SELECT SCOPE_IDENTITY() AS NEWID"
		Call OpenRs(rsHE, SQL)		
		HEADID = rsHE("NEWID")
		Call CloseRs(rsHE)
		
		'NEXT INSERT A DUMMY ROW WITH A VALUE OF 0 INTO F_HEAD_ALLOCATION
		SQL = "INSERT INTO F_HEAD_ALLOCATION (HEADID, HEADALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE) " &_
				"VALUES (" & HEADID & ", 0, GETDATE(), " & SESSION("USERID") & ", 1)"
		Conn.Execute SQL, recaffected

		'FINALLY MAKE THE LINK BETWEEN THE HEAD AND DEVELOPMENT
		SQL = "INSERT INTO F_DEVELOPMENT_HEAD_LINK (DEVELOPMENTID, HEADID) VALUES " &_
				"(" & DEVID & ", " & HEADID & ")"
		Conn.Execute SQL, recaffected				
		UpdateFunding(DEVID)
								
		GO()		
	End Function
	
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)

		'UPDATE THE DEVELOPMENT INFORMATION		
		SQL = "UPDATE P_DEVELOPMENT " & strSQL & " WHERE DEVELOPMENTID = " & ID
		Conn.Execute SQL, recaffected
		
		'UPDATE THE HEAD NAME UNDER THE DEVELOPMENT COST CENTRE AS FOLLOWS
		SQL = "UPDATE F_HEAD SET DESCRIPTION = D.DEVELOPMENTNAME FROM P_DEVELOPMENT D " &_
				"INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN F_HEAD H ON H.HEADID = DHL.HEADID " &_
				"WHERE D.DEVELOPMENTID = " & ID
		Conn.Execute SQL
		
		UpdateFunding(ID)
		
		GO()
	End Function
	
	Function DeleteRecord()
		ID = Request.Form(UpdateID)	

		// check development has no attached properties
		//if none found then let the item go to next stage else exit function		
		SQL = "SELECT COUNT(*) FROM P__PROPERTY WHERE DEVELOPMENTID = " & ID
		Call OpenRs(rsSet, SQL)
		cnt = rsSet(0)
		Call CloseRs(rsSet)
		if cnt > 0 Then 
			dev_has_properties = 1
			Exit Function
		End If

		//NEXT CHECK IF ANY PURCHASES HAVE BEEN MADE WITH THE ASSOCIATED HEAD
		//IF NOT THEN ALLOW DELETION, AND ALSO DELETE ALL THE HEADS AND EXPENDITURES.
		SQL = "SELECT COUNT(*) AS THECOUNT FROM F_PURCHASEITEM PI " &_
				"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
				"INNER JOIN F_HEAD HE ON HE.HEADID = EX.HEADID " &_
				"INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.HEADID = HE.HEADID " &_
				"WHERE DHL.DEVELOPMENTID = " & ID
		Call OpenRs(rsCheck, SQL)
		ExpenditureCount = rsCheck("THECOUNT")
		Call CloseRs(rsCheck)
		if ExpenditureCount > 0 Then 
			Purchase_Items_Found = 1
			Exit Function
		End If

		//NEXT CHECK IF ANY GRANTS HAVE BEEN ASSIGNED TO THE DEVELOPMENT AS FOLLOWS
		SQL = "SELECT COUNT(*) AS GRANTS FROM P_DEVLOPMENTGRANT WHERE DEVCENTREID = " & ID
		Call OpenRs(rsCheck, SQL)
		Grants_Count = rsCheck("GRANTS")
		Call CloseRs(rsCheck)
		if Grants_Count > 0 Then 
			Grants_Found = 1
			Exit Function
		End If

		//FINALLY CHECK FOR ANY PURCHASE ORDERS..
		SQL = "SELECT COUNT(*) AS POS FROM F_PURCHASEORDER WHERE DEVELOPMENTID = " & ID
		Call OpenRs(rsCheck, SQL)
		PO_Count = rsCheck("POS")
		Call CloseRs(rsCheck)
		if PO_Count > 0 Then 
			POS_Found = 1
			Exit Function
		End If
		
		//if checks passed then delete all related items
		IF (Purchase_Items_Found = 0 AND dev_has_properties = 0 and POS_Found = 0 AND Grants_Found = 0) THEN
			
			SQL = "DELETE FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected
		
			SQL = "DELETE FROM F_EXPENDITURE_ALLOCATION " &_
					"FROM F_DEVELOPMENT_HEAD_LINK DHL " &_
					"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = DHL.HEADID " &_
					"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID " &_
					"WHERE DHL.DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected
		
			SQL = "DELETE FROM F_EXPENDITURE " &_
					"FROM F_DEVELOPMENT_HEAD_LINK DHL " &_
					"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = DHL.HEADID " &_
					"WHERE DHL.DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected
			
			SQL = "DELETE FROM F_HEAD_ALLOCATION " &_
					"FROM F_DEVELOPMENT_HEAD_LINK DHL " &_
					"INNER JOIN F_HEAD_ALLOCATION HE ON HE.HEADID = DHL.HEADID " &_
					"WHERE DHL.DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected

			SQL = "DELETE FROM F_HEAD " &_
					"FROM F_DEVELOPMENT_HEAD_LINK DHL " &_
					"INNER JOIN F_HEAD HE ON HE.HEADID = DHL.HEADID " &_
					"WHERE DHL.DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected	
		
			SQL = "DELETE FROM F_DEVELOPMENT_HEAD_LINK WHERE DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected
			SQL = "DELETE FROM P_DEVELOPMENTFUNDINGLIST WHERE DEVELOPMENTID = " & ID
			Conn.Execute SQL, recaffected
		END IF
		
		GO()	
			
	End Function
	
	Function LoadRecord()
		ID = Request.Form(UpdateID)
		LoaderString = ""
		Call OpenRs(rsLoader, "SELECT * FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & ID)
		if not (rsLoader.EOF) then
			for i=0 to Ubound(FormFields)
				FormArray = Split(FormFields(i), "|")
				TargetItem = FormArray(0)
				LoaderString = LoaderString & "parent.RSLFORM." & TargetItem & ".value = """ & rsLoader(RIGHT(TargetItem, Len(TargetItem)-4)) & """" & VbCrLf
			next
		end if
		Call BuildSelectListBox_SP(lstFunding, "sel_DEVELOPMENTFUNDINGLIST", "P_DEVELOPMENT_GET_FUNDING", null,null,null,"listbox200", " TABINDEX=8",ID)
		CloseRs(rsLoader)
	End Function
	
	Function UpdateFunding(ID)
	
	    'ID = Request.Form(UpdateID)
	    sql= "DELETE FROM P_DEVELOPMENTFUNDINGLIST WHERE DEVELOPMENTID=" & ID
		Conn.Execute SQL, recaffected
	
		If FundingListCols <> "" then 
			FundingListValues = 	Replace(FundingListCols,",","','")
		    sql= "INSERT INTO P_DEVELOPMENTFUNDINGLIST (DEVELOPMENTID," & FundingListCols & ") values (" & ID & ", '" & FundingListValues & "')" 
		    response.Write sql
		    Conn.Execute SQL, recaffected
	    
		End If    
		
		
	End Function
	
	
	Function GO()
		CloseDB()
		Response.Redirect "development_reloader.asp"	
	End Function
	
	TheAction = Request("hid_Action")
	
	OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	CloseDB()
%>
<html>
<body onLoad="ReturnData()">


<script language=javascript>
function ReturnData(){
	<% if TheAction = "LOAD" then %>
	<%=LoaderString%>
	parent.document.getElementById("sel_DEVELOPMENTFUNDINGLIST").outerHTML = DEVDATA.innerHTML;
	parent.filltextbox();
	
	parent.RSLFORM.hid_ACTION.value = "AMEND"
	parent.checkForm()
	parent.swap_div(3);
	parent.checkForm();
	<% end if %>
	
	<% if dev_has_properties = 1 Then %>
		alert("This development has properties attached and therefore can not be deleted.");
	<% elseif Purchase_Items_Found = 1 Then %>
		alert("This development has purchases attached and therefore can not be deleted.");	
	<% elseif Grants_Found = 1 Then %>
		alert("This development has grants attached and therefore can not be deleted.");	
	<% elseif PO_Count = 1 Then %>
		alert("This development has purchases attached and therefore can not be deleted.");	
	<% end if %>
	}
</script>
<div id="DEVDATA"><%=lstFunding%></div>
</body>
</html>