<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (29)
Dim DataTypes    (29)
Dim ElementTypes (29)
Dim FormValues   (29)
ReDim FormFields   (29)
UpdateID	  = "hid_PropertyID2"
FormFields(0) = "txt_FLOORAREA|NUMBER"
FormFields(1) = "txt_MAXPEOPLE|NUMBER"
FormFields(2) = "txt_BEDROOMS|NUMBER"
FormFields(3) = "txt_BEDROOMSITTINGROOM|NUMBER"
FormFields(4) = "txt_KITCHEN|NUMBER"
FormFields(5) = "txt_KITCHENDINER|NUMBER"
FormFields(6) = "txt_LIVINGROOM|NUMBER"
FormFields(7) = "txt_LIVINGROOMDINER|NUMBER"
FormFields(8) = "txt_DININGROOM|NUMBER"
FormFields(9) = "txt_WC|NUMBER"
FormFields(10) = "txt_BATHROOM|NUMBER"
FormFields(11) = "txt_BATHROOMWC|NUMBER"
FormFields(12) = "txt_SHOWER|NUMBER"
FormFields(13) = "txt_SHOWERWC|NUMBER"
FormFields(14) = "txt_BOXROOMUTILITYROOM|NUMBER"
FormFields(15) = "txt_SHAREDKITCHENLIVING|NUMBER"
FormFields(16) = "txt_SHAREDWC|NUMBER"
FormFields(17) = "txt_SHAREDSHOWERWC|NUMBER"
FormFields(18) = "sel_FURNISHING|NUMBER"
FormFields(19) = "sel_SECURITY|NUMBER"
FormFields(20) = "sel_HEATING|NUMBER"
FormFields(21) = "sel_GARDEN|NUMBER"
FormFields(22) = "sel_PARKING|NUMBER"
FormFields(23) = "sel_PETS|NUMBER"
FormFields(24) = "txt_KITCHENDINERLOUNGE|NUMBER"
FormFields(25) = "sel_ACCESSIBILITYSTANDARDS|NUMBER"
FormFields(26) = "sel_FRONTPORCH|NUMBER"
FormFields(27) = "sel_CONSERVATORYUPVC|NUMBER"
FormFields(28) = "sel_GARAGE|NUMBER"
FormFields(29) = "cde_PROPERTYID|'" & Request.Form("hid_PropertyID2") & "'"

Function NewRecord()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)
	SQL = "INSERT INTO P_ATTRIBUTES " & strSQL & ""
	Conn.Execute SQL
	Call GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
	Redim Preserve FormFields (28)
	Call MakeUpdate(strSQL)	
	SQL = "UPDATE P_ATTRIBUTES " & strSQL & " WHERE PROPERTYID = '" & ID & "'"
	Conn.Execute SQL, recaffected
	Call GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)
    SQL = "DELETE FROM P__PROPERTY WHERE PROPERTYID = '" & ID & "'"
	Conn.Execute SQL, recaffected
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../Attributes.asp?PropertyID=" & ID
End Function

TheAction = Request("hid_Action2")

Call OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
Call CloseDB()
%>