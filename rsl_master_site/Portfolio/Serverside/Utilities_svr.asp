<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (6)
Dim DataTypes    (6)
Dim ElementTypes (6)
Dim FormValues   (6)
ReDim FormFields   (6)
UpdateID	  = "hid_PropertyID"
FormFields(0) = "txt_COUNCILTAXPAYABLE|NUMBER"
FormFields(1) = "txt_WATERMETERNUMBER|TEXT"
FormFields(2) = "sel_WATERAUTHORITY|NUMBER"
FormFields(3) = "sel_ELECTRICITYSUPPLIER|NUMBER"
FormFields(4) = "sel_COUNCILTAXBAND|NUMBER"
FormFields(5) = "sel_GASSUPPLIER|NUMBER"
FormFields(6) = "cde_PROPERTYID|'" & Request.Form("hid_PropertyID") & "'"
  
Function NewRecord ()
	ID = Request.Form(UpdateID)	
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO P_UTILITIES " & strSQL & ""
	Conn.Execute SQL
	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	
	Redim Preserve FormFields (5)
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE P_UTILITIES " & strSQL & " WHERE PROPERTYID = '" & ID & "'" 
	
	Conn.Execute SQL, recaffected
	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)	

	SQL = "DELETE FROM P__PROPERTY WHERE PROPERTYID = '" & ID & "'" 
	Conn.Execute SQL, recaffected
End Function

Function GO()
	CloseDB()
	Response.Redirect "../Utilities.asp?PropertyID=" & ID
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>