<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (24)
Dim DataTypes    (24)
Dim ElementTypes (24)
Dim FormValues   (24)
ReDim FormFields (24)
UpdateID = "hid_PropertyID"
Dim TableString
Dim THESELECT
  
Function NewRecord ()
	ID = Request.Form(UpdateID)	
	SQL = "INSERT INTO P_PORTFOLIOADAPTATIONS (PROPERTYID, ADAPTATIONID) VALUES ('" & ID & "', " & Request.Form("sel_ADAPTIONS") & ")"
	Conn.Execute SQL
	Rebuild()
End Function

Function Rebuild()
	SQL = "SELECT AD.DESCRIPTION, AD.ADAPTATIONID FROM P_PORTFOLIOADAPTATIONS PA INNER JOIN P_ADAPTATIONS AD ON PA.ADAPTATIONID = AD.ADAPTATIONID WHERE PROPERTYID = '" & ID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString = "<tr>"		
		while NOT rsLoader.EOF 
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString = TableString & "</tr><tr>"				
			TableString = TableString & "<td width=""216"">" & rsLoader("DESCRIPTION") & "</td><td style=""cursor:pointer"" width=""30"" onclick=""DeleteAD(" & rsLoader("ADAPTATIONID") & ")"" title=""Delete""><font color=""red"">DEL</font></td>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString = TableString & "<td colspan=""4""></td>"
		if (Remainder = 2) then TableString = TableString & "<td colspan=""2""></td>"		
		TableString = TableString & "</TR>"
	else
		TableString = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	end if
	Call CloseRs(rsLoader)

	THESELECT = "<select name=""sel_ADAPTIONS"" id=""sel_ADAPTIONS"" class=""textbox200"" style=""width:350""><option value="""">Please Select an Adapt Cat</option></select>"
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM P_PORTFOLIOADAPTATIONS WHERE PROPERTYID = '" & ID & "' AND ADAPTATIONID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute (SQL)
	Rebuild()
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)
	Call BuildSelect(THESELECT, "sel_ADAPTIONS", "P_ADAPTATIONS AD WHERE NOT EXISTS (SELECT * FROM P_PORTFOLIOADAPTATIONS PA WHERE PA.ADAPTATIONID = AD.ADAPTATIONID AND PROPERTYID = '" & ID & "') AND ADAPTATIONCAT = " & Request.Form("sel_ADS") & " ", "ADAPTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", "  style=""width:350"" ")		
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module</title>
    <script type="text/javascript" language="javascript">

        function sendData(){
        <% If (TheAction = "LOAD") Then %>
	        parent.document.getElementById("dvADAPTIONS").innerHTML	= document.getElementById("dvADAPTIONS").innerHTML	
        <% Else %>
	        parent.document.getElementById("AD").innerHTML = document.getElementById("AD").innerHTML
	        parent.document.getElementById("sel_ADS").value = ""
	        parent.document.getElementById("dvADAPTIONS").innerHTML	= document.getElementById("dvADAPTIONS").innerHTML	
        <% End If %>
        }

    </script>
</head>
<body onload="sendData()">
    <div id="dvADAPTIONS">
        <%=THESELECT%>
    </div>
    <div id="AD">
        <table cellspacing="2" cellpadding="1" style="border-collapse: collapse" border="1"
            width="100%" height="100%">
            <%=TableString%>
            <tr>
                <td height="100%" colspan="6">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
