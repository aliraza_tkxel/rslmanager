<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim adapatationcat
		adapatationcat = Request.Form("sel_ADAPTATIONCAT")

	If (adapatationcat = "") Then
		adapatationcat = -1
	End If

	SQL = "SELECT * FROM P_ADAPTATIONS A WHERE A.ADAPTATIONCAT = " & adapatationcat & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsAdaptation, SQL)	
		count = 0
		optionstr = ""
		If (NOT rsAdaptation.EOF) Then
			While (NOT rsAdaptation.EOF)
				optionstr = optionstr & "<option value=""" & rsAdaptation("ADAPTATIONID") & """>" & rsAdaptation("DESCRIPTION") & "</option>"
				rsAdaptation.moveNext
				count = 1
			Wend
		End If
	Call CloseRs(rsAdaptation)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module > Propert Search > Property Blocks</title>
    <script type="text/javascript" language="JavaScript">
    function ReturnData(){
    <% if count = 0 then %>
	    parent.document.getElementById("dvADAPTATIONS").innerHTML = "<select name='sel_ADAPTATIONS' id='sel_ADAPTATIONS' class='textbox200' disabled><option value=''>None Found</option></select>";
	    parent.document.getElementById("sel_ADAPTATIONS").disabled = true
    <% else %>
	    parent.document.getElementById("dvADAPTATIONS").innerHTML = document.getElementById("dvADAPTATIONS").innerHTML
	    parent.document.getElementById("sel_ADAPTATIONS").disabled = false
    <% end if %>
	    }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvADAPTATIONS">
        <select name="sel_ADAPTATIONS" id="sel_ADAPTATIONS" class="textbox200">
            <option value="">Please Select</option>
            <%=optionstr%>
        </select></div>
</body>
</html>
