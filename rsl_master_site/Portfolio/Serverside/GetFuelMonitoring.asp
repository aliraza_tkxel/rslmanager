<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,AppointmentBooking,fuelid,uprn
	Dim HeadPlusData,MonitoringData,AllData,Menu,Head,monitoringid,AppointmentData,SubHead
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function
	
	Function GetPostedData()
		'This function will get all the posted data
		orgid=nulltest(Request.Item("sel_ORGANISATION"))
		patchid=nulltest(Request.Form.Item("sel_PATCH"))
		developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
        fuelid=nulltest(Request.Form.Item("sel_FUEL"))
        uprn=nulltest(Request.Form.Item("txt_UPRN"))
		monitoringid=Request.Item("hid_mon_menu") 
	End Function
	
	Dim str_Filter
		str_Filter = ""

		If IsNull(patchid) then
			' FILTER IS FILTER
		Else
			str_Filter=" AND PD.PATCHID = " & PATCHID & " "
		End If

		If IsNull(developmentid) then
			' FILTER IS FILTER
		Else
			str_Filter = str_Filter & " AND PD.DEVELOPMENTID = " & DEVELOPMENTID & " "
		End If
	
	Function GetHead(StyleLine,Title,Id,styleTitle)
		Dim AnyHead
			AnyHead="      <div>"
			AnyHead=AnyHead & "   <div class='heading' id='" & Id & "' " & styleTitle & ">" & Title & "</div>"
			AnyHead=AnyHead & "   <div class='content_headline' "& StyleLine &">"
			AnyHead=AnyHead & "		<div class='content_headlinediv' style='border-bottom:1px solid #133e71;'></div> "
			AnyHead=AnyHead & "		<div class='content_headlinediv' style='border-right:1px solid #133e71;'></div> "
			AnyHead=AnyHead & "    </div>"
			AnyHead=AnyHead & "</div>"
			GetHead=AnyHead
	End Function

	Function MenuOption()
	    Menu="<p style='margin:20 0 5 0'>"
        Menu=Menu & "<select id=""sel_Monitoring"" name=""sel_Monitoring"" class='textbox200' style='width:190px;font-weight:bold;color:#133e71;font-size:12px;' onchange=""ChangeMenu();"">"   
            Menu=Menu & "<option value='1' selected>LGSR Status</option>"
        Menu=Menu & "</select>"
        Menu=Menu & "</p>"
        Menu=Menu & "<p class='line'> </p>"
	End Function
	
	
Function GetPropertyCP12Status()

	MonitoringData="<table style='border-collapse:collapse;width:100%;' cellspacing=4 cellpadding=2>"
		
	set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "P_LGSR_COUNTS"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
        spUser.Parameters.Append spUser.CreateParameter("@FUELID", 3, 1,4,fuelid)
        spUser.Parameters.Append spUser.CreateParameter("@UPRN", 200, 1,10,uprn)
	set rsSet = spUser.Execute
		If NOT rsSet.EOF Then
					MonitoringData=MonitoringData & "<tr><td class='fonttwelevewithcolor'>No Cert. Data <span class='linkstylesmall' onclick=""GetDetailMonitoring(8)"">" & rsSet("NO_LGSR") & "</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Expired <span class='linkstylesmall' onclick=""GetDetailMonitoring(0)"">" & rsSet("EXPIRED") & "</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 1 week <span class='linkstylesmall' onclick=""GetDetailMonitoring('1')"">" & rsSet("LESSTHANEQUALTOWEEK_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 1 - 4 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('2')"">" & rsSet("BETWEEN1AND4WEEKS_CP12") &"</span></td></tr>"
					MonitoringData=MonitoringData & "<tr><td class='fonttwelevewithcolor'>Due in 4 - 8 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('3')"">" & rsSet("BETWEEN4AND8WEEKS_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 8 - 12 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('4')"">" & rsSet("BETWEEN8AND12WEEKS_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 12 - 16 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('5')"">" & rsSet("BETWEEN12AND16WEEKS_CP12") &"</span></td>"
                    MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 16 - 52 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('6')"">" & rsSet("BETWEEN16AND52WEEKS_CP12") &"</span></td></tr>"
					MonitoringData=MonitoringData & "<tr><td class='fonttwelevewithcolor'>Due in > 52 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('7')"">" & rsSet("GREATERTHAN52WEEKS_CP12") &"</span></td>"
                    MonitoringData=MonitoringData & "<td></td><td></td><td class='fonttwelevewithcolor'><b>Total Units: " & rsSet("TOTAL_UNITS") &"</b></td></tr>"
		End If
	set sprocDisplayIagManagerUsers = nothing
	
	 MonitoringData= MonitoringData & "</table>"
	 AllData="<div class=""content_body"" style='width:97%;'>" & Menu & MonitoringData & "</div>"
     rsSet.Close

End Function
	


Call OpenDB()
    Call GetPostedData()
    Head=GetHead("style='width:75%;'","Monitoring","MonitoringHead","style='width:22%;'")
    SubHead=GetHead("style='width:73%'","","MonitoringSubHead","style='width:25%;'")
    'Call MenuOption()
    Call GetPropertyCP12Status()

Call CloseDB()
HeadPlusData="<div id=""mon_menu_data"" style='width:100%;height:100%;'><div id=""mon_menu"" style='width:100%;margin-bottom:10px;'>" & Head & AllData & "</div><div id=""mon_data"" style='border:1px solid white;float:left;width:100%;'>" & SubHead &  "<div class=""content_body"" id=""subdatabody"" style='width:98%;height:100%;padding-bottom:29px;'></div> </div></div>"
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>LGSR Report</title></head>
<script language="javascript" type="text/javascript">
function ReturnData()
{
	parent.GetDetailMonitoring(0);
	parent.document.getElementById("content").innerHTML= parent.document.getElementById("content").innerHTML + Summary.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<form name = "thisForm" method="post" action="">
<div id="Heading"><%=Heading%></div>
<div style=" text-align:center;" id="Summary"><%=HeadPlusData%></div>
</form>
</body>
</html>