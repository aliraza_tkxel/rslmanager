<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Function CylicalNumber (strWord)
		CylicalNumber = "<b>CYC</b> " & characterPad(strWord,"0", "l", 6)
	End Function

	Dim ID, LoaderString, zoneid, areaid, itemid, repairid
	Dim lst_item, lst_element, lst_repair, str_priority, RepairCost
	Dim pmstring
	
	Dim FieldCounter
	FieldCounter = 8
	
	reDim DataFields   (FieldCounter)
	reDim DataTypes    (FieldCounter)
	reDim ElementTypes (FieldCounter)
	reDim FormValues   (FieldCounter)
	reDim FormFields   (FieldCounter)
	
	UpdateID	  = "hid_PMID"
	FormFields(0) = "txt_STARTDATE|DATE"
	FormFields(1) = "txt_CYCLE|INTEGER"
	FormFields(2) = "txt_FREQUENCY|INT"
	FormFields(3) = "hid_DEVELOPMENTID|TEXT"
	FormFields(4) = "sel_BLOCK|SELECT"
	FormFields(5) = "sel_REPAIR|SELECT"
	FormFields(6) = "sel_CONTRACTOR|SELECT"
 	FormFields(7) = "txt_CONTRACTSTARTDATE|DATE"
 	FormFields(8) = "hid_USERID|INT"


	' ENTER NEW WARRANTY RECORD
	Function NewRecord ()
		ID = Request.Form(UpdateID)		
		Call MakeInsert(strSQL)	
		SQL = "SET NOCOUNT ON;INSERT INTO P_PLANNEDMAINTENANCE " & strSQL & ";SELECT SCOPE_IDENTITY() AS NEWID"
		Response.Write SQL
		Call OpenRs(rsNEW, SQL)
		ID = rsNEW("NEWID")
		Call CloseRs(rsNEW)
		
		SQL = ""
		Response.Write "Saved"
		
		' GET THE REPAIR COST ITEMS
		RepairCosts = Request.Form("hid_REPAIRPRICES")
		RepairCostArray = Split(RepairCosts, "|DDD|")

		' THIS PART CREATES A DICTIONARY WITH THE REPAIR ID AS KEYS FOR THE REPAIR COSTS.
		Dim RC   ' Create a variable.
		Set RC = CreateObject("Scripting.Dictionary")
		for i=0 to Ubound(RepairCostArray)
			tRepairArray = Split(RepairCostArray(i), "||")
			tRepairID = tRepairArray(0)
			tRepairCost = tRepairArray(1)

			RC.Add tRepairID, tRepairCost   ' Add some keys and items.
		next
		
		' NEXT UPDATE THE INCLUDES TABLE APPROPRIATELY 
		IF (Request.Form("IncludeProperties") <> "-1") then
			iData = Request.Form("IncludeProperties")
			iArray = Split(iData, "||<>||")
			for i=0 to ubound(iArray)
				iMinorArray = Split(iArray(i), "|")
				SQL = "INSERT INTO P_PLANNEDINCLUDES (PROPERTYID, REPAIRID, PMID, REPAIRNETCOST) VALUES ('" & iMinorArray(1) & "', " & iMinorArray(0) & ", " & ID & "," & RC.Item(iMinorArray(0)) & ")"
				Conn.Execute SQL
			next
		END IF	
		
		run_job()
		
		Response.Write "finished"	
	End Function
	
	' AMEND WARRANTY RECORD
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE P_PLANNEDMAINTENANCE " & strSQL & " WHERE PMID = '" & ID & "'"
		
		Conn.Execute SQL, recaffected
	End Function
	
	Function DeleteRecord()
		ID = Request.Form(UpdateID)	
		SQL = "SELECT * FROM P_RUNNINGMAINTENANCE WHERE PMID = " & ID
		Call OpenRs(rsRUN, SQL)
		'This cyclical repair has never been ran, therefore can be deleted completely.
		if (rsRUN.EOF) then
			SQL = "DELETE FROM P_PLANNEDMAINTENANCE WHERE PMID = '" & ID & "'"
			Conn.Execute SQL, recaffected
		'THIS ITeM HAS BEEN RUN AT LEAST ONCE THEREFORE CANNOT BE DELETED
		else
			SQL = "UPDATE P_PLANNEDMAINTENANCE SET PM_ACTIVE = 0, MADE_INACTIVE = GETDATE() WHERE PMID = '" & ID & "'"
			Conn.Execute SQL, recaffected
		end if
		Call CloseRs(rsRUN)
				
	End Function
	
	' BUILD LOAD STRING --// NOT USED AS YET
	Function LoadRecord()
	
		pm_id = Request.Form("hid_PMID")
	
		SQL = "WHERE 	P.PMID = " & pm_id	
				
		Call OpenRs(rsLoader, SQL)
		LoaderString = ""
		if (NOT rsLoader.EOF) then
			LoaderString = LoaderString & "parent.RSLFORM.txt_CONTRACTSTARTDATE.value = """ & rsLoader("CCONTRACTDATE") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.sel_BLOCK.value = """ & rsLoader("BLOCKID") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.sel_CONTRACTOR.value = """ & rsLoader("CONTRACTORID") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_STARTDATE.value = """ & rsLoader("CSTART") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_CYCLE.value = """ & rsLoader("CCYCLE") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_NOTES.value = """ & rsLoader("CNOTES") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_FREQUENCY.value = """ & rsLoader("CFREQUENCY") & """;"	
			zoneid = rsLoader("ZONEID")
			item_id =  rsLoader("ITEMID")
			areaid = rsLoader("AREAID")
			repairid = rsLoader("CREPAIR")
		end if
		CloseRs(rsLoader)
		
	End Function

	' RUNS THE SP WHICH GENERATES THE PLANNED MAINTENANCE CYCLKE (ONLY IF CONTRACT START DATE HAS PASSSED)
	Function run_job()
		
		Set Rs = Server.CreateObject("ADODB.Command")
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4 
		Rs.commandtext = "CYC_REPAIRS_INSERT_INITIAL_LOOP"
		Rs.Parameters(1) = ID
		Rs.execute

	End Function

	Function BL_BuildTable(WHICH)
		IF (WHICH = 1) THEN
			SQL = "SELECT  p.PMID, ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, " &_
					"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
					"ISNULL(P.CYCLE,'') AS CCYCLE, P.FREQUENCY, " &_
					"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_
					"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
					"ISNULL(P.NOTES ,'') AS CNOTES, " &_
					"ISNULL(S.NAME,'') AS CONTRACTOR, " &_
					"R.DESCRIPTION, " &_
					"CURRENTSTATUS = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'INACTIVE' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'COMPLETED' " &_					
					"ELSE 'ACTIVE' " &_
					"END, " &_
					"NEXTCYCLE = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'N/A' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'N/A' " &_					
					"WHEN LASTEXECUTED = '1 JULY 1970' THEN 'NOT STARTED' " &_					
					"ELSE CONVERT(NVARCHAR, DATEADD(M, P.CYCLE, LASTEXECUTED), 103) " &_					
					"END, " &_
					"(SELECT ISNULL(SUM(REPAIRNETCOST),0) AS REPAIRNETCOST FROM P_PLANNEDINCLUDES PINC WHERE PINC.PMID = P.PMID) AS REPAIRNETCOST " &_
				"FROM 	P_PLANNEDMAINTENANCE P " &_
					"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
					"INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
					"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
				"WHERE 	P.BLOCK = " & Request.Form("sel_BLOCK") & " " &_
				"ORDER BY CURRENTSTATUS ASC, P.PMID DESC "					
		ELSE
		SQL = "SELECT  p.PMID, ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, " &_
					"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
					"ISNULL(P.CYCLE,'') AS CCYCLE, P.FREQUENCY, " &_
					"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_
					"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
					"ISNULL(P.NOTES ,'') AS CNOTES, " &_
					"ISNULL(S.NAME,'') AS CONTRACTOR, " &_
					"R.DESCRIPTION, " &_
					"CURRENTSTATUS = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'INACTIVE' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'COMPLETED' " &_					
					"ELSE 'ACTIVE' " &_
					"END, " &_
					"NEXTCYCLE = CASE  " &_
					"WHEN PM_ACTIVE = 0 THEN 'N/A' " &_
					"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'N/A' " &_					
					"WHEN LASTEXECUTED = '1 JULY 1970' THEN 'NOT STARTED' " &_					
					"ELSE CONVERT(NVARCHAR, DATEADD(M, P.CYCLE, LASTEXECUTED), 103) " &_					
					"END, " &_
					"(SELECT ISNULL(SUM(REPAIRNETCOST),0) AS REPAIRNETCOST FROM P_PLANNEDINCLUDES PINC WHERE PINC.PMID = P.PMID) AS REPAIRNETCOST " &_
			  	"FROM 	P_PLANNEDMAINTENANCE P " &_
			  		"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
					"INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
					"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
				"WHERE 	P.BLOCK IS NULL AND P.DEVELOPMENTID = " & Request.Form("hid_DEVELOPMENTID") & " " &_
				"ORDER BY CURRENTSTATUS ASC, P.PMID DESC "
						
		END IF

		'Response.Write SQL
						
		Call OpenRs(rsLoader, SQL)
		
		if (NOT rsLoader.EOF) then
		
			WHILE NOT rsLoader.EOF
				CURRENTSTATUS = rsLoader("CURRENTSTATUS")
				IF (CURRENTSTATUS = "INACTIVE") THEN
					pmstring = pmstring &_
								"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "') style='text-decoration:line-through'>" &_
									"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
									"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
									"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
									"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
									"<TD bgcolor=white TITLE=""Inactive Cyclical Repair"" width=500 align=center><font color=red>INA</font></TD></TR>"						
				ELSEIF (CURRENTSTATUS = "COMPLETED") THEN
					pmstring = pmstring &_
								"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "')>" &_
									"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
									"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
									"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
									"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
									"<TD bgcolor=white TITLE=""Completed Cyclical Repair"" width=500 align=center><font color=blue>CO</font></TD></TR>"
				ELSE
					pmstring = pmstring &_
								"<TR VALIGN=TOP TITLE='" & rsLoader("DESCRIPTION") & "' style='cursor:hand')>" &_
									"<TD width=90 nowrap>" & rsLoader("CSTART") & "</TD>" &_
									"<TD width=410 nowrap>" & CylicalNumber(rsLoader("PMID")) & "<br>" & rsLoader("CREPAIR") & "<br>" & rsLoader("CONTRACTOR") & "<div ALIGN=RIGHT><b>Every</b> " & rsLoader("CCYCLE")& " <b>months for</b> " & rsLoader("FREQUENCY") & " <b>occurrences. Total NetCost:</b> " & FormatCurrency(rsLoader("REPAIRNETCOST"))& "</div></TD>" &_
									"<TD width=100 nowrap>" & rsLoader("NEXTCYCLE") & "</TD>" &_
									"<TD width=100 nowrap>" & rsLoader("CLAST") & "</TD>" &_
									"<TD bgcolor=white width=500 TITLE=""Delete/De-activate Cyclical Repair"" align=center onclick=""DeleteMe(" & rsLoader("PMID") & ")""><font color=red>DEL</font></TD></TR>"						
				
				END IF
				rsLoader.MoveNext
			wend
			
		else
			IF (WHICH = 1) THEN		
				pmstring = pmstring & "<tfoot><Tr><TD colspan=6 align=center>No Cyclical Repairs exist for this block</TD></TR></tfoot	>" 
			else
				pmstring = pmstring & "<tfoot><Tr><TD colspan=6 align=center>No Cyclical Repairs exist for this development</TD></TR></tfoot	>" 
			end if			
		end if
		CloseRs(rsLoader)	
	End Function
	
	
	TheAction = Request("hid_Action")
	development_id = Request.Form("hid_DevelopmentID")
	OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		'Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		'Case "LOAD"	    LoadRecord()
	End Select

	//if a block then..
	if (Request.Form("sel_BLOCK") <> "") then 
		BL_BuildTable(1)
	//else if a development then...
	else
		BL_BuildTable(2)
	end if
		
	CloseDB()
%>
<html>
<body onLoad="ReturnData()">
<div id="CYCDATA">
<table cellspacing=0 cellpadding=3 border=1 STYLE='behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' hlcolor='STEELBLUE' width=100%>
  <%=pmstring%> 
</table>
</div>
<script type="text/javascript">
function ReturnData(){
	parent.document.getElementById("CYCDATA").innerHTML = CYCDATA.innerHTML
	parent.RSLFORM.reset()	
	parent.FinalWarning.innerHTML = "<BR><BR><BR><CENTER style='font-size:22px;font-weight:bold'>Cyclical repair saved successfully.</CENTER>"
	setTimeout("parent.CancelSave()",1000)
	}
</script>
</body>
</html>
