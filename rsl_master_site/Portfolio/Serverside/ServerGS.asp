<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	' ************************************************
	' CODE MODIFIED AS PER REQUESTED THROUGH TASK 4485
	' BY ADNAN MIRZA
	' ON 4TH FEB 2008
	' MODIFIED FUNCTIONS NEWRECORD(),AMENDRECORD()
	' ************************************************
Dim ID
Dim DataFields   (24)
Dim DataTypes    (24)
Dim ElementTypes (24)
Dim FormValues   (24)
ReDim FormFields (24)
UpdateID  = "hid_PropertyID"
Dim TableString
Dim THESELECT
Dim applianceid,manufacturer,locationid,contractorid
Dim dateinstalled,issuedate,cp12number,warrantyexpirydate,propertyapplianceid,model,warrantyexpirydatepartsandlabour
Dim updateClause,app_type,loc,manufact,contractor
  
Function NewRecord ()
	' ************************************************
	' CODE MODIFIED AS PER REQUESTED THROUGH TASK 4485
	' BY ADNAN MIRZA
	' ON 4TH FEB 2008
	' ************************************************
	ID = Request.Form(UpdateID)
	app_type= Request.Form("sel_gsappliancetype")
	loc = Request.Form("sel_gslocation")
	manufact= Request.Form("sel_gsmanufacturer")
	contractor= Request.Form("sel_contractor_warranty") 
	
	if app_type="" then app_type="NULL"
	if loc="" then loc="NULL"
	if manufact="" then manufact="NULL"
	if contractor="" then contractor="NULL"

	SQL = "INSERT INTO GS_PROPERTY_APPLIANCE "&_
	      "        (PROPERTYID, APPLIANCETYPEID,MANUFACTURERID,LOCATIONID,MODEL,ORGID " &_
	      "         ,DATEINSTALLED,ISSUEDATE,CP12NUMBER,WARRANTYEXPIRYDATE,WARRANTYEXPIRYDATEPARTSANDLABOUR) " &_ 
	      " VALUES ('" & ID & "', " & app_type &_
	      "          ," & manufact &_ 
	      "          , " & loc &_ 
	      "          , '" & Request.Form("txt_model") & "'" &_ 
	      "          , " & contractor &_
	      "          ,'" & Request.Form("txt_dateinstalled") & "'" &_ 
	      "          ,'" & Request.Form("txt_dateofissue") & "' " &_
	      "          ,'" & Request.Form("txt_cp12number") &"'" &_
	      "          ,'" & Request.Form("txt_warrantyexpiryparts") &"'" &_
	      "          ,'" & Request.Form("txt_warrantyexpirypartsandlabour") & "')"

	Conn.Execute SQL
	Rebuild()
End Function


Function Rebuild()
	
	SQL = " SELECT PROPERTYAPPLIANCEID,AT.APPLIANCETYPE AS APPLIANCETYPE,AT.APPLIANCETYPEID, "&_
	      " DATEINSTALLED,CP12NUMBER,ISSUEDATE,WARRANTYEXPIRYDATE,CP12DOCUMENT,DOCUMENTTYPE  " &_ 
	      " FROM GS_PROPERTY_APPLIANCE GS " &_
	      " INNER JOIN GS_APPLIANCE_TYPE AT ON AT.APPLIANCETYPEID=GS.APPLIANCETYPEID " &_ 
	      " WHERE GS.PROPERTYID= '" & ID & "' "
	
	Call OpenRs(rsLoader,SQL)
	TableString = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString = TableString & "<TR style='BORDER-BOTTOM: #996699 1px solid; border-left-width:0px;' bgcolor='beige'  > " 
		TableString = TableString & "<TD><b>Appliance Type</b></TD>"
		TableString = TableString &  "<TD><b>Date Installed</b></TD>"
		TableString = TableString &	 "<TD><b>LGSR Number</b></TD>"
		TableString = TableString &  "<TD><b>Date of Issue</b></TD>"
		TableString = TableString &  "<TD><b>Warranty Expiry</b></TD>"
		TableString = TableString &  " <td>&nbsp;</td>"
		TableString = TableString &  " <td>&nbsp;</td>"
		TableString = TableString & "</TR>"
		TableString = TableString & "<TR onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"		
		while NOT rsLoader.EOF 
			TableString = TableString & "<TD style='cursor:hand'  onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("APPLIANCETYPE") & "</TD>"
			TableString = TableString & "<TD style='cursor:hand'  onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("DATEINSTALLED") & "</TD>"
			TableString = TableString & "<TD style='cursor:hand'  onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("CP12NUMBER") & "</TD>"
			TableString = TableString & "<TD  style='cursor:hand' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("ISSUEDATE") & "</TD>"
			TableString = TableString & "<TD  style='cursor:hand' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("WARRANTYEXPIRYDATE") & "</TD>"
			if(rsLoader("DOCUMENTTYPE")<>"") then
			  TableString = TableString & "<TD style='cursor:hand' onclick=""DisplayDoc( '" & ID & "'," & rsLoader("APPLIANCETYPEID") & ")""><img alt='' src='/myImages/paper_clip.gif'/></TD>"  
			else
			  TableString = TableString & "<TD style='cursor:hand'  onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")""></TD>"
			end if
			TableString = TableString & "<TD style='cursor:hand' width=30 onclick=""DeleteGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")""><font color=red>DEL</font></TD>"
			Counter = Counter + 1
			TableString = TableString & "</TR><TR onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (COUNTER = 0) then 
			TableString = "<TR><TD colspan=6 align=center>No items found</td></tr>"
		end if
	end if
	CloseRs(rsLoader)
	
	
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	// STORED PROCEDURE TO DEAL WITH DELETIONS SHOULD DELETE ALL PENDING JOBS
    
	SQL = " DELETE FROM P_APPLIANCE_INSPECTION "&_ 
	      " WHERE PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute (SQL)

	SQL = " DELETE FROM GS_PROPERTY_APPLIANCE "&_ 
	      " WHERE PROPERTYID = '" & ID & "'" &_
	      " AND PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute (SQL)
	Rebuild()
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)
	// STORED PROCEDURE TO DEAL WITH DELETIONS SHOULD DELETE ALL PENDING JOBS
	SQL = "SELECT PROPERTYAPPLIANCEID,APPLIANCETYPEID,MANUFACTURERID,MODEL,LOCATIONID, " &_
	      "       ORGID,DATEINSTALLED,ISSUEDATE,CP12NUMBER,WARRANTYEXPIRYDATE,WARRANTYEXPIRYDATEPARTSANDLABOUR "&_
	      " FROM GS_PROPERTY_APPLIANCE WHERE PROPERTYID = '" & ID & "' " &_ 
	      " AND PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	      
	Call OpenRs(rsLoader,SQL)
	if (NOT rsLoader.EOF) then
	    propertyapplianceid=rsLoader("PROPERTYAPPLIANCEID")
	    applianceid=rsLoader("APPLIANCETYPEID")
	    manufacturer=rsLoader("MANUFACTURERID")
	    locationid=rsLoader("LOCATIONID") 
	    model=rsLoader("MODEL") 
	    contractorid=rsLoader("ORGID") 
	    dateinstalled=rsLoader("DATEINSTALLED") 
	    issuedate=rsLoader("ISSUEDATE")
	    cp12number=rsLoader("CP12NUMBER")
	    warrantyexpirydate=rsLoader("WARRANTYEXPIRYDATE")
	    warrantyexpirydatepartsandlabour=rsLoader("WARRANTYEXPIRYDATEPARTSANDLABOUR")
	end if    
	CloseRs(rsLoader)
	Rebuild()
End Function

Function AmendRecord()
' ************************************************
' CODE MODIFIED AS PER REQUESTED THROUGH TASK 4485
' BY ADNAN MIRZA
' ON 4TH FEB 2008
' ************************************************
 ID = Request.Form(UpdateID)
	
	if Request.Form("sel_gsappliancetype") <> "" then
		updateClause = updateClause & " ,APPLIANCETYPEID=" & Request.Form("sel_gsappliancetype") 
	end if
	
	if Request.Form("sel_gsmanufacturer") <> "" then
		updateClause = updateClause & " ,MANUFACTURERID=" & Request.Form("sel_gsmanufacturer") 
	end if
	
	if Request.Form("sel_gslocation") <> "" then
		updateClause = updateClause & " ,LOCATIONID=" & Request.Form("sel_gslocation")
	end if
	
	if Request.Form("txt_model") <> "" then
		updateClause = updateClause & " ,MODEL='" & Request.Form("txt_model") & "'"
	end if
	
	if Request.Form("sel_contractor_warranty") <> "" then
		updateClause = updateClause & " ,ORGID=" & Request.Form("sel_contractor_warranty")
	end if
	
	if Request.Form("txt_dateinstalled") <> "" then
		updateClause = updateClause & " ,DATEINSTALLED='" & Request.Form("txt_dateinstalled") & "'"
	end if
	
	if Request.Form("txt_dateofissue") <> "" then
		updateClause = updateClause & " ,ISSUEDATE='" & Request.Form("txt_dateofissue") & "'"
	end if
	
	if Request.Form("txt_cp12number") <> "" then
		updateClause = updateClause & " ,CP12NUMBER='" & Request.Form("txt_cp12number") & "'"
	end if
	
	if Request.Form("txt_warrantyexpiryparts") <> "" then
		updateClause = updateClause & " ,WARRANTYEXPIRYDATE='" & Request.Form("txt_warrantyexpiryparts") & "'"
	end if
	
	if Request.Form("txt_warrantyexpirypartsandlabour") <> "" then
		updateClause = updateClause & " ,WARRANTYEXPIRYDATEPARTSANDLABOUR='" & Request.Form("txt_warrantyexpirypartsandlabour") & "'"
	end if

	SQL = " UPDATE GS_PROPERTY_APPLIANCE " &_
	      " SET PROPERTYID= '" & ID & "' " & updateClause &_
	      " WHERE PROPERTYID = '" & ID & "' " &_  
	      " AND PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "

	'SQL = " UPDATE GS_PROPERTY_APPLIANCE " &_
	'      " SET APPLIANCETYPEID=" & Request.Form("sel_gsappliancetype") &_
	'         " ,MANUFACTURERID=" & Request.Form("sel_gsmanufacturer") &_
	'         " ,LOCATIONID=" & Request.Form("sel_gslocation") &_
	'         " ,MODEL='" & Request.Form("txt_model") & "'" &_
	'         " ,ORGID=" & Request.Form("sel_contractor_warranty") &_
	'         " ,DATEINSTALLED='" & Request.Form("txt_dateinstalled") & "' " &_
	'         " ,ISSUEDATE='" & Request.Form("txt_dateofissue") & "' " &_ 
	'         " ,CP12NUMBER='" & Request.Form("txt_cp12number") &"' " &_ 
	'         " ,WARRANTYEXPIRYDATE='" & Request.Form("txt_warrantyexpiryparts") & "'" &_
	'         " ,WARRANTYEXPIRYDATEPARTSANDLABOUR ='" & Request.Form("txt_warrantyexpirypartsandlabour") & "'" &_
	'      " WHERE PROPERTYID = '" & ID & "' " &_  
	'      " AND PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	Conn.Execute SQL
	Rebuild()
End Function


TheAction = Request("hid_Action")
'RESPONSE.WRITE "LKJFLKSJFLDSKFJSLDFKJSLDKFJSLDKFJLSDKFJLSDKFJSLDKFJSLDKFJSLDKFJSL"
OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head> <title></title></head>
<script language="javascript"  type="text/javascript">
function sendData(){
    if("<%=TheAction %>"=="LOAD")
    {
        parent.THISFORM.sel_gsappliancetype.value="<%=applianceid%>";
        parent.THISFORM.sel_gslocation.value="<%=locationid%>";
        parent.THISFORM.sel_gsmanufacturer.value="<%=manufacturer%>";
        parent.THISFORM.txt_model.value="<%=model%>";
        parent.THISFORM.sel_contractor_warranty.value="<%=contractorid%>";
        parent.THISFORM.txt_dateinstalled.value="<%=dateinstalled%>";
        parent.THISFORM.txt_warrantyexpiryparts.value="<%=warrantyexpirydate%>";
        parent.THISFORM.txt_warrantyexpirypartsandlabour.value="<%=warrantyexpirydatepartsandlabour%>";
        parent.THISFORM.txt_dateofissue.value="<%=issuedate%>"
        parent.THISFORM.txt_cp12number.value="<%=cp12number%>"
        parent.THISFORM.gsbutton.value="Amend"
        parent.THISFORM.gsbutton.onclick=function() {parent.AmendGS("<%=propertyapplianceid%>")}
     }
         else
    {
        if("<%=TheAction %>"=="AMEND")
        {
            parent.THISFORM.gsbutton.value="Save"    
            parent.THISFORM.gsbutton.onclick=function() {parent.ADDGS()}
        }
        parent.THISFORM.sel_gsappliancetype.value="";
        parent.THISFORM.sel_gslocation.value="";
        parent.THISFORM.sel_gsmanufacturer.value="";
        parent.THISFORM.txt_model.value="";
        parent.THISFORM.sel_contractor_warranty.value="";
        parent.THISFORM.txt_dateinstalled.value="";
        parent.THISFORM.txt_warrantyexpiryparts.value="";
        parent.THISFORM.txt_warrantyexpirypartsandlabour.value="";
        parent.THISFORM.txt_dateofissue.value=""
        parent.THISFORM.txt_cp12number.value=""
    }
	parent.AS.innerHTML = Reloader.innerHTML

}
</script>
<body onload="sendData()">
    <div id="Reloader">
        <table style='border-collapse:collapse; border-width:1px; width:100%;height:100%;'>
            <%=TableString%>
            <tr><td style="height:100%;" colspan="6"></td></tr>
        </table>
    </div>
</body>
</html>