<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, lst_item, lst_element, lst_repair, str_priority, RepairCost, wiElement, PropID, PopWindow, ExtraSQL
	Dim GROSSCOST, VAT, VATTYPE, EMPLOYEELIMIT, TOTAL_EXP_LEFT, EXPENDITUREID, TOTAL_CC_LEFT, COSTCENTREID
	
	int_source = Request("source")
	OpenDB()
	
	// apply filter to repairs if necessary
	iNature = Request("natureid")
	ExtraSQL = ""
	if (iNature = 2) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'D%' "
	elseif (iNature = 20) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'T%' "
	elseif (iNature = 21) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'M%' "
	elseif (iNature = 22) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'C%' "
	elseif (iNature = 34) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'K%' "
	elseif (iNature = 35) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'A%' "
	elseif (iNature = 36) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'F%' "
	elseif (iNature = 37) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'I%' "
	elseif (iNature = 38) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'S%' "
	elseif (iNature = 39) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'G%' "
	elseif (iNature = 40) then
		ExtraSQL = " AND I.MAINTENANCECODE LIKE 'R%' "
	end if

	// DETERMINE WHERE TO GO
	if int_source = 1 Then
		source_is_zone()
	elseif int_source = 2 Then
		source_is_item()
	elseif int_source = 3 Then
		check_warranty_list()
		source_is_element()
	else
		source_is_repair()
	end if
		
	CloseDB()
	
	Function check_warranty_list()
		if (Request("hid_PROPERTYID") <> "") then
			propid = Request("hid_PROPERTYID")
			wiElement = Request.form("sel_ELEMENT")
			if (wiElement = "") then wiElement = -1
			SQL = "SELECT * FROM P_WARRANTY WHERE EXPIRYDATE >= GETDATE() AND AREAITEM = " & wiElement & " AND PROPERTYID = '" & propid & "'"

			Call OpenRs(rsWar, SQL)
			if (NOT rsWar.EOF) then
				PopWindow = true
			else
				PopWindow = false
			end if
			CloseRs(rsWar)
		end if
	End Function
		
	Function source_is_zone()
		
		ilocation = Request.Form("sel_LOCATION")
		if (ilocation = "") then
			Call reset_select(lst_item, "sel_ITEM", "Please select a Location")
		else
			ftn_sql = 	"SELECT * FROM R_AREA WHERE AREAID IN " &_
							"(SELECT AREAID FROM R_ITEMDETAIL I " &_
							"WHERE I.DEFUNCT = 0 " & ExtraSQL & " AND I.ZONEID = " & iLocation & ") "

			Call build_select(ftn_sql, "'sel_ITEM' onchange='select_change(2)'", lst_item, "Items ")
		end if
		response.write ftn_sql
		Call reset_select(lst_element, "sel_ELEMENT", "Please select an Item")
		Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")
		
	End Function

	Function source_is_item()
		
		zoneid = Request.Form("sel_LOCATION")
		item_id = request.form("sel_ITEM")
		
		if (item_id = "") then
			Call reset_select(lst_element, "sel_ELEMENT", "Please select an Item")		
		else
			ftn_sql = 	"SELECT * FROM R_AREAITEM WHERE AREAITEMID IN " &_
							"(SELECT AREAITEMID FROM R_ITEMDETAIL I " &_
								" WHERE I.DEFUNCT = 0 " & ExtraSQL &_
									" AND I.ZONEID = " & zoneid & " AND I.AREAID = " & item_id & ")"
			
			Call build_select(ftn_sql, "'sel_ELEMENT' onchange='select_change(3)'", lst_element, "Elements ")
		end if
		
		Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")
		
	End Function
	
	Function source_is_element()
		
		Call GetCurrentYear()
		
		iElement = Request.Form("sel_ELEMENT")
		iNature = Request("natureid")
		if (iElement = "") then
			Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")			
		else
			ftn_sql = 	"SELECT 	I.ITEMDETAILID, " &_
						"			I.DESCRIPTION,  " &_
						"			P.DESCRIPTION  " &_
						"FROM 		R_ITEMDETAIL I " &_
						"			INNER JOIN F_EXPENDITURE EX ON I.R_EXP_ID = EX.EXPENDITUREID " &_
						"			INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID " &_
						"			INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE " &_
						"			LEFT JOIN R_PRIORITY P ON I.PRIORITY = P.PRIORITYID " &_
						"			WHERE EXA.ACTIVE = 1 AND EXA.FISCALYEAR = " & GetCurrent_YRange & " AND " &_
                        "           I.ZONEID = " & Request.form("sel_LOCATION") & " AND " &_
						"			I.AREAID = " & Request.form("sel_ITEM") & ExtraSQL & " AND I.AREAITEMID = " & iElement &_
						" ORDER		BY I.DESCRIPTION "
			//response.write ftn_sql
			Call build_select(ftn_sql, "'sel_REPAIR' onchange='select_change(4)'", lst_repair, "Repair Details ")
		
		end if
		
	End Function

	Function source_is_repair()
		Call GetCurrentYear()
			
		iRepair = Request.form("sel_REPAIR")
		if (iRepair <> "") then
			ftn_sql = 	"SELECT 	CC.COSTCENTREID, P.DESCRIPTION, ESTTIME, ISNULL(COST,0) AS REPAIRCOST, ISNULL(V.VATRATE,0) AS VATRATE, V.VATID, ISNULL(EL.LIMIT,0) AS EMLIMIT,  " &_
						"			ISNULL(EXA.EXPENDITUREALLOCATION,0) - ISNULL((SELECT SUM(GROSSCOST) AS TOTALSPENT FROM F_PURCHASEITEM WHERE EXPENDITUREID = I.R_EXP_ID AND ACTIVE = 1 AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "'),0) AS TOTAL_EXP_LEFT, EX.EXPENDITUREID, " &_
						"			ISNULL(CCA.COSTCENTREALLOCATION,0) - ISNULL((SELECT SUM(GROSSCOST) AS TOTALSPENT FROM F_PURCHASEITEM IPI INNER JOIN F_EXPENDITURE IEX ON IEX.EXPENDITUREID = IPI.EXPENDITUREID INNER JOIN F_HEAD IHE ON IHE.HEADID = IEX.HEADID WHERE IHE.COSTCENTREID = CC.COSTCENTREID AND ACTIVE = 1 AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "'),0) AS TOTAL_CC_LEFT  " &_			
						"FROM 		R_ITEMDETAIL I " &_
						"			INNER JOIN R_PRIORITY P ON I.PRIORITY = P.PRIORITYID AND " &_
						"			I.ITEMDETAILID = " & iRepair & " " &_
						"			LEFT JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = I.R_EXP_ID " &_
						"			LEFT JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND EXA.FISCALYEAR = " & GetCurrent_YRange & " " &_ 
						"			LEFT JOIN F_HEAD HE ON HE.HEADID = EX.HEADID " &_
						"			LEFT JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = HE.COSTCENTREID " &_
						"			LEFT JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.FISCALYEAR = " & GetCurrent_YRange & " " &_ 
						"			LEFT JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = I.R_EXP_ID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_
						"			LEFT JOIN F_VAT V ON I.VATTYPE = V.VATID " 						
						
			
			Call OpenRs(rsSet, ftn_sql)
			If Not rsSet.EOF Then 
				str_priority = rsSet(0) & " -- Est Time: " & rsSet("ESTTIME")
				RepairCost = FormatNumber(rsSet("REPAIRCOST"),2,-1,0,0)
				VATTYPE = rsSet("VATID")
				VATRATE = CDbl(rsSet("VATRATE"))
				VAT = FormatNumber( (RepairCost / 100 * VATRATE) ,2,-1,0,0)
				GROSSCOST = FormatNumber((CDBL(RepairCost) + CDBL(VAT)),2,-1,0,0)
				EMPLOYEELIMIT = FormatNumber(rsSet("EMLIMIT"),2,-1,0,0)
				TOTAL_EXP_LEFT = FormatNumber(rsSet("TOTAL_EXP_LEFT"),2,-1,0,0)
				EXPENDITUREID = rsSet("EXPENDITUREID")
				TOTAL_CC_LEFT = rsSet("TOTAL_CC_LEFT")
				COSTCENTREID = rsSet("COSTCENTREID")
			Else
				VATRATE = 0
				VATTYPE = "0"
				VAT = "0.00"
				GROSSCOST = "0.00"
				str_priority = "Not known"
				RepairCost = "0.00"
				EMPLOYEELIMIT = "0.00"
				TOTAL_EXP_LEFT = "0.00"
				EXPENDITUREID = 0
				TOTAL_CC_LEFT = "0.00"
				COSTCENTREID = 0				
			End If
			CloseRs(rsSet)
		else
			VATRATE = 0
			VATTYPE = "0"
			VAT = "0.00"
			GROSSCOST = "0.00"
			str_priority = "Not known"
			RepairCost = "0.00"
			EMPLOYEELIMIT = "0.00"
			TOTAL_EXP_LEFT = "0.00"
			EXPENDITUREID = 0
			TOTAL_CC_LEFT = "0.00"
			COSTCENTREID = 0							
		end if
		
	End Function
	
	Function build_select(ftn_sql, str_name, ByRef lst, txt_empty)
		
		Call OpenRs(rsSet, ftn_sql)
		
		lst = "<select name=" & str_name & " class='textbox200' style='width:300px'>"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		
		Wend
		
		CloseRs(rsSet)				
		lst = lst & "</select>" 
				
		If int_lst_record = 0 then
			lst = "<select name=" & str_name & " class='textbox200' style='width:300px' disabled><option value=''> No " & txt_empty & " found</option></select>"
		End If			

	End Function
	
	Function reset_select(ByRef lst, str_name, str_sentence)
	
		lst = "<select name='" & str_name & "' class='textbox200' style='width:300px' disabled><option value=''>" & str_sentence & "</option></select>"
	
	End Function

%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	if (<%=int_source%> == 1){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_NETCOST.value = "0.00";
		parent.RSLFORM.sel_ITEM.outerHTML = "<%=lst_item%>";
		parent.RSLFORM.sel_ELEMENT.outerHTML = "<%=lst_element%>";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		parent.RSLFORM.txt_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.txt_TOTALEXPLEFT.value = "0.00";					
		parent.RSLFORM.hid_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.hid_TOTALEXPLEFT.value = "0.00";
		parent.RSLFORM.hid_TOTALCCLEFT.value = "0.00";								
		}
	else if (<%=int_source%> == 2){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_NETCOST.value = "0.00";
		parent.RSLFORM.sel_ELEMENT.outerHTML = "<%=lst_element%>";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		parent.RSLFORM.txt_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.txt_TOTALEXPLEFT.value = "0.00";					
		parent.RSLFORM.hid_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.hid_TOTALEXPLEFT.value = "0.00";		
		parent.RSLFORM.hid_TOTALCCLEFT.value = "0.00";				
		}
	else if (<%=int_source%> == 3){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_NETCOST.value = "0.00";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		parent.RSLFORM.txt_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.txt_TOTALEXPLEFT.value = "0.00";					
		parent.RSLFORM.hid_EMPLOYEELIMIT.value = "0.00";	
		parent.RSLFORM.hid_TOTALEXPLEFT.value = "0.00";		
		parent.RSLFORM.hid_TOTALCCLEFT.value = "0.00";				
		<% if (PopWindow = true) then %>
		window.showModelessDialog("../Popups/WarrantyList.asp?PropID=<%=PropID%>&wiElement=<%=wiElement%>&TenancyID=<%=Request("hid_TENANCYID")%>&CustomerID=<%=Request("hid_CUSTOMERID")%>", "Warranties", "dialogHeight: 420px; dialogWidth: 420px; status: No; resizable: No;");
		<% end if %>
		}
	else {
		parent.RSLFORM.txt_PRIORITY.value = "<%=str_priority%>";
		parent.RSLFORM.txt_NETCOST.value = "<%=RepairCost%>";
		parent.RSLFORM.txt_VAT.value = "<%=VAT%>";
		parent.RSLFORM.sel_VATTYPE.value = "<%=VATTYPE%>";		
		parent.RSLFORM.txt_GROSSCOST.value = "<%=GROSSCOST%>";				
		parent.RSLFORM.DatabaseCost.value = "<%=GROSSCOST%>";		
		parent.RSLFORM.hid_EMPLOYEELIMIT.value = "<%=EMPLOYEELIMIT%>";				
		parent.RSLFORM.hid_EXPENDITUREID.value = "<%=EXPENDITUREID%>";								
		parent.RSLFORM.hid_COSTCENTREID.value = "<%=COSTCENTREID%>";										
		parent.SetLimits("<%=FormatNumber(TOTAL_EXP_LEFT,2,-1,0,0)%>", "<%=FormatNumber(TOTAL_CC_LEFT,2,-1,0,0)%>");
		parent.RSLFORM.txt_EMPLOYEELIMIT.value = "<%=FormatNumber(EMPLOYEELIMIT,2,-1,0,0)%>";				
		}
	}
</script>
<body onload="ReturnData()">
</body>
</html>