<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, page_source, sel_width

	int_source = Request("sel_ITEMACTIONID")
	If int_source = "" Then int_source = -1 End If
	page_source = Request("pagesource")

	' IF CALLED FROM POP UP SELECT WIDTH MUST BE SMALLER

	Call OpenDB()
	Call build_select()

	Function build_select()

		sql = "SELECT LETTERID, LETTERDESC FROM C_STANDARDLETTERS WHERE SUBITEMID = " & int_source
		Call OpenRs(rsSet, sql)

		lst = "<select name=""sel_LETTER"" id=""sel_LETTER"" class=""textbox200"">"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 then
			lst = "<select name=""sel_LETTER"" id=""sel_LETTER"" class=""textbox200"" disabled=""disabled""><option value=''> No associated letters found</option></select>"
		End If

	End Function
%>
<html>
<head>
    <title>RSLmanager > Get Letter Action</title>
    <script type="text/javascript" language="javascript">
        // Used in Portfolio Module > Popups > [pGasServiving.asp]
        // Used in Contractors Module > Popups > [pManageAppointment.asp]
        function ReturnData() {
            parent.document.getElementById("dvLetter").innerHTML = document.getElementById("dvLetter").innerHTML
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvLetter">
        <%=lst%></div>
</body>
</html>
