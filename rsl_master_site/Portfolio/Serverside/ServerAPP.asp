<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	
Dim ID
Dim DataFields   (24)
Dim DataTypes    (24)
Dim ElementTypes (24)
Dim FormValues   (24)
ReDim FormFields (24)
UpdateID  = "hid_PropertyID"
Dim TableString7
dim propertyApplianceId, applianceTypeId, locationId, manufactureId, model, fuelTypeId, dateInstalled, dateremoved
 
Function NewRecord ()
	
	ID = Request.Form(UpdateID)
	app_type= Request.Form("sel_app_gsappliancetype")
	fuel_type= Request.Form("lst_gs_app_fueltype") 
	loc = Request.Form("sel_app_gslocation")
	manufact= Request.Form("sel_app_gsmanufacturer")
	contractor= Request.Form("sel_app_contractor_warranty") 
	
	
	if app_type="" then app_type="NULL"
	if fuel_type="" then fuel_type="NULL"
	if loc="" then loc="NULL"
	if manufact="" then manufact="NULL"
	if contractor="" then contractor="NULL"

	SQL = "INSERT INTO GS_PROPERTY_APPLIANCE "&_
	      "        (PROPERTYID, "&_
          "         APPLIANCETYPEID, "&_
          "         MANUFACTURERID, "&_
          "         LOCATIONID, "&_
          "         MODEL, "&_
          "         ORGID, "&_
          "         DATEINSTALLED, "&_
          "         ISSUEDATE, "&_
          "         CP12NUMBER, "&_
          "         WARRANTYEXPIRYDATE, "&_
          "         WARRANTYEXPIRYDATEPARTSANDLABOUR, "&_
          "         CP12DOCUMENT, "&_
          "         CP12ISSUEDBY, "&_
          "         DOCUMENTTYPE, "&_
          "         NOTES, "&_
          "         MODELID, "&_
          "         FLUETYPE, "&_
          "         ApplianceFuelTypeId, "&_
          "         DATEREMOVED) " &_ 
	      " VALUES ('" & ID & "' ," &_ 
          "         " & app_type & " ," &_ 
          "         " & manufact & " ," &_ 
          "         " & loc & " ," &_ 
          "         '" & Request.Form("txt_app_model") & "' ," &_ 
          "         " & contractor & " ," &_ 
          "         '" & Request.Form("txt_app_dateinstalled") & "' ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & "NULL" & " ," &_ 
          "         " & fuel_type & " ," &_ 
          "         '" & Request.Form("txt_app_dateremoved")& "' )"
    
    Conn.Execute(SQL)
	Rebuild()
End Function

Function Rebuild()
    ID = Request.Form(UpdateID)
	SQL = "SELECT GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID, P_FUELTYPE.FUELTYPE, GS_APPLIANCE_TYPE.APPLIANCETYPE, GS_PROPERTY_APPLIANCE.DATEINSTALLED, GS_PROPERTY_APPLIANCE.DATEREMOVED FROM GS_PROPERTY_APPLIANCE INNER JOIN P_FUELTYPE on P_FUELTYPE.FuelTypeId = GS_PROPERTY_APPLIANCE.ApplianceFuelTypeId INNER JOIN GS_APPLIANCE_TYPE on GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID WHERE GS_PROPERTY_APPLIANCE.PROPERTYID = '" & ID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString7 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString7 = "<tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"		
		while NOT rsLoader.EOF
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("FUELTYPE") & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("APPLIANCETYPE") & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & Day(rsLoader("DATEINSTALLED")) & "/"& Month(rsLoader("DATEINSTALLED")) & "/" & Year(rsLoader("DATEINSTALLED")) & "</td>"
		    
		    if (rsLoader("DATEREMOVED") = "1/1/1900") then
		        TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & "N/A" & "</td>"
		    else
		        TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & Day(rsLoader("DATEREMOVED")) & "/"& Month(rsLoader("DATEREMOVED")) & "/" & Year(rsLoader("DATEREMOVED")) & "</td>"
		    end if
		    
		    ' The download link
		    TableString7 = TableString7 & "<td style='cursor:pointer' >" & "<img alt='' src='/myImages/paper_clip.gif'/>" & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""DELETEAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & "<font color=""red"">DEL</font>" & "</td>"
		    
			Counter = Counter + 1
			TableString7 = TableString7 & "</tr><tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"
			rsLoader.moveNext
		Wend
		Remainder = Counter Mod 3
		If (COUNTER = 0) Then
			TableString7 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
		End If
	End If
	Call CloseRs(rsLoader)
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)
	SQL = " DELETE FROM GS_PROPERTY_APPLIANCE "&_ 
	      " WHERE PROPERTYID = '" & ID & "'" &_
	      " AND PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	response.write SQL
	Conn.Execute (SQL)
	Rebuild()
End Function

Function AmendRecord()
    ID = Request.Form(UpdateID)
	app_type= Request.Form("sel_app_gsappliancetype")
	fuel_type= Request.Form("lst_gs_app_fueltype") 
	loc = Request.Form("sel_app_gslocation")
	manufact= Request.Form("sel_app_gsmanufacturer")
	contractor= Request.Form("sel_app_contractor_warranty") 
	dateRemoved = Request.Form("txt_app_dateremoved")
	
	
	if app_type="" then app_type="NULL"
	if fuel_type="" then fuel_type="NULL"
	if loc="" then loc="NULL"
	if manufact="" then manufact="NULL"
	if contractor="" then contractor="NULL"
	if dateRemoved="" then dateRemoved="NULL"

	SQL = "UPDATE GS_PROPERTY_APPLIANCE SET "&_
	      "         PROPERTYID = '" & ID & "', "&_
          "         APPLIANCETYPEID = " & app_type & ", "&_
          "         MANUFACTURERID = " & manufact & ", "&_
          "         LOCATIONID = " & loc & ", "&_
          "         MODEL = '" & Request.Form("txt_app_model") & "', "&_
          "         ORGID = " & contractor & ", "&_
          "         DATEINSTALLED = '" & Request.Form("txt_app_dateinstalled") & "' , "&_
          "         ApplianceFuelTypeId = " & fuel_type & ", "&_
          "         DATEREMOVED ='" & dateRemoved & "'" &_ 
	      "WHERE GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
   
    Conn.Execute(SQL)
    Rebuild()
	
End Function

Function LoadRecord()
	ID = Request.Form(UpdateID)
	
	SQL = "SELECT GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID, GS_PROPERTY_APPLIANCE.APPLIANCETYPEID, GS_PROPERTY_APPLIANCE.LOCATIONID, GS_PROPERTY_APPLIANCE.MANUFACTURERID, GS_PROPERTY_APPLIANCE.MODEL, GS_PROPERTY_APPLIANCE.ApplianceFuelTypeId, GS_PROPERTY_APPLIANCE.DATEINSTALLED, GS_PROPERTY_APPLIANCE.DATEREMOVED FROM GS_PROPERTY_APPLIANCE WHERE GS_PROPERTY_APPLIANCE.PROPERTYID = '" & ID & "' AND GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID = " & Request.Form("hid_DELETE") & " "
	      
	Call OpenRs(rsLoader,SQL)
	if (NOT rsLoader.EOF) then
	    propertyApplianceId = rsLoader("PROPERTYAPPLIANCEID")
	    applianceTypeId = rsLoader("APPLIANCETYPEID")
	    locationId = rsLoader("LOCATIONID")
	    manufactureId = rsLoader("MANUFACTURERID")
	    model = rsLoader("MODEL")
	    fuelTypeId = rsLoader("ApplianceFuelTypeId")
	    dateInstalled = rsLoader("DATEINSTALLED")
	    dateRemoved = rsLoader("DATEREMOVED")
	end if    
	CloseRs(rsLoader)
	Rebuild()
End Function


TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>

<script language="javascript" type="text/javascript">
    function sendData() {
        if ("<%=TheAction %>" == "LOAD") {
            parent.THISFORM.sel_app_gsappliancetype.value = "<%=applianceTypeId%>";
            parent.THISFORM.sel_app_gslocation.value = "<%=locationId%>";
            parent.THISFORM.sel_app_gsmanufacturer.value = "<%=manufactureId%>";
            parent.THISFORM.lst_gs_app_fueltype.value = "<%=fuelTypeId%>";

            parent.THISFORM.txt_app_model.value = "<%=model%>";
            parent.THISFORM.txt_app_dateinstalled.value = "<%=dateInstalled%>";
            parent.THISFORM.txt_app_dateremoved.value = "<%=dateRemoved%>";


            parent.THISFORM.appButton.value = "Amend"
            parent.THISFORM.appButton.onclick = function() { parent.AMENDAPP("<%=propertyApplianceId%>") }

        } else if ("<%=TheAction %>" == "AMEND") {

            parent.THISFORM.appButton.value = "Save"
            parent.THISFORM.appButton.onclick = function() { parent.ADDGS() }

            parent.THISFORM.sel_app_gsappliancetype.value = "";
            parent.THISFORM.sel_app_gslocation.value = "";
            parent.THISFORM.sel_app_gsmanufacturer.value = "";
            parent.THISFORM.lst_gs_app_fueltype.value = "";

            parent.THISFORM.txt_app_model.value = "";
            parent.THISFORM.txt_app_dateinstalled.value = "";
            parent.THISFORM.txt_app_dateremoved.value = "";
        }

        parent.GASAPPLAINCES.innerHTML = Reloader.innerHTML
    }
</script>

<body onload="sendData()">
    <div id="Reloader">
        <table style="height: 100%; border-collapse: collapse" border="1" width="100%">
            <tr style="border-bottom: #996699 1px solid; border-left-width: 0px;" bgcolor="#f5f5dc">
                <td>
                    <b>Appliance Fuel Type</b>
                </td>
                <td>
                    <b>Appliance Type</b>
                </td>
                <td>
                    <b>Date Installed</b>
                </td>
                <td>
                    <b>Date Removed</b>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%=TableString7%>
            <tr>
                <td height="100%" colspan="7">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
