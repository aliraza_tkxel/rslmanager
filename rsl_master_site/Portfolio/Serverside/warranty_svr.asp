<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID, LoaderString
	Dim FieldCounter
	FieldCounter = 5
	reDim DataFields   (FieldCounter)
	reDim DataTypes    (FieldCounter)
	reDim ElementTypes (FieldCounter)
	reDim FormValues   (FieldCounter)
	reDim FormFields   (FieldCounter)

	UpdateID	  = "hid_WARRANTYID"
	FormFields(0) = "sel_AREAITEM|SELECT"
	FormFields(1) = "txt_EXPIRYDATE|TEXT"
	FormFields(2) = "sel_WARRANTYTYPE|SELECT"
	FormFields(3) = "sel_CONTRACTOR|SELECT"
	FormFields(4) = "hid_PROPERTYID|TEXT"
	FormFields(5) = "txt_NOTES|TEXT"

	' ENTER NEW WARRANTY RECORD
	Function NewRecord ()
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO P_WARRANTY " & strSQL & ""
		Conn.Execute SQL, recaffected
	End Function

	' AMEND WARRANTY RECORD
	Function AmendRecord()
		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)
		SQL = "UPDATE P_WARRANTY " & strSQL & " WHERE WARRANTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected
	End Function

	Function DeleteRecord()
		ID = Request.Form(UpdateID)
		SQL = "DELETE FROM P_WARRANTY WHERE WARRANTYID = '" & ID & "'"
		Conn.Execute SQL, recaffected
	End Function

	' BUILD LOAD STRING
	Function LoadRecord()

		warrantyid = Request.Form("hid_WARRANTYID")

		SQL = "SELECT W.*, T.DESCRIPTION FROM P_WARRANTY W LEFT JOIN P_WARRANTYTYPE T ON W.WARRANTYTYPE = T.WARRANTYTYPEID WHERE WARRANTYID = " & warrantyid
		Call OpenRs(rsLoader, SQL)
		    LoaderString = ""
		    If (NOT rsLoader.EOF) Then
			    LoaderString = LoaderString & "parent.document.getElementById(""sel_WARRANTYTYPE"").value = """ & rsLoader("WARRANTYTYPE") & """;"
			    LoaderString = LoaderString & "parent.document.getElementById(""sel_AREAITEM"").value = """ & rsLoader("AREAITEM") & """;"
			    LoaderString = LoaderString & "parent.document.getElementById(""sel_CONTRACTOR"").value = """ & rsLoader("CONTRACTOR") & """;"
			    LoaderString = LoaderString & "parent.document.getElementById(""txt_NOTES"").value = """ & rsLoader("NOTES") & """;"
			    LoaderString = LoaderString & "parent.document.getElementById(""txt_EXPIRYDATE"").value = """ & rsLoader("EXPIRYDATE") & """;"
		    End If
		Call CloseRs(rsLoader)

	End Function

	TheAction = Request("hid_Action")
	property_id = Request.Form("hid_PROPERTYID")

	Call OpenDB()
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Portfolio Module</title>
    <script type="text/javascript" language="javascript">
        function ReturnData(){
	        if ("<%=TheAction%>" != "LOAD")
		        parent.location.href = "../Warranties.asp?PropertyID=<%=property_id%>"
	        else {
		        <%=LoaderString%>
		        parent.checkForm();
		        parent.SetButtons('AMEND');
		        }
        }
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
