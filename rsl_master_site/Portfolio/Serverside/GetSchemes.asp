<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull
		
	End Function
	
	ORGID = nulltest(Request.Form("sel_contractor_warranty"))
	PATCHID = nulltest(Request.Form("sel_PATCH"))
	
	Dim lstDevelopments,Developmentid

	OpenDB()

   set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GS_SCHEME_AVAILABLE_WO"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,ORGID)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,PATCHID)
	set rsSet = spUser.Execute
	
   
    if(not rsSet.eof) then
        While(not rsSet.eof)
                if(Developmentid="") then
                   Developmentid=rsSet("DEVELOPMENTID")
                else
                   Developmentid=Developmentid & "," & rsSet("DEVELOPMENTID") 
                end if   
        rsSet.movenext()
        wend
              Call BuildSelect(lstDEVELOPEMENT, "sel_SCHEME", "P_DEVELOPMENT WHERE DEVELOPMENTID IN(" & Developmentid & ")" , "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_DEVELOPMENT,null, "textbox200", "style='margin-left:25px;width:230;' OnChange='SchemeChange();'" )
    else
              'Call BuildSelect(lstDEVELOPEMNT, "sel_SCHEME", "P_DEVELOPMENT WHERE DEVELOPMENTID IN(-1)" , "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_DEVELOPMENT,null, "textbox200", "style='margin-left:25px;width:230;'  disabled='disabled' ")
             lstDEVELOPEMENT=" <select id=""sel_SCHEME"" name=""sel_SCHEME"" class=""textbox200"" style=""margin-left:24px;width:230px;"" > " &_
                             "   <option value="""">All</option> "&_
                             "  </select> "
	end if    

	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml"  >
<head><title>Scheme</title></head>
 <script language="javascript" type="text/javascript">
    function ReturnData(){
    if("<%=lstDEVELOPEMNT%>"!="")   
        parent.document.getElementById("sel_SCHEME").disabled = false;    
        parent.document.getElementById("sel_SCHEME").outerHTML = SCHEMEDATA.innerHTML;	   
	return;
}
</script>
    <body onload="ReturnData()">
        <div id="SCHEMEDATA"><%=lstDEVELOPEMENT%></div>
    </body>
</html>