<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = 116
		End If
		nulltest = str_isnull
		
	End Function
	
	TEAMID = nulltest(Request.Form("sel_TEAMS"))
    SQL = "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J  ON E.EMPLOYEEID = J.EMPLOYEEID " &_
	      " WHERE J.TEAM = " & TeamID & " AND J.ACTIVE = 1"

    Call OpenDB()    	
    Call OpenRs(rsEmployeeDetail, SQL)
    If(Not rsEmployeeDetail.eof) Then
        Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", null, NULL, "textbox200","onchange='GetEmployee_detail()'")
	End If
	Call CloseDB()		
%>
<html>
<head>
    <title>RSLmanager > Portfolio Module > Get Employee List</title>
</head>
<script language="javascript" type="text/javascript">
    function ReturnData() {
        if ("<%=lstUsers%>" != "")
            parent.document.getElementById("dvUsers").innerHTML = document.getElementById("dvUsers").innerHTML;
        return;
    }
</script>
<body onload="ReturnData()">
    <div id="dvUsers"><%=lstUsers%></div>
</body>
</html>
