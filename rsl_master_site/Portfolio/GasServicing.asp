<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<%
	Dim data_table,developmentid,localauthority,processdate,postcode,dbrs,dbcmd,norecords,count,formpost
	Dim patchid
	
    Call OpenDB()
    Call BuildSelect(lstContractor, "sel_contractor_warranty", "S_ORGANISATION WHERE ORGID IN (SELECT DISTINCT ORGID FROM S_SCOPE WHERE AREAOFWORK IN(5,6))", "ORGID, NAME", "NAME", "Please Select", Null, Null, "textbox200", " STYLE='margin-left:9px;width:230;' TABINDEX=11 OnChange=""GetPatch();"" ")	    
    Call CloseDB()
	data_table="There is no data to display"
%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Portfolio --> Gas Servicing </title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style  type="text/css" media="screen">
    #filter {border: gray solid 1;width:402px;display:inline;margin:0 0 0 10;padding:10 0 10 0;}
    #creatorder{padding:0 0 11 10;width:100px;display:inline;}
    #content{border: gray solid 1px;height:1px;margin:10 0 0 10;padding:5 5 5 5;width:100%;}
    h1 {font-size:14px;margin:5px 10px 10px 10px;font-weight: bold;}
    .lable {font-weight: bold;}
    .other_filter{margin-left:8;padding-top:0;padding-bottom:0;margin-top:6px;margin-bottom:6px;}
    .button{padding-left:8px;display:inline;}
    .thinline{border-style: solid;border-color:gray;border-width:0 0 1px 0; padding:0 0 0 0; margin:0 0 0 0;}
</style>

</head>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
  function print_data()
  {
    document.getElementById("content").focus();
    window.print();
  }
  
  function GetSchemes()
  {
  
    document.getElementById("content").innerHTML="Please use the find buton to search the data for new filter"
	RSLFORM.sel_SCHEME.disabled = true;
	RSLFORM.btn_createorder.disabled=true;
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetSchemes.asp"
	RSLFORM.submit()
  }
  
  function GetPatch()
  {
	document.getElementById("content").innerHTML="Please use the find buton to search the data for new filter"
	RSLFORM.sel_PATCH.disabled = true;
	RSLFORM.btn_createorder.disabled=true;
	RSLFORM.sel_SCHEME.disabled = true;
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetPatch.asp"
	RSLFORM.submit()
   }
  
  function SchemeChange()
  {
    document.getElementById("content").innerHTML="Please use the find buton to search the data for new filter"
    RSLFORM.btn_createorder.disabled=true;
  }
  
  function Filter()
  {
	if(RSLFORM.sel_contractor_warranty.value=="")
	{
	    alert("Please select the contractor from the drop down");
	    return;
	}    
	if(RSLFORM.sel_PATCH.value=="")
	{
	    alert("Please select the patch from the drop down");
	    return;
	}    
	
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GasServicing_svr.asp"
	RSLFORM.submit()
	
	    
   }
  
   function LoadDetail(Patchid){
			
		
		var ColumnCounter,doc
		ColumnCounter=1
		doc = document.all;
		
		if (doc["ChildLine" + Patchid + 0].style.display == "none")
		{
	       newStatus = "block";
	       doc["Parent" + Patchid].attributes("src").value="images/minus.gif"
	       
	    }   
        else
        {
	       newStatus = "none";
	       doc["Parent" + Patchid].attributes("src").value="images/plus.gif"
	      
	    }   
	            
	        
        for (i=0; i<=ColumnCounter; i++)
        {
            if(ElementExist("ChildLine" + Patchid + i)==true && document.getElementById("ChildLine" + Patchid + i).value!="")
	          {
	            doc["ChildLine" + Patchid + i].style.display = newStatus
	            ColumnCounter=ColumnCounter+1;
	          }  
	        else
	         break;
		}
	}  
  
   function ElementExist(theVal)
	{
	    if (document.getElementById(theVal) != null) {
	        return true;
	    } 
	    else {
	     return false;
	  }

	}   
	
	function CreateOrder()
	{
	    var querystring;

        if(RSLFORM.sel_contractor_warranty.value==null){
            
            alert("Please select the contractor for which the work order is to be generated.");
            return;
        }    
        else
        {
            querystring="?orgid="+RSLFORM.sel_contractor_warranty.value;
            
            if(RSLFORM.sel_PATCH.value!="") 
            {
               querystring=querystring +"&patchid=" + RSLFORM.sel_PATCH.value;
            }
            
            if(RSLFORM.sel_SCHEME.value!="")
            {
                querystring=querystring + "&developmentid=" + RSLFORM.sel_SCHEME.value;
            }
            RSLFORM.btn_createorder.disabled=true;
            window.open("PopUps/GSWorkOrder.asp"+querystring+"","display","width=900px,height=520px,left=180,top=120"); 
        }	    
	    
	   
	}
 
 </script>
<body bgcolor="#FFFFFF" onLoad="initSwipeMenu(3);" onUnload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
<form name = "RSLFORM" method="post">
    <h1>Gas Servicing </h1>
    <div id="filer_createorder">
        <div  id="filter">
            <p class="other_filter">
                <label class="lable"> Contractor:</label>
                <%=lstContractor %>
            </p>
            <p class="other_filter">
                <label class="lable"> Patch:</label>
                <select id="sel_PATCH" class="textbox200" style='margin-left:37px;width:230;' disabled="disabled">
                    <option>Please Select</option>
                </select>
            </p>
            <p class="other_filter" style="display:inline;">
                  <label class="lable"> Scheme:</label>
                  <select id="sel_SCHEME" class="textbox200" style='margin-left:24px;width:230;' disabled="disabled">
                    <option>Please Select</option>
                  </select>
            </p>
            <p class="button" id="findbtn"><input type="button"  name="btn_filter" value="FIND" class="RSLButton" style="width:65px;" onClick="Filter()" /> </p>
       </div>
       <div id="creatorder">
            <p class="button"><input type="button"  name="btn_createorder" value="CREATE ORDER" class="RSLButton"  onclick="CreateOrder();" disabled="disabled" /> </p>
       </div>
    </div>
    <div id="content" >
           <%=data_table%>
           
    </div>
 </form>
 <table><tr><td></td></tr></table>

<!--#include virtual="Includes/Bottoms/BodyBottom_portfolioreport.asp" -->
<iframe src="/secureframe.asp" name="ServerFrame" height="600" width="600" style="display:none"></iframe>
</body>
</html>