<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	property_id = Request("propertyid")
	HISTORYTYPE = Request("sel_HISTORYTYPE")
	OpenDB()
	
	If not HISTORYTYPE <> "" then HISTORYTYPE = "1" End If
	
	''''''''''
	' If the option is tenancy history or is intial opening of page
	''''''''''
	If HISTORYTYPE = "1" then
		SQL = "select ct.startdate, CT.enddate , ct.customerid, ct.tenancyid, tl.description + ' ' + c.firstname + ' ' + c.lastname as fullname from c_customertenancy ct " &_
				" inner join c_tenancy t on  t.tenancyid = ct.tenancyid " &_
				" inner join c__customer c on c.customerid = ct.customerid " &_
				" left join g_title tl on tl.titleid = c.title " &_
				" where ct.tenancyid in (select tenancyid from c_tenancy where propertyid = '" & property_id & "')" &_
				" order by ct.startdate desc"
	
	'response.write SQL
		Call OpenRs(rsLoader, SQL)
		
		if (NOT rsLoader.EOF) then
		
		Dim enddate
			WHILE NOT rsLoader.EOF
			enddate = rsLoader("enddate")
				if ( rsLoader("enddate") <> "") then
					Pic = "<img src='/Customer/Images/FT.gif'  border=0 title='Previous Tenancy'>"
				else
					Pic = "<img src='/Customer/Images/T.gif'  border=0 title='Tenancy On going'>"
					enddate = "On going"
				end if			
				pmstring = pmstring &_
							"<TR>" &_
								"<TD width=12 nowrap>" & Pic & "</TD>" &_
								"<TD width=105 nowrap>" & rsLoader("startdate") & "</TD>" &_
								"<TD width=115 nowrap>" & enddate & "</TD>" &_
								"<TD width=256 nowrap>" & rsLoader("fullname") & " (<a href='/Customer/CRM.asp?CustomerID=" & rsLoader("customerid") & "' target='_parent'>"  & rsLoader("customerid") & "</a>)</TD>" &_
								"<TD width=212>" & tenancyreference(rsLoader("tenancyid")) & "</TD>" &_
								"</TR>"						
				rsLoader.MoveNext
			wend
			
		else
			pmstring = pmstring & "<Tr><TD colspan=5 align=center>No Tenancy History is available for this Property</TD></TR>" 
		end if
	End If
	
	''''''''''
	' If the option is rent history
	''''''''''
	If HISTORYTYPE = "2" then
				
	SQL = 		"SELECT " &_
							"FH.RENT, FH.SERVICES, FH.COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, FH.INELIGSERV , FH.SUPPORTEDSERVICES, FH.WATERRATES, "&_
							"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, TC.description as therenttype "&_
					"FROM P_FINANCIAL FH "&_
							"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
							"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
							"	LEFT JOIN C_TENANCYTYPE TC ON TC.TENANCYTYPEID = fh.renttype " &_
					"WHERE PROPERTYID = '" & property_id & "' "	&_	
			"union SELECT " &_
							"FH.RENT, FH.SERVICES, FH.COUNCILTAX, FH.DATERENTSET, FH.RENTEFFECTIVE, FH.INELIGSERV, FH.SUPPORTEDSERVICES, FH.WATERRATES,"&_
							"FH.TARGETRENT, TL.DESCRIPTION + ' ' + E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME,  TC.description as therenttype "&_
					"FROM P_FINANCIAL_HISTORY FH "&_
							"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = FH.PFUSERID "&_
							"	LEFT JOIN G_TITLE TL ON TL.TITLEID = E.TITLE "&_
							"	LEFT JOIN C_TENANCYTYPE TC ON TC.TENANCYTYPEID = fh.renttype " &_
					"WHERE PROPERTYID = '" & property_id & "' ORDER BY DATERENTSET"
		
		'response.write SQL
		Call OpenRs(rsLoader, SQL)
		
		if (NOT rsLoader.EOF) then
		
			WHILE NOT rsLoader.EOF
	
'					"<table>" &_
				pmstring = pmstring &_
						"<tr>" &_
							 "<TD HEIGHT=20PX width=2 nowrap ></TD>" &_
							 "<TD width=75 nowrap >" & rsLoader("daterentset") & "</TD>" &_
							 "<TD width=80 nowrap >" & rsLoader("renteffective") & "</TD>" &_
							  "<TD width=90	 nowrap >" & rsLoader("therenttype") & "</TD>" &_
							 "<TD width=70 >�" & rsLoader("services") & "</TD>" &_
							 "<TD width=77 >�" & rsLoader("counciltax") & "</TD>" &_
							 "<TD width=50 >�" & rsLoader("WATERRATES") & "</TD>" &_
							  "<TD width=50 >�" & rsLoader("INELIGSERV") & "</TD>" &_
							 "<TD width=65>�" & rsLoader("SUPPORTEDSERVICES") & "</TD>" &_
							 "<TD width=58><b>�" & rsLoader("rent") & "</b></TD>" &_
							 "<TD width=>�" & rsLoader("targetrent") & "</TD>" &_
							"<TD width=>�" & rsLoader("FULLNAME") & "</TD>" &_
						"</tr>" &_
				rsLoader.MoveNext
'					"</table>"
			wend
				
		else
			pmstring = pmstring & "<Tr><TD colspan=7 align=center valign=top>No Rent History is available for this Property</TD></TR>" 
		end if
	End If

	CloseRs(rsLoader)
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

		function changeHistoryType(){

			RSLFORM.target = ""
			RSLFORM.action = "iPropertyHistory.asp"
			RSLFORM.submit()		
		}
		
		function Go (){
		
		document.location.href = "iPropertyHistoryGraph.asp?propertyid=<%=property_id%>"
		}

</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 height=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
  <FORM NAME=RSLFORM METHOD=POST>
    <TR> 
      <TD height=1 valign="top"> 
        <TABLE cellspacing=0 cellpadding=3 border=0 style='border-top:1px solid #133E71;border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=100% >
          <TR style='color:#133e71' height=1px> 
            <TD nowrap bgcolor=beige style='border-bottom:1px solid silver'>
			
			<%' this is the start of the top strip table with dropdown in %>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
							  <tr> 
								<td height=1PX width=7 nowrap bgcolor="">&nbsp;</td>
								<% If  HISTORYTYPE = "2" then %>
								
                  <td width=85 nowrap align="left"> 
                    <input type="button" name="Submit" value="View Chart" onclick="Go()" class="rslbutton">
                    <% End If 	%>
								</td>
								
                  <td nowrap align="right" width=""> Choose History Type: 
                    <input type="hidden" name="propertyid" value="<%=Request("propertyid")%>">
								  <select name='sel_HISTORYTYPE' class='textbox200' onChange="changeHistoryType()">
									<option value='1'>Please Select</option>
									<option value='1'>Tenancy History</option>
									<option value='2'>Rent History</option>
								  </select>
								</td>
							  </tr>
							  <tr> 
								<td height=5 colspan="3" nowrap></td>
							  </tr>
							  <tr>
								<td height=1PX colspan="3" nowrap ></td>
							  </tr>
			  </table>
				<%' this is the end of the top strip table with dropdown in %>
<% If  NOT HISTORYTYPE = "2" then %>
                    
			  <table cellpadding="0" cellspacing="0" border="0" width="721" height="18">
                <tr style='border-left:1px solid #133E71;border-bottom:1px solid #133E71'> 
                        
                  <TD width=24 nowrap bgcolor="beige"></TD>
                        
                  <TD width=106 nowrap bgcolor="beige"><b>Start Date</b></TD>
                        
                  <TD width=119 nowrap bgcolor="beige"><b>End Date</b></TD>
                        
                  <TD width=262 nowrap bgcolor="beige"><b>Customer</b></TD>
                        
                  <TD width=210 bgcolor="beige"><b>Tenancy Reference</b></TD>
                </tr>
            </table>
<% ELSE %>
                    
			  <table cellpadding="0" cellspacing="0" border="0" width="722">
                <tr> 
                        
                  <TD width=9 nowrap bgcolor="beige"></TD>
                        
                  <TD width=77 nowrap bgcolor="beige"><b>Date Set</b></TD>
                        
                  <TD width=87 nowrap bgcolor="beige"><b>Effective</b></TD>
                        
                  <TD width=95 nowrap bgcolor="beige"><b>Tenancy Type</b></TD>
                        
                  <TD width=70 bgcolor="beige"><b>Services</b></TD>
                        
                  <TD width=80 bgcolor="beige"><b>Council Tax</b></TD>
				  	
				  <TD width=50 bgcolor="beige"><b>Water</b></TD>
                        
                  <TD width=60 bgcolor="beige"><strong>Inelig.<br> Serv</strong></TD>
				  
                  <TD width=63 bgcolor="beige"><strong>Supt. <br> Serv </strong></TD>
                 
				  <TD width=56 bgcolor="beige" height="18"><strong>Rent</strong></TD>
				  
				  <TD width=65 bgcolor="beige"><b>Target</b></TD>

				  <TD width=65 bgcolor="beige"><b>By</b></TD>
				  
                </tr>
            </table>
<% END IF %>			
			</TD>
          </TR>
		   <tr> 
            <td ><div style="width:700px; height:116px; overflow: auto;"><table><%=pmstring%></table></div>
            </td>
          </tr>        

        </TABLE>
      </td>
    </TR>
  </FORM>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	