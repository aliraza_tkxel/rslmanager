<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="/ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION, PropertyID

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then
	    PropertyID = -1
	    ACTION = "NEW"
	Else
	    ACTION = "AMEND"
	End If

	f_renteffective = "31/03/2005"
	f_daterentset = "01/04/2004"

  SQL = " SELECT TOP 1 *, DATEADD(DD,-1,DATEADD(YY,1,REASSESMENTDATE)) AS RENTEFFECTIVE, " &_
		" (RENT+SERVICES+COUNCILTAX+WATERRATES+INELIGSERV+SUPPORTEDSERVICES) AS TOTALRENT " &_
		" FROM P_TARGETRENT T " &_
		"	INNER JOIN P_RENTINCREASE_FILES F ON F.FILEID = T.FILEID " &_
		" WHERE PROPERTYID = '" & PropertyID & "' AND REASSESMENTDATE > GETDATE() ORDER BY TARGETRENTID DESC"
	Call OpenDB()
    Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		f_fileuploaded = rsLoader("UPLOADDATE")
		f_renteffective = rsLoader("RENTEFFECTIVE")
		If isNull(f_renteffective) or f_renteffective = "" Then f_renteffective = "31/03/2005" End If
		f_daterentset = rsLoader("REASSESMENTDATE")
		If isNull(f_daterentset) Then f_daterentset = "01/04/2004" End If
		f_rent = rsLoader("RENT")
		If isNull(f_rent) Then f_rent = "0.00" End If
		f_counciltax = rsLoader("COUNCILTAX")
		If isNull(f_counciltax) Then f_counciltax = "0.00" End If
		f_services = rsLoader("SERVICES")
		If isNull(f_services) Then f_services = "0.00" End If
		f_water = rsLoader("WATERRATES")
		If isNull(f_water) Then f_water = "0.00" End If
		f_support = rsLoader("SUPPORTEDSERVICES")
		If isNull(f_support) Then f_support = "0.00" End If
		f_inelig = rsLoader("INELIGSERV")
		If isNull(f_inelig) Then f_inelig = "0.00" End If
		'f_garage = rsLoader("GARAGE")
		'If isNull(f_garage) Then f_garage = "0.00" End If
		f_totalrent = rsLoader("TOTALRENT")
		If isNull(f_totalrent) Then f_totalrent = "0.00" End If
	End If
    Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio--&gt; Financial</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        .style3
        {
            color: #FFFFFF;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table width="283" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="iFinancial_CurrentUpdate.asp?PropertyID=<%=PropertyID%>">
                    <img name="tab_main_details" src="../images/financial/tab_current_01.gif" border="0"
                        alt="Current" /></a></td>
            <td rowspan="2">
                <a href="iFinancial_TargetUpdate.asp?PropertyID=<%=PropertyID%>">
                    <img name="tab_financial" src="../images/financial/tab_target_02.gif" border="0"
                        alt="Target" /></a></td>
            <td rowspan="2">
                <img name="tab_attributes" src="../images/financial/tab_annrent_03.gif" border="0"
                    alt="Annual Rent" /></td>
            <td>
                <img src="/myImages/spacer.gif" width="132" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="../images/spacer.gif" width="132" height="1" alt="" /></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td height="37" colspan="3" valign="top" nowrap="nowrap">
                <table width="360">
                    <tr>
                        <td width="341">
                            <table width="353" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td>
                                        File Uploaded:
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_FILEUPLOADED" id="txt_FILEUPLOADED" tabindex="3" class="textbox200"
                                            value="<%=f_fileuploaded %>" />
                                        <img src="/js/FVS.gif" name="img_FILEUPLOADED" id="img_FILEUPLOADED" width="15px"
                                            height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date Rent Set:
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_DATERENTSET" id="txt_DATERENTSET" tabindex="3" class="textbox200"
                                            value="<%=f_daterentset%>" />
                                        <img src="/js/FVS.gif" name="img_DATERENTSET" id="img_DATERENTSET" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rent Effective to :
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_RENTEFFECTIVE" id="txt_RENTEFFECTIVE" tabindex="4" class="textbox200"
                                            value="<%=f_renteffective%>" />
                                        <img src="/js/FVS.gif" name="img_RENTEFFECTIVE" id="img_RENTEFFECTIVE" width="15px"
                                            height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="101">
                                        Rent
                                    </td>
                                    <td width="87">
                                        <input onblur="calc_rent()" tabindex="9" type="text" name="txt_RENT" id="txt_RENT"
                                            class="textbox100" style="width: 60px" value="<%=FormatNumber(f_rent)%>" />
                                        <img src="/js/FVS.gif" name="img_RENT" id="img_RENT" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                    <td width="65">
                                        Services
                                    </td>
                                    <td width="80">
                                        <input onblur="calc_rent()" type="text" tabindex="10" name="txt_SERVICES" id="txt_SERVICES"
                                            class="textbox100" style="width: 60px" value="<%=FormatNumber(f_services)%>" />
                                        <img src="/js/FVS.gif" name="img_SERVICES" id="img_SERVICES" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Council Tax
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_COUNCILTAX" id="txt_COUNCILTAX"
                                            tabindex="11" class="textbox100" style="width: 60px" value="<%=FormatNumber(f_counciltax)%>" />
                                        <img src="/js/FVS.gif" name="img_COUNCILTAX" id="img_COUNCILTAX" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td>
                                        Water
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_WATERRATES" id="txt_WATERRATES"
                                            tabindex="12" class="textbox100" style="width: 60px" value="<%=FormatNumber(f_water)%>" />
                                        <img src="/js/FVS.gif" name="img_WATERRATES" id="img_WATERRATES" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inelig.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_INELIGSERV" id="txt_INELIGSERV"
                                            tabindex="13" class="textbox100" style="width: 60px" value="<%=FormatNumber(f_inelig)%>" />
                                        <img src="/js/FVS.gif" name="img_INELIGSERV" id="img_INELIGSERV" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td>
                                        Supp.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_SUPPORTEDSERVICES" id="txt_SUPPORTEDSERVICES"
                                            tabindex="14" class="textbox100" style="width: 60px" value="<%=FormatNumber(f_support)%>" />
                                        <img src="/js/FVS.gif" name="img_SUPPORTEDSERVICES" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr style="display:none">
                                    <td>
                                        Garage.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_GARAGE" id="txt_GARAGE" tabindex="13"
                                            class="textbox100" style="width: 60px" value="<%'=FormatNumber(f_garage)%>" />
                                        <img src="/js/FVS.gif" name="img_GARAGE" id="img_GARAGE" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr bgcolor="#133E71">
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr bgcolor="#040455">
                                                <td width="105" bgcolor="#133E71">
                                                    <span class="style3">Total Rent</span>
                                                </td>
                                                <td bgcolor="#133E71">
                                                    <input type="text" name="txt_TOTALRENT" id="txt_TOTALRENT" class="textbox100" style="width: 60px"
                                                        readonly="readonly" value="<%=FormatNumber(f_totalrent)%>" />
                                                    <img src="/js/FVS.gif" name="img_TOTALRENT" id="img_TOTALRENT" width="15px" height="15px"
                                                        border="0" alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
</body>
</html>
