<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="/ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION, lstfundingauthority, lstrenttype, PropertyID, l_address1, f_targetrentset, isChecked

	PropertyID = Request("PropertyID")

    If (PropertyID = "") Then
	    PropertyID = -1
	    ACTION = "NEW"
	Else
	    ACTION = "AMEND"
	End If

	'--Retrieve First Line of Address
	SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenDB()
    Call OpenRs(rsAddress, SQL)
	    If (NOT rsAddress.EOF) Then
		    l_address1 = rsAddress("ADDRESS1")
	    Else
		    l_address1 = "[None Given]"
	    End If
	    l_housenumber = rsAddress("HOUSENUMBER")
	    l_propertyid = UCASE(rsAddress("PROPERTYID"))
	    l_propertystatus = l_propertyid
	Call CloseRs(rsAddress)
	'--Retrieve First Line of Address

	f_renteffective = "31/03/2005"
	f_daterentset = "01/04/2004"

	SQL = "SELECT * FROM P_FINANCIAL WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		f_targetrentset = rsLoader("TARGETRENTSET")  'This is when a property becomes available, to keep in line with rent targets
		F_nominatingbody = rsLoader("NOMINATINGBODY")
		f_renttype = rsLoader("RENTTYPE")
		f_renteffective = rsLoader("RENTEFFECTIVE")
		If isNull(f_renteffective) or f_renteffective = "" Then f_renteffective = "31/03/2005" End If
		f_targetrent = rsLoader("TARGETRENT")
		If isNull(f_targetrent) Then f_targetrent = FormatNumber(0.00) End If
		f_daterentset = rsLoader("DATERENTSET")
		If isNull(f_daterentset) Then f_daterentset = "01/04/2004" End If
		f_fundingauthority = rsLoader("FUNDINGAUTHORITY")
		f_charge = rsLoader("CHARGE")
		f_chargevalue = rsLoader("CHARGEVALUE")	
		If isNull(f_chargevalue) Then f_chargevalue = "0.00" End If
		f_rent = rsLoader("RENT")
		If isNull(f_rent) Then f_rent = "0.00" End If
		f_counciltax = rsLoader("COUNCILTAX")
		If isNull(f_counciltax) Then f_counciltax = "0.00" End If
		f_services = rsLoader("SERVICES")
		If isNull(f_services) Then f_services = "0.00" End If
		f_water = rsLoader("WATERRATES")
		If isNull(f_water) Then f_water = "0.00" End If
		f_support = rsLoader("SUPPORTEDSERVICES")
		If isNull(f_support) Then f_support = "0.00" End If
		f_inelig = rsLoader("INELIGSERV")
		If isNull(f_inelig) Then f_inelig = "0.00" End If
		f_totalrent = rsLoader("TOTALRENT")
		If isNull(f_totalrent) Then f_totalrent = "0.00" End If
		f_garage = rsLoader("GARAGE")
		If isNull(f_garage) Then f_garage = "0.00" End If
		'valuation boxes
		f_omvst = rsLoader("OMVST")
		If isNull(f_omvst) Then f_omvst = "0.00" End If
		f_euv = rsLoader("EUV")
		If isNull(f_euv) Then f_euv = "0.00" End If
		f_insurancevalue = rsLoader("INSURANCEVALUE")
		If isNull(f_insurancevalue) Then f_insurancevalue = FormatNumber(0.00) End If
		f_omv = rsLoader("OMV")
		If isNull(f_omv) Then f_omv = "0.00" End If
	End If
    Call CloseRs(rsLoader)

	Call BuildSelect(lstfundingauthority, "sel_FUNDINGAUTHORITY", "P_FUNDINGAUTHORITY", "FUNDINGAUTHORITYID, DESCRIPTION", "DESCRIPTION", "Please Select", f_fundingauthority, NULL, "textbox", "  tabindex='8' ")
	Call BuildSelect(lstrenttype, "sel_RENTTYPE", "C_TENANCYTYPE", "TENANCYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", f_renttype, NULL, "textbox", "STYLE='WIDTH:200PX'  tabindex='2' ")
	Call BuildSelect(lstcharge, "sel_CHARGE", "P_CHARGE", "CHARGEID, DESCRIPTION", "DESCRIPTION", "Please Select", f_charge, NULL, "textbox", "STYLE='WIDTH:200PX'  tabindex='2' ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio--&gt; Financial</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        .style3
        {
            color: #FFFFFF;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_DATERENTSET|Date Rent Set|DATE|N"
        FormFields[1] = "txt_RENTEFFECTIVE|Rent Effective From|DATE|N"
        FormFields[2] = "txt_TOTALRENT|Total Rent|TEXT|N"
        FormFields[3] = "txt_RENT|Rent|CURRENCY|Y"
        FormFields[4] = "txt_SERVICES|Services|CURRENCY|Y"
        FormFields[5] = "txt_COUNCILTAX|Council Tax|CURRENCY|Y"
        FormFields[6] = "txt_WATERRATES|Water Rates|CURRENCY|Y"
        FormFields[7] = "txt_INELIGSERV|Ineligible Services|CURRENCY|Y"
        FormFields[8] = "txt_SUPPORTEDSERVICES|Supported Services|CURRENCY|Y"
        FormFields[9] = "sel_RENTTYPE|Rent type|SELECT|Y"
        FormFields[10] = "txt_GARAGE|Garage|FLOAT|Y|2"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.target = "";
            document.RSLFORM.action = "../ServerSide_iVuso/financial_Current_svr.asp"
            document.RSLFORM.submit()
        }

        function calc_rent() {
            if (!checkForm()) return false;
            var rent, services, counciltax, water, inelgserv, supp, totalrent
            rent = parseFloat(document.getElementById("txt_RENT").value);
            services = parseFloat(document.getElementById("txt_SERVICES").value);
            counciltax = parseFloat(document.getElementById("txt_COUNCILTAX").value);
            water = parseFloat(document.getElementById("txt_WATERRATES").value);
            inelgserv = parseFloat(document.getElementById("txt_INELIGSERV").value);
            supp = parseFloat(document.getElementById("txt_SUPPORTEDSERVICES").value);
            garage = parseFloat(document.getElementById("txt_GARAGE").value);
            totalrent = rent + services + counciltax + water + inelgserv + supp + garage
            document.getElementById("txt_TOTALRENT").value = FormatCurrency(totalrent);
        }

    </script>
</head>
<body>
    <form name="RSLFORM" method="post" action="">
    <table width="283" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_main_details" src="../images/financial/tab_current_02.gif" border="0" alt="" /></td>
            <td rowspan="2">
                <a href="iFinancial_TargetUpdate.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_financial" src="../images/financial/tab_target_01.gif" border="0" alt="" /></a></td>
            <td rowspan="2">
                <a href="iFinancial_AnnualUpdate.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img name="tab_attributes" src="../images/financial/tab_annrent_01.gif" border="0" alt="" /></a></td>
            <td>
                <img src="/myImages/spacer.gif" width="132" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="../images/spacer.gif" width="132" height="1" alt="" /></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style='height: 7px'>
            <td>
            </td>
        </tr>
        <tr>
            <td height="37" colspan="3" valign="top" nowrap="nowrap">
                <table width="360">
                    <tr>
                        <td width="341">
                            <table width="353">
                                <tr>
                                    <td>
                                        Date Rent Set:
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_DATERENTSET" id="txt_DATERENTSET" tabindex="3" class="textbox200" value="<%=f_daterentset%>" />
                                        <img src="/js/FVS.gif" name="img_DATERENTSET" id="img_DATERENTSET" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rent Effective to :
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_RENTEFFECTIVE" id="txt_RENTEFFECTIVE" tabindex="4" class="textbox200" value="<%=f_renteffective%>" />
                                        <img src="/js/FVS.gif" name="img_RENTEFFECTIVE" id="img_RENTEFFECTIVE" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rent Type
                                    </td>
                                    <td colspan="3">
                                        <%=lstrenttype%>
                                        <img src="/js/FVS.gif" name="img_RENTTYPE" id="img_RENTTYPE" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="101">
                                        Rent
                                    </td>
                                    <td width="87">
                                        <input onblur="calc_rent()" tabindex="9" type="text" name="txt_RENT" id="txt_RENT" class="textbox100"
                                            style="width: 60px" value="<%=FormatNumber(f_rent)%>" />
                                        <img src="/js/FVS.gif" name="img_RENT" id="img_RENT" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td width="65">
                                        Services
                                    </td>
                                    <td width="80">
                                        <input onblur="calc_rent()" type="text" tabindex="10" name="txt_SERVICES" id="txt_SERVICES" class="textbox100"
                                            style="width: 60px" value="<%=FormatNumber(f_services)%>" />
                                        <img src="/js/FVS.gif" name="img_SERVICES" id="img_SERVICES" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Council Tax
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_COUNCILTAX" id="txt_COUNCILTAX" tabindex="11" class="textbox100"
                                            style="width: 60px" value="<%=FormatNumber(f_counciltax)%>" />
                                        <img src="/js/FVS.gif" name="img_COUNCILTAX" id="img_COUNCILTAX" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                    <td>
                                        Water
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_WATERRATES" id="txt_WATERRATES" tabindex="12" class="textbox100"
                                            style='width: 60PX' value="<%=FormatNumber(f_water)%>" />
                                        <img src="/js/FVS.gif" name="img_WATERRATES" id="img_WATERRATES" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inelig.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_INELIGSERV" id="txt_INELIGSERV" tabindex="13" class="textbox100"
                                            style="width: 60px" value="<%=FormatNumber(f_inelig)%>" />
                                        <img src="/js/FVS.gif" name="img_INELIGSERV" id="img_INELIGSERV" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                    <td>
                                        Supp.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_SUPPORTEDSERVICES" id="txt_SUPPORTEDSERVICES" tabindex="14"
                                            class="textbox100" style="width: 60px" value="<%=FormatNumber(f_support)%>" />
                                        <img src="/js/FVS.gif" name="img_SUPPORTEDSERVICES" id="img_SUPPORTEDSERVICES" width="15px" height="15px" border="0"
                                            alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Garage.
                                    </td>
                                    <td>
                                        <input onblur="calc_rent()" type="text" name="txt_GARAGE" id="txt_GARAGE" tabindex="13" class="textbox100"
                                            style="width: 60px" value="<%=FormatNumber(f_garage)%>" />
                                        <img src="/js/FVS.gif" name="img_GARAGE" id="img_GARAGE" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr bgcolor="#133E71">
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr bgcolor="#040455">
                                                <td width="97" bgcolor="#133E71">
                                                    <span class="style3">Total Rent</span>
                                                </td>
                                                <td bgcolor="#133E71">
                                                    <input type="text" name="txt_TOTALRENT" id="txt_TOTALRENT" class="textbox100" style="width: 60px" readonly="readonly"
                                                        value="<%=FormatNumber(f_totalrent)%>" />
                                                    <img src="/js/FVS.gif" name="img_TOTALRENT" id="img_TOTALRENT" width="15px" height="15px" border="0"
                                                        alt="" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="hidden" name="hid_PropertyID" id="hid_PropertyID" value="<%=PropertyID%>" />
                                        <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right" colspan="2">
                                        <input type="button" class="RSLButton" value=" SAVE Current Rent " title=" SAVE Current Rent " onclick="SaveForm()"
                                            tabindex="15" name="button" style="cursor:pointer" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe name="frm_team" id="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
