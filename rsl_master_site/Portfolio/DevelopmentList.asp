<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match
	Dim TDFunc		   (6)
	Dim searchSQL, searchName

	searchName = Replace(Request("name"),"'","''")
	searchSQL = ""

	If searchName <> "" Then
		searchSQL = " Where (SCHEMENAME LIKE '%" & searchName & "%' ) "
	Else
		searchName = ""
	End If

	ColData(0)  = "Scheme Name|DEVELOPMENTNAME|270"
	SortASC(0) 	= "DEVELOPMENTNAME ASC"
	SortDESC(0) = "DEVELOPMENTNAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Type|DEVELOPMENTTYPE|120"
	SortASC(1) 	= "DEVELOPMENTTYPE ASC"
	SortDESC(1) = "DEVELOPMENTTYPE DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "Start|STARTDATE2|75"
	SortASC(2) 	= "STARTDATE ASC"
	SortDESC(2) = "STARTDATE DESC"
	TDSTUFF(2)  = ""
	TDFunc(2) = ""

	ColData(3)  = "End|TARGETDATE2|75"
	SortASC(3) 	= "TARGETDATE ASC"
	SortDESC(3) = "TARGETDATE DESC"
	TDSTUFF(3)  = ""
	TDFunc(3) = ""

	ColData(4)  = "Props|TOTALPROPERTIES|80"
	SortASC(4) 	= "TOTALPROPERTIES ASC"
	SortDESC(4) = "TOTALPROPERTIES DESC"
	TDSTUFF(4)  = ""
	TDFunc(4) = ""

	ColData(5)  = "Available|AVAILABLE|100"
	SortASC(5) 	= "AVAILABLE ASC"
	SortDESC(5) = "AVAILABLE DESC"
	TDSTUFF(5)  = ""
	TDFunc(5) = ""

	ColData(6)  = "Un-avail...|UNAVAILABLE|110"
	SortASC(6) 	= "UNAVAILABLE ASC"
	SortDESC(6) = "UNAVAILABLE DESC"
	TDSTUFF(6)  = ""
	TDFunc(6) = ""

	PageName = "DevelopmentList.asp"
	EmptyText = "No scheme found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""onclick=""""load_me("" & rsSet(""DEVELOPMENTID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT PS.SCHEMEID DEVELOPMENTID, SCHEMENAME DEVELOPMENTNAME, DT.DESCRIPTION AS DEVELOPMENTTYPE, " &_
			"STARTDATE2 = ISNULL(CONVERT(VARCHAR, STARTDATE, 103),'N/A') ,  " &_
			"TARGETDATE2 = ISNULL(CONVERT(VARCHAR, TARGETDATE, 103),'N/A'),  " &_
			"ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') AS FULLNAME,  " &_
			"ISNULL(TP.TOTALPROPERTIES,0) AS TOTALPROPERTIES,  " &_
			"ISNULL(AV.AVAILABLE,0) AS AVAILABLE,   " &_
			"ISNULL(TP.TOTALPROPERTIES,0) - ISNULL(AV.AVAILABLE,0) AS UNAVAILABLE  " &_
			"FROM P_SCHEME PS " &_
			"	LEFT JOIN PDR_DEVELOPMENT D  ON PS.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"	LEFT JOIN   " &_
			"		(	SELECT COUNT(*) AS AVAILABLE, SCHEMEID   " &_
			"			FROM P__PROPERTY WHERE STATUS IN (1,5) GROUP BY SCHEMEID) AV   " &_
			"	ON PS.SCHEMEID = AV.SCHEMEID  " &_
			"	LEFT JOIN   " &_
			"		(SELECT COUNT(*) AS TOTALPROPERTIES, SCHEMEID   " &_
			"			FROM P__PROPERTY GROUP BY SCHEMEID) TP  " &_
			"	ON PS.SCHEMEID = TP.SCHEMEID  	" &_
			"	LEFT JOIN P_DEVELOPMENTTYPE DT ON D.DEVELOPMENTTYPE = DT.DEVELOPMENTTYPEID  " &_
			"	LEFT JOIN E__EMPLOYEE E ON D.LEADDEVOFFICER = E.EMPLOYEEID " &_
		    " " &  searchSQL & " " &_ 
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

	Call Create_Table()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Portfolio --> Tools --> Scheme List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

    function load_me(employee_id){
	    location.href = "PropertyList.asp?DevelopmentID=" + employee_id + "&DevPage=<%=Request("page")%>&DevSort=<%=Request("CC_Sort")%>";
	    }

    function quick_find(){	
		    location.href = "DevelopmentList.asp?name="+document.getElementById("findemp").value;
	    }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_Scheme" id="tab_Scheme" title="Scheme List" src="images/schemelist.gif" width="102"
                    height="20" border="0" alt="" /></td>
            <td>
                <img src="images/spacer.gif" width="355" height="20" alt="" /></td>
            <td nowrap="nowrap">
                <input type="button" name="btn_FIND" id="btn_FIND" title="Search for matches using scheme Name."
                    class="RSLBUTTON" value="Quick Find" onclick="quick_find()" style="cursor: pointer" />
                &nbsp;<input type="text" size="18" name="findemp" id="findemp" class="textbox200" /></td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="2">
                <img src="images/spacer.gif" width="347" height="1" alt="" /></td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4"
        style="display: none"></iframe>
</body>
</html>
