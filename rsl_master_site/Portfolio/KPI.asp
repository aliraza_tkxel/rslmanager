<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Const QTR_CNT = 4
	Dim kpi_cnt, KPIArray, qtr_start, qtr_end, RentDueArray
		
	OpenDB()
	
	SQL = "SELECT * FROM P_KPI ORDER BY KPIID"
	Call OpenRs(rskpi, SQL)
	kpi_cnt = rskpi.recordCount
	
	build_holding_array()	
	build_value_array()
	
	
	' FILL QTR ARRAYS WITH APPROPRIATE START AND END DATES
	' FILL RENT RECEIVED ARRAY TO AVOID HAVING TO RUN THIS FUNCTION MORE THAN 4 TIMES
	Function build_holding_array()
	
		' QTR DATES
		Redim qtr_start(QTR_CNT)		
		Redim qtr_end(QTR_CNT)
		
		SQL = "SELECT DATEADD(D, -1, YSTART) AS QTR_1_START, " &_
				"DATEADD(M, 3, YSTART) AS QTR_1_END, " &_
				"DATEADD(D, -1, DATEADD(M, 3, YSTART)) AS QTR_2_START, " &_
				"DATEADD(M, 6, YSTART) AS QTR_2_END, " &_
				"DATEADD(D, -1, DATEADD(M, 6, YSTART)) AS QTR_3_START, " &_
				"DATEADD(M, 9, YSTART) AS QTR_3_END, " &_
				"DATEADD(D, -1, DATEADD(M, 9, YSTART)) AS QTR_4_START, " &_
				"DATEADD(M, 12, YSTART) AS QTR_4_END " &_
			 "FROM F_FISCALYEARS WHERE YSTART <= '" & DATE & "' AND YEND >= '" & DATE & "'"

		'rw SQL
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			qtr_start(1) = rsSet("QTR_1_START")
			qtr_start(2) = rsSet("QTR_2_START")
			qtr_start(3) = rsSet("QTR_3_START")
			qtr_start(4) = rsSet("QTR_4_START")
			qtr_end(1)   = rsSet("QTR_1_END")
			qtr_end(2)   = rsSet("QTR_2_END")
			qtr_end(3)   = rsSet("QTR_3_END")
			qtr_end(4)   = rsSet("QTR_4_END")
		End If
		CloseRs(rsSet)
		
		' RENT RECEIVED
		Redim RentDueArray(QTR_CNT)
		For x=1 To QTR_CNT
			RentDueArray(x) = rent_due(qtr_start(x), qtr_end(x))
			'rw "(" & x & ") - "  & RentDueArray(x) & "<BR>"
		Next
			
	End Function
	
	' BUILD AND SET VALUE ARRAY TO ZERO
	' THEN FILL ARRAY USING APPROPRIATE FUNCTIONS
	Function build_value_array()
	
		Redim KPIArray(kpi_cnt, QTR_CNT)
		' SET ALL VALUES IN ARRAY TO ZERO
		for z = 1 to kpi_cnt
			for y = 1 to QTR_CNT
				KPIArray(z, y) = 0
			next
		next
	
		for x = 1 To QTR_CNT
			KPIArray(1, x) = KPI_1(qtr_start(x), qtr_end(x))
			'rw "(" & 1 & "," & x & ") - "  & KPIArray(1, x) & "<BR>"
			KPIArray(2, x) = KPI_2(qtr_start(x), qtr_end(x))
			'rw "(" & 2 & "," & x & ") - "  & KPIArray(2, x) & "<BR>"
			KPIArray(3, x) = KPI_3(qtr_start(x), qtr_end(x))
			KPIArray(4, x) = KPI_4(qtr_start(x), qtr_end(x), x)
			KPIArray(5, x) = KPI_5(qtr_start(x), qtr_end(x), x)
			KPIArray(6, x) = KPI_6(qtr_end(x))	
			KPIArray(7, x) = KPI_7_8(qtr_start(x), qtr_end(x), "D", 7)
			KPIArray(8, x) = KPI_7_8(qtr_start(x), qtr_end(x), "F", 31)
			
		next
		
	End Function
	
	' FUNCTION TO COMPLETE TOTALS FOR KPI 1 (AVERAGE WEEKLY RENT)
	Function KPI_1(startdate, enddate)
	
		SQL = 	"SELECT 	SUM(RJ.AMOUNT) AS QTR_RENT, COUNT(RJ.AMOUNT) QTR_LET " &_
				"FROM 		F_RENTJOURNAL RJ " &_
				"			INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 		DAY(RJ.TRANSACTIONDATE) = 1 AND RJ.PAYMENTTYPE IS NULL AND RJ.ITEMTYPE = 1 AND " &_
				"			RJ.TRANSACTIONDATE > '"&startdate&"' AND RJ.TRANSACTIONDATE < '"&enddate&"' AND " &_
				"			T.TENANCYTYPE = 1 AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) "

		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			If rsSet(1) <> 0 Then 
				KPI_1 = rsSet(0) / rsSet(1)
			Else
				KPI_1 = 0 
			End If
		Else
			KPI_1 = 0
		End If
		CloseRs(rsSet)
	
	End Function
	
	' FUNCTION TO COMPLETE TOTALS FOR KPI 7 (MAINTENANCE COSTS PER DWELLING PER WEEK)
	Function KPI_2(startdate, enddate)
	
		Dim cost, NoProps
	
		SQL = 	"SELECT SUM(PI.GROSSCOST) AS COST " &_
				"FROM   F_PURCHASEITEM PI " &_ 
				"		INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID " &_
				"		INNER JOIN P_WORKORDER W ON W.ORDERID = PO.ORDERID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = W.PROPERTYID " &_
				"WHERE  PI.PISTATUS IN (10, 11, 12, 14) AND " &_
				"		EXPENDITUREID IN (24,25,944,941,942,943,945,27, 974,29,26,28,50,28,56,59) AND " &_
				"		(P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) AND " &_
				"		PIDATE >= '" & startdate & "' AND PIDATE <= '" & enddate & "' "

		'RW sql & "<br><br>"
		' CALCULATE GROSS COST SPENT ON MAINTENANCE AND REPAIRS DURING THIS QTR		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then cost = rsSet("COST") Else cost = 0 End If
		
		' CALCULATE NUMBER OF PROPERTIES LET DURING THIS QUARTER
		SQL = 	" SELECT COUNT(*) AS NOPROPS FROM P__PROPERTY WHERE ASSETTYPE = 1 OR ASSETTYPE IS NULL "

		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then NoProps = rsSet("NOPROPS") Else NoProps = 0 End If
		If NoProps <> 0 And cost <> 0 Then
			'KPI_2 = (cost / NoProps) / 13
			KPI_2 = (cost / 13) / NoProps
		Else
			KPI_2 = 0
		End If
		CloseRs(rsSet)
				
	End Function

	' FUNCTION TO COMPLETE TOTALS FOR KPI 2 (% OF RENT IN ARREARS)
	Function KPI_4(startdate, enddate, qtr)
	
		Dim rentDue, RentReceived
		RentDue = RentDueArray(qtr)		
		RentReceived = rent_received(startdate, enddate)
		If rentDue <> 0 And RentReceived <> 0 Then
			'KPI_2 = ((RentDue - RentReceived) / RentDue) * 100
			KPI_4 = 100 - (RentReceived / RentDue * 100)
		Else
			KPI_4 = 0
		End If
				
	End Function

	' FUNCTION TO COMPLETE TOTALS FOR KPI 4 (% RENT COLLECTED)
	Function KPI_3(startdate, enddate)
	
		Dim RentDue, RentReceived

		' GET RENT DUE
		SQL = 	"SELECT SUM(AMOUNT) AS RENTDUE FROM F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE (ITEMTYPE IN (8, 9) OR (ITEMTYPE = 1 AND PAYMENTTYPE IS NULL) ) " &_
				"		AND STATUSID NOT IN (1,4) " &_
				"		AND RJ.TRANSACTIONDATE > '"&startdate&"' AND RJ.TRANSACTIONDATE < '"&enddate&"' " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " &_
				"		AND T.STARTDATE <= '" & startdate & "' AND " &_
				"		(T.ENDDATE >= '" & enddate & "' OR T.ENDDATE IS NULL) "		' CURRENT TENANTS AT START OF MONTH ONLY		

				
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			RentDue = rsSet("RENTDUE")
		Else
			RentDue = 0
		End If
		
		' GET RENT RECEIVED
		SQL = 	"SELECT SUM(ABS(AMOUNT)) RR FROM F_RENTJOURNAL RJ " &_
				"		INNER JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 	PT.RENTRECEIVED = 1 " &_
				"		AND ITEMTYPE IN (1) AND STATUSID NOT IN (1,4) " &_
				"		AND RJ.TRANSACTIONDATE > '"&startdate&"' AND RJ.TRANSACTIONDATE < '"&enddate&"' " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) "

		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			RentReceived = rsSet("RR")
		Else
			RentReceived = 0
		End If
		CloseRs(rsSet)
		
		If RentDue <> 0 And RentReceived <> 0 Then
			KPI_3 = (RentReceived / RentDue) * 100
		Else
			KPI_3 = 0
		End If
				
	End Function

	' FUNCTION TO COMPLETE TOTALS FOR KPI 3 (% RENT LOST DUE TO DWELLING BEING EMPTY)
	Function KPI_5(startdate, enddate, qtr)
	
		Dim RentDue, RentLost, TotalRent

		SQL = 	"SELECT	SUM(TOTALOFRENTS) / COUNT(*) AS RENTLOST " &_
				"FROM	F_UNLETDAILYSNAPSHOT " &_
				"WHERE	(UNLETTIMESTAMP > '"&startdate&"' AND UNLETTIMESTAMP < '"&enddate&"') "
				
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			RentLost = rsSet("RENTLOST")
		Else
			RentLost = 0
		End If
		CloseRs(rsSet)
		
		RentDue = RentDueArray(qtr)		
		TotalRent = RentDue + RentLost
		
		If RentDue <> 0 And RentLost <> 0 Then
			KPI_5 = (RentLost / TotalRent) * 100
		Else
			KPI_5 = 0
		End If
				
	End Function
	
	' FUNCTION TO COMPLETE TOTALS FOR KPI 8 (USE STORED PROC TO CALCULATE AVERAGE RELET TIME IN DAYS)
	Function KPI_6(enddate)
	
		Dim average_relet	

		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "KPI_AVERAGE_RELET_TIME"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = qtr_start(1)
		comm.parameters(2) = enddate
		comm.execute
		average_relet = comm.parameters(3)
		Set comm = Nothing
		
		KPI_6 = average_relet
		
	End Function

	' FUNCTION TO COMPLETE TOTALS FOR KPI 5 and 6 (% OF REPAIRS ON TIME)
	Function KPI_7_8(startdate, enddate, priority, days)
	
		Dim NoRepairs, WithinLimit
	
		SQL = 	"SELECT COUNT(*) AS REPAIRS " &_
				"FROM 	C_JOURNAL J " &_
				"		INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_
				"		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WTR.ORDERITEMID " &_
				"		INNER JOIN ( " &_
				"		SELECT MIN(REPAIRHISTORYID) AS EARLIESTFINISH, JOURNALID, ITEMDETAILID " &_
				"		FROM C_REPAIR " &_
				"		WHERE ITEMACTIONID IN (9,10,15) " &_
				"		GROUP BY JOURNALID, ITEMDETAILID " &_
				"		) R ON J.JOURNALID = R.JOURNALID " &_
				"		INNER JOIN C_REPAIR RR ON RR.REPAIRHISTORYID = R.EARLIESTFINISH " &_
				"		INNER JOIN R_ITEMDETAIL IT ON IT.ITEMDETAILID = R.ITEMDETAILID " &_
				"		INNER JOIN R_PRIORITY P ON IT.PRIORITY = P.PRIORITYID " &_
				"WHERE J.CREATIONDATE >= '" & startdate & "' AND J.CREATIONDATE <= '" & enddate & "' " &_
				"		AND IT.PRIORITY IN ('" & priority & "') " 
								
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then NoRepairs = rsSet("REPAIRS") Else NoRepairs = 0 End If
		
		' APPLY RECOMMENDED FINISH TIME
		SQL = SQL & " AND DATEDIFF(D, J.CREATIONDATE, RR.LASTACTIONDATE) <= " & days & " " 
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then WithinLimit = rsSet("REPAIRS") Else WithinLimit = 0 End If
		If WithinLimit <> 0 And NoRepairs <> 0 Then
			KPI_7_8 = (WithinLimit / NoRepairs) * 100
		Else
			KPI_7_8 = 0
		End If
		CloseRs(rsSet)
				
	End Function

	'NEEDS AMENDING TO USE CORRECT RENT GRABBER SQL
	Function rent_due(startdate, enddate)
	
		SQL = 	"SELECT SUM(AMOUNT) AS RENTDUE FROM F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE ((ITEMTYPE = 1 AND PAYMENTTYPE IS NULL)) " &_
				"		AND STATUSID NOT IN (1,4) " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " &_
				"		AND T.ENDDATE IS NULL " &_
				"		AND TRANSACTIONDATE > '"&startdate&"' AND TRANSACTIONDATE < '"&enddate&"' "
				
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			rent_due = rsSet("RENTDUE")
		Else
			rent_due = 0
		End If
		CloseRs(rsSet)
	
	End Function 
	
	'NEEDS AMENDING TO USE CORRECT RENT GRABBER SQL
	Function rent_received(startdate, enddate)
	
		SQL = 	"SELECT SUM(ABS(AMOUNT)) AS RR FROM F_RENTJOURNAL RJ " &_
				"		INNER JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 	PT.RENTRECEIVED = 1 " &_
				"		AND ITEMTYPE IN (1) AND STATUSID NOT IN (1,4) " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " &_
				"		AND T.ENDDATE IS NULL " &_
				"		AND TRANSACTIONDATE > '"&startdate&"' AND TRANSACTIONDATE < '"&enddate&"' "
				
		'RW sql & "<br><br>"		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			rent_received = rsSet("RR")
		Else
			rent_received = 0
		End If
		CloseRs(rsSet)
	
	End Function 
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio--&gt; Main Details</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	
	function show_pie(qtr1, qtr2, qtr3, qtr4, strname){
	
		window.showModelessDialog("popups/KPIPieChart.asp?qtr1="+qtr1+"&qtr2="+qtr2+"&qtr3="+qtr3+"&qtr4="+qtr4+"&strname="+strname+"&timestamp="+new Date(), "","dialogHeight: 340px; dialogWidth: 560px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		//alert("popups/KPIPieChart.asp.asp?qtr1="+qtr1+"&qtr2="+qtr2+"&qtr3="+qtr3+"&qtr4="+qtr4);
	}

</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<BR><BR>
<TABLE WIDTH="98%" BORDER="0" CELLSPACING="2" CELLPADDING="2">
  <% While Not rskpi.Eof %>
		  <TR> 
			<TD STYLE='WIDTH:15PX'>&nbsp;</TD>
			<TD ALIGN=CNETER>
			  <table width="98%" border cellspacing="0" bordercolor='black' cellpadding="2" style="border-collapse:collapse;border: 1px solid black">
				<tr> 
				  <td bgcolor=beige NOWRAP><b><%=rskpi("KPIID")%>. <%=rskpi("KPITYPE")%></b></td>
				  <td colspan=7>&nbsp;&nbsp;<%=rskpi("KPINAME")%> </td>
				</tr>
				<tr> 
				  <td colspan=8>
					<%=rskpi("KPIDESCRIPTION")%><BR><BR>
					<I>Additional information</I> : <a href="<%=rskpi("KPILINK")%>" TARGET='_BLANK'><%=rskpi("KPILINK")%></a>		  
				  </td>
				</tr>
				<tr bgcolor=beige align=center> 
				<% For i = 1 To QTR_CNT 
				  Response.write "<td colspan=2 style='width:25%'>qtr" & i & ": <b>" & Round(KPIArray(rsKPI("KPIID"),i),2) & "</b></td>"
				  Next %>
				</tr>
			  </table>
			</TD>
			<TD STYLE='WIDTH:15PX'>&nbsp;</TD>
		  </TR>
		  <TR STYLE='HEIGHT:20PX'><TD></TD>
		  	<TD ALIGN=RIGHT>
		  	<input type="button" value=" Chart " class="RSLButton" onclick="show_pie(<%=Round(KPIArray(rsKPI("KPIID"),1),2)%>, <%=Round(KPIArray(rsKPI("KPIID"),2),2)%>, <%=Round(KPIArray(rsKPI("KPIID"),3),2)%>, <%=Round(KPIArray(rsKPI("KPIID"),4),2)%>, <%=rsKPI("KPIID")%>)">
		    &nbsp;&nbsp;&nbsp;
			</TD><TD></TD>
		</TR>
	<% rskpi.movenext()
	   Wend 
	%>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name="Property_Server" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
<%   	'CloseDB()
%>

