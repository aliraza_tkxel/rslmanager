<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<head>
<%
Dim l_address1
Dim Refurbishment_Detail
Set dbrs = server.createobject("ADODB.Recordset")
Set dbcmd = server.createobject("ADODB.Command")
Dim xmlString
Dim cnt,index_Element,counter,index_Level
Dim ElementID()     ' Arrays
Dim RiskLevelId()   ' Arrays

    index_Element = 0
    index_Level = 0
    cnt = 1

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then PropertyID = -1 End If

	Call OpenDB()

	'--Retrieve First Line of Address
		SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
		Call OpenRs(rsAddress, SQL)
		If (NOT rsAddress.EOF) Then
			l_address1 = rsAddress("ADDRESS1")
			l_housenumber = rsAddress("HOUSENUMBER")
		    l_propertyid = UCASE(rsAddress("PROPERTYID"))
		    l_propertystatus = l_propertyid
		Else
			l_address1 = "[None Given]"
		End If

		'Build Option Select(combo) for ASBESTOS ELEMENT
		Call BuildSelect(lstASBESTOS, "sel_ASBESTOS", "P_ASBESTOS WHERE OTHER=0", "ASBESTOSID, RISKDESCRIPTION", "SORDER", "Please Select", l_ASBESTOS, null, "textbox200", "style='width:240px' onchange=checkOther()")
		'Build Option Select (combo) for ASBESTOS RISK  ID  e.g A1-A25
		Call BuildSelect(lstASBRISK, "sel_ASBRISK", "P_ASBRISK", "ASBRISKID, ASBRISKID","ORDERBY","Please Select", l_ASBRISK, null, "textbox200", "style='width:240px'")
		'Build Option Select(combo) for ASBESTOS RISK LEVEL 
		Call BuildSelect(lstASBRISKLEVEL, "sel_ASBRISKLEVEL", "P_ASBRISKLEVEL", "ASBRISKLEVELID, ASBRISKLEVELDESCRIPTION", "ASBRISKLEVELID","Please Select", l_ASBRISKLEVEL, null, "textbox200", "style='width:240px'")
		Call CloseRs(rsAddress)

        SQL = "SELECT  COUNT(1) As ArraySize " &_
                "FROM    P_ASBRISK PRI , " &_
                "        P_PROPERTY_ASBESTOS_RISKLEVEL PR " &_
                "        LEFT JOIN P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID = PAR.PROPASBLEVELID " &_
                "        INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID " &_
                "        INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID " &_
                "        LEFT JOIN dbo.E__EMPLOYEE E ON PR.UserId = E.EMPLOYEEID " &_
                "WHERE   PAR.ASBRISKID = PRI.ASBRISKID " &_
                "        AND PR.PROPERTYID = '" & PropertyID & "' "
        Call OpenRs(rsArraySize, SQL)
		If (NOT rsArraySize.EOF) Then
            ArraySize = (rsArraySize(0) -1)
        Else
            ArraySize = 0
        End If
        Call CloseRs(rsArraySize)

        Call CloseDB()

        ReDim ElementID(ArraySize)
        ReDim RiskLevelId(ArraySize)


				sql="SELECT PL.ASBRISKLEVELID AS RISKLEVELID, PL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL, " &_
                    "P.ASBESTOSID AS ELEMENTID, P.RISKDESCRIPTION AS ELEMENTDISCRIPTION, PRI.ASBRISKID AS RISK, " &_
                    " CASE P.OTHER WHEN 0 THEN NULL ELSE P.RISKDESCRIPTION END AS OTHER, PR.DATEADDED, PR.DATEREMOVED, LEFT(E.FIRSTNAME, 1)+LEFT(E.LASTNAME, 1) As EmployeeInitials, E.EMPLOYEEID As UserId, PR.NOTES " &_
                    "FROM P_ASBRISK PRI, P_PROPERTY_ASBESTOS_RISKLEVEL PR " &_  
                    "LEFT JOIN P_PROPERTY_ASBESTOS_RISK PAR ON PR.PROPASBLEVELID= PAR.PROPASBLEVELID " &_  
                    "INNER JOIN P_ASBRISKLEVEL PL ON PR.ASBRISKLEVELID = PL.ASBRISKLEVELID " &_ 
                    "INNER JOIN P_ASBESTOS P ON PR.ASBESTOSID = P.ASBESTOSID " &_
                    "LEFT JOIN dbo.E__EMPLOYEE E ON PR.UserId = E.EMPLOYEEID " &_
                    "WHERE PAR.ASBRISKID = PRI.ASBRISKID " &_ 
                    "AND PR.PROPERTYID = '" & PropertyID & "' " &_
                    "ORDER BY PL.ASBRISKLEVELID, P.ASBESTOSID "

		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 1
		dbcmd.CommandText = sql

		Set dbrs=dbcmd.execute

		Dim RecordNo

		If Not dbrs.eof Then
			While Not dbrs.eof
			    counter = 1
				xmlString = xmlString & "<DATA>"
					xmlString = xmlString & "<RISKLEVELID>" & dbrs("RISKLEVELID") & "</RISKLEVELID>"
					xmlString = xmlString & "<RISKLEVEL>" & dbrs("RISKLEVEL") & "</RISKLEVEL>"
					xmlString = xmlString & "<RISK>" & dbrs("RISK") & "</RISK>"
                    xmlString = xmlString & "<DATEADDED>" & dbrs("DATEADDED") & "</DATEADDED>"
                    xmlString = xmlString & "<DATEREMOVED>" & dbrs("DATEREMOVED") & "</DATEREMOVED>"
                    xmlString = xmlString & "<USERID>" & dbrs("UserId") & "</USERID>"
                    xmlString = xmlString & "<USER>" & dbrs("EmployeeInitials") & "</USER>"
                    xmlString = xmlString & "<NOTES>" & dbrs("NOTES") & "</NOTES>"
					xmlString = xmlString & "<ELEMENTID>" & dbrs("ELEMENTID") & "</ELEMENTID>"
					xmlString = xmlString & "<ELEMENTDESCRIPTION>" & dbrs("ELEMENTDISCRIPTION") & "</ELEMENTDESCRIPTION>"
					xmlString = xmlString & "<PROPERTYID>" &  PropertyID &  "</PROPERTYID>"
 
 					for i=0 to ubound(RiskLevelId)
					    If(RiskLevelId(i)=dbrs("RISKLEVELID") ) then
					        counter = counter+1
					    end if
					next

					xmlString=xmlString & "<LEVELRECORDNO>" & counter &"</LEVELRECORDNO>"
					counter = 1

					for i=0 to ubound(ElementId)
					    If(ElementId(i)=dbrs("ELEMENTID") and RiskLevelId(i)=dbrs("RISKLEVELID") ) then
					        counter=counter+1
					    end if
					next

					xmlString=xmlString & "<RECORDNO>" & counter & "</RECORDNO>"
					xmlString=xmlString & "<OTHER> " & dbrs("ELEMENTDISCRIPTION") & "</OTHER>"
					ElementId(index_Element)=dbrs("ELEMENTID")
					RiskLevelId(index_Level)=dbrs("RISKLEVELID")
				xmlString=xmlString & "</DATA>"
				cnt=cnt+1
				index_Element=index_Element+1
				index_Level=index_Level+1
				dbrs.movenext
			wend
		else
			xmlString=xmlString & "<DATA>"
					xmlString=xmlString & "<RISKLEVELID></RISKLEVELID>" 
					xmlString=xmlString & "<RISKLEVEL></RISKLEVEL>"
					xmlString=xmlString & "<RISK></RISK>"
                    xmlString=xmlString & "<DATEADDED></DATEADDED>"
                    xmlString=xmlString & "<DATEREMOVED></DATEREMOVED>"
                    xmlString=xmlString & "<USERID></USERID>"
                    xmlString=xmlString & "<USER></USER>"
                    xmlString=xmlString & "<NOTES></NOTES>"
					xmlString=xmlString & "<ELEMENTID></ELEMENTID>"
					xmlString=xmlString & "<ELEMENTDESCRIPTION></ELEMENTDESCRIPTION>"
					xmlString=xmlString & "<PROPERTYID></PROPERTYID>"
					xmlString=xmlString & "<LEVELRECORDNO></LEVELRECORDNO>"
					xmlString=xmlString & "<RECORDNO></RECORDNO>"
					xmlString=xmlString & "<OTHER></OTHER>"
				xmlString=xmlString & "</DATA>"
		end if
	    dbrs.Close()
	    dbcmd.Cancel()
%>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Business</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="Javascript">

var FormFields = new Array();
var selectedRec;
var selectedElementRec,selectedRiskRec,selectedRiskLevelRec;
var cnt=-1;

function checkOther()
{
	if(parseInt(frmHealth.sel_ASBESTOS.value)==14)
			document.getElementById("spother").style.visibility="visible";
	else
			document.getElementById("spother").style.visibility="hidden";
    return;
}

//Function to get detail from xml document and format it using xslt
function getDetails()
{
		var xmlstr
		var xmlDoc = new ActiveXObject("Msxml2.DOMDocument.3.0");
		    xmlDoc.async = false;

		if(!rec.recordset.EOF && !rec.recordset.BOF)
		{
		     rec.recordset.movefirst();
		}
		if( rec.recordset.EOF && rec.recordset.BOF)
		{
		    xmlDoc.load(emptyxml.XMLDocument);
		    var xslDOM = XSLStyle1.XMLDocument;
		    document.getElementById("divResults").innerHTML = xmlDoc.transformNode(xslDOM);
		    return false;
		 }
		 else
		 {
		    xmlDoc.load(rec.XMLDocument);
		    var xslDOM = XSLStyle1.XMLDocument;
		    document.getElementById("divResults").innerHTML = xmlDoc.transformNode(xslDOM);
		    return true;
		 }
		//return;
}

//Function to delete record from database
function deleteRecord(Elementid, Risk, RiskLevel, flag)
{
    	var findRisk=Risk;
    	var findElement= parseInt(Elementid);
    	var findRiskLevel= parseInt(RiskLevel);
		var xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM");
	    var RecordNo=0,Element=0,LevelRecordNo=0,Levelid=0;
	    var temp;
		
		if(!rec.recordset.BOF){ rec.recordset.movefirst();}
				
		while(!rec.recordset.EOF&&!rec.recordset.BOF)
		{		
			if(rec.recordset("ELEMENTID")==findElement && rec.recordset("RISK")==findRisk && rec.recordset("RISKLEVELID")==findRiskLevel)
			{
			    if(rec.recordset("LEVELRECORDNO")==1) 
				{
				   LevelRecordNo=rec.recordset("LEVELRECORDNO")				     
				}
				
				if(rec.recordset("RECORDNO")==1) 
				{
				    RecordNo = parseInt(rec.recordset("RECORDNO"));				    			    
				}
				
				url="/Includes/xmlServer/xmlsrv.asp?ASBRISKID="+rec.recordset("RISK")+"&PROPERTYID="+rec.recordset("PROPERTYID")+"&ASBESTOSID="+rec.recordset("ELEMENTID")+"&ASBRISKLEVELID="+rec.recordset("RISKLEVELID")+"&task=DELETEASBRISK";
				rec.recordset.Delete();
							
				xmlHttp.open("POST", url, false);
				xmlHttp.setRequestHeader("Content-Type","text/xml");
				xmlHttp.send();
				xmlobj=xmlHttp.responseXML;

				var msg=xmlobj.xml;
				var ALERT=xmlobj.selectSingleNode("output").text;
				if(flag=="1") 
			    {
				     alert(ALERT);
				}
				
				if(!rec.recordset.EOF && !rec.recordset.BOF) 
		        {
		            rec.recordset.movefirst() ;
		        } 
		
		        //if( rec.recordset.EOF || rec.recordset.BOF)
		        //{
		        	//var xmlstr;
		        	//var xmlDoc = new ActiveXObject("Msxml2.DOMDocument.3.0");
		        	//xmlDoc.async = false;
						
			        //xmlDoc.load(emptyxml.XMLDocument);
			
			        //var xslDOM = XSLStyle1.XMLDocument;
			        //document.getElementById("divResults").innerHTML = xmlDoc.transformNode(xslDOM);
			        if(getDetails()==false) break;
			        
			     //   break;
		        //}
				
				rec.recordset.movefirst();
				continue;				
			}
			
			if(findRiskLevel==rec.recordset("RISKLEVELID"))
			{			        
			        //alert("risklevel:"+findRiskLevel+" Element:"+findElement)
			  
			        if(rec.recordset("ELEMENTID")==findElement && LevelRecordNo==0 && RecordNo==0)
			        {
			            //alert("in level o and record no 0")
			            rec.recordset.movenext();
			            continue;
			        }
			        
			        if(rec.recordset("LEVELRECORDNO")>1 && LevelRecordNo!=0)
			        {
    		           
			           rec.recordset("LEVELRECORDNO")=parseInt(rec.recordset("LEVELRECORDNO"))-1;
			           
			           if(rec.recordset("RECORDNO")>1 && rec.recordset("ELEMENTID")==findElement)
			            {
			                rec.recordset("RECORDNO")=parseInt(rec.recordset("RECORDNO"))-1;
			            }
    		        }
    		        
    		        else if(rec.recordset("RECORDNO")>1)
			        {
			                rec.recordset("RECORDNO")=parseInt(rec.recordset("RECORDNO"))-1;
			        }    		        
			}
			rec.recordset.movenext();
		}
		
		return;			
}

//Function to delete record from the database when X button is used
//function deleteit(Elementid,Risk,RiskLevel)
//{
//		var findRisk=Risk;
//		var findElement= parseInt(Elementid);
//		var findRiskLevel=parseInt(RiskLevel);
//		
//		rec.recordset.movefirst();
//		selectedElementRec=findElement;
//		selectedRiskRec=findRisk;
//		selectedRiskLevelRec=findRiskLevel;
//	
//		if(!rec.recordset.EOF)	rec.recordset.movefirst();
//		
//		if (confirm("Are you sure you want to delete this item?."))
//		{
//            this.deleteRecord(findElement,findRisk,findRiskLevel,"1");
//		}
//	    getDetails();
//		frmHealth.btnAdd.disabled=false;
//		frmHealth.btnAmmend.disabled=true;
//		return;
//}

//Function to add record into the database
function AddRec()
{
		var RecordNo = 1;
		var LevelRecordNo = 1;
		var flag = 1;        

            FormFields[0] = "sel_ASBRISKLEVEL|Asbestos Level|SELECT|Y"
            FormFields[1] = "sel_ASBESTOS|Asbestos Element|SELECT|Y"
            FormFields[2] = "sel_ASBRISK|Risk|SELECT|Y"
            FormFields[3] = "txt_AsbestosAdded|Added|DATE|Y"
            FormFields[4] = "txt_AsbestosRemoved|Removed|DATE|N"
            FormFields[5] = "txt_Notes|Notes|TEXT|N"
            FormFields[6] = "txt_OTHER|Other|TEXT|N"
 
		/*
        if(frmHealth.sel_ASBRISKLEVEL.value == "")
		{
		    alert("Please select the value from Asbestos Level");
			return;
		}

	    if (frmHealth.sel_ASBESTOS.value == "")
		{
			alert("Please select the value from Asbestos Elements");
			return;
		}

		if(frmHealth.sel_ASBRISK.value == "")
		{
			alert("Please select the value from Risk");
			return;
		}

        if (frmHealth.txt_AsbestosAdded.value == "") {
            alert("Please add a date to the 'added field'");
            return;
        }
        */

        if (!checkForm()) return false;

		if (!rec.recordset.EOF&& !rec.recordset.BOF)
		{
		
			rec.recordset.movefirst();
			
			if(rec.recordset("ELEMENTDESCRIPTION")=="")
		    {
				rec.recordset.Delete();
			}
		}
		
	    if(!rec.recordset.EOF)  rec.recordset.movefirst();

	    while(!rec.recordset.EOF)
		{
		    
		    if(flag==1)
		    {
		        rec.recordset.movefirst();
		        flag=2;
		    }

			//if (!rec.recordset.EOF)
			//{
				//if( parseInt(rec.recordset("ELEMENTID"))==parseInt(frmHealth.sel_ASBESTOS.value) && rec.recordset("RISK")==frmHealth.sel_ASBRISK.value)
                if( parseInt(rec.recordset("ELEMENTID"))==parseInt(frmHealth.sel_ASBESTOS.value) && rec.recordset("RISK")==frmHealth.sel_ASBRISK.value && parseInt(rec.recordset("RISKLEVELID"))==parseInt(frmHealth.sel_ASBRISKLEVEL.value))
				{
					alert("Item already exist in list.");
					return;
				}
				if( parseInt(rec.recordset("RISKLEVELID"))==parseInt(frmHealth.sel_ASBRISKLEVEL.value))			
				{
                    if( parseInt(rec.recordset("ELEMENTID"))==parseInt(frmHealth.sel_ASBESTOS.value))			
				    {
				    //alert("in recordno change")
				    RecordNo=parseInt(rec.recordset("RECORDNO"))+1;
				    //alert(RecordNo)
				    }
				    LevelRecordNo=parseInt(rec.recordset("LEVELRECORDNO"))+1;
				    //alert("New RCORD LevelNO:"+LevelRecordNo)
				    //break;
    			}
				rec.recordset.movenext();
			//}
		}
		//getDetails();
		rec.recordset.AddNew();
		
		rec.recordset("RISKLEVELID")=frmHealth.sel_ASBRISKLEVEL.value;
        rec.recordset("RISK") = frmHealth.sel_ASBRISK.value;
		rec.recordset("DATEADDED") = frmHealth.txt_AsbestosAdded.value;
		rec.recordset("DATEREMOVED") = frmHealth.txt_AsbestosRemoved.value;
        rec.recordset("USERID") = <%=Session("UserID")%>;
        rec.recordset("USER") = "";		
        rec.recordset("NOTES") = frmHealth.txt_Notes.value;
		rec.recordset("RISKLEVEL")=frmHealth.sel_ASBRISKLEVEL.options[frmHealth.sel_ASBRISKLEVEL.selectedIndex].text ;
		rec.recordset("ELEMENTID")=frmHealth.sel_ASBESTOS.value;
        rec.recordset("ELEMENTDESCRIPTION")=frmHealth.sel_ASBESTOS.options[frmHealth.sel_ASBESTOS.selectedIndex].text;		
		rec.recordset("PROPERTYID")=frmHealth.PropertyID.value;
		rec.recordset("OTHER")=frmHealth.txt_OTHER.value;    	
		rec.recordset("LEVELRECORDNO")=LevelRecordNo;
        rec.recordset("RECORDNO")=RecordNo;

		if (parseInt(frmHealth.sel_ASBESTOS.value)==14)
		{
			rec.recordset("ELEMENTDESCRIPTION")=frmHealth.txt_OTHER.value;
		}

        rec.recordset.movefirst();
		getDetails();
		cnt=cnt-1;

		frmHealth.sel_ASBESTOS.value = "";
		frmHealth.sel_ASBRISK.value = "";
		frmHealth.sel_ASBRISKLEVEL.value = "";
		frmHealth.txt_AsbestosAdded.value = "";
		frmHealth.txt_AsbestosRemoved.value = "";
        frmHealth.txt_Notes.value = "";
		//return;

        save();
        window.location.reload(false);
}

//Function to retrieve value for edit of record whenever a record is selected
function edit(ElementId,Risk,RiskLevelId)
{
        frmHealth.txt_AsbestosRemoved.disabled = false;
        frmHealth.btnCancel.disabled = false;
        frmHealth.btnAmmend.disabled = true;

		var findElement = parseInt(ElementId);
		var findRisk = Risk;
		var findRiskLevel = RiskLevelId;

		rec.recordset.movefirst();

		selectedRiskRec = findRisk;
		selectedElementRec = findElement;
		selectedRiskLevelRec = findRiskLevel

		while(!rec.recordset.EOF)
		{
			if(parseInt(rec.recordset("ELEMENTID"))==findElement && rec.recordset("RISK")==findRisk)
			{
				frmHealth.sel_ASBESTOS.value = parseInt(rec.recordset("ELEMENTID"));
			  	frmHealth.sel_ASBRISK.value = rec.recordset("RISK");
			  	frmHealth.sel_ASBRISKLEVEL.value = rec.recordset("RISKLEVELID");
			  	frmHealth.txt_AsbestosAdded.value = rec.recordset("DATEADDED");
			  	frmHealth.txt_AsbestosRemoved.value = rec.recordset("DATEREMOVED");
                frmHealth.txt_Notes.value = rec.recordset("NOTES");
			}
				rec.recordset.movenext();
		}
		findElement = -1;
		frmHealth.btnAdd.disabled = true;
		frmHealth.btnAmmend.disabled = false;

		return;
}

function CancelRec()
{
    frmHealth.btnCancel.disabled = true;
    frmHealth.btnAmmend.disabled = true;
    frmHealth.btnAdd.disabled = false;
    frmHealth.sel_ASBESTOS.value = "";
    frmHealth.sel_ASBRISK.value = "";
    frmHealth.sel_ASBRISKLEVEL.value = "";
    frmHealth.txt_AsbestosAdded.value = "";
    frmHealth.txt_AsbestosRemoved.value = "";
    frmHealth.txt_Notes.value = "";
}

//Funcion to Ammend data in the database when Ammend button is used
function AmmendRec()
{
		var LevelRecordNo, RecordNo;
		LevelRecordNo=1;
		RecordNo=1;
		rec.recordset.movefirst();

		/*
        if(frmHealth.sel_ASBRISKLEVEL.value == "")
		{
		    alert("Please select the value from Asbestos Level");
			return;
		}

		if (frmHealth.sel_ASBESTOS.value == "")
		{
			alert("Please select the value from Asbestos Elements");
			return;
		}

		if(frmHealth.sel_ASBRISK.value == "") 
		{
			alert("Please select the value from Risk");
			return;
		}

        if (frmHealth.txt_AsbestosAdded.value == "") {
            alert("Please add a date to the 'added field'");
        return;
        }
        */

        FormFields[0] = "sel_ASBRISKLEVEL|Asbestos Level|SELECT|Y"
        FormFields[1] = "sel_ASBESTOS|Asbestos Element|SELECT|Y"
        FormFields[2] = "sel_ASBRISK|Risk|SELECT|Y"
        FormFields[3] = "txt_AsbestosAdded|Added|DATE|Y"
        FormFields[4] = "txt_AsbestosRemoved|Removed|DATE|N"
        FormFields[5] = "txt_Notes|Notes|TEXT|N"
        FormFields[6] = "txt_OTHER|Other|TEXT|N"

        if (!checkForm()) return false;

		while(!rec.recordset.EOF)
		{
//			if( parseInt(rec.recordset("ELEMENTID"))==parseInt(frmHealth.sel_ASBESTOS.value) && rec.recordset("RISK")==frmHealth.sel_ASBRISK.value && parseInt(rec.recordset("RISKLEVELID"))==parseInt(frmHealth.sel_ASBRISKLEVEL.value))
//			{
//                alert("Item already exist in list.");
//                return;
				/*if(parseInt(rec.recordset("RISKLEVELID"))!=parseInt(frmHealth.sel_ASBRISKLEVEL.value))
				{
				    alert("Only Risk Level cannot be ammended.To change Risk Level delete record and create again");
				    frmHealth.btnAdd.disabled=false;
		            frmHealth.btnAmmend.disabled=true;
		            frmHealth.sel_ASBESTOS.value="";
		            frmHealth.sel_ASBRISK.value="";
		            frmHealth.sel_ASBRISKLEVEL.value="";
				    return
				}
				else
				{*/
//				    alert("Item already exist in list.");
//				    frmHealth.btnAdd.disabled = false;
//		            frmHealth.btnAmmend.disabled = true;
//		            frmHealth.sel_ASBESTOS.value = "";
//		            frmHealth.sel_ASBRISK.value = "";
//		            frmHealth.sel_ASBRISKLEVEL.value = "";
//                  frmHealth.txt_AsbestosAdded.value = "";
//                  frmHealth.txt_AsbestosRemoved.value = "";
//                  frmHealth.txt_Notes.value = "";
//					return;
				//}
//			}

			if( parseInt(rec.recordset("ELEMENTID"))==selectedElementRec && rec.recordset("RISK")==selectedRiskRec && parseInt(rec.recordset("RISKLEVELID"))==selectedRiskLevelRec)
			{
			    deleteRecord(selectedElementRec,selectedRiskRec,selectedRiskLevelRec,"0");
			    getDetails();

                if(!rec.recordset.BOF){ rec.recordset.movefirst();}
			    //rec.recordset.movefirst();
	            continue;
	            //break;
			}
			if( parseInt(rec.recordset("ELEMENTID"))==parseInt(frmHealth.sel_ASBESTOS.value) && parseInt(rec.recordset("RISKLEVELID"))==parseInt(frmHealth.sel_ASBRISKLEVEL.value) )			
			{
				    //alert("amdrecordno:"+rec.recordset("RECORDNO"))
				    RecordNo=parseInt(rec.recordset("RECORDNO"))+1;
				    //alert("amdrecordno:"+RecordNo)
		    }
			if( parseInt(rec.recordset("RISKLEVELID"))==parseInt(frmHealth.sel_ASBRISKLEVEL.value))
			{
			        //alert("amdLevelNo:"+LevelRecordNo)
				    LevelRecordNo=parseInt(rec.recordset("LEVELRECORDNO"))+1;
				    //alert("amdLevelNo:"+LevelRecordNo)
    		}

				rec.recordset.movenext();
		}

				rec.recordset.AddNew();
				rec.recordset("LEVELRECORDNO") = LevelRecordNo;
				rec.recordset("RECORDNO") = RecordNo;
				rec.recordset("ELEMENTDESCRIPTION") = frmHealth.sel_ASBESTOS.options[frmHealth.sel_ASBESTOS.selectedIndex].text;
				rec.recordset("RISK") = frmHealth.sel_ASBRISK.value;
				rec.recordset("RISKLEVEL") = frmHealth.sel_ASBRISKLEVEL.options[frmHealth.sel_ASBRISKLEVEL.selectedIndex].text;
				rec.recordset("ELEMENTID") = frmHealth.sel_ASBESTOS.value;
				rec.recordset("RISKLEVELID") = frmHealth.sel_ASBRISKLEVEL.value;
				rec.recordset("PROPERTYID") = frmHealth.PropertyID.value;
				rec.recordset("OTHER") = frmHealth.txt_OTHER.value;
                rec.recordset("DATEADDED") = frmHealth.txt_AsbestosAdded.value;
                rec.recordset("DATEREMOVED") = frmHealth.txt_AsbestosRemoved.value;
                rec.recordset("NOTES") = frmHealth.txt_Notes.value;
                rec.recordset("USER") = "<%=left(Session("FirstName"),1) & left(Session("LastName"),1) %>";
                rec.recordset("USERID") = <%=Session("UserID")%>;

				if (parseInt(frmHealth.sel_ASBESTOS.value) == 15)
		        {
			        rec.recordset("ELEMENTDESCRIPTION") = frmHealth.txt_OTHER.value;
		        }

		        getDetails();
				//rec.recordset.movelast();
				//this.getDetails();

		//this.getDetails();

		//AddRec();
        //this.getDetails();
        //this.save();

        //this.getDetails();
        //deleteRecord(selectedElementRec,selectedRiskRec,"0");
    	//t/his.getDetails();

		alert("Item Successfully Ammended");

		frmHealth.btnAdd.disabled = false;
		frmHealth.btnAmmend.disabled = true;
		frmHealth.sel_ASBESTOS.value = "";
		frmHealth.sel_ASBRISK.value = "";
		frmHealth.sel_ASBRISKLEVEL.value = "";
        frmHealth.txt_AsbestosAdded.value = "";
        frmHealth.txt_AsbestosRemoved.value = "";
        frmHealth.txt_Notes.value = "";

		//return;
        save();
}

//Funcion to save changes when ever the record is edited, added or deleted
function save()
{
		var xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj = new ActiveXObject("Microsoft.XMLDOM");

		url="/Includes/xmlServer/xmlsrv.asp?id=0&task=SAVEASBRISK";

		if( parseInt(rec.XMLDocument.documentElement.childNodes.length)==0 )
		{
			alert("Please add some item before saving.");
			return;
		}

		xmlHttp.open("POST", url, false);
		xmlHttp.setRequestHeader( "Content-Type","text/xml");
		xmlHttp.send(rec.XMLDocument);

		xmlobj=xmlHttp.responseXML;
		var msg=xmlobj.xml;

	    //if(flag==1)
	    //{
	    //  alert(xmlobj.selectSingleNode("output").text);
		//}

	return;
}

function numbersonly(e)
{
		var str = frmHealth.sel_ASBRISK.value;
		frmHealth.sel_ASBRISK.value = str.toUpperCase();
}

function limitText(textArea, length) {
    if (textArea.value.length > length) {
        textArea.value = textArea.value.substr(0,length);
    }
}

function ffDisabled(el)
{
    if (document.getElementById(el).disabled == false) { 
        YY_Calendar('txt_AsbestosRemoved',113,50,'de','#FFFFFF','#133E71','YY_calendar1');
    }
}

</script>
</head>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(0);preloadImages();getDetails();" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<object id="rec" classid="clsid:550dda30-0541-11d2-9ca9-0060b0ec3d39" width="0" height="0">
	<CATALOG>
		<%=xmlString%>
	</CATALOG>
</object>

<xml id="emptyxml">
	<CATALOG>
		<DATA>
			<PROPASBESTOSRISK></PROPASBESTOSRISK>
			<PROPASBESTOSLEVELID></PROPASBESTOSLEVELID>
			<RISKLEVELID></RISKLEVELID>
            <RISKLEVEL></RISKLEVEL>
            <RISK></RISK>
            <DATEADDED></DATEADDED>
            <DATEREMOVED></DATEREMOVED>
            <USERID></USERID>
            <USER></USER>
            <NOTES></NOTES>
            <ELEMENTID></ELEMENTID>
            <ELEMENTDESCRIPTION></ELEMENTDESCRIPTION>
            <PROPERTYID></PROPERTYID>
            <LEVELRECORDNO></LEVELRECORDNO>
            <RECORDNO></RECORDNO>
            <OTHER></OTHER>
        </DATA>
    </CATALOG>
</xml>

<XML ID="XSLStyle1">
	<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        <xsl:template match="/">
			<table style="border-bottom-width: 1px" width="750" align="left" cellpadding="2" cellspacing="0" id="table2">
					<tr style="border-bottom: #996699 1px solid;" bgcolor="beige">
					    <td width="240" class="TABLE_HEAD" bgcolor="beige"><b>RISK LEVEL</b></td>
						<td width="240" class="TABLE_HEAD" bgcolor="beige"><b>ELEMENT</b></td>
						<td width="50" class="TABLE_HEAD" bgcolor="beige"><b>RISK</b></td>
                        <td width="80" class="TABLE_HEAD" bgcolor="beige"><b>ADDED</b></td>
                        <td width="80" class="TABLE_HEAD" bgcolor="beige"><b>REMOVED</b></td>
                        <td width="50" class="TABLE_HEAD" bgcolor="beige"><b>USER</b></td>
						<!-- <td width="10" class="TABLE_HEAD" bgcolor="beige"></td> //-->
					</tr>
					<xsl:for-each select="CATALOG/DATA">
					        <xsl:sort select="RISKLEVEL" order="ascending"/>		        				        
					        <xsl:sort select="LEVELRECORDNO" order="ascending"/>
					        <xsl:sort select="RECORDNO" order="ascending"/>
					        <tr title="Click to edit" style="word-wrap:normal;cursor:pointer" onclick="edit('{ELEMENTID}','{RISK}','{RISKLEVELID}')">
                                 <td>
							         <xsl:if test="LEVELRECORDNO = 1">
							                <xsl:value-of select="RISKLEVEL" />
							         </xsl:if>  
							     </td>
							     <td>
							         <xsl:if test="RECORDNO = 1">
							                <!--<xsl:choose>
							                    <xsl:when test="ELEMENTID = 14">
							                        <xsl:value-of select="OTHER" />
							                    </xsl:when>
                                                <xsl:otherwise>-->
                                                    <xsl:value-of select="ELEMENTDESCRIPTION" />
                                                <!--</xsl:otherwise>
                                            </xsl:choose>-->
    						         </xsl:if>
                                </td>
							    <td><xsl:value-of select="RISK" /></td>
                                <td><xsl:value-of select="DATEADDED" /></td>
                                <td><xsl:value-of select="DATEREMOVED" /></td>
                                <td><xsl:value-of select="USER" /></td>
							    <!--
                                <td width="10" bgcolor="white">
							        <img src="/myImages/x.gif" onclick="event.cancelBubble=true;deleteit('{ELEMENTID}','{RISK}','{RISKLEVELID}')" alt="Click to delete"></img>
							    </td>
                                -->
						    </tr>
					</xsl:for-each>
			</table >
		</xsl:template>
    </xsl:stylesheet>
</XML>
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<!--<form name="frmHealth" action="Serverside\hands_srv.asp" method="post">-->
    <form name="frmHealth" action="" method="post">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0" height="1">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img alt="" name="tab_main_details" src="images/tab_main_details.gif" width="97"
                        height="20" border="0" /></a>
            </td>
            <td rowspan="2">
                <img alt="" name="tab_financial" src="images/2-closed.gif" width="79" height="20"
                    border="0" />
            </td>
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img alt="" name="tab_warranties" src="images/tab_warranties.gif" width="89" height="20"
                        border="0" /></a>
            </td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img alt="" name="tab_utilities" src="images/tab_utilities.gif" width="68" height="20"
                        border="0" /></a>
            </td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img alt="" name="tab_marketing" src="images/tab_marketing.gif" width="76" height="20"
                        border="0" /></a>
            </td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>">
                    <img alt="" name="tab_hands" src="images/tab_healthsafety.gif" width="110" height="20"
                        border="0" /></a>
            </td>
            <td>
                <img alt="" src="/myImages/spacer.gif" width="151" height="19" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img alt="" src="images/spacer.gif" width="151" height="1" /></td>
        </tr>
    </table>
    <div id="Calendar1" style="background-color: white; position: absolute; left: 1px;
        top: 1px; width: 200px; height: 115px; z-index: 20; visibility: hidden">
    </div>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style="height: 7px">
            <td colspan="8">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                &nbsp;Asbestos Level:
            </td>
            <td>
                <%=lstASBRISKLEVEL%>
                <img src="/js/FVS.gif" name="img_ASBRISKLEVEL" id="img_ASBRISKLEVEL"
                                width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Added:
            </td>
            <td>
                <input type="text" id="txt_AsbestosAdded" name="txt_AsbestosAdded" value="" maxlength="10"
                    class="textbox" style="width: 75px" />
                <a href="javascript:;" onclick="YY_Calendar('txt_AsbestosAdded',113,50,'de','#FFFFFF','#133E71','YY_calendar1')">
                    <span style="color: #003333; font-weight: bold;">#</span></a>
                    
            </td>
            <td><img src="/js/FVS.gif" name="img_AsbestosAdded" id="img_AsbestosAdded"
                                width="15px" height="15px" border="0" alt="" /></td>
            <td nowrap="nowrap">
                Removed:
            </td>
            <td>
                <input type="text" id="txt_AsbestosRemoved" name="txt_AsbestosRemoved" value="" maxlength="10"
                    class="textbox" style="width: 75px" disabled="disabled" />
                <a href="javascript:;" onclick="ffDisabled('txt_AsbestosRemoved')">
                    <span style="color: #003333; font-weight: bold;">#</span></a>
                    
            </td>
            <td><img src="/js/FVS.gif" name="img_AsbestosRemoved" id="img_AsbestosRemoved"
                                width="15px" height="15px" border="0" alt="" /></td>
        </tr>
        <tr style="visibility: hidden" id="spother">
            <td nowrap="nowrap">
                Other :
            </td>
            <td colspan="7">
                <input type="text" name="txt_OTHER" class="textbox200" maxlength="100" value="<%=l_OTHER%>"
                    tabindex="4" />
                    <img src="/js/FVS.gif" name="img_OTHER" id="img_OTHER"
                                width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                &nbsp;Asbestos Elements :
            </td>
            <td>
                <%=lstASBESTOS%>
                <img src="/js/FVS.gif" name="img_ASBESTOS" id="img_ASBESTOS"
                                width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Notes:
            </td>
            <td colspan="5" rowspan="2">
                <textarea id="txt_Notes" name="txt_Notes" class="textbox200" rows="4" cols="10" style="width: 100%"
                    onkeypress="limitText(this,199);"></textarea>
                    <img src="/js/FVS.gif" name="img_Notes" id="img_Notes"
                                width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                &nbsp;Risk :
            </td>
            <td>
                <%=lstASBRISK%>
                <img src="/js/FVS.gif" name="img_ASBRISK" id="img_ASBRISK"
                                width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td colspan="7">
                <input type="button" value=" Add " name="btnAdd" class="RSLButton" onclick="AddRec();" />
                &nbsp;&nbsp;
                <input type="button" value=" Amend " name="btnAmmend" class="RSLButton" onclick="AmmendRec();"
                    disabled="disabled" />
                    &nbsp;&nbsp;
                <input type="button" value=" Cancel " name="btnCancel" class="RSLButton" onclick="CancelRec();"
                    disabled="disabled" />
            </td>
        </tr>
        <tr style="color: white" bgcolor="#004174">
            <td colspan="8">
                <b>Details</b>
            </td>
        </tr>
        <tr>
            <td colspan="8" align="center">
                <div align="center" id="divResults">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" name="PropertyID" value="<%=PropertyID%>" />
    </form>
    <div id="TOP_DIV" style="display: block; overflow: hidden">
        <iframe name="HEALTH_AND_SAFETY_BOTTOM_FRAME" src="iframes/iRefurbishment.asp?propertyid=<%=PropertyID%>"
            style="overflow: hidden" frameborder="0" width="760px" height="350"></iframe>
    </div>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
