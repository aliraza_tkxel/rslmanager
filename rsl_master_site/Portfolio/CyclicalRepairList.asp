<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderFlexible.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Function CylicalNumber (strWord)
		CylicalNumber = "<b>CYC</b> " & characterPad(strWord,"0", "l", 6)
	End Function
			
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (12)	 'USED BY CODE
	Dim DatabaseFields (12)	 'USED BY CODE
	Dim ColumnWidths   (12)	 'USED BY CODE
	Dim TDSTUFF        (12)	 'USED BY CODE
	Dim TDPrepared	   (12)	 'USED BY CODE
	Dim ColData        (12)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (12)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (12)	 'All Array sizes must match
	Dim TDFunc         (12)	 'stores any functions that will be applied
	Dim SubQuery			 'used to get the name of the parent object
	Dim SubQueryTitle		 'the title of the parent object
	
	ColData (0) = "Cyclical ID|PMID|100"
	SortASC (0)	= "p.PMID ASC"
	SortDESC(0) = "p.PMID DESC"
	TDSTUFF (0) = " "" NOWRAP "" "
	TDFunc  (0) = "CylicalNumber(|)"

	ColData (1) = "Type|TYPE|150"
	SortASC (1)	= "TYPE ASC"
	SortDESC(1) = "TYPE DESC"
	TDSTUFF (1) = " "" NOWRAP "" "
	TDFunc  (1) = ""

	ColData (2) = "Name|TYPENAME|250"
	SortASC (2)	= "TYPENAME ASC"
	SortDESC(2) = "TYPENAME DESC"	
	TDSTUFF (2) = " "" NOWRAP "" "
	TDFunc  (2) = ""		

	ColData (3) = "Start Date|CSTART|100"
	SortASC (3)	= "P.STARTDATE ASC"
	SortDESC(3) = "P.STARTDATE DESC"	
	TDSTUFF (3) = " "" NOWRAP "" "
	TDFunc  (3) = ""		

	ColData (4) = "Cycle|CCYCLE|100"
	SortASC (4)	= "P.CYCLE ASC"
	SortDESC(4) = "P.CYCLE DESC"	
	TDSTUFF (4) = " "" NOWRAP "" "
	TDFunc  (4) = ""		

	ColData (5) = "Frequency|FREQUENCY|100"
	SortASC (5)	= "P.FREQUENCY ASC"
	SortDESC(5) = "P.FREQUENCY DESC"	
	TDSTUFF (5) = " "" NOWRAP "" "
	TDFunc  (5) = ""		

	ColData (6) = "Cyclical Repair|CREPAIR|240"
	SortASC (6)	= "CREPAIR ASC"
	SortDESC(6) = "CREPAIR DESC"	
	TDSTUFF (6) = " "" NOWRAP "" "	
	TDFunc  (6) = ""
	
	ColData (7) = "Last Cycle|CLAST|100"
	SortASC (7)	= "CLAST ASC"
	SortDESC(7) = "CLAST DESC"	
	TDSTUFF (7) = " "" NOWRAP "" "	
	TDFunc  (7) = ""

	ColData (8) = "Next Cycle|NEXTCYCLE|100"
	SortASC (8)	= "NEXTCYCLE ASC"
	SortDESC(8) = "NEXTCYCLE DESC"	
	TDSTUFF (8) = " "" NOWRAP "" "	
	TDFunc  (8) = ""
	
	ColData (9) = "Next Insert|NEXTINSERT|110"
	SortASC (9)	= "NEXTINSERT ASC"
	SortDESC(9) = "NEXTINSERT DESC"	
	TDSTUFF (9) = " "" NOWRAP "" "	
	TDFunc  (9) = ""
	
	ColData (10) = "Cost|REPAIRNETCOST|100"
	SortASC (10)	= "REPAIRNETCOST ASC"
	SortDESC(10) = "REPAIRNETCOST DESC"	
	TDSTUFF (10) = " "" NOWRAP align=right "" "	
	TDFunc  (10) = "FormatCurrency(|)"
	
	ColData (11) = "Ran|TOTALOCCURENCES|100"
	SortASC (11) = "TOTALOCCURENCES ASC"
	SortDESC(11) = "TOTALOCCURENCES DESC"	
	TDSTUFF (11) = " "" NOWRAP align=center "" "	
	TDFunc  (11) = ""				

	ColData (12) = "Status|CURRENTSTATUS|100"
	SortASC (12) = "CURRENTSTATUS ASC"
	SortDESC(12) = "CURRENTSTATUS DESC"	
	TDSTUFF (12) = " "" NOWRAP align=center "" "	
	TDFunc  (12) = ""				

	PageName = "CyclicalRepairList.asp"
	EmptyText = "No cyclical repairs found in the system."
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me('"" & rsSet(""PMID"") & ""')"""" """ 

	THE_TABLE_HIGH_LIGHT_COLOUR = "STEELBLUE WIDTH=1650PX"
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE = "SELECT  P.PMID, " &_ 
				"CASE WHEN P.BLOCK IS NULL THEN 'Development' " &_
				"ELSE 'Block' " &_
				"END AS TYPE, " &_ 
				"CASE WHEN P.BLOCK IS NULL THEN D.DEVELOPMENTNAME " &_
				"ELSE DD.DEVELOPMENTNAME + ' -> ' +  B.BLOCKNAME " &_
				"END AS TYPENAME, " &_
				"PM_ACTIVE, " &_ 
				"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_ 
				"ISNULL(P.CYCLE,'') AS CCYCLE, P.FREQUENCY, " &_ 
				"CREPAIR = CASE WHEN LEN(R.DESCRIPTION) > 35 THEN LEFT(R.DESCRIPTION,35) + '...' ELSE R.DESCRIPTION END, " &_ 
				"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST, " &_	 
				"ISNULL(S.NAME,'') AS CONTRACTOR, " &_ 
				"R.DESCRIPTION, " &_ 
				"CURRENTSTATUS = CASE " &_  
				"WHEN PM_ACTIVE = 0 THEN 'INACTIVE' " &_ 
				"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'COMPLETED' " &_ 					
				"ELSE 'ACTIVE' " &_ 
				"END, " &_ 
				"NEXTCYCLE = CASE " &_  
				"WHEN PM_ACTIVE = 0 THEN 'N/A' " &_ 
				"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'N/A' " &_ 					
				"WHEN LASTEXECUTED = '1 JULY 1970' THEN 'NOT STARTED' " &_ 					
				"ELSE CONVERT(NVARCHAR, DATEADD(M, P.CYCLE, LASTEXECUTED), 103) " &_ 					
				"END, " &_ 
				"NEXTINSERT = CASE " &_  
				"WHEN PM_ACTIVE = 0 THEN 'N/A' " &_ 
				"WHEN DATEADD(MONTH, 1, GETDATE()) > DATEADD(MONTH, P.FREQUENCY * P.CYCLE, P.STARTDATE) THEN 'N/A' " &_ 					
				"WHEN LASTEXECUTED = '1 JULY 1970' THEN 'NOT STARTED' " &_ 					
				"ELSE CONVERT(NVARCHAR, DATEADD(M, P.CYCLE-1, LASTEXECUTED), 103) " &_ 					
				"END,  " &_
				"(SELECT ISNULL(SUM(REPAIRNETCOST),0) AS REPAIRNETCOST FROM P_PLANNEDINCLUDES PINC WHERE PINC.PMID = P.PMID) AS REPAIRNETCOST, " &_ 
				"(SELECT ISNULL(COUNT(RUNID),0) AS TOTAL FROM P_RUNNINGMAINTENANCE RUNM WHERE RUNM.PMID = P.PMID) AS TOTALOCCURENCES " &_ 
			"FROM 	P_PLANNEDMAINTENANCE P " &_ 
			"	LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
			"	LEFT JOIN P_DEVELOPMENT DD ON B.DEVELOPMENTID = DD.DEVELOPMENTID " &_
			"	LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"	INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
			"	INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID " &_
			"ORDER BY " + Replace(orderBy, "'", "''") + ""

	// response.write SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --> Cyclical Repair List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(PMID){
	TableData.innerHTML = "<i><b>Please wait loading...</b></i>"
	THISFORM.PMID.value = PMID
	THISFORM.action = "ServerSide/CyclicalRepairDetails.asp"
	THISFORM.submit()
	}

function PO(Order_id){
	window.showModelessDialog("/finance/Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY onload="window.focus()" BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
Click on a row to get the work order and purchase order numbers.
<FORM NAME="THISFORM" TARGET="ServerInfo" method="POST">
<TABLE WIDTH=1650 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
<input type="hidden" name="PMID">
</FORM>
<DIV ID="TableData">
</DIV>
<iframe src="/secureframe.asp" name="ServerInfo" style='display:none'></iframe>
</BODY>
</HTML>