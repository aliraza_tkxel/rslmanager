<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue
	Dim change_of_tenancy, day_to_day, services, decorating, gas, timber, total
	DIM TOTAL_REPAIRS ,TOTAL_ACHIEVED, TOTAL_NOTACHIVED, THE_KPI_FIG 
	Redim KPIVALUES(MONTHCOUNT)
	Redim INTFRAME(MONTHCOUNT)
	Redim TOTALREPAIRS(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	total = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
		KPIVALUES(MONTHCOUNT) = 0
		INTFRAME(MONTHCOUNT) = 0
		TOTALREPAIRS(MONTHCOUNT) = 0
	next
	
	' GET MONTHS PASSED FOR KPI CALCULATION
	If month(date) < 4 Then months_passed = month(date) + 9 Else months_passes = month(date) - 3 End If
	
	OpenDB()

	get_fiscal_boundary()
	
	Dmonth = -1
	DYear = -1
	
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
	End If
	
	Call get_kpi_header(TID)
	Call get_monthly_breakdown()
	Call build_fiscal_month_select(lstBox, Dmonth)

	for x = 0 to MONTHCOUNT 
		ytd_repairs = ytd_repairs + TOTALREPAIRS(x)
		ytd_intframe = ytd_intframe + INTFRAME(x)
		If TOTALREPAIRS(x) = 0 Then KPIVALUES(x) = 0 Else KPIVALUES(X) = ( (INTFRAME(x) / TOTALREPAIRS(x) )* 100 ) End If
	next

	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd)  Then
		A = ytd_repairs
		B = ytd_intframe
		C = ytd_repairs - ytd_intframe
		if ytd_repairs = 0 then kpi = 0 else kpi = ( (ytd_intframe / ytd_repairs )* 100 ) end if
	Else 
		A = TOTALREPAIRS(Dmonth)
		B = INTFRAME(Dmonth)
		C = TOTALREPAIRS(Dmonth) - INTFRAME(Dmonth)
		kpi = KPIVALUES(Dmonth)
	End If
	
	' GET FIGURES FOR MONTHLY BREAKDOWN
	Function get_monthly_breakdown()
	
		sql = 	"SELECT	COUNT(*) AS THECOUNT,MONTH(J.CREATIONDATE) AS MONTHNUMBER, " &_
				"	SUM(CASE WHEN (DATEDIFF(SS, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) <= 86400 " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS INTF, " &_
				"	SUM(CASE WHEN (DATEDIFF(SS, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) > 86400 " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS OUTTF " &_
				"FROM	C_JOURNAL J " &_
				"	LEFT JOIN C_MAXCOMPLETEDREPAIRDATE RC ON RC.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID " &_
				"	LEFT JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				"	LEFT JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				"	LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"	LEFT JOIN C_LASTCOMPLETEDACTION LC ON LC.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR GROUP BY JOURNALID, ITEMDETAILID) R ON R.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID " &_
				"	INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = R.ITEMDETAILID " &_
				"	INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY " &_
				"WHERE	I.PITYPE = 2 " &_
				"	AND REP.PRIORITY IN ('A','B') " &_
				"	AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) >= '" & FStart & "' AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) <= '" & FEnd & "' " &_
				"	AND  RP.ITEMACTIONID IN (6,9,10,15) " &_
				"	AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " &_
				"GROUP 	BY MONTH(J.CREATIONDATE) "

'				"	AND  J.CREATIONDATE >= '" & FStart & "' AND J.CREATIONDATE <= '" & FEnd & "' " &_

'
		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			'REPAIRVALUES(rsSet("MONTHNUMBER"))rsSet("MONTHNUMBER")) = ((rsSet("INTF")/ rsSet("THECOUNT"))*100)
			INTFRAME(rsSet("MONTHNUMBER")) = rsSet("INTF")
			TOTALREPAIRS(rsSet("MONTHNUMBER")) = rsSet("THECOUNT")
			rsSet.Movenext()
		Wend
	
	End Function
		
	' GET THE YEAR TO DATE REPAIR COSTS
	Function get_ytd_repair_cost()
	
		if not STARTDATE <> "" then
		STARTDATE = FStart
		end if
		
		if not ENDDATE <> "" then
		ENDDATE = FEnd
		end if
		
		sql = 	"SELECT	COUNT(*) AS THECOUNT, " &_
				"	SUM(CASE WHEN (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,RP.LASTACTIONDATE))) <= 1 " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS INTF, " &_
				"	SUM(CASE WHEN (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,RP.LASTACTIONDATE))) > 1 " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS OUTTF " &_
				"FROM	C_JOURNAL J " &_
				"	LEFT JOIN C_REPAIRCOMPLETION RC ON RC.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID " &_
				"	INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				"	INNER JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				"	LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"	INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR GROUP BY JOURNALID, ITEMDETAILID) R ON R.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID " &_				
				"	INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = R.ITEMDETAILID " &_
				"	INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY " &_
				"WHERE	I.PITYPE = 2 " &_
				"	AND REP.PRIORITY IN ('A','B') " &_
				"	AND ISNULL(RC.COMPLETIONDATE,RP.LASTACTIONDATE) >= '" & STARTDATE & "' AND INV.TAXDATE <= '" & ENDDATE & "' " &_
				"	AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " 
		
			Call OpenRs(rsSet, SQL)
			TOTAL_REPAIRS = isNullNumber(rsSet("THECOUNT"))
			TOTAL_ACHIEVED = isNullNumber(rsSet("INTF"))
			TOTAL_NOTACHIVED = isNullNumber(rsSet("OUTTF"))
			THE_KPI_FIG =  (rsSet("INTF")/ rsSet("THECOUNT"))*100
	
	End Function
	for x = 0 to MONTHCOUNT 
			'RW MONTHVALUES(x) & "<br>"
	next

	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI18.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3><b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=KPIVALUES(1)%>&FEB=<%=KPIVALUES(2)%>&MAR=<%=KPIVALUES(3)%>&APR=<%=KPIVALUES(4)%>&MAY=<%=KPIVALUES(5)%>&JUN=<%=KPIVALUES(6)%>&JUL=<%=KPIVALUES(7)%>&AUG=<%=KPIVALUES(8)%>&SEP=<%=KPIVALUES(9)%>&OCT=<%=KPIVALUES(10)%>&NOV=<%=KPIVALUES(11)%>&DEC=<%=KPIVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD></TD></TR>
				<TR><TD></TD><TD><b>Repairs</b></TD><TD></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD>
				<TD valign="top">(A) Notified with 'EMERGENCY' status<br> 
				  (A &amp; B) </TD>
				<TD ALIGN=RIGHT valign="top"><%=A%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD>
				<TD>(B) Completed within timeframe </TD>
				<TD ALIGN=RIGHT><%=B%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD>
				<TD>(C) Completed after timeframe </TD>
				<TD ALIGN=RIGHT><%=C%></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD ALIGN=RIGHT><b>KPI = </b></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=Formatnumber(kpi)%></b></TD></TR>
				<TD></TD></TR>
		  </TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
