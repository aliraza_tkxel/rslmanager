<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, kpi, selValue, divisor, opening_balance
	Dim A, B, C
	Dim ytd_let, ytd_available, ytd_unavailable  
	Redim KPIVALUES(MONTHCOUNT)
	Redim LET_VALUES(MONTHCOUNT)
	Redim AVAILABLE_VALUES(MONTHCOUNT)
	Redim UNAVAILABLE_VALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	ytd_let = 0
	ytd_available = 0
	ytd_unavailable = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPIVALUES(x) = 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		divisor = 1
	End If
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_end_of_month_property_count(2, LET_VALUES)
	Call get_end_of_month_property_count(1, AVAILABLE_VALUES)
	Call get_end_of_month_property_count(4, UNAVAILABLE_VALUES)
	for x = 0 to MONTHCOUNT 
			ytd_let = ytd_let + LET_VALUES(x)
			ytd_available = ytd_available + AVAILABLE_VALUES(x)
			ytd_unavailable = ytd_unavailable + UNAVAILABLE_VALUES(x)
			
			TempA = LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x)
			If TempA = 0 Then KPIVALUES(x) = 0 Else KPIVALUES(x) = (UNAVAILABLE_VALUES(x) / TempA) * 100 End If
	next

	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
		A = LET_VALUES(Month(date)) + AVAILABLE_VALUES(Month(date)) + UNAVAILABLE_VALUES(Month(date))
		B = LET_VALUES(Month(date))
		C = UNAVAILABLE_VALUES(Month(date))
		If A = 0 Then KPI = 0 Else KPI = (C / A) * 100 End If
	Else 
		A = LET_VALUES(DMonth) + AVAILABLE_VALUES(DMonth) + UNAVAILABLE_VALUES(DMonth)
		B = LET_VALUES(DMonth)
		C = UNAVAILABLE_VALUES(DMonth)
		If A = 0 Then KPI = 0 Else KPI = (C / A) * 100 End If
	End If
	
	CloseDB()
	
	Function get_end_of_month_property_count(ByRef p_status, ByRef PROPERTY_COUNT_VALUES)
	
		SQL = 	"SELECT 	PMONTH, AMOUNT " &_
				"FROM 		P_DAILYSTATUS S " &_
				"			INNER JOIN   (SELECT MONTH(DTIMESTAMP)AS PMONTH, MAX(DTIMESTAMP) AS PRE " &_
				"			   FROM P_DAILYSTATUS WHERE  ASSETTYPEID = 1 AND STATUSID = " & Cint(p_status) &_
				"			   AND ASSETTYPEID = 1 AND STATUSID = " & Cint(p_status) &_
				"		      AND DTIMESTAMP >= '" & FStart & "' AND DTIMESTAMP <= '" & FEnd & "' " &_
				"			  GROUP BY MONTH(DTIMESTAMP))  PP ON PP.PRE = S.DTIMESTAMP " &_
				"WHERE	 S.DTIMESTAMP >= '" & FStart & "' AND S.ASSETTYPEID = 1 AND S.DTIMESTAMP <= '" & FEnd & "' AND S.STATUSID = " & Cint(p_status) &_
				"ORDER   BY MONTH(DTIMESTAMP) "
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("PMONTH")) = rsSet("AMOUNT")
			rsSet.Movenext()
		Wend
	
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI10.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=KPIVALUES(1)%>&FEB=<%=KPIVALUES(2)%>&MAR=<%=KPIVALUES(3)%>&APR=<%=KPIVALUES(4)%>&MAY=<%=KPIVALUES(5)%>&JUN=<%=KPIVALUES(6)%>&JUL=<%=KPIVALUES(7)%>&AUG=<%=KPIVALUES(8)%>&SEP=<%=KPIVALUES(9)%>&OCT=<%=KPIVALUES(10)%>&NOV=<%=KPIVALUES(11)%>&DEC=<%=KPIVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD></TD></TR>
				<TR><TD></TD><TD><b>Properties</b></TD><TD></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(A) Total Number</TD><TD ALIGN=RIGHT><%=FormatNumber(A,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(B) Let</TD><TD ALIGN=RIGHT><%=FormatNumber(B,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(C) Unavailable To Let</TD><TD ALIGN=RIGHT><%=FormatNumber(C,2)%></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD ALIGN=RIGHT><b>KPI = </b></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:55PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD></TD><TD> calculation : (C / A) x 100</TD><TD></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
