<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, KPI, selValue, divisor, opening_balance
	Dim A, B, C
	Dim ytd_rentdue, ytd_initialrent, ytd_rebate, ytd_payment, ytd_arrears
	Redim RENTDUE_VALUES(MONTHCOUNT)
	Redim INITIALRENT_VALUES(MONTHCOUNT)
	Redim REBATE_VALUES(MONTHCOUNT)
	Redim PAYMENT_VALUES(MONTHCOUNT)
	Redim KPI_VALUES(MONTHCOUNT)
	Redim ARREARS_VALUES(MONTHCOUNT)

	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if

	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) = 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		divisor = 1
	End If
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_rent_due(RENTDUE_VALUES, 1) 
	Call get_rebate(REBATE_VALUES ,1) 
	Call get_initial_rent(INITIALRENT_VALUES, 1) 
	Call get_payments(PAYMENT_VALUES, 1)
	for x = 0 to MONTHCOUNT 
			ytd_rentdue = ytd_rentdue + RENTDUE_VALUES(x)
			ytd_initialrent = ytd_initialrent + INITIALRENT_VALUES(x)
			ytd_rebate = ytd_rebate + Abs(REBATE_VALUES(x)) 
			ytd_payment = ytd_payment + aBS(PAYMENT_VALUES(x))
			ARREARS_VALUES(x) = (RENTDUE_VALUES(X) + INITIALRENT_VALUES(X) - Abs(REBATE_VALUES(x)) ) - aBS(PAYMENT_VALUES(x))
			ytd_arrears = ytd_arrears + ARREARS_VALUES(x)
			'If ytd_arrears = 0 Then 
			If ( RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - Abs(REBATE_VALUES(x)) ) = 0 Then 
				KPI_VALUES(x) = 0 
			Else 
				KPI_VALUES(x) = ARREARS_VALUES(x) / ( ( RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - REBATE_VALUES(x) ) )  * 100 
			End If
	next
	'rw ytd_rentdue & "<BR>" & ytd_initialrent & "<BR>" & ytd_rebate
	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
		A = (ytd_rentdue + ytd_initialrent - ytd_rebate)
		B = ytd_payment
		C = ytd_arrears
		If C = 0 Then KPI = 0 Else KPI = (C / A) * 100 End If
	Else 
		A = (RENTDUE_VALUES(DMonth) + INITIALRENT_VALUES(DMonth) - - REBATE_VALUES(DMonth))
		B = aBS(PAYMENT_VALUES(DMonth))
		C = ARREARS_VALUES(DMonth)
		If C = 0 Then KPI = 0 Else KPI = (C / A) * 100 End If
	End If
	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI06.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD></TD></TR>
				<TR><TD></TD><TD>(A) Rent Due</TD><TD ALIGN=RIGHT><%=FormatCurrency(A,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(B) Rent Collected</TD><TD ALIGN=RIGHT><%=FormatCurrency(B,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(C) Amount of Arrears</TD><TD ALIGN=RIGHT><%=FormatCurrency(C,2)%></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD ALIGN=RIGHT><b>KPI = </b></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:85PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD></TD><TD> calculation : (C / A) x 100</TD><TD></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
