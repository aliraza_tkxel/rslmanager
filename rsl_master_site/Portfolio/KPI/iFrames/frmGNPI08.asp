<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, kpi, selValue, divisor
	Dim A, B, C, D, E, F, G
	Dim ytd_let, ytd_available, ytd_unavailable, ytd_rentdue, ytd_initialrent, ytd_rebate, ytd_voidrent, ytd_payment
	Redim MONTHVALUES(MONTHCOUNT)
	Redim LET_VALUES(MONTHCOUNT)
	Redim AVAILABLE_VALUES(MONTHCOUNT)
	Redim UNAVAILABLE_VALUES(MONTHCOUNT)
	Redim RENTDUE_VALUES(MONTHCOUNT)
	Redim INITIALRENT_VALUES(MONTHCOUNT)
	Redim REBATE_VALUES(MONTHCOUNT)
	Redim VOID_RENT_VALUES(MONTHCOUNT)	
	Redim PAYMENT_VALUES(MONTHCOUNT)
	Redim KPI_VALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	total = 0
	ytd_let = 0
	ytd_available = 0
	ytd_unavailable = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) = 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		divisor = 1
	End If
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_monthly_property_count(2, LET_VALUES)
	Call get_monthly_property_count(1, AVAILABLE_VALUES)
	Call get_monthly_property_count(4, UNAVAILABLE_VALUES)
	Call get_rent_due(RENTDUE_VALUES ,0) 
	Call get_initial_rent(INITIALRENT_VALUES ,0) 
	Call get_rebate(REBATE_VALUES ,0)
	Call get_void_rent(VOID_RENT_VALUES)
	Call get_payments(PAYMENT_VALUES ,0)
	for x = 0 to MONTHCOUNT 
			ytd_let = ytd_let + LET_VALUES(x)
			ytd_available = ytd_available + AVAILABLE_VALUES(x)
			ytd_unavailable = ytd_unavailable + UNAVAILABLE_VALUES(x)
			ytd_rentdue = ytd_rentdue + RENTDUE_VALUES(x)
			ytd_initialrent = ytd_initialrent + INITIALRENT_VALUES(x)
			ytd_rebate = ytd_rebate + Abs(REBATE_VALUES(x))
			ytd_voidrent = ytd_voidrent + VOID_RENT_VALUES(x)
			ytd_payment = ytd_payment + aBS(PAYMENT_VALUES(x))
			
			TempD = RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - Abs(REBATE_VALUES(x))
			TempE = VOID_RENT_VALUES(x) - INITIALRENT_VALUES(x) + Abs(REBATE_VALUES(x))
			'If TempD = 0 Or TempE = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = aBS(PAYMENT_VALUES(x))/(TempD + TempE + opening_balance) * 100 End If
			If TempD = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = (TempE/TempD) * 100 End If
	next
	'rw ytd_rentdue & "<BR>" & ytd_initialrent & "<BR>" & ytd_rebate
	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
		A = ytd_let/divisor
		B = ytd_available/divisor
		C = ytd_unavailable/divisor
		D = (ytd_rentdue + ytd_initialrent) - ytd_rebate
		E = ytd_voidrent - ytd_initialrent + ytd_rebate
		F = ytd_payment
		If D = 0 Then KPI = 0 Else KPI = (E / D) * 100 End If
	Else 
		A = LET_VALUES(DMonth)/divisor
		B = AVAILABLE_VALUES(DMonth)/divisor
		C = UNAVAILABLE_VALUES(DMonth)/divisor
		D = (RENTDUE_VALUES(DMonth) + INITIALRENT_VALUES(DMonth)) - Abs(REBATE_VALUES(DMonth))
		E = VOID_RENT_VALUES(DMonth) - INITIALRENT_VALUES(DMonth) + Abs(REBATE_VALUES(DMonth))
		F = aBS(PAYMENT_VALUES(DMonth))
		G = opening_balance
		If D = 0 Then KPI = 0 Else KPI = (E / D) * 100 End If
	End If
	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI08.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD></TD><TD COLSPAN=2><B>Properties (Average)</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(A) Let</TD><TD ALIGN=RIGHT><%=FormatNumber(A,2)%></TD></TR>
				<TR><TD></TD><TD>(B) Available to Let</TD><TD ALIGN=RIGHT><%=FormatNumber(B,2)%></TD></TR>
				<TR><TD></TD><TD>(C) Unavailable to Let</TD><TD ALIGN=RIGHT><%=FormatNumber(C,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD COLSPAN=2><B>Rent</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(D) Available</TD><TD ALIGN=RIGHT><%=FormatCurrency(D+E,2)%></TD></TR>
				<TR><TD></TD><TD>(E) Void Rent</TD><TD ALIGN=RIGHT><%=FormatCurrency(E,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD><b>KPI = </b></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:30PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>D = Total Income from A + B + C</TD><TD></TD></TR>
				<TR><TD></TD><TD>E = Total Income from B + C</TD><TD></TD></TR>
				<TR STYLE='HEIGHT:10PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD></TD><TD>calculation: (E / D) x 100</TD><TD></TD></TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
