<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Dim monthno, monthsql, cnt, total
	Dim MONTHNAMES
	MONTHNAMES = aRRAY ("YTD","January","February","March","April","May","June","July","August","September","October","November","December")
	monthno = Request("MONTHNO")
	IF monthno = "-1" Then 
		monthsql = " " 
		monthno = 0
	Else 
		monthsql = " AND MONTH(T.STARTDATE) = " & monthno 
	End If
	OpenDB()
	get_fiscal_boundary()
	cnt = 0
	total = 0

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<TITLE>Relet Properties</TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI11.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" CLASS='TA'>
<BR><TABLE align=center WIDTH=95% CELLPADDING=1 CELLSPACING=2 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=steelblue>
	<THEAD>
		<TR><td COLSPAN=2><%=MONTHNAMES(monthno)%></td><TD colspan=2 align=center><B>Dates</B></TD><TD align=right></TD></TR>
		<TR> 
			<TD><b>No</b></TD>
			<TD STYLE='WIDTH:230px' CLASS='TABLE_HEAD'>&nbsp;<B>Address</B></TD>
		  	<TD STYLE='WIDTH:90px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Termination</B></TD>
		  	<TD STYLE='WIDTH:90px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Relet</B></TD>
		  	<TD  NOWRAP STYLE='WIDTH:60px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Days</B></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR><TD COLSPAN=5>&nbsp;</TD></TR>
		<%
		
		SQL = 	"SELECT 	HOUSENUMBER + ' ' +  P.ADDRESS1 AS ADDRESS, " &_	
				"			OLD.ENDDATE, T.STARTDATE, T.PROPERTYID, " &_
				"			DATEDIFF (D, OLD.ENDDATE, T.STARTDATE) AS DAYS " &_
				"FROM 		C_TENANCY T " &_
				"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"			INNER JOIN C_TENANCY OLD ON OLD.PROPERTYID = T.PROPERTYID AND OLD.ENDDATE <= T.STARTDATE " &_
				"WHERE		P.ASSETTYPE = 1 AND T.STARTDATE > '"&FStart&"' AND OLD.ENDDATE > '"&FStart&"'  " &_
				"			AND T.STARTDATE <= '"&FEnd&"' " & monthsql &_
				"ORDER		BY T.STARTDATE "
		'rw sql		
		'Call OpenRs(rsSet, SQL)
		
		set dbcmd= server.createobject("ADODB.Command")
		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 4
		dbcmd.CommandText = "KPI_RELETDAYS_DETAIL"
		dbcmd.Parameters.Refresh
		dbcmd.Parameters(1) = FStart
		dbcmd.Parameters(2) = FEnd
		dbcmd.Parameters(3) = Request("MONTHNO")
		set rsSet=dbcmd.execute

		While not rsSet.EOF 
			cnt = cnt + 1
			total = total + rsSet("DURATION")
			Response.write "<TR><TD>" & CNT & "</TD>"
			Response.write "<TD>" & rsSet("DESCRIPTION") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("TERM") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("RELET") & "</TD>"
			Response.write "<TD ALIGN=RIGHT>" & rsSet("DURATION") & "</TD>"
			Response.write "</TR>"
			rsSet.Movenext()
		Wend		
		
		%>
		<TR>
			<TD COLSPAN=3 STYLE='BORDER-top:2PX SOLID #133E71' align=right>&nbsp;</TD>
			<TD STYLE='BORDER-top:2PX SOLID #133E71' align=right><B>(F)&nbsp;<%=CNT%></B>&nbsp;</TD>
			<TD STYLE='BORDER-top:2PX SOLID #133E71' align=right><B>(D)&nbsp;<%=total%></B></TD>
		</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
