﻿Imports System.Data.OleDb
Imports System.Data
Imports System.Xml

Partial Class Portfolio_OpenFile001
    Inherits System.Web.UI.Page

    Protected Sub form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles form1.Load
        OpenFileAsMe()
    End Sub

    Dim FilePath As String = "C:\Inetpub\sysdata\bha.rslmanager\Portfolio\rentIncreaseUploadFiles\OakLodge.xls"
    Dim SheetName As String = "[Sheet1$]"
    Dim ColumnsList As String = "*"

    Public Function OpenFileAsMe() As XmlDocument

        Dim excelCon As OleDbConnection = Nothing
        Dim dbcom As OleDbCommand = Nothing
        Dim dbadap As OleDbDataAdapter = Nothing
        Dim xlsDataSet As New DataSet()
        Dim xmldoc As New XmlDocument
        Try
            Dim con As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=""Excel 12.0;HDR=YES;"""
            Response.Write(con)
            If (String.IsNullOrEmpty(SheetName) = True) Then
                SheetName = "[Sheet1$]"
            End If
            If (String.IsNullOrEmpty(ColumnsList) = True) Then
                ColumnsList = "*"
            End If
            Dim sql As String = "SELECT " + ColumnsList + " FROM " + SheetName
            excelCon = New OleDbConnection(con)
            excelCon.Open()

            dbcom = New OleDbCommand(sql, excelCon)
            dbadap = New OleDbDataAdapter(dbcom)
            dbadap.Fill(xlsDataSet)

            Response.Write(xlsDataSet.GetXml())

            xmldoc.LoadXml(xlsDataSet.GetXml())
            Return xmldoc
        Catch ex As Exception
            Throw ex
        Finally
            xlsDataSet.Dispose()
            dbadap.Dispose()
            dbcom.Dispose()
            excelCon.Dispose()
        End Try
    End Function


End Class
