<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim lst_heating, lst_parking, lst_garden, lst_furnishing, lst_security, lst_as, lst_ACCESSIBILITYSTANDARDS
	Dim lst_kitchen, lst_scheme, lst_ss, lst_ads,l_address1

	PropertyID = Request("PropertyID")
	if (PropertyID = "") then PropertyID = -1 end if

	Call OpenDB()

		'--Retrieve First Line of Address
		SQL = "SELECT ADDRESS1,ISNULL(HOUSENUMBER,'') HOUSENUMBER,PROPERTYID FROM P__PROPERTY P LEFT JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
		Call OpenRs(rsAddress, SQL)
		    If (NOT rsAddress.EOF) Then
			    l_address1 = rsAddress("ADDRESS1")
		    Else
			    l_address1 = "[None Given]"
		    End If
		l_housenumber = rsAddress("HOUSENUMBER")
		l_propertyid = UCASE(rsAddress("PROPERTYID"))
		l_propertystatus = l_propertyid
		Call CloseRs(rsAddress)
		'--Retrieve First Line of Address

	ACTION = "NEW"

	SQL = "SELECT * FROM P_ATTRIBUTES WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		FLOORAREA = rsLoader("FLOORAREA")
		MAXPEOPLE = rsLoader("MAXPEOPLE")
		BEDROOMS = rsLoader("BEDROOMS")
		BEDROOMSITTINGROOM = rsLoader("BEDROOMSITTINGROOM")
		KITCHEN = rsLoader("KITCHEN")
		KITCHENDINER = rsLoader("KITCHENDINER")
		LIVINGROOM = rsLoader("LIVINGROOM")
		LIVINGROOMDINER = rsLoader("LIVINGROOMDINER")
		DININGROOM = rsLoader("DININGROOM")
		WC = rsLoader("WC")
		BATHROOM = rsLoader("BATHROOM")
		BATHROOMWC = rsLoader("BATHROOMWC")
		SHOWER = rsLoader("SHOWER")
		SHOWERWC = rsLoader("SHOWERWC")
		BOXROOMUTILITYROOM = rsLoader("BOXROOMUTILITYROOM")
		SHAREDKITCHENLIVING = rsLoader("SHAREDKITCHENLIVING")
		SHAREDWC = rsLoader("SHAREDWC")
		SHAREDSHOWERWC = rsLoader("SHAREDSHOWERWC")
		HEATING = rsLoader("HEATING")
		PARKING = rsLoader("PARKING")
		GARDEN = rsLoader("GARDEN")
		FURNISHING = rsLoader("FURNISHING")
		PETS = rsLoader("PETS")
		SECURITY = rsLoader("SECURITY")
		KITCHENDINERLOUNGE= rsLoader("KITCHENDINERLOUNGE")
		ACCESSIBILITYSTANDARDS = rsLoader("ACCESSIBILITYSTANDARDS")
        FRONTPORCH = rsLoader("FrontPorch")
        CONSERVATORYUPVC = rsLoader("ConservatoryUPVC")
        GARAGE = rsLoader("Garage")
		ACTION = "AMEND"
	End If

	' build page top select boxs
	Call BuildSelect(lst_heating, "sel_HEATING", "P_HEATING", "HEATINGID, DESCRIPTION", "DESCRIPTION", "Please Select", HEATING, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_garden, "sel_GARDEN", "P_GARDEN", "GARDENID, DESCRIPTION", "DESCRIPTION", "Please Select", GARDEN, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_furnishing, "sel_FURNISHING", "P_FURNISHING", "FURNISHINGID, DESCRIPTION", "DESCRIPTION", "Please Select", FURNISHING, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_security, "sel_SECURITY", "P_SECURITY", "SECURITYID, DESCRIPTION", "DESCRIPTION", "Please Select", SECURITY, "width:150px", "textbox200", NULL)
	Call BuildSelect(lst_ACCESSIBILITYSTANDARDS, "sel_ACCESSIBILITYSTANDARDS", "P_ACCESSIBILITYSTANDARDS", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", ACCESSIBILITYSTANDARDS, "width:200px", "textbox200", NULL)

	' build page botton select boxs
	Call BuildSelect(lst_kitchen, "sel_KITCHEN", "P_KITCHENAPPLIANCES KA WHERE NOT EXISTS (SELECT * FROM P_ATTTOKITCHEN AK WHERE AK.KID = KA.KID AND PROPERTYID = '" & PropertyID & "')", "KID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)	

	' GTE THE KITCHEN APPLIANCES...
	SQL = "SELECT KA.DESCRIPTION, KA.KID FROM P_ATTTOKITCHEN AK INNER JOIN P_KITCHENAPPLIANCES KA ON AK.KID = KA.KID WHERE PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString1 = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString1 = "<tr>"
		while NOT rsLoader.EOF
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString1 = TableString1 & "</tr><tr>"
			TableString1 = TableString1 & "<td width=""216"">" & rsLoader("DESCRIPTION") & "</td><td style=""cursor:pointer"" width=""30"" onclick=""DeleteKA(" & rsLoader("KID") & ")""><font color=""red"">DEL</font></td>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString1 = TableString1 & "<td colspan=""4""></td>"
		if (Remainder = 2) then TableString1 = TableString1 & "<td colspan=""2""></td>"
		TableString1 = TableString1 & "</tr>"
	else
		TableString1 = "<tr><td colspan=""6"" align=center>No items found</td></tr>"
	end if
	Call CloseRs(rsLoader)

	Call BuildSelect(lst_scheme, "sel_SCHEME", "P_SCHEMEFACILITIES SF WHERE NOT EXISTS (SELECT * FROM P_ATTTOSCHEME ASS WHERE ASS.SID = SF.FID AND PROPERTYID = '" & PropertyID & "')", "FID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL,NULL, "textbox200", NULL)

	' GTE THE SCHEME FACILITIES
	SQL = "SELECT SF.DESCRIPTION, SF.FID FROM P_ATTTOSCHEME ASS INNER JOIN P_SCHEMEFACILITIES SF ON ASS.SID = SF.FID WHERE PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString2 = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString2 = "<tr>"
		while NOT rsLoader.EOF
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString2 = TableString2 & "</tr><tr>"
			TableString2 = TableString2 & "<td width=""216"">" & rsLoader("DESCRIPTION") & "</td><td style=""cursor:pointer"" width=""30"" onclick=""DeleteSF(" & rsLoader("FID") & ")""><font color=""red"">DEL</font></td>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString2 = TableString2 & "<td colspan=""4""></td>"
		if (Remainder = 2) then TableString2 = TableString2 & "<td colspan=""2""></td>"
		TableString2 = TableString2 & "</tr>"
	else
		TableString2 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	end if
	Call CloseRs(rsLoader)

	Call BuildSelect(lst_ss, "sel_SS", "P_SUPPLIEDSERVICES SS WHERE NOT EXISTS (SELECT * FROM P_ATTTOSERVICES ASS WHERE ASS.SSID = SS.SID AND PROPERTYID = '" & PropertyID & "')", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)

	' GTE THE SUPPLIED SERVICES
	SQL = "SELECT SS.DESCRIPTION, SS.SID FROM P_ATTTOSERVICES ASS INNER JOIN P_SUPPLIEDSERVICES SS ON ASS.SSID = SS.SID WHERE PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString3 = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString3 = "<tr>"
		while NOT rsLoader.EOF 
			if (Counter Mod 3 = 0 and Counter <> 0) then	TableString3 = TableString3 & "</tr><tr>"
			TableString3 = TableString3 & "<td width=""216"">" & rsLoader("DESCRIPTION") & "</td><td style=""cursor:pointer"" width=""30"" onclick=""DeleteSS(" & rsLoader("SID") & ")""><font color=""red"">DEL</font></TD>"
			Counter = Counter + 1
			rsLoader.moveNext
		wend
		Remainder = Counter mod 3
		if (Remainder = 1) then TableString3 = TableString3 & "<td colspan=""4""></td>"
		if (Remainder = 2) then TableString3 = TableString3 & "<td colspan=""2""></td>"		
		TableString3 = TableString3 & "</tr>"
	else
		TableString3 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	end if
	Call CloseRs(rsLoader)
	
	Call BuildSelect(lst_ads, "sel_ADS", "P_ADAPTATIONCAT", "ADAPTATIONCATID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " onchange=""GetAdaptions()"" ")

	SQL = "SELECT AD.DESCRIPTION, AD.ADAPTATIONID FROM P_PORTFOLIOADAPTATIONS PA INNER JOIN P_ADAPTATIONS AD ON PA.ADAPTATIONID = AD.ADAPTATIONID WHERE PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString4 = ""
	If (NOT rsLoader.EOF) then
		Counter = 0
		TableString4 = "<tr>"
		While NOT rsLoader.EOF
			If (Counter Mod 3 = 0 And Counter <> 0) Then TableString4 = TableString4 & "</tr><tr>"
			TableString4 = TableString4 & "<td width=""216"">" & rsLoader("DESCRIPTION") & "</td><td style=""cursor:pointer"" width=""30"" onclick=""DeleteAD(" & rsLoader("ADAPTATIONID") & ")""><font color=""red"">DEL</font></td>"
			Counter = Counter + 1
			rsLoader.moveNext
		Wend
		Remainder = Counter Mod 3
		If (Remainder = 1) Then TableString4 = TableString4 & "<td colspan=""4""></td>"
		If (Remainder = 2) Then TableString4 = TableString4 & "<td colspan=""2""></td>"
		TableString4 = TableString4 & "</tr>"
	Else
		TableString4 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	End If
	Call CloseRs(rsLoader)

	'GTE THE ANNUAL SERVICINGS
	Call BuildSelect(lst_gs_appliancetype, "sel_gsappliancetype", "GS_APPLIANCE_TYPE", "APPLIANCETYPEID, APPLIANCETYPE", "APPLIANCETYPE", "Please Select", NULL,NULL, "textbox200", "style='margin-left:9px;'")    
	Call BuildSelect(lst_gs_location, "sel_gslocation", "GS_LOCATION", "LOCATIONID, LOCATION", "LOCATION", "Please Select", NULL, "width:200px", "textbox200", "style='margin-left:46px'")
	Call BuildSelect(lst_gs_manufacturer, "sel_gsmanufacturer", "GS_MANUFACTURER", "MANUFACTURERID, MANUFACTURER", "MANUFACTURER", "Please Select", NULL, "width:200px", "textbox200", "style='margin-left:20px;'")
	Call BuildSelect(lst_contractor_warranty, "sel_contractor_warranty", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", NULL, "width:250px", "textbox200", "style='margin-left:5px;'")


	ASSQL = "R_ITEMDETAIL R WHERE NOT EXISTS " &_
			  "(SELECT * FROM P_ANNUALSERVICINGS ANS WHERE R.ITEMDETAILID = ANS.REPAIRID AND PROPERTYID = '" & PropertyID & "')" &_
			  	"AND ISPROPERTYSPECIFIC = 1" ' DEFUNCT FOR TIME BEING AS WE CAN HAVE MORE THAN ONE FIRE ETC
	Call BuildSelect(lst_as, "sel_AS", "R_ITEMDETAIL WHERE ISPROPERTYSPECIFIC = 1 ", "ITEMDETAILID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:200' ")

	SQL = "SELECT PROPERTYAPPLIANCEID,AT.APPLIANCETYPE AS APPLIANCETYPE,AT.APPLIANCETYPEID,DATEINSTALLED,CP12NUMBER,ISSUEDATE,WARRANTYEXPIRYDATE,CP12DOCUMENT,DOCUMENTTYPE FROM GS_PROPERTY_APPLIANCE GS INNER JOIN GS_APPLIANCE_TYPE AT ON AT.APPLIANCETYPEID=GS.APPLIANCETYPEID WHERE GS.PROPERTYID=  '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString5 = ""
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString5 = "<tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"		
		while NOT rsLoader.EOF
		    TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("APPLIANCETYPE") & "</td>"
		    TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("DATEINSTALLED") & "</td>"
		    TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("CP12NUMBER") & "</td>"
		    TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("ISSUEDATE") & "</td>" 
			TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("WARRANTYEXPIRYDATE")  & "</td>"
			if(rsLoader("DOCUMENTTYPE")<>"") then
			    TableString5 = TableString5 & "<td style='cursor:pointer' onclick=""DisplayDoc( '" & PropertyID & "'," & rsLoader("APPLIANCETYPEID") & ")""><img alt='' src='/myImages/paper_clip.gif'/></td>"
			else
			    TableString5 = TableString5 & "<td style='cursor:pointer'  onclick=""LoadGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")""></td>"
			end if
			TableString5 = TableString5 & "<td style='cursor:pointer' width=30 onclick=""DeleteGS(" & rsLoader("PROPERTYAPPLIANCEID") & ")""><font color=""red"">DEL</font></td>"
			Counter = Counter + 1
			TableString5 = TableString5 & "</tr><tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"
			rsLoader.moveNext
		Wend
		Remainder = Counter Mod 3
		If (COUNTER = 0) Then
			TableString5 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
		End If
	End If
	Call CloseRs(rsLoader)
	
	
	'Get the Gas Appliances - Hussain
	
	Call BuildSelect(lst_gs_app_appliancetype, "sel_app_gsappliancetype", "GS_APPLIANCE_TYPE", "APPLIANCETYPEID, APPLIANCETYPE", "APPLIANCETYPE", "Please Select", NULL,NULL, "textbox200", "style='margin-left:9px;'")    
	Call BuildSelect(lst_gs_app_fueltype, "lst_gs_app_fueltype", "P_FUELTYPE", "FuelTypeId, FUELTYPE", "FUELTYPE", "Please Select", NULL,NULL, "textbox200", "style='margin-left:9px;'")    
	Call BuildSelect(lst_gs_app_location, "sel_app_gslocation", "GS_LOCATION", "LOCATIONID, LOCATION", "LOCATION", "Please Select", NULL, "width:200px", "textbox200", "style='margin-left:46px'")
	Call BuildSelect(lst_gs_app_manufacturer, "sel_app_gsmanufacturer", "GS_MANUFACTURER", "MANUFACTURERID, MANUFACTURER", "MANUFACTURER", "Please Select", NULL, "width:200px", "textbox200", "style='margin-left:20px;'")
	Call BuildSelect(lst_app_contractor_warranty, "sel_app_contractor_warranty", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", NULL, "width:250px", "textbox200", "style='margin-left:5px;'")
	
	
	
	SQL = "SELECT GS_PROPERTY_APPLIANCE.PROPERTYAPPLIANCEID, P_FUELTYPE.FUELTYPE, GS_APPLIANCE_TYPE.APPLIANCETYPE, GS_PROPERTY_APPLIANCE.DATEINSTALLED, GS_PROPERTY_APPLIANCE.DATEREMOVED FROM GS_PROPERTY_APPLIANCE INNER JOIN P_FUELTYPE on P_FUELTYPE.FuelTypeId = GS_PROPERTY_APPLIANCE.ApplianceFuelTypeId INNER JOIN GS_APPLIANCE_TYPE on GS_APPLIANCE_TYPE.APPLIANCETYPEID = GS_PROPERTY_APPLIANCE.APPLIANCETYPEID WHERE GS_PROPERTY_APPLIANCE.PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString7 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString7 = "<tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"		
		while NOT rsLoader.EOF
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("FUELTYPE") & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & rsLoader("APPLIANCETYPE") & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & Day(rsLoader("DATEINSTALLED")) & "/"& Month(rsLoader("DATEINSTALLED")) & "/" & Year(rsLoader("DATEINSTALLED")) & "</td>"
		    
		    if (rsLoader("DATEREMOVED") = "1/1/1900") then
		        TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & "N/A" & "</td>"
		    else
		        TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""LOADAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & Day(rsLoader("DATEREMOVED")) & "/"& Month(rsLoader("DATEREMOVED")) & "/" & Year(rsLoader("DATEREMOVED")) & "</td>"
		    end if
		    
		    ' The download link
		    TableString7 = TableString7 & "<td style='cursor:pointer' >" & "<img alt='' src='/myImages/paper_clip.gif'/>" & "</td>"
		    TableString7 = TableString7 & "<td style='cursor:pointer' onclick=""DELETEAPP(" & rsLoader("PROPERTYAPPLIANCEID") & ")"">" & "<font color=""red"">DEL</font>" & "</td>"
		    
			Counter = Counter + 1
			TableString7 = TableString7 & "</tr><tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"
			rsLoader.moveNext
		Wend
		Remainder = Counter Mod 3
		If (COUNTER = 0) Then
			TableString7 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
		End If
	End If
	Call CloseRs(rsLoader)
	
	
	'Get the Certificates - Hussain
	
	SQL = "Select AS_APPOINTMENTS.JSGNUMBER, P_LGSR_HISTORY.CP12NUMBER,  P_LGSR_HISTORY.ISSUEDATE,  P_LGSR_HISTORY.CP12DOCUMENT, E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME as IssuerName, P_LGSR_HISTORY.DTIMESTAMP  FROM  P_LGSR_HISTORY  INNER JOIN P_LGSR on P_LGSR_HISTORY.LGSRID = P_LGSR.LGSRID  INNER JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = P_LGSR_HISTORY.CP12ISSUEDBY INNER JOIN AS_JOURNAL on AS_JOURNAL.PROPERTYID = P_LGSR.PROPERTYID INNER JOIN AS_APPOINTMENTS on AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID WHERE  P_LGSR.PROPERTYID = '" & PropertyID & "' "
	Call OpenRs(rsLoader,SQL)
	TableString8 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
	if (NOT rsLoader.EOF) then
		Counter = 0
		TableString8 = "<tr>"		
		while NOT rsLoader.EOF
		    ' The hard coded values
		    TableString8 = TableString8 & "<td >" & rsLoader("JSGNumber") & "</td>"
		    TableString8 = TableString8 & "<td >" & "LGSR (GAS)" & "</td>"
		    
		    ' Actual Values from the db
		    
		    TableString8 = TableString8 & "<td >" & rsLoader("CP12Number") & "</td>"
		    TableString8 = TableString8 & "<td >" & "N/A" & "</td>"
		    TableString8 = TableString8 & "<td >" & Day(rsLoader("IssueDate")) & "/"& Month(rsLoader("IssueDate")) & "/" & Year(rsLoader("IssueDate")) & "</td>"		    
		    TableString8 = TableString8 & "<td >" & rsLoader("IssuerName") & "</td>"
		    'if (rsLoader("DTIMESTAMP") <> "") then
		    '    TableString8 = TableString8 & "<td >" & "N/A" & "</td>"		    
		    'else 
		        TableString8 = TableString8 & "<td >" & Day(rsLoader("DTIMESTAMP")) & "/"& Month(rsLoader("DTIMESTAMP")) & "/" & Year(rsLoader("DTIMESTAMP")) & "</td>"
		    'end if
		    
		    ' The download link
		    if(rsLoader("CP12NUMBER")<>"") then
		        TableString8 = TableString8 & "<td style='cursor:pointer' >" & "" & "</td>"
		    else 
		        TableString8 = TableString8 & "<td style='cursor:pointer' >" & "<img alt='' src='/myImages/paper_clip.gif'/>" & "</td>"
		    end if
		    
		    TableString8 = TableString8 & "<td style='cursor:pointer' >" & "" & "</td>"
			
			Counter = Counter + 1
			TableString8 = TableString8 & "</tr><tr>"
			rsLoader.moveNext
		Wend
		Remainder = Counter Mod 3
		If (COUNTER = 0) Then
			TableString8 = "<tr><td colspan=""6"" align=""center"">No items found</td></tr>"
		End If
	End If
	Call CloseRs(rsLoader)
	
	
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio --&gt; Attributes</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .linkstyle
        {
            text-decoration: underline;
            color: blue;
            cursor: hand;
            font-size: 9px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript">

    var FormFields = new Array();
    FormFields[0] = "txt_FLOORAREA|Floor Area|FLOAT|N|2"
    FormFields[1] = "txt_MAXPEOPLE|Max People|INTEGER|N"
    FormFields[2] = "txt_BEDROOMS|Bedrooms|INTEGER|N"
    FormFields[3] = "txt_BEDROOMSITTINGROOM|Bedroom/Sitting Room|INTEGER|N"
    FormFields[4] = "txt_KITCHEN|Kitchen|INTEGER|N"
    FormFields[5] = "txt_KITCHENDINER|Kitchen Diner|INTEGER|N"
    FormFields[6] = "txt_LIVINGROOM|Living Room|INTEGER|N"
    FormFields[7] = "txt_LIVINGROOMDINER|Living Room Diner|INTEGER|N"
    FormFields[8] = "txt_DININGROOM|Dining Room|INTEGER|N"
    FormFields[9] = "txt_WC|WC|INTEGER|N"
    FormFields[10] = "txt_BATHROOM|Bathroom|INTEGER|N"
    FormFields[11] = "txt_BATHROOMWC|Bathroom WC|INTEGER|N"
    FormFields[12] = "txt_SHOWER|Shower|INTEGER|N"
    FormFields[13] = "txt_SHOWERWC|Shower WC|INTEGER|N"
    FormFields[14] = "txt_BOXROOMUTILITYROOM|Box / Utility Room|INTEGER|N"
    FormFields[15] = "txt_SHAREDKITCHENLIVING|Shared Kitchen/Living|INTEGER|N"
    FormFields[16] = "txt_SHAREDWC|Shared WC|INTEGER|N"
    FormFields[17] = "txt_SHAREDSHOWERWC|Shared Shower WC|INTEGER|N"
    FormFields[18] = "sel_FURNISHING|Furnishing|SELECT|N"
    FormFields[19] = "sel_SECURITY|Security|SELECT|N"
    FormFields[20] = "sel_HEATING|Heating|SELECT|N"
    FormFields[21] = "sel_GARDEN|Garden|SELECT|N"
    FormFields[22] = "sel_PARKING|Parking|SELECT|N"
    FormFields[23] = "sel_PETS|Pets|SELECT|N"
    FormFields[24] = "txt_KITCHENDINERLOUNGE|Kitchen/Diner/Lounge|INTEGER|N"
    FormFields[25] = "sel_ACCESSIBILITYSTANDARDS|WheelChair|SELECT|Y"
    FormFields[26] = "sel_FRONTPORCH|Front Porch|SELECT|N"
    FormFields[27] = "sel_CONSERVATORYUPVC|ConservatoryUPVC|SELECT|N"
    FormFields[28] = "sel_GARAGE|Garage|SELECT|N"

    function swap_div(int_which_one, str_where) {
        var divid, imgid, upper
        upper = 8; lower = 1;
        imgid = "img" + int_which_one.toString();
        // manipulate images and close all divs
        for (j = lower; j <= upper; j++) {
            document.getElementById("img" + j + "").src = "Images/" + j + "-att-closed.gif";
            document.getElementById("div" + j + "").style.display = "none";
        }
        // unless last image in row
        if (int_which_one != upper)
            document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-att-previous.gif"
        // open image and div
        document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-att-open.gif"
        document.getElementById("div" + int_which_one + "").style.display = "block";
        if (int_which_one == 5) {
            GetAttribute("<%=PROPERTYID%>")
        }
    }

    function SaveForm() {
        if (!checkForm()) return false
        document.RSLFORM.action = "ServerSide/Attributes_svr.asp"
        document.RSLFORM.submit()
    }

   function ADDKA() {
        if (document.getElementById("sel_KITCHEN").value != "") {
            document.getElementById("hid_ACTION").value = "NEW"
            document.THISFORM.action = "ServerSide/ServerKA.asp"
            document.THISFORM.submit()
        }
    }

    function ADDAS() {
        if (document.getElementById("sel_AS").value != "") {
            document.getElementById("hid_ACTION").value = "NEW"
            document.THISFORM.action = "ServerSide/ServerAS.asp"
            document.THISFORM.submit()
        }
    }


    function ADDSF() {
        if (document.getElementById("sel_SCHEME").value != "") {
            document.getElementById("hid_ACTION").value = "NEW"
            document.THISFORM.action = "ServerSide/ServerSF.asp"
            document.THISFORM.submit()
        }
    }

    function ADDSS() {
        if (document.getElementById("sel_SS").value != "") {
            document.getElementById("hid_ACTION").value = "NEW"
            document.THISFORM.action = "ServerSide/ServerSS.asp"
            document.THISFORM.submit()
        }
    }

    function ADDAD() {
        if (document.getElementById("sel_ADS").value != "" && document.getElementById("sel_ADAPTIONS").value != "") {
            document.getElementById("hid_ACTION").value = "NEW"
            document.THISFORM.action = "ServerSide/ServerAD.asp"
            document.THISFORM.submit()
        }
    }
    function ADDGS() {
        // Please see task 4485 for the reason to comment if -else clause 
        //if (THISFORM.sel_gsappliancetype.value != "" && THISFORM.sel_gslocation.value != "" && THISFORM.sel_gsmanufacturer.value != "" && THISFORM.sel_contractor_warranty.value != "") {
        document.getElementById("hid_ACTION").value = "NEW"
        document.THISFORM.action = "ServerSide/ServerGS.asp"
        document.THISFORM.submit()
        //	}
        //else
        //    {
        //    alert("Please fill all the fields to save property appliance.");
        //    return false;
        //    }
    }

    function ADDAPP() {
        if (!checkApplianceForm()) return false
        document.getElementById("hid_ACTION").value = "NEW"
        document.THISFORM.action = "ServerSide/ServerAPP.asp"
        document.THISFORM.submit()                
    }

    function DELETEAPP(intID) {
        result = confirm("You are about to remove an Appliance. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerAPP.asp"
        document.THISFORM.submit()
    }

    function LOADAPP(intID) {
        //if (!checkAppForm()) return false
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "LOAD"
        document.THISFORM.action = "ServerSide/ServerAPP.asp"
        document.THISFORM.submit()

        
    }

    function AMENDAPP(intID) {
        if (!checkApplianceForm()) return false
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "AMEND"
        document.THISFORM.action = "ServerSide/ServerAPP.asp"
        document.THISFORM.submit()
    }

    function DeleteKA(intID) {
        result = confirm("You are about to delete a Kitchen Appliance. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerKA.asp"
        document.THISFORM.submit()
    }

    function DeleteAS(intID) {
        result = confirm("You are about to remove a Annual Servicing option. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerAS.asp"
        document.THISFORM.submit()
    }

    function DeleteGS(intID) {
        result = confirm("You are about to remoce a Annual Servicing option. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerGS.asp"
        document.THISFORM.submit()
    }
    function LoadGS(intID) {
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "LOAD"
        document.getElementById("txt_cp12number").disabled = true
        document.getElementById("txt_dateofissue").disabled = true
        document.getElementById("sel_gsappliancetype").disabled = true
        document.THISFORM.action = "ServerSide/ServerGS.asp"
        document.THISFORM.submit()
    }

    function AmendGS(intID) {
        // Please see task 4485 for the reason to comment if -else clause 
        //if (THISFORM.sel_gsappliancetype.value != "" && THISFORM.sel_gslocation.value != "" && THISFORM.sel_gsmanufacturer.value != "" && THISFORM.sel_contractor_warranty.value != "") {
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("txt_cp12number").disabled = false
        document.getElementById("txt_dateofissue").disabled = false
        document.getElementById("sel_gsappliancetype").disabled = false
        document.getElementById("hid_ACTION").value = "AMEND"
        document.THISFORM.action = "ServerSide/ServerGS.asp"
        document.THISFORM.submit()
        //    }	
        // else
        //	{
        //	    alert("Please fill all the fields to amend property appliance.");
        //	    return false;
        //	}   
    }

    function DeleteSF(intID) {
        result = confirm("You are about to delete a Scheme Facilities. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerSF.asp"
        document.THISFORM.submit()
    }

    function DeleteSS(intID) {
        result = confirm("You are about to delete a Supplied Services. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerSS.asp"
        document.THISFORM.submit()
    }

    function DeleteAD(intID) {
        result = confirm("You are about to delete an Adaptation. Do you wish to continue?\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
        if (!result) return false;
        document.getElementById("hid_DELETE").value = intID
        document.getElementById("hid_ACTION").value = "DELETE"
        document.THISFORM.action = "ServerSide/ServerAD.asp"
        document.THISFORM.submit()
    }

    function GetAdaptions() {
        if (document.getElementById("sel_ADS").value != "") {
            document.getElementById("hid_ACTION").value = "LOAD"
            document.THISFORM.action = "ServerSide/ServerAD.asp"
            document.THISFORM.submit()
        }
        else
            document.getElementById("dvADAPTIONS").innerHTML = "<select name=\"sel_ADAPTIONS\" id=\"sel_ADAPTIONS\" class=\"textbox200\" style=\"width:350\"><option value=\"\">Please Select an Adapt Cat</option></select>"
    }
    function DisplayDoc(propertyid, appliancetypeid) {
        window.open("PopUps/pCP12Document.asp?propertyid=" + propertyid + "&appliancetypeid=" + appliancetypeid, "_blank", null);
    }
    function GetAttribute(a) {
        window.open("/RSLWebSurvey/popAttrib.asp?pid=" + a, "display", "scrollbars=1,resizable=1,left=100,top=200");
    }											
</script>
</head>
<body onload="initSwipeMenu(4);" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="property.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Main Details">
                    <img name="tab_main_details" src="images/tab_main_details.gif" width="97" height="20"
                        border="0" alt="Main Details" /></a></td>
            <td rowspan="2">
                <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Financial">
                    <img name="tab_financial" src="images/tab_financial.gif" width="79" height="20" border="0" alt="Financial" /></a></td>
            <td rowspan="2">
                <img name="tab_attributes" src="images/tab_attributes-over.gif" width="84" height="20"
                    border="0" alt="Attributes" /></td>
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Warranties">
                    <img name="tab_warranties" src="images/tab_warranties-tab_attribut.gif" width="89"
                        height="20" border="0" alt="Warranties" /></a></td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Utilities">
                    <img name="tab_utilities" src="images/tab_utilities.gif" width="68" height="20" border="0" alt="Utilities" /></a></td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Marketing">
                    <img name="tab_marketing" src="images/tab_marketing.gif" width="76" height="20" border="0" alt="Marketing" /></a>
            </td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Health and Safety">
                    <img name="tab_hands" src="images/tab_healthsafety_1.gif" width="110" height="20"
                        border="0" alt="Health and Safety" /></a></td>
            <td>
                <img src="/myImages/spacer.gif" width="151" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004174">
                <img src="images/spacer.gif" width="151" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <form name="RSLFORM" method="post" action="">
                <table border="0">
                    <tr>
                        <td nowrap="nowrap" title="Square Metres">
                            Floor Area(sq m):
                        </td>
                        <td>
                            <input type="text" name="txt_FLOORAREA" id="txt_FLOORAREA" class="textbox100" style="width: 50px" value="<%=FLOORAREA%>" />
                            <img src="/js/FVS.gif" name="img_FLOORAREA" id="img_FLOORAREA" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td>
                            Furnishing:
                        </td>
                        <td colspan="2">
                            <%=lst_furnishing%>
                            <img src="/js/FVS.gif" name="img_FURNISHING" id="img_FURNISHING" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td>
                            Security:
                        </td>
                        <td colspan="2" align="right">
                            <%=lst_security%>
                            <img src="/js/FVS.gif" name="img_SECURITY" id="img_SECURITY" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Max People:
                        </td>
                        <td>
                            <input type="text" name="txt_MAXPEOPLE" id="txt_MAXPEOPLE" class="textbox100" style="width: 50px" value="<%=MAXPEOPLE%>" />
                            <img src="/js/FVS.gif" name="img_MAXPEOPLE" id="img_MAXPEOPLE" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td>
                            Heating:
                        </td>
                        <td colspan="2">
                            <%=lst_heating%>
                            <img src="/js/FVS.gif" name="img_HEATING" id="img_HEATING" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td>
                            Garden:
                        </td>
                        <td colspan="2" align="right">
                            <%=lst_garden%>
                            <img src="/js/FVS.gif" name="img_GARDEN" id="img_GARDEN" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Bedrooms:
                        </td>
                        <td>
                            <input type="text" name="txt_BEDROOMS" id="txt_BEDROOMS" class="textbox100" style="width: 50px" value="<%=BEDROOMS%>" />
                            <img src="/js/FVS.gif" name="img_BEDROOMS" id="img_BEDROOMS" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Bedroom/Sitting Room:
                        </td>
                        <td>
                            <input type="text" name="txt_BEDROOMSITTINGROOM" id="txt_BEDROOMSITTINGROOM" class="textbox100" style="width: 50px"
                                value="<%=BEDROOMSITTINGROOM%>" />
                            <img src="/js/FVS.gif" name="img_BEDROOMSITTINGROOM" id="img_BEDROOMSITTINGROOM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Kitchen:
                        </td>
                        <td>
                            <input type="text" name="txt_KITCHEN" id="txt_KITCHEN" class="textbox100" style="width: 50px" value="<%=KITCHEN%>" />
                            <img src="/js/FVS.gif" name="img_KITCHEN" id="img_KITCHEN" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Kitchen/Diner:
                        </td>
                        <td>
                            <input type="text" name="txt_KITCHENDINER" id="txt_KITCHENDINER" class="textbox100" style="width: 50px"
                                value="<%=KITCHENDINER%>" />
                            <img src="/js/FVS.gif" name="img_KITCHENDINER" id="img_KITCHENDINER" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Living Room:
                        </td>
                        <td>
                            <input type="text" name="txt_LIVINGROOM" id="txt_LIVINGROOM" class="textbox100" style="width: 50px" value="<%=LIVINGROOM%>" />
                            <img src="/js/FVS.gif" name="img_LIVINGROOM" id="img_LIVINGROOM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Living Room/Diner:
                        </td>
                        <td>
                            <input type="text" name="txt_LIVINGROOMDINER" id="txt_LIVINGROOMDINER" class="textbox100" style="width: 50px"
                                value="<%=LIVINGROOMDINER%>" />
                            <img src="/js/FVS.gif" name="img_LIVINGROOMDINER" id="img_LIVINGROOMDINER" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Dining Room:
                        </td>
                        <td>
                            <input type="text" name="txt_DININGROOM" id="txt_DININGROOM" class="textbox100" style="width: 50px" value="<%=DININGROOM%>" />
                            <img src="/js/FVS.gif" name="img_DININGROOM" id="img_DININGROOM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            WC:
                        </td>
                        <td>
                            <input type="text" name="txt_WC" id="txt_WC" class="textbox100" style="width: 50px" value="<%=WC%>" />
                            <img src="/js/FVS.gif" name="img_WC" id="img_WC" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Bathroom:
                        </td>
                        <td>
                            <input type="text" name="txt_BATHROOM" id="txt_BATHROOM" class="textbox100" style="width: 50px" value="<%=BATHROOM%>" />
                            <img src="/js/FVS.gif" name="img_BATHROOM" id="img_BATHROOM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Bathroom/WC:
                        </td>
                        <td>
                            <input type="text" name="txt_BATHROOMWC" id="txt_BATHROOMWC" class="textbox100" style="width: 50px" value="<%=BATHROOMWC%>" />
                            <img src="/js/FVS.gif" name="img_BATHROOMWC" id="img_BATHROOMWC" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Shower:
                        </td>
                        <td>
                            <input type="text" name="txt_SHOWER" id="txt_SHOWER" class="textbox100" style="width: 50px" value="<%=SHOWER%>" />
                            <img src="/js/FVS.gif" name="img_SHOWER" id="img_SHOWER" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Shower/WC:
                        </td>
                        <td>
                            <input type="text" name="txt_SHOWERWC" id="txt_SHOWERWC" class="textbox100" style="width: 50px" value="<%=SHOWERWC%>" />
                            <img src="/js/FVS.gif" name="img_SHOWERWC" id="img_SHOWERWC" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Box/Utility Room:
                        </td>
                        <td>
                            <input type="text" name="txt_BOXROOMUTILITYROOM" id="txt_BOXROOMUTILITYROOM" class="textbox100" style="width: 50px"
                                value="<%=BOXROOMUTILITYROOM%>" />
                            <img src="/js/FVS.gif" name="img_BOXROOMUTILITYROOM" id="img_BOXROOMUTILITYROOM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Shared Kitchen/Living:
                        </td>
                        <td>
                            <input type="text" name="txt_SHAREDKITCHENLIVING" id="txt_SHAREDKITCHENLIVING" class="textbox100" style="width: 50px"
                                value="<%=SHAREDKITCHENLIVING%>" />
                            <img src="/js/FVS.gif" name="img_SHAREDKITCHENLIVING" id="img_SHAREDKITCHENLIVING" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Shared WC:
                        </td>
                        <td>
                            <input type="text" name="txt_SHAREDWC" id="txt_SHAREDWC" class="textbox100" style="width: 50px" value="<%=SHAREDWC%>" />
                            <img src="/js/FVS.gif" name="img_SHAREDWC" id="img_SHAREDWC" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Shared Shower/WC:
                        </td>
                        <td>
                            <input type="text" name="txt_SHAREDSHOWERWC" id="txt_SHAREDSHOWERWC" class="textbox100" style="width: 50px"
                                value="<%=SHAREDSHOWERWC%>" />
                            <img src="/js/FVS.gif" name="img_SHAREDSHOWERWC" id="img_SHAREDSHOWERWC" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Parking:
                        </td>
                        <td>
                            <select class="RSLButton" name="sel_PARKING" id="sel_PARKING" style="width: 50px">
                                <%
					            SELECTED = ""
					            If (PARKING = 0) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="0" <%=SELECTED%>>No</option>
                                <%
					            SELECTED = ""
					            If (PARKING = 1) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="1" <%=SELECTED%>>Yes</option>
                            </select>
                            <img src="/js/FVS.gif" name="img_PARKING" id="img_PARKING" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td style="white-space: nowrap">
                            Kitchen/Diner/Lounge:
                        </td>
                        <td>
                            <input type="text" name="txt_KITCHENDINERLOUNGE" id="txt_KITCHENDINERLOUNGE" class="textbox100" style="width: 50px"
                                value="<%=KITCHENDINERLOUNGE%>" />
                            <img src="/js/FVS.gif" name="img_KITCHENDINERLOUNGE" id="img_KITCHENDINERLOUNGE" width="15px" height="15px" border="0" alt="" />
                        </td>
                        <td nowrap="nowrap">
                            Pets:
                        </td>
                        <td>
                            <select class="RSLButton" name="sel_PETS" id="sel_PETS" style="width: 50px">
                                <%
					            SELECTED = ""
					            If (PETS = 0) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="0" <%=SELECTED%>>No</option>
                                <%
					            SELECTED = ""
					            If (PETS = 1) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="1" <%=SELECTED%>>Yes</option>
                            </select>
                            <img src="/js/FVS.gif" name="img_PETS" id="img_PETS" width="15px" height="15px" border="0" alt="" />
                        </td>                        
                    </tr>
                    <tr>
                        <td>Front Porch</td>
                        <td><select class="RSLButton" name="sel_FRONTPORCH" id="sel_FRONTPORCH" style="width: 50px">
                                <%
					            SELECTED = ""
					            If (FRONTPORCH = 0) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="0" <%=SELECTED%>>No</option>
                                <%
					            SELECTED = ""
					            If (FRONTPORCH = 1) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="1" <%=SELECTED%>>Yes</option>
                            </select>
                            <img src="/js/FVS.gif" name="img_FRONTPORCH" id="img_FRONTPORCH" width="15px" height="15px" border="0" alt="" /></td>
                        <td>Conservatory (UPVC)</td>
                        <td><select class="RSLButton" name="sel_CONSERVATORYUPVC" id="sel_CONSERVATORYUPVC" style="width: 50px">
                                <%
					            SELECTED = ""
					            If (CONSERVATORYUPVC = 0) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="0" <%=SELECTED%>>No</option>
                                <%
					            SELECTED = ""
					            If (CONSERVATORYUPVC = 1) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="1" <%=SELECTED%>>Yes</option>
                            </select>
                            <img src="/js/FVS.gif" name="img_CONSERVATORYUPVC" id="img_CONSERVATORYUPVC" width="15px" height="15px" border="0" alt="" /></td>
                        <td>Garage</td>
                        <td><select class="RSLButton" name="sel_GARAGE" id="sel_GARAGE" style="width: 50px">
                                <%
					            SELECTED = ""
					            If (GARAGE = 0) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="0" <%=SELECTED%>>No</option>
                                <%
					            SELECTED = ""
					            If (GARAGE = 1) Then
					                SELECTED = " selected=""selected"" "
					            End If
                                %>
                                <option value="1" <%=SELECTED%>>Yes</option>
                            </select>
                            <img src="/js/FVS.gif" name="img_GARAGE" id="img_GARAGE" width="15px" height="15px" border="0" alt="" /></td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            W/Chair Access:
                        </td>
                        <td colspan="3">
                            <%=lst_ACCESSIBILITYSTANDARDS%>
                            <img src="/js/FVS.gif" name="img_ACCESSIBILITYSTANDARDS" id="img_ACCESSIBILITYSTANDARDS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td colspan="5" align="right">
                            <input type="hidden" name="hid_PROPERTYID2" id="hid_PROPERTYID2" value="<%=PropertyID%>" />
                            <input type="hidden" name="hid_ACTION2" id="hid_ACTION2" value="<%=ACTION%>" />
                            <input type="button" name="txt_SaveButton" id="txt_SaveButton" onclick="SaveForm()" value=" SAVE " title=" SAVE " class="RSLButton" style="cursor:pointer" />
                            <img src="/js/FVS.gif" name="img_CCCCCC" id="img_CCCCCC" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    
                </table></form>
            </td>
        </tr>
    </table>
    <br/>
    <form name="THISFORM" method="post" target="ServerAtt" action=""> 
    <table width="710" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="img1" id="img1" title="Kitchen Appliances" src="images/1-att-open.gif" width="137"
                    height="20" border="0" onclick="swap_div(1)" style="cursor: pointer" alt="Kitchen Appliances" /></td>
            <td rowspan="2">
                <img name="img2" id="img2" title="Scheme Facilities" src="images/2-att-previous.gif" width="125"
                    height="20" border="0" onclick="swap_div(2)" style="cursor: pointer" alt="Scheme Facilities" /></td>
            <td rowspan="2">
                <img name="img3" id="img3" title="Supplied Services" src="images/3-att-closed.gif" width="128"
                    height="20" border="0" onclick="swap_div(3)" style="cursor: pointer" alt="Supplied Services" /></td>
            <td rowspan="2">
                <img name="img4" id="img4" title="Adaptions" src="images/4-att-closed.gif" width="96" height="20"
                    border="0" onclick="swap_div(4)" style="cursor: pointer" alt="Adaptions" /></td>
            <td rowspan="2">
                <img name="img5" id="img5" title="Planned Maintenance" src="images/5-att-closed.gif" width="100"
                    height="20" border="0" onclick="swap_div(5)" style="cursor: pointer" alt="Planned Maintenance" /></td>
            <td rowspan="2">
                <img name="img6" id="img6" title="Gas Servicing" src="images/6-att-closed.gif" width="57" height="20"
                    border="0" onclick="swap_div(6)" style="cursor: pointer" alt="Gas Servicing" /></td>
            <td rowspan="2">
                <img name="img7" id="img7" title="Gas Servicing" src="images/7-att-closed.gif" width="95" height="20"
                    border="0" onclick="swap_div(7)" style="cursor: pointer" alt="Gas Servicing" /></td>
            <td rowspan="2">
                <img name="img8" id="img8" title="Gas Servicing" src="images/8-att-closed.gif" width="95" height="20"
                    border="0" onclick="swap_div(8)" style="cursor: pointer" alt="Gas Servicing" /></td>        
                    
            <td>
                <img src="images/spacer.gif" width="110" height="19" alt="" /></td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="images/spacer.gif" width="110" height="1" alt="" /></td>
        </tr>
    </table> 
    <table width="99%" style="height:210px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="0" border="0" class="TAB_TABLE">        
        <tr>
            <td>
                <div id="div1" style="display: block; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Kitchen Appliance:
                                <%=lst_kitchen%>&nbsp;&nbsp;
                                <input type="button" class="rslbutton" value=" Add " title=" Add " onclick="ADDKA()" />
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Kitchen Appliance(s)</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="KA" style="height: 160; width: 750; overflow: auto" class="TA">
                                    <table cellspacing="2" cellpadding="1" style="height:100%; border-collapse: collapse" border="1"
                                        width="100%">
                                        <%=TableString1%>
                                        <tr>
                                            <td height="100%" colspan="6">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div3" style="display: none; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Supplied Services:
                                <%=lst_ss%>&nbsp;&nbsp;
                                <input type="button" class="rslbutton" value=" Add " title=" Add " onclick="ADDSS()" />
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Supplied Services</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="SS" style="height: 160; width: 750; overflow: auto" class="TA">
                                    <table cellspacing="2" cellpadding="1" style="height:100%; border-collapse: collapse" border="1"
                                        width="100%">
                                        <%=TableString3%>
                                        <tr>
                                            <td height="100%" colspan="6">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div2" style="display: none; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Scheme Facilities:
                                <%=lst_scheme%>&nbsp;&nbsp;
                                <input type="button" class="rslbutton" value=" Add " title=" Add " onclick="ADDSF()" />
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Scheme Facilities</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="SF" style="height: 160; width: 750; overflow: auto" class="TA">
                                    <table cellspacing="2" cellpadding="1" style="height:100%; border-collapse: collapse" border="1"
                                        width="100%">
                                        <%=TableString2%>
                                        <tr>
                                            <td height="100%" colspan="6">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div4" style="display: none; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;Adapt Cat:
                                            <%=lst_ads%>&nbsp;&nbsp;
                                        </td>
                                        <td>
                                            Adaption:&nbsp;
                                        </td>
                                        <td>
                                            <div id="dvADAPTIONS"><select id="sel_ADAPTIONS" name="sel_ADAPTIONS" class="textbox200" style="width: 350">
                                                <option value="">Please select an Adpat Cat</option>
                                            </select></div>
                                        </td>
                                        <td>
                                            &nbsp;<input type="button" class="rslbutton" value=" Add " title=" Add " onclick="ADDAD()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Adaptions</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="AD" style="height: 160; width: 750; overflow: auto" class="TA">
                                    <table cellspacing="2" cellpadding="1" style="height:100%; border-collapse: collapse" border="1"
                                        width="100%">
                                        <%=TableString4%>
                                        <tr>
                                            <td height="100%" colspan="6">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div5" style="display: none; overflow: hidden">
                    Please use the pop up for Surveyer<br />
                    If a new window doesn't appear after 10 seconds please <span class="linkstyle" onclick="GetAttribute('<%=PropertyID%>');">
                        click here</span>
                </div>
                <div id="div6" style="display: none; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="3">
                                    <tr>
                                        <td>
                                            Appliance Type:<%=lst_gs_appliancetype%>
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td style="margin: 0 0 0 0; padding: 0 0 0 0;">
                                            LGSR Number:<input type="text" name="txt_cp12number" id="txt_cp12number" class="textbox200" style="margin-left: 130px;
                                                width: 180px;" value="" />
                                            <img alt="" src="/js/FVS.gif" name="img_cp12number" id="img_cp12number" width="15px" height="15px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Location:<%=lst_gs_location%>
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td style="margin: 0 0 0 0; padding: 0 0 0 0;">
                                            Date of Issue:<input type="text" name="txt_dateofissue" id="txt_dateofissue" class="textbox200" style="margin-left: 130px;
                                                width: 180px;" value="" />
                                            <img alt="" src="/js/FVS.gif" name="img_dateofissue" id="img_dateofissue" width="15px" height="15px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Manufacturer:<%=lst_gs_manufacturer%>
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td style="white-space: nowrap">
                                            Contrator for Warranty:
                                            <%=lst_contractor_warranty %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 0px; margin-right: 0px;">
                                            Model:<input type="text" name="txt_model" id="txt_model" class="textbox200" style="margin: 0 0 0 58px;"
                                                value="" />
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td>
                                            Warranty Expiry: Part<input type="text" name="txt_warrantyexpiryparts" id="txt_warrantyexpiryparts" class="textbox200"
                                                style="margin-left: 90px; width: 180px;" value="" />
                                            <img alt="" src="/js/FVS.gif" name="img_warrantyexpiryparts" id="img_warrantyexpiryparts" width="15px" height="15px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date Installed:<input type="text" name="txt_dateinstalled" id="txt_dateinstalled" class="textbox100" style="margin-left: 12px;"
                                                value="" />
                                            <img alt="" src="/js/FVS.gif" name="img_dateinstalled" id="img_dateinstalled" width="15px" height="15px" />
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td style="padding-left: 100px;">
                                            Parts & Labour<input type="text" name="txt_warrantyexpirypartsandlabour" id="txt_warrantyexpirypartsandlabour" class="textbox200"
                                                style="margin-left: 30px; margin-top: 0px; width: 180px;" value="" />
                                            <img alt="" src="/js/FVS.gif" name="img_warrantyexpirypartsandlabour" id="img_warrantyexpirypartsandlabour" width="15px"
                                                height="15px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <!--<td>&nbsp;&nbsp;Required Annual servicings: <%=lst_as%>&nbsp;&nbsp;</td>-->
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td style="width: 25px">
                                            &nbsp;
                                        </td>
                                        <td style="padding-left: 350px;">
                                            <input type="button" class="rslbutton" id="gsbutton" value=" Save " onclick="ADDGS()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Gas Appliance</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="AS" style="height: 160; width: 750; overflow: auto" class="TA">
                                    <table style="height:100%; border-collapse: collapse" border="1" width="100%">
                                        <tr style="border-bottom: #996699 1px solid; border-left-width: 0px;" bgcolor="#f5f5dc">
                                            <td>
                                                <b>Appliance Type</b>
                                            </td>
                                            <td>
                                                <b>Date Installed</b>
                                            </td>
                                            <td>
                                                <b>LGSR Number</b>
                                            </td>
                                            <td>
                                                <b>Date of Issue</b>
                                            </td>
                                            <td>
                                                <b>Warranty Expiry</b>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <%=TableString5%>
                                        <tr>
                                            <td height="100%" colspan="7">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- Changes by Hussain START -->
                
                <div id="div7" style="display: none; overflow: hidden">
                    <table cellspacing="0" width="100%" cellpadding="0" width:"100%">
                        <tr style="height: 15px">
                            <td>
                                                            
                            </td>
                        </tr>  
                        <tr>
                            <td>
                                <table cellspacing="0" width="70%" cellpadding="3">
                                    <tr>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        Appliance Type:
                                                    </td>
                                                    <td style="width:50%">
                                                        <%=lst_gs_app_appliancetype%>
                                                    </td>
                                                </tr>                                            
                                            </table>
                                            
                                        </td>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        Appliance Fuel Type:
                                                    </td>
                                                    <td style="width:50%">
                                                        <%=lst_gs_app_fueltype%>
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        Location:
                                                    </td>
                                                    <td style="width:50%">
                                                        <%=lst_gs_app_location%>
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:25%">
                                                        Date Installed:
                                                    </td>
                                                    <td style="width:50%">                                                        
                                                        <input type="text" name="txt_app_dateinstalled" id="txt_app_dateinstalled" class="textbox100" style="margin-left: 12px;" value="" />
                                                        <img alt="" src="/js/FVS.gif" name="img_dateinstalled" id="img12" width="15px" height="15px" />
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        Manufacturer:
                                                    </td>
                                                    <td style="width:50%">
                                                        <%=lst_gs_app_manufacturer%>
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:25%">
                                                        Date Removed:
                                                    </td>
                                                    <td style="width:50%;text-align:left">                                                        
                                                        <input type="text" name="txt_app_dateremoved" id="txt_app_dateremoved" class="textbox100" style="margin-left: 12px;" value="" />
                                                        <img alt="" src="/js/FVS.gif" name="img_dateinstalled" id="img9" width="15px" height="15px" />
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:50%">
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:50%">
                                                        Model:
                                                    </td>
                                                    <td style="width:50%">
                                                        <input type="text" name="txt_app_model" id="txt_app_model" class="textbox200" style="margin: 0 0 0 58px;" value="" />
                                                    </td>
                                                </tr>                                            
                                            </table>
                                        </td>
                                        <td style="width:50%">
                                           &nbsp;                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <!--<td>&nbsp;&nbsp;Required Annual servicings: <%=lst_as%>&nbsp;&nbsp;</td>-->
                                        <td style="width:50%">
                                            &nbsp;
                                        </td>
                                        <td style="width:50%; text-align:right">
                                            <input type="button" class="rslbutton" id="appButton" value=" Save " onclick="ADDAPP()" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>            
                        
                        <tr style="height: 15px">
                            <td>
                            </td>
                        </tr>
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Gas Appliance</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="GASAPPLAINCES" style="height: 160; width: 930px; overflow: auto" class="TA">
                                    <table style="height:100%; border-collapse: collapse" border="1" width="100%">
                                        <tr style="border-bottom: #996699 1px solid; border-left-width: 0px;" bgcolor="#f5f5dc">
                                            <td>
                                                <b>Appliance Fuel Type</b>
                                            </td>
                                            <td>
                                                <b>Appliance Type</b>
                                            </td>
                                            <td>
                                                <b>Date Installed</b>
                                            </td>
                                            <td>
                                                <b>Date Removed</b>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <%=TableString7%>
                                        <tr>
                                            <td height="100%" colspan="7">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>          
                    </table>
                </div>
                <div id="div8" style="display: none; overflow: hidden">
                    <table cellspacing="0" cellpadding="0">
                        <tr bgcolor="#133E71" style="color: #FFFFFF">
                            <td width="140" nowrap="nowrap" height="18">
                                <b>&nbsp;Certificates</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="CERTIFICATES" style="height: 160; width: 800; overflow: auto" class="TA">
                                    <table style="height:100%; border-collapse: collapse" border="0" width="100%">
                                        <tr style="border-bottom: #996699 1px solid; border-left-width: 0px;" bgcolor="#f5f5dc">
                                            <td>
                                                <b>Inspection Ref</b>
                                            </td>
                                            <td>
                                                <b>Type</b>
                                            </td>
                                            <td>
                                                <b>Certificate No</b>
                                            </td>
                                            <td>
                                                <b>Doc Title</b>
                                            </td>
                                            <td>
                                                <b>Date of Issue</b>
                                            </td>
                                            <td>
                                                <b>Uploaded By</b>
                                            </td>
                                            <td>
                                                <b>Uploaded On</b>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <%=TableString8%>
                                        <tr>
                                            <td height="100%" colspan="7">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>          
                    </table>
                </div>
                <!-- Changes by Hussain END -->
                <input type="hidden" name="hid_DELETE" id="hid_DELETE" />
                <input type="hidden" name="hid_PROPERTYID" id="hid_PROPERTYID" value="<%=PropertyID%>" />
                <input type="hidden" name="hid_ACTION" id="hid_ACTION" value="<%=ACTION%>" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="ServerAtt" id="ServerAtt" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
