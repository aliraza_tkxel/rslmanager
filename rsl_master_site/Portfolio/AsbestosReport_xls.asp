<%
''Response.Write "dddd"
''Response.End()
'Response.Buffer = True
'Response.ContentType = "application/vnd.ms-excel"
Response.Buffer = True
Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="Includes/Functions/LibFunctions.asp" -->
<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include
	'		sometimes kills the xls transfer

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count
		Dim theURL	
		Dim PageName
		Dim TheUser		 	' The Housing Officer ID - default is the session
		Dim TheArrearsTotal
		Dim ochecked, cchecked
		Dim CompareValue,DEVELOPMENTFILTER,ASBESTOS

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    'Get Record to display on xsl
	     Call OpenDB()

	    If Request.QueryString("DEVELOPMENTID") <> "" Then
	        If(Request.QueryString("PATCHID") <> "") Then
	            DEVELOPMENTFILTER = "  AND SUB.DEVELOPMENTID = " & Request.QueryString("DEVELOPMENTID")
	            PATCHFILETER = " AND SUB.PATCHID = "  & Request.QueryString("PATCHID")
	            HEADING = "Asbestos Risk - FOR PATCH" & Request.QueryString("PATCHNAME") & "SCHEME " & Request.QueryString("DEVELOPMENTNAME")
	        Else
	            DEVELOPMENTFILTER = "  AND SUB.DEVELOPMENTID = " & Request.QueryString("DEVELOPMENTID")
	            HEADING = "Asbestos Risk - FOR SCHEME " & Request.QueryString("DEVELOPMENTNAME")
	        End If 
	    Else
	        If(Request.QueryString("PATCHID") <> "") Then
	            PATCHFILETER = " AND SUB.PATCHID = "  & Request.QueryString("PATCHID")
	            HEADING = "Asbestos Risk - FOR PATCH" & Request.QueryString("PATCHNAME")
	        Else  
	            DEVELOPMENTFILTER = ""
	            HEADING = "Asbestos Risk - "
            End If
	    End If

        ASBESTOS = ""
        str_data = ""

           strSQL=" SELECT SUB.PROPERTYID,SUB.PROPERTY_ADDRESS,SUB.REFURBISMENT_DATE,PARL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL "&_
                  " FROM (SELECT DISTINCT PP.PROPERTYID AS PROPERTYID, REPLACE(                                                "&_ 
                  "                       CASE ISNULL(PP.FLATNUMBER,'EMPTY')                                                   "&_
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_
	              "                          ELSE PP.FLATNUMBER+','                                                            "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                                                 "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.HOUSENUMBER+','                                                           "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS1,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS1+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS2,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS2+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS3,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.ADDRESS3+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.TOWNCITY,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.TOWNCITY+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.COUNTY,'EMPTY')                                                      "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.COUNTY+','                                                                "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.POSTCODE,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.POSTCODE                                                                  "&_ 
                  "                        END,',,',',')                                                                       "&_ 
                  "                     AS PROPERTY_ADDRESS,MAX(PRH.REFURBISHMENT_DATE) AS REFURBISMENT_DATE,MAX(PAL.RISKLEVELID) AS MAX_RISKLEVELID, "&_
                  "                     PP.DEVELOPMENTID AS DEVELOPMENTID,EP.PATCHID AS PATCHID                                "&_ 
		          "                     FROM P__PROPERTY PP                                                                    "&_ 
                  "                     LEFT OUTER JOIN P_REFURBISHMENT_HISTORY PRH ON PP.PROPERTYID=PRH.PROPERTYID            "&_ 
                  "                     INNER JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID=PD.DEVELOPMENTID                       "&_
                  "                     LEFT JOIN E_PATCH EP ON EP.PATCHID=PD.PATCHID                                          "&_
                  "                     INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL PPARL ON PP.PROPERTYID=PPARL.PROPERTYID       "&_ 
		          "                     INNER JOIN 	P_PROPERTY_ASBESTOS_RISK PPAR ON PPAR.PROPASBLEVELID=PPARL.PROPASBLEVELID  "&_ 
		          "                     INNER JOIN P_ASBRISKLEVEL PAL ON PAL.ASBRISKLEVELID=PPARL.ASBRISKLEVELID               "&_ 
		          "                     GROUP BY PP.PROPERTYID, REPLACE(                                                       "&_ 
                  "                        CASE ISNULL(PP.FLATNUMBER,'EMPTY')                                                  "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.FLATNUMBER+','                                                            "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                                                 "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.HOUSENUMBER+','                                                           "&_ 
                  "                         END                                                                                "&_ 
                  "                       + CASE ISNULL(PP.ADDRESS1,'EMPTY')                                                   "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS1+','                                                              "&_ 
                  "                         END                                                                                "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS2,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS2+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS3,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.ADDRESS3+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.TOWNCITY,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.TOWNCITY+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.COUNTY,'EMPTY')                                                      "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.COUNTY+','                                                                "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.POSTCODE,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.POSTCODE                                                                  "&_ 
                  "                        END,',,',','),PP.DEVELOPMENTID,EP.PATCHID                                           "&_ 
                  "                     ) SUB,P_ASBRISKLEVEL AS PARL                                                           "&_ 
                  "  WHERE SUB.MAX_RISKLEVELID=PARL.RISKLEVELID	  " & DEVELOPMENTFILTER &  PATCHFILETER &             "        "&_ 
                  " ORDER BY SUB.PROPERTYID"

		Call OpenRs(rsSet, strSQL)
		While NOT rsSet.EOF
		    Call Build_Abestos_Detail(rsSet("PROPERTYID"))
		    STRrow  =   STRrow & "<tr>" &_
	                                "  <td valign=""top"">" & rsSet("PROPERTYID") & "</td>" &_
				    		        "  <td>" & rsSet("PROPERTY_ADDRESS") & "</td>" &_
			    	    	        "  <td align=""center"">" & rsSet("REFURBISMENT_DATE") & "</td>" &_
						            "  <td>" & rsSet("RISKLEVEL") & "</td>" &_
						            "</tr>"
		    'STRrow = STRrow & ASBESTOS
            STRrow = STRrow & "<tr><td colspan=""4""><table>" & ASBESTOS & "</table></td></tr>"
            str_data =  STRrow
		    rsSet.moveNext()
		Wend
		Call CloseRs(rsSet)
		Set rsSet = Nothing
		Call CloseDB()

		Function Build_Abestos_Detail(PropertyId)
		    Dim DevID

            'Collecting ASBESTOS data
            SQL="SELECT PAR.PROPASBRISKID AS PROPASBESTOSRISK ,PR.PROPASBLEVELID AS PROPASBESTOSLEVELID, " &_
	            "PL.ASBRISKLEVELID AS RISKLEVELID, PL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL,PRI.ASBRISKID AS RISK, " &_
	            "P.ASBESTOSID AS ELEMENTID, P.RISKDESCRIPTION AS ELEMENTDISCRIPTION, IsNull(convert(varchar(max),PR.DATEADDED,103),'No Data') AS DATEADDED, IsNull(convert(varchar(max),PR.DATEREMOVED,103),'No Data') AS DATEREMOVED FROM P_ASBRISK PRI,"&_
	            "P_PROPERTY_ASBESTOS_RISKLEVEL PR left join  P_PROPERTY_ASBESTOS_RISK PAR ON " &_ 
	            "PR.PROPASBLEVELID=PAR.PROPASBLEVELID INNER Join P_ASBRISKLEVEL PL " &_
	            "ON PR.ASBRISKLEVELID=PL.ASBRISKLEVELID INNER Join P_ASBESTOS P ON " &_
	            "PR.ASBESTOSID =P.ASBESTOSID WHERE PAR.ASBRISKID=PRI.ASBRISKID AND PR.PROPERTYID='" & PropertyID & "' ORDER BY PL.ASBRISKLEVELID DESC"

            Call OpenRs(rsASB, SQL)

                ASBESTOS=""
                Dim flag,COLUMNCOUNTER,ROWCOUNTER
                ReDim ASBESTOS_ARRAY(100,100)
                flag=0
                COLUMNCOUNTER=0
                ROWCOUNTER=1

                while NOT rsASB.EOF
                    for i=0 to COLUMNCOUNTER
                        if(ASBESTOS_ARRAY(i,0)= rsASB("RISKLEVEL")) then
                            'ASBESTOS_ARRAY(i,ROWCOUNTER)=rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b>"
                            ASBESTOS_ARRAY(i,ROWCOUNTER) = "<td>" & rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b></td><td>" & rsASB("DATEADDED") & "</td><td>" & rsASB("DATEREMOVED")
                            ROWCOUNTER=ROWCOUNTER+1
                            flag=1
                            exit for
                        end if
                    next
                    if(flag=0) then
                        ASBESTOS_ARRAY(COLUMNCOUNTER,0)=rsASB("RISKLEVEL")
                        'ASBESTOS_ARRAY(COLUMNCOUNTER,ROWCOUNTER)=rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b>"
                        ASBESTOS_ARRAY(COLUMNCOUNTER,ROWCOUNTER) = "<td>" & rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b></td><td>" & rsASB("DATEADDED") & "</td><td>" & rsASB("DATEREMOVED")
                        COLUMNCOUNTER=COLUMNCOUNTER+1
                        ROWCOUNTER=ROWCOUNTER+1
                     end if
                    flag=0
                    rsASB.movenext
                wend

                Call CloseRs(rsASB)

                if(COLUMNCOUNTER>=0) then
                    for i=0 to COLUMNCOUNTER
                        if(i<1 and ASBESTOS_ARRAY(i+1,0)="" ) then
                            ASBESTOS=ASBESTOS & "<tr><td></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=2>"
                        elseif(i=1) then
                            ASBESTOS=ASBESTOS & "<tr><td></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=2>"
                         else
                            ASBESTOS=ASBESTOS & "<tr><td></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=2>"
                        end if
                        'for j=1 to ROWCOUNTER
                            'If(ASBESTOS_ARRAY(i,j)<>"") then
                                'ASBESTOS=ASBESTOS & ASBESTOS_ARRAY(i,j) & "<br/>" 
                            'end if
                        'next
if i < COLUMNCOUNTER then
ASBESTOS=ASBESTOS & "<table cellpadding='0' cellspacing='2' style='border-collapse: collapse'>"
ASBESTOS=ASBESTOS & "<tr><td width=""300"">&nbsp;</td><td width=""100""><b>Added</b></td><td width=""100""><b>Removed</b></td></tr>"
                        for j=1 to ROWCOUNTER
                            If(ASBESTOS_ARRAY(i,j)<>"") then
ASBESTOS=ASBESTOS & "<tr>"
                                ASBESTOS=ASBESTOS & ASBESTOS_ARRAY(i,j) & "<br />"
ASBESTOS=ASBESTOS & "</tr>"
                            end if
                        next
ASBESTOS=ASBESTOS & "</table>"

end if
                        ASBESTOS=ASBESTOS & "</td></tr>"
                   next
                end if

		End Function

	'End Get Record to display
%>
<html>
<head>
    <title>RSL Manager Customers --> Contact and Communication Need</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
    body
    {
        background-color: White;
        margin: 10px 0px 0px 10px;
    }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table width="750" cellpadding="1" cellspacing="2" border="1">
        <tr>
            <td colspan="4">
                <div align="left">
                    <font color="#0066CC"><b><font size="4">
                        <%=HEADING%></font>
                        <%
                            Dim todaysDate
				                todaysDate = now()
				                Response.Write(todaysDate)
                        %>
                    </b></font>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<b>PROPERTY ID</b>
            </td>
            <td>
                &nbsp;<b>PROPERTY ADDRESS</b>&nbsp;
            </td>
            <td>
                &nbsp;<b>REFURBISHMENT DATE</b>&nbsp;
            </td>
            <td>
                &nbsp;<b>MAX RISK LEVEL</b>&nbsp;
            </td>
        </tr>
        <%=str_data%>
    </table>
</body>
</html>
