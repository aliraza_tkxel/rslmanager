<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Const UNAVAILABLE = 4
	COnst UNDERCONSTRUCTION = 1

	Dim lstDevelopments, lstStatus, lstSubStatus, lstAssetType, lstPropertyType, lstLevel
	Dim lstBlocks ,l_address1, lstHousingOfficers, disableAssetType
	
	OpenDB()

	Dim rsLoader, ACTION
	read_only = ""
	ACTION = "NEW"
	l_propertystatus = "New Property"
	PropertyID = Request("PropertyID")
	if (PropertyID = "") then PropertyID = -1 end if

	'Defaulyt values required to build select boxes
	l_blockid = -1
	l_developmentid = -1
	l_status = -1	
	l_propertyid = "Auto Generated"

	SQL = "SELECT P.*, D.SCHEMENAME FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID WHERE PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		' tbl E_JOBDETAILS
		l_propertyid = UCASE(rsLoader("PROPERTYID"))
		l_propertystatus = l_propertyid
		'l_propertyid = PropertyReference(rsLoader("PROPERTYID")) & "/" & l_propertyref
		
		l_schemename = rsLoader("SCHEMENAME")
		
		l_developmentid = rsLoader("DEVELOPMENTID")
		if (l_developmentid = "" OR isNull(l_developmentid)) then
			l_developmentid = -1
		end if
				
		l_blockid = rsLoader("BLOCKID")
		if (l_blockid = "" OR isNull(l_blockid)) then
			l_blockid = -1
		end if
		
		l_housenumber = rsLoader("HOUSENUMBER")
		l_address1 = rsLoader("ADDRESS1")
		l_address2 = rsLoader("ADDRESS2")	
		l_address3 = rsLoader("ADDRESS3")						
		l_towncity = rsLoader("TOWNCITY")						
		l_county = rsLoader("COUNTY")						
		l_postcode = rsLoader("POSTCODE")						
		l_housingofficer = rsLoader("HOUSINGOFFICER")								
		
		'this part checks for a current tenant in the property, in which case it disables the user from updating the status of the
		'selected property.
		'ExtraFilter = " WHERE STATUSID <> 2 "
		'SQL = "SELECT * FROM C_TENANCY T WHERE (T.ENDDATE IS NULL OR (T.ENDDATE IS NOT NULL AND T.ENDDATE > GETDATE())) AND T.PROPERTYID = '" & l_propertyid & "'"
		'Call OpenRs(rsTenant, SQL)
		'if (NOT rsTenant.EOF) then
		'	DisabledStatus = " disabled"
		'	ExtraFilter = ""
		'else
		'	SQL = "SELECT * FROM C_ADDITIONALASSET WHERE (ENDDATE IS NULL OR (ENDDATE IS NOT NULL AND ENDDATE > GETDATE())) AND PROPERTYID = '" & l_propertyid & "'"			
		'	Call OpenRs(rsAdTenant, SQL)
		'	if (NOT rsAdTenant.EOF) then
		'		DisabledStatus = " disabled"
		'		ExtraFilter = ""				
		'	END IF
		'	CloseRs(rsAdTenant)
		'end if
		'CloseRs(rsTenant)
		
		l_status = rsLoader("STATUS")
		If isNull(rsLoader("SUBSTATUS")) Then
			l_substatus = -1
		Else
			l_substatus = rsLoader("SUBSTATUS")						
		End If
		l_avdate = rsLoader("AVDATE")						
		l_propertytype = rsLoader("PROPERTYTYPE")						
		l_assettype = rsLoader("ASSETTYPE")						
		l_propertylevel = rsLoader("PROPERTYLEVEL")
	
		l_datebuilt = rsLoader("DATEBUILT")						
		l_viewingdate = rsLoader("VIEWINGDATE")						
		l_lettingdate = rsLoader("LETTINGDATE")
	
		l_righttobuy = rsLoader("RIGHTTOBUY")
		l_unitdesc = rsLoader("UNITDESC")
		l_defectsperiodend = rsLoader("DEFECTSPERIODEND")		
		disableAssetType = " disabled "
		ACTION = "AMEND"
	else		' FOR A NEW PROPERTY STATUS MUST BE SET TO UNAVAILABLE
		l_status = UNAVAILABLE
		l_substatus = UNDERCONSTRUCTION		
	end if
	
	CloseRs(rsLoader)

	SQL = "SELECT * FROM P_SUBSTATUS S WHERE S.STATUSID = " & l_status & " order by description "
	Call OpenRs(rsSubStatus, SQL)
	
	count = 0
	optionstr = "<select name='sel_SUBSTATUS' class='textbox200' DISABLED><option value=''>Please Select</option>"
	while (NOT rsSubStatus.EOF) 
		isSelected = ""
		if (CStr(rsSubStatus("SUBSTATUSID")) = CStr(l_substatus)) then
			isSelected = " selected"
		end if
		optionstr = optionstr & "<option value=""" & rsSubStatus("SUBSTATUSID") & """ " & isSelected & ">" & rsSubStatus("DESCRIPTION") & "</option>"
		rsSubStatus.moveNext
		count = 1
	wend	
	if (count = 0) then
		lstSubStatus = "<select name='sel_SUBSTATUS' class='textbox200' disabled><option value=''>Not Applicable</option></select>"
		SubStatusValidate = "N"
	else
		lstSubStatus = optionstr & "</select>"
		SubStatusValidate = "N"
	end if

	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "P_development", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_developmentid, NULL, "textbox200", " onchange=""GetBlocks()""  TABINDEX=1" )
	Call BuildSelect(lstBlocks, "sel_BLOCKID", "P_BLOCK WHERE DEVELOPMENTID = " & l_developmentid, "BLOCKID, BLOCKNAME", "BLOCKNAME", "Please Select", l_blockid, NULL, "textbox200", " onchange=""GetAddressData()""  TABINDEX=1")
	'Call BuildSelect(lstStatus, "sel_STATUS", "P_STATUS" & ExtraFilter, "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_status, NULL, "textbox200", " onchange=""GetSubStatus()""  TABINDEX=2 READONLY " & DisabledStatus & " ")
	Call BuildSelect(lstStatus, "sel_STATUS", " P_STATUS ", "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_status, NULL, "textbox200", " TABINDEX=2 disabled ")
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_assettype, NULL, "textbox200", " TABINDEX=2 " & disableAssetType & " ")
	Call BuildSelect(lstPropertyType, "sel_PROPERTYTYPE", "P_PROPERTYTYPE", "PROPERTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_propertytype, NULL, "textbox200", " TABINDEX=2")
	Call BuildSelect(lstLevel, "sel_PROPERTYLEVEL", "P_LEVEL", "LEVELID, DESCRIPTION", "DESCRIPTION", "Please Select", l_propertylevel, NULL, "textbox200", " TABINDEX=2")
	Call BuildSelect(lstHousingOfficers, "sel_HOUSINGOFFICER", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID INNER JOIN G_TEAMCODES G ON G.TEAMID = J.TEAM WHERE G.TEAMCODE IN ('HOU','FIN','EST') AND J.ACTIVE = 1 ", "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", l_housingofficer, NULL, "textbox200", " TABINDEX=2")	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio--&gt; Main Details</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

var FormFields = new Array();
FormFields[0] = "txt_PROPERTYID|Property Ref No|TEXT|Y"
FormFields[1] = "sel_PROPERTYLEVEL|Property Level|SELECT|N"
FormFields[2] = "txt_AVDATE|Estimated Av Date|DATE|N"
FormFields[3] = "txt_HOUSENUMBER|House Number|TEXT|Y"
FormFields[4] = "txt_ADDRESS1|Address 1|TEXT|Y"
FormFields[5] = "txt_ADDRESS2|Address 2|TEXT|N"
FormFields[6] = "txt_DATEBUILT|Date Built|DATE|N"
FormFields[7] = "txt_ADDRESS3|Address 3|TEXT|N"
FormFields[8] = "txt_VIEWINGDATE|Viewing Date|DATE|N"
FormFields[9] = "txt_TOWNCITY|Town / City|TEXT|Y"
FormFields[10] = "txt_LETTINGDATE|Letting Date|DATE|N"
FormFields[11] = "txt_UNITDESC|Unit Description|TEXT|N"
FormFields[12] = "rdo_RIGHTTOBUY|Right To buy|RADIO|Y"
FormFields[13] = "sel_ASSETTYPE|Asset Type|SELECT|Y"
FormFields[14] = "txt_COUNTY|COUNTY|TEXT|N"
FormFields[15] = "txt_POSTCODE|Postcode|POSTCODE|N"
FormFields[16] = "sel_STATUS|Status|SELECT|Y"
FormFields[17] = "sel_DEVELOPMENTID|Development|SELECT|Y"
FormFields[18] = "sel_SUBSTATUS|Sub - Status|SELECT|<%=SubStatusValidate%>"
FormFields[19] = "sel_BLOCKID|Block|SELECT|N"
FormFields[20] = "sel_PROPERTYTYPE|Property Type|SELECT|Y"
FormFields[21] = "sel_HOUSINGOFFICER|Housing Officer|SELECT|Y"
FormFields[22] = "txt_DEFECTSPERIODEND|Defects Period|DATE|N"

function SaveForm(){
	if (!checkForm()) return false
	RSLFORM.action = "ServerSide/Property_svr.asp"
	RSLFORM.target = ""
	RSLFORM.submit()
	} 

function GetBlocks(){
	RSLFORM.target = "Property_Server"
	RSLFORM.action = "Serverside/Property_Blocks.asp"
	RSLFORM.submit()
	}

function GetSubStatus(){
	FormFields[18] = "sel_SUBSTATUS|SUBSTATUS|SELECT|N"
	parent.RSLFORM.sel_SUBSTATUS.disabled = true
	RSLFORM.target = "Property_Server"
	RSLFORM.action = "Serverside/Property_SubStatus.asp"
	RSLFORM.submit()
	}

function GetAddressData(){
	RSLFORM.target = "Property_Server"
	RSLFORM.action = "Serverside/Property_Address.asp"
	RSLFORM.submit()	
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method="post">
  <table width=750 border=0 cellpadding=0 cellspacing=0>
    <% if ACTION = "NEW" then %>
    <tr> 
      <td rowspan=2> <img name="tab_main_details" src="images/tab_main_details-over.gif" width=97 height=20 border=0></td>
      <td rowspan=2> <img name="tab_main_details" src="images/TabEnd.gif" width=8 height=20 border=0></td>
      <td align=right height=19><font color=red><%=l_propertystatus%></font></td>
    </tr>
    <tr> 
      <td bgcolor=#133E71> <img src="images/spacer.gif" width=649 height=1></td>
    </tr>
    <% else %>
    <tr> 
      <td rowspan=2> <img name="tab_main_details" src="images/tab_main_details-over.gif" width=97 height=20 border=0></td>
      <td rowspan=2> <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>"><img name="tab_financial" src="images/tab_financial-tab_main_deta.gif" width=79 height=20 border=0></a></td>      
      <td rowspan=2> <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>"><img name="tab_warranties" src="images/tab_warranties.gif" width=89 height=20 border=0></a></td>
      <td rowspan=2> <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>"><img name="tab_utilities" src="images/tab_utilities.gif" width=68 height=20 border=0></a></td>
      <td rowspan=2> <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>"><img name="tab_marketing" src="images/tab_marketing.gif" width=90 height=20 border=0></a></td>
      <td align=right height=19><font color=red><%=l_housenumber & " " & l_address1%> - <%=l_propertystatus%></font></td>
    </tr>
    <tr> 
      <td bgcolor=#133E71> <img src="images/spacer.gif" width=247 height=1></td>
    </tr>
    <% end if %>
    <tr> 
      <td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="images/spacer.gif" width=53 height=6></td>
    </tr>
  </table>
  <table width="99%" border="0" cellpadding="2" cellspacing="0" style=" border-right: 1px solid #133E71; border-LEFT: 1px solid #133E71;  border-BOTTOM: 1px solid #133E71 ">
    <tr> 
      <td nowrap>Property Ref:</td>
      <td nowrap> 
        <input type="text" name="txt_PROPERTYID" class="textbox200" readonly MAXLENGTH="20" VALUE="<%=l_propertyid%>" style='text-transform:uppercase' TABINDEX=1>
      </td>
      <td  nowrap> <image src="/js/FVS.gif" name="img_PROPERTYID" width="15px" height="15px" border="0"> 
      </td>
      <td nowrap>Status:</td>
      <td nowrap> <%=lstStatus%> </td>
      <td><image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"></td>
      <td width=100%>&nbsp;</td>
    </tr>
    <tr> 
      <td nowrap>Development Name:</td>
      <td nowrap> <%=lstDevelopments%> </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_DEVELOPMENTID" width="15px" height="15px" border="0"></td>
      <td nowrap>Sub - Status:</td>
      <td nowrap> <%=lstSubStatus%> </td>
      <td><image src="/js/FVS.gif" name="img_SUBSTATUS" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Scheme Name:</td>
      <td nowrap> 
        <input type="text" name="txt_SCHEMENAME" class="textbox200" readonly MAXLENGTH="50" VALUE="<%=l_schemename%>" TABINDEX=1>
      </td>
      <td  nowrap></td>
      <td nowrap>Est. Practical Completion: </td>
      <td nowrap> 
        <input type="text" name="txt_AVDATE" class="textbox200" MAXLENGTH="10" VALUE="<%=l_avdate%>" TABINDEX=2>
      </td>
      <td><image src="/js/FVS.gif" name="img_AVDATE" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Block:</td>
      <td nowrap> <%=lstBlocks%> </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_BLOCKID" width="15px" height="15px" border="0"></td>
      <td nowrap>Property Type:</td>
      <td nowrap> <%=lstPropertyType%> </td>
      <td><image src="/js/FVS.gif" name="img_PROPERTYTYPE" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>House Number:</td>
      <td nowrap> 
        <input type="text" name="txt_HOUSENUMBER" class="textbox200" MAXLENGTH="10" VALUE="<%=l_housenumber%>" TABINDEX=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_HOUSENUMBER" width="15px" height="15px" border="0"></td>
      <td nowrap>Asset Type:</td>
      <td nowrap> <%=lstAssetType%> </td>
      <td><image src="/js/FVS.gif" name="img_ASSETTYPE" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Address 1:</td>
      <td nowrap> 
        <input type="text" name="txt_ADDRESS1" class="textbox200" MAXLENGTH="100" VALUE="<%=l_address1%>" TABINDEX=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_ADDRESS1" width="15px" height="15px" border="0"></td>
      <td nowrap>Level:</td>
      <td nowrap> <%=lstLevel%> </td>
      <td><image src="/js/FVS.gif" name="img_PROPERTYLEVEL" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Address 2:</td>
      <td nowrap> 
        <input type="text" name="txt_ADDRESS2" class="textbox200" MAXLENGTH="100" VALUE="<%=l_address2%>" TABINDEX=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_ADDRESS2" width="15px" height="15px" border="0"></td>
      <td nowrap>Date Built:</td>
      <td nowrap> 
        <input type="text" name="txt_DATEBUILT" class="textbox200" MAXLENGTH="10" VALUE="<%=l_datebuilt%>" TABINDEX=2>
      </td>
      <td><image src="/js/FVS.gif" name="img_DATEBUILT" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Address 3:</td>
      <td nowrap> 
        <input type="text" name="txt_ADDRESS3" class="textbox200" MAXLENGTH="100" VALUE="<%=l_address3%>" TABINDEX=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_ADDRESS3" width="15px" height="15px" border="0"></td>
      <td nowrap>Viewing Date:</td>
      <td nowrap> 
        <input type="text" name="txt_VIEWINGDATE" class="textbox200" MAXLENGTH="10" VALUE="<%=l_viewingdate%>" TABINDEX=2>
      </td>
      <td><image src="/js/FVS.gif" name="img_VIEWINGDATE" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Town / City:</td>
      <td nowrap> 
        <input type="text" name="txt_TOWNCITY" class="textbox200" MAXLENGTH="80" VALUE="<%=l_towncity%>" TABINDEX=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_TOWNCITY" width="15px" height="15px" border="0"></td>
      <td nowrap>Hand-over  Date:</td>
      <td nowrap> 
        <input type="text" name="txt_LETTINGDATE" class="textbox200" MAXLENGTH="10" VALUE="<%=l_lettingdate%>" TABINDEX=2>
      </td>
      <td><image src="/js/FVS.gif" name="img_LETTINGDATE" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>County:</td>
      <td nowrap> 
        <input type="text" name="txt_COUNTY" class="textbox200" maxlength="50" value="<%=l_county%>" tabindex=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_COUNTY" width="15px" height="15px" border="0"></td>
      <td nowrap>Right to Acquire:</td>
      <td nowrap> 
        <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_righttobuy = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
        <input type="radio" name="rdo_RIGHTTOBUY" VALUE="1" <%=yesChecked%> TABINDEX=2>
        Yes 
        <input type="radio" name="rdo_RIGHTTOBUY" VALUE="0" <%=noChecked%> TABINDEX=2>
        No </td>
      <td><image src="/js/FVS.gif" name="img_RIGHTTOBUY" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Postcode:</td>
      <td nowrap> 
        <input type="text" name="txt_POSTCODE" class="textbox200" maxlength="10" value="<%=l_postcode%>" tabindex=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_POSTCODE" width="15px" height="15px" border="0"></td>
      <td nowrap>Housing Officer:</td>
      <td nowrap> 
        <%=lstHousingOfficers%>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_HOUSINGOFFICER" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Defects Period:</td>
      <td nowrap> 
        <input type="text" name="txt_DEFECTSPERIODEND" class="textbox200" maxlength="10" value="<%=l_defectsperiodend%>" tabindex=1>
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_DEFECTSPERIODEND" width="15px" height="15px" border="0"></td>
    </tr>	
    <tr> 
      <td nowrap colspan=7 STYLE='DISPLAY:NONE'> 
        <hr style='border:1px dotted #133e71'>
      </td>
    </tr>
    <tr STYLE='DISPLAY:NONE'> 
      <td nowrap valign=top>UNIT DESCRIPTION:</td>
      <td nowrap colspan=4> 
        <textarea name="txt_UNITDESC" class="textbox" rows=4 cols=88 TABINDEX=3><%=l_unitdesc%></TEXTAREA>
      </td>
      <td valign=top><image src="/js/FVS.gif" name="img_UNITDESC" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap colspan=5 ALIGN=RIGHT> 
	  	<input type=hidden name="hid_Address" value="<%=l_address1%>">	
        <input type=hidden name="hid_PropertyID" value="<%=PropertyID%>">
        <input type=hidden name="hid_Action" value="<%=ACTION%>">
        <input type="button" onclick="SaveForm()" value=" SAVE " class="RSLButton" TABINDEX=3>
      </td>
    </tr>
  </table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name="Property_Server" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

