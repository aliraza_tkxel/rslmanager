<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size
	Dim nopounds
	Dim searchSQL,searchName

	searchName = Replace(Request("name"),"'","''")
	searchSQL = " where 1=1 "
	if searchName <> "" Then
		 searchSQL = searchSQL & " and (DEVELOPMENTNAME LIKE '%" & searchName & "%' ) "
	End If

    Company = Request("Company")
    if (Company="") then Company = "1"

    CompanyFilter = Request("CompanyFilter")
    if (CompanyFilter<>"") then 
        searchSQL = searchSQL & " and (d.Companyid = " & CompanyFilter & " ) "
    end if 
    

	nopounds = 0

	Call getTeams()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	' retrives team details from database and builds 'paged' report
	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT  DEVELOPMENTID, DEVELOPMENTNAME, STARTDATE = ISNULL(CONVERT(VARCHAR, STARTDATE, 103), 'N/A'), " &_
					"		 TARGETDATE = ISNULL(CONVERT(VARCHAR, TARGETDATE, 103),'N/A'), " &_
					"			ISNULL(SCHEMENAME, 'N/A') AS SCHEMENAME, " &_
					"			T.DESCRIPTION AS TYPE " &_
					"			FROM P_DEVELOPMENT D " &_
					"				LEFT JOIN P_DEVELOPMENTTYPE T ON T.DEVELOPMENTTYPEID = D.DEVELOPMENTTYPE " &_
					"" & searchSQL & ""&_
					" ORDER BY DEVELOPMENTNAME"

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<tbody class=""CAPS"">"
			For intRecord = 1 to rsSet.PageSize

				team_name = rsSet("DEVELOPMENTNAME")
				str_data = str_data & "<tr style=""cursor:pointer"" onclick=""load_team(" & rsSet("DEVELOPMENTID") & ")"">" &_
					"<td nowrap=""nowrap"">" & rsSet("DEVELOPMENTNAME") & "</td>" &_
					"<td>" & rsSet("SCHEMENAME") & "</td>" &_
					"<td>" & rsSet("TYPE") & "</td>" &_
					"<td>" & rsSet("STARTDATE") & "</td>" &_
					"<td>" & rsSet("TARGETDATE") & "</td>" &_
					"<td onclick=""del_team(" & rsSet("DEVELOPMENTID") & ")"" style=""border-bottom:1px solid silver"" class=""DEL"">DEL</td></tr>"
					' removed until referential integrity is implemented - PP
					'"<td style=""border-bottom:1px solid silver"" class=""DEL"" onclick=""del_team(" & rsSet("DEVELOPMENTID") & ")"">Del</td></tr>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""7"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">" &_
			"<a href='DEVELOPMENT.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='DEVELOPMENT.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='DEVELOPMENT.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='DEVELOPMENT.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td><td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71; font-size:11px"">&nbsp;"  &_
			"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" title=""GO"" onclick=""JumpPage()"" style=""font-size:10px; cursor:pointer"">"  &_
			"</td></tr></thead></table></td></tr></tfoot>" 
			
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""6"" align=""center""><b>No DEVELOPMENTS exist within the system !!</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""7"" align=""center"" style=""background-color:white"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Dim lstLeadOfficers, lstSupportOfficers, lstLocalAuthorities, lstUseTypes, lstDevelopmentTypes, lstMostSuitable, lstFundingTypes, lstSupportedClients
	Dim lstContractors, lstProjectManager, rsDev

	SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = 'PARTD'"
	Call OpenDB()
	Call OpenRs(rsDev, SQL)
	If (NOT rsDev.EOF) Then
		DevTeam  = rsDev("TEAMID")
	Else
		DevTeam = -1
	End If
	Call CloseRs(rsDev)

	lstOfficersWhere = " E__EMPLOYEE E, E_JOBDETAILS J WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = " & DevTeam
	Call BuildSelect(lstLeadOfficers, "sel_LEADDEVOFFICER", lstOfficersWhere, "E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME", "FIRSTNAME + ' ' + LASTNAME", "Please Select", NULL, NULL, "textbox200", " tabindex=""13"" ")
	Call BuildSelect(lstSupportOfficers, "sel_SUPPORTDEVOFFICER", lstOfficersWhere, "E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME", "FIRSTNAME + ' ' + LASTNAME", "Please Select", NULL, NULL, "textbox200", " tabindex=""14"" ")
	Call BuildSelect(lstSurveyor, "sel_SURVEYOR", lstOfficersWhere, "E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME", "FIRSTNAME + ' ' + LASTNAME", "Please Select", NULL, NULL, "textbox200", " tabindex=""15"" ")
	Call BuildSelect(lstLocalAuthorities, "sel_LOCALAUTHORITY", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " tabindex=""12"" ")
	Call BuildSelect(lstDevelopmentTypes, "sel_DEVELOPMENTTYPE", "P_DEVELOPMENTTYPE", "DEVELOPMENTTYPEID, DESCRIPTION", "DEVELOPMENTTYPEID", "Please Select", NULL, NULL, "textbox200", " tabindex=""8"" ")
	Call BuildSelect(lstUseTypes, "sel_USETYPE", "P_USETYPE", "USETYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " tabindex=""10"" ")	
	Call BuildSelect(lstMostSuitable, "sel_SUITABLEFOR", "P_MOSTSUITABLEFOR", "MOSTSUITABLEFORID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " tabindex=""11"" ")	
	Call BuildSelect(lstContractors, "sel_CONTRACTOR", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select", NULL, NULL, "textbox200", " tabindex=""16"" ")	
	Call BuildSelect(lstPatch, "sel_PATCHID", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "Please Select", NULL, NULL, "textbox200", " tabindex=11")	
	Call BuildSelectListBox(lstFundingTypes, "sel_FUNDINGTYPE", "P_DEVELOPMENT_FUNDING_NROSH", "NROSH_CODE, DESCRIPTION", "NROSH_CODE", null, NULL, NULL, "listbox200", " tabindex=""8"" ")
	Call BuildSelect(lstSupportedClients, "sel_SUPPORTEDCLIENTS", "P_DEVELOPMENT_SUPPORTEDCLIENTS", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " tabindex=""16"" ")	
	Call BuildSelect(lstCompany, "sel_COMPANYID", "G_Company", "CompanyId, Description", "CompanyId", NULL, NULL, NULL, "textbox200", "   tabindex=5")
    Call BuildSelect(lstCompanyFilter, "sel_COMPANY", "G_Company", "CompanyId, Description", "CompanyId", "Select Company..", CompanyFilter, NULL, "textbox100", " style='width:150px'   tabindex=5")


    
    Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Portfolio --&gt; Tools --&gt; Development Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_DEVELOPMENTNAME|Development Name|TEXT|Y"
        FormFields[1] = "txt_SCHEMENAME|Scheme Name|TEXT|Y"
        FormFields[2] = "txt_SCHEMECODE|Scheme Code|TEXT|N"
        FormFields[3] = "txt_STARTDATE|Contract Start|DATE|Y"
        FormFields[4] = "txt_TARGETDATE|Contract Completion|DATE|N"
        FormFields[5] = "txt_ACTUALDATE|Actual Date|DATE|N"
        FormFields[6] = "txt_DEFECTSPERIOD|Defects Period|DATE|N"
        FormFields[7] = "sel_DEVELOPMENTTYPE|Type|SELECT|Y"
        FormFields[8] = "txt_NUMBEROFUNITS|Number of Units|INTEGER|N"
        FormFields[9] = "sel_USETYPE|Dev Type|SELECT|N"
        FormFields[10] = "sel_SUITABLEFOR|Suitable For|SELECT|N"
        FormFields[11] = "sel_LOCALAUTHORITY|Local Authority|SELECT|Y"
        FormFields[12] = "sel_LEADDEVOFFICER|Lead Dev Officer|SELECT|N"
        FormFields[13] = "sel_SUPPORTDEVOFFICER|Support Dev Officer|SELECT|N"
        FormFields[14] = "txt_PROJECTMANAGER|Project Manager|SELECT|N"
        FormFields[15] = "sel_CONTRACTOR|Contractor|SELECT|N"
        FormFields[16] = "txt_INSURANCEVALUE|Insurance Value|FLOAT|N|2"
        FormFields[17] = "txt_AQUISITIONCOST|Aquisition Cost|CURRENCY|N"
        FormFields[18] = "txt_CONTRACTORCOST|Contractor Cost|CURRENCY|N"
        FormFields[19] = "txt_OTHERCOST|Other Cost|CURRENCY|N"
        FormFields[20] = "txt_SCHEMECOST|Scheme Cost|CURRENCY|N"
        FormFields[21] = "txt_DEVELOPMENTADMIN|Development Admin|CURRENCY|N"
        FormFields[22] = "txt_INTERESTCOST|Interest|CURRENCY|N"
        FormFields[23] = "txt_BALANCE|Balance|CURRENCY|N"
        FormFields[24] = "txt_HAG|HAG|CURRENCY|N"
        FormFields[25] = "txt_MORTGAGE|Mortgage|CURRENCY|N"
        FormFields[26] = "txt_BORROWINGRATE|Borrowing Rate|CURRENCY|N"
        FormFields[27] = "txt_NETTCOST|Nett Cost|CURRENCY|N"
        FormFields[28] = "sel_PATCHID|Patch|SELECT|Y"
        FormFields[29] = "sel_SUPPORTEDCLIENTS|Supported Clients|SELECT|N";
        FormFields[30] = "sel_SURVEYOR|Surveyor|SELECT|N";
        FormFields[31] = "sel_COMPANYID|Company|SELECT|N";
        // called by save and amend buttons

        function click_save(str_action) {
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_team";
            document.getElementById("hid_ACTION").value = str_action
            document.RSLFORM.action = "serverside/development_srv.asp"
            document.RSLFORM.submit();
        }

        function SetCyclicalRepairs(int_id) {
            event.cancelBubble = true
            location.href = "CyclicalRepairs.asp?DevelopmentID=" + int_id
        }

        // called by delete button
        function del_team(int_id) {
            event.cancelBubble = true
            var answer
            answer = window.confirm("Are you sure you wish to delete this development?")
            // if yes then send to server page to delete
            if (answer) {
                document.RSLFORM.target = "frm_team";
                document.getElementById("hid_ACTION").value = "DELETE"
                document.getElementById("hid_DEVELOPMENTID").value = int_id;
                document.RSLFORM.action = "serverside/development_srv.asp"
                document.RSLFORM.submit();
            }
        }

        // called when user clicks on a row
        function load_team(int_id) {
            document.RSLFORM.target = "frm_team";
            document.getElementById("hid_ACTION").value = "LOAD"
            document.getElementById("hid_DEVELOPMENTID").value = int_id;
            document.RSLFORM.action = "serverside/development_srv.asp"
            document.RSLFORM.submit();
        }

        // swaps save button for amend button and vice versa
        // called by server side page
        function swap_button(int_which_one) { //   1 = show amend, 2 = show save
            if (int_which_one == 2) {
                document.getElementById("BTN_SAVE").style.display = "block"
                document.getElementById("BTN_AMEND").style.display = "none"
            }
            else {
                document.getElementById("BTN_SAVE").style.display = "none"
                document.getElementById("BTN_AMEND").style.display = "block"
            }
        }

        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING
        function swap_div(int_which_one) {
            if (int_which_one == 1) {
                document.getElementById("new_div").style.display = "none";
                document.getElementById("table_div").style.display = "block";
                document.getElementById("tab_dev").src = 'images/dev-open.gif';
                document.getElementById("tab_dev_team").src = 'images/new-dev-prev.gif';
                document.getElementById("tab_amend_dev").src = 'images/amend-dev-closed-inviso.gif';
            }
            else if (int_which_one == 2) {
                clear()
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_dev").src = 'images/dev-closed.gif';
                document.getElementById("tab_dev_team").src = 'images/new-dev-open.gif';
                document.getElementById("tab_amend_dev").src = 'images/amend-dev-closed-inviso2.gif';
                swap_button(2)
            }
            else {
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_dev").src = 'images/dev-closed.gif';
                document.getElementById("tab_dev_team").src = 'images/new-dev-closed.gif';
                document.getElementById("tab_amend_dev").src = 'images/amend-dev-open.gif';
                swap_button(3)
            }
            checkForm()
        }

        // clear form fields
        function clear() {
            document.getElementById("txt_DEVELOPMENTNAME").value = ""
            document.getElementById("txt_SCHEMENAME").value = ""
            document.getElementById("txt_SCHEMECODE").value = ""
            document.getElementById("txt_STARTDATE").value = ""
            document.getElementById("txt_TARGETDATE").value = ""
            document.getElementById("txt_ACTUALDATE").value = ""
            document.getElementById("txt_DEFECTSPERIOD").value = ""
            document.getElementById("sel_DEVELOPMENTTYPE").value = ""
            document.getElementById("txt_NUMBEROFUNITS").value = ""
            document.getElementById("sel_USETYPE").value = ""
            document.getElementById("sel_SUITABLEFOR").value = ""
            document.getElementById("sel_LOCALAUTHORITY").value = ""
            document.getElementById("sel_LEADDEVOFFICER").value = ""
            document.getElementById("sel_SUPPORTDEVOFFICER").value = ""
            document.getElementById("sel_PATCHID").value = ""
            document.getElementById("txt_PROJECTMANAGER").value = ""
            document.getElementById("sel_CONTRACTOR").value = ""
            document.getElementById("txt_INSURANCEVALUE").value = ""
        }

        function JumpPage() {
            iPage = document.getElementById("QuickJumpPage").value
            if (iPage != "" && !isNaN(iPage))
                location.href = "Development.asp?page=" + iPage
            else
                document.getElementById("QuickJumpPage").value = ""
        }

        function calc_cost() {
            if (!checkForm(true)) return false;
            // calculate scheme cost
            var ac = parseFloat(document.getElementById("txt_AQUISITIONCOST").value);
            var con = parseFloat(document.getElementById("txt_CONTRACTORCOST").value);
            var other = parseFloat(document.getElementById("txt_OTHERCOST").value);
            var schemecost = ac + con + other
            document.getElementById("txt_SCHEMECOST").value = FormatCurrency(schemecost);
            // calculate balance cost
            var admin = parseFloat(document.getElementById("txt_DEVELOPMENTADMIN").value);
            var int = parseFloat(document.getElementById("txt_INTERESTCOST").value);
            var bal = schemecost - (admin + int)
            document.getElementById("txt_BALANCE").value = FormatCurrency(bal);
            // calculate nett cost
            var hag = parseFloat(document.getElementById("txt_HAG").value);
            var nett = bal - hag
            document.getElementById("txt_NETTCOST").value = FormatCurrency(nett);
        }

        function addOption_list() {
            for (i = document.RSLFORM.sel_FUNDINGTYPE.options.length - 1; i >= 0; i--) {
                var Category = document.RSLFORM.sel_FUNDINGTYPE;
                if (document.RSLFORM.sel_FUNDINGTYPE[i].selected) {
                    addOption(document.RSLFORM.sel_DEVELOPMENTFUNDINGLIST, document.RSLFORM.sel_FUNDINGTYPE[i].text, document.RSLFORM.sel_FUNDINGTYPE[i].value);
                }
            }
        }

        function addOption(selectbox, text, value) {
            var optn = document.createElement("OPTION");
            optn.text = text;
            optn.value = value;
            if (selectbox.options.length > 0) {
                for (k = selectbox.options.length - 1; k >= 0; k--) {

                    if (selectbox.options[k].text == optn.text)
                    { return true; }
                }
                selectbox.options.add(optn);
                document.getElementById("txtList").value = document.getElementById("txtList").value + "," + optn.value;
            }
            else {
                selectbox.options.add(optn);
                document.getElementById("txtList").value = optn.value;
            }
        }


        function removeOptions(selectbox) {
            var i;
            for (i = selectbox.options.length - 1; i >= 0; i--) {
                if (selectbox.options[i].selected)
                    selectbox.remove(i);
            }
            document.getElementById("txtList").value = "";
            for (j = selectbox.options.length - 1; j >= 0; j--) {
                if (document.getElementById("txtList").value == "") {
                    document.getElementById("txtList").value = selectbox.options[j].value;
                }
                else {
                    document.getElementById("txtList").value = selectbox.options[j].value + "," + document.RSLFORM.txtList.value;
                }
            }
        }

        function filltextbox() {
            document.getElementById("txtList").value = '';
            for (p = document.RSLFORM.sel_DEVELOPMENTFUNDINGLIST.options.length - 1; p >= 0; p--) {
                if (document.getElementById("txtList").value != '')
                { document.getElementById("txtList").value = document.RSLFORM.sel_DEVELOPMENTFUNDINGLIST.options[p].value + "," + document.RSLFORM.txtList.value; }
                else
                { document.getElementById("txtList").value = document.RSLFORM.sel_DEVELOPMENTFUNDINGLIST.options[p].value; }
            }
        }

        function quick_find() {
            location.href = "Development.asp?name=" + document.getElementById("findemp").value + "&CompanyFilter=" + document.getElementById("sel_COMPANY").value;;
        }

        function SetSchemeName() {
            if (document.getElementById("txt_SCHEMENAME").value == "") {
                document.getElementById("txt_SCHEMENAME").value = document.getElementById("txt_DEVELOPMENTNAME").value
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_dev" id="tab_dev" onclick="swap_div(1)" style="cursor: pointer" src="images/dev-open.gif"
                    width="100" height="20" border="0" alt="" /></td>
            <td rowspan="2">
                <img name="tab_dev_team" id="tab_dev_team" onclick="swap_button(2);swap_div(2)" style="cursor: pointer"
                    src="images/new-dev-prev.gif" width="133" height="20" border="0" alt="" /></td>
            <td rowspan="2">
                <img name="tab_amend_dev" id="tab_amend_dev" src="images/amend-dev-closed-inviso.gif" width="155" height="20"
                    border="0" alt="" /></td>
            <td height="19" align="right" nowrap="nowrap">
                <%=lstCompanyFilter %>
                
                &nbsp;<input type="text" size="18" name="findemp" id="findemp" class="textbox100" style="cursor:pointer" />
                <input type="button" name="btn_FIND" id="btn_FIND" title="Search for matches using Development Name."
                    class="RSLBUTTON" value="Find" onclick="quick_find()" style="cursor:pointer" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="362" height="1" alt="" /></td>
        </tr>
    </table>
    <div id="new_div" style="display: none">
        <table width="750" style="border-right: solid 1px #133e71" cellpadding="1" cellspacing="2"
            border="0" class="tab_table_without_bottom">
            <tr style="height: 3px">
                <td>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    Development Name:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_DEVELOPMENTNAME" id="txt_DEVELOPMENTNAME" maxlength="100"
                        size="30" tabindex="1" onblur="SetSchemeName()" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DEVELOPMENTNAME" id="img_DEVELOPMENTNAME" width="15px" height="15px" border="0" alt="" />
                </td>
                <td rowspan="8">
                    <table border="0">
                        <tr>
                            <td>
                                <div style="height: 200px; overflow: scroll; width: 150px;">
                                    <%=lstFundingTypes%>
                                </div>
                            </td>
                            <td>
                                <input type="button" name="move" value=">>" onclick="addOption_list();" />
                                <br />
                                <input type="button" name="move" value="<<" onclick="removeOptions(sel_DEVELOPMENTFUNDINGLIST);" />
                            </td>
                            <td rowspan="1">
                                <div style="height: 200px; overflow: scroll; width: 150px;">
                                    <select name="sel_DEVELOPMENTFUNDINGLIST" id="sel_DEVELOPMENTFUNDINGLIST" multiple="multiple"
                                        class="listbox200">
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="3" rowspan="8" align="center" valign="middle" nowrap="nowrap">
                    <!--
			THIS AREA HAS BEEN HIDDEN BECAUSE THE FIGURES WHICH WERE ENTERED HERE WERE MANUAL FIGURES, WHICH SHOULD ARE NOW CALCULATED
			USING THE FINANCE SECTION - GO TO ACCOUNTS->REPORTS->DEVELOPMENT REPORT.
			THESE CAN BE CLEARED IF NOT USED AFTER THE 31/APRIL 2005. ZANFAR ALI
			-->
                    <table width="95%" style="height:95%; display: none; border: solid 1px #133e71"
                        border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td>
                                Aquisition
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_AQUISITIONCOST" id="txt_AQUISITIONCOST" maxlength="10" size="10"
                                    tabindex="18" />
                                <img src="/js/FVS.gif" name="img_AQUISITIONCOST" id="img_AQUISITIONCOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                            <td>
                                Dev. Admin.
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_DEVELOPMENTADMIN" id="txt_DEVELOPMENTADMIN" maxlength="10" size="10"
                                    tabindex="21" />
                                <img src="/js/FVS.gif" name="img_DEVELOPMENTADMIN" id="img_DEVELOPMENTADMIN" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contractor
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_CONTRACTORCOST" id="txt_CONTRACTORCOST" maxlength="10" size="10"
                                    tabindex="19" />
                                <img src="/js/FVS.gif" name="img_CONTRACTORCOST" id="img_CONTRACTORCOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                            <td>
                                Interest
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_INTERESTCOST" id="txt_INTERESTCOST" maxlength="10" size="10"
                                    tabindex="22" />
                                <img src="/js/FVS.gif" name="img_INTERESTCOST" id="img_INTERESTCOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Other
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_OTHERCOST" id="txt_OTHERCOST" maxlength="10" size="10"
                                    tabindex="20" />
                                <img src="/js/FVS.gif" name="img_OTHERCOST" id="img_OTHERCOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Scheme cost</b>
                            </td>
                            <td>
                                <input readonly="readonly" style="width: 75px" value="<%=FormatNumber(nopounds)%>" type="text"
                                    class="textbox100" name="txt_SCHEMECOST" id="txt_SCHEMECOST" maxlength="10" size="10" />
                                <img src="/js/FVS.gif" name="img_SCHEMECOST" id="img_SCHEMECOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                            <td>
                                <b>Balance.</b>
                            </td>
                            <td>
                                <input readonly="readonly" style="width: 75px" value="<%=FormatNumber(nopounds)%>" type="text"
                                    class="textbox100" name="txt_BALANCE" id="txt_BALANCE" maxlength="10" size="10" />
                                <img src="/js/FVS.gif" name="img_BALANCE" id="img_BALANCE" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr style="height: 10px">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                HAG
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_HAG" id="txt_HAG" maxlength="10" size="10" tabindex="23" />
                                <img src="/js/FVS.gif" name="img_HAG" id="img_HAG" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Borrowing Rate
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_BORROWINGRATE" id="txt_BORROWINGRATE" maxlength="10" size="10"
                                    tabindex="25" />
                                <img src="/js/FVS.gif" name="img_BORROWINGRATE" id="img_BORROWINGRATE" width="15px" height="15px" border="0" alt="" />
                            </td>
                            <td>
                                Mortgage
                            </td>
                            <td>
                                <input onblur="calc_cost()" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    type="text" class="textbox100" name="txt_MORTGAGE" id="txt_MORTGAGE" maxlength="10" size="10" tabindex="24" />
                                <img src="/js/FVS.gif" name="img_MORTGAGE" id="img_MORTGAGE" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <b>Nett Cost</b>
                            </td>
                            <td>
                                <input type="text" name="txt_NETTCOST" id="txt_NETTCOST" maxlength="10" size="10" readonly="readonly" style="width: 75px" value="<%=FormatNumber(nopounds)%>"
                                    class="textbox100" />
                                <img src="/js/FVS.gif" name="img_NETTCOST" id="img_NETTCOST" width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Patch Name:
                </td>
                <td align="right">
                    <%=lstPatch %>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_PATCHID" id="img_PATCHID" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Scheme Name:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_SCHEMENAME" id="txt_SCHEMENAME" maxlength="100" size="30"
                        tabindex="2" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SCHEMENAME" id="img_SCHEMENAME" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Scheme Code:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_SCHEMECODE" id="txt_SCHEMECODE" maxlength="50" size="30"
                        tabindex="3" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SCHEMECODE" id="img_SCHEMECODE" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Contract Start:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_STARTDATE" id="txt_STARTDATE" maxlength="10" size="30"
                        tabindex="4" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td title="Anticipated Completion">
                    Ant. Completion:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_TARGETDATE" id="txt_TARGETDATE" maxlength="10" size="30"
                        tabindex="5" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_TARGETDATE" id="img_TARGETDATE" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Actual Completion:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_ACTUALDATE" id="txt_ACTUALDATE" maxlength="10" size="30"
                        tabindex="6" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_ACTUALDATE" id="img_ACTUALDATE" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Defects Period End:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_DEFECTSPERIOD" id="txt_DEFECTSPERIOD" maxlength="100" size="30"
                        tabindex="7" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DEFECTSPERIOD" id="img_DEFECTSPERIOD" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
             <tr>
                <td>
                    Company:
                </td>
                <td align="right">
                    <%=lstCompany %>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_COMPANYID" id="img_COMPANYID" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
        <table width="750" cellpadding="1" cellspacing="2" border="0" class="tab_table_without_head">
            <tr>
                <td>
                    Development Type:
                </td>
                <td align="right">
                    <%=lstDevelopmentTypes%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DEVELOPMENTTYPE" id="img_DEVELOPMENTTYPE" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Lead Dev Officer:
                </td>
                <td align="right">
                    <%=lstLeadOfficers%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_LEADDEVOFFICER" id="img_LEADDEVOFFICER" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Number of Units:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_NUMBEROFUNITS" id="txt_NUMBEROFUNITS" maxlength="20" size="30"
                        tabindex="9" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_NUMBEROFUNITS" id="img_NUMBEROFUNITS" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Support Dev Officer:
                </td>
                <td align="right">
                    <%=lstSupportOfficers%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SUPPORTDEVOFFICER" id="img_SUPPORTDEVOFFICER" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Use Type:
                </td>
                <td align="right">
                    <%=lstUseTypes%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_USETYPE" id="img_USETYPE" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Project Manager:
                </td>
                <td align="right">
                    <input type="text" class="textbox200" name="txt_PROJECTMANAGER" id="txt_PROJECTMANAGER" maxlength="100" size="30"
                        tabindex="15" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_PROJECTMANAGER" id="img_PROJECTMANAGER" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Most Suitable for:
                </td>
                <td align="right">
                    <%=lstMostSuitable%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SUITABLEFOR" id="img_SUITABLEFOR" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Surveyor:
                </td>
                <td align="right">
                    <%=lstSurveyor%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SURVEYOR" id="img_SURVEYOR" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Local Authority:
                </td>
                <td align="right">
                    <%=lstLocalAuthorities%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_LOCALAUTHORITY" id="img_LOCALAUTHORITY" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Contractor:
                </td>
                <td align="right">
                    <%=lstContractors%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Supported Client Groups:
                </td>
                <td align="right">
                    <%=lstSupportedClients%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SUPPORTEDCLIENTS" id="img_SUPPORTEDCLIENTS" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Insurance Value: (�)
                </td>
                <td align="right">
                    <input type="text" onblur="checkForm(true)" class="textbox200" name="txt_INSURANCEVALUE" id="txt_INSURANCEVALUE"
                        maxlength="20" size="30" tabindex="17" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_INSURANCEVALUE" id="img_INSURANCEVALUE" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="5">
                    <input type="button" name="BTN_SAVE" id="BTN_SAVE" value="Save" title="Save" class="rslbutton"
                        onclick="click_save('NEW')" style="display: block; cursor:pointer" />
                    <input type="button" name="BTN_AMEND" id="BTN_AMEND" value="Amend" title="Amend" class="rslbutton"
                        onclick="{ click_save('AMEND'); }" style="display: none; cursor:pointer" />
                    <input type="hidden" name="txtList" id="txtList" />
                </td>
            </tr>
        </table>
    </div>
    <!-- This DIV is used for displaying development record only -->
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="behavior: url(/Includes/Tables/tablehl.htc);
            border-collapse: collapse" slcolor='' border="1" hlcolor="STEELBLUE">
            <thead>
                <tr style="height: 3px; border-bottom: 1px solid #133e71">
                    <td colspan="5" class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td width="200px" class='TABLE_HEAD'>
                        <b>Development Name</b>
                    </td>
                    <td width="290px" class='TABLE_HEAD'>
                        <b>Scheme</b>
                    </td>
                    <td width="110px" class='TABLE_HEAD'>
                        <b>Type</b>
                    </td>
                    <td width="90px" class='TABLE_HEAD'>
                        <b>Start Date</b>
                    </td>
                    <td width="90px" class='TABLE_HEAD'>
                        <b>Target Date</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD' colspan="7" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
    </div>
    <input type="hidden" name="hid_DEVELOPMENTID" id="hid_DEVELOPMENTID" />
    <input type="hidden" name="hid_ACTION" id="hid_ACTION" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>