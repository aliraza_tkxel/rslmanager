<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName,searchSQL
	Dim ASBESTOS,CloseRecord,COLUMNCOUNTER,DEVELOPMENTFILTER,PATCHFILETER
	Dim PropertyId,DEVELOPMENTSELECT
	
    ' This variable is defined to store dropdown state as query string that will be used in paging and go to page button action.
    Dim dropDownQueryString
    dropDownQueryString = ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	'STORES THE CURRENT OPEN PROPERTY VALUE
	
	CurrentPropertyId=Request("PropertyID")
	CloseRecord =Request("CLOSEASBESTOS")
	'Developmentid=Request("DEVELOPMENTID")
	DEVELOPMENTFILTER = "0"
	str_data=""
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	If Request.QueryString("DEVELOPMENTID") <> "" Then
	    if Request.QueryString("DEVELOPMENTID") = "ALL" then
	        DEVELOPMENTFILTER=""
	    else
	        l_DEVELOPMENT=Request.QueryString("DEVELOPMENTID")
	        DEVELOPMENTFILTER= "  AND SUB.DEVELOPMENTID=" & Request.QueryString("DEVELOPMENTID")
                        
	        dropDownQueryString = dropDownQueryString & "&DEVELOPMENTID=" & l_DEVELOPMENT
	    end if    
	Else
	    DEVELOPMENTFILTER = ""
	    l_DEVELOPMENT=""
	End If

    If(Request.QueryString("DEVELOPMENTNAME")<>"") Then
        dropDownQueryString = dropDownQueryString & "&DEVELOPMENTNAME=" & Request.QueryString("DEVELOPMENTNAME")
    End If

	
	If(Request.QueryString("PATCHID")<>"") Then
	    l_Patch=Request.QueryString("PATCHID")
	    PATCHFILETER= " AND SUB.PATCHID="  & Request.QueryString("PATCHID")
	    DEVELOPMENTSELECT=" WHERE PATCHID=" & Request.QueryString("PATCHID")

        dropDownQueryString = dropDownQueryString & "&PATCHID=" & l_Patch
	Else
	    l_Patch=""
	    PATCHFILETER=""
	    DEVELOPMENTSELECT=""
	End if

    If (Request.QueryString("PATCHNAME")<>"") Then
        dropDownQueryString = dropDownQueryString & "&PATCHNAME=" & Request.QueryString("PATCHNAME")
    End if

	PageName = "AsbestosReport.asp"
	MaxRowSpan = 4
	COLUMNCOUNTER = 1
	
    Call OpenDB()
	'Build Option Select(combo) for ASBESTOS ELEMNT
	Call BuildSelect(lstDEVELOPEMNT, "sel_Development", "PDR_DEVELOPMENT" & DEVELOPMENTSELECT , "DEVELOPMENTID,DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select</OPTION><OPTION VALUE=ALL>ALL", l_DEVELOPMENT,null, "textbox200", "STYLE='WIDTH:240PX' onChange=Return_Date()" )
	Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", l_Patch, NULL, "textbox200", " STYLE='WIDTH:240PX' TABINDEX=11 onChange=Return_Date()")	
	Dim CompareValue
	Call get_asbestos_property()	
	Call CloseDB()	
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<script type=""text/javascript"" language=""javascript"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & " + """ & dropDownQueryString & """" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</script>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function		
		
		' pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<tr>" & vbcrlf &_
                "<td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td>" & vbcrlf &_
                "</tr>" & vbcrlf
				cnt = cnt + 1
			wend		
		
		End Function
		
		Function get_asbestos_property()

		   Dim strSQL, rsSet, intRecord 
		   intRecord = 0		   
		   
		   strSQL=" SELECT SUB.PROPERTYID,SUB.PROPERTY_ADDRESS,SUB.REFURBISMENT_DATE,PARL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL "&_
                  " FROM (SELECT DISTINCT PP.PROPERTYID AS PROPERTYID, REPLACE(                                                "&_ 
                  "                       CASE ISNULL(PP.FLATNUMBER,'EMPTY')                                                   "&_
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_
	              "                          ELSE PP.FLATNUMBER+','                                                            "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                                                 "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.HOUSENUMBER+','                                                           "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS1,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS1+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS2,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS2+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS3,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.ADDRESS3+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.TOWNCITY,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.TOWNCITY+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.COUNTY,'EMPTY')                                                      "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.COUNTY+','                                                                "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.POSTCODE,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.POSTCODE                                                                  "&_ 
                  "                        END,',,',',')                                                                       "&_ 
                  "                     AS PROPERTY_ADDRESS,MAX(PRH.REFURBISHMENT_DATE) AS REFURBISMENT_DATE,MAX(PAL.RISKLEVELID) AS MAX_RISKLEVELID, "&_
                  "                     PP.DEVELOPMENTID AS DEVELOPMENTID,EP.PATCHID AS PATCHID                                "&_                      
		          "                     FROM P__PROPERTY PP                                                                    "&_ 
                  "                     LEFT OUTER JOIN P_REFURBISHMENT_HISTORY PRH ON PP.PROPERTYID=PRH.PROPERTYID            "&_    
                  "                     INNER JOIN PDR_DEVELOPMENT PD ON PP.DEVELOPMENTID=PD.DEVELOPMENTID                       "&_
                  "                     LEFT JOIN E_PATCH EP ON EP.PATCHID=PD.PATCHID                                          "&_
                  "                     INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL PPARL ON PP.PROPERTYID=PPARL.PROPERTYID       "&_ 
		          "                     INNER JOIN 	P_PROPERTY_ASBESTOS_RISK PPAR ON PPAR.PROPASBLEVELID=PPARL.PROPASBLEVELID  "&_ 
		          "                     INNER JOIN P_ASBRISKLEVEL PAL ON PAL.ASBRISKLEVELID=PPARL.ASBRISKLEVELID               "&_ 
		          "                     GROUP BY PP.PROPERTYID, REPLACE(                                                       "&_  
                  "                        CASE ISNULL(PP.FLATNUMBER,'EMPTY')                                                  "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.FLATNUMBER+','                                                            "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                                                 "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.HOUSENUMBER+','                                                           "&_ 
                  "                         END                                                                                "&_  
                  "                       + CASE ISNULL(PP.ADDRESS1,'EMPTY')                                                   "&_  
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS1+','                                                              "&_ 
                  "                         END                                                                                "&_  
                  "                      + CASE ISNULL(PP.ADDRESS2,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.ADDRESS2+','                                                              "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.ADDRESS3,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.ADDRESS3+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.TOWNCITY,'EMPTY')                                                    "&_ 
	              "                           WHEN 'EMPTY' THEN ''                                                             "&_ 
	              "                           ELSE PP.TOWNCITY+','                                                             "&_ 
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.COUNTY,'EMPTY')                                                      "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.COUNTY+','                                                                "&_  
                  "                        END                                                                                 "&_ 
                  "                      + CASE ISNULL(PP.POSTCODE,'EMPTY')                                                    "&_ 
	              "                          WHEN 'EMPTY' THEN ''                                                              "&_ 
	              "                          ELSE PP.POSTCODE                                                                  "&_ 
                  "                        END,',,',','),PP.DEVELOPMENTID,EP.PATCHID                                           "&_ 
                  "                     ) SUB,P_ASBRISKLEVEL AS PARL                                                           "&_ 
                  "  WHERE SUB.MAX_RISKLEVELID=PARL.RISKLEVELID	  " & DEVELOPMENTFILTER &  PATCHFILETER &             "        "&_ 
                  " ORDER BY SUB.PROPERTYID"            
                  
            set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
            
            ' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
            If intRecordCount > 0 Then
			    ' Display the record that you are starting on and the record
			    ' that you are finishing on for this page by writing out the
			    ' values in the intStart and the intFinish variables.
			    str_data = str_data & "<tbody class=""CAPS"">"
				    ' Iterate through the recordset until we reach the end of the page
				    ' or the last record in the recordset.
				    PrevTenancy = ""
				    For intRecord = 1 to rsSet.PageSize
				        
				        if (intRecord <> 1 ) then
				            PropertyId = PropertyId & "-" & rsSet("PROPERTYID")
				        else
				            PropertyId= PropertyId & rsSet("PROPERTYID")
				        end if				        
				        
				        '//HERE WE HAVE CHECK IF THE PROPERTY HAS BEEN SELECTED IN WHICH CASE WE SHOW THE RESPECTIVE ASBESTOS FOR THE PROPERTY
				        '//ON THE STRING THAT APPEARS NEXT YOU WILL SEE THAT AN <A> TAG HAS BEEN ENTERED THIS IS SO THAT THE FRAME
				        '//CAN BE SCROLLED TO THIS PROPERTY ID ON LOAD....
    				    'if ( (CStr(CurrentPropertyId) = CStr(PropertyId)) ) then    
				               Call Build_Abestos_Detail(rsSet("PROPERTYID")) 
				        
				                STRrow  =  STRrow & "<tr style=""background-color:#F5F5F5;border-bottom-width:thin"" id=""Line"& rsSet("PropertyId") &""" style=""cursor:pointer"" onclick=""LoadAsbestosDetail('" & rsSet("PropertyId") & "'," & COLUMNCOUNTER & ")"">" &_
	                                      "  <td align=""left"" style=""border-color:#F5F5F6"" class=""NO-BORDER"">" & rsSet("PROPERTYID") & "</td>" &_
						                  "  <td align=""left"" style=""border-color:#F5F5F6"" class=""NO-BORDER"">" & rsSet("PROPERTY_ADDRESS") & "</td>" &_
						                  "  <td align=""left"" style=""border-color:#F5F5F6"" class=""NO-BORDER"">" & rsSet("REFURBISMENT_DATE") & "</td>" &_
						                  "  <td align=""left"" style=""border-color:#F5F5F6"" class=""NO-BORDER"">" & rsSet("RISKLEVEL") & "</td>" &_
						            "</tr>"
				                
                   		        '//CHECK WHEHTER THE PROPERTY HAS BEEN CLICKED ON AGAIN IN WHICH CASE WE CLOSE THE ITEM
                   		        'if (CloseRecord <> "1") then
                   		            
                   		            
                   		        'else
                   		        '    //RESET THE CURRENTPROPERTYID VARIABLE AS IT HAS BEEN CLOSED
                   		        '    CurrentPropertyId = ""    
                   		        '    ASBESTOS=""
                   		        'end if
                   		        STRrow = STRrow & "<tr><td colspan=""4""><table>" & ASBESTOS & "</table></td></tr>"
                                'STRrow = STRrow & ASBESTOS
                   		        str_data =  STRrow
                   		'else
                   		'    //THIS PART BUILDS THE STANDARD PROPERTY ID LINE WHICH IS CLOSED...
                   		'    STRrow  =  STRrow & "<tr style='cursor:hand' onClick=""LoadAsbestosDetail('" & rsSet("PropertyId") & "')"">" &_
	                    '                  "<TD style='background-color:white'><img src='/js/tree/img/plus.gif' width=18 height=18 border=0></TD>" &_
						'                  "  <td align='left' bgcolor='#F5F5F5'>" & rsSet("PROPERTYID") & "</td>" &_
						'                  "  <td align='left'>" & rsSet("PROPERTY_ADDRESS") & "</td>" &_
						'                  "  <td align='left'>" & rsSet("REFURBISMENT_DATE") & "</td>" &_
						'                  "  <td align='left'>" & rsSet("RISKLEVEL") & "</td>" &_
						'            "</tr>"
				        '        str_data = STRrow
                   		'end if
               		    count = count + 1
					    rsSet.movenext()

					    If rsSet.EOF Then Exit for

				    Next

                    str_data = str_data & "</tbody>"

				    'ensure table height is consistent with any amount of records
				    Call fill_gaps()

				    ' links
				    str_data = str_data &_
				    "<tfoot>" & vbcrlf &_
                    "<tr>" & vbcrlf &_
                    "<td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" & vbcrlf &_
				    "<table cellspacing=""0"" cellpadding=""0"" width=""100%"">" & vbcrlf &_
                    "<thead>" & vbcrlf &_
                    "<tr>" & vbcrlf &_
                    "<td width=""100""></td>" & vbcrlf &_
                    "<td align=""center"">" & vbcrlf &_
				    "<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1" & dropDownQueryString & "'><b><font color=""blue"">First</font></b></a> "  &_
				    "<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & dropDownQueryString & "'><b><font color=""blue"">Prev</font></b></a>"  &_
				    " Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				    " <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage &  dropDownQueryString & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
				    " <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & dropDownQueryString & "'><b><font color=""blue"">Last</font></b></a>"  &_
				    "</td>" & vbcrlf &_
                    "<td align=""right"" width=""100"">" & vbcrlf &_
                    "Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value='' size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"" />&nbsp;"  &_
				    "<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"" />" & vbcrlf &_
				    "</td>" & vbcrlf &_
                    "</tr>" & vbcrlf &_
                    "</thead>" & vbcrlf &_
                    "</table>" & vbcrlf &_
                    "</td>" & vbcrlf &_
                    "</tr>" & vbcrlf &_
                    "</tfoot>"
			    End If

                ' if no teams exist inform the user
			    If intRecord = 0 Then 
				    str_data = "<tr>" & vbcrlf &_
				    "<td colspan=""" & MaxRowSpan & """ style=""font:12px"" align=""center"">No Asbestos Risk found</td>" & vbcrlf &_
				    "</tr>" & vbcrlf
				    Call fill_gaps()
			    End If

			  rsSet.close()
			  Set rsSet = Nothing

		End Function
		
		Function Build_Abestos_Detail(PropertyId)
		    Dim DevID
            'Dim PropertyID
            'PropertyID= Request.QueryString("PropertyId") 

            'Collecting ASBESTOS data
            SQL="SELECT PAR.PROPASBRISKID AS PROPASBESTOSRISK ,PR.PROPASBLEVELID AS PROPASBESTOSLEVELID, " &_
	            "PL.ASBRISKLEVELID AS RISKLEVELID, PL.ASBRISKLEVELDESCRIPTION AS RISKLEVEL,PRI.ASBRISKID AS RISK, " &_
	            "P.ASBESTOSID AS ELEMENTID, P.RISKDESCRIPTION AS ELEMENTDISCRIPTION, IsNull(convert(varchar(max),PR.DATEADDED,103),'No Data') AS DATEADDED, IsNull(convert(varchar(max),PR.DATEREMOVED,103),'No Data') AS DATEREMOVED FROM P_ASBRISK PRI,"&_
	            "P_PROPERTY_ASBESTOS_RISKLEVEL PR left join  P_PROPERTY_ASBESTOS_RISK PAR ON " &_ 
	            "PR.PROPASBLEVELID=PAR.PROPASBLEVELID INNER Join P_ASBRISKLEVEL PL " &_
	            "ON PR.ASBRISKLEVELID=PL.ASBRISKLEVELID INNER Join P_ASBESTOS P ON " &_
	            "PR.ASBESTOSID =P.ASBESTOSID WHERE PAR.ASBRISKID=PRI.ASBRISKID AND PR.PROPERTYID = '" & PropertyID & "' order by PL.ASBRISKLEVELID desc "
	 
            Call OpenRs(rsASB, SQL)		
            ASBESTOS=""
            Dim flag,COLUMNCOUNTER,ROWCOUNTER
            redim ASBESTOS_ARRAY(15,15)
            flag=0
            COLUMNCOUNTER=0
            ROWCOUNTER=1

            'For i = 0 to UBound(ASBESTOS_ARRAY)    
            'For j = 0 to UBound(ASBESTOS_ARRAY, 2)        
            'Response.Write(i & "," & j) & "<br/>" 
            'Next
            'Next

                while NOT rsASB.EOF
                    for i=0 to COLUMNCOUNTER
                        if(ASBESTOS_ARRAY(i,0) = rsASB("RISKLEVEL")) then                      
                            ASBESTOS_ARRAY(i,ROWCOUNTER) = "<td>" & rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b></td><td>" & rsASB("DATEADDED") & "</td><td>" & rsASB("DATEREMOVED")
                            ROWCOUNTER=ROWCOUNTER+1
                            flag=1
                            exit for
                        end if   
                    next
         
                    if(flag=0) then
                        ASBESTOS_ARRAY(COLUMNCOUNTER,0) = rsASB("RISKLEVEL")
                        ASBESTOS_ARRAY(COLUMNCOUNTER,ROWCOUNTER) = "<td>" & rsASB("ELEMENTDISCRIPTION") & " - <b>" & rsASB("RISK") & "</b></td><td>" & rsASB("DATEADDED") & "</td><td>" & rsASB("DATEREMOVED")
                        COLUMNCOUNTER=COLUMNCOUNTER+1
                        ROWCOUNTER=ROWCOUNTER+1
                     end if
                    flag=0
                    rsASB.movenext
                wend
                
                Call CloseRs(rsASB)
                
                if(COLUMNCOUNTER>=0) then
                    'ASBESTOS="<table cellspacing=0 cellpadding=2 width=644><tr><td class='CAPS'></td></tr>" 
                    
                    for i=0 to COLUMNCOUNTER
                        if(i<1 and ASBESTOS_ARRAY(i+1,0)="" ) then
                            ASBESTOS=ASBESTOS & "<tr style=""display:none;"" id=""ChildLine"& PropertyId & i & """ ><td class='NO-BORDER'></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=""2"">"
                        elseif(i=1) then
                            ASBESTOS=ASBESTOS & "<tr style=""display:none;"" id=""ChildLine"& PropertyId & i & """><td class='NO-BORDER'></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=""2"">"
                         else
                             ASBESTOS=ASBESTOS & "<tr style=""display:none;"" id=""ChildLine"& PropertyId & i & """><td></td><td>" & ASBESTOS_ARRAY(i,0) & "</td><td colspan=""2"">"     
                        end if
if i < COLUMNCOUNTER then
ASBESTOS=ASBESTOS & "<table cellpadding='0' cellspacing='2' style='border-collapse: collapse'>"
ASBESTOS=ASBESTOS & "<tr><td width=""300"">&nbsp;</td><td width=""100""><b>Added</b></td><td width=""100""><b>Removed</b></td></tr>"
                        for j=1 to ROWCOUNTER
                            If(ASBESTOS_ARRAY(i,j)<>"") then
ASBESTOS=ASBESTOS & "<tr>"
                                ASBESTOS=ASBESTOS & ASBESTOS_ARRAY(i,j) & "<br />"
ASBESTOS=ASBESTOS & "</tr>"
                            end if
                        next
ASBESTOS=ASBESTOS & "</table>"

end if

                        ASBESTOS=ASBESTOS & "</td></tr>"
                        
                   next 
                   'ASBESTOS=ASBESTOS & "</table>"
                  
                end if
                
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Abestos Report</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
    body
    {
        background-color: White;
        margin: 10px 0px 0px 10px;
    }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript" defer="defer">

 function LoadAsbestosDetail(PropertyId,ColumnCounter){
	
		//window.open ("Popups/pAsbestosReport.asp?PropertyId=" + PropertyId , "","width=700,height=390, left=150,top=100,scrollbars=yes")
		
		//CHECK IF THE ASBESTOS FOR A PROPERTY IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		//if ("<%=CurrentPropertyId%>" == PropertyId)
		//	PropertyId = "&CLOSEASBESTOS=1"
		//location.href = "AsbestosReport.asp?PropertyID=" + PropertyId 
		
		
		doc = document.all;
		//alert(ColumnCounter)
		//alert(doc["ChildLine" + PropertyId + 0].style.display)
        if (doc["ChildLine" + PropertyId + 0].style.display == "none")
	        newStatus = "block";
        else
	        newStatus = "none";
	        
        for (i=0; i<=ColumnCounter+2; i++)
        {
            if(ElementExist("ChildLine" + PropertyId + i)==true && document.getElementById("ChildLine" + PropertyId + i).value!="")
	            doc["ChildLine" + PropertyId + i].style.display = newStatus
	        else
	         break;
		}
	}
	
	function ElementExist(theVal)
	{
	    if (document.getElementById(theVal) != null) {
	        return true;
	    } 
	    else {
	     return false;
	  }

	}
    
    function ExpandAll()
    {
      doc = document.all;
      var PropertyId = "<%=PropertyId%>";
      var PropertyArray= PropertyId.split("-");
   
      for(i=0; i<=PropertyArray.length; i++)
        for(j=0;j<=5;j++)
        {
           if(ElementExist("ChildLine" + PropertyArray[i] + j)==true)
            {
               if (doc["ChildLine" + PropertyArray[i] + j].style.display == "none")
                 {   
                    doc["ChildLine" + PropertyArray[i] + j].style.display = "block";
                    document.getElementById("btn_expand").value="Close All" 
                  }  
                else
                  {  
                    doc["ChildLine" + PropertyArray[i] + j].style.display = "none";
                    document.getElementById("btn_expand").value="Expand All" 
                  }  
                    
            }   	     
	      }     
   }
   
   	function Return_Date()
	{
	    //var developmentid
	    //alert(thisForm.sel_Development.options[thisForm.sel_Development.selectedIndex].text)
	    if(thisForm.sel_Development.value=="")
	          if(thisForm.sel_PATCH.value=="") 
	            {
	         location.href = "<%=PageName & "?CC_Sort=" & orderBy %>"
	            }
	          else
	            {
	                location.href = "<%=PageName & "?CC_Sort=" & orderBy %>&PATCHID="+thisForm.sel_PATCH.value+"&PATCHNAME="+thisForm.sel_PATCH.options[thisForm.sel_PATCH.selectedIndex].text;
	            }
	     else
	         location.href = "<%=PageName & "?CC_Sort=" & orderBy %>&DEVELOPMENTID=" + thisForm.sel_Development.value+"&DEVELOPMENTNAME="+thisForm.sel_Development.options[thisForm.sel_Development.selectedIndex].text+"&PATCHID="+thisForm.sel_PATCH.value+"&PATCHNAME="+thisForm.sel_PATCH.options[thisForm.sel_PATCH.selectedIndex].text;    
	}
   
   	 /////////////////////////////////
	//	Opening data in XLS Below   //
	/////////////////////////////////
	function ExportMe()
	{
		var developmentid="<%=Request.QueryString("DEVELOPMENTID")%>"
		var developmentname="<%=Request.QueryString("DEVELOPMENTNAME")%>"
		var patchid="<%=Request.QueryString("PATCHID")%>"
		var patchname="<%=Request.QueryString("PATCHNAME")%>"
				
		if (developmentid == "" && patchid == "")
		    window.open("AsbestosReport_xls.asp")
		else
		   { 
		    if (patchid=="")
		        window.open("AsbestosReport_xls.asp?DEVELOPMENTID="+developmentid+"&DEVELOPMENTNAME="+developmentname)
		    else
		        window.open("AsbestosReport_xls.asp?DEVELOPMENTID="+developmentid+"&DEVELOPMENTNAME="+developmentname+"&PATCHID="+patchid+"&PATCHNAME="+patchname)   
		   } 		
	}	

    </script>
    <%=WriteJavaJumpFunction%>
</head>
<body onload="initSwipeMenu(2);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="3" valign="bottom">
                <img name="tab_Tenants" id="tab_Tenants" title="Tenants" src="../Portfolio/images/tab_asbestos_report.gif"
                    width="120" height="20" border="0" alt="" /></td>
            <td></td>
        </tr>
        <tr>
            <td height="1">&nbsp;</td>
        </tr>
        <tr>
            <td height="1" valign="bottom">
                <table width="93%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="right" width="10%">
                            &nbsp;
                        </td>
                        <td>
                            <b>Patch:</b>
                        </td>
                        <td>
                            <%=lstPatch%>
                        </td>
                        <td align="right">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" width="10%">
                            &nbsp;
                        </td>
                        <td>
                            <b>Scheme:</b>
                        </td>
                        <td>
                            <%=lstDEVELOPEMNT%>
                        </td>
                        <td align="right">
                            <table border="0">
                                <tr>
                                    <td nowrap="nowrap" align="center">
                                        <span style="font-weight: bold; vertical-align: text-top">Excel Version</span>&nbsp;<img
                                            src="../myImages/PrinterIcon.gif" width="31" height="20" onclick="ExportMe()"
                                            style="cursor: pointer" border="0" alt="Print Asbestos List" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" valign="bottom">
                            <input type="button" name="btn_expand" id="btn_expand" value="Expand All" class="RSLButton" style="margin-bottom: 3px"
                                onclick="ExpandAll()" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" height="1" colspan="5">
                            <img src="images/spacer.gif" width="630" height="1" border="0" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="0" cellspacing="2" class="TAB_TABLE" style="border-collapse: collapse"
        slcolor='' hlcolor="#4682b4" border="1">
        <thead>
            <tr>
                <td class='NO-BORDER' width="70">
                    <b>Property Id</b>
                </td>
                <td class='NO-BORDER' width="250">
                    &nbsp;<b>Property Address </b>
                </td>
                <td class='NO-BORDER' width="124">
                    &nbsp;<b>Refurbishment Date</b>&nbsp;
                </td>
                <td class='NO-BORDER' width="150">
                    &nbsp;<b>Max Risk Level</b>&nbsp;
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="4" align="center" style="border-bottom: 2px solid #133e71"></td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
