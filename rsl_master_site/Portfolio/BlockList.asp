	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE file="TableBuilderBlock.asp" -->
<%
	OpenDB()
	SQL = "SELECT SCHEMENAME FROM P_DEVELOPMENT WHERE DEVELOPMENTID = " & REQUEST("DEVID")
	Call OpenRs(rsDEV,SQL)
	THE_DEV_NAME = rsDEV("SCHEMENAME")
	Call CloseRs(rsDEV)
	CloseDB()

	DEVID = Request("DEVID")
	
	CONST CONST_PAGESIZE = 8

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim searchSQL,searchName
	
	
	searchName = Replace(Request("blockname"),"'","''")
	searchSQL = ""
	if searchName <> "" Then
		searchSQL = " AND (BLOCKNAME LIKE '%" & searchName & "%' ) "
	End If
	
	ColData(0)  = "Block Name|BLOCKNAME|170"
	SortASC(0) 	= "BLOCKNAME ASC"
	SortDESC(0) = "BLOCKNAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Address 1|ADDRESS1|200"
	SortASC(1) 	= "ADDRESS1 ASC"
	SortDESC(1) = "ADDRESS1 DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Postcode|POSTCODE|85"
	SortASC(2) 	= "POSTCODE ASC"
	SortDESC(2) = "POSTCODE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Props|TOTALPROPERTIES|80"
	SortASC(3) 	= "TOTALPROPERTIES ASC"
	SortDESC(3) = "TOTALPROPERTIES DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Available|AVAILABLE|95"
	SortASC(4) 	= "AVAILABLE ASC"
	SortDESC(4) = "AVAILABLE DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "Un-avail...|UNAVAILABLE|95"
	SortASC(5) 	= "UNAVAILABLE ASC"
	SortDESC(5) = "UNAVAILABLE DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""
	
	PageName = "BlockList.asp"
	EmptyText = ""
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""BLOCKID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE = 	"SELECT B.BLOCKID, BLOCKNAME, ADDRESS1, ADDRESS2, POSTCODE , COMPLETIONDATE, " &_
				"ISNULL(TP.TOTALPROPERTIES,0) AS TOTALPROPERTIES, " &_
				"ISNULL(AV.AVAILABLE,0) AS AVAILABLE,  " &_
				"ISNULL(TP.TOTALPROPERTIES,0) - ISNULL(AV.AVAILABLE,0) AS UNAVAILABLE " &_
				"FROM P_BLOCK B " &_
				"	LEFT JOIN  " &_
				"		(SELECT COUNT(*) AS AVAILABLE, BLOCKID  " &_
				"			FROM P__PROPERTY WHERE STATUS IN (1,5) GROUP BY BLOCKID) AV  " &_
				"	ON B.BLOCKID = AV.BLOCKID " &_
				"	LEFT JOIN  " &_
				"		(SELECT COUNT(*) AS TOTALPROPERTIES, BLOCKID  " &_
				"			FROM P__PROPERTY GROUP BY BLOCKID) TP " &_
				"	ON B.BLOCKID = TP.BLOCKID " &_				
				"WHERE DEVELOPMENTID = " & DEVID & " " &  searchSQL & " " &_
				" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --> Estate Maintenance --> Development</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Loading.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(block_id){
	location.href = "PropertyListEM.asp?DEVID=<%=Request("DevID") & "&DevSort=" & Request("DevSort") & "&DevPage=" & Request("DevPage") & "&name=" & Request("name")%>&BlockSort=<%=Server.UrlEncode(SortASC(i))%>&blockname=<%=searchName%>&BlockPage=<%=Request("page")%>&BLOCKID=" + block_id;
	}

function quick_find(){
	location.href = "<%=PageName & "?DEVID=" & Request("DevID") & "&DevSort=" & Request("DevSort") & "&DevPage=" & Request("DevPage") & "&name=" & Request("name")%>&CC_Sort=<%=Server.UrlEncode(SortASC(i))%>&blockname="+thisForm.findemp.value;
	}

	var JOURNALID
	var iFrameArray = new Array ()								
	iFrameArray[70] = "iRepairJournalDev"
	iFrameArray[71] = "iAccountDev"
	iFrameArray[72] = "iPurchasesDev"	
	iFrameArray[73] = "iRepairDetailsDev"	
								
	var MAIN_OPEN_BOTTOM_FRAME = 70
	var MASTER_OPEN_WORKORDER = ""
	var MASTER_OPEN_PURCHASEORDER = ""	
		
	var LOADER_FRAME_APPEND
	// swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
	// where is either 'top' or 'bottom' relating to the tab area of the page
	// FULL RELOAD FUNCTIONALITY NEEDS TO BE IMPLEMETED TO RELOAD ALL DYNAMIC DIVS -- PDP
	function swap_div(int_which_one, str_where){
		
		var divid, imgid, upper
		
		upper = 73;	lower = 70;
		//STARTLOADER('BOTTOM')
		MAIN_OPEN_BOTTOM_FRAME = int_which_one			
		DEV_BOTTOM_FRAME.location.href = "/portfolio/iframes/" + iFrameArray[int_which_one] + ".asp?DEVID=<%=DEVID%>&CWO=" + MASTER_OPEN_WORKORDER + "&CPO=" + MASTER_OPEN_PURCHASEORDER;

		imgid = "img" + int_which_one.toString();
		
		// manipulate images
		for (j = lower ; j <= upper ; j++)
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
		
		// unless last image in row
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
	}

	function synchronize_tabs(int_which_one, str_where){
		var divid, imgid, upper
		upper = 73;	lower = 70;
		
		imgid = "img" + int_which_one.toString();
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
			}
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
		}
	
	function work_order(){
		window.showModelessDialog("popups/WorkOrderDev.asp?DEVID=<%=DEVID%>&Random=" + new Date(),"","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			

	function purchase_order(){
		window.showModelessDialog("popups/PurchaseOrderDev.asp?DEVID=<%=DEVID%>&Random=" + new Date(),"","dialogHeight: 555px; dialogWidth: 800px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			

	function cyc_repair(){
		window.showModelessDialog("popups/CycRepairDev.asp?DEVID=<%=DEVID%>&Random=" + new Date(),"","dialogHeight: 555px; dialogWidth: 800px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		}			
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
	  <TD ROWSPAN=2><a href="javascript:location.href='EstateMaintenance.asp?CC_Sort=<%=Request("DevSort")%>&page=<%=Request("DevPage")%>&name=<%=Request("name")%>'"><img name="tab_employees" title='Scheme List' src="images/scheme_list-previous.gif" width=95 height=20 border=0></a></TD>
	  <TD ROWSPAN=2><IMG NAME="tab_employees" TITLE='Scheme:<%=THE_DEV_NAME%>' SRC="images/scheme-open.gif" WIDTH=74 HEIGHT=20 BORDER=0></TD>
		<TD><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=19></TD>
		<td nowrap><INPUT TYPE='BUTTON' name='btn_FIND' title='Search for matches using Block Name.' CLASS='RSLBUTTON' VALUE='Quick Find' ONCLICK='quick_find()'>
					&nbsp;<input type=text  size=18 name='findemp' class='textbox200' value="<%=searchName%>">
		</td>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71 colspan=2><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=1></TD>
	</TR>
	<tr><td style='border-left:1px solid #133e71;border-right:1px solid #133e71;text-transform:uppercase' COLSPAN=5 height=20>&nbsp;<b><i>Scheme :</i> <font color=blue><%=THE_DEV_NAME%></font></b></td></tr>	
</TABLE>
<%=TheFullTable%>
<br>
	<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img70" SRC="images/70-open.gif" WIDTH=54 HEIGHT=20 BORDER=0  onclick="swap_div(70, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom><IMG NAME="img71" SRC="images/71-previous.gif" WIDTH=117 HEIGHT=20 BORDER=0 onclick="swap_div(71, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom><IMG NAME="img72" SRC="images/72-closed.gif" WIDTH=145 HEIGHT=20 BORDER=0 onclick="swap_div(72, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom><IMG NAME="img73" SRC="images/73-closed.gif" WIDTH=77 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
        <TD WIDTH=100 ALIGN=CENTER>
				<img src="/myImages/Repairs/sbasket.gif" title="Create new work order" width="27" height="22" style='cursor:hand' ONCLICK='work_order()'>
				<img src="/myImages/Repairs/porder.gif" title="Create new purchase order" width="27" height="22" style='cursor:hand' ONCLICK='purchase_order()'>				
				<img src="/myImages/Repairs/sCyc.gif" title="Create cyclical repair" width="27" height="22" style='cursor:hand' ONCLICK='cyc_repair()'></TD>				
		<TD align=right height=19>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004174 colspan=2><IMG SRC="images/spacer.gif" WIDTH=355 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=BOTTOM_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=0 border=0 CLASS="TAB_TABLE" >
	  <TR> 
		<TD><iframe name=DEV_BOTTOM_FRAME src="iFrames/iRepairJournalDev.asp?DEVID=<%=DEVID%>" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=BOTTOM_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:210' WIDTH=750 HEIGHT=210>
<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=10 CELLSPACING=10 border=0 CLASS="TAB_TABLE">
    <TR>
		<TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_BOTTOM"></DIV></b></TD>
	</TR>		
</TABLE>
</DIV>
<input name="null" type="hidden">
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>