<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Const UNAVAILABLE = 4
	Const UNDERCONSTRUCTION = 1

	Dim lstDevelopments, lstStatus, lstSubStatus, lstAssetType, lstPropertyType, lstLevel , lstOwnership
	Dim lstBlocks ,l_address1, lstHousingOfficers, disableAssetType, lettingDateReadOnly, l_ownership
	Dim l_purchaselevel, l_minpurchase , lstDwellingType , l_dwellingtype, lstNroshAssetTypeMain, lstNroshAssetTypeSub,lstFuelType,l_fueltype

	Call OpenDB()

	Dim rsLoader, ACTION
		read_only = ""
		ACTION = "NEW"
		l_propertystatus = "New Property"

	PropertyID = Request("PropertyID")
	If (PropertyID = "") Then PropertyID = -1 End If

	'Defaulyt values required to build select boxes
	l_blockid = -1
	l_developmentid = -1
	l_status = -1
	l_propertyid = "Auto Generated"

	SQL = "SELECT P.*, D.SCHEMENAME, M.MANAGINGAGENT FROM P__PROPERTY P LEFT JOIN P_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID LEFT JOIN P_MANAGINGAGENT M ON M.PROPERTYID = P.PROPERTYID WHERE P.PROPERTYID = '" & PropertyID & "'"
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		' tbl E_JOBDETAILS
		l_propertyid = UCASE(rsLoader("PROPERTYID"))
		l_propertystatus = l_propertyid
		l_schemename = rsLoader("SCHEMENAME")
		l_developmentid = rsLoader("DEVELOPMENTID")
		If (l_developmentid = "" OR isNull(l_developmentid)) Then
			l_developmentid = -1
		End If
		l_blockid = rsLoader("BLOCKID")
		If (l_blockid = "" OR isNull(l_blockid)) Then
			l_blockid = -1
		End If
		l_housenumber = rsLoader("HOUSENUMBER")
		l_address1 = rsLoader("ADDRESS1")
		l_address2 = rsLoader("ADDRESS2")
		l_towncity = rsLoader("TOWNCITY")
		l_county = rsLoader("COUNTY")
		l_postcode = rsLoader("POSTCODE")
		l_housingofficer = rsLoader("HOUSINGOFFICER")
		l_status = rsLoader("STATUS")
		If isNull(rsLoader("SUBSTATUS")) Then
			l_substatus = -1
		Else
			l_substatus = rsLoader("SUBSTATUS")
		End If
		l_avdate = rsLoader("AVDATE")
		l_propertytype = rsLoader("PROPERTYTYPE")
		l_assettype = rsLoader("ASSETTYPE")
		l_NroshAssetTypeMain = rsLoader("NROSHASSETTYPEMAIN")
		l_NroshAssetTypeSub = rsLoader("NROSHASSETTYPESUB")
		l_propertylevel = rsLoader("PROPERTYLEVEL")
		l_StockType = rsLoader("STOCKTYPE")
		l_ManagingAgent = rsLoader("MANAGINGAGENT")
		l_datebuilt = rsLoader("DATEBUILT")
		l_viewingdate = rsLoader("VIEWINGDATE")
		l_lettingdate = rsLoader("LETTINGDATE")
		l_righttobuy = rsLoader("RIGHTTOBUY")
		l_unitdesc = rsLoader("UNITDESC")
		l_defectsperiodend = rsLoader("DEFECTSPERIODEND")
		l_ownership = rsLoader("OWNERSHIP")
		l_minpurchase = rsLoader("MINPURCHASE")
		l_purchaselevel = rsLoader("PURCHASELEVEL")
		l_dwellingtype = rsLoader("DWELLINGTYPE")
		l_fueltype = rsLoader("FUELTYPE")
		disableAssetType = "  "
		ACTION = "AMEND"
		If Not isNull(l_lettingdate) Then
			If l_lettingdate <> "" And CDate(l_lettingdate) <= date Then lettingDateReadOnly = " READONLY " End If
		End If
	Else		' FOR A NEW PROPERTY STATUS MUST BE SET TO UNAVAILABLE
		l_status = UNAVAILABLE
		l_substatus = UNDERCONSTRUCTION
	End If
	Call CloseRs(rsLoader)

	SQL = "SELECT * FROM P_SUBSTATUS S WHERE S.STATUSID = " & l_status & " order by description "
	Call OpenRs(rsSubStatus, SQL)
	count = 0
	optionstr = "<select name='sel_SUBSTATUS' id='sel_SUBSTATUS' class='textbox200' disabled='disabled'><option value=''>Please Select</option>"
	While (NOT rsSubStatus.EOF)
		isSelected = ""
		If (CStr(rsSubStatus("SUBSTATUSID")) = CStr(l_substatus)) Then
			isSelected = " selected"
		End If
		optionstr = optionstr & "<option value=""" & rsSubStatus("SUBSTATUSID") & """ " & isSelected & ">" & rsSubStatus("DESCRIPTION") & "</option>"
		rsSubStatus.moveNext
		count = 1
	wend
	If (count = 0) Then
		lstSubStatus = "<select name='sel_SUBSTATUS' id='sel_SUBSTATUS' class='textbox200' disabled='disabled'><option value=''>Not Applicable</option></select>"
		SubStatusValidate = "N"
	Else
		lstSubStatus = optionstr & "</select>"
		SubStatusValidate = "N"
	End If

	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "P_development", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_developmentid, NULL, "textbox200", " onchange=""GetBlocks()""  tabindex='1' ")
	Call BuildSelect(lstBlocks, "sel_BLOCKID", "P_BLOCK WHERE DEVELOPMENTID = " & l_developmentid, "BLOCKID, BLOCKNAME", "BLOCKNAME", "Please Select", l_blockid, NULL, "textbox200", " onchange=""GetAddressData()""  tabindex='1' ")
	'Call BuildSelect(lstStatus, "sel_STATUS", "P_STATUS" & ExtraFilter, "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_status, NULL, "textbox200", " onchange=""GetSubStatus()""  TABINDEX=2 READONLY " & DisabledStatus & " ")
	Call BuildSelect(lstStatus, "sel_STATUS", " P_STATUS ", "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_status, NULL, "textbox200", " TABINDEX=2 disabled ")
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_assettype, NULL, "textbox200"," onchange=""EnableDisablePurchaseLevel()"" TABINDEX=2 " & disableAssetType & " ")
	Call BuildSelect(lstNroshAssetTypeMain, "sel_NROSHASSETTYPEMAIN", "P_ASSETTYPE_NROSH_MAIN", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", l_NroshAssetTypeMain, NULL, "textbox200"," onchange=""GetNroshAssetSub()"" tabindex='2' ")
	Call BuildSelect(lstNroshAssetTypeSub, "sel_NROSHASSETTYPESUB", "P_ASSETTYPE_NROSH_SUB", "SID, DESCRIPTION", "DESCRIPTION", "Please Select", l_NroshAssetTypeSub, NULL, "textbox200"," tabindex='2' ")
	Call BuildSelect(lstPropertyType, "sel_PROPERTYTYPE", "P_PROPERTYTYPE WHERE PROPERTYTYPEID <> 17", "PROPERTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_propertytype, NULL, "textbox200"," onchange=""EnableDisableDwellType()"" tabindex='2' ") 
	Call BuildSelect(lstLevel, "sel_PROPERTYLEVEL", "P_LEVEL", "LEVELID, DESCRIPTION", "DESCRIPTION", "Please Select", l_propertylevel, NULL, "textbox200", " TABINDEX=2")
	Call BuildSelect(lstHousingOfficers, "sel_HOUSINGOFFICER", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID INNER JOIN G_TEAMCODES G ON G.TEAMID = J.TEAM WHERE G.TEAMCODE IN ('HOU','FIN','EST') AND J.ACTIVE = 1 ", "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", l_housingofficer, NULL, "textbox200", " tabindex='2' ")
	Call BuildSelect(lstStockType, "sel_STOCKTYPE", "P_STOCKTYPE", "STOCKTYPE, DESCRIPTION", "DESCRIPTION", "Please Select", l_StockType, NULL, "textbox200", " onchange='changeStockType();'")
	Call BuildSelect(lstOwnership, "sel_OWNERSHIP", "P_OWNERSHIP", "OWNERSHIPID, DESCRIPTION", "OWNERSHIPID", "Please Select", l_ownership, NULL, "textbox200", " tabindex='2' ")
	Call BuildSelect(lstDwellingType, "sel_DWELLINGTYPE", "P_DWELLINGTYPE", "DTYPEID, DESCRIPTION", "DTYPEID", "Please Select", l_dwellingtype, NULL, "textbox200", " tabindex='2' ")
    'Call BuildSelect(lstFuelType, "sel_FUELTYPE", "P_FUELTYPE", "FUELTYPEID, FUELTYPE", "FUELTYPE", "Please Select", l_fueltype, NULL, "textbox200", " tabindex='2' ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Portfolio--&gt; Main Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_PROPERTYID|Property Ref No|TEXT|Y"
        FormFields[1] = "sel_PROPERTYLEVEL|Property Level|SELECT|N"
        FormFields[2] = "txt_AVDATE|Estimated Av Date|DATE|N"
        FormFields[3] = "txt_HOUSENUMBER|House Number|TEXT|Y"
        FormFields[4] = "txt_ADDRESS1|Address 1|TEXT|Y"
        FormFields[5] = "txt_ADDRESS2|Address 2|TEXT|N"
        FormFields[6] = "txt_DATEBUILT|Date Built|DATE|N"
        FormFields[7] = "txt_VIEWINGDATE|Viewing Date|DATE|N"
        FormFields[8] = "txt_TOWNCITY|Town / City|TEXT|Y"
        FormFields[9] = "txt_LETTINGDATE|Letting Date|DATE|N"
        FormFields[10] = "txt_UNITDESC|Unit Description|TEXT|N"
        FormFields[11] = "rdo_RIGHTTOBUY|Right To buy|RADIO|Y"
        FormFields[12] = "sel_ASSETTYPE|Asset Type|SELECT|Y"
        FormFields[13] = "txt_COUNTY|COUNTY|TEXT|N"
        FormFields[14] = "txt_POSTCODE|Postcode|POSTCODE|Y"
        FormFields[15] = "sel_STATUS|Status|SELECT|Y"
        FormFields[16] = "sel_DEVELOPMENTID|Development|SELECT|Y"
        FormFields[17] = "sel_SUBSTATUS|Sub - Status|SELECT|<%=SubStatusValidate%>"
        FormFields[18] = "sel_BLOCKID|Block|SELECT|N"
        FormFields[19] = "sel_PROPERTYTYPE|Property Type|SELECT|Y"
        FormFields[20] = "txt_DEFECTSPERIODEND|Defects Period|DATE|N"
        FormFields[21] = "sel_STOCKTYPE|Stock Type|SELECT|Y"
        FormFields[22] = "txt_MANAGINGAGENT|Managing Agent|TEXT|N"
        FormFields[23] = "sel_OWNERSHIP|Confirmation of Ownership|SELECT|Y"
        FormFields[24] = "txt_MINPURCHASE|Initial Minimum Purchase|FLOAT|N"
        FormFields[25] = "txt_PURCHASELEVEL|Current Purchase Level|FLOAT|N"
        FormFields[26] = "sel_DWELLINGTYPE|Dwelling Type|SELECT|N"
        FormFields[27] = "sel_NROSHASSETTYPEMAIN|Nrosh Asset Type 1|SELECT|N"
        FormFields[28] = "sel_NROSHASSETTYPESUB|Nrosh Asset Type 2|SELECT|N"
        FormFields[29] = "sel_FUELTYPE|Fuel Type|SELECT|Y"

        function SaveForm() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/Property_svr.asp"
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }

        function GetBlocks() {
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Blocks.asp"
            document.RSLFORM.submit()
        }

        function GetNroshAssetSub() {
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Nrosh_AssetType.asp"
            document.RSLFORM.submit()
        }


        function GetSubStatus() {
            FormFields[18] = "sel_SUBSTATUS|SUBSTATUS|SELECT|N"
            parent.RSLFORM.sel_SUBSTATUS.disabled = true
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_SubStatus.asp"
            document.RSLFORM.submit()
        }

        function GetAddressData() {
            document.RSLFORM.target = "Property_Server"
            document.RSLFORM.action = "Serverside/Property_Address.asp"
            document.RSLFORM.submit()
        }

        function EnableDisableDwellType() {
            var ptype = document.getElementById("sel_PROPERTYTYPE").value;
            // FOR GARAGES,CAR PORTS AND CAR SPACES
            if (ptype == 6 || ptype == 8 || ptype == 9) {
                FormFields[26] = "sel_DWELLINGTYPE|Dwelling Type|SELECT|N"
            }
            else {
                FormFields[26] = "sel_DWELLINGTYPE|Dwelling Type|SELECT|Y"
            }
        }

        function EnableDisablePurchaseLevel() {
            // for Shared Ownership
            if (document.getElementById("sel_ASSETTYPE").value == 10) {
                FormFields[24] = "txt_MINPURCHASE|Initial Minimum Purchase|FLOAT|Y"
                FormFields[25] = "txt_PURCHASELEVEL|Current Purchase Level|FLOAT|Y"
                document.getElementById("txt_MINPURCHASE").style.backgroundColor = "#FFFFFF";
                document.getElementById("txt_PURCHASELEVEL").style.backgroundColor = "#FFFFFF";
                document.getElementById("txt_MINPURCHASE").disabled = false;
                document.getElementById("txt_PURCHASELEVEL").disabled = false;
            }
            else {
                FormFields[24] = "txt_MINPURCHASE|Initial Minimum Purchase|FLOAT|N"
                FormFields[25] = "txt_PURCHASELEVEL|Current Purchase Level|FLOAT|N"
                document.getElementById("txt_MINPURCHASE").style.backgroundColor = "#C0C0C0";
                document.getElementById("txt_PURCHASELEVEL").style.backgroundColor = "#C0C0C0";
                document.getElementById("txt_MINPURCHASE").disabled = true;
                document.getElementById("txt_PURCHASELEVEL").disabled = true;
                document.getElementById("txt_PURCHASELEVEL").value = '';
                document.getElementById("txt_MINPURCHASE").value = '';
            }
        }

        function changeStockType() {
            if (document.getElementById("sel_STOCKTYPE").value == 2)
                document.getElementById("txt_MANAGINGAGENT").style.display = "";
            else
                document.getElementById("txt_MANAGINGAGENT").style.display = "none";
            document.getElementById("txt_MANAGINGAGENT").value = "";
        }

    </script>
</head>
<body onload="initSwipeMenu(4);preloadImages();EnableDisablePurchaseLevel();EnableDisableDwellType()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" height="19">
                <font color="red">
                    <%=l_housenumber & " " & l_address1%>
                    -
                    <%=l_propertystatus%></font>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <% If ACTION = "NEW" Then %>
        <tr>
            <td rowspan="2">
                <img name="tab_main_details" src="images/tab_main_details-over.gif" width="97" height="20"
                    border="0" alt="Main Details" /></td>
            <td rowspan="2">
                <img name="tab_main_details" src="images/TabEnd.gif" width="8" height="20" border="0" alt="" /></td>
            <td align="right" height="19">
                <font color="red">
                    <%=l_propertystatus%></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="649" height="1" alt="" /></td>
        </tr>
        <% Else %>
        <tr>
            <td rowspan="2">
                <img name="tab_main_details" src="images/tab_main_details-over.gif" width="97" height="20"
                    border="0" alt="Main Details" /></td>
            <td rowspan="2">
                <a href="Financial.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Financial">
                    <img name="tab_financial" src="images/tab_financial-tab_main_deta.gif" width="79"
                        height="20" border="0" alt="Financial" /></a></td>           
            <td rowspan="2">
                <a href="Warranties.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Warranties">
                    <img name="tab_warranties" src="images/tab_warranties.gif" width="89" height="20"
                        border="0" alt="Warranties" /></a></td>
            <td rowspan="2">
                <a href="Utilities.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Utilities">
                    <img name="tab_utilities" src="images/tab_utilities.gif" width="68" height="20" border="0" alt="Utilities" /></a></td>
            <td rowspan="2">
                <a href="Marketing.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Marketing" >
                    <img name="tab_marketing" src="images/6-closed.gif" width="76" height="20" border="0" alt="Marketing" /></a></td>
            <td rowspan="2">
                <a href="healthandsafty.asp?PropertyID=<%=PropertyID%>&strAddress=<%=l_address1%>" title="Health and Safety">
                    <img name="tab_hands" src="images/tab_healthsafety_1.gif" width="110" height="20"
                        border="0" alt="Health and Safety" /></a></td>
            <td>
                <img alt="" src="/myImages/spacer.gif" width="151" height="19" /></td>
        </tr>
        <tr>
            <td bgcolor="#004174">
                <img src="images/spacer.gif" width="151" height="1" alt="" /></td>
        </tr>
        <% End If %>
        <tr>
            <td colspan="8" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="40" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table width="99%" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr>
            <td nowrap="nowrap">
                Property Ref:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_PROPERTYID" id="txt_PROPERTYID" class="textbox200" maxlength="20"
                    value="<%=l_propertyid%>" style='text-transform: uppercase' tabindex="1" readonly="readonly" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_PROPERTYID" id="img_PROPERTYID" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Status:
            </td>
            <td nowrap="nowrap">
                <%=lstStatus%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Development Name:
            </td>
            <td nowrap="nowrap">
                <%=lstDevelopments%>
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_DEVELOPMENTID" id="img_DEVELOPMENTID" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Sub - Status:
            </td>
            <td nowrap="nowrap">
                <%=lstSubStatus%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SUBSTATUS" id="img_SUBSTATUS" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Scheme Name:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_SCHEMENAME" id="txt_SCHEMENAME" class="textbox200" maxlength="50"
                    value="<%=l_schemename%>" tabindex="1" readonly="readonly" />
            </td>
            <td nowrap="nowrap">
            </td>
            <td nowrap="nowrap">
                Est. Practical Completion:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_AVDATE" id="txt_AVDATE" class="textbox200" maxlength="10" value="<%=l_avdate%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_AVDATE" id="img_AVDATE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Block:
            </td>
            <td nowrap="nowrap">
                <div id="dvBLOCKID"><%=lstBlocks%></div>
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_BLOCKID" id="img_BLOCKID" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Property Type:
            </td>
            <td nowrap="nowrap">
                <%=lstPropertyType%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PROPERTYTYPE" id="img_PROPERTYTYPE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                House Number:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_HOUSENUMBER" id="txt_HOUSENUMBER" class="textbox200" maxlength="30" value="<%=l_housenumber%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_HOUSENUMBER" id="img_HOUSENUMBER" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Dwelling Type:
            </td>
            <td nowrap="nowrap">
                <%=lstDwellingType%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DWELLINGTYPE" id="img_DWELLINGTYPE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Address 1:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_ADDRESS1" id="txt_ADDRESS1" class="textbox200" maxlength="100" value="<%=l_address1%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Level:
            </td>
            <td nowrap="nowrap">
                <%=lstLevel%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PROPERTYLEVEL" id="img_PROPERTYLEVEL" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Address 2:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_ADDRESS2" id="txt_ADDRESS2" class="textbox200" maxlength="100" value="<%=l_address2%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Asset Type:
            </td>
            <td nowrap="nowrap">
                <%=lstAssetType%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ASSETTYPE" id="img_ASSETTYPE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Town / City:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_TOWNCITY" id="txt_TOWNCITY" class="textbox200" maxlength="80" value="<%=l_towncity%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_TOWNCITY" id="img_TOWNCITY" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Nrosh Asset Type 1:
            </td>
            <td nowrap="nowrap">
                <%=lstNroshAssetTypeMain%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NROSHASSETTYPEMAIN" id="img_NROSHASSETTYPEMAIN" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                County:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_COUNTY" id="txt_COUNTY" class="textbox200" maxlength="50" value="<%=l_county%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Nrosh Asset Type 2:
            </td>
            <td nowrap="nowrap">
                <%=lstNroshAssetTypeSub%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NROSHASSETTYPESUB" id="img_NROSHASSETTYPESUB" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Postcode:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_POSTCODE" id="txt_POSTCODE" class="textbox200" maxlength="10" value="<%=l_postcode%>"
                    tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Initial Min Purchase:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_MINPURCHASE" id="txt_MINPURCHASE" class="textbox200"
                    maxlength="100" value="<%=l_minpurchase%>" tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MINPURCHASE" id="img_MINPURCHASE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Defects Period:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_DEFECTSPERIODEND" id="txt_DEFECTSPERIODEND" class="textbox200" maxlength="10"
                    value="<%=l_defectsperiodend%>" tabindex="1" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_DEFECTSPERIODEND" id="img_DEFECTSPERIODEND" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Current Purchase Level:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_PURCHASELEVEL" id="txt_PURCHASELEVEL" class="textbox200" maxlength="10" value="<%=l_purchaselevel%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_PURCHASELEVEL" id="img_PURCHASELEVEL" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Stock Type / Agent
            </td>
            <td nowrap="nowrap">
                <%=lstStockType %><br />
                <input type="text" name="txt_MANAGINGAGENT" id="txt_MANAGINGAGENT" class="textbox200" value="<%=l_ManagingAgent %>"
                    style="display: <% if l_StockType <> 2 then rw "none" %>" />
            </td>
            <td nowrap="nowrap">
                <img src="/js/FVS.gif" name="img_STOCKTYPE" id="img_STOCKTYPE" width="15px" height="15px" border="0"
                    alt="" /><br />
                <img src="/js/FVS.gif" name="img_MANAGINGAGENT" id="img_MANAGINGAGENT" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Date Built:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_DATEBUILT" id="txt_DATEBUILT" class="textbox200" maxlength="10" value="<%=l_datebuilt%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DATEBUILT" id="img_DATEBUILT" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Confirmation of Ownership
            </td>
            <td nowrap="nowrap">
                <%=lstOwnership%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_OWNERSHIP" id="img_OWNERSHIP" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Viewing Date:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_VIEWINGDATE" id="txt_VIEWINGDATE" class="textbox200" maxlength="10" value="<%=l_viewingdate%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_VIEWINGDATE" id="img_VIEWINGDATE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Right to Acquire:
            </td>
            <td nowrap="nowrap">
                <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_righttobuy = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
                %>
                <input type="radio" name="rdo_RIGHTTOBUY" value="1" <%=yesChecked%> tabindex="2" />
                Yes
                <input type="radio" name="rdo_RIGHTTOBUY" value="0" <%=noChecked%> tabindex="2" />
                No
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RIGHTTOBUY" id="img_RIGHTTOBUY" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Hand Over Date:
            </td>
            <td nowrap="nowrap">
                <input type="text" name="txt_LETTINGDATE" id="txt_LETTINGDATE" class="textbox200" maxlength="10" value="<%=l_lettingdate%>"
                    tabindex="2" <%=lettingDateReadOnly%> />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LETTINGDATE" id="img_LETTINGDATE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
       <!-- <tr>
            <td nowrap="nowrap">
                Fuel Type:
            </td>
            <td nowrap="nowrap">
                <%=lstFuelTYpe%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FUELTYPE" id="img_FUELTYPE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr> -->
        <tr>
            <td nowrap="nowrap" colspan="7" style="display: none">
                <hr style="border: 1px dotted #133e71" />
            </td>
        </tr>
        <tr style="display: none">
            <td nowrap="nowrap" valign="top">
                UNIT DESCRIPTION:
            </td>
            <td nowrap="nowrap" colspan="4">
                <textarea name="txt_UNITDESC" id="txt_UNITDESC" class="textbox" rows="4" cols="88" tabindex="3"><%=l_unitdesc%></textarea>
            </td>
            <td valign="top">
                <img src="/js/FVS.gif" name="img_UNITDESC" id="img_UNITDESC" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" colspan="5" align="right">
                <input type="hidden" name="hid_Address" id="hid_Address" value="<%=l_address1%>" />
                <input type="hidden" name="hid_PropertyID" id="hid_PropertyID" value="<%=PropertyID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="button" name="BtnSave" id="BtnSave" onclick="SaveForm()" value=" SAVE " title=" SAVE " class="RSLButton" style="cursor:pointer" tabindex="3" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Property_Server" id="Property_Server" width="4" height="4" style="display: none"></iframe>
</body>
</html>
