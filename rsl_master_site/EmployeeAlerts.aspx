<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmployeeAlerts.aspx.vb" Inherits="EmployeeAlerts" Trace="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="css/RSL.css" />
    <script type="text/javascript" language="javascript">
    function goback() {
      parent.location.href = "BusinessManager/Employees.asp?alerts=true";
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:Panel ID="Panel1" runat="server" Height="50px" Width="750px">
        <div>
        <table border="0" cellpadding="0" cellspacing="0" visible="true" width="100%">
            <tr>
                <td rowspan="2" style="width: 137px">
                    <img src="images/tab_alerts.gif" />
                </td>
                <td style="width: 75px; height: 21px;">
                    <img height="19" src="images/spacer.gif" width="695" /></td>
            </tr>
            <tr>
                <td bgcolor="#133e71" style="width: 75px; height: 1px">
                    <img height="1" src="../Portfolio/images/spacer.gif" width="74" /></td>
            </tr>
            <tr>
                <td colspan="3" style="border-right: #133e71 1px solid; border-left: #133e71 1px solid;
                    height: 6px">
                    <img height="6" src="../Portfolio/images/spacer.gif" width="53" /></td>
            </tr>
        </table>
    
    </div>
        <table style="border-right: #133e71 1px solid; border-left: #133e71 1px solid; border-bottom: #133e71 1px solid"
            width="100%">
            <tr>
                <td style="padding-right: 5px; padding-left: 5px; padding-bottom: 10px; padding-top: 10px; vertical-align: middle; text-align: left;">
                    <asp:FormView ID="fvEmployee" runat="server" CellPadding="5" CellSpacing="5" DataKeyNames="EMPLOYEEID"
                        DataSourceID="dsEmployee" Width="70%">
                        <EditItemTemplate>
                            EMPLOYEEID:
                            <asp:Label ID="EMPLOYEEIDLabel1" runat="server" Text='<%# Eval("EMPLOYEEID") %>'>
                            </asp:Label><br />
                            FULLNAME:
                            <asp:TextBox ID="FULLNAMETextBox" runat="server" Text='<%# Bind("FULLNAME") %>'>
                            </asp:TextBox><br />
                            TEAMNAME:
                            <asp:TextBox ID="TEAMNAMETextBox" runat="server" Text='<%# Bind("TEAMNAME") %>'>
                            </asp:TextBox><br />
                            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update"
                                Text="Update">
                            </asp:LinkButton>
                            <asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancel">
                            </asp:LinkButton>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            FULLNAME:
                            <asp:TextBox ID="FULLNAMETextBox" runat="server" Text='<%# Bind("FULLNAME") %>'>
                            </asp:TextBox><br />
                            TEAMNAME:
                            <asp:TextBox ID="TEAMNAMETextBox" runat="server" Text='<%# Bind("TEAMNAME") %>'>
                            </asp:TextBox><br />
                            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert"
                                Text="Insert">
                            </asp:LinkButton>
                            <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                Text="Cancel">
                            </asp:LinkButton>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <table cellpadding="2" width="100%">
                                <tr>
                                    <td style="text-align: right">
                                Employee :</td>
                                    <td>
                                        <asp:Label ID="FULLNAMELabel" runat="server" Text='<%# Bind("FULLNAME") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                Team :</td>
                                    <td>
                                        <asp:Label ID="TEAMNAMELabel" runat="server" Text='<%# Bind("TEAMNAME") %>'></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; text-align: right">
                                Alerts :</td>
                                    <td style="vertical-align: top; text-align: left">
                                <asp:CheckBoxList ID="cbEAlerts" runat="server" CellPadding="2" CellSpacing="2" DataSourceID="dsEAlerts"
                                    DataTextField="AlertName" DataValueField="AlertID" RepeatColumns="2">
                                </asp:CheckBoxList></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; text-align: right">
                                <asp:Button ID="btnSelectAll" runat="server" Text="Select All" CssClass="RSLButton" OnClick="btnSelectAll_Click" /></td>
                                    <td>
                                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="RSLButton" OnClick="btnBack_Click" OnClientClick="goback()" />
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="RSLButton" OnClick="btnSave_Click" /></td>
                                </tr>
                            </table>
                            
                        </ItemTemplate>
                    </asp:FormView>
                    <asp:SqlDataSource ID="dsEAlerts" runat="server" ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>"
                        SelectCommand="SELECT AlertID, AlertName FROM E_ALERTS ORDER BY SOrder"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsEmployee" runat="server" ConnectionString="<%$ ConnectionStrings:RSL Manager Broadlands DevConnectionString %>"
                        SelectCommand="SELECT E__EMPLOYEE.EMPLOYEEID, CASE WHEN E__EMPLOYEE.FIRSTNAME IS NULL THEN ' ' ELSE E__EMPLOYEE.FIRSTNAME END + ' ' + CASE WHEN E__EMPLOYEE.MIDDLENAME IS NULL THEN ' ' ELSE E__EMPLOYEE.MIDDLENAME END + ' ' + CASE WHEN E__EMPLOYEE.LASTNAME IS NULL THEN ' ' ELSE E__EMPLOYEE.LASTNAME END AS FULLNAME, E_TEAM.TEAMNAME FROM E__EMPLOYEE INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID INNER JOIN E_TEAM ON E_JOBDETAILS.TEAM = E_TEAM.TEAMID WHERE (E__EMPLOYEE.EMPLOYEEID = @empid)">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="empid" QueryStringField="empid" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </table>
        </asp:Panel>
    </form>
</body>
</html>
