<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
Dim i = 1

'    Set objMailNot = CreateObject("CDONTS.NewMail")
'	objMailNot.From = "bh@rslmanager.co.uk"
'	objMailNot.To = "robert.egan@reidmark.com"
'	objMailNot.Subject = "It's Broke"
'	objMailNot.BodyFormat = 0 
'	objMailNot.MailFormat = 0 
'
'	HTML = ""
'	HTML = "<html><head><title>Live RSL Manager Error</title>" 
'	HTML = HTML & "<style><!--.iagManagerSmallBlk {font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 10px;font-style: normal;line-height: normal;font-weight: normal;color: #000000;text-decoration: none;}--></STYLE>"
'	HTML = HTML & "</HEAD><body bgcolor='#FFFFFF' CELLSPACING=0 CELLPADDING=0 BORDER=0 HEIGHT=100% BORDERCOLOR=#FFFFFF>" 
'	HTML = HTML & "<TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 width=100% BORDERCOLOR=#FFFFFF><TR><TD><image src=""http://bh.rslmanager.co.uk/myimages/rslmanager_logo.gif""></TD></TR></TABLE>" 
'	HTML = HTML & "</body></html>"
'
'	objMailNot.Body = HTML
'	objMailNot.Send 
'
'	Set objMailNot = Nothing 
%>
<%
Dim iMsg
Dim iConf
Dim Flds
Dim HTML
Dim strSmartHost

Const cdoSendUsingPort = 2
StrSmartHost = "localhost" //must be one word or will not send email

set iMsg = CreateObject("CDO.Message")
set iConf = CreateObject("CDO.Configuration")

Set Flds = iConf.Fields

' set the CDOSYS configuration fields to use port 25 on the SMTP server

With Flds
.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = cdoSendUsingPort
.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = strSmartHost
.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10
.Update
End With



UserName = ""
UserPass = ""
UserEmail = ""


ErrorType = " REAL "


	HTML = ""
	HTML = "<html><head><title>Batch Manager Error</title>" 
	HTML = HTML & "<style><!--TD{border-top:1px dotted silver} .EmailFont {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-style: normal;line-height: normal;font-weight: normal;color: #000000;text-decoration: none;}--></STYLE>"
	HTML = HTML & "</HEAD><body bgcolor='#ffffff' CELLSPACING=0 CELLPADDING=0 BORDER=0 HEIGHT=100% BORDERCOLOR=#FFFFFF>" 
	HTML = HTML & "<div width='100%' style='background-color=#BDBDBD;padding:10px;text-align:right;border:1px solid black'><img src=""http://lsciag.misystem.co.uk/images/lscmimanager.gif""></div><br>"

	HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
	HTML = HTML &  "<tr><td colspan=2 style='font-size:14px;border-top:none'><b><u>Main Error Details</u></b></td></tr>"	
	HTML = HTML &  "<tr><td width='160'><font color=blue>System:</font></td><td>"& IagType &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>User Name & ID:</font></td><td>" & Session("FIRSTNAME") & " " & Session("LASTNAME") & ", ["& Session("USERID") &"]</td></tr>"		
	HTML = HTML &  "<tr><td><font color=blue>User Login Name:</font></td><td>"& UserName &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>User Email:</font></td><td>"& UserEmail &"</td></tr>"			
	HTML = HTML &  "<tr><td><font color=blue>Request Objects:</font></td><td>"& Session("MadError") &"</td></tr>"			
	HTML = HTML &  "<tr><td><font color=blue>Organisation / Type:</font></td><td>"& Session("ORGNAME") & " " & Session("TYPENAME") & "</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Date/Time:</font></td><td>"& now & "</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Error Number:</font></td><td>"& strNumber &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Source:</font></td><td>"& strSource &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>File:</font></td><td>"& strPage &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Querystring:</font></td><td>"& request.QueryString &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>POST Data:</font></td><td>"& request.Form &"</td></tr>"
	HTML = HTML &  "<tr valign='top'><td ><font color=blue>Description:</font></td><td>" & strDesc & ". " & strASPDesc &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Code:</font></td><td>"& strcode &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Line:</font></td><td>"& strLine &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Remote Address:</font></td><td>"& strRemoteAddr &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Remote Host:</font></td><td>"& strRemoteHost &"</td></tr>"
	HTML = HTML &  "<tr><td><font color=blue>Local Address:</font></td><td>"& strLocalAddr &"</td></tr>"
	HTML = HTML &  "</table>"

	HTML = HTML & "<BR>"

	HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
	HTML = HTML &  "<tr><td colspan=2 style='font-size:14px;border-top:none'><b><u>Extended Server Variable Information</u></b></td></tr>"		
	HTML = HTML &  "<tr><td width=160><b>Server Variable Name</b></td><td><b>Server Variable Value</b></td></tr>"		
	for each x in Request.ServerVariables
	  HTML = HTML &  "<tr><td><font color=blue>" & x & ":</font></td><td>" & Request.ServerVariables(x) & "</td></tr>"
	next
	HTML = HTML &  "</table>"			
	
	HTML = HTML &  "</body></html>"
	
' apply the settings to the message
With iMsg
Set .Configuration = iConf
.To = "robert.egan@reidmark.com"
.From = "bha@reidmark.com"
.Subject = "Batch Manager - " & ErrorType & "Error"
.HTMLBody = HTML
'.Send
End With

' cleanup of variables
Set iMsg = Nothing
Set iConf = Nothing
Set Flds = Nothing

rw HTML
%>