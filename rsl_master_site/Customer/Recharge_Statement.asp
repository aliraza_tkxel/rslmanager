<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../iFrames/iHB.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color, ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED

	CurrentDate = CDate("1 jan 2004") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	
	'REsponse.Write ThePreviousDate
	OpenDB()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")
	if (tenancy_id = "") then
		Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
		If Not rsSet.EOF Then 
			tenancy_id = rsSet(0)
		Else
			tenancy_id = -1
		End If
		CloseRs(rsSet)
	end if
		
	build_journal()
	
	'HBOWED = TOTAL_HB_OWED_THIS_DATE(customer_id, tenancy_id)
	
	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, J.TRANSACTIONDATE, 103) ,'') AS FTRANSACTIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTSTARTDATE, 103) ,'') AS FPAYMENTSTARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTENDDATE, 103) ,'') AS PAYMENTENDDATE, " &_
					"			ISNULL(I.DESCRIPTION, ' ') AS ITEMTYPE, " &_
					"			ISNULL(P.DESCRIPTION, ' ') AS PAYMENTTYPE, " &_
					"			ISNULL(J.AMOUNT, 0) AS AMOUNT, " &_
					"			ISNULL(J.ISDEBIT, 1) AS ISDEBIT, " &_
					"			ISNULL(S.DESCRIPTION, ' ') AS STATUS " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.STATUSID IS NULL) AND J.ITEMTYPE IN (5)" &_
					"ORDER BY TRANSACTIONDATE ASC, PAYMENTTYPE"
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			If rsSet("ISDEBIT") = 0 Then
				str_color = "STYLE='COLOR:RED'"
			Else str_color = "" End If	
			
			str_journal_table = str_journal_table & 	"<TR " & str_color & " >" &_
															"<TD>" & rsSet("FTRANSACTIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ITEMTYPE") & "</TD>" &_
															"<TD>" & rsSet("PAYMENTTYPE") & "</TD>" &_
															"<TD>" & rsSet("FPAYMENTSTARTDATE") & "&nbsp;" & rsSet("PAYMENTENDDATE") & "</TD>" &_
															"<TD ALIGN=RIGHT>" & FormatCurrency(rsSet("AMOUNT"))  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR>"
		End If


		strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
					"			AND J.ITEMTYPE IN (5)"

		'response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		ACCOUNT_BALANCE = CDbl(rsSet("AMOUNT"))
		CloseRs(rsSet)
	End Function

	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			"WHERE T.TENANCYID = " & tenancy_id
	
	HBOWED = 0

	Call OpenRS(rsCust, SQL)

	count = 1
	while NOT rsCust.EOF
		'This will get the hbowed for each customer
		HBOWED = HBOWED + TOTAL_HB_OWED_THIS_DATE(rsCust("CUSTOMERID"), tenancy_id)	
		
		StringName = ""
		Title = rsCust("TITLE")
		if (Title <> "" AND NOT isNull(Title)) then
			StringName = StringName & Title & " "
		end if
		FirstName = rsCust("FIRSTNAME")
		if (FirstName <> "" AND NOT isNull(FirstName)) then
			StringName = StringName & FirstName & " "
		end if
		LastName =rsCust("LASTNAME")
		if (LastName <> "" AND NOT isNull(LastName)) then
			StringName = StringName & LastName & " "
		end if
		
		if (count = 1) then
			StringFullName = StringName
			count = 2
		else
			StringFullName = StringFullName & "& " & StringName
		end if
		rsCust.moveNext
	wend
	if (count = 2) then
		rsCust.moveFirst()
		housenumber = rsCust("housenumber")
		if (housenumber = "" or isNull(housenumber)) then
			housenumber = rsCust("flatnumber")
		end if
		if (housenumber = "" or isNull(housenumber)) then
			FirstLineOfAddress = rsCust("Address1")
		else
			FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
		end if

		RemainingAddressString = ""
		AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCust(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap>" & temp & "</TD></TR>"
			end if
		next
		tenancyref = rsCust("TENANCYID")
	end if
	
	FullAddress = "<TR><TD nowrap>" & StringFullName & "</TD></TR>" &_
				"<TR><TD nowrap>" & FirstLineOfAddress & "</TD></TR>" &_
				RemainingAddressString				


	SQL = "SELECT  E.FIRSTNAME + ' ' + E.LASTNAME as FullName FROM C_TENANCY T " &_
			" INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			" INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			" INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
			" LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			" WHERE T.TENANCYID = " & tenancy_id
			
	Call OpenRS(rsHousingOfficer, SQL)
	If not rsHousingOfficer.eof then Fullname = rsHousingOfficer("Fullname") End If
	
	CloseDB()
%>
<HTML>
<HEAD>
<TITLE>Customer Info:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<script lanaguage=javascript>
function PrintMe(){
	document.getElementById("PrintButton").style.visibility = "hidden"
	print()
	document.getElementById("PrintButton").style.visibility = "visible"	
	}
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()" class='ta'>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="110" valign="top"> 
      <table width=100%>
        <tr> 
          <td nowrap valign=top width="137"> 
            <table>
              <tr> 
                <td nowrap><u><b><font color="#133e71" style="font-size:14px">Recharge 
                  Statement</font></b></u><br>
                  <br>
                </td>
              </tr>
              <tr> 
                <td nowrap style='border:1px solid #133E71'>Tenancy Ref: <b><%=TenancyReference(tenancyref)%></b></td>
              </tr>
              <%=FullAddress%> 
            </table>
          </td>
          <td align=right width=683 valign=top>&nbsp; </td>
          <td align=right valign="top" width="150"><img src="../Images/BHALOGOLETTER.gif" width="145" height="113"> 
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="171"> 
      <table width=100% cellpadding=1 cellspacing=0 style="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
        <thead> 
        <tr valign=TOP> 
          <td colspan=4 height=20><b><font color='#133e71' style='font-size:14px'>Date: 
            <%=MonthName(DatePart("m", Date)) & " " & DatePart("yyyy", Date)%></font></b></td>
          <td colspan=3 height=20> 
            <input type="button" value=" Print Statement" onClick="PrintMe()" class="RSLButton" name="PrintButton">
          </td>
        </tr>
        <tr valign=TOP> 
          <td style="BORDER-BOTTOM:1PX SOLID" width=120><b><font color='#133e71'>Date</font></b></td>
          <td width="150" style="BORDER-BOTTOM:1PX SOLID"><b><font color='#133e71'> 
            Item Type</font></b></td>
          <td width="200" style="BORDER-BOTTOM:1PX SOLID"><b><font color='#133e71'>Payment 
            Type</font></b></td>
          <td width="150" style="BORDER-BOTTOM:1PX SOLID"><b><font color='#133e71'>Duration</font></b></td>
          <td align=RIGHT style="BORDER-BOTTOM:1PX SOLID"><b><font color='#133e71'>Amount</font></b></td>
        </tr>
        <tr style='HEIGHT:7PX'> 
          <td colspan=4></td>
        </tr>
        </thead> <tbody><%=str_journal_table%></tbody><tfoot> 
        <%If HBOWED <> 0 then %>
        <tr valign=TOP> 
          <td style="BORDER-TOP:1PX SOLID" colspan=4 height=20 align=right><b><font color='red' style='font-size:14px'>Anticipated 
            HB Still Due:&nbsp;</font></b></td>
          <td style="BORDER-TOP:1PX SOLID" align=RIGHT><b><font color='red' style='font-size:14px'><%=FormatCurrency(HBOWED)%></font></b></td>
        </tr>
        <%Else %>
        <tr valign=TOP> 
          <td style="BORDER-TOP:1PX SOLID" colspan=4 height=20 align=right></td>
          <td style="BORDER-TOP:1PX SOLID" align=RIGHT></td>
        </tr>
        <%End If %>
        <tr valign=TOP> 
          <td colspan=4 height=20 align=right><b><font color='#133e71' style='font-size:14px'>Balance 
            <%If HBOWED <> 0 then %>
            after Housing Benefit 
            <%End If %>
            :&nbsp;</font></b></td>
          <td align=RIGHT><b><font color='#133e71' style='font-size:14px'><%=FormatCurrency(ACCOUNT_BALANCE-HBOWED)%></font></b></td>
        </tr>
        <tr valign=TOP> 
          <td colspan=4 height=20><br>
            <br>
            Please contact your housing officer <%= FullName%> to discuss further.</td>
        </tr>
        </tfoot> 
      </table>
    </td>
  </tr>
  <tr>
    <td valign="bottom" align="right" height="100%"> 
      <table width="151" align=right>
        <tr> 
          <td> 
            <div align="right">&nbsp;Broadland Housing Association</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">&nbsp;NCFC Jarrold Stand</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">&nbsp;Carrow Road</div>
          </td>
        </tr>
        <tr> 
          <td> 
            <div align="right">&nbsp;Norwich NR1 1HU</div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</BODY>
</HTML>

