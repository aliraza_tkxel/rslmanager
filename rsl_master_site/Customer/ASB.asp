<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 15

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim ochecked, cchecked
	Dim rqType, rqCategory, rqEmployee, rqToDate, rqDescription, SQL_Filter, rqFromDate 
	Dim rqPatch, rqScheme, developmentfilter, employeefilter, rqStatus, rqGrade
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	PageName = "ASB.asp"
	MaxRowSpan = 8

	Call OpenDB()
	Call RequestVars()

	If (rqPatch <> "") Then
	    developmentfilter = " WHERE PATCHID=" & rqPatch & " "
	    employeefilter = " AND J.PATCH=" & rqPatch & " " 
	Else
	    developmentfilter = ""
	    employeefilter = ""
	End If

    	Call BuildSelect(selType, "sel_TYPE", " C_NATURE WHERE ITEMNATUREID IN (8,24,25) ", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "All", rqType, NULL, "textbox200", "onchange='' tabindex='2' style='width:140px' ")
    	Call BuildSelect(selGrade, "sel_GRADE", "C_ASB_GRADE","GRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", rqGrade, NULL, "textbox200", " style='width:140px' onchange='grade_change()' ")
        'Call BuildSelect(selCategory, "sel_CATEGORY", " C_ASB_CATEGORY_BY_GRADE", "CATEGORYID, DESCRIPTION", "DESCRIPTION", "All", rqCategory, NULL, "textbox200", "onchange='' tabindex='3' ")
   	    Call BuildSelect(selEmployee, "sel_EMPLOYEE", " E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON J.EMPLOYEEID=E.EMPLOYEEID WHERE E.EMPLOYEEID IN (SELECT ASSIGNTO FROM C_ASB) " & employeefilter, "E.EMPLOYEEID, ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'')", "ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'')", "All", rqEmployee, NULL, "textbox200", "onchange='' tabindex='4' style='width:200px'")
	    Call BuildSelect(selScheme, "sel_SCHEME", "P_SCHEME INNER JOIN PDR_DEVELOPMENT D ON P_SCHEME.DevelopmentId= D.DEVELOPMENTID " & developmentfilter, " SCHEMEID, SCHEMENAME ", " SCHEMENAME ", "All", rqScheme, NULL, "textbox200", "onchange='' tabindex='3' ")
		Call BuildSelect(selPatch, "sel_PATCH", "E_PATCH", " PATCHID, LOCATION ", " LOCATION ", "All", rqPatch, NULL, "textbox200", " TABINDEX=11 style='width:140px' onChange='GO()' ")	
		Call BuildSelect(selstatus, "sel_STATUS", " C_STATUS WHERE ITEMSTATUSID IN(13,14) ", "ITEMSTATUSID, DESCRIPTION", "ITEMSTATUSID", "All", rqStatus, NULL, "textbox200", "onchange='' tabindex='2' style='width:140px' ")

	Call ApplyFilters()
	Call get_ASB()
	Call CloseDB()


    Function RequestVars()

        rqPatch = Request("sel_PATCH")
        rqScheme = Request("SEL_SCHEME")
        rqType = Request("sel_Type")
        rqGrade = Request("sel_Grade")
        rqCategory = Request("sel_Category")
        rqEmployee = Request("sel_Employee")
        rqFromDate = Request("txt_FromDate")
        rqToDate = Request("txt_ToDate")
        rqStatus = Request("sel_STATUS")

    End Function

    Function ApplyFilters()

        If rqPatch<>"" Then
            SQL_Filter = SQL_Filter & " AND EP.PATCHID = " & rqPatch
        End If

        If rqScheme<>"" Then
            SQL_Filter = SQL_Filter & " AND SCH.SCHEMEID = " & rqScheme
        End If

        If rqType <> "" Then
            SQL_Filter = SQL_Filter & " AND J.ITEMNATUREID = " & rqType
        End If

        If rqGrade <> "" Then
            SQL_Filter = SQL_Filter & " AND CAT.GRADEID = " & rqGrade
        End If

        If rqCategory <> "" Then
            SQL_Filter = SQL_Filter & " AND AC.CATEGORYID = " & rqCategory
        End If

        If rqEmployee <> "" Then
            SQL_Filter = SQL_Filter & " AND E.EMPLOYEEID = " & rqEmployee
        End If

        If rqFromDate <> "" Then
            SQL_Filter = SQL_Filter & " AND J.CREATIONDATE >= '" & rqFromDate & " 00:00:00'"
        End If

        If rqToDate <> "" Then
            SQL_Filter = SQL_Filter & " AND J.CREATIONDATE <= '" & rqToDate & " 23:59:00'"
        End If

        If rqStatus <> "" Then
            SQL_Filter = SQL_Filter & " AND J.CURRENTITEMSTATUSID = " & rqStatus
        End If

    End Function


	Function get_ASB()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 

			intRecord = 0
			str_data = ""
			strSQL =		"SELECT J.CREATIONDATE, J.CUSTOMERID, " &_ 
							"		LTRIM(ISNULL(TIT.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS CNAME, " &_
							"		P.HOUSENUMBER + ' ' + P.ADDRESS1 AS ADDRESS, N.DESCRIPTION, G.NOTES, " &_
							"		LTRIM(ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,' ')) AS ENAME, " &_
							"		ISNULL(CAT.DESCRIPTION,AC.DESCRIPTION) AS CATEGORY,J.TITLE,CAT.GRADEID AS GRADE " &_
							" FROM 	C_JOURNAL J  " &_
							"		INNER JOIN C_ASB G ON J.JOURNALID = G.JOURNALID " &_
                            " LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE CAT ON CAT.CATEGORYID=G.CATEGORY " &_
							"		LEFT JOIN  C_ASB_CATEGORY AC ON AC.CATEGORYID = G.CATEGORY " &_
							"		INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID  " &_
							"		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
							"		LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " &_
							"		INNER JOIN (  " &_
							"				SELECT MAX(TENANCYID) AS MAXTEN, CUSTOMERID  " &_
							"				FROM   C_CUSTOMERTENANCY  " &_
							"				GROUP BY CUSTOMERID " &_
							"			   ) MT ON MT.CUSTOMERID = C.CUSTOMERID " &_
							"		INNER JOIN (  " &_
							"				SELECT MAX(ASBHISTORYID) AS MAXHIST, JOURNALID " &_
							"				FROM   C_ASB " &_
							"				GROUP BY JOURNALID " &_
							"			   ) MH ON MH.JOURNALID = J.JOURNALID " &_
							"		INNER JOIN C_TENANCY T ON T.TENANCYID = MT.MAXTEN  " &_
							"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
							"       LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " &_
                            "	LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
                            "   INNER JOIN P_SCHEME SCH ON P.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
							"       LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID " &_
							"		INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
							"		INNER JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE " &_
							"	WHERE J.ITEMNATUREID IN (8,9,24,25) " & SQL_Filter & " " &_
							"   AND G.ASBHISTORYID=(SELECT MAX(ASBHISTORYID) FROM C_ASB WHERE JOURNALID=G.JOURNALID) "&_
							"	ORDER BY J.CREATIONDATE DESC, LTRIM(ISNULL(TIT.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) ASC "

			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()

			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount
			intRecordCount = rsSet.RecordCount

			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If

			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If

			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If

			' Make sure that the recordset is not empty.  If it is not, then set the
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If

			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<tbody class=""CAPS"">"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize

					strStart = 	"<tr><td>" & rsSet("CREATIONDATE") & "</td>"
					strEnd = 	"<td>" & rsSet("TITLE") & "</td>" &_
								"<td style=""cursor:pointer"" onclick=""load_me(" & rsSet("CUSTOMERID") & ")""><font color=""blue""> " & rsSet("CNAME") & "</font></td>" &_
								"<td>" & rsSet("ADDRESS") & "</td>" &_
								"<td>" & rsSet("DESCRIPTION") & "</td>" &_
								"<td>" & rsSet("GRADE") & "</td>" &_
								"<td>" & rsSet("CATEGORY") & "</td>" &_
								"<td>" & rsSet("ENAME") & "</td></tr>"
					str_data = str_data & strStart & strEnd
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for

				Next

				str_data = str_data & "</tbody>"

				'ensure table height is consistent with any amount of records
				Call fill_gaps()

				' links
				str_data = str_data &_
				"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
				"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
				"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
				"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
				" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>"  &_
				"</td><td align=""right"" width=""100"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133e71;font-size:11px"">&nbsp;"  &_
				"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"">"  &_
				"</td></tr></thead></table></td></tr></tfoot>"
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<tr><td colspan=""" & MaxRowSpan & """ style=""font:12px"" align=""center"">No Records found.</td></tr>" 
				Call fill_gaps()
			End If

			rsSet.close()
			Set rsSet = Nothing

		End function

		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<script type=""text/javascript"" language=""JavaScript"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</script>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		' pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Arrears List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

	function load_me(CustomerID){
		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

	var FormFields = new Array()

	FormFields[0] = "sel_GRADE|Type|INT|N"
	FormFields[1] = "sel_STATUS|Status|INT|N"
	FormFields[2] = "sel_TYPE|Type|INT|N"
	FormFields[3] = "sel_CATEGORY|Category|INT|N"
	FormFields[4] = "sel_EMPLOYEE|Employee|INT|N"
    FormFields[5] = "txt_FromDate|From Date|DATE|y"
    FormFields[6] = "txt_ToDate|To Date|DATE|N"

	function GO1(){
		if (!checkForm()) return false;
			document.RSLFORM.action = "<%=PageName%>"
			document.RSLFORM.target = ""
			document.RSLFORM.submit()
		}

    function GO(){
		if (!checkForm()) return false;
		location.href = "<%=PageName%>?go_clicked=1&sel_GRADE=" + document.getElementById("sel_GRADE").value + "&sel_TYPE=" + document.getElementById("sel_TYPE").value + "&sel_CATEGORY=" + document.getElementById("sel_CATEGORY").value + "&sel_STATUS=" + document.getElementById("sel_STATUS").value + "&sel_Scheme=" + document.getElementById("sel_SCHEME").value + "&sel_PATCH=" + document.getElementById("sel_PATCH").value + "&sel_EMPLOYEE=" + document.getElementById("sel_EMPLOYEE").value + "&txt_ToDate=" + document.getElementById("txt_ToDate").value + "&txt_FromDate=" + document.getElementById("txt_FromDate").value
	}

     function grade_change(){
		document.RSLFORM.action = "Serverside/asb_grade_change.asp?page_id=REPORT&GRADEID="+document.getElementById("sel_GRADE").value
		document.RSLFORM.target = "ServerIFrame"
		document.RSLFORM.submit()
	}

	/////////////////////////////////
	//	Printing and Xls Below
	/////////////////////////////////

	function PrintMe(){
		window.open("PrintMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + document.getElementById("txt_VALUE").value + "&WHICH=" + document.getElementById("WHICH").value + "&sel_Scheme=" + document.getElementById("sel_SCHEME").value + "&sel_Action=" + document.getElementById("sel_ACTION").value+ "&txt_ASATDATE=" + document.getElementById("txt_ASATDATE").value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
	}

	function XLSMe()
	{
		//Build Query string
		var querystring
		    querystring = ""
	        querystring=querystring + "?sel_Patch="+ document.getElementById("sel_PATCH").value
	        querystring=querystring + "&sel_Scheme="+ document.getElementById("sel_SCHEME").value
	        querystring=querystring + "&sel_STATUS="+ document.getElementById("sel_STATUS").value
	        querystring=querystring + "&sel_TYPE="+ document.getElementById("sel_TYPE").value
	        querystring=querystring + "&sel_CATEGORY="+ document.getElementById("sel_CATEGORY").value
	        querystring=querystring + "&sel_EMPLOYEE="+ document.getElementById("sel_EMPLOYEE").value
	        querystring=querystring + "&txt_FromDate="+ document.getElementById("txt_FromDate").value
	        querystring=querystring + "&txt_ToDate="+ document.getElementById("txt_ToDate").value
	        querystring=querystring + "&sel_GRADE="+ document.getElementById("sel_GRADE").value

		    window.open("ASB_xls.asp"+ querystring);
		}

    </script>
    <%=WriteJavaJumpFunction%>
</head>
<body onload="initSwipeMenu(7);preloadImages();grade_change()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td height="1" valign="bottom">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table style="border: 1px solid #133e71" bgcolor="#f5f5dc">
                                <tr>
                                    <td height="1" valign="bottom">
                                        <strong>ASB List</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        Use the filters below to search for individual ASB entries more easily. Just click
                                        'Apply Filters' and this will apply your selection.<br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        Patch:<img name="img_PATCH" id="img_PATCH" src="/js/FVS.gif" width="1" height="1"
                                            alt="" />
                                    </td>
                                    <td valign="middle">
                                        <%=selPatch %>
                                    </td>
                                    <td valign="middle">
                                        Scheme:<img name="img_SCHEME" id="img_SCHEME" src="/js/FVS.gif" width="1" height="1"
                                            alt="" />
                                    </td>
                                    <td valign="middle">
                                        <%=selScheme %>
                                    </td>
                                    <td valign="middle" style="padding-top: 10px; padding-right: 0px;">
                                        Status :
                                        <img name="img_STATUS" id="img_STATUS" src="/js/FVS.gif" width="1" height="1" alt="" />
                                    </td>
                                    <td valign="middle" style="padding-top: 10px; padding-right: 15px; margin-left: 0px;">
                                        <%=selstatus %>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" style="padding-top: 10px;">
                                        Type :
                                        <img name="img_TYPE" id="img_TYPE" src="/js/FVS.gif" width="1" height="1" alt="" />
                                    </td>
                                    <td valign="middle" style="padding-top: 10px;">
                                        <%=selType %>
                                    </td>
                                    <td valign="middle" style="padding-top: 10px; padding-right: 0px;">
                                        Employee :
                                        <img name="img_EMPLOYEE" id="img_EMPLOYEE" src="/js/FVS.gif" width="1" height="1"
                                            alt="" />
                                    </td>
                                    <td valign="middle" style="padding-top: 10px; padding-right: 15px; margin-left: 0px;">
                                        <%=selEmployee %>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" style="padding-top: 10px;">
                                        Grade:<img name="img_GRADE" id="img_GRADE" src="/js/FVS.gif" width="1" height="1"
                                            alt="" />
                                    </td>
                                    <td valign="middle" style="padding-top: 10px;">
                                        <%=selGrade %>
                                    </td>
                                    <td valign="middle" style="padding-top: 10px;">
                                        Category:<img name="img_CATEGORY" id="img_CATEGORY" src="/js/FVS.gif" width="1" height="1"
                                            alt="" />
                                    </td>
                                    <td valign="middle" style="padding-top: 10px;">
                                        <div id="dvCategory">
                                            <select name="sel_CATEGORY" id="sel_CATEGORY" class="textbox200" style="width: 200px">
                                                <option value=''>Please select Category</option>
                                            </select>
                                        </div>
                                    </td>
                                    <!--<td></td>-->
                                </tr>
                                <tr>
                                    <td valign="middle">
                                        From:
                                    </td>
                                    <td valign="middle">
                                        <input type="text" name="txt_FromDate" id="txt_FromDate" value="<%=rqFromDate%>"
                                            class="textbox" maxlength="10" style="width: 140px; text-align: right" />
                                        <img name="img_FromDate" id="img_FromDate" src="/js/FVS.gif" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td valign="middle">
                                        To:
                                    </td>
                                    <td valign="middle">
                                        <input type="text" name="txt_ToDate" id="txt_ToDate" value="<%=rqToDate%>" class="textbox"
                                            maxlength="10" style="width: 200px; text-align: right" />
                                        <img name="img_ToDate" id="img_ToDate" src="/js/FVS.gif" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td style="padding-top: 10px; vertical-align: top; text-align: center;" colspan="2">
                                        <span style="padding-right: 15px; padding-left: 10px;">
                                            <input id="Button1" type="button" value="Apply Filter" title="Apply Filter" tabindex="6"
                                                class="RSLButton" onclick="GO()" style="cursor: pointer" />
                                        </span><span>
                                            <input id="Button2" type="button" value="  Export  " title="  Export  " tabindex="6"
                                                class="RSLButton" onclick="XLSMe()" style="cursor: pointer" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            &nbsp;<br />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" height="1">
                            <img src="images/spacer.gif" width="1" height="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' width="83">
                    <b>Created</b>
                </td>
                <td class='NO-BORDER' width="100">
                    &nbsp;<strong>Title</strong>
                </td>
                <td class='NO-BORDER' width="105">
                    <b>Customer Name</b>
                </td>
                <td class='NO-BORDER' width="116">
                    <b>Address</b>
                </td>
                <td class='NO-BORDER' width="90" align="left">
                    <b>Type</b>
                </td>
                <td width="35" class='NO-BORDER'>
                    <strong>Grade</strong>
                </td>
                <td width="100" class='NO-BORDER'>
                    <strong>Category</strong>
                </td>
                <td width="70" class='NO-BORDER'>
                    <strong>Employee</strong>
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="10" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4" height="1"
        style="display: none"></iframe>
</body>
</html>
