<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
ErrorID = Request("ErrorID")
if (ErrorID = "") then ErrorID = -1 end if

OpenDB()

SQL = "SELECT * FROM G_ERRORMESSAGES " &_
		"WHERE ERROR_ID = " & ErrorID 

Call OpenRs(rsName, SQL)
If (NOT rsName.EOF) Then
	l_time = rsName("ER_TIME")
	l_2= rsName("ER_SOURCE")
	l_3 = rsName("ER_NUMBER")
	l_4 = rsName("ER_PAGE")
	l_5 = rsName("ER_DESC")
	l_6 = rsName("ER_CODE")
	l_7 = rsName("ER_LINE")						
	l_8 = rsName("ER_REMOTE_ADDR")						
	l_9 = rsName("ER_REMOTE_HOST")								
	l_10 = rsName("ER_LOCAL_ADDR")
	l_11 = rsName("LUMP_CODE")
	l_12 = rsName("ACTION_TAKEN")	
	l_13 = rsName("STAT")		
End If
CloseRS(rsName)

CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Errors</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function SaveForm(){
	RSLFORM.submit()
	}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
        <table width=750 border=0 cellpadding=0 cellspacing=0>
          <tr> 
            <td bgcolor=#133E71> <img src="images/spacer.gif" width=53 height=1></td>
          </tr>
          <tr> 
            <td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> 
              <img src="images/spacer.gif" width=53 height=6></td>
          </tr>
        </table>
        
<TABLE cellspacing=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="90%">
  <form name="RSLFORM" method="POST" ACTION="ServerSide/Error_svr.asp">
    <TR> 
      <TD nowrap valign=top>Error ID:</td>
      <td valign=top> 
		<%=ErrorID%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Time:</td>
      <td valign=top> 
		<%=l_time%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Number:</td>
      <td valign=top> 
		<%=l_3%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Source:</td>
      <td valign=top> 
		<%=l_2%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Page:</td>
      <td valign=top> 
		<%=l_4%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Description:</td>
      <td valign=top> 
		<%=l_5%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Code:</td>
      <td valign=top> 
		<%=l_6%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Line:</td>
      <td valign=top> 
		<%=l_7%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Remote Address:</td>
      <td valign=top> 
		<%=l_8%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Remote Host:</td>
      <td valign=top> 
		<%=l_9%>
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign=top>Error Local Address:</td>
      <td valign=top> 
		<%=l_10%>
      </TD>
    </TR>
     <TR> 
      <TD nowrap valign=top>Lump code:</td>
      <td valign=top> 
		<%=l_11%>
      </TD>
    </TR>   
	<TR> 
      <TD nowrap valign=top>Action Taken:</td>
      <td valign=top> 
		<TEXTAREA NAME="txt_ACTION_TAKEN" rows="5" cols=100 class="textbox"><%=l_12%></TEXTAREA>
      </TD>
    </TR>
	<%
	fchecked = ""
	ochecked = " checked"
	if (l_13 = 1) then
		fchecked = " checked"
		ochecked = ""
	end if
	%>
	<TR> 
      <TD nowrap valign=top>Status:</td>
      <td valign=top> 
		CLOSE <input type="radio" name="rdo_STAT" value="1" <%=fchecked%>>
		OPEN <input type="radio" name="rdo_STAT" value="0" <%=ochecked%>>		
      </TD>
    </TR>

    <TR> 
      <TD></TD>
      <TD align=right> 
        <input type=hidden name="hid_ERRORID" value="<%=ErrorID%>">
        <input type=button class="RSLButton" value=" BACK " onClick="history.back()">
        <input type=button class="RSLButton" value=" SAVE " onclick="SaveForm()">
      </TD>
    </TR>
    <TR height="100%"> 
      <TD height="100%">&nbsp;</TD>
    </TR>
  </FORM>
</TABLE>
        <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
