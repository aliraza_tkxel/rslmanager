<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<!--#include file="iFrames/iHB.asp" -->
<%


	CONST CONST_PAGESIZE = 15
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TotalArrearsCases
	Dim ArrearActionCount
	Dim TotalArrearsForHO			' for housing officer
	Dim TotalPercentofArrearsForHO  ' for housing officer
	Dim ochecked, cchecked, ES, GR
	Dim TotalArrears
	Dim orderBy
	Dim CompareValue
	Dim DateAs,MaxRowSpan
	Dim OVERALLCOUNT

	'Open database and create a dropdown compiled with the housing officers
	OpenDB()
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	SetVars()
	Function SetVars()
	
		If Request.QueryString("page") = "" Then
			intPage = 1	
		Else
			intPage = Request.QueryString("page")
		End If
		
		PageName = "ArrearsManagementReport.asp"
		MaxRowSpan = 9

		DateAs = Request("DateAs")
		if (not DateAs <> "") then
			DateAs = Date()
		else
			DateAs = "1/" & DateAs
			DateAs = dateAdd("m", 1 , DateAs)
		End If

	End Function
		
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()


		
SQL = " EXEC C_SATISFACTION_SURVEY_REPORT "
					Call OpenRs(rsRES, SQL)
		
						
					QUESTION_1_ANSWER1 = rsRES("Q1_A1")
					QUESTION_1_ANSWER2 = rsRES("Q1_A2")
					QUESTION_1_ANSWER3 = rsRES("Q1_A3")
					QUESTION_1_ANSWER4 = rsRES("Q1_A4")
					
					QUESTION_2_ANSWER1 = rsRES("Q2_A1")
					QUESTION_2_ANSWER2 = rsRES("Q2_A2")
					QUESTION_2_ANSWER3 = rsRES("Q2_A3")
					QUESTION_2_ANSWER4 = rsRES("Q2_A4")
					
					QUESTION_3_ANSWER1 = rsRES("Q3_A1")
					QUESTION_3_ANSWER2 = rsRES("Q3_A2")
					QUESTION_3_ANSWER3 = rsRES("Q3_A3")
					QUESTION_3_ANSWER4 = rsRES("Q3_A4")
					
					QUESTION_4_ANSWER1 = rsRES("Q4_A1")
					QUESTION_4_ANSWER2 = rsRES("Q4_A2")
					QUESTION_4_ANSWER3 = rsRES("Q4_A3")
					QUESTION_4_ANSWER4 = rsRES("Q4_A4")
					
					QUESTION_1 = rsRES("Q1")
					QUESTION_2 = rsRES("Q2")
					QUESTION_3 = rsRES("Q3")
					QUESTION_4 = rsRES("Q4")
					OVERALLCOUNT = rsRES("OVERALLCOUNT")
				Call CloseRs(rsRES)
					
			
				str_data = str_data & "<TR>" &_
										 " <TD WIDTH=420  bgcolor='#F5F5F5' >1. How easy was it to report this repair?</TD> " &_
										" <TD  bgcolor='#E0EEFE'>" & QUESTION_1 & " </TD> " &_
										 " <TD>" & FormatNumber(QUESTION_1_ANSWER1,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_1_ANSWER2,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_1_ANSWER3,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_1_ANSWER4,2) & " %</TD> " &_
										"</TR>"
				str_data = str_data & "<TR>" &_
										 " <TD WIDTH=420  bgcolor='#F5F5F5'>2. How helpful where the BHA staff you dealt with on this occasion?</TD> " &_
										 " <TD   bgcolor='#E0EEFE'>" & QUESTION_2 & " </TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_2_ANSWER1,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_2_ANSWER2,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_2_ANSWER3,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_2_ANSWER4,2) & " %</TD> " &_
										"</TR>"
				str_data = str_data & "<TR>" &_
										 " <TD WIDTH=420  bgcolor='#F5F5F5'>3. What did you think of the contractors?</TD> " &_
										" <TD   bgcolor='#E0EEFE'>" & QUESTION_3 & " </TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_3_ANSWER1,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_3_ANSWER2,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_3_ANSWER3,2) & " %</TD> " &_
										 " <TD  >" & FormatNumber(QUESTION_3_ANSWER4,2) & " %</TD> " &_
										"</TR>"					
				str_data = str_data & "<TR>" &_
										 " <TD WIDTH=420  bgcolor='#F5F5F5'>4. In your opinion, what is the standard of the repair? </TD> " &_
										 " <TD  bgcolor='#E0EEFE'>" & QUESTION_4 & " </TD> " &_
										 " <TD>" & FormatNumber(QUESTION_4_ANSWER1,2) & " %</TD> " &_
										 " <TD>" & FormatNumber(QUESTION_4_ANSWER2,2) & " %</TD> " &_
										 " <TD>" & FormatNumber(QUESTION_4_ANSWER3,2) & " %</TD> " &_
										 " <TD>" & FormatNumber(QUESTION_4_ANSWER4,2) & " %</TD> " &_
										"</TR>"
			'ensure table height is consistent with any amount of records
			fill_gaps()
		
	End function
	
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
		
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function GO(){
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>&WHICH=" + thisForm.WHICH.value
	}

function PrintMe(){
	window.showModalDialog("PrintMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>", "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
	}

function XLSMe(){
	window.open("XLSMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>");
	}
	
function GoReport(){

		var DateAs; 
		DateAs = thisForm.selMonth.value;
		location.href = "ArrearsManagementReport.asp?DateAs=" + DateAs;

	}
	
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <td colspan=2> <br>
        Surveys Issued : <b><%=OVERALLCOUNT%></b><br>
        <br>
      </td>
    </tr>
    <TR> 
      <TD ROWSPAN=2 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="Images/tab_surveyresults.gif" WIDTH=123 HEIGHT=20 BORDER=0></TD>
      <TD align=right>
		<SELECT NAME='selMonth' class='textbox' value="<%=DateAs%>" STYLE="DISPLAY:NONE">
              <%=lstBox%>
		</SELECT> 
	  <input name=" Go " type="button" class="RSLButton" value=" Go " onClick="GoReport()"  STYLE="DISPLAY:NONE">
      &nbsp; </TD>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71><IMG SRC="../Customer/images/spacer.gif" WIDTH=627 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD WIDTH=420 CLASS='NO-BORDER'><strong>Question</strong></TD>
	   <TD WIDTH=56 align="left" CLASS='NO-BORDER'><div ><strong>Replies</strong></div></TD>
      <TD WIDTH=60 align="left" CLASS='NO-BORDER'><div ><strong>Good</strong></div></TD>
      <TD WIDTH=60 align="left" CLASS='NO-BORDER'><div ><strong>Average</strong></div></TD>
      <TD WIDTH=60 align="left" CLASS='NO-BORDER'><div ><strong>Poor</strong></div></TD>
	   <TD WIDTH=60 align="left" CLASS='NO-BORDER'><div ><strong>Unanswered</strong></div></TD>
    </TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=6 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>	