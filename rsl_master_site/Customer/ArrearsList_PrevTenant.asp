<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal
	Dim ochecked, cchecked
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Catch Housing Officer ID from dropdown
		theUser = Request("WHICH")  								
		If theUser <> "" then
			If NOT theUser = "All" then 
				theUser = cInt(theUser)
			Else
				theUser = theUser
			End If
		Else
			' This will be the initial value of the dropdown on loading of page 
			theUser = "All"
		End If
		
		'build the scheme and action sql 
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to
		rqAction 	= Request("SEL_ACTION")	' Action last Taken
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Build and set filter boxes and dropdowns situated at the top of the page	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Open database and create a dropdown compiled with the housing officers
		OpenDB()
		
		Call BuildSelect(selAction, "sel_ACTION", "C_LETTERACTION WHERE ACTIONID NOT IN (13,15,19,22,26,29,41,57,62,70,81) ", "ACTIONID, DESCRIPTION", "DESCRIPTION", "All", rqAction, NULL, "textbox200", "onchange='' TABINDEX=3")
		Call BuildSelect(selScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME ", "SCHEMENAME", "All", rqScheme, NULL, "textbox200", "onchange='' TABINDEX=3")
		
		'SQL statement to show housing officers that are assigned to a property that have arreas
		SQL = 	" SELECT E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME " &_
				" FROM E__EMPLOYEE E " &_
				" 	INNER JOIN (SELECT DISTINCT P.HOUSINGOFFICER  " &_
				"				FROM P__PROPERTY P " &_
				" 					LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
				" 				WHERE E.EMPLOYEEID = P.HOUSINGOFFICER) A ON  E.EMPLOYEEID = A.HOUSINGOFFICER " 	
	    Call OpenRs(rsHO, SQL)
		
			Sel_HousingOfficer = "<select name='WHICH' class='textbox200' onchange='' >"
			Dim isSelected
			
			While (NOT rsHO.EOF)
			
			   ' Set option to be selected in the housing officers dropdown
			  IF theUser = rsHO.Fields.Item("EMPLOYEEID").Value then 
				 isSelected = "selected"
			  end if
			  
			  Sel_HousingOfficer = Sel_HousingOfficer & "<option value='" & (rsHO.Fields.Item("EMPLOYEEID").Value) & "' " & IsSelected & " >"  & (rsHO.Fields.Item("FULLNAME").Value) & "</option>"	  
			  isSelected = ""
	
			  rsHO.MoveNext()
			Wend
			
			If (rsHO.CursorType > 0) Then
				rsHO.MoveFirst
			Else
				rsHO.Requery
			End If
				
			Sel_HousingOfficer = Sel_HousingOfficer & "<option value='All' "
			
			if theUser = "All" then 
				 Sel_HousingOfficer = Sel_HousingOfficer & "selected"
			End If
			
			Sel_HousingOfficer = Sel_HousingOfficer & ">All</option>"
			Sel_HousingOfficer = Sel_HousingOfficer & "</SELECT>"
		
		CloseRs(rsHO)
		
		CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Building of filter boxes and dropdowns 	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsList_PrevTenant.asp"
	MaxRowSpan = 8
	
	
	OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		Dim orderBy
		orderBy = " LASTPAYMENTDATE "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
		
		' What arrears value to compare against
		CompareValue = "300.00"
		if (Request("COMPAREVALUE") <> "") then
			CompareValue = Request("COMPAREVALUE")
		end if
	
	
		' This is the user id - which
		If NOT theUser = "All" then 
			ES = " AND P.HOUSINGOFFICER = " & cInt(theUser)	
		Else 
			ES = "" 
		End If
	
			
		' As at Date - shows the arrears value from the time of date requested
		If rqAsAtDate <> "" then
			AsAtDate = "AND I_J.TRANSACTIONDATE <= '" & rqAsAtDate & "' "
		Else
			AsAtDate = ""
		End If

			
		' The Scheme that the property applies to ( Also known as Development )
		if rqScheme <> "" then	
			Scheme_sql = " and SCH.SCHEMEID = " & rqScheme	
		else	
			Scheme_sql = " " 
		End If
			
			
		' The Action that was last taken ( Arrears actions )
		if rqAction <> "" then
			Action_sql = " and VJ.actionid = " & rqAction	
		else 
			Action_sql = " " 
		End If
	
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

				TotalSQL =		"	SELECT  " &_
							"		sum((ISNULL(A.AMOUNT,0) - ISNULL(ABS(B.AMOUNT),0))) AS TOTALARREARS    " &_
							"	FROM C_TENANCY T    " &_
							"		inner JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  "  & AsAtDate &_ 
							"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID   " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID   " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID   " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) " & AsAtDate &_
							"					AND PAYMENTTYPE = 1    " &_
							"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN (SELECT MAX(JOURNALID) AS JOURNALID, CUSTOMERID FROM C_JOURNAL where ITEMNATUREID = 10 GROUP BY CUSTOMERID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID   " &_
							"	WHERE ISNULL(A.AMOUNT,0)> 0 AND T.ENDDATE IS NOT NULL " &_
							"		  AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)   "
			'rw TotalSQL & "<BR><BR>"
			'Call OpenRs(rsTotal, TotalSQL)
			'TheArrearsTotal = FormatCurrency(rsTotal("TOTALARREARS"),2)
			'CloseRs(rsTotal)
					
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 
	
			intRecord = 0
			str_data = ""
			strSQL =		" SELECT  VJ.DESCRIPTION AS ACTIONDESC,   " &_
							"		VJ.JOURNALID,   " &_
							"		T.TENANCYID, T.STARTDATE,    " &_
							"		P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE,   " &_
							"		ISNULL(A.AMOUNT,0) AS GROSSARREARS, "&_ 
							"		(ISNULL(A.AMOUNT,0)) AS TOTALARREARS,    " &_
							"		( SELECT MAX(TRANSACTIONDATE) FROM F_RENTJOURNAL LEFT JOIN F_PAYMENTTYPE PY ON PY.PAYMENTTYPEID = PAYMENTTYPE WHERE TRANSACTIONDATE IS NOT NULL AND TENANCYID = T.TENANCYID AND STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND PY.RENTRECEIVED IS NOT NULL) AS LASTPAYMENTDATE    " &_
                            "		,CONVERT(NVARCHAR,T.ENDDATE,103) AS ENDDATE    " &_
							" FROM C_TENANCY T  "&_ 
							"	Cross Apply  (	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT,  "&_ 
							"				I_J.TENANCYID  "&_ 
							"			FROM F_RENTJOURNAL I_J  "&_ 
							"				LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID  "&_ 
							"				LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  "&_ 
							"				LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  "&_ 
							"			WHERE I_J.TENANCYID = T.TENANCYID AND I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  " & AsAtDate &_ 
							"			GROUP BY I_J.TENANCYID ) As A   "&_ 
							"	Outer Apply ( 	SELECT 	ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT,  "&_ 
							"				I_J.TENANCYID  "&_ 
							"			FROM F_RENTJOURNAL I_J  "&_ 
							"				LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID  "&_ 
							"				LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  "&_ 
							"				LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  "&_ 
							"				WHERE I_J.TENANCYID = T.TENANCYID AND I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) AND PAYMENTTYPE = 1  "&_ 
							"			GROUP BY I_J.TENANCYID ) AS B  "&_ 
							"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  "&_ 
							"	LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID  "&_ 
							"	LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID  "&_                            
							"	LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
                            "   INNER JOIN P_SCHEME SCH ON P.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
							"	LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID  "&_ 
							"	LEFT JOIN (	SELECT J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION, LA.ACTIONID "&_  
							"			FROM C_JOURNAL  J "&_ 
							"				INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID "&_ 
							"				INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID AND AR.ITEMACTIONID NOT IN (12,13) "&_ 
							"			WHERE 	J.JOURNALID = (SELECT MAX(JOURNALID) FROM C_JOURNAL WHERE CUSTOMERID = J.CUSTOMERID) "&_ 
							"				AND AR.ARREARSHISTORYID = ( select max(ARREARSHISTORYID) from C_ARREARS where JOURNALID = J.JOURNALID  ) " &_ 
							"			GROUP BY J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION,LA.ACTIONID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID  " &_					 
							" WHERE 	((ISNULL(A.AMOUNT,0))>=  "  & CompareValue & ") "&_ 
							"	AND T.ENDDATE IS NOT NULL  " & ES  & scheme_sql & action_sql &_
							"	AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)  "&_ 
							" order By " & orderBy

			'rw strSQL & "<BR>"
			'Response.Write(strSQL)
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
					TenancyID = rsSet("TENANCYID")
					CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
							"FROM C_CUSTOMERTENANCY CT " &_
							"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
							"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
							"WHERE CT.ENDDATE IS NOT NULL AND CT.TENANCYID = " & TenancyID
							
					Call OpenRs(rsCust, CustSQL)
					strUsers = ""
					CustCount = 0
					while NOT rsCust.EOF 
						if CustCount = 0 then
							strUsers = strUsers & "<A HREF='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
							CustCount = 1
						else
							strUsers = strUsers & ", " & "<A HREF='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
						end if					
						rsCust.moveNext()
					wend
					CloseRs(rsCust)
					SET rsCust = Nothing
														
					strStart = 	"<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
					strEnd = 	"<TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & ", " & rsSet("TOWNCITY") & "</TD>" &_
								"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_										
								"<TD align=right>" & FormatCurrency(rsSet("TOTALARREARS")) & "</TD>" &_										
								"<TD align=right>" & rsSet("ENDDATE") & "</TD></TR>"

	
					str_data = str_data & strStart & "<TD width=190>" & strUsers & "</TD>" & strEnd
										
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				str_data = str_data & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Arrears List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</head>
<script language="JavaScript" src="/js/preloader.js"></script>
<script language="JavaScript" src="/js/general.js"></script>
<script language="JavaScript" src="/js/menu.js"></script>
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript">
<!--
	function load_me(CustomerID){
		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

	var FormFields = new Array()
	FormFields[0] = "txt_VALUE|Compare Value|CURRENCY|Y"
	FormFields[1] = "txt_ASATDATE|Date - As At|DATE|N"

	function GO(){

		if (!checkForm()) return false;
		if (thisForm.txt_VALUE.value < 0 ){
			alert("The Min. Arrears value must not a negative number.");
			return false;
			}
		location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value +  "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value
		}
		

	/////////////////////////////////
	//	Printing and Xls Below
	/////////////////////////////////
	
	function PrintMe(){
	
		window.open("PrintMe_PrevTenant.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
		
		}
	
	function XLSMe(){
		//alert("unavailable at this time")
		//return false
		window.open("XLSMe_PrevTenant.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value +  "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value);
		}
	
// -->
</script>
<%=WriteJavaJumpFunction%>
<!-- End Preload Script -->
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages()" onunload="macGo()"
    marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get">
    <table border="0" width="750" cellpadding="2" cellspacing="2" height="40PX" style='border: 1PX SOLID #133E71'
        bgcolor="beige">
        <tr>
            <td nowrap width="22%">
                <b>Min. Arrears Value:</b>
            </td>
            <td width="26%">
                <b>Housing Officer:</b>
            </td>
            <td width="40%">
                &nbsp;
            </td>
            <td width="12%">
                Xls Version
            </td>
        </tr>
        <tr>
            <td nowrap width="22%">
                <input type="text" id="txt_VALUE" name="txt_VALUE" value="<%=CompareValue%>" class="textbox" maxlength="10"
                    style='width: 100px; text-align: right'>
                <img name="img_VALUE" src="/js/FVS.gif" width="15" height="15">
                <img src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0">
            </td>
            <td nowrap width="26%">
                <%=Sel_HousingOfficer%><img src="/js/FVS.gif" name="img_TO" width="15px" height="15px"
                    border="0">
                <td nowrap width="40%">
                    &nbsp;
                </td>
                <td width="12%" nowrap>
                    <img src="Images/excel.gif" style="cursor: hand" alt="Excel Arrears List" width="25"
                        height="25" onclick="XLSMe()">
                    <span class="style1">* new </span>
                </td>
        </tr>
        <tr>
            <td nowrap width="22%" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="58%" valign="middle">
                            <b>Date</b> ( As At )
                        </td>
                        <td width="42%">
                            <img src="../myImages/info.gif" width="15" height="16" title="As At Date: represents all dates pre the date you set here."
                                style="cursor: hand">
                        </td>
                    </tr>
                </table>
            </td>
            <td nowrap width="26%">
                <b>Scheme: </b>
                <td nowrap width="40%">
                    &nbsp;
                </td>
                <td width="12%" nowrap>
                    Print version
                </td>
        </tr>
        <tr>
            <td nowrap width="22%" valign="bottom">
                <input type="text" id="txt_ASATDATE" name="txt_ASATDATE" value="<%=rqAsAtDate%>" class="textbox" maxlength="10"
                    style='width: 100px; text-align: right'>
                <img name="img_ASATDATE" src="/js/FVS.gif" width="15" height="15">
            </td>
            <td nowrap width="26%">
                <%=SelScheme%>
                <td nowrap width="40%">
                    <input type="button" value=" GO " class="RSLButton" onclick="GO()" name="button">
                    &nbsp;
                </td>
                <td width="12%" nowrap>
                    <img src="../myImages/PrinterIcon.gif" width="31" height="20" onclick="PrintMe()"
                        style="cursor: hand" alt="Print Arrears List">
                </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="3" valign="bottom">
                <img name="tab_Tenants" title="Tenants" src="Images/tab_arrears.gif" width="78" height="20"
                    border="0">
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td height="1" valign="bottom">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td height="1" valign="bottom">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71" height="1">
                            <img src="images/spacer.gif" width="672" height="1">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor="" hlcolor="STEELBLUE" border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' width="70PX">
                    &nbsp;<b>Tenancy</b>
                </td>
                <td class='NO-BORDER' width="150PX">
                    &nbsp;<b>Customer</b>&nbsp;
                </td>
                <td class='NO-BORDER' width="200PX">
                    &nbsp;<b>Address</b>&nbsp;
                </td>
                <td class='NO-BORDER' width="70PX" align="center">
                    <b>Last Payment</b>
                </td>
                <td class='NO-BORDER' width="80PX">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TOTALARREARS asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16"></a>&nbsp;<b>Value</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TOTALARREARS desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12"></a>
                </td>
                <td class='NO-BORDER' width="85PX"  align="center">
                <b>End Date</b>
                </td>
            </tr>
        </thead>
        <tr style='height: 3PX'>
            <td colspan="7" align="CENTER" style='border-bottom: 2PX SOLID #133E71'>
            </td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
