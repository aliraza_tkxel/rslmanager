<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim CONST_PAGESIZE
        CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim TotalSum, TotalCount, clist, csql, ccnt, ctotal, view
	Dim GENERAL_REQUEST_STRING

	GENERAL_REQUEST_STRING = "TENANT="&REQUEST("TENANT")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&GAP="&REQUEST("GAP")&""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

'	SET ORDER BY
	Dim ORDERBY
	    ORDERBY = " J.CREATIONDATE "

	If (Request("CC_Sort") <> "") Then ORDERBY = Request("CC_Sort") End If
	If Request("TENANT") <> "" Then SRCH_TENANT = Request("TENANT") Else SRCH_TENANT = "" End If
	If Request("GAP") <> "" Then SRCH_GAP = Request("GAP") Else SRCH_GAP = 0 End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If

    Call OpenDB()
	'Call SetSort()
	Call getData()
	Call CloseDB()

	Function getData()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""

        SQLCODE = " EXEC F_BADDEBTREPORT " & SRCH_GAP & ", '" & SRCH_TENANT & "'"
		set rsSet = Server.CreateObject("ADODB.Recordset")
		    rsSet.ActiveConnection = RSL_CONNECTION_STRING
		    rsSet.Source = SQLCODE
		    rsSet.CursorType = 3
		    rsSet.CursorLocation = 3
		    rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class=""CAPS"">"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

					str_data = str_data &_
						"<tr>" &_
						"<td width=""75px"" nowrap=""nowrap"" align=""center"">" & rsSet("EARLIESTBADEBT") & "</td>" &_
						"<td width=""185px"" nowrap=""nowrap"" style=""cursor:pointer"" title=""Go to customer"" onclick='location.href=""CRM.asp?CUSTOMERID=" & rsSet("CID") & " ""'><b>" & rsSet("CNAME") & "</b></td>" &_
						"<td width=""145px"" nowrap=""nowrap"">" & rsSet("ADDRESS1") & "</td>" & _
						"<td width=""95px"" nowrap=""nowrap"" align=""center"">" & FormatCurrency(rsSet("BADDEBT")) & "</td>" &_
						"<td width=""125px"" nowrap=""nowrap"" align=""center"">" & rsSet("DATELASTPAYMENT") & "</td>"  &_
						"<td width=""125px"" nowrap=""nowrap"" align=""center"">" & FormatCurrency(abs(rsSet("AMOUNT"))) & "</td></tr>" 
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next

			str_data = str_data & "</tbody>"
			
			'ensure table height is consistent with any amount of records
			Call fill_gaps()
			
			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""6"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a style=""cursor:pointer"" onclick=""gotopage(1)""><b><font color=""blue"">First</font></b></a> " &_
			"<a style=""cursor:pointer"" onclick=""gotopage(" & prevpage & ")""><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a style=""cursor:pointer"" onclick=""gotopage(" & nextpage & ")""><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a style=""cursor:pointer"" onclick=""gotopage(" & intPageCount & ")""><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""6"" align=""center"">No Bad Debts found in the system for the specified criteria!!!</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
			Call fill_gaps()
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""6"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function SetSort()
		If (Request("CC_Sort") <> "") Then 
			PotentialOrderBy = Request("CC_Sort")
			For i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy Then
					orderBy = PotentialOrderBy
					Exit For
				End If
				If SortDESC(i) = PotentialOrderBy Then
					orderBy = PotentialOrderBy
					Exit For
				End If
			Next
		End If
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer --> Service Complaints List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        function toggle(what) {
            what = new String(what)
            var coll = document.getElementsByName(what);
            if (coll != null)
                for (i = 0; i < coll.length; i++)
                    if (coll[i].style.display == "none")
                        coll[i].style.display = "block";
                    else
                        coll[i].style.display = "none";
        }

        function gotopage(pageno) {
            location.href = "baddebtreport.asp?CC_Sort=<%=ORDERBY%>&page=" + pageno + "&TENANT=" + document.getElementById("txt_TENANT").value + "&GAP=" + document.getElementById("txt_GAP").value + "&PAGESIZE=" + document.getElementById("txt_PAGESIZE").value;
        }

        // OPEN OR CLOSE CREDIT NOTE ROWS
        function toggle(what) {
            what = new String(what)
            var coll = document.getElementsByName(what);
            if (coll != null)
                for (i = 0; i < coll.length; i++)
                    if (coll[i].style.display == "none")
                        coll[i].style.display = "block";
                    else
                        coll[i].style.display = "none";
        }

        var FormFields = new Array();
        FormFields[0] = "txt_TENANT|Tenant|TEXT|N"
        FormFields[1] = "txt_PAGESIZE|Page Size|INTEGER|Y"
        FormFields[2] = "txt_GAP|Payment Age|INTEGER|Y"

        // fires when proceed button is clicked
        function click_go() {
            if (!checkForm()) return false;
            location.href = "baddebtreport.asp?TENANT=" + document.getElementById("txt_TENANT").value + "&page=1&GAP=" + document.getElementById("txt_GAP").value + "&PAGESIZE=" + document.getElementById("txt_PAGESIZE").value;
        }

        function open_MPI() {
            window.open("Popups/pMPIServiceComplaints.asp", "display", "width=850,height=550,left=50,top=50");
            //window.showModalDialog("popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
        }

        function openHelp() {
            window.open("popups/baddebtreporthelp.asp", "", "height=473,width=600");
        }

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <br />
    <table width="750" cellpadding="2" cellspacing="0" style="border: 1px solid black;
        border-collapse: collapse; behavior: url(/Includes/Tables/tablehl.htc)" slcolor=''
        hlcolor="STEELBLUE" border="1">
        <thead>
            <tr>
                <td colspan="4">
                    <input type="button" tabindex="1" name="BTN_HELP" id="BTN_HELP" value="HELP" class="rslbutton"
                        onclick="openHelp()" style="cursor:pointer" />
                    <input type="button" tabindex="2" name="BTN_MPI" id="BTN_MPI" value="MPI" class="rslbutton" onclick="open_MPI()" style="cursor:pointer" />
                    <span title='Enter any keyword/reference code to search for'><b>Tenant
                        <input type="text" class="textbox" name="txt_TENANT" id="txt_TENANT" maxlength="12" size="20" tabindex="1"
                            value="<%=Request("TENANT")%>" />
                        <img src="/js/FVS.gif" name="img_TENANT" id="img_TENANT" width="15px" height="15px" border="0" alt="" />
                    </b></span>&nbsp;<b>Payment Age</b>
                    <input type="text" class="textbox" name="txt_GAP" id="txt_GAP" maxlength="12" size="20" tabindex="2"
                        value="<%=Request("GAP")%>" />
                    <img src="/js/FVS.gif" name="img_GAP" id="img_GAP" width="15px" height="15px" border="0" alt="" />
                </td>
                <td colspan="2" align="right">
                    Pagesize&nbsp;
                    <input type="text" class="textbox" name="txt_PAGESIZE" id="txt_PAGESIZE" maxlength="3" size="5" tabindex="3"
                        value="<%=CONST_PAGESIZE%>" />
                    <img src="/js/FVS.gif" name="img_PAGESIZE" id="img_PAGESIZE" width="15px" height="15px" border="0" alt="" />
			&nbsp;&nbsp;	
			<input type="button" tabindex="4" id="BTN_GO" name="BTN_GO" value="Proceed" class="rslbutton" onclick="click_go()" style="cursor:pointer" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" class='TABLE_HEAD' width="75px" align="center">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;B
                    Date&nbsp;<a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                <td nowrap="nowrap" class='TABLE_HEAD' width="185px">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=C.LASTNAME" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Tenant(s)&nbsp;<a
                            href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=C.LASTNAME DESC" style="text-decoration: none"><img
                                src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending" /></a>
                </td>
                <td nowrap="nowrap" class='TABLE_HEAD' width="145px">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ESCALATIONID" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Address&nbsp;<a
                            href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ESCALATIONID DESC"
                            style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                                alt="Sort Descending" /></a>
                </td>
                <td nowrap="nowrap" class='TABLE_HEAD' width="95px" align="center">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=E.LASTNAME" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;BDW&nbsp;<a
                            href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=E.LASTNAME DESC" style="text-decoration: none"><img
                                src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending" /></a>
                </td>
                <td nowrap="nowrap" class='TABLE_HEAD' width="125px" align="center">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;P
                    Date&nbsp;<a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                <td nowrap="nowrap" class='TABLE_HEAD' width="125px" align="center">
                    <a href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Amount&nbsp;<a
                            href="baddebtreport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT DESC"
                            style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                                alt="Sort Descending" /></a>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="6" align="center" style="border-bottom: 2px solid #133e71" class="TABLE_HEAD">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    <input type="hidden" name="hid_CreditSum" id="hid_CreditSum" />
    <input type="hidden" name="hid_CreditCount" id="hid_CreditCount" />
    <input type="hidden" name="hid_CreditList" id="hid_CreditList" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
