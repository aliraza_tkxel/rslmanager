<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<!--#include file="iFrames/iHB.asp" -->
<%


	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TotalArrearsCases
	Dim ArrearActionCount
	Dim TotalArrearsForHO			' for housing officer
	Dim TotalPercentofArrearsForHO  ' for housing officer
	Dim ochecked, cchecked, ES, GR
	Dim TotalArrears
	Dim orderBy
	Dim CompareValue
	Dim DateAs,MaxRowSpan
	Dim TotalRow
	Dim Patch
	Dim DevelopmentId,DEVELOPMENTSELECT

	'Open database and create a dropdown compiled with the housing officers
	OpenDB()
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	SetVars()
	Function SetVars()
	
		If Request.QueryString("page") = "" Then
			intPage = 1	
		Else
			intPage = Request.QueryString("page")
		End If
		
		PageName = "ArrearsManagementReport.asp"
		MaxRowSpan = 10

		'DateAs = Request("DateAs")
		'if (not DateAs <> "") then
		'	DateAs = Date()
		'else
		'	DateAs = "1/" & DateAs
		'	DateAs = dateAdd("m", 1 , DateAs)
		'End If
		
		If(Request.Item("sel_PATCHID")<>"") then
		    Patch=Request.Item("sel_PATCHID")
		    DEVELOPMENTSELECT=" WHERE PATCHID=" & Request.Item("sel_PATCHID")  
		else
		    Patch=null
		    DEVELOPMENTSELECT=""
		end if
		
		If(Request.Item("sel_Development")<>"") then
		    DevelopmentId=Request.Item("sel_Development")
		else
		    DevelopmentId=null
		end if
		

	End Function
		
	get_newtenants()
	Call BuildSelect(lstPatch, "sel_PATCHID", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "Please Select", Patch, NULL, "textbox100", " TABINDEX=11 onChange=Filter()")	
	Call BuildSelect(lstDEVELOPEMNT, "sel_Development", "PDR_DEVELOPMENT" & DEVELOPMENTSELECT, "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTID", "Please Select", DevelopmentId,null, "textbox200", "STYLE='WIDTH:240PX'" )
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

		strSQL = "execute ARREARS_GET_MANAGEMENT_REPORT"
		
		if (Patch<>"") then
		  strSQL= strSQL & " @PATCH=" & Patch
		end if    
		
		if(DevelopmentId<>"" and Patch<>"") then
		    strSQL= strSQL & ",@DEVELOPMENTID=" & DevelopmentId
		elseif(DevelopmentId<>"") then
		    strSQL= strSQL & "@DEVELOPMENTID=" & DevelopmentId    
		end if
			
			HB = 0
			VAL = 0
			GR = 0
			PAY = 0
			ARR_CASE = 0 
			RENT = 0
			TOT = 0
			PROP_CNT = 0
			PROP_TEN = 0
			perc = 0

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
	
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
				strUsers = ""
				CustCount = 0
			PrevTenancy = ""
			For intRecord = 1 to rsSet.PageSize
			'HOTotalArrears = rsSet("TOTALARREARS")
			
			
			TotalPercentofArrearsForHO = (rsSet("GROSSARREARS")/rsSet("grand_gross_total") * 100)
			HB = HB + rsSet("ESTHB")
			ADVHB = ADVHB + rsSet("ADVHB")
			NET = NET + rsSet("NETARREARS")
			GR = GR + rsSet("GROSSARREARS")
			PAY = PAY + rsSet("PAYMENT_AGREEMENTS") 
			ARR_CASE = ARR_CASE + rsSet("ARREARS_CASES") 
			RENT = RENT + rsSet("TOTALRENT")
			TotalPercentofArrearsForHO2 = TotalPercentofArrearsForHO2 + TotalPercentofArrearsForHO
			TOT = TOT + rsSet("GROSSARREARS")
			PROP_CNT = PROP_CNT + rsSet("PROPERTYCOUNT")
			PROP_TEN = PROP_TEN +  rsSet("PROPERTY_TENANCIES")
			perc = perc + TotalPercentofArrearsForHO

				str_data = str_data & "<TR><TD>" & rsSet("HO") & "</TD>" &_
					"<TD>&nbsp;" & rsSet("PROPERTYCOUNT") & " (" &  rsSet("PROPERTY_TENANCIES")  & ") " & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(rsSet("TOTALRENT"),2) & "</TD>" &_
					"<TD>&nbsp;" & rsSet("ARREARS_CASES") & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(rsSet("GROSSARREARS"),2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(rsSet("ESTHB"),2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(rsSet("ADVHB"),2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(rsSet("NETARREARS"),2) & "</TD>" &_
					"<TD>&nbsp; **</TD>" &_
					"<TD>&nbsp;	" & FormatNumber(TotalPercentofArrearsForHO,2) & "</TD>" &_
					"</TR>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				' takin this out as its a wrong figure
				'" & rsSet("PAYMENT_AGREEMENTS") & "
			Next
			
			' LETS ADD ON A TOTAL  ROW
			TotalRow = TotalRow & "<TR bgcolor='#99CC99'><TD><strong>TOTALS</strong></TD>" &_
					"<TD>&nbsp;" & PROP_CNT & " (" &  PROP_TEN  & ") " & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(RENT,2) & "</TD>" &_
					"<TD>&nbsp;" & ARR_CASE & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(GR,2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(HB,2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(ADVHB,2) & "</TD>" &_
					"<TD>&nbsp;" & FormatCurrency(NET,2) & "</TD>" &_
					"<TD>&nbsp;" & PAY & "</TD>" &_
					"<TD>&nbsp;" & perc & "</TD>" &_
					"</TR>"
			
			str_data = str_data & "</TBODY>" 
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			str_data = str_data & TotalRow
			' JIMMY HIS THIS NEXT PAGE FUNCTION SO THAT WE CAN HAVE PROPER TOTALS WITHOUT NEW REC SETS
			str_data_JIM_HIDE = str_data_JIM_HIDE &_
			"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
			fill_gaps()						
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	
		
	End function
	

	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	opendb()
	
	Call build_fiscal_month_select(lstBox, month(date()))
	closedb()
	Function build_fiscal_month_select(ByRef lst, selectedMonth)
	
		SQL = "SELECT * FROM F_FISCALYEARS WHERE YSTART <= '" & date & "' AND YEND >= '" & date & "'"
		selectedMonth =  month(DateAs)
		Call OpenRs(rsPeriod, SQL)
		PS = CDate(FormatDateTime(rsPeriod("YSTART"),1))
		PE = CDate(FormatDateTime(rsPeriod("YEND"),1))
		Call CloseRs(rsPeriod)

		YearStart = Year(PS)
		YearEnd = Year(PE)
		MonthStart = Month(PS)
		MonthEnd = Month(PE)
		
		'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
		MonthC = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
		lst = "<OPTION VALUE=''>YTD</OPTION>" 
		for i=1 to MonthC
			If not MonthStart > Month(date) then 
				If cint(MonthStart) = cint(selectedMonth) then isSelected = "selected" Else isSelected = "" End If
				lst = lst & "<OPTION VALUE=""" & MonthStart & "/" & YearStart & """ "&isSelected&">" & MonthName(MonthStart) & " " & YearStart & "</OPTION>"
				MonthStart = MonthStart + 1
				if (MonthStart = 13) then
					MonthStart = 1
				end if
			end if
		next
	
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function GO(){
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>&WHICH=" + thisForm.WHICH.value
	}

function PrintMe(){
	window.showModalDialog("PrintMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>", "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
	}

function XLSMe(){
	window.open("XLSMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>");
	}
	
function GoReport(){

		var DateAs; 
		DateAs = thisForm.selMonth.value;
		location.href = "ArrearsManagementReport.asp?DateAs=" + DateAs;

	}
function Filter()
{
    //var patch,developmentid
    
   // patch=thisForm.sel_PATCHID.value;
    
    //developmentid=thisForm.sel_Development.value;
    
    thisForm.submit();
    
}	
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=post>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <td colspan=5> This report returns the 'Housing Officers' who are allocated 
        to arrears.&nbsp;<br>
        <span class="style1">** Currently unavailable </span><br>
        <br>
      </td>
    </tr>
    <TR> 
      <TD ROWSPAN=2 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="../Customer/Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD nowrap=nowrap style="padding-bottom:10px;padding-left:40px;margin-right:0px;padding-right:4px;">Patch Name:</TD>
      <TD style="padding-bottom:10px;padding-left:0px;padding-right:5px;margin-left:0px;"><%=lstPatch%></TD>
      <TD  nowrap=nowrap style="padding-bottom:10px;padding-right:0px;margin-right:0px">Scheme Name:</TD>
      <TD style="padding-bottom:10px;margin-right:0px;padding-right:0px;"><%=lstDEVELOPEMNT%></TD>
      <td style="padding-bottom:10px;padding-left:0px;margin-left:0px;"><input type="button" class="RSLButtonSmall" value="GO" id="go" onclick="Filter();" /></td>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71 colspan=5><IMG SRC="../Customer/images/spacer.gif" WIDTH=689 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=769 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD WIDTH=118 CLASS='NO-BORDER'><b>Rent Recovery Officer</b></TD>
      <TD width="85" CLASS='NO-BORDER'><strong>Properties (Tenancy)</strong></TD>
      <TD WIDTH=73 CLASS='NO-BORDER'><strong>Total Rent </strong></TD>
      <TD WIDTH=51 CLASS='NO-BORDER'><B>Arrears Cases</B>&nbsp;</TD>
      <TD WIDTH=90 CLASS='NO-BORDER'><strong>Gross</strong></TD>
      <TD WIDTH=90 CLASS='NO-BORDER'><strong>Ant HB </strong></TD>
      <TD WIDTH=90 CLASS='NO-BORDER'><strong>Adv. HB </strong></TD>
	   <TD WIDTH=90 CLASS='NO-BORDER'><strong>Net</strong></TD>
	   <TD CLASS='NO-BORDER' WIDTH=82><b>Payment Agreements</b></TD>
	   <TD WIDTH=109 CLASS='NO-BORDER'><strong>&nbsp;% of Total Arrears</strong></TD>
    </TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=11 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>	