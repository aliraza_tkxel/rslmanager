<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_ReasonCategory, lst, int_lst_record, ValidateToogle

    Dim mysql
        mysql = ""

    reason_id = Request("reasonid")
	If reason_id = "" Then reason_id = 0 End If
        mysql = "SELECT TRA.TERMINATION_REASON_ACTIVITYID, TRA.DESCRIPTION " &_
            "FROM C_TERMINATION_REASON TR " &_
            "INNER JOIN C_TERMINATION_REASON_TOACTIVITY TR2A ON TR.REASONID = TR2A.REASONID " &_
            "INNER JOIN C_TERMINATION_REASON_ACTIVITY TRA ON TR2A.TERMINATION_REASON_ACTIVITYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
            "WHERE TR.REASONID = " & reason_id & " " &_
            "ORDER BY TRA.DESCRIPTION"

	Call OpenDB()
	Call build_ActivitySelect(str_ReasonCategory, mysql, null)
	Call CloseDB()

    Function build_ActivitySelect(ByRef lst, ftn_sql, isSelected)

    If ftn_sql = "" Then
        Exit Function
    End If

		Call OpenRs(rsSet, ftn_sql)
        If rsSet.EOF Then
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>No options available</option>"
            lst = lst & "</select>"
        Else
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>Please select</option>"
            int_lst_record = 0 
            If NOT isNull(isSelected) Then
			    While (NOT rsSet.EOF)
				    lstid = rsSet(0)
				    optionSelected = ""
				    If (CStr(lstid) = CStr(isSelected)) Then
					    optionSelected = " selected"
				    End If
				    lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				    rsSet.MoveNext()
                    int_lst_record = int_lst_record + 1
			    Wend
		    Else
			    While (NOT rsSet.EOF)
				    lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				    rsSet.MoveNext()
                    int_lst_record = int_lst_record + 1
			    Wend
		    End If
            lst = lst & "</select>"
        End If
		Call CloseRs(rsSet)

        If int_lst_record = 0 Then ValidateToogle = "N" Else ValidateToogle = "Y" End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Termination Category Change</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvReasonCategory").innerHTML = document.getElementById("dvReasonCategory").innerHTML;
            parent.document.getElementById("hid_TVal").value = "<%=ValidateToogle%>";
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvReasonCategory">
        <%=str_ReasonCategory%>
    </div>
</body>
</html>
