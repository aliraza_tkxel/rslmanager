<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%

Dim ID
ReDim DataFields   (52)
ReDim DataTypes    (52)
ReDim ElementTypes (52)
ReDim FormValues   (52)
ReDim FormFields   (52)

UpdateID	  = "hid_CustomerID"
FormFields(0) = "txt_FIRSTNAME|TEXT"
FormFields(1) = "txt_OCCUPATION|TEXT"
FormFields(2) = "txt_LASTNAME|TEXT"
FormFields(3) = "txt_NINUMBER|TEXT"
FormFields(4) = "txt_DOB|DATE"
FormFields(5) = "sel_TITLE|NUMBER"
FormFields(6) = "sel_EMPLOYMENTSTATUS|NUMBER"
FormFields(7) = "sel_GENDER|TEXT"
FormFields(8) = "sel_MARITALSTATUS|NUMBER"
FormFields(9) = "sel_ETHNICORIGIN|NUMBER"
FormFields(10) = "txt_ORGFLAG|NUMBER"
FormFields(11) = "txt_TAKEHOMEPAY|NUMBER"
FormFields(12) = "sel_RELIGION|NUMBER"
FormFields(13) = "sel_SEXUALORIENTATION|NUMBER"
FormFields(14) = "hid_COMMUNICATION|TEXT"
FormFields(15) = "hid_DISABILITY|TEXT"
FormFields(16) = "txt_FIRSTLANGUAGE|TEXT"
FormFields(17) = "sel_PREFEREDCONTACT|NUMBER"
FormFields(18) = "txt_MIDDLENAME|TEXT"
FormFields(19) = "txt_TITLEOTHER|TEXT"

FormFields(20) = "txt_DISABILITYOTHER|TEXT"
FormFields(21) = "txt_COMMUNICATIONOTHER|TEXT"
FormFields(22) = "txt_SEXUALORIENTATIONOTHER|TEXT"
FormFields(23) = "txt_RELIGIONOTHER|TEXT"
FormFields(24) = "txt_ETHNICORIGINOTHER|TEXT"

FormFields(25) = "txt_OCCUPANTSADULTS|NUMBER"
FormFields(26) = "txt_OCCUPANTSCHILDREN|NUMBER"
FormFields(27) = "chk_CONSENT|NUMBER"

FormFields(28) = "sel_LOCALAUTHORITY|NUMBER"
FormFields(29) = "hid_INTERNETACCESS|TEXT"
FormFields(30) = "sel_RENTSTATEMENTTYPE|NUMBER"

FormFields(31) = "sel_WHEELCHAIR|NUMBER"
FormFields(32) = "sel_CARER|NUMBER"
FormFields(33) = "sel_OWNTRANSPORT|NUMBER"
FormFields(34) = "sel_NATIONALITY|NUMBER"
FormFields(35) = "txt_NATIONALITYOTHER|TEXT"
FormFields(36) = "txt_INTERNETACCESSOTHER|TEXT"
FormFields(37) = "hid_SUPPORTAGENCIES|TEXT"
FormFields(38) = "txt_SUPPORTAGENCIESOTHER|TEXT"
FormFields(39) = "sel_HOUSEINCOME|NUMBER"
FormFields(40) = "hid_BENEFIT|TEXT"
FormFields(41) = "hid_BANKFACILITY|TEXT"
FormFields(42) = "txt_BANKFACILITYOTHER|TEXT"
FormFields(43) = "txt_EMPADDRESS1|TEXT"
FormFields(44) = "txt_EMPADDRESS2|TEXT"
FormFields(45) = "txt_EMPLOYERNAME|TEXT"
FormFields(46) = "txt_EMPTOWNCITY|TEXT"
FormFields(47) = "txt_EMPPOSTCODE|TEXT"
FormFields(48) = "sel_TEXTYESNO|NUMBER"
FormFields(49) = "sel_EMAILYESNO|NUMBER"
FormFields(50) = "hid_LASTACTIONUSER|INT"
FormFields(51) = "hid_LASTACTIONTYPE|INT"
FormFields(52) = "txt_BENEFITOTHER|TEXT"

	' ONLY ON NEW CUSTOMER DO WE ALLOW THE USER TO CHANGE THE CUSTOMER TYPE
	If request("sel_CUSTOMERTYPE") <> "" Then

		ReDim  Preserve DataFields   (53)
		ReDim  Preserve DataTypes    (53)
		ReDim  Preserve ElementTypes (53)
		ReDim  Preserve FormValues   (53)
		ReDim  Preserve FormFields   (53)

		FormFields(53) = "sel_CUSTOMERTYPE|NUMBER"

	End If

Function NewRecord()

	Call MakeInsert(strSQL)
	SQL = "SET NOCOUNT ON INSERT INTO C__CUSTOMER " & strSQL & " SELECT SCOPE_IDENTITY() AS NewID SET NOCOUNT OFF"
	Call OpenRs (rsCustomer, SQL)
	ID = rsCustomer("NewID")
	Call CloseRs (rsCustomer)

	ADDRESSSQL = "INSERT INTO C_ADDRESS (CUSTOMERID, HOUSENUMBER, ADDRESS1, ADDRESS2, ADDRESS3, TOWNCITY, POSTCODE, COUNTY, ISDEFAULT, ADDRESSTYPE) "&_
					"VALUES (" & ID & ", "&_
					"'" & Replace(Request("txt_HOUSENUMBER"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_ADDRESS1"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_ADDRESS2"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_ADDRESS3"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_TOWNCITY"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_POSTCODE"), "'", "''") & "', " &_
					"'" & Replace(Request("txt_COUNTY"), "'", "''") & "', " &_
					"1, " &_
					"3) "

	conn.Execute (ADDRESSSQL)
	Call GO()

End Function

Function AmendRecord()

	ID = Request.Form(UpdateID)
   
	Call MakeUpdate(strSQL)
	SQL = "UPDATE C__CUSTOMER " & strSQL & ", LASTACTIONTIME = GETDATE()  WHERE CUSTOMERID = " & ID
	Conn.Execute SQL, recaffected

    Dim CustomerType ,some
    SQL = "SELECT CUSTOMERTYPE " &_
			"FROM C__CUSTOMER C"&_
			"	LEFT JOIN C_ADDRESS A ON A.CUSTOMERID = C.CUSTOMERID " &_
			"WHERE C.CUSTOMERID = " & ID
      
	Call OpenRS(rsCustomer, SQL)
	If Not rsCustomer.eof Then
        CustomerType		=	rsCustomer("CUSTOMERTYPE")
    end if
    if CustomerType = 1 Then
        ADDRESSSQL = "UPDATE C_ADDRESS SET HOUSENUMBER = " &_
        "'" & Replace(Request("txt_HOUSENUMBER"), "'", "''") & "', " & " ADDRESS1 = " &_ 
					"'" & Replace(Request("txt_ADDRESS1"), "'", "''") & "', " &  " ADDRESS2 = " &_
					"'" & Replace(Request("txt_ADDRESS2"), "'", "''") & "', " &  " ADDRESS3 = " &_
					"'" & Replace(Request("txt_ADDRESS3"), "'", "''") & "', " &  " TOWNCITY = " &_
					"'" & Replace(Request("txt_TOWNCITY"), "'", "''") & "', " &  " POSTCODE = " &_
					"'" & Replace(Request("txt_POSTCODE"), "'", "''") & "', " &  " COUNTY = " &_
					"'" & Replace(Request("txt_COUNTY"), "'", "''") & "'  WHERE ADDRESSTYPE = 3 AND CUSTOMERID = " & ID

	    conn.Execute (ADDRESSSQL)
    end if
    
	Response.Redirect "../CRM.asp?CustomerID=" & ID

End Function

Function DeleteRecord(Id, theID)

	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected

End Function

Function GO()

	Response.Redirect "../CRM.asp?CustomerID=" & ID

End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW" NewRecord()
	Case "AMEND" AmendRecord()
	Case "DELETE" DeleteRecord()
	Case "LOAD" LoadRecord()
End Select
Call CloseDB()
%>