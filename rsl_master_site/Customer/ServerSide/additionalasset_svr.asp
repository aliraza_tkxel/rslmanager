<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%

	Dim property_id, tenancy_id, customer_id, action, str_possible_assets
	
	action = Request.FORM("hid_ACTION")
	property_id = Request("propertyid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")

	response.write "action is "  & property_id & "<BR>"

	OpenDB()
	
	If action = "DELETE" Then 
		delete_asset()
	Elseif action = "GETASSETS" Then
		get_assets()
	Elseif action = "ATTACH" Then
		attach_asset()
	End If		
	
	CloseDB()
	
	// RETRIEVE A LIST OF ASSETS THAT EXIST WITHIN THIS SPECIFIC DEVELOPENT
	Function get_assets()
		
		Dim dev_id
		dev_id = Request("devid")
		str_possible_assets = ""
		str_SQL = "SELECT 	ISNULL(P.PROPERTYID,'') AS PROPERTYID, " &_
					"		ISNULL(T.DESCRIPTION,'') + ' ' + ISNULL(P.HOUSENUMBER,'') AS ADDRESS,  " &_
					"		ISNULL(ADDRESS1,'') AS ADDR, " &_
					"		ISNULL(F.TOTALRENT,0) AS RENT, F.PROPERTYID AS ISNULLFINANCIAL " &_		
					"FROM	P__PROPERTY P " &_
					"		LEFT JOIN P_PROPERTYTYPE T ON P.PROPERTYTYPE = T.PROPERTYTYPEID " &_
					"		LEFT JOIN P_FINANCIAL F ON P.PROPERTYID = F.PROPERTYID " &_
					"WHERE	P.PROPERTYTYPE IN (6,8,9) AND STATUS = 1 " &_ 
					"ORDER BY HOUSENUMBER "
'AND DEVELOPMENTID = " & dev_id &_					
					
		str_possible_assets = str_possible_assets &_
			"<table height='100%' width='100%' hlcolor='steelblue' slcolor='' " &_
			"style='behavior:url(/Includes/Tables/tablehl.htc);BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:" &_
			"COLLAPSE' cellpadding=3 cellspacing=0 border=1 class='TAB_TABLE'><tHEAD><tr bgcolor=#133E71 style='COLOR:WHITE'>" &_
			"<td><b>Property Ref</b></td><td><b>Type-No</b></td><td><b>Location</b></td><td><b>Rent</b></td></tr></THEAD>"
		
		Call OpenRs(rsSet, str_SQL)
		cnt = 0
		While not rsSet.EOF 
			cnt = cnt + 1
			isNullFinancial = rsSet("ISNULLFINANCIAL")
			if isNull(isNullFinancial) then
				str_possible_assets = str_possible_assets &_
						"<TR ><TD>" & rsSet("PROPERTYID") & "</TD><TD>" & rsSet("ADDRESS") & "</TD><TD>" & rsSet("ADDR") &_
						"</TD><TD align=center title='No rent set for this item. Contact the estates team.'><img src='/myImages/NR.gif' border=0></TD></TR>"
			else
				str_possible_assets = str_possible_assets &_
						"<TR STYLE='CURSOR:HAND' TITLE='Attach Asset' ONCLICK=""attach_asset('" & rsSet("PROPERTYID") & "', " & rsSet("RENT") & ")""><TD>" & rsSet("PROPERTYID") & "</TD><TD>" & rsSet("ADDRESS") & "</TD><TD>" & rsSet("ADDR") &_
						"</TD><TD>" & FormatCurrency(rsSet("RENT")) & "</TD></TR>"
			end if			
			rsSet.movenext()
			
		Wend
		if cnt = 0 then str_possible_assets = str_possible_assets & "<TR><TD colspan=4 ALIGN=CENTER>No suitable assets available !!</TD></TR>"
		str_possible_assets = str_possible_assets & "<TFOOT><TR><TD colspan=4 height='100%' align=right valign=bottom><INPUT TYPE=BUTTON VALUE='&nbsp;&nbsp;Back&nbsp;&nbsp;' class='rslbutton' onclick='do_swap_tables()'></TD></TR></TFOOT></table>"
		CloseRs(rsSet)
	
	End Function
	
	
	// ATTACH CHOSEN ASSET TO THIS PROPERTY
	Function attach_asset()
	
		Dim asset_id, rent
		asset_id = Request("assetid")
		rent = Request("rent")
		// INSERT ADDITIONAL ASSET ROW
		conn.execute("INSERT INTO C_ADDITIONALASSET (TENANCYID, PROPERTYID, CREATEDBY, STARTDATE) VALUES (" &_
					 tenancy_id & ", '" & asset_id & "', "  & Session("USERID") &  ", '" & FormatDateTime(Request("startdate"), 2) & "')")
		// UPDATE GARAGE STATUS TO LET			 
		conn.execute("UPDATE P__PROPERTY SET STATUS = 2, SUBSTATUS = NULL WHERE PROPERTYID = '" & asset_id & "'")
		
		// get start and end of month
		startdate = Request("startdate")
		enddate = getlastday(startdate)
		
		// execute proc to enter adjusted rent row
		Set rs = Server.CreateObject("ADODB.Command")
		Set rs.activeconnection = Conn
		rs.commandtype = 4 
		rs.commandtext = "F_PART_MONTH_RENT_INSERT"
		
		//response.write "start" & startdate & "<BR>" response.write "end" & enddate & "<BR>" response.write "prop" & asset_id & "<BR>" response.write "ten" & tenancy_id & "<BR>"
		
		//set parameters
		rs.parameters(1) = startdate 	' Start Date	
		rs.parameters(2) = enddate		' End date
		rs.parameters(3) = asset_id	' Propertyid
		rs.parameters(4) = tenancy_id	' Tenancyid
		
		rs.execute
		
	End Function
			
	// delete additional asset
	Function delete_asset()
			
		// CREATE REBATE ROW IN RENT JOURNAL
	
		// get start and end of month
		startdate = Request("enddate")
		enddate = getlastday(startdate)
		
		conn.execute ("UPDATE C_ADDITIONALASSET SET LASTMODIFIEDBY = " & Session("USERID") & ", ENDDATE = '" & FormatDateTime(Request("enddate"), 2) & "' WHERE TENANCYID = " & tenancy_id & " AND PROPERTYID = '" & property_id & "'")
		conn.execute ("UPDATE P__PROPERTY SET STATUS = 1, SUBSTATUS = NULL WHERE PROPERTYID = '" & property_id & "'" )

		response.write "start" & startdate & "<BR>" 
		response.write "end" & enddate & "<BR>" 
		response.write "prop" & property_id & "<BR>" 
		response.write "ten" & tenancy_id & "<BR>"
		
		Set rs = Server.CreateObject("ADODB.Command")
		Set rs.activeconnection = Conn
		rs.commandtype = 4 
		rs.commandtext = "F_PART_MONTH_RENT_DEDUCT"
		
		//set parameters
		rs.parameters(1) = startdate ' Start Date	
		rs.parameters(2) = enddate' End date
		rs.parameters(3) = property_id	' Propertyid
		rs.parameters(4) = tenancy_id	' Tenancyid
		rs.parameters(5) = 2	' Removal Type 2 (Additional Asset)				
		
		rs.execute
				
	End Function
	
		// calculate last day of month
	Function getlastday(datum)
		
		thedate = datevalue(datum)
		if month(thedate) = 4 or month(thedate) = 9 or month(thedate) = 6 or month(thedate) = 11 then
			monthdays = 30
		elseif month(thedate) = 2 then
			if year(thedate) = 2004 or year(thedate) = 2008 or year(thedate) = 2012 then
				monthdays = 29
			else monthdays = 28 end if
		else monthdays = 31 end if
			
		getlastday = (monthdays & "/" & month(thedate) & "/" & year(thedate))
		
	End function
	
%>
<html>
<head></head>
<script language=javascript>
	function ReturnData(){
		if ("<%=action%>" == "DELETE")
			parent.parent.CRM_TOP_FRAME.location.href = "../iframes/iTenancy.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&toshow=3"
		else if ("<%=action%>" == "GETASSETS") {
			parent.ass_body.innerHTML = AssetData.innerHTML;
			parent.swap_tables('table6');
			}
		else if ("<%=action%>" == "ATTACH")
			parent.parent.CRM_TOP_FRAME.location.href = "../iframes/iTenancy.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&toshow=3"
		}
</script>
<body onload="ReturnData()">
<div id="AssetData"><%=str_possible_assets%></div>
</body>
</html>