<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, page_source, sel_width

	int_source = Request("ITEMACTIONID")
	if int_source = "" then int_source = -1 end If
	page_source = Request("pagesource")

	' IF CALLED FROM POP UP SELECT WIDTH MUST BE SMALLER
	If page_source = 1 then 
		sel_width = "style=""width:250px"" "
	Else
		sel_width = "style=""width:300px"" "
	End If

	Call OpenDB()
	Call build_select()

	Function build_select()

		sql = "SELECT LETTERID, LETTERDESC FROM C_STANDARDLETTERS WHERE SUBITEMID = " & int_source & " ORDER BY LETTERID"

		Call OpenRs(rsSet, sql)

		lst = "<select name=""sel_LETTER"" id=""sel_LETTER"" class=""textbox200"" " & sel_width & ">"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name=""sel_LETTER"" id=""sel_LETTER"" class=""textbox200"" " & sel_width & " disabled=""disabled""><option value=''> No associated letters found</option></select>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Change Letter Action</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvLetter").innerHTML = document.getElementById("dvLetter").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvLetter">
        <%=lst%>
    </div>
</body>
</html>
