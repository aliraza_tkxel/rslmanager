<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_nature, lst, Label, accesskey
    Dim Recs 'Boolean 1 = Yes, 0 = No
        Recs = 0

    Dim mysql
        mysql = ""

    nature_id = Request("natureid")
	If nature_id = "" Then nature_id = 0 End If

    If nature_id = 64 Then

        mysql = "SELECT NST.NATURESUBTYPEID, NST.DESCRIPTION " &_
        "FROM C_NATURE N " &_
        "INNER JOIN C_NATURETONATURESUBTYPE NNST ON N.ITEMNATUREID = NNST.ITEMNATUREID " &_
        "INNER JOIN C_NATURESUBTYPE NST ON NNST.NATURESUBTYPEID = NST.NATURESUBTYPEID " &_
        "WHERE N.ITEMNATUREID = " & nature_id & " " &_
        "ORDER BY NST.DESCRIPTION"

         Label = "<span style=""text-decoration: underline"">T</span>ype:"
         accesskey = "T"

    ElseIf nature_id = 27 Then

        mysql = "SELECT REASONID, [DESCRIPTION] FROM dbo.C_TERMINATION_REASON WHERE ACTIVE = 1 ORDER BY [DESCRIPTION]"
        Label = "<span style=""text-decoration: underline"">R</span>eason:"
        accesskey = "R"

    End If

	Call OpenDB()
	Call build_select(str_nature, mysql)
	Call CloseDB()

	Function build_select(ByRef lst, ftn_sql)

    if ftn_sql = "" then
        Exit Function
    end if
     
		Call OpenRs(rsSet, ftn_sql)
        If rsSet.EOF Then
            'rw "No Records"
            Recs = 0
            lst = ""
        Else
            Recs = 1
            lst = "<select name=""sel_SUBNATURE"" id=""sel_SUBNATURE"" class=""textbox200"">"
            lst = lst & "<option value=''>Please select</option>"
            'int_lst_record = 0
            While (NOT rsSet.EOF)
			    lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
	    	    rsSet.MoveNext()
			    'int_lst_record = int_lst_record + 1
		    Wend
                lst = lst & "</select>"
        End If

		Call CloseRs(rsSet)

	End Function

'Response.Write str_nature
'Response.End()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Item Change</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
        if (<%=Recs%> == 1) {
            parent.document.getElementById("dvTitle").style.display = "none";
            parent.document.getElementById("dvSubNature").style.display = "block";
            parent.document.getElementById("dvSubNature").innerHTML = document.getElementById("dvSubNature").innerHTML;
            parent.frm_nature.location.href = "../blank.asp";
            }
            else
            {
                parent.document.getElementById("dvTitle").style.display = "block";
                parent.document.getElementById("dvSubNature").style.display = "none";
                parent.document.getElementById("dvSubNature").innerHTML = ""
            }
        }
    </script>
</head>
<body onload="ReturnData()">
<div class="row" id="dvSubNature">
    <span class="label">
        <label for="sel_SUBNATURE" accesskey="<%=accesskey%>" style="cursor: pointer">
            <%=Label%>
        </label>
    </span>
    <span class="formw">
        <%=str_nature%><img src="/js/FVS.gif" name="img_SUBNATURE" id="img_SUBNATURE" width="15px" height="15px"
            border="0" alt="" />
    </span>
</div>
</body>
</html>
