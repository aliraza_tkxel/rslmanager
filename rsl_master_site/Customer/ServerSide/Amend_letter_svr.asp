<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_content, str_code, javaString, str_desc,DivString, int_nature
	Dim int_action, form_action, lst, int_from, int_to, amend_count
	Dim str_temp,str_Signature,str_tempSIG
	
	form_action 	= request.form("hid_ACTION")
	amend_count 	= request.form("hid_AMENDCOUNT")
	OpenDB()
	
	If amend_count = "2" then
		
			insert_temp()
	
	else
	
			If form_action = "NEW" Then
					new_record()
				ElseIf form_action = "LOAD" Then
					
					load_record()
				Else 
					amend_record()
				End If
				
	End If
	CloseDB()
	
		
	Function amend_record()
	
	End Function
	
	
	Function get_data()
	
		str_code 	  = request.form("txt_LETTERCODE")
		str_desc 	  = request.form("txt_LETTERDESC")
		str_content   = Replace(request.form("txt_LETTERTEXT"),"'", "''")
		str_Signature = Replace(request.form("txt_SIGNATURE"),"'", "''")
		int_nature 	  = request.form("sel_NATUREID")
		int_action 	  = request.form("sel_SUBITEMID")
		int_from 	  = request.form("sel_FROM")
		int_to	 	  = request.form("sel_TO")
	
	End Function
	
	Function insert_temp()
		
		get_data()
		Dim clean_content
		Dim letter_id
			
		letter_id = Request("hid_letterid")
		clean_content = Replace(Replace(str_content, Chr(10), ""), Chr(13), "<BR>")
		'clean_SIG = Replace(Replace(str_Signature, Chr(10), ""), Chr(13), "<BR>")
			
		
		SQL = "UPDATE  C_STANDARDLETTERS SET TEMPLETTER = '" & clean_content & "', TEMPSIG = '" & clean_SIG & "' WHERE LETTERID = " &  letter_id
		'RW SQL
		conn.execute(SQL)
		
		'reset_data()	
		
		SQL = "SELECT * FROM C_STANDARDLETTERS WHERE LETTERID = " & letter_id
		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF then
		
			str_temp	 	= rsSet("TEMPLETTER")
			str_SIG			= rsSet("LETTERSIG")
			str_CONTENT	 	= rsSet("TEMPLETTER")
			str_tempSIG 	= rsSet("TEMPSIG")
			
			rw str_temp
			
			
			if str_tempSIG <> "" then 
				 DivString = "<div id='SIG'>"
				 If amend_count = "2" then 
						DivString = DivString & str_tempSIG
				 Else
				 		DivString = DivString & str_SIG
				 END IF 
				 DivString = DivString & "</div>"
				 javaString = "parent.RSLFORM.txt_SIGNATURE.value = SIG.innerText"
				 
			End If
			'rw str_temp
		Else
			str_content = "No associated letter exists please contact Reidmark Ebusiness Systems."
		End If
		
		'load_record()
	
	End Function
	
	Function new_record()
	
		Dim clean_content
		get_data()
		
		clean_content = Replace(Replace(str_content, Chr(10), ""), Chr(13), "<BR>")
		clean_SIG = Replace(Replace(str_Signature, Chr(10), ""), Chr(13), "<BR>")
		
		SQL = "INSERT INTO C_STANDARDLETTERS (NATUREID, FROMID, TOID, SUBITEMID, LETTERCODE, LETTERDESC, LETTERTEXT, LETTERSIG) " &_
				"VALUES (" & int_nature & "," &_
							 int_from & "," &_
							 int_to & "," &_
							 int_action & ",'" &_
							 str_code & "','" &_
							 str_desc & "','" &_
							 clean_content & "','" &_
							 clean_SIG & "')"
		conn.execute(SQL)
		load_record()
	
	End Function
	
	Function reset_data()
	
		str_code 	= ""
		str_desc 	= ""
		str_content = ""
		int_nature 	= ""
		int_action  = ""
	
	End Function
	
	Function load_record()
	
		Dim letter_id
		letter_id = Request("hid_letterid")
		
		SQL = "SELECT * FROM C_STANDARDLETTERS WHERE LETTERID = " & letter_id
		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF then
			str_code 	= rsSet("LETTERCODE")
			str_desc 	= rsSet("LETTERDESC")
			str_content = rsSet("LETTERTEXT")
			str_SIG 	= rsSet("LETTERSIG")
			int_nature 	= rsSet("NATUREID")
			int_action 	= rsSet("SUBITEMID")
			int_from 	= rsSet("FROMID")
			int_to	 	= rsSet("TOID")
			str_temp 	= rsSet("TEMPLETTER")
			str_tempSIG = rsSet("TEMPSIG")
			if str_tempSIG <> "" then 
				 DivString = "<div id='SIG'>"
				 If amend_count = "2" then 
						DivString = DivString & str_tempSIG
				 Else
				 		DivString = DivString & str_SIG
				 END IF 
				 DivString = DivString & "</div>"
				 javaString = "parent.RSLFORM.txt_SIGNATURE.value = SIG.innerText"
			End If
			'rw str_temp
		Else
			str_content = "No associated letter exists please contact Reidmark Ebusiness Systems."
		End If
	
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript">

	function return_data(){
	
		var client_action = "<%=form_action%>";

		if (client_action == "LOAD"){
			do_load();
			
			}
		else if (client_action == "AMEND" || client_action == "NEW")
			parent.location.href = "../standardletters.asp"
	}	
	
	function do_load(){
	
		//parent.RSLFORM.sel_NATUREID.value = "<%=int_nature%>";
		//parent.RSLFORM.txt_LETTERCODE.value = "<%=str_code%>";
		//parent.RSLFORM.txt_LETTERDESC.value = "<%=str_desc%>";					
		parent.RSLFORM.txt_LETTERTEXT.value = Content1.innerText					
		<%=javaString%>
		//parent.swap_button(1);
		//parent.RSLFORM.sel_SUBITEMID.outerHTML = "<%=lst%>";
		//parent.RSLFORM.sel_SUBITEMID.value = "<%=int_action%>";
		//parent.RSLFORM.sel_FROM.value = "<%=int_from%>";
		//parent.RSLFORM.sel_TO.value = "<%=int_to%>";

	}	
	
	
</SCRIPT>
<BODY onload="return_data()">
<div id="Content1"><% If amend_count <> "" then %><%=str_temp%><% Else %><%=str_content%><%END IF %></div>
<%=DivString%>
</BODY>
</HTML>
