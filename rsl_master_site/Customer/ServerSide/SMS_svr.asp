<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="GlobalVariables.asp" -->

<%
Dim item_id, itemnature_id, title, customer_id, tenancy_id, nature_id, nature_desc, mobile, message

OpenDB()
get_querystring()
new_record()
CloseDB()
PostHTML()

    Function get_querystring()
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
        nature_desc = Request("nature_desc")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
        mobile = Request.Form("txt_mobile")
        message = Request.Form("txt_message")
    End Function

	Function new_record()
	
		Dim strSQL, journal_id
		get_querystring()

		if not tenancy_id <> "" then tenancy_id = "NULL" end if
 		
		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", "& tenancy_id &", NULL,  " & item_id & ", " & itemnature_id & ", 14, '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		rsSet.close()
		Set rsSet = Nothing
		
				
		// GENERAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_SMS " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONDATE, LASTACTIONUSER, [MESSAGE], MOBILE) " &_
					"VALUES (" & journal_id & ", 14, '" & Now & "', " & Session("userid") &_
					 ", '" & Replace(message, "'", "''") & "', '" & Replace(mobile, "'", "''") & "')" &_
                     "SELECT SCOPE_IDENTITY() AS SMSHISTORYID;"
	

		set rsSet = Conn.Execute(strSQL)
		smshistory_id = rsSet.fields("SMSHISTORYID").value
		rsSet.close()
		Set rsSet = Nothing
		'Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End Function

  Function PostHTML()

        message = Server.urlencode(message) 'encode special characters (e.g. �, & etc) 
        from = "Broadland" 
        address = "https://api.txtlocal.com/send/?apiKey=" 
        selectednums = mobile
        apiKey = "resw5bAZdIs-fnUwznzIfzJM66WfRdWSQRXlFtR7V2"
        url = address & apiKey & "&numbers=" & selectednums & "&message=" & message & "&sender=" & from
        set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
        xmlhttp.open "POST", url, false 
        xmlhttp.send "" 
        msg = xmlhttp.responseText 
        response.write(msg) 
        set xmlhttp = nothing 
         
    End Function

%>

<html>
<head>

<script language="javascript">

    function redirect() {
        parent.parent.location.href = "../iFrames/iCustomerJournal.asp?tenancyid=<%=tenancy_id%>&customerid=<%=tenancy_id%>&SyncTabs=1";
    }

</script>
</head>
<body onload="redirect();">
</body>
</html>