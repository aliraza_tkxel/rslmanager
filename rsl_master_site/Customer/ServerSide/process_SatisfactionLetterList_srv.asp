<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
''''''''''''
'' PAGE NOTE: This is part of the repair approval tool that shows all the repairs that have been completed and 
'' awaiting approval.
''
'' NOTE: Pages to be aware of: CompletedRepairsList.asp 
''							   CompletedRepairs_srv.asp 
''						       process_CompletedRepairslist_srv.asp
''''''''''''

bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim SATISFACTION_LETTER_HTML
	Dim insert_list  
	Dim sel_split
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color
	Dim ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor
	Dim StringName ,Title , ShortName, FirstName, LastName ,StringFullName, str_Dear , FullAddress

	Dim JOURNALID,orderno,repairdetails 
	Dim	orderdate,timeframe,tenancy_id
	Dim	ItemContractor,theTitle,ItemDetailID

	CurrentDate = CDate("1 jan 2004") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear

	insert_list = Request.Form("idlist")

	'response.write "idlist : " & insert_list & "<BR><BR>"
		
	OpenDB()
	
	update_Repair()
	
	CloseDB()
	
	// This function Adds a new History item to c_repair and updates the c_journal
 	
	Function update_Repair()
		
		Dim icount
		icount = 0
		sel_split = Split(insert_list ,",") ' split selected string
		For each key in sel_split
		icount = icount + 1
		next
		For each key in sel_split

			itemdetail_id = KEY
			
			if itemdetail_id <> "" then
			
			   SQL =" SELECT 	PI.ORDERID AS ORDERNUMBER, " &_
					"			I.DESCRIPTION AS DETAILSOFREPAIR, " &_
					"			J.CREATIONDATE AS ORDERDATE, R.ITEMDETAILID, " &_
					"			PRI.ESTTIME AS TIMEFRAME, " &_
					"			C.CUSTOMERID," &_
					"			O.NAME AS CONTRACTORNAME," &_
					"			R.CONTRACTORID, " &_
					"			CT.TENANCYID, " &_
					"			J.JOURNALID, J.TITLE " &_				
					" FROM C_SATISFACTIONLETTER SL " &_
					"			INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID " &_
					"			INNER JOIN C_REPAIR R ON R.JOURNALID = SL.JOURNALID " &_
					"			INNER JOIN R_ITEMDETAIL I ON R.ITEMDETAILID = I.ITEMDETAILID " &_
					"			INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY " &_
					"			INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
					"			INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C.CUSTOMERID " &_
					"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
					"			INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID  " &_
					"			INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_
					"			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOTO.ORDERITEMID " &_
					" WHERE 	CT.ENDDATE IS NULL " &_
					"			AND SL.SATISFACTION_LETTERSTATUS = 1 " &_
					"			AND PI.ORDERITEMID IS NOT NULL " &_
					"			AND R.ITEMACTIONID IN (5,6,10,15,9)  " &_
					"			AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID)AS REPAIRHISTORYID  FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
					"			AND pi.ORDERITEMID = " & itemdetail_id  &_
					" ORDER BY  R.LASTACTIONDATE DESC "
							
					'RW SQL & "<br><br>"
					Call OpenRS(rsOrderDetails, SQL)
					if not rsOrderDetails.eof then
					JOURNALID = rsOrderDetails("JOURNALID")
					orderno = rsOrderDetails("ORDERNUMBER")
					Contractor =  rsOrderDetails("CONTRACTORNAME")
					repairdetails = rsOrderDetails("DETAILSOFREPAIR")
					orderdate = FormatDateTime(rsOrderDetails("ORDERDATE"),2)
					timeframe = rsOrderDetails("TIMEFRAME")
					customer_id   = rsOrderDetails("CUSTOMERID")
					tenancy_id = rsOrderDetails("TENANCYID")
					ItemContractor = rsOrderDetails("CONTRACTORID")
					theTitle = replace(rsOrderDetails("TITLE"),"'","''")
					ItemDetailID = rsOrderDetails("ITEMDETAILID")
				 
				' End of item details collection
				
				' This builds the address and name structures that go on to the letter, this section goes on for quite a bit
					
					' Get the contact details for the letter
					SQL = "SELECT E.FIRSTNAME + ' ' + E.LASTNAME as FullName, C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
							"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
							"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
							"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
							" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_	
							"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
							"WHERE CT.ENDDATE IS NULL AND CT.CUSTOMERID = " & customer_id
							
					'RW SQL & "<br><br>"
					Call OpenRS(rsCust, SQL)
					
					count = 1
					while NOT rsCust.EOF
										
						'===============================================================================
						'	Start Building name and address info for letter
						'===============================================================================
						
						''''''''''''''''''''
						' Build Names First
						''''''''''''''''''''
						
							ShortName = ""
							StringName = ""
													
							'TITLE
							Title = rsCust("TITLE")
							if (Title <> "" AND NOT isNull(Title)) then
								StringName = StringName & Title & " "
								ShortName = ShortName & Title & " "
							end if
							
							'FIRSTNAME
							FirstName = rsCust("FIRSTNAME")
							if (FirstName <> "" AND NOT isNull(FirstName)) then
								StringName = StringName & FirstName & " "
							end if
							
							'LASTNAME
							LastName =rsCust("LASTNAME")
							if (LastName <> "" AND NOT isNull(LastName)) then
								StringName = StringName & LastName & " "
								ShortName = ShortName & LastName
							end if
							
							' DEAR LINE IN LETTER
							if (count = 1) then
								StringFullName = StringName
								str_Dear = "Dear " & ShortName
								count = 2
							else
								StringFullName = StringFullName & " and " & StringName
								str_Dear = str_Dear & " and " & ShortName
							end if
							
							rsCust.moveNext
						wend
					
					'''''''''''''''''''''''
					' End Build Names First
					'''''''''''''''''''''''
					
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''
						
						if (count = 2) then
							rsCust.moveFirst()
							housenumber = rsCust("housenumber")
							if (housenumber = "" or isNull(housenumber)) then
								housenumber = rsCust("flatnumber")
							end if
							if (housenumber = "" or isNull(housenumber)) then
								FirstLineOfAddress = rsCust("Address1")
							else
								FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
							end if
					
							RemainingAddressString = ""
							AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
							for i=0 to Ubound(AddressArray)
								temp = rsCust(AddressArray(i))
								if (temp <> "" or NOT isNull(temp)) then
									RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font:12PT'>" & temp & "</TD></TR>"
								end if
							next
							tenancyref = rsCust("TENANCYID")
						end if
						
						' now combine the name and address to give a post label
						FullAddress = "<TR><TD nowrap >" & StringFullName & "</TD></TR>" &_
									"<TR><TD nowrap >" & FirstLineOfAddress & "</TD></TR>" &_
									RemainingAddressString 
								
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''				
								
					'========================================================================================
					'	End building name and address info
					'========================================================================================
				
				
			'**********************
			'NEED TO REVIEW THIS BIT HERE
			'***			*********************
				
			'Update the Satisfaction letter status to sent (2) in the database to strike off the list
			strSQL = "UPDATE C_SATISFACTIONLETTER SET SATISFACTION_LETTERSTATUS = 2 WHERE JOURNALID = " & JournalID
			'RW strSQL & "<br><br>"
			Conn.Execute(strSQL)
			
			'***			**********************
			'NEED TO REVIEW THIS BIT HERE
			'*********************
			
			
					if not sel_split(icount-1) = key then
						DoPageBreakStart = "<p style='page-break-after: always'>"
						DoPageBreakEnd = "</p>"
					else
						DoPageBreakStart = ""
						DoPageBreakEnd = ""
					End If
			
					SATISFACTION_LETTER_HTML_PAGE_1 = DoPageBreakStart & SL_PAGE1()  
					SATISFACTION_LETTER_HTML_PAGE_2 = SL_PAGE2() & DoPageBreakEnd
					
					' Attach cover Letter part
					SATISFACTION_LETTER_HTML = SATISFACTION_LETTER_HTML & SATISFACTION_LETTER_HTML_PAGE_1 
					'Attach Survey section
					SATISFACTION_LETTER_HTML = SATISFACTION_LETTER_HTML & SATISFACTION_LETTER_HTML_PAGE_2
					ClearCustomerVars()
						
					end if
				End If
		Next
	
	End Function
	
	' THIS FUNCTION CLEARS THE CUSTOMER VARS SO THAT A CUSTOMERS NAME DOESNT APPEAR ON THE NEXT LETTER IN THE LOOP
	Function ClearCustomerVars()
	
			StringName 	= ""
			Title 		= ""
			ShortName 	= ""
			FirstName 	= ""
			LastName 	= ""
			StringFullName = ""
			str_Dear 	= ""
	
	End Function
	
	
	''''''
	' NOTE ABOUT FOLLOWING FUNCTIONS
	'			These functions are the individual pages that make up all the letter in pages 1,2....n
	'''''
	
	' Page 1 is the covering letter
	Function SL_PAGE1()
	
			LetterHTML = LetterHTML & "     <table width='647' height='100%' border='0' cellspacing='0' cellpadding='0' style='page-break-after: always'> " & vbCrLf
			LetterHTML = LetterHTML & "					  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "						<td height='110' valign='top'>" & vbCrLf
			LetterHTML = LetterHTML & "						  <table width=100>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td nowrap valign=top width='137' HEIGHT='113'>&nbsp; </td>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td align=right width=683 valign=top>&nbsp; </td>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td align=right valign='top' width='150'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "						 </table>" & vbCrLf
			LetterHTML = LetterHTML & "					    </td>" & vbCrLf
			LetterHTML = LetterHTML & "					  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "					  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "						<td valign='top' height='171'>" & vbCrLf
			LetterHTML = LetterHTML & "						  <table width='647' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML & "									<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td> " & FullAddress  & vbCrLf
			LetterHTML = LetterHTML & "							  </td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr><tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td height='95'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td> "&formatdatetime(date,1) & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>" & str_Dear & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>We confirm that the following work will be carried" & vbCrLf
			LetterHTML = LetterHTML & "							    out by the nominated Broadland contractor. Once the work has been" & vbCrLf
			LetterHTML = LetterHTML & "							    completed we would be grateful if you could complete the information" & vbCrLf
			LetterHTML = LetterHTML & "							    on the questionnaire attached. We have enclosed a prepaid envelope for you" & vbCrLf
			LetterHTML = LetterHTML & "							    to return your questionnaire to us.<br>" & vbCrLf
			LetterHTML = LetterHTML & "							    <br>" & vbCrLf
			LetterHTML = LetterHTML & "Broadland Housing Association is committed to providing an efficient and responsive repair service. If you are unable to keep the appointment agreed with the Contractor can you please notify the Contractor or call Broadland direct on 01603 750250 so that a new appointment can be arranged. </td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "							<tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <td>" & vbCrLf
			LetterHTML = LetterHTML & "							    <table width='100%' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr >" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr >" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>Tenancy No:</td>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>"& Tenancy_ID & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr >" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>Order No:</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>" & orderno & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>Contractor:</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>" & contractor & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>Details of Repair:</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>" & repairdetails & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>Order Date:</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>" & OrderDate & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & " 								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>Complete Within: </td>" & vbCrLf
			LetterHTML = LetterHTML & "								<td width='76%'>" & Timeframe  & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "							  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "							  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='24%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "									<td width='76%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML & "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>Broadland Housing </td>" & vbCrLf
			LetterHTML = LetterHTML & "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML & "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML & "							    </table>" & vbCrLf
			LetterHTML = LetterHTML & "							  </td>" & vbCrLf
			LetterHTML = LetterHTML & "							</tr>" & vbCrLf
			LetterHTML = LetterHTML & "						  </table>" & vbCrLf
			LetterHTML = LetterHTML & "</td>" & vbCrLf
			LetterHTML = LetterHTML & "</tr>" & vbCrLf
			LetterHTML = LetterHTML & "</table>" & vbCrLf
			LetterHTML = LetterHTML & ""
	
			SL_PAGE1 = LetterHTML
			
	End Function
	
	' Page 2 is the survey
	Function SL_PAGE2()

			LetterHTML = LetterHTML &  "<table width='647' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML &  "					  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "						<td valign='bottom' align='left' height='100%'>" & vbCrLf
			LetterHTML = LetterHTML &  "						  <table width='649' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr align=""center"" bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td height=""27"" colspan='2'><strong>Repair Satisfaction Survey </strong></td>" & vbCrLf
			LetterHTML = LetterHTML &  "						    </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td colspan='2'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "						    </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td colspan='2'>Please answer these few questions" & vbCrLf
			LetterHTML = LetterHTML &  "							    (where relevant) after the work has been completed. <br>" & vbCrLf
			LetterHTML = LetterHTML &  "							    Please don't return to the contractor.</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							</tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='23%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='77%'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							</tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='23%' height='27'>Date work completed:</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='77%' height='27'>..................................................... Time Completed : ....................................</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							</tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='23%' height='9'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td width='77%' height='9'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							</tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							<tr>" & vbCrLf
			LetterHTML = LetterHTML &  "							  <td colspan='2'>" & vbCrLf
			LetterHTML = LetterHTML &  "							    <table width='647' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan=""2"">&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td width=""29%"" align=""center"">Please Tick </td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan=""2"">&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td rowspan=""10"" valign=""top""><table width='100%' border='0' cellspacing='0' cellpadding='0'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td width='33%' height=""19""><b> Good</b></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td width='33%'><b>&nbsp;&nbsp;Average&nbsp;&nbsp;</b></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td width='33%'><b>Poor</b></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""5"" colspan=""3"">" & vbCrLf
			LetterHTML = LetterHTML &  "										</td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""19""><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									     <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""5"" colspan=""3"">" & vbCrLf
			LetterHTML = LetterHTML &  "										</td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""19""><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									     <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""5"" colspan=""3"">" & vbCrLf
			LetterHTML = LetterHTML &  "										</td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""19""><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									     <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""5"" colspan=""3"">" & vbCrLf
			LetterHTML = LetterHTML &  "										</td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""19""><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td><img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									     <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "                                        <td height=""5"" colspan=""3"">" & vbCrLf
			LetterHTML = LetterHTML &  "										</td>" & vbCrLf
			LetterHTML = LetterHTML &  "                                      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "                                    </table></td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "						          </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""14"" colspan=""2"">1. How easy was it to report this repair?</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""14"" colspan=""2"">2. How helpful where the BHA staff you dealt with on this occasion?</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""14"" colspan=""2"">3." & vbCrLf
			LetterHTML = LetterHTML &  "" & vbCrLf
			LetterHTML = LetterHTML &  "" & vbCrLf
			LetterHTML = LetterHTML &  "How do you rate the contractors service?</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""14"" colspan=""2"">4. In your opinion, what is the standard of the repair? </td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan=""2"">5. Have you any suggestions on improving the service? </td>" & vbCrLf
			LetterHTML = LetterHTML &  "							        <td valign=""top"">&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <tr valign=""bottom"">" & vbCrLf
			LetterHTML = LetterHTML &  "								      <td height=""40"" colspan=""3"">								        <br>" & vbCrLf
			LetterHTML = LetterHTML &  "								        ............................................................................................................................................................<br>" & vbCrLf
			LetterHTML = LetterHTML &  "							            <br>" & vbCrLf
			LetterHTML = LetterHTML &  "						              .............................................................................................................................................................<br>" & vbCrLf
			LetterHTML = LetterHTML &  "						              <br>" & vbCrLf
			LetterHTML = LetterHTML &  ".............................................................................................................................................................<br></td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan=""2"">" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5""></td>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>6. Would you like to be involved in reviewing the repair service offered by <BR> Broadland?&nbsp;&nbsp;&nbsp; Yes <img src='../../myImages/box.JPG' width='18' height='15'>&nbsp;&nbsp; No <img src='../../myImages/box.JPG' width='18' height='15'><BR><BR></td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>7. Did the contractor contact you to make an appointment?&nbsp;&nbsp;&nbsp; Yes <img src='../../myImages/box.JPG' width='18' height='15'>&nbsp;&nbsp; No <img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf	
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>8. Was the job completed on the first appointment?&nbsp;&nbsp;&nbsp; Yes <img src='../../myImages/box.JPG' width='18' height='15'>&nbsp;&nbsp; No <img src='../../myImages/box.JPG' width='18' height='15'></td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf	
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'><p> <em><strong>Please Note</strong></em> :<br>" & vbCrLf
			LetterHTML = LetterHTML &  "								      If you have answered &lsquo;poor&rsquo; to questions 1,2,3 or 4 then you will be a contacted by a Broadland representative</p>" & vbCrLf
			LetterHTML = LetterHTML &  "								      </td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									<td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr align='center'>" & vbCrLf
			LetterHTML = LetterHTML &  "									<td colspan='3'><b><i>Thank you for helping" & vbCrLf
			LetterHTML = LetterHTML &  "									  us help you!</i></b></td>" & vbCrLf
			LetterHTML = LetterHTML &  "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "									<td colspan='3'>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan='3'>" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan='2'> Office use Only : </td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "									<td width=""18%"">Tenancy No: </td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td width=""53%"" bgcolor=""#F8F8F8"">"& tenancy_ID & "</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								  </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>Order No: </td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>"& OrderNo & "</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>Repair Desc: </td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td colspan=""2"">"& repairdetails & "</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr bgcolor=""#F8F8F8"">" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td height=""5"" colspan='3'>" & vbCrLf
			LetterHTML = LetterHTML &  "									</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								  <tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "								    <td>&nbsp;</td>" & vbCrLf
			LetterHTML = LetterHTML &  "							      </tr>" & vbCrLf
			LetterHTML = LetterHTML &  "								</table>" & vbCrLf
			LetterHTML = LetterHTML &  "							  </td>" & vbCrLf
			LetterHTML = LetterHTML &  "							</tr>" & vbCrLf
			LetterHTML = LetterHTML &  "						  </table>" & vbCrLf
			LetterHTML = LetterHTML &  "					    </td>" & vbCrLf
			LetterHTML = LetterHTML &  "</tr></table>" & vbCrLf
			LetterHTML = LetterHTML &  ""

			SL_PAGE2 = LetterHTML
	
	End Function
	
	
		
%>
<html>
<head>
<style type="text/css">
<!--
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
}
-->
</style>
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/print.js"></SCRIPT>
<script language=javascript>

	function PrintMe(){
		printFrame(this);
		}
	
	function ReturnData(){
		parent.str_idlist = ""
		document.location.href = "SatisfactionLetterList_srv.asp"
		
		}
</script>
<body onload="PrintMe();ReturnData()" class="style1">
<%=SATISFACTION_LETTER_HTML%>
</body>
</html>