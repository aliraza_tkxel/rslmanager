<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, customer_id, row_id
	
	OpenDB()

	customer_id = Request.Form("hid_CUSTOMERID")
	tenancy_id = Request.Form("hid_TENANCYID")	
	action = Request("action")
	intAddedDetails = Request("AddedDetails")
	
	If isNull(intAddedDetails) or intAddedDetails = "" Then intAddedDetails = "0" End If
	
	Dim DataFields   (19)
	Dim DataTypes    (19)
	Dim ElementTypes (19)
	Dim FormValues   (19)
	ReDim FormFields (19)
	UpdateID	  = "hid_ADDRESSID"
	FormFields(0) = "txt_HOUSENUMBER|TEXT"
	FormFields(1) = "txt_TEL|TEXT"
	FormFields(2) = "txt_ADDRESS1|TEXT"
	FormFields(3) = "txt_MOBILE|TEXT"
	FormFields(4) = "txt_ADDRESS2|TEXT"
	FormFields(5) = "txt_EMAIL|TEXT"
	FormFields(6) = "txt_ADDRESS3|TEXT"
	FormFields(7) = "txt_TOWNCITY|TEXT"
	FormFields(8) = "txt_ISDEFAULT|INT"
	FormFields(9) = "txt_POSTCODE|TEXT"
	FormFields(10) = "txt_ISPOWEROFATTORNEY|INT"
	FormFields(11) = "txt_COUNTY|TEXT"
	FormFields(12) = "hid_CUSTOMERID|INT"
	FormFields(13) = "hid_ADDRESSTYPE|INT"
	FormFields(14) = "txt_TELWORK|TEXT"
	FormFields(15) = "txt_MOBILE2|TEXT"
	
	
	//Added Details//
	If	intAddedDetails = "1" Then
	        FormFields(16) = "txt_RELATIONSHIP|TEXT"
			FormFields(17) = "txt_CONTACTFIRSTNAME|TEXT"
			FormFields(18) = "txt_CONTACTSURNAME|TEXT"
			FormFields(19) = "txt_CONTACTDOB|DATE"
	Else
	    FormFields(16) = "txt_TELRELATIONSHIP|TEXT"
	    FormFields(17) = "txt_TELRELATIVE|TEXT"
		ReDim Preserve FormFields(17)
	End If 
	//-------------//
    
	do_contacts()
		
	CloseDB()

	// deals with Direct debit payments
	Function do_contacts()
		
		Dim str_front_sql
		If action = "NEW" Then
			new_record()
		ElseIf action = "AMEND" Then
			amend_record()
		End If	
	
	End Function
	
	Function amend_record()
	
		// ENSURE ONLY ONE POWER OF ATTORNEY AND ONE CORRESPONDENCE ADDRESS
		check_checks()
		
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE C_ADDRESS " & strSQL & " WHERE ADDRESSID = " & ID
		
		//Response.Write SQL
		Conn.Execute SQL, recaffected
		'Response.write strSQL
		Response.Redirect "../iFrames/icontact.asp?customerid="&customer_id&"&tenancyid="&tenancy_id
	End Function

	
	// creates all kinds of new records depending on SQL
	Function new_record()
		
		// ENSURE ONLY ONE POWER OF ATTORNEY AND ONE CORRESPONDENCE ADDRESS
		check_checks()
		
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO C_ADDRESS " & strSQL & ""
		//Response.Write SQL
		Conn.Execute(SQL)

		Response.Redirect "../iFrames/icontact.asp?customerid="&customer_id&"&tenancyid="&tenancy_id

	End Function

	Function check_checks()
	
		If Request.form("txt_ISDEFAULT") = 1 Then
			Conn.Execute( "UPDATE C_ADDRESS SET ISDEFAULT = 0 WHERE CUSTOMERID = " & customer_id )
		End If
		If Request.form("txt_ISPOWEROFATTORNEY") = 1 Then
			Conn.Execute( "UPDATE C_ADDRESS SET ISPOWEROFATTORNEY = 0 WHERE CUSTOMERID = " & customer_id )
		End If
		
	End Function
	
	
	
%>
<html>
<head></head>
<script language=javascript>
	function ReturnData(){
		
//		parent.parent.STARTLOADER('TOP')
		//parent.location.href = "../iFrames/icontact.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
		}
</script>
<body onload="ReturnData()">
</body>
</html>