<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, grade_id, page_id, sel_width

	grade_id = Request("GRADEID")
    page_id = Request("PAGE_ID")

    If grade_id = "" Then grade_id = -1 End If

    If page_id="UPDATE" Then
        sel_width = "style=""width:250px"""
    ElseIf page_id="REPORT" Then
        sel_width = "style=""width:200px"""
    Else
        sel_width = "style=""width:300px"""
    End If

	Call OpenDB()
	Call build_select()

	Function build_select()

		sql = "SELECT CATEGORYID, DESCRIPTION FROM C_ASB_CATEGORY_BY_GRADE WHERE GRADEID = " & grade_id
		Call OpenRs(rsSet, sql)

		lst = "<select name=""sel_CATEGORY"" id=""sel_CATEGORY"" class=""textbox200"" " & sel_width & ">"
		lst = lst & "<option value=''>Please select Category</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name=""sel_CATEGORY"" id=""sel_CATEGORY"" class=""textbox200"" " & sel_width & " disabled=""disabled""><option value=''> No category attached</option></select>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > ABS Server Grade</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvCategory").innerHTML = document.getElementById("dvCategory").innerHTML;
        }
</script>
</head>
<body onload="ReturnData()">
    <div id="dvCategory">
        <%=lst%>
    </div>
</body>
</html>
</body>
</html>