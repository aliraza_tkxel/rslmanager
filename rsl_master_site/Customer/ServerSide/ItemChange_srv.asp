<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, str_nature, lst

	item_id = Request("itemid")
	If item_id = "" then item_id = 0 end if

	mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM C_NATURE WHERE ITEMID = " & item_id &_
				" AND ITEMNATUREID NOT IN (10,15,16,2,20,22,21,31,34,35,36,37,38,39,40,42,45,47,49) ORDER BY DESCRIPTION "
	Call OpenDB()
	Call build_select(str_nature, mysql)
	Call CloseDB()

	Function build_select(ByRef lst, ftn_sql)

		Call OpenRs(rsSet, ftn_sql)
        lst = "<select name=""sel_NATURE"" id=""sel_NATURE"" class=""textbox200"" onchange=""setTitleOrListBox(this.value)"">"
		lst = lst & "<option value=''>Please select</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet("ITEMNATUREID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 then
			lst = "<select name=""sel_NATURE"" id=""sel_NATURE"" class=""textbox200""><option value=''>Select Item</option></select>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Item Change</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvNature").innerHTML = document.getElementById("dvNature").innerHTML;
            parent.frm_nature.location.href = "../blank.asp";
        }
    </script>
</head>
<body onload="ReturnData()">
    <span id="dvNature">
        <%=str_nature%>
    </span>
</body>
</html>
