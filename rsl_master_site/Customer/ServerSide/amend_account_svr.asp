<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	' CREATED BY  	: PAUL PATTERSON
	' DATE			: 10 MARCH 2005
	' MODIFIED BY	: 
	' MODIFIED DATE	:
	' PURPOSE		:				
	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================

		Dim JID, ACTION, PAGEERROR, ERRORMESSAGE, SID
		Dim TDate, IType, PType, SDate, EDate, Amount, chq, office, orig_amount
		Dim nom_txnid, nom_transactionid, nom_value, nom_desc, nom_db_Acc, nom_cr_Acc, sign, orig_sign
		Dim orig_Ptype
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	OpenDB()

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	
		' GET RE5QUEST VARIABLES
		JID		= Request("JOURNALID")
		TID 	= Request("tenancyid")
		CID		= Request("customerid")
		ACTION	= Request("ACTION")
		
		PAGEERROR = false
		ERRORMESSAGE = ""
		
		TDate	= Request("txt_TRANSACTIONDATE")
		IType	= Request("sel_ITEMTYPE")
		PType	= Request("sel_PAYMENTTYPE")
		SDate	= Request("txt_STARTDATE")
		EDate	= Request("txt_ENDDATE")
		chq		= Request("txt_CHQNUMBER")
		office	= Request("sel_OFFICE")
		Amount	= Request("txt_AMOUNT")
		orig_amount	= Request("hid_ORIGAMOUNT")
		orig_sign  = Request("hid_ORIGSIGN")
		orig_Itype = Request("hid_ORIGITYPE")
		orig_Ptype = Request("hid_ORIGPTYPE")
		orig_tdate = Request("hid_ORIGTDATE")
	
	'===========================================================================================
	'	End Request Values from form or page
	'===========================================================================================	


	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	
	
	'GET AMOUNT SIGN OF NEW TYPE IF CHANGED
	sign = new_sign_of_amount(PType)
	
	' ORGANISE VARIABLES
	If SDate = "" Then SDate = "NULL" Else SDate = "'" & SDate & "'" End If
	If EDate = "" Then EDate = "NULL" Else EDate = "'" & EDate & "'" End If
	If office = "" Then office = "NULL" End If

	'===========================================================================================
	'	End Initialise variables
	'===========================================================================================	

	'===========================================================================================
	'	Start Call Functions
	'===========================================================================================	
	
		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
		' WHAT HAPPENS BELOW IS WE FIRST CHECK ANY RELATIONS TO THE CHANGED ITEM SUCH AS HB SCHEDULES, 
		' CASHPOSTING ETC AND FIRSTLY MAKE SURE THEY EXIST, IF THEY DO EXIST THEY ARE UPDATED ACCORDINGLY.
		' IF THEY DO NOT EXIST THEN ERROR IS RETURNED. WE THEN CHECK TO MAKE SURE THAT THERE IS AN ASSOC
		' ENTRY IN THE NOMINAL. IF NOT ERROR I9 RETURNED. THE NEXT STEP IS TO SAVE A COPY OF THE ORIGINAL
		' ACCOUNT ENTRY, UPDATE THE ORIGINAL AND THEN SAVE A COPY OF THE CHANGES MADE. WE THEN REVERSE
		' OUT THE ORIGINAL NOMINAL ENTRY. NEXT STEP IS DEPENDENT ON THE ITEM TYPE AND ORIGINAL  ITEMTYPE, 
		' IF RECHARGE TO RENT WE MUST REMOVE THE SALES PAYMENT ENTRY THEN CREATE THE NEW NOMINAL ENTRY. 
		' THE CR AND DB SIDE OF THESE ENTRIES ARE DETERMINED BY THE VALUE OF SIGN. 1 REPRESENTS REFUNDS AND 
		' 0 REPRESENTS RECEIPTS. FOR TYPE FROM RENT TO RECHARGE WE MUST INSERT A ROW IN SALES PAYMENT TABLE. 
		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
	
		Call update_relations()			'	CHECK TO SEE IF CORRECT ASSOC FINANCE ROWS EXIST AND IF SO ADJUST ELSE ERROR
		Call check_nominal()			'	CHECK TO SEE IF CORRECT ASSOC NOMINAL ROWS EXIST
		

	
		If Not(PAGEERROR) Then
			Call save_changes()
			Call cancel_orig_nominal(orig_Itype)
			
			If Cint(IType) = 1 Then													' RENT PAYMENT
				If Cint(orig_Itype) = 5  OR Cint(orig_Itype) = 13 Then 
				    Call delete_sales_payment() 
				End If		' FROM RENT TO RECHARGE-DELETE RECHARGE ENTRY
				If ACTION = "AMENDED" Then											' IF AMENDMENT
					If CInt(sign) = 0 Then 											' RECEIPT
						Call create_new_account_entry(get_accountid("BANKCURRENTACCOUNTID"), get_accountid("RENTCONTROLACCOUNTID"))
					Else															' REFUND
						Call create_new_account_entry(get_accountid("RENTCONTROLACCOUNTID"), get_accountid("BANKCURRENTACCOUNTID"))					
					End If
				End If
				
			ElseIf Cint(IType) = 5 Then												' RECHARGE ENTRY 
				If Cint(orig_Itype) = 1 Then Call insert_sales_payment(IType) End If		' FROM RECHARGE TO RENT-CREATE NEW SALES PAYMENT ENTRY FOR RECHARGE
					If Cint(orig_Itype) = 5 And ACTION = "AMENDED" Then										' IF AMENDMENT
						If CInt(sign) = 0 Then 										' RECEIPT
							Call create_new_account_entry(get_accountid("BANKCURRENTACCOUNTID"), get_accountid("SALESCONTROLACCOUNTID"))
						Else														' REFUND
							Call create_new_account_entry(get_accountid("SALESCONTROLACCOUNTID"), get_accountid("BANKCURRENTACCOUNTID"))					
						End If
					End If
			ElseIf Cint(IType) = 13 Then												' RECHARGE ENTRY 
				If Cint(orig_Itype) = 1 Then Call insert_sales_payment(IType) End If		' FROM RECHARGE TO RENT-CREATE NEW SALES PAYMENT ENTRY FOR RECHARGE
					If Cint(orig_Itype) = 13 And ACTION = "AMENDED" Then										' IF AMENDMENT
						If CInt(sign) = 0 Then 										' RECEIPT
							Call create_new_account_entry(get_accountid("BANKCURRENTACCOUNTID"), get_accountid("SALESCONTROLACCOUNTID"))
						Else														' REFUND
							Call create_new_account_entry(get_accountid("SALESCONTROLACCOUNTID"), get_accountid("BANKCURRENTACCOUNTID"))					
						End If
					End If		
			End If
			response.redirect "../iframes/iRentAccount.asp?customerid=" & CID & "&tenancyid=" & TID & "&SyncTabs=1"
		End If
			
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
		
	CloseDB()
 
	'===========================================================================================
	'	Function List
	'===========================================================================================	

	
			' WE MUST AMEND DEPENDENT TABLES IF WE ARE AMENDING ROWS WITHIN THE CRM ACCOUNT
			Function update_relations()
			
				Select Case cint(PType)
					Case 1 		'HB
						Call update_hb_sched()
					Case 3 		' STANDING ORDER
						Call update_standingorder_data(ACTION)
					Case 5,8,92,93 	' CASH/CHEQUE 
						Call update_cashposting(ACTION)
					Case 13		' PAYMENTCARD
						Call update_paymentcard_data(ACTION)
				End Select
				
			End Function
			
		'	WE NEED TO DETERMINE THE SIGN OF THE NEW PAYMENT TYPE - BUT ONLY IF IT HAS CHANGED
			Function new_sign_of_amount(payment_type)
			
				Dim myreturn
				SQL = "SELECT REFUND FROM F_PAYMENTTYPE WHERE PAYMENTTYPEID = " & PType
				Call OpenrS(rsSet, SQL)
				if not rsSet.EOF Then 
					myreturn = rsSet(0) 
				Else 
					myreturn = -1 
				End If
				CloseRs(rsSet)
				new_sign_of_amount = myreturn

			End Function
			
		'	CHECK TO SEE IF THERE IS AN ASSOCIATED NOMINAL ACCOUNT	
			Function check_nominal()
		
		'		WE NEED TO DETERMINE IF WE ARE CHECKING FOR A SALES NOMINAL ENTRY OF A RENT NOMINAL ENTRY
		'		CREATE SQL CRITERIA ACCORDINGLY
				If Cint(orig_Itype) = 5 OR Cint(orig_Itype) = 13 Then
		'			GET ROWID OF SALESPAYMENT ROW TO MATCH TO NOMINAL TRANSACTIONID  
					SQL = "SELECT ROWID FROM F_SALESPAYMENT WHERE JOURNALID = " & JID
					Call OpenRs(rsSet, SQL)
					If Not rsSet.Eof Then SID = rsSet(0) Else SID = -1 End If
					CloseRs(rsSet)
					extra_sql = " WHERE TRANSACTIONTYPE IN (4,16) AND TRANSACTIONID = " & SID
				ElseIf Cint(orig_Itype) = 1 Then
					extra_sql = " WHERE TRANSACTIONTYPE IN (8,16)  AND TRANSACTIONID = " & JID
				End If
				
		'		CHECK THAT ASSOC RECORD EXISTS AND THAT THERE IS ONLY ONE OF THEM
				SQL = "SELECT COUNT(*) FROM NL_JOURNALENTRY " & extra_sql
				Call OpenRs(rsSet, SQL)
				if not rsSet.EOF Then rowcount = rsSet(0) Else rowcount = 0 End If
				If rowcount < 1 Then 
					ERRORMESSAGE = "There is a problem with the associated nominal entry.\nContact system administrator"
					PAGEERROR	 = True 
					Exit Function
				End If			
				CloseRs(rsSet)		
			
			End Function
		
			Function delete_sales_payment()
			    
				SQL = "DELETE F_SALESPAYMENT WHERE JOURNALID = " & JID
				Conn.execute(SQL)
						
			End Function 
			
			Function insert_sales_payment(IType)
			
			    if IType = 13 then ' legal recharge
				SQL = "INSERT INTO F_SALESPAYMENT " &_
						"(JOURNALID, TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, " &_
						"ISDEBIT, STATUSID, ACCOUNTTIMESTAMP) " &_
						" SELECT JOURNALID, TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, ISDEBIT, STATUSID, ACCOUNTTIMESTAMP FROM F_rENTJOURNAL " &_
						" WHERE ITEMTYPE = 13 AND PAYMENTTYPE IS NOT NULL AND JOURNALID = " & JID
				else
				    SQL = "INSERT INTO F_SALESPAYMENT " &_
						"(JOURNALID, TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, " &_
						"ISDEBIT, STATUSID, ACCOUNTTIMESTAMP) " &_
						" SELECT JOURNALID, TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, ISDEBIT, STATUSID, ACCOUNTTIMESTAMP FROM F_rENTJOURNAL " &_
						" WHERE ITEMTYPE = 5 AND PAYMENTTYPE IS NOT NULL AND JOURNALID = " & JID
				end if		
				Conn.execute(SQL)
			
			End Function
			
			' CHANGE THE SIGN OF THE AMOUNT DEPENDENT ON REFUND VALUE IN PAYMENTTYPE TABLES
			' 1 = REFUND -- 0 = RECEIPT
			Function signage(iswhat, Dvalue)
			
				If cint(iswhat) =  1 Then
					signage = abs(Dvalue)
				Else
					signage = -abs(Dvalue)
				End If
			
			End Function
			
		'	RETURNS THE ID OF THE PASSED ACCOUNT FROM RSLDEFAULTS USING THE DEFAULT NAME AS CRITERIA	
			Function get_Accountid(defaultName)
			
				Dim myreturn
				SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = '"&defaultName&"'"
				Call OpenRs(rsSet, SQL)
				if not rsSet.EOF Then 
					myreturn = rsSet(0)
				Else
					myreturn = -1	
				End If
				CloseRs(rsSet)
				get_Accountid = myreturn

			End Function
			
			Function save_changes()
			
		'		WE STORE A COPY OF THE ORIGINAL IN THE AMEND TABLE TO ACT AS A PARENT FOR THE ACTUAL AMENDS
		'		THIS IS TO ENABLE US TO DISPLAY CLEARY WHAT ACTIONS HAVE TAKEN PLACE ON THE ROW
		'		ORIGINALLY PLAN WAS TO USE THE RENT JOURNAL BUT THIS IS NO GOOD FOR CANCELLATIONS AS THE ROW IS GONE AND THE JOIN LOST
		'		BUT ONLY IF IT DOESN'T ALREADY EXIST
				SQL = "SELECT * FROM F_RENTJOURNALAMEND WHERE ORIGINAL = 1 AND JOURNALID = " & JID
				Call OpenRs(rsSet, SQL) 
				If rsSet.EOF Then
				Conn.execute ("INSERT INTO F_RENTJOURNALAMEND SELECT JOURNALID,TENANCYID,TRANSACTIONDATE,ITEMTYPE, PAYMENTTYPE,PAYMENTSTARTDATE, PAYMENTENDDATE, AMOUNT, ISDEBIT,STATUSID,ACCOUNTTIMESTAMP,QBSENT, 1, " & Session("USERID") & ", NULL, NULL, NULL, GETDATE() FROM F_RENTJOURNAL WHERE JOURNALID = " & JID)
				End If
				CloseRs(rsSet)
		
		'		RECORD CANCELLATION
				If ACTION = "CANCELLED" Then
					SQL = "INSERT INTO F_RENTJOURNALAMEND SELECT JOURNALID,TENANCYID,TRANSACTIONDATE,ITEMTYPE, PAYMENTTYPE,PAYMENTSTARTDATE, PAYMENTENDDATE,AMOUNT,ISDEBIT, STATUSID,ACCOUNTTIMESTAMP,QBSENT, 0, " & Session("USERID") & ", '" & chq & "', " & office & ", '" & ACTION & "', GETDATE() FROM F_RENTJOURNAL WHERE JOURNALID = " & JID
					Conn.execute(SQL)
				End If
				
		'	 	UPDATE ORIGINAL ENTRY IN RENT JOURNAL
				If ACTION = "AMENDED" Then		
					SQL = "UPDATE F_RENTJOURNAL SET TRANSACTIONDATE = '" & TDate & "', ITEMTYPE = " & IType &_
							", PAYMENTTYPE = " & PType & ", PAYMENTSTARTDATE = " & SDate &_
								", PAYMENTENDDATE = " & EDate & ", AMOUNT = " & signage(sign, Amount) &_ 
									" WHERE JOURNALID = " & JID
				Else
					SQL = "DELETE F_RENTJOURNAL WHERE JOURNALID = " & JID
				End If
				Conn.execute(SQL)
		
		'		RECORD AMENDMENT OF ORIGINAL IN F_RENTJOURNALAMEND TABLE
				If ACTION = "AMENDED" Then
					SQL = "INSERT INTO F_RENTJOURNALAMEND SELECT JOURNALID,TENANCYID,TRANSACTIONDATE, ITEMTYPE,PAYMENTTYPE, PAYMENTSTARTDATE, PAYMENTENDDATE, AMOUNT,ISDEBIT,STATUSID,ACCOUNTTIMESTAMP,QBSENT, 0, " & Session("USERID") & ", '" & chq & "', " & office & ", '" & ACTION & "', GETDATE() FROM F_RENTJOURNAL WHERE JOURNALID = " & JID
					Conn.execute(SQL)
				End If
			
			End Function
			
			' UPDATE ORIGINAL ENTRY IN RENT JOURNAL
			Function update_original(DoWhat)
			
				If DoWhat = "AMENDED" Then		
					SQL = "UPDATE F_RENTJOURNAL SET TRANSACTIONDATE = '" & TDate & "', ITEMTYPE = " & IType &_
							", PAYMENTTYPE = " & PType & ", PAYMENTSTARTDATE = " & SDate &_
								", PAYMENTENDDATE = " & EDate & ", AMOUNT = " & signage(sign, Amount) &_ 
									" WHERE JOURNALID = " & JID
				Else
					SQL = "DELETE F_RENTJOURNAL WHERE JOURNALID = " & JID
				End If
				Conn.execute(SQL)
			
			End Function
		
		'	CREATE A HISTORICAL RECORD OF ENTRY ABOUT TO BE AMENDED
			Function insert_history()
			
		'		WE STORE A COPY OF THE ORIGINAL IN THE AMEND TABLE TO ACT AS A PARENT FOR THE ACTUAL AMENDS
		'		THIS IS TO ENABLE US TO DISPLAY CLEARY WHAT ACTIONS HAVE TAKEN PLACE ON THE ROW
		'		ORIGINALLY PLAN WAS TO USE THE RENT JOURNAL BUT THIS IS NO GOOD FOR CANCELLATIONS AS THE ROW IS GONE AND THE JOIN LOST
		'		BUT ONLY IF IT DOESN'T ALREADY EXIST
				SQL = "SELECT * FROM F_RENTJOURNALAMEND WHERE ORIGINAL = 1 AND JOURNALID = " & JID
				Call OpenRs(rsSet, SQL) 
				If rsSet.EOF Then
					Conn.execute ("INSERT INTO F_RENTJOURNALAMEND SELECT *, 1, " & Session("USERID") & ", NULL, NULL, NULL, GETDATE() FROM F_RENTJOURNAL WHERE JOURNALID = " & JID)
				End If
				CloseRs(rsSet)
			
				SQL = "INSERT INTO F_RENTJOURNALAMEND SELECT *, 0, " & Session("USERID") & ", '" & chq & "', " & office & ", '" & ACTION & "', GETDATE() FROM F_RENTJOURNAL WHERE JOURNALID = " & JID
				Conn.execute(SQL)
					
			End Function
			
		'	REVERSE ORIGINAL NOMINAL ENTRY	
			Function cancel_orig_nominal(item)
			
		'		WE NEED TO CANCEL MOST RECENT ACCOUNT IN RELEVANT ACCOUNT
				If Cint(orig_Itype) = 5 or Cint(orig_Itype) = 13 Then
					extra_sql = " WHERE CR.ACCOUNTID = " & get_accountid("SALESCONTROLACCOUNTID") &_
									" AND TRANSACTIONTYPE IN (4,16) AND TRANSACTIONID = " & SID
				ElseIf Cint(orig_Itype) = 1 Then
		'		THER IS AN EXTRA LINE IN THIS SQL BECAUSE RENT CONTROL CAN BE DEBITED OR CREDITED DEPENDENT ON TYPE		
					extra_sql = " WHERE (CR.ACCOUNTID = " & get_accountid("RENTCONTROLACCOUNTID") &_
								  " OR DB.ACCOUNTID = " & get_accountid("RENTCONTROLACCOUNTID") &_
									") AND TRANSACTIONTYPE IN (8,16) AND TRANSACTIONID = " & JID
					
				End If
		
		'		GET ORIGINAL NOMINAL DETAILS OF MOST RECENT RELATED ENTRY
				SQL = " SELECT TOP 1 J.TXNID, J.TRANSACTIONID AS JID, DB.AMOUNT, DB.DESCRIPTION, DB.ACCOUNTID AS DB_ACC, "&_
						" CR.ACCOUNTID AS CR_ACC, J.TXNDATE AS TDATE " &_
						" FROM 	NL_JOURNALENTRY J " &_
						"		INNER JOIN NL_JOURNALENTRYCREDITLINE CR ON CR.TXNID = J.TXNID " &_
						"		INNER JOIN NL_JOURNALENTRYDEBITLINE DB ON DB.TXNID = J.TXNID " & extra_sql &_
						" ORDER BY J.TXNID DESC " 
				rw SQL
				Call OpenRs(rsSet, SQL)
				if not rsSet.EOF Then 
					nom_txnid			= rsSet("TXNID")
					nom_transactionid	= rsSet("JID")
					nom_value			= abs(rsSet("AMOUNT"))
					nom_desc			= rsSet("DESCRIPTION")
					nom_db_Acc			= rsSet("DB_ACC")
					nom_cr_Acc			= rsSet("CR_ACC")
					nom_tdate			= rsSet("TDATE")
				End If
				CloseRs(rsSet)
						
		' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ADJUSTMENT ENTRY 
				Set comm = Server.CreateObject("ADODB.Command")
				comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
				comm.commandtype = 4
				Set comm.activeconnection = Conn
				comm.parameters(1) = 16
				comm.parameters(2) = jid
				comm.parameters(3) = NOW
				comm.parameters(4) = nom_tdate
				comm.parameters(5) = NULL
				comm.parameters(6) = NULL		
				comm.execute
				adj_txnid = comm.parameters(0)
				
'				ENTER CREDIT AND DEBIT LINES		
				For i=1 To 2
					if i = 1 Then 
						cmdText = "F_INSERT_NL_JOURNALENTRYDEBITLINE" 
						accid = nom_cr_Acc
					Else 
						cmdText = "F_INSERT_NL_JOURNALENTRYCREDITLINE" 
						accid = nom_db_Acc
					End If
					
					comm.commandtext = cmdText
					comm.parameters(1) = Clng(adj_txnid)
					comm.parameters(2) = accid
					comm.parameters(3) = NOW
					comm.parameters(4) = Cint(i)
					comm.parameters(5) = nom_tdate
					comm.parameters(6) = nom_value
					comm.parameters(7) = TID & ": " & get_paymenttype_description(orig_Ptype) & " Cancellation"
					comm.parameters(8) = "Customer account adjustment for JournalID: " & JID & " Tenancy No: " & TID & " Customer No: " & CID
					If i = 1 Then comm.parameters(9) = null end if
					comm.execute
				Next
		
			End Function
			
		'	CREATE NEW ENTRY IN RENT ACCOUNT/BANK ACCOUNT
			Function create_new_account_entry(db, cr)
			
		' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ADJUSTMENT ENTRY 
				Set comm = Server.CreateObject("ADODB.Command")
				comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
				comm.commandtype = 4
				Set comm.activeconnection = Conn
				comm.parameters(1) = 16
				comm.parameters(2) = JID
				comm.parameters(3) = NOW
				comm.parameters(4) = TDate
				comm.parameters(5) = NULL
				comm.parameters(6) = NULL		
				comm.execute
				adj_txnid = comm.parameters(0)
				
		'		ENTER CREDIT AND DEBIT LINES		
				For i=1 To 2
					if i = 1 Then 
						cmdText = "F_INSERT_NL_JOURNALENTRYDEBITLINE" 
						accid = db
					Else 
						cmdText = "F_INSERT_NL_JOURNALENTRYCREDITLINE" 
						accid = cr
					End If

'					BUILD DESCRIPTION STRING FOR NOMINAL WITH REGARDS TO PAYMENT TYPE CHANGE
					If Cint(orig_Ptype) <> CInt(PType) Then				' PAYMENTTYPE HAS CHANGED
						str_pt = get_paymenttype_description(orig_Ptype) & " to " & get_paymenttype_description(Ptype) & " "
					End If

'					BUILD DESCRIPTION STRING FOR NOMINAL WITH REGARDS TO AMOUNT CHANGE
					If CLng(orig_amount) <> CLng(Amount) Then			' PAYMENTTYPE HAS CHANGED
						str_amount = FormatCurrency(orig_amount) & " to " & FormatCurrency(Amount) & " "
					End If

'					BUILD DESCRIPTION STRING FOR NOMINAL WITH REGARDS TO AMOUNT CHANGE
					If orig_tdate <> TDate Then				' PAYMENTTYPE HAS CHANGED
						str_tdate = orig_tdate & " to " & TDate & " "
					End If
					str_desc = TID & ": " & str_pt & str_amount & str_tdate & "Amendment"
					comm.commandtext = cmdText
					comm.parameters(1) = Clng(adj_txnid)
					comm.parameters(2) = accid
					comm.parameters(3) = formatDateTime(NOW ,2)
					comm.parameters(4) = Cint(i)
					comm.parameters(5) = TDate
					comm.parameters(6) = ABS(Amount)
					comm.parameters(7) = str_desc
					comm.parameters(8) = "Customer account adjustment for JournalID: " & JID & " Tenancy No: " & TID & " Customer No: " & CID
					If i = 1 Then comm.parameters(9) = null end if
					comm.execute
				Next
		
			End Function
			
		'	RETURN DESCRIPTION OF PAYMENT TYPE BASED ON PAYMENTTYPEID	
			Function get_paymenttype_description(paymenttypeid)
			
				RW "pt" & paymenttypeid & "<br>"
				SQL = "SELECT DESCRIPTION FROM F_PAYMENTTYPE WHERE PAYMENTTYPEID = " & paymenttypeid
				Call openRs(rsSet, SQL)
				If Not rsSet.EOF Then myreturn = rsSet(0) Else myreturn = "Unknown payment type" End If
				CloseRs(rsSet)
				get_paymenttype_description = myreturn
				
			End Function
			
		'	UPDATE HBACTUALSCHED ONLY IF RELATED ROW  EXISTS OTHER WISE SET ERROR	
			Function update_hb_sched()
			
		'		CHECK THAT ASSOC RECORD EXISTS AND THAT THERE IS ONLY ONE OF THEM
				SQL = "SELECT COUNT(*) FROM F_HBACTUALSCHEDULE WHERE JOURNALID = " & JID
				Call OpenRs(rsSet, SQL)
				if not rsSet.EOF Then rowcount = rsSet(0) Else rowcount = 0 End If
				If cint(rowcount) <> 1 Then 
					ERRORMESSAGE = "There is a problem with the associated HB schedule.\nContact system administrator"
					PAGEERROR	 = True 
					Exit Function
				End If			
				CloseRs(rsSet)		
		'		UPDATE ASSOC ROW IN HBACTUALSCHEDULE		
				SQL = " UPDATE F_HBACTUALSCHEDULE SET STARTDATE = "&SDate&", ENDDATE = "&EDate&", HB = "&-abs(Amount)&" WHERE JOURNALID = " & JID
				Conn.execute(SQL)
			
			End Function
			
		'	WE MUST FIRST CHECK TO SEE IF THIS ITEM WAS ENTERED VIA THE DIRECT POSTING TOOL. IF SO DO NOTHING
		'	IF IT WAS ENTERED VIA A FILE UPLOAD THEN WE MUST AMEND THE ASSOC ROWS WHICH AFFECT THE STANDING ORDER
		'	PAYMENT CALENDAR, NAMELY F_STANDINGORDERDATA AND F_STANDINGORDERFILES	
			Function update_standingorder_data(DoWhat)
			
		'		 CHECK IF THIS ITEM WAS ENTERED VIA THE DIRECT POSTING TOOL - IF SO NO FURTHER WORK
				SQL = "SELECT * FROM F_STANDINGORDERDATA WHERE JOURNALID = " & JID
				Call OpenRs(rsSet, SQL)
				if rsSet.EOF Then Exit Function End If 
		
				CloseRs(rsSet)
		'		UPDATE STANDING ORDER DATA
				If DoWhat = "AMENDED" Then		
					SQL = "UPDATE F_STANDINGORDERFILES SET PROCESSVALUE = PROCESSVALUE - " & abs(orig_amount) & " + " & abs(Amount) &_
						  " FROM	F_STANDINGORDERFILES F " &_
							" INNER JOIN F_STANDINGORDERDATA D ON D.FILEID = F.FILEID AND D.JOURNALID = " & JID
				Else 
					SQL = "UPDATE F_STANDINGORDERFILES SET PROCESSVALUE = PROCESSVALUE - " & abs(Amount) &_
							" , PROCESSCOUNT = PROCESSCOUNT - 1 " &_
							 " FROM	F_STANDINGORDERFILES F " &_
							   " INNER JOIN F_STANDINGORDERDATA D ON D.FILEID = F.FILEID AND D.JOURNALID = " & JID
				End If
				Conn.Execute(SQL)
			
			End Function
			
		'	IF A CASH OR CHEQUE POSTING IS AMENDED THEN WE MUST AMEND THE ASSOC ROW IN THE TABLE F_CASHPOSTING
		'	WE CHECK FIRST TO SEE IF ASSOC ROW EXISTS - IF NOT THEN WE RETURN ERROR AND EXIT	
			Function update_cashposting(DoWhat)
			
		'		CHECK THAT ASSOC RECORD EXISTS AND THAT THERE IS ONLY ONE OF THEM
				SQL = "SELECT COUNT(*) FROM F_CASHPOSTING WHERE JOURNALID = " & JID
				Call OpenRs(rsSet, SQL)
				if not rsSet.EOF Then rowcount = rsSet(0) Else rowcount = 0 End If
				If cint(rowcount) <> 1 Then 
					ERRORMESSAGE = "There is a problem with the associated cash posting record.\nContact system administrator"
					PAGEERROR	 = True 
					Exit Function
				End If			
				CloseRs(rsSet)		
		'		UPDATE ASSOC ROW IN F_CASHPOSTING		
				If DoWhat = "AMENDED" Then		
					SQL = " UPDATE F_CASHPOSTING SET OFFICE = " & office & ", ITEMTYPE = " & IType & ", PAYMENTTYPE = " & PType &_
									", AMOUNT = " & abs(AMount) & ", CHQNUMBER = '" & chq & "' WHERE JOURNALID = " & JID
				Else
					SQL = " DELETE F_CASHPOSTING WHERE JOURNALID = " & JID
				End If
				Conn.execute(SQL)
			
			End Function
			
		'	WE MUST AMEND THE ASSOC ROWS WHICH AFFECT THE PAYMENT CARD ITEM
		'	PAYMENT CALENDAR, NAMELY F_PAYMENTCARDDATA AND F_PAYMENTCARDFILES	
			Function update_paymentcard_data(DoWhat)
			
		'		UPDATE PAYMENT CARD DATA		
				If DoWhat = "AMENDED" Then		
					SQL = "UPDATE F_PAYMENTCARDFILES SET PROCESSVALUE = PROCESSVALUE - " & abs(orig_amount) & " + " & abs(Amount) &_
						  " FROM	F_PAYMENTCARDFILES F " &_
							" INNER JOIN F_PAYMENTCARDDATA D ON D.FILEID = F.FILEID AND D.JOURNALID = " & JID
				Else
					SQL = "UPDATE F_PAYMENTCARDFILES SET PROCESSVALUE = PROCESSVALUE - " & abs(Amount) &_
							" , PROCESSCOUNT = PROCESSCOUNT - 1 " &_
							 " FROM	F_PAYMENTCARDFILES F " &_
							   " INNER JOIN F_PAYMENTCARDDATA D ON D.FILEID = F.FILEID AND D.JOURNALID = " & JID
				End If
				Conn.Execute(SQL)
			
			End Function
			
	'===========================================================================================
	'	End Function List
	'===========================================================================================	


%>
<html>
<head></head>
<script language=javascript>
	
//	CHECK FOR ANY ERRORS
	function loadMe(){
	
		var error = "<%=PAGEERROR%>"
		var message = "<%=ERRORMESSAGE%>"
		if (error == "True")
			alert(message);
		parent.CRM_BOTTOM_FRAME.location.href = "../iframes/iRentAccount.asp?customerid=<%=CID%>&tenancyid=<%=TID%>&SyncTabs=1"
	}
</script>
<body onload="loadMe()">
</body>
</html>
