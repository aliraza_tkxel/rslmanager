<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TenancyID = Request.Form("TENANCYID")
CustomerID = Request.Form("CUSTOMERID")
YearStart = Request.Form("HBStartDate1")
YearEnd   = Request.Form("HBEndDate13")
HBREF = Request.Form("txt_HBREF")
action = Request.Form("Action")
if (TenancyID = "" OR NOT isDate(YearStart)) then
	TenancyID = -1
end if

iYear = DatePart("yyyy", YearStart)

OpenDB()

if (action = "NEW") then
	EX_SQL = "SET NOCOUNT ON;INSERT INTO F_HBINFORMATION (CUSTOMERID, TENANCYID, HBREF, INITIALPAYMENT, INITIALSTARTDATE, INITIALENDDATE) " &_
							"VALUES (" & CustomerID &_
								 "," & TenancyID & " " &_							 							 
								 ",'" & HBREF & "'" &_							 							 
								 ",'" & Request.Form("txt_INITIAL") & "'" &_							 							 
								 ",'" & Request.Form("txt_STARTDATE") & "'" &_
								 ",'" & Request.Form("txt_ENDDATE") & "'" &_
								 ");SELECT SCOPE_IDENTITY() AS HBID;"
	Response.Write EX_SQL
	set rsSet = Conn.Execute(EX_SQL)
	if (NOT rsSet.EOF) then
		HBID = rsSet("HBID")
	end if

elseif (action = "UPDATE") then

	HBID = Request.Form("HBID")

	'if not validated rows, then update the initial information else do not
	if (Request("UPDATEHBINFO") = "") then
		EX_SQL = "UPDATE F_HBINFORMATION " &_
								"SET  HBREF = '" & HBREF & "'" &_							 							 
									 ", INITIALPAYMENT = '" & Request.Form("txt_INITIAL") & "'" &_							 							 
									 ", INITIALSTARTDATE = '" & Request.Form("txt_STARTDATE") & "'" &_
									 ", INITIALENDDATE = '" & Request.Form("txt_ENDDATE") & "'" &_
									 " WHERE TENANCYID = " & TenancyID & " AND CUSTOMERID = " & CustomerID & " AND HBID = " & HBID & " ;"
	else
		EX_SQL = "UPDATE F_HBINFORMATION " &_
								"SET  HBREF = '" & HBREF & "'" &_							 							 
									 " WHERE TENANCYID = " & TenancyID & " AND CUSTOMERID = " & CustomerID & " AND HBID = " & HBID & " ;"
	end if
	Response.Write EX_SQL
	Conn.Execute(EX_SQL)
end if

'END OF INITIAL INFORMATION INSERTION,,,,,,,,

if (action = "NEW") then
	For i=1 to 13
		IN_SQL = "INSERT INTO F_HBACTUALSCHEDULE (HBID, STARTDATE, ENDDATE, HB) " &_
						"VALUES (" & HBID &_
							 ",'" & Request.Form("HBStartDate" & i) & "'" &_
							 ",'" & Request.Form("HBEndDate" & i) & "'" &_
							 ",'" & Request.Form("HBHB" & i) & "'" &_							 							 
							 ")"
		Conn.execute (IN_SQL)
	Next
elseif (action = "UPDATE") then
	'DELETE ALL ITEMS FROM THE RENT JOURNAL WHICH ARE ALSO ESTIMATED
	SQL = "DELETE F_RENTJOURNAL " &_
			"FROM F_RENTJOURNAL RJ, F_HBACTUALSCHEDULE HBA, F_HBINFORMATION HBI " &_
			"WHERE RJ.JOURNALID = HBA.JOURNALID AND HBA.HBID = HBI.HBID AND HBI.CUSTOMERID = " & CustomerID & " AND HBI.TENANCYID = " & TenancyID & " " &_
			"AND RJ.STATUSID = 1 AND RJ.PAYMENTTYPE = 1 AND RJ.ITEMTYPE = 1"
	rESPONSE.wRITE sql
	Conn.Execute (SQL)

	'Delete all previous estimated values in the system
	SQL = "DELETE FROM F_HBACTUALSCHEDULE WHERE HBID = " & HBID & " AND VALIDATED IS NULL"
	Conn.Execute (SQL)
	
	For i=1 to 13
		IN_SQL = "INSERT INTO F_HBACTUALSCHEDULE (HBID, STARTDATE, ENDDATE, HB) " &_
						"VALUES (" & HBID &_
							 ",'" & Request.Form("HBStartDate" & i) & "'" &_
							 ",'" & Request.Form("HBEndDate" & i) & "'" &_
							 ",'" & Request.Form("HBHB" & i) & "'" &_							 							 
							 ")"
		Conn.execute (IN_SQL)
	Next
end if		

	// execute sp to create hb schedule entries
	Set Rs = Server.CreateObject("ADODB.Command")
	Set Rs.activeconnection = Conn
	Rs.commandtype = 4 
	RS.commandtimeout=0
	Rs.commandtext = "F_AUTO_HBSCHEDULE_ENTRY"
	Rs.execute

Response.Redirect "../CRM.asp?CustomerID=" & Request("CustomerID")
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000">

</body>
</html>
