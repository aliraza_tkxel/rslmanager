<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	
	'' Created By:	Munawar Nadeem(TkXel)
	'' Created On: 	June 27, 2008
	'' Reason:		Integraiton with Tenats Online, this page loads the development names 
	''			from table PDR_DEVELOPMENT, based on authorityID provided


	Dim authority_id, development_id, str_development, mysql
	
	authority_id = Request("AuthorityID")
	
	development_id = CINT(Request("DevelopmentID"))


	If authority_id = "" then authority_id = 0 end if

	

	OpenDB()

      mysql = "SELECT DEVELOPMENTID, DEVELOPMENTNAME FROM PDR_DEVELOPMENT WHERE LOCALAUTHORITY = " & authority_id & " ORDER BY DEVELOPMENTNAME"
	
	' call to function to read developement names from DB and to build a list
	Call build_select(str_development, mysql) 
	
	CloseDB()	

	' This function used to extract developments names and build list of them
	Function build_select(ByRef lst, sql)
		
		dim value

		Call OpenRs(rsSet, sql)

		lst = "<select name='sel_Development' class='textbox200' >"
		
		While NOT rsSet.EOF

			value = rsSet("DEVELOPMENTID")

			' One development is selected whtich matches development_id
			If value = development_id Then
		
				lst = lst & "<option value='" & value & "' selected='selected'>" & rsSet("DEVELOPMENTNAME") & "</option>"

			Else
				lst = lst & "<option value='" & value & "'>" & rsSet("DEVELOPMENTNAME") & "</option>"
			End if

			rsSet.movenext()
		
		Wend
		
		CloseRs(rsSet)				

		lst = lst & "</select>" 
						

	End Function

%>

<html>
<head></head>
<script language=javascript>

function ReturnData(){
	
	parent.RSLFORM.sel_Development.outerHTML = "<%=str_development%>";

	}

</script>
<body onload="ReturnData()">
</body>
</html>