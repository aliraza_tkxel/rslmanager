<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

TeamID = CStr(Request.Form("sel_TEAMS"))
Tenancyid=Request.Form("hid_TENANCYID")

If(Request.Form("hid_TENANCYID")="") then
    Tenancyid=Request.Form("tenancyid")
End if

if (TeamID = "") then
	TeamID = "-1"
end if

IF (Request("SMALL") = "1") THEN
    SELECT_WIDTH = "250"
ELSEIF(Request("SMALL") = "2") THEN
	SELECT_WIDTH = "200"
ELSE
	SELECT_WIDTH = "300"
END IF

IF (TeamID = "1") THEN
	SQL = "SELECT NAME, ORGID FROM S_ORGANISATION ORDER BY NAME"
	
	Dim rsOrgs
	OpenDB()
	Call OpenRs(rsOrgs, SQL)
	if (not rsOrgs.EOF) then
		OrgList = "<option value=''>Please Select</option>"
		while not rsOrgs.EOF
			OrgList = OrgList & "<option value='" & rsOrgs("ORGID") & "'>" & rsOrgs("NAME")& "</option>"
			rsOrgs.moveNext
		wend
	else
		OrgList = "<option value=''>No suppliers found...</option>"
	end if
	CloseRs(rsOrgs)
	CloseDB()
ELSE
	IF (TeamID = "NOT") THEN
		SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				" WHERE TEAM IS NULL AND L.ACTIVE = 1 AND J.ACTIVE = 1 ORDER BY FIRSTNAME, LASTNAME"
				
	ELSEIF(TeamID="-1") THEN 
		'SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
		'		" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1 ORDER BY FIRSTNAME, LASTNAME"
		 		
		SQL="   SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
            "   FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID "&_
            "   INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  "&_  
            "   WHERE L.ACTIVE = 1 "&_
	        "   AND J.ACTIVE = 1 "&_
	        "   AND ISNULL(J.PATCH,0)IN ( "&_
			"				SELECT PATCHID FROM E_PATCH "&_
			"				)   "&_
            "   ORDER BY FIRSTNAME, LASTNAME "	
			'"				SELECT ISNULL(PD.PATCHID,0) "&_
			'"				FROM C_TENANCY CT   "&_
			'"				INNER JOIN P__PROPERTY P ON P.PROPERTYID=CT.PROPERTYID  "&_
			'"           	INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=P.DEVELOPMENTID "&_
			'"				WHERE TENANCYID=" & Tenancyid & " "&_
			'"				)   "&_
            '"   ORDER BY FIRSTNAME, LASTNAME "			
     ELSE
            
		SQL="   SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
            "   FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID "&_
            "   INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  "&_ 
            "   INNER JOIN E_TEAM T ON J.TEAM = T.TEAMID "&_ 
            "   WHERE L.ACTIVE = 1 "&_
	        "   AND J.ACTIVE = 1 "&_
	        "   AND T.TEAMID = " & TeamID & " "&_
	        "   AND ISNULL(J.PATCH,0)=( "&_
			"				SELECT ISNULL(PD.PATCHID,0) "&_
			"				FROM C_TENANCY CT   "&_
			"				INNER JOIN P__PROPERTY P ON P.PROPERTYID=CT.PROPERTYID  "&_
			"           	INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=P.DEVELOPMENTID "&_
			"				WHERE TENANCYID=" & Tenancyid & " "&_
			"				)   "&_
            "   ORDER BY FIRSTNAME, LASTNAME "	
	END IF
	
	Dim rsEmployees
	OpenDB()
	
	Call OpenRs(rsEmployees, SQL)
	if (not rsEmployees.EOF) then
		EmployeeList = "<option value=''>Please Select</option>"
		while not rsEmployees.EOF
			EmployeeList = EmployeeList & "<option value='" & rsEmployees("EMPLOYEEID") & "'>" & rsEmployees("FULLNAME")& "</option>"
			rsEmployees.moveNext
		wend
	else
		EmployeeList = "<option value=''>No employees found...</option>"
	end if
	CloseRs(rsEmployees)
	CloseDB()
END IF
%>			
<html>
<head>
<title>Server Employee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JAVASCRIPT">
function SetEmployees(){
<% IF (TeamID = "1") THEN %>
	parent.RSLFORM.sel_USERS.outerHTML = Employees.innerHTML
<% ELSE %>
	parent.RSLFORM.sel_USERS.outerHTML = Employees.innerHTML
<% END IF %>
	}
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000" onload="SetEmployees()">
<div id="Employees">
<% IF (TeamID = "1") THEN %>
	<select name='sel_USERS' class='textbox200' STYLE='WIDTH:<%=SELECT_WIDTH%>PX' onchange="GetContractorContacts()">
<% ELSEIF(TeamID="-1") THEN %>
	<select name='sel_USERS' class='textbox200' STYLE='WIDTH:<%=SELECT_WIDTH%>PX'>
<%ELSE %>	
    <select name='sel_USERS' class='textbox200' STYLE='WIDTH:<%=SELECT_WIDTH%>PX' onchange="GetEmployee_detail()">
<% END IF %>
<%=EmployeeList%>
<%=OrgList%>
</select>
</div>
</body>
</html>
