<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, tenancy_id, row_id

	Call OpenDB()

	tenancy_id = Request.Form("hid_TENANCYID")
	customer_id = Request.Form("hid_CUSTOMERID")
	action = Request("action")

	Dim ID
	Dim DataFields   (4)
	Dim DataTypes    (4)
	Dim ElementTypes (4)
	Dim FormValues   (4)
	Dim FormFields   (4)
	UpdateID	  = "hid_BANKDETAILSID"
	FormFields(0) = "txt_ACCOUNTNAME|TEXT"
	FormFields(1) = "txt_SORTCODE|TEXT"
	FormFields(2) = "txt_ACCOUNTNUMBER|TEXT"
	FormFields(3) = "rdo_ACTIVE|NUMBER"
	FormFields(4) = "hid_CUSTOMERID|INT"

	Call do_so()

	Call CloseDB()

	' deals with Bank Payments
	Function do_so()

		If action = "NEW" Then
			Call new_record()
		ElseIf action = "AMEND" Then
			Call amend_record()
		End If

	End Function


	Function amend_record()

		ID = Request.Form(UpdateID)
		Call MakeUpdate(strSQL)

		SQL = "UPDATE F_BANKDETAILS " & strSQL & " WHERE BANKDETAILSID = " & ID
		Conn.Execute SQL, recaffected

		'this makes sures only one current bank details is present
		If (Request("rdo_ACTIVE") = 1) Then
			SQL = "UPDATE F_BANKDETAILS SET ACTIVE = 0 WHERE CUSTOMERID = " & customer_id & " AND BANKDETAILSID <> " & ID
			Conn.Execute (SQL)
		End If

	End Function


	' creates all kinds of new records depending on SQL
	Function new_record()

		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "set nocount on;INSERT INTO F_BANKDETAILS " & strSQL & ";SELECT SCOPE_IDENTITY() AS BANKID;"

		set rsSet = Conn.Execute(SQL)
			ID = rsSet("BANKID")
		rsSet.close

		'this makes sures only one current bank details is present
		If (Request("rdo_ACTIVE") = 1) Then
			SQL = "UPDATE F_BANKDETAILS SET ACTIVE = 0 WHERE CUSTOMERID = " & customer_id & " AND BANKDETAILSID <> " & ID
			Conn.Execute (SQL)
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Bank Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("STATUS_DIV").innerHTML = "<font style='font-size:13px'><b>Bank Details Saved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font>";
            parent.location.href = "../iFrames/iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
        }
</script>
</head>
<body onload="ReturnData()">
</body>
</html>