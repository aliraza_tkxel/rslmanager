<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source
	int_source = Request("natureid")

	Call OpenDB()
	Call build_select()

	Function build_select()		
		
        lst = "<select name='sel_SUBITEMID' id='sel_SUBITEMID' class='textbox200' " & sel_width & ">"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0
        sql = "SELECT ACTIONID, DESCRIPTION FROM C_LETTERACTION WHERE NATURE = " & int_source
		
        Call OpenRs(rsSet, sql)
        While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)
		
        lst = lst & "</select>"
		
        If int_lst_record = 0 then
			lst = "<select name='sel_SUBITEMID' id='sel_SUBITEMID' class='textbox200' " & sel_width & " disabled='disabled'><option value=''> No associated actions found</option></select>"
		End If

	End Function
%>
<html>
<head>
    <title>RSLmanager > Customer Module > Standard Letters > Change Nature</title>
    <script type="text/javascript" language="javascript">
        function ReturnData() {
            parent.document.getElementById("dvAction").innerHTML = document.getElementById("dvAction").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvAction">
        <%=lst%></div>
</body>
</html>
