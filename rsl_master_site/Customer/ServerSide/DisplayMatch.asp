<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->	
<%
	CONST CONST_PAGESIZE = 1
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	FirstName = Replace(Request("txt_FIRSTNAME"), "'", "''")
	LastName = Replace(Request("txt_LASTNAME"), "'", "''")
	Dob = Replace(Request("txt_DOB"), "'", "''")
	NINO = Replace(Request("txt_NINUMBER"), "'", "''")
	
	if (NINO <> "") THEN
		ninosql = " AND NINUMBER LIKE '" & NINO & "' "
	end if
	
	if (Dob <> "") THEN
		dobsql = " AND DOB = '" & Dob & "' "
	end if	

	'Call the main Function
	Call getMatchedCustomers()
	
	// retrives team details from database and builds 'paged' report
	Function getMatchedCustomers()
		
		theURL = theURLSET("page")
		
		Response.Write theURL
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = "SELECT TOP 50 C.CUSTOMERID, TIT.DESCRIPTION AS TITLE, FIRSTNAME, LASTNAME, " &_
			"ADDRESS1 = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.ADDRESS1 ELSE A.ADDRESS1 END, " &_
			"ADDRESS2 = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.ADDRESS2 ELSE A.ADDRESS2 END, " &_
			"TOWNCITY = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.TOWNCITY ELSE A.TOWNCITY END, " &_
			"POSTCODE = CASE WHEN P.PROPERTYID IS NOT NULL THEN P.POSTCODE ELSE A.POSTCODE END, " &_
			"CTY.DESCRIPTION AS CUSTOMERTYPE " &_
			"FROM C__CUSTOMER C " &_
			"LEFT JOIN G_TITLE TIT ON C.TITLE = TIT.TITLEID " &_
			"LEFT JOIN C_CUSTOMERTYPE CTY ON CTY.CUSTOMERTYPEID = C.CUSTOMERTYPE " &_
			"LEFT JOIN C_CUSTOMERTENANCY CT ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID AND T.ENDDATE IS NULL AND T.STARTDATE <= GETDATE() " &_
			"LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 " &_
			"WHERE FIRSTNAME LIKE '%" & FirstName & "%' AND " &_
			"LASTNAME LIKE '%" & LastName & "%' " & dobsql & ninosql & " " &_
			"ORDER BY FIRSTNAME, LASTNAME"
									
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
				
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(intPage)
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		//response.write "<BR>Number of pages : " & intPageCount	& "<BR>" & "Number of records : " & intRecordCount &_
		//				"<BR>Current page : " & intPage & "<br>Page size : " & rsSet.PageSize
		count = 0	
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
			For intRecord = 1 to rsSet.PageSize

				TheCustomer = rsSet("CUSTOMERID")				
				str_data = str_data &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE' WIDTH=""100"" NOWRAP>Title</TD><TD WIDTH=""100%"">" & rsSet("TITLE") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>First Name</TD><TD>" & rsSet("FIRSTNAME") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Last Name</TD><TD>" & rsSet("LASTNAME") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Address 1</TD><TD>" & rsSet("ADDRESS1") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Address 2</TD><TD>" & rsSet("ADDRESS2") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Town / City</TD><TD>" & rsSet("TOWNCITY") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Postcode</TD><TD>" & rsSet("POSTCODE") & "</TD></TR>" &_
					"<TR><TD STYLE='BORDER-LEFT:NONE' class='TITLE'>Type</TD><TD>" & rsSet("CUSTOMERTYPE") & "</TD></TR>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT>" &_
			"<TR bgcolor=#133e71><TD  STYLE='BORDER-LEFT:NONE' ALIGN=RIGHT colspan=2>" &_
			"<input type=button value=""Create New"" class=""RSLButton"" onclick=""Proceed(-1,'" & theURLSET("") & "')"">&nbsp;<INPUT TYPE='BUTTON' title='Load Previous Customer' CLASS='RSLBUTTON' VALUE='Load Customer' onclick=""ProceedPrevious(" & TheCustomer & ")""></TD>" &_
			"<TR><TD STYLE='BORDER-LEFT:NONE' height=""100%"" COLSPAN=6 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=LEFT>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD ALIGN=LEFT>" &_			
			"<A class=""RSLBlack""  TARGET = 'Customer_Match' HREF = 'ServerSide/DisplayMatch.asp?" & theURL & "page=1'><b><font color=#133e71>First</font></b></a> " &_
			"<A class=""RSLBlack""  TARGET = 'Customer_Match' HREF = 'ServerSide/DisplayMatch.asp?" & theURL & "page=" & prevpage & "'><b><font color=#133e71>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". " &_
			"<A class=""RSLBlack""  TARGET = 'Customer_Match' HREF='ServerSide/DisplayMatch.asp?" & theURL & "page=" & nextpage & "'><b><font color=#133e71>Next</font></b></a>" &_ 
			" <A class=""RSLBlack""  TARGET = 'Customer_Match' HREF='ServerSide/DisplayMatch.asp?" & theURL & "page=" & intPageCount & "'><b><font color=#133e71>Last</font></b></a>" &_
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"<input type='button' class='RSLButtonSmall' value='GO' onclick=""JumpPage('" & theURL & "')"" style='font-size:10px'>"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>"
			
		End If
		
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=6 ALIGN=CENTER><B>No Matched Customers found !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=6 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function
%>	
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript>
function ReturnData(){
	parent.ResultsDiv.innerHTML = table_div.innerHTML
	}	
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="ReturnData()">
<DIV ID=table_div>
	<TABLE CELLPADDING=3 height="100%" CELLSPACING=0 STYLE="border-collapse:collapse" border=1 WIDTH=100%>
		<TR bgcolor=#133E71 style='color:white'><TD colspan=3 style='border-top:1px solid #133e71'>&nbsp;<b>Found <%=intRecordCount%> matching record(s).</b></TD></TR>
		<%=str_data%>
	</TABLE>
</DIV>
</body>
</html>
