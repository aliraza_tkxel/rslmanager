<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, customer_id, row_id,tenancy_id,page_action
	OpenDB()

	if Request("action")	<> "" then
		action = Request("action")
	else
		action = Request.Form("hid_ACTION")
		
	end if
	'' Collect the variables, If the parent sends the variables by form or string this can account for both
	
	'' This set of variables is for adding and amending
		Function getVariables_setA()
			customer_id 		= Request.Form("hid_CUSTOMERID")
			tenancy_id 		= Request.Form("hid_TENANCYID")
			intAddedDetails = Request("AddedDetails")
		End Function
	
	' This set of variables is for deleting
		Function getVariables_setB()
			customer_id 	= request("customerid")
			tenancy_id 		= request("tenancyid")
		End Function

	
	
	If isNull(intAddedDetails) or intAddedDetails = "" Then intAddedDetails = "0" End If
	
	Dim DataFields   (11)
	Dim DataTypes    (11)
	Dim ElementTypes (11)
	Dim FormValues   (11)
	ReDim FormFields (11)
	UpdateID	  = "hid_ADDRESSID"
	
			FormFields(0) = "txt_CONTACTFIRSTNAME|TEXT"
			FormFields(1) = "txt_CONTACTSURNAME|TEXT"
			FormFields(2) = "txt_RELATIONSHIP|TEXT"
			FormFields(3) = "txt_CONTACTDOB|DATE"
			FormFields(4) = "hid_CUSTOMERID|INT"
			FormFields(5) = "hid_ADDRESSTYPE|INT"
			FormFields(6) = "hid_housenumber|TEXT"
			FormFields(7) = "hid_address1|TEXT"
			FormFields(8) = "hid_ADDRESS2|TEXT"
			FormFields(9) = "hid_towncity|TEXT"
			FormFields(10) = "hid_postcode|TEXT"
			FormFields(11) = "hid_county|TEXT"
			 
	do_contacts()
		
	CloseDB()

	// deals with Direct debit payments
	Function do_contacts()
		
		Dim str_front_sql
		If action = "NEW" Then
			getVariables_setA()
			new_record()
		ElseIf action = "AMEND" Then
			getVariables_setA()
			amend_record()
		ElseIf action = "DELETE" Then
			getVariables_setB()
			delete_record()
		End If	
	
	End Function
	
	Function amend_record()
	
		// ENSURE ONLY ONE POWER OF ATTORNEY AND ONE CORRESPONDENCE ADDRESS
		check_checks()
		
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE C_ADDRESS " & strSQL & " WHERE ADDRESSID = " & ID
		
		//Response.Write SQL
		Conn.Execute SQL, recaffected
		'Response.write strSQL
		Response.Redirect "../iFrames/icontact.asp?customerid="&customer_id&"&tenancyid="&tenancy_id
	End Function

	
	// creates all kinds of new records depending on SQL
	Function new_record()
		
		// ENSURE ONLY ONE POWER OF ATTORNEY AND ONE CORRESPONDENCE ADDRESS
		check_checks()
		
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO C_ADDRESS " & strSQL & ""
		'Response.Write SQL
		Conn.Execute(SQL)

		Response.Redirect "../iFrames/icontact.asp?customerid="&customer_id&"&tenancyid="&tenancy_id

	End Function

	Function check_checks()
	
		If Request.form("txt_ISDEFAULT") = 1 Then
			Conn.Execute( "UPDATE C_ADDRESS SET ISDEFAULT = 0 WHERE CUSTOMERID = " & customer_id )
		End If
		If Request.form("txt_ISPOWEROFATTORNEY") = 1 Then
			Conn.Execute( "UPDATE C_ADDRESS SET ISPOWEROFATTORNEY = 0 WHERE CUSTOMERID = " & customer_id )
		End If
		
	End Function
	
	' This function has been added, and is the only used by itennacy > occupants
	' Jim created this on 10 Nov 2004.
		Function delete_record()
		
			' here we only delete if the customer exists in the occupant table, 
			' prevents wrong customers being deleted in error
			check_SQL = "SELECT * FROM C_OCCUPANT WHERE CUSTOMERID = " & customer_id & " AND TENANCYID = " & tenancy_id
			Call OpenRs(rsSet, check_SQL)
				If not rsSet.eof then
					Conn.Execute("DELETE FROM C_OCCUPANT WHERE CUSTOMERID = " & customer_id & " AND TENANCYID = " & tenancy_id)
					Conn.Execute( "DELETE FROM C__CUSTOMER WHERE CUSTOMERID = " & customer_id)
					page_action = "deletion complete"
				else
					page_action = "error deleting"
				end if
			CloseRs(rsSet)
		End Function
	
	
%>
<html>
<head></head>
<script language=javascript>

	function ReturnData(){
	
		<% if page_action = "deletion complete" then %>
					parent.parent.swap_div(3, 'top')
					//parent.location.href = "../iFrames/itenancy.asp";
		<%	Elseif page_action = "error deleting" then %>
					parent.alert("There has been an error deleting this record, please note the tenancy number and the occupants name, that you are trying to delete and contact the RSL Support Team. Thank you")
					parent.parent.swap_div(3, 'top')
		<% End if %>
	}
	
</script>
<body onload="ReturnData()">
</body>
</html>