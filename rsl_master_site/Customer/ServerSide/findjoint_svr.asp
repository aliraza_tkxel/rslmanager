<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim cusname, str_SQL, str_table, isTenant, action,isdefault,propertyid

	cusname = Request.form("txt_CUSTOMERNAME")
	cusname = replace(cusname,"'","''")
	tenant_id = Request("tenantid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	action = Request.Form("hid_ACTION")
	isTenant = 0
	isdefault=0
	isdefault = request.form("hid_ISDEFAULT")
	response.write "action is "  & action
	Call OpenDB()
	If action = "ATTACH" Then
		Call attach_tenant()
	ElseIf action = "GETCUSTOMER" Then
		Call get_customers()
	Else
		Call remove_tenant()
	End If

	Call CloseDB()

	Function get_property()

		str_SQL= "SELECT PROPERTYID FROM C_TENANCY WHERE TENANCYID="&tenancy_id
		Call OpenRs(rsSet,str_SQL)
		If Not rsSet.EOF Then
			propertyid=rsSet("PROPERTYID")
		End If
		Call CloseRs(rsSet)
	End Function
	
	Function get_customers()
	
		str_SQL = 	"SELECT	C.CUSTOMERID, LTRIM(ISNULL(T.DESCRIPTION,'') + ' ' + ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, " &_
					"		ISNULL(CONVERT(NVARCHAR, C.DOB, 103) ,'') AS DOB, " &_
					"		ISNULL(CT.DESCRIPTION,'') AS CUSTOMERTYPE " &_
					"FROM 	C__CUSTOMER C " &_
					"		LEFT JOIN G_TITLE T ON C.TITLE = T.TITLEID " &_
					"		LEFT JOIN C_CUSTOMERTYPE CT ON C.CUSTOMERTYPE = CT.CUSTOMERTYPEID " &_
					"WHERE	C.CUSTOMERTYPE NOT IN (4,5) AND C.FIRSTNAME LIKE '%" & cusname & "%' OR C.LASTNAME LIKE '%" & cusname & "%'"
		Call OpenRs(rsSet, str_SQL)
		str_table = "<table height='100%' width='100%' hlcolor='steelblue' slcolor='' style='behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' cellpadding='3' cellspacing='0' border='1'>"
		While not rsSet.EOF
			str_table = str_table & "<tr title='Attach To Tenancy' style='cursor:pointer' onclick='Set_Tenancy_Dates(" & rsSet("CUSToMERID") & ")'><td width='200px'>" & rsSet("FULLNAME") & "</td><td width='100px'>" & rsSet("DOB") &_
									"</td><td>" & rsSet("CUSTOMERTYPE") & "</td></tr>"
			rsSet.movenext()
		Wend
		str_table = str_table & "<tfoot><tr><td colspan='3' height='100%'>&nbsp;</td></tr></tfoot></table>"
		Call CloseRs(rsSet)

	End Function
	
	
	Function attach_tenant()

		STARTDATE = REQUEST("txt_STARTDATE")
		If NOT STARTDATE <> "" then STARTDATE = date End If

		str_SQL = "SELECT COUNT(*) AS NUM FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & tenant_id & " AND ENDDATE IS NULL"
		Call OpenRs(rsSet, str_SQL)
		cnt = 0
		While not rsSet.EOF
			cnt = rsSet("NUM")
			rsSet.movenext()
		Wend
		Call CloseRs(rsSet)

		If cnt > 0 Then 
			isTenant = 1
		Else
			isTenant = 0
			' attach this person to the tenancy
			conn.Execute ("INSERT INTO C_CUSTOMERTENANCY (CUSTOMERID, TENANCYID, STARTDATE, CREATEDBY) " &_
				" VALUES (" & tenant_id & ", " & tenancy_id & ", '" & FormatDateTime(STARTDATE, 2) & "', " & Session("USERID") & ")")

			' make new tenant type of 'Tenant'
			conn.Execute("UPDATE C__CUSTOMER SET CUSTOMERTYPE = 2 WHERE CUSTOMERID = " & tenant_id)

			Call get_property()

			Call make_current()

		End If

	End Function

	' SET THE PROPERTY ADDRESS AS THE CURRENT CUSTOMER ADDRESS
	Function make_current()

		Set Rs = Server.CreateObject("ADODB.Command")
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4
		Rs.commandtext = "C_MAKE_DEFAULT_CONTACT"
		Rs.parameters(1) = isdefault
		Rs.parameters(2) = propertyid
		Rs.parameters(3) = tenant_id
		Rs.execute
		Set Rs = NOthing

	End Function

	Function remove_tenant()
		conn.Execute ("UPDATE C_CUSTOMERTENANCY SET LASTMODIFIEDBY = " & Session("USERID") & ", ENDDATE = '" & FormatDateTime(date, 2) & "' WHERE CUSTOMERID = " & tenant_id & " AND TENANCYID = " & tenancy_id)
		' make new tenant type of 'Tenant'
			conn.Execute("UPDATE C__CUSTOMER SET CUSTOMERTYPE = 3 WHERE CUSTOMERID = " & tenant_id)
		Response.Redirect "../iframes/iTenancy.asp?tenancyid=" & tenancy_id & "&CustomerID=" & customer_id & "&toshow=2"
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer Find Tenant</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
	function ReturnData(){
		
		if ("<%=action%>" == "ATTACH"){
			if (<%=isTenant%> == 1){
				alert("This person already exists as a tenant on the systems.");
				return false;
				}
			parent.parent.CRM_TOP_FRAME.location.href = "../iframes/iTenancy.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&toshow=2"
			}
		else if ("<%=action%>" == "GETCUSTOMER"){
			parent.customerdata.innerHTML = "<%=str_table%>";
			}
		else if ("<%=action%>" == "DELETE"){
			parent.parent.CRM_TOP_FRAME.location.href = "../iframes/iTenancy.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&toshow=2"
			}
		}
</script>
</head>
<body onload="ReturnData()">
</body>
</html>