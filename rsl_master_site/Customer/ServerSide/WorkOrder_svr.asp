<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

customer_id = Request.Form("hid_CUSTOMERID")
if (customer_id = "" OR customer_id = -1) then customerid = -1
tenancy_id = Request("hid_TENANCYID")
if (tenancy_id = "" OR tenancy_id = -1) then tenancy_id = -1
property_id = Request("hid_PROPERTYID")
if (property_id = "" OR property_id = -1) then property_id = -1

item_id = 1 ' TENANT ITEM
itemnature_id = Request.Form("sel_NATURE")
NatureTitle = Request.Form("hid_NATURETITLE")
Supplier = Request.Form("sel_SUPPLIER")
if (Supplier = "") then
	SupplierID = "NULL"
else
	SupplierID = Supplier
end if

//THIS VARIABLE STORES THE EXPECTED DELIVEY SECONDS THAT WILL BE ADDED TO THE PURCHASE ORDER DATE
MaxSeconds = 0

//THIS IS A FORMATED CURRENT DATE FOR INSERTION INTO THE DATABASE
CurrentDateStamp = Now
CurrentFullDate = FormatDateTime(CurrentDateStamp,1) & " " & FormatDateTime(CurrentDateStamp,3)

//this part will create the purchase order and set it inactive, it will be enabled at a later stage.
SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE, POTYPE, POSTATUS) VALUES " &_
		"('" & UCase(NatureTitle) & " WORK ORDER', '" & CurrentFullDate & "', 'This purchase order was created automatically from the Repairs process.', " & Session("USERID") & ", " & SupplierID & ", " &_
		" 1, 2, 3);SELECT SCOPE_IDENTITY() AS ORDERID;SET NOCOUNT OFF"
Call OpenRs(rsPO, SQL)
ORDERID = rsPO("ORDERID")
Call CloseRs(rsPO)

//CREATE THE ACTUAL WORK ORDER HERE
SQL = "SET NOCOUNT ON;INSERT INTO P_WORKORDER (ORDERID, TITLE, CUSTOMERID, PROPERTYID, TENANCYID, WOSTATUS, BIRTH_NATURE, BIRTH_ENTITY, BIRTH_MODULE) VALUES " &_
		"(" & ORDERID & ", '" & NatureTitle & "', " & customer_id & ", '" & property_id & "', " & tenancy_id & ", 2, " & itemnature_id & ", 1, 1); SELECT SCOPE_IDENTITY() AS WOID"

Call OpenRs(rsWO, SQL)
WOID = rsWO("WOID")
Call CloseRs(rsWO)

AnyQueued = false
AnyRechargeable = false
For each i in Request.Form("ID_ROW")

	iData = Request.Form("iData" & i)
	Response.Write iData
	iDataArray = Split(iData, "||<>||")
	RepairID = iDataArray(0)
	RepairNotes = Replace(iDataArray(1), "'", "''")
	VatType = iDataArray(2)
	NetCost = iDataArray(3)
	VAT = iDataArray(4)
	GrossCost = iDataArray(5)
	isQueued = CInt(iDataArray(6))
	isRechargeable = iDataArray(7)

	if (Supplier = "") then 'if no supplier selected....
		iAction = 1 'logged
		iStatus = 1 'pending
		PurchaseStatus = 3 'Work Ordered
	elseif (isQueued = 1) then 'is any item is queued
		AnyQueued = true
		iAction = 12 ' queued
		iStatus = 12 ' queued
		PurchaseStatus = 0 'Queued
	else  'SET THE ACTION TO ASSIGN TO CONTRACTOR 
		iAction = 2 ' assign to contractor
		iStatus = 2 ' assigned
		PurchaseStatus = 3 'Work Ordered
	end if

	//THIS PART GETS SOME DEFAULT INFORMATION REGARDING THE REPAIR			
	SQL = "SELECT ID.DESCRIPTION, R_EXP_ID, ESTTIME_SEC FROM R_ITEMDETAIL ID " &_
			"LEFT JOIN R_PRIORITY PR ON ID.PRIORITY = PR.PRIORITYID " &_
			"WHERE ITEMDETAILID = " & RepairID
	Call OpenRs(rsDefault, SQL)
	RepairName = Replace(rsDefault("DESCRIPTION"), "'", "''")
	ExpenditureID = rsDefault("R_EXP_ID")
	IF (ExpenditureID = "" OR isNULL(ExpenditureID)) THEN
		SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BACKUPREPAIREXPENDITUREID'"
		Call OpenRs(rsEXP, SQL)
		if NOT rsEXP.EOF then ExpenditureID = rsEXP("DEFAULTVALUE")
		CloseRs(rsEXP)
	END IF
	EstimatedSeconds = rsDefault("ESTTIME_SEC")		
	Call CloseRs(rsDefault)

	'GET THE ESTIMATED WORK COMPLETION DATE FOR EACH ITEM
	if (EstimatedSeconds = "" OR isNull(EstimatedSeconds)) then
		DELDATE = "NULL"
	else
		//THIS PART GETS THE MAXIMUM TIME FROM THE LIST OF ITEMS SO IT CAN BE ADDED TO THE PURCHASE ORDER EXPECTED DELIVERY TIME.
		if (CLng(MaxSeconds) < CLng(EstimatedSeconds)) then
			MaxSeconds = EstimatedSeconds
		end if
		DELDATE = DateAdd("s", EstimatedSeconds, CurrentDateStamp)
		DELDATE = "'" & FormatDateTime(DelDate,1) & " " & FormatDateTime(DelDate, 3) & "'"
	end if
	
	//INSERT A LINE INTO THE JOURNAL TABLE	
	SQL = 	"SET NOCOUNT ON;" &_	
			"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
			"VALUES (" & customer_id & ", " & tenancy_id & ", '" & property_id & "',  " & item_id & ", " & itemnature_id & ", " & iStatus & ", '" & RepairName & "');" &_
			"SELECT SCOPE_IDENTITY() AS JOURNALID;"
	Call OpenRs(rsSet, SQL)
	journal_id = rsSet.fields("JOURNALID").value
	Call CloseRs(rsSet)
	
	//NEXT INSERT A LINE INTO THE ACTUAL REPAIR TABLE
	SQL = 	"INSERT INTO C_REPAIR " &_
			"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
			"VALUES (" & journal_id & ", " & iStatus & " , " & iAction & ", " & Session("USERID") & ", " & RepairID &_
			 ", " & SupplierID & ", '" & RepairName & "', '" & RepairNotes & "')"
	Conn.Execute(SQL)	

	//THIS PART INSERTS THE INDIVIDUAL PURCHASE ITEMS
	SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS) VALUES " &_
			"(" & ORDERID & ", " & ExpenditureID & ", '" & RepairName & "', '" & RepairNotes & "', '" & CurrentFullDate & "', " & DELDATE & ", " &_
			" " & NetCost & ", " & VatType & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 2, " & PurchaseStatus & ");SELECT SCOPE_IDENTITY() AS ORDERITEMID"
	Call OpenRs(rsITEM, SQL)
	ORDERITEMID = rsITEM("ORDERITEMID")
	Call CloseRs(rsITEM)

	//THIS PART CREATES A SALES INVOICE IF THE ITEM IS SET TO BE RECHARGEABLE
	if (isRechargeable = 1) then

		SQL = "INSERT INTO C_RECHARGEABLE (JOURNALID) VALUES (" & journal_id & ")"
		Conn.Execute (SQL)

		//only create this section if it has not been created already.
		if (AnyRechargeable = false) then
			SQL = "SET NOCOUNT ON;INSERT INTO F_SALESINVOICE (SONAME, SODATE, SONOTES, USERID, TENANCYID, ACTIVE, SOTYPE, SOSTATUS) VALUES " &_
					"('TENANT RECHARGE', '" & FormatDateTime(Date,1) & "', 'Automated Tenant Recharge for: " & RepairName & "', " & Session("USERID") & ", " &_
					" " & tenancy_id & ", 1, 1, 1);SELECT SCOPE_IDENTITY() AS SALEID;SET NOCOUNT OFF"
			OpenDB()
			Call OpenRs(rsSALE, SQL)		
			SaleID = rsSALE("SALEID")
			CloseRs(rsSALE)
			AnyRechargeable = true
		end if
		
		// make journal entry
		SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL " &_
				"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
				"VALUES (" & tenancy_id & ", '" & FormatDateTime(Date,1) & "', 5, NULL, " & GrossCost & ", 2);" &_
				"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		set rsRentJourn = Conn.Execute(SQL)
		RentJournal_id = rsRentJourn("JOURNALID")

		//create the sales invoice individual item line
		SQL = "SET NOCOUNT ON;INSERT INTO F_SALESINVOICEITEM (SALEID, SALESCATID, ITEMNAME, ITEMDESC, SIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, SITYPE, SISTATUS, RENTJOURNALID) VALUES " &_
				"(" & SaleID & ", 1, '" & RepairName & "', '" & RepairName & "', '" & FormatDateTime(Date,1) & "', " &_
				" " & NetCost & ", " & VatType & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 1, 1, " & RentJournal_id & ");SELECT SCOPE_IDENTITY() AS SALEITEMID"
		Call OpenRs(rsITEM, SQL)
		SALEITEMID = rsITEM("SALEITEMID")
		Call CloseRs(rsITEM)

		SQL = "INSERT INTO F_SALEITEM_TO_PURCHASEITEM (SALEITEMID, ORDERITEMID) VALUES (" & SALEITEMID & ", " & ORDERITEMID & ")"
		Conn.Execute SQL
	end if

	//FINALLY JOIN ALL THE ITEMS TOGETHER SO THAT THEY ARE LINKED
	SQL = "INSERT INTO P_WOTOREPAIR (WOID, JOURNALID, ORDERITEMID) VALUES (" & WOID & ", " & journal_id & ", " & ORDERITEMID & ")"
	Conn.Execute(SQL)	
next

if (AnyRechargeable = true) then
	'enter any sales orders that were created onto the nominal.
	Conn.Execute "EXEC NL_SALESORDER " & SaleID
end if

//FINALLY SET THE PROPER STATUSES FOR THE WORK ORDER AND PURCHASE ORDER TAKING INTO ACCOUNT ANY ITEMS
//THAT MAY HAVE BEEN QUEUED IN THE WORK ORDER
if (AnyQueued = true) then
	SQL = "UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = " & WOID
	Conn.Execute (SQL)
	SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 0 WHERE ORDERID = " & ORDERID
	Conn.Execute (SQL)
elseif 	(Supplier = "") then 'if no supplier selected....
	SQL = "UPDATE P_WORKORDER SET WOSTATUS = 1 WHERE WOID = " & WOID
	Conn.Execute (SQL)
end if

CloseDB()

rESPONSE.rEDIRECT "../iFrames/iRepairJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&WOID=" & WOID & "&SyncTabs=1"
%>