<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
''''''''''''
'' PAGE NOTE: This is a list of all the repairs that an invoice has been recieved for
''
'' NOTE: Pages to be aware of: InvoiceRecievedList.asp 
''							   InvoiceRecieved_srv.asp 
''						       InvoiceRecieved.asp
''''''''''''

 bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal = Request("detotal")
	if detotal = "" Then detotal = 0 end if
	detotal = detotal
	selected = Request("selected") // get selected checkboxes into an array
	response.write selected
	sel_split = Split(selected,",")
	
	office = Request("sel_OFFICE")
	// GENERATE OFFICE SQL
	if office = "" Then 
		office_sql = "" 
	else
		office_sql = " AND CP.OFFICE = " & office & " "
	end if
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
								" <TR BGCOLOR=#133e71 STYLE='COLOR:WHITE'>" &_
								" <TD WIDTH=400><FONT STYLE='COLOR:WHITE'><B>Repair Name</B></FONT></TD>" &_
								" <TD WIDTH=355><FONT STYLE='COLOR:WHITE'><B> Contractor</B></FONT></TD>" &_
								" <TD WIDTH=55><FONT STYLE='COLOR:WHITE'><B>Value</B></FONT></TD>"&_
								" <TD><FONT STYLE='COLOR:WHITE'></FONT></TD>" &_
								" </TR>" &_
								" <TR><TD HEIGHT='100%'></TD></TR>" 

		
		cnt = 0
			
		' Main SQL that retrieves all the repairs that are awaiting approval
		strSQL = " SELECT J.JOURNALID, W.WOID ,J.TITLE AS RepairName, O.NAME AS ContractorName , P.GROSSCOST  " &_
					" FROM C_JOURNAL J  " &_
					 "  INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID   " &_
			    "  LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID   " &_
			   "   INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
			    "  INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " &_
			     " INNER JOIN F_PURCHASEORDER PORDER ON PORDER.ORDERID = P.ORDERID  " &_
			    "  INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PORDER.POSTATUS " &_
					" WHERE J.ITEMID = 1 " &_
					"    AND PORDER.POSTATUS = 7  " &_
					"    AND J.ITEMNATUREID IN (2,22,20,21) " &_
					"    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
                    " UNION " &_
                    " SELECT 0 AS JOURNALID, 0 AS WOID, PO.PONAME AS RepairName, O.NAME AS ContractorName, P.GROSSCOST FROM " &_ 
	                " F_PURCHASEORDER PO " &_
	                " INNER JOIN F_PURCHASEITEM P ON P.ORDERID = PO.ORDERID " &_
	                " LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_ 
	                " WHERE PO.ORDERID NOT IN(  " &_
	                " SELECT PORDER.ORDERID FROM C_JOURNAL J  " &_ 
	                " INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_ 
	                " LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_    
	                " INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID   " &_
	                " INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " &_   
	                " INNER JOIN F_PURCHASEORDER PORDER ON PORDER.ORDERID = P.ORDERID  " &_
	                " INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PORDER.POSTATUS  " &_
	                " WHERE J.ITEMID = 1 AND PORDER.POSTATUS = 7  " &_
	                " AND J.ITEMNATUREID IN (2,22,20,21)   " &_
	                " AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_ 
	                " AND R.CONTRACTORID = 1) AND POSTATUS=7 "
 
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 14
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if select box has been previously selected
			isselected = ""
			For each key in sel_split
				response.write "<BR> KEY : " & key & " id : " & Rs("JOURNALID")
				
				If Rs("JOURNALID") = clng(key) Then
					isselected = " checked "
				Exit For
				End If
			Next
	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			str_data = str_data & 	"<TR style='cursor:hand' onClick='open_me(" & Rs("WOID") & ")'><TD>" & Rs("RepairName") & "</TD>" &_
						"<TD>" & Rs("ContractorName") & "</TD>" &_
						"<TD>�" & Rs("GROSSCOST") & "</TD>" &_
						"<TD><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & Rs("JOURNALID") & "' value='" & Rs("JOURNALID") & "' onclick='do_sum("& Rs("JOURNALID") &")' style='dispay:none'></TD>" &_
						"</TR>"
			Rs.movenext()
			End If
		Next
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=4 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & "<TR style='height:'8px'><TD></TD></TR>" &_
				"<TR style='display:none'><TD COLSPAN=4 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
				"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=4 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE='Approve Repair' CLASS='rslbutton' ONCLICK='process()' style='dispay:none'>" &_
								"</TD></TR></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=4 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & prevpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & nextpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?page=" & numpages& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/CompletedRepairs_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=4 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>