<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim item_id, str_nature, lst
	
	item_id = Request("itemid")
	response.write item_id
	OpenDB()
	
	//mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM E_NATURE WHERE ITEMID = " & item_id & " AND EMPLOYEEONLY = 1"
	mysql = "SELECT ITEMNATUREID, DESCRIPTION FROM E_NATURE WHERE ITEMID = " & item_id
	
	Call build_select(str_nature, mysql) 
	response.write mysql
	
	CloseDB()	
	
	Function build_select(ByRef lst, ftn_sql)
		
		Call OpenRs(rsSet, ftn_sql)
		lst = "<select name='sel_NATURE' class='textbox200' onchange='change_nature()'>"
		lst = lst & "<option value=''>Please select</option>"
		int_lst_record = 0
		While (NOT rsSet.EOF)
			
			lst = lst & "<option value='" & rsSet("ITEMNATUREID") & "'>" & rsSet("DESCRIPTION") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		
		Wend
		
		CloseRs(rsSet)				
		lst = lst & "</select>" 
				
		If int_lst_record = 0 then
			lst = "<select name='sel_NATURE' class='textbox200'><option value=''>No Records Exist</option></select>"
		End If			

		//response.write lst
	End Function
	
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.RSLFORM.sel_NATURE.outerHTML = "<%=str_nature%>";
	
	}
</script>
<body onload="ReturnData()">
</body>
</html>