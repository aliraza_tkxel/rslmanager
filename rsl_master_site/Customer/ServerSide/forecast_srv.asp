<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim tenancy_id, end_date, return_value, comm, balance, ending_balance

	tenancy_id = Request.Form("txt_TENANCY")
	end_date = Request.Form("txt_ENDDATE")
	balance = Request.Form("hid_BALANCE")

	Call OpenDB()

	' CALL STORED PROCEDURE TO CALCULATE REFUND VALUE
	Set comm = Server.CreateObject("ADODB.Command")
	    comm.commandtext = "F_TENANCY_END_FORECAST"
	    comm.commandtype = 4
	Set comm.activeconnection = Conn
	    comm.parameters(1) = end_date
	    comm.parameters(2) = tenancy_id
	    comm.execute
	    return_value = -(comm.parameters(3))
	Set comm = Nothing
	Call CloseDB()

	ending_balance = return_value + balance
%>
<html>
<head>
<title>Forecast Tenancy End</title>
<script type="text/javascript" language="JavaScript">
    function ReturnData() {		
		parent.document.getElementById("txt_FORECAST").value = "<%=return_value%>";
		parent.document.getElementById("txt_ENDINGBALANCE").value = "<%=ending_balance%>";		
	}
</script>
</head>
<body onload="ReturnData()">
</body>
</html>