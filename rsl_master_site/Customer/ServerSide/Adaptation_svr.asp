<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
If (Request("SMALL") = 1) Then
	New_Width = "250"
Else
	New_Width = "300"
End If

Call OpenDB()
Call BuildSelect(THESELECT, "sel_ADAPTIONS", "P_ADAPTATIONS AD WHERE ADAPTATIONCAT = " & Request.Form("sel_ADS") & " ", "ADAPTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", "  style=""width:" & New_Width & """ ")
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Adaption</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvAdaption").innerHTML = document.getElementById("dvAdaption").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvAdaption">
        <%=THESELECT%>
    </div>
</body>
</html>
