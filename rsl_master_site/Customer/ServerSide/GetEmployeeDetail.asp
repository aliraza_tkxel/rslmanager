<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsEmployeeDetail, EmployeeId, EmployeeDetail

EmployeeId = Request.Form("sel_USERS")
If (EmployeeId = "") Then
    EmployeeId = -1
End If

If (EmployeeId = "-1") Then
	EmployeeDetail = ""
Else


    SQL= " SELECT E.FIRSTNAME + ' ' + E.LASTNAME As FullName, ISNULL(ejr.JobeRoleDescription, '') As JOBTITLE, ISNULL(CO.WORKDD,'') AS DD, ISNULL(CO.WORKEMAIL,'') As EMAIL " &_ 
            " FROM E__EMPLOYEE E " &_
            " inner join E_JOBROLETEAM ejrt on E.JobRoleTeamId=ejrt.JobRoleTeamId " &_
            " inner join E_JOBROLE ejr on ejrt.JobRoleId=ejr.JobRoleId " &_
            " LEFT JOIN E_CONTACT CO ON CO.EMPLOYEEID = E.EMPLOYEEID " &_
			" WHERE E.EMPLOYEEID = " & EmployeeId
	Call OpenDB()
	Call OpenRs(rsEmployeeDetail, SQL)
	If (Not rsEmployeeDetail.EOF) Then
		  EmployeeDetail = rsEmployeeDetail(0)
		  If (rsEmployeeDetail(1) <> "") Then
		    EmployeeDetail = EmployeeDetail & "<br />" & rsEmployeeDetail(1)
		  End If
		  If (rsEmployeeDetail(2) <> "") Then
		    EmployeeDetail = EmployeeDetail & "<br />Direct Dial " & rsEmployeeDetail(2)
		  End If
		  If (rsEmployeeDetail(3) <> "") Then
		    EmployeeDetail = EmployeeDetail & "<br />Email " & rsEmployeeDetail(3)
		  End If
    Else
		EmployeeDetail = ""
	End If
	Call CloseRs(rsEmployeeDetail)
	Call CloseDB()
End If
%>
<html>
<head>
    <title>RSLmanager > Customer Module > Letter Signature</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
    function SetSignature(){
    <% If (EmployeeId = "-1") Then %>
	    parent.document.getElementById("txt_SIGNATURE").value = ""
    <% Else %>
	    parent.document.getElementById("txt_SIGNATURE").value = document.getElementById("DvSignature").innerHTML
    <% End If %>
	}
    </script>
</head>
<body onload="SetSignature()">
    <div id="DvSignature"><%=EmployeeDetail%></div>
</body>
</html>
