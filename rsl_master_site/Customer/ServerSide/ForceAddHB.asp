<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()
'get the hb information for the rent journal insert
SQL = "SELECT HBA.*, HB.TENANCYID FROM F_HBACTUALSCHEDULE HBA " &_
		"INNER JOIN F_HBINFORMATION HB ON HB.HBID = HBA.HBID " &_
		"WHERE HBROW = " & REQUEST("HBROW")
Call OpenRs(rsHB, SQL)
if (NOT rsHB.EOF) then
	HB = rsHB("HB")
	StartDate = rsHB("STARTDATE")
	EndDate = rsHB("ENDDATE")
	Tenancy = rsHB("TENANCYID")
	CloseRs(rsHB)
	
	'insert an estimated rent journal
	SQL = "SET NOCOUNT ON; INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, PAYMENTSTARTDATE, PAYMENTENDDATE, AMOUNT, ISDEBIT, STATUSID) " &_
			"VALUES (" & Tenancy & ", '" & FormatDateTime(Date,1) & "', 1, 1, '" & FormatDateTime(StartDate,1) & "', '" & FormatDateTime(EndDate,1) & "', " & -Abs(HB) & ", 0, 1);" &_
			"SELECT SCOPE_IDENTITY() AS NEWID"
	Call OpenRs(rsInsert, SQL)
	JournalID = rsInsert("NEWID")
	CloseRs(rsInsert)
	
	'update the hbschedule...
	SQL = "UPDATE F_HBACTUALSCHEDULE SET SENT = 1, JOURNALID = " & JournalID & " WHERE HBROW = " & REQUEST("HBROW")
	Conn.Execute (SQL)
	
else
	CloseRs(rsHB)
end if
CloseDB()
Response.Redirect ("../iFrames/iHBDetails.asp?LOADTAB=2&HBID=" & Request("HBID") & "&CustomerID=" & Request("CustomerID") & "&TenancyID=" & Request("TenancyID"))

%>