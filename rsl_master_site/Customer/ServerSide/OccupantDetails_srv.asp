<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim title, firstname,lastname,gender,DOB,occupation,tenancyid,relationship,economicstatus,userid,consent,disability,customerId

	Call OpenDB()
	If request("hid_Action") = "save" Then
		Call GetAllVariables()
		Call AddNewOccupant()
	ElseIf request("hid_Action") = "amend" Then
		Call GetAllVariables()
		Call AmendOccupant()
	End If
	Call CloseDB()

	Function EscapeString(str) 
		EscapeString =  Replace(str, "'", "''")
	End Function

	Function GetAllVariables()

		title			= EscapeString(request("sel_title"))
        if not title <> "" then title = "null"
		firstname		= EscapeString(request("txt_firstname"))
		lastname		= EscapeString(request("txt_lastname"))
		DOB				= EscapeString(request("txt_DOB"))
		tenancyid		= EscapeString(request("hid_tenancyid"))
		customerId		= EscapeString(request("hid_CustomerID"))
		relationship	= EscapeString(request("sel_relationship"))
		if not relationship <> "" then relationship	= "null"
		economicstatus = request("sel_economicstatus")
		if not economicstatus <> "" then economicstatus = "null"
		disability = EscapeString(request("hid_disability"))
		consent=request("chk_consent")
		if not consent <> "" then consent = "null"
		userid = Session("UserID")

	End Function

	Function AddNewOccupant()

		strSQL = "SET NOCOUNT ON; INSERT INTO C__CUSTOMER " &_
				 " (TITLE,FIRSTNAME,LASTNAME,DOB,CUSTOMERTYPE) VALUES " &_
				 " (" & title & ",'" & firstname & "','" & lastname & "','" & DOB & "',6); " &_
				 "SELECT SCOPE_IDENTITY() AS CUSTID;"
		set rsSet = Conn.Execute(strSQL)
		CUST_id = rsSet.fields("CUSTID").value
		rsSet.close()
		set rsSet = Nothing

		SQL = 	" INSERT INTO C_OCCUPANT (TENANCYID,CUSTOMERID,RELATIONSHIP,ECONOMICSTATUS,USERID,CONSENT,DISABILITY) VALUES " &_
					" (" & tenancyid & "," & CUST_id & "," & relationship & "," & economicstatus & "," & userid & "," & consent & ",'" & disability & "')"
		Conn.Execute(SQL)

	End Function

	Function AmendOccupant()
		sql="UPDATE C_OCCUPANT SET RELATIONSHIP=" & relationship & ", ECONOMICSTATUS = " & economicstatus &  ",DISABILITY='" & disability & "',CONSENT=" & consent & ",USERID=" & userid & " WHERE CUSTOMERID=" & customerId
		Conn.Execute SQL, recaffected

		sql="UPDATE C__CUSTOMER SET TITLE=" & title & ", FIRSTNAME = '" & firstname & "',LASTNAME='" & lastname & "',DOB='" & DOB & "' WHERE CUSTOMERID=" & customerId
		Conn.Execute SQL, recaffected
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Add Occupant Details</title>
    <script type="text/javascript" language="JavaScript">

	function whereNext(){
		<% if request("hid_action") = "amend" then %>
			parent.callDiv(3, 'top')
			parent.close()
		<% else %>
			if (confirm("Click OK if you would like to add another 'Occupant'. Click Cancel if you are finished adding Occupants to this tenancy."))
				parent.RSLFORM.reset()
			else {
				parent.callDiv(3, 'top')
				parent.close()
			}
		<% end if %>
	}

    </script>
</head>
<body onload="whereNext()">
</body>
</html>
