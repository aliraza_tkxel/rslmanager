<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, lst_item, lst_element, lst_repair, str_priority, RepairCost, wiElement, PropID, PopWindow
	
	int_source = Request("source")
	RESPONSE.WRITE int_source
	OpenDB()
	if int_source = 1 Then
		source_is_zone()
	elseif int_source = 2 Then
		source_is_item()
	elseif int_source = 3 Then
		check_warranty_list()
		source_is_element()
	else
		source_is_repair()
	end if
	
	CloseDB()
	
	Function check_warranty_list()
		'need to find the propertyid to begin with...
		P_TenancyID = Request("hid_TENANCYID")
		SQL = "SELECT PROPERTYID FROM C_TENANCY WHERE TENANCYID = '" & Request("hid_TENANCYID") & "'"
		Call OpenRs(rsTen, SQL)
		if (NOT rsTen.EOF) then
			PropID = rsTen("PROPERTYID")
		else
			PropID = -1
		end if
		CloseRs(rsTen)
		
		'next check if a warranty exists within the system for the respective property
		wiElement = Request.form("sel_ELEMENT")
		if (wiElement = "") then wiElement = -1
		SQL = "SELECT WARRANTYID FROM P_WARRANTY WHERE EXPIRYDATE >= GETDATE() AND AREAITEM = " & wiElement & " AND PROPERTYID = '" & PropID & "'"
		Call OpenRs(rsWar, SQL)
		if (NOT rsWar.EOF) then
			PopWindow = true
		else
			PopWindow = false
		end if
		CloseRs(rsWar)
	
	End Function
		
	Function source_is_zone()
		
		ilocation = Request.Form("sel_LOCATION")
		if (ilocation = "") then
			Call reset_select(lst_item, "sel_ITEM", "Please select a Location")
		else
			ftn_sql = 	"SELECT 	A.AREAID, A.DESCRIPTION " &_
						"FROM 		R_AREA A " &_
						"			LEFT JOIN R_ZONETOAREA Z ON A.AREAID = Z.AREAID WHERE Z.ZONEID = " & ilocation &_
						"ORDER		BY DESCRIPTION "
			

			Call build_select(ftn_sql, "'sel_ITEM' onchange='select_change(2)'", lst_item, "Items ")
		end if
		
		Call reset_select(lst_element, "sel_ELEMENT", "Please select an Item")
		Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")
		
	End Function

	Function source_is_item()
		
		item_id = request.form("sel_ITEM")
		if (item_id = "") then
			Call reset_select(lst_element, "sel_ELEMENT", "Please select an Item")		
		else
			ftn_sql = 	"SELECT		AI.AREAITEMID, AI.DESCRIPTION " &_
						"FROM 		R_AREATOAREAITEM ATOI " &_
						"			INNER JOIN R_AREAITEM AI ON ATOI.AREAITEMID = AI.AREAITEMID AND	ATOI.AREAID = " & item_id &_
						"ORDER		BY DESCRIPTION " 
			Call build_select(ftn_sql, "'sel_ELEMENT' onchange='select_change(3)'", lst_element, "Elements ")
		end if
		
		Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")
		
	End Function
	
	Function source_is_element()
		
		iElement = Request.Form("sel_ELEMENT")
		iNature = Request("natureid")
		ExtraSQL = ""
		if (iNature = 21) then
			ExtraSQL = " AND I.MAINTENANCECODE LIKE 'M%' AND I.DESCRIPTION LIKE '%REP%' "
		end if
		if (iElement = "") then
			Call reset_select(lst_repair, "sel_REPAIR", "Please select an Element")			
		else
			ftn_sql = 	"SELECT 	I.ITEMDETAILID, " &_
						"			I.DESCRIPTION,  " &_
						"			P.DESCRIPTION  " &_
						"FROM 		R_ITEMDETAIL I " &_
						"			LEFT JOIN R_PRIORITY P ON I.PRIORITY = P.PRIORITYID " &_
						"			WHERE I.ZONEID = " & Request.form("sel_LOCATION") & " AND " &_
						"			I.AREAID = " & Request.form("sel_ITEM") & " AND I.AREAITEMID = " & iElement &_
						" 			" & ExtraSQL &_
						" ORDER		BY I.DESCRIPTION "
			//response.write ftn_sql
			Call build_select(ftn_sql, "'sel_REPAIR' onchange='select_change(4)'", lst_repair, "Repair Details ")
		
		end if
		
	End Function

	Function source_is_repair()
	
		iRepair = Request.form("sel_REPAIR")
		if (iRepair <> "") then
			ftn_sql = 	"SELECT 	P.DESCRIPTION, ESTTIME, ISNULL(COST,0) AS REPAIRCOST " &_
						"FROM 		R_ITEMDETAIL I " &_
						"			INNER JOIN R_PRIORITY P ON I.PRIORITY = P.PRIORITYID AND " &_
						"			I.ITEMDETAILID = " & iRepair
			
			Call OpenRs(rsSet, ftn_sql)
			If Not rsSet.EOF Then 
				str_priority = rsSet(0) & " -- Est Time: " & rsSet("ESTTIME")
				RepairCost = FormatNumber(rsSet("REPAIRCOST"),2,-1,0,0)
			Else
				str_priority = "Not known"
				RepairCost = "0.00"
			End If
			CloseRs(rsSet)
		else
			str_priority = ""
			RepairCost = ""
		end if		
	End Function
	
	Function build_select(ftn_sql, str_name, ByRef lst, txt_empty)
		
		Call OpenRs(rsSet, ftn_sql)
		
		lst = "<select name=" & str_name & " class='textbox200' style='width:300px'>"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		
		Wend
		
		CloseRs(rsSet)				
		lst = lst & "</select>" 
				
		If int_lst_record = 0 then
			lst = "<select name=" & str_name & " class='textbox200' style='width:300px' disabled><option value=''> No " & txt_empty & " found</option></select>"
		End If			

	End Function
	
	Function reset_select(ByRef lst, str_name, str_sentence)
	
		lst = "<select name='" & str_name & "' class='textbox200' style='width:300px' disabled><option value=''>" & str_sentence & "</option></select>"
	
	End Function

%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	if (<%=int_source%> == 1){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_REPAIRCOST.value = "";
		parent.RSLFORM.sel_ITEM.outerHTML = "<%=lst_item%>";
		parent.RSLFORM.sel_ELEMENT.outerHTML = "<%=lst_element%>";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		}
	else if (<%=int_source%> == 2){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_REPAIRCOST.value = "";
		parent.RSLFORM.sel_ELEMENT.outerHTML = "<%=lst_element%>";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		}
	else if (<%=int_source%> == 3){
		parent.RSLFORM.txt_PRIORITY.value = "";
		parent.RSLFORM.txt_REPAIRCOST.value = "";
		parent.RSLFORM.sel_REPAIR.outerHTML = "<%=lst_repair%>";
		<% if (PopWindow = true) then %>
		window.showModelessDialog("../Popups/WarrantyList.asp?PropID=<%=PropID%>&wiElement=<%=wiElement%>&TenancyID=<%=Request("hid_TENANCYID")%>&CustomerID=<%=Request("hid_CUSTOMERID")%>", "Warranties", "dialogHeight: 420px; dialogWidth: 420px; status: No; resizable: No;");
		<% end if %>
		}
	else {
		parent.RSLFORM.txt_PRIORITY.value = "<%=str_priority%>";
		parent.RSLFORM.txt_REPAIRCOST.value = "<%=RepairCost%>";
		parent.RSLFORM.DatabaseCost.value = "<%=RepairCost%>";		
		}
	}
</script>
<body onload="ReturnData()">
</body>
</html>