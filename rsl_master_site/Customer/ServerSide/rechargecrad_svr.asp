<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, tenancy_id, row_id, sdp, is_valid, active_tenant

	Call OpenDB()

	tenancy_id 	= Request.Form("hid_TENANCYID")
	customer_id = Request.Form("hid_CUSTOMERID")
	action = Request("action")

	If action = "NEW" Then
		If Not checkvalid() Then
			sdp	= Request.Form("txt_SDP")
			SQL = "INSERT INTO F_SDPTOTENANCY (SDP, TENANCYID) VALUES ('" & sdp & "', " & tenancy_id & ")"
		Else
			is_valid = "alert('SDP Number already exists for tenancy number " & active_tenant & "')"
		End If
	Else
		row_id = Request.Form("hid_SPID") 
		SQL = "UPDATE F_SDPTOTENANCY SET ACTIVE = 0 WHERE SDPID = " & row_id
	End If

	Conn.execute(SQL)

	Call CloseDB()

	Function checkvalid()

		SQL = "SELECT * FROM F_SDPTOTENANCY WHERE SDP = '" & Request.Form("txt_SDP") & "'"
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then
			active_tenant = rsSet("TENANCYID")
			checkvalid = True
			Call CloseRs(rsSet)
			Exit Function
		Else
			checkvalid = False
			Call CloseRs(rsSet)
			Exit Function
		End If

	End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module</title>
    <script type="text/javascript" language="JavaScript">
	function ReturnData(){
		<%=is_valid%>
		parent.location.href = "../iFrames/iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
	}
    </script>
</head>
<body onload="ReturnData()">
</body>
</html>
