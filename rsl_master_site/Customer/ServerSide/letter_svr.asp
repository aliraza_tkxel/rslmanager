<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_content, str_code, str_desc, int_nature, int_action, form_action, lst, int_from, int_to

	form_action = request.form("hid_ACTION")

	Call OpenDB()

	If form_action = "NEW" Then
		Call new_record()
	ElseIf form_action = "LOAD" Then
		Call load_record()
		Call build_select()
	ElseIf form_action = "DELETE" Then
		Call delete_record()
	Else 
		Call amend_record()
	End If

	Call CloseDB()

	Function get_data()

		str_code 	= request.form("txt_LETTERCODE")
		str_desc 	= request.form("txt_LETTERDESC")
		str_content = Replace(request.form("txt_LETTERTEXT"),"'", "''")
		int_nature 	= request.form("sel_NATUREID")
		int_action 	= request.form("sel_SUBITEMID")
		int_from 	= request.form("sel_FROM")
		int_to	 	= request.form("sel_TO")

	End Function

	Function amend_record()

		Dim lettid
		letterid = Request("letterid")
		Call get_data()
		SQL = "UPDATE C_STANDARDLETTERS SET " &_
					 "  NATUREID = " & int_nature &_
					 ", FROMID = " & int_from &_
					 ", TOID = " & int_to &_
					 ", SUBITEMID = " & int_action &_
					 ", LETTERCODE = '" & str_code &_
					 "', LETTERDESC = '" & str_desc &_
					 "', LETTERTEXT = '" & Replace(Replace(str_content, Chr(10), ""), Chr(13), "<BR>") &_
					 "' WHERE LETTERID = " & letterid
		conn.execute(SQL)

	End Function


	Function delete_record()

		Dim lettid
		letterid = Request("letterid")
		SQL = "DELETE FROM C_STANDARDLETTERS WHERE LETTERID = " & letterid
		conn.execute(SQL)

	End Function


	Function new_record()

		Call get_data()
		Dim clean_content
		    clean_content = Replace(Replace(str_content, Chr(10), ""), Chr(13), "<BR>")

		SQL = "INSERT INTO C_STANDARDLETTERS (NATUREID, FROMID, TOID, SUBITEMID, LETTERCODE, LETTERDESC, LETTERTEXT) " &_
				"VALUES (" & int_nature & "," &_
							 int_from & "," &_
							 int_to & "," &_
							 int_action & ",'" &_
							 str_code & "','" &_
							 str_desc & "','" &_
							 clean_content & "')"
		conn.execute(SQL)
		Call reset_data()

	End Function


	Function reset_data()

		str_code 	= ""
		str_desc 	= ""
		str_content = ""
		int_nature 	= ""
		int_action  = ""

	End Function


	Function load_record()

		Dim letter_id
		letter_id = Request("letterid")

		SQL = "SELECT * FROM C_STANDARDLETTERS WHERE LETTERID = " & letter_id
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF then
			str_code 	= rsSet("LETTERCODE")
			str_desc 	= rsSet("LETTERDESC")
			str_content = rsSet("LETTERTEXT")
			int_nature 	= rsSet("NATUREID")
			int_action 	= rsSet("SUBITEMID")
			int_from 	= rsSet("FROMID")
			int_to	 	= rsSet("TOID")
		Else
			str_content = "No associated letter exists please contact Support."
		End If

	End Function


	' REBUILD THE 'ACTION' SELECT BOX BASED ON THE VALUE OF THE 'NATURE' SELECT BOX.
	Function build_select()

		lst = "<select name='sel_SUBITEMID' id='sel_SUBITEMID' class='textbox200' style='width:200px'>"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0
        sql = "SELECT ACTIONID, DESCRIPTION FROM C_LETTERACTION WHERE NATURE = " & int_nature
        Call OpenRs(rsSet, sql)
		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name='sel_SUBITEMID' id='sel_SUBITEMID' class='textbox200' style='width:200px' disabled='disabled'><option value=''> No associated actions found</option></select>"
		End If

	End Function
%>
<html>
<head>
    <title>RSLmanager > Customer Module > Standard Letters > ServerSide</title>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">

        function return_data() {
            var client_action = "<%=form_action%>";
            if (client_action == "LOAD")
                do_load();
            else if (client_action == "AMEND" || client_action == "NEW" || client_action == "DELETE")
                parent.location.href = "../standardletters.asp"
        }

        function do_load() {
            parent.RSLFORM.sel_NATUREID.value = "<%=int_nature%>";
            parent.RSLFORM.txt_LETTERCODE.value = "<%=str_code%>";
            parent.RSLFORM.txt_LETTERDESC.value = "<%=str_desc%>";
            parent.RSLFORM.txt_LETTERTEXT.value = Content1.innerText
            parent.swap_button(1);
            parent.document.getElementById("dvAction").innerHTML = document.getElementById("dvAction").innerHTML;
            parent.RSLFORM.sel_SUBITEMID.value = "<%=int_action%>";
            parent.RSLFORM.sel_FROM.value = "<%=int_from%>";
            parent.RSLFORM.sel_TO.value = "<%=int_to%>";
        }	
	
    </script>
</head>
<body onload="return_data()">
    <div id="Content1">
        <%=str_content%></div>
    <div id="dvAction">
        <%=lst%></div>
</body>
</html>
