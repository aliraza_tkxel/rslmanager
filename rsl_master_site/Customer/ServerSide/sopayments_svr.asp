<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, tenancy_id, row_id
	
	OpenDB()

	tenancy_id = Request.Form("hid_TENANCYID")
	customer_id = Request.Form("hid_CUSTOMERID")		
	action = Request("action")
	
	Dim ID
	Dim DataFields   (9)
	Dim DataTypes    (9)
	Dim ElementTypes (9)
	Dim FormValues   (9)
	Dim FormFields   (9)
	UpdateID	  = "hid_SOSCHEDULEID"
	FormFields(0) = "txt_ACCOUNTNAME|TEXT"
	FormFields(1) = "txt_SORTCODE|TEXT"
	FormFields(2) = "txt_ACCOUNTNUMBER|TEXT"
	FormFields(3) = "txt_AMOUNT|CURRENCY"
	FormFields(4) = "txt_PAYMENTDATE|DATE"
	FormFields(5) = "hid_TENANCYID|INT"
	FormFields(6) = "hid_CUSTOMERID|INT"
	FormFields(7) = "hid_SUSPEND|INT"
	FormFields(8) = "sel_ITEMTYPE|SELECT"
 	FormFields(9) = "sel_CYCLE|SELECT"
 
	do_so()
		
	CloseDB()

	// deals with Direct debit payments
	Function do_so()
		
		If action = "NEW" Then
			new_record()
		ElseIf action = "AMEND" Then
			amend_record()
		End If	
	
	End Function
	
	Function amend_record()

		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE F_SOSCHEDULE " & strSQL & " WHERE SOSCHEDULEID = " & ID
		
		Response.Write SQL
		Conn.Execute SQL, recaffected

		run_so_job()
				
	End Function

	
	// creates all kinds of new records depending on SQL
	Function new_record()
		
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "set nocount on;INSERT INTO F_SOSCHEDULE " & strSQL & ";SELECT SCOPE_IDENTITY() AS SCHEDULEID;"
		Response.Write SQL
		set rsSet = Conn.Execute(SQL)
		sched_id = rsSet("SCHEDULEID")
		
		run_so_job()
		
	End Function

	// runs the job which controls the so rent account entries
	Function run_so_job()
		
		Set Rs = Server.CreateObject("ADODB.Command")
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4 
		Rs.commandtext = "F_AUTO_SOSCHEDULE_ENTRY"
		Rs.execute
				
	End Function
	
%>
<html>
<head></head>
<script language=javascript>
	function ReturnData(){
		
		parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>SO Details Saved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</B></FONT>";
//		parent.parent.STARTLOADER('TOP')
		parent.location.href = "../iFrames/iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
		//parent.parent.swap_div(10, 'bottom');
		}
</script>
<body onload="ReturnData()">
</body>
</html>