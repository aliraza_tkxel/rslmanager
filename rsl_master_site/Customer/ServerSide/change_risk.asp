<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, page_source, sel_width

	int_source = Request("RISKID")
	If int_source = "" Then int_source = -1 End If
	page_source = Request("pagesource")

	' IF CALLED FROM POP UP SELECT WIDTH MUST BE SMALLER
	If page_source = 1 Then 
		sel_width = "style=""width:150px"""
	Else
		sel_width = "style=""width:150px"""
	End If

	Call OpenDB()
	Call build_select()

	Function build_select()

		sql = "SELECT SUBCATEGORYID, DESCRIPTION FROM C_RISK_SUBCATEGORY WHERE CATEGORYID = " & int_source

		Call OpenRs(rsSet, sql)

		lst = "<select name='sel_RISK_SUBCATEGORY' id='sel_RISK_SUBCATEGORY' class='textbox200' " & sel_width & ">"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = "<select name='sel_RISK_SUBCATEGORY' id='sel_RISK_SUBCATEGORY' class='textbox200' " & sel_width & " disabled='disabled'><option value=''> No sub category attached</option></select>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Change Vulnerability SubCategory</title>
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvRiskSubCategory").innerHTML = document.getElementById("dvRiskSubCategory").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvRiskSubCategory">
        <%=lst%>
    </div>
</body>
</html>