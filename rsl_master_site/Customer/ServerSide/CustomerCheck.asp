<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
'Get First name, surname and dob to check weather a similar customer already exists in the database.

FirstName = Replace(Request.Form("txt_FIRSTNAME"), "'", "''")
LastName = Replace(Request.Form("txt_LASTNAME"), "'", "''")
Dob = Replace(Request.Form("txt_DOB"), "'", "''")
NINO = Replace(Request.Form("txt_NINUMBER"), "'", "''")

If (NINO <> "") Then
	ninosql = " AND NINUMBER LIKE '" & NINO & "' "
End If

If (Dob <> "") Then
	dobsql = " AND DOB = '" & Dob & "' "
End If

Dim rsChecker

SQL = "SELECT TOP 1 FIRSTNAME FROM C__CUSTOMER WHERE FIRSTNAME LIKE '%" & FirstName & "%' AND LASTNAME LIKE '%" & LastName & "%' " & dobsql & ninosql & " "
Call OpenDB()
Call OpenRs(rsChecker, SQL)
    If (NOT rsChecker.EOF) Then
	    Response.Redirect "DisplayMatch.asp?" & theURLSET("")
    Else
	    JavaS = "parent.document.getElementById('ResultsDiv').innerHTML = document.getElementById('ResultsDiv').innerHTML"
    End If
Call CloseRs(rsChecker)
Call CloseDB()
%>
<html>
<head>
    <title>Duplicate Customer Checker</title>
</head>
<body>
    <div id="ResultsDiv">
        <table cellpadding="3" cellspacing="0" style="border-collapse: collapse">
            <tr>
                <td>
                    <br/>
                    There are no matches for this Customer.
                </td>
                <td rowspan="2" valign="bottom">
                    <input type="button" value="Proceed" id="BtnProceed" name="BtnProceed" title="Proceed" class="RSLButton" onclick="Proceed(-1,'<%=theURLSET("")%>')" style="cursor:pointer" />
                </td>
            </tr>
            <tr>
                <td>
                    Click 'Proceed' to continue creating a record.
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" language="javascript">
        <%=JavaS%>
    </script>
</body>
</html>
