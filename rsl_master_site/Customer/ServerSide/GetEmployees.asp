<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TeamID = CStr(Request.Form("sel_TEAMS"))
Tenancyid=Request.Form("hid_TENANCYID")

If (Request.Form("hid_TENANCYID") = "") Then
	Tenancyid=Request.Form("tenancyid")
End If

If (TeamID = "") Then
	TeamID = "-1"
End If

If (Request("SMALL") = "1") Then
	SELECT_WIDTH = "250"
ElseIf(Request("SMALL") = "2") Then
	SELECT_WIDTH = "200"
Else
	SELECT_WIDTH = "300"
End If

If (TeamID = "1") Then
	Dim rsOrgs
	Call OpenDB()
	SQL = "SELECT NAME, ORGID FROM S_ORGANISATION ORDER BY NAME"
	Call OpenRs(rsOrgs, SQL)
	If (Not rsOrgs.EOF) Then
		OrgList = "<option value=''>Please Select</option>"
		While Not rsOrgs.EOF
			OrgList = OrgList & "<option value='" & rsOrgs("ORGID") & "'>" & rsOrgs("NAME")& "</option>"
			rsOrgs.moveNext
		wend
	Else
		OrgList = "<option value=''>No suppliers found...</option>"
	End If
	Call CloseRs(rsOrgs)
	Call CloseDB()
Else
	If (TeamID = "NOT") Then
		SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				" WHERE TEAM IS NULL AND L.ACTIVE = 1 AND J.ACTIVE = 1 ORDER BY FIRSTNAME, LASTNAME"	
	ElseIf(TeamID="-1") Then 
		SQL="   SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
			"   FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID "&_
			"   INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  "&_  
			"   WHERE L.ACTIVE = 1 "&_
			"   AND J.ACTIVE = 1 "&_
			"   ORDER BY FIRSTNAME, LASTNAME "
	Else
		SQL="   SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
			"   FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID "&_
			"   INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  "&_ 
			"   INNER JOIN E_TEAM T ON J.TEAM = T.TEAMID "&_ 
			"   WHERE L.ACTIVE = 1 "&_
			"   AND J.ACTIVE = 1 "&_
			"   AND T.TEAMID = " & TeamID & " "&_
			"   ORDER BY FIRSTNAME, LASTNAME "
	End If

	Dim rsEmployees
	Call OpenDB()
	Call OpenRs(rsEmployees, SQL)
	If (not rsEmployees.EOF) Then
		EmployeeList = "<option value=''>Please Select</option>"
		While Not rsEmployees.EOF
			EmployeeList = EmployeeList & "<option value='" & rsEmployees("EMPLOYEEID") & "'>" & rsEmployees("FULLNAME")& "</option>"
			rsEmployees.moveNext
		Wend
	Else
		EmployeeList = "<option value=''>No employees found...</option>"
	End If
	Call CloseRs(rsEmployees)
	Call CloseDB()
End If
%>
<html>
<head>
    <title>Server Employee</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
        function ReturnData() {
            parent.document.getElementById("dvUsers").innerHTML = document.getElementById("dvUsers").innerHTML;
        }
    </script>
</head>
<body onload="ReturnData()">
    <div id="dvUsers">
        <% If (TeamID = "1") Then %>
        <select name='sel_USERS' id='sel_USERS' class='textbox200' style='width: <%=SELECT_WIDTH%>px'
            onchange="GetContractorContacts()">
            <% ElseIf(TeamID="-1") Then %>
            <select name='sel_USERS' id='sel_USERS' class='textbox200' style='width: <%=SELECT_WIDTH%>px'>
                <% Else %>
                <select name='sel_USERS' id='sel_USERS' class='textbox200' style='width: <%=SELECT_WIDTH%>px'
                    onchange="GetEmployee_detail()">
                    <% End If %>
                    <%=EmployeeList%>
                    <%=OrgList%>
                </select>
    </div>
</body>
</html>
