<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim FindContractor,FndOrder
	
	FindContractor  = Request("WhatContractor")
	OrderID			= Request("txt_ORDERNO")
	
	If FindContractor <> "" then
	FndCntr = " AND O.ORGID = " & FindContractor 
	End If


	If FindContractor <> "" then
	FndCntr = " AND O.ORGID = " & FindContractor 
	End If
	
	If OrderID <> "" then
	FndOrder = " AND WO.ORDERID = " & OrderID 
	End If

	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		'Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
								" <TR BGCOLOR=#133e71 STYLE='COLOR:WHITE'>" &_
								" <TD WIDTH=250><FONT STYLE='COLOR:WHITE'><B> Order Date</B></FONT></TD>" &_
								" <TD WIDTH=200><FONT STYLE='COLOR:WHITE'><B> Order No.</B></FONT></TD>" &_
								" <TD WIDTH=250><FONT STYLE='COLOR:WHITE'><B> Est. Completion Date</B></FONT></TD>" &_
								" <TD WIDTH=150><FONT STYLE='COLOR:WHITE'><B> Customer Name</B></FONT></TD>"&_
								" <TD WIDTH=350><FONT STYLE='COLOR:WHITE'><B> Contractor</B></FONT></TD>"&_
								" <TD><FONT STYLE='COLOR:WHITE'></FONT></TD>" &_
								" </TR>" &_
								" <TR><TD HEIGHT='100%' colspan='5'></TD></TR>" 

		
		cnt = 0
			
		' Main SQL that retrieves all the repairs that are awaiting approval
	 DateFilter = " AND J.CREATIONDATE > '18 may 2005' 	"
	  strSQL = "SELECT 	SL.SURVEYID, SL.JOURNALID, " &_
				"		CASE WHEN LEN(J.TITLE) > 40   " &_
 				"			THEN LEFT(J.TITLE, 37) +  '...'   " &_
				"			ELSE  J.TITLE   " &_
        		"		END AS JTITLE, " &_
				"		CASE WHEN LEN(C.FIRSTNAME + ' ' + C.LASTNAME) > 20   " &_
 				"			THEN LEFT(C.FIRSTNAME + ' ' + C.LASTNAME, 17) +  '...'   " &_
				"			ELSE  C.LASTNAME   " &_
        		"		END AS shortfullname, " &_
				"		CASE WHEN LEN(P.HOUSENUMBER + ' ' + P.ADDRESS1 + ' ' + P.ADDRESS2 + ', ' + P.POSTCODE) > 24   " &_
 				"			THEN LEFT(P.HOUSENUMBER + ' ' + P.ADDRESS1 + ' ' + P.ADDRESS2 + ', ' + P.POSTCODE, 21) +  '...'   " &_
				"			ELSE  P.HOUSENUMBER + ' ' + P.ADDRESS1 + ' ' + P.ADDRESS2 + ', ' + P.POSTCODE   " &_
        		"		END AS SHORTFULLADDRESS, " &_
				"		P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.POSTCODE, " &_
				"		J.TITLE AS JFULLTITLE, " &_
				"		C.FIRSTNAME + ' ' + C.LASTNAME AS FULLNAME, " &_
				"		P.HOUSENUMBER + ' ' + P.ADDRESS1 + ' ' + P.ADDRESS2 + ', ' + P.POSTCODE AS FULLADDRESS, " &_
				"		CASE WHEN LEN(O.NAME) > 35   " &_
 				"			THEN LEFT(O.NAME, 32) +  '...'   " &_
				"			ELSE  O.NAME   " &_
        		"		END AS ORGNAME,j.currentitemstatusid, " &_
				"		PI.ORDERITEMID, " &_
				"		WO.ORDERID, J.CREATIONDATE, C.CUSTOMERID, " &_
				"		DATEADD(SS, PRI.ESTTIME_SEC ,J.CREATIONDATE ) AS EST_COMPLETION " &_
				" from C_SATISFACTIONLETTER SL  " &_
				"		INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID  " &_
				"		INNER JOIN C_REPAIR R ON R.JOURNALID = SL.JOURNALID  " &_
				"		INNER JOIN R_ITEMDETAIL I ON R.ITEMDETAILID = I.ITEMDETAILID " &_
				"		INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY " &_
				"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
				"		INNER JOIN C_CUSTOMERTENANCY T ON T.CUSTOMERID = C.CUSTOMERID " &_				
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"		INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID " &_
				"		INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID " &_
				"		INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_
				"		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOTO.ORDERITEMID " &_
				" WHERE T.ENDDATE IS NULL AND SL.SATISFACTION_LETTERSTATUS = 2  " &_
				" AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID)AS REPAIRHISTORYID  FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
				" AND R.ITEMACTIONID IN (5,6,10,15,9) " & FndCntr & FndOrder &_
				" AND PI.ORDERITEMID IS NOT NULL "  &  DateFilter 	&_
				" order by j.creationdate desc "
		
		if mypage = 0 then mypage = 1 end if
		pagesize = 15
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then

	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			PropertyADDRESS1 = ""
			If Rs("SHORTFULLADDRESS") <> " " Then 
				PropertyADDRESS1 =  Rs("SHORTFULLADDRESS") 
			Else 
				Housenumber =  Rs("Housenumber")
				Address1 =  Rs("Address1")
				Address2 =  Rs("Address2")
				Postcode =  Rs("Postcode")
			
				if not PropertyADDRESS1 <> "" then
					if housenumber <> " " then
						PropertyADDRESS1 =  PropertyADDRESS1 & Rs("Housenumber") & " " & Rs("Address1") & ", "
					End If
					if Address2 <> " " then
						PropertyADDRESS1 =  PropertyADDRESS1 & Rs("Address2") & ", "
					End If
					if Postcode <> " " then
						PropertyADDRESS1 =  PropertyADDRESS1 & Rs("Postcode")
					End If
				End If
				
			End If
			
			'START APPENDING EACH ROW TO THE TABLE
			if Rs("ORDERID") <> "" then
			
				if Rs("FULLNAME") <> "" then
					thename = Rs("FULLNAME") 
				else
					thename = Rs("shortFULLNAME")
				End If
				
				str_data = str_data & "<TR style='cursor:hand' onClick='open_me(" & Rs("ORDERITEMID") & "," & Rs("JOURNALID") & ")'>" 
				str_data = str_data & "<TD WIDTH=300>" & Rs("CREATIONDATE") & "</TD>" 
				str_data = str_data & "<TD WIDTH=200>" & rs("ORDERID") & "</TD>" 
				str_data = str_data & "<TD  WIDTH=400>" & Rs("EST_COMPLETION") & "</TD>" 
				str_data = str_data & "<TD WIDTH=300>" & thename  & "</TD>" 
				str_data = str_data & "<TD>" & Rs("ORGNAME") & "</TD>" 
				str_data = str_data & "</TR>"
			End If
			Rs.movenext()
			End If
		Next
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=5 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & "<TR style='height:'8px'><TD></TD></TR>" &_
				"<TR style='display:none'><TD COLSPAN=5 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
				"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=5 align=right>" &_
								"" &_
								"</TD></TR></TABLE>"
		End If
		thepage = "ServerSide/SatisfactionLetterResults_srv.asp"
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=5 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & thepage & "?page=" & orderBy & "&txt_FROM=" & fromdate & "&WhatContractor=" & FindContractor & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "'""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & thepage & "?page=" & prevpage& "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "'""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & thepage & "?page=" & nextpage & "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "'""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & thepage & "?page=" & numpages & "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "'""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='" & thepage & "?page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&WhatContractor=" & FindContractor & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr></table>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=4 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>