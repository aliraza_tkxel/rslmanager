<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%

	'' Created By:	Munawar Nadeem (TkXel)
	'' Created On: 	June 12, 2008
	'' Reason:		Integraiton with Tenats Online


	Dim selNature, enqID
      Dim customerID, tenancyID, title, itemID, natureID, natDescription
	Dim targetFrameUrl, targetFrameName

      enqID = Request("EnquiryID")


	' This function will get values for passed parameters based on EnquiryID supplied from DB table TO_Enquiry_Log
	Call select_enquiry_log(enqID, customerID, tenancyID, itemID, natureID, natDescription)      

	' This function will set target farme name, drop down values etc against provided natureID
	Call set_values(selNature, targetFrameName, title, natureID, natDescription)

      ' Setting target frame name and required query string parameters
	targetFrameUrl = "../iFrames/"  & targetFrameName & "?"

      targetFrameUrl = targetFrameUrl & "natureid=" & natureID & "&itemid=" & itemID
	targetFrameUrl = targetFrameUrl & "&title=" & title & "&customerid=" & customerID
	targetFrameUrl = targetFrameUrl & "&tenancyid=" & tenancyID & "&EnquiryID=" & enqID


	Sub select_enquiry_log(enquiryID, ByRef custID, ByRef tenID, ByRef itmID, ByRef natID, ByRef natDesc)      

		OpenDB()

		' CALL To STORED PROCEDURE TO get Enquiry Log Detail
		Set comm = Server.CreateObject("ADODB.Command")

		' setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_SelectEnquiryLogDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID

		' executing sproc and storing resutls in record set
		Set rs = comm.execute

		' if record found/returned, setting values
		If Not rs.EOF Then 
			custID = rs("CustomerID")
			tenID = rs("TenancyID")
			itmID = rs("ITEMID")
			natID = rs("ItemNatureID")
			natDesc  = rs("Description")
		End If


		Set comm = Nothing
		Set rs = Nothing


		CloseDB()


	End Sub


	Sub set_values(ByRef lst, ByRef tfName, ByRef strTitle, natID, natDesc)

		lst = "<select name='sel_NATURE' class='textbox200' onchange='setTitle()'> "

		' Using switch statement applied on natureID
		' Upon matching case, drop-down(list)'s value, target frame name and Title's value are selected 

		Select Case natID

		  ' 50 is value of "Termination (TO)" in C_Nature table
		  Case 50				
				lst = lst & "<option value='50'>" & natDesc & "</option>"
				tfName = "iTermination.asp"
				strTitle = natDesc


		  ' 51 is value of "Service Complaints (TO)" in C_Nature table
		  Case 51
				lst = lst & "<option value='51'>" & natDesc & "</option>"
				tfName = "iServiceComplaints_new.asp"
				strTitle = natDesc


		  ' 52 is value of "Garage/Car Parking (TO)" in C_Nature table
		  Case 52
				lst = lst & "<option value='52'>" & natDesc & "</option>"
				tfName = "iGarageCarParking.asp"
				strTitle = natDesc


		  ' 53 is value of "ASB-Complainant (TO)" in C_Nature table
		  Case 53
				lst = lst & "<option value='53'>" & natDesc & "</option>"
				tfName = "iAsb.asp"
				strTitle = natDesc


		  ' 54 is value of "Abandoned Property/Vehicle (TO)" in C_Nature table
		  Case 54
				lst = lst & "<option value='54'>" & natDesc & "</option>"
				tfName = "iAbandonedPropertyVehicle.asp"
				strTitle = natDesc


		  ' 55 is value of "Transfer (TO)" in C_Nature table
		  Case 55
				lst = lst & "<option value='55'>" & natDesc & "</option>"
				tfName = "iHouseMove.asp"
				strTitle = natDesc
		  ' 56 is value of "Arrears" in C_Nature table
  		  Case 56
				lst = lst & "<option value='56'>" & natDesc & "</option>"
				tfName = "iArrears.asp"
				strTitle = natDesc
		 ' 57 is value of "Arrears" in C_Nature table
 		  Case 57
				lst = lst & "<option value='57'>" & natDesc & "</option>"
				tfName = "iGeneralLetter.asp"
				strTitle = natDesc
		 ' 58 is value of "CST Request (TO)" in C_Nature table
		 Case 58
				lst = lst & "<option value='58'>" & natDesc & "</option>"
				tfName = "iCstRequest.asp"
				strTitle = natDesc
		 ' 71 is value of "Historical Tenancy File" in C_Nature table
		 Case 71
				lst = lst & "<option value='71'>" & natDesc & "</option>"
				tfName = "iHistoricalTenancyFile.asp"
				strTitle = natDesc

		
		' end of select/switch statement
		End Select 


	lst = lst & "</select>"

      End Sub

	
%>

<html>
<head></head>

<script language="javascript">

function ReturnData(){

	
	// This function gets called on "onload" event of this form.

	// parent here reffered to iNewItem.asp as request submitted on this page by iNewItem
	
	// setting item drop down to "Tenants Online"
	parent.RSLFORM.sel_ITEM.value = 4;


	// setting nature drop down to value accd. to type of enquiry
	parent.RSLFORM.sel_NATURE.outerHTML = "<%=selNature%>";

      //alert("<%=title%>");
	// setting value of title accd. to type of enquiry
      parent.RSLFORM.txt_TITLE.value = "<%=title%>";

	// setting targetframe href  (which will populated automatically) accd. to type of enquiry	
	// e.g. If Termination is the type of enquiry, then target frame would be iTermination.asp
	// with query string parameters
	parent.frm_nature.location.href = "<%=targetFrameUrl%>";

	
	//disabling next button
	parent.RSLFORM.BTN_DATES.disabled = true;

	}
</script>

<body onload="ReturnData()">
</body>

</html>



