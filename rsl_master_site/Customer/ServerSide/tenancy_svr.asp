<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim action, UpdateID, tenancy_id, row_id, customerid, close_tenancy, isdefault, cbl, rent_frequency
	Dim forecast

	Call OpenDB()

	action = Request("hid_ACTION")
	customerid = RequesT.form("hid_CUSTOMERID")
	tenancy_id = Request("tenancyid")
	rent_frequency = Request.Form("hid_RENTFREQUENCY")
	isdefault = 0
	isdefault = request.form("ISDEFAULT")
	cbl = Request.Form("hid_CBL")

	Dim ID
	Dim DataFields   (7)
	Dim DataTypes    (7)
	Dim ElementTypes (7)
	Dim FormValues   (7)
	Dim FormFields   (7)

	UpdateID	  = "hid_TENANCYID"
	FormFields(0) = "hid_CREATIONDATE|DATE"
	FormFields(1) = "hid_LASTMODIFIEDBY|INTEGER"
	FormFields(2) = "hid_PROPERTYID|TEXT"
	FormFields(3) = "sel_TENANCYTYPE|INTEGER"
	FormFields(4) = "txt_STARTDATE|DATE"
	FormFields(5) = "hid_CREATEDBY|INTEGER"
	FormFields(6) = "cde_MUTUALEXCHANGE_START|INTEGER"
	FormFields(7) = "hid_CBL|BIT"
	If action = "NEW" Then new_record() Else amend_record() End If

	Call CloseDB()


	' SET THE PROPERTY ADDRESS AS THE CURRENT CUSTOMER ADDRESS
	Function make_current()

		Set Rs = Server.CreateObject("ADODB.Command")
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4
		Rs.commandtext = "C_MAKE_DEFAULT_CONTACT"
		Rs.parameters(1) = isdefault
		Rs.parameters(2) = Request.Form("hid_PROPERTYID")
		Rs.parameters(3) = customerid
		Rs.execute
		Set Rs = NOthing

	End Function

	Function amend_record()

		close_tenancy = Request("close_tenancy")
		If close_tenancy Then
			tenancy_closure_process()
		Else
			ID = Request.Form(UpdateID)
			Call MakeUpdate(strSQL)
			SQL = "UPDATE C_TENANCY " & strSQL & " WHERE TENANCYID = " & ID
			Conn.Execute SQL, recaffected
		End If

	End Function

	' creates all kinds of new records depending on SQL
	Function new_record()

		If Request.Form("chk_mutual") <> "" Then
			FormFields(6) = "cde_MUTUALEXCHANGESTART|1"
		Else
			FormFields(6) = "cde_MUTUALEXCHANGESTART|0"
		End If

		Dim procForcast
		If rent_frequency=2 Then
			procForcast="F_TENANCY_START_FORECAST_WEEKLY"
		Else
			procForcast="F_TENANCY_START_FORECAST"
		End If

		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)
		SQL = "set nocount on;INSERT INTO C_TENANCY " & strSQL & ";SELECT SCOPE_IDENTITY() AS TENANCYID;"
		set rsSet = Conn.Execute(SQL)
		tenancy_id = rsSet("TENANCYID")

		' create tenancy join to customer
		Conn.Execute ("INSERT INTO C_CUSTOMERTENANCY (CUSTOMERID, TENANCYID, CREATEDBY, STARTDATE) " &_
						" VALUES (" & customerid & ", " & tenancy_id & ", " & Session("USERID") & ", '" & FormatDateTime(Request.Form("txt_STARTDATE"), 2) & "')")

		' CHANGE PROPERTY SUB STATUS TO 'PENDING TENANCY' BUT KEEP ORIGINAL 'AVAILABLE' STATUS
		conn.execute ("UPDATE P__PROPERTY SET SUBSTATUS = 21 WHERE PROPERTYID = '" & Request.Form("hid_PROPERTYID") & "'")

		' CHANGE CUSTOMER STATUS TO TENANT
		conn.execute ("UPDATE C__CUSTOMER SET CUSTOMERTYPE = 2 WHERE CUSTOMERID = " & customerid)

		' CREATE ESTIMATED ROW IN RENT JOURNAL
		Set spObj = Server.CreateObject("ADODB.Command")
		Set spObj.activeconnection = Conn
		spObj.commandtype = 4 
		spObj.commandtext = procForcast '"F_TENANCY_START_FORECAST"
		spObj.parameters(1) = FormatDateTime(Request.Form("txt_STARTDATE"), 2) 	' STARTDATE	
		spObj.parameters(2) = Request.Form("hid_PROPERTYID")					' PROPERTYID
		spObj.Execute
		forecast = spObj.parameters(3)											' RETURN VALUE REPRESENTING FORECAST
		Set spObj = Nothing

		Dim monthWeekEnd

		If rent_frequency = 2 Then
			Dim SDate
			If CDate(Request.Form("txt_STARTDATE")) <= Date Then
				SDate = date
			Else
				SDate = FormatDateTime(Request.Form("txt_STARTDATE"),2)
			End If
			SQLEND="SELECT dbo.Get_weekEnds('" & SDate & "') As LASTDAY"
			set rsSet = Conn.Execute(SQLEND)
			monthWeekEnd = rsSet("LASTDAY")
		Else
			If CDate(Request.Form("txt_STARTDATE")) <= Date Then
				monthWeekEnd = getlastday(date)
			Else
				monthWeekEnd = getlastday(FormatDateTime(Request.Form("txt_STARTDATE"),2))
			End If
		End If

		SQL = "	INSERT INTO F_RENTJOURNAL (	TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTSTARTDATE, " &_
					" PAYMENTENDDATE, AMOUNT, STATUSID) " &_
					" VALUES (" & tenancy_id & ", '" & FormatDateTime(Date, 2) & "', 8, '" & FormatDateTime(Request.Form("txt_STARTDATE"), 2) & "', '" & monthWeekEnd & "', " & forecast & ", 1)"
		Conn.Execute(SQL)

		' IF TENANCY TO START TODAY RUN SPROC
		Conn.Execute ("EXEC C_TENANCY_MONITORING")

		' MAKE THIS ADDRESS CURRENT IF REQUIRED
		Call make_current()

	End Function

	' I DONT THINK THIS FUCNTION IS USED ANYMORE!!!! SEE ITERMINATION AND PROCEDURE F_TENANCY_END_PROCESS
	' THIS SHOULD EVENTUALLY TAKE THE FORM OF A STORED PROCEDURE
	Function tenancy_closure_process()

		' fill tenancy closure table (BEFORE NULLS APPEAR)
		conn.Execute ("INSERT INTO C_TENANCYCLOSED (TENANCYID, CLOSUREDATE) VALUES (" & tenancy_id & ", '" & FormatDateTime(Date,1) & "')")

		' close tenancy record
		conn.Execute ("UPDATE C_TENANCY SET LASTMODIFIEDBY = "  & Session("USERID") & ", ENDDATE = '" & FormatDateTime(Request.Form("txt_ENDDATE"), 2) & "' WHERE TENANCYID = " & tenancy_id)

		' close customer/tenancy joins
		conn.Execute ("UPDATE C_CUSTOMERTENANCY SET ENDDATE = '" & FormatDateTime(Request.Form("txt_ENDDATE"), 2) &_
						 "', LASTMODIFIEDBY = " & Session("USERID") &_
						 " WHERE ENDDATE IS NULL AND TENANCYID = " & tenancy_id)

		' get start and end of month
		startdate = datevalue(Request.Form("txt_ENDDATE"))
		enddate = getlastday(Request.Form("txt_ENDDATE"))

		Dim proc_Deduct
		If rent_frequency = 2 Then
			proc_Deduct = "F_PART_WEEK_RENT_DEDUCT"
		Else
			proc_Deduct = "F_PART_MONTH_RENT_DEDUCT"
		End If

		Set rs = Server.CreateObject("ADODB.Command")
		Set rs.activeconnection = Conn
		rs.commandtype = 4
		rs.commandtext = proc_Deduct
		'set parameters
		rs.parameters(1) = startdate ' Start Date
		rs.parameters(2) = enddate' End date
		rs.parameters(3) = Request.Form("hid_PROPERTYID")	' Propertyid
		rs.parameters(4) = tenancy_id	' Tenancyid
		rs.parameters(5) = 1	' Removal Type 1 = Main Property

		tenancy_id = ""

	End Function

	' calculate last day of month
	Function getlastday(datum)

		thedate = datevalue(datum)
		if month(thedate) = 4 or month(thedate) = 9 or month(thedate) = 6 or month(thedate) = 11 then
			monthdays = 30
		elseif month(thedate) = 2 then
			if year(thedate) = 2004 or year(thedate) = 2008 or year(thedate) = 2012 then
				monthdays = 29
			else monthdays = 28 end if
		else monthdays = 31 end if

		getlastday = (monthdays & "/" & month(thedate) & "/" & year(thedate))

	End function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module</title>
</head>
<script type="text/javascript" language="JavaScript">
    function ReturnData() {
        parent.opener.parent.parent.location.href = "../CRM.asp?customerid=<%=customerid%>"
        parent.window.close();
    }
</script>
<body onload="ReturnData()">
</body>
</html>
