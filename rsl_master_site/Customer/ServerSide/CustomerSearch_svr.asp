<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'This filters the tenancy type (the checkboxes next to the key)
	isTennant = Request.Form("chk_show_Tenant")
	isFormerTennant = Request.Form("chk_show_PrevTenant")
	isProspective = Request.Form("chk_show_Prospective")
	isStakeholder = Request.Form("chk_show_Stakeholder")
	isGeneral = Request.Form("chk_show_General")
	isJoint = Request.Form("chk_show_Joint")

	Dim strCustomerType, strTenancyType, strJointTenancy, commaCount
	commaCount = 1

	' DETERMINE TENANCY TYPE STRING
	If (isTennant = "1" AND isFormerTennant = "1") Then
		strTenancyType = " "
	ElseIf (isFormerTennant = "1") Then
		strTenancyType = " AND ((CT.ENDDATE IS NOT NULL) AND (CT.ENDDATE <= CONVERT(SMALLDATETIME,CONVERT(NVARCHAR,GETDATE(),103)))) "
	ElseIf (isTennant = "1") Then
		strTenancyType = " AND ((CT.ENDDATE IS NULL) OR (CT.ENDDATE > CONVERT(SMALLDATETIME,CONVERT(NVARCHAR,GETDATE(),103)))) "
	End If

	' DETERMINE CUSTOMER TYPE STRING
	strCustomerType = " AND C.CUSTOMERTYPE IN ( "
	If (isProspective = "1") Then 
		strCustomerType = strCustomerType & " 1 "
		commaCount = commaCount + 1
	End If
	If (isStakeholder = "1") Then
		If commaCount > 1 Then
			strCustomerType = strCustomerType & ", 4 "
			commaCount = commaCount + 1
		Else
			strCustomerType = strCustomerType & " 4 "
			commaCount = commaCount + 1
		End If
	End If
	If(isGeneral = "1") Then
		If commaCount > 1 Then
			strCustomerType = strCustomerType & ", 5 "
			commaCount = commaCount + 1
		Else
			strCustomerType = strCustomerType & " 5 "
			commaCount = commaCount + 1
		End If
	End If
	If commaCount > 1 Then
		strCustomerType = strCustomerType & " ) "
	Else
		strCustomerType = " "
	End If

	' DETERMINE JOINT TENANCY STRING
	If (isJoint = "1") Then
		strJointTenancy = " AND (SELECT COUNT(CUSTOMERTENANCYID) FROM C_CUSTOMERTENANCY WHERE ENDDATE IS NULL AND " &_
				" TENANCYID = T.TENANCYID) > 1 "
	End If
	' End Of customer type filter


	lastname = Replace(Request.Form("txt_LASTNAME"), "'", "''")
	firstname = Replace(Request.Form("txt_FIRSTNAME"), "'", "''")
	If (lastname = "") Then
		namesql = ""
	Else
		namesql = " AND C.LASTNAME LIKE '%" & lastname & "%' "
	End If

	If (firstname = "") Then
		fnamesql = ""
	Else
		fnamesql = " AND C.FIRSTNAME LIKE '%" & firstname & "%' "
	End If

	customerid = Replace(Request.Form("txt_CUSTOMERID"), "'", "''")
	If (customerid = "") Then
		customersql = ""
	Else
		customersql = " AND C.CUSTOMERID = " & customerid & " "
	End If

	isOrgisIndivd = Replace(Request.Form("rdo_CustomerType"), "'", "''")
	If  (isOrgisIndivd = "3") Then
	    isOrgsql = " AND C.ORGFLAG = 1 "
	Elseif (isOrgisIndivd = "2") Then
		isOrgsql = " AND C.ORGFLAG not in (1) "
	Else
		isOrgsql = ""
	End If

	Initial = Replace(Request.Form("sel_Initial"), "'", "''")
	If (Initial = "") Then
		Initialsql = ""
	Else
		Initialsql = " AND C.TITLE = " & Initial & "	 "
	End If

	address = Replace(Request.Form("txt_ADDRESS"), "'", "''")
	If (address = "") Then
		addresssql = ""
	Else
		addresssql = " AND ((P.ADDRESS1 LIKE '%" & address & "%' OR P.ADDRESS2 LIKE '%" & address & "%' OR P.ADDRESS3 LIKE '%" & address & "%' OR P.HOUSENUMBER + ' ' + P.ADDRESS1 LIKE '%" + address + "%')  OR" &_
				" (A.ADDRESS1 LIKE '%" & address & "%' OR A.ADDRESS2 LIKE '%" & address & "%' OR A.ADDRESS3 LIKE '%" & address & "%' OR A.HOUSENUMBER + ' ' + A.ADDRESS1 LIKE '%" + address + "%')) "
	End If

	towncity = Replace(Request.Form("txt_TOWNCITY"), "'", "''")
	If (towncity = "") Then
		towncitysql = ""
	Else
		towncitysql = " AND (P.TOWNCITY LIKE '%" & towncity & "%') "
	End If

	tenancyid = Replace(Request.Form("txt_TENANCYID"), "'", "''")
	If (tenancyid = "") Then
		tenancysql = ""
	Else
		tenancysql = " AND T.TENANCYID = " & tenancyid & " "
	End If

	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " C.CUSTOMERID " &_
            "	, G.DESCRIPTION AS TITLE " &_
            "	, C.FIRSTNAME " &_
            "	, C.LASTNAME " &_
            "	, CT.ENDDATE " &_
            "	, TENANT = CASE WHEN DATEDIFF(DAY, CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103),CT.ENDDATE) <= 0 THEN 'ft' ELSE 't' END," &_
			"	ISNULL(P.HOUSENUMBER + ' ' + P.ADDRESS1, A.HOUSENUMBER + ' ' + A.ADDRESS1) AS FULLADDRESS, " &_
			"   (	SELECT COUNT(CUSTOMERTENANCYID) " &_
			"	 	FROM C_CUSTOMERTENANCY " &_
			"    	WHERE ENDDATE IS NULL AND TENANCYID = T.TENANCYID) AS TENANCYCOUNT , " &_
			"	 C.CUSTOMERTYPE " &_
			" FROM C__CUSTOMER C " &_
			"	LEFT JOIN C_CUSTOMERTENANCY CT ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"	LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
			"	LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"	LEFT JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 " &_
			"	LEFT JOIN G_TITLE G ON C.TITLE = G.TITLEID " &_
			"WHERE 1 = 1 " & tenancysql & customersql & namesql & fnamesql & towncitysql &_
							addresssql & Initialsql & isOrgsql & strJointTenancy & strCustomerType & strTenancyType &_
			"ORDER BY LASTNAME, CT.ENDDATE, FULLADDRESS, TITLE"
	Call OpenDB()
	Call OpenRs(rsSearch, SQL)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer Search</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">
        function DisplayCustomer(CustID) {
            event.cancelBubble = true
            window.open("/Customer/PopUps/Customer_Details.asp?CustomerID=" + CustID, "CustomerDetails", "width=400,height=320")
        }

        function LoadCustomer(CustID) {
            parent.location.href = "../CRM.asp?CustomerID=" + CustID
        }
    </script>
</head>
<body>
    <table cellspacing="0" cellpadding="0" style="height: 100%">
        <tr>
            <td valign="top" height="20">
                <table>
                    <tr>
                        <td width="124px" nowrap="nowrap" style="color: #133E71">
                            <b>Customer Name:</b>
                        </td>
                        <td width="200px" nowrap="nowrap" style="color: #133E71">
                            <b>Address:</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="line-height: 2">
            <td colspan="2" height="2" style="line-height: 2">
                <hr style="height: 2px; border: 1px dotted #133E71" />
            </td>
        </tr>
        <tr>
            <td height="100%" valign="top">
                <div style="height: 100%; overflow: auto" class="TA">
                    <table style="behavior: url(/Includes/Tables/tablehl.htc)" hlcolor="#133e71" width="393">
                        <%
	COUNT = 0 
	If (not rsSearch.EOF) Then
		While NOT rsSearch.EOF
			CUSTID = rsSearch("CUSTOMERID")
			title = rsSearch("TITLE")
			CustomerName = rsSearch("LASTNAME")
            FirstName = rsSearch("FIRSTNAME")
			EndDate = rsSearch("ENDDATE")
			TENANCYCOUNT = rsSearch("TENANCYCOUNT")
			CustomerType= rsSearch("CUSTOMERTYPE")

			If (NOT isNull(title)) Then
				CustomerName = title & " " & FirstName & " " & CustomerName
			End If

			Dim Result
			    Result="<tr style=""cursor:pointer"" onclick=""LoadCustomer(" & CUSTID & ")"">" &_
			       "<td width=""124px"">" & CustomerName & "</td>" 

			If(rsSearch("FULLADDRESS") <> "") Then
			    Result = Result & "<td width=""210px"">" & rsSearch("FULLADDRESS") & "</td><td width=""66"" style=""background-color:#ffffff"" align=""right"">"
			Else
			    Result = Result & "<td width=""210px""></td><td width=""66"" style=""background-color:#ffffff"" align=""right"">"
			End if

			RW Result

			'Done by jimmy to show images of the type of customer
			Dim ImageArray
			ReDim ImageArray(6)
			ImageArray(1) = "pt" 'Prospective
			ImageArray(2) = "t"  'Tennant
			ImageArray(3) = "ft" 'Former Tenanat
			ImageArray(4) = "sh" 'Stakeholder
			ImageArray(5) = "g"  'general

			    If (CustomerType <> "" AND CustomerType <> 2 AND CustomerType <> 3 AND CustomerType < 6) Then
				    rw "<img src=""../Images/"  & ImageArray(CustomerType) &  ".gif"" border=""0"" alt="" /> "
			    Else
                    If rsSearch("TENANT") = "t" Then
					    rw "<img src=""../Images/"  & ImageArray(2) &  ".gif"" border=""0"" alt="""" /> "
				    Else
					    rw "<img src=""../Images/"  & ImageArray(3) &  ".gif"" border=""0"" alt="""" /> "
				    End If
			    End If

			' Is this a joint tennancy
			IF TENANCYCOUNT > 1 then
				rw "<img src=""../Images/JT.gif"" border=""0"" alt="""" /> "
			End If

			rw "<a onclick=""DisplayCustomer(" & CUSTID & ")""><img src=""/myImages/info.gif"" border=""0"" /></a></td></tr>"
			COUNT = COUNT + 1
			rsSearch.moveNext
		Wend
	Else
		rw "<tr><td colspan=""4"" align=""center"">No Records Found</td></tr>"
	End If
	If (COUNT = MaxRecords) Then
		rw "<tr><td colspan=""4"" align=""center"">Maximum number of records returned.<br/>To Filter the list apply more filters.</td></tr>"
	End If
	Call CloseRs(rsSearch)
	Call CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="100%">
                    <tfoot>
                        <tr style="line-height: 2">
                            <td height="2" style="line-height: 2">
                                <hr style="height: 2px; border: 1px dotted #133e71" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
