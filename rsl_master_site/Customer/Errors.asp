<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match
	Dim TDFunc         (4)	 'stores any functions that will be applied		

	ColData(0)  = "Error|ERROR_ID|60"
	SortASC(0) 	= "ERROR_ID ASC"
	SortDESC(0) = "ERROR_ID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Time|ER_TIME|150"
	SortASC(1) 	= "ER_TIME ASC"
	SortDESC(1) = "ER_TIME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Page|ER_PAGE|190"
	SortASC(2) 	= "ER_PAGE ASC"
	SortDESC(2) = "ER_PAGE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Address|ER_REMOTE_ADDR|100"
	SortASC(3) 	= "ER_REMOTE_ADDR ASC"
	SortDESC(3) = "ER_REMOTE_ADDR DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Status|Stat|80"
	SortASC(4) 	= "Stat ASC"
	SortDESC(4) = "Stat DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	THETT = Request("TT")
	if (THETT = "" OR THETT = "0") then
		TES = " AND ER_REMOTE_ADDR NOT LIKE '%194.129.129.%' "
		schecked = " checked"
		tchecked = ""
	elseif (THETT = "1") then
		TES = " AND ER_REMOTE_ADDR LIKE '%194.129.129.%' "
		schecked = ""
		tchecked = " checked"
	end if

	THEWHICH = Request("WHICH")
	if (THEWHICH = "" OR THEWHICH = "0") then
		ES = " AND STAT = 0 "
		ochecked = " checked"
		cchecked = ""
		achecked = ""
	elseif (THEWHICH = "1") then
		ES = " AND STAT = 1 "
		ochecked = ""
		cchecked = " checked"
		achecked = ""
	else
		ES = "  "
		ochecked = ""
		cchecked = ""
		achecked = " checked"
	end if
			
	PageName = "Errors.asp"
	EmptyText = "No Errors found in the system for the selected option!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""ERROR_ID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE =	"SELECT ERROR_ID, ER_TIME, ER_PAGE, ER_REMOTE_ADDR, Stat = Case when Stat = 0 then '<font color=red>OPEN</font>' else 'CLOSED' end FROM G_ERRORMESSAGES WHERE ER_TIME > '01 april 2006' " &_			
			ES & TES &_
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Errors</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(employee_id){
	location.href = "ErrorDetails.asp?ErrorID=" + employee_id;
	}

function GO(){
	if (thisForm.WHICH[0].checked == true)
		THEVAL = 0
	else if (thisForm.WHICH[1].checked == true)
		THEVAL = 1
	else
		THEVAL = 3
	if (thisForm.TT[0].checked == true)
		THEVAL2 = 0
	else 
		THEVAL2 = 1

	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&WHICH="%>" + THEVAL + "&TT=" + THEVAL2
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
      <TD ROWSPAN=2 valign=bottom><IMG NAME="tab_Tenants" TITLE='System Errors' SRC="Images/tab_system_errors.gif" WIDTH=113 HEIGHT=20 BORDER=0></TD>			
	<TD align=right>
	Please Select: REAL <input type=radio name="TT" value="0" <%=schecked%>> OURS <input type=radio name="TT" value="1" <%=tchecked%>>&nbsp;&nbsp; OPEN <input type=radio name="WHICH" value="0" <%=ochecked%>>
	CLOSED <input type=radio name="WHICH" value="1" <%=cchecked%>>
	ALL <input type=radio name="WHICH" value="3" <%=achecked%>>
	<input type="button" value=" UPDATE " class="RSLButton" onclick="GO()">
	</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=635 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>