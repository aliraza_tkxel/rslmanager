<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->

<%




	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal
	Dim ochecked, cchecked
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Catch Housing Officer ID from dropdown
		theUser = Request("WHICH")  								
		If theUser <> "" then
			If NOT theUser = "All" then 
				theUser = cInt(theUser)
			Else
				theUser = theUser
			End If
		Else
			' This will be the initial value of the dropdown on loading of page 
			theUser = "All"
		End If
		
		'build the scheme and action sql 
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to
		rqAction 	= Request("SEL_ACTION")	' Action last Taken
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Build and set filter boxes and dropdowns situated at the top of the page	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Open database and create a dropdown compiled with the housing officers
		OpenDB()
		
		Call BuildSelect(selAction, "sel_ACTION", "C_LETTERACTION WHERE ACTIONID NOT IN (13,15,19,22,26,29,41,57,62,70,81) ", "ACTIONID, DESCRIPTION", "DESCRIPTION", "All", rqAction, NULL, "textbox200", "onchange='' TABINDEX=3")
		Call BuildSelect(selScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME ", "SCHEMENAME", "All", rqScheme, NULL, "textbox200", "onchange='' TABINDEX=3")
		
		'SQL statement to show housing officers that are assigned to a property that have arreas
		SQL = 	" SELECT E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME " &_
				" FROM E__EMPLOYEE E " &_
				" 	INNER JOIN (SELECT DISTINCT P.HOUSINGOFFICER  " &_
				"				FROM P__PROPERTY P " &_
				" 					LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
				" 				WHERE E.EMPLOYEEID = P.HOUSINGOFFICER) A ON  E.EMPLOYEEID = A.HOUSINGOFFICER ORDER BY E.FIRSTNAME ASC" 	
	    Call OpenRs(rsHO, SQL)
		
			Sel_HousingOfficer = "<select name='WHICH' class='textbox200' onchange='' STYLE='WIDTH:190PX' >"
			Dim isSelected
			
			While (NOT rsHO.EOF)
			
			   ' Set option to be selected in the housing officers dropdown
			  IF theUser = rsHO.Fields.Item("EMPLOYEEID").Value then 
				 isSelected = "selected"
			  end if
			  
			  Sel_HousingOfficer = Sel_HousingOfficer & "<option value='" & (rsHO.Fields.Item("EMPLOYEEID").Value) & "' " & IsSelected & " >"  & (rsHO.Fields.Item("FULLNAME").Value) & "</option>"	  
			  isSelected = ""
	
			  rsHO.MoveNext()
			Wend
			
			If (rsHO.CursorType > 0) Then
				rsHO.MoveFirst
			Else
				rsHO.Requery
			End If
				
			Sel_HousingOfficer = Sel_HousingOfficer & "<option value='All' "
			
			if theUser = "All" then 
				 Sel_HousingOfficer = Sel_HousingOfficer & "selected"
			End If
			
			Sel_HousingOfficer = Sel_HousingOfficer & ">All</option>"
			Sel_HousingOfficer = Sel_HousingOfficer & "</SELECT>"
		
		CloseRs(rsHO)
		
		CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Building of filter boxes and dropdowns 	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsList.asp"
	MaxRowSpan = 9
	
	
	OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		Dim orderBy
		orderBy = " GROSSARREARS DESC "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
		
		' What arrears value to compare against
		CompareValue = "300.00"
		if (Request("COMPAREVALUE") <> "") then
			CompareValue = Request("COMPAREVALUE")
		end if
	
	
		' This is the user id - which
		If NOT theUser = "All" then 
			ES = " AND P.HOUSINGOFFICER = " & cInt(theUser)	
		Else 
			ES = "" 
		End If
	
			
		' As at Date - shows the arrears value from the time of date requested
		If rqAsAtDate <> "" then
			AsAtDate = "AND I_J.TRANSACTIONDATE <= '" & rqAsAtDate & "' "
		Else
			AsAtDate = ""
		End If

			
		' The Scheme that the property applies to ( Also known as Development )
		if rqScheme <> "" then	
			Scheme_sql = " and dev.developmentid = " & rqScheme	
		else	
			Scheme_sql = " " 
		End If
			
			
		' The Action that was last taken ( Arrears actions )
		if rqAction <> "" then
			Action_sql = " and VJ.actionid = " & rqAction	
		else 
			Action_sql = " " 
		End If
	
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

				TotalSQL =		"	SELECT  " &_
							"		sum((ISNULL(A.AMOUNT,0) - ISNULL(ABS(B.AMOUNT),0))) AS TOTALARREARS    " &_
							"	FROM C_TENANCY T    " &_
							"		inner JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  "  & AsAtDate &_ 
							"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID   " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID   " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID   " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) " & AsAtDate &_
							"					AND PAYMENTTYPE = 1    " &_
							"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN (SELECT MAX(JOURNALID) AS JOURNALID, CUSTOMERID FROM C_JOURNAL where ITEMNATUREID = 10 GROUP BY CUSTOMERID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID   " &_
							"	WHERE ISNULL(A.AMOUNT,0)> 0 AND T.ENDDATE IS NULL " &_
							"		  AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)   "
			'rw TotalSQL & "<BR><BR>"
			Call OpenRs(rsTotal, TotalSQL)
			TheArrearsTotal = FormatCurrency(rsTotal("TOTALARREARS"),2)
			CloseRs(rsTotal)
					
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 
			
			intRecord = 0
			str_data = ""
			TotalArrears = 0
					'''SQL = " execute ARREARS_GET_CUSTOMER_HB ' AND P.HOUSINGOFFICER = 71','300',ASAT'',SCHEME'',ACTION'',ORDERBY''"
					
			SQL = " execute ARREARS_GET_CUSTOMER_HB ' " & ES &  "','" & CompareValue & "','', '" & Scheme_sql & "','" & Action_sql & "','" & OrderBy &  "'"

			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.ActiveConnection.CommandTimeout = 0 		'Run the command until it completes - it's so horrifically poorly written this can take up to 2 mins!
			rsSet.Source = SQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
						
						Ant_HB = rsSet("ESTHB")
						Adv_HB = rsSet("ADVHB")

						Net = rsSet("NETARREARS") 
						Balance = rsSet("GROSSARREARS")
						
						Ant_HBTotal = rsSet("ESTTOTAL")
						Adv_HBTotal = rsSet("ADVTOTAL")
						NetTotal = rsSet("NETTotal") 
						BalanceTotal = rsSet("GROSSARREARSTotal")
						
						' Here we break up the array which is used to have multiple customers for link		
						if rsSet("CUSTOMERID") <> "" then 
								
								strCustomerid = Split(rsSet("CUSTOMERID"), ",")	
								
								if rsSet("FULLNAME") <> "" then 
										strCustomerName = Split(rsSet("FULLNAME"), ",")	
								else 
										strCustomerName = rsSet("FULLNAME")
								end if 
								
								iold = 0
								For i = 0 to Ubound(strCustomerid)
									if i > iold then customerNameStr  = CustomerNameStr & " , " end if
									CustomerNameStr  = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='load_me(" & strCustomerid(i) & ")'>" & strCustomerName(i) & "</font> "
									iold = i
								Next								
							
						else 
								CustomerNameStr = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='load_me(" & rsSet("CUSTOMERID") & ")'>" & rsSet("FULLNAME")& "</font> "
						end if
						

						' end
					
						
						
						strStart =  "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
						strEnd =	"<TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & ", " & rsSet("TOWNCITY") & "</TD>" &_
									"<TD>" & rsSet("ACTIONDESC") & "</TD>" &_
									"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_	
									"<TD>" & fORMATcURRENCY(Balance,2) & "</TD>" &_			
									"<TD>" & fORMATcURRENCY(Ant_HB,2) & "</TD>" &_												
									"<TD align=right>" & FormatCurrency(Adv_HB,2) & "</TD>" &_
									"<TD align=right>" & FormatCurrency(Net,2) & "</TD></TR>"
		
						str_data = str_data & strStart & "<TD width=190 >" & CustomerNameStr & "</TD>" & strEnd
											
						count = count + 1
						single_totalarrears = 0
						Balance = 0
						Net = 0
						Ant_HB = 0
						Adv_HB = 0
						hbcustomer = 0
						CustomerNameStr = ""
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				
						strTotals =  "<TR bgcolor='#99CC99'><TD></TD>"  &_
									"<TD ></TD>" &_
									"<TD></TD>" &_
									"<TD></TD>" &_
									"<TD><B>Totals</B></TD>" &_	
									"<TD style='font-size:9px '><B>" & fORMATcURRENCY(BalanceTotal,2) & "</B></TD>" &_			
									"<TD style='font-size:9px '><B>" & fORMATcURRENCY(Ant_HBTotal,2) & "</B></TD>" &_												
									"<TD style='font-size:9px ' ALIGN=RIGHT><B>" & FormatCurrency(Adv_HBTotal,2) & "</B></TD>" &_
									"<TD style='font-size:9px '><B>" & FormatCurrency(NetTotal,2) & "</B></TD></TR>"
		
				str_data = str_data & strTotals & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function load_me(CustomerID){
		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

	var FormFields = new Array()
	FormFields[0] = "txt_VALUE|Compare Value|CURRENCY|Y"
	FormFields[1] = "txt_ASATDATE|Date - As At|DATE|N"

	function GO(){

		if (!checkForm()) return false;
		if (thisForm.txt_VALUE.value < 0 ){
			alert("The Min. Arrears value must not a negative number.");
			return false;
			}
		location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value
		}
		

	/////////////////////////////////
	//	Printing and Xls Below
	/////////////////////////////////
	
	function PrintMe(){
	
		window.open("PrintMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
		
		}
	
	function XLSMe(){
		//alert("unavailable at this time")
		//return false
		window.open("XLSMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value);
		}
	
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <table border=0 width=750 cellpadding=2 cellspacing=2 height=40PX style='BORDER:1PX SOLID #133E71' bgcolor=beige>
    <tr> 
      <td NOWRAP width="22%"><b>Min. Arrears Value:</b></td>
      <td width="26%"><b>Action :</b></td>
      <td width="40%"><b>Housing Officer:</b></td>
      <td width="12%">Print version</td>
    </tr>
    <tr> 
      <td NOWRAP width="22%"> 
        <input type=text name="txt_VALUE" value="<%=CompareValue%>" class="textbox" maxlength=10 style='width:100px;text-align:right'>
        <img name="img_VALUE" src="/js/FVS.gif" width=15 height=15> <image src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0"> 
      </td>
      <td NOWRAP width="26%"><%=selAction%><image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0"> 
      <td NOWRAP width="40%"><%=Sel_HousingOfficer%><image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0"> 
      </td>
      <td width=12% NOWRAP><img src="../myImages/PrinterIcon.gif" width="31" height="20" onClick="PrintMe()" style="Cursor:hand" alt="Print Arrears List"></td>
    </tr>
    <tr> 
      <td NOWRAP width="22%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="58%" valign="middle"><b style="display:none">Date ( As At )</b></td>
            <td width="42%" ><img src="../myImages/info.gif" width="15" height="16" title="As At Date: represents all dates pre the date you set here." style="cursor:hand;display:none"></td>
          </tr>
        </table>
      </td>
      <td NOWRAP width="26%"><b>Scheme: </b> 
      <td NOWRAP width="40%">&nbsp;</td>
      <td width=12% NOWRAP>&nbsp;</td>
    </tr>
    <tr> 
      <td NOWRAP width="22%" valign="bottom"> 
        <input type=text name="txt_ASATDATE" value="<%=rqAsAtDate%>" class="textbox" maxlength=10 style='width:100px;text-align:right;display:none'>
        <img name="img_ASATDATE" src="/js/FVS.gif" width=15 height=15> </td>
      <td NOWRAP width="26%"><%=SelScheme%> 
      <td NOWRAP width="40%"> 
        <input type="button" value=" GO " class="RSLButton" onClick="GO()" name="button">        </td>
      <td width=12% NOWRAP>&nbsp;</td>
    </tr>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=3 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=672 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
      <TR>
        <TD CLASS='NO-BORDER' WIDTH=70PX>&nbsp;<B>Tenancy</B></TD>
        <TD CLASS='NO-BORDER' WIDTH=170>&nbsp;<B>Customer</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=250PX>&nbsp;<B>Address</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=100PX>&nbsp;<B>Action</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX align="center"><b>Last Payment</b></TD>
        <TD WIDTH=70PX align="center" CLASS='NO-BORDER'>
<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GROSSARREARS ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<b>Balance</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GROSSARREARS DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>		
		
		</TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ESTHB ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
          <b>Ant HB<br> 
        </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ESTHB DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a> </TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ADVHB ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Adv HB<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ADVHB DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div></TD>
        <TD CLASS='NO-BORDER' WIDTH=73PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("NETARREARS ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Net<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("NETARREARS DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div></TD>
      </TR>
    </THEAD>
    <TR STYLE='HEIGHT:3PX'>
      <TD COLSPAN=11 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%>
  </TABLE>
</form>
<p>&nbsp;</p>
<p>
  <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</p>
</BODY>
</HTML>
