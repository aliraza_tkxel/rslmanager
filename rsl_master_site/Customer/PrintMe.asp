<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal
	Dim ochecked, cchecked
	Dim TotalArrears , TotalGross , TotalEstHB
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Catch Housing Officer ID from dropdown
		theUser = Request("WHICH")  								
		If theUser <> "" then
			If NOT theUser = "All" then 
				theUser = cInt(theUser)
			Else
				theUser = theUser
			End If
		Else
			' This will be the initial value of the dropdown on loading of page 
			theUser = "All"
		End If
		
		'build the scheme and action sql 
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to
		rqAction 	= Request("SEL_ACTION")	' Action last Taken
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Build and set filter boxes and dropdowns situated at the top of the page	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Open database and create a dropdown compiled with the housing officers
		OpenDB()
		
		Call BuildSelect(selAction, "sel_ACTION", "C_LETTERACTION WHERE ACTIONID NOT IN (13,15,19,22,26,29,41,57,62,70,81) ", "ACTIONID, DESCRIPTION", "DESCRIPTION", "All", rqAction, NULL, "textbox200", "onchange='' TABINDEX=3")
		Call BuildSelect(selScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME ", "SCHEMENAME", "All", rqScheme, NULL, "textbox200", "onchange='' TABINDEX=3")
		
		'SQL statement to show housing officers that are assigned to a property that have arreas
		SQL = 	" SELECT E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME " &_
				" FROM E__EMPLOYEE E " &_
				" 	INNER JOIN (SELECT DISTINCT P.HOUSINGOFFICER  " &_
				"				FROM P__PROPERTY P " &_
				" 					LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
				" 				WHERE E.EMPLOYEEID = P.HOUSINGOFFICER) A ON  E.EMPLOYEEID = A.HOUSINGOFFICER " 	
	    Call OpenRs(rsHO, SQL)
		
			Sel_HousingOfficer = "<select name='WHICH' class='textbox200' onchange='' STYLE='WIDTH:190PX' >"
			Dim isSelected
			
			While (NOT rsHO.EOF)
			
			   ' Set option to be selected in the housing officers dropdown
			  IF theUser = rsHO.Fields.Item("EMPLOYEEID").Value then 
				 isSelected = "selected"
			  end if
			  
			  Sel_HousingOfficer = Sel_HousingOfficer & "<option value='" & (rsHO.Fields.Item("EMPLOYEEID").Value) & "' " & IsSelected & " >"  & (rsHO.Fields.Item("FULLNAME").Value) & "</option>"	  
			  isSelected = ""
	
			  rsHO.MoveNext()
			Wend
			
			If (rsHO.CursorType > 0) Then
				rsHO.MoveFirst
			Else
				rsHO.Requery
			End If
				
			Sel_HousingOfficer = Sel_HousingOfficer & "<option value='All' "
			
			if theUser = "All" then 
				 Sel_HousingOfficer = Sel_HousingOfficer & "selected"
			End If
			
			Sel_HousingOfficer = Sel_HousingOfficer & ">All</option>"
			Sel_HousingOfficer = Sel_HousingOfficer & "</SELECT>"
		
		CloseRs(rsHO)
		
		CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Building of filter boxes and dropdowns 	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsList.asp"
	MaxRowSpan = 8
	
	
	OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		Dim orderBy
		orderBy = " T.TENANCYID DESC "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
		
		' What arrears value to compare against
		CompareValue = "300.00"
		if (Request("COMPAREVALUE") <> "") then
			CompareValue = Request("COMPAREVALUE")
		end if
	
	
		' This is the user id - which
		If NOT theUser = "All" then 
			ES = " AND P.HOUSINGOFFICER = " & theUser
		Else 
			ES = "" 
		End If
	
			
		' As at Date - shows the arrears value from the time of date requested
		If rqAsAtDate <> "" then
			AsAtDate = "AND I_J.TRANSACTIONDATE <= '" & rqAsAtDate & "' "
		Else
			AsAtDate = ""
		End If

			
		' The Scheme that the property applies to ( Also known as Development )
		if rqScheme <> "" then	
			Scheme_sql = " and SCH.SCHEMEID = " & rqScheme	
		else	
			Scheme_sql = " " 
		End If
			
			
		' The Action that was last taken ( Arrears actions )
		if rqAction <> "" then
			Action_sql = " and VJ.actionid = " & rqAction	
		else 
			Action_sql = " " 
		End If
		
		if request("cc_sort") <> "" then
		getOrderBy = " ORDER BY " & request("cc_sort")
		end if
	
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Get Total Of arrears
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				TotalSQL =		"	SELECT  " &_
							"		sum((ISNULL(A.AMOUNT,0) - ISNULL(ABS(B.AMOUNT),0))) AS TOTALARREARS    " &_
							"	FROM C_TENANCY T    " &_
							"		inner JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  "  & AsAtDate &_ 
							"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID   " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID   " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID   " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) " & AsAtDate &_
							"					AND PAYMENTTYPE = 1    " &_
							"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN (SELECT MAX(JOURNALID) AS JOURNALID, CUSTOMERID FROM C_JOURNAL where ITEMNATUREID = 10 GROUP BY CUSTOMERID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID   " &_
							"	WHERE ISNULL(A.AMOUNT,0)> 0 AND T.ENDDATE IS NULL " &_
							"		  AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)   "
			'rw TotalSQL & "<BR><BR>"
			Call OpenRs(rsTotal, TotalSQL)
			TheArrearsTotal = FormatCurrency(rsTotal("TOTALARREARS"),2)
			CloseRs(rsTotal)
							
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  End Get Total Of arrears
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Start to Build arrears list 
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					Dim strSQL, rsSet, intRecord 
			
					intRecord = 0
					str_data = ""
					TotalArrears = 0
					'''SQL = " execute ARREARS_GET_CUSTOMER_HB ' AND P.HOUSINGOFFICER = 71','300',ASAT'',SCHEME'',ACTION'',ORDERBY''"
					
					SQL = " execute ARREARS_GET_CUSTOMER_HB ' " & ES &  "','" & CompareValue & "','', '" & Scheme_sql & "','" & Action_sql & "','" & getOrderBy &  "'"
					
					Call OpenRs(rsSet, SQL)
					
					count = 0
					If not rsSet.eof  Then
					' Display the record that you are starting on and the record
					' that you are finishing on for this page by writing out the
					' values in the intStart and the intFinish variables.
					str_data = str_data & "<TBODY CLASS='CAPS'>"
						' Iterate through the recordset until we reach the end of the page
						' or the last record in the recordset.
						PrevTenancy = ""
						while not rsSet.eof
								
							Ant_HB = rsSet("ESTHB")
							Adv_HB = rsSet("ADVHB")
							Net = rsSet("NETARREARS") 
							Balance = rsSet("GROSSARREARS")
							
													
							strStart =  "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
							strEnd =	"<TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & ", " & rsSet("TOWNCITY") & "</TD>" &_
										"<TD>" & rsSet("ACTIONDESC") & "</TD>" &_
										"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_	
										"<TD>" & fORMATcURRENCY(Balance,2) & "</TD>" &_			
										"<TD>" & fORMATcURRENCY(Ant_HB,2) & "</TD>" &_												
										"<TD align=right>" & FormatCurrency(Adv_HB,2) & "</TD>" &_
										"<TD align=right>" & FormatCurrency(Net,2) & "</TD></TR>"
			
							str_data = str_data & strStart & "<TD width=190>" & rsSet("FULLNAME") & "</TD>" & strEnd
							
							
							AntHBtotal = AntHBtotal + Ant_HB  
							AdvHBtotal = AdvHBtotal + Adv_HB  
							NetTotal = NetTotal + Net
							TotalGross = TotalGross + Balance
							
							single_totalarrears = 0
							Balance = 0
							Net = 0
							Ant_HB = 0
							Adv_HB = 0
							hbcustomer = 0
						rsSet.movenext()
						wend
						
						str_data = str_data & "</TBODY>"
					
						str_data = str_data & 	"<TR><TD></TD><TD></TD>" &_
												"<TD></TD>" &_
												"<TD></TD>" &_
												"<TD><b>Totals</b></TD>" &_	
												"<TD><b>" & FormatCurrency(TotalGross) & "</b></TD>" &_			
												"<TD><b>" & FormatCurrency(AntHBtotal) & "</b></TD>" &_							
												"<TD align=right><b>" & FormatCurrency(AdvHBtotal) & "</b></TD>" &_
												"<TD align=right><b>" & FormatCurrency(NetTotal) & "</b></TD></TR>"				'ensure table height is consistent with any amount of records
						
						'ensure table height is consistent with any amount of records
						
						
					End If
					
					Call CloseRs(rsSet)
					
				End function
				
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {
	font-size: 12px;
	font-weight: bold;
	color: #000099;
}
.style2 {color: #000099; font-size: 12px;}
-->
</style>
</HEAD>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF  MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name = thisForm method=get>
  <table width="750" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="92%" valign="top"><div align="left"><span class="style1">Arrears Report - 
        </span><span class="style2">
        <%dim todaysDate
 todaysDate=now()
 response.write  todaysDate

%>
          </span> </div></td>
      <td width="8%"><div align="right"><img src="../myImages/My11.gif" width="56" height="40"></div></td>
    </tr>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=3 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=672 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=70PX>&nbsp;<B>Tenancy</B></TD>
      <TD CLASS='NO-BORDER' WIDTH=180PX>&nbsp;<B>Customer</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=250PX>&nbsp;<B>Address</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=100PX>&nbsp;<B>Action</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=70PX align="center"><b>Last Payment</b></TD>
      <TD WIDTH=70PX align="center" CLASS='NO-BORDER'><b>Balance</b></TD>
      <TD CLASS='NO-BORDER' WIDTH=70PX align="center"><b>Ant HB </b></TD>
      <TD CLASS='NO-BORDER' WIDTH=70PX>&nbsp;<B>Adv. HB </B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=70PX>&nbsp;<b>Net</b>&nbsp;</TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=11 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>
</BODY>
</HTML>