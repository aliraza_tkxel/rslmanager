<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	CONST_PAGESIZE = 20
	Call OpenDB()

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim TotalSum, TotalCount, clist, csql, ccnt, ctotal, view
	Dim GENERAL_REQUEST_STRING
	Dim ShowAll

	GENERAL_REQUEST_STRING = "TENANT="&REQUEST("TENANT")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&ASSIGNEDTO="&REPLACE(REQUEST("ASSIGNEDTO"),"'", "''''")&"&"

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

'	SET ORDER BY
	Dim ORDERBY
	ORDERBY = " J.CREATIONDATE "
	if (Request("CC_Sort") <> "") then ORDERBY = Request("CC_Sort") End If
	If Request("TENANT") <> "" Then SRCH_TENANT = Request("TENANT") Else SRCH_TENANT = "" End If
	If Request("ASSIGNEDTO") <> "" Then SRCH_ASSIGNEDTO = REPLACE(REQUEST("ASSIGNEDTO"),"'", "''''") Else SRCH_ASSIGNEDTO = "" End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("ShowClosed") <> "" Then ShowAll = Request("ShowClosed") Else ShowAll =  0 End If
	'Call SetSort()
	Call getData()
	Call CloseDB()


	Function getData()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		SQLCODE = " EXEC C_SERVICECOMPLAINTS_REPORT 1, '" & ORDERBY & "', '" & SRCH_TENANT & "', '" & SRCH_ASSIGNEDTO & "',''," & ShowAll

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = SQLCODE
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pasges
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class='caps'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

					' was created for the service complaints - can be used for everything now.
					If (rsSet("TRAFFIC_LIGHT") <> "" And rsSet("ITEMSTATUSID") = "13") Then
						TRAFFIC_LIGHT =  "<td><img title='OPEN -  ACTION REQUIRED' src='/myimages/trafficlight_" & rsSet("TRAFFIC_LIGHT") & ".gif' height=""15"" width=""15""></td>" 
					Else
						TRAFFIC_LIGHT = "<td><img title='CLOSED' src='/myimages/trafficlight_BLACK.gif' height=""15"" width=""15""></td>"
					End If
					
					If rsSet("ACTIONID") = 102 then
					    next_action_date = "<font color=""red"">Awaiting Customer Response</font>"
					Else
                        If (rsSet("ESCALATION_DATE") <> "" And rsSet("ITEMSTATUSID") <> 14 And rsSet("ITEMACTIONID") <> "") Then
			            	next_action_date = GetActionDateLine(rsSet("LASTACTIONDATE"),rsSet("ITEMACTIONID"))
			            Else
                            next_action_date = rsSet("NEXT_ACTION_DATE")
		                End If
					End If

                    If (rsSet("OUTCOME") <> "") Then
                        If (rsSet("OUTCOME") = "0") Then
						    OUTCOME =  "UpHeld" 
                        ElseIf (rsSet("OUTCOME") = "1") Then
						    OUTCOME = "Partially UpHeld"
                        Else
                            OUTCOME = rsSet("OUTCOME")
					    End If
                    Else
						OUTCOME = rsSet("OUTCOME")
					End If
				
					str_data = str_data &_
						"<tr onclick='toggle("&rsSet("JOURNALID")&")' style=""cursor:pointer"">" &_
						"<td title='Click to view notes' width='70px' nowrap='nowrap'>" & FormatDatetime(rsSet("CREATIONDATE"),2) & "</td>" &_
						"<td title='Click to go to customer record' WIDTH=130PX nowrap><b>" & rsSet("TENANT") &	"</b></td>" &_
						"<td title='Click to view notes' width='150px' nowrap='nowrap'>" & rsSet("ESCALATION") & "</td>" & _
						"<td title='Click to view notes' width='85px' nowrap='nowrap'>" & rsSet("ASSIGNED_TO") & "</td>" &_
						"<td title='Click to view notes' width='100px' nowrap='nowrap'>" & rsSet("LAST_ACTION") & "</td>"  &_
                        "<td title='Click to view notes' width='100px' nowrap='nowrap'>" & next_action_date & "</td>"  &_
						"<td title='Click to view notes' width='70px' nowrap='nowrap'>" & OUTCOME & "</td>" & TRAFFIC_LIGHT & "</tr>"  &_						
						"<thead><tr id='"&rsSet("JOURNALID")&"' style='display:none'><td colspan=""6"">" & rsSet("NOTES") & "</td></tr></thead>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next

			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""8"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a style=""cursor:pointer"" onclick=""gotopage(1)""><b><font color=""blue"">First</font></b></a> " &_
			"<a style=""cursor:pointer"" onclick=""gotopage(" & prevpage & ")""><b><font color=""BLUE"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a style=""cursor:pointer"" onclick=""gotopage(" & nextpage & ")""><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a style=""cursor:pointer"" onclick=""gotopage(" & intPageCount & ")""><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		If intRecord = 0 Then
			str_data = "<tr><td colspan=""8"" align=""center"">No service complaints found in the system for the specified criteria!!!</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
			Call fill_gaps()
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""8"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function SetSort()
		If (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			For i=0 To UBound(SortASC)
				If SortASC(i) = PotentialOrderBy Then
					orderBy = PotentialOrderBy
					Exit For
				End If
				If SortDESC(i) = PotentialOrderBy Then
					orderBy = PotentialOrderBy
					Exit For
				End If
			Next
		End If
	End Function

	Function GetActionDateLine(LASTDEADLINESTARTDATE,ITEMACTIONID)

		Get_nextActionDate_SQL = "C_SERVICECOMPLAINTS_ACTION_RULE '" & formatDateTime(LASTDEADLINESTARTDATE,1)  & "'," & ITEMACTIONID
		Call OpenRs (rsDate, Get_nextActionDate_SQL)

		If Not rsDate.eof Then
			NextActionDate = rsDate("NEXT_ACTION_DATE")
		 	DaysOverorUnder = rsDate("DAYS_NUMBER")
			RuleStatus = rsDate("RULE_STATUS")
		End If

		Call CloseRS(rsDate)

		GetActionDateLine = NextActionDate
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer --> Service Complaints List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        function toggle(what) {
            what = new String(what)
            var coll = document.getElementsByName(what);
            if (coll != null)
                for (i = 0; i < coll.length; i++)
                    if (coll[i].style.display == "none")
                        coll[i].style.display = "block";
                    else
                        coll[i].style.display = "none";
        }

        function gotopage(pageno) {
            var show_closed = ""
            if (thisForm.chk_SHOWALL.checked)
                show_closed = "&ShowClosed=1"
            location.href = "serviceComplaintsReport.asp?page=" + pageno + "&TENANT=" + document.getElementById("txt_TENANT").value + "&ASSIGNEDTO=" + document.getElementById("txt_ASSIGNEDTO").value + "&PAGESIZE=" + document.getElementById("txt_PAGESIZE").value + '' + show_closed;
        }

        // OPEN OR CLOSE CREDIT NOTE ROWS
        function toggle(what) {
            what = new String(what)
            var coll = document.getElementsByName(what);
            if (coll != null)
                for (i = 0; i < coll.length; i++)
                    if (coll[i].style.display == "none")
                        coll[i].style.display = "block";
                    else
                        coll[i].style.display = "none";
        }

        var FormFields = new Array();
        FormFields[0] = "txt_TENANT|Tenant|TEXT|N"
        FormFields[1] = "txt_ASSIGNEDTO|Assigned|TEXT|N"
        FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"

        // fires when proceed button is clicked
        function click_go() {
            var show_closed = ""
            if (!checkForm()) return false;
            if (document.getElementById("chk_SHOWALL").checked)
                show_closed = "&ShowClosed=1"
            location.href = "serviceComplaintsReport.asp?TENANT=" + document.getElementById("txt_TENANT").value + "&page=1&ASSIGNEDTO=" + document.getElementById("txt_ASSIGNEDTO").value + "&PAGESIZE=" + document.getElementById("txt_PAGESIZE").value + "" + show_closed;
        }

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <br />
    <table width="750" cellpadding="2" cellspacing="0" style='border: 1px solid black;
        border-collapse: collapse; behavior: url(/Includes/Tables/tablehl.htc)' slcolor=''
        hlcolor="STEELBLUE" border="1" bordercolor="silver">
        <thead>
            <tr>
                <td align="left" colspan="8">
                    <span title="Enter any keyword/reference code to search for"><b>&nbsp;Tenant Name&nbsp;
                        <input type="text" class="textbox" name="txt_TENANT" id="txt_TENANT" maxlength="12" size="20" tabindex="1"
                            value='<%=Request("TENANT")%>' />
                        <img src="/js/FVS.gif" name="img_TENANT" id="img_TENANT" width="15px" height="15px" border="0" alt="" />
                    </b></span><b>Assigned To</b>
                    <input type="text" class="textbox" name="txt_ASSIGNEDTO" id="txt_ASSIGNEDTO" maxlength="12" size="20"
                        tabindex="2" value='<%=Request("ASSIGNEDTO")%>' />
                    <img src="/js/FVS.gif" name="img_ASSIGNEDTO" id="img_ASSIGNEDTO" width="15px" height="15px" border="0" alt="" />Pagesize
			<input type="text" class="textbox" name="txt_PAGESIZE" id="txt_PAGESIZE" maxlength="3" size="5" tabindex="3" value="<%=CONST_PAGESIZE%>" />
			<img src="/js/FVS.gif" name="img_PAGESIZE" id="img_PAGESIZE" width="15px" height="15px" border="0" alt="" />
            Show Closed <input type="checkbox" name="chk_SHOWALL" id="chk_SHOWALL" <% if ShowAll = 1 then%>checked="checked"<%end if%> value="1" />&nbsp;
			<input type="button" tabindex="4" id="BTN_GO" name="BTN_GO" value="Proceed" class="rslbutton" onclick="click_go()" style="cursor:pointer" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td class='TABLE_HEAD' width="75" nowrap="nowrap">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Date
                    Created&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="154">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=CG.LIST"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Tenant&nbsp;<a
                            href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=CG.LIST DESC"
                            style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                                alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="173">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=L.DESCRIPTION"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Escalation&nbsp;<a
                            href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=L.DESCRIPTION DESC"
                            style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                                alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="85">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=E.LASTNAME"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Assigned
                    To&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=E.LASTNAME DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending"/></a>
                </td>
                <td class='TABLE_HEAD' width="93">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=LA.DESCRIPTION"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Last
                    Action &nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=LA.DESCRIPTION DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="111">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.NEXTACTIONDATE"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Next
                    Action Date &nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.NEXTACTIONDATE DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="30">
                    <a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.OUTCOME"
                        style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Outcome
                     &nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.OUTCOME DESC"
                        style="text-decoration: none"><img src="/myImages/sort_arrow_down.gif" border="0"
                            alt="Sort Descending" /></a>
                </td>
                </tr>
                <tr style="height: 3px">
                    <td colspan="8" align="center" style="border-bottom: 2px solid #133e71" class="TABLE_HEAD">
                    </td>
                </tr>
        </thead>
        <%=str_data%>
    </table>
    <input type="hidden" name="hid_CreditSum" id="hid_CreditSum" />
    <input type="hidden" name="hid_CreditCount" id="hid_CreditCount" />
    <input type="hidden" name="hid_CreditList" id="" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
