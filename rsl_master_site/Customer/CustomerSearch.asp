<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
Call OpenDB()
Call BuildSelect(CustomerTitle, "sel_Initial", "G_TITLE ", "TITLEID, DESCRIPTION", "DESCRIPTION", "All", NULL, NULL, "textbox200", "tabIndex='2'")
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Customer--&gt; Customer Search</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_LASTNAME|Last Name|TEXT|N"
        FormFields[1] = "txt_CUSTOMERID|Customer Number|INTEGER|N"
        FormFields[2] = "txt_TENANCYID|Tenancy Number|INTEGER|N"
        FormFields[3] = "txt_ADDRESS|Address|TEXT|N"
        FormFields[4] = "txt_TOWNCITY|Town / City|TEXT|N"
        FormFields[5] = "txt_FIRSTNAME|First Name|TEXT|N"

        $(function () {
            // make <enter> automatically click 'search' button
            $("input").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#BtnSEARCH").click();
                }
            })
        });

        function setValuesOfChecks(checkAction) {
            if ((checkAction == "Joint") && (RSLFORM.chk_show_Tenant.checked == false) && (RSLFORM.chk_show_Joint.checked == true)) {
                document.RSLFORM.chk_show_Tenant.checked = true
            }
        }

        function SearchNow() {
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide/CustomerSearch_svr.asp"
            document.RSLFORM.target = "SearchResults"
            document.RSLFORM.submit()
            return false
        }

        function SearchDisplay_(theType) {
            if (theType == "3") {
                document.getElementById("TypeCust1").style.display = "none"
                document.getElementById("TypeCust2").style.display = "none"
                document.getElementById("txt_TENANCYID").value = ""
                document.getElementById("txt_CUSTOMERID").value = ""
            }
            else {
                document.getElementById("TypeCust1").style.display = "block"
                document.getElementById("TypeCust2").style.display = "block"
            }
        }

        function SearchDisplay(theType) {
            var TypeCust1 = document.getElementById('TypeCust1');
            var TypeCust2 = document.getElementById('TypeCust2');
            var status = TypeCust1.style.display;
            if (theType == "3") {
                if (status == 'table-row' || status == 'block') {
                    TypeCust1.style.display = 'none';
                    TypeCust2.style.display = 'none';
                    document.getElementById("txt_TENANCYID").value = ""
                    document.getElementById("txt_CUSTOMERID").value = ""
                }
            }
            else {
                TypeCust1.style.display = 'block';
                TypeCust1.style.display = 'table-row';
                TypeCust2.style.display = 'block';
                TypeCust2.style.display = 'table-row';
            }
        }
    </script>
</head>
<body onload="initSwipeMenu(4);preloadImages();SearchDisplay(1)" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table cellspacing="0" cellpadding="0" style="height: 90%">
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" style="cursor: pointer">
                            <img name="tab_standardsearch" id="tab_standardsearch" src="Images/tab_find_customer.gif"
                                width="131" height="20" border="0" onclick="StandardSearch()" alt="" /></td>
                        <td>
                            <img src="images/spacer.gif" width="199" height="19" alt="" /></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133e71">
                            <img src="images/spacer.gif" width="199" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="images/spacer.gif" width="53" height="6" alt="" /></td>
                    </tr>
                </table>
                <table width="233" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133e71;
                    border-left: 1px solid #133e71; border-bottom: 1px solid #133e71">
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            First Name:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_FIRSTNAME" id="txt_FIRSTNAME" class="textbox200" maxlength="100" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="105px">
                            Last Name:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_LASTNAME" id="txt_LASTNAME" class="textbox200" maxlength="100" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_LASTNAME" id="img_LASTNAME" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td nowrap="nowrap">
                            <%=CustomerTitle%>
                        </td>
                        <td nowrap="nowrap">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            All
                            <input type="radio" name="rdo_CustomerType" value="1" checked="checked" onclick="SearchDisplay(1)" />
                            Individual
                            <input type="radio" name="rdo_CustomerType" value="2" onclick="SearchDisplay(2)" />
                            Organisation
                            <input type="radio" name="rdo_CustomerType" value="3" onclick="SearchDisplay(3)" />
                        </td>
                        <td nowrap="nowrap">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="3" height="10">
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="3" bgcolor="#133e71" height="1">
                        </td>
                    </tr>
                    <tr bgcolor="#F3F7FE">
                        <td nowrap="nowrap" colspan="3" height="10">
                        </td>
                    </tr>
                    <tr bgcolor="#F3F7FE">
                        <td nowrap="nowrap">
                            Address:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_ADDRESS" id="txt_ADDRESS" class="textbox200" maxlength="50" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADDRESS" id="img_ADDRESS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr bgcolor="#F3F7FE">
                        <td nowrap="nowrap">
                            Town / City:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_TOWNCITY" id="txt_TOWNCITY" class="textbox200" maxlength="50" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_TOWNCITY" id="img_TOWNCITY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr bgcolor="#f3f7fe" id="TypeCust1" style="display: block">
                        <td nowrap="nowrap">
                            Customer No:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_CUSTOMERID" id="txt_CUSTOMERID" class="textbox200" maxlength="50" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_CUSTOMERID" id="img_CUSTOMERID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr bgcolor="#f3f7fe" id="TypeCust2" style="display: block">
                        <td nowrap="nowrap" width="105px" bgcolor="#F3F7FE">
                            Tenancy No:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_TENANCYID" id="txt_TENANCYID" class="textbox200" maxlength="50" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_TENANCYID" id="img_TENANCYID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" colspan="2" align="right" bgcolor="#f3f7fe">
                            <input type="hidden" name="DONTDOIT" id="DONTDOIT" value="1" />
                            <input type="hidden" name="ADVANCED" id="ADVANCED" />
                            <input type="reset" title="RESET" value=" RESET " name="BtnRESET" id="BtnRESET" class="RSLButton" style="cursor:pointer" />
                            <input type="button" onclick="SearchNow()" title="SEARCH" value=" SEARCH " name="BtnSEARCH" id="BtnSEARCH"
                                class="RSLButton" style="cursor:pointer" />
                        </td>
                        <td bgcolor="#f3f7fe">
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table width="200" border="0" cellpadding="2" cellspacing="0" style="border-top: 1px solid #133e71;
                    border-right: 1px solid #133e71; border-left: 1px solid #133e71; border-bottom: 1px solid #133e71">
                    <tr align="center">
                        <td nowrap="nowrap" colspan="3">
                            <b><i>Legend: </i></b>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/T.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            Tenant
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_Tenant" value="1" checked="checked" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/FT.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            PreviousTenant
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_PrevTenant" id="chk_show_PrevTenant" value="1" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/JT.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            Joint Tenancy
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_Joint" id="chk_show_Joint" value="1" onclick="setValuesOfChecks('Joint')" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/PT.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            Prospective Tenant
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_Prospective" id="chk_show_Prospective" value="1" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/SH.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            Stakeholder
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_Stakeholder" id="chk_show_Stakeholder" value="1" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="29" align="center">
                            <img src="Images/G.gif" width="16" height="16" alt="" />
                        </td>
                        <td nowrap="nowrap" width="110">
                            General
                        </td>
                        <td nowrap="nowrap" width="49" align="right">
                            <input type="checkbox" name="chk_show_General" id="chk_show_General" value="1" />
                        </td>
                    </tr>
                </table>
                <p>
                    &nbsp;</p>
            </td>
            <td>
                &nbsp;&nbsp;
            </td>
            <td valign="top" height="100%">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <img name="tab_main_details" src="Images/tab_search_results.gif" width="122" height="20"
                                border="0" alt="" /></td>
                        <td>
                            <img src="images/spacer.gif" width="298" height="19" alt="" /></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133e71">
                            <img src="images/spacer.gif" width="298" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="images/spacer.gif" width="53" height="6" alt="" /></td>
                    </tr>
                </table>
                <table width="420" border="0" cellpadding="2" cellspacing="0" style="height: 90%;
                    border-right: 1px solid #133e71; border-left: 1px solid #133e71; border-bottom: 1px solid #133e71">
                    <tr>
                        <td nowrap="nowrap" height="100%" valign="top">
                            <iframe src="/secureframe.asp" name="SearchResults" height="400" width="100%" frameborder="0">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Property_Server" id="Property_Server" width="400px"
        height="400px" style="display: none"></iframe>
</body>
</html>
