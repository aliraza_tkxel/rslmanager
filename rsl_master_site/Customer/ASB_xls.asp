<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
'Response.Buffer = True
'Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_export.asp" -->
<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include 
	'		sometimes kills the xls transfer
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count
		Dim rqPatch,rqScheme,rqStatus,rqType,rqCategory,rqEmployee
		Dim rqToDate,rqFromDate,rqGrade
		Dim SQL_Filter

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        rqPatch     = Request.QueryString ("SEL_PATCH")
        rqScheme 	= Request.QueryString("SEL_SCHEME")
        rqStatus    = Request.QueryString("SEL_STATUS")
        rqType      = Request.QueryString("SEL_TYPE")
        rqCategory  = Request.QueryString("SEL_CATEGORY")
        rqEmployee  = Request.QueryString("SEL_EMPLOYEE")
        rqGrade     = Request.QueryString("SEL_GRADE")
        rqFromDate  = Request.QueryString("txt_FromDate")
        rqToDate    = Request.QueryString("txt_ToDate")
		theUser 	= Request.QueryString("WHICH")
		rqAction 	= Request.QueryString("SEL_ACTION")	    ' Action last Taken
		rqAsAtDate 	= Request.QueryString("txt_ASATDATE") 	' As At Date

        if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2)

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	Call OpenDB()
	    Call get_asblist()
	Call CloseDB()

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Function List
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Function get_asblist()

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Filter section For SQL
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

				' get patch if filter applied
                If rqPatch <> "" Then
                    SQL_Filter = SQL_Filter & " AND EP.PATCHID = " & rqPatch
                End If

                If rqScheme <> "" Then
                    SQL_Filter = SQL_Filter & " AND SCH.SCHEMEID = " & rqScheme
                End If

                If rqType <> "" Then
                    SQL_Filter = SQL_Filter & " AND J.ITEMNATUREID = " & rqType
                End If

                If rqGrade <> "" Then
                    SQL_Filter = SQL_Filter & " AND CAT.GRADEID = " & rqGrade
                End If

                If rqCategory <> "" Then
                    SQL_Filter = SQL_Filter & " AND AC.CATEGORYID = " & rqCategory
                End If

                If rqEmployee <> "" Then
                    SQL_Filter = SQL_Filter & " AND E.EMPLOYEEID = " & rqEmployee
                End If

                If rqFromDate <> "" Then
                    SQL_Filter = SQL_Filter & " AND J.CREATIONDATE >= '" & rqFromDate & " 00:00:00'"
                End If

                If rqToDate <> "" Then
                    SQL_Filter = SQL_Filter & " AND J.CREATIONDATE <= '" & rqToDate & " 23:59:00'"
                End If

                If rqStatus <> "" Then
                    SQL_Filter = SQL_Filter & " AND J.CURRENTITEMSTATUSID = " & rqStatus
                End If

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  End Filter Section
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Start to Build arrears list 
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					Dim strSQL, rsSet, intRecord

					intRecord = 0
					str_data = ""
			strSQL =		"SELECT J.CREATIONDATE, J.CUSTOMERID, " &_ 
							"		LTRIM(ISNULL(TIT.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS CNAME, " &_
							"		P.HOUSENUMBER + ' ' + P.ADDRESS1 AS ADDRESS, N.DESCRIPTION, G.NOTES, " &_
							"		LTRIM(ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,' ')) AS ENAME, " &_
							"		ISNULL(CAT.DESCRIPTION,AC.DESCRIPTION) AS CATEGORY,J.TITLE,CAT.GRADEID AS GRADE " &_
							" FROM 	C_JOURNAL J  " &_
							"		INNER JOIN C_ASB G ON J.JOURNALID = G.JOURNALID " &_
                            " LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE CAT ON CAT.CATEGORYID=G.CATEGORY " &_
							"		LEFT JOIN  C_ASB_CATEGORY AC ON AC.CATEGORYID = G.CATEGORY " &_
							"		INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID  " &_
							"		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
							"		LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " &_
							"		INNER JOIN (  " &_
							"				SELECT MAX(TENANCYID) AS MAXTEN, CUSTOMERID  " &_
							"				FROM   C_CUSTOMERTENANCY  " &_
							"				GROUP BY CUSTOMERID " &_
							"			   ) MT ON MT.CUSTOMERID = C.CUSTOMERID " &_
							"		INNER JOIN (  " &_
							"				SELECT MAX(ASBHISTORYID) AS MAXHIST, JOURNALID " &_
							"				FROM   C_ASB " &_
							"				GROUP BY JOURNALID " &_
							"			   ) MH ON MH.JOURNALID = J.JOURNALID " &_
							"		INNER JOIN C_TENANCY T ON T.TENANCYID = MT.MAXTEN  " &_
							"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
							"       LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " &_
                            "	    LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
                            "       INNER JOIN P_SCHEME SCH ON P.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
							"       LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID " &_
							"		INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
							"		INNER JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE " &_
							"	WHERE J.ITEMNATUREID IN (8,9,24,25) " & SQL_Filter & " " &_
							"   AND G.ASBHISTORYID=(SELECT MAX(ASBHISTORYID) FROM C_ASB WHERE JOURNALID=G.JOURNALID) "&_
							"	ORDER BY J.CREATIONDATE DESC, LTRIM(ISNULL(TIT.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) ASC "

					Call OpenRs(rsSet, strSQL)

	            	while NOT rsSet.EOF

						str_data = 	 str_data & "<tr><td style=""border:1px solid black;"">" & rsSet("CREATIONDATE") & "</td>" &_
					                                "<td style=""border:1px solid black;"">" & rsSet("TITLE") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("CNAME") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("ADDRESS") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("DESCRIPTION") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("GRADE") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("CATEGORY") & "</td>" &_
								                    "<td style=""border:1px solid black;"">" & rsSet("ENAME") & "</td></tr>"

		                rsSet.moveNext()
		            wend

		            Call CloseRs(rsSet)
		            SET rsSet = Nothing

				End function

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Function List
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>RSL Manager Customers --> ASB List </title>
        <link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
     </head>
      <body style="margin-left:10px;margin-top:10px; background-color:White;">
            <table style="width:750px" cellpadding="1"cellspacing="2">
                <thead>
                    <tr align="left">
                        <td  colspan="7">
                            <div style="text-align:left; color:#0066CC; font-size:10; font-weight:bold;">
                                ASB List Report-
                                <%
                                  Dim todaysDate
				                  todaysDate=now()
				                  rw (todaysDate)
				                %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid black;"><b>Created</b></td>
                        <td style="border:1px solid black;"><b>Title</b></td>
                        <td style="border:1px solid black;"><b>Customer Name</b></td>
                        <td style="border:1px solid black;"><b>Address</b></td>
                        <td style="border:1px solid black;"><b>Type</b></td>
                        <td style="border:1px solid black;"><b>Grade</b></td>
                        <td style="border:1px solid black;"><b>Category</b></td>
                        <td style="border:1px solid black;"><b>Employee</b></td>
                    </tr>
               </thead> 
               <%=str_data%>
            </table>
       </body>
</html>