<%@  language="VBSCRIPT" codepage="1252" %>
<%
'Response.Buffer = True
'Response.ContentType = "application/x-msexcel"
%>
<!--#INCLUDE VIRTUAL="Includes/Functions/Concat.asp" -->
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data

	Dim str_data2
	Dim strUsers2
	Set str_data2 = New FastString
	Set strUsers2 = New FastString

	Dim count, my_page_size
	Dim theURL	
	Dim PageName
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "MailingList.asp"
	MaxRowSpan = 6
	'Dim orderBy
	'	orderBy = "C.LASTNAME ASC, C.FIRSTNAME ASC"
	'	if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
	
	Call OpenDB()
	Dim CompareValue
	Call get_newtenants()
	Call CloseDB()	
	
	Function get_newtenants()

    	  SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	      'Call OpenDB()
	      Call OpenRs(rsDEFAULTS, SQL)
		    doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	      Call CloseRs(rsDEFAULTS)
	      'Call CloseDB()

        FullPath = doc_DEFAULTPATH & "\customer\mailingLists\"

'GP_FullFileName = GP_FilePath & GP_curPath & GP_CurFileName
'rw GP_FullFileName
'Response.End()

		Set fso = CreateObject("Scripting.FileSystemObject")
		
		'GP_curPath = Request.ServerVariables("PATH_INFO")
		'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
		'if Mid(GP_curPath,Len(GP_curPath),1) <> "/" then
		'	GP_curPath = GP_curPath & "/"
		'end if

		CurrentDate = FormatDateTime(Date,2)
		CurrentDate = Replace(CurrentDate, "/", "")
		CurrentDate = Replace(CurrentDate, " ", "")

		'FullPath = Trim(Server.mappath(GP_curPath)) & "\MailingLists\"
		FileName = "MailingList-" & CurrentDate
		
		If (fso.FileExists(FullPath & FileName & ".csv")) Then
			GP_FileExist = true
		End If   
		if GP_FileExist then
			Begin_Name_Num = 0
			while GP_FileExist    
				Begin_Name_Num = Begin_Name_Num + 1
				TempFileName = FileName & "_" & Begin_Name_Num
				GP_FileExist = fso.FileExists(FullPath & TempFileName & ".csv")
			wend  
			FileName = TempFileName
		end if
		Set MyFile= fso.CreateTextFile(FullPath & FileName & ".csv", True)

		MyFile.WriteLine "Customer, Tenancy Reference, Address1, Address2, Address3, Town/City, Postcode, County, Patch, Local Authority, Communication"

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
				
		Dim strSQL, rsSet, intRecord 

		intRecord = 0

		strSQL = "C_MAILING_LIST"
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.

			str_data2.Reset
			str_data = ""

			PrevTenancy = ""
			while NOT rsSet.EOF
				TenancyID = rsSet("TENANCYID")
				CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
						"FROM C_CUSTOMERTENANCY CT " &_
						"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
						"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_
						"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & TenancyID & " ORDER BY CT.CUSTOMERID ASC"
				Call OpenRs(rsCust, CustSQL)
				strUsers = ""
				strUsers2.Reset
				CustCount = 0
				while NOT rsCust.EOF
					if CustCount = 0 then
						strUsers2.append rsCust("FULLNAME")
						CustCount = 1
					else
						strUsers2.append " & "
						strUsers2.append rsCust("FULLNAME")
					end if
					rsCust.moveNext()
				wend
				CloseRs(rsCust)
				SET rsCust = Nothing
				
				str_data2.Append "" 
				str_data2.Append strUsers2.concat 
                str_data2.Append ","
				str_data2.Append TenancyID
				str_data2.Append ","
				str_data2.Append rsSet("HOUSENUMBER") 
				str_data2.Append " " 
				str_data2.Append rsSet("ADDRESS1") 
				str_data2.Append ","
				str_data2.Append rsSet("ADDRESS2") 
				str_data2.Append ","
				str_data2.Append rsSet("ADDRESS3") 
				str_data2.Append ","
				str_data2.Append rsSet("TOWNCITY")
				str_data2.Append ","
				str_data2.Append rsSet("POSTCODE") 
				str_data2.Append "," 
				str_data2.Append rsSet("COUNTY")
				str_data2.Append "," 
				str_data2.Append rsSet("PATCH") 
				str_data2.Append "," 
				str_data2.Append rsSet("LOCALAUTHORITY")  
				str_data2.Append "," 
				str_data2.Append rsSet("COMMUNICATION")  
 									
				count = count + 1
				rsSet.movenext()
			
				MyFile.WriteLine str_data2.concat
				
				str_data2.Reset
			wend
		End If

		
		rsSet.close()
		Set rsSet = Nothing

		MyFile.Close

		'FINALLY ADD A ROW INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
		Conn.Execute ("INSERT INTO C_MAILINGLIST ( FILENAME, PROCESSDATE) VALUES (" &_
						"'" & FileName & ".csv" & "', " &_
						"'" & FormatDateTime(Date,1) & "') ")
						
	End function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> General Enquiry List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="parent.STOPLOADER();parent.location.href='MailingListCSV.asp'">
</body>
</html>
