<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_export.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
		OpenDB()
		
				'build the scheme and action sql 
		rqCustomerStatus=1
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to 
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		rqPatch =Request("sel_PATCH") 'Patch arrears property belong to
		rqAssettype =Request("sel_AssetType") 'asset type arrears property belong to
		rqCustomerStatus=Request("CustomerStatus")
		
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
		orderBy = " RENTBALANCE DESC "
		if (Request("CC_Sort") <> "") then 
		    orderBy = Request("CC_Sort")
		    'Response.Write(orderBy)
		end if
		
		' What rent balance value to compare against
		if (Request("COMPAREVALUE") <> "") then
			CompareValue = Request("COMPAREVALUE")
		end if
		
		' What max rent balance value to compare against
		if (Request("MAX_COMPAREVALUE") <> "") then
			Max_CompareValue = Request("MAX_COMPAREVALUE")
		end if
	
	
		' The Scheme that the property applies to ( Also known as Development )
		if rqScheme <> "" then	
			Scheme_sql = " and dev.developmentid = " & rqScheme	
		else	
			Scheme_sql = "" 
		End If
		
		' The patch that the property applies to 
		if rqPatch <> "" then	
			patch_sql = " and ep.patchid = " & rqPatch	
		else	
			patch_sql = "" 
		End If	

        ' The asserttype that the property applies to 
		if rqAssettype <> "" then	
			assettype_sql = " and p.assettype = " & rqAssettype	
		else	
			assettype_sql = "" 
		End If	

		SQL = " execute ARREARS_GET_CUSTOMER_HB_RB_SLB_NEWEXPORT ' " & rqCustomerStatus &  "','" & CompareValue & "','" & rqAsAtDate  & "', '" & Scheme_sql & "','" & OrderBy &  "','" & Patch_Sql & "','" & Max_CompareValue & "','" & assettype_sql & "'"
		
	
	    Call OpenRs(rsSet, SQL)
		if rsSet.EOF and rsSet.BOF then
			response.write "<font face=""Verdana, Arial, Helvetica, sans-serif"" size=""1"">The export returned no data between the specified dates. " & _
			"<a href=""javascript:window.close()"">Click here</a> to close this window.</font>"
		else
		
			strBody = rsSet.GetString(,,", ",vbCrLf,"")
			Dim F, Head
			For Each F In rsSet.Fields
  			Head = Head &  F.Name & ", "
			Next
			Head = left(Head,len(Head)-2) & vbCrLf
			Response.clear
			Response.ContentType = "application/vnd.ms-excel"  
			Response.AddHeader "Content-Disposition","attachment; filename=ArrearsList2.csv"
			Response.Write Head
			Response.Write strBody 
			
			end if
					
		
		CloseRs(rsSet)
		
		CloseDB()
	

	
					

						  
							

%>
