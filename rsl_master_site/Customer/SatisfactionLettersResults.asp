<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%

	OpenDB()
		SQL = " SELECT DISTINCT R.CONTRACTORID, O.NAME AS ContractorName " &_
				" from C_SATISFACTIONLETTER SL  " &_
				"  	INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID  " &_
				" 	INNER JOIN C_REPAIR R ON R.JOURNALID = SL.JOURNALID  " &_
				" 	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
				" 	INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				" 	INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID " &_
				" 	INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID " &_
				" 	INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_
				" WHERE SL.SATISFACTION_LETTERSTATUS = 2 AND R.ITEMACTIONID IN (5,6,10,15,9)" 
	Call OpenRs(rsContractor, SQL)
	
	'Build Select Box to search for a particular contractor
	ListBox = "<select name='WhatContractor' class='textbox200'><option value=''>Select a Contractor</option>"
	while  not rsContractor.eof
		ListBox = ListBox & "<option value='" & rsContractor("ContractorID") & "'>" & rsContractor("ContractorName") & "</option>"
	
	rsContractor.movenext
	
	Wend
	ListBox = ListBox & "</select>"
	Call CloseRs(rsContractor)	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	FormFields[0] = "txt_ORDERNO|Order No.|INTEGER|N"

	function GetList(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/SatisfactionLetterResults_srv.asp";
		RSLFORM.submit();
	}
	
	function quick_find(){
	
		if (!checkForm()) return false
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/SatisfactionLetterResults_srv.asp";
		RSLFORM.submit();
	}	
  	
	function open_me(ORDERITEMID,JOURNALID){
		event.cancelBubble = true;
		window.open ("Popups/SatisfactionRecorder.asp?ORDERITEMID=" + ORDERITEMID + "&JOURNALID=" + JOURNALID,"display",	'width=420px,height=430px,scrollbars=yes')
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post>
  <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
    <TR>
      <TD>Select a Repair Item to send a 'Satisfaction Survey Letter' to. Select 
        the checkbox and then click the '<b>Send Survey</b>' button.</TD>
    </TR>
    <TR> 
      <TD align="right">

Order No.&nbsp;
<input name="txt_ORDERNO" ID="txt_ORDERNO"  type="text" class="textbox100" >
<img src="/js/FVS.gif" width="15" height="15" name="img_ORDERNO"> <%=ListBox%>
<input type='BUTTON' name='btn_FIND' title='Search for matches using Supplier Name.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'></TD>
    </TR>
  </TABLE>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_slip width=1px height=1px style='display:block'></iframe>
</BODY>
</HTML>

