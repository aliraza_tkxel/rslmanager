<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CustomerType, title, ethnicity, maritalstatus, employmentstatus, Religion,addReadonly
	Dim SexualOrientation, PreferedContact, TitleOther, Firstname, MiddleName
	Dim LastName, Gender, DOB, NINumber, consent_box_status, localauthority

	Dim HouseNumber, Address1, Address2, Address3, TownCity, County, PostCode
	Dim FirstLanguage, EthnicityOther, ReligionOther, SexualOrientation_Other, DISABLECUSTOMERTYPE, BenefitOther
	Dim Communication_Other, Disability_Other, Benefit_Other, communication, Disability, OccupantsAdults,OccupantsChildren,SupportAgencies, Benefit, BankFacility, BankFacility_other
	Dim occupation
	Dim InternetAccess, owntransport, nationality, carer, wheelchair, NationalityOther, InternetAccess_Other, SupportAgencies_other, houseincome
	Dim rentstatmenttype, EmployerName, EmpAddress1, EmpAddress2, EmpTownCity, EmpPostcode , textyesno, emailyesno, TakeHomePay, isProspectiveTenant

Call OpenDB()

AMENDRECORD = Request("AMENDRECORD")

If AMENDRECORD <> "" Then 
	CustomerID = Request("CustomerID")
	Call GetVarsToAmend() 
	DISABLECUSTOMERTYPE = "DISABLED"
   
    if CustomerType = 1 Then
        addReadonly = "  "            
    Else 
        addReadonly = " readonly "
        tenancyAddress = True                
    End If
     
Else
	FirstName 	= Request("txt_FIRSTNAME")
	LastName	= Request("txt_LASTNAME")
End If




Function GetVarsToAmend()

    tenancyAddress = false
    <!--Updated Customer-->
            SQL = "SELECT ISNULL(C.CUSTOMERTYPE,0) AS CUSTOMERTYPE, E.FIRSTNAME + ' ' + E.LASTNAME AS RENTOFFICERNAME, LTRIM(ISNULL(G.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS FULLNAME,  "&_
			"TC.TENANCYCLOSEDID, " &_
			"T.TENANCYID AS TTENANCYID, " &_
			"ISNULL(T.TENANCYID, '') AS TENANCYREF, " &_
			"ISNULL(P.PROPERTYID, '') AS PROPERTYID, " &_
			"ISNULL(TT.DESCRIPTION, '') AS TENANCYTYPE, " &_
			"ISNULL(CONVERT(NVARCHAR, CT.STARTDATE, 103) ,' ') AS FSTARTDATE, " &_
			"ISNULL(CONVERT(NVARCHAR, CT.ENDDATE, 103) ,' ') AS FENDDATE, " &_
			"CONVERT(NVARCHAR, CT.STARTDATE, 103) AS CT_FSTARTDATE, " &_
			"CONVERT(NVARCHAR, CT.ENDDATE, 103) AS CT_FENDDATE, " &_
			"ISNULL(P.HOUSENUMBER , ' ' ) AS HOUSENUMBER, " &_
			"ISNULL(P.FLATNUMBER , ' ' ) AS FLATNUMBER, " &_
			"ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
			"ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
			"ISNULL(P.ADDRESS3, ' ' ) AS ADDRESS3, " &_
			"ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
			"ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
			"ISNULL(P.COUNTY , ' ' ) AS COUNTY, ISNULL(PF.TOTALRENT,0) AS TOTALRENT, " &_
			"ISNULL(P.DEVELOPMENTID , ' ' ) AS DEVELOPMENTID, " &_
			"ISNULL(PF.RENT,0) AS RENT, ISNULL(PF.SERVICES,0.00) AS SERV, ISNULL(PF.COUNCILTAX,0) AS COUNCILTAX, " &_
			"ISNULL(PF.WATERRATES,0) AS WATER, ISNULL(PF.INELIGSERV,0) AS INELIG, ISNULL(PF.SUPPORTEDSERVICES,0) AS SUPP, CASE WHEN T.CBL=1 THEN 'Yes' WHEN T.CBL=0 THEN 'No' END AS CBL " &_ 
			"FROM C_CUSTOMERTENANCY CT " &_
			"INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID " &_
			"LEFT JOIN C_TENANCYCLOSED TC ON T.TENANCYID = TC.TENANCYID " &_
			"LEFT JOIN C_TENANCYTYPE TT ON T.TENANCYTYPE = TT.TENANCYTYPEID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " &_
			"LEFT JOIN P_FINANCIAL PF ON PF.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN (SELECT PATCH,EMPLOYEEID FROM E_JOBDETAILS WHERE ACTIVE=1 AND JOBTITLE= 'Rent Recovery Officer') EMP ON EMP.PATCH=D.PATCHID " &_
			"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = EMP.EMPLOYEEID " &_
			"WHERE CT.CUSTOMERID = " & CUSTOMERID & " " &_
			"ORDER BY CT.CUSTOMERTENANCYID DESC "

	        Call OpenDB()
	        Call OpenRS(rsCustomer, SQL)

            If Not rsCustomer.eof Then
                HouseNumber			=	rsCustomer("HOUSENUMBER")
			    Address1			=	rsCustomer("ADDRESS1")
			    Address2			=	rsCustomer("ADDRESS2")
			    Address3			=	rsCustomer("ADDRESS3")
			    TownCity			=	rsCustomer("TOWNCITY")
			    County				=	rsCustomer("COUNTY")
			    PostCode			=	rsCustomer("POSTCODE")
                tenancyAddress      =   true

            End If

            <!--Updated Customer-->
	SQL = "SELECT * " &_
			"FROM C__CUSTOMER C"&_
			"	LEFT JOIN C_ADDRESS A ON A.CUSTOMERID = C.CUSTOMERID " &_
			"WHERE C.CUSTOMERID = " & CustomerID
     
	Call OpenRS(rsCustomer, SQL)

	If Not rsCustomer.eof Then

			CustomerType		=	rsCustomer("CUSTOMERTYPE")
			title				=	rsCustomer("TITLE")
			ethnicity			=	rsCustomer("ETHNICORIGIN")
			maritalstatus		=	rsCustomer("MARITALSTATUS")
			employmentstatus	=	rsCustomer("EMPLOYMENTSTATUS")
			occupation 			=	rsCustomer("OCCUPATION")
			Religion			=	rsCustomer("RELIGION")
			Disability			=	rsCustomer("DISABILITY")
			communication		=	rsCustomer("COMMUNICATION")
			InternetAccess		=	rsCustomer("INTERNETACCESS")
			SupportAgencies		=	rsCustomer("SUPPORTAGENCIES")
			Benefit				=	rsCustomer("BENEFIT")
			BankFacility		=	rsCustomer("BANKFACILITY")

			SexualOrientation	=	rsCustomer("SEXUALORIENTATION")
			PreferedContact		=	rsCustomer("PREFEREDCONTACT")

			ethnicityOther			=	rsCustomer("ETHNICORIGINOther")
			ReligionOther			=	rsCustomer("RELIGIONOTHER")
			DisabilityOther			=	rsCustomer("DISABILITYOTHER")
			communicationOther		=	rsCustomer("COMMUNICATIONOTHER")
			SexualOrientationOther	=	rsCustomer("SEXUALORIENTATIONOTHER")

			TitleOther			=	rsCustomer("TITLEOTHER")
			Firstname			=	rsCustomer("FIRSTNAME")
			MiddleName			=	rsCustomer("MIDDLENAME")
			LastName			=	rsCustomer("LASTNAME")
			Gender				=	rsCustomer("GENDER")
			DOB					=	rsCustomer("DOB")
			NINumber			=	rsCustomer("NINUMBER")


			FirstLanguage				=	rsCustomer("FIRSTLANGUAGE")
			Ethnicity_Other				=	rsCustomer("ETHNICORIGINOTHER")
			Religion_Other				=	rsCustomer("RELIGIONOTHER")
			SexualOrientation_Other 	=	rsCustomer("SEXUALORIENTATIONOTHER")
			Communication_Other			=	rsCustomer("COMMUNICATIONOTHER")
			PreferedContact				=	rsCustomer("PREFEREDCONTACT")
			Disability_Other			=	rsCustomer("DISABILITYOTHER")
            Benefit_Other			    =	rsCustomer("BENEFITOTHER")

			OccupantsAdults				=	rsCustomer("OCCUPANTSADULTS")
			OccupantsChildren			=	rsCustomer("OCCUPANTSCHILDREN")
			consent						=	rsCustomer("CONSENT")

			localauthority				=	rsCustomer("LOCALAUTHORITY")

			rentstatmenttype			=	rsCustomer("RENTSTATEMENTTYPE")
			nationality					=	rsCustomer("NATIONALITY")
			nationalityother			=	rsCustomer("NATIONALITYOTHER")
			owntransport				=	rsCustomer("OWNTRANSPORT")
			wheelchair					=	rsCustomer("WHEELCHAIR")
			carer						=	rsCustomer("CARER")
			InternetAccess_Other		=	rsCustomer("INTERNETACCESSOTHER")
			SupportAgencies_other		=	rsCustomer("SUPPORTAGENCIESOTHER")
			BankFacility_other			=	rsCustomer("BANKFACILITYOTHER")
			houseIncome					=	rsCustomer("HOUSEINCOME")

			EmployerName	=	rsCustomer("EMPLOYERNAME")
			EmpAddress1		=	rsCustomer("EMPADDRESS1")
			EmpAddress2		=	rsCustomer("EMPADDRESS2")
			EmpTownCity		=	rsCustomer("EMPTOWNCITY")
			EmpPostCode		=	rsCustomer("EMPPOSTCODE")
			textyesno		=	rsCustomer("TEXTYESNO")
			emailyesno		=	rsCustomer("EMAILYESNO")
            TakeHomePay     =   rsCustomer("TAKEHOMEPAY")

            if tenancyAddress <> True Then
                HouseNumber			=	rsCustomer("HOUSENUMBER")
			    Address1			=	rsCustomer("ADDRESS1")
			    Address2			=	rsCustomer("ADDRESS2")
		    	Address3			=	rsCustomer("ADDRESS3")
			    TownCity			=	rsCustomer("TOWNCITY")
			    County				=	rsCustomer("COUNTY")
			    PostCode			=	rsCustomer("POSTCODE")
            Else 
                tenancyAddress = True
            End If
            

			If consent <> "" Then consent_box_status = "checked" Else consent_box_status = "" End If

	End If

	Call CloseRs(rsCustomer)

End Function

Call BuildSelect(lstCustomerType, "sel_CUSTOMERTYPE", "C_CUSTOMERTYPE", "CUSTOMERTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 1, NULL, "textbox200", DISABLECUSTOMERTYPE & " tabIndex='1'")
Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "Please Select", title, NULL, "textbox200", "onchange='title_change()' tabIndex='1'")
Call BuildSelect(lstEthnicities, "sel_ETHNICORIGIN", "G_ETHNICITY", "ETHID, DESCRIPTION", "DESCRIPTION", "Please Select", ethnicity, NULL, "textbox200", "onchange='Disable_Other_Boxes()' tabIndex='1'")
Call BuildSelect(lstMaritalStatus, "sel_MARITALSTATUS", "G_MARITALSTATUS", "MARITALSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", maritalstatus, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstEmploymentStatus, "sel_EMPLOYMENTSTATUS", "C_EMPLOYMENTSTATUS", "EMPLOYMENTSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", employmentstatus, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstReligion, "sel_RELIGION", "G_RELIGION", "RELIGIONID, DESCRIPTION", "DESCRIPTION", "Please Select", Religion, NULL, "textbox200", "onchange='Disable_Other_Boxes()' tabIndex='1'")
Call BuildSelect(lstSexualOrientation, "sel_SEXUALORIENTATION", "G_SEXUALORIENTATION", "SEXUALORIENTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", SexualOrientation, NULL, "textbox200", "onchange='Disable_Other_Boxes()'  tabIndex='1'")
Call BuildSelect(lstPreferedContact, "sel_PREFEREDCONTACT", "G_PREFEREDCONTACT", "PREFEREDCONTACTID, DESCRIPTION", "DESCRIPTION", "Please Select", PreferedContact, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstLOCALAUTHORITY, "sel_LOCALAUTHORITY", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION", "DESCRIPTION", "Please Select", localauthority, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstRentStatementtype, "sel_RENTSTATEMENTTYPE", "C_RENTSTATEMENTTYPE", "RENTSTATEMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", rentstatmenttype, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstTransport, "sel_OWNTRANSPORT", "G_YESNO", "YESNOID, DESCRIPTION", "DESCRIPTION", "Please Select", owntransport, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstWheelChair, "sel_WHEELCHAIR", "G_YESNO", "YESNOID, DESCRIPTION", "DESCRIPTION", "Please Select", wheelchair, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstCarer, "sel_CARER", "G_YESNO", "YESNOID, DESCRIPTION", "DESCRIPTION", "Please Select", carer, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstText, "sel_TEXTYESNO", "G_YESNO", "YESNOID, DESCRIPTION", "DESCRIPTION", "Please Select", textyesno, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstEmail, "sel_EMAILYESNO", "G_YESNO", "YESNOID, DESCRIPTION", "DESCRIPTION", "Please Select", emailyesno, NULL, "textbox200", "tabIndex='2'")
Call BuildSelect(lstNationalities, "sel_NATIONALITY", "G_CUSTOMERNATIONALITY", "NATID, DESCRIPTION", "NATID", "Please Select", nationality, NULL, "textbox200", "onchange='Disable_Other_Boxes()' tabIndex='1'")
Call BuildSelect(lstHouseIncome, "sel_HOUSEINCOME", "G_HOUSEINCOME", "INCOMEID, DESCRIPTION", "INCOMEID", "Please Select", houseincome, NULL, "textbox200", "tabIndex='1'")

	' GET THE LIST OF RECORDS (checks are activated in javascript function onload)
	SQL = "SELECT COMMUNICATIONID, DESCRIPTION FROM G_COMMUNICATION "
	Call OpenRS(rsCommunication, SQL)
	Communication_String = ""

  If Not rsCommunication.EOF Then
            Communication_String = Communication_String & "<table>"
            ComLinkCount = 0

            ' Do until the end of all records
            Do Until rsCommunication.EOF

             'change 5 to any number of columns you like
            If ComLinkCount Mod 3 = 0 Then

                If rsCommunication("DESCRIPTION") = "Other" Then
                    ComOtherID = ComLinkCount + 1
                    placeOnClick = " onclick=""Disable_Other_Boxes()"" "
                Else
                    placeOnClick = ""
                End If

               If ComLinkCount <> 0 Then Communication_String = Communication_String & "</tr>"
                  Communication_String = Communication_String &"<tr><td width=""24""><input type=""checkbox"" name='chk_Communication" & (ComLinkCount + 1) & "'  id='chk_Communication" & (ComLinkCount + 1) & "' value='" & rsCommunication("COMMUNICATIONID") & "' " & placeOnClick  & "></td><td width=""150"">" & rsCommunication("DESCRIPTION") & "</td>"
               Else
                  Communication_String = Communication_String &"<td width=""24""><input type=""checkbox"" name='chk_Communication" & (ComLinkCount + 1) & "'  id='chk_Communication" & (ComLinkCount + 1) & "' value='" & rsCommunication("COMMUNICATIONID") & "' " & placeOnClick  & "></td><td width=""150"">" & rsCommunication("DESCRIPTION") & "</td>"
               End If

			   ComLinkCount = ComLinkCount + 1

            'loop till end of records
            rsCommunication.MoveNext
            Loop

            Communication_String = Communication_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If


	SQL = "SELECT DISABILITYID, DESCRIPTION FROM G_DISABILITY "
	Call OpenRS(rsDisability, SQL)
	Disability_String = ""

  If Not rsDisability.EOF Then
            Disability_String = Disability_String & "<table>"
            DisLinkCount = 0

            ' Do until the end of all records
            Do Until rsDisability.EOF

             'change 5 to any number of columns you like 
            If DisLinkCount Mod 3 = 0 Then

                If rsDisability("DESCRIPTION") = "Other" Then
                    DisOtherID = DisLinkCount + 1
                    placeOnClick = " onclick=""Disable_Other_Boxes()"" "
                Else
                    placeOnClick = ""
                End If

               If DisLinkCount <> 0 Then Disability_String = Disability_String & "</tr>"
                  Disability_String = Disability_String &"<tr><td width=""24""><input type=""checkbox"" name='chk_Disability" & rsDisability("DISABILITYID") & "' id='chk_Disability" & rsDisability("DISABILITYID") & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
               Else
                  Disability_String = Disability_String &"<td width=""24""><input type=""checkbox"" name='chk_Disability" & rsDisability("DISABILITYID") & "' id='chk_Disability" & rsDisability("DISABILITYID") & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
               End If

               DisLinkCount = DisLinkCount + 1

            'loop till end of records
            rsDisability.MoveNext
            Loop

            Disability_String = Disability_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If


         SQL = "SELECT INTERNETACCESSID,DESCRIPTION FROM G_INTERNETACCESS "
	     Call OpenRS(rsInternetAccess, SQL)
	     InternetAccess_String = ""

          If Not rsInternetAccess.EOF Then
             InternetAccess_String = InternetAccess_String & "<table>"
             InternetLinkCount = 0

             ' Do until the end of all records
             Do Until rsInternetAccess.EOF

              'change 5 to any number of columns you like
              If InternetLinkCount Mod 3 = 0 Then

                If rsInternetAccess("DESCRIPTION") = "Other" then
                    InternetOtherID = InternetLinkCount + 1
                    placeOnClick = " onclick=""Disable_Other_Boxes()"" "
                Else
                    placeOnClick = ""
                End If

                If InternetLinkCount <> 0 Then InternetAccess_String = InternetAccess_String & "</tr>"
                  InternetAccess_String = InternetAccess_String &"<tr><td width=""24""><input type='checkbox' name='chk_InternetAccess" & (InternetLinkCount + 1) & "' id='chk_InternetAccess" & (InternetLinkCount + 1) & "' value='" & rsInternetAccess("INTERNETACCESSID") & "' " & placeOnClick & "></td><td width=""150"">" & rsInternetAccess("DESCRIPTION") & "</td>"
               Else
                  InternetAccess_String = InternetAccess_String &"<td width=""24""><input type='checkbox' name='chk_InternetAccess" & (InternetLinkCount + 1) & "' id='chk_InternetAccess" & (InternetLinkCount + 1) & "' value='" & rsInternetAccess("INTERNETACCESSID") & "' " & placeOnClick & "></td><td width=""150"">" & rsInternetAccess("DESCRIPTION") & "</td>"
               End If

               InternetLinkCount = InternetLinkCount + 1

            'loop till end of records
            rsInternetAccess.MoveNext
            Loop

            InternetAccess_String = InternetAccess_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If

         SQL = "SELECT AGID,DESCRIPTION FROM G_SUPPORTAGENCIES "
	     Call OpenRS(rsSupportAgencies, SQL)
	     SupportAgencies_String = ""

          If Not rsSupportAgencies.EOF Then
             SupportAgencies_String = SupportAgencies_String & "<table>"
             AgencyLinkCount = 0

             ' Do until the end of all records
             Do Until rsSupportAgencies.EOF

              'change 5 to any number of columns you like
              If AgencyLinkCount Mod 3 = 0 Then
    			'RW "COUNT IS" & AgencyLinkCount
                If rsSupportAgencies("DESCRIPTION") = "Other" Then
			   	    AgencyOtherID = AgencyLinkCount + 1
				    placeOnClick = " onclick='Disable_Other_Boxes()' "
				Else
				    placeOnClick = ""
			    End If

                If AgencyLinkCount <> 0 Then SupportAgencies_String = SupportAgencies_String & "</tr>"
                  SupportAgencies_String = SupportAgencies_String &"<tr><td width=""24""><input type='checkbox' name='chk_SupportAgencies" & (AgencyLinkCount + 1) & "' id='chk_SupportAgencies" & (AgencyLinkCount + 1) & "' value='" & rsSupportAgencies("AGID") & "' " & placeOnClick & "></td><td width=""150"">" & rsSupportAgencies("DESCRIPTION") & "</td>"
               Else
                  SupportAgencies_String = SupportAgencies_String &"<td width=""24""><input type='checkbox' name='chk_SupportAgencies" & (AgencyLinkCount + 1) & "' id='chk_SupportAgencies" & (AgencyLinkCount + 1) & "' value='" & rsSupportAgencies("AGID") & "' " & placeOnClick & "></td><td width=""150"">" & rsSupportAgencies("DESCRIPTION") & "</td>"
               End If

               AgencyLinkCount = AgencyLinkCount + 1

            'loop till end of records
            rsSupportAgencies.MoveNext
            Loop

            SupportAgencies_String = SupportAgencies_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If


          SQL = "SELECT BENEFITID,DESCRIPTION FROM G_BENEFIT "
	     Call OpenRS(rsBenefit, SQL)
	        Benefit_String = ""

            
        
        If Not rsBenefit.EOF Then
            Benefit_String = Benefit_String & "<table><tr>"
            BenefitCount = 0
            Dim colLoop
            colLoop = 0
            ' Do until the end of all records
            While NOT rsBenefit.EOF

                'change 5 to any number of columns you like
                if colLoop < 3 then
                    
                    If rsBenefit("DESCRIPTION") = "Other" Then
                        
                        placeOnClick = " onclick='Disable_Other_Boxes()' "
                    Elseif AMENDRECORD <> "" AND rsBenefit("DESCRIPTION") = "Universal Credit" Then
                        placeOnClick = " onclick='return false' "
                        
                    Else
                        placeOnClick = ""
                    End If
                   
                    Benefit_String = Benefit_String &"<td width=""24""><input type=""checkbox"" name='chk_Benefit" & (BenefitCount + 1) & "' id='chk_Benefit" & (BenefitCount + 1) & "' value='" & rsBenefit("BENEFITID") & "' " & placeOnClick & "></td><td width=""150"">" & rsBenefit("DESCRIPTION") & "</td>"
                    BenefitCount = BenefitCount + 1
                    colLoop = colLoop + 1
                    
                    rsBenefit.MoveNext()
                else
                    colLoop = 0
                    Benefit_String = Benefit_String & "</tr><tr>"
                End if
            wend



        

            Benefit_String = Benefit_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If

          SQL = "SELECT BANKID,DESCRIPTION FROM G_BANKFACILITY "
	     Call OpenRS(rsBankFacility, SQL)
	     BankFacility_String = ""

          If Not rsBankFacility.EOF Then
             BankFacility_String = BankFacility_String & "<table>"
             BankCount = 0

             ' Do until the end of all records
             Do Until rsBankFacility.EOF

              'change 5 to any number of columns you like
              If BankCount Mod 3 = 0 Then
                If rsBankFacility("DESCRIPTION") = "Other" then
                    BankOtherID = BankCount + 1
                    placeOnClick = " onclick=""Disable_Other_Boxes()"" "
                Else
                    placeOnClick = ""
                End If

                If BankCount <> 0 Then BankFacility_String = BankFacility_String & "</tr>"

                  BankFacility_String = BankFacility_String &"<tr><td width=""24""><input type=""checkbox"" name='chk_BankFacility" & (BankCount + 1) & "' id='chk_BankFacility" & (BankCount + 1) & "' value='" & rsBankFacility("BANKID") & "' " & placeOnClick & "></td><td width=""150"">" & rsBankFacility("DESCRIPTION") & "</td>"
               Else
                  BankFacility_String = BankFacility_String &"<td width=""24""><input type=""checkbox"" name='chk_BankFacility" & (BankCount + 1) & "' id='chk_BankFacility" & (BankCount + 1) & "' value='" & rsBankFacility("BANKID") & "' " & placeOnClick & "></td><td width=""150"">" & rsBankFacility("DESCRIPTION") & "</td>"
               End If

               BankCount = BankCount + 1

            'loop till end of records
            rsBankFacility.MoveNext
            Loop

            BankFacility_String = BankFacility_String & "</tr></table>"
         Else

            'Write no records in there are no records
            Response.Write"Sorry, no records were found!"

         End If

   ' === STAMP CHANGES ===

    Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated,customer_id

    customer_id = Request("CustomerID")
	if (customer_id = "" OR NOT isNumeric(customer_id)) then
		Response.Redirect "CRM_CustomerNotFound.asp"
	end if

    SQL_LastUpdated = "EXECUTE C_CUSTOMER_AUDIT_GETLASTUPDATED @CustomerId = " & customer_id
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

    ' === STAMP CHANGES END ===

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer --> Tools --> Customer Set Up</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
var FormFields = new Array()
FormFields[0] = "txt_FIRSTNAME|First Name|TEXT|Y"
FormFields[1] = "txt_OCCUPATION|Occupation|TEXT|N"
FormFields[2] = "txt_LASTNAME|Last Name|TEXT|Y"
FormFields[3] = "txt_ADDRESS1|Address Line 1|TEXT|Y"
FormFields[4] = "txt_ADDRESS2|Address Line 2|TEXT|N"
FormFields[5] = "txt_ADDRESS3|Address Line 3|TEXT|N"
FormFields[6] = "txt_DOB|Date of Birth|DATE|Y"
FormFields[7] = "txt_TOWNCITY|Town / City|TEXT|Y"
FormFields[8] = "txt_COUNTY|County|TEXT|N"
FormFields[9] = "txt_POSTCODE|Postcode|POSTCODE|Y"
FormFields[10] = "txt_NINUMBER|National Insurance Number|TEXT|N"
FormFields[11] = "sel_EMPLOYMENTSTATUS|Employment Status|SELECT|N"
FormFields[12] = "sel_GENDER|Gender|SELECT|Y"
FormFields[13] = "sel_MARITALSTATUS|Marital Status|SELECT|N"
FormFields[14] = "sel_ETHNICORIGIN|Ethnicity|SELECT|N"
FormFields[15] = "sel_TITLE|Title|SELECT|Y"
FormFields[16] = "txt_HOUSENUMBER|House Number|TEXT|Y"
FormFields[17] = "txt_TAKEHOMEPAY|Take Home Pay|FLOAT|N"
FormFields[18] = "sel_RELIGION|Religion|SELECT|N"
FormFields[19] = "sel_SEXUALORIENTATION|Sexual Orientation|SELECT|N"
FormFields[20] = "hid_COMMUNICATION|Communication|TEXT|N"
FormFields[21] = "hid_DISABILITY|Disability|TEXT|N"

FormFields[22] = "txt_FIRSTLANGUAGE|First Language|TEXT|N"
FormFields[23] = "sel_PREFEREDCONTACT|Prefered Contact|TEXT|N"
FormFields[24] = "txt_MIDDLENAME|Middle Name|TEXT|N"
FormFields[25] = "txt_TITLEOTHER|Other Title|TEXT|N"

FormFields[26] = "txt_DISABILITYOTHER|Other Disability|TEXT|N"
FormFields[27] = "txt_COMMUNICATIONOTHER|Prefered Contact|TEXT|N"
FormFields[28] = "txt_SEXUALORIENTATIONOTHER|Other Sexual Orientation|TEXT|N"
FormFields[29] = "txt_RELIGIONOTHER|Other Religion|TEXT|N"
FormFields[30] = "txt_ETHNICORIGINOTHER|Other Religion|TEXT|N"
FormFields[31] = "txt_OCCUPANTSADULTS|Adult Occupants|INTEGER|Y"
FormFields[32] = "txt_OCCUPANTSCHILDREN|Children Occupants|INTEGER|Y"

FormFields[33] = "sel_LOCALAUTHORITY|Nominating LA|INTEGER|N"
FormFields[34] = "hid_INTERNETACCESS|InternetAccess|TEXT|N"
FormFields[35] = "sel_RENTSTATEMENTTYPE|Rent Statement Type|INTEGER|N"
FormFields[36] = "sel_WHEELCHAIR|Wheelchair|INTEGER|N"
FormFields[37] = "sel_CARER|Carer|INTEGER|N"
FormFields[38] = "sel_OWNTRANSPORT|Own Transport|INTEGER|N"
FormFields[39] = "sel_NATIONALITY|Nationality|INTEGER|N"
FormFields[40] = "txt_NATIONALITYOTHER|Other Nationality|TEXT|N"
FormFields[41] = "txt_INTERNETACCESSOTHER|Other Internet Access|TEXT|N"
FormFields[42] = "txt_SUPPORTAGENCIESOTHER|Support Agencies|TEXT|N"
FormFields[43] = "hid_SUPPORTAGENCIES|Support Agencies|TEXT|N"
FormFields[44] = "sel_HOUSEINCOME|Household Income|INTEGER|N"
FormFields[45] = "hid_BENEFIT|Benefit Entitlement|TEXT|N"
FormFields[46] = "txt_BANKFACILITYOTHER|Banking Facility|TEXT|N"
FormFields[47] = "hid_BANKFACILITY|Banking Facility|TEXT|N"
FormFields[48] = "txt_EMPADDRESS1|Address Line 1|TEXT|N"
FormFields[49] = "txt_EMPADDRESS2|Address Line 2|TEXT|N"
FormFields[50] = "txt_EMPLOYERNAME|Emp. Name|TEXT|N"
FormFields[51] = "txt_EMPTOWNCITY|Emp.|TEXT|N"
FormFields[52] = "txt_EMPPOSTCODE|Emp. PostCode|TEXT|N"
FormFields[53] = "sel_TEXTYESNO|Consent to Text|INTEGER|N"
FormFields[54] = "sel_EMAILYESNO|Consent to Email|INTEGER|N"
/*FormFields[55] = "txt_BenefitOTHER|Other Benefits|TEXT|N"
FormFields[56] = "hid_BenefitOTHER|Other Benefits|TEXT|N"*/

<% If NOT AMENDRECORD <> "" Then %>
FormFields[55] = "sel_CUSTOMERTYPE|Customer Type|INTEGER|Y"
<% End If %>

	function SaveForm(){
		buildCommunicationArray()
		buildDisabilityArray()
		buildInternetAccessArray()
		buildSupportAgenciesArray()
		buildBenefitArray()
		buildBankArray()
       
		if (document.RSLFORM.txt_NINUMBER.value != "") {
		    //Do some custom validation on NI number
            var re = new RegExp("^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFMI\s]{0,1}$");
            if (!(document.RSLFORM.txt_NINUMBER.value.match(re))) {
                alert("Please enter a valid NI number")
                return false
            }
        }
		if (!checkForm()) return false
            
			document.RSLFORM.target = ""
			document.RSLFORM.action = "ServerSide/Customer_svr.asp"
			document.RSLFORM.submit()
            
		}

	function title_change(){
		if (RSLFORM.sel_TITLE.value == 1)
				RSLFORM.sel_GENDER.selectedIndex = 2;
		else if (RSLFORM.sel_TITLE.value == 5 || RSLFORM.sel_TITLE.value == "")
			RSLFORM.sel_GENDER.selectedIndex = 0;
		else
			RSLFORM.sel_GENDER.selectedIndex = 1;
	}

	function buildCommunicationArray(){
		var ArraySize = <%=ComLinkCount%>
		var CommList = ""

		for (i = 1; i <= ArraySize; i++){
			//WhichBox = eval("RSLFORM.chk_Communication" + i)
			WhichBox = document.getElementById("chk_Communication"+String(i));
			if (WhichBox.checked){
				CommList = CommList + WhichBox.value + ","
				}
			}
		document.getElementById("hid_COMMUNICATION").value = CommList
	}

	function buildDisabilityArray(){
	
		var DisList = "";

        var inputs = document.getElementsByTagName('input');
        var len = inputs.length;
        var re = new RegExp('^chk_Disability.*'); 

        for (var i = 0; i < len; i++) {
  
            var input = inputs[i];

            if (input.type === 'checkbox') {

                if ((input.id.search(re) != -1) && (input.checked)) {
                    DisList = DisList + input.value + ",";
                }
            }
        }  

    	document.getElementById("hid_DISABILITY").value = DisList;
}

	function buildInternetAccessArray(){
		var ArraySize = <%=InternetLinkCount%>
		var InternetList = ""

		for (i = 1; i <= ArraySize; i++){
			//WhichBox = eval("RSLFORM.chk_InternetAccess" + i)
			WhichBox = document.getElementById("chk_InternetAccess"+String(i));
			if (WhichBox.checked){
				InternetList = InternetList + WhichBox.value + ","
			}
		}
		document.getElementById("hid_INTERNETACCESS").value = InternetList
	}

	function buildSupportAgenciesArray(){
		var ArraySize = <%=AgencyLinkCount%>
		var AgencyList = ""

		for (i = 1; i <= ArraySize; i++){
			//WhichBox = eval("RSLFORM.chk_SupportAgencies" + i)
			WhichBox = document.getElementById("chk_SupportAgencies"+String(i));
			if (WhichBox.checked){
				AgencyList = AgencyList + WhichBox.value + ","
			}
		}
		document.getElementById("hid_SUPPORTAGENCIES").value = AgencyList
	}

	function buildBenefitArray(){
		var ArraySize = <%=BenefitCount%>
		var BenefitList = ""

		for (i = 1; i <= ArraySize; i++){
			//WhichBox = eval("RSLFORM.chk_Benefit" + i)
			WhichBox = document.getElementById("chk_Benefit"+String(i));
			if (WhichBox.checked){
				BenefitList = BenefitList + WhichBox.value + ","
			}
		}
		document.getElementById("hid_BENEFIT").value = BenefitList
	}

	function buildBankArray(){
	var ArraySize = <%=BankCount%>
	var BankList = ""

	for (i = 1; i <= ArraySize; i++){
		//WhichBox = eval("RSLFORM.chk_BankFacility" + i)
		WhichBox = document.getElementById("chk_BankFacility"+String(i));
		if (WhichBox.checked){
			BankList = BankList + WhichBox.value + ","
			}
		}
		document.getElementById("hid_BANKFACILITY").value = BankList
	}


function ActivateCheckBoxes(){

	var CommunicationArray 		= document.getElementById("hid_COMMUNICATION").value
	var DisabilityArray 		= document.getElementById("hid_DISABILITY").value
	var InternetAccessArray 	= document.getElementById("hid_INTERNETACCESS").value
	var SupportAgenciesArray 	= document.getElementById("hid_SUPPORTAGENCIES").value
	var BenefitArray 	 	 	= document.getElementById("hid_BENEFIT").value
	var BankArray				= document.getElementById("hid_BANKFACILITY").value
		CommunicationArray		= CommunicationArray.split(",")
		DisabilityArray			= DisabilityArray.split(",")
		InternetAccessArray		= InternetAccessArray.split(",")
		SupportAgenciesArray	= SupportAgenciesArray.split(",")
		BenefitArray			= BenefitArray.split(",")
		BankArray				= BankArray.split(",")


	for (i = 0; i < CommunicationArray[i]; i++){
		if (!(CommunicationArray[i] == "")){
			WhichBox = "chk_Communication" + CommunicationArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

	for (i = 0; i <= DisabilityArray[i]; i++){
		if (!(DisabilityArray[i] == "")){
			WhichBox = "chk_Disability" + DisabilityArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

	for (i = 0; i <= InternetAccessArray[i]; i++){
		if (!(InternetAccessArray[i] == "")){
			WhichBox = "chk_InternetAccess" + InternetAccessArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

	for (i = 0; i <= SupportAgenciesArray[i]; i++){
		if (!(SupportAgenciesArray[i] == "")){
			WhichBox = "chk_SupportAgencies" + SupportAgenciesArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

	for (i = 0; i <= BenefitArray[i]; i++){
		if (!(BenefitArray[i] == "")){
			WhichBox = "chk_Benefit" + BenefitArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

	for (i = 0; i <= BankArray[i]; i++){
		if (!(BankArray[i] == "")){
			WhichBox = "chk_BankFacility" + BankArray[i] + ""
			document.getElementById("" + WhichBox + "").checked = true;
		}
	}

}

function Disable_Other_Boxes(){

	var OptionSelectsArray = new Array("ETHNICORIGIN","RELIGION","SEXUALORIENTATION","NATIONALITY")

	for (i = 0; i < OptionSelectsArray.length; i++){
		str = "sel_" + OptionSelectsArray[i]
		none_option = document.getElementById("" + str + "").options[document.getElementById("" + str + "").selectedIndex].text
		sub_str = "txt_" + OptionSelectsArray[i] + "OTHER"

		if (none_option.indexOf('Other')> -1){
			document.getElementById("" + sub_str + "").disabled = false
			document.getElementById("" + sub_str + "").style.backgroundColor = "white"
			}
		else {
			document.getElementById("" + sub_str + "").disabled = true
			document.getElementById("" + sub_str + "").value = ""
			document.getElementById("" + sub_str + "").style.backgroundColor = "#AEB3BD"
			}
		}

	if (document.getElementById("chk_Communication<%=ComOtherID%>").checked){
		document.getElementById("txt_COMMUNICATIONOTHER").disabled = false
		document.getElementById("txt_COMMUNICATIONOTHER").style.backgroundColor = "white"
		}
	else{
		document.getElementById("txt_COMMUNICATIONOTHER").disabled = true
		document.getElementById("txt_COMMUNICATIONOTHER").value = ""
		document.getElementById("txt_COMMUNICATIONOTHER").style.backgroundColor = "#AEB3BD"
		}

	if (document.getElementById("chk_InternetAccess<%=InternetOtherID%>").checked){
		document.getElementById("txt_INTERNETACCESSOTHER").disabled = false
		document.getElementById("txt_INTERNETACCESSOTHER").style.backgroundColor = "white"
		}
	else{
		document.getElementById("txt_INTERNETACCESSOTHER").disabled = true
		document.getElementById("txt_INTERNETACCESSOTHER").value = ""
		document.getElementById("txt_INTERNETACCESSOTHER").style.backgroundColor = "#AEB3BD"
		}
/*
   if (document.getElementById("chk_Benefit<%=BenefitOtherID%>").checked){
		document.getElementById("txt_BenefitOTHER").disabled = false
		document.getElementById("txt_BenefitOTHER").style.backgroundColor = "white"
		}
	else{
		document.getElementById("txt_BenefitOTHER").disabled = true
		document.getElementById("txt_BenefitOTHER").value = ""
		document.getElementById("txt_BenefitOTHER").style.backgroundColor = "#AEB3BD"
		}
*/
	if (document.getElementById("chk_SupportAgencies<%=AgencyOtherID%>").checked){
		document.getElementById("txt_SUPPORTAGENCIESOTHER").disabled = false
		document.getElementById("txt_SUPPORTAGENCIESOTHER").style.backgroundColor = "white"
		}
	else{
		document.getElementById("txt_SUPPORTAGENCIESOTHER").disabled = true
		document.getElementById("txt_SUPPORTAGENCIESOTHER").value = ""
		document.getElementById("txt_SUPPORTAGENCIESOTHER").style.backgroundColor = "#AEB3BD"
		}

	if (document.getElementById("chk_BankFacility<%=BankOtherID%>").checked){
		document.getElementById("txt_BANKFACILITYOTHER").disabled = false
		document.getElementById("txt_BANKFACILITYOTHER").style.backgroundColor = "white"
		}
	else{
		document.getElementById("txt_BANKFACILITYOTHER").disabled = true
		document.getElementById("txt_BANKFACILITYOTHER").value = ""
		document.getElementById("txt_BANKFACILITYOTHER").style.backgroundColor = "#AEB3BD"
		}


}



    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages();ActivateCheckBoxes();Disable_Other_Boxes();"
    onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0" style="height: 26px">
        <tr>
            <td rowspan="2">
                <img src="Images/tab_create_new.gif" width="164" height="20" alt="" />
            </td>
            <td>
                <img src="images/spacer.gif" width="584" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="584" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <form name="RSLFORM" method="post" action="ServerSide/CustomerCheck.asp">
    <table cellspacing="0" cellpadding="2" style="border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td nowrap="nowrap" width="95">
                Title :
            </td>
            <td width="210">
                <%=lstTitles%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_TITLE" id="img_TITLE" alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                House Number :
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_HOUSENUMBER" id="txt_HOUSENUMBER"
                    maxlength="20" tabindex="2" value="<%=HouseNumber%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_HOUSENUMBER" id="img_HOUSENUMBER"
                    alt="" />
            </td>
            <td width="54">
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" bgcolor="#F4F9F2">
                If other, state :
            </td>
            <td bgcolor="#F4F9F2">
                <input type="text" class="textbox200" name="txt_TITLEOTHER" id="txt_TITLEOTHER" maxlength="50"
                    tabindex="1" value="<%=TitleOther%>" />
            </td>
            <td bgcolor="#F4F9F2">
                <img src="/js/FVS.gif" width="15" height="15" name="img_TITLEOTHER" id="img_TITLEOTHER"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Address Line 1 :
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1"
                    maxlength="100" tabindex="2" value="<%=Address1%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_ADDRESS1" id="img_ADDRESS1"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                First Name(s):
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="50"
                    tabindex="1" value="<%=Firstname%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME" id="img_FIRSTNAME"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Address Line 2 :
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2"
                    maxlength="100" tabindex="2" value="<%=Address2%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_ADDRESS2" id="img_ADDRESS2"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Middle Name(s):
            </td>
            <td>
                <input name="txt_MIDDLENAME" type="text" class="textbox200" id="txt_MIDDLENAME" tabindex="1"
                    value="<%=MIddleName%>" maxlength="50" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_MIDDLENAME" id="img_MIDDLENAME"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Address Line 3 :
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_ADDRESS3" id="txt_ADDRESS3"
                    maxlength="100" tabindex="2" value="<%=Address3%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_ADDRESS3" id="img_ADDRESS3"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Last Name :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="50"
                    tabindex="1" value="<%=Lastname%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_LASTNAME" id="img_LASTNAME"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Town / City:
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_TOWNCITY" id="txt_TOWNCITY"
                    maxlength="100" tabindex="2" value="<%=TownCity%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_TOWNCITY" id="img_TOWNCITY"
                    alt="" />
            </td>
        </tr>
        <tr>
            <% If NOT AMENDRECORD <> "" Then %>
            <td nowrap="nowrap" width="95">
                Customer Type:
            </td>
            <td width="210">
                <%=lstCustomerType%>
            </td>
            <% Else %>
            <td nowrap="nowrap" width="95">
                &nbsp;
            </td>
            <td width="210">
            </td>
            <% End If %>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_CUSTOMERTYPE" id="img_CUSTOMERTYPE"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                County :
            </td>
            <td width="210">
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY"
                    maxlength="100" tabindex="2" value="<%=COUNTY%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_COUNTY" id="img_COUNTY" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Gender :
            </td>
            <td width="210">
                <select class="textbox200" name="sel_GENDER" id="sel_GENDER" tabindex="1">
                    <option value="">Please Select</option>
                    <%
		if AMENDRECORD <> "" then
			if (gender = "Female") then
				FemaleSelected = " selected"
			elseif (gender = "Male") then
				MaleSelected = " selected"
			elseif (gender = "Prefer not to say") then
				NotSaySelected = " selected"
		    elseif (gender = "Transgender") then
		        TransgenderSelected = " selected"
			end if
		end if
                    %>
                    <option value="Female" <%=FemaleSelected%>>Female</option>
                    <option value="Male" <%=MaleSelected%>>Male</option>
                    <option value="Prefer not to say" <%=NotSaySelected%>>Prefer not to say</option>
                    <option value="Transgender" <%=TransgenderSelected%>>Transgender</option>
                </select>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_GENDER" id="img_GENDER" alt="" />
            </td>
            <td nowrap="nowrap">
                Postcode :
            </td>
            <td>
                <input <%=addReadonly%> type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE"
                    maxlength="10" tabindex="2" value="<%=PostCode%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_POSTCODE" id="img_POSTCODE"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Date of Birth :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_DOB" id="txt_DOB" maxlength="10"
                    tabindex="1" value="<%=Dob%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_DOB" id="img_DOB" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Ni Number :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_NINUMBER" id="txt_NINUMBER" maxlength="20"
                    tabindex="1" value="<%=NiNumber%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_NINUMBER" id="img_NINUMBER"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Nominating LA
            </td>
            <td width="210">
                <%=lstLOCALAUTHORITY%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_LOCALAUTHORITY" id="img_LOCALAUTHORITY"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td width="95">
                &nbsp;
            </td>
            <td width="210">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                Economic Status :
            </td>
            <td width="210">
                <%=lstEmploymentStatus%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPLOYMENTSTATUS" id="img_EMPLOYMENTSTATUS"
                    alt="" />
            </td>
            <td nowrap="nowrap" bgcolor="">
                Employer Name:
            </td>
            <td bgcolor="#F4F9F2">
                <input type="text" class="textbox200" name="txt_EMPLOYERNAME" id="txt_EMPLOYERNAME"
                    maxlength="50" tabindex="1" value="<%=EmployerName%>" />
            </td>
            <td bgcolor="#F4F9F2">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPLOYERNAME" id="img_EMPLOYERNAME"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                Emp. Add. Line 1 :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_EMPADDRESS1" id="txt_EMPADDRESS1"
                    maxlength="100" tabindex="2" value="<%=EmpAddress1%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPADDRESS1" id="img_EMPADDRESS1"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Emp. Add. Line 2 :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_EMPADDRESS2" id="txt_EMPADDRESS2"
                    maxlength="100" tabindex="2" value="<%=EmpAddress2%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPADDRESS2" id="img_EMPADDRESS2"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                Emp. Town/City :
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_EMPTOWNCITY" id="txt_EMPTOWNCITY"
                    maxlength="100" tabindex="2" value="<%=EmpTownCity%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPTOWNCITY" id="img_EMPTOWNCITY"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Emp. Postcode:
            </td>
            <td width="210">
                <input type="text" class="textbox200" name="txt_EMPPOSTCODE" id="txt_EMPPOSTCODE"
                    maxlength="100" tabindex="2" value="<%=EmpPostCode%>" />
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMPPOSTCODE" id="img_EMPPOSTCODE"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Occupation :
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_OCCUPATION" id="txt_OCCUPATION" maxlength="100"
                    tabindex="2" value="<%=occupation%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_OCCUPATION" id="img_OCCUPATION"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Take home Pay:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TAKEHOMEPAY" id="txt_TAKEHOMEPAY"
                    maxlength="10" tabindex="2" value="<%=TakeHomePay%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_TAKEHOMEPAY" id="img_TAKEHOMEPAY"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <strong>No. Of Occupants </strong>:
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Adults (over 18) :
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_OCCUPANTSADULTS" id="txt_OCCUPANTSADULTS"
                    maxlength="100" tabindex="2" value="<%=OCCUPANTSADULTS%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_OCCUPANTSADULTS" id="img_OCCUPANTSADULTS"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Children (under 18) :
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_OCCUPANTSCHILDREN" id="txt_OCCUPANTSCHILDREN"
                    maxlength="100" tabindex="2" value="<%=OCCUPANTSCHILDREN%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_OCCUPANTSCHILDREN" id="img_OCCUPANTSCHILDREN"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Marital Status :
            </td>
            <td>
                <%=lstMaritalStatus%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_MARITALSTATUS" id="img_MARITALSTATUS"
                    alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Ethnic Origin :
            </td>
            <td width="210">
                <%=lstEthnicities%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_ETHNICORIGIN" id="img_ETHNICORIGIN"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input name="txt_ETHNICORIGINOTHER" type="text" class="textbox200" id="txt_ETHNICORIGINOTHER"
                    tabindex="2" maxlength="50" value="<%=ethnicityother%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_ETHNICORIGINOTHER" id="img_ETHNICORIGINOTHER"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="95">
                Nationality :
            </td>
            <td width="210">
                <%=lstNationalities%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_NATIONALITY" id="img_NATIONALITY"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input type="text" name="txt_NATIONALITYOTHER" id="txt_NATIONALITYOTHER" class="textbox200"
                    tabindex="2" maxlength="50" value="<%=nationalityother%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_NATIONALITYOTHER" id="img_NATIONALITYOTHER"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Religion :
            </td>
            <td>
                <%=lstReligion%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_RELIGION" id="img_RELIGION"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input type="text" name="txt_RELIGIONOTHER" id="txt_RELIGIONOTHER" class="textbox200"
                    tabindex="2" maxlength="50" value="<%=Religion_other%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_RELIGIONOTHER" id="img_RELIGIONOTHER"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Sexual Orientation
            </td>
            <td>
                <%=lstSexualOrientation%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_SEXUALORIENTATION" id="img_SEXUALORIENTATION"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input name="txt_SEXUALORIENTATION_OTHER" type="text" class="textbox200" id="txt_SEXUALORIENTATIONOTHER"
                    tabindex="2" maxlength="50" value="<%=SexualOrientation_Other%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SEXUALORIENTATIONOTHER" width="15" height="15" id="img_SEXUALORIENTATIONOTHER"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                First Language :
            </td>
            <td>
                <input name="txt_FIRSTLANGUAGE" type="text" class="textbox200" id="txt_FIRSTLANGUAGE"
                    tabindex="2" maxlength="50" value="<%=FirstLanguage%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FIRSTLANGUAGE" width="15" height="15" id="img_FIRSTLANGUAGE"
                    alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top">
                <br />
                Communication :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=Communication_String%>
                <br />
                <input type="hidden" name="hid_COMMUNICATION" id="hid_COMMUNICATION" value="<%=Communication%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_COMMUNICATION" id="img_COMMUNICATION"
                    alt="" />
                &gt; Other :
                <input type="text" name="txt_COMMUNICATIONOTHER" id="txt_COMMUNICATIONOTHER" class="textbox200"
                    tabindex="2" maxlength="50" value="<%=Communication_other%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_COMMUNICATIONOTHER" id="img_COMMUNICATIONOTHER"
                    alt="" /><br />
                <br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                Consent to Text:
            </td>
            <td width="210">
                <%=lstText%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_TEXTYESNO" id="img_TEXTYESNO"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Consent to Email:
            </td>
            <td width="210">
                <%=lstEmail%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_EMAILYESNO" id="img_EMAILYESNO"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Preferred Contact:
            </td>
            <td>
                <%=lstPreferedContact%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_PREFEREDCONTACT" id="img_PREFEREDCONTACT"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Own Transport :
            </td>
            <td width="210">
                <%=lstTransport%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_OWNTRANSPORT" id="img_OWNTRANSPORT"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Rent Statment:
            </td>
            <td>
                <%=lstRentStatementtype%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_RENTSTATEMENTTYPE" id="img_RENTSTATEMENTTYPE"
                    alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <br />
                Disability :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=Disability_String%>
                <input type="hidden" name="hid_DISABILITY" id="hid_DISABILITY" value="<%=Disability%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_DISABILITY" id="img_DISABILITY"
                    alt="" />
                <input name="txt_DISABILITYOTHER" type="hidden" class="textbox200" id="txt_DISABILITYOTHER"
                    tabindex="2" maxlength="50" value="" />
                <img src="/js/FVS.gif" name="img_DISABILITYOTHER" width="15" height="15" id="img_DISABILITYOTHER"
                    alt="" />
                <br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                WheelChair/Mobility Scooter:
            </td>
            <td width="210">
                <%=lstWheelChair%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_WHEELCHAIR" id="img_WHEELCHAIR"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="124">
                Live in Carer:
            </td>
            <td width="210">
                <%=lstCarer%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_CARER" id="img_CARER" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <br />
                Support from Agencies :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=SupportAgencies_String%>
                <input type="hidden" name="hid_SUPPORTAGENCIES" id="hid_SUPPORTAGENCIES" value="<%=SupportAgencies%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_SUPPORTAGENCIES" id="img_SUPPORTAGENCIES"
                    alt="" />
                &gt; Other :
                <input type="text" class="textbox200" name="txt_SUPPORTAGENCIESOTHER" id="txt_SUPPORTAGENCIESOTHER"
                    tabindex="2" maxlength="50" value="<%=SupportAgencies_other%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_SUPPORTAGENCIESOTHER" id="img_SUPPORTAGENCIESOTHER"
                    alt="" /><br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <br />
                Access to Internet :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=InternetAccess_String%>
                <input type="hidden" name="hid_INTERNETACCESS" id="hid_INTERNETACCESS" value="<%=InternetAccess%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_INTERNETACCESS" id="img_INTERNETACCESS"
                    alt="" />
                &gt; Other :
                <input type="text" class="textbox200" name="txt_INTERNETACCESSOTHER" id="txt_INTERNETACCESSOTHER"
                    tabindex="2" maxlength="50" value="<%=InternetAccess_other%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_INTERNETACCESSOTHER" id="img_INTERNETACCESSOTHER"
                    alt="" /><br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <br />
                Benefit Entitlement :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=Benefit_String%>
                <input type="hidden" name="hid_BENEFIT" id="hid_BENEFIT" value="<%=Benefit%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_BENEFIT" id="img_BENEFIT"
                    alt="" />
                &gt; Other :
                <input type="text" class="textbox200" name="txt_BenefitOTHER" id="txt_BenefitOTHER"
                    tabindex="2" maxlength="50" value="<%=Benefit_other%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_BenefitOTHER" id="img_BenefitOTHER"
                    alt="" /><br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <br />
                Banking Facilities :
            </td>
            <td colspan="4" bgcolor="#F4F9F2">
                <br />
                <%=BankFacility_String%>
                <input type="hidden" name="hid_BANKFACILITY" id="hid_BANKFACILITY" value="<%=BankFacility%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_BANKFACILITY" id="img_BANKFACILITY"
                    alt="" />
                &gt; Other :
                <input type="text" class="textbox200" name="txt_BANKFACILITYOTHER" id="txt_BANKFACILITYOTHER"
                    tabindex="2" maxlength="50" value="<%=BankFacility_other%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_BANKFACILITYOTHER" id="img_BANKFACILITYOTHER"
                    alt="" /><br />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" width="124">
                Household Income :
            </td>
            <td width="210">
                <%=lstHouseIncome%>
            </td>
            <td width="15">
                <img src="/js/FVS.gif" width="15" height="15" name="img_HOUSEINCOME" id="img_HOUSEINCOME"
                    alt="" />
            </td>
            <td>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="95">
                &nbsp;
            </td>
            <td width="210">
                &nbsp;
            </td>
            <td width="15">
                &nbsp;
            </td>
            <td width="124">
                &nbsp;
            </td>
            <td width="210">
                &nbsp;
            </td>
            <td width="15">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Consent
            </td>
            <td>
                <input type="checkbox" name="chk_CONSENT" id="chk_CONSENT" value="1" <%=consent_box_status%> />
                <img src="/js/FVS.gif" name="img_CONSENT" id="img_CONSENT" width="15" height="15"
                    alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="95">
                &nbsp;
            </td>
            <td width="210">
                &nbsp;
            </td>
            <td width="15">
                &nbsp;
            </td>
            <td width="124">
                &nbsp;
            </td>
            <td width="210">
                &nbsp;
            </td>
            <td width="15">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right">
                <% If AMENDRECORD <> "" Then %>
                <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=Request("CustomerID")%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="AMEND" />
                <input type="button" class="RSLButton" value=" Amend Customer Details " title="Amend Customer Details"
                    onclick="SaveForm()" style="cursor: pointer" />
                <% Else %>
                <input type="hidden" name="hid_Action" id="hid_Action" value="NEW" />
                <input type="button" class="RSLButton" name="BtnConfirm" id="BtnConfirm" value=" Confirm "
                    title="Confirm" onclick="SaveForm()" style="cursor: pointer" />
                <% End If %>
                <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />
                <input type="hidden" name="hid_LASTACTIONTYPE" id="hid_LASTACTIONTYPE" value="1" />
            </td>
        </tr>
    </table>
    </form>
    <% If AMENDRECORD <> "" Then %>
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
    <% End If %>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
