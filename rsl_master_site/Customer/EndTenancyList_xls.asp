<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 
ByPassSecurityAccess = true
Response.ContentType = "application/vnd.ms-excel"
%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
    Dim CompareDate
	Dim CompareDate2

        CompareDate = DateAdd("m",-1,Date)
	    CompareDate2 = Date

	    If (Request("COMPAREDATE") <> "") Then
		    CompareDate = Request("COMPAREDATE")
		    CompareDate2 = Request("COMPAREDATE2")
	    End If

    Dim orderBy
		orderBy = "T.TENANCYID DESC"

		Call OpenDB()

					strSQL = "SELECT T.TENANCYID, dbo.FN_Get_CustomerNamesByTeanacyId(T.TENANCYID) As Customer, LTRIM(reverse(stuff(reverse(COALESCE(P.HOUSENUMBER+' ','') + COALESCE(P.ADDRESS1+',','') + COALESCE(P.ADDRESS2+',','') + COALESCE(P.TOWNCITY+',','')), 1, 1, ''))) AS [Address], LA.DESCRIPTION AS LOCALAUTHORITY, T.ENDDATE, CTR.DESCRIPTION As Reason, TRA.DESCRIPTION AS ReasonCategory " &_
        "FROM C_TENANCY T " &_
                 " INNER JOIN (SELECT MIN(CUSTOMERID) AS CUSTOMERID, TENANCYID FROM dbo.C_CUSTOMERTENANCY GROUP BY TENANCYID) CUT ON T.TENANCYID = CUT.TENANCYID " &_
				 " INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
                 " LEFT JOIN PDR_DEVELOPMENT PD ON P.DEVELOPMENTID = PD.DEVELOPMENTID " &_
                 " LEFT JOIN G_LOCALAUTHORITY LA ON PD.LOCALAUTHORITY = LA.LOCALAUTHORITYID " &_
				 " INNER JOIN (SELECT MAX(JOURNALID) AS JOURNALID, TENANCYID FROM C_JOURNAL WHERE ITEMNATUREID = 27 GROUP BY TENANCYID) J ON J.TENANCYID = T.TENANCYID " &_
				 " INNER JOIN C_TERMINATION CT ON CT.JOURNALID = J.JOURNALID AND CT.ITEMSTATUSID = 14 " &_
				 " INNER JOIN C_TERMINATION_REASON CTR ON CTR.REASONID = CT.REASON " &_
                 " LEFT JOIN C_TERMINATION_REASON_ACTIVITY TRA ON CT.REASONCATEGORYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
				 "WHERE T.ENDDATE >= '" & FormatDateTime(CompareDate,1) & "' AND T.ENDDATE <= '" & FormatDateTime(CompareDate2,1) & "' AND T.ENDDATE IS NOT NULL " &_
				 "ORDER BY " & orderBy

	    Call OpenRs(rsSet, strSQL)
        str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			while not rsSet.eof
        str_data = str_data & "<tbody><tr>" &_
					"<td width=""100px"">" & rsSet("TENANCYID") & "</td>" &_
					"<td width=""106px"" align=""left"">" & rsSet("Address") & "</td>" &_
                    "<td align=""left"">" & rsSet("LOCALAUTHORITY") & "</td>" &_
                    "<td align=""left"">" & rsSet("ENDDATE") & "</td>" &_
					"<td align=""left"">" & rsSet("Reason") & "</td>" &_
					"<td width=""111px"" align=""left"">" & rsSet("ReasonCategory") & "</td>"
            str_data = str_data & "</tr></tbody>"
				rsSet.movenext()
				wend

			str_data = str_data & "</tbody>"

        Call CloseRs(rsSet)
		Call CloseDB()
        %>
        <table cellpadding="1" cellspacing="2" border="1">
        <thead>
            <tr>
                <td>
                    <b>Tenancy</b>
                </td>
                <td>
                    <b>Address</b>
                </td>
                <td>
                    <b>Local Authority</b>
                </td>
                <td>
                    <b>End Date</b>
                </td>
                <td>
                    <b>Reason</b>
                </td>
                <td>
                    <b>Category</b>
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>