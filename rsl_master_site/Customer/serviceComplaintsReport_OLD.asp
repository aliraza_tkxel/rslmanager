<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	
	CONST_PAGESIZE = 20
	OpenDB()
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim TotalSum, TotalCount, clist, csql, ccnt, ctotal, view
	Dim GENERAL_REQUEST_STRING
	
	GENERAL_REQUEST_STRING = "TENANT="&REQUEST("TENANT")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&ASSIGNEDTO="&REQUEST("ASSIGNEDTO")&"&"
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If

'	SET ORDER BY
	Dim ORDERBY
	ORDERBY = " J.CREATIONDATE "
	if (Request("CC_Sort") <> "") then ORDERBY = Request("CC_Sort") End If
	If Request("TENANT") <> "" Then SRCH_TENANT = Request("TENANT") Else SRCH_TENANT = "" End If
	If Request("ASSIGNEDTO") <> "" Then SRCH_ASSIGNEDTO = Request("ASSIGNEDTO") Else SRCH_ASSIGNEDTO = "" End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If

	'Call SetSort()
	Call getData()
	Call CloseDB()
	
	
	Function getData()
		
		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = "" 
		SQLCODE = " EXEC C_SERVICECOMPLAINTS_REPORT 1, '" & ORDERBY & "', '" & SRCH_TENANT & "', '" & SRCH_ASSIGNEDTO & "',''"
		'rw sqlcode

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = SQLCODE
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				
					str_data = str_data &_
						"<TR ONCLICK='toggle("&rsSet("JOURNALID")&")' STYLE='CURSOR:HAND;COLOR:"&rsSet("STATUS")&"'>" &_
						"<TD TITLE='Click to view notes' WIDTH=75PX nowrap>" & rsSet("CDATE") & "</TD>" &_
						"<TD TITLE='Click to go to customer record' WIDTH=195PX nowrap ONCLICK='location.href=""CRM.asp?CustomerID="& rsSet("CUSTOMERID") & """'><b>" & rsSet("CNAME") &	"</b></TD>" &_
						"<TD TITLE='Click to view notes' WIDTH=195PX nowrap>" & rsSet("ESCALATIONID") & ". " & rsSet("ESCLEVEL") & "</TD>" & _
						"<TD TITLE='Click to view notes' WIDTH=195PX nowrap>" & rsSet("ENAME") & "</TD>" &_
						"<TD TITLE='Click to view notes' WIDTH=90PX nowrap>" & rsSet("RESPONSEDATE") & "</TD></TR>"  &_
						"<THEAD><TR ID='"&rsSet("JOURNALID")&"' STYLE='DISPLAY:NONE'><TD  COLSPAN=5>" & rsSet("NOTES") & "</TD></TR></THEAD>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
					
			Next
			
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			Call fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=5 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A style='cursor:hand' onclick=""gotopage(1)""><b><font color=BLUE>First</font></b></a> " &_
			"<A style='cursor:hand' onclick=""gotopage(" & prevpage & ")""><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A style='cursor:hand' onclick=""gotopage(" & nextpage & ")""><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A style='cursor:hand' onclick=""gotopage(" & intPageCount & ")""><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"

		End If
	
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=5 ALIGN=CENTER>No service complaints found in the system for the specified criteria!!!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
			Call fill_gaps()
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	' pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=5 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer --> Service Complaints List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	
	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}
	
	function gotopage(pageno){
		
		//alert("serviceComplaintsReport.asp?page="+pageno+"&TENANT="+document.getElementById("txt_TENANT").value+"&ASSIGNEDTO="+document.getElementById("txt_ASSIGNEDTO").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value)
		location.href = "serviceComplaintsReport.asp?page="+pageno+"&TENANT="+document.getElementById("txt_TENANT").value+"&ASSIGNEDTO="+document.getElementById("txt_ASSIGNEDTO").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value;
	}	

	// OPEN OR CLOSE CREDIT NOTE ROWS
	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}
	
	var FormFields = new Array();
	FormFields[0] = "txt_TENANT|Tenant|TEXT|N"
	FormFields[1] = "txt_ASSIGNEDTO|Assigned|TEXT|N"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	// fires when proceed button is clicked
	function click_go(){
	
		if (!checkForm()) return false;	
		location.href = "serviceComplaintsReport.asp?TENANT="+document.getElementById("txt_TENANT").value+"&page=1&ASSIGNEDTO="+document.getElementById("txt_ASSIGNEDTO").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value;
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post><BR>
<TABLE WIDTH=750 CELLPADDING=2 CELLSPACING=0 STYLE='border:1px solid black;BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor=STEELBLUE BORDER=1 BORDERcolor=silver>
	<THEAD>
		<TR>
			<TD ALIGN=RIGHT COLSPAN=3>
			<span title='Enter any keyword/reference code to search for'><B>&nbsp;Tenant&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TENANT" maxlength=12 size=20 tabindex=1 value='<%=Request("TENANT")%>'>
			<image src="/js/FVS.gif" name="img_TENANT" width="15px" height="15px" border="0"></B></span>
			&nbsp;&nbsp;<b>Assigned To</b>&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_ASSIGNEDTO" maxlength=12 size=20 tabindex=2 value='<%=Request("ASSIGNEDTO")%>'>
			<image src="/js/FVS.gif" name="img_ASSIGNEDTO" width="15px" height="15px" border="0">
			</TD><TD COLSPAN=2 ALIGN=RIGHT>Pagesize&nbsp;
			<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" tabindex=4 id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()">&nbsp;&nbsp;
			</TD>
		</TR>			
	<TR><TD CLASS='TABLE_HEAD' WIDTH="85px" nowrap><a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Date&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=J.CREATIONDATE DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="195px"><a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=C.LASTNAME" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Tenant&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=C.LASTNAME DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="195px"><a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ESCALATIONID" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Escalation&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ESCALATIONID DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="195px"><a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ENAME" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Assigned To&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=ENAME DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="90px"><a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Deadline&nbsp;<a href="serviceComplaintsReport.asp?<%=GENERAL_REQUEST_STRING%>CC_Sort=RESPONSEDATE_SORT DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5&nbsp; ALIGN='CENTER' STYLE='BORDER-bottom:2PX SOLID #133E71' CLASS='TABLE_HEAD'></TD></TR>
	</THEAD>
	<%=str_data%>
</TABLE>
	<input type="hidden" name="hid_CreditSum">
	<input type="hidden" name="hid_CreditCount">
	<input type="hidden" name="hid_CreditList">
	</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>