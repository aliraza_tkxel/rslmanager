<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
TenancyID = Request("TenancyID")
if (TenancyID = "") then
	TenancyID = -1
end if

CustomerID = Request("CustomerID")
if (CustomerID = "") then
	CustomerID = -1
end if

OpenDB()

onloadfunction = ""
savebuttontext = "Save Schedule"
action = "NEW"
HASVALIDATED = ""
HBID = Request("HBID")
if (HBID <> "") then
	action = "UPDATE"
	onloadfunction = "HBSchedule()"
	
	savebuttontext = "Amend Schedule"	
	
	SQL = "SELECT TOP 6 * FROM F_HBACTUALSCHEDULE WHERE VALIDATED = 1 AND HBID = " & HBID & " ORDER BY STARTDATE DESC"

	Call OpenRs(rsPreviousSchedule, SQL)
	StringTable = ""
	'if not the end of the recordset this means we have validated rows, and that the initial payment must have been validated,
	'therefore we have to display the previous few hb validated rows and also get the last estimated value which has not been validated
	if (not rsPreviousSchedule.EOF) then
		HASVALIDATED = "YES"
		'get the last estimated value for the hb schedule
		SQL = "SELECT TOP 1 * FROM F_HBACTUALSCHEDULE WHERE VALIDATED IS NULL AND HBID = " & HBID & " ORDER BY STARTDATE ASC"

		Call OpenRs(rsLastEstimate, SQL)
		if (NOT rsLastEstimate.EOF) then
			Oinitialpayment = FormatNumber(rsLastEstimate("HB"),2,-1,0,0)
			Oinitialstartdate = rsLastEstimate("STARTDATE")
			Oinitialenddate = rsLastEstimate("ENDDATE")
		end if
		CloseRs(rsLastEstimate)
		LASTHBCounter = 0
		while not rsPreviousSchedule.EOF 
			iStart = rsPreviousSchedule("STARTDATE")
			iEnd = rsPreviousSchedule("ENDDATE")
			iDay = DateDiff("d", CDate(iStart), CDate(iEnd)) + 1
			StringTable = "<TR bgcolor=beige><TD width=100 nowrap>" & FormatDateTime(iStart,2) & "</TD><TD width=100 nowrap>" & FormatDateTime(iEnd,2) & "</TD><TD width=80 nowrap align=right>" & iDay & "</TD><TD align=right width=120 nowrap>" & FormatNumber(rsPreviousSchedule("HB"),2,-1,0,0) & "</TD></TR>" & StringTable
			rsPreviousSchedule.moveNext
			LASTHBCounter = LASTHBCounter + 1
		wend
		NextTable = "<tr><td width=100 nowrap align=right valign=center><IMG SRC=""/js/FVS.gif"" width=15 height=15 name=""img_STARTDATE"">&nbsp;<INPUT type=""text"" size=13 maxlength=10 name=""txt_STARTDATE"" class=""textbox"" value=""" & Oinitialstartdate & """ onblur=""AddDays();HBSchedule()""></TD>" &_
					  "<TD width=100 nowrap  align=right valign=center><IMG SRC=""/js/FVS.gif"" width=15 height=15 name=""img_ENDDATE"">&nbsp;<INPUT type=""text"" size=13 maxlength=10  name=""txt_ENDDATE"" class=""textbox"" value=""" & Oinitialenddate & """ onblur=""HBSchedule()""></td>" &_
					  "<td width=80 nowrap ></TD><TD width=120 nowrap  align=right valign=center><IMG SRC=""/js/FVS.gif"" width=15 height=15 name=""img_INITIAL"">&nbsp;<INPUT size=15 type=""text"" name=""txt_INITIAL"" class=""textbox"" value=""" & Oinitialpayment & """ onblur=""HBSchedule()""></TD>" &_
					  "</tr>"
	end if
	CloseRs(rsPreviousSchedule)
	
	SQL = "SELECT * FROM F_HBINFORMATION WHERE HBID = " & HBID
	Call OpenRs(rsHBINFO, SQL)
	if (NOT rsHBINFO.EOF) then
		hbref = rsHBINFO("HBREF")
		initialpayment = FormatNumber(rsHBINFO("INITIALPAYMENT"),2,-1,0,0)
		initialstartdate = rsHBINFO("INITIALSTARTDATE")
		initialenddate = rsHBINFO("INITIALENDDATE")
	end if		
	CloseRs(rsHBINFO)
	
end if

CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer --&gt; Housing Benefit</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<style = type="text/css">
<!--
input {
	border:none; 
	font-size:9px; 
	font-family:verdana;arial;
	text-align:right
	}
//-->
</style>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>
var FormFields = new Array()
FormFields[0] = "txt_INITIAL|Inital Value|CURRENCY|Y|2"
FormFields[1] = "txt_STARTDATE|Start Date|DATE|Y"
FormFields[2] = "txt_ENDDATE|End Date|DATE|Y"
FormFields[3] = "txt_CYCLE|Cycle|INTEGER|Y"
FormFields[4] = "txt_HBREF|Housing Benefit Reference|TEXT|N"

function getDateDiff(sStartDate, sEndDate){
   var s, iStartDate, iEndDate;    //Declare variables.
   var MinMilli = 1000 * 60;       //Initialize variables.
   var HrMilli = MinMilli * 60;
   var DyMilli = HrMilli * 24;
   iStartDate = new Date(sStartDate);

   iEndDate = new Date(sEndDate);
   s = Math.round((iEndDate - iStartDate) / DyMilli)
   
   return(s);                      //Return results.
}

MONTHS = new Array(
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
  'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

function AddDays(){
	return false;
	if (!checkForm()) return false;
	HB_InitialStart = document.getElementById("txt_STARTDATE").value
	SDA = HB_InitialStart.split("/")
	HB_StartYear = SDA[2]
	HB_InitialStart = SDA[0] + " " + MONTHS[SDA[1]-1] + " " + SDA[2]
	tempDate = new Date(HB_InitialStart)

	//GET THE STANDARD HOUSING BENEFIT CYCLE 
	Payment_Cycle = parseInt(document.getElementById("txt_CYCLE").value,10)
		
	tempDate.setTime(tempDate.UTC() + ((Payment_Cycle-1) * parseInt(24 * 60 * 60 * 1000)))
	
	document.getElementById("txt_ENDDATE").value = tempDate.getDate() + "/" + (tempDate.getMonth()+1) + "/" + tempDate.getFullYear()
	}

function HBSchedule(){
	RESULTS2.innerHTML = ""
	if (!checkForm()) return false;
	//GET THE START DATE
	HB_InitialStart = document.getElementById("txt_STARTDATE").value
	SDA = HB_InitialStart.split("/")
	HB_StartYear = SDA[2]
	HB_InitialStart = SDA[0] + " " + MONTHS[SDA[1]-1] + " " + SDA[2]
	
	//GET THE END DATE
	HB_InitialEnd = document.getElementById("txt_ENDDATE").value
	EDA = HB_InitialEnd.split("/")
	HB_InitialEnd = EDA[0] + " " + MONTHS[EDA[1]-1] + " " + EDA[2]

	//MAKE SURE THE END DATE IS GREATER THAN OR EQUAL TO THE START DATE
	hb_st_tst = new Date(HB_InitialStart)
	hb_ed_tst = new Date(HB_InitialEnd)
	if (hb_st_tst > hb_ed_tst) {
		ManualError("img_STARTDATE", "The Start Date must be less than or equal to the End Date", 0)
		ManualError("img_ENDDATE", "The End Date must be greater than or equal to the Start Date", 0)
		return false;
		}		
	
	//NEXT FIND OUT THE FINANCIAL YEAR WE ARE WORKING WITH DEPENDING ON THE START DATE SELECTED
	YearChecker = getDateDiff("1 April " + HB_StartYear + ' 03:00', HB_InitialStart + ' 07:00')
	if (YearChecker < 0) HB_StartYear = HB_StartYear - 1
	
	//NEXT FIND OUT HOW MANY DAYS IN THE SELECTED FINANCIAL YEAR
	DaysInYear = getDateDiff("1 April " + HB_StartYear + ' 03:00', "1 April " + (parseInt(HB_StartYear,10)+1) + ' 07:00')
	
	//GET THE STANDARD HOUSING BENEFIT CYCLE 
	Payment_Cycle = parseInt(document.getElementById("txt_CYCLE").value,10)

	//INITIAL CYCLE -- THIS IS WORKED OUT FROM THE START DTAE AND END DATE
	Initial_Cycle = parseInt((getDateDiff(HB_InitialStart + ' 03:00', HB_InitialEnd + ' 07:00') + 1),10)
	//INITIAL PAYMENT
	Initial_Payment = parseFloat(document.getElementById("txt_INITIAL").value)
	if (Initial_Payment < 0) Initial_Payment = 0 - Initial_Payment
	
	//DR == DAILY RATE 
	DR = -Initial_Payment / Initial_Cycle
	//ONE12 == DAILY RATE MULTIPLIED BY THE YEAR DIVIDED BY 12
	ONE12 = (DR * DaysInYear)/12
	
	//THIS FIGURE CALCULATES THE EXACT AMOUNT THAT IS PAID PER YEAR IN HOUSING BENEFITS, BASED ON OUR INITIAL ESTIMATION
	RE_YHBE = ONE12 * 12
	
	NewTable = "<br><table border='1' style='border-collapse:collapse'><tr><td colspan=4><b>Next 13 Anticipated Housing Benefit Payments</b></td></tr>"
	NewTable += "<tr><td nowrap width=100><b>Date From</b></td><td nowrap width=100><b>Date To</b></td><td width=80px nowrap><b>Days</b></td><td width=120 nowrap><b>Amount</b></td></tr>"

	//GET THE FINANCIAL YEAR END AND PUT INTO A DATE OBJECT
	FY_EndDate = new Date(("31 March " + (parseInt(HB_StartYear,10)+1)))
	OneDay = parseInt(24 * 60 * 60 * 1000)

	//GET THE FINANCIAL YEAR START AND PUT INTO A DATE OBJECT
	FY_StartDate = new Date("1 April " + (HB_StartYear))

	//SET THE INITIAL STARTDATE INTO A DATE OBJECT
	HB_Start = new Date(HB_InitialStart + ' 03:00')

	//SET THE INITIAL ENDDATE INTO A DATE OBJECT
	HB_End = new Date(HB_InitialEnd + ' 07:00')

	//THIS IS JUST THE MONTH START CHECKER...
	INITIAL = 1

   //To get around the fact that we use daylight saving in this country add 2 hours to the time so that all calculations work.
   //DayLightAdjustment = 2 * 60 * 60 * 1000
	
	//THIS IS THE DATE COUNTER WHICH IS USED TO GENERATE THE CYCLE DATES AND PRINT OUT FORMATTED DATES
	DateCounter = new Date(HB_End.getTime() + OneDay )
	DateCounterPlusCycle = new Date(HB_End.getTime() + (Payment_Cycle * OneDay))
	
	for (i=1; i<14;i++){
		if (INITIAL == 1){

			//THESE ER_ VARIABLES ARE USED TO CALCULATE THE ROUNDING ERROR THAT IS PRODUCED
			ER_AM = DR * Payment_Cycle
			
			//AM == EXPECTED HB AMOUNT FOR SPECIFIED PERIOD			
			AM = RoundToNdp(DR * Initial_Cycle,2)
			
			//ALL FIGURES WITH A To ARE USED TO TOTAL THE RESULTS SO THAT SUMMARY TOTALS CAN BE GIVEN AT THE END
			ToAM = AM

			iDays = getDateDiff(HB_Start, HB_End) + 1
			//WRITE THE DATA TO A STRING VARIABLE
			NewTable += "<tr><td><input size=13 readonly type='text' name='HBStartDate" + i + "' value='" + DisplayDate(HB_Start) + "'></td><td><input size=13 type='text' readonly name='HBEndDate" + i + "' value='" + DisplayDate(HB_End) + "'></td><td align=right>" + iDays + "</td><td align=right><input type='text' readonly name='HBHB" + i + "' value='" + FormatCurrency(AM) + "'></td></tr>"
			//SET THE INITIAL  == 2 SO THAT THIS ELSE BLOCK IS NOT RUN AGAIN
			INITIAL	= 2			
			}
		//IF THIS IS NOT THE FIRST TIME			
		else {
			//VIEW ABOVE TO SEE WHAT EACH OF THE ELEMENTS MEANS...
			ER_AM += DR * Payment_Cycle

			AM = RoundToNdp(DR * Payment_Cycle,2)
			ToAM += AM

			iDays = getDateDiff(DateCounter, DateCounterPlusCycle) + 1
						
			NewTable += "<tr><td><input size=13 readonly type='text' name='HBStartDate" + i + "' value='" + DisplayDate(DateCounter) + "'></td><td><input size=13  readonly type='text' name='HBEndDate" + i + "' value='" + DisplayDate(DateCounterPlusCycle) + "'></td><td align=right>" + iDays + "</td><td align=right><input readonly type='text' name='HBHB" + i + "' value='" + FormatCurrency(AM) + "'></td></tr>"
			DateCounter.setTime(DateCounter.getTime() + (Payment_Cycle * OneDay))
			DateCounterPlusCycle.setTime(DateCounterPlusCycle.getTime() + (Payment_Cycle * OneDay))
			}
		}

	NewTable += "<tr><td colspan=4 style='border:none' align=right><input name='btn_SUBMIT' type='button' value=' <%=savebuttontext%> ' class='RSLButton' style='text-align:center' onclick='SubmitForm()'></td></tr>"
	NewTable += "</table>"
	RESULTS2.innerHTML = NewTable	
	}

function DisplayDate(iDate){
 	iTemp = iDate.getDate() + " " + MONTHS[iDate.getMonth()] + " " + iDate.getFullYear()
	return iTemp
	}
	
function RoundToNdp(X, N) { var T = Number('1e'+N)
  return Math.round(X*T)/T }	
  
function LoadTable(j){
	for (i=1; i<4; i++)
		document.getElementById("RESULTS" + i).style.display = "none" 
	document.getElementById("RESULTS" + j).style.display = "block" 		
	}

function SubmitForm(){
	document.getElementById("btn_SUBMIT").disabled = true
	document.getElementById("btn_SUBMIT").value = " Submitted "
	RSLFORM.submit()	
	}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages();<%=onloadfunction%>" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method="POST" action="Serverside/HB_svr.asp?CustomerID=<%=Request("CustomerID")%>">
<TABLE>
	<TR>
		
      <TD colspan=2>
	    <% if (action = "UPDATE") then %>
		When updating your housing benefit schedule, you can only update fields which have a solid border around them. If there are previously validated rows then you will only be able to update past this point.
		<% else %>
		To create a housing benefit schedule enter the initial information 
        below. The information can be estimated or actual figures. Once you enter 
        the Start Date, the End Date will automatically be populated with a figure 
        which is 28 days ahead, this figure can then be manually changed if it 
        is desired. Once all fields have been entered a schedule will automatically 
        be generated.
		<% end if %>
		<br>
        <br></TD>				
	</TR>
	<TR>
		<TD valign=top>
		
	<table><tr>
		<TD>HB Ref:</TD>
		<TD><INPUT type="text" name="txt_HBREF" class="textbox200" value="<%=hbref%>" onblur="HBSchedule()"></TD>
		<TD><IMG SRC="/js/FVS.gif" width=15 height=15 name="img_HBREF">
		<input type=hidden name="HBID" value="<%=Request("HBID")%>">
		<input type=hidden name="TenancyID" value="<%=TenancyID%>">
		<input type=hidden name="CustomerID" value="<%=CustomerID%>">		
		<input type=hidden name="UPDATEHBINFO" value="<%=HASVALIDATED%>">		
		<input type=hidden name="ACTION" value="<%=action%>"></TD>
	</TR>
<% IF (HASVALIDATED = "YES") THEN %>
	<TR>
		<TD nowrap>Initial Payment:</TD>
		<TD><INPUT type="text" style='border:none' READONLY class="textbox200" value="<%=initialpayment%>"></TD>
	</TR>
	<TR>
		<TD>Start Date:</TD>
		<TD><INPUT type="text" style='border:none' READONLY class="textbox200" value="<%=initialstartdate%>"></TD>
	</TR>
	<TR>
		<TD>End Date:</TD>
		<TD><INPUT type="text" style='border:none' READONLY class="textbox200" value="<%=initialenddate%>"></TD>
	</TR>
<% ELSE %>
	<TR>
		<TD nowrap>Initial Payment:</TD>
		<TD><INPUT type="text" name="txt_INITIAL" class="textbox200" value="<%=initialpayment%>" onblur="HBSchedule()"></TD>
		<TD><IMG SRC="/js/FVS.gif" width=15 height=15 name="img_INITIAL"></TD>		
	</TR>
	<TR>
		<TD>Start Date:</TD>
		<TD><INPUT type="text" name="txt_STARTDATE" class="textbox200" value="<%=initialstartdate%>" onblur="AddDays();HBSchedule()"></TD>
		<TD><IMG SRC="/js/FVS.gif" width=15 height=15 name="img_STARTDATE"></TD>				
	</TR>
	<TR>
		<TD>End Date:</TD>
		<TD><INPUT type="text" name="txt_ENDDATE" class="textbox200" value="<%=initialenddate%>" onblur="HBSchedule()"></TD>
		<TD><IMG SRC="/js/FVS.gif" width=15 height=15 name="img_ENDDATE"></TD>				
	</TR>
<% END IF %>	
	<TR>
		<TD>Default Cycle:</TD>
		<TD><INPUT type="text"  style='border:1px dotted #133E71' name="txt_CYCLE" class="textbox200" value="28" READONLY></TD>
		<TD><IMG SRC="/js/FVS.gif" width=15 height=15 name="img_CYCLE"></TD>				
	</TR>	
	</table>
	</td><td valign=top>
<% IF (HASVALIDATED = "YES") THEN %>
		<table border=1 style='border-collapse:collapse'>
		<tr><td colspan=4><b>Last <%=LASTHBCounter%> HB Payments</b></td></tr>
		<%=StringTable%>
		</table>
		<br>
		<table border=1 style='border-collapse:collapse'>
		<tr><td colspan=4><b>Next Estimated HB Payment</b></td></tr>
		<%=NextTable%>
		</table>
<% END IF %>		
		<div id="RESULTS2"></div>
	</TD></TR>
</TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
