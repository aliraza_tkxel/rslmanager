<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->

<%

    Dim CONST_PAGESIZE
    
    if(Request("pagesize")<>"") then
       CONST_PAGESIZE  = Request("pagesize")
    else
        CONST_PAGESIZE = 20    
    end if

	
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim PageName_Export
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal,lstcustomerstatus
	Dim ochecked, cchecked,rqCustomerStatus
	Dim orderBy
	Dim RISK,SUB_RISK, general_history_id , journal_id ,rskCount , rqName , rqCustomerId ,rqSubCat, rqMonth, rqYear,rqStatus, param ,rqPatch, rqScheme, SchemeFilter, rqCat , rqUserId
	
	' FOR WHITE BOARD ALERT
	
	rqStatus=Request("sel_Status")
    rqUserId=Request("sel_Users")
	''''''''''''''''''''''''''''''
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	rqPatch =  Request("sel_Patch") 
	IF rqStatus <>"" THEN
	param = param & "@STATUS=" & rqStatus & "," 
	END IF

    IF rqUserId <>"" THEN
	param = param & "@USER=" & rqUserId & "," 
	END IF
	        if(rqPatch<>"") then
		        SchemeFilter="P_SCHEME SCH INNER JOIN  PDR_DEVELOPMENT PD ON SCH.DEVELOPMENTID = PD.DEVELOPMENTID WHERE PD.PATCHID=" & rqPatch
		    else
		        SchemeFilter="P_SCHEME"    
		    end if

       if (Request("go_clicked")) = 1 then
      
            param = ""
	        'build the scheme and action sql 
	        rqCustomerStatus=1
	     	        
	        IF rqPatch <>"" then
	            param = param & "@PATCH=" & rqPatch & "," 
	        end iF
	        
	        rqScheme =  Request("sel_Scheme") 
	        IF rqScheme <>"" then
	            param = param & "@SCHEME=" & rqScheme & "," 
	        end iF
	        
	        rqMonth =  Request("sel_Month") 
	        IF rqMonth <>"" then
	            param = param & "@REVIEWMONTH=" & rqMonth & "," 
	        end if
	        
	        rqYear =  Request("sel_Year") 
	        IF rqYear <>"" then
	            param = param & "@REVIEWYEAR=" & rqYear & "," 
	        end iF

	        rqCat =Request("sel_RISK_CATEGORY") 
	        IF rqCat <>"" then
	            param = param & "@CATEGORYID=" & rqCat & "," 
	        end iF	        
	            	
	        rqSubCat =Request("sel_RISK_SUBCATEGORY") 
	        IF rqSubCat <>"" then
	            param = param & "@SUBCATEGORYID=" & rqSubCat & "," 
	        end iF
	        
	        rqStatus=Request("sel_Status")
	        IF rqStatus <>"" then
	            param = param & "@STATUS=" & rqStatus & "," 
	        end iF

            rqUserId=Request("sel_USERS")
	        IF rqUserId <>"" then
	            param = param & "@USER=" & rqUserId & "," 
	        end iF
    		
	       		
		END IF  

        SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID IN (101,103,104) AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		'Open database and create a dropdown compiled with the housing officers
		OpenDB()
        Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", rqUserId, NULL, "textbox200", " style='width:150px' ")
	    Call BuildSelect(selScheme, "sel_SCHEME", SchemeFilter ,"SCHEMEID, SCHEMENAME ", "SCHEMENAME", "All", rqScheme, NULL, "textbox200","TABINDEX=11")	
		Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", rqPatch, NULL, "textbox200", "TABINDEX=11 onChange=GetSchemes()")	
		Call BuildSelect(lstFullSubCategory, "sel_RISK_FULLSUBCATEGORY", "C_RISK_SUBCATEGORY" ,"SUBCATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", rqSubCat, NULL, "textbox200", " style='width:300px' ")
	    Call BuildSelect(lstMonth, "sel_MONTH", "G_MONTH" ,"MONTHORDER, DESCRIPTION", "MONTHORDER", "Please Select", rqMonth, NULL, "textbox200", " style='width:150px' ")			
		Call BuildSelect(lstYear, "sel_YEAR", "G_YEAR y" ,"YEAR, YEAR", "y.YEAR", "Please Select", rqYear, NULL, "textbox200", " style='width:150px' ")			
		Call BuildSelect(lstCategory, "sel_RISK_CATEGORY", "C_RISK_CATEGORY" ,"CATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", rqCat, NULL, "textbox100", " style='width:150px;' onchange='get_subcategory()' ")
		CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Building of filter boxes and dropdowns 	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "RiskList.asp"
	PageName_Export = "RiskList_xls.asp"
	MaxRowSpan = 7
	
	
	OpenDB()
	
	get_records()
	
	CloseDB()
	

	Function get_records()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")


	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			Dim strSQL, rsSet, intRecord 
			
			intRecord = 0
			str_data = ""
'
		IF RIGHT(PARAM,1)= "," then' AND LEN(PARAM)>2 THEN
		    PARAM = LEFT(PARAM,LEN(PARAM)-1)
		END IF   
			
							
			SQL = "execute RISK_LIST " & PARAM
			'response.write("<BR>" & sql)
			
            
			
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = SQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
				
				    journal_id = rsSet("JOURNALID") 
				    general_history_id = rsSet("RISKHISTORYID")
            				
				            '------------------------------------
				            strSQL1 =  " SELECT CAT.DESCRIPTION CATDESC ,SUBCAT.DESCRIPTION SUBDESC FROM C_CUSTOMER_RISK  CR " &_
				          " INNER JOIN C_RISK_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID=CR.SUBCATEGORYID " &_  
                          " INNER JOIN C_RISK_CATEGORY CAT ON CAT.CATEGORYID=SUBCAT.CATEGORYID " &_
                          " WHERE JOURNALID=" & journal_id & " AND RISKHISTORYID=" & general_history_id & " ORDER BY SID "
                                                      
                            Call OpenRs (rsSet1, strSQL1)  
                            rskCount = 0
                            While Not rsSet1.EOF 
                            
                                    if rskCount > 0 then
                                        RISK = RISK & "<br/>" & "- " & rsSet1("CATDESC") 
                                        SUB_RISK = SUB_RISK & "<br/>" & "- " & rsSet1("SUBDESC") 
                                    else
                                        RISK = "- " & rsSet1("CATDESC") 
                                        SUB_RISK = "- " & rsSet1("SUBDESC") 
                                    end if
                                    rskCount = rskCount + 1
      		                        rsSet1.movenext()
	                        Wend 
                            
                            CloseRs(rsSet1) 
            			
							
						
						strEnd =	"<TR><TD>" & rsSet("STARTDATE") & "</TD>" &_ 
						                 "<TD><font color=blue style='cursor:hand' onClick='load_me("""& rsSet("CUSTOMERID") & """)'>" & rsSet("NAME") & "</TD>" &_	
						                 "<TD>" & rsSet("ADDRESS") & "</TD>" &_	
						                 "<TD>" & RISK &  "</TD>" &_	
						                 "<TD>" & SUB_RISK &  "</TD>" &_	
    						             "<TD>" & rsSet("REVIEWDATE") & "</TD>" &_	 
    						             "<TD>" & rsSet("CLOSEDATE") & "</TD>" 
						                 
					  
									
									strEnd = strEnd & "</TR>"	
	
						 str_data = str_data & strEnd
											
						count = count + 1
		
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				
		
				str_data = str_data & strTotals & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=50></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page size:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='resize_page()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No Records found in the system</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing

		End function
	
		Function WriteJavaJumpFunction()

			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Risk Monitoring</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

var FormFields = new Array()


	FormFields[0] = "sel_RISK_SUBCATEGORY|Type|SELECT|N"
	FormFields[1] = "sel_MONTH|Review Month|SELECT|N"
	FormFields[2] = "sel_YEAR|Review Year|SELECT|N"
	FormFields[3] = "sel_STATUS|Status|SELECT|N"
    FormFields[4] = "sel_USERS|Assign To|SELECT|N"
	


	//function GO(){

	//	if (!checkForm()) return false;
	//		RSLFORM.action = "<%=PageName%>"
	//		RSLFORM.target = ""
	//		RSLFORM.submit()
	//	}


	function load_me(CustomerID){

		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

	function GO(){

		if (!checkForm()) return false;
       
		location.href = "<%=PageName%>?go_clicked=1&<%="CC_Sort=" & orderBy & ""%>" + "&sel_RISK_CATEGORY=" + RSLFORM.sel_RISK_CATEGORY.value + "&sel_RISK_SUBCATEGORY=" + RSLFORM.sel_RISK_SUBCATEGORY.value + "&sel_MONTH=" + RSLFORM.sel_MONTH.value + "&sel_YEAR=" + RSLFORM.sel_YEAR.value + "&sel_STATUS=" + RSLFORM.sel_STATUS.value + "&sel_Scheme=" + RSLFORM.sel_SCHEME.value + "&sel_PATCH=" + RSLFORM.sel_PATCH.value + "&sel_Users=" + RSLFORM.sel_USERS.value
		}
		
	function Export()
	{
         location.href = "<%=PageName_export%>?go_clicked=1&<%="CC_Sort=" & orderBy & ""%>" + "&sel_RISK_CATEGORY=" + RSLFORM.sel_RISK_CATEGORY.value +  "&sel_RISK_SUBCATEGORY=<%=rqSubcat%>" + "&sel_MONTH=" + RSLFORM.sel_MONTH.value + "&sel_YEAR=" + RSLFORM.sel_YEAR.value + "&sel_STATUS=" + RSLFORM.sel_STATUS.value + "&sel_Scheme=" + RSLFORM.sel_SCHEME.value + "&sel_PATCH=" + RSLFORM.sel_PATCH.value + "&sel_Users=" + RSLFORM.sel_USERS.value
	}	

	
	function resize_page()
	{
	    // RSLFORM.pagesize.value=RSLFORM.QuickJumpPage.value;
	    location.href = "<%=PageName%>?go_clicked=1&<%="CC_Sort=" & orderBy & ""%>" + "&txt_CUSTOMERNAME=" + RSLFORM.txt_CUSTOMERNAME.value + "&txt_CUSTOMERNO=" + RSLFORM.txt_CUSTOMERNO.value + "&sel_RISK_FULLSUBCATEGORY=" + RSLFORM.sel_RISK_FULLSUBCATEGORY.value + "&sel_MONTH=" + RSLFORM.sel_MONTH.value + "&sel_YEAR=" + RSLFORM.sel_YEAR.value + "&sel_STATUS=" + RSLFORM.sel_STATUS.value + "&pagesize=" + RSLFORM.QuickJumpPage.value + "&sel_Users=" + RSLFORM.sel_USERS.value
    		
	}
	
function GetSchemes(){
	    RSLFORM.sel_SCHEME.disabled = true
	    RSLFORM.target = "RISK_Server"
	    RSLFORM.action = "Serverside/GetSchemes.asp"
	    RSLFORM.submit()
	}
	
	function get_subcategory(){
	
		//alert(RSLFORM.sel_RISK_CATEGORY.value);
		RSLFORM.action = "Serverside/change_risk.asp?RISKID="+RSLFORM.sel_RISK_CATEGORY.value
		RSLFORM.target = "Risk_Server"
		RSLFORM.submit()
	}	
		
	
	
// -->
</SCRIPT>
<%'=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();get_subcategory()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method=POST>
  <table border=0 width=750 cellpadding=2 cellspacing=2 height=45PX style='BORDER:1PX SOLID #133E71;padding-right:0px;padding-bottom:10px;' bgcolor=beige>
    <tr> 
      
      <td NOWRAP><b>Patch: </b></td>
      <td>
        <%=lstPatch%>
      </TD>
      <TD>
        <img name="img_PATCH" src="/js/FVS.gif" width=7 height=7> 
      </td>
       <td NOWRAP><b>Category: </b></td>
      <td><%=lstCategory %></TD>
      <td>  
        <img name="img_RISK_CATEGORY" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>

     
      
    </tr>
    <tr> 
      
       <td NOWRAP><b>Scheme: </b></td>
      <td>
        <%=SelScheme%>
      </TD>
      <TD>
        <img name="img_SCHEME" src="/js/FVS.gif" width=7 height=7> 
      </td>
      
     <td NOWRAP><b>Sub Category: </b></td>
      			<td><SELECT NAME='sel_RISK_SUBCATEGORY' class='textbox200' style='width:150px'>
					<OPTION VALUE=''>Please select category</OPTION>
				</SELECT></td>
      <td>  
        <img name="img_RISK_SUBCATEGORY" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>

    </tr>
     <tr> 
      
        <td NOWRAP><b>Review Month </b></td>
      <td><%=lstMonth %></TD>
      <td>  
        <img name="img_MONTH" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>
       <td NOWRAP><b>Review year </b></td>
      <td><%=lstYear %></TD>
      <td>  
        <img name="img_YEAR" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>

      
    </tr>
    
    <tr> 
      
       <td NOWRAP style="height: 28px"><b>Status</b></td>
      <td>
        <select name="sel_STATUS" class="textbox">
            <option value="-1">All</option>
            <option <% if rqStatus=1 then response.write("selected")%> value="1" >Open</option>
            <option <% if rqStatus=2 then response.write("selected")%> value="2">Review Due</option>
            <option <% if rqStatus=3 then response.write("selected")%> value="3" >Closed</option>
          </select>
      </TD>
      <td>  
        <img name="img_STATUS" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>
      
       <td>
            <input type="button" value=" GO " class="RSLButton" onClick="GO()" name="button" style="width:50px;">
      </td>
      <td>
            <input type="button" value=" Export " class="RSLButton" onClick="Export()" name="button" style="width:50px;">
      </td>

    </tr>
     <tr> 
      
        <td NOWRAP><b>Assign To </b></td>
      <td><%=lstUsers %></TD>
      <td>  
        <img name="img_USERS" src="/js/FVS.gif" height=9 style="width: 7px"> 
      </td>
      
      
    </tr>
    


     <tr><td style="height:10px; width: 50px;">&nbsp;</td></tr>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=3 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=672 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
      <TR>
        <TD CLASS='NO-BORDER' WIDTH=70>&nbsp;<B>Start</B></TD>
        <TD CLASS='NO-BORDER' WIDTH=100>&nbsp;<B>Name</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=100>&nbsp;<B>Address</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=100>&nbsp;<B>Category</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=150>&nbsp;<B>Sub Category</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=50>&nbsp;<B>Review</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=90>&nbsp;<B>Close Date</B>&nbsp;</TD>
        
        
        
        </TR>
    </THEAD>
    <TR STYLE='HEIGHT:3PX'>
      <TD COLSPAN=11 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%>
  </TABLE>
  <input type="hidden" name="pagesize" value="<%=CONST_PAGESIZE%>" />
  <input type="hidden" name="SUBCAT" value="<%=rqSubCat%>" />

</form>
<p>&nbsp;</p>
<p>
  <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
 </p>
 <iframe name="Risk_Server" width=400px height=400px style='display:knone'></iframe> 
</BODY>
</HTML>
