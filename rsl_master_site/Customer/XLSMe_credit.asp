<%
Response.Buffer = True
Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 1000
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim ArrearsTotal
	
	Dim ochecked, cchecked
	
	'Catch Housing Officer ID from dropdown
	If Request("WHICH") <> "" then
	
		If NOT Request("WHICH") = "All" then 
			theUser = cInt(Request("WHICH"))
		Else
			theUser = Request("WHICH")
		End If
		
	Else
		' This will be the initial value of the dropdown on loading of page
		theUser = Session("UserID")
	End If

' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request("page") = "" Then
		intPage = 1	
	Else
		intPage = Request("page")
	End If

	PageName = "ArrearsList.asp"
	MaxRowSpan = 6
	Dim orderBy
		orderBy = "T.TENANCYID DESC"
		if (Request.QueryString("CC_Sort") <> "") then orderBy = Request.QueryString("CC_Sort")

    OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()
	
	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
				
		CompareValue = "300.00"
		if (Request.QueryString("COMPAREVALUE") <> "") then
			CompareValue = Request.QueryString("COMPAREVALUE")
		end if

		THEWHICH = Request.QueryString("WHICH")
		
		' This is the user id which
		IF NOT THEWHICH <> "" THEN 
		ES = " AND P.HOUSINGOFFICER = " & Session("USERID") 
		elseif  THEWHICH = "All" then
		ES =  ""
		else
		ES = " AND P.HOUSINGOFFICER = " & THEWHICH 
		End If
			
		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
	strSQL =	"	SELECT T.TENANCYID, T.STARTDATE, P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE, ISNULL(A.AMOUNT,0) AS TOTALARREARS, FINALAMOUNT = -A.Amount ,  " &_
				" 			(SELECT MAX(TRANSACTIONDATE) FROM F_RENTJOURNAL  " &_
				" 				LEFT JOIN F_PAYMENTTYPE PY ON PY.PAYMENTTYPEID = PAYMENTTYPE WHERE TRANSACTIONDATE IS NOT NULL AND TENANCYID = T.TENANCYID AND STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND PY.RENTRECEIVED IS NOT NULL) AS LASTPAYMENTDATE" &_
				" 			FROM C_TENANCY T " &_ 
				"		LEFT JOIN (SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID FROM F_RENTJOURNAL I_J " &_
				"		LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID " &_
				" 		LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID" &_
				" 		LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID WHERE I_J.STATUSID NOT IN (1,4) " &_
				" 			AND I_J.ITEMTYPE IN (1,8,9,10) GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID " &_
				" 					INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				" 					LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID " &_
									   "WHERE A.AMOUNT < 0 and a.amount <= - " & CompareValue & " AND T.ENDDATE IS NULL " & ES &_
				" order By " & orderBy

		'response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount
		
		' This gets the sum of the arrears
		INNSERSELECT = "SELECT 	ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID " &_
						"FROM	 	F_RENTJOURNAL I_J " &_
						"			LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID " &_
						"			LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  " &_
						"			LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID " &_
						"WHERE		((I_J.STATUSID NOT IN (1,4)) OR (I_J.STATUSID IS NULL)) AND I_J.ITEMTYPE IN (1,8,9,10) " &_
						"GROUP BY I_J.TENANCYID "
							
		str_data = ""
		strSQL2 = "SELECT  " &_
					"ISNULL(SUM(abs(AMOUNT)), 0) AS TOTALAMOUNT FROM C_TENANCY T  " &_
					"LEFT JOIN (" & INNSERSELECT & ") A ON A.TENANCYID = T.TENANCYID " &_	
					"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
					"LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID " &_					
					"WHERE A.AMOUNT >= " & CompareValue & " AND T.ENDDATE IS NULL " & ES
						

		Call OpenRs(rsSum, strSQL2)
		Dim theTotalAmount
		theTotalAmount = rsSum("TOTALAMOUNT")
		'End Get sum 
		
		' Sort pages
		intpage = CInt(Request.QueryString("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
		
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			PrevTenancy = ""
			ArrearsTotal = 0
			For intRecord = 1 to rsSet.PageSize
				TenancyID = rsSet("TENANCYID")
				CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
						"FROM C_CUSTOMERTENANCY CT " &_
						"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
						"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
						"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & TenancyID
						
				Call OpenRs(rsCust, CustSQL)
				strUsers = ""
				CustCount = 0
				while NOT rsCust.EOF 
					if CustCount = 0 then
						strUsers = strUsers &  rsCust("FULLNAME") 
						CustCount = 1
					else
						strUsers = strUsers & ", " &  rsCust("FULLNAME")
					end if					
					rsCust.moveNext()
				wend
				CloseRs(rsCust)
				SET rsCust = Nothing
				
				strStart = "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
				strEnd = "<TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & ", " & rsSet("TOWNCITY") & "</TD>" &_
					"<TD>" & rsSet("POSTCODE") & "</TD>" &_
					"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_												
					"<TD align=right>" & FormatCurrency(rsSet("FINALAMOUNT")) & "</TD></TR>"

				str_data = str_data & strStart & "<TD width=190>" & strUsers & "</TD>" & strEnd

				'Calculates the total amount of Arrears
				ArrearsTotal = 	rsSum("TOTALAMOUNT")
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
		End If
		
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
		
		End If
		
		rsSet.close()
		Set rsSet = Nothing

	End function

%>

<HTML>
<HEAD>
<TITLE>RSL Manager Customers --> Arrears List --> Previous Tenants</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 >
    <THEAD> 
  <TR align="left"> 
    <TD CLASS='NO-BORDER' colspan="6"> 
      <div align="left"><font color="#0066CC"><b><font size="4">Tenant Credit 
        Report -</font> 
        <%dim todaysDate
				 todaysDate=now()
				 response.write  todaysDate
				%>
        </b></font></div>
      </TD>
    </TR>
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=90PX><B>Tenancy</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=190PX><B>Customer</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=250PX><B>Address</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=90PX><B>Postcode</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=90PX><B>Startdate</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=80PX><B>Value</B>&nbsp;</TD>
    </TR>
    </THEAD> <%=str_data%> 
	    <TR> 
      <TD CLASS='NO-BORDER' colspan="4" height="2"></TD>
      <TD CLASS='NO-BORDER' colspan="2" bgcolor="#0066CC" height="2"></TD>
    </TR>
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=90PX>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=190PX>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=250PX>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=90PX>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=90PX><b>Total</b></TD>
      <TD CLASS='NO-BORDER' WIDTH=80PX align="right"> <font color="#0066CC"><b>
        <% response.write FormatCurrency(ArrearsTotal) %></b>
        </font> </TD>
  </TABLE>
</BODY>
</HTML>