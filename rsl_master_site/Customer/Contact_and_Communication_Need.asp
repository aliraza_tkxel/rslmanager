<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal
	Dim ochecked, cchecked
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Catch Housing Officer ID from dropdown
		theUser = Request("WHICH")  								
		If theUser <> "" then
			If NOT theUser = "All" then 
				theUser = cInt(theUser)
			Else
				theUser = theUser
			End If
		Else
			' This will be the initial value of the dropdown on loading of page 
			theUser = "All"
		End If
		
		'build the scheme and action sql 
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to
		rqAction 	= Request("SEL_ACTION")	' Action last Taken
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsList.asp"
	MaxRowSpan = 9
	
	
	

	// pads table out to keep the height consistent
	Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
	End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Contact and Communication Need </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	 /////////////////////////////////
	//	Opening data in Xls Below
	/////////////////////////////////
	function ExportMe()
	{
		window.open("Contact_and_Communication_Need_xls.asp")//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
	}	
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border=0 width=750 cellpadding=2 cellspacing=2 height=40PX style='BORDER:1PX SOLID #133E71' bgcolor=beige>
 <tr> 
      <td><p> This page provides an export link that runs a data query to provide a results report detailing those customers with an alternative correspondence address or a communication need. This is for reference purposes only to enable you to ensure that the customer's request is adhered to. It is advisable to run this report to use in conjunction with Mailing List, Rent Statement and Rent Letter export tools.</p></td>
      <td>
            <table>
                    <tr>
                        <td width=12% NOWRAP><b>Excel Version</b></td>
                    </tr>
                    <tr>    
                        <td width=12% NOWRAP align="center"><img src="../myImages/PrinterIcon.gif" width="31" height="20" onClick="ExportMe()" style="Cursor:hand" alt="Print Arrears List"></td>
                    </tr>
            </table>
       </td>
 </tr>
  </table>
<p>
  <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</p>
</BODY>
</HTML>