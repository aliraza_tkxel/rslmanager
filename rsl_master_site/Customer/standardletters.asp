<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%

	CONST CONST_PAGESIZE = 20
	
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc		   (3)

	Dim lstNature, lstFrom, lstTo

	ColData(0)  = "Nature|LNATURE|80"
	SortASC(0) 	= "N.DESCRIPTION ASC"
	SortDESC(0) = "N.DESCRIPTION DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Action|LACTION|100"
	SortASC(1) 	= "A.DESCRIPTION ASC"
	SortDESC(1) = "A.DESCRIPTION DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Letter Code|LCODE|100"
	SortASC(2) 	= "L.LETTERCODE ASC"
	SortDESC(2) = "L.LETTERCODE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Description|LDESC|130"
	SortASC(3) 	= "L.LETTERDESC ASC"
	SortDESC(3) = "L.LETTERDESC DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""
	
	PageName = "standardletters.asp"
	EmptyText = "No standard letters found in the system.!!!"
	DefaultOrderBy = SortASC(3)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""LETTERID"") & "")"""" """ 
	
	' this is from filter
	theNature =  request("sel_NATUREFILTER")
	if theNature <> "" then
	WhereStr = " WHERE L.NATUREID = " & theNature
	End If
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE =	" SELECT L.LETTERID, ISNULL(N.DESCRIPTION, 'N/A') AS LNATURE, " &_
				"		ISNULL(A.DESCRIPTION, 'N/A') AS LACTION,  " &_
				"		ISNULL(L.LETTERCODE, 'N/A') AS LCODE,  " &_
				"		ISNULL(L.LETTERDESC, 'N/A') AS LDESC " &_
				" FROM 	C_STANDARDLETTERS L " &_
				"		LEFT JOIN C_NATURE N ON N.ITEMNATUREID = L.NATUREID " &_
				"		LEFT JOIN C_LETTERACTION A ON A.ACTIONID = L.SUBITEMID " & WhereStr &_
				" ORDER 	BY " & orderBy

	'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
	
	OpenDB()
	Call BuildSelect(lstNature, "sel_NATUREID", "C_NATURE", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:200px' onchange='nature_change()'")
	Call BuildSelect(lstNatureFilter, "sel_NATUREFILTER", "C_NATURE", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:200px' ")
	Call BuildSelect(lstFrom, "sel_FROM", "C_STANDARDLETTERADDRESSES", "ADDRESSID, ADDRESS1", "ADDRESS1", "Please Select", NULL, NULL, "textbox200", " style='width:200px'")
	Call BuildSelect(lstTo, "sel_TO", "C_STANDARDLETTERADDRESSES", "ADDRESSID, ADDRESS1", "ADDRESS1", "Please Select", NULL, NULL, "textbox200", " style='width:200px'")

	CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Adaptation Enquiry List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
    <!--
        var FormFields = new Array();
        FormFields[0] = "txt_LETTERCODE|LETTERCODE|TEXT|Y"
        FormFields[1] = "txt_LETTERDESC|LETTERDESC|TEXT|Y"
        FormFields[2] = "sel_NATUREID|NATUREID|SELECT|Y"
        FormFields[3] = "sel_SUBITEMID|SUBITEMID|SELECT|Y"
        FormFields[4] = "sel_FROM|FROm|SELECT|Y"
        FormFields[5] = "sel_TO|TO|SELECT|Y"

        function save_form() {
            if (!checkForm()) return false;
            document.RSLFORM.hid_ACTION.value = "NEW";
            document.RSLFORM.target = "LETTERFRAME";
            document.RSLFORM.action = "serverside/letter_svr.asp";
            document.RSLFORM.submit();
        }

        function load_me(letter_id) {
            document.RSLFORM.hid_ACTION.value = "LOAD";
            document.RSLFORM.hid_LETTERID.value = letter_id;
            document.RSLFORM.target = "LETTERFRAME";
            document.RSLFORM.action = "serverside/letter_svr.asp?letterid=" + letter_id;
            document.RSLFORM.submit();
            swap_div(3)
        }

        function amend_form() {
            if (!checkForm()) return false;
            document.RSLFORM.hid_ACTION.value = "AMEND";
            document.RSLFORM.target = "LETTERFRAME";
            document.RSLFORM.action = "serverside/letter_svr.asp?letterid=" + RSLFORM.hid_LETTERID.value;
            document.RSLFORM.submit();
        }

        function delete_form() {
            var confirm_delete = confirm("Are you sure you would like to delete this standard letter.")
            if (confirm_delete == true) {
                if (!checkForm()) return false;
                document.RSLFORM.hid_ACTION.value = "DELETE";
                document.RSLFORM.target = "LETTERFRAME";
                document.RSLFORM.action = "serverside/letter_svr.asp?letterid=" + RSLFORM.hid_LETTERID.value;
                document.RSLFORM.submit();
            }
            else return false
        }

        var set_length = 7999;
        function countMe() {
            var str = event.srcElement
            var str_len = str.value.length;
            if (str_len >= set_length)
                str.value = str.value.substring(0, set_length - 1);
        }

        function nature_change() {
            document.RSLFORM.action = "serverside/letter_change_nature.asp?natureid=" + RSLFORM.sel_NATUREID.value
            document.RSLFORM.target = "LETTERFRAME";
            document.RSLFORM.submit();
        }

        function preview() {
            if (!checkForm()) return false;
            window.open("Popups/arrears_letter_preview.asp?showHF=" + RSLFORM.showHF.checked + "&from=" + RSLFORM.sel_FROM.value + "&to=" + RSLFORM.sel_TO.value, "_blank", "width=530,height=600,left=100,top=50,scrollbars=yes");
        }
 
        function swap_div(int_which_one) {
            // manipulate images
            for (j = 1; j < 4; j++) {
                document.getElementById("img" + j + "").src = "Images/" + j + "-s-closed.gif"
                document.getElementById("div" + j + "").style.display = "none"
            }

            // unless last image in row
            if (int_which_one != 3) {
                document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-s-previous.gif"
            }
            document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-s-open.gif"
            if (int_which_one == 3)
                int_which_one = 2;
            document.getElementById("div" + int_which_one + "").style.display = "block"
        }

        function swap_button(int_which) {
            if (int_which == 1) {
                document.getElementById("BTNSAVE").style.display = "none";
                document.getElementById("BTNAMEND").style.display = "block";
                document.getElementById("BTNDELETE").style.display = "block";
            }
            else {
                document.getElementById("BTNDELETE").style.display = "none";
                document.getElementById("BTNAMEND").style.display = "none";
                document.getElementById("BTNSAVE").style.display = "block";
            }
        }

        function nature_filter() {
            document.RSLFORM.action = "standardletters.asp"
            document.RSLFORM.target = "";
            document.RSLFORM.submit();
        }
    // -->
    </script>
</head>
<body onload="initSwipeMenu(5);preloadImages()" onunload="macGo()" class="ta">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="img1" id="img1" title="Standard Letters" onclick="swap_div(1)" src="Images/1-s-open.gif" width="128" height="20" border="0" alt="Standard Letters" style="cursor: pointer" /></td>
            <td rowspan="2" valign="bottom">
                <img name="img2" id="img2" title="New Letter" onclick="swap_div(2);swap_button(2);RSLFORM.reset()" src="Images/2-s-previous.gif" width="54" height="20" border="0" alt="New Letter" style="cursor: pointer" /></td>
            <td rowspan="2" valign="bottom">
                <img name="img3" id="img3" title="Amend Letter" src="Images/3-s-closed.gif" width="73" height="20" border="0" alt="Amend Letter" /></td>
            <td valign="bottom">
                <table width="495" border="0" cellpadding="0" cellspacing="0" style="height:19px">
                    <tr>
                        <td width="23">&nbsp;</td>
                        <td width="218">&nbsp;</td>
                        <td width="212" align="right"><%=lstNatureFilter%></td>
                        <td width="42" align="right">
                            <input type="button" name="BTN_DATES22" id="BTN_DATES22" value='Go' class="RSLBUTTON" style="display: block; width: 30px" onclick="nature_filter()" /></td>
                    </tr>
                </table></td>
        </tr>
        <tr>
            <td bgcolor="#133e71"><img src="images/spacer.gif" width="495" height="1" alt="" /></td>
        </tr>
    </table>
    <div id="div2" style='display: none'>
        <table width="750" style="border-right: solid 1px #133e71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr>
                <td colspan="6">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td rowspan="5">
                    &nbsp;
                </td>
                <td>
                    Nature
                </td>
                <td>
                    <%=lstNature%><img src="/js/FVS.gif" name="img_NATUREID" id="img_NATUREID" width="15px" height="15px"
                        border="0" alt="" />
                </td>
                <td>
                    Code
                </td>
                <td>
                    <input type="text" name="txt_LETTERCODE" id="txt_LETTERCODE" class="textbox200" style="width: 300px" />
                    <img src="/js/FVS.gif" name="img_LETTERCODE" id="img_LETTERCODE" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Action
                </td>
                <td>
                <div id="dvAction"><select name="sel_SUBITEMID" id="sel_SUBITEMID" class="textbox200" style="width: 200px">
                        <option value=''>Please Select Nature</option>
                    </select></div>
                    <img src="/js/FVS.gif" name="img_SUBITEMID" id="img_SUBITEMID" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    Desc
                </td>
                <td>
                    <input type="text" name="txt_LETTERDESC" id="txt_LETTERDESC" class='textbox200' style="width: 300px" />
                    <img src="/js/FVS.gif" name="img_LETTERDESC" id="img_LETTERDESC" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    From
                </td>
                <td>
                    <%=lstFrom%>
                    <img src="/js/FVS.gif" name="img_FROM" id="img_FROM" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    To
                </td>
                <td>
                    <%=lstTo%>
                    <img src="/js/FVS.gif" name="img_TO" id="img_TO" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <br />
                    &nbsp;&nbsp;&nbsp;<u><b>Content</b></u><br />
                    <table cellpadding="3" cellspacing="2">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="border: 1px solid #133e71">
                                <textarea id="lettertext" style="overflow: scroll; width: 530px; border: none" onkeyup="countMe()"
                                    class="textbox200" cols="5" rows="20" name="txt_LETTERTEXT"></textarea>
                            </td>
                            <td style="width: 10px">
                                &nbsp;
                            </td>
                            <td style="border: 1px solid #133e71" bgcolor="beige" valign="top">
                                <b><u>Key</u></b><br />
                                <br />
                                <b>'D*'</b> -- Date Field<br />
                                <b>'I*'</b> -- Number Field<br />
                                <b>'T*'</b> -- Text Field<br />
                                <br />
                                <b>[1]</b> -- Current Balance<br />
                                <b>[2]</b> -- Total Monthly Rent<br />
                                <b>[3]</b> -- Prop. Monthly Rent<br />
                                <br />
                                * Incremental integer
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" nowrap="nowrap">
                    Show preview with Broadland Housing header and footer
                    <input type="checkbox" name="showHF" id="showHF" value="yes" checked="checked" />
                </td>
                <td align="right" colspan="2" nowrap="nowrap">
                    <table width="240" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="95">
                                <input type="button" name="BTN_DATES2" id="BTN_DATES2" value='Preview' class="RSLBUTTON" style='display: block;
                                    width: 70px' onclick='preview()' />
                            </td>
                            <td width="80" align="left">
                                <input type="button" name="BTN_DATES" id="BTN_DATES" value='Reset' class="RSLBUTTON" style='display: block;
                                    width: 70px' onclick='RSLFORM.reset()' />
                            </td>
                            <td width="80" align="left">
                                <input type="button" name="BTN_SAVE" id='BTNSAVE' value='Save' style='display: block;
                                    width: 70px' class="RSLBUTTON" onclick='save_form()' />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" height="5">
                            </td>
                        </tr>
                        <tr>
                            <td width="95">
                                &nbsp;
                            </td>
                            <td width="80" align="left">
                                <input type="button" name="BTN_AMEND" id="BTNAMEND" value='Amend' style='display: block;
                                    width: 70' class="RSLBUTTON" onclick="amend_form()" />
                            </td>
                            <td width="80" align="left">
                                <input type="button" name="BTN_DELETE" id="BTNDELETE" value='Delete' style='display: block;
                                    width: 70' class="RSLBUTTON" onclick="delete_form()" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div id="div1">
        <%=TheFullTable%>
    </div>
    <div id="div3" style="display: none">
        Ziss is ze div threee
    </div>
    <input type="hidden" name="hid_ACTION" id="hid_ACTION" />
    <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" />
    <input type="hidden" name="hid_LETTERID" id="hid_LETTERID" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="LETTERFRAME" id="LETTERFRAME" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
