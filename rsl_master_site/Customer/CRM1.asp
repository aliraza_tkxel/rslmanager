<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box_dummy.asp" -->
<%
	
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=200" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	// id for sql
	Dim customer_id, tenancy_id, property_id
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev, disa_bled
	
	OpenDB()
	customer_id = Request("CustomerID")
	if (customer_id = "" OR NOT isNumeric(customer_id)) then
		Response.Redirect "CRM_CustomerNotFound.asp"
	end if
	
	SQL = "SELECT * FROM C__CUSTOMER WHERE CUSTOMERID = " & customer_id
	Call OpenRs(rsCustomerExists, SQL)
	if (rsCustomerExists.EOF) then
		CloseRs(rsCustomerExists)
		Response.Redirect "CRM_CustomerNotFound.asp"
	end if
	CloseRs(rsCustomerExists)
	
	disa_bled = ""
'	Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
'NOW THAT WE HAVE MORE THAN ONE TENANCY PER CUSTOMER BECAUSE OF HISTORICAL STUFF WE HAVE TO GET THE LATEST TENANCYID
'THIS DONE BY ZANFAR ALI....
	Call OpenRs(rsSet, "SELECT CT.TENANCYID, T.PROPERTYID FROM C_CUSTOMERTENANCY CT LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID LEFT JOIN C_TENANCYCLOSED TC ON TC.TENANCYID = CT.TENANCYID WHERE CUSTOMERID = " & customer_id & " AND TC.TENANCYCLOSEDID IS NULL AND CT.ENDDATE IS NULL")	
	If Not rsSet.EOF Then 
		tenancy_id = rsSet(0)
		property_id = rsSet(1)
	Else
		tenancy_id = -1
		disa_bled = " style='display:none'"
	End If
	CloseRs(rsSet)

	'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
				"FROM	 	F_RENTJOURNAL J " &_
				"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
				"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))"
	Call OpenRs (rsSet, strSQL) 
	ACCOUNT_BALANCE = FormatCurrency(rsSet("AMOUNT"))
	CloseRs(rsSet)
	'END OF GET BALANCE
	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Loading.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	
	var MASTER_TENANCY_NUM
	MASTER_TENANCY_NUM = <%=tenancy_id%>
	var MASTER_OPEN_WORKORDER
	MASTER_OPEN_WORKORDER = ""
	
	// preload image variables -- global
	var open1, closed1, open2, closed2, previous2, open3, closed3, previous3, open4, closed4, previous4, open5
	var closed5, previous5, open6, closed6, previous6, open7, closed7, previous7

	preloadFlag = false;
	function preloadImages2() {
	
		if (document.images) {
			open1 = newImage("Images/1-open.gif");
			closed1 = newImage("Images/1-closed.gif");
			open2 = newImage("Images/2-open.gif");
			closed2 = newImage("Images/2-closed.gif");
			previous2 = newImage("Images/2-previous.gif");
			open3 = newImage("Images/3-open.gif");
			closed3 = newImage("images/3-closed.gif");
			previous3 = newImage("images/3-previous.gif");
			open4 = newImage("images/4-open.gif");
			closed4 = newImage("images/4-closed.gif");
			previous4 = newImage("images/4-previous.gif");
			open5 = newImage("images/5-open.gif");
			closed5 = newImage("images/5-closed.gif");
			previous5 = newImage("images/5-previous.gif");
			open6 = newImage("images/6-open.gif");
			closed6 = newImage("images/6-closed.gif");
			previous6 = newImage("images/6-previous.gif");
			open7 = newImage("images/7-open.gif");
			closed7 = newImage("images/7-closed.gif");
			previous7 = newImage("images/7-previous.gif");
			itemopen = newImage("images/item-1-open.gif");
			itemclosed = newImage("images/item-1-closed.gif");
			newopen = newImage("images/item-2-open.gif");
			newclosed = newImage("images/item-2-previous.gif");
			preloadFlag = true;
			}
	}

	//OPENS THE TENANT STATEMENT
	function OpenStatement(whichOne){
		if (MASTER_TENANCY_NUM == -1) 
			alert("Please select a tenancy before viewing a Statement")
		else
			
			window.open('/Customer/Popups/' + whichOne + '_Statement.asp?customerid=<%=Request("customerid")%>&tenancyid=' + MASTER_TENANCY_NUM, whichOne +'Statement','width=750px,height=500px,scrollbars=yes')	
		
		
		}

	var iFrameArray = new Array("",
								"iInformation",
								"iContact",
								"iTenancy",
								"iHousingBenefit",
								"iSupport",
								"iPayments",
								"iReference",
								"iCustomerJournal",
								"iNewItem",
								"iRentAccount",
								"iRepairJournal",
								"iRepairJournal",
								"iAmendAccount"
								)
	
	var iBlockifNotTenant = new Array ("",
									   "",
									   "",
									   "",
									   "DISABLED",
									   "",
									   "DISABLED",
									   "",
									   "",
									   "",
									   "DISABLED",
									   "DISABLED"
									   )
												

	var MAIN_OPEN_BOTTOM_FRAME = 8
	var MAIN_OPEN_TOP_FRAME = 1	
	// swaps the images on the divs when the user clicks 
	function swap_div(int_which_one, str_where){
		var divid, imgid, upper
		
		if (str_where == 'top'){
			upper = 7; lower = 1;
			STARTLOADER('TOP')
			MAIN_OPEN_TOP_FRAME = int_which_one
			if (MASTER_TENANCY_NUM == -1 && iBlockifNotTenant[int_which_one] == "DISABLED") 
				CRM_TOP_FRAME.location.href = "/customer/iframes/iTopUnavailable.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;			
			else	
				CRM_TOP_FRAME.location.href = "/customer/iframes/" + iFrameArray[int_which_one] + ".asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;			
			}
		else {
			upper = 12;	lower = 8;
			STARTLOADER('BOTTOM')
			MAIN_OPEN_BOTTOM_FRAME = int_which_one
			if (MASTER_TENANCY_NUM == -1 && iBlockifNotTenant[int_which_one] == "DISABLED") 
				CRM_BOTTOM_FRAME.location.href = "/customer/iframes/iBottomUnavailable.asp";			
			else	
				CRM_BOTTOM_FRAME.location.href = "/customer/iframes/" + iFrameArray[int_which_one] + ".asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM+"&CWO="+MASTER_OPEN_WORKORDER;			
			}
		
		imgid = "img" + int_which_one.toString();
		
		// manipulate images
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
		}
		
		// unless last image in row
		if (int_which_one != 13){		
			if (int_which_one != upper)
				document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
			document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
			}
	}

	function synchronize_tabs(int_which_one, str_where){
		var divid, imgid, upper
		if (str_where == 'top'){upper = 7; lower = 1;}
		else {upper = 12;	lower = 8;}
		
		imgid = "img" + int_which_one.toString();
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
			}
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
		
	}
	
	function open_dp(){
		window.open("Popups/pDirectPostings.asp", "display","width=300,height=300,left=100,top=200");
		}
	
	function blank(){
		frm_nature.location.href = "blank.asp";
		}
	
	// receives the url of the page to open plus the required width and the height of the popup
	function update_record(str_redir, wid, hig){
	
		window.open(str_redir, "display","width="+wid+",height="+hig+",left=100,top=200") ;
	
	}
	
	function forecast(){
		window.showModalDialog("popups/forecast_tenancy_end.asp?balance=<%=ACCOUNT_BALANCE%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		//window.open("popups/forecast_tenancy_end.asp?balance=<%=ACCOUNT_BALANCE%>&tenancyid="+MASTER_TENANCY_NUM, "","height=473,width=600");
	}

	
	function post_cash(){
		window.showModalDialog("popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		//window.open("popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px;");
	}

	function work_order(){
		window.showModelessDialog("popups/WorkOrder.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
	}

	function open_pad(){
		window.open("popups/pScratchPad.asp?tenancyid="+MASTER_TENANCY_NUM, "","height=473,width=600");
	}
	
	function refreshitems(){
		parent.frm_crm.location.href = "iFrames/iCustomerJournal.asp?customerid=<%=customer_id%>"
		//parent.swap_div(8, 'bottom')	
	}

	function UpdateMainBalance(){
		BalanceServer.location.href = "/customer/iframes/BalanceServer.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;					
		}
// -->
</SCRIPT>
<!-- End Preload Script -->


<BODY BGCOLOR=#FFFFFF onload="initSwipeMenu(0);preloadImages();" onUnload="macGo()" class='ta' MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post> 
<!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2>
				<IMG NAME="img1" TITLE='Customer Information' SRC="images/1-open.gif" WIDTH=92 HEIGHT=20 BORDER=0 onclick="swap_div(1, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img2" TITLE='Contact Details' SRC="images/2-previous.gif" WIDTH=71 HEIGHT=20 BORDER=0 onclick="swap_div(2, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img3" TITLE='Tenancy Information' SRC="images/3-closed.gif" WIDTH=75 HEIGHT=20 BORDER=0 onclick="swap_div(3, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img4" TITLE='Housing Benefit Details - If Appropriate' SRC="images/4-closed.gif" WIDTH=118 HEIGHT=20 BORDER=0 onclick="swap_div(4, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img5" TITLE='Tenant Support Information' SRC="images/5-closed.gif" WIDTH=132 HEIGHT=20 BORDER=0 onclick="swap_div(5, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img6" TITLE='Payment Details' SRC="images/6-closed.gif" WIDTH=134 HEIGHT=20 BORDER=0 onclick="swap_div(6, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img7" TITLE='References' SRC="images/7-closed.gif" WIDTH=99 HEIGHT=20 BORDER=0 onclick="swap_div(7, 'top')" STYLE='CURSOR:HAND'></TD>
		<TD>
			<IMG SRC="images/spacer.gif" WIDTH=29 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376>
			<IMG SRC="images/spacer.gif" WIDTH=29 HEIGHT=1></TD>
	</TR>
</TABLE>
<DIV ID=TOP_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<IFRAME frameborder=0 NAME=CRM_TOP_FRAME <%=TABLE_DIMS%> SRC="iFrames/iInformation.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>" STYLE="OVERFLOW:hidden"></IFRAME>
</DIV>
<DIV ID=TOP_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:200' <%=TABLE_DIMS%>>
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
    <TR>
      <TD WIDTH=70% HEIGHT=100%> 		
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=0 CLASS="TAB_TABLE">
				<TR><TD width=17px></TD><TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_TOP"></DIV></b></TD></TR>
			</TABLE>
      </TD>
		<TD WIDTH=30% HEIGHT=100% valign=top>
			<%=str_repairs%>
		</TD>
	</TR>		
</TABLE>
</DIV>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img8" SRC="images/8-open.gif" WIDTH=54 HEIGHT=20 BORDER=0  onclick="swap_div(8, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img9" SRC="images/9-previous.gif" WIDTH=82 HEIGHT=20 BORDER=0 onclick="swap_div(9, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img10" SRC="images/10-closed.gif" WIDTH=77 HEIGHT=20 BORDER=0 onclick="swap_div(10, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img11" SRC="images/11-closed.gif" WIDTH=120 HEIGHT=20 BORDER=0 onclick="swap_div(11, 'bottom')" STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img12" SRC="images/12-closed.gif" WIDTH=79 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD align=right>
			
			
        <TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0>
          <TR> 
            <TD width="9%"><img name="cend" src="Images/cforecast.gif" width="27" height="22" title='Forecast final month payment based on projected tenancy end date.' style='cursor:hand' ONCLICK='forecast()' <%=disa_bled%>></TD>
            <TD width="9%"><img name="cpost" src="Images/cpost.gif" width="27" height="22" title='Post cash/chq to this tenancy' style='cursor:hand' ONCLICK='post_cash()' <%=disa_bled%>></TD>
            <TD width="9%"><img name="sbasket" src="/myImages/Repairs/sbasket.gif" title="Create new work order" width="27" height="22" style='cursor:hand' ONCLICK='work_order()' <%=disa_bled%>></TD>
            <TD width="10%"><img name="spad" src="Images/spad.gif" width="27" height="22" style='cursor:hand' title='Open up General/Legal scratch pad data' ONCLICK='open_pad()' <%=disa_bled%>></TD>
            <TD ALIGN=RIGHT width="56%"><B><FONT COLOR='#133e71' style='font-size:14px'>Balance:&nbsp;</FONT></B> 
              <input type=text value='<%=ACCOUNT_BALANCE%>' size=8 READONLY name='TenantBalance' style='BORDER:1PX SOLID black;font-size:14px;color:BLUE;font-weight:bold;text-align:right'>
            </TD>
          </TR>
        </TABLE>
			</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376 colspan=2>
			<IMG SRC="images/spacer.gif" WIDTH=338 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=BOTTOM_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=0 border=0 CLASS="TAB_TABLE" >
	  <TR> 
		<TD><iframe name=CRM_BOTTOM_FRAME src="iFrames/iCustomerJournal1.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=BOTTOM_DIV_LOADER STYLE='DISPLAY:block;OVERFLOW:hidden;width:750;height:210' WIDTH=750 HEIGHT=210>
<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=10 CELLSPACING=10 border=0 CLASS="TAB_TABLE">
    <TR>
		<TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_BOTTOM"></DIV></b></TD>
	</TR>		
</TABLE>
</DIV>

		<input type=hidden name=customerid value=<%=customer_id%>>
		<input type=hidden name=propertyid value=<%=property_id%>>
</form>
<iframe name=BalanceServer style="display:block" src="iFrames/BalanceServer.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>"></iframe>
<!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

