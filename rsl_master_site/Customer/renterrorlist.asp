<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 20
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "renterrorlist.asp"
	MaxRowSpan = 13
	Dim orderBy
		orderBy = "T.TENANCYID DESC"
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
	
	OpenDB()
	Dim CompareDate
	Dim CompareDate2	

	get_newtenants()
	CloseDB()
	
	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
				
		CompareDate = DateAdd("m",-1,Date)
		CompareDate2 = Date		
		if (Request("COMPAREDATE") <> "") then
			CompareDate = Request("COMPAREDATE")
			CompareDate2 = Request("COMPAREDATE2")			
		end if
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""

		strSQL = "SELECT M.ITEMNAME AS ITEMNAME, " & _
		"AMOUNT = CASE WHEN E.DIRECTION <> '-' THEN E.AMOUNT * -1 ELSE E.AMOUNT END, " & _
		"RENT = CASE WHEN E.DIRECTION <> '-' THEN E.RENT * -1 ELSE E.RENT END, " & _
		"SERVICES = CASE WHEN E.DIRECTION <> '-' THEN E.SERVICES * -1 ELSE E.SERVICES END, " & _
		"COUNCILTAX = CASE WHEN E.DIRECTION <> '-' THEN E.COUNCILTAX * -1 ELSE E.COUNCILTAX END, " & _
		"WATERRATES = CASE WHEN E.DIRECTION <> '-' THEN E.WATERRATES * -1 ELSE E.WATERRATES END, " & _
		"INELIGSERV = CASE WHEN E.DIRECTION <> '-' THEN E.INELIGSERV * -1 ELSE E.INELIGSERV END, " & _
		"SUPPORTEDSERVICES = CASE WHEN E.DIRECTION <> '-' THEN E.SUPPORTEDSERVICES * -1 ELSE E.SUPPORTEDSERVICES END, " & _
		"GARAGE = CASE WHEN E.DIRECTION <> '-' THEN E.GARAGE* -1 ELSE E.GARAGE END, " & _
		"TOTALRENT = CASE WHEN E.DIRECTION <> '-' THEN E.TOTALRENT * -1 ELSE E.TOTALRENT END, " & _
		"E.JOURNALID,E.TENANCYID,E.TRANSACTIONDATE,E.PROPERTYID,E.RENTTYPE,E.PROPERTYTYPE, " & _
		"E.ENDPAYMENTDATE,E.STARTPAYMENTDATE,E.ITEMTYPEID,E.PAYMENTTYPEID,E.DIRECTION " & _
		"FROM F_RENTJOURNAL_ERRORS_MONTHLY E " & _ 
			"INNER JOIN F_MISPOSTITEMS M ON E.PAYMENTTYPEID = M.F_PAYMENTTYPEID  " & _
		"WHERE E.TRANSACTIONDATE >= '" & FormatDateTime(CompareDate,1) & "' AND E.TRANSACTIONDATE <= '" & FormatDateTime(CompareDate2,1) & "'"






		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			PrevTenancy = ""
			For intRecord = 1 to rsSet.PageSize
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.


				TenancyID = rsSet("TENANCYID")
				CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
						"FROM C_CUSTOMERTENANCY CT " &_
						"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
						"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
						"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & TenancyID
				Call OpenRs(rsCust, CustSQL)
				strUsers = ""
				CustCount = 0
				while NOT rsCust.EOF 
					if CustCount = 0 then
						strUsers = strUsers & "<A HREF='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
						CustCount = 1
					else
						strUsers = strUsers & ", " & "<A HREF='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
					end if					
					rsCust.moveNext()
				wend
				CloseRs(rsCust)
				SET rsCust = Nothing




				strStart = "<TR><TD align=""left"">" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
				strEnd = 	"<TD align=""left"" nowrap>" & rsSet("ITEMNAME") & "</TD>" &_
							"<TD style=""padding-left=5px;padding-right=5px""  align=""left"">" & FormatDatetime(rsSet("TRANSACTIONDATE")) & "</TD>" &_
							"<TD nowrap style=""padding-left=5px;padding-right=5px"" >" & FormatDatetime(rsSet("STARTPAYMENTDATE")) & "-" & FormatDatetime(rsSet("ENDPAYMENTDATE")) & "</TD>" &_
							"<TD align=""left"">" & FormatCurrency(rsSet("RENT")) & "</TD>" &_
							"<TD align=""left"">" & FormatCurrency(rsSet("SERVICES")) & "</TD>" &_				
							"<TD align=""left"">" & FormatCurrency(rsSet("COUNCILTAX")) & "</TD>" &_
							"<TD align=""left"">" & FormatCurrency(rsSet("WATERRATES")) & "</TD>" &_				
							"<TD align=""left"">" & FormatCurrency(rsSet("INELIGSERV")) & "</TD>" &_
							"<TD align=""left"">" & FormatCurrency(rsSet("SUPPORTEDSERVICES")) & "</TD>" &_
							"<TD align=""left"">" & FormatCurrency(rsSet("GARAGE")) & "</TD>" &_												
					"<TD align=right>" & FormatCurrency(rsSet("AMOUNT")) & "</TD></TR>"

				str_data = str_data & strStart & "<TD width=190>" & strUsers & "</TD>" & strEnd
								
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No new tenancies exist for the specified criteria</TD></TR>" 
			fill_gaps()						
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> New Tenancies</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(CustomerID){
	location.href = "CRM.asp?CustomerID=" + CustomerID
	}

var FormFields = new Array()
FormFields[0] = "txt_DATE|From Date|DATE|Y"
FormFields[1] = "txt_DATE2|To Date|DATE|Y"

function GO(){
	if (!checkForm()) return false;
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + thisForm.txt_DATE.value + "&COMPAREDATE2=" + thisForm.txt_DATE2.value
	}
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->  
<form name = thisForm method=get>
  <TABLE WIDTH=1049 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
		
      
		
      <td width=119 valign="bottom" rowspan="2"><img src="/Customer/Images/tab_renterrorhistory.gif" width="141" height="20"></td>
      <TD align=right width="930">Find postings processed between created between 
        the dates:&nbsp; FROM: 
        <input type=text name="txt_DATE" value="<%=CompareDate%>" class="textbox100" maxlength=10>&nbsp;<img name="img_DATE" src="/js/FVS.gif" width=15 height=15>&nbsp;
        TO: <input type=text name="txt_DATE2" value="<%=CompareDate2%>" class="textbox100" maxlength=10>&nbsp;<img name="img_DATE2" src="/js/FVS.gif" width=15 height=15>&nbsp;
		<input type="button" value=" GO " class="RSLButton" onclick="GO()"></TD>
	</TR>
	<TR>
		
      <TD BGCOLOR=#133E71 width="930"><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
  <TABLE WIDTH=1048 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR valign="top"> 
      <TD CLASS='NO-BORDER' WIDTH=82><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TENANCYID asc")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<B>Tenancy</B>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TENANCYID desc")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=68><B>Customer</B></TD>
	  <TD CLASS='NO-BORDER' WIDTH=65 align="left"><B>Post Type</B></TD>
      <TD CLASS='NO-BORDER' style="padding-left=5px;padding-right=5px"   WIDTH=55 align="left"><B>Process Date</B></TD>
	  <TD CLASS='NO-BORDER' style="padding-left=5px;padding-right=5px"  WIDTH=171 align="left"><B>Correction 
        Period</B></TD>		
      <TD CLASS='NO-BORDER' WIDTH=40 align="left"><B>Rent</B></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=56 align="left"><B>Services</B></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=76 align="left"> <B>Council Tax</B> </TD>
		
      <TD CLASS='NO-BORDER' WIDTH=59 align="left"> <B>Water Rates</B> </TD>
		
      <TD CLASS='NO-BORDER' WIDTH=62 align="left"><B>Ineligible</B></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=67 align="left"><B>Supported</B></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=75 align="left"><B>Garage</B></TD>
		
      <TD CLASS='NO-BORDER' WIDTH=78 align="left"><B>Total</B></TD>						
	</TR>
 	</THEAD>
	<TR STYLE='HEIGHT:3PX'>	
		<TD COLSPAN=13 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
	</TR>
		 <%=str_data%>
</TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>