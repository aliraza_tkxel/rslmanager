<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->   
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="14">&nbsp;</td>
		<td>
		<table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
	 		<tr> 
        	<tr> 
				<td><b>Customers</b></td><td>&nbsp;</td>
			</tr>
		  		
          <td width="35%"><img src="../myImages/HoldingGraphics/customers.gif" width="295" height="283"></td>
          		<td width="65%" valign="top"> 
            		<p><br>
            The Customer Module enables RSL Teams and their partners 
            to manage clients in a dynamic way through the appropriate sharing 
            of client information that facilitates better service delivery and 
            MI reporting. The MI process is fully automated with all required 
            client MI being captured throughout the client relationship. 
            <p>Working alongside all other modules 
                    the Customer Module ensures:</p>
                  <ul>
                    <li class="iagManagerSmallBlk"> Standard approach to collecting 
                      data on customer characteristics and circumstances</li>
                    <li class="iagManagerSmallBlk">Sophisticated customer tracking 
                      and monitoring tools</li>
                    <li class="iagManagerSmallBlk">Real time access to customer 
                      records and rent history</li>
                    <li class="iagManagerSmallBlk">Delivery of quality assured 
                      information </li>
                    <li class="iagManagerSmallBlk">Seamless integration with other 
                      RSL software
                    </li>
                  </ul>
          		</td>
			</tr>
      	</table>
		</td>
	</tr>
	<tr>
		<td width="14">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
