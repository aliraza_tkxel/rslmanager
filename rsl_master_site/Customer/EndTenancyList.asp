<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
    Dim PageName_Export

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	PageName = "EndTenancyList.asp"
    PageName_Export = "EndTenancyList_xls.asp"
	MaxRowSpan = 7

	Dim orderBy
		orderBy = "T.TENANCYID DESC"

	If (Request("CC_Sort") <> "") Then orderBy = Request("CC_Sort")

	Dim CompareDate
	Dim CompareDate2

	Call OpenDB()
	Call get_newtenants()
	Call CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

		CompareDate = DateAdd("m",-1,Date)
		CompareDate2 = Date

		If (Request("COMPAREDATE") <> "") Then
			CompareDate = Request("COMPAREDATE")
			CompareDate2 = Request("COMPAREDATE2")
		End If

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""

		strSQL = "SELECT CUT.CUSTOMERID, T.TENANCYID, reverse(stuff(reverse(COALESCE(P.HOUSENUMBER+' ','') + COALESCE(P.ADDRESS1+',','') + COALESCE(P.ADDRESS2+',','') + COALESCE(P.TOWNCITY+',','')), 1, 1, '')) As CustomerAddress, CTR.DESCRIPTION As Reason, LA.DESCRIPTION AS LOCALAUTHORITY, T.ENDDATE, TRA.DESCRIPTION AS ReasonCategory " &_
        "FROM C_TENANCY T " &_
                 " INNER JOIN (SELECT MIN(CUSTOMERID) AS CUSTOMERID, TENANCYID FROM dbo.C_CUSTOMERTENANCY GROUP BY TENANCYID) CUT ON T.TENANCYID = CUT.TENANCYID " &_
				 " INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
                 " LEFT JOIN PDR_DEVELOPMENT PD ON P.DEVELOPMENTID = PD.DEVELOPMENTID " &_
                 " LEFT JOIN G_LOCALAUTHORITY LA ON PD.LOCALAUTHORITY = LA.LOCALAUTHORITYID " &_
				 " INNER JOIN (SELECT MAX(JOURNALID) AS JOURNALID, TENANCYID FROM C_JOURNAL WHERE ITEMNATUREID = 27 GROUP BY TENANCYID) J ON J.TENANCYID = T.TENANCYID " &_
				 " INNER JOIN C_TERMINATION CT ON CT.JOURNALID = J.JOURNALID AND CT.ITEMSTATUSID = 14 " &_
				 " INNER JOIN C_TERMINATION_REASON CTR ON CTR.REASONID = CT.REASON " &_
                 " LEFT JOIN C_TERMINATION_REASON_ACTIVITY TRA ON CT.REASONCATEGORYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
				 "WHERE T.ENDDATE >= '" & FormatDateTime(CompareDate,1) & "' AND T.ENDDATE <= '" & FormatDateTime(CompareDate2,1) & "' AND T.ENDDATE IS NOT NULL " &_
				 "ORDER BY " & orderBy

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class=""CAPS"">"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			PrevTenancy = ""
			For intRecord = 1 to rsSet.PageSize
				TenancyID = rsSet("TENANCYID")
				strStart = "<tr><td><a href=""CRM.asp?CustomerID=" & rsSet("CUSTOMERID") & """>" & TenancyReference(rsSet("TENANCYID")) & "</a></td>"
				strEnd = "<td>" & rsSet("CustomerAddress") & "</td>" &_
                    "<td>" & rsSet("LOCALAUTHORITY") & "</td>" &_			
					"<td>" & rsSet("ENDDATE") & "</td>" &_
					"<td>" & rsSet("Reason") & "</td>" &_
                    "<td>" & rsSet("ReasonCategory") & "</td></tr>"

				'str_data = str_data & strStart & "<td width=""190"">" & strUsers & "</td>" & strEnd
                str_data = str_data & strStart & strEnd

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
			"<a href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1""><b><font color=""blue"">First</font></b></a> "  &_
			"<a href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & """><b><font color=""blue"">Prev</font></b></a>"  &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
			" <a href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & """><b><font color=""blue"">Next</font></b></a>"  &_ 
			" <a href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & """><b><font color=""blue"">Last</font></b></a>"  &_
			"</td><td align=""right"" width=""150"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71; font-size:11px"">&nbsp;"  &_
			"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" title=""GO"" onclick=""JumpPage()"" style=""font-size:10px; cursor:pointer"">"  &_
			"</td></tr></thead></table></td></tr></tfoot>"
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""" & MaxRowSpan & """ style=""font:12px"" align=""center"">No new tenancies exist for the specified criteria</td></tr>"
			Call fill_gaps()
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<script type=""text/javascript"" language=""JavaScript"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</script>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> New Tenancies</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

    function load_me(CustomerID){
	    location.href = "CRM.asp?CustomerID=" + CustomerID
	    }

    var FormFields = new Array()
        FormFields[0] = "txt_DATE|From Date|DATE|Y"
        FormFields[1] = "txt_DATE2|To Date|DATE|Y"

    function GO(){
	    if (!checkForm()) return false;
	    location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + document.getElementById("txt_DATE").value + "&COMPAREDATE2=" + document.getElementById("txt_DATE2").value
	    }

    function Export(){
	    if (!checkForm()) return false;
	    location.href = "<%=PageName_Export%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + document.getElementById("txt_DATE").value + "&COMPAREDATE2=" + document.getElementById("txt_DATE2").value
	    }

    </script>
    <%=WriteJavaJumpFunction%>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">
                <table>
                    <tr>
                        <td>
                            Find Tenancies that ended between the dates...
                        </td>
                        <td>
                            From:
                        </td>
                        <td>
                            <input type="text" name="txt_DATE" id="txt_DATE" value="<%=CompareDate%>" class="textbox100"
                                maxlength="10" />
                        </td>
                        <td>
                            <img name="img_DATE" id="img_DATE" src="/js/FVS.gif" width="15" height="15" alt="" />
                        </td>
                        <td>
                            To:
                        </td>
                        <td>
                            <input type="text" name="txt_DATE2" id="txt_DATE2" value="<%=CompareDate2%>" class="textbox100"
                                maxlength="10" />
                        </td>
                        <td>
                            <img name="img_DATE2" id="img_DATE2" src="/js/FVS.gif" width="15" height="15" alt="" />
                        </td>
                        <td>
                            <input type="button" value=" GO " title=" GO " class="RSLButton" onclick="GO()" style="cursor: pointer" />
                        </td>
                        <td>
                            <img src="Images/excel.gif" style="cursor: pointer" alt="Excel Report" width="25"
                                height="25" onclick="Export()" border="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="tab_Tenants" title="Tenants" src="Images/tab_ended_tenancies.gif" width="133"
                    height="20" border="0" alt="" />
            </td>
            <td height="20">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133e71">
                <img src="images/spacer.gif" width="617" height="1" alt="" />
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' width="100px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TENANCYID asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>&nbsp;<b>Tenancy</b>&nbsp;<a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TENANCYID desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='NO-BORDER' width="220px">
                    &nbsp;<b>Address</b>&nbsp;
                </td>
                <td class='NO-BORDER' width="200px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("LOCALAUTHORITY asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>&nbsp;<b>Local Authority</b>&nbsp;<a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("LOCALAUTHORITY desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='NO-BORDER' width="120px">
                     <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndDate asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>&nbsp;<b>End Date</b>&nbsp;<a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndDate desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='NO-BORDER' width="180px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("Reason asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>&nbsp;<b>Reason</b>&nbsp;<a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("Reason desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='NO-BORDER' width="110px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ReasonCategory asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>&nbsp;<b>Category</b>&nbsp;<a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ReasonCategory desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="8" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
