<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% 
ByPassSecurityAccess = true
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition","attachment; filename=UniversalCreditReport.xls"

%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
    Dim CompareDate, CompareDate2, searchedText, strSearchedText, strDateClause, orderBy
       
    CompareDate = ""
    CompareDate2 = ""
    strDateClause  = ""
    strSearchedText = ""

	If (Request("COMPAREDATE") <> "") Then
		CompareDate = Request("COMPAREDATE")
		CompareDate2 = Request("COMPAREDATE2")
        strDateClause  = " AND StartDate >= '" & FormatDateTime(CompareDate,1) & "' AND StartDate <= '" & FormatDateTime(CompareDate2,1) & "' AND StartDate IS NOT NULL "
	
	End If

    If Request("search") <> "" Then
        searchedText = Request("search")
	    strSearchedText = " AND ( (P.HOUSENUMBER + ' ' + " &_
                            " P.ADDRESS1 + ' ' + " &_
                            " P.ADDRESS2 + ' ' + " &_
                            " P.TOWNCITY LIKE '%" & searchedText & "%') " &_
                            " OR (C.FirstName + ' ' + " &_
                            " C.LastName LIKE '%" & searchedText & "%') ) "
	End If

	orderBy = "CreationDate_sortCol DESC"
    If (Request("CC_Sort") <> "") Then
        orderBy = Request("CC_Sort")
    End if
        
	Call OpenDB()

	strSQL = "SELECT ISNULL(CONVERT(NVARCHAR, UC.CreationDate, 103) ,'')AS CREATIONDATE, UC.CreationDate as CreationDate_sortCol, " &_
    " C.FirstName + ' '+ C.LastName AS CustomerName , reverse(stuff(reverse(COALESCE(P.HOUSENUMBER+' ','') + COALESCE(P.ADDRESS1+',','') + COALESCE(P.ADDRESS2+',','') + COALESCE(P.TOWNCITY+',','')), 1, 1, '')) As CustomerAddress,  " &_ 
    " ISNULL(CONVERT(NVARCHAR, UC.StartDate, 103), '-') AS StartDate,  UC.PaidDirectlyTo, " &_
	" ISNULL(CONVERT(NVARCHAR, UC.EndDate, 103), '-') AS EndDate, UC.StartRentBalance, ISNULL(CONVERT(NVARCHAR,UC.EndRentBalance),'-') as EndRentBalance , AMOUNT as CurrentRentBalance " &_
    " FROM C_UniversalCredit UC INNER JOIN C_JOURNAL J ON J.JOURNALID = UC.JOURNALID " &_
    " INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
    " INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
    " INNER JOIN (SELECT TENANCYID,sUM(AMOUNT) as AMOUNT " &_
    " FROM F_RENTJOURNAL WHERE (STATUSID NOT IN (1,4) OR PAYMENTTYPE IN (17,18)) " &_
    " GROUP by TENANCYID) as RJ ON RJ.TENANCYID = J.TENANCYID " &_
    " WHERE UC.isCurrent = 1 " & strDateClause & strSearchedText &_
    " ORDER BY " & orderBy
   
	    Call OpenRs(rsSet, strSQL)
        str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			while not rsSet.eof

                if NOT rsSet("EndRentBalance") = "-" then
                    endRentBalance= FormatCurrency(rsSet("EndRentBalance"),2)
                else
                    endRentBalance = rsSet("EndRentBalance")
                end if
                startRentBalance = FormatCurrency(rsSet("StartRentBalance"),2)
                currentRentBalance = FormatCurrency(rsSet("CurrentRentBalance"),2)
                str_data = str_data & "<tbody><tr>" &_
                    "<td width=""100px"">" & rsSet("CREATIONDATE") & "</td>" &_
                    "<td width=""106px"" align=""left"">" & rsSet("CustomerName") & "</td>" &_
                    "<td align=""left"">" & rsSet("CustomerAddress") & "</td>" &_
                    "<td align=""left"">" & rsSet("StartDate") & "</td>" &_
                    "<td align=""left"">" & rsSet("EndDate") & "</td>" &_
                    "<td align=""left"">" & rsSet("PaidDirectlyTo") & "</td>" &_
                    "<td align=""left"">" & startRentBalance & "</td>" &_
                    "<td align=""left"">" & endRentBalance & "</td>" &_
                    "<td width=""111px"" align=""left"">" & currentRentBalance & "</td>"
                str_data = str_data & "</tr></tbody>"
				rsSet.movenext()
				wend

			str_data = str_data & "</tbody>"

        Call CloseRs(rsSet)
		Call CloseDB()
        %>
        <table cellpadding="1" cellspacing="2" border="1">
        <thead>
            <tr>
                <td>
                    <b>Date</b>
                </td>
                <td>
                    <b>Tenant</b>
                </td>
                 <td>
                    <b>Address</b>
                </td>
                <td>
                    <b>Start</b>
                </td>
                <td>
                    <b>End</b>
                </td>
                <td>
                    <b>Paid Directly To</b>
                </td>
                <td>
                    <b>Start Balance</b>
                </td>
                <td>
                    <b>End Balance</b>
                </td>
                 <td>
                    <b>Current Balance</b>
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>