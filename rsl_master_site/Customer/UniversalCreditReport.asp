<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 25

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
    Dim PageName_Export
    Dim CompareDate, CompareDate2, searchedText, strSearchedText, strDateClause, orderBy
    
	PageName = "UniversalCreditReport.asp"
    PageName_Export = "UniversalCreditReport_xls.asp"
	MaxRowSpan = 9
    orderBy = ""
    CompareDate = ""
    CompareDate2 = ""
    strDateClause  = ""
    strSearchedText = ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If


	If (Request("CC_Sort") <> "") Then 
        orderBy = Request("CC_Sort")
    Else
        orderBy = " CreationDate_sortCol DESC "
    End If
 
    If Request.QueryString("search") <> "" Then
        searchedText = Request.QueryString("search")
		strSearchedText = " AND ( (P.HOUSENUMBER + ' ' + " &_
                          " P.ADDRESS1 + ' ' + " &_
                          " P.ADDRESS2 + ' ' + " &_
                          " P.TOWNCITY LIKE '%" & searchedText & "%') " &_
                          " OR (C.FirstName + ' ' + " &_
                          " C.LastName LIKE '%" & searchedText & "%') ) "
	End If

	Call OpenDB()
	Call getRecord_UC()
	Call CloseDB()

	Function getRecord_UC()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

		If (Request("COMPAREDATE") <> "") Then
			CompareDate = Request("COMPAREDATE")
			CompareDate2 = Request("COMPAREDATE2")
            strDateClause  = " AND StartDate >= '" & FormatDateTime(CompareDate,1) & "' AND StartDate <= '" & FormatDateTime(CompareDate2,1) & "' AND StartDate IS NOT NULL "
		End If

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""

		strSQL = " SELECT ISNULL(CONVERT(NVARCHAR, UC.CreationDate, 103) ,'')AS CREATIONDATE, UC.CreationDate as CreationDate_sortCol, " &_
                 " C.FirstName + ' '+ C.LastName AS CustomerName , reverse(stuff(reverse(COALESCE(P.HOUSENUMBER+' ','') + COALESCE(P.ADDRESS1+',','') + COALESCE(P.ADDRESS2+',','') + COALESCE(P.TOWNCITY+',','')), 1, 1, '')) As CustomerAddress,  " &_ 
                 " ISNULL(CONVERT(NVARCHAR, UC.StartDate, 103), '-') AS StartDate, UC.StartDate as StartDate_sortCol, UC.PaidDirectlyTo, " &_
	             " ISNULL(CONVERT(NVARCHAR, UC.EndDate, 103), '-') AS EndDate, UC.EndDate as EndDate_sortCol, UC.StartRentBalance, ISNULL(CONVERT(NVARCHAR,UC.EndRentBalance),'-') as EndRentBalance, UC.EndRentBalance as EndRentBalance_sortCol , AMOUNT as CurrentRentBalance " &_
                 " FROM C_UniversalCredit UC INNER JOIN C_JOURNAL J ON J.JOURNALID = UC.JOURNALID " &_
                 " INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
                 " INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
                 " INNER JOIN (SELECT TENANCYID,sUM(AMOUNT) as AMOUNT " &_
                 " FROM F_RENTJOURNAL WHERE (STATUSID NOT IN (1,4) OR PAYMENTTYPE IN (17,18)) " &_
                 " GROUP by TENANCYID) as RJ ON RJ.TENANCYID = J.TENANCYID " &_
                 " WHERE UC.isCurrent = 1 " & strDateClause & strSearchedText &_
                 " ORDER BY " & orderBy
                
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
        
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class=""CAPS"">"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			PrevTenancy = ""
            
			For intRecord = 1 to rsSet.PageSize
                
                if NOT rsSet("EndRentBalance") = "-" then
                    endRentBalance= FormatCurrency(rsSet("EndRentBalance"),2)
                else
                    endRentBalance = rsSet("EndRentBalance")
                end if
                startRentBalance = FormatCurrency(rsSet("StartRentBalance"),2)
                currentRentBalance = FormatCurrency(rsSet("CurrentRentBalance"),2)
				strStart = "<tr><td>" & rsSet("CREATIONDATE") & "</td>"
				strEnd ="<td>" & rsSet("CustomerName") & "</td>" &_
                        "<td>" & rsSet("CustomerAddress") & "</td>" &_			
                        "<td>" & rsSet("StartDate") & "</td>" &_
                        "<td>" & rsSet("EndDate") & "</td>" &_
                        "<td>" & rsSet("PaidDirectlyTo") & "</td>" &_
                        "<td>" & startRentBalance & "</td>" &_
                        "<td>" & endRentBalance & "</td>" &_
                        "<td>" & currentRentBalance & "</td></tr>"

                str_data = str_data & strStart & strEnd
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()
           
			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
			"<a style=""text-decoration: none"" href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1""><b><font color=""black""><<</font></b></a> "  &_
			"<a style=""text-decoration: none"" href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & """><b><font color=""black""><</font></b></a>"  &_
			" Records " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count & " of " & intRecordCount & "  Page " & intpage & " of " & intPageCount &_
			" <a style=""text-decoration: none"" href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & """><b><font color=""black"">></font></b></a>"  &_ 
			" <a style=""text-decoration: none"" href=""" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & """><b><font color=""black"">>></font></b></a>"  &_
			"</td><td width=""150""><input type=""button"" class=""RSLButtonSmall"" value=""Export to XLS"" title=""Export to XLS"" onclick=""Export()"" style=""font-size:10px; cursor:pointer"">"  &_
			"</td></tr></thead></table></td></tr></tfoot>"
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""" & MaxRowSpan & """ style=""font:12px"" align=""center"">No Universal Credit entries exist for the specified criteria</td></tr>"
			Call fill_gaps()
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Universal Credit Report</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .img_Calander
        {
            vertical-align:bottom;
        }
        #txt_search
        {
            float: right;
            width: 70%;
            box-sizing: border-box;
            border: 1px solid #90908F;
            border-radius: 5px;
            font-size: 12px;
            background-color: white;
            background-image: url('images/search.png');
            background-position: 100% 0px;
            background-repeat: no-repeat;
            padding: 3px 20px 3px 10px;
            color: Gray;
            font-style: italic;
        }
        #txt_search:focus 
        {
            font-style: normal;
        }
        .style1
        {
            width: 266px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

    function load_me(CustomerID){
	    location.href = "CRM.asp?CustomerID=" + CustomerID
	    }

    var FormFields = new Array()
        FormFields[0] = "txt_DATE|From Date|DATE|Y"
        FormFields[1] = "txt_DATE2|To Date|DATE|Y"

    function GO(){
	    if (!checkForm()) return false;
        if (!valdiateDates()) return false;
	    location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + document.getElementById("txt_DATE").value + "&COMPAREDATE2=" + document.getElementById("txt_DATE2").value + "&search=" + document.getElementById("txt_search").value
	    }

    function Export(){
	    if (!checkForm()) return false;
        if (!valdiateDates()) return false;
	    location.href = "<%=PageName_Export%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + document.getElementById("txt_DATE").value + "&COMPAREDATE2=" + document.getElementById("txt_DATE2").value + "&search=" + document.getElementById("txt_search").value
	    }
        
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example is 5000

        function TypingInterval() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(search_onclick, doneTypingInterval);
        }
        function search_onclick() {
            if (!valdiateDates()) return false;
            if (!checkForm()) return false;
            location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREDATE="%>" + document.getElementById("txt_DATE").value + "&COMPAREDATE2=" + document.getElementById("txt_DATE2").value + "&search=" + document.getElementById("txt_search").value
        }

        function valdiateDates() {
            ucStartDate = document.getElementById("txt_DATE").value;
            ucEndDate = document.getElementById("txt_DATE2").value;
	   
            if (ucEndDate != "" && ucStartDate != "") {

                arrSplit = ucStartDate.split("/");
                ucStartDate = new Date(arrSplit[2] , arrSplit[1]-1 , + arrSplit[0]);

                arrSplit = ucEndDate.split("/");
                ucEndDate = new Date(arrSplit[2] , arrSplit[1]-1 , + arrSplit[0]);

                if (ucEndDate < ucStartDate)
                {
                    alert("You have entered an To Date prior to From Date!");
                    return false;
                }
            }
            else if(ucEndDate == "" && ucStartDate == "")
            {
                FormFields[0] = "txt_DATE|From Date|DATE|N"
                FormFields[1] = "txt_DATE2|To Date|DATE|N"
            }
            return true;
          }
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="get" action="">
    <table width="760" border="0" cellpadding="0" cellspacing="0">
       
        <tr>
            <td style="width: 98%; background-color: Black; height: 25px;">
                <span style="color: White;  margin: 4px; font-size: 13px;  font-weight: inherit;">Universal Credit</span>
            </td>
        </tr>
        <br />
         <tr>
            <td colspan="4">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            Start Date
                        </td>
                        <td>
                            From:
                        </td>
                       
                        <td>
                            <input type="text" name="txt_DATE" id="txt_DATE" value="<%=CompareDate%>"
                                maxlength="10" size="29" class="textbox100" style="width: 100px" />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_DATE',110,132,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                            <img class="img_Calander" alt="calender_image" width="18px" height="19px" src="images/calendar_2.png" /></a>
                        </td>
                        <td>
                            <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                                top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img  src="/js/FVS.gif" name="img_DATE" id="img_DATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                            To:
                        </td>
                        <td>
                            <input type="text" name="txt_DATE2" id="txt_DATE2" value="<%=CompareDate2%>"
                                maxlength="10" size="29" class="textbox100" style="width: 100px" />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_DATE2',275,132,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                            <img class="img_Calander" alt="calender_image" width="18px" height="19px" src="images/calendar_2.png" /></a>
                        </td>
                        <td>
                            <div id='Calendar2' style='background-color: white; position: absolute; left: 1px;
                                top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img src="/js/FVS.gif" name="img_DATE2" id="img_DATE2" width="15px" height="15px"
                                border="0" alt="" />
                        </td>



                        <td >
                            <input type="button" value=" GO " title=" GO " class="RSLButton" onclick="GO()" style="cursor: pointer" />
                        </td>
                        <td valign="inherit" class="style1">
                            &emsp;&emsp;&emsp;&emsp;
                            <input id="txt_search" type="text" name="txt_search" value="<%=searchedText%>" placeholder="Quick find" onkeyup="TypingInterval()">
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_search" id="img_search" width="15px" height="15px"
                            border="0" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr>
            <td bgcolor="#133e71">
                <img src="images/spacer.gif" width="617" height="1" alt="" />
            </td>
        </tr>
    </table>
    <table width="760" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="7">
        <thead>
            <tr valign="bottom">
                 <td class='NO-BORDER' width="110px">
                     <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CreationDate_sortCol asc")%>"
                            style="text-decoration: none">
                            <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                     </div>
                     <div style=" position:relative;   display: inline-block; width: 46%;"><b>Date</b></div>
                     <div style="position:relative; display:inline-block; float:right;"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CreationDate_sortCol desc")%>"
                            style="text-decoration: none">
                            <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>
                    
                    </div>
                </td>
                <td class='NO-BORDER' width="140px">
                    <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CustomerName asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                    </div>
                    <div style=" position:relative;   display: inline-block; width: 46%;"><b>Tenant</b></div>
                    <div style="position:relative; display:inline-block; float:right;"><a
                        href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CustomerName desc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>
                    </div>
                </td>
                <td class='NO-BORDER' width="180px">
                    <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CustomerAddress asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                    </div>
                    <div style=" position:relative;   display: inline-block; width: 46%;"><b>Address</b></div>
                    <div style="position:relative; display:inline-block; float:right;"><a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CustomerAddress desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                                height="12" border="0" alt="Sort Descending" /></a>
                    </div>
                </td>
                <td class='NO-BORDER' width="100px">
                    <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("StartDate_sortCol asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                    </div>
                    <div style=" position:relative;   display: inline-block; width: 46%;"><b>Start</b></div>
                    <div style="position:relative; display:inline-block; float:right;"><a
                        href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("StartDate_sortCol desc")%>"
                        style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                        height="12" border="0" alt="Sort Descending" /></a>
                    </div>
                </td>
                <td class='NO-BORDER' width="100px">
                    <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndDate_sortCol asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                    </div>
                    <div style=" position:relative;   display: inline-block; width: 46%;"><b>End</b></div>
                    <div style="position:relative; display:inline-block; float:right;"><a
                        href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndDate_sortCol desc")%>"
                        style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                        height="12" border="0" alt="Sort Descending" /></a>
                    </div>
                </td>
                 <td class='NO-BORDER' width="180px">
                    <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("UC.PaidDirectlyTo asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                    </div>
                    <div style=" position:relative;   display: inline-block; width: 46%;"><b>Paid Directly To</b></div>
                    <div style="position:relative; display:inline-block; float:right;"><a
                        href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("UC.PaidDirectlyTo desc")%>"
                        style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                        height="12" border="0" alt="Sort Descending" /></a>
                    </div>
                </td>
                <td class='NO-BORDER' width="200px">
                        <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("UC.StartRentBalance asc")%>"
                            style="text-decoration: none">
                            <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                        </div>
                        <div style=" position:relative;   display: inline-block; width: 46%;"><b>Start Balance</b></div>
                        <div style="position:relative; display:inline-block; float:right;"><a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("UC.StartRentBalance desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>
                        </div>
                </td>
                <td class='NO-BORDER' width="200px">
                        <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndRentBalance_sortCol asc")%>"
                            style="text-decoration: none">
                            <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                        </div>
                        <div style=" position:relative;   display: inline-block; width: 46%;"><b>End Balance</b></div>
                        <div style="position:relative; display:inline-block; float:right;"><a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("EndRentBalance_sortCol desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>
                        </div>
                </td>
                <td class='NO-BORDER' width="200px">
                        <div style=" position:relative; display:inline-block; float:left; width: 27%;" ><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CurrentRentBalance asc")%>"
                            style="text-decoration: none">
                            <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                        </div>
                        <div style=" position:relative;   display: inline-block; width: 46%;"><b>Current Balance</b></div>
                        <div style="position:relative; display:inline-block; float:right;"><a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("CurrentRentBalance desc")%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>
                        </div>
                </td>
          
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="9" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
