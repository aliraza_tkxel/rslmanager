<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->

<%
Server.ScriptTimeout=350
    Dim CONST_PAGESIZE
    
    if(Request("pagesize")<>"") then
       CONST_PAGESIZE  = Request("pagesize")
    else
        CONST_PAGESIZE = 20    
    end if

	
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim PageName_Export
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal,lstcustomerstatus
	Dim ochecked, cchecked,rqCustomerStatus
	Dim orderBy
	
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		
		'build the scheme and action sql 
		rqCustomerStatus=1
		rqScheme 	= Request("SEL_SCHEME")	' Sceme arrears property belongs to 
		rqAsAtDate 	= Request("txt_ASATDATE") 	' As At Date
		rqPatch =Request("sel_PATCH") 'Patch arrears property belong to
		rqAssettype =Request("sel_AssetType") 'asset type arrears property belong to
		if(rqPatch<>"") then
		    SchemeFilter="P_SCHEME SCH INNER JOIN  PDR_DEVELOPMENT PD ON SCH.DEVELOPMENTID = PD.DEVELOPMENTID WHERE PD.PATCHID=" & rqPatch
		else
		    SchemeFilter="P_SCHEME"    
		end if
		rqCustomerStatus=Request("CustomerStatus")
		
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Build and set filter boxes and dropdowns situated at the top of the page	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Open database and create a dropdown compiled with the housing officers
		OpenDB()
			
		Call BuildSelect(selScheme, "sel_SCHEME", SchemeFilter ,"SCHEMEID, SCHEMENAME ", "SCHEMENAME", "All", rqScheme, NULL, "textbox200","TABINDEX=11")	
		Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", rqPatch, NULL, "textbox200", "TABINDEX=11 onChange=GetSchemes()")	
		Call BuildSelect(lstAssettype, "sel_AssetType", "P_ASSETTYPE WHERE ASSETTYPEID IN(1,2)" ,"ASSETTYPEID, DESCRIPTION ", "DESCRIPTION", "All", rqAssettype, NULL, "textbox200","TABINDEX=11")
				
		CloseDB()
		
		lstcustomerstatus=lstcustomerstatus & "<select class=""textbox200"" id=""sel_customer_status"">"
		
		if(rqCustomerStatus=2) then
		      lstcustomerstatus=lstcustomerstatus &  "<option  value=""2"" selected=""selected"">Former Tenancy</option>"
		      lstcustomerstatus=lstcustomerstatus & "<option  value=""1"" >All</option>"
		      lstcustomerstatus=lstcustomerstatus & "<option  value=""3"">Current Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option value=""4"">Non Tenancy</option>"
        elseif(rqCustomerStatus=3) then
              lstcustomerstatus=lstcustomerstatus & "<option  value=""3"" selected=""selected"">Current Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option  value=""1"" >All</option>"
		      lstcustomerstatus=lstcustomerstatus &  "<option  value=""2"" >Former Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option value=""4"">Non Tenancy</option>"
        elseif(rqCustomerStatus=4) then
              lstcustomerstatus=lstcustomerstatus & "<option value=""4"" selected=""selected"">Non Tenancy</option>"      
              lstcustomerstatus=lstcustomerstatus & "<option  value=""1"" >All</option>"
		      lstcustomerstatus=lstcustomerstatus &  "<option  value=""2"" >Former Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option  value=""3"" >Current Tenancy</option>"
        else
              lstcustomerstatus=lstcustomerstatus & "<option  value=""1"" selected=""selected"" >All</option>"
		      lstcustomerstatus=lstcustomerstatus &  "<option  value=""2"" >Former Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option  value=""3"" >Current Tenancy</option>"
              lstcustomerstatus=lstcustomerstatus & "<option value=""4"" >Non Tenancy</option>" 
        end if  
        
        lstcustomerstatus=lstcustomerstatus & "</select>"
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Building of filter boxes and dropdowns 	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsList_2.asp"
	PageName_Export = "ArrearsList2_xls.asp"
	MaxRowSpan = 9
	
	
	OpenDB()
	Dim CompareValue,Max_CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		
		orderBy = " RENTBALANCE DESC "
		if (Request("CC_Sort") <> "") then 
		    orderBy = Request("CC_Sort")
		    'Response.Write(orderBy)
		end if
		
		' What rent balance value to compare against
		if (Request("COMPAREVALUE") <> "") then
			CompareValue = Request("COMPAREVALUE")
		end if
		
		' What max rent balance value to compare against
		if (Request("MAX_COMPAREVALUE") <> "") then
			Max_CompareValue = Request("MAX_COMPAREVALUE")
		end if
	
	
		' The Scheme that the property applies to ( Also known as Development )
		if rqScheme <> "" then	
			Scheme_sql = " and dev.developmentid = " & rqScheme	
		else	
			Scheme_sql = "" 
		End If
		
		' The patch that the property applies to 
		if rqPatch <> "" then	
			patch_sql = " and ep.patchid = " & rqPatch	
		else	
			patch_sql = "" 
		End If	
		
		' The asserttype that the property applies to 
		if rqAssettype <> "" then	
			assettype_sql = " and p.assettype = " & rqAssettype	
		else	
			assettype_sql = "" 
		End If	
			
		' The Action that was last taken ( Arrears actions )
		''if rqAction <> "" then
		''	Action_sql = " and VJ.actionid = " & rqAction	
		''else 
		''	Action_sql = "" 
		''End If
	
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			'	TotalSQL =		"	SELECT  " &_
			'				"		sum((ISNULL(A.AMOUNT,0) - ISNULL(ABS(B.AMOUNT),0))) AS TOTALARREARS    " &_
			'				"	FROM C_TENANCY T    " &_
			'				"		inner JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
			'				"				FROM F_RENTJOURNAL I_J    " &_
			'				"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
			'				"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
			'				"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
			'				"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  "  & AsAtDate &_ 
			'				"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID    " &_
			'				"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID   " &_
			'				"				FROM F_RENTJOURNAL I_J    " &_
			'				"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID   " &_
			'				"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID   " &_
			'				"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
			'				"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) " & AsAtDate &_
			'				"					AND PAYMENTTYPE = 1    " &_
			'				"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
			'				"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " &_
			'				"		LEFT JOIN (SELECT MAX(JOURNALID) AS JOURNALID, CUSTOMERID FROM C_JOURNAL where ITEMNATUREID = 10 GROUP BY CUSTOMERID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID   " &_
			'				"	WHERE ISNULL(A.AMOUNT,0)> 0 AND T.ENDDATE IS NULL " &_
			'				"		  AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)   "
			'rw TotalSQL & "<BR><BR>"
			'Call OpenRs(rsTotal, TotalSQL)
			'TheArrearsTotal = FormatCurrency(rsTotal("TOTALARREARS"),2)
			'CloseRs(rsTotal)
					
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Get Total Of arrears
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			Dim strSQL, rsSet, intRecord 
			
			intRecord = 0
			str_data = ""
			TotalArrears = 0
								
			'SQL = " execute ARREARS_GET_CUSTOMER_HB_RB_SLB ' " & rqCustomerStatus &  "','" & CompareValue & "','" & rqAsAtDate  & "', '" & Scheme_sql & "','" & OrderBy &  "','" & Patch_Sql & "','" & Max_CompareValue & "','" & assettype_sql & "'"
	        Set rsSet = Server.CreateObject("ADODB.Recordset")
            Set cmd = Server.CreateObject("ADODB.Command")
            cmd.ActiveConnection = RSL_CONNECTION_STRING
            cmd.CommandText = "ARREARS_GET_CUSTOMER_HB_RB_SLB"
            cmd.CommandType = 4
            cmd.CommandTimeout = 0
            'cmd.Prepared = true
            cmd.Parameters.Append cmd.CreateParameter("@CUSTOMERSTATUS", 3, 1,4,rqCustomerStatus)
		    cmd.Parameters.Append cmd.CreateParameter("@RENTBALANCEFROM", 200,1,200,CompareValue)
		    cmd.Parameters.Append cmd.CreateParameter("@ASATDATE", 200, 1,50,rqAsAtDate)
		    cmd.Parameters.Append cmd.CreateParameter("@SCHEME_SQL", 200, 1,200,Scheme_sql)
		    cmd.Parameters.Append cmd.CreateParameter("@ORDERBY", 200, 1,200,OrderBy)
		    cmd.Parameters.Append cmd.CreateParameter("@PATCH_SQL", 200, 1,200,Patch_Sql)
		    cmd.Parameters.Append cmd.CreateParameter("@RENTBALANCETO", 200, 1,200,Max_CompareValue)
		    cmd.Parameters.Append cmd.CreateParameter("@ASSETTYPE_SQL", 200, 1,200,assettype_sql)
		  
            
            rsSet.CursorLocation = 3
            rsSet.CursorType = 3      
            rsSet.open cmd , , 3,1
   
  			'set rsSet = Server.CreateObject("ADODB.Recordset")
			'rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			'rsSet.Source = SQL
			'rsSet.CursorType = 3
			'rsSet.CursorLocation = 3
			'rsSet.Open()

			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
						
			
			intRecordCount = rsSet.RecordCount 
			

			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
						
						Rent_Balance=  rsSet("RENTBALANCE") 
						Ant_HB = rsSet("ESTHB")
						Adv_HB = rsSet("ADVHB")
						Net = rsSet("NETARREARS") 
                        Sale_Ledger_Balance=rsSet("SALESLEDGERBALANCE") 
                        Total=rsSet("TOTAL")
						'Balance = rsSet("GROSSARREARS")
						
						Rent_BalanceTotal=rsSet("RENTBALANCETOTAL")
						Ant_HBTotal = rsSet("ESTTOTAL")
						Adv_HBTotal = rsSet("ADVTOTAL")
						NetTotal = rsSet("NETTotal") 
						Sale_Ledger_BalanceTotal=rsSet("SALESLEDGERBALANCETOTAL")
						TotalTotal=rsSet("TOTALTOTAL")
						'BalanceTotal = rsSet("GROSSARREARSTotal")
						
						' Here we break up the array which is used to have multiple customers for link		
						if rsSet("CUSTOMERID") <> "" AND rsSet("TENANCYID")<>"" then 
								
								if Instr(rsSet("CUSTOMERID"),",")<>0 then
								strCustomerid = Split(rsSet("CUSTOMERID"), ",")	
								
								if rsSet("FULLNAME") <> "" then 
										strCustomerName = Split(rsSet("FULLNAME"), ",")	
								end if 
								
								iold = 0
								For i = 0 to Ubound(strCustomerid)
									if i > iold then customerNameStr  = CustomerNameStr & " , " end if
									'CustomerNameStr  = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='Load("& rsSet("CUSTOMERTYPE") & "," & rsSet("TENANCYID") & ")'>" & strCustomerName(i) & "</font> "
									CustomerNameStr  = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='load_me("& strCustomerid(i) & ")'>" & strCustomerName(i) & "</font> "
									iold = i
								Next								
								  
								  else
								      CustomerNameStr  = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='load_me("& rsSet("CUSTOMERID") & ")'>" & rsSet("FULLNAME") & "</font> "    
								  end if    								
							
						else 
								'CustomerNameStr = CustomerNameStr & "<font color=blue style='cursor:hand' onClick='Load("& rsSet("CUSTOMERTYPE") & "," & rsSet("TENANCYID") & ")'>" & rsSet("FULLNAME")& "</font> "
								CustomerNameStr = CustomerNameStr & "<font color=blue>" & rsSet("FULLNAME")& "</font> "
						end if
						
                        

					
						
						
						strStart =  "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
					
						strEnd =	"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_	
									"<TD align=right>" & fORMATcURRENCY(Rent_Balance,2) & "</TD>" &_			
									"<TD align=right>" & fORMATcURRENCY(Ant_HB,2) & "</TD>" &_												
									"<TD align=right>" & FormatCurrency(Adv_HB,2) & "</TD>" &_
									"<TD align=right>" & FormatCurrency(Net,2) & "</TD>"	
									
									if(rsSet("TENANCYID")<>"") then
									    strEnd = strEnd &  "<TD align=right>" & "<font color=blue style='cursor:hand' onClick='Load("& rsSet("CUSTOMERTYPE") & "," & rsSet("TENANCYID") & ")'>" & FormatCurrency(Sale_Ledger_Balance,2) & "</font> " & "</TD>"
									else
									    strEnd = strEnd & "<TD align=right>" &  "<font color=blue style='cursor:hand' onClick='Load("& rsSet("CUSTOMERTYPE") & "," & rsSet("CUSTOMERID") & ")'>" & FormatCurrency(Sale_Ledger_Balance,2) & "</font> " & "</TD>"
									end if   
									
									strEnd = strEnd & "<TD align=right>" & FormatCurrency(Total,2) & "</TD></TR>"			
		
						 str_data = str_data & strStart & "<TD width=190 >" & CustomerNameStr & "</TD>" & strEnd
											
						count = count + 1
						single_totalarrears = 0
						Balance = 0
						Net = 0
						Ant_HB = 0
						Adv_HB = 0
						hbcustomer = 0
						CustomerNameStr = ""
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				
						strTotals =  "<TR bgcolor='#99CC99'><TD></TD>"  &_
									"<TD></TD>" &_
									"<TD><B>Totals</B></TD>" &_	
									"<TD style='font-size:9px ' align=right><B>" & fORMATcURRENCY(Rent_BalanceTotal,2) & "</B></TD>" &_			
									"<TD style='font-size:9px ' align=right><B>" & fORMATcURRENCY(Ant_HBTotal,2) & "</B></TD>" &_												
									"<TD style='font-size:9px ' ALIGN=RIGHT><B>" & FormatCurrency(Adv_HBTotal,2) & "</B></TD>" &_
									"<TD style='font-size:9px ' align=right><B>" & FormatCurrency(NetTotal,2) & "</B></TD>"&_
									"<TD style='font-size:9px ' align=right><B>" & FormatCurrency(Sale_Ledger_BalanceTotal,2) & "</B></TD>"&_
									"<TD style='font-size:9px ' align=right><B>" & FormatCurrency(TotalTotal,2) & "</B></TD></TR>"
		
				str_data = str_data & strTotals & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=50></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page size:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='resize_page()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing

		End function
	
		Function WriteJavaJumpFunction()

			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {color: #FF0000}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function load_me(CustomerID){

		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

   function Load(TYPE, ID){
	parent.location.href = "../Finance/SalesInvoice/SalesAccount.asp?ACCID=" + ID + "&ACCTYPE=" + TYPE
	}

	var FormFields = new Array()
	//Makes a mandatory field
	//FormFields[0] = "txt_from|Compare Value|CURRENCY|Y"
	//FormFields[1] = "txt_ASATDATE|Date - As At|DATE|N"

	function GO(){

		if (!checkForm()) return false;
		//if (thisForm.txt_from.value < 0 ){
		//	alert("The Min. rent balance value must not a negative number.");
		//	return false;
		//	}
		location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value + "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value + "&sel_AssetType=" + thisForm.sel_AssetType.value
		}
		
	function Export()
	{
		//alert("<%=PageName_Export%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value + "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value);
		location.href = "<%=PageName_export%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value + "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value + "&sel_AssetType=" + thisForm.sel_AssetType.value
	}	

	/////////////////////////////////
	//	Printing and Xls Below
	/////////////////////////////////
	
	function PrintMe(){
		alert("When prompted please save this file to disk rather than open it directly in the browser")
		window.open("PrintMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value+ "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
		
		}
	function ExportMe(){
	
		window.open("ArrearsList_xls.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value+ "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
		
		}	
	function XLSMe(){
		//alert("unavailable at this time")
		//return false
		window.open("XLSMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value);
		}
	function clear_data(textbox)
	{
	    if(textbox==1) 
	    {
	        thisForm.txt_from.value="";
	        thisForm.txt_from.style.textAlign = "right";
    	}
	    else
	    {
	        thisForm.txt_to.value="";
	        thisForm.txt_to.style.textAlign = "right";
	    }   
	}
	
	function GetSchemes(){
	thisForm.sel_SCHEME.disabled = true
	thisForm.target = "ArrearList_Server"
	thisForm.action = "Serverside/Get_Schemes.asp"
	thisForm.submit()
	}
	
	function resize_page(){
	// thisForm.pagesize.value=thisForm.QuickJumpPage.value;
	// thisForm.submit();   
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_from.value + "&CustomerStatus=" + thisForm.sel_customer_status.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value + "&sel_PATCH=" + thisForm.sel_PATCH.value + "&MAX_COMPAREVALUE=" + thisForm.txt_to.value + "&pagesize=" + thisForm.QuickJumpPage.value
		
	}
	
// -->
</SCRIPT>
<%'=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=post>
  <table border=0 width=750 cellpadding=2 cellspacing=2 height=45PX style='BORDER:1PX SOLID #133E71;padding-right:0px;padding-bottom:10px;' bgcolor=beige>
    <tr> 
      
      <td NOWRAP width="26%"><b>Patch: </b></td>
       <td NOWRAP width="26%"><b>Scheme: </b> </td>
      <td NOWRAP rowspan=4>
            <div style="border:dotted thin black; width:256; height:60px; margin-right:10px;margin-left:15px;padding-top:5px;padding-left:5px;margin-top:10px;">
                <span style="font-weight:bold; padding-top:10px;">Search Rent Balance between:</span><br /><br />
                �<input type=text id="txt_from" value="<%=CompareValue%>" class="textbox" maxlength=10 style='width:100px;text-align:right'>
                <img name="img_from" src="/js/FVS.gif" width=15 height=15>
                and
                �<input type=text id="txt_to" value="<%=Max_CompareValue%>" class="textbox" maxlength=10 style='width:100px;text-align:right'>
                 <img name="img_to" src="/js/FVS.gif" width=15 height=15>
            </div>
      </td>
      
    </tr>
    <tr> 
      
      <td NOWRAP width="26%" valign="top"><%=lstPatch%> </td> 
      <td NOWRAP width="26%" valign="top"><%=SelScheme%> </td>
    
      
      
    </tr>
    <tr> 
      
      <td NOWRAP width="22%"><b>Customer Status: </b>  </td> 
      <td NOWRAP width="22%"><b>Asset Type</td>
      
    </tr>
    <tr>
 
      <td NOWRAP width="22%" valign="top"><%=lstcustomerstatus%></td>
      <td NOWRAP width="22%" valign="top"><%=lstAssettype%></td>
      
    </tr>
    <tr>
        
        <td colspan="4">
            <div style=" white-space:nowrap; text-align:right;padding-right:20px;">
                <b>As At</b>
                <input type="text" name="txt_ASATDATE" value="<%=rqAsAtDate%>" class="textbox" maxlength="10" style='width:130px;text-align:right;display:inline'/>
                <img name="img_ASATDATE" src="/js/FVS.gif" width="7" height="7"/> 
                <input type="button" value=" GO " class="RSLButton" onclick="GO()" name="button" style="width:50px;"/>
			    <input type="button" value=" Export " class="RSLButton" onclick="Export()" name="button" style="width:50px;"/>
			</div>
		</td>	
    </tr>
    <tr><td style="height:10px;">&nbsp;</td></tr>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=3 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=672 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
      <TR>
        <TD CLASS='NO-BORDER' WIDTH=70PX>&nbsp;<B>Tenancy</B></TD>
        <TD CLASS='NO-BORDER' WIDTH=170>&nbsp;<B>Customer</B>&nbsp;</TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX align="center"><b>Last Payment</b></TD>
         <TD CLASS='NO-BORDER' WIDTH=100PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("RENTBALANCE ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Rent Balance<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("RENTBALANCE DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div>
        </TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ESTHB ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
          <b>Ant HB<br> 
        </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ESTHB DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a> </TD>
        <TD CLASS='NO-BORDER' WIDTH=72PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ADVHB ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Adv HB<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ADVHB DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div></TD>
        <TD CLASS='NO-BORDER' WIDTH=100PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("NETARREARS ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Net Rent<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("NETARREARS DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div>
        </TD>
        <TD CLASS='NO-BORDER' WIDTH=100PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("SALESLEDGERBALANCE ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Sales Ledger Balance<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("SALESLEDGERBALANCE DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div>
        </TD>
         <TD CLASS='NO-BORDER' WIDTH=73PX><div align="center"><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("TOTAL ASC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<br>
            <b>Total<br>
          </b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("TOTAL DESC")%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>&nbsp;</div>
        </TD>
        </TR>
    </THEAD>
    <TR STYLE='HEIGHT:3PX'>
      <TD COLSPAN=11 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%>
  </TABLE>
  <input type="hidden" name="pagesize" value="<%=CONST_PAGESIZE%>" />
</form>
<p>&nbsp;</p>
<p>
  <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
 </p>
 <iframe name="ArrearList_Server" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
