<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Pay by Card</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
	 <style type="text/css">
        body
        {
            background-image: none;
            margin: 10px 10px 0px 10px;
        }
    </style>
	
</head>
<body>

<%   

 On Error Resume Next
 
        Dim objXmlHttp
        Dim strHTML
		Dim objNode
		Dim url
		Dim tenancyId
		Dim customerId
		Dim staffId
		Dim requestUrl
		Dim resolveTimeout 
        Dim connectTimeout
        Dim sendTimeout
        Dim receiveTimeout

        ' seconds * milliseconds in a minute = total milliseconds
        resolveTimeout = 120 * 1000 ' infinite is default
        connectTimeout = 180 * 1000 ' 60 sec is default 
        sendTimeout = 30 * 1000 ' 30 sec is default
        receiveTimeout = 30 * 1000 ' 30 sec is default
        	
		tenancyId = Request.QueryString("tenancyId")
		customerId = Request.QueryString("customerId")
		staffId = Request.QueryString("staffId")
		
		requestUrl = "https://" & Request.ServerVariables("HTTP_HOST") & "/PaythruWebLocal/Paythru.svc/geturl?tenancyId=" & tenancyId & "&customerId=" & customerId & "&applet=staff&employeeId=" & staffId
		
		'Response.Write requestUrl
		
		Set objXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP")
        objXmlHttp.setTimeouts resolveTimeout, connectTimeout, sendTimeout, receiveTimeout
        objXmlHttp.open "GET", requestUrl, False
        objXmlHttp.send

        if Err.number = 0 and objXmlHttp.status = 200 then
          strHTML = objXmlHttp.responseText
        else
          strHTML = ""
        end if

        Set objXmlHttp = Nothing
		
		Set objXML = CreateObject("Microsoft.XMLDOM")
		objXML.loadXML(strHTML)
		
		Set objNode = objXML.SelectSingleNode("/string")
		url = objNode.text

		Set objXML = Nothing
		Set objNode = Nothing
					
		Response.Redirect url
		
If Err.Number <> 0 Then

   strNumber = Err.Number
   strSource = Server.HTMLEncode(Err.Source)
   strPage = Request.ServerVariables("SCRIPT_NAME")
   strDesc = Err.Description
   strCode = Server.HTMLEncode(Err.Source)
   If strCode = "" then strCode = "No code available"
   strLine = ""
   strASPDesc = Err.ASPDescription
    
   'You get the entire context of the page that had the error.
   'Review the server variables to see if you would want to store more information.
   strRemoteAddr = Request.ServerVariables("REMOTE_ADDR")
   strRemoteHost = Request.ServerVariables("REMOTE_HOST")
   strLocalAddr = Request.ServerVariables("LOCAL_ADDR")
   IagType = "RSL <font color=#133E71>MANAGER</font>"	

	UserName = ""
	UserPass = ""
	UserEmail = ""
	set rsUserDetails = Server.CreateObject("ADODB.Recordset")
	rsUserDetails.ActiveConnection = RSL_CONNECTION_STRING
	rsUserDetails.Source = "SELECT * from AC_LOGINS WHERE EMPLOYEEID = " & Session("UserID")
	rsUserDetails.CursorType = 0
	rsUserDetails.CursorLocation = 2
	rsUserDetails.LockType = 3
	rsUserDetails.Open()
	rsUserDetails_numRows = 0
	if (NOT rsUserDetails.EOF) then
		UserName = rsUserDetails("LOGIN")
		UserPass = rsUserDetails("PASSWORD")
	end if
	rsUserDetails.close()
	Set rsUserDetails = Nothing

	Dim MailToUser, ErrorType
	TheIP = Request.ServerVariables("REMOTE_ADDR")
	ErrorType = " puny developer "

	Dim LNames 
	'LNames = Array("girly", "stupid", "mad", "abnormal", "gay", "idiotic", "great", "fantastic", "excellent", "dumb", "dangerous", "serious", "severe", "chaotic", "mega", "evil", "good", "fabulous", "whopping", "stupendous", "horrendous", "gigantic", "humongous", "little", "barbaric", "f**k**g", "happy", "limp", "dry cough type", "drunken", "superb")
	LNames = Array("CAMPIOLI CAMPIOLI OLE OLE OLE", "CHAMPIONS OF EUROPE", "ISTANBUL", "European Cup Number 5", "wimbledon r boss", "franny jeffers", "once a blue.....", "bitter blue", "everwho?",  "camp toffeemen", "no eyebrows", "champions league yer avin a laugh error", "7-0")			
	'upperbound = 30
	upperbound = 12
	lowerbound = 0
	Randomize
	RandError = LNames(Int((upperbound - lowerbound + 1) * Rnd + lowerbound)) & " error "

	MyPos = Instr(1,CStr(TheIP), "84.9.221.",1) '194.129.129.

	MailToUser = "enterprisedev@broadlandgroup.org"

	if (isNull(MyPos) OR MyPos = 0) then
		ErrorType = "Real Error "
		TheSYSTEM = "RSLMANAGER (BROADLANDS)"	
	else
		ErrorType = RandError
		TheSYSTEM = "RSLMANAGER"	
	end if

	Dim iMsg
	Dim iConf
	Dim Flds
	Dim HTML
	Dim strSmartHost

	Const cdoSendUsingPort = 2
	StrSmartHost = "localhost" //must be one word or will not send email

	set iMsg = CreateObject("CDO.Message")
	set iConf = CreateObject("CDO.Configuration")

	Set Flds = iConf.Fields

	' set the CDOSYS configuration fields to use port 25 on the SMTP server

	
	 Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
	Flds.Update
	

		HTML = ""
		HTML = "<html><head><title>RSL Manager Error</title>" 
		HTML = HTML & "<style><!--TD{border-top:1px dotted silver} .EmailFont {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-style: normal;line-height: normal;font-weight: normal;color: #000000;text-decoration: none;}--></STYLE>"
		HTML = HTML & "</HEAD><body bgcolor='#ffffff' CELLSPACING=0 CELLPADDING=0 BORDER=0 HEIGHT=100% BORDERCOLOR=#FFFFFF>" 
		HTML = HTML & "<div width='100%' style='background-color=#FFFFFF;padding:10px;text-align:right;border:1px solid black'><img src=""https://crm.broadlandhousinggroup.org/myimages/rslmanager_logo.gif""></div><br>"
		HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
		HTML = HTML &  "<tr><td colspan=2 style='font-size:14px;border-top:none'><b><u>Main Error Details</u></b></td></tr>"	
		HTML = HTML &  "<tr><td width='160'><font color=blue>System:</font></td><td>"& IagType &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>User Name & ID:</font></td><td>" & Session("FIRSTNAME") & " " & Session("LASTNAME") & ", ["& Session("USERID") &"]</td></tr>"		
		HTML = HTML &  "<tr><td><font color=blue>User Login Name:</font></td><td>"& UserName &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>User Email:</font></td><td>"& UserEmail &"</td></tr>"			
		HTML = HTML &  "<tr><td><font color=blue>Date/Time:</font></td><td>"& now & "</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Error Number:</font></td><td>"& strNumber &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Source:</font></td><td>"& strSource &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>File:</font></td><td>"& strPage &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Querystring:</font></td><td>"& request.QueryString  &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>POST Data:</font></td><td>"& request.Form &"</td></tr>"
		HTML = HTML &  "<tr valign='top'><td ><font color=blue>Description:</font></td><td>" & strDesc & ". " & strASPDesc &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Code:</font></td><td>"& strcode &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Line:</font></td><td>"& strLine &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Remote Address:</font></td><td>"& strRemoteAddr &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Remote Host:</font></td><td>"& strRemoteHost &"</td></tr>"
		HTML = HTML &  "<tr><td><font color=blue>Local Address:</font></td><td>"& strLocalAddr &"</td></tr>"
		HTML = HTML &  "</table>"
		HTML = HTML & "<BR>"
		HTML = HTML &  "<table width='100%' cellspacing='0' cellpadding='3' border='0' style='border-collapse:collapse' class='EmailFont'>"
		HTML = HTML &  "<tr><td colspan=2 style='font-size:14px;border-top:none'><b><u>Extended Server Variable Information</u></b></td></tr>"		
		HTML = HTML &  "<tr><td width=160><b>Server Variable Name</b></td><td><b>Server Variable Value</b></td></tr>"		
		for each x in Request.ServerVariables
		  HTML = HTML &  "<tr><td><font color=blue>" & x & ":</font></td><td>" & Request.ServerVariables(x) & "</td></tr>"
		next
		HTML = HTML &  "</table>"	
		HTML = HTML &  "</body></html>"

	'rw html

	' apply the settings to the message
	With iMsg
	Set .Configuration = iConf
	.To = MailToUser
	.From = "do-not-reply@crm.broadlandhousinggroup.org"
	.Subject = "(" & TheSYSTEM & ") " & ErrorType
	.HTMLBody = HTML
	.Send
	End With

	' cleanup of variables
	Set iMsg = Nothing
	Set iConf = Nothing
	Set Flds = Nothing


	   'Your basic ADO stuff.  This is generic code.  I would use a stored procedure to do this on SQL Server.
	   set rs = Server.CreateObject("ADODB.Recordset")
	   set EConn = Server.CreateObject("ADODB.Connection")
	   
	   'SQL Server Connection, make sure you put in your information!
	   EConn.ConnectionString = RSL_CONNECTION_STRING
	   EConn.open
	   rs.Open "G_ERRORMESSAGES", EConn, 2, 3, &H0002
	  
	   'Adding Record
	   rs.AddNew
		'The datetime is set on the backend as a default value.
		rs("er_number") = strNumber
		rs("er_source") = strSource
		rs("er_page") = strPage
		rs("er_desc") = strDesc + ". " + strASPDesc
		rs("LUMP_CODE") = HTML
		rs("er_code") = strCode
		rs("er_line") = strLine
		rs("er_remote_addr") = strRemoteAddr
		rs("er_remote_host") = strRemotehost
		rs("er_local_addr") = strLocalAddr
	   rs.Update
   
   %>
<br>
    <table width="80%" align="center" cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse">
	  <tr>
      <td colspan=2 align=center>An error has occured, if this problem persists 
      please contact RSL Support on <a href="mailto:rslsupport@broadlandhousing.org">rslsupport@broadlandhousing.org</a></td>
      </tr>
      <tr> 
        <td width="200">Error Number:</td>
        <td><%=strNumber%></td>
      </tr>
      <tr> 
        <td width="200">Source:</td>
        <td><%=strSource%></td>
      </tr>
      <tr> 
        <td width="200">File:</td>
        <td><%=strPage%></td>
      </tr>
      <tr valign="top"> 
        <td width="200">Description:</td>
        <td><%=strDesc + ". " + strASPDesc%></td>
      </tr>
      <tr> 
        <td width="200">Code:</td>
        <td><%=strcode%></td>
      </tr>
      <tr> 
        <td width="200">Line:</td>
        <td><%=strLine%></td>
      </tr>
      <tr> 
        <td width="200">Remote Address:</td>
        <td><%=strRemoteAddr%></td>
      </tr>
      <tr> 
        <td width="200">Remote Host:</td>
        <td><%=strRemoteHost%></td>
      </tr>
      <tr> 
        <td width="200">Local Address:</td>
        <td><%=strLocalAddr%></td>
      </tr>

    </table>
<%  
   'Kill them their objects
   set rs = nothing
   set EConn = nothing
   Response.End

End If
				
%>

</body>
</html>
