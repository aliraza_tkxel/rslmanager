<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=200" 'declare table sizes as a constant for all tables
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager -- > Invalid Customer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="initSwipeMenu(0);preloadImages();" onUnload="macGo()" class='ta' MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TR>
  <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772>
    <TR>
      
  <TD> 
    <TR>
      <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772>
        <TR>
          <TD> 
            <!--#include virtual="Includes/Tops/BodyTop.asp" -->
<!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2>
				<IMG NAME="img1" TITLE='Customer Information' SRC="images/1-open.gif" WIDTH=92 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img2" TITLE='Contact Details' SRC="images/2-previous.gif" WIDTH=71 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img3" TITLE='Tenancy Information' SRC="images/3-closed.gif" WIDTH=75 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img4" TITLE='Housing Benefit Details - If Appropriate' SRC="images/4-closed.gif" WIDTH=118 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img5" TITLE='Tenant Support Information' SRC="images/5-closed.gif" WIDTH=132 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img6" TITLE='Payment Details' SRC="images/6-closed.gif" WIDTH=134 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2>
				<IMG NAME="img7" TITLE='References' SRC="images/7-closed.gif" WIDTH=99 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD>
			<IMG SRC="images/spacer.gif" WIDTH=29 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376>
			<IMG SRC="images/spacer.gif" WIDTH=29 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=TOP_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<IFRAME NAME=CRM_TOP_FRAME <%=TABLE_DIMS%> SRC="iFrames/iTopDisabled.asp" STYLE="OVERFLOW:hidden" frameborder=0></IFRAME>
</DIV>
<DIV ID=TOP_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:200' <%=TABLE_DIMS%>>
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
    <TR>
      <TD WIDTH=70% HEIGHT=100%> 		
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=0 CLASS="TAB_TABLE">
				<TR><TD width=17px></TD><TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_TOP"></DIV></b></TD></TR>
			</TABLE>
      </TD>
		<TD WIDTH=30% HEIGHT=100% valign=top>
		</TD>
	</TR>		
</TABLE>
</DIV>

	<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img8" SRC="images/8-open.gif" WIDTH=54 HEIGHT=20 BORDER=0  STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img9" SRC="images/9-previous.gif" WIDTH=82 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img10" SRC="images/10-closed.gif" WIDTH=77 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img11" SRC="images/11-closed.gif" WIDTH=120 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD ROWSPAN=2 valign=bottom>
				<IMG NAME="img12" SRC="images/12-closed.gif" WIDTH=79 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD align=right>
			
			<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0><TR>
				<TD ALIGN=RIGHT>
					<INPUT TYPE='BUTTON' name='btn_CASHPOSTING' disabled title='Post cash/chq to this tenancy' style='background-color:beige' CLASS='RSLBUTTON' VALUE='Cash Posting'>
				</TD>
				<TD ALIGN=RIGHT><B><FONT COLOR='#133e71' style='font-size:14px'>Balance:&nbsp;</FONT></B>
					<input type=text value='0.00' size=8 READONLY name='TenantBalance' style='BORDER:1PX SOLID black;font-size:14px;color:red;font-weight:bold;text-align:right'>
				</TD></TR>
			</TABLE>
			</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376 colspan=2>
			<IMG SRC="images/spacer.gif" WIDTH=338 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=BOTTOM_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=0 border=0 CLASS="TAB_TABLE" >
	  <TR> 
		<TD><iframe name=CRM_BOTTOM_FRAME src="iFrames/iBottomDisabled.asp" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=BOTTOM_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:210' WIDTH=750 HEIGHT=210>
<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=10 CELLSPACING=10 border=0 CLASS="TAB_TABLE">
    <TR>
		<TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_BOTTOM"></DIV></b></TD>
	</TR>		
</TABLE>
</DIV>
<!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

