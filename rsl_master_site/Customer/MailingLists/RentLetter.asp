<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	CONST BROADLAND = 2
	CONST TENANT = 1

	Dim tenancy_id, lett_id, FullAddress, str_letter, StringFullName,build_letter_SIG
	Dim arrInserts, textFieldCounter, formFieldsArray, signature, d_cnt, i_cnt, t_cnt
	Dim intTo, intFrom, str_recipient_address, str_return_address, str_Dear, str_Footer,AmendCount
	Dim RentEffective,	Rent, SupportServices ,	InelServ , OtherServices, CouncilTax, WaterRates, GarageSpace
	Dim t_Rent, t_SupportServices ,	t_InelServ , t_OtherServices, t_CouncilTax, t_WaterRates, t_GarageSpace
	Dim LetterBody
	Dim SchemeID
	SchemeID = Request("SchemeID")

	formFieldsArray = ""
	LetterBody = ""
	OpenDB()
		' DETERMINE WHICH RECIPIENT ADDRESS TO USE


		str_recipient_address = get_tenant_address()

	
	Function get_tenant_address()	

		DEVELOPMENTID = REQUEST("DEVELOPMENTID")
		if not DEVELOPMENTID <> "" then DEVELOPMENTID = 0 end if
		
		SQL = "SELECT 	FIN.*,C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, " &_
				"		C.FIRSTNAME, C.LASTNAME, A.HOUSENUMBER, A.HOUSENUMBER, " &_
				"		A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY, " &_
				"		TR.RENTEFFECTIVE, TR.RENT AS T_RENT, TR.SERVICES AS T_SERVICES, TR.COUNCILTAX AS T_COUNCILTAX, TR.WATERRATES AS T_WATERRATES, TR.INELIGSERV AS T_INELIGSERV, TR.SUPPORTEDSERVICES AS T_SUPPORTEDSERVICES, "&_
				"		FIN.RENT, FIN.SERVICES, FIN.COUNCILTAX, FIN.WATERRATES, FIN.INELIGSERV, FIN.SUPPORTEDSERVICES " &_
				"FROM 	C_TENANCY T " &_
				"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
				"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID " &_
				"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"		INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID "&_
				"WHERE  CT.ENDDATE IS NULL AND P.DEVELOPMENTID = " & DEVELOPMENTID
		rw sql
		Call OpenRS(rsCust, SQL)	
		if rsCust.EOF then
		CloseRS(rsCust)
		SQL = "SELECT 	C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, " &_
				"		C.FIRSTNAME, C.LASTNAME, A.HOUSENUMBER, A.HOUSENUMBER, " &_
				"		A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY " &_
				"		TR.RENTEFFECTIVE, TR.RENT AS T_RENT, TR.SERVICES AS T_SERVICES, TR.COUNCILTAX AS T_COUNCILTAX, TR.WATERRATES AS T_WATERRATES, TR.INELIGSERV AS T_INELIGSERV, TR.SUPPORTEDSERVICES AS T_SUPPORTEDSERVICES, "&_
				"		FIN.RENT, FIN.SERVICES, FIN.COUNCILTAX, FIN.WATERRATES, FIN.INELIGSERV, FIN.SUPPORTEDSERVICES " &_
				"FROM 	C_TENANCY T " &_
				"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
				"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID " &_
				"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"		INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID "&_
				"WHERE  CT.ENDDATE IS NULL AND A.ISDEFAULT = 1 "
				
		Call OpenRS(rsCust, SQL)
		End If
		count = 1
		while NOT rsCust.EOF
			
			RentEffective	=	rsCust("RentEffective")
			Rent 			= 	rsCust("Rent")
			SupportServices = 	rsCust("SUPPORTEDSERVICES")
			InelServ		=	rsCust("INELIGSERV")
			OtherServices	=	rsCust("SERVICES")
			CouncilTax		=	rsCust("COUNCILTAX")
			WaterRates		=	rsCust("WATERRATES")
			GarageSpace		=	rsCust("SERVICES")
			
			T_Rent 			= 	rsCust("T_Rent")
			T_SupportServices = 	rsCust("T_SUPPORTEDSERVICES")
			T_InelServ		=	rsCust("T_INELIGSERV")
			T_OtherServices	=	rsCust("T_SERVICES")
			T_CouncilTax		=	rsCust("T_COUNCILTAX")
			T_WaterRates		=	rsCust("T_WATERRATES")
			T_GarageSpace		=	rsCust("T_SERVICES")			
			
			StringName = ""
			Title = rsCust("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
				ShortName = ShortName & Title & " "
			end if
			FirstName = rsCust("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName =rsCust("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
				ShortName = ShortName & LastName
			end if
			
			if (count = 1) then
				StringFullName = StringName
				str_Dear = "Dear " & ShortName
				AddressNames = ShortName
				count = 2
			else
				StringFullName = StringFullName & " and " & StringName
				str_Dear = str_Dear & " and " & StringName
				AddressNames = AddressNames & " and " & StringName
			end if
			
			LetterBodyBuild()
			rsCust.moveNext
			
		wend
		if (count = 2) then
			rsCust.moveFirst()
			housenumber = rsCust("housenumber")
			if (housenumber = "" or isNull(housenumber)) then
				FirstLineOfAddress = rsCust("Address1")
			else
				FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
			end if
	
			RemainingAddressString = ""
			AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE" )
			for i=0 to Ubound(AddressArray)
				temp = rsCust(AddressArray(i))
				if (temp <> "" and not isNull(temp)) then
					RemainingAddressString = RemainingAddressString &  "<BR>" & temp '"<TR style='font:10PT'><TD nowrap style='font:10PT'>" & temp & "</TD></TR>"
				end if
			next
			tenancyref = rsCust("TENANCYID")
		end if
		
		get_tenant_address ="<TR style='height:65px;font:10PT ARIAL'><TD nowrap style='font:10PT ARIAL'>"& formatdatetime(date,1) & "<BR><BR><BR>" & AddressNames & "" &_
					"<BR>" & FirstLineOfAddress & RemainingAddressString & "</TD></TR>" 
					
		
	End Function


	Function LetterBodyBuild()
	
	'Address Info
	LetterBody = LetterBody &  "<table width=100% border=0 style='height:24cm'>"
    LetterBody = LetterBody &  "<TBODY ID='LETTERDIV'><tr><td align=right valign=top style='font:12PT ARIAL' height='39'><TABLE border=0 id='return_address' style='display:block' >" & str_return_address & "</TABLE></td>"
    LetterBody = LetterBody &  "</tr><tr><td nowrap valign=top><table><tr><td nowrap style='border:1px solid #133E71;font:12pt ARIAL'>Tenancy Ref: <b>" & TenancyReference(tenancy_id) & "</b></td></tr>" & str_recipient_address 
    LetterBody = LetterBody &  "</table></td></tr><tr><td>&nbsp;</td></tr><tr><td> </td></tr>"
   	' Dear Line
    LetterBody = LetterBody &  "<tr><td style='font:10PT ARIAL' >" & str_Dear & "</td></tr>"
	'explanation text
    LetterBody = LetterBody & "<tr style='height:10px'><td > </td></tr><tr id='LetterContent1' style='display:block'><td style='font:12PT ARIAL' valign='top' class='RSLBlack><div id=letterText>"
    LetterBody = LetterBody & "<span class='style1'>RE-ASSESSMENT OF RENT UNDER ASSURED TENANCIES</span><p class='RSLBlack style2'> The rent on your property has been re-assessed as from<strong> <em>" & RentEffective & "</em></strong>. The table below shows the breakdown of your existing rent and your new rent.</p>"
    ' rentbreakdown block   
	LetterBody = LetterBody & " <table width='625' border='0' cellspacing='0' cellpadding='0'>"
    LetterBody = LetterBody & " <tr><td width='159' class='style2'>&nbsp;</td>"
    LetterBody = LetterBody & "    <td width='10' class='style2'><div align='right'></div></td>"
    LetterBody = LetterBody & "    <td width='125' align='right' class='style2'><div align='right'><strong>Existing Rent </strong></div></td>"
    LetterBody = LetterBody & "    <td width='10' align='right' class='style2'><div align='right'></div></td>"
    LetterBody = LetterBody & "    <td width='125' align='right' class='style2'><div align='right'><strong>New Rent wef </strong>01/04/2005</div></td></tr>"
    LetterBody = LetterBody & " <tr><td valign='top' class='style2'><table width='100%'  border='0' cellspacing='0' cellpadding='0'><tr><td><span class='style2'>Rent</span></td></tr>"
    LetterBody = LetterBody & "<tr><td><span class='style2'>Support Services </span></td></tr>"
    LetterBody = LetterBody & " <tr><td><span class='style2'>Ineligible Services </span></td></tr>"
    LetterBody = LetterBody & "  <tr><td><span class='style2'>Other Services </span></td></tr>"
    LetterBody = LetterBody &         " <tr><td><span class='style2'>Council Tax </span></td></tr>"
    LetterBody = LetterBody &         " <tr><td><span class='style2'>Water Rates </span></td></tr>"
    LetterBody = LetterBody &         " <tr><td><span class='style2'>Garage/Car Space </span></td></tr></table></td>"
    LetterBody = LetterBody &       "<td width='10' class='style2'>&nbsp;</td>"
    LetterBody = LetterBody &       " <td valign='top' class='style2'><table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
    ' Current rent table
	LetterBody = LetterBody &   "<tr><td align='right' class='style4'>" &FormatCurrency(Rent,2)& "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(SupportServices,2)& "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(InelServ,2) & "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(OtherServices,2) & "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(CouncilTax,2) & "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(WaterRates,2) & "</td></tr>"
    LetterBody = LetterBody &    " <tr><td align='right' class='style4'>" & FormatCurrency(GarageSpace,2) & "</td></tr></table></td>"
    ' Target rent table
    LetterBody = LetterBody &	 "<td width='10' class='style2'>&nbsp;</td><td valign='top' class='style2'><table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
    LetterBody = LetterBody &	 " <tr><td align='right' class='style4'>" &FormatCurrency(t_Rent,2) & "</td></tr>"
    LetterBody = LetterBody &	 "<tr><td align='right' class='style4'>" &FormatCurrency(t_SupportServices,2) & "</td></tr>"
    LetterBody = LetterBody &	 "<tr><td align='right' class='style4'>" &FormatCurrency(t_InelServ,2) & "</td></tr>"
    LetterBody = LetterBody &	 "<tr><td align='right' class='style4'>" &FormatCurrency(t_OtherServices,2) & "</td> </tr>"
    LetterBody = LetterBody &	 " <tr><td align='right' class='style4'>" &FormatCurrency(t_CouncilTax,2) & "</td></tr>"
    LetterBody = LetterBody &	 " <tr><td align='right' class='style4'>" &FormatCurrency(t_WaterRates,2) & "</td></tr>"
    LetterBody = LetterBody &	 "<tr><td align='right' class='style4'>" & FormatCurrency(GarageSpace,2) & "</td></tr></table></td></tr></table>"
	' rest of the letter
    LetterBody = LetterBody &	 "    <p class='style2'> <span class='style3'>The following tells you what you need to do to make sure that you pay the new rent. </span></p> "
    LetterBody = LetterBody &	 "    <h1 class='RSLBlack style2'><strong> Customers Paying by Payment Card </strong></h1> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'>If you currently pay by a Rent Payment Card please adjust your payment to reflect the amount of new rent you need to pay to Broadland Housing Association. </p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'><strong>Customers Paying by Direct Debit </strong></p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'>If you currently pay by Direct Debit, we will automatically adjust your payment to reflect the amount of new rent you need to pay to Broadland Housing Association. A notification of a change in the direct debit is enclosed. </p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'><strong>Customers Paying by Standing Order</strong></p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'>If you currently pay by Standing Order please contact you bank to instruct them to change your payments to reflect the amount of new rent you need to pay to Broadland Housing Association </p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'><strong>Customers who receive Housing Benefit </strong></p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'>If you claim Housing Benefit for all or part of your rent then we have already notified your Local Authority of your new rent. They should now adjust your Housing Benefit but in the meantime please continue to pay any difference between Housing Benefit and your existing rent. </p> "
    LetterBody = LetterBody &	 "    <p class='style2'><span class='style3'>Once Housing Benefit is adjusted in line with the new rent, we will contact you again with the difference between the Housing Benefit and the new rent which will be the amount you will then need to pay. If your new housing benefit claim is insufficient to cover the rent increase in full then you will also need to pay any built up arrears &ndash; if this happens we will agree a payment plan with you.</span> <br> "
    LetterBody = LetterBody &	 "      <span class='style3'><br> "
	LetterBody = LetterBody &	 " If the Local Authority asks for proof of your new rent then please contact your Housing Officer at Broadland Housing Association.</span></p> "
    LetterBody = LetterBody &	 "    <p class='RSLBlack style2'> If you have any concerns or questions regarding this letter then please do not hesitate to contact us.</p> "
    LetterBody = LetterBody &	 " </div></td></tr><tr><td> </td></tr><tr style='height:60px'><td class='RSLBlack style2'>&nbsp;</td></tr> "
    LetterBody = LetterBody &	 " <tr style='height:60px'><td class='RSLBlack style2'><span class='style6'>Yours sincerely</span></td></tr> "
    LetterBody = LetterBody &	 "<tr style='height:60px'> "
    LetterBody = LetterBody &	 "  <td class='RSLBlack style2'><span class='style6'>Jenny Mayne </span></td> "
    LetterBody = LetterBody &	 " </tr> "
	LetterBody = LetterBody &	 "<tr> "
	LetterBody = LetterBody &	 "  <td align=left id='footer' style='visiblity:hidden'><img src='../Images/Jenny_mayne.gif' width='113' height='56'></td> "
	LetterBody = LetterBody &	 "  </tr> "
	LetterBody = LetterBody &	 "<tr> "
	LetterBody = LetterBody &	 "    <td align=right id='footer' style='visiblity:hidden'>&nbsp; </td> "
    LetterBody = LetterBody &	 "</tr> "
    LetterBody = LetterBody &	 "</TBODY>  "
 	LetterBody = LetterBody &	 " </table> "
rw LetterBody
	End Function
	
	
	CloseDB()

%>
<HTML>
<HEAD>
<TITLE>Customer Info:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<style type="text/css">
<!--
.style1 {
	font-size: 10pt;
	font-weight: bold;
}
.style2 {font-size: 12px}
.style3 {font-style: normal; line-height: normal; font-weight: normal; color: #000000; text-decoration: none; font-family: Verdana, Arial, Helvetica, sans-serif;}
.style4 {font-style: normal; line-height: normal; font-weight: normal; color: #000000; text-decoration: none; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }
.style6 {font-size: 12}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>
	
	// USED TO ENSURE THAT PAGE IS FORMATTED ONLY PRIOR TO FIRST PRINT
	var print_count = 0
	var int_from = <%=intFrom%>
	
	function format_page(){
		
		var numinputs = RSLFORM.hid_NUMINPUTS.value;
		print_count ++;
		if (print_count == 1)
			for (i = 0 ; i < numinputs ; i++){
				document.getElementById("img_INPUT" + i + "").style.display = "none";
				document.getElementById("SPAN" + i + "").innerHTML = eval("RSLFORM.txt_INPUT" + i + ".value");
				}
	}

	function PrintMe(){
		
		// IF FIRST PRINT THEN FORMAT PAGE	
		if (print_count == 0){
			if (!checkForm()) return false
			answer = window.confirm("Please check the letter is correct as you will not be able to change them at a later date.")
			if (!answer) return false
					format_page();
					hide_borders();
					save_letter();
		}
		//alert("1")
		// HIDE BROADLAND LOGO AND FOOTER DUE TO HEADED PAPER
		if (int_from == 2){
		document.getElementById("return_address").style.display = "none";		
		document.getElementById("footer").style.display = "none";
			}
		
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("CloseButton").style.visibility = "hidden";	
		document.getElementById("AmendContent").style.visibility = "hidden";

		//alert("2")
		print()
		window.close()

		document.getElementById("PrintButton").style.visibility = "visible";
		document.getElementById("CloseButton").style.visibility = "visible";
		document.getElementById("AmendContent").style.visibility = "visible";
		document.getElementById("CloseButton").style.border = "1px solid";
		document.getElementById("PrintButton").style.border = "1px solid";
		document.getElementById("AmendContent").style.border = "1px solid";		
					
		// HIDE BROADLAND LOGO AND FOOTER DUE TO HEADED PAPER
		if (int_from == 2){
			document.getElementById("return_address").style.display = "block";			
			document.getElementById("footer").style.display = "block";
			}		


	}	
	// SAVE LETTER STRING AND ASSOCIATE WITH THIS LEVEL OF ARREARS PROCESS
	function save_letter(){
		
		opener.RSLFORM.hid_LETTERTEXT.value = document.getElementById("LETTERDIV").innerHTML;
		//opener.RSLFORM.hid_SIG.value = document.getElementById("SIGDIV").innerHTML;
	
	}
	
	var FormFields = new Array()
	<%=formFieldsArray%>

	function hide_borders(){
	
		var coll = document.all.tags("INPUT");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	coll[i].style.border = "none"
	}				
	
	function AmendMe(amendtype){

	if (amendtype == "showamend"){
	document.getElementById("LetterContent1").style.display = "none";
	document.getElementById("LetterContent2").style.display = "block";
	document.getElementById("LetterContent3").style.display = "none";
	document.getElementById("LetterContent4").style.display = "block";
	document.getElementById("AmendContent").style.display = "none";
	document.getElementById("PrintButton").disabled = true;	
	RSLFORM.target = "frm_letter_srv"
	RSLFORM.hid_ACTION.value = "LOAD"
	RSLFORM.action = "../serverside/amend_letter_svr.asp"
	RSLFORM.submit();
	}
	else if (amendtype == "cancel"){
	document.getElementById("LetterContent1").style.display = "block";
	document.getElementById("LetterContent2").style.display = "none";
	document.getElementById("LetterContent3").style.display = "block";
	document.getElementById("LetterContent4").style.display = "none";
	document.getElementById("AmendContent").style.display = "block";
	document.getElementById("PrintButton").disabled = false;		
	}
	else {
	RSLFORM.target = "_self"
	RSLFORM.hid_AmendCount.value = "2"
	RSLFORM.hid_ACTION.value = "LOADNEWLETTER"
	RSLFORM.action = "arrears_letter.asp"
	RSLFORM.submit();
	//document.getElementById("LetterContent1").style.display = "block";
	//document.getElementById("letterText").innerText = RSLFORM.txt_LETTERTEXT.value;
	//document.getElementById("LetterContent2").style.display = "none";
	//document.getElementById("AmendContent").style.display = "block";
	}
	}
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=0 CLASS='TA'>
<iframe  src="/secureframe.asp" name=frm_letter_srv width=400px height=400px style='display:none'></iframe>
<%=LetterBody%> 
</BODY>
</HTML>

