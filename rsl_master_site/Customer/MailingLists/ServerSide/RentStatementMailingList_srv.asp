<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim DevelopemntID, DevString, LetterIs_SQL
	Dim PrintAction,FndTenancy,allTenanciesCheckBoxes
	Dim DDANAMString, DDAnomaly,DDCompare, DD_JOIN
				
	PrintAction = Request("PrintAction")
	DevelopemntID = Request("DevelopmentID")
	quicktenancyfind = Request("quicktenancyfind")
	'DDAnomaly	= Request("DDAnomaly")

	If DevelopemntID <> "" then
	FndCntr = " AND p.developmentid = " & DevelopemntID 
	DevString = "&developmentid=" & DevelopemntID
	End If
	
	'ANOMOLY LIST NEGATOR
	'If DDAnomaly = "ShowDD" then
		'DDCompare = " AND DDPAY.REGULARPAYMENT <> FIN.TOTALRENT "
		'DD_JOIN   =	"	INNER JOIN ( SELECT DDSCHEDULEID, PAYMENTDATE, TENANCYID, REGULARPAYMENT " &_ 
'					"		  		  FROM F_DDSCHEDULE WHERE SUSPEND NOT IN (1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID "  & DDCompare  & " "
		'DDANAMString = "&DDAnomaly=" & DDAnomaly
	'ELSE
		'DDCompare = " AND DDPAY.REGULARPAYMENT = FIN.TOTALRENT "	
		DD_JOIN   =	" LEFT JOIN ( SELECT DDSCHEDULEID, PAYMENTDATE, TENANCYID, REGULARPAYMENT " &_ 
					"		  		  FROM F_DDSCHEDULE WHERE SUSPEND NOT IN (1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID "  & DDCompare  & " "

	'End If
	rw quicktenancyfind
	if quicktenancyfind <> "" then
	FndTenancy = " AND T.TENANCYID = " & quicktenancyfind
	end if
	

	select case PrintAction
		case "FirstPrint"
				LetterIs_SQL = " AND RL.STATEMENTID IS NULL " 
				DevString = DevString & "&PrintAction=" & PrintAction
				DisableButton = ""
		case "RePrint"
				LetterIs_SQL = " AND YEAR(RL.DATESTAMP) = YEAR(GETDATE()) AND  MONTH(RL.DATESTAMP) = MONTH(GETDATE()) AND RL.STATEMENTID IS NOT NULL "
				DevString = DevString & "&PrintAction=" & PrintAction
				DisableButton = ""				
	end select
		
	selected = Request("selected") // get selected checkboxes into an array
	response.write selected
	sel_split = Split(selected,",")
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		'Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
								" <TR BGCOLOR=#133e71 STYLE='COLOR:WHITE'>" &_
								" <TD WIDTH=130><FONT STYLE='COLOR:WHITE'><B> Tenancy Number</B></FONT></TD>" &_
								" <TD WIDTH=300><FONT STYLE='COLOR:WHITE'><B> Tenant</B></FONT></TD>" &_
								" <TD WIDTH=300><FONT STYLE='COLOR:WHITE'><B> Property</B></FONT></TD>" &_
								" <TD><FONT STYLE='COLOR:WHITE'></FONT></TD>" &_
								" </TR>" &_
								" <TR><TD HEIGHT='100%' colspan='5'></TD></TR>" 

		
		cnt = 0
			
		' Main SQL that retrieves all the repairs that are awaiting approval
		
	  strSQL = "SELECT CNG.I AS TENANCYID, CNG.LIST AS JOINTNAME, P.PROPERTYID, DDPAY.DDSCHEDULEID " &_
				"FROM C_CUSTOMER_NAMES_GROUPED_VIEW CNG " &_
				"	INNER JOIN C_TENANCY T ON T.TENANCYID = CNG.I " &_
				"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"	LEFT JOIN C_RENTSTATEMENT RL ON RL.TENANCYID = T.TENANCYID AND YEAR(RL.DATESTAMP) = YEAR(GETDATE()) AND MONTH(RL.DATESTAMP) = MONTH(GETDATE()) " &_
				"	INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID  " & DD_JOIN &_
				" WHERE T.TENANCYTYPE in (1,8,10,13,4,18,20) "&_
				"   AND T.TENANCYID IN( "&_
                "                       SELECT TENANCYID "&_
                "                       FROM dbo.C__CUSTOMER "&_
                "                           INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C__CUSTOMER.CUSTOMERID = dbo.C_CUSTOMERTENANCY.CUSTOMERID "&_ 
                "                       WHERE RENTSTATEMENTTYPE=1 "&_
                "                      ) "&_
				" AND T.ENDDATE IS NULL " & FndCntr & LetterIs_SQL & FndTenancy & " " &_
				"  ORDER BY P.PROPERTYID, P.HOUSENUMBER, P.ADDRESS1 ASC " 
		rw strsql
		if mypage = 0 then mypage = 1 end if
		pagesize = 15
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then
						
			// determine if select box has been previously selected
			isselected = ""
			
			theTenancy = Rs("TENANCYID")
			
			For each key in sel_split
				response.write "<BR> KEY : " & key & " id : " & theTenancy
				
				If theTenancy = clng(key) Then
					isselected = " checked "
				End If
			Next
	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			Propertyid =  Rs("Propertyid")
										
			'START APPENDING EACH ROW TO THE TABLE
			if Rs("TENANCYID") <> "" then
										
				str_data = str_data & 	"<TR style='cursor:hand' >" &_
										"<TD  >" & Rs("TENANCYID") & "</TD>" &_
										"<TD  >" & Rs("JOINTNAME") & "</TD>" &_
										"<TD  ><A href='/Portfolio/PRM.asp?PropertyID=" & Rs("PROPERTYID") & "'>" & Rs("PROPERTYID") & "</a></TD>" &_
										"<TD><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & Rs("TENANCYID") & "' value='" & Rs("TENANCYID") & "' onclick='do_sum("& Rs("TENANCYID") &")'></TD>" &_
										"</TR>"	
				if allTenanciesCheckBoxes 	<> "" then
					allTenanciesCheckBoxes = allTenanciesCheckBoxes & "," & Rs("TENANCYID")
				else
					allTenanciesCheckBoxes = Rs("TENANCYID")
				End if
			
			End If
			Rs.movenext()
			End If
			
		Next
		
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data &	"<TR><TD COLSPAN=4 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
									"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & 	"<TR style='height:'8px'><TD></TD></TR>" &_
									"<TR style='display:none'><TD COLSPAN=4 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
									"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=4 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE='Print Statements' CLASS='rslbutton' ONCLICK='process()' " & DisableButton & ">" &_
								"</TD></TR></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=4 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementMailingList_srv.asp?page=" & orderBy & "&txt_FROM=" & fromdate &  DevString & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementMailingList_srv.asp?page=" & prevpage&  DevString & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementMailingList_srv.asp?page=" & nextpage & DevString & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementMailingList_srv.asp?page=" & numpages &  DevString & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/RentStatementMailingList_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & DevString &"&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr></table>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=4 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	rw allTenanciesCheckBoxes
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.RSLFORM.TickAll.checked = false;
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	parent.RSLFORM.hid_tenancylist.value = "<%=allTenanciesCheckBoxes%>"
	}
</script>
<body onLoad="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>