<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim WhatScheme
	
	WhatScheme = Request("Quickfind")
	response.write WhatScheme
	If WhatScheme <> "" then
	FndCntr = " WHERE D.SCHEMENAME LIKE '%" & WhatScheme & "%' "
	End If
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		'Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
								" <TR BGCOLOR=#133e71 STYLE='COLOR:WHITE'>" &_
								" <TD WIDTH=540><FONT STYLE='COLOR:WHITE'><B> Scheme Name</B></FONT></TD>" &_
								" <TD WIDTH=70><FONT STYLE='COLOR:WHITE'><B> </B></FONT></TD>" &_
								" <TD WIDTH=35><FONT STYLE='COLOR:WHITE'><B> </B></FONT></TD>" &_
								" <TD WIDTH=70><FONT STYLE='COLOR:WHITE'><B> </B></FONT></TD>" &_
								" <TD><FONT STYLE='COLOR:WHITE'></FONT></TD>" &_
								" </TR>" &_
								" <TR><TD HEIGHT='100%' colspan='5'></TD></TR>" 

		
		cnt = 0
			
		' Main SQL that retrieves all the repairs that are awaiting approval
				
	  FirstPrintCount = "LEFT JOIN (SELECT ISNULL(COUNT(*),0) AS PRINTCOUNT, P.DEVELOPMENTID " &_
						"FROM C_CUSTOMER_NAMES_GROUPED_VIEW CNG " &_
						"	INNER JOIN C_TENANCY T ON T.TENANCYID = CNG.I " &_
						"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
						"	LEFT JOIN C_RENTSTATEMENT RL ON RL.TENANCYID = T.TENANCYID AND YEAR(RL.DATESTAMP) = YEAR(GETDATE()) AND MONTH(RL.DATESTAMP) = MONTH(GETDATE()) " &_
						" WHERE T.TENANCYTYPE in (1,8,10,13,4,18,20,30) "&_  
						"   AND T.TENANCYID IN( "&_
						"                       SELECT TENANCYID "&_
                        "                       FROM dbo.C__CUSTOMER "&_
	                    "                           INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C__CUSTOMER.CUSTOMERID = dbo.C_CUSTOMERTENANCY.CUSTOMERID "&_ 
                        "                       WHERE RENTSTATEMENTTYPE=1 "&_
						"                      ) "&_
						" AND RL.STATEMENTID IS NULL AND  T.ENDDATE IS NULL GROUP BY  P.DEVELOPMENTID ) fIRSTPRINT ON fIRSTPRINT.DEVELOPMENTID = D.DEVELOPMENTID "
						
	  RePrintCount = "LEFT JOIN (SELECT ISNULL(COUNT(*),0) AS RECOUNT, P.DEVELOPMENTID " &_
						"FROM C_CUSTOMER_NAMES_GROUPED_VIEW CNG " &_
						"	INNER JOIN C_TENANCY T ON T.TENANCYID = CNG.I " &_
						"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
						"	LEFT JOIN C_RENTSTATEMENT RL ON RL.TENANCYID = T.TENANCYID " &_
						" WHERE T.TENANCYTYPE in (1,8,10,13,4,18,20,30) "&_
						"   AND T.TENANCYID IN( "&_
						"                       SELECT TENANCYID "&_
                        "                       FROM dbo.C__CUSTOMER "&_
	                    "                           INNER JOIN dbo.C_CUSTOMERTENANCY ON dbo.C__CUSTOMER.CUSTOMERID = dbo.C_CUSTOMERTENANCY.CUSTOMERID "&_ 
                        "                       WHERE RENTSTATEMENTTYPE=1 "&_
						"                      ) "&_
						" AND YEAR(RL.DATESTAMP) = YEAR(GETDATE()) AND MONTH(RL.DATESTAMP) = MONTH(GETDATE())  AND RL.STATEMENTID IS NOT NULL AND T.ENDDATE IS NULL GROUP BY P.DEVELOPMENTID ) REPRINT ON REPRINT.DEVELOPMENTID = D.DEVELOPMENTID " 
												
	  strSQL =  "SELECT D.DEVELOPMENTID, SCH.SCHEMENAME, ISNULL(fIRSTPRINT.PRINTCOUNT,0) AS PRINTCOUNT , ISNULL(REPRINT.RECOUNT,0) AS RECOUNT " &_
	  			"FROM PDR_DEVELOPMENT D INNER JOIN P_SCHEME SCH ON D.DEVELOPMENTID = SCH.DEVELOPMENTID" & FirstPrintCount & RePrintCount &_
				" " & FndCntr & " ORDER BY SCH.SCHEMENAME ASC "						
response.Write(strsql)
		if mypage = 0 then mypage = 1 end if
		pagesize = 20
		rw strSQL
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if select box has been previously selected
			isselected = ""
			DEVELOPMENTID = Rs("DEVELOPMENTID")
	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
										
			'START APPENDING EACH ROW TO THE TABLE
			if DEVELOPMENTID <> "" then
										
				str_data = str_data & 	"<TR style='cursor:hand' >" &_
										"<TD >" & Rs("SCHEMENAME") & "</TD>" &_
										"<TD><A HREF='RentStatementMailingList.asp?PrintAction=FirstPrint&developmentid=" &  DEVELOPMENTID &  "&schemename=" & Rs("SCHEMENAME") & "'>First Print</a> (" & Rs("PRINTCOUNT") & ")</TD>"  &_
										"<TD></TD>"  &_
										"<TD><A HREF='RentStatementMailingList.asp?PrintAction=RePrint&developmentid=" &  DEVELOPMENTID &  "&schemename=" & Rs("SCHEMENAME") & "'>Re-Print</a> (" & Rs("RECOUNT") & ")</TD>"  &_
										"</TR>"										
			
			End If
			Rs.movenext()
			End If
			
		Next
		
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data &	"<TR><TD COLSPAN=4 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
									"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & 	"<TR style='height:'8px'><TD></TD></TR>" &_
									"<TR style='display:none'><TD COLSPAN=4 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
									"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=4 align=right>" &_
								"</TD></TR></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=4 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementDevelopmentList_srv.asp?page=" & orderBy & "&txt_FROM=" & fromdate & "&WhatContractor=" & FindContractor & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementDevelopmentList_srv.asp?page=" & prevpage& "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementDevelopmentList_srv.asp?page=" & nextpage & "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/RentStatementDevelopmentList_srv.asp?page=" & numpages & "&WhatContractor=" & FindContractor & "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/RentStatementDevelopmentList_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&WhatContractor=" & FindContractor & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr></table>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=4 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onLoad="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>