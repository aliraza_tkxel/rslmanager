<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% bypasssecurityaccess = True
Server.ScriptTimeout = 3600000
 %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim SATISFACTION_LETTER_HTML
	Dim insert_list  
	Dim sel_split
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color
	Dim ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor,LetterBody, is_a_DD_Tennant
	Dim PrintAction
	Dim targetrentid
	dim JournalTitle
	JournalTitle=mid(year(now),3) & " / " &  mid(year(now)+1,3) & " Increase"
	CurrentDate = CDate("1 jan 2006") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	
	insert_list = Request("idlist")
	PrintAction =  Request("PrintAction")
	
	OpenDB()
	
	Display_Letter()
	
	CloseDB()
	 	
	Function Display_Letter()
		Dim icount
		icount = 0
		sel_split = Split(insert_list ,",") ' split selected string
		For each key in sel_split
		icount = icount + 1
		next
		For each key in sel_split

			itemdetail_id = KEY
			
			if itemdetail_id <> "" then
						
					SQL = "SELECT 	YEAR(GETDATE()) AS YEARNUMBER,FIN.*,C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, ISNULL(DDPAY.REGULARPAYMENT,0) AS REGULARPAYMENT, DDPAY.DDSCHEDULEID, RIGHT('0' + cast(DAY(DDPAY.PAYMENTDATE )as varchar),2) AS PAYMENTDATE, " &_
							"		C.FIRSTNAME, C.LASTNAME, " &_
							"		A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY, " &_
							"		case WHEN FIN.DATERENTSET < TR.CUTOFFDATE  then 1 " &_
							"		else 0 end SETRENTTO, " &_
							"		TR.TARGETRENTID, " &_
							"		TR.REASSESMENTDATE, " &_
							"		ISNULL(ADNL.GARAGE_COUNT,0) AS GARAGE_COUNT, ISNULL(TR.OLDGARAGECHARGE,0) AS OLDGARAGECHARGE ,ISNULL(TR.NEWGARAGECHARGE,0) AS NEWGARAGECHARGE , " &_
							"		ISNULL(TR.RENT,0) AS T_RENT, ISNULL(TR.SERVICES,0) AS T_SERVICES, ISNULL(TR.COUNCILTAX,0) AS T_COUNCILTAX, ISNULL(TR.WATERRATES,0) AS T_WATERRATES, ISNULL(TR.INELIGSERV,0) AS T_INELIGSERV, ISNULL(TR.SUPPORTEDSERVICES,0) AS T_SUPPORTEDSERVICES, "&_
							"		ISNULL(FIN.GARAGE,0) AS GARAGE, ISNULL(ADNL.ADDITIONAL_RENT,0) AS ADDITIONAL_GARAGE, FIN.RENT, FIN.SERVICES, FIN.COUNCILTAX, FIN.WATERRATES, FIN.INELIGSERV, FIN.SUPPORTEDSERVICES, " &_
							"		(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' +  ISNULL(P.POSTCODE,'')) AS P_FULLADDRESS " &_							
							"FROM 	C_TENANCY T " &_
							"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
							"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
							"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID AND A.ISDEFAULT = 1 " &_
							"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
							"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
							"		INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID  " &_
							"		INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID and active  = 0 "&_
							"		LEFT JOIN (SELECT DDSCHEDULEID, PAYMENTDATE, TENANCYID, REGULARPAYMENT FROM F_DDSCHEDULE WHERE SUSPEND NOT IN (1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID " &_
							" 		LEFT JOIN (SELECT COUNT(*) AS GARAGE_COUNT ,SUM(F.TOTALRENT) AS ADDITIONAL_RENT,A.TENANCYID FROM C_ADDITIONALASSET A INNER JOIN P__PROPERTY P ON P.PROPERTYID = A.PROPERTYID INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID WHERE A.ENDDATE IS NULL GROUP BY A.TENANCYID) ADNL ON ADNL.TENANCYID = T.TENANCYID " &_							
							"WHERE  T.TENANCYTYPE IN (1,8,13,4,6) AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE()) AND CT.ENDDATE IS NULL AND CT.TENANCYID = " & itemdetail_id
	
					Call OpenRS(rsCust, SQL)

					count = 1
					while NOT rsCust.EOF
					 
						'============================
						' Get Vars
						'============================
					 
						'============================
						' Get Vars
						'============================
							old_garge_charge	=	rsCust("OLDGARAGECHARGE")
							new_garge_charge	=	rsCust("NEWGARAGECHARGE")
							
							GARAGE_COUNT 		=   rsCust("GARAGE_COUNT")
							
							YEARNUMBER			=   rsCust("YEARNUMBER")
							
							REGULARPAYMENT		= 	rsCust("REGULARPAYMENT")
							DDSCHEDULEID		=	rsCust("DDSCHEDULEID")
							DDPAYMENTDATE		=	RScUST("PAYMENTDATE")
							DDPAYMENTDATE		=	DDPAYMENTDATE & "/04/" & YEARNUMBER
							
							DATERENTSET			=   rsCust("SETRENTTO")
							RentEffective		=	rsCust("REASSESMENTDATE")
							Rent 				= 	rsCust("Rent")
							SupportServices 	= 	rsCust("SUPPORTEDSERVICES")
							InelServ			=	rsCust("INELIGSERV")
							OtherServices		=	rsCust("SERVICES")
							CouncilTax			=	rsCust("COUNCILTAX")
							WaterRates			=	rsCust("WATERRATES")
							ADDITIONAL_GARAGE 	=   rsCust("ADDITIONAL_GARAGE")
							GarageSpace			=	rsCust("OLDGARAGECHARGE")
							'GarageSpace			= 	GarageSpace + ADDITIONAL_GARAGE
							
							totalrent			=	rsCust("TOTALRENT") + ADDITIONAL_GARAGE
							
							T_Rent 				= 	rsCust("T_Rent")
							T_SupportServices 	= 	rsCust("T_SUPPORTEDSERVICES")
							T_InelServ			=	rsCust("T_INELIGSERV")
							T_OtherServices		=	rsCust("T_SERVICES")
							T_CouncilTax		=	rsCust("T_COUNCILTAX")
							T_WaterRates		=	rsCust("T_WATERRATES")
							T_GarageSpace		=	rsCust("NEWGARAGECHARGE")			
						
							if T_GarageSpace > 0 then
								'rw "dev testing by jim : garage stnd"
								T_GarageSpace = new_garge_charge
							end if
							
							if ADDITIONAL_GARAGE > 0 then
								'rw "<BR>dev testing by jim : garage addtnl"
								t_ADDITIONAL_GARAGE = new_garge_charge
							else
								t_ADDITIONAL_GARAGE = ADDITIONAL_GARAGE
							end if
														
							T_totalrent			=   T_Rent + T_SupportServices + T_InelServ + T_OtherServices + T_CouncilTax + T_WaterRates + T_GarageSpace '+ t_ADDITIONAL_GARAGE

							'T_GarageSpace		= 	T_GarageSpace + t_ADDITIONAL_GARAGE
							
							p_fulladdress		=	rsCust("P_FULLADDRESS")
							

							
							targetrentid		=	rsCust("TARGETRENTID")
							
						'===============================================================================
						'	Start Building name and address info for letter
						'===============================================================================
						
						''''''''''''''''''''
						' Build Names First
						''''''''''''''''''''
						
							ShortName = ""
							StringName = ""
													
							'TITLE
							Title = rsCust("TITLE")
							if (Title <> "" AND NOT isNull(Title)) then
								StringName = StringName & Title & " "
								ShortName = ShortName & Title & " "
							end if
							
							'FIRSTNAME
							FirstName = rsCust("FIRSTNAME")
							if (FirstName <> "" AND NOT isNull(FirstName)) then
								StringName = StringName & FirstName & " "
							end if
							
							'LASTNAME
							LastName =rsCust("LASTNAME")
							if (LastName <> "" AND NOT isNull(LastName)) then
								StringName = StringName & LastName & " "
								ShortName = ShortName & LastName
							end if
							
							' DEAR LINE IN LETTER
							if (count = 1) then
								StringFullName = StringName
								str_Dear = "Dear " & ShortName
								count = 2							
						    else
								StringFullName = StringFullName & " and " & StringName
								str_Dear = str_Dear & " and " & ShortName
							end if
							
							rsCust.moveNext
						wend
					
					'''''''''''''''''''''''
					' End Build Names First
					'''''''''''''''''''''''
					
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''
						
						if (count = 2) then
							rsCust.moveFirst()
							housenumber = rsCust("housenumber")
							if (housenumber = "" or isNull(housenumber)) then
								'housenumber = rsCust("flatnumber")
							end if
							if (housenumber = "" or isNull(housenumber)) then
								FirstLineOfAddress = rsCust("Address1")
							else
								FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
							end if
					
							RemainingAddressString = ""
							AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
							for i=0 to Ubound(AddressArray)
								temp = rsCust(AddressArray(i))
								if (temp <> "" or NOT isNull(temp)) then
									RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font:12PT ARIAL'>" & temp & "</TD></TR>"
								end if
							next
							tenancyref = rsCust("TENANCYID")
						end if
						
						' now combine the name and address to give a post label
						FullAddress = "<TR><TD nowrap style='font:12PT ARIAL'>" & StringFullName & "</TD></TR>" &_
									"<TR><TD nowrap style='font:12PT ARIAL'>" & FirstLineOfAddress & "</TD></TR>" &_
									RemainingAddressString 
								
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''				
								
					'========================================================================================
					'	End building name and address info
					'========================================================================================
					
			
					DoPageBreak2 = "style='page-break-after: always'"
					
					SingleLetterBody = SingleLetterBody &  "<p " & DoPageBreak &  "><table width=100% border=0 style='height:21cm'>"
					SingleLetterBody = SingleLetterBody &  "<TBODY ID='LETTERDIV'><tr><td align=right valign=top style='font:12PT ARIAL' height='99'><TABLE border=0 id='return_address' style='display:block' >" & str_return_address & "</TABLE></td>"
					SingleLetterBody = SingleLetterBody &  "</tr><tr><td nowrap valign=top><table><tr><td style='font:12PT ARIAL'>20 February " & YEARNUMBER & "</td></tr><tr><td nowrap style='border:1px solid #133E71;font:12PT ARIAL'>Tenancy Ref: <b>" & TenancyReference(tenancyref) & "</b></td></tr>" & FullAddress 
					SingleLetterBody = SingleLetterBody &  "</table></td></tr><tr><td>&nbsp;</td></tr><tr><td> </td></tr>"
					' Dear Line
					SingleLetterBody = SingleLetterBody &  "<tr><td style='font:12PT ARIAL' >" & str_Dear & "</td></tr>"
					'explanation text
					SingleLetterBody = SingleLetterBody & "<tr style='height:10px'><td > </td></tr><tr id='LetterContent1' style='display:block'><td style='font:12PT ARIAL' valign='top' class='RSLBlack><div id=letterText>"
					SingleLetterBody = SingleLetterBody & "<span style='font:12PT ARIAL'>"
					
'// Increase Date -- needs to come from database
	
					IncreaseDate_HTML = ""
					IncreaseDate_HTML = IncreaseDate_HTML & RentEffective  
					
'// RENT FIGURES TABLE - PLACE WHERE USER HAS ENTERED [R]

					RentAccountInfo_HTML = " " & p_fulladdress & "<br><br>"

					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<table  border='0' cellspacing='0' cellpadding='0'>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td style='width:442'></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='18' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top'><b>Existing Rent</b></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='37' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='110' valign='top' align='right'><b>New Rent wef " & RentEffective  &  "</b></td>"					
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "</table>"


					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table style='width:90%'>"
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td style='width:60%'></td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top' style='font:12PT ARIAL'><b>Existing Rent</b></td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top' style='font:12PT ARIAL'><b>New Rent wef " & RentEffective  &  "</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "</tr>"

					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td style='width:60%'>"
	
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
	
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td><span style='font:12PT ARIAL'>Rent</span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td><span style='font:12PT ARIAL'>Support Services </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & " <tr><td><span style='font:12PT ARIAL'>Ineligible Services </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "  <tr><td><span style='font:12PT ARIAL'>Other Services </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Council Tax </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Water Rates </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Garage/Car Space </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>&nbsp;</span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Total Rent Payable</span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         "</table>"
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & 		"</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &       "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &       "<td valign='top' style='font:12PT ARIAL'>"    
					
					' Current rent table						
					RentAccountInfo_HTML = RentAccountInfo_HTML & 	"<table width='100%'  border='0' cellspacing='0' cellpadding='0'>" 
					
					RentAccountInfo_HTML = RentAccountInfo_HTML &   "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(Rent,2)& "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(SupportServices,2)& "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(InelServ,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(OtherServices,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(CouncilTax,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(WaterRates,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(garageSpace,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(totalrent,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &    "</table>"

					RentAccountInfo_HTML = RentAccountInfo_HTML & "</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<td width='10' style='font:12PT ARIAL'>&nbsp;</td><td valign='top' style='font:12PT ARIAL'>"
					
					// this bit will be generated if enetered in letter

					IF DATERENTSET = "1" THEN
					' Target rent table
							RentAccountInfo_HTML = RentAccountInfo_HTML & "<table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
						
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_Rent,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_SupportServices,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_InelServ,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_OtherServices,2) & "</td> </tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_CouncilTax,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_WaterRates,2) & "</td></tr>"
						'RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(GARAGE_COUNT * t_GarageSpace,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(t_GarageSpace,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(t_totalrent,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML & 	 "</table>"
						'</td></tr></table>"
					
					ELSE
					
'						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<td width='10' style='font:12PT ARIAL'>&nbsp;</td><td valign='top' style='font:12PT ARIAL'>"
						
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'><b>New Rent wef " & RentEffective  &  "</b><br><br></td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(Rent,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(SupportServices,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(InelServ,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(OtherServices,2) & "</td> </tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(CouncilTax,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(WaterRates,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(GarageSpace,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(totalrent,2) & "</td></tr>"
						RentAccountInfo_HTML = RentAccountInfo_HTML & 	 "</table>" 
					

					END IF					
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & 	"</td></tr></table>"


' PAGE BREAK			
					PageBreak_HTML = ""
					PageBreak_HTML = PageBreak_HTML &	"<p style='page-break-after: always'></p>"
					
	
' DD ACCOUNT HTML

					' SHOW DD PAY AGREEMENT IF TENANT PAYS THAT WAY
					DDAccountInfo_HTML = ""
					IF DDSCHEDULEID <> "" THEN
						isDDCustomer = 1
						if REGULARPAYMENT = totalrent then		
							FullDDMatch = 1
							DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
							DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><br>Your Direct Debit Payment has been automatically adjusted to reflect the amount of new rent you need to pay to Broadland Housing Association.<BR><BR>The amount of money that will be collected will be changing from " & FormatCurrency(totalrent,2) & " to " & FormatCurrency(t_totalrent,2) & " with effect from " & RentEffective  & "<BR><BR>On the " & DDPAYMENTDATE  & " we will collect " & FormatCurrency(t_totalrent,2) & " and " & FormatCurrency(t_totalrent,2) & " each month thereafter until further notice. "
						else
							FullDDMatch = 0
							DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
							DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><br>Your Direct Debit has not been automatically adjusted, therefore your payment on " & DDPAYMENTDATE  & " will remain at " & FormatCurrency(REGULARPAYMENT,2) & ", until further notice.<br><br>You will shortly receive confirmation of your new Direct debit amount, to be effective from April " & YEARNUMBER  & ". "
						End if
					ELSE
						isDDCustomer = 0
						FullDDMatch = 0
						DDPAYMENTDATE = "NULL"
					END IF
					
				
					'/////////////////
				
					
					sql = "select * from P_RENTINCREASE_LETTERTEXT"
					' this bit puts in the letter
					Call OpenRS(rsLetterBody, SQL)
					MainBodyFromDB = rsLetterBody("LETTERTEXT")
					Call CloseRS(rsLetterBody)
					
					'REPLACE LETTER BUILDER BOLD TAGS AND REPLACE WITH HTML BOLD TAGS
					MainBodyFromDB = replace(MainBodyFromDB,"[B]","<B>")
					MainBodyFromDB = replace(MainBodyFromDB,"[/B]","</B>")
					
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[D]",IncreaseDate_HTML)
					
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[R]",RentAccountInfo_HTML)

					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[DD]",DDAccountInfo_HTML)
					
					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[PB]",PageBreak_HTML)
					
					SingleLetterBody = SingleLetterBody & MainBodyFromDB
					
					
					'////////////////					

					SingleLetterBody = SingleLetterBody &	 " </td></tr><tr><td> </td></tr><tr style='height:30px'><td style='font:12PT ARIAL'>&nbsp;</td></tr> "
					SingleLetterBody = SingleLetterBody &	 " <tr style='height:20px'><td style='font:12PT ARIAL'><span font:12PT ARIAL>Yours sincerely</span></td></tr> "
					SingleLetterBody = SingleLetterBody &	 "<tr> "
					SingleLetterBody = SingleLetterBody &	 "  <td align=left id='footer' style='visiblity:hidden'><img src='../Images/Ivan_Johnson.gif' width='113' height='56'></td> "
					SingleLetterBody = SingleLetterBody &	 "  </tr> "
					SingleLetterBody = SingleLetterBody &	 "<tr style='height:60px' valign='top'> "
					SingleLetterBody = SingleLetterBody &	 "  <td style='font:12PT ARIAL'><span font:12PT ARIAL>Ivan Johnson<BR>Director of Housing</span></td> "
					SingleLetterBody = SingleLetterBody &	 " </tr> "					
					SingleLetterBody = SingleLetterBody &	 "<tr> "
					SingleLetterBody = SingleLetterBody &	 "    <td align=right id='footer' style='visiblity:hidden'>&nbsp; </td> "
					SingleLetterBody = SingleLetterBody &	 "</tr></table></p>"
					

					if not sel_split(icount-1) = key then

						SingleLetterBody= SingleLetterBody & "<p style='page-break-after: always'> &nbsp;</p>"
					else
						SingleLetterBody= SingleLetterBody &	 ""
					End If				
					
					LetterBody = LetterBody  & SingleLetterBody

					letterBody_forStore = SingleLetterBody
					
					SingleLetterBody = ""
					StringName 	= ""
					Title 		= ""
					ShortName 	= ""
					FirstName 	= ""
					LastName 	= ""
					StringFullName = ""
					str_Dear 	= ""
					MainBodyFromDB = ""
					
					end if


					STRsql = " exec C_RENTLETTER_RECORDPRINT_fixer " & itemdetail_id & "," & targetrentid & ",'" & replace(letterBody_forStore,"'","''") & "',1"
					Conn.Execute(strSQL)
			RW STRsql 

					
					
					HISTORY_SQL = 	" INSERT INTO C_RENTLETTER_HISTORY (	" &_
					"										TENANCYID	,	TARGETRENTID, ITEMIDD	,	ITEMNATUREID	,	CURRENTITEMSTATUSID	, " &_
					"										RENTEFFECTIVEDATE	,	PROPERTYADDRESS	,	RENT	,	SUPPORTEDSERVICES	, " &_
					"										INELIGSERV	,	OTHERSERVICES	,	COUNCILTAX	,	WATER	,	GARAGE	,	TOTALRENT	,	NEW_RENT	, " &_
					"										NEW_SUPPORTEDSERVICES	,	NEW_INELIGSERV	,	NEW_OTHERSERVICES	,	NEW_COUNCILTAX	,	NEW_WATER	, " &_
					"										NEW_GARAGE	,	NEW_TOTALRENT	,	IS_DD_CUSTOMER	,	FULL_DD_MATCH_ON_RENT	,	DD_PAYMENTDATE	, DD_REGULARPAYMENT, " &_
					"										USERID	)" &_
					"									VALUES " &_
					"									( " &_
					"										" & itemdetail_id & "	, " & targetrentid & " ,	2	,	41	,	23,	" &_
					"										'" & RentEffective & "'	,	'" & replace(p_fulladdress,"'","''") & "'	,	" & Rent & "	,	" & SupportServices	& ", " &_
					"										"  & InelServ  & "	,	" & OtherServices & "	,	" & CouncilTax & "	,	" & WaterRates & "	,	" & GarageSpace & "	,	" & TotalRent & "	,	" & t_rent & "	, " &_
					"										" & t_SupportServices & "	,	" & t_InelServ &  "	,	" & t_OtherServices & "	,	" & t_CouncilTax & "	,	" & t_WaterRates & "	, " &_
					"										" & GarageSpace & "	,	" & t_totalrent	& ",	" & isDDCustomer & "	,	" & FullDDMatch & "	,	" & DDPAYMENTDATE & "	, " & REGULARPAYMENT & "	,	" &_
					"										" & Session("userid") & ")"
					
					'Conn.Execute(HISTORY_SQL)

'' GET THE ID ABOVE AND THEN SAVE IN A HISTORY HTML TABLE -- WITH THE FULL LETTER

		Next
	
	End Function
		
%>
<html>
<head></head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/print.js"></SCRIPT>
<script language="Javascript">

	function PrintMe(){
		printFrame(this);
		}
	
	function ReturnData(){
		parent.str_idlist = ""
		parent.location.href = "RentLettermailingList2.asp"
		}		
	
</script>
<body  onLoad="">
<form name="RSLFORM" method="get">
<%'<input name="PrintLetter" id="PrintLetter" src="/dummy.asp" style="display:block" value="Print Letter" type="button" onClick="PrintMe()">
%></form>
<%'=LetterBody%>
</body>
</html>
