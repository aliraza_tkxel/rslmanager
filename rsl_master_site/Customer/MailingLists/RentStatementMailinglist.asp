<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PrintAction 	= request("PrintAction")
	DevelopmentID 	= request("DevelopmentID")
	quicktenancyfind =  request("quicktenancyfind")
	
	TodaysDate  = Date()
	EarlyDate	=  DateAdd ("m", -3, TodaysDate)

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();

	function process(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "statement.asp?DevelopmentID=<%=DevelopmentID%>&PrintAction=<%=PrintAction%>";
		RSLFORM.submit();
	}

	function GetList(){
		
		findString = ""
		if ((RSLFORM.quicktenancyfind.value)){
			findString = "&quicktenancyfind=" + RSLFORM.quicktenancyfind.value
			}
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/RentStatementMailingList_srv.asp?DevelopmentID=<%=DevelopmentID%>&PrintAction=<%=PrintAction%>" + findString;
		RSLFORM.submit();
	}
	
	function quick_find(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/RentStatementMailingList_srv.asp?DevelopmentID=<%=DevelopmentID%>&LetterAction=<%=PrintAction%>&quicktenancyfind=" + RSLFORM.quicktenancyfind.value;
		RSLFORM.submit();
	}	
  
  	function TickAllChecks(){
	
		var theNum,TheAnswer,numberIDarray	
		 
		if (RSLFORM.TickAll.checked)
			TheAnswer = true
		else 
			TheAnswer = false
					
		numberIDarray = RSLFORM.hid_tenancylist.value
		numberarray = numberIDarray.split(",")
		
		for(i=0;i<numberarray.length; i++){
			theNum = numberarray[i];
			if (document.getElementById("chkpost" + theNum +"").checked != TheAnswer) {
				document.getElementById("chkpost" + theNum +"").checked = TheAnswer
				do_sum(theNum)
				}
		}
	
	}
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("idlist").value = str_idlist;
		
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	function open_me(WorkOrder_id){
		event.cancelBubble = true;
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post>

	
  <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
    <TR>
      <TD>Select the tenancies that you would like to send a Statement to. </TD>
    </TR>
    <TR bgcolor="#F7F7F7">
      <TD align="left">&nbsp;</TD>
    </TR>
    <TR>
      <TD align="left"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="26%">Set date range for statement/s : </td>
          <td width="5%"><strong>from</strong></td>
          <td width="18%"><input name="DateFrom" id="DateFrom" value="<%=EarlyDate%>" type="text" class="textbox100"></td>
          <td width="3%"><strong>To</strong></td>
          <td width="48%"><input name="DateTo" id="DateTo" value="<%=TodaysDate%>" type="text" class="textbox100"></td>
        </tr>
      </table></TD>
    </TR>
    <TR>
      <TD align="left"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><strong>To Select All the records you can see below on this page click here </strong></td>
          <td width="48%">  <table width="25" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="1"></td>
            </tr>
            <tr>
              <td width="25" height="25" align="center" bgcolor="#133e71">
                <input type="checkbox" name="TickAll" value="1" onClick="TickAllChecks()">
					<input name="hid_tenancylist" type="hidden" value="">
			  </td>
            </tr>
         <tr>
              <td height="1"></td>
            </tr>
          </table></td>
        </tr>
      </table></TD>
    </TR>
    <TR align="right" bgcolor="#F7F7F7">
      <TD><input name="idlist" id="idlist" value="" type="HIDDEN">
        <input type='BUTTON' name='btn_FIND' title='Search for matches using Supplier Name.' class='RSLBUTTON' value='Find Tenancy' onClick='quick_find()'>
&nbsp;
<input name="quickfind" id="quicktenancyfind" value="<%=request("quicktenancyfind")%>" type="text"  class="textbox100"></TD>
    </TR>
    <TR> 
      <TD align="left"><div align="left"><strong>
        </strong>          
          
      <strong>          </strong></div></TD>
    </TR>
  </TABLE>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp"  name=frm_slip width=1 height=1 style='display:block'></iframe>
</BODY>
</HTML>

