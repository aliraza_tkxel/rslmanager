<%@LANGUAGE="VBSCRIPT"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../iFrames/iHB.asp" -->
<%
'response.ContentType="text/html"
'response.CharSet="utf-8"

	Dim cnt, customer_id, str_journal_table, str_color,bal_color,bal_text,str_open_bracket,str_close_bracket, itemdetail_id, PrintAction, HBOWED
	Dim CANAMEND
	Dim icount
	
	'customer_id = "1368"
	'tenancy_id 	= "270393"
	insert_list = Request("idlist")
	datefrom	= request.form("DateFrom")
	dateto		= request.form("DateTo")
	PrintAction = request("PrintAction")
	
	'////////////////////////////
	'	Function List
	'////////////////////////////
	
	' This build the filter depending upon what dates the user enters
	Function FilterSQL()
	
		if datefrom <> "" then
			datefrom_sql = " AND J.TRANSACTIONDATE >= '" & datefrom & "'"
		End If
		
		if dateto <> "" then
			dateto_sql = " AND J.TRANSACTIONDATE <= '" & dateto & "'"
		End If
	
		FilterSQL = datefrom_sql & " " & dateto_sql
	
	End Function
	
	' This function builds the list of payment items with balance , deb, cred ect
	Function build_journal()
	
		dim rsCSet,strCSQL,dbCmd,Param,dbCon
		cnt = 0
		
		strCSQL="EXEC F_GET_CUSTOMER_ACCOUNT_WITH_DATEPARAMS @TENANCYID= " &  itemdetail_id & " , @DATEFROM = '"& DATEFROM & "', @DATETO='" & DATETO & "'"
		set rsCSet=server.createobject("ADODB.Recordset")
		set rsCSet=Conn.execute(strCSQL)
		
		str_journal_table = ""
		While Not rsCSet.EOF
			
			cnt = cnt + 1
			'If NOT isNULL(rsCSet("CREDIT")) Then str_color ="COLOR:BLACK" Else str_color =   ";COLOR:BLACK" End If	
			If rsCSet("BREAKDOWN") < 0 Then
			   str_color = ";COLOR:RED" 
			   str_open_bracket="("
			   str_close_bracket=")" 
			Else 
			   str_color = ""
			   	str_open_bracket=""
			   str_close_bracket="" 
			End If
			
			'str_color=""
			If Cint(CANAMEND) = 1 Then str_canamend = " ONCLICK=""open_amend("&rsCSet("JOURNALID")&")"" STYLE='CURSOR:HAND' TITLE='Amend' " End If
			
				if ltrim(rsCSet("PAYMENTTYPE")) = "" then
			        paymenttype=rsCSet("ITEMTYPE")
			        TR_style=";font-weight:bold" 
			    else
			        paymenttype=rsCSet("PAYMENTTYPE")
			        TR_style="" 
			    end if	
			
			str_journal_table = str_journal_table &_
			 	"<TR STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0 " & TR_style & "'" & str_canamend & " >" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsCSet("F_TRANSACTIONDATE_TEXT") & "</TD>" &_
				"<TD NOWRAP STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & paymenttype & "</TD>" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'> " & rsCSet("PAYMENTSTARTDATE") & "&nbsp;" & rsCSet("PAYMENTENDDATE") & "</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsCSet("debit") & "&nbsp;&nbsp;</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsCSet("credit") & "&nbsp;&nbsp;</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0 " & str_color & "'>&nbsp;&nbsp;" & str_open_bracket & abs(rsCSet("BreaKDown")) & str_close_bracket & "&nbsp;&nbsp;</TD>" &_
				"<tr>"
			rsCSet.movenext()
			
		Wend
		CloseRs(rsCSet)
		
		If cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=7 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	Function GetBalance()
		
		'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
		' - item type has to be Rent,Initial Rent, Opening Balance
		' - can be ( DD Unpaid,Cheque Unpaid  )OR (Paid,Unpaid,Due,Validated,Transferred,ReturnedDeclined,Recovered,Adjustment) 
		
		strSQL = "SELECT ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
			 " FROM 	F_RENTJOURNAL J " &_
			 " LEFT 	JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
			 " WHERE	J.TENANCYID = " & itemdetail_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) AND J.ITEMTYPE IN (1,8,9,10) " &_
             " AND J.TRANSACTIONDATE <= '" & dateto & "'"
		Call OpenRs (rsSet, strSQL) 
		GetBalance = FormatCurrency(rsSet("AMOUNT"))
		CloseRs(rsSet)
		'END OF GET BALANCE
		
	End Function
	
	Function GetOpeningBalance()
		
		'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
		' - item type has to be Rent,Initial Rent, Opening Balance
		' - can be ( DD Unpaid,Cheque Unpaid  )OR (Paid,Unpaid,Due,Validated,Transferred,ReturnedDeclined,Recovered,Adjustment) 

		strSQL = "SELECT ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
			 "FROM 	F_RENTJOURNAL J " &_
			"WHERE	J.TENANCYID = " & itemdetail_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))  AND J.TRANSACTIONDATE < '" & DateFrom & "'"
		Call OpenRs (rsSet, strSQL) 
		GetOpeningBalance = FormatCurrency(rsSet("AMOUNT"))
		CloseRs(rsSet)
		'END OF GET BALANCE
		
	End Function

	Function GetAddress()

'		SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
'				"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
'				"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
'				"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
'				"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
'				"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & itemdetail_id
	

	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, N.LIST,  " & vbCrLf &_
			"		A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY , A.ISDEFAULT,  " & vbCrLf &_
			"		CASE WHEN D.CAREOF = 1 THEN 'c/o ' + A.CONTACTFIRSTNAME + ' ' + A.CONTACTSURNAME ELSE '' END AS CAREOF  " & vbCrLf &_
			"FROM C_TENANCY T   " & vbCrLf &_
			"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " & vbCrLf &_
			"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID    " & vbCrLf &_
			"	INNER JOIN C_CUSTOMER_NAMES_GROUPED_VIEW N ON N.I = T.TENANCYID  " & vbCrLf &_
			"	LEFT JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID  " & vbCrLf &_
			"	LEFT JOIN C_ADDRESSTYPE D ON A.ADDRESSTYPE = D.ADDRESSTYPEID " & vbCrLf &_
			"	LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID    " & vbCrLf &_
			"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & itemdetail_id & " " & vbCrLf &_
			"ORDER BY ISDEFAULT DESC, D.CAREOF DESC " & vbCrLf &_
			""

		
		HBOWED = 0

		Call OpenRS(rsCust, SQL)
        HBOWED = HBOWED + TOTAL_HB_OWED_THIS_DATE(rsCust("CUSTOMERID"), itemdetail_id)	
		rsCust.moveFirst()
		StringFullName = rsCust("LIST")
		if rsCust("CAREOF") <> "" then
			StringFullName = StringFullName & "<BR />" & rsCust("CAREOF")
		end if
		housenumber = rsCust("housenumber")
		if (housenumber = "" or isNull(housenumber)) then
			FirstLineOfAddress = FirstLineOfAddress & rsCust("Address1")
		else
			FirstLineOfAddress = FirstLineOfAddress & housenumber & " " & rsCust("Address1")		
		end if

		RemainingAddressString = ""
		AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCust(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font-size:12px'>" & temp & "</TD></TR>"
			end if
		next

		GetAddress = "<TR><TD nowrap class='style1'>" & StringFullName & "</TD></TR>" &_
					"<TR><TD nowrap class='style1'>" & FirstLineOfAddress & "</TD></TR>" &_
					RemainingAddressString			
					
	End Function
	
	Function GetHOfficer()	

		SQL = "SELECT  E.FIRSTNAME + ' ' + E.LASTNAME as FullName FROM C_TENANCY T " &_
				" INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				" INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
				" INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
				" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
				" LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				" WHERE CT.ENDDATE IS NULL AND T.TENANCYID = " & itemdetail_id
			
		Call OpenRS(rsHousingOfficer, SQL)
		If not rsHousingOfficer.eof then 
			GetHOfficer = rsHousingOfficer("Fullname") 
		Else
			GetHOfficer = ""
		End If
		Call CloseRS(rsHousingOfficer)

	End Function
	
	Function GetStatement()
	
		' See the Statement Template ( 	Statement_template.html if letter 
		'								needs to be changed and use o-code html converter )
	

		
				StatementStr = StatementStr &  "<table width=""100%"" height=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbCrLf
				StatementStr = StatementStr &  "  <tr>" & vbCrLf
				StatementStr = StatementStr &  "    <td height=""110"" valign=""top""><table width=""100%"">" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td nowrap valign=top width=""137""><table>" & vbCrLf
				StatementStr = StatementStr &  "              <tr>" & vbCrLf
				StatementStr = StatementStr &  "                <td nowrap><u><span class=""style3""><b><font color=""#133e71"" style=""font-size:14px"">Rent Statement</font></b></span></u><span class=""style3""><br>" & vbCrLf
				StatementStr = StatementStr &  "                                    </span><br>" & vbCrLf
				StatementStr = StatementStr &  "                </td>" & vbCrLf
				StatementStr = StatementStr &  "              </tr>" & vbCrLf
				StatementStr = StatementStr &  "              <tr>" & vbCrLf
				StatementStr = StatementStr &  "                <td nowrap style='border:1px solid #133E71'><font style=""font-size:12px;font-family: Arial, Verdana, Helvetica, sans-serif;""><span class=""style3"">Tenancy Ref: <b>" & TenancyReference(itemdetail_id) & "</b></span></font></td>" & vbCrLf
				StatementStr = StatementStr &  "              </tr>" & FullAddress & vbCrLf
				StatementStr = StatementStr &  "          </table></td>" & vbCrLf
				StatementStr = StatementStr &  "          <td align=right width=683 valign=top>&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td align=right valign=""top"" width=""150""><img src=""../Images/BHALOGOLETTER.gif"" width=""145"" height=""122""> </td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "    </table></td>" & vbCrLf
				StatementStr = StatementStr &  "  </tr>" & vbCrLf
				StatementStr = StatementStr &  "  <tr>" & vbCrLf
				StatementStr = StatementStr &  "    <td valign=""top"" height=""171""><table width=""100%"" cellpadding=1 cellspacing=0 style=""behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse"" slcolor='' border=0 hlcolor=STEELBLUE>" & vbCrLf
				StatementStr = StatementStr &  "        <thead>" & vbCrLf
				StatementStr = StatementStr &  "          <tr valign=TOP>" & vbCrLf
				StatementStr = StatementStr &  "            <td colspan=4 height=20><b><font color='Black' style='font-size:14px'><span class=""style3"">Date</span>: " &  DatePart("d", Date) & " " & MonthName(DatePart("m", Date)) & " " & DatePart("yyyy", Date) & "</font></b></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td colspan=5 height=20>&nbsp;            </td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" & vbCrLf
				StatementStr = StatementStr &  "          <tr valign=TOP>" & vbCrLf
				StatementStr = StatementStr &  "            <td style=""BORDER-BOTTOM:1PX SOLID"" width=105><span class=""style7""><font color='Black'>Date</font></span></td>" & vbCrLf
				'StatementStr = StatementStr &  "            <td width=""80"" style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color='Black'> Item Type</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td width=""165"" style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color='Black'>Transaction Type</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td width=""110"" style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color='Black'>Period</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td width=""95"" align=RIGHT style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color='Black'>Monies Due</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td width=""95"" align=RIGHT style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color='Black'>Monies In</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td width=""95"" align=RIGHT style=""BORDER-BOTTOM:1PX SOLID""><span class=""style7""><font color=""Black"">Balance</font></span></td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" & vbCrLf
				
				' Opening Balance Row
				StatementStr = StatementStr &  "         		<TR STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0COLOR:BLACK'  >" & vbCrLf
				StatementStr = StatementStr &  "  		<TD colspan=""4"" STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;</TD>" & vbCrLf
				StatementStr = StatementStr &  "  		<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'><em>Opening Balance </em></TD>" & vbCrLf
				StatementStr = StatementStr &  "  		<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'><em><b>" & FormatCurrency(OPENING_BALANCE) & "&nbsp;&nbsp;</b></em></TD></tr>" & vbCrLf
				StatementStr = StatementStr &  "" & vbCrLf
				StatementStr = StatementStr &  ""
				StatementStr = StatementStr &  "          <tr style='HEIGHT:7PX'>" & vbCrLf
				StatementStr = StatementStr &  "            <td colspan=6></td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" & vbCrLf
				StatementStr = StatementStr &  "        </thead>" & vbCrLf
				StatementStr = StatementStr &  "        <tbody>" & vbCrLf
				StatementStr = StatementStr &  str_journal_table & vbCrLf
				StatementStr = StatementStr &  "        </tbody>" & vbCrLf
				StatementStr = StatementStr &  "        <tfoot>" 
				
				If HBOWED <> 0 then
				StatementStr = StatementStr &  "          <tr valign=TOP>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=4 align=right style=""BORDER-TOP:1PX SOLID""><span class=""style7""><b><font color='Black' style='font-size:12px'>Estimated Housing Benefit Due:</font></b><font color='#133e71' style='font-size:14px'></font><font color='#133e71' style='font-size:12px'></font></span><font color='#133e71' style='font-size:14px'>&nbsp;</font></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=2 align=right style=""BORDER-TOP:1PX SOLID""><span class=""style7""><b><font color='Black' style='font-size:12px'>" & FormatCurrency(HBOWED) & "</font></b></span></td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" 
				else
				StatementStr = StatementStr &  "          <tr valign=TOP>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=4 align=right style=""BORDER-TOP:1PX SOLID""></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=2 align=right style=""BORDER-TOP:1PX SOLID""></td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" 
				end if
				
				StatementStr = StatementStr &  "          <tr valign=TOP>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=4 align=right  style='font-size:12px;font-family: Arial, Verdana, Helvetica, sans-serif;'><span class=""style7""><b><font color='Black' style='font-size:12px'>" & bal_text & " :&nbsp;</font></b></span></td>" & vbCrLf
				StatementStr = StatementStr &  "            <td height=20 colspan=2 align=right  style='font-size:12px;font-family: Arial, Verdana, Helvetica, sans-serif;'><span class=""style7""><b><font color='" & bal_color & "' style='font-size:12px'>" & ACCOUNT_BALANCE & "</font></b></span></td>" & vbCrLf
				StatementStr = StatementStr &  "          </tr>" & vbCrLf
				StatementStr = StatementStr &  "        </tfoot>" & vbCrLf
				StatementStr = StatementStr &  "    </table></td>" & vbCrLf
				StatementStr = StatementStr &  "  </tr>" & vbCrLf
				StatementStr = StatementStr &  "  <tr>" & vbCrLf
				StatementStr = StatementStr &  "    <td valign=""bottom"" align=""right"" height=""100%""><table width=""100%"" align=right>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style='font-size:12px'><span class=""style3""><strong>Payments made within the last 3 working days may not appear on your statement.</strong><br><br>" & vbCrLf
				StatementStr = StatementStr &  "           Credits are not always refundable where there is money outstanding from Housing Benefit. Please contact Customer Services on 0303 303 0003 for further information about this or any aspect of your statement.<br>" & vbCrLf
				StatementStr = StatementStr &  "                <br>" & vbCrLf
				StatementStr = StatementStr &  "                <strong>All our documents can be supplied in large print, Braille, audio tape and in languages other than English. Please contact 0303 303 0003 if you require this service.</strong></td>" & vbCrLf
                StatementStr = StatementStr &  "          <td  style='font-size:12px'>&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style='font-size:12px'>&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""31%"" style='font-size:12px'><div align=""right"" class=""style3"">&nbsp;Broadland Housing Association</div></td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style='font-size:12px'>&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td  style=""font-size:12px""><div align=""right"" class=""style3"">&nbsp;NCFC Jarrold Stand</div></td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style=""font-size:12px"">&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td style=""font-size:12px"" align=""right"">&nbsp;<font face=""Arial, Helvetica, sans-serif"">Carrow Road</font></td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style=""font-size:12px"">&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td style=""font-size:12px"" align=""right"">&nbsp;<font face=""Arial, Helvetica, sans-serif"">Norwich NR1 1HU</font></td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "        <tr>" & vbCrLf
				StatementStr = StatementStr &  "          <td width=""69%"" valign=""top"" style=""font-size:12px"">&nbsp;</td>" & vbCrLf
				StatementStr = StatementStr &  "          <td style=""font-size:12px"" align=""right"">&nbsp;<font face=""Arial, Helvetica, sans-serif""><b>Registered under the Industrial and Provident Societies Act 1965 as a non profit making housing association with charitable status Reg. No. 16274R. Housing Corporation Reg. No. L0026</b></font></td>" & vbCrLf
				StatementStr = StatementStr &  "        </tr>" & vbCrLf
				StatementStr = StatementStr &  "    </table></td>" & vbCrLf
				StatementStr = StatementStr &  "  </tr>" & vbCrLf
				StatementStr = StatementStr &  " </table>" & vbCrLf
				if not sel_split(icount-1) = key then
				    StatementStr = StatementStr &  " <div style=""font-size:16px;font-family: Arial; margin-top:45px;page-break-after: always"">" & vbCrLf
				else
				    StatementStr = StatementStr &  " <div style=""font-size:16px;font-family: Arial; margin-top:45px;"">" & vbCrLf
				end if
                StatementStr = StatementStr &  "    <p style="" text-align:center; vertical-align:top;""><img src=""../Images/BHALOGOLETTER.gif"" width=""145"" height=""113"" alt=""BHG Logo""/></p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style="" text-align:center; vertical-align:top; text-decoration:underline; font-weight:bold;"">UNDERSTANDING YOUR RENT STATEMENT</p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style=""margin-bottom:10px;"">" & vbCrLf
                StatementStr = StatementStr &  "        <b>Tenancy Reference</b> - This is the unique number we use to identify your tenancy and needs to be quoted whenever you contact us." & vbCrLf
                StatementStr = StatementStr &  "    </p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style=""margin-bottom:10px;"">" & vbCrLf
                StatementStr = StatementStr &  "        <b>Estimated Housing Benefit due</b> - For those in receipt of Housing Benefit, this is the amount estimated to be received up-to the end of the month, in which the statement is dated." & vbCrLf
                StatementStr = StatementStr &  "        <br />" & vbCrLf
                StatementStr = StatementStr &  "        Please note this is only an estimate which is based upon the last amount received, and assumes your circumstances have not changed. Housing Benefit may vary, therefore changing your final balance." & vbCrLf
                StatementStr = StatementStr &  "    </p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style=""margin-bottom:10px;"">" & vbCrLf
                StatementStr = StatementStr &  "        <b>Owed to BHA (in red):</b>This is the amount owed to BHA by you. It takes into account any Housing Benefit estimated as still due to the end of the month in which the statement is dated." & vbCrLf
                StatementStr = StatementStr &  "    </p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style=""margin-bottom:10px;"">" & vbCrLf
                StatementStr = StatementStr &  "        <b>Owed from BHA (in Black):</b>This means your account is in credit and you may be due a refund. " & vbCrLf
                StatementStr = StatementStr &  "        <br />" & vbCrLf
                StatementStr = StatementStr &  "        Credits are not always refundable to you; where Housing Benefit is in payment, the Local Authority must be contacted to confirm if there are any overpayments owed to them." & vbCrLf
                StatementStr = StatementStr &  "    </p>" & vbCrLf
                StatementStr = StatementStr &  "    <p style=""font-weight:bold;text-decoration:underline;margin-left:40px;"" >Additional Information</p>" & vbCrLf
                StatementStr = StatementStr &  "    <ul>" & vbCrLf
                StatementStr = StatementStr &  "        <li style=""margin-bottom:20px;"">Your rent is due on the first day of each month and is payable in advance (unless paid by Direct Debit). </li>" & vbCrLf
                StatementStr = StatementStr &  "        <li style=""margin-bottom:20px;"">If you would like to set up direct debit payments on your account please contact customer services for the relevant forms.</li>" & vbCrLf
                StatementStr = StatementStr &  "        <li style=""margin-bottom:20px;"">We have a variety of ways in which you can pay your rent, please contact us for further information.</li>" & vbCrLf
                StatementStr = StatementStr &  "        <li style=""margin-bottom:20px;""><b>If you have any problems paying your rent or you do not understand any part of this letter please contact our Customer Services Team on 0303 303 0003 immediately as we can offer a range of help and advice.</b></li>" & vbCrLf
                StatementStr = StatementStr &  "    </ul>" & vbCrLf
                StatementStr = StatementStr &  "</div>" & vbCrLf
                StatementStr = StatementStr &  ""		
		
		'if not sel_split(icount-1) = key then
		 '   StatementStr = StatementStr & "<p style='page-break-after: always'></p>"
		'else
		'	StatementStr = StatementStr &	 ""
		'End If		

		GetStatement = StatementStr
		
	End Function
	
	Function RecordPrint()

		select case PrintAction
						
						case "FirstPrint"
							strSQL = "INSERT INTO C_RENTSTATEMENT(TENANCYID,LETTERSTATUS,PRINTCOUNT,USERID,DATETO,DATEFROM) VALUES (" & itemdetail_id & ",1,1," & SESSION("USERID") & ",'" & DATETO & "','" & DATEFROM & " ');" &_
									 "INSERT INTO C_JOURNAL (TENANCYID,ITEMID,ITEMNATUREID,TITLE, CURRENTITEMSTATUSID) VALUES (" & itemdetail_id & ",2,45,'Rent Statement',23)" 
						case "RePrint"
							strSQL = "UPDATE C_RENTSTATEMENT SET PRINTCOUNT = PRINTCOUNT + 1 WHERE TENANCYID = " & itemdetail_id

		end select
		
		Conn.Execute(strSQL)
	
	End Function
	
	'////////////////////////////
	'	End Function List
	'////////////////////////////
	
	icount = 0
	sel_split = Split(insert_list ,",") ' split selected string
	For each key in sel_split
	icount = icount + 1
	next
	
	OpenDB()
	
	For each key in sel_split

		itemdetail_id = KEY	

		build_journal()
		SQLFilter = FilterSQL()
		
		OPENING_BALANCE = GetOpeningBalance()
		ACCOUNT_BALANCE = GetBalance()
		FullAddress 	= GetAddress()
		
		'ACCOUNT_BALANCE = ACCOUNT_BALANCE - HBOWED
		
		'************************************************************************************************
		'MODIFIED BY ADNAN MIRZA
		'MODIFIED ON 23/6/2008
		'REASON: TASK 5263 (FOR JOINT TENANTS, HB WAS NOT COMING UP).
		
		SQL = " execute ARREARS_GET_SINGLE_CUSTOMER_HB '" & itemdetail_id &  "'"
		Call OpenRs(rsSet, SQL)
				
			Ant_HB = rsSet("ANTHB")
			Adv_HB = rsSet("ADVHB")
			
			if Ant_HB > 0 then
				HBOWED = Ant_HB
				' Work Out Account Balance 
				' If there is ant we reduce the balance and if we have adv we increase the balance
				ACCOUNT_BALANCE = ACCOUNT_BALANCE - Ant_HB
			else
				HBOWED = Adv_HB
				ACCOUNT_BALANCE = ACCOUNT_BALANCE + Adv_HB
			End If
		Call CloseRs(rsSet)	
		'************************************************************************************************
		Fullname 		= GetHOfficer()
	
		    if ACCOUNT_BALANCE < 0 then
			    CR_DBval = "CR"
			    bal_color ="Black"
			    bal_text="Owed from BHA"
			    ACCOUNT_BALANCE = FormatCurrency(abs(ACCOUNT_BALANCE))	
			    'ACCOUNT_BALANCE = replace(ACCOUNT_BALANCE, "-", CR_DBval & " ")
			else  
		        CR_DBval = "DR"
		        bal_color ="red"
		        bal_text="Owed to BHA"
			    ACCOUNT_BALANCE = FormatCurrency(ACCOUNT_BALANCE)
			    'ACCOUNT_BALANCE = CR_DBval & " " & ACCOUNT_BALANCE
			end if
				
		Statement_HTML = Statement_HTML & GetStatement()
		RecordPrint()
		
		
		
	Next	 
	
	Call CloseDB()		
%>
<html>
<head>
    <title>Customer Info:</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-size: 12px;
        }
        body
        {
            background-color: white;
            background-image: none;
            margin: 6px,0px,0px,0px;
        }
    </style>
</head>
<script language="javascript" type="text/javascript" src="/js/print.js"></script>
<script language="javascript" type="text/javascript">
    function PrintMe() {
        printFrame(this);
    }

    function ReturnData() {
        parent.str_idlist = ""
        parent.location.href = "RentStatementDevelopmentList.asp"
    }
</script>
<body onload="PrintMe();ReturnData()">
    <%=Statement_HTML%>
</body>
</html>
