<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../iFrames/iHB.asp" -->
<%
	Dim SATISFACTION_LETTER_HTML
	Dim insert_list  
	Dim sel_split
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color
	Dim ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor,LetterBody, is_a_DD_Tennant
	Dim PrintAction
	Dim targetrentid
	dim JournalTitle
	JournalTitle=mid(year(now),3) & " / " &  mid(year(now)+1,3) & " Increase"
	CurrentDate = CDate("1 jan 2006") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	
	insert_list = Request("idlist")
	PrintAction =  Request("PrintAction")
	
	OpenDB()
	
	update_Repair()
	
	CloseDB()
	 	
	Function update_Repair()
		Dim icount
		icount = 0
		sel_split = Split(insert_list ,",") ' split selected string
		For each key in sel_split
		icount = icount + 1
		next
		For each key in sel_split

			itemdetail_id = KEY
			
			if itemdetail_id <> "" then
			
					SQL = "SELECT 	YEAR(GETDATE()) AS YEARNUMBER,FIN.*,C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, ISNULL(DDPAY.REGULARPAYMENT,0) AS REGULARPAYMENT, DDPAY.DDSCHEDULEID, DAY(DDPAY.PAYMENTDATE) AS PAYMENTDATE, " &_
							"		C.FIRSTNAME, C.LASTNAME, " &_
							"		A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY, " &_
							"		case WHEN FIN.DATERENTSET < TR.CUTOFFDATE  then 1 " &_
							"		else 0 end SETRENTTO, " &_
							"		TR.TARGETRENTID, " &_
							"		TR.REASSESMENTDATE, " &_
							"		ISNULL(ADNL.GARAGE_COUNT,0) AS GARAGE_COUNT, TR.OLDGARAGECHARGE,TR.NEWGARAGECHARGE, " &_
							"		ISNULL(TR.RENT,0) AS T_RENT, ISNULL(TR.SERVICES,0) AS T_SERVICES, ISNULL(TR.COUNCILTAX,0) AS T_COUNCILTAX, ISNULL(TR.WATERRATES,0) AS T_WATERRATES, ISNULL(TR.INELIGSERV,0) AS T_INELIGSERV, ISNULL(TR.SUPPORTEDSERVICES,0) AS T_SUPPORTEDSERVICES, "&_
							"		ISNULL(FIN.GARAGE,0) AS GARAGE, ISNULL(ADNL.ADDITIONAL_RENT,0) AS ADDITIONAL_GARAGE, FIN.RENT, FIN.SERVICES, FIN.COUNCILTAX, FIN.WATERRATES, FIN.INELIGSERV, FIN.SUPPORTEDSERVICES, " &_
							"		(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' +  ISNULL(P.POSTCODE,'')) AS P_FULLADDRESS " &_							
							"FROM 	C_TENANCY T " &_
							"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
							"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	" &_
							"		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID AND A.ISDEFAULT = 1 " &_
							"		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
							"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
							"		INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID " &_
							"		INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID "&_
							"		LEFT JOIN (SELECT DDSCHEDULEID, PAYMENTDATE, TENANCYID, REGULARPAYMENT FROM F_DDSCHEDULE WHERE SUSPEND NOT IN (1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID " &_
							" 		LEFT JOIN (SELECT COUNT(*) AS GARAGE_COUNT ,SUM(F.TOTALRENT) AS ADDITIONAL_RENT,A.TENANCYID FROM C_ADDITIONALASSET A INNER JOIN P__PROPERTY P ON P.PROPERTYID = A.PROPERTYID INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID WHERE A.ENDDATE IS NULL GROUP BY A.TENANCYID) ADNL ON ADNL.TENANCYID = T.TENANCYID " &_							
							"WHERE  T.TENANCYTYPE IN (1,13,6,8) AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE()) AND CT.ENDDATE IS NULL AND CT.TENANCYID = " & itemdetail_id
							
				SQL = "SELECT 	YEAR(REASSESMENTDATE) AS YEARNUMBER, TR.RentEffective, " &_
							"	C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE,  " &_
							"	ISNULL(TR.DDREGULARPAYMENT,0) AS REGULARPAYMENT,  " &_
							"	TR.DDSCHEDULEID,  " &_
							"	DAY(TR.DDPAYMENTDATE) AS PAYMENTDATE,   " &_
							"	C.FIRSTNAME, C.LASTNAME,   " &_
							"	A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY,  " &_ 
							"	ISNULL(TR.OLD_ADLASSETTOTAL,0) AS ADDITIONAL_GARAGE,  " &_
							"	TR.TARGETRENTID,   " &_
							"	TR.REASSESMENTDATE,   " &_
							"	ISNULL(TR.GARAGECOUNT,0) AS GARAGE_COUNT,  " &_
							"	TR.OLDGARAGECHARGE,TR.NEWGARAGECHARGE,  " &_
							"	ISNULL(TR.RENT,0) AS T_RENT, ISNULL(TR.SERVICES,0) AS T_SERVICES, ISNULL(TR.COUNCILTAX,0) AS T_COUNCILTAX, ISNULL(TR.WATERRATES,0) AS T_WATERRATES, ISNULL(TR.INELIGSERV,0) AS T_INELIGSERV, ISNULL(TR.SUPPORTEDSERVICES,0) AS T_SUPPORTEDSERVICES,  " &_
							"	TR.OLD_RENT, TR.OLD_SERVICES, TR.OLD_COUNCILTAX, TR.OLD_WATERRATES, TR.OLD_INELIGSERV, TR.OLD_SUPPORTEDSERVICES,  TR.OLD_RENTTOTAL ," &_
							"	(ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' +  ISNULL(P.POSTCODE,'')) AS P_FULLADDRESS , " &_
							"	RL.DATESTAMP AS LETTERPRINTEDDATE " &_
							"FROM 	C_TENANCY T   " &_
							"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID   " &_
							"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	  " &_
							"	INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID AND A.ISDEFAULT = 1   " &_
							"	LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID   " &_
							"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID   " &_
							"	INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID  " &_
							"	INNER JOIN C_RENTLETTER RL ON RL.TARGETRENTID = TR.TARGETRENTID " &_
							"WHERE  T.TENANCYTYPE IN (1,13,6,8) AND CT.ENDDATE IS NULL AND CT.TENANCYID = " & itemdetail_id & " AND RL.LETTERID =  " & request("Letterid")
				
					Call OpenRS(rsCust, SQL)
					
					count = 1
					while NOT rsCust.EOF
					 
						'============================
						' Get Vars
						'============================
							old_garge_charge	=	rsCust("OLDGARAGECHARGE")
							new_garge_charge	=	rsCust("NEWGARAGECHARGE")
							
							GARAGE_COUNT 		=   rsCust("GARAGE_COUNT")
							
							YEARNUMBER			=   rsCust("YEARNUMBER")
							
							REGULARPAYMENT		= 	rsCust("REGULARPAYMENT")
							DDSCHEDULEID		=	rsCust("DDSCHEDULEID")
							DDPAYMENTDATE		=	RScUST("PAYMENTDATE")
							DDPAYMENTDATE		=	DDPAYMENTDATE & "/04/" & YEARNUMBER
							
							DATERENTSET			=   "1" 'rsCust("SETRENTTO")
							RentEffective		=	rsCust("REASSESMENTDATE")
							Rent 				= 	rsCust("OLD_Rent")
							SupportServices 	= 	rsCust("OLD_SUPPORTEDSERVICES")
							InelServ			=	rsCust("OLD_INELIGSERV")
							OtherServices		=	rsCust("OLD_SERVICES")
							CouncilTax			=	rsCust("OLD_COUNCILTAX")
							WaterRates			=	rsCust("OLD_WATERRATES")
							ADDITIONAL_GARAGE 	=   rsCust("ADDITIONAL_GARAGE")
							GarageSpace			=	rsCust("OLDGARAGECHARGE")
							'GarageSpace			= 	GarageSpace + ADDITIONAL_GARAGE
							
							totalrent			=	rsCust("OLD_RENTTOTAL") + ADDITIONAL_GARAGE
							
							T_Rent 				= 	rsCust("T_Rent")
							T_SupportServices 	= 	rsCust("T_SUPPORTEDSERVICES")
							T_InelServ			=	rsCust("T_INELIGSERV")
							T_OtherServices		=	rsCust("T_SERVICES")
							T_CouncilTax		=	rsCust("T_COUNCILTAX")
							T_WaterRates		=	rsCust("T_WATERRATES")
							T_GarageSpace		=	rsCust("NEWGARAGECHARGE")			
						
							if T_GarageSpace > 0 then
								'rw "dev testing by jim : garage stnd"
								T_GarageSpace = new_garge_charge
							end if
							
							if ADDITIONAL_GARAGE > 0 then
								'rw "<BR>dev testing by jim : garage addtnl"
								t_ADDITIONAL_GARAGE = new_garge_charge
							else
								t_ADDITIONAL_GARAGE = ADDITIONAL_GARAGE
							end if
														
							T_totalrent			=   T_Rent + T_SupportServices + T_InelServ + T_OtherServices + T_CouncilTax + T_WaterRates + T_GarageSpace '+ t_ADDITIONAL_GARAGE

							'T_GarageSpace		= 	T_GarageSpace + t_ADDITIONAL_GARAGE
							
							p_fulladdress		=	rsCust("P_FULLADDRESS")
							

							
							targetrentid		=	rsCust("TARGETRENTID")
							
						'===============================================================================
						'	Start Building name and address info for letter
						'===============================================================================
						
						''''''''''''''''''''
						' Build Names First
						''''''''''''''''''''
						
							ShortName = ""
							StringName = ""
													
							'TITLE
							Title = rsCust("TITLE")
							if (Title <> "" AND NOT isNull(Title)) then
								StringName = StringName & Title & " "
								ShortName = ShortName & Title & " "
							end if
							
							'FIRSTNAME
							FirstName = rsCust("FIRSTNAME")
							if (FirstName <> "" AND NOT isNull(FirstName)) then
								StringName = StringName & FirstName & " "
							end if
							
							'LASTNAME
							LastName =rsCust("LASTNAME")
							if (LastName <> "" AND NOT isNull(LastName)) then
								StringName = StringName & LastName & " "
								ShortName = ShortName & LastName
							end if
							
							' DEAR LINE IN LETTER
							if (count = 1) then
								StringFullName = StringName
								str_Dear = "Dear " & ShortName
								count = 2							
						    else
								StringFullName = StringFullName & " and " & StringName
								str_Dear = str_Dear & " and " & ShortName
							end if
							
							rsCust.moveNext
						wend
					
					'''''''''''''''''''''''
					' End Build Names First
					'''''''''''''''''''''''
					
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''
						
						if (count = 2) then
							rsCust.moveFirst()
							housenumber = rsCust("housenumber")
							if (housenumber = "" or isNull(housenumber)) then
								'housenumber = rsCust("flatnumber")
							end if
							if (housenumber = "" or isNull(housenumber)) then
								FirstLineOfAddress = rsCust("Address1")
							else
								FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
							end if
					
							RemainingAddressString = ""
							AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
							for i=0 to Ubound(AddressArray)
								temp = rsCust(AddressArray(i))
								if (temp <> "" or NOT isNull(temp)) then
									RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font:12PT ARIAL'>" & temp & "</TD></TR>"
								end if
							next
							tenancyref = rsCust("TENANCYID")
						end if
						
						' now combine the name and address to give a post label
						FullAddress = "<TR><TD nowrap style='font:12PT ARIAL'>" & StringFullName & "</TD></TR>" &_
									"<TR><TD nowrap style='font:12PT ARIAL'>" & FirstLineOfAddress & "</TD></TR>" &_
									RemainingAddressString 
								
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''				
								
					'========================================================================================
					'	End building name and address info
					'========================================================================================
					
					select case PrintAction
						
						case "FirstPrint"
							strSQL = "INSERT INTO C_RENTLETTER (TENANCYID,TARGETRENTID,LETTERSTATUS) VALUES (" & itemdetail_id & "," & targetrentid & " ,1);INSERT INTO C_JOURNAL (TENANCYID,ITEMID,ITEMNATUREID,TITLE, CURRENTITEMSTATUSID) VALUES (" & itemdetail_id & ",2,41,'05/06 Increase',23)" 
						case "RePrint"
							strSQL = "UPDATE C_RENTLETTER SET LETTERSTATUS = 2 WHERE TENANCYID = " & itemdetail_id	& " and YEAR(DATESTAMP) = YEAR(GETDATE()) "			
					end select
			
					
					Conn.Execute(strSQL)
			
					DoPageBreak2 = "style='page-break-after: always'"
					
					LetterBody = LetterBody &  "<p " & DoPageBreak &  "><table width=100% border=0 style='height:21cm'>"
					LetterBody = LetterBody &  "<TBODY ID='LETTERDIV'><tr><td align=right valign=top style='font:12PT ARIAL' height='99'><TABLE border=0 id='return_address' style='display:block' >" & str_return_address & "</TABLE></td>"
					LetterBody = LetterBody &  "</tr><tr><td nowrap valign=top><table><tr><td style='font:12PT ARIAL'>20 February " & YEARNUMBER & "</td></tr><tr><td nowrap style='border:1px solid #133E71;font:12PT ARIAL'>Tenancy Ref: <b>" & TenancyReference(tenancyref) & "</b></td></tr>" & FullAddress 
					LetterBody = LetterBody &  "</table></td></tr><tr><td>&nbsp;</td></tr><tr><td> </td></tr>"
					' Dear Line
					LetterBody = LetterBody &  "<tr><td style='font:12PT ARIAL' >" & str_Dear & "</td></tr>"
					'explanation text
					LetterBody = LetterBody & "<tr style='height:10px'><td > </td></tr><tr id='LetterContent1' style='display:block'><td style='font:12PT ARIAL' valign='top' class='RSLBlack><div id=letterText>"
					LetterBody = LetterBody & "<span style='font:12PT ARIAL'><b>RE-ASSESSMENT OF RENT UNDER ASSURED TENANCIES</b></span><p style='font:12PT ARIAL'> The rent on your property has been re-assessed as from<strong> <em>" & RentEffective & "</em></strong>. The table below shows the breakdown of your existing rent and your new rent for the following property <BR><BR></p>"
					
					' this is the garge line that was assed to the letter for 2006
					'LetterBody = LetterBody & "<p style='font:12PT ARIAL'> Also if you hold a tenancy for one of the Associationís garages, may I advise you that the rent has also been re-assessed as from " & RentEffective & ".  The rent charge has been reviewed from a static charge of " & FormatCurrency(old_garge_charge,2) & " to " & FormatCurrency(new_garge_charge,2) & " per month and will now continue to be re-assessed on an annual basis alongside our assured property rents. <BR><BR> " & p_fulladdress& "</p>"				
					 
					' rentbreakdown block   
					LetterBody = LetterBody & " <table width='625' border='0' cellspacing='0' cellpadding='0'>"
					LetterBody = LetterBody & " <tr><td width='159' style='font:12PT ARIAL'>&nbsp;</td>"
					LetterBody = LetterBody & "    <td width='10' style='font:12PT ARIAL'></td>"
					LetterBody = LetterBody & "    <td width='125' align='right' style='font:12PT ARIAL' ><strong>Existing Rent </strong></td>"
					LetterBody = LetterBody & "    <td width='10' align='right' style='font:12PT ARIAL'></td>"
					LetterBody = LetterBody & "    <td width='140' align='right' style='font:12PT ARIAL'><strong>New Rent wef </strong>01/04/" & YEARNUMBER &  " </td></tr>"
					LetterBody = LetterBody & " <tr><td valign='top' style='font:12PT ARIAL'><table width='100%'  border='0' cellspacing='0' cellpadding='0'><tr><td><span style='font:12PT ARIAL'>Rent</span></td></tr>"
					LetterBody = LetterBody & "<tr><td><span style='font:12PT ARIAL'>Support Services </span></td></tr>"
					LetterBody = LetterBody & " <tr><td><span style='font:12PT ARIAL'>Ineligible Services </span></td></tr>"
					LetterBody = LetterBody & "  <tr><td><span style='font:12PT ARIAL'>Other Services </span></td></tr>"
					LetterBody = LetterBody &         " <tr><td><span style='font:12PT ARIAL'>Council Tax </span></td></tr>"
					LetterBody = LetterBody &         " <tr><td><span style='font:12PT ARIAL'>Water Rates </span></td></tr>"
					LetterBody = LetterBody &         " <tr><td><span style='font:12PT ARIAL'>Garage/Car Space </span></td></tr>"
					LetterBody = LetterBody &         " <tr><td><span style='font:12PT ARIAL'>&nbsp; </span></td></tr>"
					LetterBody = LetterBody &         " <tr><td><span style='font:12PT ARIAL'>Total Rent Payable</span></td></tr>"
					LetterBody = LetterBody &         "</table></td>"
					LetterBody = LetterBody &       "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					LetterBody = LetterBody &       " <td valign='top' style='font:12PT ARIAL'><table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
					' Current rent table
					LetterBody = LetterBody &   "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(Rent,2)& "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(SupportServices,2)& "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(InelServ,2) & "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(OtherServices,2) & "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(CouncilTax,2) & "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(WaterRates,2) & "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(garageSpace,2) & "</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
					LetterBody = LetterBody &    " <tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(totalrent,2) & "</td></tr>"
					LetterBody = LetterBody &    "</table></td>"
					
					IF DATERENTSET = "1" THEN
					' Target rent table
						LetterBody = LetterBody &	 "<td width='10' style='font:12PT ARIAL'>&nbsp;</td><td valign='top' style='font:12PT ARIAL'><table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_Rent,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_SupportServices,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_InelServ,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_OtherServices,2) & "</td> </tr>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_CouncilTax,2) & "</td></tr>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(t_WaterRates,2) & "</td></tr>"
						'LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(GARAGE_COUNT * t_GarageSpace,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(t_GarageSpace,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(t_totalrent,2) & "</td></tr>"
						LetterBody = LetterBody & 	 "</table></td></tr></table>"
					
					ELSE
					
						LetterBody = LetterBody &	 "<td width='10' style='font:12PT ARIAL'>&nbsp;</td><td valign='top' style='font:12PT ARIAL'><table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(Rent,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(SupportServices,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(InelServ,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(OtherServices,2) & "</td> </tr>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(CouncilTax,2) & "</td></tr>"
						LetterBody = LetterBody &	 " <tr><td align='right' style='font:12PT ARIAL'>" &FormatCurrency(WaterRates,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(GarageSpace,2) & "</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>&nbsp;</td></tr>"
						LetterBody = LetterBody &	 "<tr><td align='right' style='font:12PT ARIAL'>" & FormatCurrency(totalrent,2) & "</td></tr>"
						LetterBody = LetterBody & 	 "</table></td></tr></table>"

					END IF					
					' rest of the letter
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'> <span style='font:12PT ARIAL'>The following tells you what you need to do to make sure that you pay the new rent. </span></p> "
					LetterBody = LetterBody &	 "   <p style='font:12PT ARIAL'><strong>Customers Paying by Payment Card </strong></p> "
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'>If you currently pay by a Rent Payment Card please adjust your payment to reflect the amount of new rent you need to pay to Broadland Housing Association. </p> "
					LetterBody = LetterBody &	"<p " & DoPageBreak2 & "></p>"
					
					' SHOW DD PAY AGREEMENT IF TENANT PAYS THAT WAY
					IF DDSCHEDULEID <> "" THEN
						isDDCustomer = 1
						if REGULARPAYMENT = totalrent then		
							FullDDMatch = 1
							LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'><strong>Customers Paying by Direct Debit </strong></p> "
							LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'>Your Direct Debit Payment has been automatically adjusted to reflect the amount of new rent you need to pay to Broadland Housing Association.<BR><BR>The amount of money that will be collected will be changing from " & FormatCurrency(totalrent,2) & " to " & FormatCurrency(t_totalrent,2) & " with effect from " & RentEffective  & "<BR><BR>On the " & DDPAYMENTDATE  & " we will collect " & FormatCurrency(t_totalrent,2) & " and " & FormatCurrency(t_totalrent,2) & " each month thereafter until further notice.</p> "
						else
							FullDDMatch = 0
							LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'><strong>Customers Paying by Direct Debit </strong></p> "
							LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'>Your Direct Debit has not been automatically adjusted, therefore your payment on " & DDPAYMENTDATE  & " will remain at " & FormatCurrency(REGULARPAYMENT,2) & ", until further notice.<br><br>However if you have a payment arrangement for clearance of rent arrears, please contact your Housing Officer upon receipt of this letter.</p> "
						End if
					ELSE
						isDDCustomer = 0
						FullDDMatch = 0
						DDPAYMENTDATE = "NULL"
					END IF
					
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'><strong>Customers Paying by Standing Order</strong></p> "
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'>If you currently pay by Standing Order please contact your bank to instruct them to change your payments to reflect the amount of new rent you need to pay to Broadland Housing Association </p> "		
					LetterBody = LetterBody &	 "     "
					
					IF DDPAYMENTDATE = "/04/" & YEARNUMBER & "" THEN
						DDPAYMENTDATE = ""
					end If
					
					LetterBody = LetterBody &	 "<p style='font:12PT ARIAL' ><strong>Customers who receive Housing Benefit </strong></p> "
					
					' this hb line has been ameded for the 2006 letter
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'>If you claim Housing Benefit for all or part of your rent it is your responsibility to take this letter to your Local Authority immediately to enable them to reassess your claim.  In the meantime please continue to pay any difference between Housing Benefit and your existing rent. </p> "
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'><span style='font:12PT ARIAL'>Once Housing Benefit is adjusted in line with the new rent, we will contact you again with the difference between the Housing Benefit and the new rent which will be the amount you will then need to pay. If your new housing benefit claim is insufficient to cover the rent increase in full then you will also need to pay any built up arrears &ndash; if this happens we will agree a payment plan with you.</span> <br> "
					LetterBody = LetterBody &	 "      <span style='font:12PT ARIAL'><br> "
					LetterBody = LetterBody &	 " If the Local Authority asks for proof of your new rent then please contact your Housing Officer at Broadland Housing Association.</span></p> "
					LetterBody = LetterBody &	 "    <p style='font:12PT ARIAL'> If you have any concerns or questions regarding this letter then please do not hesitate to contact us.</p> "
					LetterBody = LetterBody &	 " </div></td></tr><tr><td> </td></tr><tr style='height:30px'><td style='font:12PT ARIAL'>&nbsp;</td></tr> "
					LetterBody = LetterBody &	 " <tr style='height:20px'><td style='font:12PT ARIAL'><span font:12PT ARIAL>Yours sincerely</span></td></tr> "
					LetterBody = LetterBody &	 "<tr> "
					LetterBody = LetterBody &	 "  <td align=left id='footer' style='visiblity:hidden'><img src='../Images/gary_orr.gif' width='113' height='56'></td> "
					LetterBody = LetterBody &	 "  </tr> "
					LetterBody = LetterBody &	 "<tr style='height:60px' valign='top'> "
					LetterBody = LetterBody &	 "  <td style='font:12PT ARIAL'><span font:12PT ARIAL>Gary Orr MBA BSc (Hons) MCIH.<BR>Director of Housing</span></td> "
					LetterBody = LetterBody &	 " </tr> "					
					LetterBody = LetterBody &	 "<tr> "
					LetterBody = LetterBody &	 "    <td align=right id='footer' style='visiblity:hidden'>&nbsp; </td> "
					LetterBody = LetterBody &	 "</tr></table></p>"
					
					if not sel_split(icount-1) = key then
						LetterBody = LetterBody & "<p style='page-break-after: always'></p>"
					else
						LetterBody = LetterBody &	 ""
					End If				
					
					StringName 	= ""
					Title 		= ""
					ShortName 	= ""
					FirstName 	= ""
					LastName 	= ""
					StringFullName = ""
					str_Dear 	= ""
					
					end if
					
					HISTORY_SQL = 	" INSERT INTO C_RENTLETTER_HISTORY (	" &_
					"										TENANCYID	,	TARGETRENTID, ITEMIDD	,	ITEMNATUREID	,	CURRENTITEMSTATUSID	, " &_
					"										RENTEFFECTIVEDATE	,	PROPERTYADDRESS	,	RENT	,	SUPPORTEDSERVICES	, " &_
					"										INELIGSERV	,	OTHERSERVICES	,	COUNCILTAX	,	WATER	,	GARAGE	,	TOTALRENT	,	NEW_RENT	, " &_
					"										NEW_SUPPORTEDSERVICES	,	NEW_INELIGSERV	,	NEW_OTHERSERVICES	,	NEW_COUNCILTAX	,	NEW_WATER	, " &_
					"										NEW_GARAGE	,	NEW_TOTALRENT	,	IS_DD_CUSTOMER	,	FULL_DD_MATCH_ON_RENT	,	DD_PAYMENTDATE	, DD_REGULARPAYMENT, " &_
					"										USERID	)" &_
					"									VALUES " &_
					"									( " &_
					"										" & itemdetail_id & "	, " & targetrentid & " ,	2	,	41	,	23,	" &_
					"										'" & RentEffective & "'	,	'" & replace(p_fulladdress,"'","''") & "'	,	" & Rent & "	,	" & SupportServices	& ", " &_
					"										"  & InelServ  & "	,	" & OtherServices & "	,	" & CouncilTax & "	,	" & WaterRates & "	,	" & GarageSpace & "	,	" & TotalRent & "	,	" & t_rent & "	, " &_
					"										" & t_SupportServices & "	,	" & t_InelServ &  "	,	" & t_OtherServices & "	,	" & t_CouncilTax & "	,	" & t_WaterRates & "	, " &_
					"										" & GarageSpace & "	,	" & t_totalrent	& ",	" & isDDCustomer & "	,	" & FullDDMatch & "	,	" & DDPAYMENTDATE & "	, " & REGULARPAYMENT & "	,	" &_
					"										" & Session("userid") & ")"
					
					Conn.Execute(HISTORY_SQL)
		Next
	
	End Function
		
%>
<html>
<head></head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/print.js"></SCRIPT>
<script language="Javascript">

	function PrintMe(){
		document.getElementById("PrintLetter").style.display = "none"
		printFrame(this);
		window.close();
		}
	
	// function below not used but useful within iframes
	function ReturnData(){
		parent.str_idlist = ""
		parent.location.href = "RentLetterDevelopmentList.asp"
		}
		
	
</script>
<body>
<form name="RSLFORM" method="get"><input name="PrintLetter" id="PrintLetter" style="display:block" value="Print Letter" type="button" onClick="PrintMe()"></form>
<%=LetterBody%>
</body>
</html>
