<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim SATISFACTION_LETTER_HTML
	Dim insert_list  
	Dim sel_split
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color
	Dim ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor,LetterBody, is_a_DD_Tennant
	Dim PrintAction
	Dim targetrentid
	dim JournalTitle
	JournalTitle=mid(year(now),3) & " / " &  mid(year(now)+1,3) & " Increase"
	CurrentDate = CDate(NOW) 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	TheNextYearDate = TheCurrentYear + 1
	
	insert_list = Request("idlist")
	PrintAction =  Request("PrintAction")
	
	OpenDB()
	
	Display_Letter()
	
	CloseDB()
	 	
	Function Display_Letter()
	TheNextYear=Mid(TheNextYearDate, 3)
		Dim icount
		icount = 0
		sel_split = Split(insert_list ,",") ' split selected string
		For each key in sel_split
		icount = icount + 1
		next
		For each key in sel_split

			itemdetail_id = KEY
			
			if itemdetail_id <> "" then
                      SQL=   "  SELECT DISTINCT YEAR(GETDATE()) AS YEARNUMBER,FIN.PROPERTYID,FIN.NOMINATINGBODY,FIN.FUNDINGAUTHORITY,ISNULL(FIN.RENT,0) AS RENT,ISNULL(FIN.SERVICES,0) AS SERVICES,ISNULL(FIN.COUNCILTAX,0) AS COUNCILTAX, "&_ 
	                         " 	        ISNULL(FIN.WATERRATES,0) AS WATERRATES,ISNULL(FIN.INELIGSERV,0) AS INELIGSERV,ISNULL(FIN.SUPPORTEDSERVICES,0) AS SUPPORTEDSERVICES, "&_
	                         " 	        ISNULL(FIN.GARAGE,0) AS GARAGE,ISNULL(FIN.TOTALRENT,0) AS TOTALRENT,ISNULL(FIN.RENTTYPE,0) AS RENTTYPE,ISNULL(FIN.OLDTOTAL,0) AS OLDTOTAL, "&_
	                         "	        FIN.RENTTYPEOLD,FIN.DATERENTSET,FIN.RENTEFFECTIVE,ISNULL(FIN.TARGETRENT,0) AS TARGETRENT,ISNULL(FIN.YIELD,0) AS YIELD, "&_
	                         " 	        ISNULL(FIN.CAPITALVALUE,0) AS CAPITALVALUE,ISNULL(FIN.INSURANCEVALUE,0) AS INSURANCEVALUE, ISNULL(FIN.OMVST,0) AS OMVST, "&_
	                         "	        ISNULL(FIN.EUV,0) AS EUV,ISNULL(FIN.OMV,0) AS OMV,ISNULL(FIN.CHARGE,0) AS CHARGE,ISNULL(FIN.CHARGEVALUE,0) AS CHARGEVALUE, "&_
	                         "	        ISNULL(FIN.NORENTCHANGE,0) AS NORENTCHANGE,ISNULL(FIN.TARGETRENTSET,0) AS TARGETRENTSET,ISNULL(FIN.PFUSERID,0) AS PFUSERID, "&_
	                         "	        FIN.FRApplicationDate,FIN.FRRegistrationDate,FIN.FREffectDate,FIN.FRStartDate,FIN.FRRegNum,FIN.FRRegNextDue,FIN.PFTIMESTAMP, "&_
	                         "	        C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, ISNULL(DDPAY.REGULARPAYMENT,0) AS REGULARPAYMENT, DDPAY.DDSCHEDULEID, RIGHT('0' + cast(DAY(DDPAY.PAYMENTDATE )as varchar),2) AS PAYMENTDATE, "&_  
	                         "	        C.FIRSTNAME, C.LASTNAME, A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY,  "&_ 
	                         "	        case WHEN FIN.DATERENTSET < TR.CUTOFFDATE  then 1  else 0 end SETRENTTO, TR.TARGETRENTID,TR.REASSESMENTDATE,  "&_ 
	                         "	        ISNULL(ADNL.GARAGE_COUNT,0) AS GARAGE_COUNT, ISNULL(TR.OLDGARAGECHARGE,0) AS OLDGARAGECHARGE ,ISNULL(TR.NEWGARAGECHARGE,0) AS NEWGARAGECHARGE ,  "&_ 
	                         "	        ISNULL(TR.RENT,0) AS T_RENT, ISNULL(TR.SERVICES,0) AS T_SERVICES, ISNULL(TR.COUNCILTAX,0) AS T_COUNCILTAX, ISNULL(TR.WATERRATES,0) AS T_WATERRATES, ISNULL(TR.INELIGSERV,0) AS T_INELIGSERV, ISNULL(TR.SUPPORTEDSERVICES,0) AS T_SUPPORTEDSERVICES, "&_
	                         "	        ISNULL(FIN.GARAGE,0) AS GARAGE, ISNULL(ADNL.ADDITIONAL_RENT,0) AS ADDITIONAL_GARAGE, ISNULL(FIN.RENT,0) AS RENT, ISNULL(FIN.SERVICES,0) AS SERVICES, ISNULL(FIN.COUNCILTAX,0) AS COUNCILTAX, ISNULL(FIN.WATERRATES,0) AS WATERRATES, ISNULL(FIN.INELIGSERV,0) AS INELIGSERV , ISNULL(FIN.SUPPORTEDSERVICES,0) AS SUPPORTEDSERVICES,  "&_
	                         "	        (ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ', ' + ISNULL(P.TOWNCITY,'') + ', ' +  ISNULL(P.POSTCODE,'')) AS P_FULLADDRESS,RE.FIRSTNAME +' '+RE.LASTNAME as incomeOfficer,RE.WORKEMAIL, RE.WORKMOBILE "&_ 							
                             "  FROM 	C_TENANCY T  "&_
	                         "		INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID  "&_
	                         "		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID 	"&_ 
	                         "		INNER JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID AND A.ISDEFAULT = 1 "&_ 
	                         "		LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID  "&_
	                         "		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  "&_
							 "OUTER Apply(SELECT top 1 RE.FIRSTNAME,RE.LASTNAME,EC.WORKEMAIL, EC.WORKMOBILE FROM AM_ResourcePatchDevelopment RPD INNER JOIN "&_
							 " AM_Resource R ON RPD.ResourceId = R.ResourceId AND R.IsActive=1 "&_
   "INNER JOIN E__EMPLOYEE RE ON R.EmployeeId=RE.EMPLOYEEID INNER JOIN E_CONTACT EC ON RE.EMPLOYEEID = EC.EMPLOYEEID WHERE RPD.SCHEMEID = P.SCHEMEID "&_
"   AND RPD.IsActive=1 order by ResourcePatchDevelopmentId DESC) AS RE "&_							
	                         "		INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = P.PROPERTYID  "&_
	                         "		INNER JOIN P_TARGETRENT	TR ON TR.PROPERTYID = P.PROPERTYID AND TR.ACTIVE = 1 "&_
	                         "		LEFT JOIN (SELECT DDSCHEDULEID, PAYMENTDATE, TENANCYID, REGULARPAYMENT FROM F_DDSCHEDULE WHERE ISNULL(SUSPEND,0) NOT IN (1) AND ISNULL(TEMPSUSPEND,0) NOT IN(1)) DDPAY ON DDPAY.TENANCYID = T.TENANCYID  "&_
	                         "		LEFT JOIN (SELECT COUNT(*) AS GARAGE_COUNT ,SUM(F.TOTALRENT) AS ADDITIONAL_RENT,A.TENANCYID FROM C_ADDITIONALASSET A INNER JOIN P__PROPERTY P ON P.PROPERTYID = A.PROPERTYID INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID WHERE A.ENDDATE IS NULL GROUP BY A.TENANCYID) ADNL ON ADNL.TENANCYID = T.TENANCYID  "&_							
	                         "  WHERE  T.TENANCYTYPE IN (1,8,13,4,6,18,15,20,30) AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE()) AND CT.ENDDATE IS NULL "&_
                             "  AND TR.TARGETRENTID=(SELECT MAX(TARGETRENTID) FROM dbo.P_TARGETRENT WHERE dbo.P_TARGETRENT.PROPERTYID=P.PROPERTYID) "&_
                             "  AND CT.TENANCYID  = " & itemdetail_id			
'response.write(SQL)						
'response.end()
					Call OpenRS(rsCust, SQL)

					count = 1
					while NOT rsCust.EOF
					 
						'============================
						' Get Income Officer
						'============================
							incomeOfficer		= 	rsCust("incomeOfficer")
							WORKEMAIL		=	rsCust("WORKEMAIL")
							WORKMOBILE		=	RScUST("WORKMOBILE")
						'============================
						' Get Vars
						'============================
							old_garge_charge	=	rsCust("OLDGARAGECHARGE")
							new_garge_charge	=	rsCust("NEWGARAGECHARGE")
							
							GARAGE_COUNT 		=   rsCust("GARAGE_COUNT")
							
							YEARNUMBER			=   rsCust("YEARNUMBER")
							
							REGULARPAYMENT		= 	rsCust("REGULARPAYMENT")
							DDSCHEDULEID		=	rsCust("DDSCHEDULEID")
							DDPAYMENTDATE		=	RScUST("PAYMENTDATE")
							DDPAYMENTDATE		=	DDPAYMENTDATE & "/04/" & YEARNUMBER
							
							DATERENTSET			=   rsCust("SETRENTTO")
							RentEffective		=	rsCust("REASSESMENTDATE")
							Rent 				= 	rsCust("Rent")
							SupportServices 	= 	rsCust("SUPPORTEDSERVICES")
							InelServ			=	rsCust("INELIGSERV")
							OtherServices		=	rsCust("SERVICES")
							CouncilTax			=	rsCust("COUNCILTAX")
							WaterRates			=	rsCust("WATERRATES")
							ADDITIONAL_GARAGE 	=   rsCust("ADDITIONAL_GARAGE")
							GarageSpace			=	rsCust("OLDGARAGECHARGE")
							'Updated by:Umair
							'Updated Reason:Not picking old garage rent
							'Update Done:10-Feb-2010
							'Update Example:Tenancyid's:123129,012238
							'Update start
							if(GarageSpace=0) Then
							    GarageSpace			= 	GarageSpace + ADDITIONAL_GARAGE
							end if
							'Update end
							
							totalrent			=	rsCust("TOTALRENT") + ADDITIONAL_GARAGE
							
							T_Rent 				= 	rsCust("T_Rent")
							T_SupportServices 	= 	rsCust("T_SUPPORTEDSERVICES")
							T_InelServ			=	rsCust("T_INELIGSERV")
							T_OtherServices		=	rsCust("T_SERVICES")
							T_CouncilTax		=	rsCust("T_COUNCILTAX")
							T_WaterRates		=	rsCust("T_WATERRATES")
							T_GarageSpace		=	rsCust("NEWGARAGECHARGE")			
						
							if T_GarageSpace > 0 then
								'rw "dev testing by jim : garage stnd"
								T_GarageSpace = new_garge_charge
							end if
							
							if ADDITIONAL_GARAGE > 0 then
								'rw "<BR>dev testing by jim : garage addtnl"
								t_ADDITIONAL_GARAGE = new_garge_charge
							else
								t_ADDITIONAL_GARAGE = ADDITIONAL_GARAGE
							end if
														
							T_totalrent			=   T_Rent + T_SupportServices + T_InelServ + T_OtherServices + T_CouncilTax + T_WaterRates + T_GarageSpace '+ t_ADDITIONAL_GARAGE

							'T_GarageSpace		= 	T_GarageSpace + t_ADDITIONAL_GARAGE
							
							p_fulladdress		=	rsCust("P_FULLADDRESS")
							

							
							targetrentid		=	rsCust("TARGETRENTID")
							
						'===============================================================================
						'	Start Building name and address info for letter
						'===============================================================================
						
						''''''''''''''''''''
						' Build Names First
						''''''''''''''''''''
						
							ShortName = ""
							StringName = ""
													
							'TITLE
							Title = rsCust("TITLE")
							if (Title <> "" AND NOT isNull(Title)) then
								StringName = StringName & Title & " "
								ShortName = ShortName & Title & " "
							end if
							
							'FIRSTNAME
							FirstName = rsCust("FIRSTNAME")
							if (FirstName <> "" AND NOT isNull(FirstName)) then
								StringName = StringName & FirstName & " "
							end if
							
							'LASTNAME
							LastName =rsCust("LASTNAME")
							if (LastName <> "" AND NOT isNull(LastName)) then
								StringName = StringName & LastName & " "
								ShortName = ShortName & LastName
							end if
							
							' DEAR LINE IN LETTER
							if (count = 1) then
								StringFullName = StringName
								str_Dear = "<br/>Dear " & ShortName
								count = 2							
						    else
								StringFullName = StringFullName & " <br/>and " & StringName
								str_Dear = str_Dear & " and " & ShortName
							end if
							
							rsCust.moveNext
						wend
					
					'''''''''''''''''''''''
					' End Build Names First
					'''''''''''''''''''''''
					
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''
						
						if (count = 2) then
							rsCust.moveFirst()
							housenumber = rsCust("housenumber")
							if (housenumber = "" or isNull(housenumber)) then
								'housenumber = rsCust("flatnumber")
							end if
							if (housenumber = "" or isNull(housenumber)) then
								FirstLineOfAddress = rsCust("Address1")
							else
								FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
							end if
					
							RemainingAddressString = ""
							AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
							for i=0 to Ubound(AddressArray)
								temp = rsCust(AddressArray(i))
								if (temp <> "" or NOT isNull(temp)) then
									RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font:12PT ARIAL;padding-left:20mm;'>" & temp & "</TD></TR>"
								end if
							next
							tenancyref = rsCust("TENANCYID")
						end if
						
						' now combine the name and address to give a post label
						FullAddress = "<TR><TD nowrap style='font:12PT ARIAL; padding-left:20mm;'>" & StringFullName & "</TD><td nowrap style='text-align:right; '><span style='width:150px;font:12PT ARIAL'>Your Tenancy Ref: <b>" & TenancyReference(tenancyref) & "</b></span></td></TR>" &_
									"<TR><TD nowrap style='font:12PT ARIAL;padding-left:20mm;'>" & FirstLineOfAddress & "</TD><td style='text-align:right; font:12PT ARIAL'>Date: 20 February " & YEARNUMBER & "</td></TR>" &_
									RemainingAddressString 
								
					'''''''''''''''''''''''
					' Build Address detail
					'''''''''''''''''''''''				
								
					'========================================================================================
					'	End building name and address info
					'========================================================================================
					
			
					DoPageBreak2 = "style='page-break-after: always; '"
					
					SingleLetterBody = SingleLetterBody &  "<div style='margin-top:0px !important;'><p " & DoPageBreak &  "><table width=100% border=0 style='height:21cm'>"
					SingleLetterBody = SingleLetterBody &  "<TBODY ID='LETTERDIV'><tr><td style='font:12PT ARIAL'><div border=0 id='return_address' style='overflow:hidden;' ><div style='font:14PT ARIAL;float:left;padding-top:50px;'>Your Rent Reassessment </div><div style='float:right;'>&nbsp; <img src='/myImages/LOGOLETTER.gif' style='text-align:right; width:138px; height:117px; ' /></div></div></td>"
					SingleLetterBody = SingleLetterBody &  "</tr><tr><td nowrap valign=top><table width='100%'><tr><td nowrap style='text-align:right; '><span style='width:150px;font:12PT ARIAL'></span></td></tr><tr><td style='text-align:right; font:12PT ARIAL;padding-left:20mm;'> </td></tr>" & FullAddress 
					SingleLetterBody = SingleLetterBody &  "</table></td></tr>"
					SingleLetterBody = SingleLetterBody &"<tr><td style='font:12PT ARIAL' ><div style='width:60%; border-right:2px solid black;float:left'><table width='100%'> "
					' Dear Line
					SingleLetterBody = SingleLetterBody &  "<tr><td style='font:12PT ARIAL' >" & str_Dear & "</td></tr>"
					'explanation text
					SingleLetterBody = SingleLetterBody & "<tr style='height:10px'><td > </td></tr><tr id='LetterContent1' style='display:block'><td style='font:12PT ARIAL' valign='top' class='RSLBlack><div id=letterText>"
					SingleLetterBody = SingleLetterBody & "<span style='font:12PT ARIAL'>"
					
'// Increase Date -- needs to come from database
	
					IncreaseDate_HTML = ""
					IncreaseDate_HTML =YEARNUMBER
					
'// RENT FIGURES TABLE - PLACE WHERE USER HAS ENTERED [R]

					RentAccountInfo_HTML = " " & p_fulladdress & "<br>"

					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<table  border='0' cellspacing='0' cellpadding='0'>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td style='width:442'></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='18' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='right' valign='top'><b>Existing Rent</b></td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='37' style='font:12PT ARIAL'>&nbsp;</td>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='110' valign='top' align='right'><b>New Rent wef " & RentEffective  &  "</b></td>"					
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & "</table>"
				IF DATERENTSET <> "1" THEN
				t_Rent = Rent
				t_SupportServices = SupportServices
				t_InelServ = InelServ
				t_OtherServices = OtherServices
				t_CouncilTax = CouncilTax
				t_WaterRates = WaterRates
				t_garageSpace = garageSpace
				t_totalrent=totalrent
				END IF

					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table style='font:12PT ARIAL;width:100%'>"
										
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td style='font:12PT ARIAL;width:100%' colspan='5'>"
	
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<table width='100%'  border='0' cellspacing='0' cellpadding='0'>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr style='margin:2px;'>"					
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td width='10' style='font:12PT ARIAL'>&nbsp;</td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='center' valign='top' style='margin:2px;font:12PT ARIAL; text-align:center; border:1px solid black;'><b>Existing</b></td>"
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<td align='center' valign='top' style='margin:2px;font:12PT ARIAL;text-align:center;border:1px solid black;'><b>New Rent </td>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "</tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Rent</span></td> <td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(Rent,2)& "</td> <td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_Rent,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "<tr><td><span style='font:12PT ARIAL'>Support Services </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(SupportServices,2)& "</td><td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_SupportServices,2) & "</td></tr>"
					'RentAccountInfo_HTML = RentAccountInfo_HTML & " <tr><td><span style='font:12PT ARIAL'>Ineligible Services </span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Services Ineligible for HB</span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(InelServ,2) & "</td> <td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_InelServ,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML & "  <tr><td><span style='font:12PT ARIAL'>Other Services </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(OtherServices,2) & "</td><td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_OtherServices,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Council Tax </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(CouncilTax,2) & "</td><td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_CouncilTax,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>Water Rates </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(WaterRates,2) & "</td><td align='Right' style='font:12PT ARIAL'>" &FormatCurrency(t_WaterRates,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr style='background-color:#b3ffff;'><td><span style='font:12PT ARIAL'>Garage/Car Space </span></td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(garageSpace,2) & "</td><td align='Right' style='font:12PT ARIAL'>" & FormatCurrency(t_GarageSpace,2) & "</td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td><span style='font:12PT ARIAL'>&nbsp;</span></td></tr>"
					RentAccountInfo_HTML = RentAccountInfo_HTML &         " <tr><td style='padding: 2px;border: 1px solid black;border-right: none;'><span style='font:12PT ARIAL; '>Total Rent Payable</span></td><td align='Right' style='padding: 2px;font:12PT ARIAL;border: 1px solid black;border-right: none;border-left: none;'>" & FormatCurrency(totalrent,2) & "</td><td align='Right' style='padding: 2px;font:12PT ARIAL;border: 1px solid black;border-left: none;'><b>" & FormatCurrency(t_totalrent,2) & "</b></td></tr>"
					
					RentAccountInfo_HTML = RentAccountInfo_HTML &         "</table>"
					
				
										
					
					RentAccountInfo_HTML = RentAccountInfo_HTML & 	"</td></tr></table>"


' PAGE BREAK			
					PageBreak_HTML = ""
					PageBreak_HTML = PageBreak_HTML &	"<p style='page-break-after: always'></p>"
					
	
' DD ACCOUNT HTML

					' SHOW DD PAY AGREEMENT IF TENANT PAYS THAT WAY
					DDAccountInfo_HTML = ""
					'IF DDSCHEDULEID <> "" THEN
					'	isDDCustomer = 1
					'	if REGULARPAYMENT = totalrent then		
					'		FullDDMatch = 1
					'		DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
					'		DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><br>Your Direct Debit Payment has been automatically adjusted to reflect the amount of new rent you need to pay to Broadland Housing Association.<BR><BR>The amount of money that will be collected will be changing from " & FormatCurrency(totalrent,2) & " to " & FormatCurrency(t_totalrent,2) & " with effect from " & RentEffective  & "<BR><BR>On the " & DDPAYMENTDATE  & " we will collect " & FormatCurrency(t_totalrent,2) & " and " & FormatCurrency(t_totalrent,2) & " each month thereafter until further notice. "
					'	else
					'		FullDDMatch = 0
					'		DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
							'DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><br>Your Direct Debit has not been automatically adjusted, therefore your payment on " & DDPAYMENTDATE  & " will remain at " & FormatCurrency(REGULARPAYMENT,2) & ", until further notice.<br><br>You will shortly receive confirmation of your new Direct debit amount, to be effective from April " & YEARNUMBER  & ". "
                     '       DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br/>Your Direct Debit has not been automatically adjusted, therefore your payment on " & DDPAYMENTDATE  & " will remain at " & FormatCurrency(REGULARPAYMENT,2) & ", until further notice."
					'	End if
					'ELSE
					'	isDDCustomer = 0
					'	FullDDMatch = 0
					'	DDPAYMENTDATE = "NULL"
					'END IF
					' SHOW DD PAY AGREEMENT IF TENANT PAYS THAT WAY
					DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br><strong>Customers Paying by Direct Debit </strong> "
						
                    DDAccountInfo_HTML = DDAccountInfo_HTML &	 "   <br/>If you pay your rent via Direct Debit, we will make any necessary adjustments and you will be notified separately. " 
				
					'/////////////////
				
					
					sql = "select * from P_RENTINCREASE_LETTERTEXT"
					' this bit puts in the letter
					Call OpenRS(rsLetterBody, SQL)
					MainBodyFromDB ="<div style='width:100%; float:left; font:12PT ARIAL !important;'>" & rsLetterBody("LETTERTEXT") &"</div>"
					Call CloseRS(rsLetterBody)
					
					'REPLACE LETTER BUILDER BOLD TAGS AND REPLACE WITH HTML BOLD TAGS
					MainBodyFromDB = replace(MainBodyFromDB,"[B]","<B>")
					MainBodyFromDB = replace(MainBodyFromDB,"[/B]","</B>")
					
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[D]",IncreaseDate_HTML)
					MainBodyFromDB = replace(MainBodyFromDB,"[N]",TheNextYear)
					
					'REPLACE RENT ACCOUNT TAGS WITH RENT ACCOUNT HTML
					MainBodyFromDB = replace(MainBodyFromDB,"[R]",RentAccountInfo_HTML)

					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[DD]",DDAccountInfo_HTML)
					
					'REPLACE DD ACCOUNT WITH DD ACCOUNT INFO LINE
					MainBodyFromDB = replace(MainBodyFromDB,"[PB]",PageBreak_HTML)
					
					SingleLetterBody = SingleLetterBody & MainBodyFromDB
					
					
					'////////////////					

					SingleLetterBody = SingleLetterBody &	 " </td></tr><tr><td> </td></tr><tr style='height:10px'><td style='font:12PT ARIAL'>&nbsp;</td></tr> "
					SingleLetterBody = SingleLetterBody &	 " <tr style='height:20px'><td style='font:12PT ARIAL'><span font:12PT ARIAL>Yours sincerely</span></td></tr> "
					SingleLetterBody = SingleLetterBody &	 "<tr> "
					SingleLetterBody = SingleLetterBody &	 "  <td align=left id='footer' style='visiblity:hidden'><img src='../Images/Ivan_Johnson.gif' width='113' height='85'></td> "
					SingleLetterBody = SingleLetterBody &	 "  </tr> "
					SingleLetterBody = SingleLetterBody &	 "<tr style=' valign='top'> "
					SingleLetterBody = SingleLetterBody &	 "  <td style='font:12PT ARIAL'><span font:12PT ARIAL>Ivan Johnson<BR>Executive Housing Director</span></td> "
					SingleLetterBody = SingleLetterBody &	 " </tr> "					
					SingleLetterBody = SingleLetterBody &	 "<tr> "
					SingleLetterBody = SingleLetterBody &	 "    <td align=right id='footer' style='visiblity:hidden'>&nbsp; </td> "
					SingleLetterBody = SingleLetterBody &	 "</table></div><div style='width:37%;font:12PT ARIAL !important; vertical-align:top;float:left'> <div style='width:100%;'> <div style='width:100%; float: left;'> <div style='margin: 2px;float: left;color: white;background-image: url(/myimages/arrow-iconGrey.png);width: 110px;height: 22px;padding-top: 10px;padding-left: 14px;font:12PX ARIAL;'>Total Current Rent </div> <div style='margin: 2px; float: left; padding: 7px;'><span style='border:2px solid black; font-weight:bold; padding: 5px; font:12PT ARIAL; '>" & FormatCurrency(totalrent,2) & "</span></div> </div> <div style='width:100%; float: left;'> <div style='margin: 2px; float: left;color: white;background-image: url(/myimages/arrow-iconGreen.png);width: 110px;height: 22px;padding-top: 10px;padding-left: 14px;font:12PX ARIAL;'>New Total Rent </div> <div style='margin: 2px; float: left; padding: 7px;'><span style='border:2px solid black; font-weight:bold; padding: 5px; font:12PT ARIAL; '>" & FormatCurrency(t_totalrent,2) & "</span></div> </div> <div style='width:100%; float: left; text-align: center;margin-top:3px; margin-left: 5px;'><span style='font:14PT ARIAL; '>How to get in touch:</span></div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconlogin.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Login to Tenants Online<br/></span><span style='font:12PT ARIAL; '>www.broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconTel.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Call us<br />0303 303 0003</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconMail.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Email us<br />rent@broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconPC.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Visit us online <br />www.broadlandgroup.org</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconFB.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Find us on Facebook <br />@Broadland</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconTwt.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Find us on Twitter <br />@BroadlandHsg</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px;'> <div style='width:10%; margin-right:10px; float:left;'><img src='/myimages/iconEdit.png' width='30px' height='30px' /></div> <div style='width:80%; margin-left:10px; float:left;'><span style='font:12PT ARIAL; '>Write to us at<br />Broadland Housing Group<br />NCFC<br />Carrow Road<br />Norwich<br />NR1 1HU</span></div> </div> <div style='width:100%; float: left; margin-top:3px; margin-left: 5px; line-height:20px; font:12PT ARIAL;'><span style='font:12PT ARIAL; font-weight:bold;'> Ways to pay us</span><br /><br /><b>Login to Tenants Online:</b><br /><span>www.broadlandgroup.org</span><br /><b>Direct Debit:</b> Call 0303 303 0003<br /><b>Standing Order:</b><br/> Sort code: 60-15-31<br />Account: 21481229<br />Quote Ref: <b>"&TenancyReference(tenancyref)&"</b><br />For alternative payment methods <br />please contact us.<br/> <br/> <br/> </div> </div><div style='width:99%;float: left;margin-left:-5px; height:45mm;background: white;'>&nbsp;</div> <div id='newRent' style='width:100%;float: left; background-color:#eaf2ff;padding:3px; margin: 5px; text-align:left; font:12PT ARIAL;'>Your new total rent shown is payable from 01/04/2018. If you have any problems paying your rent please contact your Income Officer, <b>"&incomeOfficer&"</b> by calling <b>"&WORKMOBILE&"</b> or email <span style='font: 10PT ARIAL !important; font-weight:bold  !important;'>"&WORKEMAIL&"</span></div><div id='bottom' style='width:99%;margin-top:530px; float: left; margin-right: 10px; text-align:right; font:12px ARIAL;'>Part of the Broadland Housing Group,<br />incorporating Broadland Housing<br />Association Limited, Broadland Merdian,<br />Broadland St Benedicts Limited and<br />Broadland Development Services.<br /><br />Broadland Housing Association Limited:<br />Registered address NCFC, Carrow Road,<br />Norwich, NR1 1HU. Registered in England<br />and Wales as a Registered Society under<br />the Co-operative and Community Benefit<br />Societies Act 2014 as a non profit making<br />housing association with charitable status.<br />Registered Society No. 16274R. HCA<br />Registration No. L0026<br /></div> <div style='width:100%; float: left; margin-left: 10px; text-align:right; font:10PT ARIAL;'><img src='/myimages/CapturePrint.JPG' width='200px' height='40px'</div></div> "
					SingleLetterBody = SingleLetterBody &	 "</td></tr></table></p><div>"
					

					if not sel_split(icount-1) = key then

						SingleLetterBody= SingleLetterBody & "<p style='page-break-after: always'> &nbsp;</p>"
					else
						SingleLetterBody= SingleLetterBody &	 ""
					End If				
					
					LetterBody = LetterBody  & SingleLetterBody
					'response.Write(LetterBody)
					'response.end()

					letterBody_forStore = SingleLetterBody
					
					SingleLetterBody = ""
					StringName 	= ""
					Title 		= ""
					ShortName 	= ""
					FirstName 	= ""
					LastName 	= ""
					StringFullName = ""
					str_Dear 	= ""
					MainBodyFromDB = ""
					
					end if

					select case PrintAction
						
						case "FirstPrint"

							STRsql = " exec C_RENTLETTER_RECORDPRINT " & itemdetail_id & "," & targetrentid & ",'" & replace(letterBody_forStore,"'","''") & "',1"
							Conn.Execute(strSQL)
						case "RePrint"
							STRsql = " exec C_RENTLETTER_RECORDPRINT " & itemdetail_id & "," & targetrentid & ",'" & replace(letterBody_forStore,"'","''") & "',0"
							Conn.Execute(strSQL)	
					end select
					
					
					HISTORY_SQL = 	" INSERT INTO C_RENTLETTER_HISTORY (	" &_
					"										TENANCYID	,	TARGETRENTID, ITEMIDD	,	ITEMNATUREID	,	CURRENTITEMSTATUSID	, " &_
					"										RENTEFFECTIVEDATE	,	PROPERTYADDRESS	,	RENT	,	SUPPORTEDSERVICES	, " &_
					"										INELIGSERV	,	OTHERSERVICES	,	COUNCILTAX	,	WATER	,	GARAGE	,	TOTALRENT	,	NEW_RENT	, " &_
					"										NEW_SUPPORTEDSERVICES	,	NEW_INELIGSERV	,	NEW_OTHERSERVICES	,	NEW_COUNCILTAX	,	NEW_WATER	, " &_
					"										NEW_GARAGE	,	NEW_TOTALRENT	,	IS_DD_CUSTOMER	,	FULL_DD_MATCH_ON_RENT	,	DD_PAYMENTDATE	, DD_REGULARPAYMENT, " &_
					"										USERID	)" &_
					"									VALUES " &_
					"									( " &_
					"										" & itemdetail_id & "	, " & targetrentid & " ,	2	,	41	,	23,	" &_
					"										'" & RentEffective & "'	,	'" & replace(p_fulladdress,"'","''") & "'	,	" & Rent & "	,	" & SupportServices	& ", " &_
					"										"  & InelServ  & "	,	" & OtherServices & "	,	" & CouncilTax & "	,	" & WaterRates & "	,	" & GarageSpace & "	,	" & TotalRent & "	,	" & t_rent & "	, " &_
					"										" & t_SupportServices & "	,	" & t_InelServ &  "	,	" & t_OtherServices & "	,	" & t_CouncilTax & "	,	" & t_WaterRates & "	, " &_
					"										" & GarageSpace & "	,	" & t_totalrent	& ",	" & isDDCustomer & "	,	" & FullDDMatch & "	,	" & DDPAYMENTDATE & "	, " & REGULARPAYMENT & "	,	" &_
					"										" & Session("userid") & ")"
					
					'Conn.Execute(HISTORY_SQL)

'' GET THE ID ABOVE AND THEN SAVE IN A HISTORY HTML TABLE -- WITH THE FULL LETTER

		Next
	
	End Function
		
%>
<html>
<head>  <meta http-equiv="X-UA-Compatible" content="IE=10" /></head>

<script language="Javascript">

	function PrintMe(){
		 var dvTarget = document.getElementById("printContent");
            var printWindow;
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            if (isChrome) {
			//document.getElementById('bottom').style.marginTop = '470px';
                printWindow = window.open('chrome://test/content/scanWindow.xul', 'Scan', '**chrome,width=850,height=150,centerscreen**');
            } else {
                printWindow = window.open('', '', 'left=0,top=0,toolbar=0,sta­tus=0,scrollbars=1');
            }

            printWindow.document.write(dvTarget.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
		
		//printFrame(this);
		//window.print();
		}
	
	function ReturnData(){
		parent.str_idlist = ""
		parent.location.href = "RentLetterDevelopmentList.asp"
		}		
	
</script>
<style type="text/css">
.thisHere{
	position:relative;
	top:-100px;
	-webkit-print-color-adjust:exact;
}
@page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin: 5mm 5mm 5mm 5mm;  
} 
</style>
<body class="thisHere" onLoad="PrintMe();">
<form name="RSLFORM" method="get">
<%'<input name="PrintLetter" id="PrintLetter" style="display:block" value="Print Letter" type="button" onClick="PrintMe()">
%>
<DIV id="printContent">
<%=LetterBody%>
</DIV>
</form>
</body>
</html>
