<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	CONST BROADLAND = 2
	CONST TENANT = 1
	Dim intTo, intFrom, str_recipient_address, str_return_address, str_Dear, str_Footer, showHF
	
	intTo 	= Cint(Request("to"))
	intFrom	= Cint(Request("from"))
	showHF = Request("showHF")
	
	OpenDB()
	
	' DETERMINE WHICH RECIPIENT ADDRESS TO USE
	if intTo = TENANT Then
		str_recipient_address = "<TR><TD style='BORDER-RIGHT: #133e71 1px solid; BORDER-TOP: #133e71 1px solid; BORDER-LEFT: #133e71 1px solid; BORDER-BOTTOM: #133e71 1px solid;font:12PT ARIAL' noWrap>Tenancy Ref: <B>654987</B></TD></TR>" &_
			"<TR style='FONT:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL'>15/07/2004<BR><br><br><br>Mr Paul Jones </TD></TR>" &_
			"<TR style='font:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL'>1 Maypole Court</TD></TR>" &_
			"<TR style='font:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL'>Kings Lynn</TD></TR>" &_
			"<TR style='font:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL' noWrap>Norwich</TD></TR>" &_
			"<TR style='font:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL'>Norfolk</TD></TR>" &_
			"<TR style='font:12PT ARIAL'>" &_
			"<TD style='font:12PT ARIAL'>NW34 3TE</TD>" &_
		"</TR>"
		str_Dear = "<BR>Dear Mr Jones"
	Else
		str_recipient_address = get_address(intTo)
		str_Dear = "<BR>Dear Sir/Madam"
	End If

	' DETERMINE WHICH RETURN ADDRESS TO USE
	if intFrom = BROADLAND Then
		If showHF Then
			str_return_address = "<img src='/Customer/Images/BHALOGOLETTER.gif' width='145' height='113'>"
			str_Footer = "<table width='95%' border='0' cellspacing='2' cellpadding='1'>" &_
						"<tr><td style='font:8;color:#b31b34'>Broadland Housing Association</td></tr>" &_
						"<tr><td style='font:8;color:#b31b34''>NCFC Jarrold Stand</td></tr>" &_
						 "<tr><td style='font:8;color:#b31b34''>Carrow Road</td></tr>" &_
						 "<tr><td style='font:8;color:#b31b34''>Norwich NR1 1HU</td></tr>" &_
						 "<tr><td style='font:8;color:#b31b34'>tel 01603 750200</td></tr>" &_
						 "<tr><td style='font:8;color:#b31b34'>fax 01603 750222</td></tr>" &_
						 "<tr><td style='font:8;color:#b31b34''>www.broadlandhousing.org</td></tr>" &_					 
						 "<tr><td style='font:7;color:#133e71''>" &_
							"Reqistered under the Industrial and<BR>Provident Societies Act 1965 as a non<BR>" &_
							"profit making housing association with<BR>charitable status. Reg No. 16274R " &_
							"Housing Corporation Reg. No. L0026" &_						
							"</td></tr>" &_					 
						 "</table>"
		End If
	Else
		str_return_address = get_address(intFrom)
	End If

	
	CloseDB()
		
	Function get_address(int_addressid)
	
		Dim str_address
		
		SQL = "SELECT 	ISNULL(ADDRESS1,'') AS ADD1, " &_
				"		ISNULL(ADDRESS2,'') AS ADD2, " &_
				"		ISNULL(ADDRESS3,'') AS ADD3, " &_
				"		ISNULL(TOWNCITY,'') AS ADD4, " &_
				"		ISNULL(COUNTY,'') AS ADD5, " &_
				"		ISNULL(POSTCODE,'') AS ADD6 " &_
				"FROM 	C_STANDARDLETTERADDRESSES WHERE ADDRESSID = " & int_addressid
				
		'response.write SQL
		Call OpenRS(rsAdd, SQL)
	
		count = 1
		while NOT rsAdd.EOF
			
			'RemainingAddressString = "<TABLE>"
			AddressArray = Array("ADD1", "ADD2", "ADD3", "ADD4", "ADD5", "ADD6")
			for i=0 to Ubound(AddressArray)
				temp = rsAdd(AddressArray(i))
				if (temp <> "" and not isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "<TR><TD nowrap style='font:12PT ARIAL''>sss" & temp & "dgdfg</TD></TR>"
				end if
			next
		rsAdd.movenext
		Wend
		'RemainingAddressString = RemainingAddressString & "</TABLE>"
		CloseRS(rsAdd)
		get_address = RemainingAddressString
	
	End Function
%>
<HTML>
<HEAD>
<TITLE>Arreas Letter:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function PrintMe(){
		
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("CloseButton").style.visibility = "hidden";
		print()
		document.getElementById("CloseButton").style.visibility = "visible";
		document.getElementById("PrintButton").style.visibility = "visible";
	}
	
	function hide_borders(){
	
		var coll = document.all.tags("INPUT");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	coll[i].style.border = "none"
	}				
	
 	function load_me(){
	
		var str = opener.document.getElementById("lettertext").innerHTML
		str = str.replace(/\r|\f/g, "<BR>");
		str = str.replace(/'D\d'/g, "<B>DD/MM/YYYY</B>");
		str = str.replace(/'I\d'/g, "<B>NNN</B>");
		str = str.replace(/'T\d'/g, "<B>XXX</B>");
		str = str.replace(/\[\d\]/g, "<B>Auto</B>");
		LETTER.innerHTML = str
		
}
 
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=0 onload="load_me()" class='ta'>
  <table width=100% border=0 style='height:24cm'>
  <tr> 
    <td nowrap valign=top> 
      
	  <table border=0>
        <tr>
          <td nowrap height="261">&nbsp;</td>
        </tr>
		<%=str_recipient_address%>
      </table>
	  
    </td>
    <td align=right width=100% valign=top>&nbsp;</td>
    <td align=right valign=top> 
      <TABLE>
        <TR> 
          <TD style='font:12PT ARIAL''><%=str_return_address%></TD>
        </TR>
        <TR>
          <TD height="14">&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TABLE>
    </td>
  </tr>
  <tr>
    <td colspan=3 style='font:12PT ARIAL''><%=str_Dear%></td>
  </tr>
  <tr style='height:10px'>
    <td colspan=3> </td>
  </tr>
  <TR style='font:12PT ARIAL''> 
    <TD id='LETTER' colspan=3 style='font:12PT ARIAL''></TD>
  </TR>
  <tr>
    <td colspan=3></td>
  </tr>
  <tr style='height:60px'>
    <td colspan=3 style='font:12PT ARIAL''>Yours sincerely</td>
  </tr>
  <tr style='height:60px'>
    <td colspan=3>&nbsp;</td>
  </tr>
  <tr> 
    <td colspan=2 style='font:12PT ARIAL''><b>Employee Name<BR>
      Housing Officer<br>Direct Dial<BR>Email</b></td>
    <td align=right> 
        <input type="button" value=" Print " onclick="PrintMe()" title='Print Letter' class="RSLButton" name="PrintButton">
	    <input type="button" value="Close" onclick="window.close()" title='Close Me' class="RSLButton" name="CloseButton">
    </td>
  </tr>
 <tr> 
      <td height=100%>&nbsp;</td>
</tr>
  <tr>
    <td colspan=2>&nbsp;</td>
    <td align=right >
      	<%=str_Footer%>
    </td>
  </tr>
</table>
</BODY>
</HTML>

