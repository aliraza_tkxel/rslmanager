<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
   Dim FileName

	SetUpBy = Session("FirstName") + " "  + Session("LastName")
	SetUp = FormatDateTime(now(),2)
	FirstPayment = FormatCurrency(request("FirstPayment"),2)
	FirstPaymentDate = request("FirstPaymentDate")
	NextPayment = FormatCurrency(request("NextPayment"),2)
	PaymentDay = request("PaymentDay") + " day of every month"
	strACTION  = Request("strACTION")

	SQL = "SELECT PAYMENTDAY + '' + DAYALIAS as payday FROM F_DD_PAYMENTDAY WHERE PAYMENTDAY = day('" & FormatDateTime(request("FirstPaymentDate"),1) & "')"
	Call OpenDB()
	Call OpenRs(rsDDinfo, SQL)
		PaymentDay = rsDDinfo("payday")
	Call CloseRs(rsDDinfo)
	Call CloseDB()
%>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<script type="text/javascript" language="javascript">

    function ConfirmChanges() {
        var opener = window.dialogArguments;
        opener.SaveForm("<%=strACTION %>");
        close()
    }

</script>
<body bgcolor="#FFFFFF" margintop="0" marginheight="0" topmargin="3" onload="window.focus()">
    <div id="maincontent_popup">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
            <tr>
                <td valign="top" style='font-size: 16'>
                    <br>
                    <b>Confirm Amendments to Direct Debit</b><br>
                    <br>
                </td>
            </tr>
            <tr>
                <td height="130" valign="top" style='border-top: 1px solid #133E71; border-bottom: 1px solid #133E71;
                    padding-left: 10px;'>
                    <table>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Set up by:</b>
                            </td>
                            <td>
                                <%=SetUpBy%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Set Up:</b>
                            </td>
                            <td>
                                <%=SetUp%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>1st Payment:</b>
                            </td>
                            <td>
                                <%=FirstPayment %>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>On:</b>
                            </td>
                            <td>
                                <%=FirstPaymentDate%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Subsequent</b>
                            </td>
                            <td>
                                <%=NextPayment  %>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td height="5">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>On:</b>
                            </td>
                            <td>
                                On
                                <%=PaymentDay %>
                                day of every month.
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="40" align="right" valign="middle" style="padding-left: 10px">
                    <table>
                        <tr>
                            <td>
                                <input type="button" name="ViewProperties" id="ViewProperties" class="RSLButton"
                                    style="display: block" onclick="ConfirmChanges()" value="Confirm Changes" />
                            </td>
                            <td>
                                <input type="button" name="button" class="RSLButton" onclick="window.close()" value="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <iframe name="ServerFrame" style="display: none"></iframe>
</body>
</html>
