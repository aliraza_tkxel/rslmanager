<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'' Modified By:	Munawar Nadeem(TkXel)
	'' Created On: 	July 6, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim training_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, lstActivity, lstKeyWorker
    Dim type_id
    Dim keyworker_id
    Dim activity_dt

	'Code Changes Start by TkXel=================================================================================

	Dim journalID, enqID, closeFlag , readFlag, serviceId

	'Code Changes End by TkXel=================================================================================

	path = request.form("hid_go")
	training_history_id = Request("traininghistoryid")

	'begin processing
	Call OpenDB()

	If path = "" Then path = 0 End If ' initial entry to page

	If path = 0 Then

		Call get_record()
		Call get_enquiry_id(journalID, enqID)

	ElseIf path = 1 Then

		Call new_record()

	'Code Changes Start by TkXel=================================================================================
		'Checking whether SHOW TO TENANT checkbox is selected or not
		'If selected then response would be sent to Message Alert System
		If Request.Form("chk_SHOW_TENANT") = 1 Then
			readFlag = 0
			'Value of journalID  is already obtained in new_record()
			'obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			'If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			If enqID <> "" Then
				'Enters only if enquiry is generated from Enquiry Management Area
				'Checking whether CLOSE ENQUIRY checkbox is selected or not
				If Request.Form("chk_CLOSE") = 1 Then
					closeFlag = 1
				Else
					closeFlag = 0
				End if
				'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
				Call save_response_tenant(enqID, closeFlag,readFlag, serviceId)
			End If
		ElseIf Request.Form("chk_CLOSE") = 1 Then
 			readFlag = 1
			closeFlag = 1
			'Value of journalID  is already obtained in new_record()
			'obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
				'If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
				If enqID <> "" Then
					'Enters only if enquiry is generated from Enquiry Management Area
					'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables
					'Call save_response_tenant( enqID, closeFlag) 
					Call save_response_tenant(enqID, closeFlag, readFlag, serviceId)
			    End If
		   End If

		Response.Redirect ("../iFrames/iTrainingEmploymentDetail.asp?journalid=" & journalid & "&SyncTabs=1")

	'Code Changes End by TkXel=================================================================================

	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT     JJ.JOURNALID,JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') As CREATIONDATE " &_
					"			, E.FIRSTNAME + ' ' + E.LASTNAME As FULLNAME " &_
					"			, G.ASSIGNTO " &_
					"			, JD.TEAM " &_
					"			, ISNULL(G.NOTES, 'N/A') As NOTES " &_
					"			, Case When Len(J.TITLE) > 40 Then Left(J.TITLE,40) + '...' Else IsNull(J.TITLE,IsNull(NST.DESCRIPTION,'No Title')) End As TITLE " &_
					"			, G.traininghistoryid " &_
					"			, J.ITEMNATUREID " &_
					"			, G.ITEMACTIONID " &_
					"			, S.DESCRIPTION As STATUS " &_
                    "           , G.NATURESUBTYPEID As NatureType " &_
                    "           , G.NATUREACTIVITYID As Activity " &_
                    "           , G.KEYWORKERID " &_
                    "           , G.ACTIVITYDATE " &_
					"FROM		C_TRAININGEMPLOYMENT G " &_
                    "           INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON G.ASSIGNTO = E2.EMPLOYEEID " &_
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_
					"			LEFT JOIN C_NATURESUBTYPE NST ON G.NATURESUBTYPEID = NST.NATURESUBTYPEID " &_
					"			LEFT JOIN C_NATUREACTIVITY NA ON G.NATUREACTIVITYID = NA.NATUREACTIVITYID " &_
					"WHERE 		traininghistoryid = " & training_history_id

		Call OpenRs(rsSet, strSQL)

		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		fullname	= rsSet("FULLNAME")
		assignto	= rsSet("ASSIGNTO")
		team		= rsSet("TEAM")
		item_id		= rsSet("ITEMID")
		itemnature_id = rsSet("ITEMNATUREID")
		itemaction_id = rsSet("ITEMACTIONID")
		journalID = rsSet("JOURNALID")
        type_id = rsSet("NatureType")
        activity_id = rsSet("Activity")
        keyworker_id = rsSet("KEYWORKERID")
        activity_dt = rsSet("ACTIVITYDATE")

		Call CloseRs(rsSet)

		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & team & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", " style='width:250px' ")
		Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = " & itemnature_id ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", itemaction_id, NULL, "textbox200", " style='width:250px' onchange='action_change()'")

        SQL = "C_NATURESUBTYPE NST " &_
        "INNER JOIN C_NATURESUBTYPETOACTIVITY NNSTA ON NST.NATURESUBTYPEID = NNSTA.NATURESUBTYPEID " &_
        "INNER JOIN C_NATUREACTIVITY NA ON NNSTA.NATUREACTIVITYID = NA.NATUREACTIVITYID " &_
        "WHERE NST.NATURESUBTYPEID = " & type_id
        Call BuildSelect(lstActivity, "sel_NATUREACTIVITY", SQL ,"NA.NATUREACTIVITYID, NA.DESCRIPTION", "NA.DESCRIPTION", "Please Select", activity_id, NULL, "textbox200", " style='width:250px' ")

        SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = 109 AND L.ACTIVE = 1 AND J.ACTIVE = 1"
        Call BuildSelect(lstKeyWorker, "sel_KEYWORKER", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", keyworker_id, NULL, "textbox200", " style='width:250px' ")

    End Function

	Function new_record()

		strSQL = "SELECT JOURNALID FROM C_TRAININGEMPLOYMENT WHERE traininghistoryid = " & training_history_id
		Call OpenRs(rsSet, strSQL)
			journalid = rsSet("JOURNALID")
		Call CloseRS(rsSet)

		notes = Request.form("txt_NOTES")
		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
			Call update_journal(journalid, New_Status)
		Else
			New_Status = 13
		End If

		ItemActionID = Request.Form("sel_ITEMACTIONID")
		If (ItemActionID = "") Then
			ItemActionID = "NULL"
		End If

        activity_id = Request.Form("sel_NATUREACTIVITY")
        If activity_id = "" Then activity_id = "NULL" End If

        subactivity_id = Request.Form("hid_NATURESUBTYPEID")
        If subactivity_id = "" Then subactivity_id = "NULL" End If

        keyworker_id = Request.Form("sel_KEYWORKER")
        If keyworker_id = "" Then keyworker_id = "NULL" End If

        activity_dt = Request.Form("txt_ACTIVITYDATE")
        If activity_dt = "" Then activity_dt = "NULL" Else activity_dt = "'" & Request.Form("txt_ACTIVITYDATE") & "'" End If

		strSQL = 	"SET NOCOUNT ON;INSERT INTO C_TRAININGEMPLOYMENT " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ASSIGNTO, NOTES, NATURESUBTYPEID, NATUREACTIVITYID, KEYWORKERID, ACTIVITYDATE) " &_
					"VALUES (" & journalid & ", " & New_Status & ", " & ItemActionID & ", " & Session("userid") & ", " & Request.Form("sel_USERS") & ", '" & Replace(notes, "'", "''") & "', " & subactivity_id & "," & activity_id & ", " & keyworker_id & "," & activity_dt & ")" &_
					"SELECT SCOPE_IDENTITY() AS traininghistoryid;"

		Set rsSet = Conn.Execute(strSQL)
			traininghistory_id = rsSet.fields("traininghistoryid").value
			serviceId = rsSet.fields("traininghistoryid").value
			rsSet.close()
		Set rsSet = Nothing

		' INSERT LETTER IF ONE EXISTS
		If Request.Form("hid_LETTERTEXT") <> "" Then
			strSQL = "INSERT INTO C_TRAININGCUSTOMERLETTERS (traininghistoryid, LETTERCONTENT) VALUES (" & traininghistory_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "')"
			conn.execute(strSQL)
		End If

		'Response.Redirect ("../iFrames/iTrainingEmploymentDetail.asp?journalid=" & journalid & "&SyncTabs=1")

	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function


	'Code Changes Start by TkXel=================================================================================
	'This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jourID, ByRef enqID)

		'CALL To STORED PROCEDURE TO get enquiry ID
		Set comm = Server.CreateObject("ADODB.Command")
		'setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		'setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jourID
		'executing sproc and storing resutls in record set
		Set rs = comm.execute
		'if record found/returned, setting values
		If Not rs.EOF Then 
			enqID = rs("EnquiryLogID")
		End If
		Set comm = Nothing
		Set rs = Nothing

	End Function


	'This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag,readFlag, serviceId) 

		Set comm = Server.CreateObject("ADODB.Command")
		'setting stored procedure name
		comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		'setting input parameter for sproc
		comm.parameters(1)= enqID
		comm.parameters(2) =  closeFlag
		comm.parameters(3) =  readFlag
		comm.parameters(4) =  serviceId
		'executing sproc
		comm.execute

	End Function

	'Code Changes End by TkXel=================================================================================
%>
<html>
<head>
    <title>RSL Manager -- > Update -- > Training and Employment </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
        FormFields[3] = "sel_ITEMACTIONID|Action|SELECT|N"
        FormFields[4] = "sel_NATUREACTIVITY|Activity|SELECT|Y"
        FormFields[5] = "txt_ACTIVITYDATE|Date|DATE|Y"
        FormFields[6] = "sel_KEYWORKER|KeyWorker|SELECT|Y"

        function save_form() {
            if (!checkForm()) return false
            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = "pTraining.asp"
            document.RSLFORM.target = "CRM_BOTTOM_FRAME"
            document.RSLFORM.submit();
            window.close()
        }

        function action_change() {
            document.RSLFORM.action = "../Serverside/change_letter_action.asp?pagesource=1&ITEMACTIONID=" + document.getElementById("sel_ITEMACTIONID").options[document.getElementById("sel_ITEMACTIONID").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        function GetEmployee_detail() { }

        function GetEmployees() {
            if (document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value != "") {
                document.RSLFORM.action = "../Serverside/GetEmployees.asp?SMALL=1"
                document.RSLFORM.target = "ServerIFrame"
                document.RSLFORM.submit()
            }
            else {
                document.getElementById("dvUsers").innerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200' style='width:250px'><option value=''>Please select a team</option></select>"
            }
        }

        function open_letter() {
            if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
                ManualError("img_LETTERID", "You must first select a letter to view", 1)
                return false;
            }
            var tenancy_id = opener.parent.MASTER_TENANCY_NUM
            window.open("arrears_letter.asp?tenancyid=" + tenancy_id + "&letterid=" + document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

    </script>
</head>
<body onload="window.focus();action_change()">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71;">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style='border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71'>
                <table>
                    <tr valign="top">
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td width="100%">
                            <%=title%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Activity :
                        </td>
                        <td>
                            <%=lstActivity%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NATUREACTIVITY" id="img_NATUREACTIVITY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>Date : </td>
                        <td><input id="txt_ACTIVITYDATE" name="txt_ACTIVITYDATE" class="textbox" style="width: 75px;" value="<%=activity_dt %>" onclick="YY_Calendar('txt_ACTIVITYDATE',0,0,'de','#FFFFFF','#133E71','YY_calendar1')" /></td>
                        <td><img src="/js/FVS.gif" name="img_ACTIVITYDATE" id="img_ACTIVITYDATE" width="15px" height="15px"
                                border="0" alt="" /></td>
                    </tr>
                    <tr>
                        <td>Key Worker : </td>
                        <td><%=lstKEYWORKER %></td>
                        <td><img src="/js/FVS.gif" name="img_KEYWORKER" id="img_KEYWORKER" width="15px" height="15px"
                                border="0" alt="" /></td>
                    </tr>
                    <tr>
                        <td>
                            Team :
                        </td>
                        <td>
                            <%=lstTeams%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Assign To :
                        </td>
                        <td>
                            <div id="dvUsers">
                                <%=lstUsers%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action :
                        </td>
                        <td>
                            <%=lstAction%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Letter :
                        </td>
                        <td>
                            <div id="dvLetter">
                                <select name="sel_LETTER" id="sel_LETTER" class="textbox200" style="width: 250px">
                                    <option value=''>Please select action</option>
                                </select></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Status:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_STATUS" id="txt_STATUS"
                                value="<%=i_status%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style='width: 250' name="txt_RECORDEDBY" id="txt_RECORDEDBY"
                                value="<%=fullname%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 250px" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES" rows="2" cols="10"><%=notes%></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="PreviousAction" id="PreviousAction" value="<%=action%>" />
                            <input type="hidden" name="traininghistoryid" id="traininghistoryid" value="<%=training_history_id%>" />
                            <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                            <input type="hidden" name="hid_NATURESUBTYPEID" id="hid_NATURESUBTYPEID" value="<%=type_id%>" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="center" colspan="2">
                            Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                            &nbsp;
                            <%If(Len(enqID)>0) Then%>
                            Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" id="chk_SHOW_TENANT"
                                value="1" checked="checked" />
                            &nbsp;
                            <%End If%>
                            <input type="button" value="View Letter" class="RSLButton" onclick="open_letter()"
                                name="button" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style="border-bottom: 1px solid #133e71; border-left: 1px solid #133e71">
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    value=" Close " class="RSLButton" style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" title="Save"
                    value=" Save " class="RSLButton" style="cursor: pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <div id="Calendar1" style="background-color: white; position: absolute; left: 1px;
        top: 1px; width: 200px; height: 115px; z-index: 20; visibility: hidden">
    </div>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" style="display: none">
    </iframe>
</body>
</html>
