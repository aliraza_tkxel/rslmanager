<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
//ZANFAR ALI. THIS WHOLE FILE WAS SHAGGED
//NO VALUES WERE BEING SENT OVER DUE TO RENAMED AND DISABLED FIELDS, ETC

	
	
	Dim str_SQL, cusname, action, tenancy_id, customer_id
	Dim propertyref, propertynumber, address1, address2, towncity, postcode, propertytype, tenancyref, lst_type, startdate, enddate
	
	action = "AMEND"
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	
	OpenDB()
	
	// get customer details 
	str_SQL = "SELECT ISNULL(C.FIRSTNAME, '') + ' ' + ISNULL(C.LASTNAME, '') AS FULLNAME " &_
				"FROM	 	C_TENANCY T " &_
				"			LEFT JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID " &_
				"			LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
				"WHERE		T.TENANCYID = " & tenancy_id
				
	Call OpenRs(rsSet, str_SQL)
	
	if not rsSet.EOF Then 
		cusname = rsSet(0) 
	Else 
		cusname = "" 
	End IF
		
	CloseRs(rsSet)
	
	str_SQL = 	"SELECT 	DISTINCT ISNULL(P.PROPERTYID, '') AS PROPERTYREF, " &_
				"			LTRIM(ISNULL(P.HOUSENUMBER , ' ' ) + ' ' + ISNULL(FLATNUMBER,' ')) AS PROPERTYNUMBER,  " &_
				"			ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1,  " &_
				"			ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2,  " &_
				"			ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY,  " &_
				"			ISNULL(P.POSTCODE , ' ' ) AS POSTCODE,  " &_
				"			ISNULL(A.DESCRIPTION, ' ') AS PROPERTYTYPE, " &_
				"			ISNULL(CONVERT(NVARCHAR, T.STARTDATE, 103) ,'') AS STARTDATE, " &_
				"			ISNULL(CONVERT(NVARCHAR, T.ENDDATE, 103) ,'') AS ENDDATE, " &_
				"			ISNULL(TT.TENANCYTYPEID,'') AS TENANCYTYPE, " &_
				"			ISNULL(T.TENANCYID,'') AS TENANCYREF " &_
				"FROM	 	C_TENANCY T " &_
				"			LEFT JOIN C_CUSTOMERTENANCY CT ON T.TENANCYID = CT.TENANCYID " &_
				"			LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
				"			LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
				"			LEFT JOIN P_PROPERTYTYPE A ON P.PROPERTYTYPE = A.PROPERTYTYPEID  " &_
				"			LEFT JOIN C_TENANCYTYPE TT ON T.TENANCYTYPE = TT.TENANCYTYPEID " &_
				"WHERE	 	T.TENANCYID = " & tenancy_id
	//response.write str_SQL
	Call OpenRs(rsSet, str_SQL)
	
	If Not rsSet.EOF Then
	
		propertyref 	= rsSet("PROPERTYREF")
		propertynumber	= rsSet("PROPERTYNUMBER")	
		address1		= rsSet("ADDRESS1")		
		address2		= rsSet("ADDRESS2")		
		towncity		= rsSet("TOWNCITY")		
		postcode		= rsSet("POSTCODE")		
		propertytype	= rsSet("PROPERTYTYPE")
		startdate		= rsSet("STARTDATE")
		enddate			= rsSet("ENDDATE")
		tenancytype		= rsSet("TENANCYTYPE")	
		tenancyref		= rsSet("TENANCYREF")	
		
	End If
	
	CloseRs(rsSet)
	
	Call BuildSelect(lst_type, "sel_TENANCYTYPE_DIS", "C_TENANCYTYPE", "TENANCYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", tenancytype, nULL, "textbox200", " DISABLED ")
		
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Amend Tenancy for <%=cusname%></TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">
	
	var FormFields = new Array()
	FormFields[0] = "sel_TENANCYTYPE|Tenancy Type|SELECT|Y"
	FormFields[1] = "txt_STARTDATE|Start Date|DATE|Y"
	FormFields[2] = "txt_ENDDATE|End Date|DATE|Y"

	// transforms a dd/mm/yyyy into a long date format javascript date
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}

	function SaveForm(){
		
		var answer = false
		
		if (!checkForm()) return false
		if (RSLFORM.txt_ENDDATE.value != ""){
			if (real_date(RSLFORM.txt_ENDDATE.value) < real_date(RSLFORM.txt_STARTDATE.value)){
				alert("The end date you have entered is less than the start date!");
				return false;
				}
			}
		if (RSLFORM.txt_ENDDATE.value != ""){
			answer = window.confirm("You have entered an end date for this tenancy.\nAre you sure you want to end this tenancy ?")
			}
		RSLFORM.target = "frm_ct_srv"
		RSLFORM.action = "../ServerSide/tenancy_svr.asp?tenancyid=<%=tenancy_id%>&close_tenancy="+answer
		RSLFORM.submit()
		}
	
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name="RSLFORM" method="POST">
	<TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
		<tr>
			<td valign="top" height="20">
				<TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
					<TR>					
						<TD ROWSPAN=2 width="79"><img src="../Images/tab_tenancy.gif" width="79" height="20"></TD>
						<TD HEIGHT=19></TD>
					</TR>
					<TR>
						<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=200 HEIGHT=1></TD>
					</TR>
				</TABLE>		
			</td>
		</tr>
		<tr> 
			<td valign="top" STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71;BORDER-BOTTOM:1PX SOLID #133E71'> 
				<BR>
				<table cellspacing=2 cellpadding=2  WIDTH=90% ALIGN=CENTER border=0>
					<TR> 
						<TD>Tenancy Type</TD>
						<TD><%=lst_type%> <image src="/js/FVS.gif" name="img_TENANCYTYPE" width="15px" height="15px" border="0"></TD>
					</TR>
					<TR> 
						<TD COLSPAN=2 ALIGN=CENTER> Start 
						<INPUT TYPE=TEXT NAME=txt_STARTDATE VALUE="<%=startdate%>" CLASS='TEXTBOX100' READONLY>
						<image src="/js/FVS.gif" name="img_STARTDATE" width="15px" height="15px" border="0"> 
						End 
						<INPUT TYPE=TEXT NAME=txt_ENDDATE VALUE="<%=enddate%>" CLASS='TEXTBOX100'>
						<image src="/js/FVS.gif" name="img_ENDDATE" width="15px" height="15px" border="0"> 
						</TD>
					</TR>
				</table>
				<BR>
				<table cellspacing=2 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER>
					<TR>
						<TD>Name</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_NAME  VALUE="<%=cusname%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Tenancy Ref</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_TENANCYREF VALUE="<%=tenancyref%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Property Type</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_PROPERTYTYPE VALUE="<%=propertytype%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Property Ref</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_PROPERTYREF VALUE="<%=propertyref%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Address 1</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_NUMBER VALUE="<%=propertynumber & " " & address1%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Address 2</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_ADD2 VALUE="<%=address2%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Town/City</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_TOWNCITY VALUE="<%=towncity%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
					<TR>
						<TD>Post Code</TD>
						<TD><INPUT TYPE=TEXT NAME=txt_POSTCODE VALUE="<%=postcode%>" CLASS='TEXTBOX200' READONLY></TD>
					</TR>
				</table>
				<BR>
				<table cellspacing=2 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER>
					<TR>
						<TD WIDTH=90%>&nbsp;</td>
						<TD><INPUT TYPE='BUTTON' VALUE='Close' NAME='btn_BACK' CLASS='RSLBUTTON' ONCLICK='window.close()'></TD>
						<TD><INPUT TYPE='BUTTON' VALUE='Close Tenancy' NAME='btn_BACK' CLASS='RSLBUTTON' ONCLICK='SaveForm()'></TD>
					</TR>
				</table>
			<INPUT TYPE=HIDDEN NAME=hid_LASTMODIFIEDBY VALUE=<%=Session("userid")%>>
			<INPUT TYPE=HIDDEN NAME=hid_ACTION VALUE="<%=action%>">
			<INPUT TYPE=HIDDEN NAME=hid_TENANCYID VALUE="<%=tenancy_id%>">
			<INPUT TYPE=HIDDEN NAME=hid_CUSTOMERID VALUE=<%=customer_id%>>
			<!--RENAMED AND PUT THE BELOW TWO HIDDEN VALUES -->
			<INPUT TYPE=HIDDEN NAME=hid_PROPERTYID VALUE="<%=propertyref%>"	>
			<INPUT TYPE=HIDDEN NAME=sel_TENANCYTYPE VALUE=<%=tenancytype%>>
			</td>
		</tr>
	</table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp"  name=frm_ct_srv width=400px height=400px style='display:BLOCK'></iframe> 
</BODY>
</HTML>