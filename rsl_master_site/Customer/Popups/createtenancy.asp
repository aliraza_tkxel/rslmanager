<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim property_id, customer_id, str_SQL, cusname, action, renttype, tenancy_type, end_date
	Dim propertyref, propertynumber, address1, address2, towncity, postcode
	Dim propertytype, tenancyref, lst_type, startdate, enddate, previous_tenancy_id, me_attributes
	Dim rentfrequency

	action = "NEW"
	customer_id = Request("customerid")
	property_id = Request("propertyid")

	Call OpenDB()

	' get customer details
	str_SQL = "SELECT ISNULL(FIRSTNAME, '') + ' ' + ISNULL(LASTNAME, '') AS FULLNAME FROM C__CUSTOMER WHERE CUSTOMERID = " & customer_id
	Call OpenRs(rsSet, str_SQL)
	If Not rsSet.EOF Then cusname = rsSet(0) Else cusname = "" End If
	Call CloseRs(rsSet)

	str_SQL = 	"SELECT 	ISNULL(F.RENTTYPE,'') AS RENTTYPE, ISNULL(P.PROPERTYID, '') AS PROPERTYREF, " &_
				"			LTRIM(ISNULL(P.HOUSENUMBER , ' ' ) + ' ' + ISNULL(FLATNUMBER,' ')) AS PROPERTYNUMBER, " &_
				"			ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
				"			ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
				"			ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
				"			ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
				"			ISNULL(A.DESCRIPTION, ' ') AS PROPERTYTYPE, " &_
				"			ISNULL(TT.DESCRIPTION, 'Unkown please contact supervisor.') AS TENANCYTYPE, " &_
				"			ISNULL(RENTFREQUENCY,1) AS RENTFREQUENCY  " &_
				"FROM 		P__PROPERTY P " &_
				"			LEFT JOIN P_PROPERTYTYPE A ON P.PROPERTYTYPE = A.PROPERTYTYPEID " &_
				"			INNER JOIN P_FINANCIAL F ON F.PROPERTYID = P.PROPERTYID " &_
				"			LEFT JOIN C_TENANCYTYPE TT ON F.RENTTYPE = TT.TENANCYTYPEID " &_
				" WHERE P.PROPERTYID = '" & property_id & "'"

	Call OpenRs(rsSet, str_SQL)

	If Not rsSet.EOF Then

		tenancy_type	= rsSet("TENANCYTYPE")
		renttype 		= rsSet("RENTTYPE")
		propertyref 	= rsSet("PROPERTYREF")
		propertynumber	= rsSet("PROPERTYNUMBER")
		address1		= rsSet("ADDRESS1")
		address2		= rsSet("ADDRESS2")
		towncity		= rsSet("TOWNCITY")
		postcode		= rsSet("POSTCODE")
		propertytype	= rsSet("PROPERTYTYPE")
		rentfrequency	= rsSet("RENTFREQUENCY")

	End If

	Call CloseRs(rsSet)

	tenancyref = "Auto Generated"

	Call BuildSelect(lst_type, "sel_TENANCYTYPE", "C_TENANCYTYPE", "TENANCYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", renttype, NULL, "textbox200", "")
	Call get_available_date()
	Call isMutualExchange()
	Call CloseDB()

	Function get_available_date()

		str_SQL = "SELECT MAX(ENDDATE) AS ENDDATE FROM C_TENANCY WHERE PROPERTYID = '" & property_id & "'"
		Call OpenRs(rsSet, str_SQL)
		If Not rsSet.EOF Then end_date = rsSet("ENDDATE") End If
		Call CloseRs(rsSet)

	End Function

	' CHECK TO SEE IF THIS PROPERTY'S LAST TENANCWA ENDED ON A MUTUAL EXCHANGE
	' IF SO WE MUST AUTO CHECK AND LOCK 'MUTUAL EXCHANGE' CHECK BOX
	Function isMutualExchange()

		Dim isMutualExchangeVar
		SQL = " SELECT 	TOP 1 MUTUALEXCHANGEEND, TENANCYID " &_
			  " FROM 	C_TENANCY WHERE PROPERTYID = '" & property_id & "' AND ENDDATE IS NOT NULL " &_
			  " ORDER BY ENDDATE DESC "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then 
			isMutualExchangeVar	= rsSet("MUTUALEXCHANGEEND")
			previous_tenancy_id	= rsSet("TENANCYID")
		Else
			isMutualExchangeVar	= -1
		End If
		If Cint(isMutualExchangeVar) = 1 Then				' PREVIOUS TENANCY WAS MUTUAL EXCHANGE
			me_attributes		= " checked='checked' onclick='chk_me("&previous_tenancy_id&", 1)' "
		ElseIf isMutualExchangeVar = 0 Then					' PREVIOUS TENANCY WAS NOT MUTUAL EXCHANGE OR DOES NOT EXIST
			me_attributes		= " onclick='chk_me("&previous_tenancy_id&", 2)' "
		ElseIf isMutualExchangeVar = -1 Then				' NO PREVIOUS TENANCY
			me_attributes		= " onclick='chk_me(1, 3)' "
		End If

		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Create Tenancy for
        <%=cusname%></title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()
        FormFields[0] = "sel_TENANCYTYPE|Tenancy Type|SELECT|Y"
        FormFields[1] = "txt_STARTDATE|Start Date|DATE|Y"

        function chk_me(previous_tenancy_id, action) {
            if (action == 1) {
                alert("You may not uncheck this box as the previous tenancy(" + previous_tenancy_id + ") for this property was marked as mutual exchange.");
                document.getElementById("chk_mutual").checked = true;
            }
            else if (action == 2) {
                alert("You may not check this box as the previous tenancy(" + previous_tenancy_id + ") for this property was not marked as mutual exchange.");
                document.getElementById("chk_mutual").checked = false;
            }
            else if (action == 3) {
                alert("This option is unavailable.");
                document.getElementById("chk_mutual").checked = false;
            }
        }

        function SaveForm() {
            if (!checkForm()) return false
            if (real_date(document.getElementById("txt_STARTDATE").value) < real_date(document.getElementById("hid_DATE").value)) {
                alert("This property does not become available until " + document.getElementById("hid_DATE").value + " at the earliest !");
                return false;
            }
            if (document.getElementById("chk_cbl").checked == true) {
                document.getElementById("hid_CBL").value = 1;
            }
            document.RSLFORM.target = "frm_ct_srv";
            document.RSLFORM.action = "../ServerSide/tenancy_svr.asp";
            document.RSLFORM.submit();
            document.getElementById("btn_cREATE").disabled = true;
        }

        // transforms a dd/mm/yyyy into a long date format javascript date
        function real_date(str_date) {
            var datearray;
            var months = new Array("nowt", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
            str_date = new String(str_date);
            datearray = str_date.split("/");
            return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
        }

    </script>
</head>
<body>
    <form name="RSLFORM" method="post" action="">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="79">
                            <img src="../Images/tab_tenancy.gif" width="79" height="20" alt="Tenancy" />
                        </td>
                        <td height="19">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="../images/spacer.gif" height="1" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71">
                <br />
                <table cellspacing="2" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center">
                    <tr>
                        <td colspan="2">
                            Tenancy Type:&nbsp;&nbsp;&nbsp;<b><%=tenancy_type%></b>
                        </td>
                        <td style="display: none">
                            <%=lst_type%>
                            <img src="/js/FVS.gif" name="img_TENANCYTYPE" id="img_TENANCYTYPE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            Start
                            <input type="text" name="txt_STARTDATE" id="txt_STARTDATE" <%=startdate%> class="TEXTBOX100" />
                            <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                border="0" alt="" />
                            <!--span style="display:none">End 
<input type="text" name="txt_ENDDATE" id="txt_ENDDATE" <%=enddate%> class="TEXTBOX100" readonly="readonly">

<img src="/js/FVS.gif" name="img_ENDDATE" id="img_ENDDATE" width="15px" height="15px" border="0" alt="" /> </span-->
                            Mutual Exchange
                            <input type="checkbox" name="chk_mutual" id="chk_mutual" value="1" <%=me_attributes%> />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Choice Based Letting
                            <input type="checkbox" name="chk_cbl" id="chk_cbl" checked="checked" />
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="2" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center">
                    <tr>
                        <td>
                            Name
                        </td>
                        <td>
                            <input type="text" name="txt_NAME" id="txt_NAME" value="<%=cusname%>" class="TEXTBOX200"
                                readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tenancy Ref
                        </td>
                        <td>
                            <input type="text" name="txt_TENANCYREF" id="txt_TENANCYREF" value="<%=tenancyref%>"
                                class="TEXTBOX200" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Property Type
                        </td>
                        <td>
                            <input type="text" name="txt_PROPERTYTYPE" id="txt_PROPERTYTYPE" value="<%=propertytype%>"
                                class="TEXTBOX200" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Property Ref
                        </td>
                        <td>
                            <input type="text" name="txt_PROPERTYREF" id="txt_PROPERTYREF" value="<%=propertyref%>"
                                class="TEXTBOX200" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address 1
                        </td>
                        <td>
                            <input type="text" name="txt_NUMBER" id="txt_NUMBER" value="<%=propertynumber & " " & address1%>"
                                class="TEXTBOX200" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address 2
                        </td>
                        <td>
                            <input type="text" name="txt_ADD2" id="txt_ADD2" value="<%=address2%>" class="TEXTBOX200"
                                readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Town/City
                        </td>
                        <td>
                            <input type="text" name="txt_TOWNCITY" id="txt_TOWNCITY" value="<%=towncity%>" class="TEXTBOX200"
                                readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Post Code
                        </td>
                        <td>
                            <input type="text" name="txt_POSTCODE" id="txt_POSTCODE" value="<%=postcode%>" class="TEXTBOX200"
                                readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Make default address for this tenant:
                            <input type="checkbox" name="ISDEFAULT" id="ISDEFAULT" value="1" checked="checked" />
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="2" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center">
                    <tr>
                        <td width="90%">
                            &nbsp;
                        </td>
                        <td>
                            <input type="button" value="Close" title="Close" name="btn_BACK" id="btn_BACK" class="RSLBUTTON"
                                onclick="window.close()" style="cursor: pointer" />
                        </td>
                        <td>
                            <input type="button" value="Create" title="Create" name="btn_cREATE" id="btn_cREATE"
                                class="RSLBUTTON" onclick="SaveForm()" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
                <br />
                <input type="hidden" name="hid_RENTFREQUENCY" id="hid_RENTFREQUENCY" value="<%=rentfrequency%>">
                <input type="hidden" name="hid_PROPERTYID" id="hid_PROPERTYID" value="<%=property_id%>" />
                <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=customer_id%>" />
                <input type="hidden" name="hid_CREATEDBY" id="hid_CREATEDBY" value="<%=Session("userid")%>" />
                <input type="hidden" name="hid_ACTION" id="hid_ACTION" value="<%=action%>" />
                <input type="hidden" name="hid_CREATIONDATE" id="hid_CREATIONDATE" value="<%=DATE%>" />
                <input type="hidden" name="hid_DATE" id="hid_DATE" value="<%=end_date%>" />
                <input type="hidden" name="hid_CBL" id="hid_CBL" value="0" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="frm_ct_srv" id="frm_ct_srv" width="400px" height="400px"
        style="display: none"></iframe>
</body>
</html>
