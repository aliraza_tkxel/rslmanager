<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
	Dim repair_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, lst_contractor, action, ActionString, ActionString2, ContractorName, ContractorID, itemdetail, employeelimit, REASSIGNCOST
	Dim repair_text, EstimatedSeconds, DateStamp, DelDate
	Dim VAT, VATTYPE, GROSSCOST, NetCost, OrderID, lstVAT, RepairPriority
	Dim printletter, logdate, logdateInMinutes, repairPlusSevenDays			
			
	path = request.form("hid_go")
	
	repair_history_id 	= Request("repairhistoryid")
	nature_id 			= Request("natureid")
	// begin processing
	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	If path = 0 Then
		get_record()
	Elseif path = 1 Then
		new_record()
	End If
	
	CLoseDB()
	
	Function get_record()
		'	CONVERT(VARCHAR, J.CREATIONDATE, 103) AS LOGDATE
		Dim strSQL
		
		strSQL = 	"SELECT 	CONVERT(VARCHAR, J.CREATIONDATE, 103) AS LOGDATE, J.CREATIONDATE AS LOGDATEMINS, " &_	
					"			ISNULL(JD.EMPLOYEELIMIT,0) AS EMPLOYEELIMIT, PI.VATTYPE, PI.VAT, PI.GROSSCOST, ISNULL(ISNULL(PI.NETCOST,ID.COST),0) AS NETCOST, " &_
					"			((ISNULL(ID.COST,0) * V.VATRATE / 100) + ISNULL(ID.COST,0)) AS REASSIGNCOST, ISNULL(CONVERT(NVARCHAR, R.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
					"			ISNULL(R.NOTES, 'N/A') AS NOTES, " &_
					"			R.TITLE, R.REPAIRHISTORYID, O.NAME, O.ORGID, R.ITEMACTIONID AS ACTION, " &_
					"			S.DESCRIPTION AS STATUS, " &_					
					"			ID.DESCRIPTION AS ITEMDETAIL, " &_										
					"			A.EMPLOYEEONLY, A.CONTRACTORONLY, R.ITEMDETAILID, ISNULL(ID.PRIORITY,'') AS REPAIRPRIORITY " &_															
					"FROM		C_REPAIR R " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = R.JOURNALID " &_
					"			LEFT JOIN C_ACTION A ON A.ITEMACTIONID = R.ITEMACTIONID  " &_
					"			LEFT JOIN C_STATUS S ON R.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN R_ITEMDETAIL ID ON R.ITEMDETAILID = ID.ITEMDETAILID  " &_
					"			LEFT JOIN F_VAT V ON ID.VATTYPE = V.VATID  " &_					
					"			LEFT JOIN S_ORGANISATION O ON R.CONTRACTORID = O.ORGID " &_
					"			LEFT JOIN E__EMPLOYEE E ON R.LASTACTIONUSER = E.EMPLOYEEID " &_					
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
					"			LEFT JOIN P_WOTOREPAIR WTP ON WTP.JOURNALID = R.JOURNALID " &_
					"			LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WTP.ORDERITEMID " &_	
					"WHERE 		REPAIRHISTORYID = " & repair_history_id
			'rw strSQL		
		Call OpenRs(rsSet, strSQL)
		
		i_status 	= rsSet("STATUS")
		title 		= rsSet("ITEMDETAIL")
		notes		= rsSet("NOTES")
		action 		= rsSet("ACTION")
		contractor	= rsSet("ORGID")
		fullname	= rsSet("FULLNAME")
		employeeactions = rsSet("EMPLOYEEONLY")
		contractoractions = rsSet("CONTRACTORONLY")
		itemdetail 	= rsSet("ITEMDETAILID")
		NETCOST = rsSet("NETCOST")
		VAT = rsSet("VAT")
		VATTYPE = rsSet("VATTYPE")		
		GROSSCOST = rsSet("GROSSCOST")				
		employeelimit = rsSet("EMPLOYEELIMIT")
		REASSIGNCOST = rsSet("REASSIGNCOST")
		RepairPriority = rsSet("REPAIRPRIORITY") 
		logdate	= rsSet("LOGDATE")
		logdateInMinutes = rsSet("LOGDATEMINS")
		CloseRs(rsSet)
		'this variable denotes whether a the contractor select box can be changed or not...
		'as you do not want users to change it once it has been assigned to a contractor.
		AllowContractorToBeChanged = false
		
		'change this to false for contractors
		isEmployer = true
		
		'this part sets the generic variable with the appropriate actions to be displayed
		if (isEmployer = true) then
			if (employeeactions = "" OR isNull(employeeactions)) then
				employeeactions = "-1"
			end if
			ActionList = employeeactions
		'the bottom two statements should be changed to an else to get the employee and contractor filter working
		end if

		'build a list of appropriate actions depending on the previous action
		SQL = "SELECT ITEMACTIONID, DESCRIPTION FROM C_ACTION WHERE ITEMACTIONID IN (" & ActionList & ")"
		Call OpenRs(rsActions, SQL)		
		if (NOT rsActions.EOF) then
			ActionString = "<option style='background-color:beige' value=''>Please Select</option>"
			while NOT rsActions.EOF 
				ActionID = rsActions("ITEMACTIONID")
				if (ActionID = 2) then
					AllowContractorToBeChanged = true
				end if
				ActionString = ActionString & "<option style='background-color:beige' value='" & ActionID & "'>" & rsActions("DESCRIPTION") & "</option>"
				rsActions.moveNext
			wend
		else
			ActionString = "<option style='background-color:beige' value=''>There are no relevant options</option>"			
		end if
		CloseRs(rsActions)
		'end of action build list

		if (isEmployer = true) then
			if (contractoractions = "" OR isNull(contractoractions)) then
				contractoractions = "-1"
			end if
			'this part overides the previous code 'ActionList = contractoractions'  to give all options....
			ActionList = contractoractions
		end if			

		'build a list of appropriate actions depending on the previous action
		SQL = "SELECT ITEMACTIONID, DESCRIPTION FROM C_ACTION WHERE ITEMACTIONID IN (" & ActionList & ")"
		Call OpenRs(rsActions, SQL)		
		if (NOT rsActions.EOF) then
			ActionString2 = "<option style='background-color:#e0e0e0' value=''>Please Select</option>"
			while NOT rsActions.EOF 
				ActionID = rsActions("ITEMACTIONID")
				if (ActionID = 2) then
					AllowContractorToBeChanged = true
				end if
				ActionString2 = ActionString2 & "<option style='background-color:#e0e0e0' value='" & ActionID & "'>" & rsActions("DESCRIPTION") & "</option>"
				rsActions.moveNext
			wend
		else
			ActionString2 = "<option style='background-color:#e0e0e0' value=''>There are no relevant options</option>"			
		end if
		CloseRs(rsActions)
		'end of action build list
				
		ContractorName = "Not Assigned"
		ContractorID = ""
		'next get the name of the contractor that the repair has been assigned to if applicable
		if (contractor <> "" and NOT isNull(contractor)) then
			SQL = "SELECT ORGID, NAME FROM S_ORGANISATION WHERE ORGID = " & contractor
			Call OpenRs(rsCon, SQL)
			if (NOT rsCon.EOF) then
				ContractorName = rsCon("NAME")
				ContractorID = contractor
			end if
			CloseRs(rsCon)
		end if

		Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, VATTYPE, NULL, "textbox200", " onchange='SetVat()' disabled")		
	End Function

	Function new_record()
	
		strSQL = "SELECT JOURNALID, CONTRACTORID, ITEMACTIONID FROM C_REPAIR WHERE REPAIRHISTORYID = " & repair_history_id
	
		Call OpenRs(rsSet, strSQL)
		journalid = rsSet("JOURNALID")
		contractor = rsSet("CONTRACTORID")
		prevactionid = rsSet("ITEMACTIONID")
		if (contractor = "" OR isNULL(contractor)) then
			contractor = "NULL"
		end if
		CloseRS(rsSet)
		
		actionid = Request.Form("sel_ACTION")
		
		' OLD FUNCTIONAILty If action is that of a contractor accepting a repair then assign a value for 
		' Javascript to print an automatic questionnaire letter.
		' NEW - set in C_SATISFACTIONLETTER the status of the letter is ready to be sent  to refer to it on the whiteboard	
		if actionid = "5" then
			SQL = "DELETE FROM C_SATISFACTIONLETTER WHERE JOURNALID = " & journalid & " AND SATISFACTION_LETTERSTATUS = 1"
			Conn.Execute (SQL)			
			SQL = "INSERT INTO C_SATISFACTIONLETTER (JOURNALID, SATISFACTION_LETTERSTATUS) VALUES (" & journalid & ",1)" 
			Conn.Execute (SQL)	
		end If		
		' End set value
		
		notes = Replace(Request.form("txt_NOTES"),"'","''")
		if (CInt(actionid) = 14) then 'is price change
			actionid = prevactionid
			TooExpensive = CInt(Request.Form("hid_TOOEXPENSIVE"))
			notes = "PRICE CHANGED. " & chr(13) & notes

			Call LOG_PI_ACTION_USING_JID(journalid, "REPAIR ITEM PRICE CHANGE")	
						
			'CHANGE THE PRICE OF THE ITEM HERE
			SQL = "UPDATE F_PURCHASEITEM SET NETCOST = " & Request.Form("txt_NETCOST") & ", VATTYPE = " & Request.Form("sel_VATTYPE") & ", " &_
					"VAT = " & Request.Form("txt_VAT") & ", GROSSCOST = " & Request.Form("txt_GROSSCOST") & " FROM F_PURCHASEITEM PI " &_
					"INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID " &_
					"WHERE WTR.JOURNALID = " & journalid
			Conn.Execute (SQL)
		end if
		
		title = Replace(Request.form("txt_TITLE"),"'","''")
		itemdetail = Request.Form("hid_ITEMDETAIL")
		
		strSQL = "SELECT STATUS, POACTION FROM C_ACTION WHERE ITEMACTIONID = " & actionid
		Call OpenRs(rsSet, strSQL)
			r_status = rsSet("STATUS")
			po_action = rsSet("POACTION")
		CloseRS(rsSet)		
		
		if (TooExpensive = 1) then
			actionid = 12
			r_status = 12

			Call LOG_PI_ACTION_USING_JID(journalid, "REPAIR ITEM PRICE CHANGE (QUEUED ALSO)")				
			'RESET THE PURCHASE ITEM STATUS TO QUEUED...
			SQL = "UPDATE F_PURCHASEITEM SET PISTATUS = 0 FROM F_PURCHASEITEM PI " &_
					"INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID " &_
					"WHERE WTR.JOURNALID = " & journalid
			Conn.Execute (SQL)
		end if

  		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_REPAIR " &_
					"(JOURNALID, ITEMSTATUSID, ITEMDETAILID, ITEMACTIONID, LASTACTIONUSER, CONTRACTORID, TITLE, NOTES) " &_
					"VALUES (" & journalid & ", " & r_status & ", " & itemdetail & ", " & actionid & ", " & Session("userid") & ", " & contractor & ", '" & title & "', '" & notes & "')" &_
					";SELECT SCOPE_IDENTITY() AS HISTORYID"
		Call OpenRs(rsRepairHistory, strSQL)
		Dim repairhistory_id
		repairhistory_id = rsRepairHistory("HISTORYID")
		Call CloseRs(rsRepairHistory)
		
		If Cint(actionid) = 8 Then
			//COPY PREVIOUS STUFF INTO THE HISTORY TABLE
			SQL = "INSERT INTO C_REPAIRCOMPLETION_HISTORY (JOURNALID, REPAIRHISTORYID, ITEMDETAILID, COMPLETIONDATE, TRIGGEREDBY) " &_
				  "SELECT JOURNALID, REPAIRHISTORYID, ITEMDETAILID, COMPLETIONDATE, " & actionid & " FROM C_REPAIRCOMPLETION WHERE JOURNALID = " & journalid
			Conn.Execute SQL
				  
			//THIS IS A CHECK TO MAKE SURE DUPLICATE COMPLETION DATES ARE NOT ENTERED. 
			SQL = "DELETE FROM C_REPAIRCOMPLETION WHERE JOURNALID = " & journalid
			Conn.Execute SQL
		end if
		
		'INSERT ROW INTO JOB COMPLETION TABLE ONLY IF ACTION = 6 (COMPLETION)
		If Cint(actionid) = 6 Then
			' THIS IS A CHECK TO MAKE SURE DUPLICATE NO ACCESS ROWS ARE NOT ENTERED. 
			SQL = "DELETE FROM C_REPAIR_NOACCESSLIST WHERE JOURNALID = " & journalid
			Conn.Execute (SQL)
		
			//COPY PREVIOUS STUFF INTO THE HISTORY TABLE
			SQL = "INSERT INTO C_REPAIRCOMPLETION_HISTORY (JOURNALID, REPAIRHISTORYID, ITEMDETAILID, COMPLETIONDATE, TRIGGEREDBY) " &_
				  "SELECT JOURNALID, REPAIRHISTORYID, ITEMDETAILID, COMPLETIONDATE, " & actionid & " FROM C_REPAIRCOMPLETION WHERE JOURNALID = " & journalid
			Conn.Execute SQL
				  
			//THIS IS A CHECK TO MAKE SURE DUPLICATE COMPLETION DATES ARE NOT ENTERED. 
			SQL = "DELETE FROM C_REPAIRCOMPLETION WHERE JOURNALID = " & journalid
			Conn.Execute SQL
						
			strSQL = 	"INSERT INTO C_REPAIRCOMPLETION " &_
						"(JOURNALID, REPAIRHISTORYID, ITEMDETAILID, COMPLETIONDATE) " &_
						"VALUES (" & journalid & ", " & repairhistory_id & ", " & itemdetail & ",'" & FormatDateTime(Request.Form("txt_CompletionDate"),1) & " " & Request.Form("txt_COMPLETIONTIME") & "')"
			Conn.Execute(strSQL)
			
			' CHECK TO SEE IF COMPLETION WAS DUE TO NO ACCESS TO PROPERTY
			' IF SO RECORD DETAILS TO ENABLE JOBS TO BE REMOVED FROM KPIS AND 
			' ALSO TO BE USED FOR WHITEBOARD ALERTS 'JOBS TO BE RE-ASSIGNED'
			If Request("chk_NOACCESS") = "1" Then
				
				SQL = " INSERT INTO C_REPAIR_NOACCESSLIST (JOURNALID, LOGGEDBY) " &_
							" VALUES (" & journalid & ", " & Session("USERID") & ")"
				Conn.Execute (SQL)
			End If

		End If
		
		Call update_journal(journalid, r_status)
		
		Select Case po_action
			Case "CANCEL"
				Call CancelPO(journalid)
			Case "COMPLETE"
				NEW_STATUS = 5 'WORK COMPLETED
				Call ChangePIStatus(journalid, NEW_STATUS)
			Case "INVOICE"
				NEW_STATUS = 7 'INVOICED RECIEVED
				Call ChangePIStatus(journalid, NEW_STATUS)
			Case "DEFAULT" 
				Call SET_PO_STATUS(journalid)
		End Select

		
		
	End Function
	
	// updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
	
		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid		
		set rsSet = Conn.Execute(strSQL)
	
	End Function

%>
<html>
<head>
<title>Update Repair</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

	var CompletionTimeError = false

	var FormFields = new Array();
	FormFields[0] = "txt_TITLE|Title|TEXT|Y"
	FormFields[1] = "sel_ACTION|Action|SELECT|Y"
	FormFields[2] = "txt_NOTES|Notes|TEXT|N"
	FormFields[3] = "txt_NETCOST|Net Cost|CURRENCY|Y"	
	FormFields[4] = "txt_VAT|VAT|CURRENCY|Y"	
	FormFields[5] = "txt_GROSSCOST|Total|CURRENCY|Y"

	var priority = "<%=RepairPriority%>" // SET PRIORITY
	var repairPlusSevenDays = "<%=DateAdd("d", 8, logdate)%>";

	function real_date(str_date){

		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(Number(datearray[0]) + " " + months[Number(datearray[1])] + " " + datearray[2]);
		}

	function save_form(){
		if (RSLFORM.sel_ACTION.value == 6){
			FormFields[6] = "txt_CompletionDate|Completion Date|DATE|Y";
			<% if (RepairPriority = "A" OR RepairPriority = "B") then %>
			FormFields[7] = "txt_COMPLETIONTIME|Completion Time|TEXT|Y";
			<% else %>						
			FormFields[7] = "txt_COMPLETIONTIME|Completion Time|TEXT|N";
			<% end if %>						
			if (CompletionTimeError) return false;
		}
		else
			FormFields.length = 6
		
		if (!checkForm()) return false;

		if (RSLFORM.sel_ACTION.value == 6){
			<%	' NEW DIRECTIVE -- ONLY ALLOW COMPLATION DATE NOT MORE THAN SEVEN DAYS PRIOR TO TODAY
				' AND NOT AT ANY TIME IN THE FUTURE -- PP 16 JUN 2005
				'TheDateMinusSeven = DateAdd("d", -7, Date)
				If RepairPriority = "A" Or RepairPriority = "B" Then ' USE MINUTES
					TheDateMinusSeven = DateAdd("n", -10080, NOW) ' USING MINUTES
					TheDateToday = now
				Else
					TheDateMinusSeven = DateAdd("D", -7, Date) ' USING DAYS
					TheDateToday = date
				End If
				' THE ABOVE IS DEFUNCT
				TheDateToday = date
			%>
			// WHEN DOING OUR COMPARISONS, IF THE PRIORITY IS 'EMERGENCY' 
			// THEN WE MUST USE DATE AND TIME FOR LOGDATE
			var logdateComparison, date_entered, title
			var LessSevenDays = "<%=TheDateMinusSeven%>";
			var Today = "<%=TheDateToday%>";
			var logdate = "<%=logdate%>";
			var logdateInMinutes = "<%=logdateInMinutes%>";
			if (priority == "A" || priority == "B") {
				logdateComparison = logdateInMinutes;
				date_entered = new String (document.getElementById("txt_CompletionDate").value + " " + document.getElementById("txt_CompletionTime").value)
				title = "and time "
				}
			else {
				logdateComparison = logdate;
				date_entered = new String (document.getElementById("txt_CompletionDate").value)
				title = ""				
				}

			if ( (real_date(date_entered) < real_date(logdateComparison)) || (real_date(new String(document.getElementById("txt_CompletionDate").value)) > real_date(Today)) ){
				alert("Please enter a completion date that is:\nNo earlier than the log date "+title+"of the repair '"+logdateComparison+"'.\nNo later than the current day - "+Today+".");
				return false;
				}
			if (!(confirm("Please confirm that the following completion information is correct:\n\nWorks Order Job status = Completion\n\nDate and time entered is : " + date_entered))) return false

			}

		if (RSLFORM.sel_ACTION.value == 14){
			EmployeeLimit = "<%=employeelimit%>";
			GrossCost = document.getElementById("txt_GROSSCOST").value
			document.getElementById("hid_TOOEXPENSIVE").value = 0
			if (GrossCost == "") GrossCost = 0
			if ((parseFloat(EmployeeLimit) < parseFloat(GrossCost))){
				document.getElementById("hid_TOOEXPENSIVE").value = 1
				result = confirm("The repair total cost (�" + FormatCurrency(GrossCost) + ") is more than your employee repair limit.\nTherefore this item will be put in a queue for authorisation by a manager.\nIf you wish to continue click on 'OK'.\nOtherwise click on 'CANCEL'.");
				if (!result) return false;
				
			}
			}
		RSLFORM.hid_go.value = 1;

		
		RSLFORM.submit();
	}

	function return_data(){
		if (<%=path%> == 1)	{
			try {
				opener.location.reload();
				}
			catch (e) {
				temp = 1
				}
				//<% 'If printletter = "true" then %>
				//RSLFORM.target = "iPrintLetter";
				//RSLFORM.action = "SatisfactionSurvey.asp";
				//RSLFORM.submit();
				//<% 'Else %>
				window.close();
				//<%' End If %>	
			}
		else {
			var tday = new Date();
			if (tday > real_date(repairPlusSevenDays)){
				document.getElementById("txt_CompletionDate").style.backgroundColor = "silver";			
				document.getElementById("txt_CompletionTime").style.backgroundColor = "silver";
				document.getElementById("txt_CompletionDate").disabled = true;
				document.getElementById("txt_CompletionTime").disabled = true;
				document.getElementById("txt_CompletionDate").title = "This job is over 7 days old and therefore completion date has become inactive.";
				document.getElementById("txt_CompletionTime").title = "This job is over 7 days old and therefore completion date has become inactive.";
				}
			}
		}


	function SetPrice(){
	
		if (RSLFORM.sel_ACTION.value == 14){
		
			document.getElementById("sel_VATTYPE").disabled = false
			document.getElementById("txt_NETCOST").readOnly = false;
			}
		else {
		
			document.getElementById("sel_VATTYPE").disabled = true
			document.getElementById("txt_NETCOST").readOnly = true;
			document.getElementById("sel_VATTYPE").value = "<%=VATTYPE%>"
			document.getElementById("txt_NETCOST").value = "<%=NETCOST%>";
			
				if (RSLFORM.sel_ACTION.value == 6){  // if completed is chosen 
				window.resizeTo(417, 490)
				if (priority == "A" || priority == "B")
					document.getElementById("ctime").style.visibility = "visible";
				else
					document.getElementById("ctime").style.visibility = "hidden";
				document.getElementById("row_CompletionDate").style.display = "block";
				document.getElementById("row_NoAccess").style.display = "block";
				}
				else{
				window.resizeTo(417, 438)
				document.getElementById("row_CompletionDate").style.display = "none";
				document.getElementById("row_NoAccess").style.display = "none";
				}
				
			SetVat()
			}
		
		}

	function IsValidTime() {
		ManualError("img_COMPLETIONTIME",'',3);
		CompletionTimeError = false
		timeStr = document.getElementById("txt_COMPLETIONTIME").value;
		if (timeStr == "") return false;		
		timeStr = timeStr.replace(".", ":");
		var timePat = /^(\d{1,2}):(\d{1,2})$|^(\d{1,2}):$|^:(\d{1,2})$|^(\d{1,2})$/;
		var matchArray = timeStr.match(timePat);
		if (matchArray == null) {
			ManualError("img_COMPLETIONTIME",'Incorrect time entered, time must be in the format HH:MM.',0);		
			CompletionTimeError = true		
			return false;
			}
		
		matchArray = timeStr.split(":")
		hour = matchArray[0];
		if (matchArray[0] == "")
			hour = 0;
		if (matchArray.length > 1) {
			if (matchArray[1] == "")
				minute = 0;
			else
				minute = matchArray[1];
			}
		else minute = 0;
		
		if (hour < 0  || hour > 23 || minute < 0 || minute > 59) {
			ManualError("img_COMPLETIONTIME","You have entered an incorrect time,\nHours must be between 0 and 23 inclusive\nMinutes must be between 0 and 59 inclusive",0);		
			CompletionTimeError = true				
			return false;
			}
		minute = "" + minute + "";
		if (minute.length == 1) minute = minute + "0";
		document.getElementById("txt_COMPLETIONTIME").value = hour + ":" + minute;
		}

	function getMins(iMinsArr){
		iMinsArr = iMinsArr.split(":")
		iMins = parseInt(iMinsArr[0],10) * 60 + parseInt(iMinsArr[1],10);
		return iMins;
		}
			
	function SetOptions(){
		if (document.getElementsByName("rdo_AT")[0].checked == true)
			document.getElementById("ACT").innerHTML = "<SELECT NAME=\"sel_ACTION\" class=\"textbox200\" onchange=\"SetPrice()\">" + document.getElementById("ACT1").innerHTML + "</SELECT>"
		else
			document.getElementById("ACT").innerHTML = "<SELECT NAME=\"sel_ACTION\" class=\"textbox200\" onchange=\"SetPrice()\">" + document.getElementById("ACT2").innerHTML + "</SELECT>"
		}		
	function printletter(){	

	}
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onLoad="return_data();window.focus();printletter()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td width=10 height=10><img src="/Customer/Images/tab_update_repair.gif" width="117" height="20" alt="" border=0 /></td>
	  <td width=302 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'>&nbsp;</td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr valign=top>
			<td nowrap>Title:</td>
			<td width=100%><%=title%><input type="HIDDEN" class="textbox200" name="txt_TITLE" value="<%=title%>"></td>
			<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>
		</tr>
		<TR>
			<TD nowrap>Action Options:</TD>
			<TD>
				<span style='background-color:beige;border:1px dotted black'>&nbsp;Broadland: <input type="radio" value=1 name="rdo_AT" checked onClick="SetOptions()">&nbsp;</span> 
				<span style='background-color:#e0e0e0;border:1px dotted black'>&nbsp;Supplier: <input type="radio" value=2 name="rdo_AT" onClick="SetOptions()">&nbsp;</span>
			</TD>
			<TD>
				<image src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0">
			</TD>
		</TR>
		<TR>
			<TD>Action</TD>
			<TD><DIV id="ACT">
				<SELECT NAME="sel_ACTION" class="textbox200" onChange="SetPrice()">
				<%=ActionString%>
				</SELECT>
				</DIV>
				<div id="ACT1" style='display:none'>
				<%=ActionString%>
				</div>
				<div id="ACT2" style='display:none'>
				<%=ActionString2%>
				</div>
			</TD>
			<TD>
				<image src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0">
			</TD>
		</TR>
		<TR id="row_CompletionDate" name="row_CompletionDate" style="display:none" nowrap>
			<TD>
				Comp. Date:</TD>
			<TD colspan=2><input type="TEXT" class="textbox" size=12 maxlength=10 name="txt_CompletionDate" value="">
				<image src="/js/FVS.gif" name="img_CompletionDate" width="15px" height="15px" border="0">
				<span id="ctime" style="visibility:hidden">Time:
				<input type="TEXT" class="textbox" size=7 maxlength=5 name="txt_COMPLETIONTIME" value=""  onblur="IsValidTime()">				
				<image src="/js/FVS.gif" name="img_COMPLETIONTIME" width="15px" height="15px" border="0"></span>
			</TD>
		</TR>
		<TR id="row_NoAccess" name="row_NoAccess" style="display:none">
			<TD>
				No Access:</TD>
			<TD colspan=2><input type="CHECKBOX" name="chk_NOACCESS" value="1">
				<img src="/myImages/info.gif" style='cursor:hand' title='Click this checkbox only if this job has been completed because no access was gained to the property'>
			</TD>
		</TR>
		<TR>
			<TD>Contractor</TD>
			<TD>
				<input type="TEXT" class="textbox200" readonly name="PreviousContractor" value="<%=ContractorName%>">
				<input type=hidden name="PreviousContractorID" value="<%=ContractorID%>">
			</TD><TD>
				<image src="/js/FVS.gif" name="img_CONTRACTOR" width="15px" height="15px" border="0">
			</TD>
		</TR>
		<TR>
			<TD>Net Cost:</TD>
			<TD><input type="text" class="textbox200" name="txt_NETCOST" value="<%=FormatNumber(NETCOST,2,-1,0,0)%>" READONLY onBlur="SetVat()" STYLE='TEXT-ALIGN:RIGHT'></TD>		
			<TD><image src="/js/FVS.gif" name="img_NETCOST" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
			<TD>VAT Type:</TD>
			<TD><%=lstVAT%></TD>		
			<TD><image src="/js/FVS.gif" name="img_VATTYPE" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
			<TD>VAT (�):</TD>
			<TD><input type="text" class="textbox200" name="txt_VAT" value="<%=FormatNumber(VAT,2,-1,0,0)%>" READONLY STYLE='TEXT-ALIGN:RIGHT'></TD>		
			<TD><image src="/js/FVS.gif" name="img_VAT" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
			<TD>Total (�):</TD>
			<TD><input type="text" class="textbox200" name="txt_GROSSCOST" value="<%=FormatNumber(GROSSCOST,2,-1,0,0)%>" READONLY STYLE='TEXT-ALIGN:RIGHT'></TD>		
			<TD><image src="/js/FVS.gif" name="img_GROSSCOST" width="15px" height="15px" border="0"></TD>
		</TR>

		<TR>
			<TD nowrap>Status</td>
			<TD><input type="text" class="textbox200" name="txt_STATUS" value="<%=i_status%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"></TD>
		</TR>		

		<TR>
			<TD nowrap>Last Action By</td>
			<TD><input type="text" class="textbox200" name="txt_RECORDEDBY" value="<%=fullname%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_RECORDEDBY" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD valign=top>Notes</TD>
			<TD><textarea style='OVERFLOW:HIDDEN' class="textbox200" name="txt_NOTES" rows=4></textarea></TD>	
			<TD valign=top><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
				<input type="hidden" name="hid_ITEMDETAIL" value="<%=itemdetail%>">
				<input type="hidden" name="hid_go" value=0>
		        <input type="hidden" name="hid_TOOEXPENSIVE" value="0">						
			</TD>
		</TR>           

</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <%=RepairPriority%><!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
        <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
        <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
        </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp"  name="iPrintLetter"  id="iPrintLetter" height="1" width="1" style="display:block"></iframe>
</BODY>
</HTML>

