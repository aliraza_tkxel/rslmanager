<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../iFrames/iHB.asp" -->
<%
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color,bal_color,bal_text,str_open_bracket,str_close_bracket, ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED

	CurrentDate = CDate("1 jan 2004")
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear

	Call OpenDB()

	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")

	If (tenancy_id = "") Then
		Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
		If Not rsSet.EOF Then 
			tenancy_id = rsSet(0)
		Else
			tenancy_id = -1
		End If
		Call CloseRs(rsSet)
	End If

	Call build_journal()

	Function build_journal()

		cnt = 0
		strSQL = "EXEC F_GET_CUSTOMER_ACCOUNT_RECHARGES @TENANCYID = " &  tenancy_id
		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If rsSet("BREAKDOWN") > 0 Then
				str_color = "style='color:red'"
				str_open_bracket = "("
				str_close_bracket = ")"
			Else
				str_color = ""
				str_open_bracket = ""
				str_close_bracket = ""
			End If

			If Not rsSet("status") = "Estimated" Then

		        If ltrim(rsSet("DEBIT")) <> "" Then
			        TR_str = "<tr style='font-weight:bold;'>" 
			    Else
			        TR_str = "<tr>"
			    End If

                str_journal_table = str_journal_table & TR_str &_
																"<td class='style3'>" & rsSet("F_TRANSACTIONDATE_TEXT") & "</td>" &_
																"<td class='style3'>" & rsSet("CATEGORY") & "</td>" &_
																"<td class='style3'>" & rsSet("ITEMDESC") & "</td>" &_
																"<td class='style3' align=right>" & rsSet("DEBIT") & "</td>" &_
																"<td class='style3' align=right>" & rsSet("CREDIT") & "</td>" &_
																"<td class='style3' " & str_color & " align='right'>" & str_open_bracket & FormatCurrency(ABS(rsSet("BREAKDOWN")))  & str_close_bracket & "</td>" &_
															"<tr>"

			End If

			Balance = rsSet("BREAKDOWN")
			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tr><td colspan='6' align='center'>No journal entries exist.</td></tr>"
		End If


		strSQL = 	"SELECT 	ISNULL(ABS(SUM(J.AMOUNT)), 0) As AMOUNT " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
					"			AND J.ITEMTYPE IN (5)"

		Call OpenRs (rsSet, strSQL)
		    RC = CDbl(rsSet("AMOUNT"))
		Call CloseRs(rsSet)

		strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) as AMOUNT " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND J.STATUSID IN (8) " &_
					"			AND J.ITEMTYPE IN (12)"
		Call OpenRs (rsSet, strSQL)
		    OB = CDbl(rsSet("AMOUNT"))

		    ACCOUNT_BALANCE = Balance

	        If ACCOUNT_BALANCE <= 0 Then
		        CR_DBval = "CR"
		        bal_color = "Black"
		        bal_text = "Owed from BHA"
			    ACCOUNT_BALANCE = FormatCurrency(abs(ACCOUNT_BALANCE))
		    Else
			    CR_DBval = "DR"
			    bal_color = "Red"
			    bal_text = "Owed to BHA"
			    ACCOUNT_BALANCE = FormatCurrency(ACCOUNT_BALANCE)
		    End If

		Call CloseRs(rsSet)

	End Function

	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, N.LIST,  " & vbCrLf &_
			"		A.HOUSENUMBER, A.ADDRESS1, A.ADDRESS2, A.ADDRESS3, A.TOWNCITY, A.POSTCODE, A.COUNTY , A.ISDEFAULT,  " & vbCrLf &_
			"		CASE WHEN D.CAREOF = 1 THEN 'c/o ' + A.CONTACTFIRSTNAME + ' ' + A.CONTACTSURNAME ELSE '' END AS CAREOF  " & vbCrLf &_
			"FROM C_TENANCY T " & vbCrLf &_
			"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " & vbCrLf &_
			"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " & vbCrLf &_
			"	INNER JOIN C_CUSTOMER_NAMES_GROUPED_VIEW N ON N.I = T.TENANCYID " & vbCrLf &_
			"	LEFT JOIN C_ADDRESS A ON A.CUSTOMERID = CT.CUSTOMERID " & vbCrLf &_
			"	LEFT JOIN C_ADDRESSTYPE D ON A.ADDRESSTYPE = D.ADDRESSTYPEID " & vbCrLf &_
			"	LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " & vbCrLf &_
			"WHERE T.TENANCYID = " & tenancy_id & vbCrLf &_
			" AND C.CUSTOMERID = " & customer_id & vbCrLf &_
			"ORDER BY ISDEFAULT DESC, D.CAREOF DESC "

	HBOWED = 0

	Call OpenRS(rsCust, SQL)
	rsCust.moveFirst()

	StringFullName = rsCust("LIST")
	If rsCust("CAREOF") <> "" Then
		StringFullName = StringFullName & "<br />" & rsCust("CAREOF")
	End If

	housenumber = rsCust("housenumber")
	If (housenumber = "" Or isNull(housenumber)) Then
		FirstLineOfAddress = FirstLineOfAddress & rsCust("Address1")
	Else
		FirstLineOfAddress = FirstLineOfAddress & housenumber & " " & rsCust("Address1")
	End If

	RemainingAddressString = ""
	AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY", "POSTCODE")

	For i=0 To Ubound(AddressArray)
		temp = rsCust(AddressArray(i))
		If (temp <> "" Or NOT isNull(temp)) Then
			RemainingAddressString = RemainingAddressString &  "<tr><td nowrap='nowrap' style='font-size:12px'>" & temp & "</td></tr>"
		End If
	Next

	tenancyref = rsCust("TENANCYID")

	FullAddress = "<tr><td nowrap='nowrap' style='font-size:12px'>" & StringFullName     & "</td></tr>" &_
				  "<tr><td nowrap='nowrap' style='font-size:12px'>" & FirstLineOfAddress & "</td></tr>" &_
				RemainingAddressString
	Call CloseRs(rsCust)
	Call CloseDB()
%>
<html>
<head>
    <title>Customer Info:</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .style3
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .style7
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12;
            font-weight: bold;
        }
        .style8
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        body
        {
            background-color: white;
            background-image: none;
            margin: 6px,0px,0px,0px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PrintMe() {
            document.getElementById("PrintButton").style.visibility = "hidden"
            print()
            document.getElementById("PrintButton").style.visibility = "visible"
        }
    </script>
</head>
<body onload="window.focus()">
    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="110" valign="top">
                <table width="100%">
                    <tr>
                        <td nowrap="nowrap" valign="top" width="165">
                            <table width="163">
                                <tr>
                                    <td nowrap="nowrap">
                                        <u><span class="style3"><b><font color="#133e71" style="font-size: 14px">Recharge Statement</font></b></span></u><span
                                            class="style3"><br />
                                        </span>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap" style='border: 1px solid #133E71'>
                                        <font style="font-size: 12px; font-family: Arial, Verdana, Helvetica, sans-serif;"><span
                                            class="style3">Tenancy Ref: <b>
                                                <%=TenancyReference(tenancyref)%></b></span></font>
                                    </td>
                                </tr>
                                <%=FullAddress%>
                            </table>
                        </td>
                        <td align="right" width="525" valign="top">
                            &nbsp;
                        </td>
                        <td align="right" valign="top" width="150">
                            <img src="../Images/BHALOGOLETTER.gif" width="145" height="113" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" height="171">
                <table width="100%" cellpadding="1" cellspacing="0" style="behavior: url(../../Includes/Tables/tablehl.htc);
                    border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                    <thead>
                        <tr valign="top">
                            <td colspan="4" height="20">
                                <b><font color='Black' style='font-size: 14px'><span class="style3">Date</span>:
                                    <%= DatePart("d", Date) & " " & MonthName(DatePart("m", Date)) & " " & DatePart("yyyy", Date)%></font></b>
                            </td>
                            <td colspan="5" height="20">
                                <input type="button" value=" Print Statement" onclick="PrintMe()" class="RSLButton"
                                    name="PrintButton" id="PrintButton" />
                            </td>
                        </tr>
                        <tr valign="top">
                            <td style="border-bottom: 1px solid" width="120">
                                <span class="style7"><font color='Black'>Date</font></span>
                            </td>
                            <td width="130" style="border-bottom: 1px solid">
                                <span class="style7"><font color='Black'>Category</font></span>
                            </td>
                            <td width="129" style="border-bottom: 1px solid">
                                <span class="style7"><font color='Black'>Item Description</font></span>
                            </td>
                            <td width="70" align="right" style="border-bottom: 1px solid">
                                <span class="style7"><font color='Black'>Monies Due</font></span>
                            </td>
                            <td width="70" align="right" style="border-bottom: 1px solid">
                                <span class="style7"><font color='Black'>Monies In</font></span>
                            </td>
                            <td width="70" align="right" style="border-bottom: 1px solid">
                                <span class="style7"><font color="Black">Balance</font></span>
                            </td>
                        </tr>
                        <tr style="height: 7px">
                            <td colspan="5">
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <%=str_journal_table%></tbody><tfoot>
                            <% If HBOWED <> 0 Then %>
                            <tr valign="top">
                                <td height="20" colspan="5" align="right" style="border-top: 1px solid">
                                    <span class="style7"><b></b><font color='red' style='font-size: 14px'></font><font
                                        color='red' style='font-size: 12px'></font></span><font color='red' style='font-size: 14px'>
                                            &nbsp;</font>
                                </td>
                                <td height="20" colspan="2" align="right" style="border-top: 1px solid">
                                    &nbsp;
                                </td>
                            </tr>
                            <% Else %>
                            <tr valign="top">
                                <td style="border-top: 1px solid" colspan="3" height="20" align="right">
                                    <span class="style3"></span>
                                </td>
                                <td style="border-top: 1px solid" align="right">
                                    <span class="style3"></span>
                                </td>
                                <td style="border-top: 1px solid" align="right">
                                    <span class="style3"></span>
                                </td>
                                <td style="border-top: 1px solid" align="right">
                                    <span class="style3"></span>
                                </td>
                            </tr>
                            <% End If %>
                            <tr valign="top">
                                <td height="20" colspan="4" align="right" style='font-size: 12px; font-family: Arial, Verdana, Helvetica, sans-serif;'>
                                    <span class="style7"><b><font color='Black' style='font-size: 12px'>
                                        <%=bal_text %>
                                        :&nbsp;</font></b></span>
                                </td>
                                <td height="20" colspan="2" align="right" style='font-size: 12px; font-family: Arial, Verdana, Helvetica, sans-serif;'>
                                    <span class="style7"><b><font color='<%=bal_color%>' style='font-size: 12px'>
                                        <%=ACCOUNT_BALANCE%></font></b></span>
                                </td>
                            </tr>
                        </tfoot>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom" align="right" height="100%">
                <table width="100%" align="right">
                    <tr>
                        <td width="69%" valign="top" style='font-size: 12px'>
                            <strong>Payments made within the last 3 working days may not appear on your statement.</strong>
                            <br />
                            <br />
                            Credits are not always refundable where there is money outstanding from Housing
                            Benefit. Please contact Customer Services on 0303 303 0003 for further information
                            about this or any aspect of your statement.<br />
                            <br />
                            <strong>All our documents can be supplied in large print, Braille, audio tape and in
                                languages other than English. Please contact 0303 303 0003 if you require this service.</strong>
                        </td>
                        <td style='font-size: 12px'>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="69%" valign="top" style='font-size: 12px'>
                            &nbsp;
                        </td>
                        <td width="31%" style='font-size: 12px'>
                            <div align="right" class="style3">
                                &nbsp;Broadland Housing Association</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="69%" valign="top" style='font-size: 12px'>
                            &nbsp;
                        </td>
                        <td width="31%" style='font-size: 12px'>
                            <div align="right" class="style3">
                                &nbsp;NCFC Jarrold Stand</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="69%" valign="top" style='font-size: 12px'>
                            &nbsp;
                        </td>
                        <td style='font-size: 12px'>
                            <div align="right" class="style3">
                                &nbsp;Carrow Road</div>
                        </td>
                    </tr>
                    <tr>
                        <td width="69%" valign="top" style='font-size: 12px'>
                            &nbsp;
                        </td>
                        <td style='font-size: 12px' align="right">
                            &nbsp;<font face="Arial, Helvetica, sans-serif">Norwich NR1 1HU</font>
                        </td>
                    </tr>
                    <tr>
                        <td width="65%" valign="top" style='font-size: 12px'>
                            &nbsp;
                        </td>
                        <td style='font-size: 12px' align="right">
                            &nbsp;<font face="Arial, Helvetica, sans-serif"><b>Registered under the Industrial and
                                Provident Societies Act 1965 as a non profit making housing association with charitable
                                status Reg. No. 16274R. Housing Corporation Reg. No. L0026</b></font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
