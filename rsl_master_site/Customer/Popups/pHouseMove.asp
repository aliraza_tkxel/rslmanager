<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="/Includes/Database/adovbs.inc"-->


<%

	'' Created By:	Munawar Nadeem(TkXel)
	'' Created On: 	JULY 1, 2008
	'' Reason:		Integraiton with Tenats Online 

	

	Dim general_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	
	Dim path				' determines whether path has been submitted from itself
	

	Dim  lstAuthority, lstDevelopment

	Dim title, description, authorityID, developmentID, noOfBedrooms, occupantsAbove18, occupantsBelow18

	
	Dim journalID 	
	
	
	// begin processing

	path = request.form("hid_go")
	general_history_id 	= Request("generalhistoryid")

	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	' Frist Time request 
	If path = 0 Then

		get_record()
		
		Call BuildSelect(lstAuthority, "sel_Authority", "G_LOCALAUTHORITY"  , "LocalAuthorityID , Description ", "Description", "Please Select", NULL, NULL, "textbox200", "onchange='authority_change()'")

		CloseDB()

	' enters into if page submitted on itself
	Elseif path = 1 Then

		Call new_record(journalID)

		CloseDB()

		'response.write "<p>JournalID " & journalID
		'response.end

		Response.Redirect ("../iFrames/iHouseMoveDetail.asp?journalid=" & journalID )

	End If
	

	
	' This function will get detail/record from C_HOUSE_MOVE table that will be used to populate the form
	Function get_record()
	
	 	Dim strSQL

		strSQL = 	"SELECT		" &_
					"			ISNULL(H.MOVINGBECAUSE, 'N/A') AS NOTES," &_
					"			H.LOCALAUTHORITYID, " &_
					"			H.DEVELOPMENTID, " &_
					"			H.NOOFBEDROOMS, H.OCCUPANTSABOVE18, H.OCCUPANTSBELOW18, " &_
					"			J.TITLE " &_
					"   FROM C_HOUSE_MOVE H  " &_
					"			INNER JOIN C_JOURNAL J ON H.JOURNALID = J.JOURNALID " &_
					"WHERE		H.HOUSEMOVEHISTORYID = " & general_history_id 

	
		Call OpenRs(rsSet, strSQL)
		
		description			= rsSet("NOTES")
		authorityID			= rsSet("LOCALAUTHORITYID")
		developmentID		= rsSet("DEVELOPMENTID")
		noOfBedrooms		= rsSet("NOOFBEDROOMS")
		occupantsAbove18		= rsSet("OCCUPANTSABOVE18")
		occupantsBelow18		= rsSet("OCCUPANTSBELOW18")
		title 			= rsSet("TITLE")
	
		CloseRs(rsSet)

	End Function
	


     ' This function use to save reponse entered by admin
	Function new_record(ByRef journID)

		genHistoryID    = Request.form("generalhistoryid")
		description		  = Request.form("txt_MovingBecause")
		authorityID		  = Request.form("sel_Authority")
		developmentID	  = Request.form("sel_Development")
		noOfBedrooms	  = Request.form("sel_Bedroom")
		occupantsAbove18	  = Request.form("sel_OccupantsAbove18")
		occupantsBelow18	  = Request.form("sel_OccupantsBelow18")
				
	
		' Checking whether CLOSE ENQUIRY checkbox is ticked or not
		If (Request.Form("chk_CLOSE") = 1) then
			newStatus = 14 '-- close in C_STATUS
			closeFlag = 1
	
		Else
			newStatus = 13 '-- open in C_STATUS
			closeFlag = 0
		End if		

		
		userID = Session("userid") 
	
		

		ON ERROR RESUME NEXT

		'' Starting transaction 
		Conn.BeginTrans


		'Call to save response in C_JOURNAL and C_HOUSE_MOVE tables	
		Call save_response_rsl (journID, genHistoryID, newStatus, userID, description, authorityID, developmentID, noOfBedrooms, occupantsAbove18, occupantsBelow18)


   	     If Conn.Errors.Count <> 0 Then
		

	      ' Rollback if error ocured while calling save_response_rsl

		Conn.RollbackTrans
	

	    Elseif Request.Form("chk_SHOW_TENANT") = 1 Then

		' Value of journalID  is already obtained by calling save_response_rsl
		' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
		Call get_enquiry_id(journID, enqID)


		'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
		Call save_response_tenant(enqID, closeFlag) 


		  If Conn.Errors.Count <> 0 Then


		    ' Rollback if error ocured while calling save_response_tenant
		    Conn.RollbackTrans

		  Else
   		    ' Commit if both save_response_tenant & save_ersponse_rsl are executed successfully
		    Conn.CommitTrans			



		  End if

	    Else

		' if show to Tenant checkbox is unchecked then commit transaction
   		' Commit only if save_ersponse_rsl is executed successfully
		Conn.CommitTrans

		
	    End if	


	End Function


	' This function is used to INSERT record/response IN C_HOUSE_MOVE TABLE and to UPDATE C_JOURNAL TABLE

	Function save_response_rsl(ByRef journalID, historyID, status, userID, desc, authID, devID, bedrooms, occupAbove18, occupBelow18) 


	  Set comm = Server.CreateObject("ADODB.Command")

	  ' setting stored procedure name
	  comm.commandtext = "C_HOUSE_MOVE_UPDATE"
	  comm.commandtype = 4
	  comm.namedparameters = true
	  Set comm.activeconnection = Conn

	 
	  ' this parameter will return code/value, i.e. JOURNALID or -1
	  comm.parameters.append( comm.CreateParameter ("@returnValue" , adInteger, adParamReturnValue) )

	 ' setting input parameter for sproc 
	  comm.parameters.append (comm.CreateParameter ("@HISTORYID", adInteger, adParamInput, 4, historyID ) )
	  comm.parameters.append (comm.CreateParameter ("@CURRENTITEMSTATUSID", adInteger, adParamInput, 4, status ) )
	  comm.parameters.append (comm.CreateParameter ("@LASTACTIONUSER", adInteger, adParamInput, 4, userID ) )
	  comm.parameters.append (comm.CreateParameter ("@MOVINGBECAUSE", adVarChar, adParamInput, 4000, desc ) )
	  comm.parameters.append (comm.CreateParameter ("@LOCALAUTHORITYID", adInteger, adParamInput, 4, authID ) )

	  comm.parameters.append (comm.CreateParameter ("@DEVELOPMENTID", adInteger, adParamInput, 4, devID ) )

	  comm.parameters.append (comm.CreateParameter ("@NOOFBEDROOMS", adInteger, adParamInput, 4, bedrooms ) )
	  comm.parameters.append (comm.CreateParameter ("@OCCUPANTSABOVE18", adInteger, adParamInput, 4, occupAbove18 ) )
	  comm.parameters.append (comm.CreateParameter ("@OCCUPANTSBELOW18", adInteger, adParamInput, 4, occupBelow18 ) )
		
  	  ' executing sproc 
 
	  comm.execute()

	  journalID =  comm.Parameters("@returnValue").value	

	End Function



	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jrnlID, ByRef enqID)


		' CALL To STORED PROCEDURE TO get enquiry ID

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jrnlID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

					
	     ' if record found/returned, setting values
		If Not rs.EOF Then 
			enqID = rs("EnquiryLogID")
	      End If

		
	     Set comm = Nothing
	     Set rs = Nothing

	End Function



	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag) 

	  Set comm = Server.CreateObject("ADODB.Command")

	  ' setting stored procedure name
	  comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
	  comm.commandtype = 4
	  comm.namedparameters = true
	  Set comm.activeconnection = Conn

  

	 ' setting input parameter for sproc 
	  comm.parameters.append (comm.CreateParameter ("@enqLogID", adInteger, adParamInput, 4, enqID ) )
	  comm.parameters.append (comm.CreateParameter ("@closeFlag", adBoolean, adParamInput, 4, closeFlag ) )
		
  	  ' executing sproc 
	  comm.execute


	End Function


%>

<html>
<head>
<title>Update Enquiry</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

      var FormFields = new Array();
	FormFields[0] = "txt_MovingBecause|MovingBecause|TEXT|Y"
	FormFields[1] = "sel_Authority|Authority|SELECT|Y"
	FormFields[2] = "sel_Development|Development|SELECT|Y"
	FormFields[3] = "sel_Bedroom|Bedroom|Bedroom|N"
	FormFields[4] = "sel_OccupantsAbove18|OccupantsAbove18|SELECT|N"
	FormFields[5] = "sel_OccupantsBelow18|OccupantsBelow18|SELECT|N"


	function authority_change(){

      	// gets called when Aurthority value changed from drop down and returns development (drop down) list
		RSLFORM.target = "ServerIFrame";
 		RSLFORM.action = "../serverside/AuthorityChange_srv.asp?AuthorityID=" + RSLFORM.sel_Authority.value + "&DevelopmentID=" + <%=developmentID%> ;

		RSLFORM.submit();
	
	}


	function save_form(){

		if (!checkForm()) return false


		if ((RSLFORM.chk_CLOSE.checked)  && (RSLFORM.txt_MovingBecause.value == "")) {
			alert("Notes have to be recorded when a item is closed.")
			return false;
		}

		
		RSLFORM.hid_go.value = 1;
		RSLFORM.action = "pHouseMove.asp"
		RSLFORM.target = "CRM_BOTTOM_FRAME"	
			if(RSLFORM.chk_CLOSE.checked == true){

				if(!confirm("The item will be automatically closed once you save.")){
					return false;
				}
			}

		RSLFORM.submit();
	      window.close();
	}


	// this function will get called on onLoad event of this form
	function set_values(){
				
		// setting value for Moving Because (reason/ description)
		var desc = document.getElementById("txt_MovingBecause");
            desc.value = "<%=description%>"

		// setting value for authority drop down
		var authorityID = document.getElementById("sel_Authority");
	      authorityID.value = "<%=authorityID%>"

		// development values ar based on Authority. This method returns list of development values 
		// with one pre-selected development, if sent by Tenant while creating/sending enquiry
		authority_change();
	

		// setting value for no. of Bedroom drop down
		var noOfBedrooms = document.getElementById("sel_Bedroom");
	      noOfBedrooms.value = "<%=noOfBedrooms%>"

		// setting value for OccupantsAbove18 drop down
		var occupantsAbove18 = document.getElementById("sel_OccupantsAbove18");
	      occupantsAbove18.value = "<%=occupantsAbove18%>"

		// setting value for OccupantsBelow18 drop down
		var occupantsBelow18 = document.getElementById("sel_OccupantsBelow18");
	      occupantsBelow18.value = "<%=occupantsBelow18%>"

	}

</script>	

<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onLoad="window.focus();set_values()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border=0 /></td>
      <td  height=10><img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border=0 /></td>	  
	  <td width=302 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'>&nbsp;</td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=4 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr valign=top>
			<td nowrap>Title:</td>
			<td width=100%> <%=title%> </td>
		</tr>

		<tr style='display:block'>
			<td>Moving Because: </td>
			<TD><textarea style='OVERFLOW:HIDDEN;WIDTH:200' class="textbox200" name="txt_MovingBecause" rows=5>
			    </textarea>
			</TD>	
			<TD><image src="/js/FVS.gif" name="img_MovingBecause" width="15px" height="15px" border="0">    </TD>

		</tr>
		
		<TR>
			<TD>Local Authority: </TD>
			<TD> 
				<%=lstAuthority%>
			</TD>
			<TD><image src="/js/FVS.gif" name="img_Authority" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>Development: </TD>
			<TD> 
				<select name="sel_Development" class='textbox200' > 	
				<option value="0" >Any</option>
				</select>


			</TD>
				<TD><image src="/js/FVS.gif" name="img_Development" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD >No. of Bedrooms: </TD>
			<TD> 
				<select name="sel_Bedroom" class='textbox200' > 
				<option value="0" selected="selected">0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_Bedroom" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD nowrap>No. of Occupants: 18+ </TD>
			<TD> 
				<select name="sel_OccupantsAbove18" class='textbox200'> 
				<option value="0" >0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_OccupantsAbove18" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>No. of Occupants: <18 </TD>
			<TD> 
				<select name="sel_OccupantsBelow18" class='textbox200'> 
				<option value="0" >0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_OccupantsBelow18" width="15px" height="15px" border="0"></TD>							
		</TR>

		<tr >
			<TD valign=top><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
				<input type="hidden" name="hid_go" value=0>
				<input type="hidden" name="small" value=1>
				<input type="hidden" name="generalhistoryid" value="<%=general_history_id%>">	
			</TD>	
			
		</tr>
		<TR>
			<TD nowrap align="right">Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" value="1"> </TD>
				
			<TD nowrap align="right"> Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" value="1" CHECKED> </TD>

		</TR>	 
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=3 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->  
		<INPUT TYPE=hidden NAME=hid_SIGNATURE value="">		
		<INPUT TYPE=hidden NAME=hid_LETTERTEXT value="">
         <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
         <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
	  </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerIFrame" style='display:none' height=400 width=300></iframe>
</BODY>
</HTML>

