<% ByPassSecurityAccess = true %>
<!--#include virtual="GlobalVariables.asp" -->

<%
OpenDB()
Function TerminationChangeEmailForVoidInspection(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs ,Recipient,EmployeeId,OperativeId, FullAddress
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, CONVERT(CHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) as AppointmentDate, " &_
			"			ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, PDR_APPOINTMENTS.ASSIGNEDTO AS OperativeId " &_
			"	 from PDR_JOURNAL " &_
			"	 inner join PDR_APPOINTMENTS on PDR_APPOINTMENTS.JOURNALID=PDR_JOURNAL.JOURNALID " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 inner join E__EMPLOYEE on EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        OperativeName = rs("OperativeName")
        AppointmentDate = rs("AppointmentDate")
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
        Recipient = rs("Email")
		OperativeId= rs("OperativeId")
    End IF
    Call CloseRs(rs)

     socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------
    Dim emailbody
    emailbody =  TerminationChangeEmailForVoidInspectionTemplate()
    if(NOT ISNULL(OperativeName)) THEN
    emailbody = Replace(emailbody,"{Operative}",OperativeName)
    END IF

    if(NOT ISNULL(AppointmentDate)) THEN
    emailbody = Replace(emailbody,"{AppointmentDate}",AppointmentDate)
    END IF

    if(NOT ISNULL(Address)) THEN
    emailbody = Replace(emailbody,"{Address}",Address)
    END IF

    if(NOT ISNULL(TownCity)) THEN
    emailbody = Replace(emailbody,"{TownCity}",TownCity)
    END IF

    if(NOT ISNULL(PostCode)) THEN
    emailbody = Replace(emailbody,"{PostCode}",PostCode)
    END IF

    if(NOT ISNULL(prevTerminationDate)) THEN
    emailbody = Replace(emailbody,"{FromTerminationDate}",prevTerminationDate)
    END IF

    if(NOT ISNULL(currTerminationDate)) THEN
    emailbody = Replace(emailbody,"{ToTerminationDate}",currTerminationDate)
    END IF

    if(NOT ISNULL(currTerminationDate)) THEN
    emailbody = Replace(emailbody,"{ReletDate}",currReletDate)
    END IF
	FullAddress = Address & "," & TownCity & "," & PostCode
    emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
    emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
    sendEmail emailbody,Recipient,"Termination Date has been changed"
	PostHTML OperativeId,FullAddress


End Function

Function TerminationChangeEmailForVoidWorksToBeArranged(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs ,Recipient,EmployeeId,OperativeId, FullAddress
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo
   
    SQL = "select top 1 ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, CONVERT(CHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) as AppointmentDate, " &_
			"			ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, PDR_APPOINTMENTS.ASSIGNEDTO AS OperativeId " &_
			"	 from PDR_JOURNAL " &_
			"	 inner join PDR_APPOINTMENTS on PDR_APPOINTMENTS.JOURNALID=PDR_JOURNAL.JOURNALID " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 inner join E__EMPLOYEE on EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        OperativeName = rs("OperativeName")
        AppointmentDate = rs("AppointmentDate")
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
        Recipient = rs("Email")
		OperativeId= rs("OperativeId")
    End IF
    Call CloseRs(rs)
   
     socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------
    Dim emailbody
    emailbody =  TerminationChangeEmailForVoidWorksToBeArrangedTemplate()
    if(NOT ISNULL(OperativeName)) THEN
    emailbody = Replace(emailbody,"{Operative}",OperativeName)
    END IF

    if(NOT ISNULL(AppointmentDate)) THEN
    emailbody = Replace(emailbody,"{AppointmentDate}",AppointmentDate)
    END IF

    if(NOT ISNULL(Address)) THEN
    emailbody = Replace(emailbody,"{Address}",Address)
    END IF

    if(NOT ISNULL(TownCity)) THEN
    emailbody = Replace(emailbody,"{TownCity}",TownCity)
    END IF

    if(NOT ISNULL(PostCode)) THEN
    emailbody = Replace(emailbody,"{PostCode}",PostCode)
    END IF

    if(NOT ISNULL(prevTerminationDate)) THEN
    emailbody = Replace(emailbody,"{FromTerminationDate}",prevTerminationDate)
    END IF

    if(NOT ISNULL(currTerminationDate)) THEN
    emailbody = Replace(emailbody,"{ToTerminationDate}",currTerminationDate)
    END IF

    if(NOT ISNULL(currTerminationDate)) THEN
    emailbody = Replace(emailbody,"{ReletDate}",currReletDate)
    END IF
     FullAddress = Address & "," & TownCity & "," & PostCode
    emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
    emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
    sendEmail emailbody,Recipient,"Termination Date has been changed"

PostHTML OperativeId,FullAddress

End Function

Function TerminationChangeEmailForVoidWorksAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs ,Recipient,EmployeeId, purchaseOrder
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo
   
   SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)
   
    SQL = " select DISTINCT(PurchaseOrderId) PurchaseOrderId, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, S_ORGANISATION.NAME as OrgName " &_
			"	from V_RequiredWorks " &_
            "    INNER JOIN PDR_CONTRACTOR_WORK ON V_RequiredWorks.WorksJournalId = PDR_CONTRACTOR_WORK.JournalId " &_
			"	INNER JOIN F_PURCHASEORDER ON PDR_CONTRACTOR_WORK.PurchaseOrderId = F_PURCHASEORDER.ORDERID " &_
			"	INNER JOIN F_POSTATUS ON F_PURCHASEORDER.POSTATUS = F_POSTATUS.POSTATUSID  " &_
			"	INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_CONTRACTOR_WORK.ContactId " &_
			"	INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	INNER JOIN S_ORGANISATION ON PDR_CONTRACTOR_WORK.ContractorId = S_ORGANISATION.ORGID " &_
            "   WHERE F_POSTATUS.POSTATUSNAME in ('Queued', 'Goods Ordered', 'Work Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved') " &_
			"	and InspectionJournalId = " & PdrJournalID
    

      socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------

    Call OpenRs(rs, SQL)
    While Not rs.EOF
        Dim emailbody
        emailbody =  TerminationChangeEmailForVoidWorksAssignedToContractorTemplate()
        
        emailbody = Replace(emailbody,"{Operative}",rs("OrgName"))

        emailbody = Replace(emailbody,"{ToTerminationDate}",currTerminationDate)
		if(NOT ISNULL(Address)) THEN
			emailbody = Replace(emailbody,"{Address}",Address)
			END IF

			if(NOT ISNULL(TownCity)) THEN
			emailbody = Replace(emailbody,"{TownCity}",TownCity)
			END IF

			if(NOT ISNULL(PostCode)) THEN
			emailbody = Replace(emailbody,"{PostCode}",PostCode)
			END IF
        Recipient = Trim(rs("Email"))
        If Recipient <> "" then
            sendEmail emailbody,Recipient,"Termination Date has been changed"
        End if

        rs.MoveNext
    Wend
    Call CloseRs(rs)


End Function

Function ReletChangeEmailForVoidWorksAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs ,Recipient,EmployeeId, purchaseOrder
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo
   
   SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)
   
   
    SQL = " select DISTINCT(PurchaseOrderId) PurchaseOrderId, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, S_ORGANISATION.NAME as OrgName " &_
			"	from V_RequiredWorks " &_
            "    INNER JOIN PDR_CONTRACTOR_WORK ON V_RequiredWorks.WorksJournalId = PDR_CONTRACTOR_WORK.JournalId " &_
			"	INNER JOIN F_PURCHASEORDER ON PDR_CONTRACTOR_WORK.PurchaseOrderId = F_PURCHASEORDER.ORDERID " &_
			"	INNER JOIN F_POSTATUS ON F_PURCHASEORDER.POSTATUS = F_POSTATUS.POSTATUSID  " &_
			"	INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_CONTRACTOR_WORK.ContactId " &_
			"	INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	INNER JOIN S_ORGANISATION ON PDR_CONTRACTOR_WORK.ContractorId = S_ORGANISATION.ORGID " &_
            "   WHERE F_POSTATUS.POSTATUSNAME in ('Queued', 'Goods Ordered', 'Work Ordered', 'Goods Received', 'Invoice Received', 'Invoice Approved', 'Reconciled', 'Declined', 'Goods Approved') " &_
			"	and InspectionJournalId = " & PdrJournalID
    

      socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------

    Call OpenRs(rs, SQL)
    While Not rs.EOF
        Dim emailbody
        emailbody =  ReletChangeEmailForVoidWorksAssignedToContractorTemplate()
        
        emailbody = Replace(emailbody,"{Operative}",rs("OrgName"))

        emailbody = Replace(emailbody,"{ToReletDate}",currReletDate)
		if(NOT ISNULL(Address)) THEN
			emailbody = Replace(emailbody,"{Address}",Address)
			END IF

			if(NOT ISNULL(TownCity)) THEN
			emailbody = Replace(emailbody,"{TownCity}",TownCity)
			END IF

			if(NOT ISNULL(PostCode)) THEN
			emailbody = Replace(emailbody,"{PostCode}",PostCode)
			END IF
        Recipient = Trim(rs("Email"))
        If Recipient <> "" then
            sendEmail emailbody,Recipient,"Relet Date has been changed"
        End if

        rs.MoveNext
    Wend
    Call CloseRs(rs)


End Function

Function ReletChangeEmailForVoidWorksArranged(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs, rsSet ,Recipient,EmployeeId,OperativeId, FullAddress
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)

    strSQL = "select ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, " &_
           " ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, PDR_APPOINTMENTS.ASSIGNEDTO AS OperativeId  " &_
			"	 from PDR_APPOINTMENTS " &_
			"	 INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where JOURNALID in  " &_
			"	 (Select distinct work.WorksJournalId " &_
             "        from PDR_JOURNAL j   " &_
              "   INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
              "   INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
              "   INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
              "   OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
              "   case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status] " &_                                               
              "    from V_RequiredWorks w   " &_
              "    Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
              "    Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
              "    LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
              "    where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
              "    inner join PDR_JOURNAL pj on pj.JOURNALID = work.WorksJournalId   " &_
			"	  inner join PDR_APPOINTMENTS pas on pas.JOURNALID=pj.JOURNALID    " &_
			"	 INNER JOIN PDR_MSAT pms ON pms.MSATId = pj.MSATID    " &_
			"	 INNER JOIN P__PROPERTY pp ON pp.PROPERTYID = pms.PropertyId    " &_
			"	 inner join E__EMPLOYEE emp on emp.EMPLOYEEID = pas.ASSIGNEDTO    " &_
             "   INNER JOIN E_CONTACT ec ON ec.EMPLOYEEID = emp.EMPLOYEEID    " &_
              "   Where j.JOURNALID = " &  PdrJournalID & " and work.WorksJournalId is not null and work.PurchaseOrderId is null AND work.Status = 'Arranged') "
    
    Call OpenRs(rsSet, strSQL)
    While Not rsSet.EOF
        OperativeName = rsSet("OperativeName")
        Recipient = Trim(rsSet("Email"))
		OperativeId= rsSet("OperativeId")
        if Recipient <> "" then
             socket   = "http"
            If Request.ServerVariables("HTTPS") = "on" then 
                socket = "https"
            End if

            '------------------------------------------------
            ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
            '------------------------------------------------
            Dim emailbody
            emailbody =  ReletChangeEmailForVoidWorksArrangedTemplate()
            if(NOT ISNULL(OperativeName)) THEN
            emailbody = Replace(emailbody,"{Operative}",OperativeName)
            END IF
			if(NOT ISNULL(Address)) THEN
			emailbody = Replace(emailbody,"{Address}",Address)
			END IF

			if(NOT ISNULL(TownCity)) THEN
			emailbody = Replace(emailbody,"{TownCity}",TownCity)
			END IF

			if(NOT ISNULL(PostCode)) THEN
			emailbody = Replace(emailbody,"{PostCode}",PostCode)
			END IF
            emailbody = Replace(emailbody,"{ToReletDate}",currReletDate)
			FullAddress = Address & "," & TownCity & "," & PostCode
            emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
            emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
            sendEmail emailbody,Recipient,"Relet Date has been changed"
			PostHTML OperativeId,FullAddress
        end if
        rsSet.MoveNext
    Wend
    Call CloseRs(rsSet)


End Function

Function TerminationCancelEmailForVoidInspection(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, CONVERT(CHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) as AppointmentDate, " &_
			"			ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email " &_
			"	 from PDR_JOURNAL " &_
			"	 inner join PDR_APPOINTMENTS on PDR_APPOINTMENTS.JOURNALID=PDR_JOURNAL.JOURNALID " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 inner join E__EMPLOYEE on EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        OperativeName = rs("OperativeName")
        AppointmentDate = rs("AppointmentDate")
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
        Recipient = rs("Email")
    End IF
    Call CloseRs(rs)

     socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------
    Dim emailbody
    emailbody =  TerminationCancelEmailForVoidInspectionTemplate()
    if(NOT ISNULL(OperativeName)) THEN
    emailbody = Replace(emailbody,"{Operative}",OperativeName)
    END IF

    if(NOT ISNULL(AppointmentDate)) THEN
    emailbody = Replace(emailbody,"{AppointmentDate}",AppointmentDate)
    END IF

    if(NOT ISNULL(Address)) THEN
    emailbody = Replace(emailbody,"{Address}",Address)
    END IF

    if(NOT ISNULL(TownCity)) THEN
    emailbody = Replace(emailbody,"{TownCity}",TownCity)
    END IF

    if(NOT ISNULL(PostCode)) THEN
    emailbody = Replace(emailbody,"{PostCode}",PostCode)
    END IF


    emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
    emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
    sendEmail emailbody,Recipient,"Termination has been cancelled"

End Function

Function TerminationCancelEmailForVoidWorks(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs, rsSet ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)

    strSQL = "select ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, " &_
            "           ISNULL(E_CONTACT.WORKEMAIL, '')  AS Email " &_
			"	 from PDR_APPOINTMENTS " &_
			"	 INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where JOURNALID in  " &_
			"	 (Select distinct work.WorksJournalId " &_
             "        from PDR_JOURNAL j   " &_
              "   INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
              "   INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
              "   INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
              "   OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
              "   case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status] " &_                                               
              "    from V_RequiredWorks w   " &_
              "    Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
              "    Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
              "    LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
              "    where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
              "    inner join PDR_JOURNAL pj on pj.JOURNALID = work.WorksJournalId   " &_
			"	  inner join PDR_APPOINTMENTS pas on pas.JOURNALID=pj.JOURNALID    " &_
			"	 INNER JOIN PDR_MSAT pms ON pms.MSATId = pj.MSATID    " &_
			"	 INNER JOIN P__PROPERTY pp ON pp.PROPERTYID = pms.PropertyId    " &_
			"	 inner join E__EMPLOYEE emp on emp.EMPLOYEEID = pas.ASSIGNEDTO    " &_
             "   INNER JOIN E_CONTACT ec ON ec.EMPLOYEEID = emp.EMPLOYEEID    " &_
              "   Where j.JOURNALID = " &  PdrJournalID & " and work.WorksJournalId is not null and work.PurchaseOrderId is null AND work.Status = 'Arranged' and pas.APPOINTMENTSTATUS = 'NotStarted') "
    
    Call OpenRs(rsSet, strSQL)
    While Not rsSet.EOF
        OperativeName = rsSet("OperativeName")
        Recipient = Trim(rsSet("Email"))
        if Recipient <> "" then
             socket   = "http"
            If Request.ServerVariables("HTTPS") = "on" then 
                socket = "https"
            End if

            '------------------------------------------------
            ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
            '------------------------------------------------
            Dim emailbody
            emailbody =  TerminationCancelEmailForVoidWorksTemplate()
            if(NOT ISNULL(OperativeName)) THEN
            emailbody = Replace(emailbody,"{Operative}",OperativeName)
            END IF

            if(NOT ISNULL(Address)) THEN
            emailbody = Replace(emailbody,"{Address}",Address)
            END IF

            if(NOT ISNULL(TownCity)) THEN
            emailbody = Replace(emailbody,"{TownCity}",TownCity)
            END IF

            if(NOT ISNULL(PostCode)) THEN
            emailbody = Replace(emailbody,"{PostCode}",PostCode)
            END IF


            emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
            emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
            sendEmail emailbody,Recipient,"Termination has been cancelled"
        end if
        rsSet.MoveNext
    Wend
    Call CloseRs(rsSet)


End Function

Function TerminationCancelEmailForVWStarted(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs, rsSet ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)

    strSQL = "select ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, " &_
            "           ISNULL(E_CONTACT.WORKEMAIL, '')  AS Email " &_
			"	 from PDR_APPOINTMENTS " &_
			"	 INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where JOURNALID in  " &_
			"	 (Select distinct work.WorksJournalId " &_
             "        from PDR_JOURNAL j   " &_
              "   INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
              "   INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
              "   INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
              "   OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
              "   case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status] " &_                                               
              "    from V_RequiredWorks w   " &_
              "    Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
              "    Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
              "    LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
              "    where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
              "    inner join PDR_JOURNAL pj on pj.JOURNALID = work.WorksJournalId   " &_
			"	  inner join PDR_APPOINTMENTS pas on pas.JOURNALID=pj.JOURNALID    " &_
			"	 INNER JOIN PDR_MSAT pms ON pms.MSATId = pj.MSATID    " &_
			"	 INNER JOIN P__PROPERTY pp ON pp.PROPERTYID = pms.PropertyId    " &_
			"	 inner join E__EMPLOYEE emp on emp.EMPLOYEEID = pas.ASSIGNEDTO    " &_
             "   INNER JOIN E_CONTACT ec ON ec.EMPLOYEEID = emp.EMPLOYEEID    " &_
              "   Where j.JOURNALID = " &  PdrJournalID & " and work.WorksJournalId is not null and work.PurchaseOrderId is null AND work.Status = 'Arranged' and pas.APPOINTMENTSTATUS != 'NotStarted' and pas.APPOINTMENTSTATUS != 'Complete') "
    
    Call OpenRs(rsSet, strSQL)
    While Not rsSet.EOF
        OperativeName = rsSet("OperativeName")
        Recipient = Trim(rsSet("Email"))
        if Recipient <> "" then
             socket   = "http"
            If Request.ServerVariables("HTTPS") = "on" then 
                socket = "https"
            End if

            '------------------------------------------------
            ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
            '------------------------------------------------
            Dim emailbody
            emailbody =  TerminationCancelEmailForVWStartedTemplate()
            if(NOT ISNULL(OperativeName)) THEN
            emailbody = Replace(emailbody,"{Operative}",OperativeName)
            END IF
			if(NOT ISNULL(Address)) THEN
            emailbody = Replace(emailbody,"{Address}",Address)
            END IF

            if(NOT ISNULL(TownCity)) THEN
            emailbody = Replace(emailbody,"{TownCity}",TownCity)
            END IF

            if(NOT ISNULL(PostCode)) THEN
            emailbody = Replace(emailbody,"{PostCode}",PostCode)
            END IF

            emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
            emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
            sendEmail emailbody,Recipient,"Termination has been cancelled"
        end if
        rsSet.MoveNext
    Wend
    Call CloseRs(rsSet)


End Function

Function TerminationCancelEmailForVWCompleted(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs, rsSet ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

  
    SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)

    strSQL = "select ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, " &_
            "           ISNULL(E_CONTACT.WORKEMAIL, '')  AS Email " &_
			"	 from PDR_APPOINTMENTS " &_
			"	 INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where JOURNALID in  " &_
			"	 (Select distinct work.WorksJournalId " &_
             "        from PDR_JOURNAL j   " &_
              "   INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
              "   INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
              "   INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
              "   OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
              "   case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status] " &_                                               
              "    from V_RequiredWorks w   " &_
              "    Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
              "    Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
              "    LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
              "    where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
              "    inner join PDR_JOURNAL pj on pj.JOURNALID = work.WorksJournalId   " &_
			"	  inner join PDR_APPOINTMENTS pas on pas.JOURNALID=pj.JOURNALID    " &_
			"	 INNER JOIN PDR_MSAT pms ON pms.MSATId = pj.MSATID    " &_
			"	 INNER JOIN P__PROPERTY pp ON pp.PROPERTYID = pms.PropertyId    " &_
			"	 inner join E__EMPLOYEE emp on emp.EMPLOYEEID = pas.ASSIGNEDTO    " &_
             "   INNER JOIN E_CONTACT ec ON ec.EMPLOYEEID = emp.EMPLOYEEID    " &_
              "   Where j.JOURNALID = " &  PdrJournalID & " and work.WorksJournalId is not null and work.PurchaseOrderId is null AND work.Status = 'Arranged' and pas.APPOINTMENTSTATUS = 'Complete') "
    
    Call OpenRs(rsSet, strSQL)
    While Not rsSet.EOF
        OperativeName = rsSet("OperativeName")
        Recipient = Trim(rsSet("Email"))
        if Recipient <> "" then
             socket   = "http"
            If Request.ServerVariables("HTTPS") = "on" then 
                socket = "https"
            End if

            '------------------------------------------------
            ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
            '------------------------------------------------
            Dim emailbody
            emailbody =  TerminationCancelEmailForVWCompletedTemplate()
            if(NOT ISNULL(OperativeName)) THEN
            emailbody = Replace(emailbody,"{Operative}",OperativeName)
            END IF
			if(NOT ISNULL(Address)) THEN
            emailbody = Replace(emailbody,"{Address}",Address)
            END IF

            if(NOT ISNULL(TownCity)) THEN
            emailbody = Replace(emailbody,"{TownCity}",TownCity)
            END IF

            if(NOT ISNULL(PostCode)) THEN
            emailbody = Replace(emailbody,"{PostCode}",PostCode)
            END IF
            emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
            emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
            sendEmail emailbody,Recipient,"Termination has been cancelled"
        end if
        rsSet.MoveNext
    Wend
    Call CloseRs(rsSet)


End Function

Function TerminationCancelEmailForVWAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
   
   Dim OperativeName,AppointmentDate,Address,TownCity,PostCode, Email,TenantName, Telephone, rs, rsSet ,Recipient,EmployeeId,purchaseOrder
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo
 
    SQL = "select top 1 ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode " &_
			"	 from PDR_JOURNAL " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 where PDR_JOURNAL.JOURNALID = " & PdrJournalID


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Address = rs("Address")
        TownCity = rs("TownCity")
        PostCode = rs("PostCode")
    End IF
    Call CloseRs(rs)
   
   
    strSQL = " select ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, " &_
			 "			ISNULL(E_CONTACT.WORKEMAIL, '')  AS Email, PurchaseOrderId " &_
			 "   from PDR_CONTRACTOR_WORK " &_
			"	 INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = PDR_CONTRACTOR_WORK.ContactId " &_
			"	 INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where JOURNALID in  " &_
			"	 (Select distinct work.WorksJournalId " &_
             "        from PDR_JOURNAL j   " &_
              "   INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
               "  INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
              "   INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
              "   OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
              "   case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]                                        " &_                                               
              "    from V_RequiredWorks w   " &_
              "    Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
              "    Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
              "    LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
              "    where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
              "   Where j.JOURNALID = " &  PdrJournalID & " and work.WorksJournalId is not null and work.PurchaseOrderId is not null AND work.Status = 'Assigned To Contractor') "

    Call OpenRs(rsSet, strSQL)
    While Not rsSet.EOF
        OperativeName = rsSet("OperativeName")
		Address = rsSet("Address")
        TownCity = rsSet("TownCity")
        PostCode = rsSet("PostCode")
        purchaseOrder = PurchaseNumberFault(Trim(Cstr(rsSet("PurchaseOrderId"))))
        Recipient = Trim(rsSet("Email"))
        if Recipient <> "" then
             socket   = "http"
            If Request.ServerVariables("HTTPS") = "on" then 
                socket = "https"
            End if

            '------------------------------------------------
            ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
            '------------------------------------------------
            Dim emailbody
            emailbody =  TerminationCancelEmailForVWAssignedToContractorTemplate()
            if(NOT ISNULL(OperativeName)) THEN
                emailbody = Replace(emailbody,"{Operative}",OperativeName)
            END IF

            if(NOT ISNULL(purchaseOrder)) THEN
                emailbody = Replace(emailbody,"{PONumber}",purchaseOrder)
            END IF
			if(NOT ISNULL(Address)) THEN
            emailbody = Replace(emailbody,"{Address}",Address)
            END IF

            if(NOT ISNULL(TownCity)) THEN
            emailbody = Replace(emailbody,"{TownCity}",TownCity)
            END IF

            if(NOT ISNULL(PostCode)) THEN
            emailbody = Replace(emailbody,"{PostCode}",PostCode)
            END IF

            emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
            emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
            sendEmail emailbody,Recipient,"Termination has been cancelled"
        end if
        rsSet.MoveNext
    Wend
    Call CloseRs(rsSet)


End Function


    '--------------------------  TEST

   Function sendEmail(body,recipient,subject)
   
    if recipient <> "" then
        Dim iMsg
    Set iMsg = CreateObject("CDO.Message")
    Dim iConf
    Set iConf = CreateObject("CDO.Configuration")

    Dim Flds
    Set Flds = iConf.Fields
    Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

    emailbody = body
    emailSubject = subject    
    Set iMsg.Configuration = iConf
    iMsg.To = recipient
    iMsg.From = "noreply@broadlandgroup.org"
    iMsg.Subject = emailSubject
    iMsg.HTMLBody = emailbody
    iMsg.Send	

    end if
End Function

Function TerminationChangeEmailForVoidInspectionTemplate()
    Dim TerminationChangeForVoidInspectionEmailTemplate
    TerminationChangeForVoidInspectionEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'> You are being notified of the change in termination date, as you are currently scheduled to attend a Void Inspection on {AppointmentDate} at:<br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> The termination date has been changed from {FromTerminationDate} to {ToTerminationDate} for the above property. </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> The Relet Date is now {ReletDate}. <br/> <br/> Regards<br/> <br/> Broadland<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationChangeEmailForVoidInspectionTemplate = TerminationChangeForVoidInspectionEmailTemplate
End Function

Function TerminationChangeEmailForVoidWorksToBeArrangedTemplate()
    Dim TerminationChangeForVoidWorksToBeArrangedEmailTemplate
    TerminationChangeForVoidWorksToBeArrangedEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'> You are being notified of the change in termination date, as you completed a Void Inspection on {AppointmentDate} at:<br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> The termination date has been changed from {FromTerminationDate} to {ToTerminationDate} for the above property. </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> The Relet Date is now {ReletDate}.  <br /> <br />Please cancel any recorded void works.<br/> <br/> Regards<br/> <br/> Broadland<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationChangeEmailForVoidWorksToBeArrangedTemplate = TerminationChangeForVoidWorksToBeArrangedEmailTemplate
End Function

Function TerminationChangeEmailForVoidWorksAssignedToContractorTemplate()
    Dim TerminationChangeForVoidWorksAssignedToContractorEmailTemplate
    TerminationChangeForVoidWorksAssignedToContractorEmailTemplate = " <table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> <br /><br />Please Note:<br/><br/></td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'> The termination date has been changed for this property. The new termination date is {ToTerminationDate}. For further details please contact us.<bhg disclaimer text> </td></tr><tr> <td> <br />Broadland Housing</td></tr></table>  "
    TerminationChangeEmailForVoidWorksAssignedToContractorTemplate = TerminationChangeForVoidWorksAssignedToContractorEmailTemplate
End Function

Function ReletChangeEmailForVoidWorksAssignedToContractorTemplate()
    Dim ReletChangeForVoidWorksAssignedToContractorEmailTemplate
    ReletChangeForVoidWorksAssignedToContractorEmailTemplate = " <table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'> <br />Please Note:<br/><br/></td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'>The relet date has been changed for this property. The new relet date is {ToReletDate}. For further details please contact us.<bhg disclaimer text> </td></tr><tr> <td> <br />Broadland Housing</td></tr></table> "
    ReletChangeEmailForVoidWorksAssignedToContractorTemplate = ReletChangeForVoidWorksAssignedToContractorEmailTemplate
End Function

Function TerminationCancelEmailForVoidInspectionTemplate()
    Dim TerminationCancelForVoidInspectionEmailTemplate
    TerminationCancelForVoidInspectionEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'> You are being notified of the change in termination date, as you are currently scheduled to attend a Void Inspection on {AppointmentDate} at:<br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr><td><br/> Regards<br/> <br/> Broadland<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationCancelEmailForVoidInspectionTemplate = TerminationCancelForVoidInspectionEmailTemplate
End Function

Function TerminationCancelEmailForVoidWorksTemplate()
    Dim TerminationCancelForVoidWorksEmailTemplate
    TerminationCancelForVoidWorksEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'>You are being notified cancelled termination, as you have void works appointments arranged/started at:<br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> As termination date has been cancelled, all voids works appointments associated with the above property will automatically be removed from your app when you next sync with RSLmanger. Please note any details/photographs will not be saved. </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td> <br/> Regards<br/> <br/> Broadland<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationCancelEmailForVoidWorksTemplate = TerminationCancelForVoidWorksEmailTemplate
End Function

Function TerminationCancelEmailForVWAssignedToContractorTemplate()
    Dim TerminationCancelFoVWAssignedToContractorEmailTemplate
    TerminationCancelFoVWAssignedToContractorEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'>Please note that {PONumber} Has been cancelled - please do not proceed with the works previously requested. If you have any questions regarding this cancellation please contact the PO originator.<br/>  </td></tr><tr> <td> <br/> Many thanks<br/> <br/> Broadland<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationCancelEmailForVWAssignedToContractorTemplate = TerminationCancelFoVWAssignedToContractorEmailTemplate
End Function

Function TerminationCancelEmailForVWStartedTemplate()
    Dim TerminationCancelForVoidWorksEmailTemplate
    TerminationCancelForVoidWorksEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> Hi<b> {Operative}</b><br/><br/> Please Note: </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'>The void works appointment has been cancelled due to a change in tenancy termination date. <br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>Please contact your supervisor for further details.</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td>  Thanks<br/> <br/> Customer Services Team<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationCancelEmailForVWStartedTemplate = TerminationCancelForVoidWorksEmailTemplate
End Function

Function TerminationCancelEmailForVWCompletedTemplate()
    Dim TerminationCancelForVoidWorksEmailTemplate
    TerminationCancelForVoidWorksEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> Hi<b> {Operative}</b><br/><br/> Please Note:<br/></td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'>The void works appointment has been cancelled due to a change in tenancy termination date. <br/> <br/> <bhg disclaimer text> </td></tr><tr> <td> <table> <tr> <td style='padding-right: 50px;'>Please contact your supervisor for further details.</td></tr></table> </td></tr><tr> <td> &nbsp; </td><td> &nbsp; </td><td> &nbsp; </td></tr><tr> <td>  Thanks<br/> <br/> Customer Services Team<br/> </td><td colspan='2'> &nbsp; </td></tr></table> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    TerminationCancelEmailForVWCompletedTemplate = TerminationCancelForVoidWorksEmailTemplate
End Function

Function ReletChangeEmailForVoidWorksArrangedTemplate()
    Dim ReletChangeForVoidWorksEmailTemplate
    ReletChangeForVoidWorksEmailTemplate = "<table style='width: 100%;'> <tr> <td colspan='3'> <b>Dear {Operative}</b> </td></tr><tr> <td colspan='3'> <br />Please Note:<br/><tr> <td> <table> <tr> <td style='padding-right: 50px;'>{Address}</td></tr><tr> <td style='padding-right: 50px;'>{TownCity}</td></tr><tr> <td style='padding-right: 50px;'>{PostCode}</td></tr></table> </td></tr><tr> <td colspan='3'>The relet date has been updated. The new relet date is {ToReletDate}. <br/><br/>Please contact your supervisor for further details.<bhg disclaimer text> </td></tr><tr> <td> <br />Thanks <br />Customer Services Team</td></tr></table>"
    ReletChangeEmailForVoidWorksArrangedTemplate = ReletChangeForVoidWorksEmailTemplate
End Function

Function PurchaseNumberFault(strWord)
    PurchaseNumberFault = "PO " & characterPad(strWord,"0", "l", 7)
End Function	

Function PostHTML(operativeId,propertyAddress)
        
          message = Server.urlencode(message) 'encode special characters (e.g. £, & etc) 
        
        url = "https://" & Request.ServerVariables("HTTP_HOST") & "/RSLHRModuleApi/VoidTermination?operativeId=" & operativeId & "&propertyAddress=" &propertyAddress
        
        'piKey = "resw5bAZdIs-fnUwznzIfzJM66WfRdWSQRXlFtR7V2"
        'url = address & apiKey & "&numbers=" & selectednums & "&message=" & message & "&sender=" & from
        set xmlhttp = CreateObject("MSXML2.ServerXMLHTTP") 
        xmlhttp.open "Get", url, false 
        xmlhttp.send "" 
        msg = xmlhttp.responseText 
        response.write(msg) 
        set xmlhttp = nothing 
         
    End Function
'--------------------------------------------------------------


%>
