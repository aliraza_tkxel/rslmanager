<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
TenancyID = Request("TenancyID")
if (TenancyID = "" OR NOT isNumeric(TenancyID)) then
	TenancyID = 0
	TableString = "<TR><TD align=center>No Scratch Pad Data Found.</TD></TR>"
else
	SQL = "SELECT * FROM G_LEGALSCRATCHPAD WHERE TENANCYID = " & TenancyID & " ORDER BY PAGE_NO ASC"
	TableString = ""
	Counter = 0
	OpenDB()
	FieldString = Array("NISP_ISSUED","COMMENCED_PPDATE","ACTION_AFTER_DATE","DATE_OF_HEARING","OUTCOME","TOTAL_ARREARS","COSTS_ASSESSED","TOTAL_JUDGEMENT_DEBT","SUSPENSION_TERMS_RENT_PLUS","FIRST_PAYMENT_DATE","APPLICATION_FOR_WARRANT_DATE","DATE_OF_BAILIFFS_WARRANT_EXECUTION","SUSPENSION_HEARING_DATE","SUSPENSION_OF_WARRANT_TERMS_RENT_PLUS","FURTHER_WARRANT_ISSU_SUSPENSION","DATE_OF_FINAL_REPOSSESSION","Wpad1","Wpad2","Wpad3","Wpad4","Wpad5","Wpad6","Wpad7")
	NameString = Array("NISP_ISSUED","COMMENCED_PPDATE","ACTION_AFTER_DATE","DATE_OF_HEARING","OUTCOME","TOTAL_ARREARS","COSTS_ASSESSED","TOTAL_JUDGEMENT_DEBT","SUSPENSION_TERMS_RENT_PLUS","FIRST_PAYMENT_DATE","APPLICATION_FOR_WARRANT_DATE","DATE_OF_BAILIFFS_WARRANT_EXECUTION","SUSPENSION_HEARING_DATE","SUSPENSION_OF_WARRANT_TERMS_RENT_PLUS","FURTHER_WARRANT_ISSU_SUSPENSION","DATE_OF_FINAL_REPOSSESSION","Wpad1","Wpad2","Wpad3","Wpad4","Wpad5","Wpad6","Wpad7")
		
	Call OpenRs(rsSC, SQL)
	if (NOT rsSC.EOF) then
		while NOT rsSC.EOF 
			TableString = TableString & "<TR><TD>"
			for i=0 to UBound(FieldString)
				'16
				tempItem = rsSC(FieldString(i))
				if (NOT isNULL(tempItem)) then
					if (Replace(tempItem," ","") <> "") then
						IF (i >= 16) then
							TableString = TableString & "<hr style='border:1px dotted #133e71'><br>"
							TableString = TableString & tempItem & "<BR>"
						else
							TableString = TableString & NameString(i) & " : " & tempItem & "<BR>"					
						end if
					end if
				end if
			next
'			TempDesc = rsSC("WPAD1") & " " & rsSC("WPAD2") & " " & rsSC("WPAD3") & " " & rsSC("WPAD4") & " " & rsSC("WPAD5") & " " & rsSC("WPAD6") & " " & rsSC("WPAD7")
'			if (Counter > 0) then
'				TableString = TableString & "<TR><TD><hr style='border:1px dotted #133e71'></TD></TR>"
'			end if
			Counter = Counter + 1
'			if (NOT ISNULL(TempDesc)) then
				TableString = Replace(TableString, chr(13), "<BR>")				
'			end if
			TableString = TableString & "</TD></TR>"
			rsSC.moveNext
		wend
	else
		TableString = "<TR><TD align=center>No Scratch Pad Data Found.</TD></TR>"
	end if
	CloseRs(rsSC)
	CloseDB()
end if
%>
<html>
<head>
<title>Legal ScratchPad Data</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=579 cellspacing="0" cellpadding="0" style='border-collapse:collapse' border=0>
<form name="RSLFORM" method="POST">
  <tr> 
      <td rowspan=2 height=10 width=92 nowrap><a href="pScratchPad.asp?tenancyid=<%=TenancyID%>"><img src="/Customer/Images/1-closed.gif" width="92" height="20" alt="" border=0 /></a></td>
      <td rowspan=2 height=10 width=69 nowrap><a href="pLegalScratchPad.asp?tenancyid=<%=TenancyID%>"><img src="/Customer/Images/tab_legal-over.gif" width="69" height="20" alt="" border=0 /></a></td>
	  <td height=19 width="100%" align=right> Tenancy Reference : <b><%=TenancyReference(TenancyID)%></b>
      </td>
  </tr>
  <tr><td width=100% bgcolor=#133e71 height=1></td></tr>
  <tr>
  	  <td height=370 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<br>
<div style='overflow:auto;height:350;padding:5px;border-bottom:1px solid #133e71;border-top:1px solid #133e71' class='TA'>
<table width=100% border=0>
<%=TableString%>
<TR><TD height=100%></TD></TR>
</table>	  
</div>	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        </td>
	  <td colspan=1 width=100% align="right">
	  	<table cellspacing=0 cellpadding=0 border=0 width='100%'>
			<tr>
            <td width=100% style='border-bottom:1px solid #133E71'>&nbsp; </td>
			<td width=67>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp"  name="ServerIFrame" style='display:none'></iframe>
</BODY>
</HTML>

