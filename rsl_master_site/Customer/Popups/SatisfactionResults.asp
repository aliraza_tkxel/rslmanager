<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
		
	Dim ORDERITEMID,DATECOMPLETED,TIMECOMPLETED
	dIM Q1,Q2,Q3,Q4,q6,Q7,CLOSEWINDOW,Q5
	
	OpenDB()
	
	OrderItemID = request("OrderItemID")
	get_record()
	
	CloseDB()


	Function get_record()
	
		sql = "SELECT " &_
				"	 DATECOMPLETED AS [THEDate], " &_
				"	 CONVERT(varchar(2), " &_
				"		  CASE " &_
				"			   WHEN DATEPART([hour], DATECOMPLETED) > 12 THEN CONVERT(varchar(2), (DATEPART([hour], DATECOMPLETED) - 12)) " &_
				"			   WHEN DATEPART([hour], DATECOMPLETED) = 0 THEN '00' " &_
				"			   ELSE CONVERT(varchar(2), DATEPART([hour], DATECOMPLETED)) " &_
				"		  END " &_
				"	 ) + ':' + " &_
				"	 CONVERT(char(2), SUBSTRING(CONVERT(char(5), DATECOMPLETED, 108), 4, 2)) + ' ' +  " &_
				"	 CONVERT(varchar(2), " &_
				"		  CASE " &_
				"			   WHEN DATEPART([hour], DATECOMPLETED) > 12 THEN 'PM' " &_
				"			   ELSE 'AM' " &_
				"		  END " &_
				"	 ) AS [THETime], " &_
				"	Q1.DESCRIPTION AS Q_1, " &_
				"	Q2.DESCRIPTION AS Q_2, " &_
				"	Q3.DESCRIPTION AS Q_3, " &_
				"	Q4.DESCRIPTION AS Q_4, " &_
				"	C.Q5 AS Q_5, " &_
				"	Q6.DESCRIPTION AS Q_6, " &_
				"	Q7.DESCRIPTION AS Q_7 " &_
				"FROM C_SATISFACTION_ANSWERS C " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q1 ON Q1.SATISFACTION_ANSWER_TYPE_ID = C.Q1 " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q2 ON Q2.SATISFACTION_ANSWER_TYPE_ID = C.Q2 " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q3 ON Q3.SATISFACTION_ANSWER_TYPE_ID = C.Q3 " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q4 ON Q4.SATISFACTION_ANSWER_TYPE_ID = C.Q4 " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q6 ON Q6.SATISFACTION_ANSWER_TYPE_ID = C.Q6 " &_
				"		LEFT JOIN C_SATISFACTION_ANSWER_TYPE Q7 ON Q7.SATISFACTION_ANSWER_TYPE_ID = C.Q7 " &_
				"WHERE C.orderitemid = " & OrderItemID
				
		Call OpenRs (rsSet, sql) 

			DateCompleted 	= 	rsSet("THEDATE")
			TimeCompleted 	= 	rsSet("THETIME")
			Q1				= 	rsSet("Q_1")
			Q2				= 	rsSet("Q_2")
			Q3				= 	rsSet("Q_3")
			Q4				= 	rsSet("Q_4")
			q5				= 	rsSet("Q_5")
			Q6				= 	rsSet("Q_6")
			Q7				= 	rsSet("Q_7")

		Call CloseRs(rsSet)
		
	End Function
	
%>
<html>
<head>
<title>Satisfaction Results Recorder</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	<% If CLOSEWINDOW = "TRUE" Then %>
		opener.GetList()
		window.close()
	<%  End If %>

</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=386 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/tab_surveyresults.gif" width="123" height="20" alt="" border=0 /></td>
      <td width=199 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'><p>&nbsp;</p>
      </td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table width="376">
		<tr valign=top>
		  <td nowrap>&nbsp;</td>
		  <td align="right">&nbsp;</td>
		  <TD>&nbsp;</TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>Date Completed: </td>
		  <td align="right"><%=DateCompleted%></td>
		  <TD><image src="/js/FVS.gif" name="img_DATECOMPLETED" width="15px" height="15px" border="0"></TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>Time Completed: </td>
		  <td align="right"><%=TimeCompleted%></td>
		  <TD><image src="/js/FVS.gif" name="img_TIMECOMPLETED" width="15px" height="15px" border="0"></TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>&nbsp;</td>
		  <td align="right">&nbsp;</td>
		  <TD>&nbsp;</TD>
		  </tr>
		<tr valign=top>
			<td width="66%" nowrap>1. How easy was it to report a repair? </td>
			<td width=26% align="right"><%=Q1%></td>
						<TD width="8%"><image src="/js/FVS.gif" name="img_Q1" width="15px" height="15px" border="0"></TD>							

		</tr>
		<tr>
			<td>2. How helpful where the BHA staff? </td>
			<td align="right"><%=Q2%></td>
			<TD width="8%"><image src="/js/FVS.gif" name="img_Q2" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>3. What did you think of the contractors? </td>
			<td align="right"><%=Q3%></td>
			<TD><image src="/js/FVS.gif" name="img_Q3" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
			<TD nowrap>4. What was the standard of the repair? </td>
			<TD align="right"><%=Q4%></TD>
			<TD><image src="/js/FVS.gif" name="img_Q4" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD nowrap>5. Service improvement suggestions: </td>
			<TD align="right">&nbsp;</TD>
			<TD><image src="/js/FVS.gif" name="img_Q5" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD colspan="3" nowrap><textarea style='OVERFLOW:HIDDEN;WIDTH:350' class="textbox200" name="txt_Q5" rows=5><%=Q5%></textarea></td>
		  </TR>		
		<TR>
			<TD valign=top>6. Review Involvement?		    </TD>
			<TD align="right"><%=Q6%></TD>	
			<TD valign=top><image src="/js/FVS.gif" name="img_Q6" width="15px" height="15px" border="0"> 
			</TD>
		</TR>
		<TR>
		  <TD valign=top>7. Completed on first appointment </TD>
		  <TD align="right"><%=Q7%></TD>
		  <TD valign=top><image src="/js/FVS.gif" name="img_Q7" width="15px" height="15px" border="0"> </TD>
		  </TR>   
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
        <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
      </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerIFrame" style='display:none'></iframe>
</BODY>
</HTML>

