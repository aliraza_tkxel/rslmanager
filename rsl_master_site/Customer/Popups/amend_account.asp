<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim str_SQL, lst_itemtype, lst_paymenttype, lst_office, employee_name, employee_id,employee_team,str_SQLTeam
	Dim cashposting_id, customer_id, tenancy_id, journal_id, theaction, grouping_id
	Dim tdate, itemtype, ptype, startdate, enddate, amount, statusid, chq
	Dim helptext, isrentsql, perioddates, office, sign
	Dim disablePost, isremovable
			
	journal_id 	= Request("journalid")
	tenancy_id 	= Request("tenancyid")
	customer_id	= Request("customerid")
	theaction 	= Request("action") 
	helptext 	= "Amend required fields."
	perioddates = ""
	
	OpenDB()
	Call get_usr_details()
    Call get_usr_team()
	Call get_perioddate_ids()
	Call get_item_details(journal_id)
	Call check_validity(ptype)
	psql = " F_PAYMENTTYPE P INNER JOIN F_PAYMENTTYPE_TO_PAYMENTTYPE F ON P.PAYMENTTYPEID = F.PAYMENTTYPE WHERE	F.GROUPING = " & grouping_id
	Call BuildSelect(lst_itemtype, "sel_ITEMTYPE", "F_ITEMTYPE " & isrentsql, "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", itemtype, NULL, "textbox200", disablePost)
	Call BuildSelect(lst_paymenttype, "sel_PAYMENTTYPE", psql, "PAYMENTTYPEID, P.DESCRIPTION", "P.DESCRIPTION", "Please Select", ptype, NULL, "textbox200", "ONCHANGE='pay_change(this.value)' " & disablePost)
	Call BuildSelect(lst_office, "sel_OFFICE", "G_OFFICE", "OFFICEID, DESCRIPTION", "DESCRIPTION", "Please Select", office, NULL, "textbox200", disablePost)

	CloseDB()
	
	' THIS FUCNTION BUILDS A STRING ARRAY TO BE USED BY THE CLIENT SIDE IN 
	' DETERMING WHETHER OR NOT PERIOD DATES ARE REQUIRED
	Function get_perioddate_ids()
	
		SQL = "SELECT PAYMENTTYPE FROM F_PAYMENTTYPE_TO_PAYMENTTYPE WHERE PERIODDATES = 1 "
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF
			perioddates = perioddates & rsSet(0)
			rsSet.movenext()
			if not rsSet.EOF Then perioddates = perioddates & "," End If
		Wend
		CloseRs(rsSet)
	End Function
	
	' CHECK THAT THIS ROW CAN ACTUALLY BE AMENDED
	Function check_validity(ptype)
	
		If (cint(statusid) = 1) And (employee_team <> "Finance Services" )Then
			helptext = "This item cannot be amended."
			Call set_readonly("DISABLED")
			Exit Function
		End If
		
		' CHECK TO SEE IF ITEM HAS BEEN BANK RECONCILED. IF SO PREVENT ANY CHANGES
		SQL = "SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = " & journal_id
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then
			helptext = "This item has been bank reconciled and therefore cannot be amended."
			CloseRs(rsSet)
			Call set_readonly("DISABLED")
			Exit Function
		Else
			CloseRs(rsSet)
		End If
			
		Select Case cint(ptype)
			Case 5,8,92,93
				Call check_cash()
			Case 14, 19, 23, 24  
				Call check_refund(ptype)
			Case 1,3,7,9,13,15,16,20,21,22,25
				helptext = "Amend required fields."
			Case Else
				If employee_team <> "Finance Services" Then
                helptext = "This item cannot be amended."
				Call set_readonly("DISABLED")
                End If
		End Select
	
	End Function
	
	' FOR CASH OR CHECK ENSURE ITEM HAS NOT BEEN ATTACHED TO PAYMENT SLIP
	Function check_cash()
	
'		` CHECK THAT CHEQUE DOESNT HAVE ASSOCIATED RETURNED CHEQUE ROW
		If cint(ptype) = 8 Then
			SQL = "SELECT * FROM F_DIRECTPOSTLINKING WHERE UPDATED_JOURNALID = " & journal_id
			Call OpenRs(rsSet, SQL)
			if rsSet.EOF Then 
				Call set_readonly("")
			Else 
				helptext = "NB: The amount of this cheque posting may not be changed as it has been returned unpaid."
				Call set_readonly("DISABLED")
				Exit Function
			End If
		End If		
'		CHECK THAT ITEM HASNT ALREADY BEEN ATTACHED TO PAYMENT SLIP
		SQL = "SELECT * FROM F_CASHPOSTING WHERE PAYMENTSLIPNUMBER IS NULL AND JOURNALID = " & journal_id
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then 
			Call set_readonly("")
		Else 
			helptext = "NB: The amount of this cash\cheque posting may not be changed as it has already been attached to a paymentslip or has no associated cash posting ref."
			Call set_readonly("DISABLED")
		End If		
		
		
		closeRs(rsSet)
		
	End Function
	
	' FOR CASH OR CHECK ENSURE ITEM HAS NOT BEEN  ATTACHED TO PAYMENT SLIP
	Function check_refund(PID)
	
		If PID = 14 Or PID = 23 Then
			SQL = "SELECT * FROM F_LABACS WHERE JOURNALID = " & journal_id
		Else
			SQL = "SELECT * FROM F_TENANTBACS WHERE JOURNALID = " & journal_id
		End If
		Call OpenRs(rsSet, SQL)
		if rsSet.EOF Then 
			Call set_readonly("")
		Else 
			helptext = "NB: The details of this refund may not be changed as it has already been paid via the payment tool."
			Call set_readonly("DISABLED")
		End If		
		closeRs(rsSet)
		
	End Function

	' GET ITEM DETAILS AND ALSO GROUPINGID TO GROUP SIMILAR ITEM TYPESS
	Function get_item_details(JID)
	
		SQL = "SELECT 	R.JOURNALID, CONVERT(NVARCHAR, R.TRANSACTIONDATE, 103) AS TRANSACTIONDATE, " &_
				"		R.TENANCYID, R.ITEMTYPE, ISNULL(R.PAYMENTTYPE,-1) AS PTYPE, " &_
				"		ISNULL(CONVERT(NVARCHAR, R.PAYMENTSTARTDATE, 103),'') AS STARTDATE, " &_
				"		ISNULL(CONVERT(NVARCHAR, R.PAYMENTENDDATE, 103),'') AS ENDDATE, " &_
				"		ISNULL(R.AMOUNT,0) AS AMOUNT, " &_
				"		ISNULL(R.STATUSID,-1) AS STATUS, " &_
				"		ISNULL(C.CHQNUMBER, '') AS CHQ, " &_
				"		ISNULL(C.OFFICE, '') AS OFFICE, P.REFUND " &_
				"FROM 	F_RENTJOURNAL R " &_
				"		LEFT JOIN F_CASHPOSTING C ON R.JOURNALID = C.JOURNALID " &_
				"		INNER JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = R.PAYMENTTYPE " &_
				"WHERE  R.JOURNALID = " & JID

		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then
			tdate		= rsSet("TRANSACTIONDATE")
			itemtype	= rsSet("ITEMTYPE")
			ptype		= rsSet("PTYPE")
			startdate	= rsSet("STARTDATE")
			enddate		= rsSet("ENDDATE")
			amount		= abs(rsSet("AMOUNT"))
			statusid	= rsSet("STATUS")
			chq			= rsSet("CHQ")
			office		= rsSet("OFFICE")
			sign		= rsSet("REFUND")		
		else 
			ptype = -1
		end if
		
		' GET GROUPING ID TO ENABLE PAYMENT TYPE DROP ONLY CONTAINS ASSOC ROWS
		SQL = "SELECT GROUPING, ISRENTONLY, PERIODDATES, ISREMOVABLE FROM F_PAYMENTTYPE_TO_PAYMENTTYPE WHERE PAYMENTTYPE = " & ptype
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then 
			grouping_id = rsSet(0) 
			isrentonly 	= rsSet(1)
			Select Case rsSet(3)
				Case 2
					isremovable = " DISABLED "
				Case Else
					isremovable = ""
			End Select
		Else 
			grouping_id = -1 
			isrentonly = -1
			isremovable = ""
		End If
		
		' CHECK FOR RENT ONLY ITEMS
		if cint(isrentonly) = 1 then isrentsql = " WHERE ITEMTYPEID = 1 " Else isrentsql = " WHERE ITEMTYPEID IN (1,5) " End If
		closeRs(rsSet)

	End Function
	
	' COPY ORIGINAL ROW TO AMEND TABLE AND MAKE AMENDMENTS TO ORIGINAL ROW
	Function do_insert()

				
		response.redirect "../iframes/iRentAccount.asp?customerid=" & Request.Form("hid_CUSTOMERID") & "&tenancyid=" & Request.Form("hid_TENANCYID") & "&SyncTabs=1"
		
	End Function
	
	' GET USER NAME AND ID OF CURRENT6 USER
	Function get_usr_details()
	
		str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")
		Call OpenRs(rsSet, str_SQL)	
		If not rsSet.EOF Then 
			employee_id = rsSet("EMPLOYEEID")
			employee_name = rsSet("FULLNAME")
		Else
			employee_name = ""
		End If
		CloseRs(rsSet)

	End Function



    ' GET USER TEAM NAME TO ALLOW FINANCE SERVICES TEAM TO AMEND DATA
	Function get_usr_team()
		str_SQLTeam = "SELECT E__EMPLOYEE.EMPLOYEEID,E__EMPLOYEE.FIRSTNAME, " &_
				"		E__EMPLOYEE.LASTNAME,E_JOBROLETEAM.JobRoleId, " &_
				"		E_JOBROLE.JobeRoleDescription,E_JOBROLETEAM.TeamId,E_TEAM.TEAMNAME " &_
				"		FROM E__EMPLOYEE " &_
				"		INNER JOIN E_JOBROLETEAM " &_
				"		ON E__EMPLOYEE.JobRoleTeamId = E_JOBROLETEAM.JobRoleTeamId " &_
				"		INNER JOIN E_JOBROLE " &_
				"		ON E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId " &_
				"       INNER JOIN E_TEAM " &_
				"		ON E_JOBROLETEAM.TeamId = E_TEAM.TEAMID " &_
				" WHERE E__EMPLOYEE.EMPLOYEEID = " & Session("USERID")
		Call OpenRs(rsSet, str_SQLTeam)	
		If not rsSet.EOF Then 
			employee_team = rsSet("TEAMNAME")
		Else
			employee_team = ""
		End If
		CloseRs(rsSet)
	End Function
	' THIS VARIABLE WILL BE USED TO DISABLE THE POST BUTTON THUS STOPPING THE USER FROM AMENDING THE ITEM
	Function set_readonly(toWhat)
	
		disablePost = toWhat
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Amend Account -- <%=tenancy_id%> -- JID <%=journal_id%></TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">
	
	var FormFields = new Array()
	FormFields[0] = "txt_EMPLOYEENAME|CREATED BY|TEXT|Y"
	FormFields[1] = "txt_TRANSACTIONDATE|TRANSACTION DATE|DATE|Y"
	FormFields[2] = "txt_STARTDATE|START DATE|DATE|N"
	FormFields[3] = "txt_AMOUNT|AMOUNT|CURRENCY|N"
	FormFields[4] = "sel_ITEMTYPE|ITEMTYPE|SELECT|Y"
	FormFields[5] = "sel_PAYMENTTYPE|PAYMENTTYPE|SELECT|Y"
	FormFields[6] = "txt_ENDDATE|END DATE|DATE|N"
 	FormFields[7] = "txt_CHQNUMBER|Local Office|SELECT|N"
 	FormFields[8] = "sel_OFFICE|Local Office|SELECT|N"
	
	idArray = new Array(<%=perioddates%>)

	// CHECK TO SEE IF PERIOD DATES ARE REQUIRED TO BE VALIDATED
	function IsPeriodDates(PID){
		
		for (i=0 ; i<idArray.length ; i++)
			if (PID == idArray[i]){
				return true;
			}
		return false;
	}

	// CHECK TO SEE IF THE CONTENTS OF THE FORM HAVE CHANGED BECAUSE WE ONLY WANT TO SAVE A CHANGED ROW
	function isDirty(oForm){
	
		var iNumElems = oForm.elements.length;
		for (var i=0;i<iNumElems;i++){
			var oElem = oForm.elements[i];
			if ("text" == oElem.type || "TEXTAREA" == oElem.tagName){
				if (oElem.value != oElem.defaultValue) return true;
				}
			else if ("checkbox" == oElem.type || "radio" == oElem.type){
				if (oElem.checked != oElem.defaultChecked) return true;
				}
			else if ("SELECT" == oElem.tagName){
				var oOptions = oElem.options;
				var iNumOpts = oOptions.length;
				for (var j=0;j<iNumOpts;j++){
					var oOpt = oOptions[j];
					if (oOpt.selected != oOpt.defaultSelected) return true;
					}
				}
			}
		return false;
	}

	function Cancel(){
	
		if (!window.confirm("Are you sure you wish to permanently remove this entry ? "))
			return false;
		RSLFORM.target = "CRM_BOTTOM_FRAME"
		RSLFORM.action = "../serverside/amend_account_svr.asp?CUSTOMERID=<%=customer_id%>&TENANCYID=<%=tenancy_id %>&ACTION=CANCELLED&JOURNALID=<%=journal_id%>"
		//document.getElementById("btn_POST").disabled = true
		//document.getElementById("btn_CANCEL").disabled = true
		RSLFORM.submit()
		window.close()
	}
	
	// ALTER REQUIRED FIELDS ACCORDINGLY AND SUBMIT IF VALID 
	function SaveForm(){
	
		//if ( ! isDirty(document.forms[0]) ) {
		//	alert("The form has not changed");
		//	return false;
		//	}
		if (RSLFORM.sel_PAYMENTTYPE.value == "8" && RSLFORM.txt_CHQNUMBER.value == ""){
		 	FormFields[7] = "txt_CHQNUMBER|cheque number|TEXT|Y"
		 	FormFields[8] = "sel_OFFICE|Local Office|SELECT|Y"
			}
		//if (!checkForm()) return false
		//if (RSLFORM.sel_PAYMENTTYPE.value == "1" && ! window.confirm("Amending a HB entry may affect the future HB schedule !\nAre you sure you wish to proceed"))
		//	return false;
		if(!ConvertDateFormat(RSLFORM.txt_STARTDATE.value,RSLFORM.txt_ENDDATE.value))  {
		
		    return false;
		
		}
		RSLFORM.target = "CRM_BOTTOM_FRAME"
		RSLFORM.action = "../serverside/amend_account_svr.asp?CUSTOMERID=<%=customer_id%>&TENANCYID=<%=tenancy_id %>&ACTION=AMENDED&JOURNALID=<%=journal_id%>"
		//document.getElementById("btn_POST").disabled = true
		//document.getElementById("btn_CANCEL").disabled = true
		RSLFORM.submit()
		window.close()
	}
	
	
	function ConvertDateFormat(startDate,endDate) {
	
	
      // startDate=startDate.split(/\//).reverse().join('-');
	  // endDate=endDate.split(/\//).reverse().join('-'); 
	  
	    var start = startDate.split("/");
	    var end = endDate.split("/");
        startDate=new Date(start[2], start[1] - 1, start[0]);
		endDate=new Date(end[2], end[1] - 1, end[0]);
	   if(startDate > endDate){
          alert('Start Date is later than End Date');
		  return false;
        }
	return true;
	}
	
	function pay_change(PID){
	
		// IF PERIOD DATES ARE REQUIRED
		if (IsPeriodDates(PID)){
			document.getElementById("txt_STARTDATE").disabled = false;
			document.getElementById("txt_ENDDATE").disabled = false;
			FormFields[2] = "txt_STARTDATE|START DATE|DATE|Y"
			FormFields[6] = "txt_ENDDATE|END DATE|DATE|Y"
			}
		// OTHERWISE
		else {
			document.getElementById("txt_STARTDATE").disabled = true
			document.getElementById("txt_ENDDATE").disabled = true
			FormFields[2] = "txt_STARTDATE|START DATE|DATE|N"
			FormFields[6] = "txt_ENDDATE|END DATE|DATE|N"
			}
		// IF CASH OR CHEQUE SHOW OFFICE SELECT BOX
		if (PID == "8" || PID == "5" || PID == "92" || PID == "93"){
			OFFROW.style.display = "block";
			// IF PAYMENTTYPE IS CHEQUE SHOW CHEQUE FIELD
			if (PID == "8") 
				CHQROW.style.display = "block" 
			else 
				CHQROW.style.display = "none";
			}
		else
			OFFROW.style.display = "none";
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" ONLOAD='pay_change(RSLFORM.sel_PAYMENTTYPE.value)'>
<form name="RSLFORM" method="POST">
	<TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
		<tr>
			<td>
				<table cellspacing=2 cellpadding=2 style='HEIGHT:35PX;border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0> 
					<TR>
						<TD><b><%=helptext%></b></TD>
					</TR>
				</table>
			</td>
		</tr>
		<tr> 
			<td valign="top"> 
				<table cellspacing=1 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0>  
					<TR>
						
            <TD>Changed By</TD>
						<TD>
							<INPUT TYPE=TEXT ID="txt_EMPLOYEENAME" NAME=txt_EMPLOYEENAME CLASS='TEXTBOX200' VALUE="<%=employee_name%>" READONLY <%=disablePost%>>
							<INPUT TYPE=TEXT ID="txt_CREATEDBY" NAME=txt_CREATEDBY CLASS='TEXTBOX200' STYLE='DISPLAY:NONE' VALUE=<%=employee_id%>>
							<image src="/js/FVS.gif" name="img_EMPLOYEENAME" width="15px" height="15px" border="0">
						</TD>
					</TR>
				<TR ID='OFFROW' STYLE='DISPLAY:NONE'>
					<TD>Office</TD>
					<TD>
						<%=lst_office%>
						<image src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0">
					</TD>
				</TR>
				<TR>
            <TD>Transaction Date</TD>
						<TD>
							<INPUT TYPE=TEXT ID=txt_TRANSACTIONDATE NAME=txt_TRANSACTIONDATE CLASS='TEXTBOX200' VALUE="<%=tdate%>" <%=disablePost%>>
							<image src="/js/FVS.gif" name="img_TRANSACTIONDATE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Item Type</TD>
						<TD>
							<%=lst_itemtype%>
							<image src="/js/FVS.gif" name="img_ITEMTYPE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Payment Type</TD>
						<TD>
							<%=lst_paymenttype%>
							<image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR ID='CHQROW' STYLE='DISPLAY:NONE'>
						<TD>Chq. Number</TD>
						<TD>
							<INPUT TYPE=TEXT ID=txt_CHQNUMBER NAME=txt_CHQNUMBER CLASS='TEXTBOX200' MAXLENGTH=29 VALUE="<%=CHQ%>" <%=disablePost%>>
							<image src="/js/FVS.gif" name="img_CHQNUMBER" width="15px" height="15px" border="0">
						</TD>
					</TR>
							<TR>
            <TD>Payment Start Date</TD>
						<TD>
							<INPUT TYPE=TEXT ID=txt_STARTDATE NAME=txt_STARTDATE CLASS='TEXTBOX200' VALUE="<%=startdate%>" <%=disablePost%>>
							<image src="/js/FVS.gif" ID=img_STARTDATE name="img_STARTDATE" width="15px" height="15px" border="0">
						</TD>
					</TR>		<TR>
            <TD>Payment End Date</TD>
						<TD>
							<INPUT TYPE=TEXT ID=txt_ENDDATE NAME=txt_ENDDATE CLASS='TEXTBOX200' VALUE="<%=enddate%>" <%=disablePost%>>
							<image src="/js/FVS.gif" ID=img_ENDDATE name="img_ENDDATE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Amount</TD>
						<TD>
							<INPUT TYPE=TEXT ID=txt_AMOUNT NAME=txt_AMOUNT CLASS='TEXTBOX200' ONBLUR='checkForm(true)' value="<%=amount%>" <%=disablePost%>>
							<image src="/js/FVS.gif" ID=img_AMOUNT name="img_AMOUNT" width="15px" height="15px" border="0">
						</TD>
					</TR>
				</TABLE>
				<br>
				<table cellspacing=2 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0> 
					<TR>
						<TD COLSPAN=2 ALIGN=CENTER>
						<INPUT TYPE=BUTTON ID=btn_POST NAME=btn_POST CLASS='RSLBUTTON' VALUE=' Post ' title='Post Changes' ONCLICK='SaveForm()' <%=disablePost%>>
						<INPUT TYPE=BUTTONID=btn_CANCEL  NAME=btn_CANCEL CLASS='RSLBUTTON' VALUE='Cancel Entry' onclick='Cancel()' title='Remove this entry from this rent account.' <%=disablePost%> <%=isremovable%>>
						<INPUT TYPE=BUTTON ID=btn_CLOSE NAME=btn_CLOSE CLASS='RSLBUTTON' VALUE='Close' onclick='window.close()' title='Close window'>
						</TD>
					</TR>
				</table>
				<INPUT TYPE=HIDDEN NAME=hid_TENANCYID VALUE=<%=tenancy_id%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGTDATE VALUE=<%=tdate%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGITYPE VALUE=<%=itemtype%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGPTYPE VALUE=<%=ptype%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGSTARTDATE VALUE=<%=startdate%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGENDDATE VALUE=<%=enddate%>>
				<INPUT TYPE=HIDDEN NAME=hid_ORIGAMOUNT VALUE=<%=amount%>>
				<INPUT TYPE=HIDDEN NAME=hid_SIGN VALUE=<%=sign%>>
		</td>
		</tr>
	</table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp"  name=frm_ct_srv width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>