<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="/Includes/Database/adovbs.inc"-->

<%

	'' Created By:	Naveed Iqbal(TkXel)
    	'' Created On: 	July 2, 2008
    	'' Reason:		Integraiton with Tenats Online + New Development (Not present in RSL Manager previously)


	Dim general_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	
	Dim path					' determines whether pahe has been submitted from itself
	

	Dim description,requestNatureID
	Dim  lstCstNature

	Dim notes, title,  lstCategory

	Dim journalID, serviceId	
	
	path = request.form("hid_go")
	general_history_id 	= Request("generalhistoryid")
	
	// begin processing
	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	If path = 0 Then

		get_record()

		CLoseDB()

	Elseif path = 1 Then

		Call new_record(journalID)

		CLoseDB()

		Response.Redirect ("../iFrames/iCstRequestDetails.asp?journalid=" & journalID)

	End If
	
	
	Function get_record()
	
		Dim strSQL


strSQL=	"SELECT		ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
		"			G.REQUESTNATUREID , " &_
		"			ISNULL(G.DESCRIPTION, 'N/A') AS NOTES, JJ.TITLE  " &_
		"FROM C_CST_REQUEST  G  " &_
		"	INNER JOIN	C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
		"	WHERE CSTREQUESTEHISTORYID =  " & general_history_id

					
		Call OpenRs(rsSet, strSQL)
		
		title 			= rsSet("TITLE")
		Description			= rsSet("NOTES")
		requestNatureID		= rsSet("REQUESTNATUREID")

		
		Call BuildSelect(lstCategory, "sel_Category", "C_NATURE WHERE ITEMID=2 AND ITEMNATUREID IN(5, 10, 8, 26, 28, 7, 4, 9, 27, 6, 59)"  , " ITEMNATUREID, DESCRIPTION ", "DESCRIPTION ", "Please Select" , NULL, NULL, "textbox200", "style='width:250px' ")

		CloseRs(rsSet)

	End Function
	

     ' This function use to save reponse entered by admin
	Function new_record(ByRef journID)
					
		genHistoryID 	=	Request.form("generalhistoryid")
		requestNatureID 	=	Request.form("sel_Category")
	  	notes 		= 	Request.form("txt_Notes")

		IF NOT notes <> "" THEN notes = ""
		

		' Checking whether CLOSE ENQUIRY checkbox is ticked or not
		If (Request.Form("chk_CLOSE") = 1) then
			newStatus = 14 '-- close in C_STATUS
			closeFlag = 1		
		Else
			newStatus = 13 '-- open in C_STATUS
			closeFlag = 0
		End if		

		
		userID = Session("userid") 
	
		

		ON ERROR RESUME NEXT

		'' Starting transaction 
		Conn.BeginTrans


		'Call to save response in C_JOURNAL and C_CST_REQUEST tables	
		Call save_response_rsl (journID, genHistoryID, newStatus, userID, notes, requestNatureID)


   	     If Conn.Errors.Count <> 0 Then
		

	      ' Rollback if error ocured while calling save_response_rsl

		Conn.RollbackTrans
	

	    Elseif Request.Form("chk_SHOW_TENANT") = 1 Then
        readFlag = 0
		' Value of journalID  is already obtained by calling save_response_rsl
		' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
		Call get_enquiry_id(journID, enqID)


		'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
		Call save_response_tenant(enqID, closeFlag ,readFlag, serviceId) 


		  If Conn.Errors.Count <> 0 Then


		    ' Rollback if error ocured while calling save_response_tenant
		    Conn.RollbackTrans

		  Else
   		    ' Commit if both save_response_tenant & save_ersponse_rsl are executed successfully
		    Conn.CommitTrans			


		  End if

	    Elseif Request.Form("chk_CLOSE") = 1 Then
            readFlag = 1
            Call get_enquiry_id(journID, enqID)
            Call save_response_tenant(enqID, closeFlag ,readFlag, serviceId) 
            
                 If Conn.Errors.Count <> 0 Then
		            Conn.RollbackTrans
		          Else
		            Conn.CommitTrans			
		          End if

	    Else

		' if show to Tenant checkbox is unchecked then commit transaction
   		' Commit only if save_ersponse_rsl is executed successfully
		Conn.CommitTrans

		
	    End if	
	

		
	End Function



	' This function is used to INSERT record/response IN C_CST_REQUEST TABLE and to UPDATE C_JOURNAL TABLE

	Function save_response_rsl(ByRef journalID, historyID, status, userID, notes, reqNatID)

	  Set comm = Server.CreateObject("ADODB.Command")

	  ' setting stored procedure name
	  comm.commandtext = "C_CST_REQUEST_UPDATE"
	  comm.commandtype = 4
	  comm.namedparameters = true
	  Set comm.activeconnection = Conn

	 
	  ' this parameter will return code/value, i.e. JOURNALID or -1
	  'comm.parameters.append( comm.CreateParameter ("@returnValue" , adInteger, adParamReturnValue) )

	 ' setting input parameter for sproc 
	  comm.parameters.append (comm.CreateParameter ("@HISTORYID", adInteger, adParamInput, 4, historyID ) )
	  comm.parameters.append (comm.CreateParameter ("@CURRENTITEMSTATUSID", adInteger, adParamInput, 4, status ) )
	  comm.parameters.append (comm.CreateParameter ("@LASTACTIONUSER", adInteger, adParamInput, 4, userID ) )
	  comm.parameters.append (comm.CreateParameter ("@NOTES", adVarChar, adParamInput, 4000, notes ) )
	  comm.parameters.append (comm.CreateParameter ("@REQUESTNATUREID", adInteger, adParamInput, 4, reqNatID ) )

		
  	  ' executing sproc
 
	 set rsCST=comm.execute()
	 if rsCST.EOF = False then
	  	journalID 	= rsCST("JOURNALID")
		SERVICEID   = rsCST("SERVICEID")
	  end if
	  
	  'journalID =  comm.Parameters("@returnValue").value	

	End Function



	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jrnlID, ByRef enqID)


		' CALL To STORED PROCEDURE TO get enquiry ID

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jrnlID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

					
	     ' if record found/returned, setting values
		If Not rs.EOF Then 
			enqID = rs("EnquiryLogID")
	      End If

		
	     Set comm = Nothing
	     Set rs = Nothing

	End Function



	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag, readFlag, ServiceId) 

	  Set comm = Server.CreateObject("ADODB.Command")

	  ' setting stored procedure name
	  comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
	  comm.commandtype = 4
	  comm.namedparameters = true
	  Set comm.activeconnection = Conn

  

	 ' setting input parameter for sproc 
      comm.parameters.append (comm.CreateParameter ("@enqLogID", adInteger, adParamInput, 4, enqID ) )
	  comm.parameters.append (comm.CreateParameter ("@closeFlag", adBoolean, adParamInput, 4, closeFlag ) )
	  comm.parameters.append (comm.CreateParameter ("@readFlag", adBoolean, adParamInput, 4, readFlag ) )
	  comm.parameters.append (comm.CreateParameter ("@serviceId", adInteger, adParamInput, 4, SERVICEID ) )
		
  	  ' executing sproc 
	  comm.execute


	End Function

%>
<html>
<head>
<title>Update Enquiry</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "sel_Category|Category|SELECT|Y"
	FormFields[1] = "txt_Notes|Notes|TEXT|Y"
	

	function save_form(){

		if (!checkForm()) return false;


		if ((RSLFORM.chk_CLOSE.checked)  && (RSLFORM.txt_Notes.value == "")) {
			alert("Notes have to be recorded when a item is closed.")
			return false;
		}

		
		RSLFORM.hid_go.value = 1;
		RSLFORM.action = "pCstRequest.asp"
		RSLFORM.target = "CRM_BOTTOM_FRAME"	
		
			if(RSLFORM.chk_CLOSE.checked == true){

				if(!confirm("The item will be automatically closed once you save.")){
					return false;
				}
			}

		RSLFORM.submit();

	      window.close()
	}

function set_values(){
			// setting value for complaint category drop down
			var category = document.getElementById("sel_Category");
	            category.value = "<%=requestNatureID%>"
	}

</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onLoad="window.focus();set_values()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border=0 /></td>
      <td  height=10><img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border=0 /></td>	  
	  <td width=302 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'>&nbsp;</td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=4 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr valign=top>
			<td nowrap>Title:</td>
			<td width=100%><%=title%></td>
		</tr>
		
		<tr style='display:block'>
		  <td>Category:</td>
		  <td><%=lstCategory%></td>
		  <TD><image src="/js/FVS.gif" name="img_Category" width="15px" height="15px" border="0"></TD>
		  </tr>
		<tr >
			<td>Notes : </td>
			<TD><textarea style='OVERFLOW:HIDDEN;WIDTH:250' class="textbox200" name="txt_Notes" rows=5><%=description%></textarea></TD>
			<TD valign=top><image src="/js/FVS.gif" name="img_Notes" width="15px" height="15px" border="0"> 
				<input type="hidden" name="hid_go" value=0>
				<input type="hidden" name="small" value=1>
				<input type="hidden" name="generalhistoryid" value="<%=general_history_id%>">	
			</TD>	
			
		</tr>
		<TR>
			<TD nowrap align="center" colspan=2>Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" value="1"> &nbsp; &nbsp;
				
			Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" value="1" CHECKED> </TD>
			
		</TR>	
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=3 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->  
		<INPUT TYPE=hidden NAME=hid_SIGNATURE value="">		
		<INPUT TYPE=hidden NAME=hid_LETTERTEXT value="">
         <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
         <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
	  </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerIFrame" style='display:none' height=400 width=300></iframe>
</BODY>
</HTML>

