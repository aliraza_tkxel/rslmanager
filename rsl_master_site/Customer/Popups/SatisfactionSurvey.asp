<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../iFrames/iHB.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color, ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED, Contractor

	CurrentDate = CDate("1 jan 2004") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	
	'REsponse.Write ThePreviousDate
	OpenDB()
	
	'rw contractor_name
	itemdetail_id = request("hid_ITEMDETAIL")
	'rw itemdetail_id
	
		SQL =" SELECT 	PIT.ORDERID AS ORDERNUMBER, " &_
	"			I.DESCRIPTION AS DETAILSOFREPAIR, " &_
	"			R.LASTACTIONDATE AS ORDERDATE, " &_
	"			PRI.ESTTIME AS TIMEFRAME, " &_
	"			C.CUSTOMERID," &_
	"			O.NAME AS CONTRACTORNAME," &_
	"			T.TENANCYID " &_
	" FROM R_ITEMDETAIL I " &_
			"	INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = I.PRIORITY  " &_
			"	INNER JOIN C_REPAIR R ON R.ITEMDETAILID = I.ITEMDETAILID " &_
			"	INNER JOIN C_JOURNAL J ON J.JOURNALID = R.JOURNALID " &_
			"	INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
			"	INNER JOIN F_PURCHASEITEM PIT ON PIT.ORDERITEMID = W.ORDERITEMID " &_
			"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
			"	INNER JOIN C_CUSTOMERTENANCY T ON T.CUSTOMERID = C.CUSTOMERID " &_
			"	INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_
	"WHERE I.ITEMDETAILID = " & itemdetail_id &_
	" ORDER BY  R.LASTACTIONDATE DESC "
	'rw SQL
	Call OpenRS(rsOrderDetails, SQL)
	orderno = rsOrderDetails("ORDERNUMBER")
	Contractor =  rsOrderDetails("CONTRACTORNAME")
	repairdetails = rsOrderDetails("DETAILSOFREPAIR")
	orderdate = rsOrderDetails("ORDERDATE")
	timeframe = rsOrderDetails("TIMEFRAME")
	customer_id   = rsOrderDetails("CUSTOMERID")
	tenancy_id = rsOrderDetails("TENANCYID")
	
 	if (tenancy_id = "") then
		Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
		If Not rsSet.EOF Then 
			tenancy_id = rsSet(0)
		Else
			tenancy_id = -1
		End If
		CloseRs(rsSet)
	end if
		
	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			"WHERE T.TENANCYID = " & tenancy_id
	'rw SQL
	Call OpenRS(rsCust, SQL)

	count = 1
		while NOT rsCust.EOF
		
			'This will get the hbowed for each customer
			HBOWED = HBOWED + TOTAL_HB_OWED_THIS_DATE(rsCust("CUSTOMERID"), tenancy_id)	
			
			' Get the title and attach to string for whole of name
			Title = rsCust("TITLE")
			rw Title & "1<BR>"
			StringName = ""
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
				rw StringName & "2<BR>"
			end if
			
			'Get firstname and attach to full name
			FirstName = rsCust("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			
			'Get firstname and attach to full name
			LastName =rsCust("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
				ShortName = ShortName & LastName
				rw StringName & "3<BR>"
				rw ShortName & "4<BR>"
			end if
			
			if (count = 1) then
				StringFullName = StringName
				str_Dear = "Dear " & title & " " & ShortName
				rw str_Dear & "5<BR>"
				count = 2
			else
				StringFullName = StringFullName & " and " & StringName
				str_Dear = str_Dear & " and " & StringName
				rw str_Dear & "6<BR>"
			end if
			
			
			rsCust.moveNext
		wend
		
	if (count = 2) then
		rsCust.moveFirst()
		housenumber = rsCust("housenumber")
		if (housenumber = "" or isNull(housenumber)) then
			housenumber = rsCust("flatnumber")
		end if
		if (housenumber = "" or isNull(housenumber)) then
			FirstLineOfAddress = rsCust("Address1")
		else
			FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
		end if

		RemainingAddressString = ""
		AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE")
		for i=0 to Ubound(AddressArray)
			temp = rsCust(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font:10PT'>" & temp & "</TD></TR>"
			end if
		next
		tenancyref = rsCust("TENANCYID")
	end if
	
	FullAddress = "<TR><TD nowrap style='font:10PT'>" & StringFullName & "</TD></TR>" &_
				"<TR><TD nowrap style='font:10PT'>" & FirstLineOfAddress & "</TD></TR>" &_
				RemainingAddressString 


	SQL = "SELECT  E.FIRSTNAME + ' ' + E.LASTNAME as FullName FROM C_TENANCY T " &_
			" INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			" INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			" INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
			" LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			" WHERE T.TENANCYID = " & tenancy_id
		'	rw SQL
	Call OpenRS(rsHousingOfficer, SQL)
	If not rsHousingOfficer.eof then Fullname = rsHousingOfficer("Fullname") End If
	
	CloseDB()
%>
<HTML>
<HEAD>
<TITLE>Customer Info:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/print.js"></SCRIPT>
<script lanaguage=javascript>
function PrintMe(){
	alert("A Satisfaction Survey will now be sent to the printer.")
	printFrame(this);
	parent.window.close()
	
	}
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="PrintMe()" class='ta'>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td height="110" valign="top"> 
      <table width=100>
        <tr> 
          <td nowrap valign=top width="137" HEIGHT="113">&nbsp; </td>
          <td align=right width=683 valign=top>&nbsp; </td>
          <td align=right valign="top" width="150">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="top" height="171"> 
      <table width="647" border="0" cellspacing="0" cellpadding="0">
                <tr> 
          <td style='font:10PT'><%=FullAddress%> </td>
        </tr><tr>
          <td height="95">&nbsp;</td>
        </tr>

        <tr> 
          <td style='font:10PT'><%=formatdatetime(date,1)%></td>
        </tr>
		<tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style='font:10PT'><%=str_Dear%></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style='font:10PT'>We confirm that the following work will be carried 
            out by the nominated Broadland contractor. Once the work has been 
            completed we would be grateful if you could complete the information 
            below the dotted line. We have enclosed a prepaid envelope for you 
            to return your questionnaire to us.</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
        <tr style='font:10PT'> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr > 
                <td width="24%" style='font:10PT'>&nbsp;</td>
                <td width="76%">&nbsp;</td>
              </tr>
              <tr > 
                <td width="24%" style='font:10PT'>Order No:</td>
                <td width="76%" style='font:10PT'><%=orderno%></td>
              </tr>
              <tr> 
                <td width="24%" style='font:10PT'>Contractor:</td>
                <td width="76%" style='font:10PT'><%=contractor%></td>
              </tr>
              <tr> 
                <td width="24%" style='font:10PT'>Details of Repair:</td>
                <td width="76%" style='font:10PT'><%=repairdetails%></td>
              </tr>
              <tr> 
                <td width="24%" style='font:10PT'>Order Date:</td>
                <td width="76%" style='font:10PT'><%=OrderDate%></td>
              </tr>
              <tr> 
                <td width="24%" style='font:10PT'>Timeframe:</td>
                <td width="76%" style='font:10PT'><%=Timeframe%></td>
              </tr>
              <tr> 
                <td width="24%" style='font:10PT'>&nbsp;</td>
                <td width="76%">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td valign="bottom" align="right" height="100%">
      <div align="left"><img src="../../myImages/scissors.jpg" width="669" height="15"></div>
    </td>
  </tr>
  <tr>
    <td valign="bottom" align="left" height="100%"> 
      <table width="649" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td colspan="2" style='font:10PT'>Please answer these few questions 
            (where relevant) after the work has been completed. <br>
            Please don't return to the contractor.</td>
        </tr>
        <tr> 
          <td width="30%">&nbsp;</td>
          <td width="70%">&nbsp;</td>
        </tr>
        <tr> 
          <td width="30%" height="27" style='font:10PT'>Date work completed:</td>
          <td width="70%" height="27">.................................................................................................................</td>
        </tr>
        <tr> 
          <td width="30%" height="9">&nbsp;</td>
          <td width="70%" height="9">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="2">
            <table width="706" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="61%">&nbsp;</td>
                <td width="39%"> 
                  <table width="127" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td colspan="3"><font size="1">Please Tick</font></td>
                    </tr>
                    <tr align="center"> 
                      <td><b> Good</b></td>
                      <td><b>Average</b></td>
                      <td><b>Poor</b></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="2" height="2"></td>
              </tr>
              <tr> 
                <td width="61%" style='font:10PT'>1. How easy was it to report 
                  this repair?</td>
                <td width="39%"> 
                  <table width="127" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td> <img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="2" height="2"></td>
              </tr>
              <tr> 
                <td width="61%" style='font:10PT'>2. How helpful where the BHA 
                  staff you dealt with on this ocasion?</td>
                <td width="39%"> 
                  <table width="127" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td> <img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="2" height="2"></td>
              </tr>
              <tr> 
                <td width="61%" style='font:10PT'>3. What did you think of the 
                  services provided by our contractors?</td>
                <td width="39%"> 
                  <table width="127" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td> <img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="2" height="2"></td>
              </tr>
              <tr> 
                <td width="61%" style='font:10PT'>4. In your opinion, what is 
                  the standard of the repair? </td>
                <td width="39%"> 
                  <table width="127" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"> 
                      <td> <img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                      <td><img src="../../myImages/box.JPG" width="18" height="15"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td colspan="2" height="5"></td>
              </tr>
              <tr> 
                <td colspan="2" style='font:10PT'> 
                  <p><br>
                    5. Any other comments ..................................................................................................<br>
                    <br>
                    ..................................................................................................................................<br>
                    <br>
                    ..................................................................................................................................<br>
                  </p>
                </td>
              </tr>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr align="center"> 
                <td colspan="2" style='font:10PT'><b><i>Thank you for helping 
                  us help you!</i></b></td>
              </tr>
              <tr> 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr> 
                <td colspan="2" style='font:10PT'>ORDER No. <%=orderno%></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</BODY>

</HTML>

