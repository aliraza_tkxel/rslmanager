<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
OpenDB()

CustomerID = Request("CustomerID")
if (CustomerID = "") then CustomerID = -1 end if

OpenDB()
SQL = "SELECT * FROM C__CUSTOMER WHERE CUSTOMERID = " & CustomerID
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then

	l_title = rsLoader("TITLE")
	l_titleother = rsLoader("TITLEOTHER")
	l_lastname = rsLoader("LASTNAME")
	l_middlename = rsLoader("MIDDLENAME")
	l_firstname = rsLoader("FIRSTNAME")
	l_dob = rsLoader("DOB")
	l_gender = rsLoader("GENDER")
	l_maritalstatus = rsLoader("MARITALSTATUS")
	l_ethnicity = rsLoader("ETHNICORIGIN")						
	l_ninumber = rsLoader("NINUMBER")
	l_employmentstatus = rsLoader("EMPLOYMENTSTATUS")
	l_occupation = rsLoader("OCCUPATION")
	l_customertype = rsLoader("CUSTOMERTYPE")
	l_religion = rsLoader("RELIGION")
	l_sexualorientation = rsLoader("SEXUALORIENTATION")
	l_communication = rsLoader("COMMUNICATION")
	l_disability = rsLoader("DISABILITY")
	
end if

CloseRs(rsLoader)

Call BuildSelect(lstReligion, "sel_RELIGION", "G_RELIGION", "RELIGIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_religion, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstSexualOrientation, "sel_SEXUALORIENTATION", "G_SEXUALORIENTATION", "SEXUALORIENTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_sexualorientation, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstCommunication, "sel_COMMUNICATION", "G_COMMUNICATION", "COMMUNICATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_communication, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstDisability, "sel_DISABILITY", "G_DISABILITY", "DISABILITYID, DESCRIPTION", "DESCRIPTION", "Please Select", l_disability, NULL, "textbox200", "tabIndex='1'")

Call BuildSelect(lstCustomerType, "sel_CUSTOMERTYPE", "C_CUSTOMERTYPE", "CUSTOMERTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_customertype, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_title, NULL, "textbox200", "onchange='title_change()' tabIndex='1'")
Call BuildSelect(lstEthnicities, "sel_ETHNICORIGIN", "G_ETHNICITY", "ETHID, DESCRIPTION", "DESCRIPTION", "Please Select", l_ethnicity, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstMaritalStatus, "sel_MARITALSTATUS", "G_MARITALSTATUS", "MARITALSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_maritalstatus, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstEmploymentStatus, "sel_EMPLOYMENTSTATUS", "C_EMPLOYMENTSTATUS", "EMPLOYMENTSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_employmentstatus, NULL, "textbox200", "tabIndex='1'")
Call BuildSelect(lstMTF, "sel_MOVINGTIMEFRAME", "C_MOVINGTIMEFRAME", "MTFID, DESCRIPTION", "MTFID", "Please Select", l_movingtimeframe, NULL, "textbox200", "tabIndex='1'")
CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Update Customer Details</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">

	var FormFields = new Array()
	FormFields[0] = "txt_FIRSTNAME|First Name|TEXT|Y"
	FormFields[1] = "txt_OCCUPATION|Occupation|TEXT|N"
	FormFields[2] = "txt_LASTNAME|Last Name|TEXT|Y"
	FormFields[3] = "txt_NINUMBER|National Insurance Number|TEXT|N"
	FormFields[4] = "sel_EMPLOYMENTSTATUS|Employment Status|SELECT|N"
	FormFields[5] = "sel_CUSTOMERTYPE|Customer Type|SELECT|Y"
	FormFields[6] = "sel_GENDER|Gender|SELECT|Y"
	FormFields[7] = "sel_MARITALSTATUS|Marital Status|SELECT|N"
	FormFields[8] = "sel_ETHNICORIGIN|Ethnicity|SELECT|N"
	FormFields[9] = "sel_TITLE|Title|SELECT|N"
	FormFields[10] = "txt_DOB|Date of Birth|DATE|N"	
	FormFields[11] = "sel_RELIGION|Religion|SELECT|N"	
	FormFields[12] = "sel_SEXUALORIENTATION|Sexual Orienation|SELECT|N"	
	FormFields[13] = "sel_COMMUNICATION|Communication|SELECT|N"	
	FormFields[14] = "sel_DISABILITY|Disability Desctrimination Act 1995|SELECT|N"	
	FormFields[15] = "txt_MIDDLENAME|Middle Name|TEXT|N"
	FormFields[16] = "txt_TITLEOTHER|Other Title|TEXT|N"
	

	function SaveForm(){
		
		
		if (!checkForm()) return false
		
		opener.STARTLOADER('TOP')
		
		RSLFORM.target = "CRM_TOP_FRAME"
		RSLFORM.action = "../ServerSide/Customer_svr.asp"
		RSLFORM.submit()
		window.close()
		}

	function title_change(){
		if (RSLFORM.sel_TITLE.value == 1)
				RSLFORM.sel_GENDER.selectedIndex = 2;
		else if (RSLFORM.sel_TITLE.value == 5 || RSLFORM.sel_TITLE.value == "")
			RSLFORM.sel_GENDER.selectedIndex = 0;
		else 
			RSLFORM.sel_GENDER.selectedIndex = 1;
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<table width=100% border=0 cellpadding=0 cellspacing=0 HEIGHT=27>
	<tr > 
		
    <td rowspan=2> <img title="Personal Details" name="E_PD" src="../Images/1-open.gif" width=92 height=20 border=0></td>
		
    <td rowspan=2> <img title="Personal Details" name="E_PD" src="../../Customer/Images/TabEnd.gif" width=8 height=20 border=0></td>		
		<td width=100% height=19 align="right">&nbsp;</td>
	</tr>
	<tr> 
		<td bgcolor=#133E71> <img src="/images/spacer.gif" width=53 height=1></td>
	</tr>
	<tr> 
		<td colspan=3 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> <img src="/images/spacer.gif" width=53 height=6></td>
	</tr>
</table>
<table width=100% cellspacing=0 cellpadding=2 style='border:1px solid #133E71;border-top:none'>
  <form name="RSLFORM" method="POST" action="CustomerCheck.asp">
    <tr> 
      <td nowrap width=130>Title : </td>
      <td> <%=lstTitles%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_TITLE"></td>
      <td nowrap colspan="2" rowspan="12">&nbsp;</td>
      <td width="100%"></td>
    </tr>
    <tr>
      <td nowrap>If other, state : </td>
      <td><input type="text" class="textbox200" name="txt_TITLEOTHER" maxlength="50" TABINDEX=1 value='<%=l_titleother%>'></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_TITLEOTHER"></td>
    </tr>
    <tr> 
      <td nowrap width=130>First Name(s): </td>
      <td> 
        <input type="text" class="textbox200" name="txt_FIRSTNAME" maxlength="50" TABINDEX=1 value='<%=l_firstname%>'>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME"></td>
    </tr>
    <tr>
      <td nowrap>Middle  Name(s): </td>
      <td><input type="text" class="textbox200" name="txt_MIDDLENAME" maxlength="50" TABINDEX=1 value='<%=l_middlename%>'></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_MIDDLENAME"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Last Name : </td>
      <td> 
        <input type="text" class="textbox200" name="txt_LASTNAME" maxlength="50" TABINDEX=1 value='<%=l_lastname%>'>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_LASTNAME"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Type : </td>
      <td> <%=lstCustomerType%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_CUSTOMERTYPE"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Gender : </td>
      <td> 
        <select class="textbox200" name="sel_GENDER"  tabindex="1">
          <option value="">Please Select</option>
          <%
		if (l_gender = "Female") then
			FemaleSelected = " selected"
		elseif (l_gender = "Male") then
			MaleSelected = " selected"
		elseif (l_gender = "N/A") then
			NASelected = " selected"
		elseif (l_gender = "Prefer not to say") then
				NotSaySelected = " selected"
		end if
		%>
          <option value="Female" <%=FemaleSelected%>>Female</option>
          <option value="Male" <%=MaleSelected%>>Male</option>
          <option value="N/A" <%=NASelected%>>N/A</option>		
		  <option value="N/A" <%=NotSaySelected%>>Prefer not to say</option>	  
        </select>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_GENDER"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Date of Birth : </td>
      <td> 
        <input type="text" class="textbox200" name="txt_DOB" maxlength="10" TABINDEX=1 value='<%=l_dob%>'>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_DOB"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Marital Status : </td>
      <td> <%=lstMaritalStatus%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_MARITALSTATUS"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Ethnic Origin : </td>
      <td> <%=lstEthnicities%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_ETHNICORIGIN"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Ni Number :</td>
      <td> 
        <input type="text" class="textbox200" name="txt_NINUMBER" maxlength="20" TABINDEX=1 value='<%=l_ninumber%>'>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_NINUMBER"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Economic Status : </td>
      <td> <%=lstEmploymentStatus%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_EMPLOYMENTSTATUS"></td>
    </tr>
    <tr> 
      <td nowrap width=130>Occupation : </td>
      <td> 
        <input type="text" class="textbox200" name="txt_OCCUPATION" maxlength="100" TABINDEX=1 value="<%=l_occupation%>">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_OCCUPATION"></td>
    </tr>
    <tr>
      <td nowrap>Religion : </td>
      <td><%=lstReligion%></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_RELIGION"></td>
    </tr>
    <tr>
      <td nowrap>Sexual Orientation : </td>
      <td><%=lstSexualOrientation%></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_SEXUALORIENTATION"></td>
    </tr>
    <tr>
      <td nowrap>Communication : </td>
      <td><%=lstCommunication%></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_COMMUNICATION"></td>
    </tr>
    <tr>
      <td nowrap>Disablity <br>
        Discrimination<BR>
      Act 1995 : </td>
      <td><%=lstDisability%></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_DISABILITY"></td>
    </tr>
    <tr> 
      <td colspan=2 align=right> 
        <input type=hidden name="hid_CustomerID" value="<%=CustomerID%>">
        <input type=hidden name="hid_Action" value="AMEND">
        <input type="button" class="RSLButton" value=" Update " onClick="SaveForm()">
      </td>
    </tr>
  </form>
</table>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</BODY>
</HTML>