<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim TRANSFEREXCHANGE_HISTORY_ID		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstDevelopments, movingtimeframe

	path = request.form("hid_go")

	TRANSFEREXCHANGE_HISTORY_ID = Request("transferexchangehistoryid")

	' begin processing
	Call OpenDB()

	If path = "" then path = 0 end if ' initial entry to page

	If path = 0 Then
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT     JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, G.ASSIGNTO, JD.TEAM, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, G.TRANSFEREXCHANGEHISTORYID, " &_
					"			S.DESCRIPTION AS STATUS, G.MOVINGTIMEFRAME, G.DEVELOPMENT " &_
					"FROM		C_TRANSFEREXCHANGE G INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON G.ASSIGNTO = E2.EMPLOYEEID " &_
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_
					"WHERE 		TRANSFEREXCHANGEHISTORYID = " & TRANSFEREXCHANGE_HISTORY_ID

		Call OpenRs(rsSet, strSQL)

			i_status 	= rsSet("STATUS")
			title 		= rsSet("TITLE")
			notes		= rsSet("NOTES")
			fullname	= rsSet("FULLNAME")
			assignto	= rsSet("ASSIGNTO")
			team		= rsSet("TEAM")
			item_id		= rsSet("ITEMID")
			development = rsSet("DEVELOPMENT")
			movingtimeframe = rsSet("MOVINGTIMEFRAME")

		Call CloseRs(rsSet)

		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & team & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", " style='width:250px' ")
		Call BuildSelect(lstDevelopments, "sel_DEVELOPMENT", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", development, NULL, "textbox200", " style='width:250px' ")

	End Function

	Function new_record()

		strSQL = "SELECT JOURNALID FROM C_TRANSFEREXCHANGE WHERE TRANSFEREXCHANGEHISTORYID = " & TRANSFEREXCHANGE_HISTORY_ID
		Call OpenRs(rsSet, strSQL)
			journalid = rsSet("JOURNALID")
		Call CloseRS(rsSet)

		notes = Request.form("txt_NOTES")
		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
			Call update_journal(journalid, New_Status)
		Else
			New_Status = 13
		End If

		strSQL = 	"INSERT INTO C_TRANSFEREXCHANGE " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, ASSIGNTO, NOTES, MOVINGTIMEFRAME, DEVELOPMENT) " &_
					"VALUES (" & journalid & ", " & New_Status & ", " & Session("userid") & ", " & Request.Form("sel_USERS") & ", '" & Replace(notes, "'", "''") & "','" & Replace(Request.Form("txt_MOVINGTIMEFRAME"), "'", "''") & "'," & Request.Form("sel_DEVELOPMENT") & ")"
		Conn.Execute(strSQL)

		Response.Redirect ("../iFrames/iTransferExchangeDetail.asp?journalid=" & journalid & "&SyncTabs=1")
	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function
%>
<html>
<head>
    <title>Update Enquiry</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
        FormFields[3] = "sel_DEVELOPMENT|Preferred Scheme|SELECT|Y"

        function save_form() {
            if (!checkForm()) return false
            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = ""
            document.RSLFORM.target = "CRM_BOTTOM_FRAME"
            document.RSLFORM.submit();
            window.close()
        }

        function GetEmployees() {
            if (document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value != "") {
                document.RSLFORM.action = "../Serverside/GetEmployees.asp?SMALL=1"
                document.RSLFORM.target = "ServerIFrame"
                document.RSLFORM.submit()
            }
            else {
                document.getElementById("dvUsers").innerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200' style='width:250px'><option value=''>Please select a team</option></select>"
            }
        }

        function GetEmployee_detail() { }

        var set_length = 3000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

    </script>
</head>
<body onload="window.focus()">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71;">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style="border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71">
                <table>
                    <tr valign="top">
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td width="100%">
                            <%=title%>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Preferred Scheme :
                        </td>
                        <td>
                            <%=lstDevelopments%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DEVELOPMENT" id="img_DEVELOPMENT" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Moving Timeframe :
                        </td>
                        <td>
                            <input type="text" name="txt_MOVINGTIMEFRAME" id="txt_MOVINGTIMEFRAME" class="textbox"
                                style='width: 250px' value="<%=movingtimeframe%>" maxlength="50" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_MOVINGTIMEFRAME" id="img_MOVINGTIMEFRAME" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Team :
                        </td>
                        <td>
                            <%=lstTeams%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Assign To :
                        </td>
                        <td>
                            <div id="dvUsers">
                                <%=lstUsers%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Status:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_STATUS" id="txt_STATUS"
                                value="<%=i_status%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_RECORDEDBY"
                                id="txt_RECORDEDBY" value="<%=fullname%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 250px" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES" rows="5" cols="10" onkeyup="countMe(clickHandler(event));"><%=notes%></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="PreviousAction" id="PreviousAction" value="<%=action%>" />
                            <input type="hidden" name="TRANSFEREXCHANGE_HISTORY_ID" id="TRANSFEREXCHANGE_HISTORY_ID"
                                value="<%=TRANSFEREXCHANGE_HISTORY_ID%>" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Close Enquiry:
                        </td>
                        <td>
                            <input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style='border-bottom: 1px solid #133e71; border-left: 1px solid #133e71'>
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    value=" Close " title="Close" class="RSLButton" style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    title="Save" class="RSLButton" style="cursor: pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" style="display: none">
    </iframe>
</body>
</html>
