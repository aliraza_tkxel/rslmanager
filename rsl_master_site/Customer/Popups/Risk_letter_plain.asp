<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim letter_id, strLetter, tenancy_id, hasLogo
	
	letter_id = Request("letterid")
	tenancy_id = Request("tenancyid")
	OpenDB()
	strLetter = build_letter_string()
	
	// WE NEED DISCOVER IF THIS LETTER IS FROM BROADLAND IN WHICH CASE WE HIDE THE LOGO AND FOOTER
	// IF THIS LETTER HAS THE LOGO AND FOOTER THEN hasLOGO WILL RETURN A VALUE MORE THAN 0
	hasLogo = Instr(strLetter, "BHALOGOLETTER")
	'response.write "HASLOGO : " & hasLogo
	
	' GET ALL ASSOCIATED LETTER TEXT FROM TABLE C_STANDARLETTERS
	Function build_letter_string()
	
		SQL = " SELECT LETTERCONTENT FROM C_RISKLETTER WHERE RISKHISTORYID = " & letter_id
		Call OpenRs(rsLetter, SQL)
		if not rsLetter.Eof then
			build_letter_string = rsLetter(0)
		Else
			build_letter_string = "The system is unable to find assiciated letter. Please contast E-BUSINESS TEAM."
		End If
	
	End Function
	

	CloseDB()
%>
<HTML>
<HEAD>
<TITLE>General Letter:</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	var has_logo = <%=hasLogo%>
	
	function PrintMe(){
		
		if (has_logo > 0){
		document.getElementById("return_address").style.visibility = "hidden";		
		document.getElementById("footer").style.visibility = "hidden";
			}
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("CloseButton").style.visibility = "hidden";
		hide_borders();
		print()
		window.close()
		document.getElementById("CloseButton").style.visibility = "visible";
		document.getElementById("PrintButton").style.visibility = "visible";
		document.getElementById("CloseButton").style.border = "1px solid";
		document.getElementById("PrintButton").style.border = "1px solid";
		// HIDE BROADLAND LOGO AND FOOTER DUE TO HEADED PAPER
		if (has_logo > 0){
			document.getElementById("return_address").style.visibility = "visible";		
			document.getElementById("footer").style.visibility = "visible";
			}		
	}
	
	function hide_borders(){
	
		var coll = document.all.tags("INPUT");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	coll[i].style.border = "none"
	}				
	
 	function load_me(){
		try {
			document.getElementById("AmendContent").style.display = "none";	
			}
		catch(e){
			temp = 1
			}
		document.getElementById("CloseButton").style.visibility = "visible";
		document.getElementById("CloseButton").style.border = "1px solid";
		document.getElementById("PrintButton").style.visibility = "visible";
		document.getElementById("PrintButton").style.border = "1px solid";
		document.getElementById("PrintButton").value = " Print ";		
	}
 
</script>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="load_me()" class='ta'>
<table width=100%>
  <TBODY ID='LETTERDIV'> <%=strLetter%> </TBODY> 
</table>
</BODY>
</HTML>
