<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim str_SQL, lst_itemtype, lst_paymenttype, lst_office, employee_name, employee_id, cashposting_id, tenancy_id, theaction
	Dim customer_id
		
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	theaction = Request("action")
	
	'response.End()
	'response.Write("<br>tenanctyy_id is " & tenancy_id)
	
	OpenDB()
	If theaction = "submit" Then
		Dim DataFields   (9)
		Dim DataTypes    (9)
		Dim ElementTypes (9)
		Dim FormValues   (9)
		Dim FormFields   (9)
		
		FormFields(0) = "txt_CREATEDBY|INTEGER"
		FormFields(1) = "txt_CREATIONDATE|DATE"
		FormFields(2) = "txt_RECEIVEDFROM|TEXT"
		FormFields(3) = "txt_AMOUNT|CURRENCY"
		FormFields(4) = "sel_OFFICE|INT"
		FormFields(5) = "sel_ITEMTYPE|INTEGER"
		FormFields(6) = "sel_PAYMENTTYPE|INTEGER"
	 	FormFields(7) = "hid_TENANCYID|INTEGER"
		FormFields(8) = "txt_CHQNUMBER|TEXT"
		FormFields(9) = "hid_CUSTOMERID|INTEGER"

		do_insert()
	End If

	str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")

	// get user details
	Call OpenRs(rsSet, str_SQL)	
	If not rsSet.EOF Then 
		employee_id = rsSet("EMPLOYEEID")
		employee_name = rsSet("FULLNAME")
	Else
		employee_name = ""
	End If
	CloseRs(rsSet)
	
	Call BuildSelect(lst_itemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5,13)", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)
	Call BuildSelect(lst_paymenttype, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (5,8,92,93) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "ONCHANGE='pay_change()'")
	Call BuildSelect(lst_office, "sel_OFFICE", "G_OFFICE", "OFFICEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
		SQL = "EXEC GET_VALIDATION_PERIOD 13"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	CloseDB()
	
	Function do_insert()

		''Call MakeInsert(strSQL)
		
			
	 	
		// make journal entry
		SQL = "set nocount on;INSERT INTO F_RENTJOURNAL  " &_
				"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
				" VALUES (" &_
							Request.Form("hid_TENANCYID") & ", '" &_
						    FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
							Request.Form("sel_ITEMTYPE") & ", " &_
							Request.Form("sel_PAYMENTTYPE") & ", -" &_
							Request.Form("txt_AMOUNT") & ", " &_
							2 &_
						  ");SELECT SCOPE_IDENTITY() AS JOURNALID;"
						  
		set rsRentJourn = Conn.Execute(SQL)
		RentJournal_id = rsRentJourn("JOURNALID")
		
		chqnumber = Request.Form("txt_CHQNUMBER")
		if (chqnumber = "") then
			chqnumber = "NULL"
		else
			chqnumber = "'" & Replace(chqnumber, "'", "''") & "'"
		end if
		
		SQL = "set nocount on;INSERT INTO F_CASHPOSTING  " &_
				"(CREATEDBY, CREATIONDATE, RECEIVEDFROM, AMOUNT, OFFICE, ITEMTYPE,PAYMENTTYPE,TENANCYID , CHQNUMBER, JOURNALID ,CUSTOMERID) " &_
				" VALUES (" &_	
					Request.Form("txt_CREATEDBY")& ", '" &_
					Request.Form("txt_CREATIONDATE")& "', '" &_
					Replace(Request.Form("txt_RECEIVEDFROM"), "'", "''") & "', " &_
					Request.Form("txt_AMOUNT")& ", " &_
					Request.Form("sel_OFFICE") & ", " &_
					Request.Form("sel_ITEMTYPE") & ", " &_
					Request.Form("sel_PAYMENTTYPE") & ", " &_
					Request.Form("hid_TENANCYID") & ", " &_
					chqnumber & ", " &_
					RentJournal_id & ", " &_
					Request.Form("hid_CUSTOMERID") &_
				");SELECT SCOPE_IDENTITY() AS CASHPOSTINGID;"

		Response.Write SQL

		set rsSet = Conn.Execute(SQL)
		cashposting_id = rsSet("CASHPOSTINGID")
				
		response.redirect "../iframes/iRentAccount.asp?customerid=" & Request.Form("hid_CUSTOMERID") & "&tenancyid=" & Request.Form("hid_TENANCYID") & "&SyncTabs=1"
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Cash Posting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">
	
	var FormFields = new Array()
	FormFields[0] = "txt_EMPLOYEENAME|CREATEDBY|TEXT|Y"
	FormFields[1] = "txt_CREATIONDATE|CREATIONDATE|DATE|Y"
	FormFields[2] = "txt_RECEIVEDFROM|RECEIVEDFROM|TEXT|Y"
	FormFields[3] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
	FormFields[4] = "sel_ITEMTYPE|ITEMTYPE|SELECT|Y"
	FormFields[5] = "sel_PAYMENTTYPE|PAYMENTTYPE|SELECT|Y"
 	FormFields[6] = "sel_OFFICE|Local Office|SELECT|Y"
 	FormFields[7] = "txt_CHQNUMBER|Local Office|SELECT|N"	

	function SaveForm(){
		
		FormFields[7] = "txt_CHQNUMBER|cheque number|TEXT|N"
		if (RSLFORM.sel_PAYMENTTYPE.value == "8" && RSLFORM.txt_CHQNUMBER.value == ""){
			//ManualError("img_CHQNUMBER", "Please enter a number for the cheque.", 0);
		 	FormFields[7] = "txt_CHQNUMBER|cheque number|TEXT|Y"				
			}
		if (!checkForm()) return false
 
	    var YStart = new Date("<%=YearStartDate%>")
	    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    var YEnd = new Date("<%=YearendDate%>")
	    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
	    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 2, YearStartDate),1) %>") 
	

	   if (!checkForm()) return false;
		
		if (RSLFORM.hid_TENANCYID.value==-1)
		{
			   alert("Please select the tenancy.")
               return false;	
		}
		
	    if  ( real_date(RSLFORM.txt_CREATIONDATE.value) < YStart || real_date(RSLFORM.txt_CREATIONDATE.value) > YEnd ) {
	        // we're outside the grace period and also outside this financial year
                alert("Please enter a date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
                return false;                
	    }	
		/**/

 		RSLFORM.target = "CRM_BOTTOM_FRAME"
		RSLFORM.action = "cashposting.asp?action=submit"
		document.getElementById("btn_POST").disabled = true
		RSLFORM.submit()
		window.close()
		}
	
	function pay_change(){
	
		if (RSLFORM.sel_PAYMENTTYPE.value == "8")
			CHQROW.style.display = "block";
		else
			CHQROW.style.display = "none";
	
	}
	
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name="RSLFORM" method="POST">
	<TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
		<tr> 
			<td valign="top" height="20">
				<TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
					<TR>						
						<TD ROWSPAN=2 width="79"><img src="../Images/tab_cheque_cash.gif" width="173" height="20"></TD>
						
            <TD HEIGHT=19>&nbsp;</TD>
					</TR>
					<TR>
						<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<tr> 
			<td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
				<br>
				<table cellspacing=1 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0>  
					<TR>
						<TD>Taken By</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_EMPLOYEENAME CLASS='TEXTBOX200' VALUE="<%=employee_name%>">
							<INPUT TYPE=TEXT NAME=txt_CREATEDBY CLASS='TEXTBOX200' STYLE='DISPLAY:NONE' VALUE=<%=employee_id%>>
							<image src="/js/FVS.gif" name="img_EMPLOYEENAME" width="15px" height="15px" border="0">
						</TD>
					</TR>
						<TR>
						<TD>Office</TD>
						<TD>
							<%=lst_office%>
							<image src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0">
						</TD>
					</TR>
				
					<TR>
						
            <TD>Date</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_CREATIONDATE CLASS='TEXTBOX200' VALUE="<%=DATE%>">
							<image src="/js/FVS.gif" name="img_CREATIONDATE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Received From</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_RECEIVEDFROM CLASS='TEXTBOX200' MAXLENGTH=99>
							<image src="/js/FVS.gif" name="img_RECEIVEDFROM" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Item Type</TD>
						<TD>
							<%=lst_itemtype%>
							<image src="/js/FVS.gif" name="img_ITEMTYPE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Payment Type</TD>
						<TD>
							<%=lst_paymenttype%>
							<image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR ID='CHQROW' STYLE='DISPLAY:NONE'>
						<TD>Chq. Number</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_CHQNUMBER CLASS='TEXTBOX200' MAXLENGTH=29>
							<image src="/js/FVS.gif" name="img_CHQNUMBER" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						
            <TD>Amount</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_AMOUNT CLASS='TEXTBOX200' ONBLUR='checkForm(true)'>
							<image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0">
						</TD>
					</TR>
				</TABLE>
				<br>
				<table cellspacing=2 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0> 
					<TR>
						<TD COLSPAN=2 ALIGN=CENTER>
						<INPUT TYPE=BUTTON NAME=btn_POST CLASS='RSLBUTTON' VALUE=' Post ' ONCLICK='SaveForm()'>
						<INPUT TYPE=BUTTON NAME=btn_CANCEL CLASS='RSLBUTTON' VALUE='Close' onclick='window.close()'>
						</TD>
					</TR>
				</table>
				<INPUT TYPE=HIDDEN NAME=hid_TENANCYID VALUE=<%=tenancy_id%>>
				<INPUT TYPE=HIDDEN NAME=hid_CUSTOMERID VALUE=<%=customer_id%>>
			</td>
		</tr>
	</table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ct_srv width=400px height=400px style='display:knone'></iframe> 
</BODY>
</HTML>