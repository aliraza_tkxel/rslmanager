<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim str_SQL, lst_itemtype, lst_paymenttype, lst_office, employee_name, employee_id, cashposting_id, tenancy_id, theaction
	Dim customer_id

	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	theaction = Request("action")

	Call OpenDB()

	If theaction = "submit" Then
		Dim DataFields   (9)
		Dim DataTypes    (9)
		Dim ElementTypes (9)
		Dim FormValues   (9)
		Dim FormFields   (9)

		FormFields(0) = "txt_CREATEDBY|INTEGER"
		FormFields(1) = "txt_CREATIONDATE|DATE"
		FormFields(2) = "txt_RECEIVEDFROM|TEXT"
		FormFields(3) = "txt_AMOUNT|CURRENCY"
		FormFields(4) = "sel_OFFICE|INT"
		FormFields(5) = "sel_ITEMTYPE|INTEGER"
		FormFields(6) = "sel_PAYMENTTYPE|INTEGER"
	 	FormFields(7) = "hid_TENANCYID|INTEGER"
		FormFields(8) = "txt_CHQNUMBER|TEXT"
		FormFields(9) = "hid_CUSTOMERID|INTEGER"

		Call do_insert()

	End If

	str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")

	' get user details
	Call OpenRs(rsSet, str_SQL)
	If not rsSet.EOF Then 
		employee_id = rsSet("EMPLOYEEID")
		employee_name = rsSet("FULLNAME")
	Else
		employee_name = ""
	End If
	Call CloseRs(rsSet)

	Call BuildSelect(lst_itemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5,13)", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)
	Call BuildSelect(lst_paymenttype, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (5,8,92,93) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange=""pay_change()"" ")
	Call BuildSelect(lst_office, "sel_OFFICE", "G_OFFICE WHERE DESCRIPTION <> 'Paythru' ", "OFFICEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	SQL = "EXEC GET_VALIDATION_PERIOD 13"
	Call OpenRs(rsTAXDATE, SQL)
	    YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	    YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	Call CloseDB()

	Function do_insert()

		''Call MakeInsert(strSQL)

		' make journal entry
		SQL = "set nocount on;INSERT INTO F_RENTJOURNAL  " &_
				"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
				" VALUES (" &_
							Request.Form("hid_TENANCYID") & ", '" &_
						    FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
							Request.Form("sel_ITEMTYPE") & ", " &_
							Request.Form("sel_PAYMENTTYPE") & ", -" &_
							Request.Form("txt_AMOUNT") & ", " &_
							2 &_
						  ");SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsRentJourn = Conn.Execute(SQL)
		RentJournal_id = rsRentJourn("JOURNALID")

		chqnumber = Request.Form("txt_CHQNUMBER")
		If (chqnumber = "") Then
			chqnumber = "NULL"
		Else
			chqnumber = "'" & Replace(chqnumber, "'", "''") & "'"
		End If

		SQL = "set nocount on;INSERT INTO F_CASHPOSTING  " &_
				"(CREATEDBY, CREATIONDATE, RECEIVEDFROM, AMOUNT, OFFICE, ITEMTYPE,PAYMENTTYPE,TENANCYID , CHQNUMBER, JOURNALID ,CUSTOMERID) " &_
				" VALUES (" &_	
					Request.Form("txt_CREATEDBY")& ", '" &_
					Request.Form("txt_CREATIONDATE")& "', '" &_
					Replace(Request.Form("txt_RECEIVEDFROM"), "'", "''") & "', " &_
					Request.Form("txt_AMOUNT")& ", " &_
					Request.Form("sel_OFFICE") & ", " &_
					Request.Form("sel_ITEMTYPE") & ", " &_
					Request.Form("sel_PAYMENTTYPE") & ", " &_
					Request.Form("hid_TENANCYID") & ", " &_
					chqnumber & ", " &_
					RentJournal_id & ", " &_
					Request.Form("hid_CUSTOMERID") &_
				");SELECT SCOPE_IDENTITY() AS CASHPOSTINGID;"

		set rsSet = Conn.Execute(SQL)
		cashposting_id = rsSet("CASHPOSTINGID")

		Response.Redirect "../iframes/iRentAccount.asp?customerid=" & Request.Form("hid_CUSTOMERID") & "&tenancyid=" & Request.Form("hid_TENANCYID") & "&SyncTabs=1"

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Cash Posting</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array()
	FormFields[0] = "txt_EMPLOYEENAME|CREATEDBY|TEXT|Y"
	FormFields[1] = "txt_CREATIONDATE|CREATIONDATE|DATE|Y"
	FormFields[2] = "txt_RECEIVEDFROM|RECEIVEDFROM|TEXT|Y"
	FormFields[3] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
	FormFields[4] = "sel_ITEMTYPE|ITEMTYPE|SELECT|Y"
	FormFields[5] = "sel_PAYMENTTYPE|PAYMENTTYPE|SELECT|Y"
 	FormFields[6] = "sel_OFFICE|Local Office|SELECT|Y"
 	FormFields[7] = "txt_CHQNUMBER|Local Office|SELECT|N"

	function SaveForm() {

		FormFields[7] = "txt_CHQNUMBER|cheque number|TEXT|N"
        var pType = document.getElementById("sel_PAYMENTTYPE").options[document.getElementById("sel_PAYMENTTYPE").selectedIndex].value
		if (pType == "8" && document.getElementById("txt_CHQNUMBER").value == "") {
		 	FormFields[7] = "txt_CHQNUMBER|cheque number|TEXT|Y"
		}
		if (!checkForm()) return false

	    var YStart = new Date("<%=YearStartDate%>")
	    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    var YEnd = new Date("<%=YearendDate%>")
	    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
	    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 2, YearStartDate),1) %>") 

	    if (!checkForm()) return false;
		
        if (document.getElementById("hid_TENANCYID").value == -1)
		{
			   alert("Please select the tenancy.")
               return false;
		}

	    //if  (real_date(document.getElementById("txt_CREATIONDATE").value) < YStart || real_date(document.getElementById("txt_CREATIONDATE").value) > YEnd ) {
	    //    // we're outside the grace period and also outside this financial year
        //    alert("Please enter a date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
        //    return false;
	    //}

 		document.RSLFORM.target = "CRM_BOTTOM_FRAME"
		document.RSLFORM.action = "cashposting.asp?action=submit"
		document.getElementById("btn_POST").disabled = true
		document.RSLFORM.submit()
		window.close()
		}

	function pay_change() {
		if (document.RSLFORM.sel_PAYMENTTYPE.value == "8") {
			document.getElementById("CHQROW").style.display = "block";
            document.getElementById("CHQROW").style.display = 'table-row';
        }
		else {
			document.getElementById("CHQROW").style.display = "none";
        }
	}

	function real_date(str_date) {
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
	}

    </script>
</head>
<body>
    <form name="RSLFORM" method="post" action="">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="79">
                            <img src="../Images/tab_cheque_cash.gif" width="173" height="20" alt="" /></td>
                        <td height="19">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="../images/spacer.gif" height="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71">
                <br />
                <table cellspacing="1" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center" border="0">
                    <tr>
                        <td>
                            Taken By
                        </td>
                        <td>
                            <input type="text" name="txt_EMPLOYEENAME" id="txt_EMPLOYEENAME" class="TEXTBOX200"
                                value="<%=employee_name%>" readonly />
                            <input type="text" name="txt_CREATEDBY" id="txt_CREATEDBY" class="TEXTBOX200" style="display: none"
                                value="<%=employee_id%>" />
                            <img src="/js/FVS.gif" name="img_EMPLOYEENAME" id="img_EMPLOYEENAME" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Office
                        </td>
                        <td>
                            <%=lst_office%>
                            <img src="/js/FVS.gif" name="img_OFFICE" id="img_OFFICE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date
                        </td>
                        <td>
                            <input type="text" name="txt_CREATIONDATE" id="txt_CREATIONDATE" class="TEXTBOX200"
                                value="<%=DATE%>" />
                            <img src="/js/FVS.gif" name="img_CREATIONDATE" id="img_CREATIONDATE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Received From
                        </td>
                        <td>
                            <input type="text" name="txt_RECEIVEDFROM" id="txt_RECEIVEDFROM" class="TEXTBOX200"
                                maxlength="99" />
                            <img src="/js/FVS.gif" name="img_RECEIVEDFROM" id="img_RECEIVEDFROM" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Item Type
                        </td>
                        <td>
                            <%=lst_itemtype%>
                            <img src="/js/FVS.gif" name="img_ITEMTYPE" id="img_ITEMTYPE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Payment Type
                        </td>
                        <td>
                            <%=lst_paymenttype%>
                            <img src="/js/FVS.gif" name="img_PAYMENTTYPE" id="img_PAYMENTTYPE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr id="CHQROW" style="display: none">
                        <td>
                            Chq. Number
                        </td>
                        <td>
                            <input type="text" name="txt_CHQNUMBER" id="txt_CHQNUMBER" class="TEXTBOX200" maxlength="29" />
                            <img src="/js/FVS.gif" name="img_CHQNUMBER" id="img_CHQNUMBER" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Amount
                        </td>
                        <td>
                            <input type="text" name="txt_AMOUNT" id="txt_AMOUNT" class="TEXTBOX200" onblur="checkForm(true)" />
                            <img src="/js/FVS.gif" name="img_AMOUNT" id="img_AMOUNT" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="2" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center" border="0">
                    <tr>
                        <td colspan="2" align="center">
                            <input type="button" name="btn_POST" id="btn_POST" class="RSLBUTTON" value=" Post "
                                title="Post" onclick="SaveForm()" />
                            <input type="button" name="btn_CANCEL" id="btn_CANCEL" class="RSLBUTTON" value="Close"
                                title="Close" onclick="window.close()" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
    </table>
    <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
    <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=customer_id%>" />
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe name="frm_ct_srv" id="frm_ct_srv" width="1px" height="1px" style="display: none">
    </iframe>
</body>
</html>
