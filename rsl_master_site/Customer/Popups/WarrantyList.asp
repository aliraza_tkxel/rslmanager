<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	OpenDB()
	
	PropID= Request("PropID")
	wiElement = Request("wiElement")
	if (wiElement = "") then wiElement = -1
	SQL = "SELECT AI.DESCRIPTION AS AREAITEMNAME, EXPIRYDATE = CONVERT(VARCHAR, EXPIRYDATE, 103), W.* FROM P_WARRANTY W LEFT JOIN R_AREAITEM AI ON W.AREAITEM = AI.AREAITEMID WHERE W.EXPIRYDATE >= GETDATE() AND W.AREAITEM = " & wiElement & " AND W.PROPERTYID = '" & PropID & "'"

	Call OpenRs(rsWar, SQL)
	TableString = "<TABLE width='100%' CELLSPACING=0 CELLPADDING=2 STYLE='BORDER-COLLAPSE:COLLAPSE;BEHAVIOUR:URL();border:1px solid #133e71'>"
	TableString = TableString & "<TR><TD style='border-bottom:1px dotted #133e71'><b><i>Warranty List</i></b></TR>"
	if (NOT rsWar.EOF) then
		Counter = 1
		while NOT rsWar.EOF 
			ItemString = "<b>Warranty Item :</b> " & rsWar("AREAITEMNAME") & "<br><b>Expiry Date :</b> " & rsWar("EXPIRYDATE") & "<br><b>Serial No :</b> " & rsWar("SERIALNUMBER") & "<br><b>Model :</b> " & rsWar("MODEL") & "<br><b>Notes :</b> " & rsWar("NOTES") & ""
			ItemStringStripped = "Warranty Item : " & rsWar("AREAITEMNAME") & VbCrLf & "Expiry Date : " & rsWar("EXPIRYDATE") & VbCrLf & "Serial No : " & rsWar("SERIALNUMBER") & VbCrLf & "Model : " & rsWar("MODEL") & VbCrLf & "Notes : " & rsWar("NOTES") & ""
			TableString = TableString & "<TR><TD>" & ItemString & "<br><div align=right><input type=""button"" name=""btnDI"" class=""RSLButton"" value=""Create DI"" onclick=""CreateDefect(" & Counter & ")""></div><textarea style='display:none' name=""Data" & counter & """>" & ItemStringStripped & "</textarea><input type=""hidden"" value=""" & rsWar("CONTRACTOR") & """ name=""Contractor" & Counter & """></TD></TR>"
			rsWar.moveNext
			Counter = Counter + 1
		wend
	else
		TableString = TableString & "<TR><TD COLSPAN=2 ALIGN=CENTER>NO WARRANTIES FOUND</TD></TR>"
	end if
	CloseRs(rsWar)
	TableString = TableString & "</TABLE>"
	CloseDB()
%>
<html>
<head>
<title>Warranty List</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript>
function CreateDefect(iEl){
    document.getElementById("frm_nature3").style.display="block";
    RSLFORM.btnDI.disabled = true
	RSLFORM.WarrantyItem.value = iEl
	RSLFORM.target = "frm_nature3"
	RSLFORM.action = "../iFrames/iDefects.asp"
	RSLFORM.submit()
	//window.close()
	}

</script>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000">
<table HEIGHT=100% WIDTH=100% cellspacing=5><tr><td>
<TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
	<tr> 
		<td valign="top" height="20">
			<TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
				<TR>					
					<TD ROWSPAN=2 width="79"><img src="../Images/tab_tenancy.gif" width="79" height="20"></TD>
					<TD HEIGHT=19></TD>
				</TR>
				<TR>
					<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	<tr> 
		<td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'>
		<table width=100%>
		<FORM name="RSLFORM" method="POST">
		<tr><td>
		The list below will show all warranties for the selected item. If one matches the repair requested then click on the 'Create DI' to create a Defect Item. If not click on 'Close Window'.<br><br>
		<div style='overflow:auto;width:410;height:320'>
		<%=TableString%>
		<IFRAME  src="/secureframe.asp" NAME="frm_nature3" id="frm_nature3" WIDTH=400 HEIGHT=210 STYLE='DISPLAY:NONE'></IFRAME>
		</div>
		<div align=right>
		    <input type="button" class="RSLButton" value=" Close Window " onclick="window.close()">
		</div>
		<input type="hidden" name="WarrantyItem" value="">
		<input type="hidden" name="customerid" value="<%=Request("CustomerID")%>">
		<input type="hidden" name="tenancyid" value="<%=Request("TenancyID")%>">				
		

		</td></tr>
		
		</FORM>
		</table>
		</td>
	</tr>

</table>
</td></tr></table>
</body>
</html>
