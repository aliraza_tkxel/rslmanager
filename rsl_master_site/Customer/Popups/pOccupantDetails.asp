<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim title, firstname,lastname,gender,DOB,occupation,tenancyid,relationship,economicstatus,userid,Disability_String, disability, consent

	Call OpenDB()

	CustomerID = Request("CustomerID")
    TenancyId = Request("tenancyid")
	action = Request("action")

    btnvalue= " Add Occupant "

    If action = "amend" Then

            str_SQL = 	" SELECT C.TITLE,C.FIRSTNAME,C.LASTNAME,C.DOB,OC.OCCUPANTID,OC.CUSTOMERID,OC.TENANCYID, OC.RELATIONSHIP,OC.ECONOMICSTATUS,OC.DISABILITY,OC.CONSENT " &_
				    " FROM		C_OCCUPANT OC " &_
				    "			INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = OC.CUSTOMERID " &_
				    "			LEFT JOIN G_TITLE T ON C.TITLE = T.TITLEID " &_
				    " WHERE 	1=1 AND OC.CUSTOMERID = " & CustomerID 
            Call OpenRs(rsOccupant, str_SQL)

            If Not rsOccupant.eof Then
                title = rsOccupant("title")
                firstname = rsOccupant("firstname")
                lastname  = rsOccupant("lastname")
                dob = rsOccupant("dob")
                disability = rsOccupant("disability")
                relationship = rsOccupant("relationship")
                economicstatus = rsOccupant("economicstatus")
                consent = rsOccupant("consent")
            End If

            Call CloseRs(rsOccupant)

            btnvalue= " Amend Occupant "

    End If

    Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "Please Select", title, NULL, "textbox200", "tabIndex='1'")
	Call BuildSelect(lstRelationship, "sel_RELATIONSHIP", "C_RELATIONSHIP ", "RELATIONSHIPID, DESCRIPTION", "DESCRIPTION ", "Please Select", relationship, NULL, "textbox200", "tabIndex='1'")
	Call BuildSelect(lstEconomicStatus, "sel_ECONOMICSTATUS", "C_EMPLOYMENTSTATUS ", "EMPLOYMENTSTATUSID, DESCRIPTION ", "DESCRIPTION", "Please Select", economicstatus, NULL, "textbox200", "tabIndex='1'")

    SQL = "SELECT DISABILITYID, DESCRIPTION FROM G_DISABILITY"
	Call OpenRS(rsDisability, SQL)
	Disability_String = ""

	If Not rsDisability.EOF Then
		Disability_String = Disability_String & "<table>"
		DisLinkCount = 0
		' Do until the end of all records
		Do Until rsDisability.EOF
			'change 5 to any number of columns you like
			If DisLinkCount Mod 3 = 0 Then
				placeOnClick = ""
				If DisLinkCount <> 0 Then Disability_String = Disability_String & "</tr>"
					Disability_String = Disability_String &"<tr><td width=""24""><input type=""checkbox"" name='chk_Disability" & (DisLinkCount + 1) & "' id='chk_Disability" & (DisLinkCount + 1) & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
				Else
					Disability_String = Disability_String &"<td width=""24""><input type=""checkbox"" name='chk_Disability" & (DisLinkCount + 1) & "' id='chk_Disability" & (DisLinkCount + 1) & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
				End If
			DisLinkCount = DisLinkCount + 1
			'loop till end of records
			rsDisability.MoveNext
		Loop
		Disability_String = Disability_String & "</tr></table>"
	Else 
        'Write no records in there are no records
        Response.Write"Sorry, no records were found!"
	End If
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Add Occupant Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()
            FormFields[0] = "txt_FIRSTNAME|First Name|TEXT|Y"
            FormFields[1] = "txt_LASTNAME|Last Name|TEXT|Y"
            FormFields[2] = "sel_ECONOMICSTATUS|Employment Status|SELECT|N"
            FormFields[3] = "sel_TITLE|Title|SELECT|N"
            FormFields[4] = "txt_DOB|DOB|DATE|N"
            FormFields[5] = "sel_RELATIONSHIP|Relationship|TEXT|Y"
            FormFields[6] = "chk_CONSENT|Consent|NUMBER|N"

        function SaveForm(){
            if (!checkForm()) return false
            buildDisabilityArray()
            document.RSLFORM.target = "occFrame"
	        document.RSLFORM.action = "../serverside/OccupantDetails_srv.asp"
	        document.RSLFORM.submit()
	        }

        function callDiv(whatTab, where){
            window.opener.SwapDiv(whatTab, where);
        }

        function buildDisabilityArray(){
	        var ArraySize = <%=DisLinkCount%>
	        var DisList = ""
	        for (i = 1; i <= ArraySize; i++){
                WhichBox = document.getElementById("chk_Disability"+String(i));
		        if (WhichBox.checked){
			        DisList = DisList + WhichBox.value + ","
			        }
		        }
	        document.getElementById("hid_DISABILITY").value = DisList
        }

        function ActivateCheckBoxes(){
	        var DisabilityArray = document.getElementById("hid_DISABILITY").value
	            DisabilityArray = DisabilityArray.split(",")
	        for (i = 0; i <= DisabilityArray[i]; i++){
		        if (!(DisabilityArray[i] == "")){
                    WhichBox = "chk_Disability" + DisabilityArray[i] + ""
			        document.getElementById("" + WhichBox + "").checked = true;
		        }
	        }
        }

    </script>
</head>
<body onload="ActivateCheckBoxes();">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <form name="RSLFORM" method="post" action="">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2">
                            <img title="Information" src="../Images/1-open.gif" width="92" height="20" alt="" /></td>
                        <td rowspan="2">
                            <img src="../../Customer/Images/TabEnd.gif" width="8" height="20" alt="" /></td>
                        <td width="100%" height="19" align="right">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="/images/spacer.gif" width="53" height="1" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="/images/spacer.gif" width="53" height="6" alt="" /></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" cellpadding="2" style="border: 1px solid #133E71;
                    border-top: none">
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            Title :
                        </td>
                        <td>
                            <%=lstTitles%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_TITLE" id="img_TITLE" alt="" />
                        </td>
                        <td nowrap="nowrap" colspan="2" rowspan="5">
                            &nbsp;
                        </td>
                        <td width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            First Name(s):
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="50"
                                tabindex="1" value="<%=firstname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME" id="img_FIRSTNAME"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            Last Name :
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="50"
                                tabindex="1" value="<%=lastname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_LASTNAME" id="img_LASTNAME"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            Date of Birth :
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DOB" id="txt_DOB" maxlength="10"
                                tabindex="1" value="<%=dob%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_DOB" id="img_DOB" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap="nowrap">
                            <br />
                            Disability :
                        </td>
                        <td colspan="4" bgcolor="#F4F9F2">
                            <br />
                            <%=Disability_String%>
                            <input type="hidden" name="hid_DISABILITY" id="hid_DISABILITY" value="<%=Disability%>" />
                            <img src="/js/FVS.gif" width="15" height="15" name="img_DISABILITY" id="img_DISABILITY"
                                alt="" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            Employment Status :
                        </td>
                        <td>
                            <%=lstEconomicStatus%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_ECONOMICSTATUS" id="img_ECONOMICSTATUS"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            Relationship :<br />
                            ( to tenant )
                        </td>
                        <td valign="top">
                            <%=lstRelationship%>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" width="15" height="15" name="img_RELATIONSHIP" id="img_RELATIONSHIP"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Consent
                        </td>
                        <td>
                            <input type="checkbox" name="chk_CONSENT" id="chk_CONSENT" value="1" <% if consent = 1 then response.write("checked") %> />
                            <img src="/js/FVS.gif" name="img_CONSENT" id="img_CONSENT" width="15" height="15"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <input type="hidden" name="hid_CustomerID" id="hid_CustomerID" value="<%=CustomerID%>" />
                            <input type="hidden" name="hid_CustomerType" id="hid_CustomerType" value="6" />
                            <input type="hidden" name="hid_tenancyid" id="hid_tenancyid" value="<%=request("tenancyid")%>" />
                            <input type="hidden" name="hid_Action" id="hid_Action" value="<%=action %>" />
                            <input type="button" class="RSLButton" value="<%=btnvalue %>" onclick="SaveForm()"
                                name="button" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="occFrame" id="occFrame" height="4px" width="4px"
        style="display: none"></iframe>
</body>
</html>
