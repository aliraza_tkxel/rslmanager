<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
    Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim  notes, paidDirectlyTo, i_status, action, ActionString, lstUsers, lstTeams, lstAction, actionid
	Dim tenancy_id, journalID, uc_endDate, uc_startDate, startRentBalance, endRentBalance, paidDirectlyToBHG
    Dim paidDirectlyToDefault, paidDirectlyToTenant, hid_txt_EndRentBalance, hid_txt_StartRentBalance

    Call init()
    Call get_querystring()
	path = request.form("hid_go")
    syncBalance = request("syncBalance")

	'begin processing
	OpenDB()
	
	If path  = "" then
     path = 0 
    end if
	
     If syncBalance = "" Then 
        syncBalance = 0
    End If
    
    

	If path = 0 AND syncBalance = 0 Then
		call get_record()
        call getRentBalance()
	Elseif path = 0 AND Not syncBalance = 0 Then
        call populateData() 
        call getRentBalance()
	Elseif path = 1 Then
		call new_record()
	End If
	
	CLoseDB()
	
	Function get_record()
        Dim strSQL
		
        strSQL = " SELECT ISNULL(UC.Notes, 'N/A') AS Notes,  " &_
                 " ISNULL(CONVERT(NVARCHAR, UC.EndDate, 103), 'N/A') AS EndDate,  " &_
	             " ISNULL(CONVERT(NVARCHAR, UC.StartDate, 103), 'N/A') AS StartDate, UC.StartRentBalance, UC.EndRentBalance,  UC.PaidDirectlyTo   " &_
	             " FROM		C_UniversalCredit UC  " &_
                 " WHERE		UC.JOURNALID = " & journalID & " AND UC.isCurrent = 1  " &_  
                 " ORDER 		BY UC.CreationDate DESC "
					
        Call OpenRs(rsSet, strSQL)
        notes = rsSet("Notes")
        uc_startDate = rsSet("startDate")
        startRentBalance = rsSet("StartRentBalance")
        endRentBalance = rsSet("EndRentBalance")
        paidDirectlyTo = rsSet("PaidDirectlyTo")

        if paidDirectlyTo = "BHG" then
            paidDirectlyToBHG = "selected"
        else
            paidDirectlyToTenant = "selected"
        end if
		
	End Function

	Function new_record()
		Dim strSQL
        Call get_querystring()
        Call getFormData()
				
		if NOT uc_endDate = "" then
			New_Status = 14
			Call update_journal(journalID, New_Status)			
		else
            uc_endDate = "NULL"
			New_Status = 13
		end if
    
        Set rsSet = Nothing
        if NOT uc_endDate = "NULL" then

            Call updateUC_isCurrent(journalID)
            strSQL = "INSERT INTO C_UniversalCredit (JournalId, PaidDirectlyTo, StartRentBalance, IsCurrent, CreatedBy, Notes, StartDate, CreationDate, EndRentBalance, EndDate) " &_
            "VALUES (" & journalID & ", '" & paidDirectlyTo & "', " & hid_txt_StartRentBalance & ", 1, " & Session("userid") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") &_
            "', '" & uc_startDate & "', '" & now() & "', " & hid_txt_EndRentBalance & ", '" & uc_endDate & "')"
          
            Conn.Execute(strSQL)
            
        else

            Call updateUC_isCurrent(journalID)
            strSQL = "INSERT INTO C_UniversalCredit (JournalId, PaidDirectlyTo, StartRentBalance, IsCurrent, CreatedBy, Notes, StartDate, CreationDate) " &_
            "VALUES (" & journalID & ", '" & paidDirectlyTo & "', " & hid_txt_StartRentBalance & ", 1, " & Session("userid") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "', '" & uc_startDate & "', '" & now() & "')"
           
            Conn.Execute(strSQL)
    
        end if 
	
		Response.Redirect ("../iFrames/iUniversalCreditDetail.asp?journalid=" & journalID & "&natureid=" & nature_id)  
    End Function
	
	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
	
		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid		
		set rsSet = Conn.Execute(strSQL)
	
	End Function

    'Updates the UC entries with all isCurrent = false before new entry
    Function updateUC_isCurrent(jid)
	
        strSQL = "UPDATE C_UniversalCredit SET IsCurrent = 0 WHERE JournalId = " & jid
        Conn.Execute(strSQL)

    End Function

    'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	Function getRentBalance()
    
        Dim dateFilter
        Set rsSet = Nothing
        strSQL = "SELECT TENANCYID AS TID FROM C_JOURNAL WHERE JOURNALID = " & journalID & "AND ITEMNATUREID = " & nature_id 
        Call OpenRs(rsSet, strSQL)

        If Not rsSet.EOF Then 
            tenancy_id = rsSet("TID")
        Else 
            tenancy_id = 0 
        End If

        Call CloseRs(rsSet)

        if syncBalance = 2 then
            dateFilter = uc_endDate
        else
            dateFilter = uc_startDate
        End if

        strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
        "FROM	 	F_RENTJOURNAL J " &_
        "			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
        "WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
        " AND J.TRANSACTIONDATE <=  '" & dateFilter & "'"
      
       
        Set rsSet = Nothing
        Call OpenRs (rsSet, strSQL)
        
        if syncBalance = 2 then
            hid_txt_EndRentBalance = rsSet("AMOUNT")
            endRentBalance = FormatCurrency(rsSet("AMOUNT"),2)
        else
            hid_txt_StartRentBalance = rsSet("AMOUNT")
        End if

        Call CloseRs(rsSet)
     
	End Function

    Function populateData()
        
        if Not syncBalance = 0 then
            call getFormData()
            if paidDirectlyTo = "BHG" then
                paidDirectlyToBHG = "selected"
            elseif paidDirectlyTo = "Tenant" then
                paidDirectlyToTenant = "selected"
            else
                paidDirectlyToDefault = "selected"
            end if
        else
            paidDirectlyToDefault = "selected" 
        end if

    End Function


    Function getFormData()
        uc_startDate =  Request.Form("txt_UCStartDate")
        uc_endDate =  Request.Form("txt_UCEndDate")
        hid_txt_EndRentBalance = Request.Form("hid_txt_EndRentBalance")

        if NOT hid_txt_EndRentBalance = "" then
            endRentBalance = FormatCurrency(hid_txt_EndRentBalance,2)
        else
            endRentBalance = hid_txt_EndRentBalance
        end if

        hid_txt_StartRentBalance = Request.Form("hid_txt_StartRentBalance")
        startRentBalance = FormatCurrency(hid_txt_StartRentBalance,2)
        paidDirectlyTo = Request.Form("lst_paidDirecltyTo")
        notes = Request.Form("txt_NOTES")
    End Function



     'Retirves fields from querystring which can be from either parent crm page or from this page when submitting to its self
	Function get_querystring()
		nature_id = Request("natureid")
        journalID  = Request("journalid")
	End Function

    Function init()    
        'paidDirectlyToBHG = ""
        'paidDirectlyToTenant = ""
        'paidDirectlyToDefault = ""
        'hid_txt_EndRentBalance = ""
        'hid_txt_StartRentBalance = ""
        'uc_endDate = ""
        'uc_startDate = ""
		'endRentBalance = ""
        'startRentBalance = ""
        'paidDirectlyTo = ""
        'notes = ""
	End Function

	
%>
<html>
<head>
    <title>Update Universal Credit</title> 
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <style type="text/css">
    #confrmTextDiv
{
  padding: 0px 5px 0px 5px;
  font-size:11px;
}
#confrmButtonDiv
{
  padding: 0px 10px 10px 10px;
}
#confrmHeaderDiv
{
  background-color:Black;
  color:White;
  height:20px;
  padding: 10px 10px 5px 10px;
  font-size:12px;
}

#id_confrmdiv
{
    display:none;
    background-color: #FFF;
    border: 1px solid #000;
    position: fixed;
    width: 400px;
    left: 40%;
    top: 18%;
    margin-left: -150px;
    box-sizing: border-box;
    text-align: center;
}
#id_confrmdiv button {
    background-color: #FFF;
    display: inline-block;
    border: 2px solid #899EB7;
    padding: 1px;
    text-align: center;
    width: 70px;
    cursor: pointer;
    color:#133E70;
}
#id_confrmdiv .button:hover
{
    background-color: #ddd;
}
#confirmBox .message
{
    text-align: left;
    margin-bottom: 8px;
}
    </style>
</head>

<script language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
<script language="JavaScript">

    var FormFields = new Array();
    var set_length = 4000;

    function confirmDialog() {
       
        document.getElementById('id_confrmdiv').style.display = "block";
        document.getElementById('id_truebtn').onclick = function () {
            document.getElementById('id_confrmdiv').style.display = "none";
            postData();
        };

        document.getElementById('id_falsebtn').onclick = function () {
            document.getElementById('id_confrmdiv').style.display = "none";
            return false;
        };
    }

    function save_form() {

        FormFields[0] = "txt_UCStartDate|Start Date|DATE|Y"
        FormFields[1] = "txt_UCEndDate|End Date|DATE|N"
        FormFields[2] = "lst_paidDirecltyTo|Paid directly to|SELECT|Y"
        FormFields[3] = "txt_NOTES|Notes|TEXT|N"
      
	    paidDirectlyTo = document.getElementById("lst_paidDirecltyTo").value;

	    if (!validateDate())
	        return false;
        
        if (!checkForm())
            return false;
        
        if ( ucEndDate != ""  )
            confirmDialog();
        else
            postData();
	
	}

	function validateDate() {

	    ucStartDate = document.getElementById("txt_UCStartDate").value;
	    ucEndDate = document.getElementById("txt_UCEndDate").value;

	    arrSplit = ucStartDate.split("/");
	    ucStartDate = new Date(arrSplit[2], arrSplit[1] - 1, +arrSplit[0]);

	    dateRange_fwd = new Date();
	    dateRange_fwd = new Date(dateRange_fwd.getFullYear(), dateRange_fwd.getMonth(), dateRange_fwd.getDate());

	    dateRange_bckwd = new Date();
	    dateRange_bckwd = new Date(dateRange_bckwd.getFullYear(), dateRange_bckwd.getMonth(), dateRange_bckwd.getDate());
	    dateRange_bckwd.setMonth(dateRange_bckwd.getMonth() - 12);


	    if (ucStartDate > dateRange_fwd) {
	        alert("You have entered a Start Date greater than today!");
	        return false;
	    }
	    else if (ucStartDate < dateRange_bckwd) {
	        alert("You have entered a Start Date less than past 12 months!");
	        return false;
	    }
	    if (ucEndDate != "") {
	        arrSplit = ucEndDate.split("/");
	        ucEndDate = new Date(arrSplit[2], arrSplit[1] - 1, +arrSplit[0]);

	        FormFields[1] = "txt_UCEndDate|End Date|DATE|Y"
	        if (ucEndDate < ucStartDate) {
	            alert("You have entered an End Date prior to Start Date!");
	            return false;
	        }
	        else if (ucEndDate > dateRange_fwd) {
	            alert("You have entered a End Date greater than today!");
	            return false;
	        }
	    }
	    else {
	        FormFields[1] = "txt_UCEndDate|End Date|DATE|N"
	    }
	    return true;
	}

    function postData()
    {
        RSLFORM.hid_go.value = 1;
        RSLFORM.action = "pUniversalCredit.asp?journalid=<%=journalID%>&natureid=<%=nature_id%>"
        RSLFORM.target = "CRM_BOTTOM_FRAME"
        RSLFORM.submit();
        window.close()
    }

	function countMe(obj) {
	    if (obj.value.length >= set_length) {
	        obj.value = obj.value.substring(0, set_length - 1);
	    }
	}

	function clickHandler(e) {
	    return (window.event) ? window.event.srcElement : e.target;
	}

	function syncBalance(dateIdentifier) {
	    //dateIdentifier- 1 => Start Date, 2 => End Date

	    FormFields[0] = "txt_UCStartDate|Start Date|DATE|Y"
	    if (!checkForm()) return false;
	    if (!validateDate()) return false;
	    document.RSLFORM.action = "pUniversalCredit.asp?journalid=<%=journalID%>&natureid=<%=nature_id%>&syncBalance=" + dateIdentifier;
	    document.RSLFORM.submit();
	}

    // toggle rent Balance only if 'End Date' is chosen 
    function onload() {

        if (<%=syncBalance%> != 2 && RSLFORM.txt_UCEndDate.value == "") {
            RBblock.style.display = 'none'
        }
        else {
            RBblock.style.display = 'table-row'
        }
    }

    // toggle rent Balance only if 'End Date' is chosen 
    function toggle_rentBalance() {

        if (RSLFORM.txt_UCEndDate.value == "") {
            RBblock.style.display = 'none'
        }
        else {
            RBblock.style.display = 'table-row'
            syncBalance(2);
        }
    }
	
</script>
<body bgcolor="#FFFFFF" margintop="0" marginheight="0" topmargin="6" onload="onload()">
 <div id="id_confrmdiv">
        <div id="confrmHeaderDiv">
            <span >
                <strong>Are You Sure?</strong>
            </span>
        </div>
        <br>
        <div id="confrmTextDiv" valign="top">
            <span float:center>
            As you have recorded an End Date, the Universal Credit item will be closed and it will not be possible to edit. Do you wish to close the Universal Credit item?
            </span>
        </div>
        <br>
        <div valign="bottom" id="confrmButtonDiv">
            <button id="id_falsebtn">No</button>
            &emsp;&emsp;
            <button id="id_truebtn">Yes</button>
        </div> 
    </div>
    <table width="420" border="0" cellspacing="0" cellpadding="0" style='border-collapse: collapse'>
        <form name="RSLFORM" method="POST">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style='border-bottom: 1px solid #133e71' align="center" class='RSLWhite'>
                &nbsp;
            </td>
            <td width="67" style='border-bottom: 1px solid #133e71;'>
                <img src="/myImages/spacer.gif" height="20" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style='border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71'>
                <table>
                    <tr>
                        <td valign="middle">
                            Start Date :
                        </td>
                        <td>
                            <input type="text" name="txt_UCStartDate" id="txt_UCStartDate" value="<%=uc_startDate%>"
                                maxlength="10" size="29" class="textbox200" style="width: 200px" onblur='syncBalance(1)' />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_UCStartDate',180,35,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
                        </td>
                        <td>
                            <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                                top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img src="/js/FVS.gif" name="img_UCStartDate" id="img_UCStartDate" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Paid directly to :
                        </td>
                        <td>
                            <select id="lst_paidDirecltyTo" name="lst_paidDirecltyTo" class="textbox200" style="width: 200px;">
                                <option value="" <%=paidDirectlyToDefault%>>Please Select</option>
                                <option value="BHG" <%=paidDirectlyToBHG%>>BHG</option>
                                <option value="Tenant" <%=paidDirectlyToTenant%>>Tenant</option>
                            </select>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_paidDirecltyTo" id="img_paidDirecltyTo" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                            End Date :
                        </td>
                        <td>
                            <input type="text" name="txt_UCEndDate" id="txt_UCEndDate" maxlength="10" size="29" value="<%=uc_endDate%>"
                                class="textbox200" style="width: 200px" onblur='toggle_rentBalance()' />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_UCEndDate',180,35,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
                        </td>
                        <td>
                            <div id='Calendar2' style='background-color: white; position: absolute; left: 421px;
                                top: 20px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img src="/js/FVS.gif" name="img_UCEndDate" id="img_UCEndDate" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr id="RBblock" style="display: none;">
                        <td valign="top">
                            Rent Balance :
                        </td>
                        <td>
                            <input type="text" name="txt_EndRentBalance" id="txt_EndRentBalance" class="textbox200"
                                value="<%=endRentBalance%>" style="width: 100px" readonly />
                            (As at End Date)
                            <input type="hidden" name="hid_txt_EndRentBalance" id="hid_txt_EndRentBalance" value="<%=hid_txt_EndRentBalance%>" />
                            
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_EndRentBalance" id="img_EndRentBalance" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes :
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                                class="textbox200" rows="7" cols="10" name="txt_NOTES" id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style='border-bottom: 1px solid #133e71; border-left: 1px solid #133e71'>
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="BUTTON" name='btn_close' onclick="javascript:window.close()" value=' Close '
                    class="RSLButton">
                <input type="BUTTON" name='btn_submit' onclick='save_form()' value=' Save ' class="RSLButton">
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style='border-bottom: 1px solid #133E71'>
                            <img src="/myImages/spacer.gif" width="1" height="68" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <input type="hidden" name="hid_go" value=0>
        <input type="HIDDEN" name="hid_LETTERTEXT" value="">
        <input type="hidden" name="hid_txt_StartRentBalance" id="hid_txt_StartRentBalance" value="<%=hid_txt_StartRentBalance%>" />
        </form>
    </table>
   
    <iframe src="/secureframe.asp" name="ServerIFrame" style='display: none'></iframe>
</body>
</html>
