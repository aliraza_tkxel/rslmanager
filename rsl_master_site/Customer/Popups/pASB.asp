<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'' Modified By:	Munawar Nadeem(TkXel)
	'' Created On: 	July 5, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim GENERAL_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, lstCategory, lstGrade,category, grade

	'Code Changes Start by TkXel=================================================================================

	Dim journalID, enqID, closeFlag, readFlag, serviceId

	'Code Changes End by TkXel=================================================================================

	path = request.form("hid_go")
	GENERAL_history_id 	= Request("generalhistoryid")

	' begin processing
	Call OpenDB()

	If path = "" Then path = 0 End If ' initial entry to page

	If path = 0 Then
		Call get_record()
		Call get_enquiry_id(journalID, enqID)
	ElseIf path = 1 Then
		Call new_record()

	'Code Changes Start by TkXel=================================================================================

		' Checking whether SHOW TO TENANT checkbox is selected or not
		' If selected then response would be sent to Message Alert System
		If Request.Form("chk_SHOW_TENANT") = 1 Then
			readFlag=0
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			If enqID <> "" Then
				    ' Enters only if enquiry is generated from Enquiry Management Area
				    ' Checking whether CLOSE ENQUIRY checkbox is selected or not
					If Request.Form("chk_CLOSE") = 1 Then
						closeFlag = 1
					Else
						closeFlag = 0
					End if
				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId) 
			End If
			ElseIf Request.Form("chk_CLOSE") = 1 Then
 			readFlag = 1
			closeFlag = 1
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			    If enqID <> "" Then
				    ' Enters only if enquiry is generated from Enquiry Management Area
				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
				    'Call save_response_tenant( enqID, closeFlag)
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId) 
			    End If
		   End If

		Response.Redirect ("../iFrames/iASBDetail.asp?journalid=" & journalID & "&SyncTabs=1")

	'Code Changes End by TkXel=================================================================================

	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT   JJ.JOURNALID, ISNULL(G.CATEGORY,-1) AS CATEGORY, JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, G.ASSIGNTO, JD.TEAM, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, G.ASBHISTORYID, J.ITEMNATUREID, G.ITEMACTIONID, " &_
					"			S.DESCRIPTION AS STATUS,ISNULL(CAT.GRADEID,-1) AS GRADE " &_
					"FROM		C_ASB G INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
					"			LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE cat ON cat.CATEGORYID=g.CATEGORY " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON G.ASSIGNTO = E2.EMPLOYEEID " &_
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_
					"WHERE 		ASBHISTORYID = " & GENERAL_history_id

		Call OpenRs(rsSet, strSQL)

		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		fullname	= rsSet("FULLNAME")
		assignto	= rsSet("ASSIGNTO")
		team		= rsSet("TEAM")
		item_id		= rsSet("ITEMID")
		itemnature_id = rsSet("ITEMNATUREID")
		itemaction_id = rsSet("ITEMACTIONID")
		category = rsSet("CATEGORY")
		grade = rsSet("GRADE")
		journalID = rsSet("JOURNALID")

		Call CloseRs(rsSet)

		'Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID IN (101) AND L.ACTIVE = 1 AND J.ACTIVE = 1" 

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", " style='width:250px' ")
		Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = " & itemnature_id ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", itemaction_id, NULL, "textbox200", " style='width:250px' onchange='action_change()'")
		Call BuildSelect(lstCategory, "sel_CATEGORY", "C_ASB_CATEGORY_BY_GRADE WHERE GRADEID=" & grade ,"CATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", category, NULL, "textbox200", " style='width:250px' ")
		Call BuildSelect(lstGrade, "sel_GRADE", "C_ASB_GRADE","GRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", grade, NULL, "textbox200", " style='width:250px' onchange='grade_change()' ")

	End Function

	Function new_record()

		strSQL = "SELECT JOURNALID FROM C_ASB WHERE ASBHISTORYID = " & GENERAL_history_id
		Call OpenRs(rsSet, strSQL)
			journalid = rsSet("JOURNALID")
		Call CloseRS(rsSet)

		notes = Request.form("txt_NOTES")
		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
			Call update_journal(journalid, New_Status)
		Else
			New_Status = 13
		End If

		ItemActionID = Request.Form("sel_ITEMACTIONID")
		If (ItemActionID = "") Then
			ItemActionID = "NULL"
		End If

		strSQL = "UPDATE C_JOURNAL SET TITLE = '" & Request.form("txt_TITLE") & "' WHERE JOURNALID = " & journalid
		set rsSet = Conn.Execute(strSQL)

		strSQL = 	"SET NOCOUNT ON;INSERT INTO C_ASB " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, ASSIGNTO, NOTES, CATEGORY) " &_
					"VALUES (" & journalid & ", " & New_Status & ", " & ItemActionID & ", '" & Now & "', " & Session("userid") & ", " & Request.Form("sel_USERS") & ", '" & Replace(notes, "'", "''") & "', " & Request.FOrm("sel_CATEGORY") & " )" &_
					"SELECT SCOPE_IDENTITY() AS ASBHISTORYID;"

		set rsSet = Conn.Execute(strSQL)
			generalhistory_id = rsSet.fields("ASBHISTORYID").value
			serviceId = rsSet.fields("ASBHISTORYID").value
			rsSet.close()
		Set rsSet = Nothing

		' INSERT LETTER IF ONE EXISTS
		If Request.Form("hid_LETTERTEXT") <> "" Then
			strSQL = "INSERT INTO C_ASBLETTER (ASBHISTORYID, LETTERCONTENT) VALUES (" & generalhistory_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "')"
			conn.execute(strSQL)
		End If

		'Response.Redirect ("../iFrames/iASBDetail.asp?journalid=" & journalid & "&SyncTabs=1")
	End Function

	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function

	'Code Changes Start by TkXel=================================================================================

	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jourID, ByRef enqID)

		' CALL To STORED PROCEDURE TO get Enquiry ID
		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jourID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
		' if record found/returned, setting values
		If Not rs.EOF Then
			enqID = rs("EnquiryLogID")
		End If

		Set comm = Nothing
		Set rs = Nothing

	End Function


	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag,readFlag, serviceId)

		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc
		comm.parameters(1)= enqID
		comm.parameters(2) =  closeFlag
		comm.parameters(3) =  readFlag
		comm.parameters(4) =  serviceId
		' executing sproc
		comm.execute

	End Function

     'Code Changes End by TkXel=================================================================================
%>
<html>
<head>
    <title>Update Enquiry</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_GRADE|Team|SELECT|Y"
        FormFields[3] = "sel_ITEMACTIONID|Action|SELECT|Y"
        FormFields[4] = "sel_CATEGORY|Category|SELECT|Y"
        FormFields[5] = "txt_TITLE|Title|TEXT|Y"
        FormFields[6] = "sel_GRADE|Grade|SELECT|Y"

        function save_form() {
            if (!checkForm()) return false
            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = "pASB.asp"
            document.RSLFORM.target = "CRM_BOTTOM_FRAME"
            document.RSLFORM.submit();
            window.close()
        }

        function action_change() {
            document.RSLFORM.action = "../Serverside/change_letter_action.asp?pagesource=1&ITEMACTIONID=" + document.getElementById("sel_ITEMACTIONID").options[document.getElementById("sel_ITEMACTIONID").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        function grade_change() {
            document.RSLFORM.action = "../Serverside/asb_grade_change.asp?page_id=UPDATE&GRADEID=" + document.getElementById("sel_GRADE").options[document.getElementById("sel_GRADE").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        function GetEmployees() {
            if (document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value != "") {
                document.RSLFORM.action = "../Serverside/GetEmployees.asp?SMALL=1"
                document.RSLFORM.target = "ServerIFrame"
                document.RSLFORM.submit()
            }
            else {
                document.getElementById("dvUsers").innerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200' style='width:250px'><option value=''>Please select a team</option></select>"
            }
        }

        function open_letter() {
            if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
                ManualError("img_LETTERID", "You must first select a letter to view", 1)
                return false;
            }
            var tenancy_id = opener.parent.MASTER_TENANCY_NUM
            window.open("arrears_letter.asp?tenancyid=" + tenancy_id + "&letterid=" + document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

    </script>
</head>
<body onload="window.focus();action_change()">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style='border-bottom: 1px solid #133e71;'>
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style='border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71'>
                <table>
                    <tr valign="top">
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_TITLE" id="txt_TITLE"
                                value="<%=title%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Assign To :
                        </td>
                        <td>
                            <div id="dvUsers">
                                <%=lstUsers%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Grade :
                        </td>
                        <td>
                            <%=lstGrade%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_GRADE" id="img_GRADE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category :
                        </td>
                        <td>
                            <div id="dvCategory">
                                <%=lstCategory%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CATEGORY" id="img_CATEGORY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action :
                        </td>
                        <td>
                            <%=lstAction%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Letter :
                        </td>
                        <td>
                            <div id="dvLetter">
                                <select name="sel_LETTER" id="sel_LETTER" class="textbox200" style="width: 250px">
                                    <option value=''>Please select action</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Status:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_STATUS" id="txt_STATUS"
                                value="<%=i_status%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_RECORDEDBY"
                                value="<%=fullname%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 250px" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES" rows="5" cols="20"><%=notes%></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="PreviousAction" id="PreviousAction" value="<%=action%>" />
                            <input type="hidden" name="generalhistoryid" id="generalhistoryid" value="<%=GENERAL_history_id%>" />
                            <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="center" colspan="2">
                            Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                            &nbsp;
                            <%If(Len(enqID)>0) Then%>
                            Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" id="chk_SHOW_TENANT"
                                value="1" checked="checked" />
                            &nbsp;
                            <% End If %>
                            <input type="button" value="View Letter" title="View Letter" class="RSLButton" onclick="open_letter()"
                                name="button" id="button" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style='border-bottom: 1px solid #133e71; border-left: 1px solid #133e71'>
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    value=" Close " title="Close" class="RSLButton" style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    title="Save" class="RSLButton" style="cursor: pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133E71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" style="display: none">
    </iframe>
</body>
</html>
