<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Customer/Popups/UpdateTerminationEmail.asp" -->
<%
	'' Modified By:	Munawar Nadeem(TkXel)
	'' Created On: 	July 6, 2008
	'' Reason:		Integraiton with Tenats Online

	Const REBATE_PAYMENT_TYPE = 12

	Dim termination_history_id, tenancy_id, PdrJournalID, CustomerID, action, error_mess, date_readonly, btn_disabled, recordStatus, prevTerminationDate, currTerminationDate
	Dim notes, reason, termination_date, relet_date, orig_date_test, lst_reason, lst_ReasonCategory, journal_id, tenancy_start_date, ValidateToogle
    Dim operativeName, appDate, appStartTime, appEndTime, workMobile, vwToBeArranged, vwArranged, vwAssignedToContrator, alertForOperative
    Dim supplierName, contactName, alertForSupplier, arrangedStatus, assignedToContractorStatus, toBeArrangedStatus, startedStatus, completedStatus, prevReletDate, currReletDate
    Dim VWStarted, VWCompleted, vwArrangedAmend, natureid

    'Code Changes Start by TkXel=================================================================================
	Dim journalID, enqID, closeFlag, readFlag, serviceId
    'Code Changes End by TkXel=================================================================================

	Call OpenDB()
	path 					= request.form("hid_go")
	termination_history_id 	= Request("historyid")
	action 					= Request("action")
    natureid 				= Request("natureid")
    getPdrJournalIDAndCustomerId()
    recordStatus = getStatusByPdrJournalId()
	journal_id				= getJournalID()
    currTerminationDate     = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)
    currReletDate     = FormatDateTime(Request.Form("txt_RELETDATE"),2)
	error_mess = "0"
	' begin processing
    
	If path  = "" then path = 0 end if ' initial entry to page
   
	If path = 0 Then
		Call get_record()
        if recordStatus = 2 then
            Call getOperativeDataForAlert(PdrJournalID)
        elseif recordStatus > 2 then
            Call getAlertDataForVoidWorks()
            Call getConfirmationBoxDataForVoidWorks()
        end if
	Elseif path = 1 Then
        'Function Call To Check Status of the Record As Per Journal Id
        'recordStatus = getStatusByPdrJournalId()
		If action = "AMEND" Then
            if(recordStatus = 1) then
                if(currTerminationDate <> prevTerminationDate) then
                    Response.write("<script type=""text/javascript"">opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                    Response.write("<script type=""text/javascript"">parent.opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                end if
            elseif(recordStatus = 2) then
                if(currTerminationDate <> prevTerminationDate) then
                    Response.write("<script type=""text/javascript"">opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&tab=aptArranged&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                    Response.write("<script type=""text/javascript"">parent.opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&tab=aptArranged&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                end if
            elseif(recordStatus = 3 And arrangedStatus = 1) then
                if(currTerminationDate <> prevTerminationDate) then
                    Response.write("<script type=""text/javascript"">opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=VoidWorks&tab=rearrange&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                    Response.write("<script type=""text/javascript"">parent.opener.parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=VoidWorks&tab=rearrange&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
                end if
            end if

			Call amend_record(13)
			
     'Code Changes Start by TkXel=================================================================================

		' Checking whether SHOW TO TENANT checkbox is selected or not
		' If selected then response would be sent to Message Alert System
	      If Request.Form("chk_SHOW_TENANT") = 1 Then
                readFlag = 0
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			If enqID <> "" Then
				' Enters only if enquiry is generated from Enquiry Management Area
				' Checking whether CLOSE ENQUIRY checkbox is selected or not
			      If Request.Form("chk_CLOSE") = 1 Then
					closeFlag = 1
				Else
					closeFlag = 0
				End If
				'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables
				Call save_response_tenant(enqID, closeFlag, readFlag, serviceId)
			End If
		ElseIf Request.Form("chk_CLOSE") = 1 Then
             readFlag = 1
             closeFlag = 1
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			    If enqID <> "" Then
				    ' Enters only if enquiry is generated from Enquiry Management Area
				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables
				    'Call save_response_tenant( enqID, closeFlag) 
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId) 
			    End If
		   End If
        	'Response.write("<script type=""text/javascript"">parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&jid="&PdrJournalID&"&cid="&CustomerID&"'</script>")
		'Response.Redirect ("../iFrames/iTerminationDetail.asp?journalid=" & journal_id & "&SyncTabs=1")

     'Code Changes End by TkXel=================================================================================

		' Else to IF action = "amend"
		Else
			Call cancel_record()
		End If
	End If

	Call CloseDB()


    Function build_ActivitySelect(ByRef lst, ftn_sql, isSelected)

    If ftn_sql = "" Then
        Exit Function
    End If

		Call OpenRs(rsSet, ftn_sql)
        If rsSet.EOF Then
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>No options available</option>"
            lst = lst & "</select>"
        Else
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>Please select</option>"
            int_lst_record = 0 
            If NOT isNull(isSelected) Then
			    While (NOT rsSet.EOF)
				    lstid = rsSet(0)
				    optionSelected = ""
				    If (CStr(lstid) = CStr(isSelected)) Then
					    optionSelected = " selected"
				    End If
				    lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
				    rsSet.MoveNext()
                    int_lst_record = int_lst_record + 1
			    Wend
		    Else
			    While (NOT rsSet.EOF)
				    lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				    rsSet.MoveNext()
                    int_lst_record = int_lst_record + 1
			    Wend
		    End If
            lst = lst & "</select>"
        End If
		Call CloseRs(rsSet)

        If int_lst_record = 0 Then ValidateToogle = "N" Else ValidateToogle = "Y" End If

	End Function


	Function getJournalID()

        strSQL = "SELECT JOURNALID, CONVERT(CHAR(10), TERMINATIONDATE, 103) as TerminationDate,  CONVERT(CHAR(10), RELETDATE, 103) as ReletDate FROM C_TERMINATION WHERE TERMINATIONHISTORYID = " & termination_history_id
		Call OpenRs(rsSet, strSQL)
		If Not rsSet.EOF Then 
            jid = rsSet("JOURNALID")
            prevTerminationDate 	= rsSet("TerminationDate")
            prevReletDate = rsSet("ReletDate")
        Else 
            jid = -1 
        End If
		Call CloseRS(rsSet)
		getJournalID = jid

	End Function

    Function getPdrJournalIDAndCustomerId()
        strSQL = "select pdr_journal.JOURNALID as JournalId, PDR_MSAT.CustomerId as CustomerId" &_
                 " FROM	PDR_JOURNAL" &_
                 " inner join PDR_STATUS on PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID " &_
                 " INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId" &_
		        " INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		" &_
		        " LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID" &_
		        " Cross Apply(Select Max(j.JOURNALID) as journalID from C_JOURNAL j where j.PropertyId=P__PROPERTY.PropertyID" &_
		        "	AND  j.ITEMNATUREID = 27 AND j.CURRENTITEMSTATUSID IN(13,14,15) " &_
		        "	 GROUP BY PROPERTYID) as CJournal" &_
		        " CROSS APPLY (Select MAX(TERMINATIONHISTORYID) as terminationhistory from  C_TERMINATION where JournalID=CJournal.journalID)as CTermination	" &_
		        " INNER JOIN C_TERMINATION T ON CTermination.terminationhistory=T.TERMINATIONHISTORYID" &_
		        " where PDR_STATUS.TITLE != 'Cancelled' and PDR_MSATType.MSATTypeName='Void Inspection' AND CTermination.terminationhistory = " & termination_history_id & " " &_
                "  order by JournalId desc "
                'response.Write(strSQL)
		Call OpenRs(rsSet, strSQL)
		If Not rsSet.EOF Then
         jid = rsSet("JournalId")
         cid = rsSet("CustomerId")
        Else
         jid = -1 
         cid = -1
        End If
		Call CloseRS(rsSet)
		PdrJournalID = jid
        CustomerID = cid

	End Function

    Function getStatusByPdrJournalId()
        'Query to select void records for givven journalId so current status of the Void can be checked
        strSQL = "Select j.JOURNALID,s.TITLE as InspectionStatus,  mt.MSATTypeName, m.PropertyId,m.CustomerId,m.TenancyId,work.WorkDescription, work.VoidWorksNotes, work.[Status],work.IsScheduled," &_
                " work.WorksJournalId,work.PurchaseOrderId, ISNULL(app.APPOINTMENTSTATUS,'') APPOINTMENTSTATUS" &_
                    " from PDR_JOURNAL j " &_
                " INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId" &_
                " INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId" &_
                " INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID" &_
                " OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId," &_
                " case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]" &_                                                                                     
                "  from V_RequiredWorks w " &_
                "  Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID" &_
                "  Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID" &_
                "  LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId" &_
                "  where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work" &_
                " left join PDR_APPOINTMENTs app on app.JOURNALID = work.WorksJournalId " &_
                " Where j.JOURNALID = " & PdrJournalID

        Call OpenRs(rsSet, strSQL)
        Dim iStatus, StatusByPdrJournalId, appStatus
        getStatusByPdrJournalId = 0
        arrangedStatus = 0
        assignedToContractorStatus = 0
        toBeArrangedStatus = 0

        While Not rsSet.EOF
            iStatus = Trim(rsSet("InspectionStatus"))
            if (iStatus = "To be Arranged") THEN
                StatusByPdrJournalId = 1 
            Elseif (iStatus = "Arranged") THEN
                StatusByPdrJournalId = 2     
            Else
                StatusByPdrJournalId = 3
                appStatus = Trim(rsSet("Status"))
                if (appStatus = "To be Arranged") THEN
                    toBeArrangedStatus = 1
                Elseif (appStatus = "Arranged") THEN
                    arrangedStatus = 1
                Elseif (appStatus = "Assigned To Contractor") THEN
                    assignedToContractorStatus = 1
                END IF
            END IF
            rsSet.MoveNext
        Wend

        Call CloseRS(rsSet)
        'Response.Write(StatusByPdrJournalId)
        'Response.End()
        getStatusByPdrJournalId = StatusByPdrJournalId
	End Function


	Function amend_record(open_closed)

		' WE NEED TO CHECK IF THE DATE HAS BEEN CHANGED COS WE THEN NEED TO UPDATE REBATE ROW ON TENANTS ACCOUNT
		orig_date_test = Cint(Request.Form("hid_DATEFLAG"))
		If orig_date_test = 1 Then update_rent_journal() End If

		REASON = Request.Form("sel_REASON")
		If (REASON = "") Then REASON = "NULL"

        REASONCATEGORYID = Request.Form("sel_REASONCATEGORY")
		If (REASONCATEGORYID = "") Then REASONCATEGORYID = "NULL"

        'currTerminationDate = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)
		' CREATE UPDATED TERMINATION RECORD
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_TERMINATION " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, TERMINATIONDATE, RELETDATE, REASON, REASONCATEGORYID, NOTES) " &_
					"VALUES (" & journal_id & ", " & open_closed & ", 1, " & Session("userid") &_
					 ", '" & FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2) & "', '" & FormatDateTime(Request.Form("txt_RELETDATE"),2) & "', " & REASON & ", " & REASONCATEGORYID & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "') "  &_
					 "SELECT SCOPE_IDENTITY() AS TERMINATIONHISTORYID;"

		' TKXEL CHANGES
		set rsSet = Conn.Execute(strSQL)
		    serviceId = rsSet.fields("TERMINATIONHISTORYID").value
		    rsSet.close()
		Set rsSet = Nothing
		' END OF TKXEL CHANGES

		SQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = "&open_closed&" WHERE JOURNALID = " & journal_id
		Conn.Execute(SQL)
        
		Conn.Execute ("EXEC C_TENANCY_MONITORING")
        
		journalid = journal_id 
        update_relet_date(journalId)
        
        if open_closed = 14 then
            if(recordStatus = 2) then
                Call TerminationCancelEmailForVoidInspection(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
            end if
            if(recordStatus > 2) then
                Call TerminationCancelEmailForVoidWorks(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
                Call cancelPurchaseOrdersOnTerminationCancellation(PdrJournalID)
                Call TerminationCancelEmailForVWAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
                Call TerminationCancelEmailForVWCompleted(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
                Call TerminationCancelEmailForVWStarted(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
            end if
            Call cancelRequiredWorkOnTerminationCancellation(PdrJournalID)
        else
            if(recordStatus = 2) then
                if(currTerminationDate <> prevTerminationDate) then
                     Call TerminationChangeEmailForVoidInspection(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
                end if
            end if
            if(recordStatus = 3) then
                if(currTerminationDate <> prevTerminationDate) then
                    'Response.write(PdrJournalID)
                    Call TerminationChangeEmailForVoidWorksAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate, prevReletDate, currReletDate)
                    'Response.End()
                     'Call TerminationChangeEmailForVoidWorksToBeArranged(PdrJournalID, prevTerminationDate, currTerminationDate)
                     'Call TerminationChangeEmailForVoidWorksAssignedToContractor(6153, prevTerminationDate, currTerminationDate)
                end if
                if(currReletDate <> prevReletDate) then
                    'Response.write(PdrJournalID)
                    Call ReletChangeEmailForVoidWorksArranged(PdrJournalID, prevTerminationDate, currTerminationDate,prevReletDate, currReletDate)
                    Call ReletChangeEmailForVoidWorksAssignedToContractor(PdrJournalID, prevTerminationDate, currTerminationDate,prevReletDate, currReletDate)
                    'Response.End()
                     'Call TerminationChangeEmailForVoidWorksToBeArranged(PdrJournalID, prevTerminationDate, currTerminationDate)
                     'Call TerminationChangeEmailForVoidWorksAssignedToContractor(6153, prevTerminationDate, currTerminationDate)
                end if
            end if
        end if
        
	End Function

    Function cancelPurchaseOrdersOnTerminationCancellation(journalId)
        Set comm = Server.CreateObject("ADODB.Command")
		    comm.commandtext = "PDR_CancelPurchaseOrderForVoidWorks"
		    comm.commandtype = 4
		Set comm.activeconnection = Conn
		    comm.parameters(1) = journalId
            comm.parameters(2) = Session("USERID")
		    comm.execute
		    'return_value = comm.parameters(6)
		Set comm = Nothing

    End Function
    
    Function cancelRequiredWorkOnTerminationCancellation(journalId)
        Set comm = Server.CreateObject("ADODB.Command")
		    comm.commandtext = "V_CancelAppointmentsOnTerminationCancellation"
		    comm.commandtype = 4
		Set comm.activeconnection = Conn
		    comm.parameters(1) = journalId
		    comm.execute
		    'return_value = comm.parameters(6)
		Set comm = Nothing

    End Function
    

    Function update_relet_date(journalId)
        'Response.Write(journalId)
        'Response.Write(FormatDateTime(Request.Form("txt_RELETDATE"),2))
        Set comm = Server.CreateObject("ADODB.Command")
		    comm.commandtext = "UpdateTerminationReletDate"
		    comm.commandtype = 4
		Set comm.activeconnection = Conn
		    comm.parameters(1) = journalId
            comm.parameters(2) = FormatDateTime(Request.Form("txt_RELETDATE"),2)
		    comm.execute
		    'return_value = comm.parameters(6)
		Set comm = Nothing

    End Function



	Function cancel_record()

		Dim return_value

		Call get_record()

		' CALL STORED PROCEDURE TO CARRY OUT TENANCY TERMINATION REVERSAL PROCESS
		Set comm = Server.CreateObject("ADODB.Command")
		    comm.commandtext = "F_TENANCY_END_PROCESS_REVERSAL"
		    comm.commandtype = 4
		Set comm.activeconnection = Conn
		    comm.parameters(1) = tenancy_id
		    comm.parameters(2) = Session("USERID")
		    comm.parameters(3) = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)
		    comm.parameters(4) = getlastday(FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2))
		    comm.parameters(5) = journal_id
		    comm.execute
		    return_value = comm.parameters(6)
		Set comm = Nothing

		If return_value <> "0" Then
			error_mess = return_value
			Response.Redirect ("../iFrames/iTerminationDetail.asp?error_code="&error_mess&"&journalid=" & journal_id & "&SyncTabs=1")
		Else
			amend_record(14)
		End If

	End Function


	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT 	J.TENANCYID " &_
                    "           , C.TERMINATIONHISTORYID " &_
                    "           , J.JOURNALID " &_
                    "           , T.STARTDATE " &_
					"			, C.TERMINATIONDATE " &_
                    "			, ISNULL(C.RELETDATE,CONVERT(DATETIME, DATEADD(DAY,7,C.TERMINATIONDATE))) AS RELETDATE " &_
					"			, C.REASON AS REASON " &_
                    "           , C.REASONCATEGORYID " &_
					"			, ISNULL(C.NOTES,'N/A') AS NOTES " &_
					"FROM 		C_TERMINATION C " &_
                    "           INNER JOIN C_JOURNAL J ON J.JOURNALID = C.JOURNALID " &_
					"			INNER JOIN C_TENANCY T ON J.TENANCYID = T.TENANCYID " &_
					"WHERE		C.TERMINATIONHISTORYID = " & termination_history_id

		Call OpenRs(rsSet, strSQL)
		IF NOT rsSet.EOF Then 
			tenancy_id			= rsSet("TENANCYID")
			reason	 			= rsSet("REASON")
            ReasonCategoryId    = rsSet("REASONCATEGORYID")
			notes				= rsSet("NOTES")
			journal_id			= rsSet("JOURNALID")
			tenancy_start_date  = rsSet("STARTDATE")
            termination_date 	= rsSet("TERMINATIONDATE")
            relet_date = rsSet("RELETDATE")
		Else
			notes = "RECORD NOT FOUND PLEASE CONTACT SYSTEM ADMINISTRATOR."
		End If
		Call CloseRs(rsSet)

		Call BuildSelect(lst_reason, "sel_REASON", "C_TERMINATION_REASON WHERE ACTIVE = 1", "REASONID, DESCRIPTION", "DESCRIPTION", "Please Select", reason, NULL, "textbox200", " style='width:300px' onchange=""item_change()"" ")
 
        mysql = "SELECT TRA.TERMINATION_REASON_ACTIVITYID, TRA.DESCRIPTION " &_
            "FROM C_TERMINATION_REASON TR " &_
            "INNER JOIN C_TERMINATION_REASON_TOACTIVITY TR2A ON TR.REASONID = TR2A.REASONID " &_
            "INNER JOIN C_TERMINATION_REASON_ACTIVITY TRA ON TR2A.TERMINATION_REASON_ACTIVITYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
            "WHERE TR.REASONID = " & reason & " " &_
            "ORDER BY TRA.DESCRIPTION"

        Call build_ActivitySelect(lst_ReasonCategory, mysql, ReasonCategoryId)

		' IF TERMINATION DATE HAS PASSED DONT LET THEM AMEND IT
		If CDate(FormatDateTime(date,2)) >= CDate(termination_date) Then
			date_readonly 	= " READONLY "
			btn_disabled	= " DISABLED "
		End If

	End Function

    Function getOperativeDataForAlert(pjId)
        strSQL = "select top 1 ISNULL(E__EMPLOYEE.FIRSTNAME, '') + ' ' + ISNULL(E__EMPLOYEE.LASTNAME, '') as OperativeName, CONVERT(CHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) as AppointmentDate,  APPOINTMENTSTARTTIME, appointmentendtime, " &_
			"			ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') AS Address, ISNULL(P__PROPERTY.TOWNCITY, '') as TownCity, ISNULL(P__PROPERTY.POSTCODE, '') as PostCode, ISNULL(E_CONTACT.WORKEMAIL, '') AS Email, ISNULL(E_CONTACT.WORKMOBILE, 'N/A') AS Mobile  " &_
			"	 from PDR_JOURNAL " &_
			"	 inner join PDR_APPOINTMENTS on PDR_APPOINTMENTS.JOURNALID=PDR_JOURNAL.JOURNALID " &_
			"	 INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID " &_
			"	 INNER JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID = PDR_MSAT.PropertyId " &_
			"	 inner join E__EMPLOYEE on EMPLOYEEID = PDR_APPOINTMENTS.ASSIGNEDTO " &_
            "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID " &_
			"	 where PDR_JOURNAL.JOURNALID = " & pjId
                'response.Write(strSQL)
		Call OpenRs(rsSet, strSQL)
		If Not rsSet.EOF Then
         operativeName = rsSet("OperativeName")
         appDate = rsSet("AppointmentDate")
         appStartTime = rsSet("APPOINTMENTSTARTTIME")
         appEndTime = rsSet("appointmentendtime")
         workMobile = rsSet("Mobile")
        Else
         operativeName = ""
         appDate = ""
         appStartTime = ""
         appEndTime = ""
         workMobile = ""
        End If
		Call CloseRS(rsSet)
		alertForOperative = "Operative : " + operativeName + " |Date : " + appDate + " |Appointment Time : " + appStartTime + "-" + appEndTime + " |Operative Contact : " + workMobile
    End Function

    Function getSupplierDataForAlert(poId)
        strSQL = "select org.NAME as OrgName, ISNULL(con.FIRSTNAME, '') + ' ' + ISNULL(con.LASTNAME, '') as ContactName, " &_
				 " ISNULL(E_CONTACT.WORKMOBILE, 'N/A') AS Mobile " &_
				 " from PDR_CONTRACTOR_WORK " &_
				 " inner join S_ORGANISATION org on org.ORGID = PDR_CONTRACTOR_WORK.ContractorId " &_
				 " LEFT join E__EMPLOYEE con on con.EMPLOYEEID = PDR_CONTRACTOR_WORK.ContactId " &_
				 " LEFT join E_CONTACT on E_CONTACT.EMPLOYEEID = con.EMPLOYEEID " &_
				 " where PurchaseOrderId = " & poId
               
		Call OpenRs(rsSet, strSQL)
		If Not rsSet.EOF Then
            supplierName = rsSet("OrgName")
            contactName = rsSet("ContactName")
            workMobile = rsSet("Mobile")
        Else
            supplierName = ""
            contactName = ""
            workMobile = ""
        End If
		Call CloseRS(rsSet)
		alertForSupplier = "Contractor Name : " + supplierName + " |Contact Name : " + contactName + " |Telephone : " + workMobile
    End Function

    Function getAlertDataForVoidWorks()
        
        strSQL = "Select j.JOURNALID,s.TITLE as InspectionStatus,  mt.MSATTypeName, m.PropertyId,m.CustomerId,m.TenancyId,work.WorkDescription, work.VoidWorksNotes, work.[Status],work.IsScheduled," &_
                " work.WorksJournalId,work.PurchaseOrderId" &_
                    " from PDR_JOURNAL j " &_
                " INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId" &_
                " INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId" &_
                " INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID" &_
                " OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId," &_
                " case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]" &_                                                                                     
                "  from V_RequiredWorks w " &_
                "  Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID" &_
                "  Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID" &_
                "  LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId" &_
                "  where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work" &_
                " Where j.JOURNALID = " & PdrJournalID & " order by work.PurchaseOrderId "

        Call OpenRs(rsSet, strSQL)
        Dim iStatus, StatusByPdrJournalId, isArranged, initialForArranged, initialForContractor, initialForAmend, isAssignedToContractor, prevOrderId, presentOrderId
        
        isArranged = 0
        isAssignedToContractor = 0
        vwToBeArranged = ""
        vwArranged = ""
        vwAssignedToContrator = ""
        presentOrderId = 0
        prevOrderId = 0
        initialForAmend = "Void Works have been arranged against the property."
        initialForArranged = "Void Works have been arranged against the property and will be removed from the Voids module."
        initialForContractor = "Void Works have been Assigned to Contractor against the property."
        While Not rsSet.EOF
            iStatus = Trim(rsSet("Status"))
            if (iStatus = "To be Arranged") THEN
                vwToBeArranged = "Void Works have been recorded against the property and will be removed from the Voids module."
            Elseif (iStatus = "Arranged") THEN
                isArranged = 1
                Call getOperativeDataForAlert(CLng(rsSet("WorksJournalId")))
                vwArranged = vwArranged + " |" + alertForOperative
            Elseif (iStatus = "Assigned To Contractor") THEN
                isAssignedToContractor = 1
                presentOrderId = CLng(Trim(rsSet("PurchaseOrderId")))
                if presentOrderId <> prevOrderId then
                    Call getSupplierDataForAlert(presentOrderId)
                    vwAssignedToContrator = vwAssignedToContrator + " |" + alertForSupplier
                    prevOrderId = presentOrderId
                end if
            END IF
            rsSet.MoveNext
        Wend

        if isArranged = 1 then
            vwArrangedAmend = initialForAmend + "|" + vwArranged
            vwArranged = initialForArranged + "|" + vwArranged
        end if 
        if isAssignedToContractor = 1 then
            vwAssignedToContrator = initialForContractor + "|" + vwAssignedToContrator
        end if 

        Call CloseRS(rsSet)
    End Function

    Function getConfirmationBoxDataForVoidWorks()
        
        strSQL = " Select j.JOURNALID, work.[Status],work.IsScheduled, " &_
                 " work.WorksJournalId, pas.APPOINTMENTSTATUS, ISNULL(emp.FIRSTNAME, '') + ' ' + ISNULL(emp.LASTNAME, '') as OperativeName, " &_
				 " ISNULL(EC.WORKDD, 'N/A') as DDNumber, ISNULL(EC.MOBILE, 'N/A') AS MOBILE" &_
                 "    from PDR_JOURNAL j  " &_
                 " INNER JOIN PDR_MSAT m on j.MSATID = m.MSATId  " &_
                 " INNER JOIN PDR_MSATType mt on m.MSATTypeId = mt.MSATTypeId  " &_
                 " INNER JOIN PDR_STATUS s on j.STATUSID = s.STATUSID  " &_
                 " OUTER APPLY (Select w.RequiredWorksId,w.WorkDescription,w.VoidWorksNotes,w.IsScheduled,w.IsCanceled,w.WorksJournalId,cw.PurchaseOrderId,  " &_
                 " case WHEN w.IsScheduled = 0 or w.IsScheduled is null then 'To be Arranged' ELSE s.TITLE END as [Status]                                        " &_                                               
                  " from V_RequiredWorks w   " &_
                 "  Left Join PDR_JOURNAL jj on w.WorksJournalId = jj.JOURNALID  " &_
                 "  Left JOIN PDR_STATUS s on jj.STATUSID = s.STATUSID  " &_
                  " LEFT JOIN PDR_CONTRACTOR_WORK cw on jj.JOURNALID = cw.JournalId  " &_
                  " where InspectionJournalId = j.JOURNALID AND (w.IsMajorWorksRequired is null OR w.IsMajorWorksRequired=0)) work  " &_
				  " inner join PDR_JOURNAL pj on pj.JOURNALID = work.WorksJournalId  " &_
				  " inner join PDR_APPOINTMENTS pas on pas.JOURNALID=pj.JOURNALID   " &_
				 " INNER JOIN PDR_MSAT pms ON pms.MSATId = pj.MSATID   " &_
				 " INNER JOIN P__PROPERTY pp ON pp.PROPERTYID = pms.PropertyId   " &_
				 " inner join E__EMPLOYEE emp on emp.EMPLOYEEID = pas.ASSIGNEDTO   " &_
                " INNER JOIN E_CONTACT ec ON ec.EMPLOYEEID = emp.EMPLOYEEID   " &_
                 " Where j.JOURNALID = " &  PdrJournalID & " and work.PurchaseOrderId is null " &_
				   " order by work.PurchaseOrderId "

        Call OpenRs(rsSet, strSQL)
        Dim iStatus, appStatus, StatusByPdrJournalId, isArranged, initialForStarted, initialForCompleted, isAssignedToContractor, opAlert
        
        startedStatus = 0
        completedStatus = 0
        VWStarted = ""
        VWCompleted = ""
        initialForStarted = "The Void Works have been started, and when the Termination is cancelled, all associated works appointments will be cancelled. "
        initialForCompleted = "The Void Works have been completed, and when the Termination is cancelled, all associated works appointments will be cancelled. "
        While Not rsSet.EOF
            iStatus = Trim(rsSet("Status"))
            appStatus = Trim(rsSet("APPOINTMENTSTATUS"))
            if (iStatus = "Completed") THEN
                completedStatus = 1
                opAlert = "|Operative : " + rsSet("OperativeName") + " |Mobile : " + rsSet("MOBILE") + " |DD Number : " + rsSet("DDNumber")
                VWCompleted = VWCompleted + " |" + opAlert
            Elseif ((iStatus = "Arranged") And (appStatus <> "Complete") And (appStatus <> "NotStarted")) THEN
                startedStatus = 1
                opAlert = "|Operative : " + rsSet("OperativeName") + " |Mobile : " + rsSet("MOBILE") + " |DD Number : " + rsSet("DDNumber")
                VWStarted = VWStarted + " |" + opAlert
            END IF
            rsSet.MoveNext
        Wend

        if completedStatus = 1 then
            VWCompleted = initialForCompleted + "|" + VWCompleted
        end if 
        if startedStatus = 1 then
            VWStarted = initialForStarted + "|" + VWStarted
        end if 

        Call CloseRS(rsSet)
    End Function


	Function get_formfields()

		reason	 			= Request("sel_REASON")
        ReasonCategory		= Request("sel_REASONCATEGORY")
		termination_date 	= Request("txt_TERMINATIONDATE")
        relet_date 	        = Request("txt_RELETDATE")
		notes				= Request("txt_NOTES")

	End Function

	' CALCULATE NEW REBATE FIGURE USING SP F_TENANCY_END_FORECAST AND UPDATE RENT JOURNAL ACCORDINGLY
	'	ALSO UPDATE THE ENDDATE IN CUSTOMERTENANCY AND TENANCY
	Function update_rent_journal()

		Dim row_count, sum_terms, no_rows

        Call get_record()
		Call get_formfields()

		' ONLY CALL THIS PROCEDURE IF THE TERMINATION HAS BEEN PROCESSED
		SQL = "SELECT COUNT(*) AS CNT FROM F_RENTJOURNAL_IR WHERE TENANCYID = " & tenancy_id & " AND ITEMTYPE = 1 "
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then no_rows = rsSet("CNT")	Else no_rows = -1 End If
		Call CloseRs(rsSet)

		If CLng(no_rows) > 0 Then
			' CALL STORED PROCEDURE TO CALCULATE REFUND VALUE
			Set comm = Server.CreateObject("ADODB.Command")
			    comm.commandtext = "F_PART_MONTH_RENT_ADJUSTMENT"
			    comm.commandtype = 4
			Set comm.activeconnection = Conn
			    comm.parameters(1) = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)
			    comm.parameters(2) = getlastday(FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2))
			    comm.parameters(3) = tenancy_id
			    comm.parameters(4) = journal_id
			    comm.execute
			Set comm = Nothing
		End If

		' UPDATE THE ENDDATE IN TENANCY
		SQL = "UPDATE C_TENANCY SET ENDDATE = '" & FormatDateTime(termination_date,2) & "' WHERE TENANCYID = " & tenancy_id
		Conn.Execute(SQL)

		' UPDATE THE ENDDATE IN ADDITIONAL ASSET
		SQL = "UPDATE C_ADDITIONALASSET SET ENDDATE = '" & FormatDateTime(termination_date,2) & "' WHERE TENANCYID = " & tenancy_id
		Conn.Execute(SQL)

		' UPDATE THE ENDDATES IN CUSTOMER TENANCY
		SQL = "UPDATE C_CUSTOMERTENANCY SET ENDDATE = '" & FormatDateTime(termination_date,2) & "' " &_
				" FROM C_CUSTOMERTENANCY CT " &_
				" INNER JOIN C_TERMINATION_CUSTOMERTENANCY CTCT ON CTCT.CUSTOMERTENANCYID = CT.CUSTOMERTENANCYID " &_
				" WHERE CTCT.JOURNALID = " & journal_id
		Conn.Execute(SQL)

	End Function


	' updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)

		strSQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid
		set rsSet = Conn.Execute(strSQL)

	End Function

	' calculate last day of month
	Function getlastday(datum)

		thedate = datevalue(datum)
		If month(thedate) = 4 Or month(thedate) = 9 Or month(thedate) = 6 Or month(thedate) = 11 Then
			monthdays = 30
		ElseIf month(thedate) = 2 Then
			If year(thedate) = 2004 Or year(thedate) = 2008 Or year(thedate) = 2012 Then
				monthdays = 29
			Else monthdays = 28 End If
		Else monthdays = 31 End If

		getlastday = (monthdays & "/" & month(thedate) & "/" & year(thedate))

	End function

     'Code Changes Start by TkXel=================================================================================

	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jourID, ByRef enqID)

		' CALL To STORED PROCEDURE TO get Enquiry ID
		Set comm = Server.CreateObject("ADODB.Command")
		    ' setting stored procedure name
		    comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		    comm.commandtype = 4
		Set comm.activeconnection = Conn
		    ' setting input parameter for sproc eventually used in WHERE clause
		    comm.parameters(1) = jourID
		    ' executing sproc and storing resutls in record set
		Set rs = comm.execute
	    ' if record found/returned, setting values
		If Not rs.EOF Then 
			enqID = rs("EnquiryLogID")
	    End If

	     Set comm = Nothing
	     Set rs = Nothing

	End Function


	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag,readFlag, serviceId)

      Set comm = Server.CreateObject("ADODB.Command")
	  ' setting stored procedure name
	  comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
	  comm.commandtype = 4
	  Set comm.activeconnection = Conn
	 ' setting input parameter for sproc
	  comm.parameters(1)= enqID
	  comm.parameters(2) =  closeFlag
	  comm.parameters(3) =  readFlag
	  comm.parameters(4) =  serviceId
  	  ' executing sproc
	  comm.execute

	End Function

     'Code Changes End by TkXel=================================================================================
%>
<html>
<head>
    <title>Update Termination</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_TERMINATIONDATE|Termination Date|DATE|Y"
        FormFields[1] = "sel_REASON|Reason for terminating|SELECT|Y"
        FormFields[2] = "txt_NOTES|Notes|TEXT|N"
        FormFields[3] = "txt_RELETDATE|Relet Date|DATE|Y"

        arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        function save_form(str_action) {
            
            if (document.getElementById('hid_TVal').value == "N") {
                FormFields[3] = "sel_REASONCATEGORY|Reason Category|SELECT|N"
            }
            else {
                FormFields[3] = "sel_REASONCATEGORY|Reason Category|SELECT|Y"
            }

            if (!checkForm()) return false
            if ((document.getElementById("txt_TERMINATIONDATE").value != document.getElementById("hid_TERMINATIONDATE").value) && str_action != "CANCEL")
                if (!(window.confirm("You have chosen to change the end date of this tenancy.\nIf already processed, this will add an adjustment row to the rent account.\nAre you sure you wish to continue ?")))
                    return false;
                else
                    document.getElementById("hid_DATEFLAG").value = 1;

            // WE NEED TO ENSURE THAT TENANCY END DATE IS NOT BEFORE TENANCY STARTDATE	
            if (str_action != "CANCEL") {

                StartDate = document.getElementById("hid_TENANCYSTARTDATE").value
                arrSplit = StartDate.split("/");
                StartDate = new Date(arrSplit[0] + " " + arrDates[(arrSplit[1] - 1)] + " " + arrSplit[2]);

                EndDate = document.getElementById("txt_TERMINATIONDATE").value
                arrSplit = EndDate.split("/");
                EndDate = new Date(arrSplit[0] + " " + arrDates[(arrSplit[1] - 1)] + " " + arrSplit[2]);

                if (StartDate > EndDate) {
                    alert("Cannot enter tenancy enddate before tenancy startdate :" + document.getElementById("hid_TENANCYSTARTDATE").value);
                    //document.getElementById("txt_TERMINATIONDATE").value = document.getElementById("hid_TERMINATIONDATE").value
                    return false;
                }
                var recordStatus = parseInt(document.getElementById("hid_recordStatus").value);
                
                if (recordStatus > 2) {
                    var prevReletDate = document.getElementById("hid_RELETDATE").value;
                    var curReletDate = document.getElementById("txt_RELETDATE").value;
                    
                    var alertDataForvwArranged = new String(document.getElementById("hid_vwArrangedAmend").value);
                    alertDataForvwArranged = alertDataForvwArranged.replace(/\|/g, '\n')
                    var alertDataForvwAssignToContractor = new String(document.getElementById("hid_vwAssignedToContrator").value);
                    alertDataForvwAssignToContractor = alertDataForvwAssignToContractor.replace(/\|/g, '\n')
                    var alertToShow = "";
                    if (alertDataForvwArranged == "") {
                        alertToShow = alertDataForvwAssignToContractor;
                    }
                    else if (alertDataForvwAssignToContractor == "") {
                        alertToShow = alertDataForvwArranged
                    }
                    else {
                        alertToShow = alertDataForvwArranged + "\n\n" + alertDataForvwAssignToContractor;
                    }
                    if (alertDataForvwArranged != "" || alertDataForvwAssignToContractor != "") {
                        alert(alertToShow);
                    }
                    
                }
            }
            var journalId = document.getElementById("hid_JournalId").value;
            var natureId = document.getElementById("hid_NatureId").value;
            document.getElementById("hid_go").value = 1;
            document.RSLFORM.action = "pTermination.asp?action=" + str_action + "&historyid=<%=termination_history_id%>&natureid=" + natureId + "";
            //document.RSLFORM.target = "CRM_BOTTOM_FRAME"
            document.getElementById("btn_submit").disabled = true
            document.getElementById("btn_close").disabled = true
            try {
                opener.parent.STARTLOADER('BOTTOM')
            }
            catch (e) {
                temp = 1
            }
            document.RSLFORM.submit();
            try {
                opener.parent.swap_div(opener.parent.MAIN_OPEN_TOP_FRAME, 'top')
                //var journalId = document.getElementById("hid_JournalId").value;
                //var natureId = document.getElementById("hid_NatureId").value;
                opener.parent.swap_divForTermination(journalId, natureId)
            }
            catch (e) {
                temp = 1
            }
            //window.close()
        }

        // verify cancel action
        function verify() {
            if (!(window.confirm("You have chosen to cancel this tenancy termination.\nAre you sure you wish to continue ?")))
                return false;
            else {
                var recordStatus = parseInt(document.getElementById("hid_recordStatus").value);
                if (recordStatus == 2) {
                    var alertDataForOperative = new String(document.getElementById("hid_alertForOperative").value);
                    alertDataForOperative = alertDataForOperative.replace(/\|/g, '\n')
                    alert("Void Inspection has been arranged for this property. \n\n " + alertDataForOperative);
                }
                if (recordStatus > 2) {
                    var alertDataForvwToBeArranged = document.getElementById("hid_vwToBeArranged").value;
                    alertDataForvwToBeArranged = alertDataForvwToBeArranged.replace(/\|/g, '\n')
                    var alertDataForvwArranged = new String(document.getElementById("hid_vwArranged").value);
                    alertDataForvwArranged = alertDataForvwArranged.replace(/\|/g, '\n')
                    var alertDataForvwAssignToContractor = new String(document.getElementById("hid_vwAssignedToContrator").value);
                    alertDataForvwAssignToContractor = alertDataForvwAssignToContractor.replace(/\|/g, '\n')
                    var alertToShow = "";
                    if (alertDataForvwToBeArranged == "" && alertDataForvwArranged == "") {
                        alertToShow = alertDataForvwAssignToContractor;
                    }
                    else if (alertDataForvwToBeArranged == "" && alertDataForvwAssignToContractor == "") {
                        alertToShow = alertDataForvwArranged;
                    }
                    else if (alertDataForvwArranged == "" && alertDataForvwAssignToContractor == "") {
                        alertToShow = alertDataForvwToBeArranged;
                    }
                    else if (alertDataForvwToBeArranged != "" && alertDataForvwArranged != "" && alertDataForvwAssignToContractor == "") {
                        alertToShow = alertDataForvwToBeArranged + "\n\n" + alertDataForvwArranged
                    }
                    else if (alertDataForvwToBeArranged != "" && alertDataForvwArranged == "" && alertDataForvwAssignToContractor != "") {
                        alertToShow = alertDataForvwToBeArranged + "\n\n" + alertDataForvwAssignToContractor
                    }
                    else if (alertDataForvwToBeArranged == "" && alertDataForvwArranged != "" && alertDataForvwAssignToContractor != "") {
                        alertToShow = alertDataForvwArranged + "\n\n" + alertDataForvwAssignToContractor
                    }
                    else {
                        alertToShow = alertDataForvwToBeArranged + "\n\n" + alertDataForvwArranged + "\n\n" + alertDataForvwAssignToContractor;
                    }
                    //alert(alertToShow);
                    if (alertToShow != "") {
                        alert(alertToShow);
                    }

                    

                    var startedStatus = parseInt(document.getElementById("hid_startedStatus").value);
                    var completedStatus = parseInt(document.getElementById("hid_completedStatus").value);
                    if (startedStatus == 1 || completedStatus == 1) {
                        var confBoxDataForStartedVW = document.getElementById("hid_VWStarted").value;
                        confBoxDataForStartedVW = confBoxDataForStartedVW.replace(/\|/g, '\n')
                        var confBoxDataForCompletedVW = document.getElementById("hid_VWCompleted").value;
                        confBoxDataForCompletedVW = confBoxDataForCompletedVW.replace(/\|/g, '\n')
                        var confDataToShow = confBoxDataForStartedVW + "\n\n" + confBoxDataForCompletedVW + "\n\nDo you want to continue Cancellation?";
                        if (!(window.confirm(confDataToShow))) {
                            return false;
                        }
                    }
                }
                //return false;
                save_form("CANCEL");
            }
        }

        function address_warning() {
            alert("When this tenancy was created any forwarding addresses associated with the tenants would have been set as correspondence addresses. It is therefore advisable to manually reset the correspondence addresses to their previous settings.");
        }

        function item_change() {
            
            document.RSLFORM.target = "ServerIFrame";
            document.RSLFORM.action = "../serverside/TerminationCategory_srv.asp?reasonid=" + document.getElementById("sel_REASON").options[document.getElementById("sel_REASON").selectedIndex].value;
            document.RSLFORM.submit();
        }
        function checkPath() {
            var path = document.getElementById("hid_Path");
            
            if (path != null) {
                
                if (path.value == 1) {
                    window.close();
                    window.parent.close();
                }
            
            }
        }

    </script>
</head>
<body onload="window.focus();checkPath();">
    <form name="RSLFORM" method="post" action="">
    <table width="309" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="222" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71;">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style="border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71">
                <table cellpadding="1" cellspacing="1" border="0">
                    <tr style="height: 7px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Termination:&nbsp;
                        </td>
                        <!--<td>
                            <input type="text" name="txt_TERMINATIONDATE" id="txt_TERMINATIONDATE" maxlength="20"
                                value="<%=termination_date%>" size="29" class="textbox200" style="width: 300px"
                                <%=date_readonly%> />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TERMINATIONDATE" id="img_TERMINATIONDATE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>-->
                        <td>
                            <input type="text" name="txt_TERMINATIONDATE" id="txt_TERMINATIONDATE" maxlength="20"
                                value="<%=termination_date%>" size="29" class="textbox200" style="width: 200px"
                                <%=date_readonly%> />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_TERMINATIONDATE',180,35,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
                        </td>
                        <td>
                            <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                                top: 1px; width: 5px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img src="/js/FVS.gif" name="img_TERMINATIONDATE" id="img_TERMINATIONDATE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Relet:&nbsp;
                        </td>
                        <!--<td>
                            <input type="text" name="txt_TERMINATIONDATE" id="txt_TERMINATIONDATE" maxlength="20"
                                value="<%=termination_date%>" size="29" class="textbox200" style="width: 300px"
                                <%=date_readonly%> />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TERMINATIONDATE" id="img_TERMINATIONDATE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>-->
                        <td>
                            <input type="text" name="txt_RELETDATE" id="txt_RELETDATE" maxlength="20"
                                value="<%=relet_date%>" size="29" class="textbox200" style="width: 200px"
                                />
                            <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_RELETDATE',180,35,'de','#FFFFFF','#133E71')"
                                tabindex="1">
                                <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
                        </td>
                        <td>
                            <div id='Div1' style='background-color: white; position: absolute; left: 1px;
                                top: 1px; width: 5px; height: 109px; z-index: 20; visibility: hidden'>
                            </div>
                            <img src="/js/FVS.gif" name="img_RELETDATE" id="img1" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Reason :
                        </td>
                        <td>
                            <%=lst_reason%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Category :
                        </td>
                        <td>
                            <div id="dvReasonCategory"><%=lst_ReasonCategory%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REASONCATEGORY" id="img_REASONCATEGORY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes :
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 300px" class="textbox200" rows="7" cols="10"
                                name="txt_NOTES" id="txt_NOTES"><%=notes%></textarea>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="center" colspan="2">
                            Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                            &nbsp; Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" id="chk_SHOW_TENANT"
                                value="1" checked="checked" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" nowrap="nowrap" style="border-bottom: 1px solid #133e71;
                border-left: 1px solid #133e71">
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="button" name="btn_cancel" id="btn_cancel" onclick="javascript:window.close()"
                    value=" Close " class="RSLButton" title="Close this window" style="cursor: pointer" />
                <input type="button" name="btn_close" id="btn_close" onclick="address_warning();verify()"
                    value=" Cancel Termination " class="RSLButton" title="Cancel this termination"
                    style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form('AMEND')"
                    value=" Save " class="RSLButton" title="Save" <%=btn_disabled%> style="cursor: pointer" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="hidden" name="hid_TERMINATIONDATE" id="hid_TERMINATIONDATE" value="<%=termination_date%>" />
                <input type="hidden" name="hid_RELETDATE" id="hid_RELETDATE" value="<%=relet_date%>" />
                <input type="hidden" name="hid_TENANCYSTARTDATE" id="hid_TENANCYSTARTDATE" value="<%=tenancy_start_date%>" />
                <input type="hidden" name="hid_DATEFLAG" id="hid_DATEFLAG" value="0" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="hid_TVal" id="hid_TVal" value="<%=ValidateToogle %>" />
                <input type="hidden" name="hid_vwToBeArranged" id="hid_vwToBeArranged" value="<%=vwToBeArranged%>" />
                <input type="hidden" name="hid_vwArranged" id="hid_vwArranged" value="<%=vwArranged%>" />
                <input type="hidden" name="hid_vwAssignedToContrator" id="hid_vwAssignedToContrator" value="<%=vwAssignedToContrator%>" />
                <input type="hidden" name="hid_VWStarted" id="hid_VWStarted" value="<%=VWStarted%>" />
                <input type="hidden" name="hid_VWCompleted" id="hid_VWCompleted" value="<%=VWCompleted%>" />
                <input type="hidden" name="hid_recordStatus" id="hid_recordStatus" value="<%=recordStatus %>" />
                <input type="hidden" name="hid_startedStatus" id="hid_startedStatus" value="<%=startedStatus %>" />
                <input type="hidden" name="hid_completedStatus" id="hid_completedStatus" value="<%=completedStatus %>" />
                <input type="hidden" name="hid_alertForOperative" id="hid_alertForOperative" value="<%=alertForOperative %>" />
                <input type="hidden" name="hid_vwArrangedAmend" id="hid_vwArrangedAmend" value="<%=vwArrangedAmend %>" />
                <input type="hidden" name="hid_JournalId" id="hid_JournalId" value="<%=journal_id %>" />
                <input type="hidden" name="hid_NatureId" id="hid_NatureId" value="<%=natureid %>" />
                <input type="hidden" name="hid_Path" id="hid_Path" value="<%=path %>" />
            </td>
            <td width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
