<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim JavascriptText
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, lstEscLevel, lstCategory,lstLETTER
	Dim director, chiefexec, housingmanager, ombudsman
	Dim directorId, chiefexecId, housingmanagerId, ombudsmanId, escid, catEGORY,NEXTACTION

	Dim scheduleid

	path = request.form("hid_go")
	scheduleid 	= Request("scheduleid")

	' begin processing
	Call OpenDB()

	If path  = "" then path = 0 end if ' initial entry to page

	If path = 0 Then
		Call get_record()
	Elseif path = 1 Then
		Call new_record()
	End If

	Call CloseDB()

	Function get_record()

		SQL = "SELECT TOP 1 TRANSACTIONTYPE FROM F_DDHISTORY WHERE DDSCHEDULEID = " & scheduleid & " ORDER BY DDHISTORYID DESC "
		Call OpenRs(rsAction,SQL)
		    TRANSACTIONTYPE = rsAction("TRANSACTIONTYPE")
		Call CloseRs(rsAction)

		SUBITEMID = 0
		IF TRANSACTIONTYPE = 1 THEN
			SUBITEMID = 118
		ELSEIF  TRANSACTIONTYPE = 2 THEN
			SUBITEMID = 119
		ELSEIF  TRANSACTIONTYPE = 3 THEN
			SUBITEMID = 120
		ELSEIF  TRANSACTIONTYPE = 5 THEN
			SUBITEMID = 124
		ELSEIF  TRANSACTIONTYPE = 6 THEN
			SUBITEMID = 121
		ELSEIF  TRANSACTIONTYPE = 7 THEN
			SUBITEMID = 122
		END IF

		'IF TRANSACTIONTYPE = 1 THEN
		Call BuildSelect(lstLETTER, "sel_LETTER", "C_STANDARDLETTERS WHERE NATUREID = 49 AND SUBITEMID = " & SUBITEMID, "LETTERID, LETTERDESC", "LETTERDESC", "Please Select", null, NULL, "textbox200", " style='width:250px' ")

	End Function


	Function new_record()

		signature =  Request.form("hid_SIGNATURE")
		lettertext =  Request.form("hid_LETTERTEXT")
		scheduleid = Request.form("hid_scheduleid")

		IF NOT signature <> "" THEN signature = ""
		IF NOT lettertext <> "" THEN lettertext = ""

		SQL = "EXEC C_DD_LETTER_STORE @scheduleid = " & scheduleid & "," &_
			" @LETTERCONTENT = '" & replace(lettertext,"'","''") & "'," &_
			" @LETTERSIG = '" &  signature  & "'"

			conn.execute(SQL)

		JavascriptText= "parent.DisablePrint();"

	End Function
%>
<html>
<head>
    <title>Update Enquiry</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 6px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

<%=JavascriptText%>

	function DisablePrint(){
		parent.opener.window.location.reload()
		window.close()
	}


	function save_form(){
		if (document.getElementById("hid_LETTERTEXT").value == ""){
			ManualError("img_LETTERID", "You must first attach a letter", 1)
			document.getElementById("errordiv").innerHTML = "<font color='red'><b>No letter has been attached</b></font>"
			return false;
		}
		var str = document.getElementById("hid_SIGNATURE").value
		if ( !(str.indexOf("There is currently no signature selected") == -1)){
			ManualError("img_LETTERID", "You must first attach a signature to the letter", 1)
			document.getElementById("errordiv").innerHTML = "<font color='red'><b>No signature has been attached to the letter</b></font>"
			return false;
		}
		document.getElementById("hid_go").value = 1;
		document.RSLFORM.action = "pDDScheduleLetterPrint.asp"
		document.RSLFORM.target = "ServerIFrame"
		document.RSLFORM.submit();
	}


	function open_letter(){
		document.getElementById("errordiv").innerHTML = " "
		var LetterVal = document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value
		if (LetterVal == "") {
			ManualError("img_LETTERID", "You must first select a letter to view", 1)
			return false;
		}
		var tenancy_id = opener.parent.MASTER_TENANCY_NUM
		window.open("arrears_letter.asp?tenancyid="+tenancy_id+"&letterid="+LetterVal, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
	}

    </script>
</head>
<body onload="window.focus();">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style='border-bottom: 1px solid #133e71' align="center" class='RSLWhite'>
                &nbsp;
            </td>
            <td width="67" style='border-bottom: 1px solid #133e71;'>
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="1" colspan="4" valign="top" style='border-left: 1px solid #133e71; border-bottom: 1px solid #133e71;
                border-right: 1px solid #133e71'>
                <table>
                    <tr>
                        <td>
                            Letter :
                        </td>
                        <td>
                            <%=lstLetter%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <b>note :</b> Please select a letter signature before attaching the letter.<div id="errordiv">
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style="border-bottom: 1px solid #133e71; border-left: 1px solid #133e71">
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_scheduleid" id="hid_scheduleid" value="<%=scheduleid%>" />
                <input type="hidden" name="hid_go" id="hid_go" value="0" />
                <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    value=" Close " title="Close" class="RSLButton" style="cursor:pointer" />
                <input type="button" value="View Letter" title="View Letter" class="RSLButton" onclick="open_letter()"
                    name="button2" style="cursor:pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    title="Save" class="RSLButton" style="cursor:pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133e71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe name="ServerIFrame" id="ServerIFrame" style="display: none" height="400" width="300"></iframe>
</body>
</html>
