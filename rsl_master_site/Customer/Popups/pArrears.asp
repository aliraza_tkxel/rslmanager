<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%

	'' Modified By:	Munawar Nadeem(TkXel)
	'' Created On: 	July 6, 2008
	'' Reason:		Integraiton with Tenats Online


	Dim arrears_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname, pdate, cdate, show_dates
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, actionid


     'Code Changes Start by TkXel=================================================================================

	Dim journalID, enqID, closeFlag, serviceId, readFlag

     'Code Changes End by TkXel=================================================================================

			
	path = request.form("hid_go")
	arrears_history_id 	= Request("arrearshistoryid")
	
	// begin processing
	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	If path = 0 Then
		call get_record()
		call get_enquiry_id(journalID, enqID)

	Elseif path = 1 Then
		new_record()



     'Code Changes Start by TkXel=================================================================================

		' Checking whether SHOW TO TENANT checkbox is selected or not
		' If selected then response would be sent to Message Alert System
	      If Request.Form("chk_SHOW_TENANT") = 1 Then
            readFlag = 0

			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			 

			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area

			If enqID <> "" Then

				' Enters only if enquiry is generated from Enquiry Management Area

				' Checking whether CLOSE ENQUIRY checkbox is selected or not
			      If Request.Form("chk_CLOSE") = 1 Then

					closeFlag = 1
				Else	
					closeFlag = 0
				End if


				'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
				Call save_response_tenant(enqID, closeFlag,readFlag,serviceId) 


			End If	
	

		ElseIf Request.Form("chk_CLOSE") = 1 Then
            
             readFlag = 1  
             closeFlag = 1 
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
		 
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area

			    If enqID <> "" Then

				    ' Enters only if enquiry is generated from Enquiry Management Area

				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables	
				    'Call save_response_tenant( enqID, closeFlag) 
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId) 

			    End If	
	

		   End If 


		Response.Redirect ("../iFrames/iArrearDetail.asp?journalid=" & journalid & "&SyncTabs=1")


     'Code Changes End by TkXel=================================================================================


	End If
	
	CLoseDB()
	
	Function get_record()
	
		Dim strSQL
		
		strSQL = 	"SELECT     JJ.JOURNALID,JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, A.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.COURTDATE, 103) ,'') AS CDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.POSSESSIONDATE, 103) ,'') AS PDATE, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, A.ASSIGNTO, JD.TEAM, " &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES, " &_
					"			ISNULL(A.ITEMACTIONID,'') AS ITEMACTIONID, J.ITEMNATUREID, " &_
					"			J.TITLE, A.arrearsHISTORYID, " &_
					"			S.DESCRIPTION AS STATUS " &_					
					"FROM		C_ARREARS A INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = A.JOURNALID " &_
					"			LEFT JOIN C_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = A.JOURNALID  " &_					
					"			LEFT JOIN E__EMPLOYEE E ON A.LASTACTIONUSER = E.EMPLOYEEID " &_					
					"			LEFT JOIN E__EMPLOYEE E2 ON A.ASSIGNTO = E2.EMPLOYEEID " &_										
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_															
					"WHERE 		ARREARSHISTORYID = " & arrears_history_id
					
		'response.write strSQL
		Call OpenRs(rsSet, strSQL)
		
		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		fullname	= rsSet("FULLNAME")
		assignto	= rsSet("ASSIGNTO")		
		team		= rsSet("TEAM")				
		item_id		= rsSet("ITEMID")
		actionid	= rsSet("ITEMACTIONID")
		cdate		= rsSet("CDATE")
		pdate		= rsSet("PDATE")
		itemnature_id = rsSet("ITEMNATUREID")
		journalID=rsSet("JOURNALID")
		
		// DECIDE WETHER OR NOT TO SHOW DATES IE IF ACTION = 7
		if actionid = 7 then show_dates = True end if
			
		CloseRs(rsSet)

		'if (item_id = 3) then
		'	Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID AND TEAMCODE IN ('EXE','MAR') ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		'	SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 T " &_
		'			" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & team & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"
		'else
			Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & team & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"
		'end if

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", " style='width:250px' ")
		Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = 10","ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", actionid, NULL, "textbox200", " style='width:250px' onchange='toggle_dates();action_change()'")

	End Function

	Function new_record()
	
		strSQL = "SELECT JOURNALID FROM C_arrears WHERE arrearsHISTORYID = " & arrears_history_id
	
		Call OpenRs(rsSet, strSQL)
		journalid = rsSet("JOURNALID")
		CloseRS(rsSet)
				
		notes = Request.form("txt_NOTES")
		if (Request.Form("chk_CLOSE") = 1) then
			New_Status = 14
			Call update_journal(journalid, New_Status)			
		else
			New_Status = 13
		end if
		
		// GET DATES IF POSSESSION ORDER SELETED OTHERWISE DEFUALT TO NULL
		cdate = "NULL"
		pdate = "NULL"
		if Request.form("txt_CDATE") <> "" Then cdate = "'" & Request.form("txt_CDATE") & "'" end if
		if Request.form("txt_PDATE") <> "" Then pdate = "'" & Request.form("txt_PDATE") & "'" end if
		
		strSQL =	"SET NOCOUNT ON;" &_
				 	"INSERT INTO C_arrears " &_
					"(JOURNALID, COURTDATE, POSSESSIONDATE, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ASSIGNTO, NOTES) " &_
					"VALUES (" & journalid & ", " & cdate & ", " & pdate & ", " & New_Status & ", " & Request.Form("sel_ITEMACTIONID") & "," & Session("userid") & ", " & Request.Form("sel_USERS") & ", '" & Replace(notes, "'", "''") & "')" &_
					"SELECT SCOPE_IDENTITY() AS ARREARSID;"
		
		set rsSet = Conn.Execute(strSQL)
		arears_id = rsSet.fields("ARREARSID").value
		serviceId = rsSet.fields("ARREARSID").value
		rsSet.close()
		Set rsSet = Nothing
		
		// INSERT LETTER IF ONE EXISTS
		//response.write "<BR> EMTPY IS IT " & Request.Form("hid_LETTERTEXT")
		If Request.Form("hid_LETTERTEXT") <> "" Then
			strSQL = "INSERT INTO C_CUSTOMERLETTERS (ARREASHISTORYID, LETTERCONTENT) VALUES (" & arears_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "')"
			conn.execute(strSQL)
		End If

        '**************************************************************************
        ' Commented by Adnan Mirza. 
        ' Reason: The response was not going to Tenants Online
		'Response.Redirect ("../iFrames/iArrearDetail.asp?journalid=" & journalid & "&SyncTabs=1")
		'**************************************************************************
	End Function
	
	// updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
	
		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid		
		set rsSet = Conn.Execute(strSQL)
	
	End Function
     'Code Changes Start by TkXel=================================================================================


	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jourID, ByRef enqID)


		' CALL To STORED PROCEDURE TO get enquiry ID

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jourID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

					
	     ' if record found/returned, setting values
		If Not rs.EOF Then 
			enqID = rs("EnquiryLogID")
	      End If

		
	     Set comm = Nothing
	     Set rs = Nothing

	End Function


	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag,readFlag,serviceId) 


	  Set comm = Server.CreateObject("ADODB.Command")

	  ' setting stored procedure name
	  comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
	  comm.commandtype = 4
	  Set comm.activeconnection = Conn

  
	 ' setting input parameter for sproc 
	  comm.parameters(1)= enqID
	  comm.parameters(2) =  closeFlag
	  comm.parameters(3) =  readFlag
	  comm.parameters(4) =  serviceId
		

  	  ' executing sproc 
	  comm.execute


	End Function

     'Code Changes End by TkXel=================================================================================



%>
<html>
<head>
<title>Update Enquiry</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
	FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
	FormFields[3] = "sel_ITEMACTIONID|Action|SELECT|Y"
	FormFields[4] = "txt_CDATE|Court Date|DATE|N"
	FormFields[5] = "txt_PDATE|Possession Date|DATE|N"
	
	function save_form(){
	
		FormFields[4] = "txt_CDATE|Court Date|DATE|N"
		FormFields[5] = "txt_PDATE|Possession Date|DATE|N"
		
		if (RSLFORM.sel_ITEMACTIONID.value == '7'){
			FormFields[4] = "txt_CDATE|Court Date|DATE|Y"
			FormFields[5] = "txt_PDATE|Possession Date|DATE|Y"
		}

	
		if (!checkForm()) return false
		RSLFORM.hid_go.value = 1;
		RSLFORM.action = "pArrears.asp"		
		RSLFORM.target = "CRM_BOTTOM_FRAME"		
		RSLFORM.submit();
		window.close()
	}

	function GetEmployees(){
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.action = "../Serverside/GetEmployees.asp?SMALL=1"
			RSLFORM.target = "ServerIFrame"
			RSLFORM.submit()
			}
		else {
			RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' class='textbox200' STYLE='WIDTH:250PX'><option value=''>Please select a team</option></select>"
			}
		}
	
	// toggle possesion and court dates only if 'Possesion Order' is chosen 
	function toggle_dates(){
	
		if (RSLFORM.sel_ITEMACTIONID.value == '7'){
			cblock.style.display = 'block'
			pblock.style.display = 'block'
			}
		else {
			cblock.style.display = 'none'
			pblock.style.display = 'none'
			RSLFORM.txt_CDATE.value = "";
			RSLFORM.txt_PDATE.value = "";
			}
	
	}
	
	// show date fields if current action is possession order
	function show_poss_dates(){
	
		if ('<%=show_dates%>' == 'True'){
			cblock.style.display = 'block'
			pblock.style.display = 'block'
			}
	}
	
	function action_change(){

		RSLFORM.action = "../Serverside/arrears_change_action.asp?pagesource=1&ITEMACTIONID="+RSLFORM.sel_ITEMACTIONID.value
		RSLFORM.target = "ServerIFrame"
		RSLFORM.submit()
		
	}
	
	function open_letter(){

		if (RSLFORM.sel_LETTER.value == "") {
			ManualError("img_LETTERID", "You must first select a letter to view", 1)
			return false;
		}		
		var tenancy_id = opener.parent.MASTER_TENANCY_NUM
		window.open("arrears_letter.asp?tenancyid="+tenancy_id+"&letterid="+RSLFORM.sel_LETTER.value, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
		
	}
</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onLoad="show_poss_dates();action_change();window.focus()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border=0 /></td>
      <td  height=10><img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border=0 /></td>	  
	  <td width=302 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'>&nbsp;</td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=4 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr valign=top>
			<td nowrap>Title:</td>
			<td width=100%><%=title%></td>
		</tr>
		<tr>
			<td>Team : </td>
			<td><%=lstTeams%></td>
			<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Assign To : </td>
			<td><%=lstUsers%></td>
			<TD><image src="/js/FVS.gif" name="img_USERS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Action : </td>
			<td><%=lstAction%></td>
			<TD><image src="/js/FVS.gif" name="img_ITEMACTIONID" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Letter : </td>
			<td><SELECT NAME='sel_LETTER' class='textbox200' style='width:250px'>
					<OPTION VALUE=''>Please select action</OPTION>
				</SELECT></td>
			<TD><image src="/js/FVS.gif" name="img_LETTERID" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
			<TD nowrap>Status:</td>
			<TD><input type="text" class="textbox200"  STYLE=';WIDTH:250' name="txt_STATUS" value="<%=i_status%>"></TD>
			<TD><image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD nowrap>Last Action By:</td>
			<TD><input type="text" class="textbox200" STYLE=';WIDTH:250' name="txt_RECORDEDBY" value="<%=fullname%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_RECORDEDBY" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR id='cblock' style='display:none'>
			<TD nowrap>Court Date:</td>
			<TD><input type="text" class="textbox200" STYLE=';WIDTH:250' name="txt_CDATE" value="<%=cdate%>"></TD>
			<TD><image src="/js/FVS.gif" name="img_CDATE" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR id='pblock' style='display:none'>
			<TD nowrap>Poss. Date:</td>
			<TD><input type="text" class="textbox200" STYLE=';WIDTH:250' name="txt_PDATE" value="<%=pdate%>"></TD>
			<TD><image src="/js/FVS.gif" name="img_PDATE" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD valign=top>Notes:</TD>
			<TD><textarea style='OVERFLOW:HIDDEN;WIDTH:250' class="textbox200" name="txt_NOTES" rows=5><%=notes%></textarea></TD>	
			<TD valign=top><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
				<input type="hidden" name="hid_go" value=0>
				<input type="hidden" name="PreviousAction" value="<%=action%>">
				<input type="hidden" name="arrearshistoryid" value="<%=arrears_history_id%>">				
			</TD>
		</TR>   
		<TR>
			<TD nowrap align="center" colspan=1>
			<TD nowrap colspan=1>Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" value="1">
			<%If(Len(enqID)>0) Then%>
   				 Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" value="1" CHECKED/> &nbsp;
			<%End If%>
                        <input type="button" value="View Letter" class="RSLButton" onClick="open_letter()" "name="button"/>
			</TD>
		</TR>		        
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=3 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
        <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
        <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
        </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
  <INPUT TYPE=HIDDEN NAME=hid_LETTERTEXT value="">
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerIFrame" style='display:none'></iframe>
</BODY>
</HTML>

