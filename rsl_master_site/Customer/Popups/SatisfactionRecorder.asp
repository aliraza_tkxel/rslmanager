<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
		
	Dim ORDERITEMID,journalid,DATECOMPLETED,timeset,LSTQ1,LSTQ2,LSTQ3,LSTQ4,LSTq6,LSTQ7,LSTQ8
	dIM Q1,Q2,Q3,Q4,q5,q6,Q7,Q8,CLOSEWINDOW
	
	OpenDB()
	
		If request("hid_go") <> "" then
	 		GetVars()
			new_record()
		Else
			Build_selects()
		End If
	CloseDB()

	Function Build_selects()
	
		Call BuildSelect(lstQ1, "sel_Q1", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (1,2,3,4) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ2, "sel_Q2", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (1,2,3,4) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ3, "sel_Q3", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (1,2,3,4) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ4, "sel_Q4", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (1,2,3,4) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ6, "sel_Q6", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (5,6) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ7, "sel_Q7", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (5,6) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")
		Call BuildSelect(lstQ8, "sel_Q8", "C_SATISFACTION_ANSWER_TYPE WHERE SATISFACTION_ANSWER_TYPE_ID IN (5,6) ", "SATISFACTION_ANSWER_TYPE_ID, DESCRIPTION", "SATISFACTION_ANSWER_TYPE_ID", "Please Select", team, NULL, "textbox200", " style='width:100px' ")

	End Function


	Function GetVars()
	
		ORDERITEMID		=	Request("hid_ORDERITEMID")
		journalid		=	Request("hid_journalid")
		DATECOMPLETED	=	Request("txt_DATECOMPLETED")
		timeset 		= 	replace(Request("txt_TIMECOMPLETED"),".",":") 
		Q1				=	Request("sel_Q1")
		Q2				=	Request.form("sel_Q2")			
		Q3				=	Request("sel_Q3")				
		Q4				=	Request("sel_Q4")				
		Q5				=	Request("txt_Q5")				
		Q6				=	Request("sel_Q6")				
		Q7				=	Request("sel_Q7")			
		Q8				=	Request("sel_Q8")			
	
	End Function


	Function new_record()
	
		strSQL = 	"INSERT INTO C_SATISFACTION_ANSWERS " &_
					"(ORDERITEMID, JOURNALID , DATECOMPLETED, Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8, USERID) " &_
					"VALUES (" & ORDERITEMID & "," & journalid & ", '" & DATECOMPLETED & " " & timeset & "'," & Q1  & ", " & Q2  & ", " & Q3  & ", " & Q4  & ", '" & Q5  & "', " & Q6  & ", " & Q7  & ", " & Q8 & "," & Session("userid") & ")"
		rw strSQL
		Conn.Execute(strSQL)
		
		strSQL = 	"UPDATE C_SATISFACTIONLETTER SET SATISFACTION_LETTERSTATUS = 3 WHERE JOURNALID = "& journalid 
		Conn.Execute(strSQL)

		CLOSEWINDOW = "TRUE"
	End Function
	
%>
<html>
<head>
<title>Satisfaction Results Recorder</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "sel_Q1|Question 1|SELECT|Y"
	FormFields[1] = "sel_Q2|Question 2|SELECT|Y"
	FormFields[2] = "sel_Q3|Question 3|SELECT|Y"
	FormFields[3] = "sel_Q4|Question 4|SELECT|Y"
	FormFields[4] = "txt_Q5|Question 5|TEXT|N"
	FormFields[5] = "sel_Q6|Question 6|SELECT|Y"
	FormFields[6] = "sel_Q7|Question 7|SELECT|Y"
	FormFields[7] = "sel_Q8|Question 8|SELECT|Y"
	FormFields[8] = "txt_DATECOMPLETED|Date Completed|DATE|Y"
	FormFields[9] = "txt_TIMECOMPLETED|Time Completed|TEXT|Y"
	

	function save_form(){
		if (!checkForm()) return false
		RSLFORM.hid_go.value = 1;
		RSLFORM.action = ""		
		RSLFORM.target = ""		
		RSLFORM.submit();
	}
	
	<% If CLOSEWINDOW = "TRUE" Then %>
		opener.GetList()
		window.close()
	<%  End If %>

</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=386 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/tab_surveyresultsrecorder.gif" width="182" height="20" alt="" border=0 /></td>
      <td width=140 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'><p>&nbsp;</p>
      </td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=3 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table width="376">
		<tr valign=top>
		  <td nowrap>&nbsp;</td>
		  <td align="right">&nbsp;</td>
		  <TD>&nbsp;</TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>Date Completed: </td>
		  <td align="right"><input name="txt_DATECOMPLETED" type="text" class="textbox200" style='WIDTH:100' value="DD/MM/YYYY" onClick="this.value = ''"></td>
		  <TD><image src="/js/FVS.gif" name="img_DATECOMPLETED" width="15px" height="15px" border="0"></TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>Time Completed: </td>
		  <td align="right"><input name="txt_TIMECOMPLETED" type="text" class="textbox200" style='WIDTH:100' value="00:00"></td>
		  <TD><image src="/js/FVS.gif" name="img_TIMECOMPLETED" width="15px" height="15px" border="0"></TD>
		  </tr>
		<tr valign=top>
		  <td nowrap>&nbsp;</td>
		  <td align="right">&nbsp;</td>
		  <TD>&nbsp;</TD>
		  </tr>
		<tr valign=top>
			<td width="66%" nowrap>1. How easy was it to report a repair? </td>
			<td width=26% align="right"><%=lstQ1%></td>
						<TD width="8%"><image src="/js/FVS.gif" name="img_Q1" width="15px" height="15px" border="0"></TD>							

		</tr>
		<tr>
			<td>2. How helpful where the BHA staff? </td>
			<td align="right"><%=lstQ2%></td>
			<TD width="8%"><image src="/js/FVS.gif" name="img_Q2" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>3. What did you think of the contractors? </td>
			<td align="right"><%=lstQ3%></td>
			<TD><image src="/js/FVS.gif" name="img_Q3" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
			<TD nowrap>4. What was the standard of the repair? </td>
			<TD align="right"><%=lstQ4%></TD>
			<TD><image src="/js/FVS.gif" name="img_Q4" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD nowrap>5. Service improvement suggestions: </td>
			<TD align="right">&nbsp;</TD>
			<TD><image src="/js/FVS.gif" name="img_Q5" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD colspan="3" nowrap><textarea style='OVERFLOW:HIDDEN;WIDTH:350' class="textbox200" name="txt_Q5" rows=5><%=q_5%></textarea>			  <image src="/js/FVS.gif" name="img_Q5" width="15px" height="15px" border="0"></td>
		  </TR>		
		<TR>
			<TD valign=top>6. Review Involvement?		    </TD>
			<TD align="right"><%=lstQ6%></TD>	
			<TD valign=top><image src="/js/FVS.gif" name="img_Q6" width="15px" height="15px" border="0"> 
			</TD>
		</TR>
		<TR>
		  <TD valign=top>7.Did the contractor contact tenant?</TD>
		  <TD align="right"><%=lstQ7%></TD>
		  <TD valign=top><image src="/js/FVS.gif" name="img_Q7" width="15px" height="15px" border="0"></TD>
		  </TR>
		<TR>
		  <TD valign=top>8. Completed on first appointment </TD>
		  <TD align="right"><%=lstQ8%></TD>
		  <TD valign=top><image src="/js/FVS.gif" name="img_Q8" width="15px" height="15px" border="0"> </TD>
		  </TR>   
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
        <input name="hid_ORDERITEMID" type="hidden" value="<%=REQUEST("ORDERITEMID")%>">
		<input name="hid_JOURNALID" type="hidden" value="<%=REQUEST("JOURNALID")%>">
        <input name="hid_go" type="hidden" value="">
        <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
        <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
      </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerIFrame" style='display:none'></iframe>
</BODY>
</HTML>

