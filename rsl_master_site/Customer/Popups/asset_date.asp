<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>

	window.returnValue = false;	
	
	var FormFields = new Array();
	FormFields[0] = "txt_THEDATE|Asset Date|DATE|Y"

	var iMonths  = new Array ("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

	function SaveForm(){
		
		var thedate = RSLFORM.txt_THEDATE.value;
		if (!checkForm()) return false;
		MinStartDate = new Date("<%=Request("MinStartDate")%>")
		Date_Array = thedate.split("/")
		CompareDate = new Date(Date_Array[0] + " " + iMonths[parseInt(Date_Array[1],10)] + " " + Date_Array[2])
		if (MinStartDate > CompareDate) {
			alert("Please enter a date which is greater than or equal to the Main Tenancy start date : '<%=Request("MinStartDate")%>'")
			return false;
			}
		window.returnValue = thedate;
		window.close();
	
	}

	function CloseForm(){
	
		window.returnValue = false;
		window.close();
	}
	
	
</script>
<body>
<form name="RSLFORM" method="POST">
<TABLE WIDTH=100% HEIGHT=100% STYLE="BORDER: SOLID 1PX #133E71" bgcolor=#e0e0e0 ALIGN=CENTER VALIGN=CENTER CELLPADDING=1 CELLSPACING=2 border=0>
	<TR>
		<TD rowspan=4 width=15px></TD><TD ALIGN=CENTER>Please enter a start date for the commencement of the additional<br>asset tenancy.<TD>
	</TR>
	<TR>
		<TD ALIGN=CENTER>
			<input type="text" name=txt_THEDATE class='textbox100'>
			<image src="/js/FVS.gif" name="img_THEDATE" width="15px" height="15px" border="0">
		<TD>
	</TR>
	<TR>	
		<TD ALIGN=CENTER>
		<INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_CANCEL' ONCLICK='CloseForm()' value = ' CANCEL '>		
		<INPUT TYPE=BUTTON NAME='btn_OK'  CLASS='RSLBUTTON' ONCLICK='SaveForm()' value=' ADD ASSET '>
		</TD>
	</TR>
</TABLE>
</FORM>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
