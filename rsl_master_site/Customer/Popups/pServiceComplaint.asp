<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'' Modified By:	Munawar Nadeem(TkXel)
	'' Created On: 	July 5, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim complaint_history_id	' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, lstEscLevel, lstCategory,lstLETTER, i_outcome
	Dim director, chiefexec, housingmanager, ombudsman
	Dim directorId, chiefexecId, housingmanagerId, ombudsmanId, escid, catEGORY,NEXTACTION
	Dim escid_incrementer  , closeItem, ResoltutionReached_javascript

	'Code Changes Start by TkXel=================================================================================

	Dim journalID, enqID, closeFlag, serviceId, readFlag

	'Code Changes End by TkXel=================================================================================

	path = request.form("hid_go")
	complaint_history_id = Request("complainthistoryid")

	'begin processing
	Call OpenDB()

	If path = "" Then path = 0 End If ' initial entry to page

	If path = 0 Then
		Call get_record()
		Call get_enquiry_id(journalID, enqID)
	Elseif path = 1 Then
		Call new_record()

	'Code Changes Start by TkXel=================================================================================

		' Checking whether SHOW TO TENANT checkbox is selected or not
		' If selected then response would be sent to Message Alert System
	      If Request.Form("chk_SHOW_TENANT") = 1 Then
             readFlag = 0
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			    If enqID <> "" Then
				    ' Enters only if enquiry is generated from Enquiry Management Area
				        ' Checking whether CLOSE ENQUIRY checkbox is selected or not
			            If Request.Form("chk_CLOSE") = 1 Then
					        closeFlag = 1
				        Else
					        closeFlag = 0
				        End if
				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables
				    'Call save_response_tenant( enqID, closeFlag) 
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId)
			    End If
           ElseIf Request.Form("chk_CLOSE") = 1 Then
             readFlag = 1
             closeFlag = 1
			 ' Value of journalID  is already obtained in new_record()
			 ' obtaining enqID value against journalID, from TO_ENQUIRY_LOG
			 Call get_enquiry_id(journalID, enqID)
			' If get_enquir_id() returns nothing, implies that enquiry was NOT generated from Enquiry Management Area
			    If enqID <> "" Then
				    ' Enters only if enquiry is generated from Enquiry Management Area
				    'Call to save response in TO_RESPONSE_LOG and TO_ENQUIRY_LOG tables
				    'Call save_response_tenant( enqID, closeFlag)
				    Call save_response_tenant(enqID, closeFlag, readFlag, serviceId)
			    End If
		   End If
		Response.Redirect ("../iFrames/iServiceComplaintDetail.asp?journalid=" & journalid & "&SyncTabs=1")

	'Code Changes End by TkXel=================================================================================

	End If

	Call CloseDB()

	Function get_record()

		escid_incrementer = 0 ' increments the escalation id if action is reached its max

		' NEED TO CREATE A RULE TO LOOK AT THE PREVIOUS ACTION AND IF THE PREV ACTION IS
		' THE MAX ONE THEN THE ESCALATION LEVEL NEEDS TO MOVE UP

		MAX_ACTION_SQL = "SELECT MAX(ACTIONID) As max_action, MIN(ACTIONID) As MIN_action FROM C_LETTERACTION WHERE NATURE = 9"
		Call OpenRs(rsSet,MAX_ACTION_SQL)
			Min_ACTION = rsSet("Min_ACTION")
			MAX_ACTION = rsSet("MAX_ACTION")
		Call CloseRs(rsSet)

		MAX_escalation_SQL = "SELECT MAX(escalationid) As max_escalation FROM C_ESCALATIONLEVEL"
		Call OpenRs(rsSet,MAX_escalation_SQL)
			max_escalation = rsSet("max_escalation")
		Call CloseRs(rsSet)

		Dim strSQL
		strSQL = 	"SELECT     JJ.JOURNALID,SCC.DESCRIPTION AS CATEGORY, G.ESCALATIONLEVEL, JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, G.ASSIGNTO, JD.TEAM, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, g.team, " &_
					"			J.TITLE, G.complaintHISTORYID, J.ITEMNATUREID, ISNULL(G.ITEMACTIONID, "& Min_ACTION & ") AS ITEMACTIONID, " &_
					"			NLA.ACTIONID as NEXTITEMACTIONID, ISNULL(NLA.DESCRIPTION, 'Logged') AS NEXTACTION, " &_
					"			S.DESCRIPTION AS STATUS, " &_
                    "			G.OUTCOME AS OUTCOME " &_
					"FROM		C_SERVICECOMPLAINT G INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON G.ASSIGNTO = E2.EMPLOYEEID " &_
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_
					"			LEFT JOIN C_SERVICECOMPLAINT_CATEGORY SCC ON SCC.CATEGORYID = G.CATEGORY " &_
					"			LEFT JOIN C_LETTERACTION LA ON LA.ACTIONID = G.ITEMACTIONID " &_
					"			LEFT JOIN C_LETTERACTION NLA ON NLA.ACTIONID = LA.NEXTACTION " &_
					"WHERE 		COMPLAINTHISTORYID = " & complaint_history_id

		Call OpenRs(rsSet, strSQL)

		i_status 			= rsSet("STATUS")
		title 				= rsSet("TITLE")
		notes				= rsSet("NOTES")
		fullname			= rsSet("FULLNAME")
		teamid				= rsSet("TEAM")
		assignto			= rsSet("ASSIGNTO")
		team				= rsSet("TEAM")
		item_id				= rsSet("ITEMID")
		itemnature_id 		= rsSet("ITEMNATUREID")
		itemaction_id 		= rsSet("ITEMACTIONID")
		NEXTitemaction_id 	= rsSet("NEXTITEMACTIONID")
		NEXTACTION 	  		= rsSet("NEXTACTION")
		escid		  		= rsSet("ESCALATIONLEVEL")
		catEGORY			= rsSet("CATEGORY")
		journalID			= rsSet("JOURNALID")
        i_outcome           = rsSet("OUTCOME")

		' show the Resolution Reached radio options if we are hitting the last action
		If NEXTitemaction_id = MAX_ACTION Then
			ResoltutionReached_javascript = "document.getElementById(""ResolutionReached_Line"").style.display = ""block""; document.getElementById(""ResolutionReached_Line"").style.display = ""table-row"" ; document.getElementById(""chk_CLOSE"").checked = true;"
		End If

		' to deal with old complaints from old process
		If itemaction_id <> "" Then
			' if the action is the last in current process
			If (cInt(MAX_ACTION) = cInt(itemaction_id)) Then
					' clear option to show Resolution Reached radio options
					ResoltutionReached_javascript = ""
					' in the escaltion dropdown we only show esc's above if it needs to increment
					Esc_level_sql = " AND ESCALATIONID > " & escid
					escid_incrementer = 1
					' set the action back to the begining - usually logged
					MAX_ACTION_SQL = "SELECT DESCRIPTION AS NEXTACTION FROM C_LETTERACTION WHERE NATURE = 9 AND ACTIONID = " & Min_ACTION
					Call OpenRs(rsAction,MAX_ACTION_SQL)
						NEXTACTION 	= rsAction("NEXTACTION")
					Call CloseRs(rsAction)
			End If
		End If

		If itemaction_id <> "" Then
			' if the escalation is the last one and the action is we need to close the item as you cant go further
			If (cInt(MAX_ACTION) = cInt(itemaction_id+1)) AND (max_escalation = escid) Then
				Esc_level_sql = " AND ESCALATIONID = " & escid
				closeItem = "checked"
			' else we increment the escalation and then start the process again
			End If
		End If

		Call CloseRs(rsSet)

		Call get_escalations()
		Call BuildSelect(lstEscLevel, "sel_ESCLEVEL", "C_ESCALATIONLEVEL WHERE ESCALATIONID >= " & escid & Esc_level_sql, "ESCALATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", escid + escid_incrementer, NULL, "textbox200", " style='width:250px' onchange='Manual_Escalaltion_Increase()' ")
		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", teamID, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		Call BuildSelect(lstLETTER, "sel_LETTER", "C_STANDARDLETTERS WHERE NATUREID = 9", "LETTERID, LETTERDESC", "LETTERDESC", "Please Select", null, NULL, "textbox200", " style='width:250px' ")

		' for old service complaints working in the old structure
		If teamID <> "" Then
			teamid_SQL = " WHERE TEAM = " & teamID
		End If
		Call BuildSelect(lstUsers, "sel_USERS", " E__EMPLOYEE E INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " & teamid_SQL  , "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", "style='width:250px'  ")

	End Function

	' RETURNS THE NAMES OF THE LAST FOUR ESCALATION LEVELS
	Function get_escalations()

		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "C_GET_ESCALATIONS"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = 0
		comm.execute
		director = comm.parameters(2)
		chiefexec = comm.parameters(3)
		housingmanager = comm.parameters(4)
		ombudsman = comm.parameters(5)
		directorId = comm.parameters(6)
		chiefexecId = comm.parameters(7)
		housingmanagerId = comm.parameters(8)
		ombudsmanId	= comm.parameters(9)
		Set comm = Nothing

	End Function

	Function new_record()

		team = Request.form("sel_teams")
		assignto = Request.form("sel_users")
		signature = Request.form("hid_SIGNATURE")
		lettertext = Request.form("hid_LETTERTEXT")
		resolutionReached = Request.form("rdo_RESOLUTIONREACHED")
		notes = Request.form("txt_NOTES")
        outcome = Request.form("rdo_OUTCOME")

		If NOT team <> "" Then team = NULL
		If NOT assignto <> "" Then assignto = NULL
		If NOT signature <> "" Then signature = ""
		If NOT lettertext <> "" Then lettertext = ""
		If NOT resolutionReached <> "" Then resolutionReached = NULL
		If NOT notes <> "" Then notes = ""
        If outcome = "" Then outcome = NULL

		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
		Else
			New_Status = 13
		End If

		Dim cmd, param

		If outcome >=0 Then
		SQL = "EXEC C_SERVICECOMPLAINTS_UPDATE @HISTORYID = " & complaint_history_id & "," &_
			" @CURRENTITEMSTATUSID = " & New_Status & ", " &_
			" @LASTACTIONUSER = " &  Session("userid") & ", " &_
			" @TEAM = " & team & "," &_
			" @ASSIGNTO = " & assignto & "," &_
			" @NOTES = '" & Replace(notes, "'", "''")  & "'," &_
			" @LETTERCONTENT = '" & replace(lettertext,"'","''") & "'," &_
			" @LETTERSIG = '" &  Replace(signature,"'","''")  & "'," &_
			" @ESCALATIONLEVEL = " & Request("sel_ESCLEVEL") & "," &_
			" @RESOLUTIONREACHED = " & resolutionReached & "," &_ 
            " @OUTCOME = " & outcome & "" 	
          ELSE 
		
		SQL = "EXEC C_SERVICECOMPLAINTS_UPDATE @HISTORYID = " & complaint_history_id & "," &_
			" @CURRENTITEMSTATUSID = " & New_Status & ", " &_
			" @LASTACTIONUSER = " &  Session("userid") & ", " &_
			" @TEAM = " & team & "," &_
			" @ASSIGNTO = " & assignto & "," &_
			" @NOTES = '" & Replace(notes, "'", "''")  & "'," &_
			" @LETTERCONTENT = '" & replace(lettertext,"'","''") & "'," &_
			" @LETTERSIG = '" &  Replace(signature,"'","''")  & "'," &_
			" @ESCALATIONLEVEL = " & Request("sel_ESCLEVEL") & "," &_
			" @RESOLUTIONREACHED = " & resolutionReached & "" 
          End If  
			Call OpenRs(rsAction,SQL)
				JOURNALID = rsAction("JOURNALID")
				SERVICEID = rsAction("SERVICEID")
			Call CloseRs(rsAction)

        'Response.Redirect ("../iFrames/iServiceComplaintDetail.asp?journalid=" & journalid & "&SyncTabs=1")

	End Function


	'Code Changes Start by TkXel=================================================================================


	' This function retrieves enquiryID against journalID from TO_ENQUIRY_LOG
	Function get_enquiry_id(jourID, ByRef enqID)

		' CALL To STORED PROCEDURE TO get enquiry ID
		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_GetEnquiryID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = jourID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
		' if record found/returned, setting values
		If Not rs.EOF Then
			enqID = rs("EnquiryLogID")
		End If
		Set comm = Nothing
		Set rs = Nothing

	End Function

	' This function is used to INSERT record in TO_RESPONSE_LOG and to UPDATE TO_ENQUIRY_LOG
	Function save_response_tenant(enqID, closeFlag ,readFlag, serviceId)

		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_RESPONSE_LOG_ENQUIRY_LOG_AddResponse"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc
		comm.parameters(1)= enqID
		comm.parameters(2) =  closeFlag
		comm.parameters(3) =  readFlag
		comm.parameters(4) =  serviceId
		' executing sproc
		comm.execute

	End Function

	'Code Changes End by TkXel=================================================================================
%>
<html>
<head>
    <title>Update Enquiry</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array();

	FormFields[0] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[1] = "sel_ESCLEVEL|Escalation Level|SELECT|Y"
	FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
	FormFields[3] = "sel_USERS|Assign To|SELECT|Y"


	function save_form(){
		if ((document.getElementById("chk_CLOSE").checked == true) && (document.getElementById("txt_NOTES").value == "")) {
			alert("Notes have to be recorded when a resolution is reached or the item is closed.")
			return false;
		}
		if (!checkForm()) return false
		document.getElementById("hid_go").value = 1;
		document.RSLFORM.action = "pServiceComplaint.asp"
		document.RSLFORM.target = "CRM_BOTTOM_FRAME"
		<%
		If closeItem <> "" Then
		%>
			document.getElementById("chk_CLOSE").checked = true;
			if(!confirm("This is the last stage in the Service Complaints Process. The item will be automatically closed once you save.")){
				return false;
			}
		<%
		End If
		%>
		document.RSLFORM.submit();
		window.close()
	}

	function action_change(){
		document.RSLFORM.action = "../Serverside/change_letter_action.asp?SMALL=1&ITEMACTIONID="+RSLFORM.sel_ITEMACTIONID.value
		document.RSLFORM.target = "ServerIFrame";
		document.RSLFORM.submit();
	}

	function GetEmployees(){
		document.RSLFORM.target = "ServerIFrame";
		document.RSLFORM.action = "../serverside/getEmployees.asp?sel_Teams=<%=item_id%>";
		document.RSLFORM.submit();
	}

	function GetEmployee_detail() { }

	function open_letter(){
		if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
			ManualError("img_LETTERID", "You must first select a letter to view", 1)
			return false;
		}
		var tenancy_id = opener.parent.MASTER_TENANCY_NUM
		window.open("arrears_letter.asp?tenancyid="+tenancy_id+"&letterid="+document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
	}

	function Manual_Escalaltion_Increase(){
		CurrentActionLevel = "<%=NEXTACTION%>"
		CurrentEscLevel	= <%=escid%>
		if (document.getElementById("sel_ESCLEVEL").options[document.getElementById("sel_ESCLEVEL").selectedIndex].value > CurrentEscLevel) {
			if(!(confirm("You have changed the escalation level which may effect your internal procedures. Click YES to continue or NO to cancel."))){
				document.getElementById("txt_ACTION").value = CurrentEscLevel;
				return false;
			}
		document.getElementById("txt_ACTION").value = "Logged"
		}
		else
			document.getElementById("txt_ACTION").value = CurrentActionLevel
	}

	function open_letter(){
		if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
			ManualError("img_LETTERID", "You must first select a letter to view", 1)
			return false;
		}
		var tenancy_id = opener.parent.MASTER_TENANCY_NUM
		window.open("arrears_letter.asp?tenancyid="+tenancy_id+"&letterid="+document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "_blank", "width=570,height=600,left=100,top=50,scrollbars=yes");
	}

	function LoadMisc(){
		<%=ResoltutionReached_javascript%>
	}
    </script>
</head>
<body onload="window.focus();LoadMisc();">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71;">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style="border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71">
                <table>
                    <tr valign="top">
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td width="100%">
                            <%=title%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category :
                        </td>
                        <td>
                            <%=Category%>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="ResolutionReached_Line" style="display: none">
                        <td colspan="2">
                            Resolution Reached :
                            <input type="radio" name="rdo_RESOLUTIONREACHED" id="rdo_RESOLUTIONREACHED" value="1"
                                checked="checked" onclick="RSLFORM.chk_CLOSE.checked = true; RSLFORM.sel_ESCLEVEL.value = <%=escid%>" />
                            Yes&nbsp;
                            <input type="radio" name="rdo_RESOLUTIONREACHED" id="Radio1" value="0" onclick="RSLFORM.chk_CLOSE.checked = false; RSLFORM.sel_ESCLEVEL.value = <%=escid%> + 1; Manual_Escalaltion_Increase()" />
                            No
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Esc. Level :
                        </td>
                        <td>
                            <%=lstEscLevel%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ESCLEVEL" id="img_ESCLEVEL" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Team :
                        </td>
                        <td>
                            <%=lstTeams%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Assign To :
                        </td>
                        <td>
                            <div id="dvUsers">
                                <%=lstUsers%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Action :
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_ACTION" id="txt_ACTION"
                                value="<%=NEXTACTION%>" readonly="readonly" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Letter :
                        </td>
                        <td>
                            <%=lstLetter%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Status:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_STATUS" id="txt_STATUS"
                                value="<%=i_status%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STATUS" id="img_STATUS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <input type="text" class="textbox200" style="width: 250px" name="txt_RECORDEDBY"
                                id="txt_RECORDEDBY" value="<%=fullname%>" readonly="readonly" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 250px" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES" rows="5" cols="10"><%=notes%></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="small" id="small" value="1" />
                            <input type="hidden" name="PreviousAction" id="PreviousAction" value="<%=action%>" />
                            <input type="hidden" name="complainthistoryid" id="complainthistoryid" value="<%=complaint_history_id%>" />
                        </td>
                    </tr>
                    <% 
                        noChecked = ""
						yesChecked = ""
                        If (i_outcome <> "") Then
                            If (i_outcome = "0") Then
						        yesChecked = "checked=""checked"""
                            ElseIf (i_outcome = "1") Then
						        noChecked = "checked=""checked"""
					        End If
					    End If
                    %>
                    <tr>
                        <td nowrap="nowrap">
                            Outcome :
                        </td>
                        <td>
                            <input type="radio" name="rdo_OUTCOME" id="rdo_Upheld" <%=yesChecked%> value="0" />
                            Upheld &nbsp;
                            <input type="radio" name="rdo_OUTCOME" id="rdo_ParticallyUpheld" <%=noChecked%> value="1" />
                            Partically Upheld
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="center" colspan="2">
                            Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                            &nbsp;
                            <% If(Len(enqID)>0) Then %>
                            Show to Tenant: &nbsp;<input type="checkbox" name="chk_SHOW_TENANT" id="chk_SHOW_TENANT"
                                value="1" checked="checked" />
                            &nbsp;
                            <% End If %>
                            <input type="button" value="View Letter" title="View Letter" class="RSLButton" onclick="open_letter()"
                                name="button" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style="border-bottom: 1px solid #133e71; border-left: 1px solid #133e71">
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    value=" Close " title="Close" class="RSLButton" style="cursor: pointer" />
                <input type="button" name="button2" id="button2" value="View Letter" title="View Letter"
                    class="RSLButton" onclick="open_letter()" style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" value=" Save "
                    title="Save" class="RSLButton" style="cursor: pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133E71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" style="display: none"
        height="4" width="3"></iframe>
</body>
</html>
