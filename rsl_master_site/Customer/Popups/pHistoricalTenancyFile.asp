<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim agreement_history_id	' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstTeams, lstAction, team , uploadstatus ,jid

	path = request.form("hid_go")
	agreement_history_id = Request("agreementhistoryid")

	' check to see whether page is coming back from uploader.vb after upload of file
	uploadstatus= request("hdupload")

	' begin processing
	Call OpenDB()

	If path = "" Then path = 0 End If ' initial entry to page

	If path = 0 Then
		Call get_record()
	End If

	Call CloseDB()

	Function get_record()

		Dim strSQL

		strSQL = 	"SELECT     JJ.JOURNALID,JJ.ITEMID, ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME,T.TEAMNAME, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, G.HISTORICALTENANCYFILEID, J.ITEMNATUREID, " &_
					"			S.DESCRIPTION AS STATUS " &_
					"FROM		C_HISTORICALTENANCYFILE G INNER JOIN C_JOURNAL JJ ON JJ.JOURNALID = G.JOURNALID " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
					"			LEFT JOIN dbo.E_TEAM T ON T.TEAMID=JD.TEAM " &_
					"WHERE 		HISTORICALTENANCYFILEID = " & agreement_history_id

		Call OpenRs(rsSet, strSQL)

		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		fullname	= rsSet("FULLNAME")
		team		= rsSet("TEAMNAME")
		item_id		= rsSet("ITEMID")
		itemnature_id = rsSet("ITEMNATUREID")
		journalID = rsSet("JOURNALID")
		jid = rsSet("JOURNALID")

		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <title>Update Enquiry</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_NOTES|Notes|TEXT|N"

        function save_form() {
            if (!checkForm()) return false;
            var result = checkFileUpload(RSLFORM, 'doc,docx,pdf');
            if (!result) return false;
            document.getElementById("btn_submit").disabled = "true";
            document.getElementById("btn_close").disabled = "true";
            document.getElementById("trMsg").style.display = "block";
            document.RSLFORM.target = "BOTTOM_FRAME";
            document.RSLFORM.action = "pHistoricalTenancyFile_srv.asp";
            document.RSLFORM.submit();
        }

        function redirect(jid) {
            parent.window.opener.location.href = "../iFrames/iHistoricalTenancyFileDetail.asp?journalid=" + jid + "&SyncTabs=1"
            parent.window.close();
        }

        function checkUploadStatus() {
            var uploadstatus = "<%=uploadstatus%>"
            // if page is coming from uploader.vb, then uploadstatus will have value of 1
            if (uploadstatus == '1') {
                redirect("<%=jid%>");
            }
        }

        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function Add_Document(agreementId) {
            document.RSLFORM.encoding = "multipart/form-data";
            document.RSLFORM.target = "BOTTOM_FRAME";
            document.getElementById("keycriteria").value = "HISTORICALTENANCYFILEID=" + agreementId;
            document.RSLFORM.action = "/DBFileUpload/uploader.aspx";
            document.RSLFORM.submit();
        }

        function getFileExtension(filePath) { //v1.0
            fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length) : filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length));
            return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
        }

        function checkFileUpload(form, extensions) { //v1.0
            document.MM_returnValue = true;
            if (extensions && extensions != '') {
                for (var i = 0; i < form.elements.length; i++) {
                    field = form.elements[i];
                    if (field.type.toUpperCase() != 'FILE') continue;
                    if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
                        ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                    }
                    if (field.value == '') {
                        ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                    }

                }
            }
            ManualError('img_DOCUMENTFILE', '', 3);
            return true;
        }
	
    </script>
</head>
<body onload="window.focus();checkUploadStatus();">
    <form name="RSLFORM" method="post" action="">
    <table width="379" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td height="10">
                <img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border="0" />
            </td>
            <td height="10">
                <img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border="0" />
            </td>
            <td width="302" style="border-bottom: 1px solid #133e71" align="center" class="RSLWhite">
                &nbsp;
            </td>
            <td width="67" style="border-bottom: 1px solid #133e71;">
                <img src="/myImages/spacer.gif" height="20" alt="" />
            </td>
        </tr>
        <tr>
            <td height="170" colspan="4" valign="top" style="border-left: 1px solid #133e71;
                border-bottom: 1px solid #133e71; border-right: 1px solid #133e71">
                <table>
                    <tr valign="top">
                        <td nowrap="nowrap">
                            Title:
                        </td>
                        <td width="100%">
                            <%=title%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Team :
                        </td>
                        <td>
                            <%=team%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Last Action By:
                        </td>
                        <td>
                            <%=fullname%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_RECORDEDBY" id="img_RECORDEDBY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <textarea style="overflow: hidden; width: 250px" class="textbox200" name="txt_NOTES"
                                id="txt_NOTES" rows="5" cols="10" onkeyup="countMe(clickHandler(event));"><%=notes%></textarea>
                        </td>
                        <td valign="top">
                            <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Upload
                        </td>
                        <td>
                            <input type="file" size="34" name="rslfile" id="rslfile" class="RSLButton" style="background-color: white;
                                color: black" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DOCUMENTFILE" id="img_DOCUMENTFILE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" name="hid_go" id="hid_go" value="0" />
                            <input type="hidden" name="agreementhistoryid" id="agreementhistoryid" value="<%=agreement_history_id%>" />
                            <!--Start of hidden fields for document upload tool-->
                            <input type="hidden" name="hid_Action" id="hid_Action" value="insert" />
                            <input type="hidden" name="keycriteria" id="keycriteria" value="" />
                            <input type="hidden" name="updatefields" id="updatefields" value="" />
                            <input type="hidden" name="filefield" id="filefield" value="DOCUMENTFILE" />
                            <input type="hidden" name="filename" id="filename" value="DOCFILE" />
                            <input type="hidden" name="tablename" id="tablename" value="C_HISTORICALTENANCYFILE" />
                            <input type="hidden" name="connectionstring" id="connectionstring" value="connRSL" />
                            <!--End of hidden fields for document upload tool-->
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" align="center" colspan="2">
                            Close Enquiry:&nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="trMsg" style="display: none">
                        <td colspan="3" align="center">
                            Document Upload in Process. Please Wait ...
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" style='border-bottom: 1px solid #133e71; border-left: 1px solid #133e71'>
                <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="button" name="btn_close" id="btn_close" onclick="javascript:window.close()"
                    title="Close" value=" Close " class="RSLButton" style="cursor: pointer" />
                <input type="button" name="btn_submit" id="btn_submit" onclick="save_form()" title="Save"
                    value=" Save " class="RSLButton" style="cursor: pointer" />
            </td>
            <td colspan="2" width="68" align="right">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="1" style="border-bottom: 1px solid #133E71">
                            <img src="/myImages/spacer.gif" width="1" height="68" alt="" />
                        </td>
                        <td>
                            <img src="/myImages/corner_pink_white_big.gif" width="67" height="69" alt="" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="BOTTOM_FRAME" id="BOTTOM_FRAME" style="display: none">
    </iframe>
</body>
</html>
