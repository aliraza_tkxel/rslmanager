<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<%
	Dim GENERAL_history_id		' the historical id of the repair record used to get the latest version of this record QUERYSTRING
	Dim nature_id				' determines the nature of this repair used to render page, either sickness or otherwise QUERYSTING
	Dim a_status				' status of repair reocrd from REQUEST.FORM - also used to UPDATE journal entry
	Dim lst_action				' action of repair reocrd from REQUEST.FORM
	Dim path					' determines whether pahe has been submitted from itself
	Dim fullname
	Dim notes, title, i_status, action, ActionString, lstUsers, lstStatus, lstTeams
			
	path = request.form("hid_go")
	
	GENERAL_history_id 	= Request("generalhistoryid")
	nature_id 			= Request("natureid")
	
	// begin processing
	OpenDB()
	
	If path  = "" then path = 0 end if ' initial entry to page
	
	If path = 0 Then
		get_record()
	Elseif path = 1 Then
		new_record()
	End If
	
	CLoseDB()
	
	Function get_record()
	
		Dim strSQL
		
		strSQL = 	"SELECT     ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			G.ITEMSTATUSID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, G.ASSIGNTO, jd.team, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, G.GENERALHISTORYID, " &_
					"			S.DESCRIPTION AS STATUS " &_					
					"FROM		C_GENERAL G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON J.JOURNALID = G.JOURNALID  " &_					
					"			LEFT JOIN E__EMPLOYEE E ON G.LASTACTIONUSER = E.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON G.ASSIGNTO = E2.EMPLOYEEID " &_										
					"			LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E2.EMPLOYEEID " &_															
					"WHERE 		GENERALHISTORYID = " & GENERAL_history_id
					
		'response.write strSQL
		Call OpenRs(rsSet, strSQL)
		
		i_status 	= rsSet("STATUS")
		title 		= rsSet("TITLE")
		notes		= rsSet("NOTES")
		fullname	= rsSet("FULLNAME")
		assignto	= rsSet("ASSIGNTO")
		d_status 	= rsSet("ITEMSTATUSID")		
		team		= rsSet("TEAM")				
			
		CloseRs(rsSet)

		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", team, NULL, "textbox200", " style='width:250px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & team & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"
		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", " style='width:250px' ")

		Call BuildSelect(lstStatus, "sel_STATUS", "C_STATUS WHERE ITEMID = 4 ", "ITEMSTATUSID, DESCRIPTION", "ITEMSTATUSID", "Please Select", d_status, NULL, "textbox200", " style='width:250px' ")
		
	End Function

	Function new_record()
	
		strSQL = "SELECT JOURNALID FROM C_GENERAL WHERE GENERALHISTORYID = " & GENERAL_history_id
	
		Call OpenRs(rsSet, strSQL)
		journalid = rsSet("JOURNALID")
		CloseRS(rsSet)
				
		New_Status = Request.Form("sel_STATUS")
		Call update_journal(journalid, New_Status)			
		
		strSQL = 	"INSERT INTO C_GENERAL " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, ASSIGNTO, NOTES) " &_
					"VALUES (" & journalid & ", " & New_Status & ", " & Session("userid") & ", " & Request.Form("sel_USERS") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "')"
		Conn.Execute(strSQL)

		Response.Redirect ("../iFrames/iRentDetail.asp?journalid=" & journalid & "&SyncTabs=1")
	End Function
	
	// updates the journal with the new status dpendent on the action taken
	Function update_journal(jid, j_status)
	
		strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & j_status & " WHERE JOURNALID = " & jid		
		set rsSet = Conn.Execute(strSQL)
	
	End Function

%>
<html>
<head>
<title>Update Enquiry</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
	FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[2] = "sel_STATUS|New Status|SELECT|Y"
	FormFields[3] = "sel_TEAMS|Team|SELECT|Y"		

	function save_form(){
		if (!checkForm()) return false
		RSLFORM.hid_go.value = 1;
		RSLFORM.action = ""		
		RSLFORM.target = "CRM_BOTTOM_FRAME"		
		RSLFORM.submit();
		window.close()
	}

	function GetEmployees(){
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.action = "../Serverside/GetEmployees.asp?SMALL=1"
			RSLFORM.target = "ServerIFrame"
			RSLFORM.submit()
			}
		else {
			RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' class='textbox200' STYLE='WIDTH:250PX'><option value=''>Please select a team</option></select>"
			}
		}

</script>	
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="window.focus()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
      <td  height=10><img src="/Customer/Images/1-open.gif" width="92" height="20" alt="" border=0 /></td>
      <td  height=10><img src="/Customer/Images/TabEnd.gif" width="8" height="20" alt="" border=0 /></td>	  
	  <td width=302 style='border-bottom:1px solid #133e71' align=center class='RSLWhite'>&nbsp;</td>
	<td width=67 style='border-bottom:1px solid #133e71;'><img src="/myImages/spacer.gif" height=20 /></td>
  </tr>
  <tr>
  	  <td height=170 colspan=4 valign=top style='border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71'>
<table>
		<tr valign=top>
			<td nowrap>Title:</td>
			<td width=100%><%=title%></td>
		</tr>
		<tr>
			<td>Team : </td>
			<td><%=lstTeams%></td>
			<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Assign To : </td>
			<td><%=lstUsers%></td>
			<TD><image src="/js/FVS.gif" name="img_USERS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
			<TD nowrap>New Status:</td>
			<TD><%=lstStatus%></TD>
			<TD><image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD nowrap>Curr. Status:</td>
			<TD><input type="text" class="textbox200"  STYLE=';WIDTH:250' name="txt_STATUS" value="<%=i_status%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_SSSTATUS" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD nowrap>Last Action By:</td>
			<TD><input type="text" class="textbox200" STYLE=';WIDTH:250' name="txt_RECORDEDBY" value="<%=fullname%>" READONLY></TD>
			<TD><image src="/js/FVS.gif" name="img_RECORDEDBY" width="15px" height="15px" border="0"></TD>
		</TR>		
		<TR>
			<TD valign=top>Notes:</TD>
			<TD><textarea style='OVERFLOW:HIDDEN;WIDTH:250' class="textbox200" name="txt_NOTES" rows=5><%=notes%></textarea></TD>	
			<TD valign=top><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
				<input type="hidden" name="hid_go" value=0>
				<input type="hidden" name="PreviousAction" value="<%=action%>">
				<input type="hidden" name="GENERAL_history_id" value="<%=GENERAL_history_id%>">				
			</TD>
		</TR>   
	</table>	  
	   
      </td>
  </tr>
  <tr> 
	  <td colspan=3 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'> 
        <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
        <input type=BUTTON name='btn_close' onClick="javascript:window.close()" value = ' Close ' class="RSLButton" >
        <input type=BUTTON name='btn_submit' onClick='save_form()' value = ' Save ' class="RSLButton" >
        </td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp"  name="ServerIFrame" style='display:none'></iframe>
</BODY>
</HTML>

