<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim tenancy_id, property_id, total_rent, balance

	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	balance = Request("balance")

	Call OpenDB()

	' GET PROPERTY RENT
	SQL = "SELECT T.PROPERTYID, F.TOTALRENT FROM C_TENANCY T INNER JOIN P_FINANCIAL F ON T.PROPERTYID = F.PROPERTYID WHERE T.TENANCYID = " & tenancy_id
	Call OpenRs(rsSet, SQL)
	If NOT rsSet.EOF Then
		property_id = rsSet(0)
		total_rent = rsSet(1)
	Else
		property_id = "Not Found"
		total_rent = 0
	End If

	' GET ANY ADDITIOANL ASSETS RENT
	SQL = "SELECT 	ISNULL(SUM(F.TOTALRENT),0)  AS ADDRENT " &_
		  "FROM 	C_ADDITIONALASSET AD " &_
		  "			INNER JOIN P_FINANCIAL F ON AD.PROPERTYID = F.PROPERTYID " &_
		"	WHERE TENANCYID = " & tenancy_id & "  AND AD.ENDDATE IS NULL"
	Call OpenRs(rsSet, SQL)
	If NOT rsSet.EOF Then total_rent = total_rent + rsSet(0) End If

	Call CloseRs(rsSet)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Forecast Tenancy End</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()
        FormFields[0] = "txt_ENDDATE|End Date|DATE|Y"

        function SaveForm() {
            if (!checkForm()) return false
            document.RSLFORM.target = "frm_forecast_srv"
            document.RSLFORM.action = "../serverside/forecast_srv.asp"
            document.RSLFORM.submit()
        }
	
    </script>
</head>
<body>
    <form name="RSLFORM" method="post" action="">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="79">
                            <img src="../images/forecast.gif" width="162" height="20" alt="" /></td>
                        <td height="19"></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="../images/spacer.gif" height="1" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71">
                <br />
                <table cellspacing="1" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center" border="0">
                    <tr>
                        <td>
                            Tenancyid
                        </td>
                        <td>
                            <input type="text" name="txt_TENANCY" id="txt_TENANCY" class="TEXTBOX200" value="<%=tenancy_id%>"
                                readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_TENANCY" id="img_TENANCY" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Propertyid
                        </td>
                        <td>
                            <input type="text" name="txt_PROPERTYID" id="txt_PROPERTYID" class="TEXTBOX200" value="<%=property_id%>"
                                readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_PROPERTYID" id="img_PROPERTYID" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Monthly Rent
                        </td>
                        <td>
                            <input type="text" name="txt_RENT" id="txt_RENT" class="TEXTBOX200" value="<%=total_rent%>" readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_RENT" id="img_RENT" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Current Balance
                        </td>
                        <td>
                            <input type="text" name="txt_CURRENTBALANCE" id="txt_CURRENTBALANCE" class="TEXTBOX200" value="<%=balance%>" readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_CURRENTBALANCE" id="img_CURRENTBALANCE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Forecast End date
                        </td>
                        <td>
                            <input type="text" name="txt_ENDDATE" id="txt_ENDDATE" class="TEXTBOX200" maxlength="99" />
                            <img src="/js/FVS.gif" name="img_ENDDATE" id="img_ENDDATE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr style="height: 10px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Part Month Payment
                        </td>
                        <td>
                            <input style="background-color: #f5f5dc" type="text" name="txt_FORECAST" id="txt_FORECAST" class="TEXTBOX200"
                                readonly="readonly" />
                            <img src="/js/FVS.gif" name="img_PARTMONTH" id="img_PARTMONTH" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ending Balance
                        </td>
                        <td>
                            <input style="background-color: #f5f5dc" type="text" name="txt_ENDINGBALANCE" id="txt_ENDINGBALANCE" class="TEXTBOX200"
                                readonly="readonly" />
                        </td>
                    </tr>
                </table>
                <br />
                <table cellspacing="2" cellpadding="2" style="border: 1px solid #133E71" width="90%"
                    align="center" border="0">
                    <tr>
                        <td colspan="2" align="center">
                            <input type="button" name="btn_POST" class="RSLBUTTON" value=" Forecast " title="Forecast" onclick="SaveForm()" />
                            <input type="button" name="btn_CANCEL" class="RSLBUTTON" value="Close" title="Close" onclick="window.close()" />
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
                <input type="hidden" name="hid_BALANCE" id="hid_BALANCE" value="<%=balance%>" />
                <br />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="frm_forecast_srv" id="frm_forecast_srv" width="1px" height="1px" style="display: none"></iframe>
</body>
</html>
