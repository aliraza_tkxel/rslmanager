<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer --> Tools --> Customer Set Up</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()

        function CheckCustomer() {
            if (document.getElementById("txt_NINUMBER").value != "") {
                FormFields[0] = "txt_FIRSTNAME|First Name(s)|TEXT|N"
                FormFields[1] = "txt_LASTNAME|Last Name|TEXT|N"
                FormFields[2] = "txt_DOB|Date of Birth|DATE|N"
                FormFields[3] = "txt_NINUMBER|National Insurance Number|TEXT|Y"
            }
            else {
                FormFields[0] = "txt_FIRSTNAME|First Name(s)|TEXT|Y"
                FormFields[1] = "txt_LASTNAME|Last Name|TEXT|Y"
                FormFields[2] = "txt_DOB|Date of Birth|DATE|N"
                FormFields[3] = "txt_NINUMBER|National Insurance Number|TEXT|N"
            }
            if (!checkForm()) return false
            document.RSLFORM.target = "Customer_Match"
            document.RSLFORM.action = "ServerSide/CustomerCheck.asp"
            document.RSLFORM.submit()
        }

        function JumpPage(PageQueryString) {
            iPage = document.getElementById("QuickJumpPage").value
            if (iPage != "" && !isNaN(iPage)) {
                document.RSLFORM.action = "ServerSide/DisplayMatch.asp?" + PageQueryString + "page=" + iPage
                document.RSLFORM.target = "Customer_Match"
                document.RSLFORM.submit()
            }
            else
                document.getElementById("QuickJumpPage").value = ""
        }

        function Proceed(CustomerID, PageQueryString) {
            document.RSLFORM.target = ""
            document.RSLFORM.action = "Customer.asp?" + PageQueryString + "CustomerID=" + CustomerID
            document.RSLFORM.submit()
        }

        function ProceedPrevious(CustomerID) {
            document.RSLFORM.target = ""
            document.RSLFORM.action = "CRM.asp?CustomerID=" + CustomerID
            document.RSLFORM.submit()
        }
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table>
        <tr>
            <td valign="top">
                <form name="RSLFORM" method="post" action="serverside/CustomerCheck.asp">
                <table width="370" border="0" cellpadding="0" cellspacing="0" style="height: 26px">
                    <tr>
                        <td rowspan="2">
                            <img src="Images/tab_ent_customer.gif" width="170" height="20" border="0" alt="" /></td>
                        <td>
                            <img src="images/spacer.gif" width="200" height="19" border="0" alt="" /></td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="images/spacer.gif" width="200" height="1" border="0" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                            <img src="images/spacer.gif" width="53" height="6" border="0" alt="" /></td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="2" style="height: 208px; border-left: 1px solid #133E71;
                    border-bottom: 1px solid #133E71; border-right: 1px solid #133E71" width="370">
                    <tr>
                        <td nowrap="nowrap" width="130px">
                            First Name(s) :
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_FIRSTNAME" id="txt_FIRSTNAME" maxlength="100" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME" id="img_FIRSTNAME"
                                alt="" />
                        </td>
                        <td width="100%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name :
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="100" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_LASTNAME" id="img_LASTNAME"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Date of Birth&nbsp;:&nbsp;
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DOB" id="txt_DOB" maxlength="10" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_DOB" id="img_DOB" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td title="National Insurance Number">
                            NI No.:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_NINUMBER" id="txt_NINUMBER" maxlength="20" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" width="15" height="15" name="img_NINUMBER" id="img_NINUMBER"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <input type="button" class="RSLButton" value=" Enter " id="BtnEnter" name="BtnEnter"
                                title="Enter" onclick="CheckCustomer()" style="cursor: pointer" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <br />
                            To search for a customer you must enter at least the
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            customer '<font color="red">firstname and lastname</font>' or the customer '<font
                                color="red">NI No.</font>'
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            You can complete other fields to get a more accurate match
                        </td>
                    </tr>
                    <tr>
                        <td height="100%">
                        </td>
                    </tr>
                </table>
            </td>
            </form>
            <td width="10px">
            </td>
            <td valign="top">
              <form name="MATCHFORM" method="post" action="serverside/CustomerCheck.asp">
                <table width="370" border="0" cellpadding="0" cellspacing="0" style="height: 20px">
                    <tr>
                        <td>
                            <img src="Images/tab_matches.gif" width="85" height="20" alt="" /></td>
                        <td>
                            <img src="images/spacer.gif" width="285" height="19" alt="" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#133E71">
                            <img src="images/spacer.gif" width="370" height="1" alt="" /></td>
                    </tr>
                </table>                
                <table cellspacing="0" cellpadding="0" style="height: 228px; border-left: 1px solid #133E71;
                    border-bottom: 1px solid #133E71; border-right: 1px solid #133E71" width="370">
                    <tr>
                        <td nowrap="nowrap" valign="top">
                            <div id="ResultsDiv">
                                <table cellpadding="3" cellspacing="0" style="border-collapse: collapse">
                                    <tr>
                                        <td>
                                            <br />
                                            Use the customer search on the left.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            If the customer already exists in the system then the customer
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            record will appear in this area.
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="Customer_Match" width="400px" height="400px"
        style="display: none"></iframe>
</body>
</html>
