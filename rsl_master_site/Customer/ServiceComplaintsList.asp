<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 15
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TheArrearsTotal
	Dim ochecked, cchecked
	
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ServiceComplaintsList.asp"
	MaxRowSpan = 8
	
	
	OpenDB()
	
	get_ASB()
	
	CloseDB()

	Function get_ASB()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Filter & Ordering Section For SQL
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Filter Section
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  Start to Build arrears list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Dim strSQL, rsSet, intRecord 
	
			intRecord = 0
			str_data = ""
			strSQL = 		"	SELECT  C.CUSTOMERID,J.CREATIONDATE, J.CUSTOMERID, " &_
							"		LTRIM(ISNULL(TIT.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS CNAME, " &_
							"		P.HOUSENUMBER + ' ' + P.ADDRESS1 AS ADDRESS, G.NOTES,  " &_
							"		LTRIM(ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,' ')) AS ENAME,  " &_
							"		G.NOTES ,ST.DESCRIPTION AS ITEMSTATUS " &_
							"	FROM 	C_JOURNAL J " &_
							"		INNER JOIN  (  " &_
							"			SELECT MAX(COMPLAINTHISTORYID) AS MAXHIST, JOURNALID  " &_
							"			FROM   C_SERVICECOMPLAINT  " &_
							"			GROUP BY JOURNALID " &_
							"		   ) MH ON MH.JOURNALID = J.JOURNALID " &_
							"		INNER JOIN C_SERVICECOMPLAINT G ON G.COMPLAINTHISTORYID = MH.MAXHIST " &_
							"		INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
							"		INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C.CUSTOMERID " &_
							"		INNER JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
							"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  " &_
							"		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
							"		LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " &_
							"		LEFT JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE " &_
							"		LEFT JOIN C_STATUS ST ON ST.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
							"		LEFT JOIN C_ESCALATIONLEVEL ESC ON G.ESCALATIONLEVEL = ESC.ESCALATIONID " &_
							"	WHERE	J.CURRENTITEMSTATUSID in (13,14)  " &_
							"	ORDER BY J.CREATIONDATE DESC "


			'rw strSQL
			
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				PrevTenancy = ""
				For intRecord = 1 to rsSet.PageSize
					
					strStart = 	"<TR><TD>" & rsSet("CREATIONDATE") & "</TD>" 
					strEnd = 	"<TD>" & rsSet("CUSTOMERID") & "</TD>" &_
								"<TD>" & rsSet("CNAME") & "</TD>" &_
								"<TD>" & rsSet("ADDRESS") & "</TD>" &_	
								"<TD>" & rsSet("NOTES") & "</TD>" &_												
								"<TD >" & rsSet("ITEMSTATUS") & "</TD>" &_
								"<TD>" & rsSet("ENAME") & "</TD></TR>"
					str_data = str_data & strStart & strEnd
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				str_data = str_data & "</TBODY>"
				
				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If
	
			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
				fill_gaps()						
			End If
			
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		Function WriteJavaJumpFunction()
			JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
			JavaJump = JavaJump & "<!--" & VbCrLf
			JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
			JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
			JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
			JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
			JavaJump = JavaJump & "else" & VbCrLf
			JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
			JavaJump = JavaJump & "}" & VbCrLf
			JavaJump = JavaJump & "-->" & VbCrLf
			JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
			WriteJavaJumpFunction = JavaJump
		End Function
		
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	'  End Build list 
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function load_me(CustomerID){
		location.href = "CRM.asp?CustomerID=" + CustomerID
	}

	var FormFields = new Array()
	FormFields[0] = "txt_VALUE|Compare Value|CURRENCY|Y"
	FormFields[1] = "txt_ASATDATE|Date - As At|DATE|N"

	function GO(){

		if (!checkForm()) return false;
		if (thisForm.txt_VALUE.value < 0 ){
			alert("The Min. Arrears value must not a negative number.");
			return false;
			}
		location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value
		}
		

	/////////////////////////////////
	//	Printing and Xls Below
	/////////////////////////////////
	
	function PrintMe(){
	
		window.open("PrintMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value)//, "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
		
		}
	
	function XLSMe(){
		//alert("unavailable at this time")
		//return false
		window.open("XLSMe.asp?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value + "&WHICH=" + thisForm.WHICH.value + "&sel_Scheme=" + thisForm.sel_SCHEME.value + "&sel_Action=" + thisForm.sel_ACTION.value+ "&txt_ASATDATE=" + thisForm.txt_ASATDATE.value);
		}
	
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD align=right></TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom">&nbsp;</TD>
    </TR>
    <TR>
      <TD  height=1 valign="bottom"><strong>Service Complaints  List </strong></TD>
    </TR>
    <TR> 
      <TD  height=1 valign="bottom"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td BGCOLOR=#133E71 height=1><IMG SRC="images/spacer.gif" WIDTH=672 HEIGHT=1></td>
          </tr>
        </table>
      </TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=83>&nbsp;<strong>Created</strong></TD>
      <TD CLASS='NO-BORDER' WIDTH=45><B>Cust No</B></TD>
      <TD CLASS='NO-BORDER' WIDTH=105><B>Customer Name</B></TD>
      <TD CLASS='NO-BORDER' WIDTH=116><B>Address</B></TD>
      <TD align="left" CLASS='NO-BORDER'><strong>Notes</strong></TD>
	  <TD align="left" CLASS='NO-BORDER'><strong>Status</strong></TD>
      <TD WIDTH=70 CLASS='NO-BORDER'><strong>Employee</strong></TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=8 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>