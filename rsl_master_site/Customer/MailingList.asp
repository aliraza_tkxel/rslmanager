<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL
	Dim PageName

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	PageName = "MailingList.asp"
	MaxRowSpan = 8
	Dim orderBy
		orderBy = "C.LASTNAME ASC, C.FIRSTNAME ASC"
		If (Request("CC_Sort") <> "") Then orderBy = Request("CC_Sort")

	Call OpenDB()
	Dim CompareValue

	Call get_newtenants()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

		CompareValue = 300.00
		If (Request("COMPAREVALUE") <> "") Then
			CompareValue = Request("COMPAREVALUE")
		End If

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = "SELECT T.TENANCYID, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_
					"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
					"WHERE T.ENDDATE IS NULL AND CT.CUSTOMERID = (SELECT MIN(CUSTOMERID) FROM C_CUSTOMERTENANCY WHERE TENANCYID = T.TENANCYID) " &_
						"ORDER BY " & orderBy
		'REMOVED MIN CLAUSE
		'  ADDED ADDRESS TABLE/JOIN
		'  ADDED ADDRESS FILTER AS PER DH'S DI REPORT
		'UPDATED TENANCY FILTER AS PER DH'S DI REPORT
		strSQL = "SELECT T.TENANCYID, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY, DP.LOCATION AS PATCH, LA.DESCRIPTION  AS LOCALAUTHORITY FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN C_ADDRESS A ON A.CUSTOMERID = C.CUSTOMERID " &_
					"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_
					"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
					"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID=P.DEVELOPMENTID " &_
					"LEFT JOIN E_PATCH DP ON DP.PATCHID=D.PATCHID " &_
					"LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID=C.LOCALAUTHORITY " &_
					"WHERE ((T.ENDDATE IS NULL) OR (T.ENDDATE > GETDATE())) AND CT.CUSTOMERID = (SELECT MIN(CUSTOMERID) FROM C_CUSTOMERTENANCY WHERE TENANCYID = T.TENANCYID) AND A.ISDEFAULT = '1' AND A.ADDRESSTYPE = '3' AND C.CUSTOMERTYPE = '2' " &_
					"ORDER BY " & orderBy

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class=""caps"">"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			PrevTenancy = ""
			For intRecord = 1 to rsSet.PageSize
				TenancyID = rsSet("TENANCYID")
				CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
						"FROM C_CUSTOMERTENANCY CT " &_
						"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
						"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_
						"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & TenancyID & " ORDER BY CT.CUSTOMERID ASC"
				Call OpenRs(rsCust, CustSQL)
				strUsers = ""
				CustCount = 0
				while NOT rsCust.EOF 
					If CustCount = 0 Then
						strUsers = strUsers & "<a href='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
						CustCount = 1
					Else
						strUsers = strUsers & " & " & "<a href='CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
					End If
					rsCust.moveNext()
				Wend
				Call CloseRs(rsCust)
				Set rsCust = Nothing

				strEnd = "<td>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & "</td>" &_
					"<td>" & rsSet("TOWNCITY") & "</td>" &_
					"<td>" & rsSet("POSTCODE") & "</td>" &_
					"<td>" & rsSet("COUNTY") & "</td>" &_
					"<td>" & rsSet("PATCH") & "</td>" &_
					"<td>" & rsSet("LOCALAUTHORITY") & "</td></tr>"

				str_data = str_data & "<tr><td width=""190"">" & strUsers & "</td>" & strEnd

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<table cellspacing=""0"" cellpadding=""0"" width=""100%""><thead><tr><td width=""100""></td><td align=""center"">"  &_
			"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=""blue"">First</font></b></a> "  &_
			"<a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>"  &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
			" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>"  &_ 
			" <a href='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>"  &_
			"</td><td align=""right"" width=""100"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value='' size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"" />&nbsp;"  &_
			"<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" style=""font-size:10px"" />"  &_
			"</td></tr></thead></table></td></tr></tfoot>"
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then
			str_data = "<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""font:18px"" align=""center"">No new tenancies exist for the specified criteria</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<script type=""text/javascript"" langauge=""JavaScript"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</script>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""" & MaxRowSpan & """ align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customers --> Mailing List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

function load_me(CustomerID){
	location.href = "CRM.asp?CustomerID=" + CustomerID
	}

var FormFields = new Array()
FormFields[0] = "txt_VALUE|Compare Value|CURRENCY|Y"

function GO(){
	if (!checkForm()) return false;
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>" + thisForm.txt_VALUE.value
	}

var TOPLOADER;
var LoaderStatus_Top = "";
var LOADINGSIZE_TOP = 0
var LOADING_TEXT_DOTS = new Array(".", "..", "...", "....", ".....")

function STOPLOADER(which){
	try {
		clearTimeout(TOPLOADER);
		LoaderStatus_Top = "";
		LOADINGTEXT_TOP.innerHTML = "File Build Complete";
		TOP_DIV_LOADER.style.display = "none"
		TOP_DIV.style.display = "block"
		}
	catch (e) {
		temp = 1
		}
	}

function STARTLOADER(which) {
	clearTimeout(TOPLOADER);
	LoaderStatus_Top = "working"
	CALLLOADER("TOP")
	}

function CALLLOADER(which) {
	if (LoaderStatus_Top == "working"){
		LOADINGSIZE_TOP++;
		if (LOADINGSIZE_TOP == 5) LOADINGSIZE_TOP = 0;
		LOADINGTEXT_TOP.innerHTML = "Building File" + LOADING_TEXT_DOTS[LOADINGSIZE_TOP] + "";
		TOPLOADER = setTimeout("CALLLOADER('TOP')",300);
		}
	else 
		LOADINGTEXT_TOP.innerHTML = "";
	}

function BuildMailingList(){
	STARTLOADER()
	ServerMailFrame.location.href = "MailingList_EXPORT.asp"
	}

    </script>
    <%=WriteJavaJumpFunction%>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="tab_Tenants" title='Tenants' src="Images/tab_mailinglist.gif" width="97"
                    height="20" border="0" alt="" /></td>
            <td align="right" nowrap="nowrap">
                The mailing list brings back all current tenants. To create a <a href='#' onclick="BuildMailingList()">
                    <font color="blue">Mailing List</font></a> click on the link.</td>
            <td width="150px" nowrap="nowrap"><div id="LOADINGTEXT_TOP" style="color: blue"></div></td>
        </tr>
        <tr>
            <td colspan="2" height="1" style="border-bottom: 1px solid #133e71">
                <img src="images/spacer.gif" width="653" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="#4682b4" border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' width="160px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("C.LASTNAME asc, C.FIRSTNAME asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Customer</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("C.LASTNAME desc, C.FIRSTNAME desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="130px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.ADDRESS1 asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Address1</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.ADDRESS1 desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="120px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.TOWNCITY asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Town/City</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.TOWNCITY desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="90px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.POSTCODE asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Postcode</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.POSTCODE desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="90px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.COUNTY asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>County</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("P.COUNTY desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="70px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("PATCH asc")%>" style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Patch</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("PATCH desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
                <td class='NO-BORDER' width="90px">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("LOCALAUTHORITY asc")%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17"
                            height="16" /></a>&nbsp;<b>Local Authority</b>&nbsp;<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("LOCALAUTHORITY desc")%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending"
                                    border="0" width="11" height="12" /></a>
                </td>
            </tr>
        </thead>
        <tr style="height: 3px">
            <td colspan="8" align="center" style="border-bottom: 2px solid #133e71">
            </td>
        </tr>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="ServerMailFrame" id="ServerMailFrame" width="4px"
        height="4px" style="display: none"></iframe>
</body>
</html>
