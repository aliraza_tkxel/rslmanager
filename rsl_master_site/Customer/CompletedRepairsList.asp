<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--''''''''''''
'' PAGE NOTE: This is part of the repair approval tool that shows all the repairs that have been completed and 
'' awaiting approval.
''
'' NOTE: Pages to be aware of: CompletedRepairsList.asp 
''							   CompletedRepairs_srv.asp 
''						       process_CompletedRepairslist_srv.asp
''''''''''''
-->

<!--#include virtual="ACCESSCHECK.asp" -->
<%

	OpenDB()
		SQL = " SELECT  DISTINCT R.CONTRACTORID, " &_
			" O.NAME AS ContractorName " &_
		" FROM C_JOURNAL J  " &_
			" INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " &_
			" LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
			" INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
			" INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " &_
		" WHERE J.ITEMID = 1  " &_
				" AND J.CURRENTITEMSTATUSID = 7  " &_
				" AND J.ITEMNATUREID IN (2,22,20,21)  " &_
				" AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " 
	Call OpenRs(rsContractor, SQL)
	
	'Build Select Box to search for a particular contractor
	ListBox = "<select name='WhatContractor' class='textbox200'><option value=''>Select a Contractor</option>"
	while  not rsContractor.eof
	
		ListBox = ListBox & "<option value='" & rsContractor("ContractorID") & "'>" & rsContractor("ContractorName") & "</option>"
	
	rsContractor.movenext
	
	Wend
	ListBox = ListBox & "</select>"
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
	FormFields[1] = "sel_OFFICE|Local Office|SELECT|N"
	FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|N"
	FormFields[3] = "txt_TO|To Date|DATE|Y"
		
	function process(){

		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/process_CompletedRepairsList_srv.asp?";
		RSLFORM.submit();

	}

	
	function GetList(){
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/CompletedRepairs_srv.asp";
		RSLFORM.submit();
	}
	
	function quick_find(){
	
	RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/CompletedRepairs_srv.asp";
		RSLFORM.submit();
	}	
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("idlist").value = str_idlist;
		
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	function open_me(WorkOrder_id){
		event.cancelBubble = true;
		//alert("../Finance/Popups/Purchaseorder_jim.asp?WORKOrderID=" + WorkOrder_id )
		window.showModelessDialog("../Finance/Popups/Purchaseorder_jim.asp?WorkOrderID=" + WorkOrder_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;");
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post>

	
  <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
    <TR>
      <TD>Select a <b>Works Completed</b> Repair Item to deem approved. Select 
        the checkbox and then click the '<b>Approve Repair</b>' button.</TD>
    </TR>
    <TR> 
      <TD align="right">
	  <input name="idlist" id="idlist" value="" TYPE="HIDDEN">
        <input type='BUTTON' name='btn_FIND' title='Search for matches using Supplier Name.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'>
        &nbsp;
        <%=ListBox%>
      </TD>
    </TR>
  </TABLE>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp"  name=frm_slip width=400px height=400px style='display:NONE'></iframe>
</BODY>
</HTML>

