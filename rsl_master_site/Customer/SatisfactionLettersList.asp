<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->

<%

	OpenDB()
		SQL = " SELECT DISTINCT R.CONTRACTORID, O.NAME AS ContractorName " &_
				" from C_SATISFACTIONLETTER SL  " &_
				"  	INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID  " &_
				" 	INNER JOIN C_REPAIR R ON R.JOURNALID = SL.JOURNALID  " &_
				" 	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
				" 	INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				" 	INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID " &_
				" 	INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID " &_
				" 	INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID " &_
				" WHERE SL.SATISFACTION_LETTERSTATUS = 1 AND R.ITEMACTIONID IN (5,6,10,15)" 
	Call OpenRs(rsContractor, SQL)
	
	'Build Select Box to search for a particular contractor
	ListBox = "<select name='WhatContractor' class='textbox200'><option value=''>Any Contractor</option>"
	while  not rsContractor.eof
		ListBox = ListBox & "<option value='" & rsContractor("ContractorID") & "'>" & rsContractor("ContractorName") & "</option>"
	
	rsContractor.movenext
	
	Wend
	ListBox = ListBox & "</select>"
	Call CloseRs(rsContractor)	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	FormFields[0] = "txt_ORDERNO|Order No.|INTEGER|N"
	
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();



	function process(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/process_SatisfactionLetterList_srv.asp?";
		RSLFORM.submit();
	}

	function GetList(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/SatisfactionLetterList_srv.asp";
		RSLFORM.submit();
	}
	
	function quick_find(){
	
		if (!checkForm()) return false
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/SatisfactionLetterList_srv.asp";
		RSLFORM.submit();
	}	
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("idlist").value = str_idlist;
		
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	function open_me(WorkOrder_id){
		event.cancelBubble = true;
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post>

	
  <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
    <TR>
      <TD>Select a Repair Item to send a 'Satisfaction Survey Letter' to. Select 
        the checkbox and then click the '<b>Send Survey</b>' button.</TD>
    </TR>
    <TR> 
      <TD align="right">
	  <input name="idlist" id="idlist" value="" TYPE="HIDDEN">
        Order No.&nbsp;
        <input name="txt_ORDERNO" ID="txt_ORDERNO"  type="text" class="textbox100" OnClick="this.value = ''">
		<img src="/js/FVS.gif" width="15" height="15" name="img_ORDERNO">
        <%=ListBox%>
        <input type='BUTTON' name='btn_FIND' title='Search for matches using Supplier Name.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'>
      </TD>
    </TR>
  </TABLE>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp"  name=frm_slip width=1px height=1px style='display:block'></iframe>
</BODY>
</HTML>

