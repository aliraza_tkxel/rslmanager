<%
'Response.Buffer = True
'Response.ContentType = "application/vnd.ms-excel"
Response.Buffer = True
Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_export.asp" -->
<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include 
	'		sometimes kills the xls transfer

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count
		Dim theURL	
		Dim PageName
		Dim TheUser		 	' The Housing Officer ID - default is the session
		Dim TheArrearsTotal
		Dim ochecked, cchecked
		Dim CompareValue
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    'Get Record to display on xsl
	     OpenDB()
        str_data = ""
			 
		strSQL= " SELECT CT.TENANCYID AS TENANCY_REF,GT.DESCRIPTION+' '+           "&_
                "    CASE ISNULL(CC.FIRSTNAME,'EMPTY')                             "&_
	            "        WHEN 'EMPTY' THEN ''                                      "&_  
	            "         ELSE CC.FIRSTNAME+','                                    "&_  
                "    END                                                           "&_  
                " +  CASE ISNULL(CC.LASTNAME,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_
                "       ELSE CC.LASTNAME                                           "&_ 
                "    END                                                           "&_ 
                "    AS CUSTOMER_NAME,                                             "&_ 
                "    SCH.SCHEMENAME AS SCHEMENAME,REPLACE(                          "&_ 
                "        CASE ISNULL(PP.FLATNUMBER,'EMPTY')                        "&_ 
	            "           WHEN 'EMPTY' THEN ''                                   "&_ 
	            "           ELSE PP.FLATNUMBER+','                                 "&_ 
                "        END                                                       "&_ 
                "  + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                           "&_ 
                "         WHEN 'EMPTY' THEN ''                                     "&_ 
	            "         ELSE PP.HOUSENUMBER+','                                  "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS1,'EMPTY')                              "&_ 
	            "         WHEN 'EMPTY' THEN ''                                     "&_ 
	            "         ELSE PP.ADDRESS1+','                                     "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS2,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.ADDRESS2+','                                       "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS3,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.ADDRESS3+','                                       "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.TOWNCITY,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.TOWNCITY+','                                       "&_ 
                "    END                                                           "&_     
                "  + CASE ISNULL(PP.COUNTY,'EMPTY')                                "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.COUNTY+','                                         "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.POSTCODE,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.POSTCODE                                           "&_ 
                "    END,',,',',')                                                 "&_ 
                "    AS FULL_PROPERTY_ADDRESS,CAT.DESCRIPTION                    "&_ 
                "    AS CONTACT_TYPE,REPLACE(CD.HOUSENUMBER+','+CD.ADDRESS1+     "&_ 
                "    CASE ISNULL(CD.ADDRESS2,'EMPTY')                              "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.ADDRESS2+','                                       "&_
                "    END                                                           "&_
                " + CASE ISNULL(CD.ADDRESS3,'EMPTY')                               "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.ADDRESS3+','                                       "&_
                "    END                                                           "&_
                " + CASE ISNULL(CD.TOWNCITY,'EMPTY')                               "&_
	            "        WHEN 'EMPTY' THEN  ''                                     "&_
	            "        ELSE CD.TOWNCITY+','                                      "&_
                "   END                                                            "&_
                " + CASE ISNULL(CD.COUNTY,'EMPTY')                                 "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.COUNTY+','	                                       "&_
                "   END                                                            "&_
                " + CASE ISNULL(CD.POSTCODE,'EMPTY')                               "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.POSTCODE                                           "&_
                "    END,',,',',')                                                 "&_                
                "   AS FULL_CONTACT_ADDRESS,                                       "&_
                " REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CC.COMMUNICATION,'9,',''),'8,',''),'5','Large Print'),'7','Other'),'6','Induction Loop'),'4','Audio Cassette'),'3','Braille'),'2','Sign Language'),'1','Translation'),'8','')  "&_
                " AS COMMUNICATION_NEED,CC.COMMUNICATIONOTHER AS COMMUNICATION_OTHER "&_
                " FROM C_CUSTOMERTENANCY CCT                                       "&_
                " INNER JOIN C_TENANCY CT ON CCT.TENANCYID=CT.TENANCYID            "&_
                " INNER JOIN C__CUSTOMER CC ON CCT.CUSTOMERID=CC.CUSTOMERID        "&_
                " INNER JOIN G_TITLE GT ON GT.TITLEID=CC.TITLE                     "&_
                " INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=CT.PROPERTYID         "&_               
                " INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID "&_
                " LEFT JOIN P_BLOCK BL ON BL.BLOCKID = PP.BLOCKID  "&_ 
                " INNER JOIN P_SCHEME SCH ON PP.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
                " INNER JOIN C_ADDRESS CD ON CD.CUSTOMERID=CC.CUSTOMERID           "&_ 
                " INNER JOIN C_ADDRESSTYPE CAT ON CAT.ADDRESSTYPEID=CD.ADDRESSTYPE "&_
                " INNER JOIN dbo.C_CUSTOMER_COMMUNICATION CCC ON CCC.CUSTOMERID=CC.CUSTOMERID "&_
                " WHERE (REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CC.COMMUNICATION,'9,',''),'8,',''),'5','Large Print'),'7','Other'),'6','Induction Loop'),'4','Audio Cassette'),'3','Braille'),'2','Sign Language'),'1','Translation'),'8','')<> '' "&_ 
                " OR (PP.POSTCODE<>CD.POSTCODE AND CAT.ADDRESSTYPEID <>3 ))  "&_
                " AND CD.ISDEFAULT<>0 "&_
		" order by CT.TENANCYID "
		
				strSQL= " SELECT CT.TENANCYID AS TENANCY_REF,GT.DESCRIPTION+' '+           "&_
                "    CASE ISNULL(CC.FIRSTNAME,'EMPTY')                             "&_
	            "        WHEN 'EMPTY' THEN ''                                      "&_  
	            "         ELSE CC.FIRSTNAME+','                                    "&_  
                "    END                                                           "&_  
                " +  CASE ISNULL(CC.LASTNAME,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_
                "       ELSE CC.LASTNAME                                           "&_ 
                "    END                                                           "&_ 
                "    AS CUSTOMER_NAME,                                             "&_ 
                "    SCH.SCHEMENAME AS SCHEMENAME,REPLACE(                          "&_ 
                "        CASE ISNULL(PP.FLATNUMBER,'EMPTY')                        "&_ 
	            "           WHEN 'EMPTY' THEN ''                                   "&_ 
	            "           ELSE PP.FLATNUMBER+','                                 "&_ 
                "        END                                                       "&_ 
                "  + CASE ISNULL(PP.HOUSENUMBER,'EMPTY')                           "&_ 
                "         WHEN 'EMPTY' THEN ''                                     "&_ 
	            "         ELSE PP.HOUSENUMBER+','                                  "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS1,'EMPTY')                              "&_ 
	            "         WHEN 'EMPTY' THEN ''                                     "&_ 
	            "         ELSE PP.ADDRESS1+','                                     "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS2,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.ADDRESS2+','                                       "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.ADDRESS3,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.ADDRESS3+','                                       "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.TOWNCITY,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.TOWNCITY+','                                       "&_ 
                "    END                                                           "&_     
                "  + CASE ISNULL(PP.COUNTY,'EMPTY')                                "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.COUNTY+','                                         "&_ 
                "    END                                                           "&_ 
                "  + CASE ISNULL(PP.POSTCODE,'EMPTY')                              "&_ 
	            "       WHEN 'EMPTY' THEN ''                                       "&_ 
	            "       ELSE PP.POSTCODE                                           "&_ 
                "    END,',,',',')                                                 "&_ 
                "    AS FULL_PROPERTY_ADDRESS,CAT.DESCRIPTION                    "&_ 
                "    AS CONTACT_TYPE,REPLACE(CD.HOUSENUMBER+','+CD.ADDRESS1+     "&_ 
                "    CASE ISNULL(CD.ADDRESS2,'EMPTY')                              "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.ADDRESS2+','                                       "&_
                "    END                                                           "&_
                " + CASE ISNULL(CD.ADDRESS3,'EMPTY')                               "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.ADDRESS3+','                                       "&_
                "    END                                                           "&_
                " + CASE ISNULL(CD.TOWNCITY,'EMPTY')                               "&_
	            "        WHEN 'EMPTY' THEN  ''                                     "&_
	            "        ELSE CD.TOWNCITY+','                                      "&_
                "   END                                                            "&_
                " + CASE ISNULL(CD.COUNTY,'EMPTY')                                 "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.COUNTY+','	                                       "&_
                "   END                                                            "&_
                " + CASE ISNULL(CD.POSTCODE,'EMPTY')                               "&_
	            "       WHEN 'EMPTY' THEN ''                                       "&_
	            "       ELSE CD.POSTCODE                                           "&_
                "    END,',,',',')                                                 "&_                
                "   AS FULL_CONTACT_ADDRESS,                                       "&_
                " CCC.COMMUNICATION "&_
                " AS COMMUNICATION_NEED,CC.COMMUNICATIONOTHER AS COMMUNICATION_OTHER "&_
                " FROM C_CUSTOMERTENANCY CCT                                       "&_
                " INNER JOIN C_TENANCY CT ON CCT.TENANCYID=CT.TENANCYID            "&_
                " INNER JOIN C__CUSTOMER CC ON CCT.CUSTOMERID=CC.CUSTOMERID        "&_
                " INNER JOIN G_TITLE GT ON GT.TITLEID=CC.TITLE                     "&_
                " INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=CT.PROPERTYID         "&_
                " INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID "&_
                " LEFT JOIN P_BLOCK BL ON BL.BLOCKID = PP.BLOCKID  "&_ 
                " INNER JOIN P_SCHEME SCH ON PP.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
                " INNER JOIN C_ADDRESS CD ON CD.CUSTOMERID=CC.CUSTOMERID           "&_ 
                " INNER JOIN C_ADDRESSTYPE CAT ON CAT.ADDRESSTYPEID=CD.ADDRESSTYPE "&_
                " INNER JOIN dbo.C_CUSTOMER_COMMUNICATION CCC ON CCC.CUSTOMERID=CC.CUSTOMERID "&_
                " WHERE (CCC.COMMUNICATION<> '' "&_
                " OR (PP.POSTCODE<>CD.POSTCODE AND CAT.ADDRESSTYPEID <>3 ))  "&_
                " AND CD.ISDEFAULT<>0 "&_
		" order by CT.TENANCYID "
						
						

		Call OpenRs(rsPOA, strSQL)
		
		while NOT rsPOA.EOF 
		    str_data=str_data & "<tr>" 
		    str_data=str_data & "<td>" & rsPOA("TENANCY_REF") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("CUSTOMER_NAME") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("SCHEMENAME") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("FULL_PROPERTY_ADDRESS") & "</td>"   
		    str_data=str_data & "<td>" & rsPOA("CONTACT_TYPE") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("FULL_CONTACT_ADDRESS") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("COMMUNICATION_NEED") & "</td>"
		    str_data=str_data & "<td>" & rsPOA("COMMUNICATION_OTHER") & "</td>"
		    str_data=str_data & "</tr>"
		rsPOA.moveNext()
		wend
		
		CloseRs(rsPOA)
		SET rsPOA = Nothing
		
		CloseDB()
		'End Get Record to display
%>


<HTML>
<HEAD>
<TITLE>RSL Manager Customers --> Contact and Communication Need </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY bgcolor="#FFFFFF" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 >
  <TR > 
    <TD  colspan="8"> 
      <div align="left"><font color="#0066CC"><b><font size="4">Contact and Communication Need 
        -</font> 
        <%dim todaysDate
				 todaysDate=now()
				 response.write  todaysDate
				%>
        </b></font></div>
      </TD>
    </TR>
    <tr> 
      <td>&nbsp;<b>TENANCY REF</b></td>
      <td>&nbsp;<b>CUSTOMER NAME</b>&nbsp;</td>
      <td>&nbsp;<b>SCHEMENAME</b>&nbsp;</td>
      <td>&nbsp;<b>FULL PROPERTY ADDRESS</b>&nbsp;</td>
      <td>&nbsp;<b>CONTACT TYPE</b></td>
      <td>&nbsp;<b>FULL CONTACT ADDRESS</b></td>
      <td>&nbsp;<b>COMMUNICATION NEED</b>&nbsp;</td>
      <td>&nbsp;<b>COMMUNICATION OTHER</b>&nbsp;</td>
    </tr>
    <%=str_data%> 
    </TABLE>
</BODY>
</HTML>