<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	ColData(0)  = "CU Num|CUSTOMERID|80"
	SortASC(0) 	= "c.CUSTOMERID ASC"
	SortDESC(0) = "c.CUSTOMERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "CustomerNumber(|)"

	ColData(1)  = "First Name|FIRSTNAME|100"
	SortASC(1) 	= "C.FIRSTNAME ASC"
	SortDESC(1) = "C.FIRSTNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Last Name|LASTNAME|100"
	SortASC(2) 	= "C.LASTNAME ASC"
	SortDESC(2) = "C.LASTNAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Nature|NATURE|130"
	SortASC(3) 	= "NATURE ASC"
	SortDESC(3) = "NATURE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Assigned To|FULLNAME|110"
	SortASC(4) 	= "E.FULLNAME ASC"
	SortDESC(4) = "E.FULLNAME DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""

	ColData(5)  = "Status|STATUS|65"
	SortASC(5) 	= "STATUS ASC"
	SortDESC(5) = "STATUS DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""

	THEWHICH = Request("WHICH")
	if (THEWHICH = "" OR THEWHICH = "0") then
		ES = " J.CURRENTITEMSTATUSID = 13 AND "
		ochecked = " checked"
		cchecked = ""
	elseif (THEWHICH = "1") then
		ES = " J.CURRENTITEMSTATUSID = 14 AND "
		ochecked = ""
		cchecked = " checked"
	end if

	THEGROUP = Request("GROUP")
	if (THEGROUP = "" OR THEGROUP = "0") then
		ES2 = " G.ASSIGNTO = " & SESSION("USERID") & " AND "
		ochecked2 = " checked"
		cchecked2 = ""
	elseif (THEGROUP = "1") then
		ES2 = ""
		ochecked2 = ""
		cchecked2 = " checked"
	end if
		
	PageName = "TransferExchangeEnquiryList.asp"
	EmptyText = "No Transfer/Exchange Enquiries found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(5)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""CUSTOMERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT C.CUSTOMERID, C.FIRSTNAME, C.LASTNAME, N.DESCRIPTION AS NATURE, S.DESCRIPTION AS STATUS, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME FROM C_JOURNAL J " &_
			"INNER JOIN C_TRANSFEREXCHANGE G ON J.JOURNALID = G.JOURNALID " &_
			"LEFT JOIN E__EMPLOYEE E ON G.ASSIGNTO = E.EMPLOYEEID " &_			
			"INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " &_
			"INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " &_
			"INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " &_
			"WHERE " & ES & ES2 &_
				"G.TRANSFEREXCHANGEHISTORYID = (SELECT MAX(TRANSFEREXCHANGEHISTORYID) FROM C_TRANSFEREXCHANGE GG WHERE GG.JOURNALID = J.JOURNALID)"  &_
				"ORDER BY " & orderBy & ",J.JOURNALID DESC"

'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Transfer/Exchnage Enquiry List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(CustomerID){
	location.href = "CRM.asp?CustomerID=" + CustomerID
	}

function GO(){
	if (thisForm.WHICH[0].checked == true)
		THEVAL = 0
	else if (thisForm.WHICH[1].checked == true)
		THEVAL = 1

	if (thisForm.GROUP[0].checked == true)
		THEVAL2 = 0
	else if (thisForm.GROUP[1].checked == true)
		THEVAL2 = 1		
	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&WHICH="%>" + THEVAL + "&GROUP=" + THEVAL2
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
      <TD ROWSPAN=2 valign=bottom><IMG NAME="tab_Tenants" TITLE='Transfer/Exchange Enquiries' SRC="Images/tab_transfer_enquiries.gif" WIDTH=201 HEIGHT=20 BORDER=0></TD>			
	<TD align=right>
	STATUS: OPEN <input type=radio name="WHICH" value="0" <%=ochecked%>>
	CLOSED <input type=radio name="WHICH" value="1" <%=cchecked%>>
	&nbsp;&nbsp;|&nbsp;&nbsp;
	USERS: MINE <input type=radio name="GROUP" value="0" <%=ochecked2%>>
	ALL <input type=radio name="GROUP" value="1" <%=cchecked2%>>	
	<input type="button" value=" UPDATE " class="RSLButton" onclick="GO()">
	</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=547 HEIGHT=1></TD>
	</TR>
</TABLE>

<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>