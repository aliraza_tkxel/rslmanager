<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
SQL = "SELECT * FROM C_MAILINGLIST ORDER BY FILEID DESC"
Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	while (NOT rsLoader.EOF)
		TableString = TableString & "<tr><td><a target=""_blank"" href=""/MailingList/" & rsLoader("FILENAME") & """><font color=""blue"">" & rsLoader("FILENAME") & "</font></a></td><td>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</td></tr>"
		rsLoader.moveNext
	Wend
Else
	TableString = "<tr><td colspan=""2"" align=""center"">No Mailing Lists found</td></tr>"
End If
Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer --> Mailing List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    You will find the <b>MAILING LISTS</b> you have created listed below. To save any
    particular file right click on it and click 'save as'.<br />
    To open the file directly just click on the link.
    <br/>
    <br/>
    <table style='border-collapse: collapse' border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td width="200">
                <b>File Name</b>
            </td>
            <td width="100">
                <b>&nbsp;&nbsp;Process Date</b>
            </td>
        </tr>
        <%=TableString%>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
