<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams, lstAction, nature_id, lstCategory,lstGrade,categoryDesc
    'Dim lstTeams
    nature_id = Request("natureid")

	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: 	June 18, 2008
	'' Reason:		Integraiton with Tenats Online

    'Code Changes Start by TkXel=================================================================================

    Dim enqID , asbCategory, asbNotes
        enqID = ""
    'Code Changes End by TkXel=================================================================================
	Call OpenDB()

	path = request("submit")
	
	If path = "" Then path = 0 End If
	If path <> 1 Then
	
	
    	   'Code Changes Start by TkXel=================================================================================
		
		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback

		enqID = Request("EnquiryID")

		asbCategory = ""
		asbNotes = ""

		' Enters into if block only, if EnquiryID is part of query string parameters
      	If enqID <>"" OR Len(enqID) > 0 Then
			' This function will get the details of ASB, based on Enquiry ID from 
			' Database tables TO_ENQUIRY_LOG and TO_ASB
			' These extracted values will be used to populate matching frame text boxes/drop downs etc. 
			Call get_asb_detail(enqID,asbCategory , asbNotes)
		End If

         'Code Changes End by TkXel=================================================================================

		Call entry()
	Else
		Call new_record()
	End If
	
	Call CloseDB()


 'Code Changes Start by TkXel=================================================================================

	Function get_asb_detail(enquiryID, ByRef category, ByRef notes )

		' CALL To STORED PROCEDURE TO get ASB Detail
		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_ASB_SelectAsbDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
		' if record found/returned, setting values
		If Not rs.EOF Then 
			notes = rs("Description")
			category = rs("CategoryID")
		End If
		Set comm = Nothing
		Set rs = Nothing

      End Function

     ' Update Enquiry Log Table
     Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID
		' Executing sproc and storing resutls in record set
		Set rs = comm.execute
		Set comm = Nothing
		Set rs = Nothing

     End Function

   'Code Changes End by TkXel===================================================================================

	Function entry()

        Call get_querystring()

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		Call OpenRs(rsTeam, SQL)
		If (NOT rsTeam.EOF) Then
			TeamID = rsTeam("TEAMID")
		Else
			TeamID = -1
		End If
		Call CloseRs(rsTeam)

		SQL = " E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1 " &_
				" AND ISNULL(J.PATCH,0) IN ( "&_
				"		   SELECT ISNULL(PP.PATCH,0) "&_
				"		   FROM P__PROPERTY PP  "&_
				"		   INNER JOIN C_TENANCY CT ON CT.PROPERTYID=PP.PROPERTYID "&_
				"		   WHERE CT.TENANCYID=" & tenancy_id & " "&_
				"		  )    "

       SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID IN (101) AND L.ACTIVE = 1 AND J.ACTIVE = 1" 

		'Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")
        Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM", "TEAMID, TEAMNAME", "TEAMNAME", "Please Select",  NULL, "textbox200", " style='width:200px' ",  "onchange=""onTeamChange()"" ")
		'Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = " & nature_id & " " ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", actionid, NULL, "textbox200", " style='width:300px' onchange='action_change()' ")
        'Call BuildSelect(lstGrade, "sel_GRADE", "C_ASB_GRADE","GRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:300px' onchange='grade_change()' ")
		'Call BuildSelect(lstCategory, "sel_CATEGORY", "C_ASB_CATEGORY" ,"CATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", null, NULL, "textbox200", " style='width:300px' ")
	End Function

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
        categoryDesc = Request("categoryDesc")
		
		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If

	End Function

	Function new_record()
	
		Dim strSQL, journal_id

        Call get_querystring()

		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
		Else
			New_Status = 13
		End If

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", "& tenancy_id &", NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		Set rsSet = Conn.Execute(strSQL)
		    journal_id = rsSet.fields("JOURNALID").value
		    rsSet.close()
		Set rsSet = Nothing

		

		' GENERAL ENTRY
		
		
		 strSQL = 	"SET NOCOUNT ON;" &_
					 "INSERT INTO C_PlannedMaintenance " &_
					 "(JOURNALID, ITEMSTATUSID ,LASTACTIONDATE, LASTACTIONUSER, ASSIGNTO, NOTES,ITEMNID, ITEMNATUREID) " &_
					 "VALUES (" & journal_id & ", " & New_Status &  " , '" & Now & "' , " & Session("userid") & ", " & Request.Form("sel_AssignTo") &_
					 ", '" & Request.Form("txt_NOTES") & "' , NULL , NULL ); "&_
					 "SELECT SCOPE_IDENTITY() AS PLANNEDMAINTENANCEID;"
		
		
		 'Response.Write(strSQL)
		 'REsponse.end
		 'REsponse.flush
		 conn.Execute(strSQL)
		 
		If enqID <> "" OR Len(enqID) > 0 Then
		    Call update_enquiry_log(enqID, journal_id)
        End If	
		
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
		'Response.Write(strSQL)
		 'REsponse.end
		 'REsponse.flush
		' Code Added by TkXel - Start

		'If enqID <> "" OR Len(enqID) > 0 Then
		'    Call update_enquiry_log(enqID, journal_id)
        'End If		

		' Code Added by TkXel - End
		
		' INSERT LETTER IF ONE EXISTS
		'If Request.Form("hid_LETTERTEXT") <> "" Then
	'		strSQL = "INSERT INTO C_ASBLETTER (ASBHISTORYID, LETTERCONTENT) VALUES (" & generalhistory_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "')"
	'		conn.execute(strSQL)
	'	End If
	'	Call sendEmail()
		
	End Function
	Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID
		' Executing sproc and storing resutls in record set
		Set rs = comm.execute
		Set comm = Nothing
		Set rs = Nothing

     End Function
    Function sendEmail()

    sql =   " SELECT ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'') CNAME,ISNULL(P.HOUSENUMBER,'') + ' ' + ISNULL(P.ADDRESS1,'') + ' ' + ISNULL(P.ADDRESS2,'') + ' '+ ISNULL(P.POSTCODE,'') AS PADDRESS FROM C__CUSTOMER C "&_
            " INNER JOIN DBO.C_CUSTOMERTENANCY CT ON CT.CUSTOMERID=C.CUSTOMERID AND CT.ENDDATE IS NULL "&_
            " INNER JOIN C_TENANCY T ON T.TENANCYID=CT.TENANCYID AND T.ENDDATE IS NULL "&_
            " INNER JOIN P__PROPERTY P ON P.PROPERTYID=T.PROPERTYID "&_
            " AND T.TENANCYID=" & tenancy_id
    Call OpenRs(rsCustomer, sql)

    If NOT rsCustomer.EOF then
        cname = rsCustomer("CNAME")
        padd =  rsCustomer("PADDRESS")
    End if
    Call CloseRs(rsCustomer)

    SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT C WHERE EMPLOYEEID=" & Request.Form("sel_USERS")
	Call OpenRs(rsEmail, SQL)

	If NOT rsEmail.EOF And NOT rsEmail.BOF Then 
		' send email
		Dim strRecipient
		Dim strPassword
		Dim strUsername
		Dim strUserEmail
		strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

        emailbody = "Tenancy Id: " &  tenancy_id & "<br/>" &_
                "Name: " & cname & "<br/>" &_
                "Address: " & padd & "<br/>" &_
                "Grade: " & Request.Form("sel_GRADE") & "<br/>" &_ 
                "Category: " & categoryDesc

        Set iMsg.Configuration = iConf
        iMsg.To = strRecipient
        iMsg.From = strRecipient
        iMsg.Subject = "New ASB Item Logged"
        iMsg.HTMLBody = emailbody
        'iMsg.TextBody = emailbody
        iMsg.Send

	End If
	Call CloseRs(rsEmail)

    End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
            background-image: none;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        //FormFields[0] = "sel_AssignTo|Assign To|SELECT|Y"
        FormFields[1] = "sel_TEAMS|Team|SELECT|Y"
		FormFields[2] = "txt_NOTES|Notes|TEXT|Y"
     
       
        function save_form() {
			
            //var category = document.getElementById("sel_CATEGORY").options[document.getElementById("sel_CATEGORY").selectedIndex].text;
			
			var assignto =  document.getElementById("sel_AssignTo").options[document.getElementById("sel_AssignTo").selectedIndex].value+"";
			var team = document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value+"";
			var notes = document.getElementById("txt_NOTES").value;
			if(assignto == "" || team == "" || notes == "")
			{
				alert("fill all fields");
				return false;
			}
            document.RSLFORM.target = "CRM_BOTTOM_FRAME";
			
            document.RSLFORM.action = "iPlannedMaintenance.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
			document.RSLFORM.submit();
			
        }

        function onTeamChange() {
			//alert(document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value+" ");
		    document.RSLFORM.target = "frm_erm_srv";
            document.RSLFORM.action = "../serverside/TeamChange_srv.asp?teamId=" + document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value;
            document.RSLFORM.submit();
        }
        function GetEmployees() {
            document.RSLFORM.action = "../Serverside/GetEmployees_ByPatch.asp"
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function open_letter() {
            if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
                ManualError("img_LETTERID", "You must first select a letter to view", 1)
                return false;
            }
            var tenancy_id = parent.parent.MASTER_TENANCY_NUM
            window.open("../Popups/arrears_letter.asp?tenancyid=" + tenancy_id + "&letterid=" + document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value + "&itemid=" + "<%=item_id%>", "display", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

        function action_change() {
            document.RSLFORM.action = "../Serverside/change_letter_action.asp?ITEMACTIONID=" + document.getElementById("sel_ITEMACTIONID").options[document.getElementById("sel_ITEMACTIONID").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        function grade_change() {
            document.RSLFORM.action = "../Serverside/asb_grade_change.asp?GRADEID=" + document.getElementById("sel_GRADE").options[document.getElementById("sel_GRADE").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }

        //-- code changes start by TkXel===========================================================================

        function set_values(enquiryID) {
            // this function will get called on onLoad event of this form
            // This function used to set values of different elements belongs to this form
            var strEnqID = enquiryID + "";
            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {
                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
                // setting value for ASB notes
                var notes = document.getElementById("txt_NOTES");
                notes.value = "<%=asbNotes%>"
                // setting value for category drop down
                var category = document.getElementById("sel_CATEGORY");
                category.value = "<%=asbCategory%>"
            }
        }

        //-- code changes end by TkXel===========================================================================

    </script>
</head>
<body class="TA" onload="set_values(<%=enqID%>)">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));" class="textbox200"
                    rows="4" cols="20" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Team :
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Assign To :
            </td>
            <td>
                <span class="formw"><span id="dvEmployees">
                        <select name="sel_AssignTo" id="sel_AssignTo" class="textbox200">
                            <option value=''>Select Item</option>
                        </select>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                Close Enquiry: &nbsp;
                <input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />
                &nbsp;
                <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
                <input type="button" value="Save" class="RSLButton" title="Save" onclick="save_form()"
                    name="button" style="cursor: pointer" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="400"
        height="100" style="display: none"></iframe>
		<iframe src="/secureframe.asp" name="frm_erm_srv" id="frm_erm_srv" width="4px" height="4px"
        style="display: none"></iframe>
</body>
</html>
