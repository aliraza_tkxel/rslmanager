<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams,DangerratingText,showJournal
	OpenDB()
	
	path = request("submit")
	if path = "" then path = 0 end if
	if path  <> 1 then
		get_querystring()
		loadData()
	Else
		new_record()
	End If
	
	CLoseDB()	
	
	// retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		txt_dangerrating = replace(Request.form("txt_dangerrating"),"'", "''")
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		
	End Function
	
	Function loadData()
		if customer_id <> "" then
		SQL = "SELECT * FROM C_PERSONALRISK WHERE CUSTOMERID = " & customer_id & " ORDER BY RISKDATE DESC"
		set rsSet = Conn.Execute(SQL)
		IF NOT rsSet.eof then
		DangerratingText = rsSet.fields("RISKDESC").value
		end if
		rsSet.close()
		Set rsSet = Nothing
		End If
	
	End Function
	
	Function new_record()
	
		Dim strSQL, journal_id
		get_querystring()
		txt_dangerrating = replace(Request.form("txt_dangerrating"),"'","''")
		// JOURNAL ENTRY
		SQL = "SET NOCOUNT ON; INSERT INTO C_journal (CUSTOMERID,TENANCYID,ITEMID, ITEMNATUREID, TITLE) VALUES (" & customer_id & "," & tenancy_id & ",2,31,'Risk Entry'); SELECT SCOPE_IDENTITY() AS JOURNALID;"
	
		set rsSet = Conn.Execute(SQL)
		journal_id = rsSet.fields("JOURNALID").value


		rsSet.close()
		
		strSQL = "INSERT INTO C_PERSONALRISK (RISKDESC,NOTEDBY,CUSTOMERID, journalid) VALUES " &_
				 "('" & txt_dangerrating & "'," & Session("UserID") & "," & customer_id & "," & journal_id &  ")"
		rw strSQL
		Conn.Execute(strSQL)
		showJournal = "true"		
		'Response.Redirect ("IDANGERRATING.asp?dangerratingset=true")
	End Function

	
%>
<% if showJournal = "true" then %>
<script language="javascript">
	parent.parent.swap_div(8, 'bottom')
	parent.parent.swap_div(1, 'top')
</script>
<% End If %>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "txt_dangerrating|DangerRating|TEXT|Y"
	
	
	function save_form(){
		if (!checkForm()) return false;
		RSLFORM.action = "IDANGERRATING.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		RSLFORM.submit();
	}
</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>

<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
  <%if request("DangerRatingSet") = "true" then %>
  <TR style='height:7px'> 
    <TD>The rating has been set.</TD>
  </TR>
  <% Else %>
  <FORM NAME=RSLFORM METHOD=POST>
    <TR style='height:7px'> 
      <TD></TD>
    </TR>
    <TR> 
      <TD VALIGN=TOP>Notes : </TD>
      <TD> 
        <textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="" class='textbox200' rows=9 name="txt_dangerrating"></textarea>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_dangerrating" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
    <tr> 
      <td colspan="2">&nbsp; </td>
      <TD></TD>
    </tr>
    <TR> 
      <TD COLSPAN=2 align=right nowrap>&nbsp;<!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
        <input type="button" name="Submit" value="Save" onClick="save_form()"  class="RSLButton">
      </TD>
    </TR>
  </FORM>
  <% End If %>
</TABLE>
<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:NONE'></IFRAME>
</BODY>
</HTML>	
	
	
	
	
	