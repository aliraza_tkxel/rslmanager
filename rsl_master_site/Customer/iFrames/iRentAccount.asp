<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #Include virtual="Includes/Database/ADOVBS.INC" -->
<%
	Dim cnt, customer_id, str_journal_table, str_color
	Dim CANAMEND
	
	OpenDB()
	CANAMEND = can_amend_rent_journal()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")
	
	build_journal()

	'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	strSQL = "SELECT ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
		 "FROM 	F_RENTJOURNAL J  WITH (NOLOCK) " &_
		 "LEFT 	JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
		"WHERE	J.TENANCYID = " & tenancy_id & "  AND  (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))"
	Call OpenRs (rsSet, strSQL) 
	ACCOUNT_BALANCE = FormatCurrency(rsSet("AMOUNT"))
	CloseRs(rsSet)
	'END OF GET BALANCE
	
	CloseDB()

	Function can_amend_rent_journal()
		
		SQL = "SELECT * FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'CANUPDATERENTJOURNAL' AND DEFAULTVALUE = " & SESSION("USERID") 
		Call OpenRs(rsSet, SQL)
		if Not rsSet.EOF Then C = 1 Else C = 0 End If
		CloseRs(rsSet)
		can_amend_rent_journal = C
	
	End Function

	Function build_journal()
	
		dim rsCSet,strCSQL,dbCmd,Param,dbCon
		cnt = 0
		
		strCSQL="EXEC F_GET_CUSTOMER_ACCOUNT @TENANCYID= " &  tenancy_id
		set rsCSet=server.createobject("ADODB.Recordset")
		set rsCSet=Conn.execute(strCSQL)
		
		str_journal_table = ""
		While Not rsCSet.EOF
			
			cnt = cnt + 1
			If NOT isNULL(rsCSet("CREDIT")) Then str_color = "STYLE='COLOR:BLACK'" Else str_color = "STYLE='COLOR:BLUE'" End If	
			If Cint(CANAMEND) = 1 Then str_canamend = " ONCLICK=""open_amend("&rsCSet("JOURNALID")&")"" STYLE='CURSOR:HAND' TITLE='Amend' " End If
			str_journal_table = str_journal_table &_
			 	"<TR STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'" & str_color & str_canamend & " >" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsCSet("F_TRANSACTIONDATE_TEXT") & "</TD>" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsCSet("ITEMTYPE") & "</TD>" &_
				"<TD NOWRAP STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsCSet("PAYMENTTYPE") & "</TD>" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'> " & rsCSet("PAYMENTSTARTDATE") & "&nbsp;" & rsCSet("PAYMENTENDDATE") & "</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsCSet("debit") & "&nbsp;&nbsp;</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsCSet("credit") & "&nbsp;&nbsp;</TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsCSet("BreaKDown") & "&nbsp;&nbsp;</TD>" &_
				"<TD STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-BOTTOM-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsCSet("STATUS")  & "</TD>" &_
				"<TR>"
			rsCSet.movenext()
			
		Wend
		CloseRs(rsCSet)
		
		If cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="refresh" content="120;URL=iRentAccount.asp?CustomerID=<%=customer_id%>&TenancyID=<%=tenancy_id%>">
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
	function UpdateBalance(){
		try {
			Ref = parent.document.getElementsByName("TenantBalance")
			Ref[0].value = "<%=ACCOUNT_BALANCE%>"
			}
		catch (e){
			temp = "What the hell is goin on ere....."
			}
		}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(10, "BOTTOM")
		}
	
	function ErrorHandlingStopLoader(){
		try {
			parent.STOPLOADER('BOTTOM')	
			}
		catch (e){
			temp = "this is a mega bug...."
			}
		}
	// function opens the amend popup for the finance team to manually amend journal items	
	function open_amend(JID){
	
		window.showModalDialog("../popups/amend_account.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&journalid="+JID+"&Random=" + new Date(), "","dialogHeight: 440px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="DoSync();UpdateBalance();ErrorHandlingStopLoader()" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>


	<TABLE WIDTH=100% CELLPADDING=2 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse; BORDER-bottom:2PX" slcolor='' border=0 hlcolor=STEELBLUE >
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=70><B><FONT COLOR='#133e71'>Date</FONT></B></TD>
				
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><FONT COLOR='#133e71'>Item</FONT></B></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100 NOWRAP><B><font color="#133e71">Payment Card</font></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" ALIGN=CENTER><B><FONT COLOR='#133e71'>Period</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B><FONT COLOR='#133e71'>Debit</FONT></B>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B><FONT COLOR='#133e71'>Credit</FONT></B>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right WIDTH=100><B><FONT COLOR='#133e71'>Balance</FONT></B>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B><FONT COLOR='#133e71'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	