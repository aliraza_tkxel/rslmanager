<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	Dim customer_id, tenancy_id, str_joint,str_occup, NewTenancyButton, str_addass, dev_id, show_table, REEDONLY, cnt, total_cnt, rent_tip

	'this variable is set later on and is used to stop people from entering
	'additional assets with a start date less than the main start date
	'it is initially set to a dummy value
	SelectedTenancyStartDate = "1/1/1900"

	show_table = Request("toshow")
	if show_table = "" then show_table = 0 end if
	cnt = 0
	total_cnt = 0
	OpenTenancy = "-Z"

	CUSTOMERID = Request("CUSTOMERID")
	tenancy_id = Request("tenancyid")
	'TENANCY_ID = -1 IF NULL OR EMPTY OTHERWISE IT CRASHES EVERYTHING.....
	'have to also compare to the string '-1' also because sometimes that is sent over a the actual requst object
	If (tenancy_id = "" Or tenancy_id = "-1") Then
		tenancy_id = -1
	End If

	SQL = "SELECT ISNULL(C.CUSTOMERTYPE,0) AS CUSTOMERTYPE, E.FIRSTNAME + ' ' + E.LASTNAME AS RENTOFFICERNAME, LTRIM(ISNULL(G.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS FULLNAME,  "&_
			"TC.TENANCYCLOSEDID, " &_
			"T.TENANCYID AS TTENANCYID, " &_
			"ISNULL(T.TENANCYID, '') AS TENANCYREF, " &_
			"ISNULL(P.PROPERTYID, '') AS PROPERTYID, " &_
			"ISNULL(TT.DESCRIPTION, '') AS TENANCYTYPE, " &_
			"ISNULL(CONVERT(NVARCHAR, CT.STARTDATE, 103) ,' ') AS FSTARTDATE, " &_
			"ISNULL(CONVERT(NVARCHAR, CT.ENDDATE, 103) ,' ') AS FENDDATE, " &_
			"CONVERT(NVARCHAR, CT.STARTDATE, 103) AS CT_FSTARTDATE, " &_
			"CONVERT(NVARCHAR, CT.ENDDATE, 103) AS CT_FENDDATE, " &_
			"ISNULL(P.HOUSENUMBER , ' ' ) AS HOUSENUMBER, " &_
			"ISNULL(P.FLATNUMBER , ' ' ) AS FLATNUMBER, " &_
			"ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
			"ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
			"ISNULL(P.ADDRESS3, ' ' ) AS ADDRESS3, " &_
			"ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
			"ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
			"ISNULL(P.COUNTY , ' ' ) AS COUNTY, ISNULL(PF.TOTALRENT,0) AS TOTALRENT, " &_
			"ISNULL(P.DEVELOPMENTID , ' ' ) AS DEVELOPMENTID, " &_
			"ISNULL(PF.RENT,0) AS RENT, ISNULL(PF.SERVICES,0.00) AS SERV, ISNULL(PF.COUNCILTAX,0) AS COUNCILTAX, " &_
			"ISNULL(PF.WATERRATES,0) AS WATER, ISNULL(PF.INELIGSERV,0) AS INELIG, ISNULL(PF.SUPPORTEDSERVICES,0) AS SUPP, CASE WHEN T.CBL=1 THEN 'Yes' WHEN T.CBL=0 THEN 'No' END AS CBL " &_ 
			"FROM C_CUSTOMERTENANCY CT " &_
			"INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID " &_
			"LEFT JOIN C_TENANCYCLOSED TC ON T.TENANCYID = TC.TENANCYID " &_
			"LEFT JOIN C_TENANCYTYPE TT ON T.TENANCYTYPE = TT.TENANCYTYPEID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " &_
			"LEFT JOIN P_FINANCIAL PF ON PF.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN (SELECT PATCH,EMPLOYEEID FROM E_JOBDETAILS WHERE ACTIVE=1 AND JOBTITLE= 'Rent Recovery Officer') EMP ON EMP.PATCH=D.PATCHID " &_
			"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = EMP.EMPLOYEEID " &_
			"WHERE CT.CUSTOMERID = " & CUSTOMERID & " " &_
			"ORDER BY CT.CUSTOMERTENANCYID DESC "

	Call OpenDB()
	Call OpenRS(rsLoader, SQL)
	IsTenancy = false
	PreviousString = ""
	TenancyString = ""
	rent_tip = ""
	if (NOT rsLoader.EOF) then
		dev_id = rsLoader("DEVELOPMENTID")

		while (NOT rsLoader.EOF)

			cnt = cnt + 1
			' make sure correct line is hightlighted
			If StrComp(tenancy_id, rsLoader("TENANCYREF"), 1) = 0 AND tenancy_id <> -1 Then
				colorstring  =  "bgcolor = ""#add8e6"""
			else
				colorstring = ""
			end if

			' LOAD TABLE ROWS FOR ALL TENANCIES
			PreviousString = PreviousString &_
					"<tr " & colorstring & " id=ten_row" & cnt & " style=""cursor:pointer"" title=""Show account details"" onclick=""reColour(" &_
					rsLoader("TTENANCYID") & ")""><td>" & TenancyReference(rsLoader("TENANCYREF")) & "</td><td>" &_
					rsLoader("FSTARTDATE") & "</td><td>" & rsLoader("FENDDATE") & "</td><td>" & rsLoader("CBL") & "</td><td colspan=""2"">" &_
					rsLoader("TENANCYTYPE") & "</td></tr>"

			' FLAG CURRENT TENANCY
			If ( (ISNULL(rsLoader("TENANCYCLOSEDID")) AND ISNULL(rsLoader("CT_FENDDATE"))) AND (CDbl(rsLoader("CUSTOMERTYPE")) = 2) ) then
				IsTenancy = True
				OpenTenancy = rsLoader("TENANCYREF")
			End If

			' BUILD DETAIL PAGE FOR CURRENT TENANCY-- needs 'DE_ZANFARRING'
			If StrComp(tenancy_id, rsLoader("TENANCYREF"), 1) = 0 Then
				rent_tip = "Rent : " & rsLoader("RENT") & VbCrLf & "Services : " & rsLoader("SERV") & VbCrLf & "Council Tax : " & rsLoader("COUNCILTAX") & VbCrLf &_
				 "Water : " & rsLoader("WATER") & VbCrLf & "Ineligible Services : " & rsLoader("INELIG") & VbCrLf & "Supported Services : " & rsLoader("SUPP")
				TenancyString = TenancyString & "<tr><td><b>Name</B></td><td>" & rsLoader("FULLNAME") & "</td>"
				housenumber = rsLoader("HOUSENUMBER")
				TenancyString = TenancyString & "<td><b>House Number</B></td><td>" & housenumber & "</td></tr>"
				SelectedTenancyStartDate = rsLoader("FSTARTDATE")
                dim hostname
                hostname = "https://" & Request.ServerVariables("HTTP_HOST")
				TenancyString = TenancyString & "<tr><td><b>Tenancy Ref</B></td><td>" & rsLoader("TENANCYREF") & "</td><td><b>Address 1</B></td><td>" & rsLoader("ADDRESS1") & "</td></tr>"
				TenancyString = TenancyString & "<tr><td><b>Type</B></td><td>" & rsLoader("TENANCYTYPE") & "</td><td><b>Address 2</B></td><td>" & rsLoader("ADDRESS2") & "</td></tr>"
				TenancyString = TenancyString & "<tr><td><b>Property Ref</B></td><td><a href='"&hostname& "/RSLApplianceServicing/Bridge.aspx?pid=" & rsLoader("PROPERTYID") & "' target='_parent' title='Click to view property'>" & rsLoader("PROPERTYID") & "</a></td><td><b>Town / City</B></td><td>" & rsLoader("TOWNCITY") & "</td></tr>"
				TenancyString = TenancyString & "<tr><td><b>Start Date</B></td><td>" & rsLoader("FSTARTDATE") & "</td><td><b>County</B></td><td>" & rsLoader("COUNTY") & "</td></tr>"
				TenancyString = TenancyString & "<tr><td><b>End Date</B></td><td>" & rsLoader("CT_FENDDATE") & "</td><td><b>Postcode</B></td><td>" & rsLoader("POSTCODE") & "</td></tr>"
				TenancyString = TenancyString & "<tr><td><b>Monthly Rent</B></td><td><span id='propspan' style='cursor:pointer' title='" & rent_tip &"'>" & formatCurrency(rsLoader("TOTALRENT")) & "</span></td><td><b>Rent Officer</B></td><td>" & rsLoader("RENTOFFICERNAME") & "</td></tr>"
			End If
			rsLoader.moveNext()
		wend

	total_cnt = cnt	
	Call CloseRs(rsLoader)
	End If

	If (TenancyString = "") Then
		TenancyString = TenancyString & "<tr><td colspan=""4"" align=""center"">No Tenancy Selected</td></tr>"
	End If

	If (IsTenancy = False) Then
		NewTenancyButton = "<tr><td height=""100%"" colspan=""6"" align=""right"" valign=""bottom""> " &_
							"<input type=""button"" name=""BTN_UPDATEPD"" id=""BTN_UPDATEPD"" value=""Create Tenancy"" title=""Create Tenancy"" class=""RSLButton"" " &_
							"onclick=""Create_Tenancy(" & CUSTOMERID & ");"" style=""cursor:pointer""></td></tr>"
	Else
		'ADDED THE -1 CHECK SO THAT THE NEW TENANCY BUTTON IS NOT OVERWRITTEN BY THE AMEND BUTTON
		If (StrComp(tenancy_id, OpenTenancy, 1) = 0) Then
			If (tenancy_id <> -1) Then
				NewTenancyButton = "<tr><td colspan=""6"" align=""right"" valign=""bottom"" height=""100%""> " &_
									"&nbsp;<input type=""button"" name=""BTN_AMEND""  title=""Close this tenancy"" value=""Close"" class=""RSLButton"" onclick=""alert('This process has moved to New Item --> Tenant --> Termination') "" style=""cursor:pointer""></td></tr>"
			End If
		End If
	End If

	'PULLED OUT DO_JOINT() FUNCTION AS OTHERWISE WE DO NOT GET THE NO ADDITIONAL BLAH DE BLAH....
	Call do_joint()
	Call do_Occupant()

	If (PreviousString = "") Then
		PreviousString = "<tr><td colspan=""6"" align=""center"">No Current/Previous Tenancies</td></tr>"
	End If

	' GET ADDITIONAL ASSETS
	Call additional_assets()

	' CHECK FOR ADDITIOANL ASSETS
	Function additional_assets()

		cnt = 0
		str_addass = ""
		str_SQL = 	"SELECT 	ISNULL(P.PROPERTYID,'') AS PROPERTYID, " &_
					"			ISNULL(T.DESCRIPTION,'') + ' ' + ISNULL(P.HOUSENUMBER,'') AS ADDRESS,  " &_
					"			ISNULL(ADDRESS1,'') AS STREET, " &_
					"			ISNULL(F.TOTALRENT, 0) AS RENT ," &_
					"			ISNULL(CONVERT(NVARCHAR, A.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, A.ENDDATE, 103) ,'') AS ENDDATE " &_
					"FROM	 	C_ADDITIONALASSET A " &_
					"			LEFT JOIN P__PROPERTY P ON A.PROPERTYID = P.PROPERTYID " &_
					"			LEFT JOIN P_FINANCIAL F ON P.PROPERTYID = F.PROPERTYID " &_
					"			LEFT JOIN P_PROPERTYTYPE T ON P.PROPERTYTYPE = T.PROPERTYTYPEID " &_
					"WHERE		A.TENANCYID = " & tenancy_id  &_
					"ORDER		BY A.ADDITIONALASSETID DESC "

		Call OpenRS(rsLoader, str_SQL)

		While Not rsLoader.EOF

			cnt = cnt + 1
			str_addass = str_addass & "<tr><td>" & rsLoader("PROPERTYID")  & "</td><td>" & rsLoader("ADDRESS") &_
										"</td><td>" & rsLoader("STARTDATE")  & "</td><td>" & rsLoader("ENDDATE") &_
										"</td><td>" & FormatCurrency(rsLoader("RENT")) & "</td>"

			'IF NO ENDDATE THEN ALLOW CANCEL
			If rsLoader("ENDDATE") = "" AND IsTenancy Then
				str_addass = str_addass & "<td align=""center"" style=""color:red;cursor:pointer"" onclick=""remove_asset('" &_
					rsLoader("PROPERTYID") & "', " & tenancy_id & ",'" & FormatDateTime(rsLoader("STARTDATE"),1) & "')"">Cancel</td></tr>"
			Else
				str_addass = str_addass & "<td>&nbsp;</td></tr>"
			End If

			rsLoader.movenext()
		Wend

		If cnt = 0 Then str_addass = "<tr><td colspan=""6"" align=""center"">No additional assets for this tenancy.</td></tr>" End If

		str_addass = str_addass & "<thead><tr style=""background-color:white;""><td valign=""bottom"" colspan=""6"" align=""right"" height=""100%"">&nbsp;"
		'ADDED THE -1 CHECK SO THAT THE NEW ADDITIONAL ASSET BUTTON IS NOT SHOWN WHEN CUSTOMER IS A NON TENANT
		If (StrComp(tenancy_id, OpenTenancy, 1) = 0) Then
			If (tenancy_id <> -1) Then
				str_addass = str_addass & "<input type=""button"" value=""&nbsp;&nbsp;New&nbsp;&nbsp;"" title=""New"" class=""rslbutton"" onclick=""get_assets(" & dev_id & ")"" style=""cursor:pointer"" />"
			End If
		End if
		str_addass = str_addass & "</td></tr></thead>"

	End Function

	' RETRIEVE JOINT TENANCIES
	Function do_joint()

		str_SQL = 	"SELECT 	C.CUSTOMERID, LTRIM(ISNULL(T.DESCRIPTION,'') + ' ' + ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, " &_
					"			ISNULL(CONVERT(NVARCHAR, CT.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, CT.ENDDATE, 103) ,'') AS ENDDATE " &_
					"FROM		C_CUSTOMERTENANCY CT " &_
					"			LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
					"			LEFT JOIN G_TITLE T ON C.TITLE = T.TITLEID " &_
					"WHERE		CT.TENANCYID = " & tenancy_id & " AND CT.CUSTOMERID <> " & customerid 

		Call OpenRs(rsSet, str_SQL)
		cnt = 0
		str_joint = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			str_joint = str_joint & "<tr style=""cursor:pointer"" title=""Take Me There"" onclick=""parent.location.href='../CRM.asp?CustomerID=" & rsSet("CUSTOMERID") & "'""><td>" & rsSet("CUSTOMERID")  & "</td><td>" & rsSet("FULLNAME") &_
									"</td><td>" & rsSet("STARTDATE")  & "</td><td>" & rsSet("ENDDATE") &_
									"</td>"

			' IF NO ENDDATE THEN ALLOW CANCEL
			' istenancy check so that we can make sure that a closed joint tenant cannot close a current tenant
			If rssET("ENDDATE") = "" AND IsTenancy Then
				str_joint = str_joint & "<td align=""center"" style=""color:red;cursor:pointer"" onclick=""remove_tenant('" & rsSet("CUSTOMERID") & "', " & tenancy_id & ")"">Cancel</td></tr>"
			Else
				str_joint = str_joint & "<td>&nbsp;</td></tr>"
			End If

			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		'ADDED A </TR> BECAUSE PAULY NEVER CLOSES HIS TR'S...ALSO CENTER ALIGNED THE TEXT AND MADE IT NON BOLD
		If cnt = 0 Then str_joint = "<tr><td colspan=""5"" align=""center"">No additional tenants are attached to this account</td></tr>" end if

		'ZANFAR ALI MOVED THE HEIGHT FROM THE TR TO THE TD, AS IT DOES NOT WORK ON TR'S....
		str_joint = str_joint & "<thead><tr style=""background-color:white""><td valign=""bottom"" colspan=""5"" align=""right"" height=""100%"">&nbsp;"

		'ZANFAR ALI ADDED THE -1 CHECK SO THAT THE ADDTENANT TENANCY BUTTON IS NOT SHOWN FOR NON TENANCT CUSTOMERS
		If (StrComp(tenancy_id, OpenTenancy, 1) = 0) Then
			If (tenancy_id <> -1) Then
				str_joint = str_joint & "<input type=""button"" name=""BTN_ADD"" id=""BTN_ADD"" value=""Add. Tenant"" title=""Add an existing customer to this tenancy"" class=""RSLButton"" onclick=""swap_tables('table5')"" />"
			End If
		End If
		str_joint = str_joint & "</td></tr></thead>"

	End Function

	Function do_Occupant()

		str_SQL = 	" SELECT 	OC.OCCUPANTID,OC.CUSTOMERID,OC.TENANCYID, " &_
					"			LTRIM(ISNULL(T.DESCRIPTION,'') + ' ' + ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'')) AS FULLNAME,  " &_
					"			R.DESCRIPTION, " &_
					"			ES.DESCRIPTION, " &_
					"			ISNULL(CONVERT(NVARCHAR, OC.STARTDATE, 103) ,'') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, OC.ENDDATE, 103) ,'') AS ENDDATE " &_
					" FROM		C_OCCUPANT OC " &_
					"			left JOIN C_ECONOMICSTATUS ES ON ES.ECONOMICSTATUSID = OC.ECONOMICSTATUS " &_
					"			left JOIN C_RELATIONSHIP R ON R.RELATIONSHIPID = OC.RELATIONSHIP " &_
					"			INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = OC.CUSTOMERID " &_
					"			LEFT JOIN G_TITLE T ON C.TITLE = T.TITLEID " &_
					" WHERE 	OC.ENDDATE IS NULL  " &_
					" 			AND OC.TENANCYID = " & tenancy_id 

		Call OpenRs(rsSet, str_SQL)
		cnt = 0
		str_occup = ""
		While Not rsSet.EOF

			CusomerID = rsSet("CUSTOMERID")
			cnt = cnt + 1
			str_occup = str_occup & "<tr STYLE=""cursor:pointer"" title=""Take Me There"" onclick=""amend_occupant('" & CusomerID & "')""><td>" & CusomerID  & "</td><td>" & rsSet("FULLNAME") &_
				"</td><td>" & rsSet("STARTDATE")  & "</td><td>" & rsSet("ENDDATE") &_
									"</td>"
			' IF NO ENDDATE THEN ALLOW CANCEL
			' zanfar ali added the istenancy check so that we can make sure that a closed joint tenant cannot close a current tenant
			If rssET("ENDDATE") = "" AND IsTenancy Then
				str_occup = str_occup & "<td align=""center"" style=""color:red;cursor:pointer"" onclick=""remove_occupant('" & CusomerID & "', " & tenancy_id & ")"">Cancel</td></tr>"
			Else
				str_occup = str_occup & "<td>&nbsp;</td></tr>"
			End If

			rsSet.movenext()

		Wend
		Call CloseRs(rsSet)

		'ADDED A </TR> BECAUSE PAULY NEVER CLOSES HIS TR'S...ALSO CENTER ALIGNED THE TEXT AND MADE IT NON BOLD
		if cnt = 0 then str_occup = "<tr><td colspan=""5"" align=""center"">No additional tenants are attached to this account</td></tr>" end if

		'ZANFAR ALI MOVED THE HEIGHT FROM THE TR TO THE TD, AS IT DOES NOT WORK ON TR'S....
		str_occup = str_occup & "<thead><tr style='background-color:white'><td valign=bottom colspan=5 align=right height='100%'>&nbsp;"

		'ZANFAR ALI ADDED THE -1 CHECK SO THAT THE ADDTENANT TENANCY BUTTON IS NOT SHOWN FOR NON TENANCT CUSTOMERS
		if (StrComp(tenancy_id, OpenTenancy, 1) = 0) then
			if (tenancy_id <> -1) then
				str_occup = str_occup & "<input  type=""button"" name=""BTN_ADD_OCCUP"" value=""Add. Occupant"" title=""Add an existing customer to this tenancy as an occupant."" class=""RSLButton"" onclick=""add_occupant()"" />"
			END IF
		end if
		str_occup = str_occup & "</td></tr></thead>"

	End Function

	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body, form
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="javascript">

    var FormFields = new Array();
        FormFields[0] = "txt_CUSTOMERNAME|Customer Name|TEXT|Y"

    function Create_Tenancy(CUSTOMERID) {
        document.getElementById("BTN_UPDATEPD").setAttribute("disabled", "disabled", true);
        javascript: location.href = "iSearchProperty.asp?CustomerID=" + CUSTOMERID
    }


    // sawps table that need more than one page to display details
    function swap_tables(str_which_one) {
        var table_cnt = 7
        // close all other tables and open src table
        for (i = 1; i <= table_cnt; i++)
            document.getElementById("table" + i + "").style.display = "none";
        document.getElementById(str_which_one).style.display = "block";
    }


    function go_joint(int_customerid) {
        parent.location.href = "../CRM.asp?CustomerID=" + int_customerid
    }


    function reColour(int_tenancy_id) {
        // set global variable on crm.asp to selected tenancy
        parent.MASTER_TENANCY_NUM = int_tenancy_id;
        parent.UpdateMainBalance()
        parent.swap_div(parent.MAIN_OPEN_BOTTOM_FRAME, 'bottom')
        // reload current page with current information regarding current tenancy
        location.href = "iTenancy.asp?customerid=<%=CUSTOMERID%>&tenancyid=" + int_tenancy_id
     }


    function SwapDiv(whatTab,where)
    {
        parent.swap_div(whatTab,where)
    }


    function open_tenancy() {
        window.open("../Popups/amendtenancy.asp?tenancyid=<%=tenancy_id%>&customerid=<%=customerid%>", "display", "width=400,height=360,left=100,top=200");
    }


    function remove_asset(str_assetid, int_tenancyid, min_date) {
        var answer
            answer = window.showModalDialog("../popups/remove_asset_date.asp?MinStartDate=" + min_date + "&rand=" + new Date(), "", "dialogHeight: 170px; dialogWidth: 250px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
        if (answer == false) { return false; }
        else {
            document.getElementById("hid_ACTION").value = "DELETE";
            document.RSLFORM.target = "frm_ten";
            document.RSLFORM.action = "../serverside/additionalasset_svr.asp?customerid=<%=CUSTOMERID%>&propertyid=" + str_assetid + "&tenancyid=" + int_tenancyid + "&enddate=" + answer;
            document.RSLFORM.submit();
            parent.STARTLOADER('TOP')
            parent.swap_div(parent.MAIN_OPEN_BOTTOM_FRAME, 'bottom')
        }
    }


    function get_assets(int_devid) {
        document.getElementById("hid_ACTION").value = "GETASSETS";
        document.RSLFORM.target = "frm_ten";
        document.RSLFORM.action = "../serverside/additionalasset_svr.asp?customerid=<%=CUSTOMERID%>&devid=" + int_devid + "&tenancyid=<%=tenancy_id%>";
        document.RSLFORM.submit();
    }


    function do_swap_tables() {
        swap_tables('table3');
    }


    function attach_asset(str_assetid, int_rent) {
        var answer
            answer = window.showModalDialog("../popups/asset_date.asp?MinStartDate=<%=FormatDateTime(SelectedTenancyStartDate,1)%>&rand=" + new Date(), "", "dialogHeight: 170px; dialogWidth: 250px;  center: Yes; help: No; resizable: No; status: No; scroll: no");
        if (answer == false) { return false; }
        else {
            document.getElementById("hid_ACTION").value = "ATTACH";
            document.RSLFORM.target = "frm_ten";
            document.RSLFORM.action = "../serverside/additionalasset_svr.asp?customerid=<%=CUSTOMERID%>&assetid=" + str_assetid + "&tenancyid=<%=tenancy_id%>&rent=" + int_rent + "&startdate=" + answer;
            document.RSLFORM.submit();
            parent.STARTLOADER('TOP')
            parent.swap_div(parent.MAIN_OPEN_BOTTOM_FRAME, 'bottom')
        }
    }


    function get_customer() {
        if (!checkForm()) return false;
        document.getElementById("hid_ACTION").value = "GETCUSTOMER";
        document.RSLFORM.target = "frm_ten";
        document.RSLFORM.action = "../serverside/findjoint_svr.asp?customerid=<%=CUSTOMERID%>&tenancyid=<%=tenancy_id%>";
        document.RSLFORM.submit();
    }


    function Set_Tenancy_Dates(int_customerid) {
        document.getElementById("hid_NEWCUSTOMERID").value = int_customerid
        if (int_customerid == "") {
            alert("This page has encountered a problem please contact the RSL Support Team")
        }
        else {
            FormFields[1] = "txt_STARTDATE|Start Date|DATE|Y"
            document.getElementById("CustomerFindRow").style.display = "none"
            document.getElementById("TitleBar").style.display = "none"
            document.getElementById("customerdata").style.display = "none"
            document.getElementById("TenancyDateEntryRow").style.display = "block"
        }

    }


    function attach_tenant() {
        if (!checkForm()) return false;
        if (window.confirm("Are you sure you wish to attach this customer this tenancy ?")) {
            int_customerid = document.getElementById("hid_NEWCUSTOMERID").value

            if (document.RSLFORM.txt_ISDEFAULT.checked == true)
                document.getElementById("hid_ISDEFAULT").value = 1
            else
                document.getElementById("hid_ISDEFAULT").value = 0

            document.getElementById("hid_ACTION").value = "ATTACH";
            document.RSLFORM.target = "frm_ten";
            document.RSLFORM.action = "../serverside/findjoint_svr.asp?customerid=<%=CUSTOMERID%>&tenantid=" + int_customerid + "&tenancyid=<%=tenancy_id%>";
            document.RSLFORM.submit();
        }
    }


    function add_occupant() {
        w = 475;
        h = 500;
        s = 'No';
        r = 'No';
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+s+',resizable = '+r+''
        window.open("../popups/poccupantdetails.asp?action=save&tenancyid=<%=tenancy_id%>", "window", settings)
    }

   function amend_occupant(customer_id) {

        w = 475;
        h = 500;
        s = 'No';
        r = 'No';
        LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
        TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
        settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+s+',resizable = '+r+''
        window.open("../popups/poccupantdetails.asp?action=amend&tenancyid=<%=tenancy_id%>&CustomerID=" + customer_id, "window", settings)
          
			
        }

    function remove_tenant(int_customerid) {
        event.cancelBubble = true
        var answer
        if (window.confirm("Are you sure you want to remove this tenant.")) {
            document.getElementById("hid_ACTION").value = "DELETE";
            document.RSLFORM.target = "";
            document.RSLFORM.action = "../serverside/findjoint_svr.asp?customerid=<%=CUSTOMERID%>&tenantid=" + int_customerid + "&tenancyid=<%=tenancy_id%>";
            document.RSLFORM.submit();
        }
    }


    function remove_occupant(int_customerid) {
        var answer
        if (window.confirm("Are you sure you want to remove this occupant.")) {
            document.getElementById("hid_ACTION").value = "DELETE";
            document.RSLFORM.target = "frm_ten";
            document.RSLFORM.action = "../ServerSide/occupant_svr.asp?customerid=" + int_customerid + "&tenancyid=<%=tenancy_id%>";
            document.RSLFORM.submit();
        }
    }


function OnloadDo()
{
	try {
		parent.STOPLOADER('TOP')
		}
	catch (e){
		temp = 1
		}
	if (<%=show_table%> == 3) 
		swap_tables('table3');
	else if (<%=show_table%> == 2) 
		swap_tables('table2');
}
    </script>
</head>
<body onload="OnloadDo()">
    <form name="RSLFORM" method="post" action="">
    <table width="750" style="height: 180px; border-right: SOLID 1PX #133E71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="68%" valign="top">
                <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                <input type="hidden" name="hid_ACTION" id="hid_ACTION" />
                <input type="hidden" name="hid_ISDEFAULT" id="hid_ISDEFAULT" />
                <div class="TA" style="overflow: auto; height: 173px;">
                    <div id="table1" style="display: none;">
                        <table width="100%" style="border: solid 1px #133e71; border-collapse: collapse"
                            cellpadding="3" cellspacing="0" border="1">
                            <tr bgcolor="#f5f5dc" style="color: black">
                                <td colspan="4" height="7px">
                                    <b>Selected Tenancy...</b>
                                </td>
                            </tr>
                            <tr bgcolor="#133E71" style="color: white">
                                <td colspan="3">
                                    <b>Tenancy Information</b>
                                </td>
                                <td align="right">
                                    <img title="View Tenancies" src="/myImages/up.gif" border="0" style="cursor: pointer"
                                        onclick="swap_tables('table4')" width="17" height="12" align="bottom" <%=REEDONLY%>
                                        alt="" />
                                    <img title="View Additional Tenants" src="/myImages/down.gif" border="0" style="cursor: pointer"
                                        onclick="swap_tables('table2')" width="17" height='12' align="bottom" <%=REEDONLY%>
                                        alt="" />
                                </td>
                            </tr>
                            <%=TenancyString%>
                            <tr>
                                <td colspan="5">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="table2" style="display: none;">
                        <table width="100%" hlcolor="#4682b4" slcolor="" style="border: solid 1px #133e71;
                            border-collapse: collapse; behavior: url(/Includes/Tables/tablehl.htc);" cellpadding="3"
                            cellspacing="0" border="1" class="TAB_TABLE">
                            <thead>
                                <tr bgcolor="#f5f5dc" style="color: black">
                                    <td colspan="5">
                                        <b>Additional Tenants</b>
                                    </td>
                                </tr>
                                <tr bgcolor="#133E71" style="color: white">
                                    <td>
                                        <b>Customer Ref</b>
                                    </td>
                                    <td>
                                        <b>Name</b>
                                    </td>
                                    <td>
                                        <b>Start</b>
                                    </td>
                                    <td>
                                        <b>End</b>
                                    </td>
                                    <td align="right">
                                        <img title="View Tenancy Information" src="/myImages/up.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table1')" width="17" height="12" align="bottom" alt="" />
                                        <img title="View Additonal Assets" src="/myImages/down.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table3')" width="17" height="12" align="bottom" alt="" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <%=str_joint%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="table3" style="display: none;">
                        <table width="100%" hlcolor="#4682b4" slcolor="" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border: solid 1px #133e71; border-collapse: collapse" cellpadding="3" cellspacing="0"
                            border="1" class="TAB_TABLE">
                            <thead>
                                <tr bgcolor="#f5f5dc" style="color: black">
                                    <td colspan="6">
                                        <b>Additional Assets</b>
                                    </td>
                                </tr>
                                <tr bgcolor="#133E71" style="color: white">
                                    <td>
                                        <b>Property Ref</b>
                                    </td>
                                    <td>
                                        <b>Type/No</b>
                                    </td>
                                    <td>
                                        <b>Start</b>
                                    </td>
                                    <td>
                                        <b>End</b>
                                    </td>
                                    <td>
                                        <b>Rent</b>
                                    </td>
                                    <td align="right">
                                        <img src='/myImages/up.gif' width="17" height="12" align="bottom" style="visibility: hidden"
                                            alt="" /><img title="View Additional Tenants" src='/myImages/up.gif' border="0" style="cursor: pointer"
                                                onclick="swap_tables('table2')" width="17" height="12" align="bottom" alt="" />
                                        <img title="View Additonal Occupants" src="/myImages/down.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table7')" width="17" height="12" align="bottom" alt="" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <%=str_addass%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="table4" style="display: block;">
                        <table width="100%" hlcolor="#4682b4" slcolor="" style="border: solid 1px #133e71;
                            border-collapse: collapse" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                            <thead>
                                <tr bgcolor="#f5f5dc" style='color: black'>
                                    <td colspan="7">
                                        <b>Tenancies</b>
                                    </td>
                                </tr>
                                <tr bgcolor="#133E71" style="color: white">
                                    <td width="80">
                                        <b>Tenancy Ref</b>
                                    </td>
                                    <td width="100">
                                        <b>Start Date</b>
                                    </td>
                                    <td width="100">
                                        <b>End Date</b>
                                    </td>
                                    <td width="50">
                                        <b>CBL</b>
                                    </td>
                                    <td width="100">
                                        <b>Tenancy Type</b>
                                    </td>
                                    <td align="right">
                                        <img title="View Tenancy Information" src="/myImages/down.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table1')" width="17" height="12" align="bottom" alt="" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <%=PreviousString%>
                                <%=NewTenancyButton%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td height="100%" colspan="6">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="table5" style="display: none;">
                        <table width="100%" style="border: solid 1px #133e71; border-collapse: collapse" cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                            <tr bgcolor="#133E71" style="color: white">
                                <td colspan="3">
                                    <b>Find Customer</b><input type="hidden" name="hid_NEWCUSTOMERID" id="hid_NEWCUSTOMERID" />
                                </td>
                            </tr>
                            <tr id="CustomerFindRow" style="display: block">
                                <td colspan="3">
                                    Enter Customer Name &nbsp;
                                    <input type="text" name="txt_CUSTOMERNAME" id="txt_CUSTOMERNAME" class="textbox200" />
                                    <img src="/js/FVS.gif" name="img_CUSTOMERNAME" id="img_CUSTOMERNAME" width="15px" height="15px" border="0"
                                        alt="" />
                                    <input type="button" class="rslbutton" value=" Submit " name="BtnSubmit1" id="BtnSubmit1"
                                        onclick="get_customer()" />
                                    <input type="button" class="rslbutton" value=" Back " name="BtnBack" id="BtnBack"
                                        onclick="swap_tables('table2')" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="100%" id="TenancyDateEntryRow" style="display: none">
                                        <tr>
                                            <td colspan="4">
                                                Please enter the Start date for this additional tenant.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100" nowrap="nowrap">
                                                Enter Start Date
                                            </td>
                                            <td width="120">
                                                <input type="text" name="txt_STARTDATE" id="txt_STARTDATE" class="textbox100" /><img
                                                    src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                                    border="0" alt="" />
                                            </td>
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="149">
                                                Corr. Address
                                            </td>
                                            <td width="140">
                                                <input type="checkbox" name="txt_ISDEFAULT" id="txt_ISDEFAULT" tabindex="11" value="1"
                                                    checked="checked" />
                                                <img src="/js/FVS.gif" name="img_ISDEFAULT" id="img_ISDEFAULT" width="15px" height="15px"
                                                    border="0" alt="" />
                                            </td>
                                            <td width="20">
                                                &nbsp;
                                            </td>
                                            <td width="151">
                                                <input type="button" class="rslbutton" name="BtnSubmit2" id="BtnSubmit2" value=" Submit "
                                                    onclick="attach_tenant()" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr bgcolor="#133e71" style="color: white; display: block" id="TitleBar">
                                <td width="200px">
                                    <b>Name</b>
                                </td>
                                <td width="100px">
                                    <b>DOB</b>
                                </td>
                                <td nowrap="nowrap">
                                    <b>Customer Type</b>
                                </td>
                            </tr>
                            <tr>
                                <td height="107" colspan="4" width="100%">
                                    <div id="customerdata" class="TA" style="overflow: auto; height: 100%; width: 100%">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table id="table6" cellpadding="0" cellspacing="0" border="0" width="100%" style="height: 100%;
                        display: none">
                        <tr>
                            <td>
                                <div id="ass_body">
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="table7" style="display: none">
                        <table width="100%" hlcolor="#4682b4" slcolor="" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border: solid 1px #133e71; border-collapse: collapse" cellpadding="3" cellspacing="0"
                            border="1" class="TAB_TABLE">
                            <thead>
                                <tr bgcolor="#f5f5dc" style="color: black">
                                    <td colspan="5">
                                        <b>Additional Occupants</b>
                                    </td>
                                </tr>
                                <tr bgcolor="#133e71" style="color: white">
                                    <td>
                                        <b>Customer Ref</b>
                                    </td>
                                    <td>
                                        <b>Name</b>
                                    </td>
                                    <td>
                                        <b>Start</b>
                                    </td>
                                    <td>
                                        <b>End</b>
                                    </td>
                                    <td align="right">
                                        <img title="View Additional Assets" src="/myImages/up.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table3')" width="17" height="12" align="bottom" alt="View Additional Assets" />
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="theoccupantinfo">
                                <%=str_occup%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td height="100%" colspan="5">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </td>
            <td width="32%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>                
            </td>
        </tr>
    </table>
    </form>        
    <iframe src="/secureframe.asp" name="frm_ten" id="frm_ten" width="4px" height="4px"
        style="display: none"></iframe>
</body>
</html>
