<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id, Defects_Title, last_status

	journal_id = Request("journalid")
	nature_id = Request("natureid")	

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, P.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), P.LASTACTIONDATE, 108) ,'') AS CREATIONDATE, " &_
					"			ISNULL(P.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, P.DEFECTSHISTORYID, O.NAME, " &_
					"			S.DESCRIPTION AS STATUS, " &_
					"			A.DESCRIPTION AS ACTION " &_
					"FROM		P_DEFECTS P " &_
					"			LEFT JOIN C_STATUS S ON P.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_ACTION A ON P.ITEMACTIONID = A.ITEMACTIONID  " &_
					"			LEFT JOIN S_ORGANISATION O ON P.CONTRACTOR = O.ORGID " &_
					"			LEFT JOIN C_JOURNAL J ON P.JOURNALID = J.JOURNALID " &_
					"WHERE		P.JOURNALID = " & journal_id &_
					"ORDER 		BY DEFECTSHISTORYID DESC "

		Call OpenRs (rsSet, strSQL) 

		str_journal_table = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				repair_history_id = rsSet("DEFECTSHISTORYID")
				last_status = rsSet("STATUS")
			End If
				str_journal_table = str_journal_table & 	"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td>" & rsSet("ACTION") & "</td>" &_
															"<td>" & rsSet("NOTES") & "</td>" &_
															"<td>" & rsSet("NAME")  & "</td>" &_
														"<tr>"
			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist for this defect.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

        function update_repair(int_repair_history_id) {
            var str_win
            str_win = "../PopUps/pRepair.asp?repairhistoryid=" + int_repair_history_id + "&natureid=<%=nature_id%>";
            window.open(str_win, "display", "width=407,height=280, left=200,top=200");
        }

    </script>
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="4">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Defects Information:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" class="RSLBUTTON" value=" Update " title="Update"
                                    style="background-color: #f5f5dc" onclick="update_repair(<%=repair_history_id%>)"
                                    disabled="disabled" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="120px">
                    Date
                </td>
                <td style="border-bottom: 1px solid" width="130px">
                    Action
                </td>
                <td style="border-bottom: 1px solid" width="300px">
                    Notes
                </td>
                <td style="border-bottom: 1px solid" width="200">
                    Contractor
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
