<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, training_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText

	journal_id = Request("journalid")
	nature_id = Request("natureid")
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'') As CREATIONDATE " &_
					"			, ISNULL(G.NOTES, 'N/A') As NOTES " &_
					"			, G.TRAININGHISTORYID " &_
					"           , E.FIRSTNAME + ' ' + E.LASTNAME As FULLNAME " &_
                    "           , B.FIRSTNAME + ' ' + B.LASTNAME AS CREATEDBY " &_                    
					"			, G.ITEMSTATUSID " &_
					"           , S.DESCRIPTION As STATUS " &_
					"			, (SELECT COUNT(*) FROM C_TRAININGCUSTOMERLETTERS WHERE TRAININGHISTORYID = G.TRAININGHISTORYID) As LETTERCOUNT " &_
                    "           , NST.[DESCRIPTION] As Title " &_
                    "           , NA.[DESCRIPTION] As ACTIVITY " &_
					"FROM		C_TRAININGEMPLOYMENT G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
                    "			LEFT JOIN E__EMPLOYEE B ON B.EMPLOYEEID = G.LASTACTIONUSER " &_ 
					"			LEFT JOIN C_LETTERACTION AA ON AA.ACTIONID = G.ITEMACTIONID " &_
                    "           LEFT JOIN C_NATURESUBTYPE NST ON G.NATURESUBTYPEID = NST.NATURESUBTYPEID " &_
                    "           LEFT JOIN C_NATUREACTIVITY NA ON G.NATUREACTIVITYID = NA.NATUREACTIVITYID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY TRAININGHISTORYID DESC "

		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				training_history_id = rsSet("TRAININGHISTORYID")
				last_status = rsSet("STATUS")
				If (rsSet("ITEMSTATUSID") = 14) Then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				End If
			End If
				Notes = rsSet("NOTES")
				If (Notes = "" OR isNULL(Notes)) Then
					ResponseNotes = "[Empty Notes]"
				ElseIf (Notes = PreviousNotes) Then
					ResponseNotes = "[Same As Above]"
				Else
					ResponseNotes = Notes
				End If
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<td width=""120"">" & rsSet("CREATIONDATE") & "</td>" &_
															"<td nowrap=""nowrap"">" & rsSet("ACTIVITY") & "</td>" &_
															"<td>" & ResponseNotes & "</td>" &_
															"<td>" & rsSet("FULLNAME")  & "</td>"&_
                                                            "<td>" & rsSet("CREATEDBY")  & "</td>"
				If rsSet("LETTERCOUNT") > 0 Then
					str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" style=""cursor:pointer"" alt=""Open Letter"" onclick=""open_letter("& rsSet("TRAININGHISTORYID") &")"" /></td>"
				Else
					str_journal_table = str_journal_table & "<td>&nbsp;</td>"
				End If

				str_journal_table = str_journal_table & "</td><tr>"

			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""5"" align=""center"">No journal entries exist for this Training & Employment Enquiry.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Training & Employment Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_training(int_training_history_id){
		var str_win
		    str_win = "../PopUps/pTraining.asp?traininghistoryid="+int_training_history_id+"&natureid=<%=nature_id%>" ;
		    window.open(str_win,"display","width=407,height=365, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	function open_letter(letter_id){
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		    window.open("../Popups/training_letter_plain.asp?tenancyid="+tenancy_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	}

    </script>
</head>
<body onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        border="0">
        <thead>
            <tr>
                <td colspan="5">
                    <table cellspacing="5" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" title="<%=ButtonText%>"
                                    value="<%=ButtonText%>" style="background-color: #f5f5dc; cursor: pointer" onclick="update_training(<%=training_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="border-bottom: 1px solid"></td>
            </tr>
            <tr>
                <td >
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td>
                    <font color="blue"><b>Activity:</b></font>
                </td>
                <td>
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td>
                    <font color="blue"><b>Assigned To:</b></font>
                </td>
                <td>
                    <font color="blue"><b>Created By:</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%>
        </tbody>
    </table>
</body>
</html>
