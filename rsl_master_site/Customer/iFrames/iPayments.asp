<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750"" HEIGHT=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim tenancy_id, str_sql, customer_id, recharge_option

	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	str_holiday = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/repairs.gif"" alt="""" /></td></tr><tr><td> 00 days available</td></tr><tr><td> 00 days boooked  <b>Book Now!</b></td></tr></table>"

	str_policy = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/rents.gif"" alt="""" /></td></tr><tr><td> Policy Handbook</td></tr><tr><td> Procedure Handbook</td></tr></table>"
	Call OpenDB()

	str_sql = 	"SELECT		DDSCHEDULEID, " &_
				"			'Direct Debit' AS PAYMENTTYPE, " &_
				"			ISNULL(T.DESCRIPTION,'') AS ITEMTYPE, " &_
				"			ISNULL(ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
				"			ISNULL(REGULARPAYMENT,0) AS REGULARPAYMENT, " &_
				"			CASE WHEN SUSPEND = 1 THEN 'Inactive' WHEN SUSPEND = '0' THEN (CASE WHEN TEMPSUSPEND = 1 THEN 'Suspended' ELSE 'Active' END) ELSE " &_
				"			'N/A' END AS SUSPEND " &_
				"FROM		F_DDSCHEDULE D " &_
				"			LEFT JOIN F_ITEMTYPE T ON D.ITEMTYPE = T.ITEMTYPEID " &_
				"WHERE		TENANCYID = " & tenancy_id
	Call OpenRs(rsSet, str_sql)

	str_sql = 	"SELECT		SOSCHEDULEID, " &_
				"			'Standing Order' AS PAYMENTTYPE, " &_
				"			ISNULL(T.DESCRIPTION,'') AS ITEMTYPE, " &_
				"			ISNULL(ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
				"			ISNULL(AMOUNT,0) AS REGULARPAYMENT, " &_
				"			CASE WHEN SUSPEND = 1 THEN 'Inactive' WHEN SUSPEND = '0' THEN 'Active' ELSE " &_
				"			'N/A' END AS SUSPEND " &_
				"FROM		F_SOSCHEDULE D " &_
				"			LEFT JOIN F_ITEMTYPE T ON D.ITEMTYPE = T.ITEMTYPEID " &_
				"WHERE		TENANCYID = " & tenancy_id
	Call OpenRs(rsSet2, str_sql)

	str_sql = 	"SELECT		BANKDETAILSID, " &_
				"			'Bank Details' AS PAYMENTTYPE, " &_
				"			ISNULL(ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
				"			CASE WHEN ACTIVE = 1 THEN '<FONT COLOR=RED>Active</FONT>' WHEN ACTIVE = 0 THEN 'Inactive' ELSE " &_
				"			'N/A' END AS ACTIVE " &_
				"FROM		F_BANKDETAILS D " &_
				"WHERE		CUSTOMERID = " & customer_id
	Call OpenRs(rsSet3, str_sql)

	str_sql = 	"SELECT *, CASE WHEN ACTIVE = 1 THEN 'Active' ELSE 'Suspended' END AS DETEXT FROM F_SDPTOTENANCY WHERE TENANCYID = "  & tenancy_id & " ORDER BY SDPID"
	Call OpenRs(rsSet4, str_sql)

	If check_for_current_recharge_card() Then
		recharge_option = ""
	Else
		recharge_option = "<option value=""iRechargeCard.asp"">Recharge Card</option>"
	End If

	Function check_for_current_recharge_card()

		SQL = "SELECT * FROM F_SDPTOTENANCY WHERE ACTIVE = 1 AND TENANCYID = " & tenancy_id
		Call OpenRs(rsSetRC, SQL)
		If not rsSetRC.EOF Then
			check_for_current_recharge_card = True
			Call CloseRs(rsSetRC)
			Exit Function
		Else
			check_for_current_recharge_card = False
			Call CloseRs(rsSetRC)
			Exit Function
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        function go_sched(str_redir, row_id) {
            location.href = str_redir + "?rowid=" + row_id + "&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
        }


        function sel_change() {
            location.href = RSLFORM.sel_TYPE.value + "?action=NEW&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>";
        }


        function stopLoader() {
            try {
                parent.STOPLOADER('TOP')
            }
            catch (e) {
                temp = 1
            }
        }

    </script>
</head>
<body onload="stopLoader()">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="height: 180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td valign="top" width="70%" height="100%">
                <div class="TA" style="overflow: auto; height: 173px;">
                    <table border="1" width="100%" slcolor="" hlcolor="#4682b4" style="behavior: url(/Includes/Tables/tablehl.htc);
                        border-collapse: collapse; border: solid 1px #133e71" cellpadding="3" cellspacing="0">
                        <thead>
                            <tr bgcolor="#f5f5dc">
                                <td colspan="5">
                                    <b>New Item</b>&nbsp;
                                    <select name="sel_TYPE" id="sel_TYPE" class="TEXTBOX" style="width: 120px" onchange='sel_change()'>
                                        <option value=''>Select</option>
                                        <%=recharge_option%>
                                        <option value='iBankDetails.asp'>Bank Details</option>
                                        <option value='iDDPayment.asp'>Direct Debit</option>
                                        <option value='iSOPayment.asp'>Standing Order</option>
                                    </select>
                                </td>
                            </tr>
                            <tr bgcolor="#133E71" style="color: white">
                                <td>
                                    <b>Item Type</b>
                                </td>
                                <td>
                                    <b>Payment Type</b>
                                </td>
                                <td>
                                    <b>Account Name</b>
                                </td>
                                <td>
                                    <b>Amount</b>
                                </td>
                                <td>
                                    <b>Status</b>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <% While Not rsSet.EOF %>
                            <tr onclick="go_sched('iDDPayment.asp',<%=rsSet("DDSCHEDULEID")%>)" style="cursor: pointer">
                                <td>
                                    <%=rsSet("ITEMTYPE")%>
                                </td>
                                <td>
                                    <%=rsSet("PAYMENTTYPE")%>
                                </td>
                                <td>
                                    <%=rsSet("ACCOUNTNAME")%>
                                </td>
                                <td>
                                    �<%=FormatNumber(rsSet("REGULARPAYMENT"))%>
                                </td>
                                <td>
                                    <%=rsSet("SUSPEND")%>
                                </td>
                            </tr>
                            <% 	rsSet.movenext() 
				   Wend 
                            %>
                            <% While Not rsSet2.EOF %>
                            <tr onclick="go_sched('iSOPayment.asp',<%=rsSet2("SOSCHEDULEID")%>)" style="cursor: pointer">
                                <td>
                                    <%=rsSet2("ITEMTYPE")%>
                                </td>
                                <td>
                                    <%=rsSet2("PAYMENTTYPE")%>
                                </td>
                                <td>
                                    <%=rsSet2("ACCOUNTNAME")%>
                                </td>
                                <td>
                                    �<%=FormatNumber(rsSet2("REGULARPAYMENT"))%>
                                </td>
                                <td>
                                    <%=rsSet2("SUSPEND")%>
                                </td>
                            </tr>
                            <% 	rsSet2.movenext() 
				   Wend 
                            %>
                            <% While Not rsSet3.EOF %>
                            <tr onclick="go_sched('iBankDetails.asp',<%=rsSet3("BANKDETAILSID")%>)" style="cursor: pointer">
                                <td>
                                    N/A
                                </td>
                                <td>
                                    <%=rsSet3("PAYMENTTYPE")%>
                                </td>
                                <td>
                                    <%=rsSet3("ACCOUNTNAME")%>
                                </td>
                                <td>
                                    N/A
                                </td>
                                <td>
                                    <%=rsSet3("ACTIVE")%>
                                </td>
                            </tr>
                            <% 	rsSet3.movenext() 
				   Wend 
                            %>
                            <% While Not rsSet4.EOF %>
                            <tr onclick="go_sched('irECHARGEcARD.asp',<%=rsSet4("SDPID")%>)" style="cursor: pointer">
                                <td>
                                    Recharge
                                </td>
                                <td>
                                    Payment Card
                                </td>
                                <td>
                                    N/A
                                </td>
                                <td>
                                    N/A
                                </td>
                                <td>
                                    <%=rsSet4("DETEXT")%>
                                </td>
                            </tr>
                            <% 	rsSet4.movenext() 
				   Wend 
                            %>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td height="100%" colspan="5">
                                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>    
</body>
</html>
<% Call CloseRs(rsSet) %>
<% Call CloseRs(rsSet2) %>
<% Call CloseRs(rsSet3) %>
