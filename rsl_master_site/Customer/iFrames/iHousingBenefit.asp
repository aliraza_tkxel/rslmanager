<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
TenancyID = Request("tenancyid")
CustomerID = Request("CustomerID")

TableString = ""
AllowNewButton = "YES"

SQL = "SELECT HB.*, LA.DESCRIPTION AS LOCALAUTHORITY FROM F_HBINFORMATION HB " &_
		"LEFT JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = HB.CUSTOMERID AND CT.TENANCYID = HB.TENANCYID " &_
		"LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
		"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
		"LEFT JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
		"LEFT JOIN G_LOCALAUTHORITY LA ON D.LOCALAUTHORITY = LA.LOCALAUTHORITYID " &_
		"WHERE HB.CUSTOMERID = " & CustomerID & " AND HB.TENANCYID = " & TenancyID & " ORDER BY ACTUALENDDATE DESC"

Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	While (NOT rsLoader.EOF)
		iEnd = rsLoader("ACTUALENDDATE")
		If (iEnd = "" OR isNull(iEnd)) Then
			iEnd = "Still in progress..."
			AllowNewButton = "NO"
		End If
		TableString = TableString & "<tr style=""cursor:pointer"" onclick=""javascript:location.href='iHBDetails.asp?HBID=" & rsLoader("HBID") & "&TenancyID=" & TenancyID & "&CustomerID=" & CustomerID & "'""><TD>" & rsLoader("HBREF") & "</TD><TD>" & rsLoader("INITIALSTARTDATE") & "</TD><TD>" & iEnd & "</TD><TD>" & rsLoader("LOCALAUTHORITY") & "</TD></TR>"
		rsLoader.moveNext
	wend
Else
	TableString = "<tr><td colspan=""4"" align=""center"">No HB is setup for this Customer/Tenancy</td></tr>"
End If
Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Housing Benefit</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="height:180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="70%" height="100%" valign="top">
                <table id="table1" width="100%" style="behavior: url(/Includes/Tables/tablehl.htc);
                    border: solid 1px #133E71; border-collapse: collapse" slcolor="" hlcolor="#4682b4"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <thead>
                        <tr bgcolor="#133E71" style="color: white">
                            <td width="100">
                                <b>Claim No.</b>
                            </td>
                            <td width="100">
                                <b>Start Date</b>
                            </td>
                            <td width="100">
                                <b>End Date</b>
                            </td>
                            <td>
                                <b>Local Authority</b>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <%=TableString%>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" height="100%" valign="bottom" align="right">
                                <% if AllowNewButton = "YES" then %>
                                <input type="button" class="RSLButton" value=" New HB Schedule " title="New HB Schedule"
                                    style="cursor: pointer" onclick="javascript:parent.location.href = '../HousingBenefit.asp?TenancyID=<%=TenancyID%>&CustomerID=<%=CustomerID%>'" />
                                <% end if %>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
