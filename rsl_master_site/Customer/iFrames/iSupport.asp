<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750"" HEIGHT=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim pd_name
	Dim pd_dob
	Dim pd_gender
	Dim pd_marital
	Dim pd_ethnicity
	Dim pd_bank
	Dim pd_sortcode
	Dim pd_accnumber
	Dim pd_accname

	str_holiday = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/repairs.gif"" alt="""" /></td></tr><tr><td> 00 days available</td></tr><tr><td> 00 days boooked  <b>Book Now!</b></td></tr></table>"

	str_policy = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/rents.gif"" alt="""" /></td></tr><tr><td> Policy Handbook</td></tr><tr><td> Procedure Handbook</td></tr></table>"
	Call OpenDB()
	Call get_main_details()
	Call CloseDB()

	' get employee details
	Function get_main_details()

		Dim strSQL
		strSQL = 	"SELECT		E.EMPLOYEEID, " &_
					"			LEFT(E.FIRSTNAME,1) + ' ' + E.LASTNAME AS FULLNAME," &_
					"			ISNULL(CONVERT(NVARCHAR, E.DOB, 103), '') AS DOB," &_
					"			ISNULL(E.GENDER, '') AS GENDER," &_
					"			ISNULL(M.DESCRIPTION, '') AS MARITALSTATUS," &_
					"			ISNULL(ETH.DESCRIPTION, '') AS ETHNICITY," &_
					"			ISNULL(E.BANK, '') AS BANK," &_
					"			ISNULL(E.SORTCODE, '') AS SORTCODE," &_
					"			ISNULL(E.ACCOUNTNUMBER, '') AS ACCOUNTNUMBER, " &_
					"			ISNULL(E.ACCOUNTNAME, '') AS ACCOUNTNAME " &_
					"FROM		E__EMPLOYEE E " &_
					"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
					"			LEFT JOIN G_MARITALSTATUS M ON E.MARITALSTATUS = M.MARITALSTATUSID"  &_
					"			LEFT JOIN G_ETHNICITY ETH ON E.ETHNICITY = ETH.ETHID " &_
					"WHERE		E.EMPLOYEEID = " & Session("userid")

		Call OpenRs (rsSet,strSQL)

		' personal details section
		pd_name			= rsSet("FULLNAME")
		pd_dob			= rsSet("DOB")
		pd_gender		= rsSet("GENDER")
		pd_marital		= rsSet("MARITALSTATUS")
		pd_ethnicity	= rsSet("ETHNICITY")
		pd_bank			= rsSet("BANK")
		pd_sortcode		= rsSet("SORTCODE")
		pd_accnumber	= rsSet("ACCOUNTNUMBER")
		pd_accname		= rsSet("ACCOUNTNAME")

		Call CloseRs(rsSet)

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Supporting People</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="parent.STOPLOADER('TOP')">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="height: 180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="70%" height="100%">
                <table width="100%" style="height: 100%; border: solid 1px #133e71; border-collapse: COLLAPSE"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td align="center">
                            <font style="font-size: 16px"><b>Please make next selection.</b></font>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
