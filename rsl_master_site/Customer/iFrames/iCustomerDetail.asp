<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include file ="Customer/Pages/CustomerDetail.aspx" --> 
<%
	Call OpenDB()

	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")

<!-- code changes start by TkXel=========================================================================== -->

	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: 	June 11, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim enqID, selItem

	' URL will contain EnquiryID, if request generated from Enquiry Management Area (Create CRM button)
	' This frame is displayed as party of BOTTOM FRAME of CRM.asp
	enqID = Request("EnquiryID")

  	If enqID <>"" OR Len(enqID) > 0 Then

		Call BuildSelect(lst_item, "sel_ITEM", "C_ITEM WHERE ITEMID = 4 ", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange=""item_change()"" ")

<!-- code changes end by TkXel=========================================================================== -->


	ElseIf (tenancy_id = "" OR tenancy_id = "-1") Then
		'filter out tenant items
		Call BuildSelect(lst_item, "sel_ITEM", "C_ITEM WHERE ITEMID = 3 ", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange=""item_change()"" ")
	Else
		'give a full list of stuff...
		Call BuildSelect(lst_item, "sel_ITEM", "C_ITEM WHERE ITEMID NOT IN (4)", "ITEMID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange=""item_change()"" ")
	End If

	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        legend
        {
            display: none;
        }
        
        fieldset
        {
            border: 0px;
            margin: 5px, 0px, 0px, 5px;
        }
        
        div.row
        {
            clear: both;
            padding-top: 2px;
            width: 100%;
            line-height: 1.8;
        }
        
        div.row span.label
        {
            float: left;
            text-align: left;
            font-weight: bold;
            width: 20%; /*text-indent: 5px;*/
            white-space: nowrap;
        }
        
        div.row span.formw
        {
            float: left;
            width: 80%;
            text-align: left;
        }
        
        div.row span.form_rdo
        {
            float: left;
            width: 54%; /*margin-left: 7px;
            padding-top: 3px;
            padding-right: 2px;*/
            border-bottom: 1px solid silver;
        }
        
        div.row input.radio
        {
            float: left;
            width: 90%;
            text-align: left;
            border: 0px solid red; /*margin-left: 2px;*/ /*font-family: Tahoma, Arial, Helvetica, sans-serif;*/ /*font-size: 1.0em;*/
            color: #003366;
        }
        
        div.row input, select, textarea
        {
            float: left;
            width: 85%;
            text-align: left; /*font-family: Tahoma, Arial, Helvetica, sans-serif;*/ /*font-size: 1.0em;*/
            color: #003366;
        }
        
        div.row input.submit
        {
            text-align: center;
            width: auto;
            cursor: pointer;
        }
        
        input[type=button], input[type=submit], input[type=reset], input[type=checkbox]
        {
            background-color: #ffffff;
            border: 1px solid #133e71;
            color: #133e71;
            cursor: pointer;
            width: auto; /*padding: 5px;*/
        }
        
        input[type=button]:hover, input[type=submit]:hover
        {
            background-color: #133e71;
            color: #ffffff;
        }
        
        input[disabled], input[disabled]:hover
        {
            background-color: #999999;
            color: #fff;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_TITLE|Title|TEXT|N"
        FormFields[1] = "sel_ITEM|Item|SELECT|Y"
        FormFields[2] = "sel_NATURE|Nature|SELECT|Y"

        function item_change() {
            document.RSLFORM.target = "frm_erm_srv";
            document.RSLFORM.action = "../serverside/ItemChange_srv.asp?itemid=" + document.getElementById("sel_ITEM").options[document.getElementById("sel_ITEM").selectedIndex].value;
            document.RSLFORM.submit();
        }

        // code changes start by TkXel ===========================================================================

        function goto_framechange_crm_srv(enquiryID) {
            //This fucntion gets called on onLoad event of this form
            var strEnqID = enquiryID + "";
            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {
                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
                document.RSLFORM.target = "frm_erm_srv";
                // passing enquiry id to FrameChangeCRM_srv.asp to pre-select dropdown values based on type of enquiry
                // of item and nature which are part of this frame. i.e. INewItem
                document.RSLFORM.action = "../serverside/FrameChangeCRM_srv.asp?EnquiryID=" + strEnqID;
                document.RSLFORM.submit();
            }
        }

        // code changes end by TkXel =============================================================================


        // build array for item pages (only property at the mo)
        var iFrameArray = new Array("",
							"Defects", //1
							"Repairs", //2
							"blank",
							"GeneralLetter",
							"Adaptation", //5
							"TransferExchange",
							"TransferExchange",
							"ASB",
							"ServiceComplaints_new",
							"Arrears", //10
							"General",
							"General",
							"General",
							"General",
							"", //15
							"",
							"General",
							"General",
							"",
							"Repairs", //20
							"Repairs",
							"",
							"Inspection",
							"ASB",
							"ASB", //25
							"GeneralLetter",
							"termination",
							"GeneralLetter",
							"GeneralLetter",
							"", //30
							"DangerRating",
							"", "", "", "", "", "", "", "", // 32 - 39
							"blank", "blank", "", "baddebtwriteoff", //40 - 43
							"baddebtwriteoff", "", "General",
							"", "", "",

        //Code Changes Start By TkXel ================================================
        // These frames would be selected against TenantsOnline option
							"Termination", "ServiceComplaints_new", "GarageParking", //50-52

        //Code Changes End By TkXel ==================================================
							"", "", "", "", "", "", "General", "", "Vulnerability", "", "Risk", "TrainingEmployment", "TenancyAgreement", "SMS", "", "CustomerDetail"

							)


        function go_nature() {

            FormFields[0] = "txt_TITLE|Title|TEXT|N"

            if ((document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value == 64) || (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value == 27)) {
                if (document.getElementById('sel_SUBNATURE') != undefined) {
                    FormFields[3] = "sel_SUBNATURE|Type|SELECT|Y"
                }
            }
            else {
                if (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 2 && document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 20 && document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value != 21)
                    FormFields[0] = "txt_TITLE|Title|TEXT|Y"

                if (FormFields.length = 4) {
                    FormFields.length = FormFields.length - 1
                }
            }
            if (!checkForm()) return false;

            var nature_id, item_id, title, SubNature_id
            nature_id = document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value;
            item_id = document.getElementById("sel_ITEM").options[document.getElementById("sel_ITEM").selectedIndex].value;

            SubNature_id = ""
            if (document.getElementById('sel_SUBNATURE') != undefined) {
                SubNature_id = document.getElementById("sel_SUBNATURE").options[document.getElementById("sel_SUBNATURE").selectedIndex].value;
            }

            title = document.getElementById("txt_TITLE").value;
            customer_id = "<%=customer_id%>"
            tenancy_id = "<%=tenancy_id%>"
            if (document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value == "")
                frm_nature.location.href = "blank.asp";
            else {
                frm_nature.location.href = "i" + iFrameArray[nature_id] + ".asp?natureid=" + nature_id + "&itemid=" + item_id + "&title=" + title + "&customerid=" + customer_id + "&tenancyid=" + tenancy_id + "&typeid=" + SubNature_id;
            }
        }


        function setTitle() {
            if (document.getElementById("sel_NATURE").value == "")
                document.getElementById("txt_TITLE").value = ""
            else
                document.getElementById("txt_TITLE").value = document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].text;
        }

        function setTitleOrListBox(val) {
            var nature_id = document.getElementById("sel_NATURE").options[document.getElementById("sel_NATURE").selectedIndex].value;
            frm_erm_srv.location.href = "../serverside/NatureChange_srv.asp?natureid=" + nature_id
        }

    </script>
</head>
<body onload="parent.STOPLOADER('BOTTOM');goto_framechange_crm_srv(<%=enqID%>)" class="TA">
    <div style="float:left; width:100%">
        <div style="float:left; width:40%">
            <form name="RSLFORM" method="post" action="">
            <fieldset>
                <legend>Create a new login to the Members' Directory</legend>
                <div class="row">
                    <span class="label">
                        <label for="sel_ITEM" accesskey="I" style="cursor: pointer">
                            <span style="text-decoration: underline">I</span>tem:
                        </label>
                    </span><span class="formw">
                        <%=lst_item%><img src="/js/FVS.gif" name="img_ITEM" id="img_ITEM" width="15px" height="15px"
                            border="0" alt="" />
                    </span>
                </div>
                <div class="row">
                    <span class="label">
                        <label for="sel_NATURE" accesskey="N" style="cursor: pointer">
                            <span style="text-decoration: underline">N</span>ature:
                        </label>
                    </span><span class="formw"><span id="dvNature">
                        <select name="sel_NATURE" id="sel_NATURE" class="textbox200">
                            <option value=''>Select Item</option>
                        </select>
                    </span>
                        <img src="/js/FVS.gif" name="img_NATURE" id="img_NATURE" width="15px" height="15px"
                            border="0" alt="" /></span>
                </div>
                <div class="row" id="dvSubNature" style="display:none">
                    <span class="label"></span><span class="formw"></span>
                </div>
                <div class="row" id="dvTitle">
                    <span class="label">
                        <label for="txt_TITLE" accesskey="T" style="cursor: pointer">
                            <span style="text-decoration: underline">T</span>itle:
                        </label>
                    </span><span class="formw">
                        <input type="text" name="txt_TITLE" id="txt_TITLE" maxlength="200" size="10" class="textbox200" />
                        <span>
                            <img src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px"
                                border="0" alt="" /></span></span>
                </div>
                <div class="row">
                    <span class="label">&nbsp;</span><span class="formw">
                        <input type="button" name="BTN_DATES" id="BTN_DATES" value="Next" title="Next" onclick="go_nature()"
                            style="width: auto; float: right; margin-right:15px; text-align:center" class="RSLButton" />
                    </span>
                </div>
                <div class="row" style="width: auto">
                    A title is not necessary when creating a Repair, as the repair details will become
                    the title itself.
                    <!--#include virtual="includes/Bottoms/BlankBottom.html" -->
                </div>
            </fieldset>
            </form>
        </div><div style="float:left; width:60%;">
            <iframe src="/secureframe.asp" name='frm_nature' width="100%" height="100%" frameborder="0"
                style="border: none"></iframe>
        </div>
    </div>
    <iframe src="/secureframe.asp" name="frm_erm_srv" id="frm_erm_srv" width="4px"
        height="4px" style="display: none"></iframe>
</body>
</html>


