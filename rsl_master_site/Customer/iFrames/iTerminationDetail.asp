<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id
	Dim Defects_Title, last_status, ButtonDisabled, ButtonText, ResponseNotes

	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'') AS CREATIONDATE " &_
					"			, G.NOTES " &_
					"			, G.TERMINATIONDATE " &_
					"			, G.TERMINATIONHISTORYID " &_
                    "			, R.DESCRIPTION + ' ' + COALESCE(TRA.DESCRIPTION,'') + ' ' + COALESCE(J.TITLE,'') As TITLE" &_
					"			, J.PROPERTYID " &_
                    "			, J.TENANCYID " &_
                    "			, R.DESCRIPTION " &_
					"			, G.ITEMSTATUSID " &_
                    "			, S.DESCRIPTION AS STATUS " &_
                    "           , IsNull(TRA.DESCRIPTION,'No Option Available/Selected') AS ReasonCategory " &_
                    "           , B.FIRSTNAME + ' ' + B.LASTNAME AS CREATEDBY , ISNULL(G.RELETDATE,CONVERT(DATETIME, DATEADD(DAY,7,G.TERMINATIONDATE))) AS RELETDATE " &_
					"FROM		C_TERMINATION G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN C_TERMINATION_REASON R ON G.REASON = R.REASONID " &_
                    "			LEFT JOIN E__EMPLOYEE B ON B.EMPLOYEEID = G.LASTACTIONUSER " &_  
                    "           LEFT JOIN C_TERMINATION_REASON_ACTIVITY TRA ON G.REASONCATEGORYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY TERMINATIONHISTORYID DESC "

		Call OpenRs (rsSet, strSQL)
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			ResponseNotes = ""
			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				general_history_id = rsSet("TERMINATIONHISTORYID")
				last_status = rsSet("STATUS")
				If (rsSet("ITEMSTATUSID") = 14) Then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				End If
			End If
				Notes = rsSet("NOTES")
				If (Notes = "" OR isNULL(Notes)) Then Notes = "[Empty Notes]"  End If
				ResponseNotes = ResponseNotes & "<b>Termination Date</b> : " & rsSet("TERMINATIONDATE") & "<br/>"
                ResponseNotes = ResponseNotes & "<b>Relet Date</b> : " & rsSet("RELETDATE") & "<br/>"
				ResponseNotes = ResponseNotes & "<b>Tenancyid</b> : " & rsSet("TENANCYID") & "<br/>"
				ResponseNotes = ResponseNotes & "<b>Propertyid</b> : " & rsSet("PROPERTYID") & "<br/>"
				ResponseNotes = ResponseNotes & "<b>Reason</b> : " & rsSet("DESCRIPTION") & "<br/>"
                ResponseNotes = ResponseNotes & "<b>Category</b> : " & rsSet("ReasonCategory") & "<br/><br/>"
				ResponseNotes = ResponseNotes & Notes
				str_journal_table = str_journal_table & 	"<td colspan=""3"">" &_
															"<table width""100%""><tr><td>" &_
															"<td width=""210"" valign=""top"">" & rsSet("CREATIONDATE") & "</td>" &_
															"<td width=""290"">" & ResponseNotes & "</td>" &_
                                                            "<td width=""120"" valign=""top"">" & rsSet("CREATEDBY") & "</td>" &_                                                     
															"</td></tr></table>" &_
															"</td><TR>"
			rsSet.movenext()
			
		Wend

		Call CloseRs(rsSet)

		IF cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""3"" align=""center"">No journal entries exist for this Termination Item.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_general(int_term_history_id){
		var str_win
		str_win = "../PopUps/pTermination.asp?historyid="+int_term_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=427,height=325, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

<%
error_mess = Request("error_code")
If error_mess <> "0" And error_mess <> "" Then
	RW "alert(""" & error_mess & """)"
End If
%>
    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        border="0">
        <thead>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" title="<%=ButtonText%>"
                                    value="<%=ButtonText%>" style="background-color: #f5f5dc; cursor: pointer" onclick="update_general(<%=general_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid;padding-left: 8px;" nowrap="nowrap" width="110px">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="150px">
                    <font color="blue"><b>Details:</b></font>
                </td>
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="120px">
                    <font color="blue"><b>Created By:</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
