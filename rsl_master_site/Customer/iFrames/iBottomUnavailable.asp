<%@  language="VBSCRIPT" codepage="1252" %>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
</head>
<body onload="parent.STOPLOADER('BOTTOM')" class='TA'>
    <table width="100%" height="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0" hlcolor="#4682b4">
        <tr>
            <td align="center" colspan="6">
                <font style='font-size: 16px'><b>A tenancy must be selected before this area will be
                    enabled.</b></font>
            </td>
        </tr>
    </table>
</body>
</html>
