<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table
	
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")	

	CurrentWorkOrder = Request("CWO")

	OpenDB()
	build_work_orders()
	CloseDB()

	'			"	WHEN 20 THEN 'iRepairDetail.asp'  " &_
	'			"	WHEN 21 THEN 'iRepairDetail.asp'  " &_
	'			"	WHEN 22 THEN 'iRepairDetail.asp'  " &_		
	Function build_work_orders()
		//THIS PART BUILD THE WORK ORDERS
		cnt = 0
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WOM.BIRTH_MODULE_IMAGE, WOM.BIRTH_MODULE_DESCRIPTION, WOE.BIRTH_ENTITY_IMAGE, WOE.BIRTH_ENTITY_DESCRIPTION, " &_
				"N.DISPLAYIMAGE, WO.ORDERID, WO.WOID, WO.TITLE, WS.DESCRIPTION AS WOSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, WO.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), WO.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
				"J.JOURNALID, J.CURRENTITEMSTATUSID, " &_
				"CASE WHEN J.ITEMNATUREID = 1 THEN 'iDefectsDetail.asp' " &_
				"WHEN J.ITEMNATUREID IN (2,20,21,22,34,35,36,37,38,39) THEN 'iRepairDetail.asp'  " &_		
				"END AS REDIR, " &_
				"ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
				"ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
				"TITLE = CASE " &_
				"	WHEN LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
				"	ELSE ISNULL(J.TITLE,'No Title') " &_
				"END, J.TITLE AS FULLTITLE, " &_
				"ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
				"J.ITEMNATUREID, " &_
				"LETTERSTATUS = CASE WHEN SATISFACTION_LETTERSTATUS IS NULL AND J.CURRENTITEMSTATUSID IN (6,7,8,9,10,11,5,22) THEN -1 ELSE ISNULL(SATISFACTION_LETTERSTATUS, -2) END  " &_
				"FROM P_WORKORDER WO " &_
				"INNER JOIN P_WOTOREPAIR WTR ON WTR.WOID = WO.WOID " &_
				"INNER JOIN C_JOURNAL J ON J.JOURNALID = WTR.JOURNALID " &_					
				"INNER JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
				"INNER JOIN C_NATURE CN	ON J.ITEMNATUREID = CN.ITEMNATUREID " &_
				"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS " &_
				"INNER JOIN P_WORKORDERENTITY WOE ON WOE.BIRTH_ENTITY_ID = WO.BIRTH_ENTITY " &_				
				"INNER JOIN P_WORKORDERMODULE WOM ON WOM.BIRTH_MODULE_ID = WO.BIRTH_MODULE " &_								
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = WO.BIRTH_NATURE " &_												
				"LEFT JOIN C_SATISFACTIONLETTER SL ON SL.JOURNALID = J.JOURNALID  " &_
				"WHERE WO.TENANCYID = " & tenancy_id & " " &_
				"ORDER BY WO.WOID DESC"				
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		CurrentWorkOrder = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			WOID = rsSet("WOID")
			if (CurrentWorkOrder <> WOID) then
				ImageTD = "<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("BIRTH_MODULE_IMAGE") & ".gif' title='" & rsSet("BIRTH_MODULE_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
							"<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("BIRTH_ENTITY_IMAGE") & ".gif' title='" & rsSet("BIRTH_ENTITY_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
							"<TD style='background-color:white'><img src='/myImages/Repairs/" & rsSet("DISPLAYIMAGE") & ".gif' title=""" & rsSet("TITLE") & """ width='29' height='18' border=0></TD>"

				str_journal_table = str_journal_table & 	"<TR STYLE='CURSOR:HAND;'><A NAME=""WOID"">" &_
																"<TD style='background-color:white'><img src='/js/tree/img/minus.gif' width=18 height=18 border=0></TD>" &_
																ImageTD &_
																"<TD><b>" & rsSet("CREATIONDATE") & "</b></TD>" &_
																"<TD><b>" & PurchaseNumber(rsSet("ORDERID")) & " - " & WorkNumber(rsSet("WOID")) & "</b></TD>" &_
																"<TD><b>" & rsSet("WOSTATUSNAME")  & "</b></TD>" &_
															"<TR>"
				'BuildRepairList(WOID) //SHOW THE APPROPRIATE REPAIR ROWS IF APPLICABLE
				CurrentWorkOrder = WOID
			end if

			' Set the image that will show whether the satisfaction letter is sent/ ready to print/ or item is
			' waiting for acceptance
			ImageLength = "60"
			Colspan = "3"
			ImageName = "joinmedium"
			LetterPrintStatus = rsSet("LETTERSTATUS")
			if LetterPrintStatus = 1 then
				LetterImage = "<TD align=center style='background:#FFFFFF'><img src='/myimages/printwaiting.gif' title='Ready to Print' width=17 height=17 border=0></TD>"
			elseif LetterPrintStatus = 2 then
				LetterImage = "<TD align=center style='background:#FFFFFF'><img src='/myimages/lettersent.gif' title='Letter Sent' width=17 height=17 border=0></TD>"
			elseif LetterPrintStatus = -1 then
				LetterImage = ""
				ImageLength = "86"
				Colspan = "4"
				ImageName = "joinlong"
			else
				LetterImage = "<TD align=center style='background:#FFFFFF'><img src='/myimages/notacceptedyet.gif' title='Awaiting Completion' width=17 height=17 border=0></TD>"
			End If			
			
			icnt = icnt + 1

			TRSTYLE = ""
			if (rsSet("CURRENTITEMSTATUSID") = 5) then
				TRSTYLE = ";color:red;text-decoration:line-through"
			end if
			
			if rsSet("REDIR")<>"" then
			    TR_str="<TR ONCLICK='open_me(" & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",""" & rsSet("REDIR") & """)' STYLE='CURSOR:HAND" & TRSTYLE & "'>"
			else
			    TR_str="<TR STYLE='CURSOR:HAND" & TRSTYLE & "'>"
			end if
						
			str_journal_table = str_journal_table & 	TR_str &_
															"<TD style='background-color:white' colspan=" & Colspan & "><img src='/js/tree/img/" & ImageName & ".gif' width=" & ImageLength & " height=18 border=0></TD>" & LetterImage &_
															"<TD>&nbsp;&nbsp;" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD TITLE=""" & rsSet("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet("TITLE")  & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet("STATUS")  & "</TD>" &_
														"<TR>"
			
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=8 ALIGN=CENTER>No repair work order entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID	
		location.href = "iRepairJournal.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&CWO=" + WOID;
	}

	function open_me(int_nature, int_journal_id, str_redir){
		parent.synchronize_tabs(13, "BOTTOM")		
		location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
	}
	
	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(11, "BOTTOM")
		location.href = "#WOID"					
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM');">

	<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=20></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=34 NOWRAP></TD>								
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 NOWRAP><B><FONT COLOR='BLUE'>Date</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=350 NOWRAP><B><FONT COLOR='BLUE'>Repair Information </FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 NOWRAP><B><FONT COLOR='BLUE'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</BODY>
</HTML>	
	
	
	
	
	