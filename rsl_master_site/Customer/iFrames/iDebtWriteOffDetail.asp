<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id
	Dim Title, last_status, ButtonDisabled, ButtonText

	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonDisabled = " disabled"
	ButtonText = "No Options"

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'') AS CREATfdfIONDATE, " &_
					"			BDWTIMESTAMP AS CREATIONDATE, RIGHT('000000' + CAST(G.BDWID AS VARCHAR),6) AS BDNO, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			I.DESCRIPTION AS ITEMTYPE, G.BDWAMOUNT AS AMOUNT, " &_
					"			G.BDWID, J.TITLE, " &_
					"			E2.FIRSTNAME + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS " &_
					"FROM		C_BADDEBTWRITEOFF G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = G.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = G.ITEMTYPE " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY BDWID DESC "

		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			Title = rsSet("TITLE")
			general_history_id = rsSet("BDWID")
			last_status = rsSet("STATUS")
			Notes = rsSet("NOTES")
			If (Notes = "" OR isNULL(Notes)) Then
				ResponseNotes = "[Empty Notes]"
			ElseIf (Notes = PreviousNotes) Then
				ResponseNotes = "[Same As Above]"
			Else
				ResponseNotes = Notes
			End If
			PreviousNotes = Notes
			str_journal_table = str_journal_table &_
					"<tr valign=""top""><td>" & rsSet("CREATIONDATE") & "</td>" &_
					"<td>" & rsSet("BDNO") & "</td>" &_
					"<td>" & rsSet("ITEMTYPE") & "</td>" &_
					"<td>" & ResponseNotes & "</td>" &_
					"<td>" & FormatCurrency(rsSet("AMOUNT")) & "</td>" &_
					"<td>" & rsSet("STATUS")  & "</td>" &_
                    "<td>" & rsSet("LASTACTIONUSERNAME")  & "</td></tr>"
			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist for this General Enquiry.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Bad Debt Write Off</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Title%>&nbsp;&nbsp;&nbsp;(Hover for additional information)
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonText%>" title="<%=ButtonText%>"
                                    style="background-color: beige" onclick="update_general(<%=general_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap">
                    <font color="blue"><b>Date</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>No</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>Item</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>Notes</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>Amount</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>Status</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    <font color="blue"><b>Created By</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="6">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
