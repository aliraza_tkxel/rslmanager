<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="iHB.asp" -->
<%
	' CONSTANT DECLARATIONS OF BAD DEBT AND BANCRUPTCY NATURES AND ASSOC NOMINAL ACCOUNT IDS AND TRANSACTION TYPES
	' ANY CHANGES OF IDS MUST BE REFLECTED HERE
	Const NAT_BADDEBT = 43
	Const NAT_BANKRUPTCY = 44
	Const RENT_NOM_BADDEBT = 382
	Const RENT_NOM_BANKRUPTCY = 383
	Const SUNDRY_NOM_BADDEBT= 385
	Const TT_BADDEBT = 23
	Const TT_BANKRUPTCY = 25
	Const RENTJOURNALID = 14
	Const SUNDRYDEBTORSID = 18

	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path
	Dim lstItemType, recharge_balance, rent_balance, rentjournal_id, txnid, writeoff_id
	Dim NL_TRANSACTIONTYPE, NL_DEBIT_ID, NL_DESC, NL_CREDIT_ID

	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	Call get_querystring()
	If path <> 1 Then
		recharge_balance = rechargeBalance()
		rent_balance = rentBalance()
		Call BuildSelect(lstItemType, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5)", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", " style='width:300px' onchange=loadAmount() ")
	Else
		Call new_record()
		Call create_rent_account_entry()
		Call create_join_entry()
		Call create_nominal_entry()
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End If

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	SQL = "SELECT * FROM F_FISCALYEARS WHERE YSTART <= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
	Call OpenRs(rsTAXDATE, SQL)
		YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
		YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)
	Call CloseDB()

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

	End Function

	Function new_record()

		Dim strSQL, journal_id

		Call get_querystring()

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", NULL,  " & item_id & ", " & itemnature_id & ", 26, '" & FormatDateTime(date) & "', '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsSet = Conn.Execute(strSQL)
			journal_id = rsSet.fields("JOURNALID").value
			rsSet.close()
		Set rsSet = Nothing

		' BAD DEBT ENTRY - WE MUST GET ID FOR F_WRITEOFF_TO_RENTJOURNAL JOIN TABLE
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_BADDEBTWRITEOFF " &_
					"(JOURNALID, ITEMTYPE, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, BDWDATE, BDWAMOUNT, NOTES) " &_
					"VALUES (" & journal_id & ", " & Request.Form("sel_ITEMTYPE") & ", 26, 1, '" & FormatDateTime(date) & "', " & Session("userid") &_
					 ", '" & Request.Form("txt_BDWDATE") & "', " & Request.Form("txt_AMOUNT") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS WRITEOFFID;"

		set rsSet = Conn.Execute(strSQL)
			writeoff_id = rsSet.fields("WRITEOFFID").value
			rsSet.close()
		Set rsSet = Nothing

	End Function

	Function rechargeBalance()

		Dim b
		strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) as AMOUNT  " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
					"			AND J.ITEMTYPE IN (5,12)"

		Call OpenRs (rsSet, strSQL)
			b = CDbl(rsSet("AMOUNT"))
		Call CloseRs(rsSet)
		rechargeBalance = b

	End Function

	Function rentBalance()

		Dim rent, HBOWED

		' GET RENT OWED BALANCE
		strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
					"			AND J.ITEMTYPE IN (1,8,9,10)" 

		Call OpenRs (rsSet, strSQL) 
			rent = CDbl(rsSet("AMOUNT"))
		Call CloseRs(rsSet)

		HBOWED = 0
		rentBalance = rent - HBOWED

	End Function

	' ENTER ROW INTO RENT JOURNAL
	Function create_rent_account_entry()

		Dim isdebit

		'CHECK FIRST THAT WHETHER THE ITEM TYPE IS RENT OR RECHARGE
		If(Request.Form("sel_ITEMTYPE")=1) then
				'CHECK IF BAD DEBIT NATURE OR BANKRUPCY NATURE
				If Cint(itemnature_id) = NAT_BADDEBT Then
					'CHECK WHETHER IT DEBIT OR CREDIT
					If(Request.Form("txt_AMOUNT")>0) Then
							in_paymenttype = 85
							amount="-" & Request.Form("txt_AMOUNT") ' abs with is debit 0
							isdebit=1
					Else
							in_paymenttype = 87
							amount=abs(Request.Form("txt_AMOUNT")) '"-" with is debit 1
							isdebit=0
					End If
				Else
					'CHECK WHETHER IT DEBIT OR CREDIT
					If(Request.Form("txt_AMOUNT")>0) Then
						in_paymenttype = 86
						amount="-" & Request.Form("txt_AMOUNT")
						isdebit=1
					Else
						in_paymenttype = 88
						amount=abs(Request.Form("txt_AMOUNT"))
						isdebit=0
					End If
				End If
		Else
				'RECHARGE PAYMENTTYPE IS SAME FOR BAD DEBIT NATURE AND BANKRUPCY NATURE
				'CHECK WHETHER IT DEBIT OR CREDIT
				If(Request.Form("txt_AMOUNT")>0) Then
					in_paymenttype = 90
					amount="-" & Request.Form("txt_AMOUNT")
					isdebit=1
				Else
					in_paymenttype = 89
					amount= abs(Request.Form("txt_AMOUNT"))
					isdebit=0
				End If
		End If

		' WE MUST ALSO GET ID FOR F_WRITEOFF_TO_RENTJOURNAL JOIN TABLE
		SQL = 	"SET NOCOUNT ON;" &_
				"INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, PAYMENTSTARTDATE, AMOUNT,ISDEBIT, STATUSID) " &_
				" VALUES ( " & tenancy_id & ", '" & FormatDateTime(date) & "', " & Request.Form("sel_ITEMTYPE") & ", " & in_paymenttype & ", '" & Request.Form("txt_BDWDATE") & "'," & amount & "," & isdebit & ", 2 );" &_
				"SELECT SCOPE_IDENTITY() AS RENTJOURNALID;"

		set rsSet = Conn.Execute(SQL)
		rentjournal_id = rsSet.fields("RENTJOURNALID").value
		rsSet.close()
		Set rsSet = Nothing

	End Function

	Function create_join_entry()

		SQL = "INSERT INTO F_WRITEOFF_TO_RENTJOURNAL (BDWID, RENTJOURNALID) " &_
					" VALUES (" & writeoff_id & ", " & rentjournal_id & ")"
		Conn.Execute(SQL)

	End Function

	Function create_nominal_entry()

	'CHECK FIRST THAT WHETHER THE ITEM TYPE IS RENT (1) OR RECHARGE (5)
	If(Request.Form("sel_ITEMTYPE")=1) Then
		'SET TRANSACTION TYPE AND DEBIT ID DEPENDANT ON NATURE (BAD DEBT OR BANKRUPTCY)
		If Cint(itemnature_id) = NAT_BADDEBT Then
			NL_TRANSACTIONTYPE = TT_BADDEBT
			If(Request.Form("txt_AMOUNT")>0) Then
				NL_DEBIT_ID = RENT_NOM_BADDEBT
				NL_CREDIT_ID = RENTJOURNALID
			Else
				NL_DEBIT_ID = RENTJOURNALID
				NL_CREDIT_ID = RENT_NOM_BADDEBT
			End If
			NL_DESC = "Rent Bad Debt Write Off"
		Else
			NL_TRANSACTIONTYPE = TT_BANKRUPTCY
			If(Request.Form("txt_AMOUNT")>0) Then
				NL_DEBIT_ID = RENT_NOM_BANKRUPTCY
				NL_CREDIT_ID = RENTJOURNALID
			Else
				NL_DEBIT_ID = RENTJOURNALID
				NL_CREDIT_ID = RENT_NOM_BANKRUPTCY
			End If
			NL_DESC = "Rent Bankruptcy Write Off"
		End If
	Else
		'SET TRANSACTION TYPE AND DEBIT ID DEPENDANT ON NATURE (BAD DEBT OR BANKRUPTCY)
		If Cint(itemnature_id) = NAT_BADDEBT Then
			NL_TRANSACTIONTYPE = TT_BADDEBT
			If(Request.Form("txt_AMOUNT")>0) Then
				NL_DEBIT_ID = SUNDRY_NOM_BADDEBT
				NL_CREDIT_ID = SUNDRYDEBTORSID
			Else
				NL_DEBIT_ID = SUNDRYDEBTORSID
				NL_CREDIT_ID = SUNDRY_NOM_BADDEBT
			End If
			NL_DESC	= "Sundry Bad Debt Write Off"
		Else
			NL_TRANSACTIONTYPE = TT_BANKRUPTCY
			If(Request.Form("txt_AMOUNT")>0) Then
				NL_DEBIT_ID = SUNDRY_NOM_BADDEBT
				NL_CREDIT_ID = SUNDRYDEBTORSID
			Else
				NL_DEBIT_ID = SUNDRYDEBTORSID
				NL_CREDIT_ID = SUNDRY_NOM_BADDEBT
			End If
			NL_DESC	= "Sundry Bankruptcy Write Off"
		End If
	End If

		Call enter_journal()
		Call enter_journal_DB()
		Call enter_journal_CR()

	End Function 

	'CREATE TOP LEVEL JOURNAL ENTRY
	Function enter_journal()

		'CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
			comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			comm.parameters(1) = NL_TRANSACTIONTYPE
			comm.parameters(2) = CLng(rentjournal_id)
			comm.parameters(3) = now 
			comm.parameters(4) = FormatDateTime(Request.Form("txt_BDWDATE"))
			comm.parameters(5) = null
			comm.parameters(6) = null
			comm.execute
			txnid = comm.parameters(0)
		Set comm = Nothing

	End Function

	'CREATE DEBIT LINE JOURNAL ENTRY
	Function enter_journal_DB()

		'CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYDEBITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
			comm.commandtext   = "F_INSERT_NL_JOURNALENTRYDEBITLINE"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			comm.parameters(1) = Clng(txnid)
			comm.parameters(2) = Cint(NL_DEBIT_ID)
			comm.parameters(3) = now
			comm.parameters(4) = 1
			comm.parameters(5) = FormatDateTime(Request.Form("txt_BDWDATE"))
			comm.parameters(6) = abs(Request.Form("txt_AMOUNT"))
			comm.parameters(7) = tenancy_id & ": " & NL_DESC
			comm.parameters(8) = Replace(Request.Form("txt_NOTES"), "'", "''")
			comm.parameters(9) = null
			comm.execute
		Set comm = Nothing

	End Function

	'CREATE CREDIT LINE JOURNAL ENTRY
	Function enter_journal_CR()

		'CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYCREDITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
			comm.commandtext   = "F_INSERT_NL_JOURNALENTRYCREDITLINE"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			comm.parameters(1) = Clng(txnid)
			comm.parameters(2) = Cint(NL_CREDIT_ID)
			comm.parameters(3) = now
			comm.parameters(4) = 2
			comm.parameters(5) = FormatDateTime(Request.Form("txt_BDWDATE"))
			comm.parameters(6) = abs(Request.Form("txt_AMOUNT"))
			comm.parameters(7) = tenancy_id & ": " & NL_DESC
			comm.parameters(8) = Replace(Request.Form("txt_NOTES"), "'", "''")
			comm.parameters(9) = null
			comm.execute
		Set comm = Nothing

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Write Offs</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 10px 0px 10px;
            background-image: none;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

	var FormFields = new Array();
	FormFields[0] = "txt_BDWDATE|BDW Date|DATE|Y"
	FormFields[1] = "sel_ITEMTYPE|Item Type|SELECT|Y"
	FormFields[2] = "txt_AMOUNT|Amount|CURRENCY|Y"
	FormFields[3] = "txt_NOTES|Notes|TEXT|N"

	function save_form(){
	    var amount_sign
		if (!checkForm()) return false;
		// GET THE ITEM TYPE WHETHER RENT OR RECHARGE
		// MAKE SURE ORDER DATE IS IN CURRENT FISCAL YEAR
		var YStart = new Date("<%=YearStartDate%>")
		var YEnd = new Date("<%=YearendDate%>")
		if ( (real_date(document.getElementById("txt_BDWDATE").value) < YStart) || (real_date(document.getElementById("txt_BDWDATE").value) > YEnd) ){
			alert("Please enter an payment date for the current financial year\nBetween '<%=YearStartDate%>' and '<%=YearendDate%>'");
			return false;
		}
		// END ------------------------------------------	
		// ENSURE WRITE OFF VALUE DOES NOT EXCEED ITEM TYPE BALANCE
		if (document.getElementById("sel_ITEMTYPE").options[document.getElementById("sel_ITEMTYPE").selectedIndex].value == 1) {
		    if (Math.abs(parseFloat(document.getElementById("txt_AMOUNT").value)) > rentbalance) {
			    alert("You have entered a write off value more than the actual balance of the item type.");
			    return;
			}
		}
		if (document.getElementById("sel_ITEMTYPE").options[document.getElementById("sel_ITEMTYPE").selectedIndex].value == 5) {
			if (Math.abs(parseFloat(document.getElementById("txt_AMOUNT").value)) > rechargebalance) {
					alert("You have entered a write off value more than the actual balance of the item type.");
				    return;
			}
		}
		document.RSLFORM.target = "CRM_BOTTOM_FRAME";
		document.RSLFORM.action = "Ibaddebtwriteoff.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		document.RSLFORM.submit();
	}

	function real_date(str_date) {
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
	}

	var set_length = 4000;

	function countMe(obj) {
		if (obj.value.length >= set_length) {
			obj.value = obj.value.substring(0, set_length - 1);
		}
	}

	function clickHandler(e) {
		return (window.event) ? window.event.srcElement : e.target;
	}

	// the actual_ balances are used to determine if this tenancy is in credit or not
	// the absolute balances are used for comparisons and display purposes
	var actual_rentbalance = parseFloat(<%=rent_balance%>);
	var actual_rechargebalance = parseFloat(<%=recharge_balance%>);
	var rentbalance = Math.abs(parseFloat(<%=rent_balance%>));
	var rechargebalance = Math.abs( parseFloat(<%=recharge_balance%>));

	function loadAmount(){
		document.getElementById("btnSave").disabled = false;
		if (document.getElementById("sel_ITEMTYPE").value == "")
			return false;
		if (document.getElementById("sel_ITEMTYPE").value == 1){
			document.getElementById("txt_AMOUNT").value = FormatCurrency(parseFloat(<%=rent_balance%>));
			}
		else {
			document.getElementById("txt_AMOUNT").value = FormatCurrency(parseFloat(<%=recharge_balance%>));
			}
	}

    </script>
</head>
<body class="TA">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Write Off Date :
            </td>
            <td>
                <input type="text" name="txt_BDWDATE" id="txt_BDWDATE" class="textbox" style="width: 300px" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BDWDATE" id="img_BDWDATE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Item Type :
            </td>
            <td>
                <%=lstItemType%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ITEMTYPE" id="img_ITEMTYPE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Amount :
            </td>
            <td>
                <input type="text" name="txt_AMOUNT" id="txt_AMOUNT" class="textbox" style="width: 300px" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_AMOUNT" id="img_AMOUNT" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" class='textbox200'
                    rows="7" cols="20" name="txt_NOTES" id="txt_NOTES" onkeyup="countMe(clickHandler(event));"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input type="button" value=" Save " title="Save" class="RSLButton" onclick="save_form()" name="btnSave" id="btnSave" style="cursor:pointer" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    <iframe src="/secureframe.asp" name="BDWFrame" id="BDWFrame" width="400" height="100" style="display: none">
    </iframe>
</body>
</html>
