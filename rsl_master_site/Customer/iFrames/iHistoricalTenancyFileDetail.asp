<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, agreement_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	Dim filename

	journal_id = Request("journalid")
	nature_id = Request("natureid")
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			G.HISTORICALTENANCYFILEID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS,G.DOCFILE " &_
					"FROM		C_HISTORICALTENANCYFILE G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY HISTORICALTENANCYFILEID DESC "

		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				agreement_history_id = rsSet("HISTORICALTENANCYFILEID")
				filename = rsSet("DOCFILE")
				last_status = rsSet("STATUS")
				If (rsSet("ITEMSTATUSID") = 14) Then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				End If
			End If
				Notes = rsSet("NOTES")
				If (Notes = "" OR isNULL(Notes)) Then
					ResponseNotes = "[Empty Notes]"
				ElseIf (Notes = PreviousNotes) Then
					ResponseNotes = "[Same As Above]"
				Else
					ResponseNotes = Notes
				End If
				PreviousNotes = Notes
				str_journal_table = str_journal_table & "<td width=""120"">" & rsSet("CREATIONDATE") & "</td>" &_
														"<td width=""438"">" & ResponseNotes & "</td>" &_
														"<td width=""22"">&nbsp;</td>" &_
														"<td width=""150"">" & rsSet("FULLNAME")  & "</td>"				
				str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" style=""cursor:pointer"" alt=""Open Attachment"" onclick=""View_Doc("& rsSet("HISTORICALTENANCYFILEID") &")"" /></td>"
				str_journal_table = str_journal_table & "</td><tr>"

			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""5"" align=""center"">No journal entries exist for this Historical Tenancy File Item.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Historical Tenancy File Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_agreement(int_agreement_history_id){
        var str_win
		str_win = "../PopUps/pHistoricalTenancyFile.asp?agreementhistoryid="+int_agreement_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=355, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

    function View_Doc(history_id) {
    var mimetype=getmimetype("<%=filename%>");
    var strUrlPrms = "ff=DOCUMENTFILE&fn=DOCFILE&tn=C_HISTORICALTENANCYFILE&cs=connRSL&cr=HistoricalTenancyFileId=" + history_id + "&mm="+mimetype;
        window.open("/DBFileUpload/uploader.aspx?" + strUrlPrms, "_blank","TOP=200, LEFT=400, scrollbars=yes, height=590, width=460, status=NO, resizable= Yes");		
    }

    function getmimetype(docfile) {
    var mimetype,filetype,splitnametype
        splitnametype=docfile.split(".")
        filetype=splitnametype[1]
        switch (filetype) {
            case 'doc':
                mimetype='application/msword'
                break;
            case 'pdf':
                mimetype='application/pdf'
                break;
            case 'docx':
                mimetype='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                break;
            default:
                mimetype=''
        }
    return  mimetype;
    }

	function open_letter(letter_id){
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/training_letter_plain.asp?tenancyid="+tenancy_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	}

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        border="0">
        <thead>
            <tr>
                <td colspan="5">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" title="<%=ButtonText%>"
                                    value="<%=ButtonText%>" style="background-color: #f5f5dc; cursor:pointer" onclick="update_agreement(<%=agreement_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="120px">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="500px">
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="150px">
                    <font color="blue"><b>Created By:</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
