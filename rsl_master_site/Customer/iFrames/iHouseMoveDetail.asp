<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	'' Created By:	Munawar Nadeem(TkXel)
	'' Created On: 	June 27, 2008
	'' Reason:		Integraiton with Tenats Online

	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status
	Dim ButtonDisabled, ButtonText

	
	OpenDB()

	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "

	build_journal()
	
	CloseDB()

	Function build_journal()


		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, H.LASTACTIONDATE, 103) ,'') AS CREATIONDATE, "  &_
					"			ISNULL(H.MOVINGBECAUSE, 'N/A') AS NOTES," &_
					"			H.HOUSEMOVEHISTORYID, 	H.ITEMSTATUSID, " &_
					"			H.NOOFBEDROOMS, H.OCCUPANTSABOVE18, H.OCCUPANTSBELOW18, " &_
					"			LA.DESCRIPTION AS AUTHORITY, " &_
					"			D.DEVELOPMENTNAME AS DEVELOPMENT, " &_
					"			E2.FIRSTNAME + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME, " &_
					"			J.TITLE ,S.DESCRIPTION AS STATUS" &_
					"   FROM C_HOUSE_MOVE H LEFT JOIN " &_
					"			C_STATUS S ON H.ITEMSTATUSID = S.ITEMSTATUSID   " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = H.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON H.JOURNALID = J.JOURNALID" &_
					"			LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = H.LOCALAUTHORITYID " &_
					"			LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = H.DEVELOPMENTID " &_
					"WHERE		H.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY HOUSEMOVEHISTORYID DESC "
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""

		While Not rsSet.EOF
			
			cnt = cnt + 1

			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("HOUSEMOVEHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If

				Notes = rsSet("NOTES") '-- MovingBecause

				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if

				PreviousNotes = Notes

				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE")  & "</TD>" &_
											"<TD>" & rsSet("LASTACTIONUSERNAME") & "</TD>" &_
											"<TD COLSPAN=3>" & ResponseNotes & "</TD>" &_
											"<TD>" & rsSet("AUTHORITY") & "</TD>" &_
											"<TD>" & rsSet("DEVELOPMENT")  & "</TD>" &_
											"<TD>" & rsSet("NOOFBEDROOMS")  & "</TD>" &_
											"<TD>" & rsSet("OCCUPANTSABOVE18")  & "</TD>" &_
											"<TD>" & rsSet("OCCUPANTSBELOW18")  & "</TD>" 

				
																			
				str_journal_table = str_journal_table & 	"</TR>"
				
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	// function to open popup to update Housemove
	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pHouseMove.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=355, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
		

</SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>

	<THEAD>
	
	<TR>
	<TD COLSPAN=10>
		
		 
	<table cellspacing=0 cellpadding=1 width=100%>
			<tr valign=top>
		          <td>
				<b>Title:</b>&nbsp;<%=Defects_Title%> 
			   </TD>

	<TD align="left">Current Status:&nbsp;<font color=red><%=last_status%></font></td>
      <td align="right">
	   <INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick='update_general(<%=general_history_id%>)' <%=ButtonDisabled%>>
	</td></tr>
   </table>	</TD></TR>

	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=80PX><font color="blue"><b>Date:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Last Action User:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" COLSPAN=3><font color="blue"><b>Moving Because:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Authority:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Development:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Bedrooms:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Occupants > 18:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><b>Occupants < 18:</b></font></TD>

	</TR>
	<TR STYLE='HEIGHT:7PX'></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	