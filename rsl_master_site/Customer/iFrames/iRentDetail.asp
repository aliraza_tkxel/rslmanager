<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			G.GENERALHISTORYID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS " &_
					"FROM		C_GENERAL G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY GENERALHISTORYID DESC "
		
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("GENERALHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 18 OR rsSet("ITEMSTATUSID") = 19) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & ResponseNotes & "</TD>" &_
															"<TD>" & rsSet("FULLNAME")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>No journal entries exist for this Rent Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > Rent Details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pRent.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=305, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=3><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	<TD nowrap width=170>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick='update_general(<%=general_history_id%>)' <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX>Date:</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=500px>Notes:</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX>Assigned To:</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	