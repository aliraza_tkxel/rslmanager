<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_PaidDirectlyTo, hid_txt_RentBalance, item_id, itemnature_id, journal_id, customer_id
    Dim tenancy_id, rentBalance,property_id, title,uc_startDate, paidDirectlyTo, notes, currItem_statusID
    Dim paidDirectlyToBHG, paidDirectlyToTenant, paidDirectlyToDefault
	
    
    Call get_querystring()
	path = request("submit")
    syncBalance = request("syncBalance")

    If path = "" Then 
        path = 0
    End If

    If syncBalance = "" Then 
        syncBalance = 0
    End If

	If path <> 1 OR syncBalance = 1 Then
        call populateData() 
        call getRentBalance()
    Else
        new_record()
	End If


    Function new_record()

		Dim strSQL
        Call get_querystring()
        Call getFormData()
      
		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

        Set rsSet = Nothing
        strSQL = "SELECT PROPERTYID AS PID FROM C_TENANCY WHERE TENANCYID =  "& tenancy_id
        Call OpenDB()
		Call OpenRs(rsSet, strSQL)
		If Not rsSet.EOF Then property_id = rsSet("PID") Else property_id = 0 End If
		Call CloseRs(rsSet)
        
          
    
        Set rsSet = Nothing
        strSQL = "SELECT ITEMSTATUSID AS ISID FROM C_STATUS WHERE DESCRIPTION = 'Open' "
       
        Call OpenRs(rsSet, strSQL)
        If Not rsSet.EOF Then 
            currItem_statusID = rsSet("ISID")
        Else 
            currItem_statusID = 0 
        End If
        Call CloseRs(rsSet)
       
		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", '" & property_id & "', " & item_id & ", " & itemnature_id & ", " & currItem_statusID & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
                    
     	set rsSet = Conn.Execute(strSQL)
        journal_id = rsSet.fields("JOURNALID").value
        rsSet.close()
        Set rsSet = Nothing
	
		' UniversalCredit ENTRY
        strSQL = "INSERT INTO C_UniversalCredit (JournalId, PaidDirectlyTo, StartRentBalance, IsCurrent, CreatedBy, Notes, StartDate, CreationDate) " &_
        "VALUES (" & journal_id & ", '" & paidDirectlyTo & "', " & rentBalance & ", 1, " & Session("userid") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "', '" & uc_startDate & "', '" & now() & "')"
           
            Conn.Execute(strSQL)
    
    
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")  
		End Function

    'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	Function getRentBalance()
    
        strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
        "FROM	 	F_RENTJOURNAL J " &_
        "			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
        "WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18)) " &_
        " AND J.TRANSACTIONDATE <=  '" & uc_startDate & "'"
        Call OpenDB()
        Call OpenRs (rsSet, strSQL)
        hid_txt_RentBalance = rsSet("AMOUNT")
        rentBalance = FormatCurrency(rsSet("AMOUNT"),2)
        Call CloseRs(rsSet)
        Call CloseDB()

	End Function

    Function populateData()
        
        if syncBalance = 1 then
            call getFormData()
            if paidDirectlyTo = "BHG" then
                paidDirectlyToBHG = "selected"
            elseif paidDirectlyTo = "Tenant" then
                paidDirectlyToTenant = "selected"
            else
                paidDirectlyToDefault = "selected"
            end if
        else
            uc_startDate = DateAdd("d", 0, Date)
            paidDirectlyToDefault = "selected" 
        end if

    End Function


    Function getFormData()
        uc_startDate =  Request.Form("txt_UCStartDate")
        rentBalance = Request.Form("hid_txt_RentBalance")
        paidDirectlyTo = Request.Form("lst_paidDirecltyTo")
        notes = Request.Form("txt_NOTES")
    End Function


	'Retirves fields from querystring which can be from either parent crm page or from this page when submitting to its self
	Function get_querystring()
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
        title = Request("title")
        uc_startDate = Request("uc_startDate")
	End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Termination</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function validateDate() {

            // we need to ensure that START DATE is not less than past 12 months AND more than next six monts //
            ucStartDate = document.getElementById("txt_UCStartDate").value;
            arrSplit = ucStartDate.split("/");
            ucStartDate = new Date(arrSplit[2], arrSplit[1] - 1, arrSplit[0]);

            dateRange_fwd = new Date();
            dateRange_fwd = new Date(dateRange_fwd.getFullYear(), dateRange_fwd.getMonth(), dateRange_fwd.getDate());
            //dateRange_fwd.setMonth(dateRange_fwd.getMonth() + 6);

            dateRange_bckwd = new Date();
            dateRange_bckwd = new Date(dateRange_bckwd.getFullYear(), dateRange_bckwd.getMonth(), dateRange_bckwd.getDate());
            dateRange_bckwd.setMonth(dateRange_bckwd.getMonth() - 12);

            if (ucStartDate > dateRange_fwd) {
                alert("You have entered a Start Date greater than today!");
                return false;
            }
            else if (ucStartDate < dateRange_bckwd) {
                alert("You have entered a Start Date less than past 12 months!");
                return false;
            }
            return true;
        }

        function save_form() {

            FormFields[0] = "txt_UCStartDate|Start Date|DATE|Y"
            FormFields[1] = "txt_RentBalance|Rent Balance|TEXT|Y"
            FormFields[2] = "lst_paidDirecltyTo|Paid directly to|SELECT|Y"
            FormFields[3] = "txt_NOTES|Notes|TEXT|N"

            if (!checkForm()) return false;
            if (!validateDate()) return false;
          
            paidDirectlyTo = document.getElementById("lst_paidDirecltyTo").value;

            document.RSLFORM.target = "CRM_BOTTOM_FRAME";
            document.RSLFORM.action = "iUniversalCredit.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&title=<%=title%>&submit=1";
            parent.parent.STARTLOADER('BOTTOM')
            document.RSLFORM.submit();
        }

        function syncBalance() {

            FormFields[0] = "txt_UCStartDate|Start Date|DATE|Y"
            if (!checkForm()) return false;
            if (!validateDate()) return false;
            document.RSLFORM.action = "iUniversalCredit.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&title=<%=title%>&syncBalance=1";
            document.RSLFORM.submit();
        }


    </script>
</head>
<body class="TA">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="middle">
                Start Date :
            </td>
            <td>
                <input type="text" name="txt_UCStartDate" id="txt_UCStartDate" value="<%=uc_startDate %>"
                    maxlength="10" size="29" class="textbox200" style="width: 200px" onblur="syncBalance()" />
                <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_UCStartDate',180,35,'de','#FFFFFF','#133E71')"
                    tabindex="1">
                    <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
            </td>
            <td>
                <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                    top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                </div>
                <img src="/js/FVS.gif" name="img_UCStartDate" id="img_UCStartDate" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Rent Balance :
            </td>
            <td>
                <input type="text" name="txt_RentBalance" id="txt_RentBalance" class="textbox200"
                    value="<%=rentBalance%>" style="width: 100px" readonly />
                (As at Start Date)
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RentBalance" id="img_RentBalance" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Paid directly to :
            </td>
            <td>
                <select id="lst_paidDirecltyTo" name="lst_paidDirecltyTo" class="textbox200" style="width: 200px;">
                    <option value="" <%=paidDirectlyToDefault%>>Please Select</option>
                    <option value="BHG" <%=paidDirectlyToBHG%>>BHG</option>
                    <option value="Tenant" <%=paidDirectlyToTenant%>>Tenant</option>
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_paidDirecltyTo" id="img_paidDirecltyTo" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                    class="textbox200" rows="7" cols="10" name="txt_NOTES" id="txt_NOTES"><%=notes%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                <!--#include virtual="includes/bottoms/blankbottom.html" -->
                <input type="button" value="Save" class="RSLButton" title="Save" onclick="save_form()"
                    name="SaveButton" id="SaveButton" style="cursor: pointer" />
                <input type="hidden" name="hid_txt_RentBalance" id="hid_txt_RentBalance" value="<%=hid_txt_RentBalance%>" />
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="400"
        height="100" style="display: none"></iframe>
</body>
</html>
