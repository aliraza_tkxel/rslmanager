<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
'	on error resume next

	Dim lst_ReasonCategory, item_id, itemnature_id, customer_id, tenancy_id, path, lstTeams, lstAction, property_id, reason_id, int_lst_record, ValidateToogle
	Dim disable_save, tenancy_start_date, Termination_Method, this_forwarding, other_forwarding, joints,newJournalId

	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: 	June 13, 2008
	'' Reason:		Integraiton with Tenats Online

	'Code Changes Start by TkXel=================================================================================
	Dim enqID, terminDate, terminNotes
		enqID = ""
	'Code Changes End by TkXel=================================================================================

	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then

		'Code Changes Start by TkXel=================================================================================
		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback
		enqID = Request("EnquiryID")
		terminDate = ""
		terminNotes = ""
		' Enters into if block only, if EnquiryID is part of query string parameters
		If enqID <> "" OR Len(enqID) > 0 Then

			' This function will get the details of Termination request, based on Enquiry ID from
			' Database tables TO_ENQUIRY_LOG and TO_TERMINATION
			' These extracted values will be used to populate matching frame text boxes/drop downs etc.
			Call get_termiantion_detail(enqID, terminDate, terminNotes)
		End If
		'Code Changes End by TkXel=================================================================================

		Call entry()

		this_forwarding = check_forwarding_address()
		other_forwarding = check_other_forwarding_address()
		joints = get_joint_tenants()

	Else

		Call new_record()

	End If

	'Call CloseDB()


	'Code Changes Start by TkXel=================================================================================

	Function get_termiantion_detail(enquiryID, ByRef date, ByRef notes)

		' CALL To STORED PROCEDURE TO get Termination Detail
		Set comm = Server.CreateObject("ADODB.Command")
		' setting stored procedure name
		comm.commandtext = "TO_TERMINATION_SelectTerminationDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
		' if record found/returned, setting values
		If Not rs.EOF Then
			notes = rs("Description")
			date = rs("MovingOutDate")
		End If
		Set comm = Nothing
		Set rs = Nothing

	End Function


	' Update Enquiry Log Table
	Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID
		' Executing sproc and storing resutls in record set
		Set rs = comm.execute
		Set comm = Nothing
		Set rs = Nothing

	End Function


	'Code Changes End by TkXel===================================================================================

	' RETURN NUMBER OF FORWARDING ADDRESS SET UP FOR THIS CUSTOMER
	Function check_forwarding_address()

		Dim num_forwarding
		SQL = "SELECT COUNT(*) AS CNT FROM C_ADDRESS WHERE ADDRESSTYPE = 5 AND CUSTOMERID = " & customer_id
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then num_forwarding = rsSet("CNT") Else num_forwarding = 0 End If
		Call CloseRs(rsSet)
		check_forwarding_address = num_forwarding

	End Function


	' RETURN NUMBER OF FORWARDING ADDRESS SET UP FOR THIS CUSTOMER
	Function check_other_forwarding_address()

		Dim num_other_forwarding
		SQL = "SELECT COUNT(*) AS CNT FROM 	C_CUSTOMERTENANCY T " &_
				" INNER JOIN C_ADDRESS A ON A.CUSTOMERID = T.CUSTOMERID " &_
					" WHERE T.TENANCYID = " & tenancy_id & " AND A.ADDRESSTYPE = 5 AND A.CUSTOMERID <> " & customer_id
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then num_other_forwarding = rsSet("CNT") Else num_other_forwarding = 0 End If
		Call CloseRs(rsSet)
		check_other_forwarding_address = num_other_forwarding

	End Function


	' FIND OUT IF ANY JOINT TENANTS EXIST FOR THIS CUSTOMER, IF SO RETURN
	Function get_joint_tenants()

		Dim names
		' FIRST CHECK TO SEE IF THIS IS A JOINT TENANCY
		SQL = "SELECT COUNT(*) AS CNT FROM C_CUSTOMERTENANCY WHERE TENANCYID = " & tenancy_id
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then TCOUNT = rsSet("CNT") Else TCOUNT = 0 End If
		Call CloseRs(rsSet)
		If TCOUNT < 2 Then 
			get_joint_tenants = "0"
			Exit Function
		End If

		SQL = "SELECT LIST FROM C_CUSTOMER_NAMES_GROUPED_VIEW WHERE I = " & tenancy_id
		Call OpenRs(rsSet, SQL)
		If not rsSet.EOF Then names = rsSet("LIST") Else names = "0" End If
		Call CloseRs(rsSet)
		get_joint_tenants = names

	End Function

    Function build_ActivitySelect(ByRef lst, ftn_sql)

    If ftn_sql = "" Then
        Exit Function
    End If

		Call OpenRs(rsSet, ftn_sql)
        If rsSet.EOF Then
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>No options available</option>"
            lst = lst & "</select>"
        Else
            lst = "<select name=""sel_REASONCATEGORY"" id=""sel_REASONCATEGORY"" class=""textbox200"">"
            lst = lst & "<option value=''>Please select</option>"
            int_lst_record = 0
            While (NOT rsSet.EOF)
			    lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
	    	    rsSet.MoveNext()
			    int_lst_record = int_lst_record + 1
		    Wend
                lst = lst & "</select>"
        End If
		Call CloseRs(rsSet)

        If int_lst_record = 0 Then ValidateToogle = "N" Else ValidateToogle = "Y" End If

	End Function


	Function entry()

		Call get_querystring()

            mysql = "SELECT TRA.TERMINATION_REASON_ACTIVITYID, TRA.DESCRIPTION " &_
            "FROM C_TERMINATION_REASON TR " &_
            "INNER JOIN C_TERMINATION_REASON_TOACTIVITY TR2A ON TR.REASONID = TR2A.REASONID " &_
            "INNER JOIN C_TERMINATION_REASON_ACTIVITY TRA ON TR2A.TERMINATION_REASON_ACTIVITYID = TRA.TERMINATION_REASON_ACTIVITYID " &_
            "WHERE TR.REASONID = " & reason_id & " " &_
            "ORDER BY TRA.DESCRIPTION"

        Call build_ActivitySelect(lst_ReasonCategory, mysql)

		' GET TENANCT STARTDATE
		SQL = "SELECT STARTDATE FROM C_TENANCY WHERE TENANCYID = " & tenancy_id
		Call OpenRs(rsSet, SQL)
		IF NOT rsSet.EOF then
			tenancy_start_date = rsSet(0)
		Else
			tenancy_start_date = FormatDateTime(date,2)
		End If
		Call CloseRs(rsSet)
		disable_save = check_for_open_termination()

	End Function


	Function check_for_open_termination()

		SQL = "SELECT JOURNALID FROM C_JOURNAL WHERE TENANCYID = " & cdbl(tenancy_id) & " AND ITEMNATUREID = 27 AND CURRENTITEMSTATUSID = 13"
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			check_for_open_termination = "disabled"
			Termination_Method = "An open termination record already exists for this tenancy.<br>You cannot not have two 'open' termination records per tenancy."
		Else
			SQL = "SELECT TENANCYID FROM C_TENANCY WHERE TENANCYID = " & cdbl(tenancy_id) & " AND ENDDATE IS NOT NULL"
			Call OpenRs(rsSet2, SQL)
			If (NOT rsSet2.EOF) Then
				check_for_open_termination = "disabled"
				Termination_Method = "This tenancy is already terminated, and cannot be terminated"
			Else
				check_for_open_termination = ""
			End If
			Call CloseRs(rsSet2)
		End If
		Call CloseRs(rsSet)

	End Function


	' retirves fields from querystring which can be from either parent crm page or from this page when submitting to its self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		property_id = Request("propertyid")
        reason_id = Request("typeid")
        If reason_id = "" Then reason_id = -1 End If

		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If

	End Function


	Function new_record()

		Dim strSQL, journal_id, mutual
		Call get_querystring()

		' SEE IF THIS TERMINATION IS AS A RESULT OF MUTUAL EXCHAMGE
		If Cint(Request("txt_REASON")) = 13 OR Cint(Request("txt_REASON")) = 14 OR Cint(Request("txt_REASON")) = 15 Then
			mutual = 1
		Else
			mutual = 0
		End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", '" & property_id & "', " & item_id & ", " & itemnature_id & ", 13, '(" & tenancy_id & ")" & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		Set rsSet = Conn.Execute(strSQL)
			journal_id = rsSet.fields("JOURNALID").value
			rsSet.close()
		Set rsSet = Nothing

		'AMENDED BY ALI TO ALLOW FOR NO REASON TERMINATIONS
		REASON = Request.Form("txt_REASON")
		If (REASON = "") Then REASON = "NULL"

        REASONCATEGORYID = Request.Form("sel_REASONCATEGORY")
		If (REASONCATEGORYID = "") Then REASONCATEGORYID = "NULL"

		' ARREARS ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_TERMINATION " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, TERMINATIONDATE, REASON, REASONCATEGORYID, NOTES) " &_
					"VALUES (" & journal_id & ", 13, 1, " & Session("userid") &_
					 ", '" & FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2) & "', " & REASON & ", " & REASONCATEGORYID & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "')"
		Conn.Execute(strSQL)
		Set rsSet = Nothing

		' Code Added by TkXel - Start
		If enqID <> "" OR Len(enqID) > 0 Then
			Call update_enquiry_log(enqID, journal_id)
		End If
		' Code Added by TkXel - End

		' CALL STORED PROCEDURE TO CALCULATE REFUND VALUE
		Set comm = Server.CreateObject("ADODB.Command")
			comm.commandtext = "F_TENANCY_END_PROCESS"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			comm.parameters(1) = tenancy_id
			comm.parameters(2) = Session("userid")
			comm.parameters(3) = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)
			comm.parameters(4) = getlastday(FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2))
			comm.parameters(5) = property_id
			comm.parameters(6) = journal_id
			comm.parameters(7) = mutual
			comm.execute
		Set comm = Nothing
		

        If mutual = 0 Then
			' CALL STORED PROCEDURE TO store value in PDR_MSAT AND PDR_Journal
		    Set comm = Server.CreateObject("ADODB.Command")
			    comm.commandtext = "V_SaveTenancyTermination"
			    comm.commandtype = 4
		    Set comm.activeconnection = Conn
			    comm.parameters(1) = property_id 
			    comm.parameters(2) = customer_id 
                comm.parameters(3) = tenancy_id
			    comm.parameters(4) = FormatDateTime(Request.Form("txt_TERMINATIONDATE"),2)			
			    comm.parameters(5) = Session("userid")
			    comm.parameters(6) = 1			
           
			    comm.execute
			     newJournalId=comm.parameters(0)
		    Set comm = Nothing
		End If    
        
		Call CloseDB()
		
        ' SEE IF THIS TERMINATION IS AS A RESULT OF MUTUAL EXCHAMGE
        If Cint(Request("txt_REASON")) = 13 OR Cint(Request("txt_REASON")) = 14 OR Cint(Request("txt_REASON")) = 15 Then
			Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
		Else
            Response.write("<script type=""text/javascript"">parent.window.location.href = '/PropertyDataRestructure/Bridge.aspx?pg=Voids&jid="&newJournalId&"&cid="&customer_id&"'</script>")
		End If
        
        'Response.Write "Calling =" LoadVoidPage() "."
		'Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function


	' calculate last day of month
	Function getlastday(datum)

		thedate = datevalue(datum)
		If month(thedate) = 4 Or month(thedate) = 9 Or month(thedate) = 6 Or month(thedate) = 11 Then
			monthdays = 30
		ElseIf month(thedate) = 2 Then
			If year(thedate) = 2004 Or year(thedate) = 2008 Or year(thedate) = 2012 Then
				monthdays = 29
			Else monthdays = 28 End If
		Else monthdays = 31 End If

		getlastday = (monthdays & "/" & month(thedate) & "/" & year(thedate))

	End function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Termination</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "txt_TERMINATIONDATE|Termination Date|DATE|Y"
	FormFields[1] = "sel_REASONCATEGORY|Reason Category|SELECT|<%=ValidateToogle %>"
	FormFields[2] = "txt_NOTES|Notes|TEXT|N"
	
	arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	
	function save_form(){
		if (!checkForm()) return false;

		// WE NEED TO ENSURE THAT TENANCY END DATE IS NOT BEFORE TENANCY STARTDATE
		StartDate = document.getElementById("hid_TENANCYSTARTDATE").value
		arrSplit = StartDate.split("/");
		StartDate = new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);

		EndDate = document.getElementById("txt_TERMINATIONDATE").value
		arrSplit = EndDate.split("/");
		EndDate = new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
		
		if (StartDate > EndDate){
			alert("You have entered a tenancy end date of : " + document.getElementById("txt_TERMINATIONDATE").value + ".\nThis is before the start date of the tenancy of : " + document.getElementById("hid_TENANCYSTARTDATE").value + ".\n\nPlease make sure the figure is greater than or equal to the start date.");
			return false;
		}
		document.RSLFORM.target = "CRM_BOTTOM_FRAME";
		document.RSLFORM.action = "iTermination.asp?propertyid="+parent.parent.document.getElementById("propertyid").value+"&itemid=<%=item_id%>&natureid=<%=itemnature_id%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
		document.getElementById("SaveButton").disabled = true
		parent.parent.STARTLOADER('BOTTOM')
		document.RSLFORM.submit();
		parent.parent.swap_div(parent.parent.MAIN_OPEN_TOP_FRAME, 'top')
	}

	function GetEmployee_detail() { }

	var set_length = 4000;

	function countMe(obj) {
		if (obj.value.length >= set_length) {
			obj.value = obj.value.substring(0, set_length - 1);
		}
	}

	function clickHandler(e) {
		return (window.event) ? window.event.srcElement : e.target;
	}

	function check_fowarding(){

		var cl_f = <%=this_forwarding%>;
		var cl_o = <%=other_forwarding%>;
		var cl_j = "<%=joints%>";

		if (cl_f == 0 && cl_j == "0") { // if this tenant has no forwarding address
			if (!confirm("You have not set up a forwarding address for this customer !!\nDo you wish to continue"))
				top.location.reload(); 
				}
		else if (cl_f == 0 && cl_j  != "0") { // if this tenant has no forwarding address and is a joint tenant
			if (!confirm("You have not set up a forwarding address for this customer !!\nThis is a joint tenancy consisting of "+cl_j+" and forwarding addresses should be set up for all tenants. Do you wish to continue"))
				top.location.reload(); 
				}
		else if (cl_f != 0 && cl_o != 0) { // if this tenant has a forwarding address but is a joint tenant with somebody who has no forwarding address
			if (!confirm("One of the other joint attached to this account does not have a forwarding address set up !!\nDo you wish to continue"))
				top.location.reload(); 
				} 
	}

//-- code changes start by TkXel===========================================================================

	function set_values(enquiryID){
		// this function will get called on onLoad event of this form
		// This function used to set values of different elements belongs to this form
		var strEnqID = enquiryID + "";
		if (isNaN (strEnqID ) == "false" || strEnqID  != 'undefined') {
			// enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
			// setting value for temination date text box
			var date = document.getElementById("txt_TERMINATIONDATE");
				date.value = "<%=terminDate%>"
			// setting value for termination notes
			var notes = document.getElementById("txt_NOTES");
				notes.value = "<%=terminNotes%>"
		}
	}
    function LoadVoidPage()
{
    
    window.location.href = "/PropertyDataRestructure/Views/Void/VoidFirstInspections.aspx?src=rsl";	
}
//-- code changes end by TkXel===========================================================================

    </script>
</head>
<body class="TA" onload="check_fowarding();set_values(<%=enqID%>)">
    <% If disable_save = "disabled" Then %>
    <table align="center">
        <tr>
            <td style="font-size: 14px; font-weight: bold; color: red">
                <br />
                <%=Termination_Method%>
            </td>
        </tr>
    </table>
    <% Else %>
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Date :
            </td>
            <td>
                <input type="text" name="txt_TERMINATIONDATE" id="txt_TERMINATIONDATE" value="<%=DateAdd("d", 28, Date) %>"
                    maxlength="10" size="29" class="textbox200" style="width: 200px" />
                <a href="javascript:;" class="iagManagerSmallBlue" onclick="YY_Calendar('txt_TERMINATIONDATE',180,35,'de','#FFFFFF','#133E71')"
                    tabindex="1">
                    <img alt="calender_image" width="18px" height="19px" src="../images/cal.gif" /></a>
            </td>
            <td>
                <div id='Calendar1' style='background-color: white; position: absolute; left: 1px;
                    top: 1px; width: 200px; height: 109px; z-index: 20; visibility: hidden'>
                </div>
                <img src="/js/FVS.gif" name="img_TERMINATIONDATE" id="img_TERMINATIONDATE" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Reason Category :
            </td>
            <td>
                <%=lst_ReasonCategory%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_REASONCATEGORY" id="img_REASONCATEGORY" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                    class="textbox200" rows="7" cols="10" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                <!--#include virtual="includes/bottoms/blankbottom.html" -->
                <input type="button" value="Save" class="RSLButton" title="Save" onclick="save_form()"
                    name="SaveButton" id="SaveButton" <%=disable_save%> style="cursor: pointer" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="hidden" name="hid_TENANCYSTARTDATE" id="hid_TENANCYSTARTDATE" value="<%=tenancy_start_date%>" />
                <input type="hidden" name="txt_REASON" id="txt_REASON" value="<%=reason_id%>" />
            </td>
        </tr>
    </table>
    </form>
    <% End If %>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="400"
        height="100" style="display: none"></iframe>
</body>
</html>
