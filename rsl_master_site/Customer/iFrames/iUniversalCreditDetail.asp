<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
Dim cnt, str_journal_table, str_endRentBalance, out_endRentBalance, nature_id, UC_Title, last_status, ButtonDisabled, ButtonText
Dim notes
OpenDB()
journal_id = Request("journalid")
nature_id = Request("natureid")	
ButtonText = " Update "
build_journal()
CloseDB()

Function build_journal()

    cnt = 0			
    strSQL =    " SELECT ISNULL(CONVERT(NVARCHAR, UC.CreationDate, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), UC.CreationDate, 108) ,'')AS date_Created, " &_ 
	            " ISNULL(UC.Notes, 'N/A') AS Notes,  " &_
                " ISNULL(CONVERT(NVARCHAR, UC.EndDate, 103), 'N/A') AS EndDate,  " &_
	            " ISNULL(CONVERT(NVARCHAR, UC.StartDate, 103), 'N/A') AS StartDate, UC.StartRentBalance, UC.EndRentBalance, E.FIRSTNAME + ' ' +  E.LASTNAME as CreatedBy , UC.PaidDirectlyTo,   " &_
	            " S.DESCRIPTION AS STATUS, ISNULL(J.Title, 'N/A') as Title, J.CURRENTITEMSTATUSID " &_
	            " FROM		C_UniversalCredit UC  " &_
                " LEFT JOIN C_JOURNAL J ON UC.JOURNALID = J.JOURNALID  " &_
                " LEFT JOIN C_STATUS S ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID   " &_
                " LEFT JOIN E__EMPLOYEE E ON UC.CREATEDBY = E.EMPLOYEEID   " &_
                " WHERE		UC.JOURNALID = " & journal_id & " " &_  
                " ORDER 		BY UC.CreationDate DESC "

    Call OpenRs (rsSet, strSQL) 
		
    str_journal_table = ""
   
    While Not rsSet.EOF

        str_endRentBalance = "" 
        out_endRentBalance = ""
        notes = rsSet("Notes")
        if notes = "" then
            notes = "N/A"
        End if  
        if(NOT isNULL(rsSet("EndRentBalance"))) then
          str_endRentBalance = "<strong>Rent Balance:&emsp;</strong>" & "<br>"
          out_endRentBalance = FormatCurrency(rsSet("EndRentBalance"),2) & " (END)<br>"
        End If

	    cnt = cnt + 1 
	    IF cnt > 1 Then
		    str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
	    else
		    UC_Title = rsSet("Title")
		    str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
		    last_status = rsSet("STATUS")
		    if (rsSet("CURRENTITEMSTATUSID") = 14) then
			    ButtonDisabled = " disabled"
			    ButtonText = "No Options"
		    end if
	    End If
		
        str_journal_table = str_journal_table & "<TD width='200'>" & rsSet("date_Created") & "</TD>" &_
					                            "<TD width='120'> <strong>Start Date:&emsp;</strong>" & "<br>" &_
                                                "<strong>Rent Balance:&emsp;</strong>" & "<br>" &_
                                                "<strong>Paid Directly To:&emsp;</strong>" & "<br>" &_
                                                "<strong>End Date:&emsp;</strong>" & "<br>" & str_endRentBalance &_
                                                "<strong>Notes:&emsp;</strong>" & "</TD>" &_
                                                "<TD width='240'>" & rsSet("StartDate") & "<br><strong>" &_
                                                FormatCurrency(rsSet("StartRentBalance"),2) & " (Start)</strong><br>" &_
                                                rsSet("PaidDirectlyTo") & "<br>" &_
                                                rsSet("EndDate") & "<br>" & out_endRentBalance &_
                                                notes & "</TD>" &_
									            "<TD width='180'>" & rsSet("CreatedBy") & "</TD>" &_
									            "<TD width='12'>&nbsp;</TD>" 			
        str_journal_table = str_journal_table & "<TD> </TD>"
        str_journal_table = str_journal_table & "</TD><TR><TR style='height:10px;'></TR>"
        rsSet.movenext()
			
    Wend
	CloseRs(rsSet)
		
	IF cnt = 0 then
		str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
	End If
		
End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

        function update_UC(){
		var str_win
		str_win = "../PopUps/pUniversalCredit.asp?journalid=<%=journal_id%>&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=500,height=355, left=200,top=200") ;
	}
		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=6><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=UC_Title%></TD>
	<TD nowrap width=150>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick="update_UC()" <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=20PX><font color="blue"><b>Date:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100px><font color="blue"><b>Details:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100px><font color="blue"><b></b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120PX><font color="blue"><b>Created By:</b></font></TD>
    
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	