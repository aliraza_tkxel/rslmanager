<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, itemnature_id, title, customer_id, tenancy_id, path, lstAction, nature_id, action,uploadstatus
	Dim Notes,enqID

		enqID = ""
		nature_id = Request("natureid")

	Call OpenDB()
		' check to see whether page is coming back from uploader.vb after upload of file
		uploadstatus= request("hdupload")
		path = request("submit")
		If path = "" Then path = 0 End If
		If path <> 1 Then
			Call get_querystring()
		End If
	Call CloseDB()

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		enqID = Request("EnquiryID")
		action = Request("submit")

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Historical Tenancy File</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body, form
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "txt_NOTES|Notes|TEXT|N"

        function redirect() {
            var tenancy_id = "<%=tenancy_id%>"
            var customer_id = "<%=customer_id%>"
            //alert('The document has been uploaded successfully');
            parent.parent.location.href = "iCustomerJournal.asp?tenancyid=" + tenancy_id + "&customerid=" + customer_id + "&SyncTabs=1"
        }

        function pause(milliseconds) {
            var dt = new Date();
            while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
        }

        function save_form() {
            if (!checkForm()) return false;
            var result = checkFileUpload(RSLFORM, 'pdf');
            if (!result) return false;
            document.getElementById("btnSave").disabled = "true";
            document.getElementById("trMsg").style.display = "block";
            document.RSLFORM.target = "BOTTOM_FRAME";
            document.RSLFORM.action = "iHistoricalTenancyFile_srv.asp";
            document.RSLFORM.submit();
        }

        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function checkUploadStatus() {
            var uploadstatus = "<%=uploadstatus%>"
            // if page is coming from uploader.vb, then uploadstatus will have value of 1
            if (uploadstatus == '1') {
                redirect();
            }
        }

        function Add_Document(agreementId) {
            document.RSLFORM.encoding = "multipart/form-data";
            document.RSLFORM.target = "BOTTOM_FRAME";
            document.getElementById("keycriteria").value = "HISTORICALTENANCYFILEID=" + agreementId;
            document.RSLFORM.action = "/DBFileUpload/uploader.aspx";
            document.RSLFORM.submit();
        }

        function getFileExtension(filePath) { //v1.0
            fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length) : filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length));
            return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
        }

        function checkFileUpload(form, extensions) { //v1.0
            document.MM_returnValue = true;
            if (extensions && extensions != '') {
                for (var i = 0; i < form.elements.length; i++) {
                    field = form.elements[i];
                    if (field.type.toUpperCase() != 'FILE') continue;
                    if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
                        ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                    }
                    if (field.value == '') {
                        ManualError('img_DOCUMENTFILE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                    }
                }
            }
            ManualError('img_DOCUMENTFILE', '', 3);
            return true;
        }

    </script>
</head>
<body class="TA" onload="checkUploadStatus();">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));" class='textbox200'
                    rows="4" cols="10" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Upload
            </td>
            <td>
                <input type="file" size="34" name="rslfile" id="rslfile" class="RSLButton" style="background-color: white;
                    color: black" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DOCUMENTFILE" id="img_DOCUMENTFILE" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                Close Enquiry: &nbsp;
                <input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />&nbsp;
                <!--#include virtual="includes/bottoms/blankbottom.html" -->
                <input type="button" value="Save" class="RSLButton" title="Save" onclick="save_form()" name="btnSave" id="btnSave" style="cursor:pointer" />
                <!--Start of hidden fields for document upload tool-->
                <input type="hidden" id="itemid" name="itemid" value="<%=item_id%>" />
                <input type="hidden" id="title" name="title" value="<%=title%>" />
                <input type="hidden" id="customerid" name="customerid" value="<%=customer_id%>" />
                <input type="hidden" id="tenancyid" name="tenancyid" value="<%=tenancy_id%>" />
                <input type="hidden" id="natureid" name="natureid" value="<%=itemnature_id%>" />
                <input type="hidden" id="hid_Action" name="hid_Action" value="insert" />
                <input type="hidden" id="keycriteria" name="keycriteria" value="" />
                <input type="hidden" id="updatefields" name="updatefields" value="" />
                <input type="hidden" id="filefield" name="filefield" value="DOCUMENTFILE" />
                <input type="hidden" id="filename" name="filename" value="DOCFILE" />
                <input type="hidden" id="tablename" name="tablename" value="C_HISTORICALTENANCYFILE" />
                <input type="hidden" id="connectionstring" name="connectionstring" value="connRSL" />
                <!--End of hidden fields for document upload tool-->
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr id="trMsg" style="display: none">
            <td>
            </td>
            <td>
                Document Upload in Process. Please Wait ...
            </td>
        </tr>
        <tr id="trSuccess" style="display: none">
            <td>
            </td>
            <td>
                Document Uploaded Successfully.
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="BOTTOM_FRAME" id="BOTTOM_FRAME" width="400" height="100" style="display: none">
    </iframe>
</body>
</html>