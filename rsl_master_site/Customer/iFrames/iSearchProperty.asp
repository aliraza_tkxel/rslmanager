<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstDevelopments, lstStatus, lstSubStatus, lstAssetType, lstPropertyType, lstLevel, lstBlocks, customer_id

	customer_id = Request("customerid")

	Call OpenDB()
	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_assettype, NULL, "textbox200", NULL)
	Call BuildSelect(lstPropertyType, "sel_PROPERTYTYPE", "P_PROPERTYTYPE", "PROPERTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lstAdapatationCat, "sel_ADAPTATIONCAT", "P_ADAPTATIONCAT", "ADAPTATIONCATID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200",  " onchange=""GetAdaptations()""")
	Call CloseDB()
%>
<html>
<head>
    <title>Untitled Document</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|N"
        FormFields[1] = "txt_POSTCODE|Postcode|POSTCODE|N"

        function GetAdaptations() {
            document.RSLFORM.sel_ADAPTATIONS.disabled = true
            document.RSLFORM.target = "Property_Server2"
            document.RSLFORM.action = "Serverside/Property_Adaptations.asp"
            document.RSLFORM.submit()
        }

        function SearchNow() {
            if (!checkForm()) return false
            document.RSLFORM.action = "ServerSide/PropertySearch_svr.asp?customerid=<%=customer_id%>"
            document.RSLFORM.target = "SearchResults"
            document.RSLFORM.submit()
            return false
        }

    </script>
</head>
<body>
    <table width="750" style="border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td style="border: 1px solid #133e71">
                <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="border-collapse: collapse;"
                    bgcolor="#f5f5dc">
                    <tr>
                        <td nowrap="nowrap" width="105px" class="TITLE">
                            Property Type:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstPropertyType%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_PROPERTYTYPE" id="img_PROPERTYTYPE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Development:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstDevelopments%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_DEVELOPMENTID" id="img_DEVELOPMENTID" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Street:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_ADDRESS1" id="txt_ADDRESS1" class="textbox200" maxlength="100" />
                            <input type="hidden" name="txt_ADDRESS2" id="txt_ADDRESS2" />
                            <input type="hidden" name="txt_ADDRESS3" id="txt_ADDRESS3" />
                            <input type="hidden" name="txt_TOWNCITY" id="txt_TOWNCITY" />
                            <input type="hidden" name="txt_COUNTY" id="txt_COUNTY" />
                            <input type="hidden" name="DONTDOIT" id="DONTDOIT" value="1" />
                            <input type="hidden" name="ADVANCED" id="ADVANCED" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Postcode:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" name="txt_POSTCODE" id="txt_POSTCODE" class="textbox200" maxlength="10" />
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Adaptation Cat:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstAdapatationCat%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADAPTATIONCAT" id="img_ADAPTATIONCAT" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Adapt. Type:
                        </td>
                        <td nowrap="nowrap">
                            <select name="sel_ADAPTATIONS" id="sel_ADAPTATIONS" class="textbox200" disabled="disabled">
                                <option value="">Please Select an Adapt' Cat'</option>
                            </select>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ADAPTATIONS" id="img_ADAPTATIONS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="TITLE">
                            Asset Type:
                        </td>
                        <td nowrap="nowrap">
                            <%=lstAssetType%>
                        </td>
                        <td nowrap="nowrap">
                            <img src="/js/FVS.gif" name="img_ASSETTYPE" id="img_ASSETTYPE" width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td nowrap="nowrap" align="right">
                            <input type="button" onclick="javascript:location.href='iTenancy.asp?CustomerID=<%=Request("CustomerID")%>'"
                                value=" BACK " title="BACK" class="RSLButtonSmall" style="cursor:pointer" />
                            <input type="button" onclick="SearchNow()" value=" SEARCH " title="SEARCH" class="RSLButtonSmall" style="cursor:pointer" />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            <td width="100%" style="border: 1px solid #133E71" align="center">
                <iframe src="/secureframe.asp" name="SearchResults" id="SearchResults" width="400" height="175" frameborder="0">
                </iframe>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="Property_Server2" id="Property_Server2" width="4px" height="4px"
        style="display: none"></iframe>
</body>
</html>
