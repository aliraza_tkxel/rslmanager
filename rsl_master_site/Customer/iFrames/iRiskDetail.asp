<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText , Risk
	Dim vulCount , latest_general_history_id
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
	
     
		
		cnt = 0
			
					
		
 strSQL = 	    " SELECT		" &_		 
	            " ISNULL(CONVERT(NVARCHAR, G.DTIMESTAMP, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.DTIMESTAMP, 108) ,'')AS CREATIONDATE, " &_ 
	            " ISNULL(CONVERT(NVARCHAR, G.STARTDATE, 103) ,'') AS START, ISNULL(CONVERT(NVARCHAR, G.REVIEWDATE, 103) ,'') AS REVIEW, " &_
	            " ISNULL(G.NOTES, 'N/A') AS NOTES,  " &_
	            " G.RISKHISTORYID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME,  " &_
	            " E2.FIRSTNAME + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME,  " &_
	            " G.ITEMSTATUSID, S.DESCRIPTION AS STATUS, " &_
	            " (SELECT COUNT(*) FROM C_RISKLETTER WHERE RISKHISTORYID = G.RISKHISTORYID) AS LETTERCOUNT " &_	
                " FROM		C_RISK G  " &_
                " LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID   " &_
                " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_ 
                " LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = G.LASTACTIONUSER  " &_
                " LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID  " &_
                " LEFT JOIN C_LETTERACTION AA ON AA.ACTIONID = G.ITEMACTIONID  	" &_				
                " WHERE		G.JOURNALID = " & journal_id & " " &_  
                " ORDER 		BY RISKHISTORYID desc "
					
	
		
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
		
					
			cnt = cnt + 1
			general_history_id = rsSet("RISKHISTORYID")
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("RISKHISTORYID")
				latest_general_history_id = general_history_id
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
			
			'------------------------------------
				strSQL1 =  " SELECT SUB.DESCRIPTION FROM C_CUSTOMER_RISK  CV " &_
              " INNER JOIN C_RISK_SUBCATEGORY SUB ON SUB.SUBCATEGORYID=CV.SUBCATEGORYID " &_
              " WHERE JOURNALID=" & journal_id & " AND RISKHISTORYID=" & general_history_id & " ORDER BY SUB.DESCRIPTION "
              
                 
                Call OpenRs (rsSet1, strSQL1)  
                rskCount = 0
                While Not rsSet1.EOF 
                
                        if rskCount > 0 then
                            Risk = Risk & "<br/>" & "- " & rsSet1("DESCRIPTION") 
                        else
                            Risk = "- " & rsSet1("DESCRIPTION") 
                        end if
                        rskCount = rskCount + 1
      		            rsSet1.movenext()
	            Wend 
                
                CloseRs(rsSet1) 
			
			'---------------------------------------
			
			
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<TD width='150'>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD width='120'>" & rsSet("LASTACTIONUSERNAME") & "</TD>" &_
															"<TD width='200'>" & RISK & "</TD>" &_
															"<TD width='100'>" & rsSet("FULLNAME")  & "</TD>" &_
															"<TD width='20'>" & rsSet("REVIEW") & "</TD>" &_
															"<TD width='338'>" & ResponseNotes & "</TD>" &_
															"<TD width='12'>&nbsp;</TD>" 
															
															
															
															
				If rsSet("LETTERCOUNT") > 0 Then 
					str_journal_table = str_journal_table & "<TD><img src='../Images/attach_letter.gif' style='cursor:hand' title='Open Letter' onclick='open_letter("& rsSet("RISKHISTORYID") &")'></TD>"
				Else
					str_journal_table = str_journal_table & "<TD> </TD>"
				End If
															
				str_journal_table = str_journal_table & 	"</TD><TR>"
				
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id,journalId){
		var str_win
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		str_win ="/RiskFlag/UpdateRiskitem.aspx?RiskHistoryId="+ int_general_history_id + "&TenancyId=" + tenancy_id + "&JournalId=" + journalId;
		window.open(str_win,"display","width=450,height=400, left=200,top=200,scrollbars=1") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	
	function open_letter(letter_id){
		
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/Risk_letter_plain.asp?tenancyid="+tenancy_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
		
	}		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=6><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	<TD nowrap width=150>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick='update_general(<%=latest_general_history_id%>,<%=journal_id%>)' <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=20PX><font color="blue"><b>Date:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100px><font color="blue"><b>Last Action:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=300px><font color="blue"><b>Risk Areas</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100PX><font color="blue"><b>Assigned To:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100PX><font color="blue"><b>Review Date</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=200PX><font color="blue"><b>Notes:</b></font></TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	