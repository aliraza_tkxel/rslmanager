<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id,Tenancy_Title, nature_id, general_history_id, last_status, ButtonDisabled,ButtonText,status_id,action_id,referralHistoryId

	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()
        
        strSQL = "Select top 1 REFERRALHISTORYID,ITEMACTIONID,ITEMSTATUSID from C_REFERRAL Where JOURNALID = "& journal_id & "order by REFERRALHISTORYID desc"  
		    Call OpenRs(rsSet, strSQL)
    		
    		 status_id = rsSet("ITEMSTATUSID")
    		 action_id = rsSet("ITEMACTIONID")
    		 referralHistoryId = rsSet("REFERRALHISTORYID")
			Session("referralHistoryId") = referralHistoryId
		    Call CloseRs(rsSet)  
              strSQL = "SELECT DESCRIPTION as Status from C_STATus Where ITEMSTATUSID  = "& status_id  
		    Call OpenRs(rsSet, strSQL)
    		
    		 last_status  = rsSet("Status")
   		    Call CloseRs(rsSet)  
            
		
        cnt = 0

        strSQL = "Select       ISNULL(CONVERT(NVARCHAR, CF.LastActiondate, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), CF.LastActiondate, 108) ,'') AS ReferralDate " &_
                            " ,ISNULL(CASE A.DESCRIPTION WHEN 'Rejected'THEN CF.REASONNOTES ELSE  CF.NOTES END,'') as Notes " &_
                            " ,ISNULL(S.DESCRIPTION, ' ') as Status, (E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME)as EmployeeName " &_
                            " ,ISNULL(J.TITLE,' ') as Title, A.DESCRIPTION as Action "&_
                            " ,ISNULL(AT.FIRSTNAME+' '+AT.LASTNAME,'N/A')as AssignedTo "&_                            
                    "FROM       C_REFERRAL CF"&_
                             "  Left JOIN C_STATUS S on CF.ITEMSTATUSID = S.ITEMSTATUSID "&_
                              " Left JOIN E__EMPLOYEE on CF.LASTACTIONUSER = E__EMPLOYEE.EMPLOYEEID "&_
                              " Left JOIN C_JOURNAL J on CF.JOURNALID = J.JOURNALID "&_
                             " Left JOIN C_ACTION A on CF.ITEMACTIONID = A.ITEMACTIONID "&_
                             " LEFT JOIN E__EMPLOYEE AT ON CF.ASSIGNTO = AT.EMPLOYEEID "&_
                    "Where      CF.JOURNALID = "& journal_id & " " &_
                    "ORDER      BY  CF.REFERRALHISTORYID DESC "
        Call OpenRs(rsSet, strSQL)

		str_journal_table = ""
		PreviousNotes = ""

        While Not rsSet.EOF

			cnt = cnt + 1
			
            Tenancy_Title = rsSet("Title")
            
            IF cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			else
				str_journal_table = str_journal_table & "<tr valign=""top"">"
            End If
				str_journal_table = str_journal_table & 	"<td width=""110"">" & rsSet("ReferralDate") & "</td>" &_
															"<td width=""120"">" & rsSet("Action") & "</td>" &_
															"<td width=""200"">" & rsSet("Notes") & "</td>" &_
                                                            "<td width=""120"">" & rsSet("AssignedTo") & "</td>" &_
															"<td width=""120"">" & rsSet("EmployeeName") & "</td>"
															
             IF cnt = 1 Then
			  str_journal_table = str_journal_table &	"<td width=""50""><img src='../Images/attach_letter.gif' style='cursor:pointer' title='Open Letter' alt='Open Letter' onclick='open_letter()' /></td>"
             End If 
              str_journal_table = str_journal_table & "</tr>"

			rsSet.movenext()
                
		Wend

		
		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist for this Tenancy Support Referral.</td></tr></tfoot>"
		End If
          Call CloseRs(rsSet) 
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

        function update_general(referralHistoryId, action_id, status_id) {
		var str_win
		str_win = "/TenancySupport/Bridge.aspx?page=updateReferralPage&refid=" + referralHistoryId + "&actid=" + action_id + "&statid=" + status_id;
		window.open(str_win,"display",'width=600,height=550,left=400,top=40, scrollbars=1') ;
	}


	function open_letter() {
	    var str_win
	    str_win = "/TenancySupport/Bridge.aspx?page=viewReferralForm"
	    window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
	}
    </script>
</head>
<body class="TA" onload="parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Tenancy_Title%>
                             </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="black"><%=last_status%></font>
                            </td>
                            <td></td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonText%>" title="<%=ButtonText%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick='update_general(<%=referralHistoryId%>,<%=action_id%>,<%=status_id%>)'
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="110px">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="120px">
                    <font color="blue"><b>Action:</b></font>
                </td>                
                <td style="border-bottom: 1px solid" width="180px">
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="100PX">
                    <font color="blue"><b>Assigned To:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="40px">
                    <font color="blue"><b>By:</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                </td>
<!--                <td style="border-bottom: 1px solid" width="150px">
                    <font color="blue"><b>Category:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="100PX">
                    <font color="blue"><b>Assigned To:</b></font>
                </td>-->
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
