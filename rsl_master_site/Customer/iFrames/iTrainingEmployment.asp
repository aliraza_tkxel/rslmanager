<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: June 17, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim lstUsers, item_id, itemnature_id, customer_id, tenancy_id, path, lstTeams, lstAction, nature_id, lstActivity, type_id, lstKeyWorker, keyworker_id
	Dim Notes, enqID

		enqID = ""
		nature_id = Request("natureid")
        type_id = Request("typeid")
        If type_id = "" Then type_id = -1 End If

	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then
		Call entry()

	'-- code changes start by TkXel===========================================================================
		If enqID <> "" OR Len(enqID) > 0 Then
			' This function will get the details of Arrears Enquiry, based on Enquiry ID from
			' Database tables TO_ENQUIRY_LOG
			' These extracted values will be used to populate matching frame text boxes/drop downs etc.
			Call get_direct_debit_rent_detail(enqID,Notes)
		End If
	'-- code changes end by TkXel===========================================================================

	Else

		Call new_record()

	End If

	Call CloseDB()

	Function entry()

		Call get_querystring()

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		Call OpenRs(rsTeam, SQL)
		If (NOT rsTeam.EOF) Then
			TeamID = rsTeam("TEAMID")
		Else
			TeamID = -1
		End If
		Call CloseRs(rsTeam)

		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", TeamID, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")
		Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = " & nature_id & " " ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", actionid, NULL, "textbox200", " style='width:300px' onchange='action_change()' ")

        SQL = "C_NATURESUBTYPE NST " &_
        "INNER JOIN C_NATURESUBTYPETOACTIVITY NNSTA ON NST.NATURESUBTYPEID = NNSTA.NATURESUBTYPEID " &_
        "INNER JOIN C_NATUREACTIVITY NA ON NNSTA.NATUREACTIVITYID = NA.NATUREACTIVITYID " &_
        "WHERE NST.NATURESUBTYPEID = " & type_id
        Call BuildSelect(lstActivity, "sel_NATUREACTIVITY", SQL ,"NA.NATUREACTIVITYID, NA.DESCRIPTION", "NA.DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:300px' ")

        SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = 109 AND L.ACTIVE = 1 AND J.ACTIVE = 1"
        Call BuildSelect(lstKeyWorker, "sel_KEYWORKER", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' ")

    End Function


	'-- code changes start by TkXel===========================================================================

	'Retrieves data for direct debit request
	Function get_direct_debit_rent_detail(enquiryID, ByRef Notes)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
			' setting stored procedure name
			comm.commandtext = "TO_ENQUIRY_LOG_SelectDirectDebitRentDetail"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			' setting input parameter for sproc eventually used in WHERE clause
			comm.parameters(1) = enquiryID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
			' if record found/returned, setting values
			If Not rs.EOF Then
				Notes = rs("notes")
			End If
		Set comm = Nothing
		Set rs = Nothing

	End Function


	' Update Enquiry Log Table
	Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
		' Setting stored procedure name
			comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
		' Setting input parameter for sproc eventually used in WHERE clause
			comm.parameters(1) = enquiryID
			comm.parameters(2) = journalID
		' Executing sproc and storing resutls in record set
		Set rs = comm.execute
		Set comm = Nothing
		Set rs = Nothing

	End Function



	'-- code changes end by TkXel===========================================================================
	' retirves fields from querystring which can be from either parent crm page or from this page when submitting to itself
	Function get_querystring()
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		enqID = Request("EnquiryID")
	'-- code changes start by TkXel===========================================================================
		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If
	'-- code changes end by TkXel===========================================================================
	End Function

	Function new_record()

		Dim strSQL, journal_id

		Call get_querystring()

		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
		Else
			New_Status = 13
		End If

        activity_id = Request.Form("sel_NATUREACTIVITY")
        If activity_id = "" Then activity_id = "NULL" End If

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, NATURESUBTYPEID, NATUREACTIVITYID) " &_
					"VALUES (" & customer_id & ", NULL, NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", " & type_id & " , " & activity_id & ");" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		Set rsSet = Conn.Execute(strSQL)
			journal_id = rsSet.fields("JOURNALID").value
			rsSet.close()
		Set rsSet = Nothing

		ItemAction = Request.Form("sel_ITEMACTIONID")

		If (ItemAction = "") Then
			ItemAction = "NULL"
		End If

        keyworker_id = Request.Form("sel_KEYWORKER")
        If keyworker_id = "" Then keyworker_id = "NULL" End If

        activity_dt = Request.Form("txt_ACTIVITYDATE")
        If activity_dt = "" Then activity_dt = "NULL" Else activity_dt = "'" & Request.Form("txt_ACTIVITYDATE") & "'" End If

		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_TRAININGEMPLOYMENT " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ASSIGNTO, NOTES, NATURESUBTYPEID, NATUREACTIVITYID, KEYWORKERID, ACTIVITYDATE) " &_
					"VALUES (" & journal_id & ", " & New_Status & ", " & Request.Form("sel_ITEMACTIONID") & ", " & Session("userid") &_
					 ", " & Request.Form("sel_USERS") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "', " & type_id & " , " & activity_id & ", " & keyworker_id & "," & activity_dt & ")" &_
					"SELECT SCOPE_IDENTITY() AS TRAININGHISTORYID;"

		Set rsSet = Conn.Execute(strSQL)
			traininghistory_id = rsSet.fields("TRAININGHISTORYID").value
			rsSet.close()
		Set rsSet = Nothing

		'-- code changes start by TkXel=========================================================================
		If enqID <> "" OR Len(enqID) > 0 Then
			Call update_enquiry_log(enqID, journal_id)
		End If
		'-- code changes end by TkXel===========================================================================

		' INSERT LETTER IF ONE EXISTS
		If Request.Form("hid_LETTERTEXT") <> "" Then
			strSQL = "INSERT INTO C_TRAININGCUSTOMERLETTERS (TRAININGHISTORYID, LETTERCONTENT) VALUES (" & traininghistory_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "')"
			conn.execute(strSQL)
		End If

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Training & Employment Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body, form
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_TEAMS|TEAM|SELECT|Y"
        FormFields[3] = "sel_ITEMACTIONID|Team|SELECT|Y"
        FormFields[4] = "sel_NATUREACTIVITY|Activity|SELECT|Y"
        FormFields[5] = "txt_ACTIVITYDATE|Date|DATE|Y"
        FormFields[6] = "sel_KEYWORKER|KeyWorker|SELECT|Y"


        function set_values(enquiryID) {
            // this function will get called on onLoad event of this form
            // This function used to set values of different elements belongs to this form
            var strEnqID = enquiryID + "";
            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {
                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
                // setting value for complaint abandoned description
                var description = document.getElementById("txt_NOTES");
                description.value = "<%=Notes%>"
            }
        }

        function save_form() {
            if (!checkForm()) return false;
            document.RSLFORM.target = "CRM_BOTTOM_FRAME";
            document.RSLFORM.action = "iTrainingEmployment.asp?typeid=<%=type_id%>&itemid=<%=item_id%>&natureid=<%=itemnature_id%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
            document.RSLFORM.submit();
        }

        function GetEmployees() {
            if (document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value != "") {
                document.RSLFORM.action = "../Serverside/GetEmployees.asp"
                document.RSLFORM.target = "ServerIFrame"
                document.RSLFORM.submit()
            }
            else {
                document.getElementById("dvUsers").innerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200' style='width:300px'><option value=''>Please select a team</option></select>"
            }
        }

        function GetEmployee_detail() { }

        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function open_letter() {
            if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
                ManualError("img_LETTERID", "You must first select a letter to view", 1)
                return false;
            }
            var tenancy_id = parent.parent.MASTER_TENANCY_NUM
            window.open("../Popups/arrears_letter.asp?tenancyid=" + tenancy_id + "&letterid=" + document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "display", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

        function action_change() {
            document.RSLFORM.action = "../Serverside/change_letter_action.asp?ITEMACTIONID=" + document.getElementById("sel_ITEMACTIONID").options[document.getElementById("sel_ITEMACTIONID").selectedIndex].value
            document.RSLFORM.target = "ServerIFrame"
            document.RSLFORM.submit()
        }
	
    </script>
</head>
<body class="TA" onload="set_values(<%=enqID%>)">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td>Activity : </td>
            <td><%=lstActivity%></td>
            <td><img src="/js/FVS.gif" name="img_NATUREACTIVITY" id="img_NATUREACTIVITY" width="15px" height="15px"
                    border="0" alt="" /></td>            
        </tr>
        <tr>
            <td>Date : </td>
            <td><input id="txt_ACTIVITYDATE" name="txt_ACTIVITYDATE" class="textbox" style="width: 75px;" value="" onclick="YY_Calendar('txt_ACTIVITYDATE',0,0,'de','#FFFFFF','#133E71','YY_calendar1')" /></td>
            <td><img src="/js/FVS.gif" name="img_ACTIVITYDATE" id="img_ACTIVITYDATE" width="15px" height="15px"
                    border="0" alt="" /></td>
        </tr>
        <tr>
            <td>Key Worker : </td>
            <td><%=lstKEYWORKER %></td>
            <td><img src="/js/FVS.gif" name="img_KEYWORKER" id="img_KEYWORKER" width="15px" height="15px"
                    border="0" alt="" /></td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                    class="textbox200" rows="2" cols="10" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Team :
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Assign To :
            </td>
            <td>
                <div id="dvUsers">
                    <%=lstUsers%></div>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Action :
            </td>
            <td>
                <%=lstAction%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Letter :
            </td>
            <td>
                <div id="dvLetter">
                    <select name="sel_LETTER" id="sel_LETTER" class="textbox200" style="width: 300px">
                        <option value=''>Please select action</option>
                    </select>
                </div>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                Close Enquiry: &nbsp;
                <input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />&nbsp;
                <!--#include virtual="includes/bottoms/blankbottom.html" -->                
                <input type="button" value="View Letter" title="View Letter" class="RSLButton" onclick="open_letter()" name="button" style="cursor: pointer" />
                <input type="button" value="Save" class="RSLButton" title="Save" onclick="save_form()" name="button" style="cursor: pointer" />
                <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                <input type="hidden" name="hid_NATURESUBTYPEID" id="hid_NATURESUBTYPEID" value="<%=type_id%>" />
            </td>
        </tr>
    </table>
    </form>
    <div id="Calendar1" style="background-color: white; position: absolute; left: 1px;
        top: 1px; width: 200px; height: 115px; z-index: 20; visibility: hidden">
    </div>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4" height="1"
        style="display: none"></iframe>
</body>
</html>
