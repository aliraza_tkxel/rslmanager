<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/Includes/Database/adovbs.inc"-->
<%
	Dim  item_id, itemnature_id, title, customer_id, tenancy_id
	Dim  lstAbandoned
	Dim locationDesc, lookupValId, AbandonedDescription 

	'' Created By:	Naveed Iqbal (TkXel)
	'' Created On: 	June 24, 2008
	'' Reason:		Integraiton with Tenats Online

    Dim enqID
    enqID = ""

	Call OpenDB()
	path = request("submit")
	if path = "" then path = 0 end if
	if path  <> 1 then

		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback

		enqID = Request("EnquiryID")

		complaintCategory = ""
		complaintNotes = ""

		' Enters into if block only, if EnquiryID is part of query string parameters
      	If enqID <>"" OR Len(enqID) > 0 Then

			' This function will get the details of Service Complaint, based on Enquiry ID from 
			' Database tables TO_ENQUIRY_LOG and TO_ABANDONED

			' These extracted values will be used to populate matching frame text boxes/drop downs etc.
			Call get_abandoned_detail(enqID, locationDesc,lookupValId, AbandonedDescription )

		End If

		Call entry()
	Else
		Call new_record()
	End If

	Call CloseDB()
 
	Function entry()
	 	Call get_querystring()
		Call BuildSelect(lstAbandoned, "sel_Category", "TO_LOOKUP_VALUE WHERE LookUpCodeID = 10"  , "LookUpValueID , Name ", "Name","Please Select" , NULL, NULL, "textbox200", "style='width:300px' ")
	End Function

	Function get_abandoned_detail(enquiryID, ByRef locationDescription, ByRef lookupValueId,ByRef abandonedDesc)

		' CALL To STORED PROCEDURE TO get Complaint Detail

		Set comm = Server.CreateObject("ADODB.Command")

		' setting stored procedure name
		comm.commandtext = "TO_ABANDONED_SelectAbandonedDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID

		' executing sproc and storing resutls in record set
		Set rs = comm.execute

		' if record found/returned, setting values
		If Not rs.EOF Then 
			locationDescription= rs("LocationDescription")
			lookupValueId= rs("LookUpValueID")
			abandonedDesc = rs("OtherInfo")
		End If

		Set comm = Nothing
		Set rs = Nothing

      End Function

	' retieves fields from querystring which can be from either parent crm page or from this page when submitting to self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If

	End Function


Function new_record()


		Dim journal_id, assignTo
		assignTo = -1
		Call get_querystring()

		lookUpValueID  = Request.Form("sel_Category")
		location  = Request.Form("txt_Location")
		otherInfo  = Replace(Request.Form("txt_Description"), "'", "''")

		Set comm = Server.CreateObject("ADODB.Command")

		' setting stored procedure name
		comm.commandtext = "C_ABANDONED_CREATE"
		comm.commandtype = 4
	    comm.namedparameters = true
   	    Set comm.activeconnection = Conn

	    ' this parameter will return code/value, i.e. JOURNALID or -1
	    comm.parameters.append( comm.CreateParameter ("@returnValue" , adInteger, adParamReturnValue) )

	 ' setting input parameter for sproc
	  comm.parameters.append (comm.CreateParameter ("@CUSTOMERID", adInteger, adParamInput, 4, customer_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TENANCYID", adInteger, adParamInput, 4, tenancy_id) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMID", adInteger, adParamInput, 4, item_id ) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMNATUREID", adInteger, adParamInput, 4, itemnature_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TITLE", adVarChar, adParamInput, 50, TITLE ) )

	  comm.parameters.append (comm.CreateParameter ("@LASTACTIONUSER", adInteger, adParamInput, 4, Session("userid")) )

	  comm.parameters.append (comm.CreateParameter ("@LOOKUPVALUEID", adInteger, adParamInput, 4, lookUpValueID) )
	  comm.parameters.append (comm.CreateParameter ("@LOCATION", adVarchar, adParamInput, 4000, location) )
	  comm.parameters.append (comm.CreateParameter ("@OTHERINFO", adVarchar, adParamInput, 4000, otherInfo) )

  	  ' executing sproc
 
	  comm.execute()

	  journalID =  comm.Parameters("@returnValue").value

	  If enqID <> "" OR Len(enqID) > 0 Then
	    Call update_enquiry_log(enqID, journalID)
      End If

		Set comm = Nothing
		Set rs = Nothing

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End Function

     ' Update Enquiry Log Table
     Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")

		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID

		' Executing sproc and storing resutls in record set
		Set rs = comm.execute

		Set comm = Nothing
		Set rs = Nothing

     End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "sel_Category|Category|SELECT|Y"
        FormFields[1] = "txt_Location|Location|TEXT|Y"
        FormFields[2] = "txt_Description|description|TEXT|N"

        var set_length = 4000;

        // if text entered in textbox exceeds max allowed characters, it will reduced to 4000
        function countMe() {

            var str = event.srcElement
            var str_len = str.value.length;

            if (str_len >= set_length) {
                str.value = str.value.substring(0, set_length - 1);
            }
        }




        function save_form() {

            if (!checkForm()) return false;

            RSLFORM.target = "CRM_BOTTOM_FRAME";
            RSLFORM.action = "iAbandonedPropertyVehicle.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
            RSLFORM.submit();
        }

        function set_values(enquiryID) {

            // this function will get called on onLoad event of this form

            // This function used to set values of different elements belongs to this form
            var strEnqID = enquiryID + "";

            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {

                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter


                // setting value for location description
                var location = document.getElementById("txt_Location");
                location.value = "<%=locationDesc%>"

                // setting value for complaint category drop down
                var category = document.getElementById("sel_Category");
                category.value = "<%=lookupValId%>"

                // setting value for complaint abandoned description
                var description = document.getElementById("txt_Description");
                description.value = "<%=AbandonedDescription%>"

            }

        }

    </script>
</head>
<body class="TA" onload="set_values(<%=enqID%>)">
    <form name="RSLFORM" method="post" action="">
    <table width="445" border="0" cellpadding="0" cellspacing="2">
        <tr style="height: 7px">
            <td>
                There is an abandoned :
            </td>
            <td>
                <%=lstAbandoned%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_Category" id="img_Category" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
            <tr>
                <td>
                    Describe location:
                </td>
                <td>
                    <textarea style="overflow: hidden; width: 300px" onkeyup="countMe()" class="textbox200"
                        rows="3" name="txt_Location" id="txt_Location"></textarea>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_Location" id="img_Location" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Other information:
                </td>
                <td>
                    <textarea style="overflow: hidden; width: 300px" onkeyup="countMe()" class='textbox200'
                        rows="3" name="txt_Description" id="txt_Description"></textarea>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_Description" id="img_Description" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
                <tr>
                    <td>
                    </td>
                    <td align="right">
                        <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                        <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                        <input type="button" value=" Save " class="RSLButton" onclick="save_form()" name="button3" />
                    </td>
                    <td align="right">
                    </td>
                </tr>
        </table>
    </form>
    <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="400"
        height="100" style="display: none"></iframe>
</body>
</html>
