<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	'' Created By:	Naveed Iqbal (TkXel)
	'' Created On: 	June 28, 2008
	'' Reason:		Integraiton with Tenats Online

	Dim cnt,  str_journal_table,  nature_id, general_history_id, last_status, ButtonDisabled, ButtonText,Abandoned_Title
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), "  &_
					"			G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.OTHERINFO, 'N/A') AS NOTES," &_
					"			G.ABANDONEDHISTORYID, J.TITLE," &_
					"			E2.FIRSTNAME + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME, " &_
					"			L.NAME as Category," &_
					"			G.ITEMSTATUSID, " &_
					"			G.LOCATION, " &_
					"			S.DESCRIPTION AS STATUS" &_
					"   FROM C_ABANDONED G LEFT JOIN " &_
					"			C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID   " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = G.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID" &_
					"			LEFT JOIN TO_LOOKUP_VALUE L ON G.LOOKUPVALUEID= L.LOOKUPVALUEID" &_			
					" WHERE		G.JOURNALID = " & journal_id & " " &_
					" ORDER 		BY ABANDONEDHISTORYID DESC "

		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""

		PreviousNotes = ""
		PreviousLocation=""

		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else

				Abandoned_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("ABANDONEDHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If

		
				Location = rsSet("LOCATION")
				if (Location = PreviousLocation) then
					ResponseLocation = "[Same As Above]"
				else
					ResponseLocation = Location
				end if
				PreviousLocation = Location


				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes


				str_journal_table = str_journal_table & 	"<TD width='110'>" & FormatDateTime(rsSet("CREATIONDATE"),2) & "</TD>" &_
															"<TD width='220'>" & rsSet("LASTACTIONUSERNAME")  & "</TD>" &_
															"<TD width='120'>" & rsSet("Category")  & "</TD>" &_
															"<TD width='338'>" & ResponseLocation & "</TD>" &_
															"<TD width='338'>" & ResponseNotes & "</TD>" &_
															"<TD width='12'>&nbsp;</TD>" 
															
				str_journal_table = str_journal_table & 	"<TD></TD><TR>"
				
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pAbandonedPropertyVehicle.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=300, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=5><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Abandoned_Title%></TD>
	<TD nowrap width=150>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick='update_general(<%=general_history_id%>)' <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=110PX><font color="blue"><b>Date:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=220px><font color="blue"><b>Last Action User:</b></font></TD>
	<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120px><font color="blue"><b>Category:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=400px><font color="blue"><b>Location:</b></font></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=400px><font color="blue"><b>Other Info:</b></font></TD>
	<td STYLE="BORDER-BOTTOM:1PX SOLID">&nbsp;</td>	
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
