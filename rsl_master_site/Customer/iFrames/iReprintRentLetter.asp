<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, tenancyid, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	Dim JournalTitle
	Dim Letterid
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	top 1 VJ.DESCRIPTION AS LASTACTION, J.JOURNALID, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			ISNULL(SL.SATISFACTION_LETTERSTATUS,0) AS LETTERSTATUS, " &_
					"			J.ITEMNATUREID, " &_
					" 			j.tenancyid ,CR.Letterid " &_
					"FROM	 	C_JOURNAL J " &_
					"			LEFT JOIN C_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"			LEFT JOIN C_SATISFACTIONLETTER SL ON SL.JOURNALID = J.JOURNALID " &_
					"			inner join C_RENTLETTER CR ON  CR.TENANCYID=J.TENANCYID and year(J.CREATIONDATE)= year(cr.datestamp) " &_
					"			LEFT JOIN (	SELECT J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION, LA.ACTIONID   " &_ 
					"						FROM C_JOURNAL  J   " &_
					"							INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID   " &_
					"							INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID AND AR.ITEMACTIONID NOT IN (12,13)   " &_
					"						WHERE 	 " &_
					"												 AR.ARREARSHISTORYID = ( select max(ARREARSHISTORYID) from C_ARREARS where JOURNALID = J.JOURNALID  )   " &_
					"		GROUP BY J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION,LA.ACTIONID) VJ ON VJ.JOURNALID = j.JOURNALID   " &_								
					"WHERE	 J.ITEMNATUREID NOT IN (1,2,20,21,22) AND J.JOURNALID ="  & JOURNAL_ID 		
			'"WHERE	 ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'')=ISNULL(CONVERT(NVARCHAR, cr.datestamp, 103) ,'')  AND 	J.ITEMNATUREID NOT IN (1,2,20,21,22) AND J.JOURNALID ="  & JOURNAL_ID 
					
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
				cnt = cnt + 1
				IF cnt > 1 Then
					str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
				else
					str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"			
				End If
				tenancyid = rsSet("tenancyid")
				Letterid= rsSet("Letterid")
				
				str_journal_table = str_journal_table & 	"<TD COLSPAN=4>" &_
															"<TABLE width'100%'><TR><TD>" &_
															"<TD width='160' valign=top>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD width='138'valign=top>" & rsSet("item") & "</TD>" &_
															"<TD width='245' valign=top>" & rsSet("nature") & "</TD>" &_
															"<TD width='246' valign=top><img src='../../myImages/PrinterIcon.gif' width='31' height='20' title='Re-Print Letter' onClick='RePrintRentLetter()' style='cursor:hand'> Click to Re-Print</TD>" &_
															"</TD></TR></TABLE>" &_
															"</TD><TR>"
				
				JournalTitle = rsSet("FULLTITLE")
		rsSet.movenext()
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function View_Report(){
		var str_win
		str_win = "../PopUps/pInspection.asp?InspectionID=<%=journal_id%>&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=550,height=400, left=100,top=100,scrollbars") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
		
	function RePrintRentLetter(Letterid){

		switch("<%=JournalTitle%>") {
			case "05/06 Increase":
			    //str_win2 = "../MailingLists/PopupRentLetter.asp?idlist=<%=tenancyid%>&printaction=RePrint&Letterid=<%=Letterid%>"  
			    alert("The rent increase letter for the year selected is no longer available.\nPlease use the current rent increase item and letter");
			    break;
			case "06/07 Increase":{
					if (confirm(" Are you sure you want to re-print a rent increase letter for 06/07")){
						str_win2 = "../MailingLists/PopupRentLetter_PRE2007.asp?idlist=<%=tenancyid%>&printaction=RePrint&Letterid=<%=Letterid%>&YEAR=2006"  
						window.open(str_win2,"display","width=700,height=400, left=100,top=100,scrollbars") 		  
					}
					break;
			} // end of case
			case "07/08 Increase":{
					if (confirm(" Are you sure you want to re-print a rent increase letter")){
						//	str_win2 = "../MailingLists/PopupRentLetter_2007.asp?idlist=<%=tenancyid%>&printaction=RePrint&Letterid=<%=Letterid%>&YEAR=2007"  
						//' IMORTANT NOTE FOR 2008:: COMMENT THE LINE BELOW AND 2007 LETTER CAN USE THE STATIC VERSION ABOVE
						str_win2 = "../MailingLists/PopupRentLetter3.asp?idlist=<%=tenancyid%>&printaction=RePrint&Letterid=<%=Letterid%>&YEAR=2007"  
						window.open(str_win2,"display","width=700,height=400, left=100,top=100,scrollbars") 
					} // end of if
					break;
			} // end of case
			default:{
					if (confirm(" Are you sure you want to re-print a rent increase letter")){
						//' IMORTANT NOTE FOR 2008:: COMMENT THE LINE BELOW AND 2007 LETTER CAN USE THE STATIC VERSION ABOVE
						str_win2 = "../MailingLists/PopupRentLetter.asp?idlist=<%=tenancyid%>&printaction=RePrint&Letterid=<%=Letterid%>&JOURNALID=<%=journal_id%>"  
						window.open(str_win2,"display","width=750,height=400, left=100,top=100,scrollbars") 
					} // end of if
					break;
			}
	    } // end of switch
	}
		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onLoad="DoSync();parent.STOPLOADER('BOTTOM')">
<form name = RSLFORM method=post>
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR >
	<TD COLSPAN=5><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	      <TD nowrap width=150></td>
          <td align=right width=100></td>
        </tr></table>
	</TD>
	</TR>
	<TR valign=top>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=155><font color="blue"><b>Date:</b></font></TD>		
    	 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=138><font color="blue"><b>Item:</b></font></TD>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=245><font color="blue"><b>Nature:</b></font></TD>
	  <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=246><strong>Action to undertake </strong></TD>
	  <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=9></TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=4></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	</form>
</BODY>
</HTML>	
	
	
	
	
	