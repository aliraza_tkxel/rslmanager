<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT				ISNULL(CONVERT(NVARCHAR, G.DATESTAMP, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.DATESTAMP, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.DESCRIPTION, 'N/A') AS NOTES, " &_
					"			G.DDITEMID AS GENERALHISTORYID, J.TITLE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS, " &_	
					"			(SELECT COUNT(*) FROM C_LETTERS_SAVED WHERE HISTORYID = g.DDITEMID and itemnatureid = 49 ) AS LETTERCOUNT " &_			
					"FROM		C_DDSCHEDULE_JOURNALITEM G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.NOTEDBY " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY DDITEMID DESC "
'rw strsql
		
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("GENERALHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<TD width='120'>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD width='438'>" & ResponseNotes & "</TD>" &_
															"<TD width='22'>&nbsp;</TD>" &_
															"<TD width='150'>" & rsSet("FULLNAME")  & "</TD>"
				If rsSet("LETTERCOUNT") > 0 Then 
					str_journal_table = str_journal_table & "<TD><img src='../Images/attach_letter.gif' style='cursor:hand' title='Open Letter' onclick='open_letter("& rsSet("GENERALHISTORYID") &","& nature_id &")'></TD>"
				Else
					str_journal_table = str_journal_table & "<TD> </TD>"
				End If
															
				str_journal_table = str_journal_table & 	"</TD><TR>"
				
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pGeneral.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=355, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	
	function open_letter(letter_id,nature_id){
		
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/servicecomplaints_letter_plain.asp?tenancyid="+tenancy_id+"&natureid="+nature_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	
	}		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=5><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	<TD nowrap width=100></td><td align=right width=150>
	
Current Status:&nbsp;<font color=red><%=last_status%></font>
<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="display:none;background-color:beige" onclick='update_general(<%=general_history_id%>)' <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX><font color="blue"><b>Date:</b></font></TD>
		
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=500px><font color="blue"><b>Description:</b></font></TD>
	<td STYLE="BORDER-BOTTOM:1PX SOLID">&nbsp;</td>	
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX><font color="blue"><b>Created By:</b></font></TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	