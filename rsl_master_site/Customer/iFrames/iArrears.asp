<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams, lstAction
	Dim Notes,enqID,balance
	enqID = ""

	OpenDB()
	
	path = request("submit")
	if path = "" then path = 0 end if
	if path  <> 1 then
		entry()
		
		If enqID <>"" OR Len(enqID) > 0 Then

			' This function will get the details of Arrears Enquiry, based on Enquiry ID from 
			' Database tables TO_ENQUIRY_LOG

			' These extracted values will be used to populate matching frame text boxes/drop downs etc. 
			Call  get_arrear_detail(enqID,Notes )
			Call get_rent_balance(enqID,balance)
		End If

	Else
		new_record()
	End If
	
	CLoseDB()
	
	Function entry()
		get_querystring()
'		if (item_id = 3) then
'			Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID AND TEAMCODE IN ('EXE','MAR') ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
'		else
'			Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")		
'		end if

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		Call OpenRs(rsTeam, SQL)
		if (NOT rsTeam.EOF) then
			TeamID = rsTeam("TEAMID")
		else
			TeamID = -1
		end if
		CloseRs(rsTeam)
		
		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", TeamID, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")
		Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE = 10" ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", actionid, NULL, "textbox200", " style='width:300px' onchange='action_change()' ")

	End Function
	
	//Retrieves data for arrear request
     Function get_arrear_detail(enquiryID, ByRef Notes )

		' CALL To STORED PROCEDURE TO get Arrears details

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_SelectArrearDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

		// if record found/returned, setting values
		If Not rs.EOF Then 
			Notes = rs("notes")

		End If
		
		Set comm = Nothing
		Set rs = Nothing

     End Function

     // Retrieves data for arrear request
     Function get_rent_balance(enquiryID, ByRef balance )

		' CALL To STORED PROCEDURE TO get Arrears details

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_Customer_GetAccountBalance"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = tenancy_id

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

		// if record found/returned, setting values
		If Not rs.EOF Then 
			balance = rs("AccountBalance")

		End If
		
		Set comm = Nothing
		Set rs = Nothing

     End Function

     ' Update Enquiry Log Table
     Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")

		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID

		' Executing sproc and storing resutls in record set
		Set rs = comm.execute

		Set comm = Nothing
		Set rs = Nothing

     End Function

	// retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		enqID = Request("EnquiryID")

		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If

	End Function
	
	Function new_record()
	
		Dim strSQL, journal_id
		get_querystring()

		if (Request.Form("chk_CLOSE") = 1) then
			New_Status = 14
		else
			New_Status = 13
		end if
		
		if not tenancy_id <> "" then tenancy_id = "NULL" end if
		
		// JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		rsSet.close()
		Set rsSet = Nothing
		
		// ARREARS ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_ARREARS " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ASSIGNTO, NOTES) " &_
					"VALUES (" & journal_id & ", " & New_Status & ", " & Request.Form("sel_ITEMACTIONID") & ", " & Session("userid") &_
					 ", " & Request.Form("sel_USERS") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "')" &_
					"SELECT SCOPE_IDENTITY() AS ARREARSID;"
	
		set rsSet = Conn.Execute(strSQL)
		arears_id = rsSet.fields("ARREARSID").value
		rsSet.close()
		Set rsSet = Nothing
			
		
		' Code Added by TkXel - Start

		If enqID <> "" OR Len(enqID) > 0 Then
		    Call update_enquiry_log(enqID, journal_id)
        End If		

		' Code Added by TkXel - End
				
		// INSERT LETTER IF ONE EXISTS
		//response.write "<BR> EMTPY IS IT " & Request.Form("hid_LETTERTEXT")
		If Request.Form("hid_LETTERTEXT") <> "" Then
			strSQL = "INSERT INTO C_CUSTOMERLETTERS (ARREASHISTORYID, LETTERCONTENT,LETTERSIG) VALUES (" & arears_id & ",'" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") & "','"& Replace(Request.Form("hid_SIGNATURE"),"'", "''") & "')"
			conn.execute(strSQL)
		End If
		
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
	FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[2] = "sel_TEAMS|TEAM|SELECT|Y"
	FormFields[3] = "sel_ITEMACTIONID|Team|SELECT|Y"
	
	function save_form(){
		if (!checkForm()) return false;
		RSLFORM.target = "CRM_BOTTOM_FRAME";
		RSLFORM.action = "IArrears.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
		RSLFORM.submit();
	}

	function GetEmployees(){
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.action = "../Serverside/GetEmployees.asp"
			RSLFORM.target = "ServerIFrame"
			RSLFORM.submit()
			}
		else {
			RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' class='textbox200' STYLE='WIDTH:300PX'><option value=''>Please select a team</option></select>"
			}
		}

	var set_length = 4000;

	function countMe() {
	  
	  	var str = event.srcElement
	  	var str_len = str.value.length;

		  if(str_len >= set_length) {
			  str.value = str.value.substring(0, set_length-1);
	  			}
	}			
	
	function open_letter(){
	
		if (RSLFORM.sel_LETTER.value == "") {
			ManualError("img_LETTERID", "You must first select a letter to view", 1)
			return false;
		}		
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/arrears_letter.asp?tenancyid="+tenancy_id+"&letterid="+RSLFORM.sel_LETTER.value, "display","width=570,height=600,left=100,top=50,scrollbars=yes");
		
	}
	
	function action_change(){
	
		RSLFORM.action = "../Serverside/arrears_change_action.asp?ITEMACTIONID="+RSLFORM.sel_ITEMACTIONID.value
		RSLFORM.target = "ServerIFrame"
		RSLFORM.submit()
	
	}
function set_values(enquiryID){

		// this function will get called on onLoad event of this form

		// This function used to set values of different elements belongs to this form
		var strEnqID = enquiryID + "";

	}
	
</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class="TA" onload="set_values(<%=enqID%>)"   >
	<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
<FORM NAME=RSLFORM METHOD=POST>		
		<TR style='height:7px'><TD></TD></TR>
		<TR>
		<TD VALIGN=TOP>Notes :
		</TD><TD><!--<textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class='textbox200' rows=4 name="txt_NOTES"><%=Data%></textarea>-->
		<textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class='textbox200' rows=4 name="txt_NOTES"><%=nOTES%></textarea>
		</TD>	
      <TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
      </TD>
		</TR>
		<tr>
			<td>Team : </td>
			<td><%=lstTeams%></td>
			<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Assign To : </td>
			<td><%=lstUsers%></td>
			<TD><image src="/js/FVS.gif" name="img_USERS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Action : </td>
			<td><%=lstAction%></td>
			<TD><image src="/js/FVS.gif" name="img_ITEMACTIONID" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Letter : </td>
			<td><SELECT NAME='sel_LETTER' class='textbox200' style='width:300px'>
					<OPTION VALUE=''>Please select action</OPTION>
				</SELECT></td>
			<TD><image src="/js/FVS.gif" name="img_LETTERID" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>		
		  <TD COLSPAN=2 align=right nowrap>Close Enquiry: &nbsp; 
	        <input type="checkbox" name="chk_CLOSE" value="1">&nbsp;
			<!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
			<input type="button" value="View Letter" class="RSLButton"  onClick="open_letter()" "name="button">
            <input type="button" value="Save" class="RSLButton" title='View letter' onClick="save_form()" "name="button">
</TD>
		</TR>
		<INPUT TYPE=HIDDEN NAME=hid_LETTERTEXT value="">
		<INPUT TYPE=HIDDEN NAME=hid_SIGNATURE value="">
	</FORM>
   	</TABLE>
<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>
</BODY>
</HTML>	
	
	
	
	
	