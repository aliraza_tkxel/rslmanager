<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim item_id, itemnature_id, title, customer_id, tenancy_id, action, nature_id, Agreementhistory_id

	Dim Notes,enqID

	Call OpenDB()

	action = request.Form("hid_Action")
	If action = "insert" Then
		Call getPostedData()
		Call new_record()
	End If
	Call CloseDB()

	Function getPostedData()
		item_id = Request.Form("itemid")
		itemnature_id = Request.Form("natureid")
		title = Request.Form("title")
		customer_id = Request.Form("customerid")
		tenancy_id = Request.Form("tenancyid")
	End Function

	Function AmendedFileName(FilePath)
		Dim FilePart
		FilePart=Split(filePath, "\")
		AmendedFileName=FilePart(UBound(FilePart))
	End Function

	Function new_record()

		Dim strSQL, journal_id

		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
		Else
			New_Status = 13
		End If

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", NULL, NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsSet = Conn.Execute(strSQL)
			journal_id = rsSet.fields("JOURNALID").value
			rsSet.close()
		Set rsSet = Nothing

		ItemAction = Request.Form("sel_ITEMACTIONID")

		If (ItemAction = "") Then
			ItemAction = "NULL"
		End If

		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_TENANCYAGREEMENT " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, NOTES, DOCFILE) " &_
					"VALUES (" & journal_id & ", " & New_Status & ", " & Session("userid") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "','" & Replace(AmendedFileName(Request("rslfile")), "'", "''") & "')" &_
					"SELECT SCOPE_IDENTITY() AS AGREEMENTHISTORYID;"

		set rsSet = Conn.Execute(strSQL)
			Agreementhistory_id = rsSet.fields("AGREEMENTHISTORYID").value
			rsSet.close()
		Set rsSet = Nothing

		'Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Tenancy Agreement</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function addDocument() {
            if ("<%=action%>" == "insert") {
                //setTimeout("red()", 47000);
                parent.Add_Document("<%=Agreementhistory_id%>")
            }
            return
        }

        function red() {
            parent.redirect();
        }
    </script>
</head>
<body onload="addDocument();">
</body>
</html>
