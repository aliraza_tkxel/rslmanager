<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim item_id, itemnature_id, status_id, title, customer_id, tenancy_id, property_id, lstContractors, L_Contractor, Data, path, supplier

	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then
		Call entry()
	Else
		Call new_record()
	End If

	Call CloseDB()

	Function entry()

		Call get_querystring()
		Call BuildSelect(lstContractors, "sel_CONTRACTOR", " S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select", L_Contractor, NULL, "textbox200", " style='width:300px' " & DisableAll & "")

	End Function

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		If (Request("Text")<> "") Then
			title = "Defect automated from Repair"
		ElseIf (Request("WarrantyItem") <> "") Then
			title = "Warranty Defect"
			WhichItem = Request("WarrantyItem")
			Data = Request("Data" & WhichItem)
			L_Contractor = Request("Contractor" & WhichItem)
		End If
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		supplier = Request.Form("sel_CONTRACTOR")
		If (supplier = "") Then
			supplier = "NULL"
		End If

	End Function

	Function get_property()
		str_sql = "SELECT PROPERTYID FROM C_TENANCY WHERE TENANCYID = " & tenancy_id
		Call OpenRs(rsSet, str_sql)
		If Not rsSet.EOF Then
			property_id = "'" & rsSet(0) & "'"
		Else
			property_id = "NULL"
		End If
		Call CloseRs(rsSet)
	End Function

	Function new_record()
	
		Dim strSQL, journal_id
		Call get_querystring()
		'get_tenancy()
		Call get_property()

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", " & property_id & ",  1, 1, 1, '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value

		' DEFECTS ENTRY
		strSQL = 	"INSERT INTO P_DEFECTS " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, CONTRACTOR, NOTES) " &_
					"VALUES (" & journal_id & ", 1 , 1, " & Session("userid") &_
					 ", " & supplier & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "')"

		set rsSet = Conn.Execute(strSQL)
		Response.Redirect "iRepairJournal.asp?customerid=" & customer_id & "&tenancyid=" & tenancy_id & "&SyncTabs=1"
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager My Job -- > Employment Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">

    var FormFields = new Array();
    FormFields[0] = "sel_CONTRACTOR|Contractor|SELECT|N"
    FormFields[1] = "txt_NOTES|Notes|TEXT|Y"

    function save_form() {
        if (!checkForm()) return false;
        document.getElementById("btnSaveDefects").disabled = true
        document.RSLFORM.target = "CRM_BOTTOM_FRAME";
        document.RSLFORM.action = "iDefects.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
        document.RSLFORM.submit();
    }

</script>
</head>
<body class="TA">
    <% If (Request("Text") <> "") Then %>
    <font color="red">
        <%=Request("Text") & "<br/>"%></font>
    <% ElseIf (Request("WarrantyItem") <> "") Then %>
    <font color="red">You are about to create a warranty defect, the notes field has been
        auto populated for you.<br />
    </font>
    <% End If %>
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 10px">
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Contractor :
            </td>
            <td>
                <%=lstContractors%>
            </td>
            <td width="30px">
                <img src="/js/FVS.gif" name="img_CONTRACTOR" id="img_CONTRACTOR" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" class="textbox200" rows="8" cols="20"
                    name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input type="button" name="btnSaveDefects" value=" Save " title="Save" class="RSLButton"
                    onclick="save_form()" id="btnSaveDefects" style="cursor: pointer" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="includes/bottoms/blankbottom.html" -->
    <iframe src="/secureframe.asp" name="frm_defects" id="frm_defects" width="1" height="1"
        style="display: none"></iframe>
</body>
</html>
