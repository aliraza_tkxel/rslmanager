<%@language="VBSCRIPT" codepage="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
    Dim item_id, itemnature_id, title, customer_id, tenancy_id, nature_id, nature_desc, mobile
	nature_id = Request("natureid")
    get_querystring()

    SQL = "SELECT ISNULL(MOBILE,'') MOBILE FROM C_ADDRESS WHERE ISDEFAULT=1 AND CUSTOMERID=" & customer_id
	Call OpenRs(rsMobile, SQL)
	
    IF rsMobile.EOF=False then    
        mobile = rsMobile("MOBILE")
	End If
    Call CloseRs(rsMobile)
    ' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
        nature_desc = Request("nature_desc")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		
	End Function
    CloseDB()

%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager -- > Send SMS</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript"  language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript"  language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript"  language="JavaScript" src="/js/FormValidation.js"></script>
<script type="text/javascript"  language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "txt_MOBILE|Mobile|TEXT|Y"
	FormFields[1] = "txt_MESSAGE|Message|TEXT|Y"
	
	function save_form(){
		if (!checkForm()) return false;
		RSLFORM.action = "../Serverside/SMS_svr.asp?itemid=<%=item_id%>&nature_desc=<%=nature_desc%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		RSLFORM.target = "ServerIFrame"
	    RSLFORM.submit()
	  
	}


	var set_length = 4000;

	function countMe() {
	  
	  	var str = event.srcElement
	  	var str_len = str.value.length;

		  if(str_len >= set_length) {
			  str.value = str.value.substring(0, set_length-1);
	  			}
	}			

</script>

<body bgcolor="#FFFFFF" class='TA' onload="">
	<form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
      		
		<tr>
			<td>Mobile : </td>
			<td><input type="text" name="txt_MOBILE" id="txt_MOBILE" value="<%=mobile%>" class="textbox200" /></td>
			<td><img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" border="0" alt=""/></td>							
		</tr>
        <tr style='height:7px'><td></td></tr>
		<tr>
		    <td valign="top">Message :
		    </td><td><textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class="textbox200" rows="4" name="txt_MESSAGE" id="txt_MESSAGE" cols="0"></textarea>
		    </td>	
            <td><img src="/js/FVS.gif" name="img_MESSAGE" id="img_MESSAGE" width="15px" height="15px" border="0" alt=""/> 
            </td>
		</tr>
		
		<tr>		
        <!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
            <td align="right" colspan="2">
            <input type="button" value="Save" class="RSLButton" title='View letter' onclick="save_form()" name="btnSave" id="btnSave" />  
            </td>
		</tr>
		
		<tr>
            <td> 
        <input type="hidden" name="hid_TENANCYID" value="<%=tenancy_id%>" />
          
             </td>
        </tr>

	
   	</table>
    </form>
<iframe src="/secureframe.asp"  name="ServerIFrame" width="400" height="100" style='DISPLAY:none'></iframe>
</body>
</html>	
	
	
	
	
	