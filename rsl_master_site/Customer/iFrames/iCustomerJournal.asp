<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'--------------------------------------------------------------------
	'PAGE UPDATED BY: UMAIR NAEEM
	'PAGE UPDATED ON:27 JUNE 2008
	'REASON FOR UPDATE: CREATED A ONE PROCEDURE NAMED [C_CUSTOMER_JOURNAL]
	'                   FOR ALL THE CUSTOMER JOURNALS INCLUDING THE
	'                   TENANCY CONSULTAION.
	'--------------------------------------------------------------------
	Dim cnt, customer_id,tenancy_id, str_journal_table,nature_id 

	Call OpenDB()
		Call GetPostedData()
		Call build_journal()
	Call CloseDB()

	'--------------------------------------------------------------------
	' GETS THE DATA POSTED TO THE PAGE
	'--------------------------------------------------------------------
	Function GetPostedData()
		customer_id = nulltest(Request("customerid"))
		tenancy_id = nulltest(Request("tenancyid"))	
		nature_id = nulltest(Request("nature"))	
	End Function

	'--------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'--------------------------------------------------------------------
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function

	'--------------------------------------------------------------------
	' GETS THE DATA FOR THE CUSTOMER JOURNAL
	'--------------------------------------------------------------------
	Function build_journal()

	'--------------------------------------------------------------------
	' STORED PROCEDURE [C_CUSTOMER_JOURNAL] IS USED TO GET THE VALUE
	' FOR CUSTOMER JOURNAL IN ORDER TO GET THE RECORD IN DATE ORDER
	'--------------------------------------------------------------------

	set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "[C_CUSTOMER_JOURNAL]"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@CUSTOMERID", 3, 1,4,customer_id)
		spUser.Parameters.Append spUser.CreateParameter("@TENANCYID", 3, 1,4,tenancy_id)
		spUser.Parameters.Append spUser.CreateParameter("@ITEMNATUREID", 3, 1,4,nature_id)
	set rsSet = spUser.Execute
		
		If(not rsSet.eof) Then
			str_journal_table = ""

			While(not rsSet.eof)
				If (rsSet("TRAFFIC_LIGHT") <> "" AND rsSet("STATUS") <> "Closed") Then
					TRAFFIC_LIGHT =  "<td><img src=""/myimages/trafficlight_" & rsSet("TRAFFIC_LIGHT") & ".gif"" height=""15"" width=""15""></td>"
				Else
					TRAFFIC_LIGHT = ""
				End If

				str_journal_table = str_journal_table & 	"<tr onclick=""open_me('" & rsSet("REDIR") & "')"" style=""cursor:pointer"">" &_
																"<td>" & rsSet("CREATIONDATE") & "</td>" &_
																"<td>" & rsSet("ITEM") & "</td>" &_
																"<td>" & rsSet("NATURE") & "</td>" &_
																"<td title=""" & rsSet("FULLTITLE")& """>" & rsSet("TITLE")  & "</td>" &_
																"<td>" & rsSet("LASTACTION") & "</td>" &_
																"<td>" & rsSet("STATUS")  & "</td>" & TRAFFIC_LIGHT &_
															"<tr>"
			    rsSet.movenext()
			Wend
		Else
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist.</td></tr></tfoot>"
		End If

		Call CloseRs(rsSet)
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">
        function open_me(str_redir){
		    parent.synchronize_tabs(13, "BOTTOM")
		    location.href = str_redir;
		    }
	    function DoSync(){
		    if ("<%=Request("SyncTabs")%>" == "1")
			    parent.synchronize_tabs(8, "BOTTOM")
		    }		
    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="behavior: url(../../Includes/Tables/tablehl.htc);
        border-collapse: collapse" slcolor='' border="0" hlcolor="#4682b4">
        <thead>
            <tr valign="top">
                <td style="border-bottom: 1px solid; width: 120; color: Blue; font-weight: bold;">
                    Date
                </td>
                <td style="border-bottom: 1px solid; width: 100; color: Blue; font-weight: bold;">
                    Item
                </td>
                <td style="border-bottom: 1px solid; width: 150; color: Blue; font-weight: bold;">
                    Nature
                </td>
                <td style="border-bottom: 1px solid; width: 150; color: Blue; font-weight: bold;">
                    Title
                </td>
                <td style="border-bottom: 1px solid; width: 120; color: Blue; font-weight: bold;">
                    Action
                </td>
                <td style="border-bottom: 1px solid; width: 70; color: Blue; font-weight: bold;">
                    Status
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="6">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
