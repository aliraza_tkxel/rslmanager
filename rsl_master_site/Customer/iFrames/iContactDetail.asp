<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim row_id, customer_id, address_id, atype_id , btn_HTML
	Dim housenum, add1, add2, add3, towncity, po, county,telephonehome,telephonework, telephonerelative
	Dim mobile1,mobile2, email, isdefault, ispoa, atype, action, dob
	Dim LeftColWidth,LeftTBWidth,strDivNextKin,firstname,surname,relationship,intAddedDetails
	Dim arrTabImageArray,strTabImage,arrTitleDisplay
	Dim telrelationship,telrelativedisplay1,telrelativedisplay2

	action = Request("action")
	atype_id = request("atype")
	row_id = Request("rowid")
	customer_id = Request("CUSTOMERID")
	tenancy_id = Request("tenancyid")
	intAddedDetails = Request("AddedDetails")

	' Change the validation rule if the contact is a social worker
	If (atype_id = 2 Or atype_id=6) Then 
		ValidateLine1 = "txt_RELATIONSHIP|RELATIONSHIP|TEXT|Y"
	ElseIf (atype_id = 4 ) Then
		ValidateLine1 = "txt_RELATIONSHIP|RELATIONSHIP|TEXT|N"
	Else
		ValidateLine1 = "txt_TELRELATIONSHIP|TELRELATIONSHIP|TEXT|N"
	End If

	contactType = atype_id

	'Tab Array
	arrTabImageArray = Array("previous_address","next_of_kin","current","social_worker","forwarding_address","power_of_attorny")

	'Array for Titles
	arrTitleDisplay  = Array("Previous_Address","Next_of_Kin","Current","Social_Worker","Forwarding_Address", "power_of_attorny")

	strTabImage = "tab_" & arrTabImageArray(atype_id - 1) &  ".gif"

	If intAddedDetails = "1" Or atype_id = "4" Then
		strDivNextKin = "Block"
		telrelativedisplay1="block"
		telrelativedisplay2="none"
	Else
		strDivNextKin = "none"
		telrelativedisplay1="none"
		telrelativedisplay2="block"
	End If

	Call OpenDB()
	If action = "NEW" Then
		Call do_new()
	Else
		Call do_amend()
	End If
	Call CloseDB()

	Function do_new()

		isdefault = 0
		ispoa = 0
		row_id = 0
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" id=""btn_SAVE"" value=""Save"" title=""Save"" onclick=""SaveForm('NEW')"" class=""RSLBUTTON"" tabindex=""14"" style=""cursor:pointer"" />"

		' get address type
		SQL = "SELECT DESCRIPTION FROM C_ADDRESSTYPE WHERE ADDRESSTYPEID = " & atype_id
		Call OpenRs(rsLoader, SQL)

		If not rsLoader.EOF Then atype = rsLoader(0) else atype = "Unrecognised Address Type !! " End If
		Call CloseRs(rsLoader)

	End Function


	Function do_amend()

		btn_HTML = "<input type=""button"" name=""btn_SAVE"" id=""btn_SAVE"" value=""Amend"" title=""Amend"" onclick=""SaveForm('AMEND')"" class=""RSLBUTTON"" tabindex=""14"" style=""cursor:pointer"" />"

		SQL = 	"SELECT	A.ADDRESSID, A.CUSTOMERID, " &_
				"		ISNULL(A.HOUSENUMBER,'') AS HOUSENUM, " &_
				"		ISNULL(A.ADDRESS1,'') AS ADDRESS1, " &_
				"		ISNULL(A.ADDRESS2,'') AS ADDRESS2, " &_
				"		ISNULL(A.ADDRESS3,'') AS ADDRESS3, " &_
				"		ISNULL(A.TOWNCITY,'') AS TOWNCITY, " &_
				"		ISNULL(A.POSTCODE,'') AS POSTCODE, " &_
				"		ISNULL(A.COUNTY,'') AS COUNTY, " &_
				"		ISNULL(A.TEL,'') AS TEL, " &_
				"		ISNULL(A.TELWORK,'') AS TELWORK, " &_
				"		ISNULL(A.TELRELATIVE,'') AS TELRELATIVE, " &_
				"		ISNULL(A.MOBILE,'') AS MOBILE, " &_
				"		ISNULL(A.MOBILE2,'') AS MOBILE2, " &_
				"		ISNULL(A.EMAIL,'') AS EMAIL, " &_
				"		CASE WHEN A.ISDEFAULT = 1 THEN 1 ELSE 0 END AS ISDEFAULT, " &_
				"		CASE WHEN A.ISPOWEROFATTORNEY = 1 THEN 1 ELSE 0 END AS ISPOWEROFATTORNEY, " &_
				"		ISNULL(T.DESCRIPTION,'') AS ATYPE, " &_
				"		ISNULL(CONTACTFIRSTNAME,'') AS FIRSTNAME, "&_
				"		ISNULL(CONTACTSURNAME,'') AS SURNAME, "&_
				"		ISNULL(CONTACTDOB,'') AS DOB, "&_
				"		ISNULL(RELATIONSHIP,'') AS RELATIONSHIP, "&_
				"		ISNULL(TELRELATIONSHIP,'') AS TELRELATIONSHIP "&_
				"FROM	C_ADDRESS A " &_
				"		LEFT JOIN C_ADDRESSTYPE T ON A.ADDRESSTYPE = T.ADDRESSTYPEID " &_
				"WHERE	ADDRESSID = " & row_id

		Call OpenRs(rsLoader, SQL)

		if (NOT rsLoader.EOF) then

			housenum	= rsLoader("HOUSENUM") 
			add1 		= rsLoader("ADDRESS1")
			add2 		= rsLoader("ADDRESS2")
			add3 		= rsLoader("ADDRESS3")
			towncity 	= rsLoader("TOWNCITY")
			po 			= rsLoader("POSTCODE")
			county  	= rsLoader("COUNTY")
			telephonehome	= rsLoader("TEL")
			telephonework 	= rsLoader("TELWORK")
			mobile1 		= rsLoader("MOBILE")
			mobile2 		= rsLoader("MOBILE2")
			email		= rsLoader("EMAIL")
			isdefault 	= rsLoader("ISDEFAULT")
			ispoa 		= rsLoader("ISPOWEROFATTORNEY")
			atype 		= rsLoader("ATYPE")
			address_id	= rsLoader("ADDRESSID")
			customer_id	= rsLoader("CUSTOMERID")

			' Extra Details needed to be shown
			If intAddedDetails = "1" Then
				firstname 		= rsLoader("FIRSTNAME")
				surname			= rsLoader("SURNAME")
				dob 			= rsLoader("DOB")
				relationship 	= rsLoader("RELATIONSHIP")
			Else
				telephonerelative 	= rsLoader("TELRELATIVE")
				telrelationship 	= rsLoader("TELRELATIONSHIP")
			End If

		End If
		Call CloseRs(rsLoader)

	End Function

	LeftColWidth = 100
	LeftTBWidth = 200
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
    body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
            background-image:none;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>
        <%=Replace(arrTitleDisplay(atype_id - 1),"_"," ")%></title>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_HOUSENUMBER|HOUSENUMBER|TEXT|Y"
	FormFields[1] = "txt_TEL|TEL|TELEPHONE|N"
	FormFields[2] = "txt_ADDRESS1|ADDRESS1|TEXT|Y"
	FormFields[3] = "txt_MOBILE|MOBILE|TELEPHONE|N"
	FormFields[4] = "txt_ADDRESS2|ADDRESS2|TEXT|N"
	FormFields[5] = "txt_EMAIL|EMAIL|EMAIL|N"
	FormFields[6] = "txt_ADDRESS3|ADDRESS3|TEXT|N"
	FormFields[7] = "txt_TOWNCITY|TOWNCITY|TEXT|Y"
	FormFields[8] = "txt_ISDEFAULT|ISDEFAULT|TEXT|N"
	FormFields[9] = "txt_POSTCODE|POSTCODE|POSTCODE|Y"
	FormFields[10] = "txt_ISPOWEROFATTORNEY|ISPOWEROFATTORNEY|TEXT|N"
	FormFields[11] = "txt_COUNTY|COUNTY|TEXT|N"
	FormFields[12] = "txt_TELWORK|TELWORK|TELEPHONE|N"
	FormFields[13] = "txt_MOBILE2|MOBILE2|TELEPHONE|N"
	FormFields[14] = "<%=ValidateLine1%>"

	if ( <%=intAddedDetails%> == "1" )
	{
		FormFields[15] = "txt_CONTACTFIRSTNAME|FIRSTNAME|TEXT|Y"
		FormFields[16] = "txt_CONTACTSURNAME|SURNAME|TEXT|Y"
		FormFields[17] = "txt_CONTACTDOB|DOB|DATE|N"
	}
	else
	{
		FormFields[15] = "txt_TELRELATIVE|TELRELATIVE|TELEPHONE|N"
	}

	function SaveForm(str_action){	
		if (!checkForm()) return false;
		document.RSLFORM.target = "CRM_TOP_FRAME";
		document.RSLFORM.action = "../ServerSide/contacts_svr.asp?action="+str_action+"&AddedDetails="+<%=intAddedDetails%>+"&random=new Date()";
		document.RSLFORM.submit();

		window.returnvalue = 1
		window.close()
	}

	function DoSomething()
	{
		checkForm()
		// ensure check boxes are checked if necessary
		if (<%=ispoa%> == 1)
			document.RSLFORM.txt_ISPOWEROFATTORNEY.checked = true;
		if (<%=isdefault%> == 1)
			document.RSLFORM.txt_ISDEFAULT.checked = true;
	}

    </script>
</head>
<body onload="DoSomething()">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%">
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td nowrap="nowrap">
                        <img src="../images/<%=strTabImage%>" height="20" border="0" alt="" /></td>
                    <td width="100%" style="border-bottom: 1px solid #133e71;" align="right">
                        &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="600px" cellpadding="1" cellspacing="2" border="0" class="TAB_TABLE">
                    <tr>
                        <td>
                            <div id="DivNextOfKin" style="display: <%=strDivNextKin%>">
                                <table height="100%" width="100%" style="border: SOLID 1PX #133E71;" cellpadding="1"
                                    cellspacing="2" border="0" class="TAB_TABLE">
                                    <tr>
                                        <td style="width: <%=LeftColWidth%>px">
                                            First Name
                                        </td>
                                        <td style="width: <%=LeftTBWidth%>px">
                                            <input type="text" name="txt_CONTACTFIRSTNAME" id="txt_CONTACTFIRSTNAME" value="<%=firstname%>" maxlength="100"
                                                style="width: 150px" class="TEXTBOX200" tabindex="7" />
                                            <img src="/js/FVS.gif" name="img_CONTACTFIRSTNAME" id="img_CONTACTFIRSTNAME" width="15px" height="15px" border="0" alt="" />
                                        </td>
                                        <td>
                                            Relationship
                                        </td>
                                        <td>
                                            <input type="text" name="txt_RELATIONSHIP" id="txt_RELATIONSHIP" value="<%=relationship%>" maxlength="100"
                                                style="width: 150px" class="TEXTBOX200" tabindex="7" />
                                            <img src="/js/FVS.gif" name="img_RELATIONSHIP" id="img_RELATIONSHIP" width="15px" height="15px" border="0" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Surname
                                        </td>
                                        <td>
                                            <input type="text" name="txt_CONTACTSURNAME" id="txt_CONTACTSURNAME" value="<%=surname%>" maxlength="100"
                                                style="width: 150px" class="TEXTBOX200" tabindex="7" />
                                            <img src="/js/FVS.gif" name="img_CONTACTSURNAME" id="img_CONTACTSURNAME" width="15px" height="15px" border="0" alt="" />
                                        </td>
                                        <td>
                                            D.O.B
                                        </td>
                                        <td>
                                            <input type="text" name="txt_CONTACTDOB" id="txt_CONTACTDOB" value="<%=dob%>" style="width: 150px" class="TEXTBOX200" />
                                            <img src="/js/FVS.gif" name="img_CONTACTDOB" id="img_CONTACTDOB" width="15px" height="15px" border="0" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table height="100%" width="100%" style="border: SOLID 1PX #133E71" cellpadding="1"
                                cellspacing="2" border="0" class="TAB_TABLE">
                                <tr>
                                    <td style="width: <%=LeftColWidth%>px">
                                        Number
                                    </td>
                                    <td style="width: <%=LeftTBWidth%>px">
                                        <input type="text" name="txt_HOUSENUMBER" id="txt_HOUSENUMBER" value="<%=housenum%>" maxlength="50" style="width: 150px"
                                            class="TEXTBOX100" tabindex="1" />
                                        <img src="/js/FVS.gif" name="img_HOUSENUMBER" id="img_HOUSENUMBER" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <!--<td colspan="3">&nbsp;&nbsp;<font style="font-size:14px"><b><i><U><%=atype%></U></i></b></font></td>-->
                                    <td>
                                        Telelphone(H)
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="txt_TEL" id="txt_TEL" value="<%=telephonehome%>" maxlength="15" style="width: 150px"
                                            class="TEXTBOX100" tabindex="8" />
                                        <img src="/js/FVS.gif" name="img_TEL" id="img_TEL" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address 1
                                    </td>
                                    <td>
                                        <input type="text" name="txt_ADDRESS1" id="txt_ADDRESS1" value="<%=add1%>" maxlength="90" style="width: 150px"
                                            class="TEXTBOX200" tabindex="2" />
                                        <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        Telephone(W)
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="txt_TELWORK" id="txt_TELWORK" value="<%=telephonework%>" maxlength="15" style="width: 150px"
                                            class="TEXTBOX100" tabindex="10" />
                                        <img src="/js/FVS.gif" name="img_TELWORK" id="img_TELWORK" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address 2
                                    </td>
                                    <td>
                                        <input type="text" name="txt_ADDRESS2" id="txt_ADDRESS2" value="<%=add2%>" maxlength="90" style="width: 150px"
                                            class="TEXTBOX200" tabindex="3" />
                                        <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        Mobile(1)
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="txt_MOBILE" id="txt_MOBILE" value="<%=mobile1%>" maxlength="15" style="width: 150px"
                                            class="TEXTBOX100" tabindex="9" />
                                        <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address 3
                                    </td>
                                    <td>
                                        <input type="text" name="txt_ADDRESS3" id="txt_ADDRESS3" value="<%=add3%>" maxlength="90" style="width: 150px"
                                            class='TEXTBOX200' tabindex="4" />
                                        <img src="/js/FVS.gif" name="img_ADDRESS3" id="img_ADDRESS3" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        Mobile(2)
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="txt_MOBILE2" id="txt_MOBILE2" value="<%=mobile2%>" maxlength="15" style="width: 150px"
                                            class="TEXTBOX100" tabindex="9" />
                                        <img src="/js/FVS.gif" name="img_MOBILE2" id="img_MOBILE2" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Town/City
                                    </td>
                                    <td>
                                        <input type="text" name="txt_TOWNCITY" id="txt_TOWNCITY" value="<%=towncity%>" maxlength="100" style="width: 150px"
                                            class="TEXTBOX200" tabindex="5" />
                                        <img src="/js/FVS.gif" name="img_TOWNCITY" id="img_TOWNCITY" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td>
                                        Email
                                    </td>
                                    <td colspan="2">
                                        <input type="text" name="txt_EMAIL" id="txt_EMAIL" value="<%=email%>" maxlength="130" style="width: 150px"
                                            class="TEXTBOX100" tabindex="10" />
                                        <img src="/js/FVS.gif" name="img_EMAIL" id="img_EMAIL" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        County
                                    </td>
                                    <td>
                                        <input type="text" name="txt_COUNTY" id="txt_COUNTY" value="<%=county%>" maxlength="100" style="width: 150px"
                                            class="TEXTBOX200" tabindex="7" />
                                        <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <td style='display: <%=telrelativedisplay1%>'>
                                        &nbsp;
                                    </td>
                                    <td style='display: <%=telrelativedisplay1%>'>
                                        &nbsp;
                                    </td>
                                    <td style='display: <%=telrelativedisplay2%>'>
                                        Alt. Contact
                                    </td>
                                    <td style='display: <%=telrelativedisplay2%>'>
                                        <input type="text" value="<%=telrelationship %>" name="txt_TELRELATIONSHIP" id="txt_TELRELATIONSHIP" maxlength="100"
                                            style="width: 150px" class="TEXTBOX200" tabindex="7" />
                                        <img src="/js/FVS.gif" name="img_TELRELATIONSHIP" id="img_TELRELATIONSHIP" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Postcode
                                    </td>
                                    <td>
                                        <input type="text" name="txt_POSTCODE" id="txt_POSTCODE" value="<%=po%>" maxlength="20" style="width: 150px"
                                            class="TEXTBOX200" tabindex="6" />
                                        <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                    <!--display if no relative number-->
                                    <td style='display: <%=telrelativedisplay1%>'>
                                        &nbsp;
                                    </td>
                                    <td style='display: <%=telrelativedisplay1%>'>
                                        &nbsp;
                                    </td>
                                    <!--display if relative number-->
                                    <td style='display: <%=telrelativedisplay2%>'>
                                        Telephone(C)
                                    </td>
                                    <td style='display: <%=telrelativedisplay2%>'>
                                        <input type="text" name="txt_TELRELATIVE" id="txt_TELRELATIVE" value="<%=telephonerelative %>" maxlength="15"
                                            style="width: 150px" class="TEXTBOX200" tabindex="7" />
                                        <img src='/js/FVS.gif' name="img_TELRELATIVE" id="img_TELRELATIVE" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        Corr. Add.
                                    </td>
                                    <td colspan="2">
                                        <input type="checkbox" name="txt_ISDEFAULT" id="txt_ISDEFAULT" tabindex="11" value="1" />
                                        <img src="/js/FVS.gif" name="img_ISDEFAULT" id="img_ISDEFAULT" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <% If contactType = 6 Then 
									isselected = "checked"
                                        %>
                                        <input type="hidden" name="txt_ISPOWEROFATTORNEY" id="txt_ISPOWEROFATTORNEY" tabindex="12" value="1" />
                                        <img src="/js/FVS.gif" name="img_ISPOWEROFATTORNEY" id="img_ISPOWEROFATTORNEY" width="15px" height="15px" border="0" alt="" />
								<% 
								   else
								%>
						  <input type="hidden" name="txt_ISPOWEROFATTORNEY" id="txt_ISPOWEROFATTORNEY" tabindex="12" value="0" />
                          <img src="/js/FVS.gif" name="img_ISPOWEROFATTORNEY" id="img_ISPOWEROFATTORNEY" width="15px" height="15px" border="0" alt="" />
								<% 
								   End If
								%>
                                    </td>
                                    <td style="text-align: right;" colspan="2">
                                        <input type="button" value="Close" title="Close" name="btn_BACK" id="btn_BACK" class="RSLBUTTON" tabindex="13"
                                            onclick="{window.close()}" style="cursor:pointer" />
                                        <%=btn_HTML%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=customer_id%>" />
                            <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
                            <input type="hidden" name="hid_ADDRESSID" id="hid_ADDRESSID" value="<%=address_id%>" />
                            <input type="hidden" name="hid_ADDRESSTYPE" id="hid_ADDRESSTYPE" value="<%=atype_id%>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
