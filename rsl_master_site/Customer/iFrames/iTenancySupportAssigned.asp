<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText,status_id,action_id,referralHistoryId

	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

        strSQL = "Select top 1 REFERRALHISTORYID,ITEMACTIONID,ITEMSTATUSID from C_REFERRAL Where JOURNALID = "& journal_id & "order by LASTACTIONDATE desc"  
		    Call OpenRs(rsSet, strSQL)
    		
    		 status_id = rsSet("ITEMSTATUSID")
    		 action_id = rsSet("ITEMACTIONID")
    		 referralHistoryId = rsSet("REFERRALHISTORYID")
			
		    Call CloseRs(rsSet) 
		cnt = 0
        set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "[TSR_TenancySupportAssigned]"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@journalid", 3, 1,4,journal_id)
	    set rsSet = spUser.Execute

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			
            Tenancy_Title = rsSet("TITLE")
            last_status = rsSet("STATUS")
            str_journal_table = str_journal_table & "<tr valign=""top"">"
				str_journal_table = str_journal_table & 	"<td width=""110"">" & rsSet("ReferralDate") & "</td>" &_
															"<td width=""120"">" & rsSet("Action") & "</td>" &_
															"<td width=""200"">" & rsSet("Notes") & "</td>" &_
															"<td width=""40"">" & rsSet("EmployeeName") & "</td>"
															
				
					str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" style=""cursor:pointer"" title=""Open Letter"" alt=""Open Letter"" /></td>"
				

				

			rsSet.movenext()

		Wend

		
		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist for this Tenancy Support Referral.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

        function update_general(referralHistoryId, action_id, status_id) {
            var str_win
            str_win = "/TenancySupport/Views/UpdateReferral.aspx?refid=" + referralHistoryId + "&actid=" + action_id + "&statid=" + status_id;
            window.open(str_win, "display", "width=570,height=355, left=200,top=200");
        }

//	function DoSync(){
//		if ("<%=Request("SyncTabs")%>" == "1")
//			parent.synchronize_tabs(12, "BOTTOM")
//		}

//	function open_letter(letter_id){
//		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
        //		window.open("../Popups/UpdateReferral.aspx?refid=2&actid=17&statid=30");
//	}
    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Tenancy_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td></td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonText%>" title="<%=ButtonText%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick='update_general(<%=referralHistoryId%>,<%=action_id%>,<%=status_id%>)'
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="110px">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="120px">
                    <font color="blue"><b>Action:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="180px">
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="40px">
                    <font color="blue"><b>By:</b></font>
                </td>
                <td style="border-bottom: 1px solid">
                </td>
<!--                <td style="border-bottom: 1px solid" width="150px">
                    <font color="blue"><b>Category:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="100PX">
                    <font color="blue"><b>Assigned To:</b></font>
                </td>-->
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
