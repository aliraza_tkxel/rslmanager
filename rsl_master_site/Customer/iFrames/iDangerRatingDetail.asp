<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	" select *, e.firstname + ' ' + e.lastname as wasnotedby from c_personalrisk p " &_
					"	 inner join e__employee e on e.employeeid = p.notedby " &_
					" where journalid = " & journal_id
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = "Personal Risk"
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				general_history_id = rsSet("riskid")
				
			End If
				Notes = rsSet("riskdesc")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<TD COLSPAN=3>" &_
															"<TABLE width'100%'><TR><TD>" &_
															"<TD width='200'>" & rsSet("RISKDATE") & "</TD>" &_
															"<TD width='400'>" & Notes & "</TD>" &_
															"<TD width='150'>" & rsSet("wasnotedby") & "</TD>" &_
															"</TD></TR></TABLE>" &_
															"</TD><TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		strSQL = 	"select * from c_personalrisk where journalid = " & journal_id
		'rw strSQL
		Call OpenRs (rsNotedBy, strSQL) 
		
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pGeneral.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=305, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR >
	<TD COLSPAN=3><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	      <TD nowrap width=150>&nbsp;</td>
          <td align=right width=100>&nbsp; </td>
        </tr></table>
	</TD>
	</TR>
	<TR valign=top>
		 <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=196PX><font color="blue"><b>Date:</b></font></TD>		
    	 <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=400px><font color="blue"><b>Notes:</b></font></TD>
		 <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX><font color="blue"><b>Noted By</b></font></TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=3></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	