<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	Dim ActionDateLine

	journal_id = Request("journalid")
	nature_id = Request("natureid")
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			ISNULL(L.DESCRIPTION, 'N/A') AS ESCLEVEL, ESCALATIONID," &_
					"			G.COMPLAINTHISTORYID, J.TITLE, " &_
					"			RTRIM(LTRIM(LEFT(ISNULL(E.FIRSTNAME,''),1) + ' ' + ISNULL(E.LASTNAME,''))) AS FULLNAME, " &_
					"			LEFT(E2.FIRSTNAME,1) + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS, ISNULL(C.DESCRIPTION,'') AS CATEGORY ," &_
					"			LA.DESCRIPTION AS ACTION, G.ESCALATIONDATE , G.ITEMACTIONID, G.LASTACTIONDATE," &_
					"			(SELECT COUNT(*) FROM C_LETTERS_SAVED WHERE HISTORYID = G.COMPLAINTHISTORYID AND ITEMNATUREID=J.ITEMNATUREID) AS LETTERCOUNT,J.ITEMNATUREID AS NATUREID " &_
					"FROM		C_SERVICECOMPLAINT G " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = G.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN C_ESCALATIONLEVEL L ON L.ESCALATIONID = G.ESCALATIONLEVEL " &_
					"			LEFT JOIN C_SERVICECOMPLAINT_CATEGORY C ON C.CATEGORYID = G.CATEGORY " &_ 
					"			LEFT JOIN C_LETTERACTION LA ON LA.ACTIONID = G.ITEMACTIONID AND LA.NATURE = 9" &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY COMPLAINTHISTORYID DESC "

		Call OpenRs (rsSet, strSQL) 
        
		str_journal_table = ""
		PreviousNotes = ""
		If (rsSet("ESCALATIONDATE") <> "" AND rsSet("ITEMSTATUSID") <> 14 AND rsSet("ITEMACTIONID") <> "") Then
			ActionDateLine = GetActionDateLine(rsSet("LASTACTIONDATE"),rsSet("ITEMACTIONID"))
		End If

		While Not rsSet.EOF

			cnt = cnt + 1

			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				general_history_id = rsSet("COMPLAINTHISTORYID")
				last_status = rsSet("STATUS")
				' rule : if status is reponse sent then org is awaiting customer response
				'       so we replace the next action date to awaiting customer response
				If (rsSet("ITEMACTIONID") = 102) Then
					ActionDateLine = "<b>Action Due : <font color=""red"">Awaiting Customer Response</font><b>"
				End If
				' if the item is closed then we get rid of the next action date
				If (rsSet("ITEMSTATUSID") = 14) Then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				End If
			End If

			Notes = rsSet("NOTES")

			If (Notes = "" OR isNULL(Notes)) Then
				ResponseNotes = "[Empty Notes]"
			ElseIf (Notes = PreviousNotes) Then
				ResponseNotes = "[Same As Above]"
			Else
				ResponseNotes = Notes
			End If
			PreviousNotes = Notes

				str_journal_table = str_journal_table & 	"<td width='90'>" & FormatDateTime(rsSet("CREATIONDATE"),2) & "</td>" &_
															"<td width='140' nowrap=""nowrap"">" & rsSet("ESCLEVEL") & "</td>" &_
															"<td width='90'>" & rsSet("CATEGORY") & "</td>" &_
															"<td width='380'>" & ResponseNotes & "</td>" &_
															"<td width='110'>" & rsSet("LASTACTIONUSERNAME") & "</td>" &_
															"<td width='150'>" & rsSet("FULLNAME") & "</td>" &_
															"<td width='50'>" & rsSet("ACTION") & "</td>"
															If rsSet("LETTERCOUNT") > 0 Then 
																str_journal_table = str_journal_table & "<td><img src=""../Images/attach_letter.gif"" style=""cursor:pointer"" title=""Open Letter"" onclick=""open_letter("& rsSet("COMPLAINTHISTORYID") &","& rsSet("NATUREID") &")""></td>"
															Else
																str_journal_table = str_journal_table & "<td>&nbsp;</td>"
															End If

				str_journal_table = str_journal_table & 	"</tr>"

			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""7"" align=""center"">No journal entries exist for this General Enquiry.</td></tr></tfoot>"
		End If

	End Function

	Function GetActionDateLine(LASTDEADLINESTARTDATE,ITEMACTIONID)

		Get_nextActionDate_SQL = "C_SERVICECOMPLAINTS_ACTION_RULE '" & formatDateTime(LASTDEADLINESTARTDATE,1)  & "'," & ITEMACTIONID
		Call OpenRs (rsDate, Get_nextActionDate_SQL)
			If Not rsDate.eof Then
				NextActionDate = rsDate("NEXT_ACTION_DATE")
				DaysOverorUnder = rsDate("DAYS_NUMBER")
				RuleStatus = rsDate("RULE_STATUS")
			End If
		Call CloseRS(rsDate)

		If (RuleStatus - 1) Then
			Beginfont = "<font  color='red' title='Action is " & DaysOverorUnder & " days overdue.'><b>"
			EndFont	= "</b></font>"
		ElseIf (DaysOverorUnder = 0) Then
			Beginfont = "<font style='cusor:pointer' color='red' title='Action is due today.'><b>"
			EndFont	= "</b></font>"
		Else
			Beginfont = "<font style='cusor:pointer' title='Action is due in " & DaysOverorUnder & " days time.'><b>"
			EndFont = "</b></font>"
		End If

		GetActionDateLine = "Action Due : " & Beginfont & NextActionDate & EndFont

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_general(int_complaint_history_id){
		var str_win
		str_win = "../PopUps/pServiceComplaint.asp?complainthistoryid="+int_complaint_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=400, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	function open_letter(letter_id,nature_id){
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/servicecomplaints_letter_plain.asp?tenancyid="+tenancy_id+"&natureid="+nature_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	}

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="7">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="45%">
                                            <b>Title:</b>&nbsp;<%=Defects_Title%>
                                        </td>
                                        <td width="55%">
                                            <%=ActionDateLine%>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonText%>" title="<%=ButtonText%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick="update_general(<%=general_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="67">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="112">
                    <font color="blue"><b>Escalation Level:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="73">
                    <font color="blue"><b>Category:</b></font>
                </td>
                <td width="172" style="border-bottom: 1px solid">
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="91">
                    <font color="blue"><b>Last Action By:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="96">
                    <font color="blue"><b><font color="blue"><b>Assigned To:</b></font></b></font>
                </td>
                <td style="border-bottom: 1px solid" width="76" nowrap="nowrap">
                    <font color="blue"><b>Action:</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="7">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
