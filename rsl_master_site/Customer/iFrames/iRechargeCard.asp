<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750"" HEIGHT=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	str_holiday = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/repairs.gif"" alt="""" /></td></tr><tr><td> 00 days available</td></tr><tr><td> 00 days boooked  <b>Book Now!</b></td></tr></table>"

	str_policy = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71' cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/rents.gif"" alt="""" /></td></tr><tr><td> Policy Handbook</td></tr><tr><td> Procedure Handbook</td></tr></table>"

	Dim row_id,  action, btn_HTML, tenancy_id, customer_id, str_id
	Dim sdp, active

	action = Request("action")
	row_id = Request("rowid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")

	If row_id <> "" Then get_sdp() End If

	If action = "NEW" Then
		row_id = 0 
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" value=""Save"" title=""Save a card to this account"" onclick=""SaveForm('NEW')"" class=""RSLBUTTON"" style=""cursor:pointer"" />"
		str_td = "<td><input type""=text"" size=""8"" maxlength=""10"" name=""txt_SDP"" id=""txt_SDP"" style=""border:1px solid black; font-size:14px; color:blue; font-weight:bold; text-align:right"">" &_
					"&nbsp;<img src=""/js/FVS.gif"" name=""img_SDP"" id=""img_SDP"" width=""15px"" height=""15px"" border=""0"" alt="""" /></td>"
	Else
		btn_HTML = "<input type=""button"" class=""RSLBUTTON"" onclick=""SaveForm('AMEND')"" name=""btn_Suspend"" value="" Suspend "" title=""Remove this card from this account"" style=""cursor:pointer"" />"
		str_td = "<td><font style=""font-size:15px"">&nbsp;&nbsp;" & sdp & "</font></td>"
	End If

	Function get_sdp()

		Call OpenDB()
		SQL = 	"SELECT * " &_
				"FROM	F_SDPTOTENANCY " &_
				"WHERE	SDPID = " & row_id

		Call OpenRs(rsLoader, SQL)
		if (NOT rsLoader.EOF) then
			sdp = rsLoader("SDP")
			active = rsLoader("ACTIVE")
		End If
		Call CloseRs(rsLoader)
		Call CloseDB()

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Recharge Card</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_SDP|SDP number|TEXT|Y"

        function SaveForm(str_action) {
            if (str_action == "NEW")
                if (!checkForm()) return false;
            document.RSLFORM.target = "frm_rc";
            document.RSLFORM.action = "../ServerSide/rechargecrad_svr.asp?action=" + str_action;
            document.RSLFORM.submit()
        }
	
    </script>
</head>
<body>
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="70%" valign="top" style="border: solid 1px #133e71">
                <table width="100%" cellpadding="0" cellspacing="0"
                    border="0">
                    <tr style="height: 5px">
                        <td>
                        </td>
                    </tr>
                    <tr id="DD">
                        <td colspan="4" valign="top">
                            <!-- RECHARGE CARD AREA----------------------------->
                            <table width="100%" border="0">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="right">
                                        <font style="font-size: 15px"><b>SDP Number:</b></font>
                                    </td>
                                    <%=str_td%>
                                    <td align="right">
                                        <img src="../Images/allpay.gif" name="allpay" id="allpay" alt="allpay" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%=btn_HTML%>
                                        <input type="button" name="btn_CANCEL" id="btn_CANCEL" value="Back" title="Back"
                                            class="RSLBUTTON" onclick="location.href ='iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'"
                                            style="cursor: pointer" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
                            <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=customer_id%>" />
                            <input type="hidden" name="hid_SPID" id="hid_SPID" value="<%=row_id%>" />
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                            <iframe src="/secureframe.asp" name="frm_rc" width="1px" height="1px" style="display: none"></iframe>
                            <!------------------------------------------------>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
