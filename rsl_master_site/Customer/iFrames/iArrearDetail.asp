<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, arrears_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, A.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), A.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(A.NOTES, 'N/A') AS NOTES, " &_
					"			A.arrearsHISTORYID, J.TITLE, B.FIRSTNAME + ' ' + B.LASTNAME AS CREATEDBY, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			A.ITEMSTATUSID, S.DESCRIPTION AS STATUS, " &_
					"			ISNULL(AA.DESCRIPTION,'N/A') AS ACTION, " &_
					"			(SELECT COUNT(*) FROM C_CUSTOMERLETTERS WHERE ARREASHISTORYID = A.ARREARSHISTORYID) AS LETTERCOUNT " &_
					"FROM		C_ARREARS A " &_
					"			LEFT JOIN C_STATUS S ON A.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = A.ASSIGNTO " &_
                    "			LEFT JOIN E__EMPLOYEE B ON B.EMPLOYEEID = A.LASTACTIONUSER " &_ 
					"			LEFT JOIN C_JOURNAL J ON A.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN C_LETTERACTION AA ON AA.ACTIONID = A.ITEMACTIONID " &_
					"WHERE		A.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY ARREARSHISTORYID DESC "
		'response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				arrears_history_id = rsSet("arrearsHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table &_
				
				 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
					"<TD>" & rsSet("ACTION") & "</TD>" &_
					"<TD>" & ResponseNotes & "</TD>" &_
					"<TD>" & rsSet("FULLNAME")  & "</TD>"&_ 
                    "<TD>" & rsSet("CREATEDBY")  & "</TD>"
					If rsSet("LETTERCOUNT") > 0 Then 
						str_journal_table = str_journal_table & "<TD><img src='../Images/attach_letter.gif' style='cursor:hand' title='Open Letter' onclick='open_letter("& rsSet("ARREARSHISTORYID") &")'></TD>"
					Else
						str_journal_table = str_journal_table & "<TD> </TD>"
					End If
					str_journal_table = str_journal_table & "<TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=5 ALIGN=CENTER>No journal entries exist for this arrears Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > arrears Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_arrears(int_arrears_history_id){
		var str_win
		str_win = "../PopUps/pArrears.asp?arrearshistoryid="+int_arrears_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=500,height=385, left=200,top=200");
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
	
	function open_letter(letter_id){
		
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		if (tenancy_id)
			window.open("../Popups/arrears_letter_plain.asp?tenancyid="+tenancy_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
		else
			window.open("../Popups/arrears_letter_plain.asp?letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	}

</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=5><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	<TD nowrap width=150>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE ="<%=ButtonText%>" style="background-color:beige" onclick='update_arrears(<%=arrears_history_id%>)' <%=ButtonDisabled%>>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX>Date:</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX>Action:</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=350px>Notes:</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX>Assigned To:</TD>
        <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150PX>Created By:</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	