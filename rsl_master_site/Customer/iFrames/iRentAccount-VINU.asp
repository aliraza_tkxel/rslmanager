<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table, str_color
	
	OpenDB()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")
	
	build_journal()


	'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
				"FROM	 	F_RENTJOURNAL J " &_
				"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
				"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))"
	Call OpenRs (rsSet, strSQL) 
	ACCOUNT_BALANCE = FormatCurrency(rsSet("AMOUNT"))
	CloseRs(rsSet)
	'END OF GET BALANCE
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	ISNULL(CONVERT(NVARCHAR, J.TRANSACTIONDATE, 103) ,'') AS F_TRANSACTIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTSTARTDATE, 103) ,'') AS PAYMENTSTARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTENDDATE, 103) ,'') AS PAYMENTENDDATE, " &_
					"			ISNULL(I.DESCRIPTION, ' ') AS ITEMTYPE, " &_
					"			ISNULL(P.DESCRIPTION, ' ') AS PAYMENTTYPE, " &_
					"			ISNULL(J.AMOUNT, 0) AS AMOUNT, " &_
					"			ISNULL(J.ISDEBIT, 1) AS ISDEBIT, " &_
					"			ISNULL(S.DESCRIPTION, ' ') AS STATUS " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id &_
					"ORDER		BY J.TRANSACTIONDATE DESC "
		//response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			If rsSet("ISDEBIT") = 0 Then
				str_color = "STYLE='COLOR:RED'"
			Else str_color = "" End If	
			
			str_journal_table = str_journal_table & 	"<TR " & str_color & " >" &_
															"<TD>" & rsSet("F_TRANSACTIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ITEMTYPE") & "</TD>" &_
															"<TD>" & rsSet("PAYMENTTYPE") & "</TD>" &_
															"<TD>" & rsSet("PAYMENTSTARTDATE") & "&nbsp;" & rsSet("PAYMENTENDDATE") & "</TD>" &_
															"<TD align=right nowrap width=90>" & FormatCurrency(rsSet("AMOUNT")) & "&nbsp;&nbsp;</TD>" &_
															"<TD>" & rsSet("STATUS")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="refresh" content="60;URL=iRentAccount.asp?CustomerID=<%=customer_id%>&TenancyID=<%=tenancy_id%>">
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
	function UpdateBalance(){
		parent.document.getElementById("TenantBalance").value = "<%=ACCOUNT_BALANCE%>"
		}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(10, "BOTTOM")
		}	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="DoSync();UpdateBalance();parent.STOPLOADER('BOTTOM')" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=90><B><FONT COLOR='#133e71'>Date</FONT></B></TD>
				
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><FONT COLOR='#133e71'>Item</FONT></B></TD>
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><font color="#133e71">Payment 
      Type</font></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" ALIGN=CENTER><B><FONT COLOR='#133e71'>Period</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B><FONT COLOR='#133e71'>Amount</FONT></B>&nbsp;&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B><FONT COLOR='#133e71'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	