<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	Dim last_development, last_movingtimeframe

	journal_id = Request("journalid")
	nature_id = Request("natureid")
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, G.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), G.LASTACTIONDATE, 108) ,'') AS CREATIONDATE, " &_
					"			ISNULL(G.NOTES, 'N/A') AS NOTES, " &_
					"			G.TRANSFEREXCHANGEHISTORYID, J.TITLE, B.FIRSTNAME + ' ' + B.LASTNAME AS CREATEDBY, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
					"			G.ITEMSTATUSID, S.DESCRIPTION AS STATUS, MOVINGTIMEFRAME, P.DEVELOPMENTNAME  " &_
					"FROM		C_TRANSFEREXCHANGE G " &_
					"			LEFT JOIN PDR_DEVELOPMENT P ON P.DEVELOPMENTID = G.DEVELOPMENT  " &_
					"			LEFT JOIN C_STATUS S ON G.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " &_
                    "			LEFT JOIN E__EMPLOYEE B ON B.EMPLOYEEID = G.LASTACTIONUSER " &_ 
					"			LEFT JOIN C_JOURNAL J ON G.JOURNALID = J.JOURNALID " &_
					"WHERE		G.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY TRANSFEREXCHANGEHISTORYID DESC "

		Call OpenRs (rsSet, strSQL)

		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			If cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			Else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				general_history_id = rsSet("TRANSFEREXCHANGEHISTORYID")
				last_status = rsSet("STATUS")
				last_development = rsSet("DEVELOPMENTNAME")
				last_movingtimeframe = rsSet("MOVINGTIMEFRAME")
				If (rsSet("ITEMSTATUSID") = 14) Then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				End If
			End If
				Notes = rsSet("NOTES")
				If (Notes = "" OR isNULL(Notes)) Then
					ResponseNotes = "[Empty Notes]"
				ElseIf (Notes = PreviousNotes) Then
					ResponseNotes = "[Same As Above]"
				Else
					ResponseNotes = Notes
				End If
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<td>" & rsSet("CREATIONDATE") & "</td>" &_
															"<td>" & ResponseNotes & "</td>" &_
															"<td>" & rsSet("FULLNAME")  & "</td>" &_
                                                            "<td>" & rsSet("CREATEDBY")  & "</td>" &_
														"<tr>"
			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""3"" align=""center"">No journal entries exist for this Tenant Enquiry.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pTransferExchange.asp?transferexchangehistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=423,height=350, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse" border="0">
        <thead>
            <tr>
                <td colspan="3">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" title="<%=ButtonText%>" class="RSLBUTTON" value="<%=ButtonText%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick="update_general(<%=general_history_id%>)"
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <b>Preferred Scheme</b> :
                    <%=last_development%>
                    &nbsp; &nbsp; &nbsp; <b>Moving Time Frame</b> :
                    <%=last_movingtimeframe%>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="120px">
                    Date:
                </td>
                <td style="border-bottom: 1px solid" width="350px">
                    Notes:
                </td>
                <td style="border-bottom: 1px solid" width="150px">
                    Assigned To:
                </td>
                <td style="border-bottom: 1px solid" width="150px">
                    Created By:
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
