<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstDevelopments, lstStatus, lstSubStatus, lstAssetType, lstPropertyType, lstLevel, lstBlocks
	
	OpenDB()
	Call BuildSelect(lstDevelopments, "sel_DEVELOPMENTID", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", NULL, NULL, "textbox200", " onchange=""GetBlocks()""")
	Call BuildSelect(lstStatus, "sel_STATUS", "P_STATUS", "STATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " onchange=""GetSubStatus()""")
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_assettype, NULL, "textbox200", NULL)
	Call BuildSelect(lstPropertyType, "sel_PROPERTYTYPE", "P_PROPERTYTYPE", "PROPERTYTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lstAdapatationCat, "sel_ADAPTATIONCAT", "P_ADAPTATIONCAT", "ADAPTATIONCATID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200",  " onchange=""GetAdaptations()""")
	CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000">
  <table width="233" border="0" cellpadding="2" cellspacing="0" style=" border-right: 1px solid #133E71; border-LEFT: 1px solid #133E71;  border-BOTTOM: 1px solid #133E71 ">
    <tr> 
      <td nowrap width=105px>Status:</td>
      <td nowrap> <%=lstStatus%></td>
      <td  nowrap> <image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
      <td nowrap width=105px>Property Type:</td>
      <td nowrap> <%=lstPropertyType%></td>
      <td  nowrap> <image src="/js/FVS.gif" name="img_STATUS" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
      <td nowrap>Development:</td>
      <td nowrap> <%=lstDevelopments%> </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_DEVELOPMENTID" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap><i><font color=#133e71>Scheme Name:</font></i></td>
      <td nowrap> 
        <input type="text" name="txt_SCHEMENAME" class="textbox200" readonly MAXLENGTH="50" STYLE='BORDER:1PX DOTTED #133E71'>
      </td>
      <td  nowrap></td>
    </tr>
    <tr> 
      <td nowrap>Street:</td>
      <td nowrap> 
        <input type="text" name="txt_ADDRESS1" class="textbox200" MAXLENGTH="100">
        <input type="hidden" name="txt_ADDRESS2">
        <input type="hidden" name="txt_ADDRESS3">
        <input type="hidden" name="txt_TOWNCITY">
        <input type="hidden" name="txt_COUNTY">
        <input type="hidden" name="DONTDOIT" value="1">
        <input type="hidden" name="ADVANCED">
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_ADDRESS1" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap>Postcode:</td>
      <td nowrap> 
        <input type="text" name="txt_POSTCODE" class="textbox200" MAXLENGTH="10">
      </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_POSTCODE" width="15px" height="15px" border="0"></td>
	</tr>
	<TR><TD>&nbsp;</td></tr>
    <tr> 
      <td nowrap><i><font color=#133e71>Adaptation Cat:</font></i></td>
      <td nowrap> <%=lstAdapatationCat%> </td>
      <td  nowrap><image src="/js/FVS.gif" name="img_ADAPTATIONCAT" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap><i><font color=#133e71>Adaptation Type:</font></i></td>
      <td nowrap><select name="sel_ADAPTATIONS" class="textbox200" disabled><option value="">Please Select an Adapt' Cat'</option></select> </td>
      <td  nowrap><image src="/js/FVS.gif" name="" width="15px" height="15px" border="0"></td>
    </tr>
    <tr> 
      <td nowrap><i><font color=#133e71>Asset Type:</font></i></td>
      <td nowrap> <%=lstAssetType%> </td>
      <td  nowrap><image src="/js/FVS.gif" name="" width="15px" height="15px" border="0"></td>
    </tr>

	
    <tr> 
      <td nowrap colspan=2 ALIGN=RIGHT> 
        <input type="reset" onclick="ResetRest()" value=" RESET " class="RSLButton">
        <input type="button" onclick="SearchNow()" value=" SEARCH " class="RSLButton">
      </td>
    </tr>
  </table>

</body>
</html>
