<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Customer/Iframes/ServerSide/ServiceComplaintEmail.asp" -->
<%
	'on error resume next
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams, lstEscLevel, lstCategory
	Dim director, chiefexec, housingmanager, ombudsman
	Dim directorId, chiefexecId, housingmanagerId, ombudsmanId
	Dim lstLetter

	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: June 16, 2008
	'' Reason:		Integraiton with Tenats Online

	'Code Changes Start by TkXel=================================================================================

	Dim enqID, complaintCategory, complaintNotes
		enqID = ""

	'Code Changes End by TkXel=================================================================================

	Call OpenDB()
		path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then

	'Code Changes Start by TkXel=================================================================================

	'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback

		enqID = Request("EnquiryID")
		complaintCategory = ""
		complaintNotes = ""

		' Enters into if block only, if EnquiryID is part of query string parameters
		If enqID <> "" OR Len(enqID) > 0 Then
			' This function will get the details of Service Complaint, based on Enquiry ID from
			' Database tables TO_ENQUIRY_LOG and TO_COMPLAINT
			' These extracted values will be used to populate matching frame text boxes/drop downs etc.
			Call get_complaint_detail(enqID, complaintCategory , complaintNotes)
		End If

	'Code Changes End by TkXel=================================================================================

		Call entry()

	Else

		Call new_record()

	End If

	Call CloseDB()

	'Code Changes Start by TkXel=================================================================================

	Function get_complaint_detail(enquiryID, ByRef category, ByRef notes)

		'CALL To STORED PROCEDURE TO get Complaint Detail
		Set comm = Server.CreateObject("ADODB.Command")
		'setting stored procedure name
		comm.commandtext = "TO_COMPLAINT_SelectComplaintDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		'setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		'executing sproc and storing resutls in record set
		Set rs = comm.execute
		'if record found/returned, setting values
		If Not rs.EOF Then
			notes = rs("Description")
			category = rs("CategoryID")
		End If
		Set comm = Nothing
		Set rs = Nothing

	End Function

	'Update Enquiry Log Table
	Function update_enquiry_log(enquiryID, journalID)

		'CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")
			'Setting stored procedure name
			comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			'Setting input parameter for sproc eventually used in WHERE clause
			comm.parameters(1) = enquiryID
			comm.parameters(2) = journalID
			'Executing sproc and storing resutls in record set
		Set rs = comm.execute
		Set comm = Nothing
		Set rs = Nothing

	End Function

	'Code Changes End by TkXel===================================================================================


	Function entry()

		Call get_querystring()

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = 'DAM' "
		Call OpenRs(rsTeam, SQL)
		If (NOT rsTeam.EOF) Then
			TeamID = rsTeam("TEAMID")
		Else
			TeamID = -1
		End If
		Call CloseRs(rsTeam)

		Call get_escalations()
		Call build_select()
		Call BuildSelect(lstCategory, "sel_CATEGORY", "C_SERVICECOMPLAINT_CATEGORY", "CATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", null, NULL, "textbox200", " style='width:300px'  onchange='GetSubCategories()'")
		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", null, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", " E__EMPLOYEE E  WHERE EMPLOYEEID = -1 ", "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", assignto, NULL, "textbox200", "style='width:300px'  ")

	End Function

	' RETURNS THE NAMES OF THE LAST FOUR ESCALATION LEVELS
	Function get_escalations()

		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "C_GET_ESCALATIONS"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = 0
		comm.execute
		director = comm.parameters(2)
		chiefexec = comm.parameters(3)
		housingmanager = comm.parameters(4)
		ombudsman = comm.parameters(5)
		directorId = comm.parameters(6)
		chiefexecId	= comm.parameters(7)
		housingmanagerId = comm.parameters(8)
		ombudsmanId	= comm.parameters(9)
		Set comm = Nothing

	End Function

	Function build_select()

		sql = "SELECT LETTERID, LETTERDESC FROM C_STANDARDLETTERS WHERE NATUREID = 9"
		Call OpenRs(rsSet, sql)
		lstLetter = "<select name='sel_LETTER' id='sel_LETTER' class='textbox200' style='width:300px'>"
		lstLetter = lstLetter& "<option value=''>Please Select...</option>"
		int_lst_record = 0
		While (NOT rsSet.EOF)
			lstLetter = lstLetter & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)
		lstLetter = lstLetter & "</select>"
		If int_lst_record = 0 then
			lstLetter = "<select name='sel_LETTER' id='sel_LETTER' class='textbox200' " & sel_width & " disabled='disabled'><option value=''> No associated letters found</option></select>"
		End If

	End Function

	' retieves fields from querystring which can be from either parent crm page or from this page when submitting to self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If

	End Function

	Function new_record()

		Dim strSQL, journal_id, assignTo, AssignedToEmail
		assignTo = -1

		Call get_querystring()

		' DEFINE ASSIGNTO LEVEL
		'If Cint(Request("sel_ESCLEVEL")) = 1 Then assignTo = Request("sel_USERS") Else assignTo = Request("sel_USERS") End If
		assignTo = Request("sel_USERS")

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		SQL = "EXEC C_SERVICECOMPLAINTS_CREATE @CUSTOMERID = " & customer_id &",@TENANCYID = 	" & tenancy_id &  ","&_
			"@ITEMID = " & item_id &  ",@ITEMNATUREID = " & itemnature_id &  ",@TITLE = '" & TITLE &  "',@LASTACTIONUSER = " & Session("userid") &  ","&_
			"@TEAM = " & Request("sel_TEAMS") &  ",@ASSIGNTO =" & assignTo &  ",@NOTES ='" & Replace(Request.Form("txt_NOTES"), "'", "''") &  "',"&_
			"@CATEGORY = " & Request("sel_CATEGORY") &  ",@subCATEGORY = " & Request("sel_subCATEGORY") &  ",@LETTERCONTENT = '" & Replace(Request.Form("hid_LETTERTEXT"),"'", "''") &  "',"&_
			"@LETTERSIG = '" & Replace(Request.Form("hid_SIGNATURE"),"'", "''")  &  "'"


		Call OpenDB()
		Set rsJournalId = Conn.execute(SQL)

		' Code Added by TkXel - Start

		If enqID <> "" OR Len(enqID) > 0 Then
			journal_id = rsJournalId.fields("JOURNALID").value
            AssignedToEmail = rsJournalId.fields("AssignedToEmail").value
			Call update_enquiry_log(enqID, journal_id)
		Else
			' journal_id = rsJournalId.fields("JOURNALID").value
            AssignedToEmail = rsJournalId.fields("AssignedToEmail").value
			' Call update_enquiry_log(enqID, journal_id)
		End If
		' Code Added by TkXel - End
		 
        call voidServiceComplaint(tenancy_id)

        call voidServiceComplaintStaffStage1(AssignedToEmail)

		Call CloseDB()
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body, form
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_CATEGORY|category|SELECT|Y"
        FormFields[3] = "sel_TEAMS|Team|SELECT|Y"

        function save_form() {
            if (!checkForm()) return false;
            document.RSLFORM.target = "CRM_BOTTOM_FRAME";
            document.RSLFORM.action = "Iservicecomplaints_new.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
            document.RSLFORM.submit();
        }

        var director = "<%=director%>"
        var chiefexec = "<%=chiefexec%>"
        var housingmanager = "<%=housingmanager%>"
        var ombudsman = "<%=ombudsman%>"
        var directorId = "<%=directorId%>"
        var chiefexecId = "<%=chiefexecId%>"
        var housingmanagerId = "<%=housingmanagerId%>"
        var ombudsmanId = "<%=ombudsmanId%>"

        function GetEmployees() {
            document.RSLFORM.target = "ServerIFrame";
            document.RSLFORM.action = "../serverside/getEmployees.asp?sel_Teams=<%=item_id%>&TENANCYID=<%=tenancy_id%>";
            document.RSLFORM.submit();
        }

        // this function alters the assign to area depending on the value of escid (escalation) chosen by user
        function get_names(escid) {
            FormFields[0] = "sel_USERS|Assign To|SELECT|N"
        }

        function GetEmployee_detail() { }

        var set_length = 4000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        function open_letter() {
            if (document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value == "") {
                ManualError("img_LETTERID", "You must first select a letter to view", 1)
                return false;
            }
            var tenancy_id = parent.parent.MASTER_TENANCY_NUM
            window.open("../Popups/arrears_letter.asp?tenancyid=" + tenancy_id + "&letterid=" + document.getElementById("sel_LETTER").options[document.getElementById("sel_LETTER").selectedIndex].value, "display", "width=570,height=600,left=100,top=50,scrollbars=yes");
        }

        //-- code changes start by TkXel===========================================================================

        function set_values(enquiryID) {
            // this function will get called on onLoad event of this form
            // This function used to set values of different elements belongs to this form
            var strEnqID = enquiryID + "";
            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {
                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
                // setting value for complaint notes
                var notes = document.getElementById("txt_NOTES");
                notes.value = "<%=complaintNotes%>"
                // setting value for complaint category drop down
                var category = document.getElementById("sel_CATEGORY");
                category.value = "<%=complaintCategory%>"
            }
        }

        //-- code changes end by TkXel===========================================================================

        
    	function GetSubCategories(){
		 	RSLFORM.action = "Serverside/GetServiceCompaintSubCategories.asp"
			RSLFORM.target = "ServerIFrame"
			RSLFORM.submit() 
        }

    </script>
</head>
<body class="TA" onload="set_values(<%=enqID%>)">
    <form name="RSLFORM" method="post" action="">
    <table width="445" style="height:189px" border="0" cellpadding="0" cellspacing="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                    class="textbox200" rows="3" cols="10" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Category :
            </td>
            <td>
                <%=lstCategory%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_CATEGORY" id="img_CATEGORY" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
			<td>Sub Category : </td>
			<td><select name="sel_SubCategory" id="sel_SubCategory" class="textbox200" STYLE="WIDTH:300PX"><option value="null">Please select a Category</option></select></td>
			<TD><image src="/js/FVS.gif" name="img_SubCategory" width="15px" height="15px" border="0"></TD>							
		</tr>
        <tr>
            <td>
                Escalation Level :
            </td>
            <td>
                <input class='textbox200' type="text" value="Expression of Dissatisfaction" style="width: 300px" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ESCLEVEL" id="img_ESCLEVEL" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Team :
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            </tr>
            <tr>
                <td>
                    Assign To :
                </td>
                <td>
                    <div id="dvUsers"><%=lstUsers%></div>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Letter :
                </td>
                <td>
                    <%=lstLetter%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_LETTERID" id="img_LETTERID" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Action :
                </td>
                <td>
                    <input name="text" type="text" class='textbox200' style='width: 300px' value="Logged" />
                </td>
                <td>
                </td>
            </tr>
                <tr>
                    <td colspan="2" align="left" nowrap="nowrap">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="22%">
                                    <input type="hidden" name="hid_LETTERTEXT" id="hid_LETTERTEXT" value="" />
                                    <input type="hidden" name="hid_SIGNATURE" id="hid_SIGNATURE" value="" />
                                </td>
                                <td width="54%">
                                    <input type="button" value="View Letter" title="View Letter" class="RSLButton" onclick="open_letter()"
                                        name="button" id="button" style="cursor:pointer" />
                                </td>
                                  <td width="15%" align="left">
                                    <input type="button" value=" Save " title="Save" class="RSLButton" onclick="save_form()"
                                        name="button3" id="button3" style="cursor:pointer" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
    </table>
    </form>
    <!--#include virtual="includes/bottoms/blankbottom.html" -->
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4" height="1"
        style="display: none"></iframe>
</body>
</html>
