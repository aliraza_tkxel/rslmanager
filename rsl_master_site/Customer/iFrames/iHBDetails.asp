<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
TenancyID = Request("tenancyid")
CustomerID = Request("CustomerID")
HBID = Request("HBID")

Call OpenDB()

SQL = "SELECT HB.*, LA.DESCRIPTION AS LOCALAUTHORITY, HB.STATUS, HB.HBID FROM F_HBINFORMATION HB " &_
		"LEFT JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = HB.CUSTOMERID AND CT.TENANCYID = HB.TENANCYID " &_
		"LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
		"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
		"LEFT JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
		"LEFT JOIN G_LOCALAUTHORITY LA ON D.LOCALAUTHORITY = LA.LOCALAUTHORITYID " &_
		"WHERE HB.HBID = " & HBID & " ORDER BY HB.HBID DESC"
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then
	if (rsLoader("STATUS") = 0) then
		ActualStatus = " Estimated"
	else
		ActualStatus = " Actual"
	end if

	InActive = false
	if (isNull(rsLoader("ACTUALENDDATE"))) then
		InActive = true
		ActiveOptions = "<input type='button' class='RSLButton' value=' End HB ' onclick='EndHB()' />&nbsp;<input type='button' class='RSLButton' value=' Update ' onclick='javascript:parent.location.href=""../HousingBenefit.asp?TenancyID=" & TenancyID & "&CustomerID=" & CustomerID & "&HBID=" & rsLoader("HBID") & """' />"	
	end if
	
	ClaimNo = rsLoader("HBREF")
	iEnd = rsLoader("ACTUALENDDATE")
	if (iEnd = "" OR isNull(iEnd)) then
		iEnd = "Still in progress..."
	end if
	HBINFO = "" &_
			"<tr><td class='TITLE'>Start Date: </td><td width=400 nowrap>" & rsLoader("INITIALSTARTDATE") & ActualStatus & "</td>" &_
			"<tr><td class='TITLE'>Initial End Date: </td><td>" & rsLoader("INITIALENDDATE") & ActualStatus & "</td></tr>" &_
			"<tr><td class='TITLE'>Initial Payment: </td><td>" & FormatCurrency(rsLoader("INITIALPAYMENT")) & ActualStatus & "</td></tr>" &_			
			"<tr><td class='TITLE'>Actual End Date: </td><td>" & iEnd & "</td></tr>" &_
			"<tr><td class='TITLE'>Local Authority: </td><td>" & rsLoader("LOCALAUTHORITY") & "</td></tr>" &_
			"<tr>" &_
			"<td colspan='2' height='100%' align='right' valign='bottom' colspan='2'><input type='button' class='RSLButton' value=' Back ' onclick='javascript:location.href=""iHousingBenefit.asp?TenancyID=" & TenancyID & "&CustomerID=" & CustomerID & "&HBID=" & rsLoader("HBID") & """' />&nbsp;" & ActiveOptions &_
			"</td></tr>"
						
	'Get the last 6 periods which have been paid
	SQL = "SELECT TOP 6 * FROM F_HBACTUALSCHEDULE WHERE VALIDATED = 1 AND HBID = " & HBID & " ORDER BY STARTDATE DESC"
	Call OpenRs(rsPreviousSchedule, SQL)
	StringTable = ""
	LASTHBCounter = 0
	while not rsPreviousSchedule.EOF 
		iStart = rsPreviousSchedule("STARTDATE")
		iEnd = rsPreviousSchedule("ENDDATE")
		StringTable = "<tr bgcolor='#f5f5dc'><td>" & FormatDateTime(iStart,2) & " - " & FormatDateTime(iEnd,2) & "</td><td align='right' width='170' colspan='2'>" & FormatCurrency(rsPreviousSchedule("HB")) & "</td></tr>" & StringTable
		rsPreviousSchedule.moveNext
		LASTHBCounter = LASTHBCounter + 1
	wend
	if LASTHBCounter > 0 then
		StringTable = "<tr bgcolor='#ffffff' style='color:#000000'><td><b>Last " & LASTHBCounter & " HB Payment(s)</b></td><td colspan='2'><img src='/myImages/up.gif' border='0' style='cursor:pointer' onclick=""swap_tables('table1')"" width='17' height='16' align='right' valign='top' /></td></tr>" &_
					"<tr bgcolor='#133E71' style='color:white'><td><b>Validated Period</b></td><td colspan='2'><b>Validated HB Payment</b></td></tr>" & StringTable
	end if
	CloseRs(rsPreviousSchedule)
	
	sctable = "<div class='TA' style='overflow:auto'><table id=""table2"" height='100%' width='100%' style='border:solid 1px #133e71;border-collapse:collapse;display:none' cellpadding='3' cellspacing='0' border='1' class='TAB_TABLE'>" 
	if (LASTHBCounter > 0) then
		sctable = sctable & StringTable
	end if

	if (InActive = true) then
		SQL = "SELECT * FROM F_HBACTUALSCHEDULE WHERE VALIDATED IS NULL AND HBID = " & HBID & " ORDER BY STARTDATE ASC"
		Call OpenRs(rsSc, SQL)
		sctable = sctable & "<tr bgcolor='#133e71' style='color:white'><td width='150'><b>Anticipated HB Period</b></td>"
		if (LASTHBCounter = 0) then
			sctable = sctable & "<td valign='top' nowrap='nowrap'><b>Anticipated HB Payment</b></td><td><img src='/myImages/up.gif' border='0' style='cursor:pointer' onclick=""swap_tables('table1')"" width='17' height='16' align='right' valign='top'></td></tr>"
		ELSE
			sctable = sctable & "<td valign='top' nowrap='nowrap' colspan='2'><b>Anticipated HB Payment</b></td>"
		end if	
		
		count = 0
		while not rsSc.EOF 
			count = count + 1
			if (count = 1) then
				if isNull(rsSc("SENT")) then
					sctable = sctable & "<tr><td nowrap='nowrap' width='200px'>" & rsSc("Startdate") & " - " & rsSc("EndDate") & "</td><td colspan='2' width='170px' align='right'><input type=""button"" class=""RslButton"" value=""ACTION"" onclick=""AddHBRow(" & rsSc("HBROW") & ")"" title=""Clicking this button will add this HB estimation to the Customer Account"" />&nbsp;&nbsp;" & FormatCurrency(rsSc("HB"),2) & "&nbsp;</td></tr>"				
				else
					sctable = sctable & "<tr><td nowrap='nowrap' width='200px'>" & rsSc("Startdate") & " - " & rsSc("EndDate") & "</td><td colspan='2' width='170px' align='right'>" & FormatCurrency(rsSc("HB"),2) & "&nbsp;</td></tr>"			
				end if
			else
				sctable = sctable & "<tr><td nowrap='nowrap' width='200px'>" & rsSc("Startdate") & " - " & rsSc("EndDate") & "</td><td colspan='2' width='170px' align='right'>" & FormatCurrency(rsSc("HB"),2) & "&nbsp;</td></tr>"
			end if
			rsSc.moveNext
		wend
		CloseRs(rsSc)
	else
		sctable = sctable & "<tr><td colspan='3' height='100%'></td></tr>"	
	end if

	sctable = sctable & "</table></div>"
							
end if

Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <title>RSLmanager > Customer Module > Housing Benefit > Detailed Information</title>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        form
        {
            padding:0px;
            margin:0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

// sawps table that need more than one page to display details
function swap_tables(str_which_one) {
	document.getElementById("table1").style.display = "none"
	document.getElementById("table2").style.display = "none"
	document.getElementById(str_which_one).style.display = "block"
}

function AddHBRow(HBROW) {
	document.RSLFORM.action = "../ServerSide/ForceAddHB.asp?HBROW=" + HBROW + "&HBID=<%=HBID%>&CUSTOMERID=<%=CustomerID%>&TENANCYID=<%=TenancyID%>"
	document.RSLFORM.submit()
}

function DoStartUp(){
	if ("<%=Request("LOADTAB")%>" == "2") {
	swap_tables('table2');
	parent.swap_div(parent.MAIN_OPEN_BOTTOM_FRAME)
	}
}

function EndHB() {
	answer = confirm("Please confirm that you wish to 'End' Housing Benefit.\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")	
	if (!answer) return false
	location.href = "../Serverside/EndHB.asp?TenancyID=<%=TenancyID%>&CustomerID=<%=CustomerID%>&HBID=<%=HBID%>"
}

    </script>
</head>
<body onload="DoStartUp()">
    <form name="RSLFORM" method="post" action="" style="padding:0px; margin:0px">
    <table width="750" style="height: 180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">        
        <tr>
            <td width="70%" height="100%" valign="top">
                <table id="table1" width="100%" style="border: solid 1px #133e71; border-collapse: collapse"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr bgcolor="#133E71" style="color: white">
                        <td colspan="2" width="100%">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr style="color: white">
                                    <td>
                                        <b>Housing Benefit Information for Claim No.
                                            <%=ClaimNo%></b>
                                    </td>
                                    <td align="right">
                                        <img title="View Anticipated HB Payments" src="/myImages/down.gif" border="0" style="cursor: pointer"
                                            onclick="swap_tables('table2')" width="17" height="16" align="bottom" alt="View Anticipated HB Payments" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%=hbinfo%>
                </table>
                <%=sctable%>
            </td>
            <td width="32%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>        
    </table>
    </form>
</body>
</html>
