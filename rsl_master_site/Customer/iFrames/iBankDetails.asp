<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	Dim row_id, action, btn_HTML, reedonly, tenancy_id, customer_id
	Dim so_accountname, so_sortcode
	Dim so_accountnumber

	action = Request("action")
	row_id = Request("rowid")
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")

	so_checked_yes = " checked"
	so_checked_no = ""

	If action = "NEW" Then
		row_id = 0
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" value=""Save"" title=""Save"" onclick=""SaveForm('NEW')"" class=""RSLBUTTON"" style=""cursor:pointer"" />"
	Else
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" value=""Amend"" title=""Amend"" onclick=""SaveForm('AMEND')"" class=""RSLBUTTON"" style=""cursor:pointer"" />"
	End If

	SQL = 	"SELECT BANKDETAILSID, " &_
			"		ISNULL(ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
			"		ISNULL(SORTCODE,'') AS SORTCODE, " &_
			"		ISNULL(ACCOUNTNUMBER,'') AS ACCOUNTNUMBER, ACTIVE " &_
			"FROM	F_BANKDETAILS " &_
			"WHERE	BANKDETAILSID = " & row_id
	Call OpenDB()
	Call OpenRs(rsLoader, SQL)
	If (NOT rsLoader.EOF) Then
		so_accountname = rsLoader("ACCOUNTNAME")
		so_sortcode = rsLoader("SORTCODE")
		so_accountnumber = rsLoader("ACCOUNTNUMBER")
		so_active = rsLoader("ACTIVE")
		If (so_active = 1) Then
			so_checked_yes = " checked"
			so_checked_no = ""
		Else
			so_checked_yes = ""
			so_checked_no = " checked"
		End If
	End If
	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Bank Details</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_ACCOUNTNAME|ACCOUNTNAME|TEXT|Y"
        FormFields[1] = "txt_SORTCODE|SORTCODE|INTEGER|Y"
        FormFields[2] = "txt_ACCOUNTNUMBER|ACCOUNTNUMBER|INTEGER|Y"

        function SaveForm(str_action) {
            // validate form
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_pbd";
            document.RSLFORM.action = "../ServerSide/bankdetails_svr.asp?action=" + str_action;
            document.getElementById("STATUS_DIV").style.visibility = 'visible';
            document.RSLFORM.submit()
        }

        function sortcode() {
            var s1, s2, s3, st
            s1 = document.getElementById("txt_SORTCODE1").value;
            s2 = document.getElementById("txt_SORTCODE2").value;
            s3 = document.getElementById("txt_SORTCODE3").value;
            st = "" + s1.toString() + "" + s2.toString() + "" + s3.toString()
            document.getElementById("txt_SORTCODE").value = st;
        }

        function onloadDo() {
            // SPLIT SORTCODE AND ENTER INTO APPROPRIATE BOXES
            var st, s1, s2, s3
            st = new String(document.getElementById("txt_SORTCODE").value);
            s1 = st.substr(0, 2);
            s2 = st.substr(2, 2);
            s3 = st.substr(4, 2);
            document.getElementById("txt_SORTCODE1").value = s1;
            document.getElementById("txt_SORTCODE2").value = s2;
            document.getElementById("txt_SORTCODE3").value = s3;
        }

    </script>
</head>
<body onload="onloadDo()">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="height:180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="70%" height="100%" style="border: solid 1px #133e71">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr style="height: 5px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" valign="middle">
                            <!------------------------------->
                            This secion stores the customer bank details used when
                            <br />
                            payments are neccesary to the customer.
                            <table align="center" border="0" cellpadding="1" cellspacing="0">
                                <tr>
                                    <td align="right" colspan="4">
                                        <b>Bank Details</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Account Name :
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_ACCOUNTNAME" id="txt_ACCOUNTNAME" class="TEXTBOX200"
                                            value="<%=so_accountname%>" maxlength="50" />
                                        <img src="/js/FVS.gif" name="img_ACCOUNTNAME" id="img_ACCOUNTNAME" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Sort Code :
                                    </td>
                                    <td>
                                        <input type="text" name="txt_SORTCODE1" id="txt_SORTCODE1" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" />
                                        &nbsp;-
                                        <input type="text" name="txt_SORTCODE2" id="txt_SORTCODE2" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" />
                                        &nbsp;-
                                        <input type="text" name="txt_SORTCODE3" id="txt_SORTCODE3" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" />
                                        <input type="hidden" name="txt_SORTCODE" id="txt_SORTCODE" value="<%=so_sortcode%>" />
                                        <img src="/js/FVS.gif" name="img_SORTCODE" id="img_SORTCODE" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Account Number :
                                    </td>
                                    <td>
                                        <input type="text" name="txt_ACCOUNTNUMBER" id="txt_ACCOUNTNUMBER" maxlength="8"
                                            class="TEXTBOX100" value="<%=so_accountnumber%>" />
                                        <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER" id="img_ACCOUNTNUMBER" width="15px"
                                            height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Active Account :
                                    </td>
                                    <td>
                                        Yes
                                        <input type="radio" name="rdo_ACTIVE" value="1" <%=so_checked_yes%> />
                                        No
                                        <input type="radio" name="rdo_ACTIVE" value="0" <%=so_checked_no%> />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right">
                                        <input type="button" name="btn_CANCEL" id="btn_CANCEL" value="Back" title="Back"
                                            class="RSLBUTTON" onclick="location.href ='iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'"
                                            style="cursor: pointer" />
                                        <%=btn_HTML%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="3" align="right">
                                        <div id="STATUS_DIV" style="visibility: hidden">
                                            <font style="font-size: 13px"><b>Please wait saving...</b></font>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!-- ---------------------------------------------->
                            <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value="<%=tenancy_id%>" />
                            <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value="<%=customer_id%>" />
                            <input type="hidden" name="hid_BANKDETAILSID" id="hid_BANKDETAILSID" value="<%=row_id%>" />
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                            <iframe src="/secureframe.asp" name="frm_pbd" id="frm_pbd" width="1px" height="1px"
                                style="display: none"></iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
