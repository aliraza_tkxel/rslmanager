<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	str_holiday = 	"<TABLE HEIGHT=100% WIDTH=100% STYLE='BORDER: SOLID 1PX #133E71' CELLPADDING=1 CELLSPACING=2 border=0 CLASS='TAB_TABLE' >" &_
					"<TR><TD><img src='../Images/repairs.gif'></TD></TR><TR><TD> 00 days available</TD></TR><TR><TD> 00 days boooked  <b>Book Now!</b></TD></TR></TABLE>"
	
	str_policy = 	"<TABLE HEIGHT=100% WIDTH=100% STYLE='BORDER: SOLID 1PX #133E71' CELLPADDING=1 CELLSPACING=2 border=0 CLASS='TAB_TABLE'>" &_
					"<TR><TD><img src='../Images/rents.gif'></TD></TR><TR><TD> Policy Handbook</TD></TR><TR><TD> Procedure Handbook</TD></TR></TABLE>"

	
	Dim row_id, lstitemtype, action, btn_HTML, reedonly, tenancy_id, customer_id
	Dim so_itemtype, so_accountname, so_sortcode, so_paymentdate, dd_amount, so_cycle
	Dim so_accountnumber, so_suspend
	
	action = Request("action")
	row_id = Request("rowid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	so_suspend = 0
	
	if action = "NEW" then 
		row_id = 0 
		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Save' onclick=""SaveForm('NEW')"" CLASS='RSLBUTTON'>"
		Call BuildSelect(lstCycle, "sel_CYCLE", "F_CYCLE", "CYCLE_ID, CYCLE_NAME", "CYCLE_ID", "Please Select", so_cycle, NULL, "textbox100", NULL)			
	Else
		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Amend' onclick=""SaveForm('AMEND')"" CLASS='RSLBUTTON'>"
	end if
	
	OpenDB()

	SQL = 	"SELECT SOSCHEDULEID, CYCLE_ID, CYCLE_NAME, " &_
			"		ISNULL(ITEMTYPE,0) AS ITEMTYPE, " &_
			"		ISNULL(ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
			"		ISNULL(SORTCODE,'') AS SORTCODE, " &_
			"		ISNULL(ACCOUNTNUMBER,'') AS ACCOUNTNUMBER, " &_
			"		ISNULL(CONVERT(NVARCHAR, PAYMENTDATE, 103) ,'') AS PAYMENTDATE, " &_
			"		ISNULL(AMOUNT,0) AS AMOUNT, " &_
			"		ISNULL(SUSPEND,0) AS SUSPEND " &_
			"FROM	F_SOSCHEDULE " &_
			"INNER JOIN F_CYCLE ON F_CYCLE.CYCLE_ID = F_SOSCHEDULE.CYCLE " &_
			"WHERE	SOSCHEDULEID = " & row_id

'			"		CASE CYCLE_ID " &_
'		   	"		WHEN 1 THEN DATEADD(DAY, 7 * (DATEDIFF(DAY, PAYMENTDATE, GETDATE()) / 7), PAYMENTDATE)  " &_
'		   	"		WHEN 2 THEN DATEDIFF(DAY, PAYMENTDATE, GETDATE()) / 14 " &_
'		   	"		WHEN 3 THEN DAY(GETDATE()) - DAY(PAYMENTDATE) " &_
'		   	"		WHEN 4 THEN DATEDIFF(MONTH, PAYMENTDATE, GETDATE()) / 3  " &_
'		   	"		WHEN 5 THEN DATEDIFF(MONTH, PAYMENTDATE, GETDATE()) / 6  " &_
'		   	"		WHEN 6 THEN DATEDIFF(MONTH, PAYMENTDATE, GETDATE()) / 12  " &_
'		   	"		END AS NEXTDATE " &_			
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		so_itemtype = rsLoader("ITEMTYPE")
		so_accountname = rsLoader("ACCOUNTNAME")
		so_sortcode = rsLoader("SORTCODE")
		so_paymentdate = rsLoader("PAYMENTDATE")
		so_amount = rsLoader("AMOUNT")
		so_accountnumber = rsLoader("ACCOUNTNUMBER")
		so_suspend = rsLoader("SUSPEND")
		so_cycle = rsLoader("CYCLE_ID")		
		so_cyclename = rsLoader("CYCLE_NAME")
'		SO_NEXTDATE = rsLoader("NEXTDATE")

		Call BuildSelect(lstCycle, "sel_CYCLE", "F_CYCLE", "CYCLE_ID, CYCLE_NAME", "CYCLE_ID", "Please Select", so_cycle, NULL, "textbox100", NULL)									
		'lstCycle = "<INPUT TYPE=HIDDEN NAME='sel_CYCLE' VALUE='" & so_cycle & "'><input type=text name='txt_dsds' value='" & so_cyclename & "' class='textbox' readonly>"
		// if first payment has been made then make initial payment and start date readonly
'		if Date >= DateValue(so_paymentdate) Then
'			reedonly = "  READONLY" 
'		Else
'			reedonly = ""
'		End If
		
	End If
	CloseRs(rsLoader)
	Call BuildSelect(lstitemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5,6) ", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", so_itemtype, NULL, "textbox100", NULL)
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">

	var FormFields = new Array();
	FormFields[0] = "txt_ACCOUNTNAME|ACCOUNTNAME|TEXT|N"
	FormFields[1] = "txt_SORTCODE|SORTCODE|INTEGER|N"
	FormFields[2] = "txt_ACCOUNTNUMBER|ACCOUNTNUMBER|INTEGER|N"
	FormFields[3] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
	FormFields[4] = "txt_PAYMENTDATE|PAYMENTDATE|DATE|Y"
	FormFields[5] = "sel_ITEMTYPE|ITEMTYPE|SELECT|Y"
	FormFields[6] = "sel_CYCLE|CYCLE|SELECT|Y"	

	function sel_change(caller){
	
		var caller;
		caller = RSLFORM.sel_TYPE.value;
		
		document.getElementById("DD").style.display = "none";
		document.getElementById("SO").style.display = "none";
		document.getElementById("PC").style.display = "none";
		
		document.getElementById("" + caller + "").style.display = "block";
	
	}
	
	function chk_clicked(int_what){
	
		if (int_what == 1){
			RSLFORM.finite.checked = false;
			soPERIOD.style.visibility='HIsoEN';
			RSLFORM.txt_soPERIOD.value = 999;
			}
		else {
			RSLFORM.infinite.checked = false;
			RSLFORM.txt_soPERIOD.value = 0;
			soPERIOD.style.visibility='visible';
			}
	}

	iMonths = new Array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
	
	function SaveForm(str_action){
		
		// validate form
		if (!checkForm()) return false;

		<% if action = "NEW" then %>
		CurrentDate = new Date ('<%=FormatDateTime(Date,1)%>')
		iArray = document.getElementById("txt_PAYMENTDATE").value.split("/")
		InitialPaymentDate = new Date(iArray[0] + " " + iMonths[parseInt(iArray[1],10)] + " " + iArray[2])
		//if (CurrentDate > InitialPaymentDate){
		//	alert("You must enter an initial payment date which is greater than or equal to the current date.")
		//	return false
		//	}
		<% end if %>
		RSLFORM.target = "frm_ps";
		RSLFORM.action = "../ServerSide/sopayments_svr.asp?action="+str_action;
		
		STATUS_DIV.style.visibility = 'visible';
		
		RSLFORM.submit()
		
	}

	function sortcode(){
	
		var s1, s2, s3, st
		s1 = RSLFORM.txt_SORTCODE1.value;
		s2 = RSLFORM.txt_SORTCODE2.value;
		s3 = RSLFORM.txt_SORTCODE3.value;
	
		st = "" + s1.toString() + "" + s2.toString() + "" + s3.toString()
		RSLFORM.txt_SORTCODE.value = st;
	}
	
	function check_amount(theItem){
		alert(document.getElementsByName("theItem").value)
		if (!(theItem.value > 0)){
			ImageID = "img_" + theItem.substring(4, theItem.length)
			document.images[ImageID].src = document.images["FVW_Image"].src
			document.getElementById(ImageID).title = "Must be greater than zero"	
			document.getElementById(ImageID).style.cursor = "hand"
			}
	}
	
	function chk_sus_clicked(){
	
		if (RSLFORM.suspend.checked == false)
			RSLFORM.hid_SUSPEND.value = 0;
		else
			RSLFORM.hid_SUSPEND.value = 1;
	}
	
</SCRIPT>
<SCRIPT FOR=window EVENT=onload LANGUAGE="JAVASCRIPT">
	
	// SPLIT SORTCODE AND ENTER INTO APPROPRIATE BOXES
	var st, s1, s2, s3
	st = new String(RSLFORM.txt_SORTCODE.value);
	s1 = st.substr(0,2);
	s2 = st.substr(2,2);
	s3 = st.substr(4,2);	
	RSLFORM.txt_SORTCODE1.value = s1;
	RSLFORM.txt_SORTCODE2.value = s2;
	RSLFORM.txt_SORTCODE3.value = s3;
	
	if (<%=so_suspend%> == 1)
		RSLFORM.suspend.checked = true;
	else
		RSLFORM.suspend.checked = false;
	
		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPAddING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
<FORM NAME=RSLFORM METHOD=POST>
	<TR>
		<TD WIDTH=70% HEIGHT=100%>
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71" CELLPADDING=0 CELLSPACING=0 BORDER=0 CLASS="TAB_TABLE">
				<TR STYLE='HEIGHT:5PX'><TD></TD></TR>
				<TR HEIGHT=100% ID=DD STYLE='DISPLAY:DISPLAY'>
				<TD COLSPAN=4>
					<!-- DIRECT DEBIT AREA----------------------------->
					
              <TABLE ALIGN=CENTER VALIGN=CENTER border=0 CELLPADDING=1 CELLSPACING=0>
                <TR> 
                  <TD>Item Type : </TD>
                  <TD COLSPAN=2><%=lstitemtype%> <image src="/js/FVS.gif" name="img_ITEMTYPE" width="15px" height="15px" border="0"> 
                  </TD>
                  <td align=right><b>Standing Order</b></td>
                </TR>
                <TR> 
                  <TD>Account Name : </TD>
                  <TD COLSPAN=3> 
                    <INPUT TYPE=TEXT NAME='txt_ACCOUNTNAME' CLASS='TEXTBOX200' VALUE="<%=so_accountname%>">
                    <image src="/js/FVS.gif" name="img_ACCOUNTNAME" width="15px" height="15px" border="0"> 
                  </TD>
                </TR>
                <TR> 
                  <TD>Sort Code : </TD>
                  <TD>
                    <INPUT TYPE=TEXT NAME='txt_SORTCODE1' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX'>
                    &nbsp;- 
                    <INPUT TYPE=TEXT NAME='txt_SORTCODE2' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX'>
                    &nbsp;- 
                    <INPUT TYPE=TEXT NAME='txt_SORTCODE3' MAXLENGTH=2 ONBLUR='sortcode()' CLASS='TEXTBOX100' STYLE='WIDTH:20PX'>
                    <INPUT TYPE=HIDDEN NAME='txt_SORTCODE' VALUE=<%=so_sortcode%>>
                    <image src="/js/FVS.gif" name="img_SORTCODE" width="15px" height="15px" border="0"> 
                  </TD>
                  <TD>Account No</TD>
                  <TD>
                    <INPUT TYPE=TEXT NAME='txt_ACCOUNTNUMBER' MAXLENGTH=8 CLASS='TEXTBOX100' value=<%=so_accountnumber%>>
                    <image src="/js/FVS.gif" name="img_ACCOUNTNUMBER" width="15px" height="15px" border="0"> 
                  </TD>
                </TR>
                <TR> 
                  <TD>Payment of :</TD>
                  <TD COLSPAN=3> 
                    <INPUT TYPE=TEXT ONBLUR='checkForm(true)' NAME='txt_AMOUNT' CLASS='TEXTBOX100' STYLE='WIDTH:60PX' VALUE=<%=FormatNumber(so_amount,2,-1,0,0)%>>
                    <image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0"> 
                    Every :&nbsp;<%=lstCycle%>&nbsp;
					<image src="/js/FVS.gif" name="img_CYCLE" width="15px" height="15px" border="0">
				</TD>
                </TR>
                <TR> 
                  <TD>Payment Date: </TD>
                  <TD COLSPAN=3> 
                    <INPUT TYPE=TEXT NAME='txt_PAYMENTDATE' CLASS='TEXTBOX100' STYLE='WIDTH:80PX' VALUE=<%=so_paymentdate%>  <%=reedonly%>>
                    <image src="/js/FVS.gif" name="img_PAYMENTDATE" width="15px" height="15px" border="0">
					</TD>
                </TR>
                <TR> 
                  <TD colspan=4> Suspend payments : 
                    <input type="checkbox" id='suspend' onClick="chk_sus_clicked()" name="checkbox">
                    <INPUT TYPE=BUTTON NAME='btn_CANCEL' VALUE='Back' CLASS='RSLBUTTON' ONCLICK="location.href ='iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'">
                    <%=btn_HTML%> </TD>
                </TR>
                <TR> 
                  <TD>&nbsp; 
                  <TD COLSPAN=3 ALIGN=RIGHT> 
                    <DIV ID=STATUS_DIV STYLE='VISIBILITY:HIDDEN'> <FONT STYLE='FONT-SIZE:13PX'><B>Please 
                      wait saving...</B></FONT> </DIV>
                </TR>
              </TABLE>
					<!-- ---------------------------------------------->
				</TD>
				</TR>
			</TABLE>
		</TD>
		<TD WIDTH=30% HEIGHT=100% valign=top>
			<%=str_repairs%>
		</TD>
	</TR>
	<INPUT TYPE=HIDDEN NAME='hid_TENANCYID' VALUE='<%=tenancy_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_CUSTOMERID' VALUE='<%=customer_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_SOSCHEDULEID' VALUE='<%=row_id%>'>
	<INPUT TYPE=HIDDEN NAME='hid_SUSPEND' VALUE=<%=so_suspend%>>
</FORM>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp"  name=frm_ps width=400px height=100px style='display:none'></iframe>
</BODY>
</HTML>	
	
	
	
	
	