<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<%
	Dim employee_id, item_id, itemnature_id, status_id, str_title, path, title, fullname
	
	fullname = Session("firstname") & " " & Session("lastname")
	path = request.form("hid_go")
	if path  = "" then path = 0 end if
	If path Then
		// write new record
		OpenDB()		
		new_record()
		CLoseDB()
	Else
		// these fields are used in the form tag of this page
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
	End If

	Function new_record()
	
		Dim strSQL, journal_id
		
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO E_JOURNAL (EMPLOYEEID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE) " &_
					"VALUES (" & Session("userid") & ", " & item_id & ", " & itemnature_id & ", 1, '" & Date & "', '" & title & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		//response.write strSQL		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		
		strSQL = 	"INSERT INTO E_ABSENCE " &_
					"(JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, STARTDATE, RETURNDATE, DURATION, REASON) " &_
					"VALUES (" & journal_id & ", 1 , 1, '" & Date & "', " & Session("userid") & ", '" & Request.Form("txt_STARTDATE") & "', '" & Request.Form("txt_RETURNDATE") & "', 0, '" & title & "')"
					
		set rsSet = Conn.Execute(strSQL)
		
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "txt_STARTDATE|Start Date|DATE|Y"
	FormFields[1] = "txt_RETURNDATE|Return Date|DATE|Y"
	FormFields[2] = "txt_RECORDEDBY|Recorded By|TEXT|Y"
	FormFields[3] = "txt_NOTES|Notes|TEXT|N"
	
	function save_form(){
	
		if (!checkForm()) return false;
		RSLFORM.hid_go.value = 1;
		RSLFORM.submit();
	
	}


	function return_data(){
	
		if (<%=path%> == 1)	{
			parent.div9.style.display = "none";
			parent.frm_erm.location.reload();
			parent.frm_nature.location.href = "../blank.asp";
			parent.swap_item_div(8);
			parent.div8.style.display = "block";
			parent.RSLFORM.reset();
		}
	}
</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" ONLOAD="return_data()">
<FORM NAME=RSLFORM METHOD=POST>
	<TABLE CELLPADDING=1 CELLSPACING=0 BORDER=0>
		
		<TR>
		<TD>Start Date</td><TD><input type="text" class="textbox200" name="txt_STARTDATE" value="<%=Date%>"></TD>
		<TD><image src="/js/FVS.gif" name="img_STARTDATE" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD>Return Date</td><TD><input type="text" class="textbox200" name="txt_RETURNDATE"></TD>
		<TD><image src="/js/FVS.gif" name="img_RETURNDATE" width="15px" height="15px" border="0">Anticipated</TD>	
		</TR>
		<TR>
		<TD>Recorded By</td><TD><input type="text" class="textbox200" name="txt_RECORDEDBY" value="<%=fullname%>" READONLY></TD>
		<TD><image src="/js/FVS.gif" name="img_RECORDEDBY" width="15px" height="15px" border="0"></TD>
		</TR>
		<TR>
		<TD>Notes</TD><TD><textarea style='OVERFLOW:HIDDEN' class="textbox200" name="txt_NOTES" ></textarea>
		</TD>	
		<TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0">
		<input type="button" value="Save" class="RSLButton"  onclick="save_form()" "name="button"></TD>
		</TR>
	   	<input type="hidden" name="hid_go" value=0>
   	</TABLE>
<!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
</FORM>
</BODY>
</HTML>	
	
	
	
	
	