<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	adapatationcat = Request.Form("sel_ADAPTATIONCAT")
	
	if (adapatationcat = "") then
		adapatationcat = -1
	end if

	OpenDB()
	SQL = "SELECT * FROM P_ADAPTATIONS A WHERE A.ADAPTATIONCAT = " & adapatationcat & " order by description "

	Call OpenRs(rsAdaptation, SQL)
	
	count = 0
	optionstr = ""
	while (NOT rsAdaptation.EOF) 
		optionstr = optionstr & "<option value=""" & rsAdaptation("ADAPTATIONID") & """>" & rsAdaptation("DESCRIPTION") & "</option>"
		rsAdaptation.moveNext
		count = 1
	wend		
	CloseDB()
		
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
<% if count = 0 then %>
	parent.RSLFORM.sel_ADAPTATIONS.outerHTML = "<select name='sel_ADAPTATIONS' class='textbox200' disabled><option value=''>None Found</option></select>";
	parent.RSLFORM.sel_ADAPTATIONS.disabled = true	
<% else %>
	parent.RSLFORM.sel_ADAPTATIONS.outerHTML = AdaptationData.innerHTML;
	parent.RSLFORM.sel_ADAPTATIONS.disabled = false	
<% end if %>	
	}
</script>
<body onload="ReturnData()">
<div id="AdaptationData"><select name="sel_ADAPTATIONS" class="textbox200"><option value="">Please Select</option><%=optionstr%></select></div>
</body>
</html>