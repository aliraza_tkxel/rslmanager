<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	customer_id = Request("customerid")
	developmentid = Request.Form("sel_DEVELOPMENTID")
	if (developmentid = "") then 
		devsql = ""
	else
		devsql = " AND P.DEVELOPMENTID = " & developmentid & " "
	end if

	address1 = Replace(Request.Form("txt_ADDRESS1"), "'", "''")
	if (address1 = "") then 
		addsql = ""
	else
		'addsql = " AND (P.ADDRESS1 LIKE '%" & address1 & "%' OR P.ADDRESS2 LIKE '%" & address1 & "%') "
        addsql = " AND P.HouseNumber+' '+P.ADDRESS1 LIKE '%" & address1 & "%'"

	end if

	'default status = 1, status = available to let
	stasql = " AND P.STATUS = 1 AND (P.SUBSTATUS in(21,(select Top 1 SUBSTATUSID from P_SUBSTATUS where [DESCRIPTION] = 'N/A')) OR P.SUBSTATUS IS NULL) "


	postcode = Replace(Request.Form("txt_POSTCODE"), "'", "''")
	if (postcode = "") then
		possql = ""
	else
		possql = " AND P.POSTCODE LIKE '%" & postcode & "%' "
	end if

	propertytpe = Request.Form("sel_PROPERTYTYPE")
	if (propertytpe = "") then
		prosql = ""
	else
		prosql = " AND P.PROPERTYTYPE = " & propertytpe & " "
	end if
	
	substatus = Request.Form("sel_SUBSTATUS")
		if (substatus = "") then 
			subsql = ""
		else
			subsql = " AND P.SUBSTATUS = " & substatus & " "
		end if
		
	if ("1" = "1") then
			
		blockid = Request.Form("sel_BLOCKID")
		if (blockid = "") then 
			blosql = ""
		else
			blosql = " AND P.BLOCKID = " & blockid & " "
		end if
	
		assettype = Request.Form("sel_ASSETTYPE")
		if (assettype = "") then
			asssql = ""
		else
			asssql = " AND P.ASSETTYPE = " & assettype & " "
		end if
	
		adaptationcat = Request.Form("sel_ADAPTATIONCAT")
		if (adaptationcat = "") then
			adasql = ""
		else
			adasql = " AND AD.ADAPTATIONCAT = " & adaptationcat & " "
		end if
	
		adaptation = Request.Form("sel_ADAPTATIONS")
		if (adaptation = "") then
			adaptsql = ""
		else
			adaptsql = " AND AD.ADAPTATIONID = " & adaptation & " "
		end if
	
	end if

	Call OpenDB()
	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " P.PROPERTYID, CASE WHEN HOUSENUMBER IS NULL THEN FLATNUMBER ELSE HOUSENUMBER END AS HOUSENUMBER, ADDRESS1, ADDRESS2, TOWNCITY, POSTCODE, FIN.RENT  FROM P__PROPERTY P " &_
			"INNER JOIN P_FINANCIAL F ON P.PROPERTYID = F.PROPERTYID " &_
			"LEFT JOIN (	" &_
			"		SELECT TOP 1 AD.ADAPTATIONID,AD.DESCRIPTION, PROPERTYID " &_
			"		FROM P_PORTFOLIOADAPTATIONS AP " &_
			"			INNER JOIN P_ADAPTATIONS AD ON AP.ADAPTATIONID = AD.ADAPTATIONID	" &_
			"	  	WHERE 1=1 " & adasql & adaptsql &_
			"	   ) AP ON AP.PROPERTYID = P.PROPERTYID " &_
			"INNER JOIN P_FINANCIAL FIN ON FIN.PROPERTYID = F.PROPERTYID " &_
			"WHERE 1 = 1 " & devsql & possql & subsql & stasql & addsql & blosql & prosql & asssql  & " ORDER BY HOUSENUMBER, ADDRESS1, ADDRESS2, TOWNCITY, POSTCODE"
    Call OpenRs(rsSearch, SQL)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript">

        function DisplayProperty(PropID) {
            event.cancelBubble = true
            window.open("/Portfolio/PopUps/Property_Details.asp?PropertyID=" + PropID, "PropertyDetails", "width=400,height=300,left=200,top=200")
        }


        function open_tenancy(propid) {
            window.open("../../Popups/createtenancy.asp?propertyid=" + propid + "&customerid=<%=customer_id%>", "display", "width=400,height=420,left=100,top=200");
        }

    </script>
</head>
<body>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td valign="top">
                <table cellspacing="0" style="border-bottom: 1px solid #133E71" width="383">
                    <tr>
                        <td width="90" nowrap="nowrap" style="color: #133E71">
                            <b>Property Ref</b>
                        </td>
                        <td nowrap="nowrap" style="color: #133E71">
                            <b>Address</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div style="height: 100%; overflow: auto" class="TA">
                    <table style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#4682b4" width="383">
                        <%
	COUNT = 0 
	if (not rsSearch.EOF) then
		while NOT rsSearch.EOF
			HOUSENUMBER = rsSearch("HOUSENUMBER")
			ADDRESS1 = rsSearch("ADDRESS1")
			ADDRESS2 = rsSearch("ADDRESS2")
			TOWNCITY = rsSearch("TOWNCITY")
			POSTCODE = rsSearch("POSTCODE")
			PROPERTYRENT = rsSearch("RENT")
			
			propaddress = HOUSENUMBER & ", " & ADDRESS1 & ", " &  ADDRESS2 &  ", " &  TOWNCITY & ", " &  POSTCODE															
			
			propaddress = Replace(propaddress, ", , ", ", ")
			propid = rsSearch("PROPERTYID")
			If PROPERTYRENT <> "" Then
				If (LEN(propaddress) > 35) Then
					Response.Write "<tr style=""cursor:pointer"" onclick=""open_tenancy('" & propid & "')"">"
                    Response.Write "<td width=""90"">" & rsSearch("PROPERTYID") & "</td>"
                    Response.Write "<td title="" " & Server.HTMLEncode(propaddress) & " "">" & Left(propaddress, 35) & "..." & "</td>"
                    Response.Write "<td align=""right"" width=""40"" style=""background-color:#ffffff""><a href=""#"" onclick=""DisplayProperty('" & propid & "')""><img src='/myImages/info.gif' border=""0""></a></td>"
                    Response.Write "</tr>"
				Else
					Response.Write "<tr style=""cursor:pointer"" onclick=""open_tenancy('" & propid & "')"">"
                    Response.Write "<td width=""90"">" & rsSearch("PROPERTYID") & "</td>"
                    Response.Write "<td>" & propaddress & "</td>"
                    Response.Write "<td width=""40"" style=""background-color:#ffffff"" align=""right""><a href=""#"" onclick=""DisplayProperty('" & propid & "')""><img src='/myImages/info.gif' border=""0""></a></td>"
                    Response.Write "</tr>"
				End If
			Else
				If (LEN(propaddress) > 35) Then
					Response.Write "<tr>"
                    Response.Write "<td width=""90"">" & rsSearch("PROPERTYID") & "</td>"
                    Response.Write "<td title="" " & Server.HTMLEncode(propaddress) & " "">" & Left(propaddress, 35) & "..." & "</td>"
                    Response.Write "<td width=""40"" style=""background-color:#ffffff"" align=""right""><img src=""/myImages/NR.gif"" border=""0"" alt="""" /> <a href=""#"" onclick=""DisplayProperty('" & propid & "')""><img src='/myImages/info.gif' border=""0""></a></td>"
                    Response.Write "</tr>"
				Else
					Response.Write "<tr>"
                    Response.Write "<td width=""90"">" & rsSearch("PROPERTYID") & "</td>"
                    Response.Write "<td>" & propaddress & "</td>"
                    Response.Write "<td width=""40"" style=""background-color:#ffffff"" align=""right""><img src=""/myImages/NR.gif"" border=""0"" alt="""" /> <a href=""#"" onclick=""DisplayProperty('" & propid & "')""><img src='/myImages/info.gif' border=0></a></td>"
                    Response.Write "</tr>"
				End If			
			End If	
			COUNT = COUNT + 1	
			rsSearch.moveNext
		wend
	Else
		Response.Write "<tr>"
        Response.Write "<td colspan=""3"" align=""center"">No Records Found</td>"
        Response.Write "</tr>"		
	End If
	If (COUNT = MaxRecords) Then
		Response.Write "<tr>"
        Response.Write "<td colspan=""3"" align=""center"">Maximum number of records returned.<br/>To Filter the list apply more filters.</td>"
        Response.Write "</tr>"
	End If
	Call CloseRs(rsSearch)
	Call CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="383" style="border-top: 1px solid #133E71">
                    <tfoot>
                        <tr>
                            <td colspan="3" align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
