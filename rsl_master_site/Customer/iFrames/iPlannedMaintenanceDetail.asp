<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText

	
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "

	Call OpenDB()
	Call build_journal()
	Call CloseDB()

	Function build_journal()

		cnt = 0
		
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, C_PlannedMaintenance.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), C_PlannedMaintenance.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(NOTES, 'N/A') AS NOTES, C_PlannedMaintenance.ASBHISTORYID, " &_
					"			E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME,C_PlannedMaintenance.ITEMSTATUSID,  " &_
					"			E2.FIRSTNAME + ' ' + E2.LASTNAME AS LASTACTIONUSERNAME, J.TITLE, S.Description as STATUS " &_
					"FROM		C_PlannedMaintenance " &_
					"			LEFT JOIN C_STATUS S ON C_PlannedMaintenance.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = C_PlannedMaintenance.ASSIGNTO " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON E2.EMPLOYEEID = C_PlannedMaintenance.LASTACTIONUSER " &_
					"			LEFT JOIN C_JOURNAL J ON C_PlannedMaintenance.JOURNALID = J.JOURNALID " &_
					"WHERE		C_PlannedMaintenance.JOURNALID = " & journal_id & " " &_
					"ORDER 		BY ASBHISTORYID DESC "
		Call OpenRs (rsSet, strSQL)
		
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF

			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & "<tr style=""color:gray"">"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & "<tr valign=""top"">"
				general_history_id = rsSet("ASBHISTORYID")
				last_status = rsSet("STATUS")
				if (rsSet("ITEMSTATUSID") = 14) then
					ButtonDisabled = " disabled"
					ButtonText = "No Options"
				end if
			End If
				Notes = rsSet("NOTES")
				if (Notes = "" OR isNULL(Notes)) then
					ResponseNotes = "[Empty Notes]"
				elseif (Notes = PreviousNotes) then
					ResponseNotes = "[Same As Above]"
				else
					ResponseNotes = Notes
				end if
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<td width=""110"">" & rsSet("CREATIONDATE") & "</td>" &_
															"<td width=""120"">" & rsSet("LASTACTIONUSERNAME") & "</td>" &_
															"<td width=""200"">" & ResponseNotes & "</td>" &_
															"<td width=""100"">" & rsSet("FULLNAME")  & "</td>"
				

				str_journal_table = str_journal_table & "</td><tr>"

			rsSet.movenext()

		Wend

		Call CloseRs(rsSet)

		If cnt = 0 Then
			str_journal_table = "<tfoot><tr><td colspan=""6"" align=""center"">No journal entries exist for this General Enquiry.</td></tr></tfoot>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Update</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
            background-image: none;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pPlannedMaintenance.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=355, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}

	function open_letter(letter_id){
		var tenancy_id = parent.parent.MASTER_TENANCY_NUM
		window.open("../Popups/ASB_letter_plain.asp?tenancyid="+tenancy_id+"&letterid="+letter_id, "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	}
    </script>
</head>
<body class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM')">
    <table width="100%" cellpadding="1" cellspacing="0" style="border-collapse: collapse"
        slcolor='' border="0">
        <thead>
            <tr>
                <td colspan="6">
                    <table cellspacing="0" cellpadding="1" width="100%">
                        <tr valign="top">
                            <td>
                                <b>Title:</b>&nbsp;<%=Defects_Title%>
                            </td>
                            <td nowrap="nowrap" width="150">
                                Current Status:&nbsp;<font color="red"><%=last_status%></font>
                            </td>
                            <td align="right" width="100">
                                <input type="button" name="BTN_UPDATE" id="BTN_UPDATE" class="RSLBUTTON" value="<%=ButtonText%>" title="<%=ButtonText%>"
                                    style="background-color: #f5f5dc; cursor:pointer" onclick='update_general(<%=general_history_id%>)'
                                    <%=ButtonDisabled%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="border-bottom: 1px solid" nowrap="nowrap" width="110px">
                    <font color="blue"><b>Date:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="120px">
                    <font color="blue"><b>Last Action User:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="180px">
                    <font color="blue"><b>Notes:</b></font>
                </td>
                <td style="border-bottom: 1px solid" width="100PX">
                    <font color="blue"><b>Assigned To:</b></font>
                </td>
            </tr>
            <tr style="height: 7px">
                <td colspan="5">
                </td>
            </tr>
        </thead>
        <tbody>
            <%=str_journal_table%></tbody>
    </table>
</body>
</html>
