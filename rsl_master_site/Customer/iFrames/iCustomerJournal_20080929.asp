	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, customer_id, str_journal_table,tenancy_filter,nature_filter ,nature_id 
	
	OpenDB()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")	
	nature_id = Request("nature")	
	filters()
	build_journal()
	
	CloseDB()
	
	Function filters()
	
		if tenancy_id <> "" then
			if  not tenancy_id = "-1" then
				tenancy_filter = 	 " OR J.TENANCYID = " & tenancy_id & "  "
			End if
		End If

		if nature_id <> "" then
			nature_filter = 	 " and N.ITEMNATUREID = " & nature_id & "  "
		end if
		
	End Function
	
	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	isnull(VJ.DESCRIPTION,ja.DESCRIPTION) AS LASTACTION, J.JOURNALID, " &_
					"			CASE " &_
					"			WHEN J.ITEMNATUREID = 4 THEN 'iGeneralDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID = 5 THEN 'iAdaptionDetail.asp'  " &_					
					"			WHEN J.ITEMNATUREID in (26,28,29,30,46) THEN 'iGeneralDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID BETWEEN 11 AND 14 THEN 'iGeneralDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID in (17,18) THEN 'iGeneralDetail.asp'  " &_					
					"			WHEN J.ITEMNATUREID in (6,7) THEN 'iTransferExchangeDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID in (10) THEN 'iArrearDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID in (27,50) THEN 'iTerminationDetail.asp'  " &_
					"			WHEN J.ITEMNATUREID in (31) THEN 'iDangerRatingDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (23) THEN 'iInspectionDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (41) THEN 'iReprintRentLetter.asp' " &_
					"			WHEN J.ITEMNATUREID in (43,44) THEN 'iDebtWriteOffDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (45) THEN 'iRentStatementDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (9,51) THEN 'iServiceComplaintDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (8,24,25,53) THEN 'iASBdetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (49) THEN 'iDDSchedule.asp' " &_
					"			WHEN J.ITEMNATUREID in (52) THEN 'iGarageCarParkingDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (54) THEN 'iAbandonedPropertyVehicleDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (55) THEN 'iHouseMoveDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (56) THEN 'iArrearDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (57) THEN 'iGeneralDetail.asp' " &_
					"			WHEN J.ITEMNATUREID in (58) THEN 'iCstRequestDetails.asp' " &_
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			ISNULL(SL.SATISFACTION_LETTERSTATUS,0) AS LETTERSTATUS, " &_
					"			J.ITEMNATUREID, " &_
					"			TRAFFIC_LIGHT = CASE " &_
					"				WHEN datediff(dd,J.NEXTACTIONDATE,getdate()) < 0 THEN 'green' " &_
					"				WHEN datediff(dd,J.NEXTACTIONDATE,getdate()) = 0 THEN 'amber' " &_
					"				WHEN datediff(dd,J.NEXTACTIONDATE,getdate()) > 0	 THEN 'red' END " &_
					"FROM	 	C_JOURNAL J " &_
					"			LEFT JOIN C_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"			LEFT JOIN C_SATISFACTIONLETTER SL ON SL.JOURNALID = J.JOURNALID " &_
					"			LEFT JOIN (	SELECT J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION, LA.ACTIONID   " &_ 
					"						FROM C_JOURNAL  J   " &_
					"							INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID   " &_
					"							INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID AND AR.ITEMACTIONID NOT IN (12,13)   " &_
					"						WHERE 	 " &_
					"												 AR.ARREARSHISTORYID = ( select max(ARREARSHISTORYID) from C_ARREARS where JOURNALID = J.JOURNALID  )   " &_
					"												GROUP BY J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION,LA.ACTIONID) VJ ON VJ.JOURNALID = j.JOURNALID   " &_	
					"			left join c_journalaction ja on ja.journalactionid = j.letteraction " &_		
					"WHERE	 	J.ITEMNATUREID NOT IN (1,2,20,21,22) AND (J.CUSTOMERID = " & customer_id & tenancy_filter & ")" & nature_filter  &_
					"ORDER		BY  J.JOURNALID DESC"
		'response.write strSQL
		' was ordered by this ???????   J.CURRENTITEMSTATUSID,
		' TOOK THIS OUT AS BROADLAND WANTED BOTH TENANTS TO SEE ALL ITEMS, ()
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			
			IF (rsSet("TRAFFIC_LIGHT") <> "" and rsSet("STATUS") <> "Closed") THEN
				TRAFFIC_LIGHT =  "<TD><img src='/myimages/trafficlight_" & rsSet("TRAFFIC_LIGHT") & ".gif' height=15 width=15></TD>" 
			ELSE
				TRAFFIC_LIGHT = ""
			END IF
			
			str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet("ITEMNATUREID") & "," & rsSet("JOURNALID") & ",""" & rsSet("REDIR") & """)' STYLE='CURSOR:HAND'>" &_
															"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ITEM") & "</TD>" &_
															"<TD>" & rsSet("NATURE") & "</TD>" &_
															"<TD TITLE='" & rsSet("FULLTITLE")& "'>" & rsSet("TITLE")  & "</TD>" &_
															"<TD>" & rsSet("LASTACTION") & "</TD>" &_
															"<TD>" & rsSet("STATUS")  & "</TD>" & TRAFFIC_LIGHT &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function open_me(int_nature, int_journal_id, str_redir){
		parent.synchronize_tabs(12, "BOTTOM")
		location.href = str_redir + "?journalid="+int_journal_id+"&natureid="+int_nature;
		}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(8, "BOTTOM")
		}		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onLoad="DoSync();parent.STOPLOADER('BOTTOM')">
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="behavior:url(../../Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=TOP>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B><FONT COLOR='BLUE'>Date</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100><B><FONT COLOR='BLUE'>Item</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B><FONT COLOR='BLUE'>Nature</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=150><B><FONT COLOR='BLUE'>Title</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=120><B><FONT COLOR='BLUE'>Action</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=70><B><FONT COLOR='BLUE'>Status</FONT></B></TD>
		</TR>
		<TR STYLE='HEIGHT:7PX'><TD COLSPAN=6></TD></TR></THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</BODY>
</HTML>	
	
	
	
	
	