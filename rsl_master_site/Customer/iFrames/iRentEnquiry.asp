<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams
	OpenDB()
	
	path = request("submit")
	if path = "" then path = 0 end if
	if path  <> 1 then
		entry()
	Else
		new_record()
	End If
	
	CLoseDB()
	
	Function entry()
	
		get_querystring()
'		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID AND TEAMCODE IN ('HOU') ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		Call OpenRs(rsTeam, SQL)
		if (NOT rsTeam.EOF) then
			TeamID = rsTeam("TEAMID")
		else
			TeamID = -1
		end if
		CloseRs(rsTeam)
		
		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", TeamID, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")
				
	End Function
	
	// retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		
	End Function
	
	Function new_record()
	
		Dim strSQL, journal_id
		get_querystring()

		// JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", NULL,  " & item_id & ", " & itemnature_id & ", 15, '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		rsSet.close()
		Set rsSet = Nothing
		
		// GENERAL ENTRY
		strSQL = 	"INSERT INTO C_GENERAL " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, ASSIGNTO, NOTES) " &_
					"VALUES (" & journal_id & ", 15, " & Session("userid") &_
					 ", " & Request.Form("sel_USERS") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "')"
					
		Conn.Execute(strSQL)

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")		
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
	FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[2] = "sel_TEAMS|TEAM|SELECT|Y"	
	
	function save_form(){
	
		if (!checkForm()) return false;
		RSLFORM.target = "CRM_BOTTOM_FRAME";
		RSLFORM.action = "iRentEnquiry.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		RSLFORM.submit();
	
	}

	function GetEmployees(){
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.action = "../Serverside/GetEmployees.asp"
			RSLFORM.target = "ServerIFrame"
			RSLFORM.submit()
			}
		else {
			RSLFORM.sel_USERS.outerHTML = "<select name='sel_USERS' class='textbox200' STYLE='WIDTH:300PX'><option value=''>Please select a team</option></select>"
			}
		}

	var set_length = 4000;

	function countMe() {
	  var str = event.srcElement
	  var str_len = str.value.length;

	  if(str_len >= set_length) {
		  str.value = str.value.substring(0, set_length-1);
	  }
	
	}				
</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
	<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
<FORM NAME=RSLFORM METHOD=POST>		
		<TR style='height:7px'><TD></TD></TR>
		<TR>
		<TD VALIGN=TOP>Notes :
		</TD><TD><textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class='textbox200' rows=10 name="txt_NOTES"><%=Data%></textarea>
		</TD>	
      <TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
      </TD>
		</TR>
		<tr>
			<td>Team : </td>
			<td><%=lstTeams%></td>
			<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Assign To : </td>
			<td><%=lstUsers%></td>
			<TD><image src="/js/FVS.gif" name="img_USERS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
		<TD COLSPAN=2 align=right><!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" --><input type="button" value=" Save " class="RSLButton"  onClick="save_form()" "name="button">
      </TD>
		</TR>
	</FORM>
   	</TABLE>

<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:NONE'></IFRAME>
</BODY>
</HTML>	
	
	
	
	
	