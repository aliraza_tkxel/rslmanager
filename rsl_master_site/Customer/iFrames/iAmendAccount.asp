<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%

	' WHAT THIS PAGE DOES IS LISTS ANY AMENDMENATS WHICH HAVE TAKEN PLACE ON THE CUSTOMERS ACCOUNT
	' ITS BUILT BY USING A DATASHAPE
	' WE ALSO CHECK TO SEE WHICH ITEMS HAVE CHENGED AND MARK THE CHANGED WITH A BLUE FONT
	
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING	
	Dim cnt, customer_id, str_journal_table, str_color
	Dim tmp_tdate, tmp_itype, tmp_ptype, tmp_amount, tmp_start, tmp_end, tmp_user
	Dim b_tdate, b_itype, b_ptype, b_amount, b_start, b_end, b_user
	Dim CANAMEND
	CANAMEND = 1
	
	OpenDB()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")
	build_journal()
	CloseDB()

	Function build_journal()
	
'		THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
		strSQL = " SHAPE {SELECT R.JOURNALID, CONVERT (NVARCHAR, TRANSACTIONDATE, 103) AS TDATE, " &_
					" 	ISNULL(I.DESCRIPTION, '') AS ITYPE,  " &_
					"	ISNULL(P.DESCRIPTION, '') AS PTYPE,  " &_
					"	CONVERT (NVARCHAR, R.PAYMENTSTARTDATE, 103) AS SDATE,  " &_
					"	CONVERT (NVARCHAR, R.PAYMENTENDDATE, 103) AS EDATE,  " &_
					"	ISNULL(R.AMOUNT, 0) AS AMOUNT  " &_
					"FROM 	F_RENTJOURNALAMEND R  " &_
					"	INNER JOIN F_ITEMTYPE I ON I.ITEMTYPEID = R.ITEMTYPE  " &_
					"	INNER JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = R.PAYMENTTYPE " &_
					"WHERE	R.ORIGINAL = 1 AND R.TENANCYID =  " & tenancy_id & "} " &_
				" APPEND ({ SELECT JOURNALID, CONVERT (NVARCHAR, TRANSACTIONDATE, 103) AS TDATE, " &_
				"		ISNULL(I.DESCRIPTION, '') AS ITYPE, " &_
				"		ISNULL(P.DESCRIPTION, '') AS PTYPE, " &_
				"		CONVERT (NVARCHAR, A.PAYMENTSTARTDATE, 103) AS SDATE, " &_
				"		CONVERT (NVARCHAR, A.PAYMENTENDDATE, 103) AS EDATE, " &_
				"		ISNULL(AMOUNT, 0) AS AMOUNT, " &_
				"		ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'') AS FULLNAME, " &_
				"		ISNULL(A.USERACTION, 'N/A') AS ACTION " &_
				"FROM 	F_RENTJOURNALAMEND A " &_
				"		INNER JOIN F_ITEMTYPE I ON I.ITEMTYPEID = A.ITEMTYPE " &_
				"		INNER JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = A.PAYMENTTYPE " &_
				"		INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = A.USERID " &_
				"WHERE	ORIGINAL = 0 AND A.TENANCYid = " & tenancy_id & " ORDER BY AMENDID} AS AMENDTABLE RELATE JOURNALID TO JOURNALID) "
		'rw strSQL		
		Set rsJournal = Server.CreateObject("ADODB.Recordset")
		rsJournal.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
		
		While Not rsJournal.EOF
		
			tmp_tdate	= ""
			tmp_itype	= ""
			tmp_ptype	= ""
			tmp_amount	= ""
			tmp_start	= ""
			tmp_end		= ""
			tmp_user 	= ""	
		
			cnt = cnt + 1
			str_journal_table = str_journal_table &_
			 	"<TR bgcolor='beige' STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" &_
				"<TD WIDTH=70 STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("TDATE") & "</TD>" &_
				"<TD WIDTH=80 STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("ITYPE") & "</TD>" &_
				"<TD nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("PTYPE") & "</TD>" &_
				"<TD align=center NOWRAP STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("AMOUNT") & "</TD>" &_
				"<TD align=center NOWRAP STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("SDATE") & "</TD>" &_
				"<TD align=center NOWRAP STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsJournal("EDATE") & "</TD>" &_
				"<TD align=center STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'> </TD>" &_
				"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;&nbsp;&nbsp;</TD>" &_
				"<TR>"
				
				' WE USE TEMP VARIABLES TO SEE IF THE VALUES HAVE CHANGED
				tmp_tdate	= rsJournal("TDATE")
				tmp_itype	= rsJournal("ITYPE")
				tmp_ptype	= rsJournal("PTYPE")
				tmp_amount	= rsJournal("AMOUNT")
				tmp_start	= rsJournal("SDATE")
				tmp_end		= rsJournal("EDATE")
				tmp_user 	= ""
				
				Set rsAmend = rsJournal("AMENDTABLE").Value
				While Not rsAmend.EOf 
						
						' IF SO WE EMBOLDEN THEM
						if tmp_tdate <> rsAmend("TDATE") Then b_tdate = "font-weight:bold;color:blue;" Else b_tdate = "" End If
						if tmp_itype <> rsAmend("ITYPE") Then b_itype = "font-weight:bold;color:blue;" Else b_itype = ""  End If
						if tmp_ptype <> rsAmend("PTYPE") Then b_ptype = "font-weight:bold;color:blue;" Else b_ptype = ""  End If
						if tmp_amount <> rsAmend("AMOUNT") Then b_amount = "font-weight:bold;color:blue;" Else b_amount = ""  End If
						if tmp_start <> rsAmend("SDATE") Then b_start = "font-weight:bold;color:blue;" Else b_start = ""  End If
						if tmp_end <> rsAmend("EDATE") Then b_end = "font-weight:bold;color:blue;" Else b_end = ""  End If
						if tmp_user <> rsAmend("FULLNAME") And tmp_user <> "" Then b_user = "font-weight:bold;color:blue;" Else b_user = ""  End If

						str_journal_table = str_journal_table &_
						"<TR STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" &_
						"<TD WIDTH=70 STYLE='"&b_tdate&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("TDATE") & "</TD>" &_
						"<TD WIDTH=80 STYLE='"&b_itype&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("ITYPE") & "</TD>" &_
						"<TD nowrap STYLE='"&b_ptype&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("PTYPE") & "</TD>" &_
						"<TD align=center NOWRAP STYLE='"&b_amount&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("AMOUNT") & "</TD>" &_
						"<TD align=center NOWRAP STYLE='"&b_start&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("SDATE") & "</TD>" &_
						"<TD align=center NOWRAP STYLE='"&b_end&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & rsAmend("EDATE") & "</TD>" &_
						"<TD align=center STYLE='"&b_user&"BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'> " & rsAmend("FULLNAME") & "</TD>" &_
						"<TD align=right nowrap STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>&nbsp;&nbsp;" & rsAmend("ACTION") & "&nbsp;&nbsp;</TD>" &_
						"<TR>"
						
						' RESET COLOR STRING
						b_tdate	= ""
						b_itype	= ""
						b_ptype	= ""
						b_amount= ""
						b_start	= ""
						b_end	= ""
						b_user	= ""
						
						' GET NEXT RSET FIELDS
						tmp_tdate	= rsAmend("TDATE")
						tmp_itype	= rsAmend("ITYPE")
						tmp_ptype	= rsAmend("PTYPE")
						tmp_amount	= rsAmend("AMOUNT")
						tmp_start	= rsAmend("SDATE")
						tmp_end		= rsAmend("EDATE")
						tmp_user 	= rsAmend("FULLNAME")

						
				rsAmend.MoveNext()
				Wend
		rsJournal.movenext()
		Wend
		Set rsJournal = Nothing
		Set rsAmend	 = Nothing		
		If cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No amend entries exist.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="refresh" content="120;URL=iRentAccount.asp?CustomerID=<%=customer_id%>&TenancyID=<%=tenancy_id%>">
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
	function UpdateBalance(){
		try {
			Ref = parent.document.getElementsByName("TenantBalance")
			Ref[0].value = "<%=ACCOUNT_BALANCE%>"
			}
		catch (e){
			temp = "What the hell is goin on ere....."
			}
		}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(10, "BOTTOM")
		}
	
	function ErrorHandlingStopLoader(){
		try {
			parent.STOPLOADER('BOTTOM')	
			}
		catch (e){
			temp = "this is a mega bug...."
			}
		}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="DoSync();UpdateBalance();ErrorHandlingStopLoader()" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
<TABLE WIDTH=100% CELLPADDING=2 CELLSPACING=0 STYLE="border-collapse:collapse; BORDER-bottom:2PX" border=0 hlcolor=STEELBLUE >
	<THEAD>
	<TR VALIGN=TOP>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=70><B><FONT COLOR='#133e71'>Date</FONT></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=80><B><FONT COLOR='#133e71'>Item</FONT></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100 NOWRAP><B><font color="#133e71">Payment Type</font></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" ALIGN=CENTER><B><FONT COLOR='#133e71'>Amount</FONT></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" ALIGN=CENTER><B><FONT COLOR='#133e71'>Start</FONT></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" ALIGN=CENTER><B><FONT COLOR='#133e71'>End</FONT></B></TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=center><B><FONT COLOR='#133e71'>User</FONT></B>&nbsp;</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B><FONT COLOR='#133e71'>Action</FONT></B>&nbsp;</TD>
	</TR>
	</THEAD>
		<%=str_journal_table%>
</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	