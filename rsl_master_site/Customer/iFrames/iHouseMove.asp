<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/Includes/Database/adovbs.inc"-->

<%
	
	'' Created By:	Munawar Nadeem(TkXel)
	'' Created On: 	June 27, 2008
	'' Reason:		Integraiton with Tenats Online + New Development (Not existed in RSL Manager previously)


	Dim  item_id, itemnature_id, title, customer_id, tenancy_id
	Dim  lstAuthority, lstDevelopment

	Dim enqID , description, authorityID, developmentID, noOfBedrooms, occupantsAbove18, occupantsBelow18
	enqID = ""
                        
	developmentID = -1

	OpenDB()
	path = request("submit")
	if path = "" then path = 0 end if
	
	if path  <> 1 then
    	  		
		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback

		enqID = Request("EnquiryID")

		' Enters into if block only, if EnquiryID is part of query string parameters
      	If enqID <>"" OR Len(enqID) > 0 Then

			' This function will get the details of TO_HOUSE_MOVE, based on Enquiry ID from 
			' Database tables TO_ENQUIRY_LOG and TO_HOUSE_MOVE
			
			' These extracted values will be used to populate matching frame text boxes/drop downs etc. 
			Call get_house_move_detail(enqID, description,authorityID, developmentID, noOfBedrooms, occupantsAbove18, occupantsBelow18)

		End If

		'Call to entry function to get querystring parameters & to populate Authority dropdown
		entry()

	Else

		new_record()

	End If
	
	CloseDB()
  

	Function entry()

	 	Call get_querystring()

		Call BuildSelect(lstAuthority, "sel_Authority", "G_LOCALAUTHORITY"  , "LocalAuthorityID , Description ", "Description", "Please Select", NULL, NULL, "textbox200", "style='width:300px' onchange='authority_change()'")

	End Function
	
	' retrieves values from TO_HOUSE_MOVE and TO_ENQUIRY_LOG tables. Later used to populate iframe fields
	FUNCTION get_house_move_detail(enquiryID, ByRef desc, ByRef authID, ByRef devID, ByRef bedrooms, ByRef occupAbove18, ByRef occupBelow18)

		' CALL To STORED PROCEDURE TO get HOUSE MOVE Detail

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_HOUSE_MOVE_SelectHouseMoveDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

		// if record found/returned, setting values
		If Not rs.EOF Then 
		
			desc = rs("Description")
			authID = rs("LocalAuthorityID")
			devID = rs("DevelopmentID")
			bedrooms = rs("NoOfBedrooms")
			occupAbove18 = rs("OccupantsNoGreater18")
			occupBelow18 = rs("OccupantsNoBelow18")

		End If
		
		Set comm = Nothing
		Set rs = Nothing

      End Function
	
	' retieves fields from querystring which can be from either parent crm page or from this page when submitting to itself
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

		If enqID = "" Or Len (enqID) = 0 Then
			enqID = Request("enquiryid")
		End If
		
	End Function


	
Function new_record()

		get_querystring()
		

		desc          = Request.Form("txt_MovingBecause")
		authID        = Request.Form("sel_Authority")
		devID         = Request.Form("sel_Development")
		bedrooms  	  = Request.Form("sel_Bedroom")
		occupAbove18  = Request.Form("sel_OccupantsAbove18")
		occupBelow18  = Request.Form("sel_OccupantsBelow18")

		' CALL To STORED PROCEDURE TO INSERT RECORD IN C_HOUSE_MOVE TABLE AND 

		Set comm = Server.CreateObject("ADODB.Command")

		' setting stored procedure name
		comm.commandtext = "C_HOUSE_MOVE_CREATE"
		comm.commandtype = 4
	    comm.namedparameters = true
   	    Set comm.activeconnection = Conn
	 
	    ' this parameter will return code/value, i.e. JOURNALID or -1
	    comm.parameters.append( comm.CreateParameter ("@returnValue" , adInteger, adParamReturnValue) )


	 ' setting input parameter for sproc 
	  comm.parameters.append (comm.CreateParameter ("@CUSTOMERID", adInteger, adParamInput, 4, customer_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TENANCYID", adInteger, adParamInput, 4, tenancy_id) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMID", adInteger, adParamInput, 4, item_id ) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMNATUREID", adInteger, adParamInput, 4, itemnature_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TITLE", adVarChar, adParamInput, 50, TITLE ) )

	  comm.parameters.append (comm.CreateParameter ("@LASTACTIONUSER", adInteger, adParamInput, 4, Session("userid")) )


	  comm.parameters.append (comm.CreateParameter ("@MOVINGBECAUSE", adVarChar, adParamInput, 4000, desc ) )
	  comm.parameters.append (comm.CreateParameter ("@LOCALAUTHORITYID", adInteger, adParamInput, 4, authID ) )

	  comm.parameters.append (comm.CreateParameter ("@DEVELOPMENTID", adInteger, adParamInput, 4, devID ) )

	  comm.parameters.append (comm.CreateParameter ("@NOOFBEDROOMS", adInteger, adParamInput, 4, bedrooms ) )
	  comm.parameters.append (comm.CreateParameter ("@OCCUPANTSABOVE18", adInteger, adParamInput, 4, occupAbove18 ) )
	  comm.parameters.append (comm.CreateParameter ("@OCCUPANTSBELOW18", adInteger, adParamInput, 4, occupBelow18 ) )
		
  	  ' executing sproc 
 
	  comm.execute()

	  journalID =  comm.Parameters("@returnValue").value	

	  If enqID <> "" OR Len(enqID) > 0 Then
	    Call update_enquiry_log(enqID, journalID)
      End If		

		Set comm = Nothing
		Set rs = Nothing

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function

     ' Update Enquiry Log Table
     Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")

		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID

		' Executing sproc and storing resutls in record set
		Set rs = comm.execute

		Set comm = Nothing
		Set rs = Nothing

     End Function
	
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<script language="javascript">

      var FormFields = new Array();
	FormFields[0] = "txt_MovingBecause|MovingBecause|TEXT|N"
	FormFields[1] = "sel_Authority|Authority|SELECT|Y"
	FormFields[2] = "sel_Development|Development|SELECT|Y"
	FormFields[3] = "sel_Bedroom|Bedroom|Bedroom|N"
	FormFields[4] = "sel_OccupantsAbove18|OccupantsAbove18|SELECT|N"
	FormFields[5] = "sel_OccupantsBelow18|OccupantsBelow18|SELECT|N"


	var set_length = 4000;

	// if text entered in textbox exceeds max allowed characters, it will reduced to 4000
	function countMe() {
	  
	  	var str = event.srcElement
	  	var str_len = str.value.length;

		  if(str_len >= set_length) {
			  str.value = str.value.substring(0, set_length-1);
	  			}
	}			



	function authority_change(){

      	// gets called when Aurthority value changed from drop down and returns development (drop down) list
		RSLFORM.target = "ServerIFrame";
 		RSLFORM.action = "../serverside/AuthorityChange_srv.asp?AuthorityID=" + RSLFORM.sel_Authority.value + "&DevelopmentID=" + <%=developmentID%> ;
		RSLFORM.submit();
	
	}


	function save_form(){

		if (!checkForm()) return false;

		RSLFORM.target = "CRM_BOTTOM_FRAME";
		RSLFORM.action = "iHouseMove.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
		RSLFORM.submit();

	}

	function set_values(enquiryID){

		// this function will get called on onLoad event of this form

		// This function used to set values of different elements belongs to this form
		var strEnqID = enquiryID + "";

		if ( isNaN (strEnqID ) == "false" || strEnqID  != 'undefined' ) {

			// enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter

			
			// setting value for Moving Because (reason/ description)
			var desc = document.getElementById("txt_MovingBecause");
            	desc.value = "<%=description%>"

			// setting value for authority drop down
			var authorityID = document.getElementById("sel_Authority");
	            authorityID.value = "<%=authorityID%>"

			// development values ar based on Authority. This method returns list of development values 
			// with one pre-selected development, if sent by Tenant while creating/sending enquiry
			authority_change();
	

			// setting value for no. of Bedroom drop down
			var noOfBedrooms = document.getElementById("sel_Bedroom");
	            noOfBedrooms.value = "<%=noOfBedrooms%>"

			// setting value for OccupantsAbove18 drop down
			var occupantsAbove18 = document.getElementById("sel_OccupantsAbove18");
	            occupantsAbove18.value = "<%=occupantsAbove18%>"

			// setting value for OccupantsBelow18 drop down
			var occupantsBelow18 = document.getElementById("sel_OccupantsBelow18");
	            occupantsBelow18.value = "<%=occupantsBelow18%>"

		}
	}

</script>


<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload='set_values(<%=enqID%>)'  >

<TABLE width="445" height="189" BORDER=0 CELLPADDING=0 CELLSPACING=0>
<FORM NAME=RSLFORM METHOD=POST>		
		<TR style='height:7px'>			
			<TD>Moving because (optional): </TD>
			<TD>
				<textarea style='OVERFLOW:HIDDEN;width:300px' onKeyUp="countMe()" class='textbox200' rows=3 name="txt_MovingBecause"></textarea>
			</TD>
			<TD><image src="/js/FVS.gif" name="img_MovingBecause" width="15px" height="15px" border="0">    </TD>

		</TR>

		

		<TR>
			<TD>Local Authority: </TD>
			<TD> 
				<%=lstAuthority%>
			</TD>
			<TD><image src="/js/FVS.gif" name="img_Authority" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>Development: </TD>
			<TD> 
				<select name="sel_Development" class='textbox200' style='width:300px'> 	
				<option value="0" >Any</option>
				</select>


			</TD>
				<TD><image src="/js/FVS.gif" name="img_Development" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>No. of Bedrooms: </TD>
			<TD> 
				<select name="sel_Bedroom" class='textbox200' style='width:300px'> 
				<option value="0" selected="selected">0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_Bedroom" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>No. of Occupants: 18+ </TD>
			<TD> 
				<select name="sel_OccupantsAbove18" class='textbox200' style='width:300px'> 
				<option value="0" >0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_OccupantsAbove18" width="15px" height="15px" border="0"></TD>							
		</TR>

		<TR>
			<TD>No. of Occupants: <18 </TD>
			<TD> 
				<select name="sel_OccupantsBelow18" class='textbox200' style='width:300px' > 
				<option value="0" >0</option>
 				<option value="1" >1</option>  <option value="2" >2</option>
 				<option value="3" >3</option>  <option value="4" >4</option>
 				<option value="5" >5</option>  <option value="6" >6</option>
 				<option value="7" >7</option>  <option value="8" >8</option>
 				<option value="9" >9</option>  <option value="10" >10</option>

				</select>
			</TD>
				<TD><image src="/js/FVS.gif" name="img_OccupantsBelow18" width="15px" height="15px" border="0"></TD>							
		</TR>
		
		
		<td></td>
		<td  align="right">
			<INPUT TYPE=HIDDEN NAME=hid_LETTERTEXT value="">
			<INPUT TYPE=HIDDEN NAME=hid_SIGNATURE value="">
			
			
			<input type="button" value=" Save " class="RSLButton"  onClick="save_form()" "name="button3">
		</td>
	 		<td  align="right">
				
			</td>
		</TR>
	
	</FORM>
</TABLE><!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->

<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>

</BODY>
</HTML>