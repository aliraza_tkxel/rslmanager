<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim row_id, customer_id, address_id, atype_id , btn_HTML
	Dim housenum, add1, add2, add3, towncity, po, county, tel, mobile, email, isdefault, ispoa, atype, action, dob
	Dim LeftColWidth,LeftTBWidth,strDivNextKin,firstname,surname,relationship,intAddedDetails
	Dim arrTabImageArray,strTabImage,arrTitleDisplay
	
	
	
	
	action = Request("action")
	atype_id = request("atype")
	row_id = Request("rowid")
	customer_id = Request("CUSTOMERID")
	tenancy_id = Request("tenancyid")
	intAddedDetails = Request("AddedDetails")
	
	do_GetDetails()
	
	// Change the validation rule if the contact is a social worker
	
	//Tab Array//
	
	
	//------------//
	If intAddedDetails = "1" or atype_id = "4" Then
		strDivNextKin = "Block"
	Else
		strDivNextKin = "none"
	End If	
	
	OpenDB()
	if action = "NEW" then 
		do_new()
	Else
		do_amend()
	end if
	CloseDB()
		
	Function do_new()
	
		isdefault = 0
		ispoa = 0
		row_id = 0 
		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Save' onclick=""SaveForm('NEW')"" CLASS='RSLBUTTON' tabindex=14>"
		
		// get address type
		SQL = "SELECT DESCRIPTION FROM C_ADDRESSTYPE WHERE ADDRESSTYPEID = " & atype_id
		
		Call OpenRs(rsLoader, SQL)
		
		If not rsLoader.EOF Then atype = rsLoader(0) else atype = "Unrecognised Address Type !! " End If
		CloseRs(rsLoader)
		
	End Function
	
	Function do_GetDetails()
	
		// get address type
		SQL = "SELECT * "
				"FROM P__PROPERTY P "
				"	INNER JOIN C_TENANCY T ON T.PROPERTYID = P.PROPERTYID "
				"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID "
				" WHERE T.TENANCYID =" & tenancy_id
		
		Call OpenRs(rsLoader, SQL)
			
			housenumber = rsLoader("housenumber")
			address1	= rsLoader("address1")
			address2	= rsLoader("address2")
			towncity	= rsLoader("towncity")
			postcode	= rsLoader("postcode")
			county		= rsLoader("county")
	
		CloseRs(rsLoader)
		
	End Function
	
	
	Function do_amend()

		btn_HTML = "<INPUT TYPE=BUTTON NAME='btn_SAVE' VALUE='Amend' onclick=""SaveForm('AMEND')"" CLASS='RSLBUTTON' tabindex=14>"

		SQL = 	"SELECT	" &_
				"		ISNULL(CONTACTFIRSTNAME,'') AS FIRSTNAME, "&_
				"		ISNULL(CONTACTSURNAME,'') AS SURNAME, "&_
				"		ISNULL(CONTACTDOB,'') AS DOB, "&_
				"		ISNULL(RELATIONSHIP,'') AS RELATIONSHIP "&_
				"FROM	C_ADDRESS A " &_
				"		LEFT JOIN C_ADDRESSTYPE T ON A.ADDRESSTYPE = T.ADDRESSTYPEID " &_
				"WHERE	ADDRESSID = " & row_id
		
		'response.write SQL		
		
		Call OpenRs(rsLoader, SQL)
		
		if (NOT rsLoader.EOF) then
		
				firstname 		= rsLoader("FIRSTNAME")
				surname			= rsLoader("SURNAME")		
				relationship 	= rsLoader("RELATIONSHIP")
				dob 			= rsLoader("DOB")
			
		End If
		CloseRs(rsLoader)
	
	End Function
	
	LeftColWidth = 100
	LeftTBWidth = 200
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<title><%'=Replace(arrTitleDisplay(atype_id - 1),"_"," ")%></title>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">

	var FormFields = new Array();
	
		FormFields[0] = "txt_CONTACTFIRSTNAME|FIRSTNAME|TEXT|Y"
		FormFields[1] = "txt_CONTACTSURNAME|SURNAME|TEXT|Y"
		FormFields[2] = "txt_CONTACTDOB|DOB|DATE|N"

	
	function SaveForm(str_action){
	
		if (!checkForm()) return false;
		RSLFORM.target = "CRM_TOP_FRAME";
		RSLFORM.action = "../ServerSide/occupant_svr.asp?action="+str_action+"&AddedDetails="+<%=intAddedDetails%>;
		//alert("<%=intAddedDetails%>");	
		RSLFORM.submit();
		
		window.returnvalue = 1
		window.close()
	}


	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<FORM NAME=RSLFORM METHOD=POST>
<br>
<table valign='center' align='right'  cellpadding=0 cellspacing=0>
	<tr> 
    	<td width='100%'>
			<table width='100%' cellpadding=0 cellspacing=0 >
				  <td nowrap><img SRC="../images/occupant.gif" height="20" alt="" border=0 ></td>
			 	  <td width='100%' style='border-bottom:1px solid #133e71;' align=right>&nbsp;</td>
			</table>
		</td>
	</tr>

	<tr>
		<td >
			<TABLE WIDTH='600px' CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
				<tr>
					<TD>	
						<DIV ID="DivNextOfKin" >
							<TABLE HEIGHT='100%' WIDTH='100%' STYLE="BORDER:SOLID 1PX #133E71;" CELLPADDING=1 CELLSPACING=2 BORDER=0 CLASS="TAB_TABLE">
								<tr>
									<td style='width:<%=LeftColWidth%>px'>First Name</td>
									<td style='width:<%=LeftTBWidth%>px'>
										<INPUT TYPE=TEXT VALUE="<%=firstname%>" NAME='txt_CONTACTFIRSTNAME' MAXLENGTH=100 STYLE='WIDTH:150PX' CLASS='TEXTBOX200' tabindex="7">
										<image src="/js/FVS.gif" name="img_CONTACTFIRSTNAME" width="15px" height="15px" border="0">
									</td>
									<td>Relationship</td>
									<td>
										<INPUT TYPE=TEXT VALUE="<%=relationship%>" NAME='txt_RELATIONSHIP' MAXLENGTH=100 STYLE='WIDTH:150PX' CLASS='TEXTBOX200' tabindex="7">
										<image src="/js/FVS.gif" name="img_RELATIONSHIP" width="15px" height="15px" border="0">
									</td>
								</tr>
								<tr>
									<td>Surname</td>
									<td>
										<INPUT TYPE=TEXT VALUE="<%=surname%>" NAME='txt_CONTACTSURNAME' MAXLENGTH=100 STYLE='WIDTH:150PX' CLASS='TEXTBOX200' tabindex="7">
										<image src="/js/FVS.gif" name="img_CONTACTSURNAME" width="15px" height="15px" border="0">
									</td>
									
									<td> D.O.B</td>
													
									<td>
									  <input type="text" name="txt_CONTACTDOB" value="<%=dob%>" width="15px" height="15px" CLASS='TEXTBOX200' STYLE='WIDTH:150PX'>
									  <image src="/js/FVS.gif" name="img_CONTACTDOB" width="15px" height="15px" border="0">
									</td>
								</tr>
							</table>
						</DIV>
					</td>
				</tr>
				<TR>
					<TD>
						
              <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 BORDER=0 CLASS="TAB_TABLE">
                <TR> 
                  <TD ALIGN=RIGHT> 
                    <INPUT TYPE=BUTTON VALUE="Close" NAME='btn_BACK' CLASS='RSLBUTTON' tabindex="13" ONCLICK="{window.close()}">
                    <!--<INPUT TYPE=BUTTON VALUE="Back" NAME='btn_BACK' CLASS='RSLBUTTON' tabindex="13" ONCLICK="location.href ='iContact.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'">-->
                    <%=btn_HTML%> </TD>
                </TR>
              </TABLE>
					</TD>
				
				</TR>
				<TR>
					<TD>
						<INPUT TYPE=HIDDEN NAME=hid_housenumber VALUE=<%=housenumber%>>
						<INPUT TYPE=HIDDEN NAME=hid_address1 VALUE=<%=address1%>>	
						<INPUT TYPE=HIDDEN NAME=hid_ADDRESS2 VALUE=<%=ADDRESS2%>>
						<INPUT TYPE=HIDDEN NAME=hid_towncity VALUE=<%=towncity%>>
						<INPUT TYPE=HIDDEN NAME=hid_postcode VALUE=<%=postcode%>>
						<INPUT TYPE=HIDDEN NAME=hid_county VALUE=<%=county%>>
					
						<INPUT TYPE=HIDDEN NAME=hid_CUSTOMERID VALUE=<%=customer_id%>>
						<INPUT TYPE=HIDDEN NAME=hid_TENANCYID VALUE=<%=tenancy_id%>>	
						<INPUT TYPE=HIDDEN NAME=hid_ADDRESSID VALUE=<%=address_id%>>
						<INPUT TYPE=HIDDEN NAME=hid_ADDRESSTYPE VALUE=<%=atype_id%>>
					</TD>
				</TR>
			</TABLE>
		</td>
 	</tr>
</table>
</FORM>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</BODY>
</HTML>	
	
	
	
	
	