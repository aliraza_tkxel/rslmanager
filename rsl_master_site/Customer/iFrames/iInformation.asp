<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = " WIDTH=""750"" HEIGHT=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	Dim pd_name
	Dim pd_dob
	Dim pd_gender
	Dim pd_marital
	Dim pd_ethnicity
	Dim pd_customerno
	Dim pd_employment
	Dim pd_occupation
	Dim pd_mtf
	Dim pd_ni
	Dim pd_type
	Dim pd_religion
	Dim pd_disability
	Dim pd_communication
	Dim pd_sexualorientation
	Dim pd_firstlanguage
    Dim pd_preferedcontact
	Dim customer_id, pd_firstname, pd_lastname, pd_danger, pd_ninumber , vul_history, displayData , risk_item , risk_history_id
	
	Call OpenDB()
		customer_id = Request("customerid")
		Call get_main_details()
	Call CloseDB()

	' get employee details
	Function get_main_details()

		Dim strSQL
		strSQL = 	"SELECT 	C.CUSTOMERID, FIRSTNAME, LASTNAME, OCCUPATION, " &_
					"			ISNULL(T.DESCRIPTION, '') AS CUSTOMERTYPE, " &_
					"			LEFT(C.FIRSTNAME,1) + ' ' + C.LASTNAME AS FULLNAME,  " &_
					"			ISNULL(CONVERT(NVARCHAR, C.DOB, 103), '') AS DOB,  " &_
					"			ISNULL(C.GENDER, '') AS GENDER,  " &_
					"			ISNULL(M.DESCRIPTION, '') AS MARITALSTATUS,  " &_
					"			ISNULL(ETH.DESCRIPTION, '') AS ETHNICITY, " &_
					"			ISNULL(E.DESCRIPTION, '') AS EMPLOYMENTSTATUS, " &_
					"			ISNULL(REL.DESCRIPTION, '') AS RELIGION, " &_
					"			ISNULL(SEX.DESCRIPTION, '') AS SEXUALORIENTATION, " &_
					"			ISNULL(C.NINUMBER, '') AS NINUMBER, " &_
					"			ISNULL(C.MOVINGTIMEFRAME, '') AS MOVINGTIMEFRAME, " &_
					"			ISNULL(E.DESCRIPTION, '') AS EMPLOYMENTSTATUS, " &_
					"			ISNULL(D.DESCRIPTION, '') AS DISABILITY, " &_
					"			ISNULL(COM.DESCRIPTION, '') AS COMMUNICATION, " &_
					"			ISNULL(TEN.TENANCYID, '') AS TENANCYID, " &_
					"			ISNULL(C.FIRSTLANGUAGE, '') AS FIRSTLANGUAGE, " &_
                    "			ISNULL(PC.DESCRIPTION, '') AS PREFEREDCONTACT, " &_
					"			ISNULL(TE.PROPERTYID, '') AS PROPERTYREF " &_
					"FROM 		C__CUSTOMER C  " &_
					"			LEFT JOIN G_MARITALSTATUS M ON C.MARITALSTATUS = M.MARITALSTATUSID  " &_
					"			LEFT JOIN G_DISABILITY D ON D.DISABILITYID  =  CAST(REPLACE(RIGHT(C.DISABILITY,3),',','') AS INT) "  &_
					"			LEFT JOIN G_ETHNICITY ETH ON C.ETHNICORIGIN = ETH.ETHID  " &_
					"			LEFT JOIN G_COMMUNICATION COM ON COM.COMMUNICATIONID = CAST(REPLACE(RIGHT(C.COMMUNICATION,3),',','') AS INT)  "  &_
					"			LEFT JOIN C_CUSTOMERTYPE T ON C.CUSTOMERTYPE = T.CUSTOMERTYPEID  " &_
					"			LEFT JOIN C_EMPLOYMENTSTATUS E ON C.EMPLOYMENTSTATUS = E.EMPLOYMENTSTATUSID " &_
					"			LEFT JOIN G_RELIGION REL ON C.RELIGION = REL.RELIGIONID " &_
					"			LEFT JOIN G_SEXUALORIENTATION SEX ON C.SEXUALORIENTATION = SEX.SEXUALORIENTATIONID " &_
					"			LEFT JOIN C_CUSTOMERTENANCY TEN ON C.CUSTOMERID = TEN.CUSTOMERID " &_
					"			LEFT JOIN C_TENANCY TE ON TEN.TENANCYID = TE.TENANCYid " &_
                    "			LEFT JOIN G_PREFEREDCONTACT PC ON C.PREFEREDCONTACT = PC.PREFEREDCONTACTID " &_
					"WHERE 		C.CUSTOMERID = " & customer_id

		Call OpenRs (rsSet,strSQL)
		CheckImage = "<img src='/Customer/Images/star1.jpg' width='15' height='13' title='Check for other items by clicking on the update button.'>"
		' personal details section
		pd_name			= rsSet("FULLNAME")
		pd_firstname	= rsSet("FIRSTNAME")
		pd_lastname		= rsSet("LASTNAME")
		pd_dob			= rsSet("DOB")
		pd_gender		= rsSet("GENDER")
		pd_marital		= rsSet("MARITALSTATUS")
		pd_ethnicity	= rsSet("ETHNICITY")
		pd_communication= rsSet("COMMUNICATION")
		If pd_communication <> "" Then
		    pd_communication = pd_communication & " " & CheckImage
		End If
		pd_customerno	= rsSet("CUSTOMERID")
		pd_employment	= rsSet("EMPLOYMENTSTATUS")
		pd_disability   = rsSet("DISABILITY")
		If pd_disability <> "" Then 
		    pd_disability = pd_disability & " " & CheckImage
		End If
		pd_occupation	= rsSet("OCCUPATION")
		pd_mtf			= rsSet("MOVINGTIMEFRAME")
		pd_ni			= rsSet("NINUMBER")
		pd_type			= rsSet("CUSTOMERTYPE")
		pd_ninumber		= rsSet("NINUMBER")
		pd_religion		= rsSet("RELIGION")
		pd_sexualorientation= rsSet("SEXUALORIENTATION")
		pd_firstlanguage = rsSet("FIRSTLANGUAGE")
        pd_preferedcontact = rsSet("PREFEREDCONTACT")

		Call CloseRs(rsSet)

		strSQL = "SELECT RISKHISTORYID,CATDESC,SUBCATDESC FROM dbo.RISK_CATS_SUBCATS (" & customer_id & ")"
		Call OpenRs(rsSet, strSQL)
			If Not rsSet.EOF Then
				risk_item = 1
				risk_history_id = rsSet("RISKHISTORYID")
				displayData = " The customer has the following Risk \n recorded against their current details \n\n"
				While Not rsSet.EOF
					displayData = displayData & rsSet("CATDESC") & " - " & rsSet("SUBCATDESC") 
					rsSet.Movenext()
					If Not rsSet.EOF Then
						displayData = displayData & "\n"
					End If
				Wend
			End If
			Call CloseRs(rsSet)

		SQL =  " SELECT VULNERABILITYHISTORYID,j.journalid,customerid FROM C_JOURNAL J " &_
				" INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID " &_
				" WHERE CUSTOMERID = " & customer_id & " AND ITEMNATUREID=61 " &_
				" AND CV.VULNERABILITYHISTORYID = (SELECT MAX(VULNERABILITYHISTORYID) FROM C_VULNERABILITY IN_CV WHERE IN_CV.JOURNALID=J.JOURNALID) " &_
				" AND CV.ITEMSTATUSID <> 14 "
 		Call OpenRs (rsVulnerability,SQL)
		If Not rsVulnerability.eof Then
			vul_history	= rsVulnerability("VULNERABILITYHISTORYID")
		End If
		Call CloseRs(rsVulnerability)

		If vul_history <> "" Then
			strSQL = "execute VULNERABILITY_CAT_SUBCAT " & vul_history
			Call OpenRs(rsSet, strSQL)
				If risk_item <> "" Then
					displayData = displayData & "\n\n"
				End If
				displayData = displayData & " The customer has the following Vulnerability \n recorded against their current details \n\n"
				While Not rsSet.EOF
					displayData = displayData & rsSet("CATDESC") & " - " & rsSet("SUBCATDESC") 
				rsSet.Movenext()
					If Not rsSet.EOF Then
						displayData = displayData & "\n"
					End If
				Wend
				Call CloseRs(rsSet)
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Information</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
        .style1
        {
            color: #FF0000;
        }
        .style3
        {
            color: #0033FF;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

function openDangerWindow(customer_id,risk_history_id){
		//window.showModalDialog("../popups/pCustomerDrating.asp?customerid="+customer_id, "","dialogHeight: 510px; dialogWidth: 603px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: yes");
		//window.open("../popups/pCustomerDrating.asp?customerid="+customer_id)//, "","dialogHeight: 510px; dialogWidth: 603px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: yes");
		window.showModalDialog("../popups/pViewRisk.asp?customerid="+customer_id+"&risk_history_id="+risk_history_id,"","dialogHeight: 330px; dialogWidth: 435px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: yes");		
}


function openVulnerabilityWindow(vul_history){
        window.showModalDialog("../popups/pViewVulnerability.asp?historyid="+vul_history, "","dialogHeight: 330px; dialogWidth: 435px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: yes");		
		//window.open("../popups/pViewVulnerability.asp?historyid="+vul_history)//, "","dialogHeight: 510px; dialogWidth: 603px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: yes");
}


function Information(tableNumber){
	if (tableNumber == 2){
	    document.getElementById("InfoTable_1").style.display = "none"
		document.getElementById("InfoTable_2").style.display = "block"
		document.getElementById("DownImage").style.display = "none"
		document.getElementById("UpImage").style.display = "block"
		}	
	else{
		document.getElementById("InfoTable_2").style.display = "none"
		document.getElementById("InfoTable_1").style.display = "block"
		document.getElementById("DownImage").style.display = "block"
		document.getElementById("UpImage").style.display = "none"
		}	
	}


	function showVulnerability()
	{
	   <% if vul_history <> "" or risk_item <>"" then  %>
	    alert("<%=displayData%>")
	   <%End If %> 
	}

    </script>
</head>
<body onload="parent.STOPLOADER('TOP');window.setTimeout(showVulnerability,3000);">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
    <table width="750" style="height: 180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="68%" height="100%">
                <table width="100%" style="height: 100%; border: solid 1px #133e71; border-collapse: collapse"
                    cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                    <tr>
                        <td class="TITLE">
                            Customer No
                        </td>
                        <td width="160">
                            <%=CustomerNumber(pd_customerno)%>
                        </td>
                        <td colspan="2" rowspan="6" class="TITLE">
                            <table width="99%" style="height: 99%;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div id="InfoTable_1" style="width: 100%; display: block">
                                            <table width="250" border="1" cellpadding="3" cellspacing="0" class="tab_table_withborders" style="height: 140px;
                                                border-collapse: collapse">
                                                <tr>
                                                    <td class="TITLE">
                                                        Preferred Contact
                                                    </td>
                                                    <td>
                                                        <%=pd_preferedcontact%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Disability
                                                    </td>
                                                    <td>
                                                        <%=pd_Disability%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        First Language
                                                    </td>
                                                    <td>
                                                        <%=pd_firstlanguage%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Occupation
                                                    </td>
                                                    <td>
                                                        <%=pd_occupation%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Risk
                                                    </td>
                                                    <% If risk_item <> "" Then %>
                                                    <td bgcolor="#ffffff" style="cursor: pointer" onclick="openDangerWindow(<%=pd_customerno%>,<%=risk_history_id%>)"
                                                        align="center" rowspan="1">
                                                        <img src="../../myImages/alertw.gif" width="20" height="20" alt="" />
                                                    </td>
                                                    <% else %>
                                                    <%
						         replaceCOL = "<td rowspan=""1"">&nbsp;</td>"
						        rw replaceCOL
                                                    %>
                                                    <% End If %>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Vulnerability
                                                    </td>
                                                    <% If vul_history <> "" Then %>
                                                    <td bgcolor="#ffffff" style="cursor: pointer" onclick="openVulnerabilityWindow(<%=vul_history%>)"
                                                        align="center" rowspan="1">
                                                        <img src="../../myImages/Vulnerability.gif" width="20" height="20" alt="" />
                                                    </td>
                                                    <% Else 
                     	       
						            replaceCOL = "<td rowspan=""1"">&nbsp;</td>"
						            rw replaceCOL
                                                    %>
                                                    <% End If %>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="InfoTable_2" style="display: none">
                                            <table width="250" style="height: 140px; border: solid 1px #133e71; border-collapse: collapse"
                                                cellpadding="3" cellspacing="0" border="1" class="TAB_TABLE">
                                                <tr>
                                                    <td class="TITLE">
                                                        Ethnic Origin
                                                    </td>
                                                    <td title='<%=pd_ethnicity%>'>
                                                        <%=pd_ethnicity%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Marital Status
                                                    </td>
                                                    <td>
                                                        <%=pd_marital%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Religion
                                                    </td>
                                                    <td width="160" title='<%=pd_religion%>'>
                                                        <%=pd_religion%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Sexual Orientation
                                                    </td>
                                                    <td>
                                                        <%=pd_SexualOrientation%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="TITLE">
                                                        Emp. Status
                                                    </td>
                                                    <td>
                                                        <%=pd_employment%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            Type
                        </td>
                        <td>
                            <%=pd_type%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            First Name
                        </td>
                        <td>
                            <%=pd_firstname%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            Last Name
                        </td>
                        <td width="160">
                            <%=pd_lastname%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            DOB
                        </td>
                        <td>
                            <%=pd_dob%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            Gender
                        </td>
                        <td>
                            <%=pd_gender%>
                        </td>
                    </tr>
                    <tr>
                        <td class="TITLE">
                            NI Number
                        </td>
                        <td>
                            <%=pd_ninumber%>
                        </td>
                        <td class="TITLE">
                            <table width="180" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="21">
                                        <span onclick="Information(2)" id="DownImage" style="display: block; cursor: pointer">
                                            <img src="/myImages/down.gif" width="17" height="16" alt="" /></span><span onclick="Information(1)"
                                                id="UpImage" style="cursor: pointer; display: none"><img width="17" height="16" src="/myImages/up.gif"
                                                    alt="" /></span>
                                    </td>
                                    <td width="159">
                                        <img src="/Customer/Images/star1.jpg" width="15" height="13" alt="" />
                                        Check for other items &gt;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <input type="button" name="BTN_UPDATEPD" id="BTN_UPDATEPD" value="Update" title="Update" class="RSLBUTTONSMALL"
                                onclick="parent.location.href = '/Customer/customer.asp?CustomerID=<%=customer_id%>&AMENDRECORD=true'" style="cursor:pointer" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="32%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
