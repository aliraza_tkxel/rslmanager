<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/Includes/Database/adovbs.inc"-->

<%
	
	'' Created By:	Naveed Iqbal (TkXel)
	'' Created On: 	June 30, 2008
	'' Reason:		Integraiton with Tenats Online +  New devlopment
    
   Dim  item_id, itemnature_id, title, customer_id, tenancy_id
   Dim path, lstCategory
    
    Dim enqID, psCategory, psNotes  ' ps --> parking space
    enqID = ""
      
	    OpenDB()
    
	path = request("submit")
	if path = "" then path = 0 end if

	if path  <> 1 then
      		
		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback

		enqID = Request("EnquiryID")

        	psCategory = ""
        	psNotes = ""

		' Enters into if block only, if EnquiryID is part of query string parameters
      	If enqID <>"" OR Len(enqID) > 0 Then

            ' This function will get the details of parking space required, based on Enquiry ID from 
            ' Database tables TO_ENQUIRY_LOG and TO_PARKING_SPACE

			' These extracted values will be used to populate matching frame text boxes/drop downs etc. 
            Call get_parking_space_detail(enqID, psCategory, psNotes)

		End If

		entry()

	Else
		new_record()
	End If
	
	CLoseDB()
	


Function get_parking_space_detail(enquiryID, ByRef category, ByRef notes )

    		' CALL To STORED PROCEDURE TO get parking space Detail

		Set comm = Server.CreateObject("ADODB.Command")

		// setting stored procedure name
		comm.commandtext = "TO_PARKING_SPACE_SelectParkingSpaceDetail"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		// setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID

		// executing sproc and storing resutls in record set
		Set rs = comm.execute

		// if record found/returned, setting values
		If Not rs.EOF Then 
			notes = rs("Description")
			category= rs("CATEGORYID")
		End If
		
		Set comm = Nothing
		Set rs = Nothing

End Function



Function entry()
    
	get_querystring()
	Call BuildSelect(lstCategory, "sel_Category", "TO_LOOKUP_VALUE WHERE LookUpCodeID=1"  , "LookUpValueID , Name ", "Name", "Please Select" , NULL, NULL, "textbox200", "style='width:300px' ")
	
End Function
	
	
	
' retieves fields from querystring which can be from either parent crm page or from this page when submitting to it self
Function get_querystring()
	
	item_id = Request("itemid")
	itemnature_id = Request("natureid")		
	title = Request("title")
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")

	If enqID = "" Or Len (enqID) = 0 Then
		enqID = Request("enquiryid")
	End If
		
End Function

	
Function new_record()

		Dim strSQL, journal_id, assignTo
		assignTo = -1
		get_querystring()

		lookUpValueID  = Request.Form("sel_Category")
        	otherInfo  = Replace(Request.Form("txt_NOTES"), "'", "''")
		Set comm = Server.CreateObject("ADODB.Command")

		' setting stored procedure name
		comm.commandtext = "C_PARKING_SPACE_CREATE"
		comm.commandtype = 4
	    comm.namedparameters = true
   	    Set comm.activeconnection = Conn
	 
	    ' this parameter will return code/value, i.e. JOURNALID or -1
	    comm.parameters.append( comm.CreateParameter ("@returnValue" , adInteger, adParamReturnValue) )

	 ' setting input parameter for sproc 
	  comm.parameters.append (comm.CreateParameter ("@CUSTOMERID", adInteger, adParamInput, 4, customer_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TENANCYID", adInteger, adParamInput, 4, tenancy_id) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMID", adInteger, adParamInput, 4, item_id ) )
	  comm.parameters.append (comm.CreateParameter ("@ITEMNATUREID", adInteger, adParamInput, 4, itemnature_id ) )
	  comm.parameters.append (comm.CreateParameter ("@TITLE", adVarChar, adParamInput, 50, TITLE ) )

	  comm.parameters.append (comm.CreateParameter ("@LASTACTIONUSER", adInteger, adParamInput, 4, Session("userid")) )

	  comm.parameters.append (comm.CreateParameter ("@LOOKUPVALUEID", adInteger, adParamInput, 4, lookUpValueID) )
	  comm.parameters.append (comm.CreateParameter ("@OTHERINFO", adVarchar, adParamInput, 4000, otherInfo) )

  	  ' executing sproc 
 
	  comm.execute()

	  journalID =  comm.Parameters("@returnValue").value	

	  If enqID <> "" OR Len(enqID) > 0 Then
	    Call update_enquiry_log(enqID, journalID)
      End If		

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End Function

     ' Update Enquiry Log Table
     Function update_enquiry_log(enquiryID, journalID)

		' CALL To STORED PROCEDURE TO get Arrears details
		Set comm = Server.CreateObject("ADODB.Command")

		' Setting stored procedure name
		comm.commandtext = "TO_ENQUIRY_LOG_UpdateJournalID"
		comm.commandtype = 4
		Set comm.activeconnection = Conn

		' Setting input parameter for sproc eventually used in WHERE clause
		comm.parameters(1) = enquiryID
		comm.parameters(2) = journalID

		' Executing sproc and storing resutls in record set
		Set rs = comm.execute

		Set comm = Nothing
		Set rs = Nothing

     End Function
	
%>


<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "sel_Category|Category|SELECT|Y"
	FormFields[1] = "txt_Notes|Notes|TEXT|Y"

	var set_length = 4000;

	// if text entered in textbox exceeds max allowed characters, it will reduced to 4000
	function countMe() {
	  
	  	var str = event.srcElement
	  	var str_len = str.value.length;

		  if(str_len >= set_length) {
			  str.value = str.value.substring(0, set_length-1);
	  			}
	}			



	function save_form(){

		if (!checkForm()) return false;

		RSLFORM.target = "CRM_BOTTOM_FRAME";
		RSLFORM.action = "iGarageCarParking.asp?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1&enquiryid=<%= enqID %>";
		RSLFORM.submit();
	}


	function set_values(enquiryID){

		// this function will get called on onLoad event of this form

		// This function used to set values of different elements belongs to this form
		var strEnqID = enquiryID + "";


		if ( isNaN (strEnqID ) == "false" || strEnqID  != 'undefined' ) {

			// enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter

			// setting value for category drop down
			var category = document.getElementById("sel_Category");
            	category.value = "<%=psCategory%>"

			

			// setting value for complaint notes
			var notes = document.getElementById("txt_Notes");
           		notes.value = "<%=psNotes%>"

		}

	}


</script>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload='set_values(<%=enqID%>)' >

<TABLE width="445" height="120" BORDER=0 CELLPADDING=0 CELLSPACING=0>
<FORM NAME=RSLFORM METHOD=POST>		

		<TR style='height:7px'><TD></TD></TR>

		<TR>
			<TD>Category : </TD> <TD><%=lstCategory%></TD>			
			<TD><image src="/js/FVS.gif" name="img_Category" width="15px" height="15px" border="0"></TD>							
		</TR>

		
		<TR>
		     <TD VALIGN=TOP>Notes : </TD> <TD><textarea style='OVERFLOW:HIDDEN;width:300px' onKeyUp="countMe()" class='textbox200' rows=3 name="txt_NOTES"><%=Data%></textarea>  </TD>	
             <TD> <image src="/js/FVS.gif" name="img_Notes" width="15px" height="15px" border="0">      </TD>
		</TR>
		

		<TR>		
		  <TD COLSPAN=2 align=LEFT nowrap>
			<table width="87%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
				  
					<td width="26%">		
						<INPUT TYPE=HIDDEN NAME=hid_LETTERTEXT value="">
						<INPUT TYPE=HIDDEN NAME=hid_SIGNATURE value="">								
					</td>
								
					<td width="1%"></td>
					<td width="1%" align="right"><input type="button" value=" Save " class="RSLButton"  onClick="save_form()" "name="button3"></td>
				  </tr>
        	</table>
		 	</TD>
		</TR>
	</FORM>
</TABLE><!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" -->
<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:none'></IFRAME>

</BODY>
</HTML>	