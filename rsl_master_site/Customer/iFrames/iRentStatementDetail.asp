<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, tenancyid, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT 	RS.TENANCYID, ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(I.DESCRIPTION, 'N/A') AS ITEM, " &_
                    "			B.FIRSTNAME + ' ' + B.LASTNAME AS CREATEDBY, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, RS.DATETO,RS.DATEFROM  " &_
					"FROM	 	C_JOURNAL J " &_
					"			LEFT JOIN C_ITEM I 	ON J.ITEMID = I.ITEMID " &_
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"			LEFT JOIN C_RENTSTATEMENT RS ON RS.TENANCYID = J.TENANCYID " &_
                    "			LEFT JOIN E__EMPLOYEE B ON B.EMPLOYEEID = RS.USERID " &_  
					"WHERE	 	J.ITEMNATUREID NOT IN (1,2,20,21,22) AND J.JOURNALID ="  & JOURNAL_ID 
					
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
				cnt = cnt + 1
				IF cnt > 1 Then
					str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
				else
					str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"			
				End If
				tenancyid = rsSet("tenancyid")
				str_journal_table = str_journal_table & 	"<TD COLSPAN=4>" &_
															"<TABLE width'100%'><TR>" &_
															"<TD width='135' valign=top>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD width='153' valign=top>" & rsSet("item") & "</TD>" &_
															"<TD width='136' valign=top>" & rsSet("nature") & "</TD>" &_
															"<TD width='350' valign=top>" & rsSet("DATEFROM") & " - " & rsSet("DATETO") &"</TD>" &_
                                                            "<TD width='150' valign=top>" & rsSet("CREATEDBY") & "</TD>" &_                                                            
															"</TR></TABLE>" &_
															"</TD><TR>"
		rsSet.movenext()
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
				
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">
<form name = RSLFORM method=post>
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR >
	<TD COLSPAN=5><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	      <TD nowrap width=150></td>
          <td align=right width=100></td>
        </tr></table>
	</TD>
	</TR>
	<TR valign=top>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=135><font color="blue"><b>Date:</b></font></TD>		
    	 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=153><font color="blue"><b>Item:</b></font></TD>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=136><font color="blue"><b>Nature:</b></font></TD>
	<TD width="350" colspan="2" STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><strong>Period</strong></font></TD>
    <TD width="150" colspan="2" STYLE="BORDER-BOTTOM:1PX SOLID"><font color="blue"><strong>Created By</strong></font></TD>
	  </TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=4></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</form>
</BODY>
</HTML>	
	
	
	
	
	