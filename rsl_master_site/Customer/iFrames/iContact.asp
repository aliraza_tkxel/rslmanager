<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = " WIDTH=""750"" HEIGHT=""180"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim tenancy_id, str_sql, customer_id

	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")

	str_holiday = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/repairs.gif"" alt=""Repairs"" /></td></tr><tr><td> 00 days available</td></tr><tr><td> 00 days boooked  <b>Book Now!</b></td></tr></table>"

	str_policy = 	"<table height=""100%"" width=""100%"" style=""border: solid 1px #133e71"" cellpadding=""1"" cellspacing=""2"" border=""0"" class=""TAB_TABLE"">" &_
					"<tr><td><img src=""../Images/rents.gif"" alt=""Rents"" /></td></tr><tr><td> Policy Handbook</td></tr><tr><td> Procedure Handbook</td></tr></table>"

	str_sql = 	"SELECT 	A.ADDRESSTYPE, A.ADDRESSID, ISNULL(T.DESCRIPTION,'') AS ATYPE, " &_
				"			LTRIM(ISNULL(A.HOUSENUMBER,'') + ' ' + ISNULL(ADDRESS1,'')) AS STREET, " &_
				"			CASE WHEN ISDEFAULT = 1 THEN 'Yes' ELSE 'No' END AS CORR, " &_
				"			CASE WHEN ISPOWEROFATTORNEY = 1 THEN 'Yes' ELSE 'No' END AS POW, " &_
				"			ISNULL(CONTACTFIRSTNAME,'N/A') AS FIRSTNAME, "&_
				"			ISNULL(CONTACTSURNAME,'N/A') AS SURNAME, "&_
				"			ISNULL(RELATIONSHIP,'N/A') AS RELATIONSHIP "&_
				"FROM 		C_ADDRESS A " &_
				"			LEFT JOIN C_ADDRESSTYPE T ON A.ADDRESSTYPE = T.ADDRESSTYPEID " &_
				"WHERE		CUSTOMERID = " & customer_id

	Call OpenDB()
	Call OpenRs(rsSet, str_sql)
	Call BuildSelect(lst_contacttype, "sel_ADDRESSTYPE", "C_ADDRESSTYPE", "ADDRESSTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " onchange=""sel_change()"" ")
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body, form
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var intDialogueHeight
        var intDialogueWidth
        var intAddedDetails

        function go_sched(row_id, atype) {
            //If the new contact is equal to next of kin//	
            if ((atype == 2) || (atype == 4) || (atype == 6)) {
                intDialogueHeight = 315;
                intDialogueWidth = 645;
                intAddedDetails = 1;
            }
            else {
                intDialogueHeight = 262;
                intDialogueWidth = 645;
                intAddedDetails = 0;
            }
            //Open new window with the correct height//
            myWindow = window.open('iContactDetail.asp?tenancyid=<%=tenancy_id%>&rowid=' + row_id + '&atype=' + atype + '&AddedDetails=' + intAddedDetails + '&Random=' + new Date(), '', 'width=' + intDialogueWidth + ',height=' + intDialogueHeight + ',resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,location=no');
            myWindow.focus();
        }

        function sel_change() {
            var atype = document.getElementById("sel_ADDRESSTYPE").value
            //If the new contact is equal to next of kin//	
            if ((atype == 2) || (atype == 4) || (atype == 6)) {
                intDialogueHeight = 325;
                intDialogueWidth = 645;
                intAddedDetails = 1;
            }
            else {
                intDialogueHeight = 282;
                intDialogueWidth = 645;
                intAddedDetails = 0;
            }
            //Open new window with the correct height//
            if (atype != "") {
                myWindow = window.open('iContactDetail.asp?action=NEW&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&atype=' + atype + '&AddedDetails=' + intAddedDetails + '&Random=' + new Date(), '', 'width=' + intDialogueWidth + ',height=' + intDialogueHeight + ',resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,location=no');
                myWindow.focus();
            }
        }

        function stopLoader() {
            try {
                parent.STOPLOADER('TOP')
            }
            catch (e) {
                temp = 1
            }
        }
		
    </script>
</head>
<body onload="stopLoader('TOP')">
    <form name="RSLFORM" method="post" action="">
    <table width="750" style="height: 180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td valign="top" width="68%" height="100%">
                <div class="TA" style="overflow: auto; height: 173;">
                    <table border="1" width="100%" slcolor='' hlcolor="#4682b4" style="behavior: url(/Includes/Tables/tablehl.htc);
                        border-collapse: collapse; border: solid 1px #133e71" cellpadding="3" cellspacing="0">
                        <thead>
                            <tr bgcolor="#f5f5dc">
                                <td colspan="5">
                                    <b>New Contact</b>&nbsp;
                                    <%=lst_contacttype%>
                                </td>
                            </tr>
                            <tr bgcolor="#133e71" style="color: white">
                                <td>
                                    <b>Contact Type</b>
                                </td>
                                <td>
                                    <b>Address</b>
                                </td>
                                <td>
                                    <b>Corr.</b>
                                </td>
                                <td>
                                    <b>P.O.A.</b>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <% While Not rsSet.EOF %>
                            <tr onclick="go_sched(<%=rsSet("ADDRESSID")%>,<%=rsSet("ADDRESSTYPE")%> )" style="cursor: pointer">
                                <% If rsSet("ADDRESSTYPE") =  2 Then %>
                                <td title='<%= rsSet("FIRSTNAME") & " " & rsSet("SURNAME") & " - " & rsSet("RELATIONSHIP") %>'>
                                    <%=rsSet("ATYPE")%>
                                </td>
                                <% Else %>
                                <td>
                                    <%=rsSet("ATYPE")%>
                                </td>
                                <% End If %>
                                <td nowrap="nowrap">
                                    <%=rsSet("STREET")%>
                                </td>
                                <td>
                                    <%=rsSet("CORR")%>
                                </td>
                                <td>
                                    <%=rsSet("POW")%>
                                </td>
                            </tr>
                            <% 	rsSet.movenext()
				   Wend
                            %>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td height="100%" colspan="5">
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </td>
            <td width="32%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%	Call CloseDB() %>
