<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=""750"" HEIGHT=""180""" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim row_id, lstitemtype, action, btn_HTML, reedonly, tenancy_id, customer_id
	Dim dd_itemtype, dd_accountname, dd_sortcode, dd_paymentdate, dd_initialamount
	Dim dd_regularpayment, dd_ddperiod,	dd_accountnumber
	Dim dd_periodbalance, dd_creationdate, dd_suspend
	Dim CollectionDateDaysRule,InitialDateChangeAllowed

	action = Request("action")
	row_id = Request("rowid")
	tenancy_id = Request("tenancyid")
	customer_id = Request("customerid")
	dd_ddperiod = 999
	dd_suspend = 0
	dd_action = 1

	' We set this to 1 so on create we allow insert of initial date
	InitialDateChangeAllowed  = 1 

	' there us a business rule that on set up you can not select a date for payment within 14 days or 5 days on amend
	' we first set it to 14 and reset it to five in the amend bit
	'*********************************************************************
	' PLEASE SEE TICKET 1007 ON THE SPICEWORKS

	' THE RULE IS 14 DAYS BUT WE HAVE USED 13 FOR NEW DD OR 4 DAYS ON AMEND. IN THE SQL OF GETDATE_COLUMN, GETDATE() IS BEING USED. THE 
	' COMPARISON OF DATE WITH GETDATE() WAS NOT ALLOWING THE RULE TO APPLIED CORRECTLY BECAUSE OF THE TIME ASSOCIATED WITH THE GETDATE()
	' FOR THAT REASON I HAVE REDUCED 1 FROM ACTUAL CollectionDateDaysRule VARIABLE
	CollectionDateDaysRule = 13

	If action = "NEW" Then
		row_id = 0
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" value=""Save"" title=""Save"" onclick=""ConfirmChanges('NEW')"" class=""RSLBUTTON"" style=""cursor:pointer"" />"
	Else
		btn_HTML = "<input type=""button"" name=""btn_SAVE"" value=""Amend"" title=""Save"" onclick=""ConfirmChanges('AMEND')"" class=""RSLBUTTON"" style=""cursor:pointer"" />"
	End if

	Call OpenDB()

	SQL = 	"SELECT	D.DDSCHEDULEID,D.INITIALDATE, D.TEMPSUSPEND , " &_
			"		ISNULL(D.ITEMTYPE,0) AS ITEMTYPE, " &_
			"		ISNULL(D.ACCOUNTNAME,'') AS ACCOUNTNAME, " &_
			"		ISNULL(D.SORTCODE,'') AS SORTCODE, " &_
			"		ISNULL(D.ACCOUNTNUMBER,'') AS ACCOUNTNUMBER, D.PAYMENTDATE as realpaydate, " &_
			"		PAYMENTDATE = ( " &_
"	CASE WHEN ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,GETDATE() + 1) >= CONVERT(DATETIME,CONVERT(NVARCHAR, GETDATE(), 103))   " &_
"	THEN ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,'')  " &_
"	ELSE  " &_
"		CASE WHEN CONVERT(DATETIME,CAST(DATEPART(DD,D.PAYMENTDATE) AS VARCHAR) + ' ' + DATENAME(MM,GETDATE()) + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)) >= CONVERT(DATETIME,CONVERT(NVARCHAR, GETDATE(), 103))  " &_ 
"		THEN CONVERT(DATETIME,CAST(DATEPART(DD,D.PAYMENTDATE) AS VARCHAR) + ' ' + DATENAME(MM,GETDATE()) + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)) " &_
"		ELSE " &_
"		DATEADD(MM,1,CONVERT(DATETIME,CAST(DATEPART(DD,D.PAYMENTDATE) AS VARCHAR) + ' ' + DATENAME(MM,GETDATE()) + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR))) " &_
"		END " &_
"	END  " &_
			"				), " &_
			"		ISNULL(DATEPART(DD,D.PAYMENTDATE) ,'') AS PAYMENTDAY, " &_
			"		ISNULL(D.INITIALAMOUNT,0) AS INITIALAMOUNT, " &_
			"		ISNULL(D.REGULARPAYMENT,0) AS REGULARPAYMENT, " &_
			"		ISNULL(D.DDPERIOD,0) AS DDPERIOD, " &_
			"		ISNULL(D.PERIODBALANCE,0) AS PERIODBALANCE, " &_
			"		ISNULL(D.CREATIONDATE,'') AS CREATIONDATE, " &_
			"		ISNULL(D.SUSPEND,0) AS SUSPEND ," &_
			"		D.LASTUPDATED , ISNULL(H.JOURNALITEMID,0) AS ITEMID, ISNULL(H.LETTERPRINTED,0) as LETTERPRINTED " &_
			"FROM	F_DDSCHEDULE D " &_	
			"	LEFT JOIN  F_DDHISTORY H ON H.DDSCHEDULEID = D.DDSCHEDULEID " &_
	 		"	AND H.DDHISTORYID = (SELECT MAX(DDHISTORYID) FROM F_DDHISTORY WHERE DDSCHEDULEID = D.DDSCHEDULEID) " &_
			"WHERE	D.DDSCHEDULEID = " & row_id


	'"		ISNULL(CONVERT(NVARCHAR, D.PAYMENTDATE, 103) ,'') AS PAYMENTDATE, " &_


	Call OpenRs(rsLoader, SQL)

	If (NOT rsLoader.EOF) Then

		amend_flag = 1

		'-- change the business rule for dates
		CollectionDateDaysRule = 4

		'-- if we are amedning we send actionid to the server
		dd_action = 2
		dd_initialdate = rsLoader("INITIALDATE")
		dd_itemID = rsLoader("ITEMID")
		dd_LETTERPRINTED = rsLoader("LETTERPRINTED")
		dd_itemtype = rsLoader("ITEMTYPE")
		dd_accountname = rsLoader("ACCOUNTNAME")
		dd_sortcode = rsLoader("SORTCODE")
		dd_paymentdate = rsLoader("PAYMENTDATE")
		dd_paymentday = rsLoader("PAYMENTDAY")
		dd_initialamount = rsLoader("INITIALAMOUNT")
		dd_regularpayment = rsLoader("REGULARPAYMENT")
		dd_ddperiod = rsLoader("DDPERIOD")
		dd_accountnumber = rsLoader("ACCOUNTNUMBER")
		dd_periodbalance = rsLoader("PERIODBALANCE")

		If dd_itemID <> 0 and dd_LETTERPRINTED = 0 Then 
			lETTERBUTTON = " <input type=""button"" name=""btn_PRINT"" value=""Save & Print"" class=""RSLBUTTON"" title=""Save & Print"" onClick=""AttachLetter()"" style=""cursor:pointer"" /> "
			btn_html = ""
		End If

		dd_tempsusupend = rsLoader("TEMPSUSPEND")
		If dd_tempsusupend = 1 AND dd_LETTERPRINTED <> 0 Then 
		    TEMPSUSPENDBUTTON = "<input type=""button"" name=""btn_UNDOTEMPSUSPEND"" value=""Direct Debit Currently Suspended - Click to Restart"" title=""Direct Debit Currently Suspended - Click to Restart"" class=""RSLBUTTON"" onClick=""chk_UNDO_tempsuspend_clicked()"" style=""width:300px;cursor:pointer"" />"
		End If

		If DateDiff("m" , dd_paymentdate , dd_initialdate) = 0 Then
			dd_paymentdate = DateAdd("m" , 1,dd_paymentdate)
		End If

		jAVASCRIPT_PAYMENTDATE = "  document.getElementById(""txt_PAYMENTDATE"").options[document.getElementById(""txt_PAYMENTDATE"").selectedIndex].text = '" & dd_paymentday & "th';"
		jAVASCRIPT_PAYMENTDATE = jAVASCRIPT_PAYMENTDATE & "document.getElementById(""txt_REGDATE"").value = '" & dd_paymentdate & "'" 
		InitialDateChangeAllowed = 0

		If dd_initialdate <> "" THEN
			date1 = Now()
			date2 = dd_initialdate
			strDateDiff = DateDiff("d" , date2 , date1)
			If strDateDiff < 0 Then
				InitialDateChangeAllowed = 1
			End If
		End If

		if dd_periodbalance = "999" then
			Javascript_paymenttype = "document.getElementById(""sel_PAYMENTTYPE"").value = 2;"
		else
			Javascript_paymenttype = "document.getElementById(""sel_PAYMENTTYPE"").value = 1;"
		End If

		dd_creationdate = rsLoader("CREATIONDATE")
		dd_suspend = rsLoader("SUSPEND")
		DD_LASTUPDATED = rsLoader("LASTUPDATED")

		If dd_initialdate <> "" Then
			InitialMessage = "on (" & FormatDateTime(dd_initialdate ,2) & ")"
		End If

		' if first payment has been made then make initial payment and start date readonly
		If Date >= DateValue(dd_paymentdate) Then
			reedonly = "  READONLY" 
		Else
			reedonly = ""
		End If

		reedonly = "  READONLY"

		sql =" SELECT TOP 1 T.PROPERTYID ,REASSESMENTDATE, OLD_DDTOTAL,  DDREGULARPAYMENT ,ISNULL(PF.TOTALRENT,0) AS TOTALRENT " &_
			" FROM P_TARGETRENT T " &_
			"	INNER JOIN C_TENANCY TN ON TN.PROPERTYID = T.PROPERTYID " &_
			"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = TN.TENANCYID " &_	
			"   INNER JOIN P__PROPERTY P ON TN.PROPERTYID = P.PROPERTYID " &_
			"   LEFT JOIN P_FINANCIAL PF ON PF.PROPERTYID = P.PROPERTYID " &_
			"WHERE REASSESMENTDATE > GETDATE()  " &_
			"	AND T.TARGETRENTID IS NOT NULL " &_
			"	AND T.DDREGULARPAYMENT IS NOT NULL AND CT.CUSTOMERID = " & customer_id & " ORDER BY T.TARGETRENTID DESC"

		Call OpenRs(rsDDinfo, SQL)
		If (NOT rsDDinfo.EOF) Then
			dd_INCREASEDATE = rsDDinfo("REASSESMENTDATE")
			dd_OLDTOTAL = rsDDinfo("OLD_DDTOTAL")
			dd_TOTALRENT= rsDDinfo("TOTALRENT")
			dd_NEWTOTAL = rsDDinfo("DDREGULARPAYMENT")
		End If
		Call CloseRs(rsDDinfo)

	End If
	Call CloseRs(rsLoader)

'************** #8992 Updated to return correct next payment date for 5th of month
'GETDATE_COLUMN = 	"CASE WHEN (CONVERT(DATETIME,DATEPART(YEAR,GETDATE())  + '' + DATEPART(MONTH,GETDATE()) +''+PAYMENTDAY,112) < GETDATE() + " & CollectionDateDaysRule  & ") " &_
'	"THEN " &_ 
'	"	case " &_ 
'	"		WHEN(MONTH(GETDATE())=12) " &_
'	"		THEN " &_
'	"			CASE " &_ 
'	"				WHEN (CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") " &_
'	"				THEN CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,DATEADD(YY,1,GETDATE())),112) " &_
'	"				ELSE " &_
'	"				     CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
'	"			END " &_
'	"		WHEN(MONTH(GETDATE())<>12) " &_
'	"		THEN " &_
'	"			CASE " &_
'	"				WHEN (CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") " &_
'	"		    	THEN " &_
'	"		    	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
'	"		ELSE " &_
'	"			CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
'	"		END " &_
'	"	END " &_
'	"ELSE CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103) " &_
'	"END "			
GETDATE_COLUMN = 	"CASE WHEN (CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103)  < GETDATE() + " & CollectionDateDaysRule & ") " &_
	"THEN CASE WHEN (dateadd(mm,1,CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103)) < GETDATE() + " & CollectionDateDaysRule & ") " &_
	"	THEN dateadd(mm,2,CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103)) " &_
	"	ELSE dateadd(mm,1,CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103)) " &_
	"	END " &_
	"ELSE CONVERT(VARCHAR,CONVERT(SMALLDATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),103),103)  " &_
	"END"			
Getdate_ORDERBY = " CASE WHEN " &_
		"	(CONVERT(DATETIME,DATEPART(YEAR,GETDATE())  + '' + DATEPART(MONTH,GETDATE()) +''+PAYMENTDAY,112) < GETDATE() +  " & CollectionDateDaysRule  & ") THEN " &_
			"case WHEN (CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) < GETDATE() +  " & CollectionDateDaysRule  & ") " &_
			"THEN " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,DATEADD(MM,1,GETDATE())) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"ELSE " &_
			"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112)  " &_
			"END " &_
		"	ELSE " &_
		"	CONVERT(DATETIME,PAYMENTDAY + ' ' + DATENAME(MONTH,GETDATE()) + ' ' + DATENAME(YEAR,GETDATE()),112) " &_
		"	END ASC "

	Call BuildSelect(lstitemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (1,5,6) ", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", dd_itemtype, NULL, "textbox100", NULL)
	Call BuildSelect(lstDates, "txt_PAYMENTDATE", "F_DD_PAYMENTDAY ", GETDATE_COLUMN & ", PAYMENTDAY + '' + dayalias", Getdate_ORDERBY , "Select", dd_paymentdate ,"", "textbox100", "style='width:50' onChange='document.RSLFORM.txt_REGDATE.value = document.RSLFORM.txt_PAYMENTDATE.options[document.RSLFORM.txt_PAYMENTDATE.selectedIndex].value'")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Customer Module > Direct Debit Payment</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_INITIALAMOUNT|Initial Amount|PCURRENCY|Y"
	FormFields[1] = "txt_PAYMENTDATE|Payment Date|DATE|Y"
	FormFields[2] = "txt_REGULARPAYMENT|Regular Payment|PCURRENCY|Y"
	FormFields[3] = "txt_DDPERIOD|Period|INTEGER|Y"
	FormFields[4] = "txt_ACCOUNTNAME|Account Name|TEXT|Y"
	FormFields[5] = "txt_ACCOUNTNUMBER|Account Number|INTEGER|Y"
	FormFields[6] = "txt_SORTCODE|Sort Code|INTEGER|Y"
	FormFields[7] = "sel_ITEMTYPE|Item Type|SELECT|Y"
	FormFields[8] = "sel_PAYMENTTYPE|Payment Type|SELECT|Y"

	function sel_change(caller){
		var caller;
		caller = RSLFORM.sel_TYPE.value;
		document.getElementById("DD").style.display = "none";
		document.getElementById("SO").style.display = "none";
		document.getElementById("PC").style.display = "none";
		document.getElementById("" + caller + "").style.display = "block";
	}


	function chk_clicked(int_what){
		if (int_what == 2){
			document.getElementById("DDPERIOD").style.display='none';
			document.getElementById("txt_DDPERIOD").value = 999;
			}
		else {
			document.getElementById("DDPERIOD").style.display='block';
			document.getElementById("txt_DDPERIOD").value = 0;
			}
	}


	function SaveForm(str_action){

		var i = document.getElementById("sel_PAYMENTTYPE").selectedIndex;
		var PaymentType =  document.getElementById("sel_PAYMENTTYPE").options[i].value

		// validate form
		if (!checkForm()) return false;
		if (PaymentType == 1 && document.getElementById("txt_DDPERIOD").value < 1){
			alert("If regular payments is chosen then you must pick at least 1 payment.")
			return false;
			}
		document.RSLFORM.target = "frm_ps";
		document.RSLFORM.action = "../ServerSide/ddpayments_svr.asp?action="+str_action;
		document.getElementById("STATUS_DIV").style.visibility = 'visible';
		// amending do not overwrite creation date
		if (str_action == "AMEND")
			document.getElementById("hid_CREATIONDATE").value = "<%=dd_creationdate%>";
		// if new dd mandate then initialise preiodbalance
		document.getElementById("hid_PERIODBALANCE").value = document.getElementById("txt_DDPERIOD").value;
		document.RSLFORM.submit()
	}

	function sortcode() {
		var s1, s2, s3, st
			s1 = document.getElementById("txt_SORTCODE1").value;
			s2 = document.getElementById("txt_SORTCODE2").value;
			s3 = document.getElementById("txt_SORTCODE3").value;
			st = "" + s1.toString() + "" + s2.toString() + "" + s3.toString()
			document.getElementById("txt_SORTCODE").value = st;
	}
	
	function check_amount(theItem){
		if (!(theItem.value > 0)){
			ImageID = "img_" + theItem.substring(4, theItem.length)
			document.images[ImageID].src = document.images["FVW_Image"].src
			document.getElementById(ImageID).title = "Must be greater than zero"
			document.getElementById(ImageID).style.cursor = "pointer"
			}
	}
	
	function chk_sus_clicked(){
	if (!confirm("Are you sure you want to cancel these direct debit payments?"))
		return false;
			document.getElementById("hid_LASTTRANSACTIONTYPE").value = 3;
			document.getElementById("hid_SUSPEND").value = 1;
			SaveForm("AMEND")
	}


	function chk_suspend_clicked(){
		if (!confirm("Are you sure you want to suspend these direct debit payments?"))
		return false;
		document.getElementById("hid_LASTTRANSACTIONTYPE").value = 6;
		document.getElementById("hid_TEMPSUSPEND").value = 1;
		document.RSLFORM.target = "frm_ps";
		document.RSLFORM.action = "../ServerSide/ddpayments_svr.asp?action=AMEND";
		document.getElementById("STATUS_DIV").style.visibility = 'visible';
		document.RSLFORM.submit()
	}


	function chk_UNDO_tempsuspend_clicked(){
		if (!confirm("Are you sure you want to activate these direct debit payments?"))
		return false;
		document.getElementById("hid_LASTTRANSACTIONTYPE").value = 7;
		document.getElementById("hid_TEMPSUSPEND").value = 0;
		document.RSLFORM.target = "frm_ps";
		document.RSLFORM.action = "../ServerSide/ddpayments_svr.asp?action=AMEND";
		document.getElementById("STATUS_DIV").style.visibility = 'visible';
		document.RSLFORM.submit()
	}


	function ConfirmChanges(strACTION){
	<% If InitialDateChangeAllowed = 1 Then %>
		document.getElementById("hid_INITIALDATE").value = document.getElementById("txt_PAYMENTDATE").value
	<% End if %>
		if (!checkForm()) return false;

		var FirstPayment = document.getElementById("txt_INITIALAMOUNT").value
		var FirstPaymentDate = document.getElementById("txt_PAYMENTDATE").value
		var NextPayment = document.getElementById("txt_REGULARPAYMENT").value
        var i = document.getElementById("sel_PAYMENTTYPE").selectedIndex;
        var PaymentDay =  document.getElementById("sel_PAYMENTTYPE").options[i].value
        window.showModalDialog("../popups/pDDSchedule_Confirm.asp?strACTION=" + strACTION + "&FirstPayment=" + FirstPayment  + "&FirstPaymentDate=" + FirstPaymentDate + "&NextPayment=" + NextPayment + "&PaymentDay=" + PaymentDay, self,"dialogHeight: 260px; dialogWidth: 400px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
	}


	function AttachLetter(){
		var str_win
		    str_win = "../PopUps/pDDScheduleLetterPrint.asp?scheduleid=<%=row_id%>" ;
		window.open(str_win,"display","width=407,height=180, left=200,top=200") ;
	}


	function onloadDo() {
	// SPLIT SORTCODE AND ENTER INTO APPROPRIATE BOXES
	var st, s1, s2, s3
		st = new String(document.getElementById("txt_SORTCODE").value);
		s1 = st.substr(0, 2);
		s2 = st.substr(2, 2);
		s3 = st.substr(4, 2);
		document.getElementById("txt_SORTCODE1").value = s1;
		document.getElementById("txt_SORTCODE2").value = s2;
		document.getElementById("txt_SORTCODE3").value = s3;

		<%=Javascript_paymenttype%>

		<%=jAVASCRIPT_PAYMENTDATE %>

		// DISABLE CHECKBOXES IF DD SCHEDULE HAS STARTED
		if ("<%=reedonly%>" == "  READONLY"){
			// comment by Adnan
            // I have commented disabling the dropdowns, as values are not been passed to the server page which is causing problems in dd amendment
            // drops down can not be made readonly. Ideal fix would be to disable the dropdowns and use hidden values to pass the values of these dropdowns
            //document.getElementById("sel_ITEMTYPE").disabled = true;
			//document.getElementById("txt_PAYMENTDATE").disabled = true;
            // End of Comment From Adnan
			document.getElementById("sel_PAYMENTTYPE").disabled = true;
			document.getElementById("txt_INITIALAMOUNT").setAttribute("readOnly",true)
			document.getElementById("txt_DDPERIOD").setAttribute("readOnly",true)
		}
	}

    </script>
</head>
<body onload="onloadDo()">
    <form name="RSLFORM" method="post" action="" style="margin: 0px; padding: 0px;">
     <table width="750" style="height:180px; border-right: solid 1px #133e71" cellpadding="1"
        cellspacing="2" border="0" class="TAB_TABLE">
        <tr>
            <td width="70%" height="100%" style="border: solid 1px #133e71">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr style="height: 5px">
                        <td>
                        </td>
                    </tr>
                    <tr id="DD">
                        <td colspan="4" valign="middle">
                            <!-- DIRECT DEBIT AREA----------------------------->
                            <table align="center" border="0" cellpadding="1" cellspacing="0">
                                <tr>
                                    <td>
                                        Item Type
                                    </td>
                                    <td colspan="2">
                                        <%=lstitemtype%>
                                        <img src="/js/FVS.gif" name="img_ITEMTYPE" id="img_ITEMTYPE" width="10px" height="10px"
                                            border="0" alt="" />
                                    </td>
                                    <td align="right">
                                        <b>Direct Debit</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Account Name
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_ACCOUNTNAME" id="txt_ACCOUNTNAME" class="TEXTBOX200"
                                            value="<%=dd_accountname%>" maxlength="50" <%=reedonly%> />
                                        <img src="/js/FVS.gif" name="img_ACCOUNTNAME" id="img_ACCOUNTNAME" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Sort Code
                                    </td>
                                    <td>
                                        <input type="text" name="txt_SORTCODE1" id="txt_SORTCODE1" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" <%=reedonly%> />&nbsp;-
                                        <input type="text" name="txt_SORTCODE2" id="txt_SORTCODE2" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" <%=reedonly%> />&nbsp;-
                                        <input type="text" name="txt_SORTCODE3" id="txt_SORTCODE3" maxlength="2" onblur="sortcode()"
                                            class="TEXTBOX100" style="width: 20px" <%=reedonly%> />
                                        <input type="hidden" name="txt_SORTCODE" id="txt_SORTCODE" value="<%=dd_sortcode%>" />
                                        <img src="/js/FVS.gif" name="img_SORTCODE" id="img_SORTCODE" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                    <td>
                                        Account No
                                    </td>
                                    <td>
                                        <input type="text" name="txt_ACCOUNTNUMBER" id="txt_ACCOUNTNUMBER" maxlength="8"
                                            class="TEXTBOX100" value="<%=dd_accountnumber%>" <%=reedonly%> />
                                        <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER" id="img_ACCOUNTNUMBER" width="15px"
                                            height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Initial payment of
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_INITIALAMOUNT" id="txt_INITIALAMOUNT" class="TEXTBOX100"
                                            style="width: 60px" value="<%=FormatNumber(dd_initialamount,2,-1,0,0)%>" <%=reedonly%> />
                                        <img src="/js/FVS.gif" name="img_INITIALAMOUNT" id="img_INITIALAMOUNT" width="10px"
                                            height="10px" border="0" alt="" />
                                        <%=InitialMessage  %>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">
                                        Regular payments of
                                    </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_REGULARPAYMENT" id="txt_REGULARPAYMENT" class="TEXTBOX100"
                                            style="width: 60px" value="<%=FormatNumber(dd_regularpayment,2,-1,0,0)%>" />
                                        <img src="/js/FVS.gif" name="img_REGULARPAYMENT" id="img_REGULARPAYMENT" width="10px"
                                            height="10px" border="0" alt="" /><%=UpdateMessage%>
                                        on
                                        <%=lstDates%>
                                        <img src="/js/FVS.gif" name="img_PAYMENTDATE" id="img_PAYMENTDATE" width="10px" height="10px"
                                            border="0" alt="" />
                                        of each month
                                        <input type="text" name="txt_REGDATE" id="txt_REGDATE" class="TEXTBOX100" style="border: 0;
                                            width: 70px" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="1">
                                    </td>
                                    <td>
                                        Payment Type
                                    </td>
                                    <td valign="top">
                                        <select id="sel_PAYMENTTYPE" name="sel_PAYMENTTYPE" class="textbox200" style="width: 70px"
                                            onchange="chk_clicked(document.RSLFORM.sel_PAYMENTTYPE.options[document.RSLFORM.sel_PAYMENTTYPE.selectedIndex].value)">
                                            <option value="" selected="selected">Select</option>
                                            <option value="1">Regular</option>
                                            <option value="2">Infinite</option>
                                        </select>
                                        <img src="/js/FVS.gif" name="img_PAYMENTTYPE" id="img_PAYMENTTYPE" width="10px" height="10px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap" colspan="4">
                                        <table width="100%">
                                            <tr>
                                                <td width="74%">
                                                    <div id="STATUS_DIV" style="visibility: hidden">
                                                        <font style='font-size: 13PX'><b>Please wait saving...</b></font>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="DDPERIOD" style="display: none">
                                                        x
                                                        <input type="text" name="txt_DDPERIOD" id="txt_DDPERIOD" class="TEXTBOX100" style="width: 30px"
                                                            <%=reedonly%> value="<%=dd_ddperiod%>" <%=reedonly%> />
                                                        Payments
                                                        <img src="/js/FVS.gif" name="img_DDPERIOD" id="img_DDPERIOD" width="10px" height="10px"
                                                            border="0" alt="" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% If dd_suspend <> 1 Then %>
                                <% If dd_tempsusupend = 1 Then %>
                                <tr>
                                    <td colspan="4" align="right">
                                        <%=TEMPSUSPENDBUTTON%><%=lETTERbUTTON%>
                                    </td>
                                </tr>
                                <% Else %>
                                <% If dd_NEWTOTAL <> "" AND dd_TOTALRENT=dd_OLDTOTAL Then %>
                                <tr>
                                    <td colspan="4">
                                        <table style="background-color: white; border: solid 1px red">
                                            <tr>
                                                <td>
                                                    Note : The current rent payments (<%=FormatCurrency(dd_OLDTOTAL,2)%>) will<br />
                                                    increase to
                                                    <%=FormatCurrency(dd_NEWTOTAL,2)%>
                                                    on
                                                    <%=dd_INCREASEDATE%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <% End If %>
                                <tr>
                                    <td nowrap="nowrap">
                                        <%
							If amend_flag = 1 Then
							  If dd_itemID <> 0 AND dd_LETTERPRINTED = 0 Then
							Else
                                        %>
                                        <input type="button" name="btnCANCEL" id="btnCANCEL" value="Cancel DD" title="Cancel DD"
                                            class="RSLBUTTON" onclick="chk_sus_clicked()" />
                                        <input type="button" name="btnSUSPEND" id="btnSUSPEND" value="Suspend DD" title="Suspend DD"
                                            class="RSLBUTTON" onclick="chk_suspend_clicked()" />
                                        <% 
							End If
							End If %>
                                    </td>
                                    <td colspan="3" align="right">
                                        <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                                        <input type="button" name="btnBACK" id="btnBACK" value="Back" title="Back" class="RSLBUTTON"
                                            onclick="location.href ='iPayments.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>'" />
                                        <%=lETTERbUTTON%>
                                        <%=btn_HTML%>
                                    </td>
                                </tr>
                                <% End If %>
                                <% Else %>
                                <tr>
                                    <td colspan="4" align="right">
                                        <%=lETTERbUTTON%>
                                        <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                                    </td>
                                </tr>
                                <% End If %>
                            </table>
                            <!-- ---------------------------------------------->
                            <input type="hidden" name="hid_TENANCYID" id="hid_TENANCYID" value='<%=tenancy_id%>' />
                            <input type="hidden" name="hid_USERID" id="hid_USERID" value='<%=Session("USERID")%>' />
                            <input type="hidden" name="hid_CUSTOMERID" id="hid_CUSTOMERID" value='<%=customer_id%>' />
                            <input type="hidden" name="hid_DDSCHEDULEID" id="hid_DDSCHEDULEID" value='<%=row_id%>' />
                            <input type="hidden" name="hid_CREATIONDATE" id="hid_CREATIONDATE" value='<%=DATE%>' />
                            <input type="hidden" name="hid_PERIODBALANCE" id="hid_PERIODBALANCE" value='<%=dd_periodbalance%>' />
                            <input type="hidden" name="hid_SUSPEND" id="hid_SUSPEND" value="<%=dd_suspend%>" />
                            <input type="hidden" name="hid_TEMPSUSPEND" id="hid_TEMPSUSPEND" value="<%=dd_tempsuspend%>" />
                            <input type="hidden" name="hid_LASTTRANSACTIONTYPE" id="hid_LASTTRANSACTIONTYPE"
                                value="<%=dd_action%>" />
                            <input type="hidden" name="hid_INITIALDATE" id="hid_INITIALDATE" value="<%=dd_initialdate %>" />
                            <iframe name="frm_ps" id="frm_ps" width="4px" height="4px" style="display: none">
                            </iframe>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="100%" valign="top" style="border: solid 1px #133e71">
                <%=str_repairs%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
