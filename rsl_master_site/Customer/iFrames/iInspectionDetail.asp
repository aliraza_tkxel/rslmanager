<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, general_history_id, Defects_Title, last_status, ButtonDisabled, ButtonText
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	ButtonText = " Update "
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	" SELECT *, E.FIRSTNAME + ' ' + E.LASTNAME AS WASNOTEDBY FROM C_INSPECTION INS " &_
					"	 INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = INS.USERID " &_
					" WHERE JOURNALID = " & journal_id
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		PreviousNotes = ""
		While Not rsSet.EOF
			
				cnt = cnt + 1
				IF cnt > 1 Then
					str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
				else
					Defects_Title = "Inspection"
					str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
					general_history_id = rsSet("INSPECTIONID")
					
				End If
			
				ConditionNotes = rsSet("CONDITION")
				DamageNotes = rsSet("DAMAGE")
				ViewDate = rsSet("VIEWDATE")
				TheDetail = rsSet("DETAILID")
				If (ConditionNotes <> "" or DamageNotes <> "") then
					ResponseNotes 	= 	"<b>Condition Note :</b> " & ConditionNotes &_
								  		"<BR><BR>" &_
										"<b>Damage or Neglect Notes :</b> " & DamageNotes 
								  
				End If
				
				If TheDetail = "4" then
				
					ResponseNotes 		=	ResponseNotes & "<BR><BR><b>View Date :</b> " & ViewDate 
				
				End If				
					
				
				PreviousNotes = Notes
				str_journal_table = str_journal_table & 	"<TD COLSPAN=3>" &_
															"<TABLE width'100%'><TR><TD>" &_
															"<TD width='200' valign=top>" & rsSet("THEDATESTAMP") & "</TD>" &_
															"<TD width='400'valign=top>" & ResponseNotes & "</TD>" &_
															"<TD width='150' valign=top>" & rsSet("wasnotedby") & "</TD>" &_
															"</TD></TR></TABLE>" &_
															"</TD><TR>"
		rsSet.movenext()
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>No journal entries exist for this General Enquiry.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_general(int_general_history_id){
		var str_win
		str_win = "../PopUps/pGeneral.asp?generalhistoryid="+int_general_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=305, left=200,top=200") ;
	}

	function DoSync(){
		if ("<%=Request("SyncTabs")%>" == "1")
			parent.synchronize_tabs(12, "BOTTOM")
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR >
	<TD COLSPAN=3><table cellspacing=0 cellpadding=1 width=100%><tr valign=top>
          <td><b>Title:</b>&nbsp;<%=Defects_Title%></TD>
	      <TD nowrap width=150>&nbsp;</td>
          <td align=right width=100>&nbsp; </td>
        </tr></table>
	</TD>
	</TR>
	<TR valign=top>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=197><font color="blue"><b>Date:</b></font></TD>		
    	 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=604><font color="blue"><b>Notes:</b></font></TD>
		 
    <TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=212><font color="blue"><b>Noted 
      By</b></font></TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=3></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	