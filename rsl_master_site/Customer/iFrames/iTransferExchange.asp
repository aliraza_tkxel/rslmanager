<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams, lstDevelopments

	'' Modified By:	Munawar Nadeem (TkXel)
	'' Modified On: 	June 19, 2008
	'' Reason:		Integraiton with Tenats Online

	'Code Changes Start by TkXel=================================================================================

		Dim enqID , transferNotes

	'Code Changes End by TkXel=================================================================================


	Call OpenDB()

	path = request("submit")
	If path = "" Then path = 0 End If
	If path <> 1 Then

		'Code Changes Start by TkXel=================================================================================

		'' path NOT EQUAL 1 means form is not submitted on itself..in short equals to NOT Postback
		enqID = Request("EnquiryID")
		transferNotes = ""

		' Enters into if block only, if EnquiryID is part of query string parameters
		If enqID <> "" OR Len(enqID) > 0 Then
			' This function will get the details of ASB, based on Enquiry ID from
			' Database table TO_ENQUIRY_LOG
			' These extracted values will be used to populate matching frame text boxes/drop downs etc.
			Call get_transfer_detail(enqID, transferNotes)
		End If

		'Code Changes End by TkXel=================================================================================

		Call entry()

	Else

		Call new_record()

	End If

	Call CloseDB()


 'Code Changes Start by TkXel=================================================================================

	Function get_transfer_detail(enquiryID, ByRef notes)

		' CALL To STORED PROCEDURE TO get Transfer Detail, in current scenario "Description" only
		' Since Notes/Description lies in TO_ENQUIRY_LOG table, so we are using only that
		Set comm = Server.CreateObject("ADODB.Command")
			' setting stored procedure name
			comm.commandtext = "TO_ENQUIRY_LOG_SelectDescription"
			comm.commandtype = 4
		Set comm.activeconnection = Conn
			' setting input parameter for sproc eventually used in WHERE clause
			comm.parameters(1) = enquiryID
		' executing sproc and storing resutls in record set
		Set rs = comm.execute
			' if record found/returned, setting values
			If Not rs.EOF Then
				notes = rs("Description")
			End If
		Set comm = Nothing
		Set rs = Nothing

	End Function

   'Code Changes End by TkXel===================================================================================


	Function entry()

		Call get_querystring()

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = '" & Session("TeamCode") & "'"
		Call OpenRs(rsTeam, SQL)
		If (NOT rsTeam.EOF) Then
			TeamID = rsTeam("TEAMID")
		Else
			TeamID = -1
		End If
		Call CloseRs(rsTeam)

		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", TeamID, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")

		Call BuildSelect(lstDevelopments, "sel_DEVELOPMENT", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", NULL, NULL, "textbox200", " style='width:300px' ")

	End Function

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

	End Function

	Function new_record()

		Dim strSQL, journal_id

		Call get_querystring()

		If (Request.Form("chk_CLOSE") = 1) Then
			New_Status = 14
		Else
			New_Status = 13
		End If

		If Not tenancy_id <> "" Then tenancy_id = "NULL" End If

		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"

		set rsSet = Conn.Execute(strSQL)
			journal_id = rsSet.fields("JOURNALID").value
			rsSet.close()
		Set rsSet = Nothing

		' GENERAL ENTRY
		DEVELOPMENT = Request.Form("sel_DEVELOPMENT")
		If Not DEVELOPMENT <> "" Then DEVELOPMENT = "NULL" End If

		strSQL = 	"INSERT INTO C_TRANSFEREXCHANGE " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, ASSIGNTO, NOTES, MOVINGTIMEFRAME, DEVELOPMENT) " &_
					"VALUES (" & journal_id & ", " & New_Status & ", " & Session("userid") &_
					 ", " & Request.Form("sel_USERS") & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "','" & Replace(Request.Form("txt_MOVINGTIMEFRAME"), "'", "''") & "'," & DEVELOPMENT & ")"

		Conn.Execute(strSQL)

		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
        FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
        FormFields[2] = "sel_TEAMS|TEAM|SELECT|Y"
        FormFields[3] = "sel_DEVELOPMENT|Preferred Scheme|SELECT|N"
        FormFields[4] = "txt_MOVINGTIMEFRAME|Moving Timeframe|TEXT|Y"

        function save_form() {
            if (!checkForm()) return false;
            document.RSLFORM.target = "CRM_BOTTOM_FRAME";
            document.RSLFORM.action = "ITRANSFEREXCHANGE.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
            document.RSLFORM.submit();
        }

        function GetEmployees() {
            if (document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value != "") {
                document.RSLFORM.action = "../Serverside/GetEmployees.asp"
                document.RSLFORM.target = "ServerIFrame"
                document.RSLFORM.submit()
            }
            else {
                document.getElementById("dvUsers").innerHTML = "<select name='sel_USERS' id='sel_USERS' class='textbox200' style='width:300px'><option value=''>Please select a team</option></select>"
            }
        }

        function GetEmployee_detail() { }

        var set_length = 3000;

        function countMe(obj) {
            if (obj.value.length >= set_length) {
                obj.value = obj.value.substring(0, set_length - 1);
            }
        }

        function clickHandler(e) {
            return (window.event) ? window.event.srcElement : e.target;
        }

        //-- code changes start by TkXel===========================================================================

        function set_values(enquiryID) {
            // this function will get called on onLoad event of this form
            // This function used to set values of different elements belongs to this form
            var strEnqID = enquiryID + "";
            if (isNaN(strEnqID) == "false" || strEnqID != 'undefined') {
                // enters only if request generated from Enquiry Mangement Area and request includes EnquiryID parameter
                // setting value for Transfer notes
                var notes = document.getElementById("txt_NOTES");
                notes.value = "<%=transferNotes%>"
            }
        }

        //-- code changes end by TkXel===========================================================================

			
    </script>
</head>
<body class="TA" onload="set_values(<%=enqID%>)">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Preferred Scheme :
            </td>
            <td>
                <%=lstDevelopments%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DEVELOPMENT" id="img_DEVELOPMENT" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Moving Timeframe :
            </td>
            <td>
                <input type="text" name="txt_MOVINGTIMEFRAME" id="txt_MOVINGTIMEFRAME" class="textbox"
                    style="width: 300px" maxlength="50" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOVINGTIMEFRAME" id="img_MOVINGTIMEFRAME" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Notes :
            </td>
            <td>
                <textarea style="overflow: hidden; width: 300px" onkeyup="countMe(clickHandler(event));"
                    class="textbox200" rows="6" cols="10" name="txt_NOTES" id="txt_NOTES"><%=Data%></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_NOTES" id="img_NOTES" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Team :
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TEAMS" id="img_TEAMS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Assign To :
            </td>
            <td>
                <div id="dvUsers">
                    <%=lstUsers%></div>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_USERS" id="img_USERS" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" nowrap="nowrap">
                Close Enquiry: &nbsp;<input type="checkbox" name="chk_CLOSE" id="chk_CLOSE" value="1" />&nbsp;
                <!--#include virtual="includes/bottoms/blankbottom.html" -->
                <input type="button" value=" Save " title="Save" class="RSLButton" onclick="save_form()"
                    name="button" id="button" style="cursor: pointer" />
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4" height="1"
        style="display: none"></iframe>
</body>
</html>
