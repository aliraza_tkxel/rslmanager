

<%
iMONTHS = Array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
iMATCHS = Array("", "10", "11", "12", "1", "2", "3", "4", "5", "6", "7", "8", "9")
Dim LAST_PAYMENT_END

Function HB_OWED_ON_MONTH (TheMonth, TheYear, CustomerID, TenancyID)
	CURRENT_HB_TO_CURRENT_MONTH = TOTAL_HB_OWED_ON_DATE(TheMonth, TheYear, CustomerID, TenancyID)
	if (TheMonth = 1) then
		NewMonth = 12
	else
		NewMonth = TheMonth -1
	end if
	CURRENT_HB_TO_PREVIOUS_MONTH = TOTAL_HB_OWED_ON_DATE(NewMonth, TheYear, CustomerID, TenancyID)
	HB_OWED_ON_MONTH = CURRENT_HB_TO_CURRENT_MONTH - CURRENT_HB_TO_PREVIOUS_MONTH
End Function

Function HB_OWED_THIS_MONTH (CustomerID, TenancyID)
	CurrentDate = Date
	CurrentMonth = DatePart("m", CurrentDate)
	CurrentYear = DatePart("yyyy", CurrentDate)
	HB_OWED_THIS_MONTH = HB_OWED_ON_MONTH (CurrentMonth, CurrentYear, CustomerID, TenancyID)
End Function

Function TOTAL_HB_OWED_THIS_DATE (CustomerID, TenancyID)
	CurrentDate = Date
	CurrentMonth = DatePart("m", CurrentDate)
	CurrentYear = DatePart("yyyy", CurrentDate)
	TOTAL_HB_OWED_THIS_DATE = TOTAL_HB_OWED_ON_DATE (CurrentMonth, CurrentYear, CustomerID, TenancyID)
End Function

Function TOTAL_HB_OWED_ON_DATE (TheMonth, TheYear, CustomerID, TenancyID)

	LAST_PAYMENT_END = "ZZ" ' Dummy variable check to make sure that there is data in the database
	
	SQL = "SELECT TOP 1 HBI.initialSTARTDATE AS HBSTART, HBA.STARTDATE, HBA.ENDDATE, HBA.HB, T.STARTDATE AS TENANCYSTART FROM F_HBACTUALSCHEDULE HBA INNER JOIN F_HBINFORMATION HBI ON HBI.HBID = HBA.HBID INNER JOIN C_TENANCY T ON T.TENANCYID = HBI.TENANCYID WHERE HBI.ACTUALENDDATE IS NULL AND HBA.VALIDATED = 1 AND HBI.CUSTOMERID = " & CustomerID & " AND HBI.TENANCYID = " & TenancyID & " ORDER BY HBA.STARTDATE DESC"
	Call OpenRs(rsLastPayment, SQL)
	'if there is a last payment then use it to calculate the daily rate
	IF (NOT rsLastPayment.EOF) THEN
		LAST_PAYMENT_END = CDate(rsLastPayment("ENDDATE"))
		LAST_PAYMENT = Abs(rsLastPayment("HB"))
		DR = LAST_PAYMENT/(DateDiff("d", CDate(rsLastPayment("STARTDATE")), LAST_PAYMENT_END)+1)
		TENANCYSTART = rsLastPayment("HBSTART")
		if (TENANCYSTART = "" OR isNull(TENANCYSTART)) then
			TENANCYSTART = CDate("1 April 2003")
		end if
		TENANCYSTART = CDate(TENANCYSTART)
		'Response.Write "AAAAAA -- " & DR		
	ELSE
		'if no last payment then the last payment date is the startdate of the first estimated payment
		SQL = "SELECT TOP 1 HBI.initialSTARTDATE AS HBSTART, HBA.STARTDATE, HBA.ENDDATE, HBA.HB, T.STARTDATE AS TENANCYSTART FROM F_HBACTUALSCHEDULE HBA INNER JOIN F_HBINFORMATION HBI ON HBI.HBID = HBA.HBID INNER JOIN C_TENANCY T ON T.TENANCYID = HBI.TENANCYID WHERE HBI.ACTUALENDDATE IS NULL AND HBI.CUSTOMERID = " & CustomerID & " AND HBI.TENANCYID = " & TenancyID & " ORDER BY HBA.STARTDATE ASC"
		Call OpenRs(rsLastEstimated, SQL)
		if (NOT rsLastEstimated.EOF) then
			'rESPONSE.wRITE rsLastEstimated("STARTDATE")
			'rESPONSE.wRITE LAST_PAYMENT_END
			
			'Have to minus one from the last payemnt edn date to get the correct one, very important!!!!!!
			LAST_PAYMENT_END = DATEADD("d", -1, CDate(rsLastEstimated("STARTDATE")))
			'LAST_PAYMENT_END = CDate(rsLastEstimated("STARTDATE"))			
			
			'rESPONSE.wRITE LAST_PAYMENT_END			
			LAST_PAYMENT = Abs(rsLastEstimated("HB"))
			DR = LAST_PAYMENT/(DateDiff("d", CDate(rsLastEstimated("STARTDATE")), CDate(rsLastEstimated("ENDDATE")))+1)
			'Response.Write "Daily Rate -- " & DR & "<BR>"
			TENANCYSTART = rsLastEstimated("HBSTART")
			if (TENANCYSTART = "" OR isNull(TENANCYSTART)) then
				TENANCYSTART = CDate("1 April 2003")
			end if
			TENANCYSTART = CDate(TENANCYSTART)			
		end if		
		CloseRs(rsLastEstimated)
	END IF
	CloseRs(rsLastPayment)
	
	if (LAST_PAYMENT_END = "ZZ") then
		'Return Error, no data found, THIS MEANS THEY HAVE NO HB OR IT HAS ENDED?
		TOTAL_HB_OWED_ON_DATE = 0
		Exit Function
	end if

	'Iniitialise Variables
	'Get the tenancy year dependant upon the required month and year
	RequiredDate = CDate("1 " & iMONTHS(TheMonth) & " " & TheYear)

	'RW RequiredDate 
	'CurrentDateString = DatePart("d",TENANCYSTART) & " " & MonthName(DatePart("m",TENANCYSTART)) & " "
	CurrentDateString = "1" & " " & MonthName(DatePart("m",TENANCYSTART)) & " "	
	
	'rESPONSE.wRITE "CurrentDateString " & CurrentDateString
	CompareDate = CDate(CurrentDateString & TheYear)
	'rw CompareDate 
	'StandardisedCompareDate = CDate("1" & " " & MonthName(DatePart("m",TENANCYSTART)) & " " & TheYear)
	'RespectiveEndDate = DateAdd("d", -1, CompareDate)
	'RespectiveEndString = DatePart("d", RespectiveEndDate) & " " & MonthName(DatePart("m", RespectiveEndDate)) & " "
	'rESPONSE.WRITE RespectiveEndString
	'RespectiveEndString = "1" & " " & MonthName(DatePart("m", RespectiveEndDate)) & " "	

	NextMonthDate = DateAdd("m", 1, RequiredDate)
	'Response.Write "RequiredDate -- " & RequiredDate & "<BR>"
	'Response.Write "CompareDate -- " & CompareDate & "<BR>"
	'Response.Write "LAST_PAYMENT_END -- " & LAST_PAYMENT_END & "<BR>"	
	if (LAST_PAYMENT_END > NextMonthDate) then
		'Return 0, as the last payment date end is greater than the end of the month required
		TOTAL_HB_OWED_ON_DATE = 0
		'Response.Write "Exited"
		Exit Function
	end if
		
	if (RequiredDate >= CompareDate) then
		YEARSTART = CompareDate

		RespectiveEndDate = DateAdd("yyyy", 1, YEARSTART)
		RespectiveEndDate = DateAdd("d", -1, RespectiveEndDate)		
'		RespectiveEndString = DatePart("d", RespectiveEndDate) & " " & MonthName(DatePart("m", RespectiveEndDate)) & " "
		
		YEAREND = RespectiveEndDate
	else
		YEARSTART = CDate(CurrentDateString & (TheYear-1))
		RespectiveEndDate = DateAdd("yyyy", 1, YEARSTART)
		RespectiveEndDate = DateAdd("d", -1, RespectiveEndDate)		

		YEAREND = RespectiveEndDate	
	END IF

	'Response.Write "<BR>RESPECTIVE YEARSTART -- " & YEARSTART & "<BR>"
	'Response.Write "RESPECTIVE YEAREND-- " & YEAREND & "<BR>"	
	'Response.Write "LAST_PAYMENT_END-- " & LAST_PAYMENT_END & "<BR>"		
	ADJ_MONTH = DateDiff("m", YEARSTART, Date)+1
	'rw ADJ_MONTH
		
	'IF THE LASTPAYMENT HAS SPANNED MORE THEN ONE FINANCIAL YEAR DO THE FOLLOWING CALCULATION
		
	if DateAdd("d",1,LAST_PAYMENT_END) < YEARSTART then
		'Response.Write "doing a multiple" & "<BR>"
		NON_REQUIRED_DAYS = 0
		A_12TH_OF_A_YEAR = (DATEDIFF("d", YEARSTART, YEAREND)+1)/12
		HB_OWED = ((ADJ_MONTH * A_12TH_OF_A_YEAR) - NON_REQUIRED_DAYS) * DR
		'Response.Write "HB_OWED -- " & HB_OWED & "<BR>"
		
		ADJ_YEARSTART = DATEADD("yyyy", -1, YEARSTART)
		ADJ_YEAREND = DATEADD("yyyy", -1, YEAREND)
		'loop whilst we still have whole years to the LAST_PAYMENT_END
		while (LAST_PAYMENT_END < ADJ_YEARSTART)
			HB_OWED = HB_OWED + (DR * (DATEDIFF("d", ADJ_YEARSTART, ADJ_YEAREND)+1))
			'Response.Write "HB_OWED -- " & HB_OWED & "<BR>"
			ADJ_YEARSTART = DATEADD("yyyy", -1, ADJ_YEARSTART)
			ADJ_YEAREND = DATEADD("yyyy", -1, ADJ_YEAREND)
		wend
		
		'Finally add the last part on
		NON_REQUIRED_DAYS = DATEDIFF("d", ADJ_YEARSTART, LAST_PAYMENT_END)
		A_12TH_OF_A_YEAR = (DATEDIFF("d", ADJ_YEARSTART, ADJ_YEAREND)+1)/12
		TOTAL_HB_OWED_ON_DATE = HB_OWED + (((12 * A_12TH_OF_A_YEAR) - NON_REQUIRED_DAYS) * DR)
	else
		
		'else only part of a year and does not span more than one financial year
		NON_REQUIRED_DAYS = DATEDIFF("d", YEARSTART, LAST_PAYMENT_END)+1
		'Response.Write "YEARSTART -- " & YEARSTART & "<BR>"
		'Response.Write "LAST_PAYMENT_END -- " & LAST_PAYMENT_END & "<BR>"

		'Response.Write "NON_REQUIRED_DAYS -- " & NON_REQUIRED_DAYS & "<BR>"
		A_12TH_OF_A_YEAR = (DATEDIFF("d", YEARSTART, YEAREND)+1)/12
		'Response.Write "A_12TH_OF_A_YEAR -- " & A_12TH_OF_A_YEAR & "<BR>"
		'Response.Write "DR -- " & DR & "<BR>"
		'Response.Write "ADJ_MONTH -- " & ADJ_MONTH & "<BR>"		
		TOTAL_HB_OWED_ON_DATE = ((ADJ_MONTH * A_12TH_OF_A_YEAR) - NON_REQUIRED_DAYS) * DR
		'rw  "((" & ADJ_MONTH & " * " &  A_12TH_OF_A_YEAR & ") - " & NON_REQUIRED_DAYS & ") * " & DR
	end if
End Function		

'TEMP = TOTAL_HB_OWED_ON_DATE(6, 2004, 1215)
'Response.Write "TOTAL_HB_OWED_ON_DATE 6 2004 ==== " & TEMP & "<BR>"

'TEMP = TOTAL_HB_OWED_THIS_DATE(1215)
'Response.Write "TOTAL_HB_OWED_THIS_DATE ==== " & TEMP & "<BR>"

'TEMP = HB_OWED_ON_MONTH(4, 2004, 1215)
'Response.Write "HB_OWED_ON_MONTH 4 2004 ==== " & TEMP & "<BR>"

'TEMP = HB_OWED_THIS_MONTH(1215)
'Response.Write "HB_OWED_THIS_MONTH ==== " & TEMP & "<BR>"
%>
