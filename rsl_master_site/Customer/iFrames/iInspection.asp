<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path
	Dim lstTeams,txt_condition,showJournal,txt_damage,txt_viewdate,sel_Detail, hid_InspectionName

	Call OpenDB()
	Call BuildSelect(lst_Type, "sel_DETAIL", "C_DETAIL WHERE NATUREID = 23 ", "DETAILID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "onchange='Show_Hide()'")
	Call get_querystring()

	FormGo = request("submit")
	If FormGo <> "" Then
		Call new_record()
	End If
	Call CloseDB()

	' retirves fields from querystring which can be from either parent crm page or from this page when submitting ti self
	Function get_querystring()

		txt_condition = Request.form("txt_CONDITION")
		hid_InspectionName = Request.form("hid_InspectionName")
		txt_damage = Request.form("txt_DAMAGE")
		txt_viewdate = Request.form("txt_viewdate")
		sel_Detail = Request.Form("sel_DETAIL")
		item_id = Request("itemid")
		itemnature_id = Request("natureid")
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")

		If not txt_viewdate <> "" Then txt_viewdate = NULL End If

	End Function

	Function new_record()

		Dim strSQL, journal_id

		Call get_querystring()

		' JOURNAL ENTRY
		SQL = "SET NOCOUNT ON; INSERT INTO C_journal (CUSTOMERID,TENANCYID,ITEMID, ITEMNATUREID, TITLE) VALUES (" & customer_id & "," & tenancy_id & ",1,23,'" & hid_InspectionName & "'); SELECT SCOPE_IDENTITY() AS JOURNALID;"
		set rsSet = Conn.Execute(SQL)
			journalid = rsSet.fields("JOURNALID").value
		rsSet.close()

		strSQL = "INSERT INTO C_INSPECTION (CONDITION,DAMAGE,VIEWDATE, JOURNALID,USERID,CUSTOMERID,DETAILID) VALUES ('" & Replace(txt_condition,"'","''") & "','" & Replace(txt_damage,"'","''") & "','" & txt_viewdate & "'," & journalid & "," & Session("UserID")& "," & customer_id & "," & sel_Detail & ")"
		Conn.Execute(strSQL)
		showJournal = "true"
		'Response.Redirect ("IDANGERRATING.asp?dangerratingset=true")
	End Function
%>
<% if showJournal = "true" then %>
<script type="text/javascript" language="javascript">
    parent.parent.swap_div(8, 'bottom')
    parent.parent.swap_div(1, 'top')
</script>
<% End If %>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > General Enquiry</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript">

        var FormFields = new Array();
        FormFields[0] = "sel_DETAIL|Detail|SELECT|Y"
        FormFields[1] = "txt_CONDITION|Condition|TEXT|N"
        FormFields[2] = "txt_DAMAGE|Damage or Neglect|TEXT|N"

        function save_form() {
            if (document.getElementById("sel_DETAIL").options[document.getElementById("sel_DETAIL").selectedIndex].value == 4) {
                FormFields[3] = "txt_VIEWDATE|Viewing Date|DATE|N"
            }
            document.getElementById("hid_InspectionName").value = document.getElementById("sel_DETAIL").options[document.getElementById("sel_DETAIL").selectedIndex].text
            if (!checkForm()) return false;
            document.RSLFORM.action = "iInspection.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
            document.RSLFORM.submit();
        }

        function Show_Hide() {
            Insp = document.getElementById("sel_DETAIL").options[document.getElementById("sel_DETAIL").selectedIndex].value
            if (Insp > 3) {
                document.getElementById("ExtraDetails").style.display = "block"
            }
            else {
                document.getElementById("ExtraDetails").style.display = "none"
            }
        }

    </script>
</head>
<body class="TA">
    <form name="RSLFORM" method="post" action="">
    <table cellpadding="1" cellspacing="1" border="0">
        <tr style="height: 7px">
            <td width="109">
            </td>
        </tr>
        <tr>
            <td valign="top" width="109">
                Inspection Type :
            </td>
            <td width="300">
                <%=lst_Type%><img src="/js/FVS.gif" name="img_DETAIL" id="img_DETAIL" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td width="1">
            </td>
        </tr>
        <tr>
            <td valign="top" width="109">
                General condition:
            </td>
            <td width="300">
                <textarea style="overflow: hidden; width: 280px" onkeyup="" class='textbox200' rows="5" cols="20"
                    name="txt_CONDITION" id="txt_CONDITION"></textarea>
                <img src="/js/FVS.gif" name="img_CONDITION" id="img_CONDITION" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td width="1">
            </td>
        </tr>
        <tr>
            <td valign="top" width="109">
                Areas of damage
                <br/>
                or neglect :
            </td>
            <td width="300">
                <textarea style="overflow: hidden; width: 280px" onkeyup="" class="textbox200" cols="20" rows="5"
                    name="txt_DAMAGE" id="txt_DAMAGE"></textarea>
                <img src="/js/FVS.gif" name="img_DAMAGE" id="img_DAMAGE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td width="1">
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="80%">
                            <table id="ExtraDetails" style="display: none" width="326" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="113" valign="top">
                                    Available viewing date :
                                </td>
                                <td width="215">
                                     <input type="text" style="overflow: hidden; width: 150px" onkeyup="" class="textbox200"
                                        name="txt_VIEWDATE" id="txt_VIEWDATE" value="" />
                                    <img src="/js/FVS.gif" name="img_VIEWDATE" id="img_VIEWDATE" width="15px" height="15px"
                                        border="0" alt="" /> 
					                <!--#include virtual="includes/bottoms/blankbottom.html" -->
                                </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" valign="top">
                            <input type="hidden" name="hid_InspectionName" id="hid_InspectionName" value="" />
                            <input type="button" value=" Save " title="Save" class="RSLButton" onclick="save_form()" name="button"
                                id="button" style="cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <iframe src="/secureframe.asp" name="ServerIFrame" id="ServerIFrame" width="4" height="1"
        style="display: none"></iframe>
</body>
</html>
