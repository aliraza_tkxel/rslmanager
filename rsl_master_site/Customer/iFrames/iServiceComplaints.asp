<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'on error resume next
	Dim lstUsers, item_id, itemnature_id, title, customer_id, tenancy_id, path, lstTeams, lstEscLevel, lstCategory
	Dim director, chiefexec, housingmanager, ombudsman
	Dim directorId, chiefexecId, housingmanagerId, ombudsmanId

	OpenDB()
	
	path = request("submit")
	if path = "" then path = 0 end if
	if path  <> 1 then
		entry()
	Else
		new_record()
	End If
	
	CLoseDB()
	
	Function entry()
		get_querystring()

		SQL = "SELECT TEAMID FROM G_TEAMCODES WHERE TEAMCODE = 'HOU' "
		Call OpenRs(rsTeam, SQL)
		if (NOT rsTeam.EOF) then
			TeamID = rsTeam("TEAMID")
		else
			TeamID = -1
		end if
		CloseRs(rsTeam)
		Call get_escalations()
		Call BuildSelect(lstEscLevel, "sel_ESCLEVEL", "C_ESCALATIONLEVEL", "ESCALATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", null, NULL, "textbox200", " style='width:300px' onchange='get_names(this.value)' ")
		Call BuildSelect(lstCategory, "sel_CATEGORY", "C_SERVICECOMPLAINT_CATEGORY", "CATEGORYID, DESCRIPTION", "DESCRIPTION", "Please Select", null, NULL, "textbox200", " style='width:300px' ")
		Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.ACTIVE = 1 AND T.TEAMID <> 1 ", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", TeamID, NULL, "textbox200", " style='width:300px' onchange='GetEmployees()' ")
		SQL = "E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1"

		Call BuildSelect(lstUsers, "sel_USERS", SQL, "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", "Please Select", Session("USERID"), NULL, "textbox200", " style='width:300px' ")
		
	End Function
	
	' RETURNS THE NAMES OF THE LAST FOUR ESCALATION LEVELS
	Function get_escalations()
	
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "C_GET_ESCALATIONS"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = 0
		comm.execute
		director	= comm.parameters(2)
		chiefexec	= comm.parameters(3)
		housingmanager	= comm.parameters(4)
		ombudsman	= comm.parameters(5)
		directorId	= comm.parameters(6)
		chiefexecId	= comm.parameters(7)
		housingmanagerId	= comm.parameters(8)
		ombudsmanId	= comm.parameters(9)
		Set comm = Nothing

	End Function
	
	' retieves fields from querystring which can be from either parent crm page or from this page when submitting to self
	Function get_querystring()
	
		item_id = Request("itemid")
		itemnature_id = Request("natureid")		
		title = Request("title")
		customer_id = Request("customerid")
		tenancy_id = Request("tenancyid")
		
	End Function
	
	Function new_record()
	
		Dim strSQL, journal_id, assignTo
		assignTo = -1
		get_querystring()

		' DEFINE ASSIGNTO LEVEL
		If Cint(Request("sel_ESCLEVEL")) = 1 Then assignTo = Request("sel_USERS") Else assignTo = Request("txt_USERID") End If

		' DEFINE OPEN/CLOSE ID
		if (Request.Form("chk_CLOSE") = 1) then
			New_Status = 14
		else
			New_Status = 13
		end if
		
		if not tenancy_id <> "" then tenancy_id = "NULL" end if
		
		' JOURNAL ENTRY
		strSQL = 	"SET NOCOUNT ON;" &_	
					"INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE) " &_
					"VALUES (" & customer_id & ", " & tenancy_id & ", NULL,  " & item_id & ", " & itemnature_id & ", " & New_Status & ", '" & Replace(title, "'", "''") & "');" &_
					"SELECT SCOPE_IDENTITY() AS JOURNALID;"
		
		set rsSet = Conn.Execute(strSQL)
		journal_id = rsSet.fields("JOURNALID").value
		rsSet.close()
		Set rsSet = Nothing
		
		' SERVICE COMPLAINTS ENTRY
		strSQL = 	"INSERT INTO C_SERVICECOMPLAINT " &_
					"(JOURNALID, ITEMSTATUSID, LASTACTIONUSER, ASSIGNTO, NOTES, ESCALATIONLEVEL, CATEGORY, ESCALATIONDATE) " &_
					"VALUES (" & journal_id & ", " & New_Status & ", " & Session("userid") &_
					 ", " & assignTo & ", '" & Replace(Request.Form("txt_NOTES"), "'", "''") & "', " & Request("sel_ESCLEVEL") & ", " & Request("sel_CATEGORY") & ", '" & FormatDateTime(date,2) & "')"
		rw strSQL		
		Conn.Execute(strSQL)
		
		Response.Redirect ("iCustomerJournal.asp?tenancyid=" & tenancy_id & "&customerid=" & customer_id & "&SyncTabs=1")
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager -- > General Enquiry</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language="javascript">
	
	var FormFields = new Array();
	FormFields[0] = "sel_USERS|Assign To|SELECT|N"
	FormFields[1] = "txt_NOTES|Notes|TEXT|Y"
	FormFields[2] = "sel_ESCLEVEL|Escalation Level|SELECT|Y"	
	FormFields[3] = "txt_USER|Assignee|TEXT|N"	
	FormFields[4] = "sel_CATEGORY|category|SELECT|Y"	

	function save_form(){
		if (!checkForm()) return false;
		RSLFORM.target = "CRM_BOTTOM_FRAME";
		RSLFORM.action = "Iservicecomplaints.ASP?itemid=<%=item_id%>&natureid=<%=itemnature_id%>&title=<%=title%>&customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&submit=1";
		RSLFORM.submit();
	}

	var director = "<%=director%>"
	var chiefexec = "<%=chiefexec%>" 
	var housingmanager = "<%=housingmanager%>"
	var ombudsman = "<%=ombudsman%>"
	var directorId = "<%=directorId%>"
	var chiefexecId = "<%=chiefexecId%>" 
	var housingmanagerId = "<%=housingmanagerId%>"
	var ombudsmanId = "<%=ombudsmanId%>"
 
 	// this function alters the assign to area depending on the value of escid (escalation) chosen by user
	function get_names(escid){
		
		FormFields[0] = "sel_USERS|Assign To|SELECT|N"
		FormFields[3] = "txt_USER|Assignee|TEXT|N"	
		
		if (escid == 1){
			document.getElementById("names").style.display = "none";
			document.getElementById("housinglist").style.display = "block";
			FormFields[0] = "sel_USERS|Assign To|SELECT|Y"
			}
		else {
			document.getElementById("housinglist").style.display = "none";
			document.getElementById("names").style.display = "block";
			FormFields[3] = "txt_USER|Assignee|TEXT|Y"	
			if (escid == 2){
				document.getElementById("txt_USER").value = housingmanager;
				document.getElementById("txt_USERID").value = housingmanagerId;
				}
			else if (escid == 3){
				document.getElementById("txt_USER").value = director;
				document.getElementById("txt_USERID").value = directorId;
				}
			else if (escid == 4){
				document.getElementById("txt_USER").value = chiefexec;
				document.getElementById("txt_USERID").value = chiefexecId;
				}
			else if (escid == 5){
				document.getElementById("txt_USER").value = ombudsman;
				document.getElementById("txt_USERID").value = ombudsmanId;
				}
			}
		}

	var set_length = 4000;

	function countMe() {
	  var str = event.srcElement
	  var str_len = str.value.length;

	  if(str_len >= set_length) {
		  str.value = str.value.substring(0, set_length-1);
	  }
	
	}			
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
	<TABLE CELLPADDING=1 CELLSPACING=1 BORDER=0>
<FORM NAME=RSLFORM METHOD=POST>		
		<TR style='height:7px'><TD></TD></TR>
		<TR>
		<TD VALIGN=TOP>Notes :
		</TD><TD><textarea style='OVERFLOW:HIDDEN;width:300px' onkeyup="countMe()" class='textbox200' rows=6 name="txt_NOTES"><%=Data%></textarea>
		</TD>	
      <TD><image src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0"> 
      </TD>
		</TR>
		<tr>
			<td>Category : </td>
			<td><%=lstCategory%></td>
			<TD><image src="/js/FVS.gif" name="img_CATEGORY" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr>
			<td>Escalation Level : </td>
			<td><%=lstEscLevel%></td>
			<TD><image src="/js/FVS.gif" name="img_ESCLEVEL" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr id='housinglist' style='display:block'>
			<td>Assign To : </td>
			<td><%=lstUsers%></td>
			<TD><image src="/js/FVS.gif" name="img_USERS" width="15px" height="15px" border="0"></TD>							
		</tr>
		<tr id='names' style='display:none'>
			<td>Assign To : </td>
			<td>
				<INPUT TYPE=TXT NAME=txt_USER MAXLENGTH="200" SIZE=29 class="textbox200" style='width:300px'>
				<INPUT TYPE=HIDDEN NAME=txt_USERID>
			</td>
			<TD><image src="/js/FVS.gif" name="img_USER" width="15px" height="15px" border="0"></TD>							
		</tr>
		<TR>
		<TR>		
		<TD COLSPAN=2 align=right nowrap>Close Enquiry: &nbsp;<input type="checkbox" name="chk_CLOSE" value="1">&nbsp;<!--#include VIRTUAL="INCLUDES/BOTTOMS/BLANKBOTTOM.html" --><input type="button" value=" Save " class="RSLButton"  onClick="save_form()" "name="button">
      </TD>
		</TR>
	</FORM>
   	</TABLE>
<IFRAME src="/secureframe.asp"  NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:NONE'></IFRAME>
</BODY>
</HTML>	
	
	
	
	
	