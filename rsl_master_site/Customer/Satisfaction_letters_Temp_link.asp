<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	OpenDB()

				' Count how many satisfaction letters are ready to be sent 
				
				  SQL = "SELECT COUNT(*) as LETTERS_READY FROM C_SATISFACTIONLETTER SL " &_
						"		INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID " &_
						"		INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID AND R.ITEMACTIONID IN (5,6,10,15) " &_
						"		INNER JOIN R_ITEMDETAIL I ON R.ITEMDETAILID = I.ITEMDETAILID  " &_
						"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
						"		INNER JOIN C_CUSTOMERTENANCY T ON T.CUSTOMERID = C.CUSTOMERID  AND T.ENDDATE IS NULL	 " &_
						"		INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID " &_
						"		INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID  " &_
						"		INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
						"		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOTO.ORDERITEMID AND PI.ORDERITEMID IS NOT NULL " &_
						"	WHERE 	SL.SATISFACTION_LETTERSTATUS = 1 " &_
						"		AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID)AS REPAIRHISTORYID  FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
						"		AND J.CREATIONDATE > '30 OCT 2006' " 
						' "		AND J.CREATIONDATE > '18 MAY 2005' " 


				'rw sql					
				CALL OpenRs(rsSatisfactionLettersWaiting, SQL)
					
					SatisfactionLettersWaiting = 0
					if (NOT rsSatisfactionLettersWaiting.EOF) then
					
						SatisfactionLettersWaiting = rsSatisfactionLettersWaiting("LETTERS_READY")
					
					End if
					
				CloseRs(rsSatisfactionLettersWaiting)
				
				' Count how many satisfaction letters are ready to be sent 
				
				  SQL = "SELECT COUNT(*) as LETTERS_WAITING FROM C_SATISFACTIONLETTER SL " &_
						"		INNER JOIN C_JOURNAL J ON J.JOURNALID = SL.JOURNALID " &_
						"		INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID AND R.ITEMACTIONID IN (5,6,10,15) " &_
						"		INNER JOIN R_ITEMDETAIL I ON R.ITEMDETAILID = I.ITEMDETAILID  " &_
						"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
						"		INNER JOIN C_CUSTOMERTENANCY T ON T.CUSTOMERID = C.CUSTOMERID  AND T.ENDDATE IS NULL	 " &_
						"		INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID = SL.JOURNALID " &_
						"		INNER JOIN P_WORKORDER WO ON WO.WOID = WOTO.WOID  " &_
						"		INNER JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " &_
						"		INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WOTO.ORDERITEMID AND PI.ORDERITEMID IS NOT NULL " &_
						"	WHERE 	SL.SATISFACTION_LETTERSTATUS = 2 " &_
						"		AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID)AS REPAIRHISTORYID  FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " &_
						"		AND J.CREATIONDATE > '18 MAY 2005' " 


				'rw sql					
				CALL OpenRs(rsSatisfactionLettersWaitingResults, SQL)
					
					SatisfactionLettersWaitingResults = 0
					if (NOT rsSatisfactionLettersWaitingResults.EOF) then
					
						SatisfactionLettersWaitingResults = rsSatisfactionLettersWaitingResults("LETTERS_WAITING")
					
					End if
					
				CloseRs(rsSatisfactionLettersWaitingResults)
			

	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Customer--&gt; Customer Search</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method="post">
<table width="676">
  <TR>
    <TD VALIGN=TOP nowrap >&nbsp;</TD>
    <TD VALIGN=TOP nowrap >&nbsp;</TD>
  </TR>
  <TR>
    <TD width="165" VALIGN=TOP nowrap ><strong>Satisfaction Letters</strong> - </TD>
    <TD width="499" VALIGN=TOP nowrap >&nbsp;</TD>
  </TR>
  <TR>
    <TD VALIGN=TOP nowrap >&nbsp;</TD>
    <TD VALIGN=TOP nowrap >&nbsp;</TD>
  </TR>
  <TR> 
    <TD VALIGN=TOP nowrap >Satisfaction Letters</TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px;'><a href='/Customer/SatisfactionLettersList.asp'><font color=red><%=SatisfactionLettersWaiting%>
      </font></a></font></TD>
  </TR>
   <TR> 
    <TD VALIGN=TOP nowrap >Satisfaction Results</TD>
    <TD VALIGN=TOP nowrap ><font style='font-size:10px;'><a href='/Customer/SatisfactionLettersResults.asp'><font color=red><%=SatisfactionLettersWaitingResults%>
      </font></a></font></TD>
  </TR>
</table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

