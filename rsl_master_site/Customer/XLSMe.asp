<%
'Response.Buffer = True
'Response.ContentType = "application/vnd.ms-excel"
'Response.Buffer = True
'Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include 
	'		sometimes kills the xls transfer



	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count
		Dim theURL	
		Dim PageName
		Dim TheUser		 	' The Housing Officer ID - default is the session
		Dim TheArrearsTotal
		Dim ochecked, cchecked
		Dim CompareValue
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Catch all variables and use for building page
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		'Catch Housing Officer ID from dropdown
		theUser 	= Request.QueryString("WHICH")  								
		rqScheme 	= Request.QueryString("SEL_SCHEME")	' Sceme arrears property belongs to
		rqAction 	= Request.QueryString("SEL_ACTION")	' Action last Taken
		rqAsAtDate 	= Request.QueryString("txt_ASATDATE") 	' As At Date
		if Not rqAsAtDate <> "" then rqAsAtDate = FormatDateTime(now(),2) 
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Catching Variables - Any other variables caught will take place in the functions
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Initialise Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	
		' Check to see if there is value in the NAV querystring.  If there
		' is, we know that the client is using the Next and/or Prev hyperlinks
		' to navigate the recordset.
		If Request.QueryString("page") = "" Then
			intPage = 1	
		Else
			intPage = Request.QueryString("page")
		End If
		
		PageName = "ArrearsList.asp"
		MaxRowSpan = 7
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Initialise Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	OpenDB()
	get_newtenants()
	CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Function List
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		Function get_newtenants()
		
				'get all previous reuqest items so we do not lose any
				'theURL = theURLSET("cc_sort|page")
				
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Filter & Ordering Section For SQL
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
				Dim orderBy
				orderBy = " T.TENANCYID DESC "
				if (Request.QueryString("CC_Sort") <> "") then orderBy = Request.QueryString("CC_Sort")
				
				' What arrears value to compare against
				CompareValue = "300.00"
				if (Request.QueryString("COMPAREVALUE") <> "") then
					CompareValue = Request.QueryString("COMPAREVALUE")
				end if
			
				' This is the user id - which
				If NOT theUser = "All" then 
					ES = " AND P.HOUSINGOFFICER = " & cInt(theUser)	
				Else 
					ES = "" 
				End If
					
				' As at Date - shows the arrears value from the time of date requested
				If rqAsAtDate <> "" then
					AsAtDate = "AND I_J.TRANSACTIONDATE < '" & rqAsAtDate & "' "
				Else
					AsAtDate = ""
				End If
					
				' The Scheme that the property applies to ( Also known as Development )
				if rqScheme <> "" then	
					Scheme_sql = " and dev.developmentid = " & rqScheme	
				else	
					Scheme_sql = " " 
				End If
					
				' The Action that was last taken ( Arrears actions )
				if rqAction <> "" then
					Action_sql = " and vj.actionid = " & rqAction	
				else 
					Action_sql = " " 
				End If
				
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  End Filter Section
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Get Total Of arrears
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				TotalSQL =		"	SELECT  " &_
							"		sum((ISNULL(A.AMOUNT,0) - ISNULL(ABS(B.AMOUNT),0))) AS TOTALARREARS    " &_
							"	FROM C_TENANCY T    " &_
							"		inner JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  "  & AsAtDate &_ 
							"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID   " &_
							"				FROM F_RENTJOURNAL I_J    " &_
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID   " &_
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID   " &_
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
							"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10) " & AsAtDate &_
							"					AND PAYMENTTYPE = 1    " &_
							"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID    " &_
							"		LEFT JOIN (SELECT MAX(JOURNALID) AS JOURNALID, CUSTOMERID FROM C_JOURNAL where ITEMNATUREID = 10 GROUP BY CUSTOMERID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID   " &_
							"	WHERE ISNULL(A.AMOUNT,0)> 0 AND T.ENDDATE IS NULL " &_
							"		  AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)   "
			'rw TotalSQL & "<BR><BR>"
			Call OpenRs(rsTotal, TotalSQL)
			TheArrearsTotal = FormatCurrency(rsTotal("TOTALARREARS"),2)
			CloseRs(rsTotal)
							
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  End Get Total Of arrears
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			'  Start to Build arrears list 
			''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
					Dim strSQL, rsSet, intRecord 
			
					intRecord = 0
					str_data = ""
									
			strSQL =		" SELECT  VJ.DESCRIPTION AS ACTIONDESC,   " &_
							"		VJ.JOURNALID,   " &_
							"		T.TENANCYID, T.STARTDATE,    " &_
							"		P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE,   " &_
							"		ISNULL(A.AMOUNT,0) AS GROSSARREARS, "&_ 
							"		(ISNULL(A.AMOUNT,0)) AS TOTALARREARS,    " &_
							"		( SELECT MAX(TRANSACTIONDATE) FROM F_RENTJOURNAL LEFT JOIN F_PAYMENTTYPE PY ON PY.PAYMENTTYPEID = PAYMENTTYPE WHERE TRANSACTIONDATE IS NOT NULL AND TENANCYID = T.TENANCYID AND STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND PY.RENTRECEIVED IS NOT NULL) AS LASTPAYMENTDATE    " &_
							" FROM C_TENANCY T  "&_ 
							"	LEFT JOIN (	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT,  "&_ 
							"				I_J.TENANCYID  "&_ 
							"				FROM F_RENTJOURNAL I_J  "&_ 
							"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID  "&_ 
							"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  "&_ 
							"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  "&_ 
							"				WHERE I_J.STATUSID NOT IN (1,4) AND I_J.ITEMTYPE IN (1,8,9,10)  " & AsAtDate &_ 
							"				GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID  "&_ 
							"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  "&_ 
							"	LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID  "&_ 
							"	LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID  "&_ 
							"	LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
							"	LEFT JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID  "&_ 
							"	LEFT JOIN (	SELECT J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION, LA.ACTIONID "&_  
							"				FROM C_JOURNAL  J "&_ 
							"						INNER JOIN C_ARREARS AR ON AR.JOURNALID = J.JOURNALID "&_ 
							"						INNER JOIN C_LETTERACTION LA ON LA.ACTIONID = AR.ITEMACTIONID AND AR.ITEMACTIONID NOT IN (12,13) "&_ 
							"					WHERE 	J.JOURNALID = (SELECT MAX(JOURNALID) FROM C_JOURNAL WHERE CUSTOMERID = J.CUSTOMERID) "&_ 
							"						AND AR.ARREARSHISTORYID = ( select max(ARREARSHISTORYID) from C_ARREARS where JOURNALID = J.JOURNALID  ) " &_ 
							"					GROUP BY J.CUSTOMERID, J.JOURNALID, LA.DESCRIPTION,LA.ACTIONID) VJ ON VJ.CUSTOMERID = CT.CUSTOMERID  " &_					 
							" WHERE 	((ISNULL(A.AMOUNT,0))>=  "  & CompareValue & ") "&_ 
							"	AND T.ENDDATE IS NULL  " & ES  & scheme_sql & action_sql &_
							"	AND CT.CUSTOMERID = ( select max(customerid) from C_CUSTOMERTENANCY where tenancyid = t.tenancyid)  "&_ 
							" order By " & orderBy
					
					Call OpenRs(rsSet, strSQL)
					
					count = 0
					If not rsSet.eof  Then
					' Display the record that you are starting on and the record
					' that you are finishing on for this page by writing out the
					' values in the intStart and the intFinish variables.
					str_data = str_data & "<TBODY CLASS='CAPS'>"
						' Iterate through the recordset until we reach the end of the page
						' or the last record in the recordset.
						PrevTenancy = ""
						while not rsSet.eof
						TenancyID = rsSet("TENANCYID")
						CustSQL = "SELECT HBI.HBID, CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
								"	FROM C_CUSTOMERTENANCY CT " &_
								"		INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
								"		LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_		
								"		LEFT JOIN F_HBINFORMATION HBI ON HBI.CUSTOMERID = CT.CUSTOMERID AND  HBI.ACTUALENDDATE IS NULL " &_
								"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & TenancyID
						
						Call OpenRs(rsCust, CustSQL)
						strUsers = ""
						CustCount = 0
						while NOT rsCust.EOF 
							if CustCount = 0 then
								strUsers = strUsers &  rsCust("FULLNAME") & " : (" &rsCust("CUSTOMERID")  & ")"
								CustCount = 1
							else
								strUsers = strUsers & ", "  & rsCust("FULLNAME") & " : (" &rsCust("CUSTOMERID")  & ")"
							end if					
							
						IF  rsCust("HBID") <> "" THEN
							' Do the HB Calculation
							Set spObj = Server.CreateObject("ADODB.Command")
							Set spObj.activeconnection = Conn
							spObj.commandtype = 4 
							spObj.commandtext = "F_GET_CUSTOMER_HB"
							spObj.parameters(1) = 3
							spObj.parameters(2) = rsCust("CUSTOMERID")			
							spObj.Execute
							
							Est_HB = Est_HB + cDbl(spObj.parameters(3))
							'Est_HB = Est_HB + cDbl(100)
							
							Set spObj = Nothing
						ELSE
							Est_HB = Est_HB + cDbl(0)
						END IF

							
							rsCust.moveNext()
						wend
						CloseRs(rsCust)
														
							'CHECK IF THE TENANT IS RECIEVING HB
							'if not Est_HB <> "" then 
								'hbcustomer = "<img src='../myImages/x.gif' width=12 height=12>" 
							'end if
					
							single_totalarrears = (cDbl(rsSet("TOTALARREARS")) - cDbl(Est_HB)) 
	
							strStart =  "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" 
							strEnd =	"<TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & ", " & rsSet("TOWNCITY") & "</TD>" &_
										"<TD>" & rsSet("ACTIONDESC") & "</TD>" &_
										"<TD>" & rsSet("LASTPAYMENTDATE") & "</TD>" &_	
										"<TD>" & Est_HB & "</TD>" &_												
										"<TD align=right>" & FormatCurrency(single_totalarrears) & "</TD>" &_
										"<TD align=right>" & FormatCurrency(rsSet("GROSSARREARS")) & "</TD></TR>"
			
							str_data = str_data & strStart & "<TD width=190>" & strUsers & "</TD>" & strEnd
																	
							TotalArrears = TotalArrears + rsSet("TOTALARREARS")
							TotalGross = TotalGross +  rsSet("GROSSARREARS") + hbcustomer
							MainHBtotal = MainHBtotal + Est_HB
							Est_HB = 0
							hbcustomer = 0
						rsSet.movenext()
						wend
						
						str_data = str_data & "</TBODY>"
					
						str_data = str_data & 	"<TR><TD></TD><TD></TD>" &_
												"<TD></TD>" &_
												"<TD></TD>" &_
												"<TD><b>Totals</b></TD>" &_	
												"<TD><b>" & FormatCurrency(MainHBtotal) & "</b></TD>" &_												
												"<TD align=right><b>" & FormatCurrency(TotalArrears) & "</b></TD>" &_
												"<TD align=right><b>" & FormatCurrency(TotalGross) & "</b></TD></TR>"				'ensure table height is consistent with any amount of records
						
						'ensure table height is consistent with any amount of records
						
						
					End If
					
					Call CloseRs(rsSet)

					
				End function
				
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Function List
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%>


<HTML>
<HEAD>
<TITLE>RSL Manager Customers --> Arrears List </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY bgcolor="#FFFFFF" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 >
    <THEAD> 
  <TR align="left"> 
    <TD  colspan="7"> 
      <div align="left"><font color="#0066CC"><b><font size="4">Arrears List Report 
        -</font> 
        <%dim todaysDate
				 todaysDate=now()
				 response.write  todaysDate
				%>
        </b></font></div>
      </TD>
    </TR>
    <TR> 
      <TD  WIDTH=70PX>&nbsp;<B>Tenancy</B></TD>
      <TD  WIDTH=180PX>&nbsp;<B>Customer</B>&nbsp;</TD>
      <TD  WIDTH=250PX>&nbsp;<B>Address</B>&nbsp;</TD>
      <TD WIDTH=100PX>&nbsp;<B>Action</B>&nbsp;</TD>
      <TD  WIDTH=70PX align="center"><b>Last Payment</b></TD>
      <TD  WIDTH=70PX align="center"><b>Est. HB</b></TD>
      <TD  WIDTH=70PX>&nbsp;<B>Value</B>&nbsp;</TD>
      <TD  WIDTH=70PX>&nbsp;<b>Gross</b>&nbsp;</TD>
    </TR>
    </THEAD> <%=str_data%> 
    </TABLE>
</BODY>
</HTML>