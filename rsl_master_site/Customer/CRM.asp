<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/tables/crm_right_box_dummy.asp" -->
<%	
	CONST TABLE_DIMS = " width=""750"" height=""200"" " 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	' id for sql
	Dim customer_id, tenancy_id, property_id,case_id,prev_case_id
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev, disa_bled
	Dim defectperiodend,defectperiodflag
    '' Modified By:	Munawar Nadeem (TkXel)

    Dim isCaseOpen '' Written By Adeel Ahmed (Tkxel) 30 june 2010
    isCaseOpen = true
       
    Dim isPreviousCase '' Written By Umair 26 April 2012
    isPreviousCase = true



      ' code changes start bt TkXel=======================================================================

		Dim enqID, journalID ,natureID 
     
		' URL will contain EnquiryID, if request generated from Enquiry Management Area (Create CRM button)
		enqID = Request("EnquiryID")
		journalID = Request("JournalID")
		natureID  = Request("NatureID")
	
	' code changes end by TkXel=======================================================================
	
	Call OpenDB()
	customer_id = Request("CustomerID")
	if (customer_id = "" OR NOT isNumeric(customer_id)) then
		Response.Redirect "CRM_CustomerNotFound.asp"
	end if
	
	SQL = "SELECT * FROM C__CUSTOMER WHERE CUSTOMERID = " & customer_id
	Call OpenRs(rsCustomerExists, SQL)
	if (rsCustomerExists.EOF) then
		CloseRs(rsCustomerExists)
		Response.Redirect "CRM_CustomerNotFound.asp"
	end if
	Call CloseRs(rsCustomerExists)
	
	disa_bled = ""
'	Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
'NOW THAT WE HAVE MORE THAN ONE TENANCY PER CUSTOMER BECAUSE OF HISTORICAL STUFF WE HAVE TO GET THE LATEST TENANCYID
'THIS DONE BY ZANFAR ALI....
	Call OpenRs(rsSet, "SELECT CT.TENANCYID, T.PROPERTYID,ISNULL(P.DEFECTSPERIODEND,getdate()-1) AS DEFECTSPERIODEND FROM C_CUSTOMERTENANCY CT LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID AND (CT.ENDDATE > GetDate() OR CT.ENDDATE is NULL) LEFT JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID WHERE CUSTOMERID = " & customer_id & " ORDER BY CT.TENANCYID DESC " )	
	If Not rsSet.EOF Then 
		tenancy_id = rsSet(0)
		property_id = rsSet(1)
		defectperiodend= rsSet(2)
		if(defectperiodend<>"" and datediff("d",Date(),defectperiodend)>0 ) then
		    defectperiodflag=1
		else  
		   defectperiodflag=0   
		end if
	Else
		tenancy_id = -1
		property_id = -1
		disa_bled = " style=""display:none"" "
	End If
	Call CloseRs(rsSet)

    'code changes start by umair for arrears management =============================================================
    Dim HbArrearsSQL
    Dim Arrears_cust_id
    Dim ten_id 'for previous tenant

    if(tenancy_id=-1) then
        prev_cus_SQL="SELECT MAX(TENANCYID) AS TENANCYID FROM dbo.C_CUSTOMERTENANCY WHERE CUSTOMERID= " & customer_id
        Call OpenRs (rsSet, prev_cus_SQL) 

        If Not rsSet.EOF Then 
	        ten_id = rsSet("TENANCYID")
        Else
            ten_id=-1
        End if    

        Call CloseRs(rsSet)

    else        
        ten_id=tenancy_id
    end if
    if(ten_id<>-1) then

        HbArrearsSQL=" SELECT CUSTOMERID "&_
                     " FROM F_HBACTUALSCHEDULE "&_
	                 "    INNER JOIN dbo.F_HBINFORMATION ON dbo.F_HBACTUALSCHEDULE.HBID = dbo.F_HBINFORMATION.HBID AND STATUS=1 "&_
	                 "  WHERE F_HBINFORMATION.ACTUALENDDATE IS NULL "&_
                     "   AND  F_HBACTUALSCHEDULE.HBROW = (SELECT MIN(HBROW) FROM dbo.F_HBACTUALSCHEDULE WHERE HBID=F_HBINFORMATION.HBID AND VALIDATED IS NULL) "&_
                     "   AND TENANCYID= " & ten_id
         Call OpenRs (rsSet, HbArrearsSQL) 
     
         'If Not rsSet.EOF Then 
	      '  Arrears_cust_id = rsSet("CUSTOMERID")
         'Else
            Arrears_cust_id=customer_id
         'End if    
          Call CloseRs(rsSet)
      end if
    

    'code changes end by umair for arrears management ===============================================================

	'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	strSQL = 	"SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
				"FROM	 	F_RENTJOURNAL J " &_
				"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
				"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))"
	Call OpenRs (rsSet, strSQL) 
	ACCOUNT_BALANCE = FormatCurrency(rsSet("AMOUNT"))
	Call CloseRs(rsSet)
	'END OF GET BALANCE

    ' Start Code Writtrn By Adeel Ahmed (TkXel) on 30 june 2010
    Call OpenDB()
    Call OpenRs(rs,"SELECT AM_Case.IsActive,AM_Case.CaseId from AM_Case INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = AM_Case.TenancyId Where C_CUSTOMERTENANCY.CUSTOMERID = " & customer_id & " and AM_Case.IsActive = 1")
    If NOT rs.EOF Then
        isCaseOpen = True
        case_id = rs("CaseId")
    Else 
    isCaseOpen = false   
    End If
	'' Code Ended by Adeel

    ' Start Code Writtrn By Umair Naeem 28 April 2012
    Call OpenDB()
    Call OpenRs(rs,"SELECT isnull(Max(AM_Case.CaseId),-1) as CaseId from AM_Case INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = AM_Case.TenancyId Where C_CUSTOMERTENANCY.CUSTOMERID = " & customer_id & " and isnull(AM_Case.IsActive,0) <> 1 ")
    IF  rs("CaseId") <>-1 Then
        isPreviousCase = true
        prev_case_id = rs("CaseId")
    ELSE 
        isPreviousCase = false   
    END IF
	'' Code Ended by Adeel



	Call BuildSelect(selNature, "sel_NATURE", "C_NATURE WHERE ITEMID IN (2,4) ", "itemNATUREID, DESCRIPTION", "DESCRIPTION", "Select Nature", Nature, NULL, "textbox200  ", "TABINDEX=3 style='width:150' onChange='FilterByNature()' ")


      ' === STAMP CHANGES ===

    Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE C_CUSTOMER_AUDIT_GETLASTUPDATED @CustomerId = " & customer_id
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

    ' === STAMP CHANGES END ===
'Start handling ASB management
    Dim isASBCaseOpen,custId,openCaseStatus
   isASBCaseOpen = false
   openCaseStatus = "Open"
    Call OpenDB()
    Call OpenRs(rs,"C_GetJointTenancyList " & customer_id)'Calling store procedure for checking joint tenancy
    
	if not rs.eof Then
        while not rs.eof
			custId = rs("CUSTOMERID")
			Call OpenRs(CaseRs,"SELECT status.Description FROM ASB_Case c INNER JOIN ASB_CaseStatus status ON c.CaseStatus = status.StatusId INNER JOIN ASB_CaseCustomerBridge ccb ON c.CaseId=ccb.CaseId WHERE ccb.PersonType= 'Customer' AND ccb.PersonId="  & custId)   
			while not CaseRs.eof    
				If CaseRs("Description") =openCaseStatus Then
					isASBCaseOpen = True    
			   End If          
			CaseRs.movenext
			wend 
		rs.movenext            
		wend    
    ELSE 
        custId = customer_id
			Call OpenRs(CaseRs,"SELECT status.Description FROM ASB_Case c INNER JOIN ASB_CaseStatus status ON c.CaseStatus = status.StatusId INNER JOIN ASB_CaseCustomerBridge ccb ON c.CaseId=ccb.CaseId WHERE ccb.PersonType= 'Customer' AND ccb.PersonId="  & custId)   
			while not CaseRs.eof    
				If CaseRs("Description") =openCaseStatus Then
					isASBCaseOpen = True    
			   End If          
			CaseRs.movenext
			wend   
    END IF
    
'End handling ASB management
    
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Customer -- > Customer Relationship Manager</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/CrossBrowserLoading.js"></script>
    <script type="text/javascript" language="JavaScript">
<!--
	
    var MASTER_TENANCY_NUM
    var MASTER_OPEN_WORKORDER
	    MASTER_TENANCY_NUM = <%=tenancy_id%>
		MASTER_OPEN_WORKORDER = ""
	
	// preload image variables -- global
	var open1, closed1, open2, closed2, previous2, open3, closed3, previous3, open4, closed4, previous4, open5
	var closed5, previous5, open6, closed6, previous6, open7, closed7, previous7

	preloadFlag = false;
	function preloadImages2() {
	
		if (document.images) {
			open1 = newImage("Images/1-open.gif");
			closed1 = newImage("Images/1-closed.gif");
			open2 = newImage("Images/2-open.gif");
			closed2 = newImage("Images/2-closed.gif");
			previous2 = newImage("Images/2-previous.gif");
			open3 = newImage("Images/3-open.gif");
			closed3 = newImage("images/3-closed.gif");
			previous3 = newImage("images/3-previous.gif");
			open4 = newImage("images/4-open.gif");
			closed4 = newImage("images/4-closed.gif");
			previous4 = newImage("images/4-previous.gif");
			open5 = newImage("images/5-open.gif");
			closed5 = newImage("images/5-closed.gif");
			previous5 = newImage("images/5-previous.gif");
			open6 = newImage("images/6-open.gif");
			closed6 = newImage("images/6-closed.gif");
			previous6 = newImage("images/6-previous.gif");
			open7 = newImage("images/7-open.gif");
			closed7 = newImage("images/7-closed.gif");
			previous7 = newImage("images/7-previous.gif");
			itemopen = newImage("images/item-1-open.gif");
			itemclosed = newImage("images/item-1-closed.gif");
			newopen = newImage("images/item-2-open.gif");
			newclosed = newImage("images/item-2-previous.gif");
			preloadFlag = true;
			}
	}

	//OPENS THE TENANT STATEMENT
	function OpenStatement(whichOne)
    {
		if (MASTER_TENANCY_NUM == -1)
			alert("Please select a tenancy before viewing a Statement")
		else {
            if (whichOne != -1) {
                if( whichOne == 'Tenant')
                {
                    window.open('/PropertyDataRestructure/Bridge.aspx?pg=RentSummary&customerid=<%=Request("customerid")%>&tenancyid=' + MASTER_TENANCY_NUM, whichOne +'Statement','width=750px,height=500px,scrollbars=yes' )
                }
                else{
                    window.open('/Customer/Popups/' + whichOne + '_Statement.asp?customerid=<%=Request("customerid")%>&tenancyid=' + MASTER_TENANCY_NUM, whichOne +'Statement','width=750px,height=500px,scrollbars=yes')	
                }
            }
        }
	}

	var iFrameArray = new Array("",
								"iInformation",
								"iContact",
								"iTenancy",
								"iHousingBenefit",
								"iSupport",
								"iPayments",
								"iReference",
								"iCustomerJournal",
								"iNewItem",
								"iRentAccount",
								"iRepairJournal",
								"iPlannedWorks",
								"iRepairJournal",
								"iAmendAccount"
								)
	
	var iBlockifNotTenant = new Array ("",
									   "",
									   "",
									   "",
									   "DISABLED",
									   "",
									   "DISABLED",
									   "",
									   "",
									   "",
									   "DISABLED",
									   "DISABLED"
									   )
												

	var MAIN_OPEN_BOTTOM_FRAME = 8
	var MAIN_OPEN_TOP_FRAME = 1	
	// swaps the images on the divs when the user clicks 
	function swap_div(int_which_one, str_where){
		var divid, imgid, upper
		
		if (str_where == 'top'){
			upper = 7; lower = 1;
			STARTLOADER('TOP')
			MAIN_OPEN_TOP_FRAME = int_which_one
			if (MASTER_TENANCY_NUM == -1 && iBlockifNotTenant[int_which_one] == "DISABLED") 
				CRM_TOP_FRAME.location.href = "/customer/iframes/iTopUnavailable.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;			
			else	
				CRM_TOP_FRAME.location.href = "/customer/iframes/" + iFrameArray[int_which_one] + ".asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;			
			}
		else {
			upper = 13;	lower = 8;

			STARTLOADER('BOTTOM')
			MAIN_OPEN_BOTTOM_FRAME = int_which_one
			if (MASTER_TENANCY_NUM == -1 && iBlockifNotTenant[int_which_one] == "DISABLED") 
				CRM_BOTTOM_FRAME.location.href = "/customer/iframes/iBottomUnavailable.asp";			
			else
			    CRM_BOTTOM_FRAME.location.href = "/customer/iframes/" + iFrameArray[int_which_one] + ".asp?customerid=<%=customer_id%>&propertyid=<%=property_id%>&tenancyid="+MASTER_TENANCY_NUM+"&CWO="+MASTER_OPEN_WORKORDER;			
			}
		
		imgid = "img" + int_which_one.toString();
		
		// manipulate images
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
		}
		
		// unless last image in row
		if (int_which_one != 14){		
			if (int_which_one != upper)
				document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
			document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
			}
	}

    function swap_divForTermination(JournalId, NatureId){
		CRM_BOTTOM_FRAME.location.href = "/customer/iframes/iTerminationDetail.asp?journalid="+JournalId+"&natureid="+NatureId+"";		
	}

	function synchronize_tabs(int_which_one, str_where){
		var divid, imgid, upper
		if (str_where == 'top'){upper = 7; lower = 1;}
		else {upper = 13;	lower = 8;}
		
		imgid = "img" + int_which_one.toString();
		for (j = lower ; j <= upper ; j++){
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
			}
		if (int_which_one != upper)
			document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
		
	}
	
	function open_dp(){
		window.open("Popups/pDirectPostings.asp", "display","width=300,height=300,left=100,top=200");
		}

    function open_Reactive_Repair()
	{
	    if (MASTER_TENANCY_NUM == -1) 
			alert("Please select a tenancy before viewing Repairs")
		else

           window.open("../../faultlocator/secure/ReactiveRepair/fl_reactive_repair.aspx?customerid=<%=customer_id %>&tenancyId="+MASTER_TENANCY_NUM, "","width=900,height=600,left=100,top=100,scrollbars=1,resizable=1");
           
	}	
    
	
	function blank(){
		frm_nature.location.href = "blank.asp";
	}
	
	// receives the url of the page to open plus the required width and the height of the popup
	function update_record(str_redir, wid, hig){	
		window.open(str_redir, "display","width="+wid+",height="+hig+",left=100,top=200");	
	}
	
	function forecast(){
		window.showModalDialog("/Customer/popups/forecast_tenancy_end.asp?balance=<%=ACCOUNT_BALANCE%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		//window.open("popups/forecast_tenancy_end.asp?balance=<%=ACCOUNT_BALANCE%>&tenancyid="+MASTER_TENANCY_NUM, "","height=473,width=600");
	}
	
	function post_cash(){
		window.showModalDialog("/Customer/popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: Yes; status: No; scroll: no");
		//window.open("popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 340px; dialogWidth: 440px;");
	}

     function post_card(){
      	
        if (MASTER_TENANCY_NUM == -1)
		{
			    alert("Please select the tenancy.")
                return false;
		}
        else {
        		var width=940;
	            var height=600;
	            var left = parseInt((screen.availWidth/2) - (width/2));
	            var top = parseInt((screen.availHeight/2) - (height/2));

	            window.open("/Customer/Paythru.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM+"&staffid=<%=Session("userid")%>", "","height=" + 600 + ",width=" + 940 + ",resizable=1,status=1,scroll=1,toolbar=1,left=" + left + ",top=" + top + ";");
        }
	}


	function work_order(){
		//window.showModelessDialog("popups/WorkOrder.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
	    
	    // I HAVE TO CHANGE IT TO POP UP BECAUSE THE WARRANTY DIALOGBOX WAS NOT APPEARING IN THE WORKORDER MODELLESSDIALOG
	    // TASK: 5317
	    // MODIFIED BY: ADNAN MIRZA
	    // MODIFIED ON: 7/7/2008
	    if("<%=defectperiodflag%>"==1)	
		{
		    alert("The defect period for the property end at: " + "<%=defectperiodend%>")	    
		}  
	    
        // window.open("../popups/WorkOrder.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","height=510,width=790,status=no,toolbar=no,menubar=no,location=no,scrollbars=no");	
        //window.open("../../faultlocator/secure/faultlocator/report_fault.aspx?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM, "","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,location=no");
	    window.open("../../BRSFaultLocator/Bridge.aspx?cid=<%=customer_id%>&pid=<%=property_id%>", "","dialogHeight: 540px; dialogWidth: 790px; edge: Raised; center: Yes; help: No; resizable=yes,scrollbars=yes,status=no,titlebar=no,toolbar=no,location=no");
        
    }

	function open_pad() {
		window.open("../popups/pScratchPad.asp?tenancyid="+MASTER_TENANCY_NUM, "","height=473,width=600");
	}
	
	function refreshitems() {
		parent.frm_crm.location.href = "iFrames/iCustomerJournal.asp?customerid=<%=customer_id%>"
		//parent.swap_div(8, 'bottom')
	}

	function UpdateMainBalance() {
		BalanceServer.location.href = "/customer/iframes/BalanceServer.asp?customerid=<%=customer_id%>&tenancyid="+MASTER_TENANCY_NUM;					
	}

	function FilterByNature() {
	    swap_div(8, 'bottom');
		CRM_BOTTOM_FRAME.location.href ="iFrames/iCustomerJournal.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>&Nature=" + RSLFORM.sel_NATURE.value
	}

	function goGasServicingDetail(propertyid, Journalid){
		location.href = "/Portfolio/PRM.asp?PropertyID=" + propertyid + "&gs_journalid=" + Journalid
	}

// -->
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();" onunload="macGo()" class="ta">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="img1" id="img1" title='Customer Information' src="images/1-open.gif" width="92"
                    height="20" border="0" onclick="swap_div(1, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img2" id="img2" title='Contact Details' src="images/2-previous.gif" width="71"
                    height="20" border="0" onclick="swap_div(2, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img3" id="img3" title='Tenancy Information' src="images/3-closed.gif"
                    width="75" height="20" border="0" onclick="swap_div(3, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img4" id="img4" title='Housing Benefit Details - If Appropriate' src="images/4-closed.gif"
                    width="118" height="20" border="0" onclick="swap_div(4, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img5" id="img5" title='Tenant Support Information' src="images/5-closed.gif"
                    width="132" height="20" border="0" onclick="swap_div(5, 'top')" style="cursor: pointer"
                    alt="" />
            </td>
            <td rowspan="2">
                <img name="img6" id="img6" title='Payment Details' src="images/6-closed.gif" width="134"
                    height="20" border="0" onclick="swap_div(6, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2">
                <img name="img7" id="img7" title='References' src="images/7-closed.gif" width="99"
                    height="20" border="0" onclick="swap_div(7, 'top')" style="cursor: pointer" alt="" />
            </td>
            <td>
                <img src="images/spacer.gif" width="29" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#004376">
                <img src="images/spacer.gif" width="29" height="1" alt="" />
            </td>
        </tr>
    </table>
    <div id="TOP_DIV" style="display: block; overflow: hidden">
        <iframe frameborder="0" name="CRM_TOP_FRAME" <%=TABLE_DIMS%> src="iFrames/iInformation.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>"
            style="overflow: hidden"></iframe>
        <br />
    </div>
    <div id="TOP_DIV_LOADER" style="display: none; overflow: hidden; width: 750; height: 200"
        <%=TABLE_DIMS%>>
        <table width="750" style="border-right: solid 1px #133e71; height: 180px" cellpadding="1"
            cellspacing="2" border="0" class="TAB_TABLE">
            <tr>
                <td width="70%" height="100%">
                    <table width="100%" style="height: 100%; border: solid 1px #133e71; border-collapse: collapse"
                        cellpadding="3" cellspacing="0" border="0" class="TAB_TABLE">
                        <tr>
                            <td width="17px">
                            </td>
                            <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                                <div id="LOADINGTEXT_TOP">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="30%" height="100%" valign="top">
                    <%=str_repairs%>
                </td>
            </tr>
        </table>
    </div>
    <table width="550" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <div>
                <!-- code changes start by TkXel. Line 373 to 385 written by adeel on june 30 2010============================================= -->
                <%if isCaseOpen = true Then%>
                <input type="button" value="Arrears Case Open" class="RSLButtonSmall" onclick="window.open('../RSLArrearsManagement/Bridge.aspx?cmd=rsl&caseid=<%=case_id%>&tenid=<%=ten_id%>&customerid=<%=customer_id%>','New');" />
                <% end if%>
                <!-- code updated by umair 26 April 2012============================================ -->
                <%if isPreviousCase = true THEN%>
                <input type="button" value="Arrears Case Closed" class="RSLButtonSmall" onclick="window.open('../RSLArrearsManagement/Bridge.aspx?cmd=rsl&caseid=<%=prev_case_id%>&tenid=<%=ten_id%>&customerid=<%=customer_id%>&casetype=prev','New');" />
                <%end if%>
                <%if isASBCaseOpen = true Then%>
                <input type="button" value="ASB Case Open" class="RSLButtonSmall" onclick="window.open('../../RSLASBModuleWeb/Cases/CustomerPreviousCases/?UserId=<%=Session("userid")%>&pid=<%=customer_id%>','New');" />
                <% end if%>
            </div>
            <%

		  ' This blcok gets exectued when request NOT came from Customer Enquiry Management Area	
		  If enqID ="" OR ISNULL(enqID) OR Len(enqID) <= 0 Then
            %>
            <td rowspan="2" valign="bottom">
                <img name="img8" id="img8" src="images/8-open.gif" width="54" height="20" border="0"
                    onclick="swap_div(8, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img9" id="img9" src="images/9-previous.gif" width="82" height="20" border="0"
                    onclick="swap_div(9, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img10" id="img10" src="images/10-closed.gif" width="77" height="20" border="0"
                    onclick="swap_div(10, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <%
		  Else
		  ' This block gets exectued when requestd initiated by Customer Enquiry Management Area	(create CRM button)
            %>
            <td rowspan="2" valign="bottom">
                <img name="img8" id="img8" src="images/8-closed.gif" width="54" height="20" border="0"
                    onclick="swap_div(8, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img9" id="img9" src="images/9-open.gif" width="82" height="20" border="0"
                    onclick="swap_div(9, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <td rowspan="2" valign="bottom">
                <img name="img10" id="img10" src="images/10-previous.gif" width="77" height="20"
                    border="0" onclick="swap_div(10, 'bottom')" style="cursor: pointer" alt="" />
            </td>
            <%
  		  End If
            %>
            <!-- code changes end by TkXel============================================================================ -->
            <!-- [9997] Hide Tab -- TkXel -->
            <!--<td rowspan="2" valign="bottom" hidden="hidden">
                <img name="img11" id="img11" src="images/11-closed.gif" width="120" height="20" border="0"
                    onclick="swap_div(11, 'bottom')" style="cursor: pointer" alt="" />
            </td>-->
            <!-- [9997] Hide Tab -- TkXel -->
            <!--<td rowspan="2" valign="bottom" hidden="hidden">
                <img name="img12" id="img12" src="images/12-closed.gif" width="74" height="20" border="0"
                    onclick="swap_div(12, 'bottom')" style="cursor: pointer" alt="" />
            </td>-->
            <img name="img11" id="img11" src="" style="display: none;" />
            <img name="img12" id="img12" src="" style="display: none;" />
            <td rowspan="2" valign="bottom">
                <img name="img13" id="img13" src="images/13-closed.gif" width="79" height="20" border="0"
                    style="cursor: pointer" alt="" />
            </td>
            <td align="right">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right">
                            <b><font color="#133e71" style="font-size: 14px">Balance:&nbsp;</font></b>
                            <input type="text" value='<%=ACCOUNT_BALANCE%>' size="8" readonly="readonly" name='TenantBalance'
                                style="border: 1px solid black; font-size: 14px; color: blue; font-weight: bold;
                                text-align: right" />
                        </td>
                    </tr>
                    <tr>
                        <td width="9%" align="right">
                            <%=selNature%>
                        </td>
                        <td width="2%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#004376" colspan="2">
                <img src="images/spacer.gif" width="458" height="1" alt="" />
            </td>
        </tr>
    </table>
    <div id="BOTTOM_DIV" style="display: block; overflow: hidden">
        <table width="750" style="border-right: 1px solid #133e71; height: 210px" cellpadding="1"
            cellspacing="0" border="0" class="TAB_TABLE">
            <tr>
                <!-- code changes start by TkXel=============================================================================== -->
                <%
		    If enqID ="" OR ISNULL(enqID) OR Len(enqID) <= 0 Then
            ' when request NOT came from Enquiry Management Area, Bottom Frame src will display iframes/iCustomerJournal.asp			
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iCustomerJournal.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%
		  Else
		  	    If natureID = "" OR ISNULL(natureID) OR Len(natureID) <= 0 Then
              	    ' when request came from Enquiry Management Area(Create CRM button), Bottom Frame src will display iframe/iNewItem.asp			
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iNewItem.asp?EnquiryID=<%=enqID%>" width="100%"
                        height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%	Else
			    ' When Request came from Response Management Area
				    Select Case natureID
					    Case 50
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iTerminationDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%
					    Case 51
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iServiceComplaintDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%
					    Case 52
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iGarageCarParkingDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 53
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iASBdetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 54
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iAbandonedPropertyVehicleDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 55
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iHouseMoveDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 56
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iArrearDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 57
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iGeneralDetail.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%		
					    Case 58
                %>
                <td>
                    <iframe name="CRM_BOTTOM_FRAME" src="iFrames/iCstRequestDetails.asp?journalid=<%= journalID %>&natureid=<%= natureID %>"
                        width="100%" height="100%" frameborder="0" style="border: block"></iframe>
                </td>
                <%
				    End Select

                %>
                <%    End If
                %>
                <%
		  End If
                %>
                <!-- code changes end by TkXel=============================================================================== -->
            </tr>
        </table>
    </div>
    <div id="BOTTOM_DIV_LOADER" style="display: block; overflow: hidden; width: 750;
        height: 210; width: 750">
        <table width="750" style="border-right: solid 1px #133e71; height: 210" cellpadding="10"
            cellspacing="10" border="0" class="TAB_TABLE">
            <tr>
                <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">
                    <div id="LOADINGTEXT_BOTTOM">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" name="customerid" id="customerid" value="<%=customer_id%>" />
    <input type="hidden" name="propertyid" id="propertyid" value="<%=property_id%>" />
    </form>
    <iframe name="BalanceServer" id="BalanceServer" style="display: none" src="iFrames/BalanceServer.asp?customerid=<%=customer_id%>&tenancyid=<%=tenancy_id%>">
    </iframe>
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
    <!--#include virtual="includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
