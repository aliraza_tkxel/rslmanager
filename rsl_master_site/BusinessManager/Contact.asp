<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
If (EmployeeID = "") Then EmployeeID = -1 End If

Call OpenDB()

SQL = "SELECT * FROM E__EMPLOYEE WHERE E__EMPLOYEE.EMPLOYEEID = " & EmployeeID

Call OpenRs(rsName, SQL)
If (NOT rsName.EOF) Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")
	l_imagefile = rsName("IMAGEPATH")
	l_rolefile = rsName("IMAGEPATH")

	If isnull(l_imagefile) Or l_imagefile = "" Then
		l_img_display = "none"
	Else
		l_img_display = "block"
		l_imagefile = "/EmployeeImage/" & l_imagefile
	End If

	If isnull(l_rolefile) Or l_rolefile = "" Then
		l_img_display = "none"
	Else
		l_role_display = "block"
		l_rolefile = "/RoleFile/" & l_rolefile
	End If

	FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"

End If
Call CloseRS(rsName)

SQL = "SELECT * FROM E_CONTACT WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsLoader, SQL)

If (NOT rsLoader.EOF) Then

	' tbl E_CONTACT
	l_address1 = rsLoader("ADDRESS1")
	l_address2 = rsLoader("ADDRESS2")
	l_address3 = rsLoader("ADDRESS3")
	l_postaltown = rsLoader("POSTALTOWN")
	l_county = rsLoader("COUNTY")
	l_postcode = rsLoader("POSTCODE")
	l_hometel = rsLoader("HOMETEL")
	l_mobile = rsLoader("MOBILE")
	l_workdd = rsLoader("WORKDD")
	l_workext = rsLoader("WORKEXT")
	l_homeemail = rsLoader("HOMEEMAIL")
	l_workemail = rsLoader("WORKEMAIL")
	l_emergencycontactname = rsLoader("EMERGENCYCONTACTNAME")
	l_emergencycontacttel = rsLoader("EMERGENCYCONTACTTEL")
	l_emergencyinfo = rsLoader("EMERGENCYINFO")
	l_workmobile = rsLoader("WORKMOBILE")

	ACTION = "AMEND"

End If

Call CloseRs(rsLoader)

 Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED @EmployeeId = " & EmployeeID
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|Y"
        FormFields[1] = "txt_ADDRESS2|Address 2|TEXT|N"
        FormFields[2] = "txt_ADDRESS3|Address 3|TEXT|N"
        FormFields[3] = "txt_POSTALTOWN|Postal Town|TEXT|N"
        FormFields[4] = "txt_COUNTY|County|TEXT|N"
        FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|Y"
        FormFields[6] = "txt_HOMETEL|Home Telephone|TELEPHONE|N"
        FormFields[7] = "txt_MOBILE|Mobile|TELEPHONE|N"
        FormFields[8] = "txt_WORKDD|Work DD|TELEPHONE|N"
        FormFields[9] = "txt_WORKEXT|Work Ext|TELEPHONE|N"
        FormFields[10] = "txt_HOMEEMAIL|Home Email|EMAIL|N"
        FormFields[11] = "txt_WORKEMAIL|Work Email|EMAIL|N"
        FormFields[12] = "txt_EMERGENCYCONTACTNAME|Emergency Contact Name|TEXT|N"
        FormFields[13] = "txt_EMERGENCYCONTACTTEL|Emergency Contact Tel|TELEPHONE|N"
        FormFields[14] = "txt_EMERGENCYINFO|Emergency Additional Info|TEXT|N"
        FormFields[15] = "txt_WORKMOBILE|Work Mobile|TELEPHONE|N"


        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.submit()
        }

        function open_window(url, width, height) {
            window.open(url, 'mywindow', 'width=' + width + ',height=' + height + ',toolbar=no, location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=no, resizable=yes')
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img name="E_PD" id="E_PD" src="Images/1-closed.gif" width="115" height="20" border="0"
                        alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" id="E_C" src="Images/2-open.gif" width="74" height="20" border="0"
                        alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" id="E_SQ" src="Images/3-previous.gif" width="94" height="20" border="0"
                        alt="Qualifications" /></a>
            </td>
            <td rowspan="2" style="display: none">
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" id="E_PE" src="images/4-closed.gif" width="86" height="20" border="0"
                        alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" id="E_DD" src="Images/5-closed.gif" width="70" height="20" border="0"
                        alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" id="E_JD" src="Images/6-closed.gif" width="52" height="20" border="0"
                        alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" id="E_B" src="images/7-closed.gif" width="82" height="20" border="0"
                        alt="Benefits" /></a>
            </td>
            <td width="265" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" /></td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <form name="RSLFORM" method="post" action="ServerSide/Contact_svr.asp" style="margin:0px">
    <table cellspacing="2" style="height:90%; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td nowrap="nowrap">
                Address 1:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1" maxlength="50" value="<%=l_address1%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap" width="85px">
                Work DD:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKDD" id="txt_WORKDD" maxlength="20" value="<%=l_workdd%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKDD" id="img_WORKDD" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 2:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2" maxlength="40" value="<%=l_address2%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td>
                Work Ext:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKEXT" id="txt_WORKEXT" maxlength="20" value="<%=l_workext%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKEXT" id="img_WORKEXT" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td width="90px">
                Address 3:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS3" id="txt_ADDRESS3" maxlength="50" value="<%=l_address3%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS3" id="img_ADDRESS3" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td>
                Home Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_HOMEEMAIL" id="txt_HOMEEMAIL" maxlength="120" value="<%=l_homeemail%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMEEMAIL" id="img_HOMEEMAIL" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Postal Town:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTALTOWN" id="txt_POSTALTOWN" maxlength="40" value="<%=l_postaltown%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTALTOWN" id="img_POSTALTOWN" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td>
                Work Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKEMAIL" id="txt_WORKEMAIL" maxlength="120" value="<%=l_workemail%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKEMAIL" id="img_WORKEMAIL" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td>
                County:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" maxlength="30" value="<%=l_county%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10" value="<%=l_postcode%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td nowrap="nowrap">
                Emergency Contact Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMERGENCYCONTACTNAME" id="txt_EMERGENCYCONTACTNAME" maxlength="30"
                    value="<%=l_emergencycontactname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMERGENCYCONTACTNAME" id="img_EMERGENCYCONTACTNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Home Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_HOMETEL" id="txt_HOMETEL" maxlength="20" value="<%=l_hometel%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMETEL" id="img_HOMETEL" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                Emergency Contact Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMERGENCYCONTACTTEL" id="txt_EMERGENCYCONTACTTEL" maxlength="40"
                    value="<%=l_emergencycontacttel%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMERGENCYCONTACTTEL" id="img_EMERGENCYCONTACTTEL" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Mobile:(BCP)
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_MOBILE" id="txt_MOBILE" maxlength="30" value="<%=l_mobile%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Emergency Additional Info :
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMERGENCYINFO" id="txt_EMERGENCYINFO" maxlength="40" value="<%=l_emergencyINFO%>"
                    tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMERGENCYINFO" id="img_EMERGENCYINFO" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <!--<TD colspan=4></TD>-->
            <td>
                Work Mobile:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_WORKMOBILE" id="txt_WORKMOBILE" maxlength="30" value="<%=l_workmobile%>"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_WORKMOBILE" id="img_WORKMOBILE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td colspan="1">
            </td>
            <td align="right">
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <br />
                <br />
                <input type="button" name="BtnSaveNext" id="BtnSaveNext" class="RSLButton" value=" SAVE & NEXT " title=" SAVE & NEXT " onclick="SaveForm()" style="cursor:pointer" />
                <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />                
            </td>
        </tr>
        <tr>
            <!--<TD colspan=4></TD>-->
            <td valign="top">
                Employee Image:
            </td>
            <td>
                <img id="imgEmployee" style="display: <%=l_img_display%>" align="right" src="<% = l_imagefile %>"
                    alt="Employee Image" />
                <a href="#" onclick="open_window('/BusinessManager/Popups/EmployeeImage.aspx?empid=<%=EmployeeID%>','400','200')">
                    Upload Image</a>
            </td>
            <td>
            </td>
            <td colspan="1">
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <!--<TD colspan=4></TD>-->
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td colspan="1">
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td height="100%">
                &nbsp;
            </td>
        </tr>
    </table>
    
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
   
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>