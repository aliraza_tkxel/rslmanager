<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match
	Dim TDFunc         (3)	 'stores any functions that will be applied

	Dim ActiveEmployee, l_ActiveEmployee, ActiveEmployeeSQL
    Dim searchName, searchSQL

	l_ActiveEmployee = Request("ActiveEmployee")
	ActiveEmployee = Request("ActiveEmployee")
	searchName = Replace(Request("name"),"'","''")

	If ActiveEmployee = "No" Then
		ActiveEmployeeSQL = " j.active = 0 AND "
	Else
		ActiveEmployeeSQL = " j.active = 1 AND "
	End If

	If searchName <> "" Then
		searchSQL = " AND (FIRSTNAME LIKE '%" & searchName & "%' OR LASTNAME LIKE '%" & searchName & "%') "
	Else
		searchSQL = ""
	End If

	ColData	(0) = "Name|FULLNAME|190"
	SortASC	(0) = "LASTNAME ASC, FIRSTNAME ASC"
	SortDESC(0) = "LASTNAME DESC, FIRSTNAME DESC"
	TDSTUFF	(0) = ""
	TDFunc	(0) = ""

	ColData	(1) = "Team|TEAM|190"
	SortASC	(1) = "TEAM ASC"
	SortDESC(1) = "TEAM DESC"
	TDSTUFF	(1) = ""
	TDFunc	(1) = ""

	ColData	(2) = "Job Title|JOBTITLE|190"
	SortASC	(2) = "JOBTITLE ASC"
	SortDESC(2) = "JOBTITLE DESC"
	TDSTUFF	(2) = ""
	TDFunc	(2) = ""

	ColData	(3) = "Grade|GRADE|80"
	SortASC	(3) = "J.GRADE ASC"
	SortDESC(3) = "J.GRADE DESC"
	TDSTUFF	(3) = ""
	TDFunc	(3) = ""

	PageName = "Employees.asp"
	EmptyText = "No employees found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""onclick=""""load_me("" & rsSet(""EMPLOYEEID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
	Call SetSort()

	SQLCODE =	"SELECT FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
			"			ISNULL(T.TEAMNAME, 'No Team') AS TEAM, " &_
			"			E.EMPLOYEEID, " &_
			"			ISNULL(ET.JobeRoleDescription, 'N/A') AS JOBTITLE, " &_
			"			ISNULL(G.DESCRIPTION, null) AS GRADE " &_
			"	FROM E__EMPLOYEE E " &_
			"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
			"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_
			"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID " &_
			"			LEFT JOIN E_JOBROLE ET ON ET.JobRoleId = J.JobRoleId " &_
			" WHERE 	" & ActiveEmployeeSQL & ActiveAlertSQL & " (J.TEAM <> 1 OR J.TEAM IS NULL) " & searchSQL &_
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

	Call Create_Table()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Employee Search for Profile</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

    function load_me(employee_id){
	<% If Request.Querystring("alerts") = "true" Then %>
    location.href = "employeealert.asp?employeeid=" + employee_id;
    <% Else %>
	location.href = "newemployee.asp?employeeid=" + employee_id;
	<% End If %>
	}

	function quick_find(){
	<% If Request.Querystring("alerts") = "true" Then %>
	    location.href = "Employees.asp?name="+document.getElementById("findemp").value + "&alerts=true";
	<% Else %>
        location.href = "Employees.asp?name="+document.getElementById("findemp").value + "&ActiveEmployee=<%=l_ActiveEmployee%>";
	<% End If %>
	}

	function setActiveFilter(isActive){
	document.getElementById("ActiveEmployee").value = isActive;
	<% If Request.Querystring("alerts") = "true" Then %>
	location.href = "employees.asp?alerts=true&ActiveEmployee=" + isActive;
	<% Else %>
	location.href = "employees.asp?ActiveEmployee=" + isActive;
	<% End If %>
	}

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_employees" src="images/tab_employees.gif" width="94" height="20" border="0"
                    alt="Employees" />
            </td>
            <!--td><img src="images/spacer.gif" width="656" height="19"></td-->
            <td align="right">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right">
                            <input type="hidden" name="ActiveEmployee" id="ActiveEmployee" value="<%=ActiveEmployee%>" /><a
                                onclick="setActiveFilter('Yes')" style="cursor: pointer"  title="Active Employees">Active</a> | <a onclick="setActiveFilter('No')"
                                    style="cursor: pointer" title="Inactive Employees">Inactive</a>
                            <input type="button" name="btn_FIND" id="btn_FIND" title="Search for matches using both first and surname"
                                class="RSLBUTTON" value="Quick Find" onclick="quick_find()" style="cursor:pointer" />
                            &nbsp;<input type="text" size="18" name="findemp" id="findemp" class="textbox200" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="650" height="1" alt="" /></td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
