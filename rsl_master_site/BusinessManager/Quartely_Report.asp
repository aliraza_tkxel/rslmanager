<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	PageName = "Establishment_report.asp"
	// only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	// redirect all director to a higher level showing their teams
	//if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, fromDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL, EMPSQL
	Dim PrevSelected, allSelected, EmployeeType,OrderBy 
	GetVariables()
	
	getTeams()
	opendb()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM  WHERE TEAMID NOT IN (1,93) AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	GetDepartmentReportVars()
	closedb()
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()
		PrevSelected =  ""
		AllSelected = ""
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
        fromDate = Request.QueryString("fromDate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")
		
		If OrderBy <> "" then
			orderSQL =  " ORDER BY  " & OrderBy
		else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End if
		
		
		
		If Dapartment <> "" then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End if
		
		If theDate <> "" then
			whereNeeded = 1
			if deptSQL <> "" then
			dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End if

        If fromDate <> "" then
			whereNeeded = 1
            if deptSQL <> "" then
			fromdateSQL = fromdateSQL & " AND "
            end if
			fromdateSQL =  fromdateSQL & " (J.ENDDATE IS NULL) OR J.ENDDATE >=  '" & fromDate & "' "

		End if
		
		if whereNeeded = 1 then
			whereSQL = " AND " & deptSQL & fromdateSQL
		End If
		
		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate & "&fromdate=" & fromDate & "&EmployeeType=" & EmployeeType
	
	End Function
	
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	CASE WHEN LEN(ISNULL(J.JOBTITLE, 'N/A')) > 12   " &_
 						"		THEN LEFT(ISNULL(J.JOBTITLE, 'N/A'), 14) +  '...'   " &_
						"		ELSE  ISNULL(J.JOBTITLE, 'N/A')   " &_
        				"	END AS JOBTITLEshort, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(EG.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE,J.FTE,PT.DESCRIPTION " &_
						"FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	lEFT JOIN E_GRADE EG ON EG.GRADEID=CAST(J.GRADE AS INT) " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
                        "	LEFT JOIN E_PARTFULLTIME PT ON PT.PARTFULLTIMEID = J.PARTFULLTIME " &_ 

						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL& orderSQL 
						
		'"	LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND JNAL.CURRENTITEMSTATUSID = 5 AND JNAL.ITEMNATUREID = 2 " &_

		'rw strSQL
        'response.end
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				'& rsSet("JOBTITLE")
				'& rsSet("ETHNICITY")
				str_data = str_data & "<TBODY><TR STYLE='CURSOR:HAND' ONCLICK='load_me(" & rsSet("EMPLOYEEID") & ")'>" &_
					"<TD WIDTH=120PX>" & rsSet("FULLNAME") & "</TD>" &_
					"<TD WIDTH=100PX ALIGN=LEFT title='" & rsSet("JOBTITLE") & "'>" & rsSet("JOBTITLEshort") & "</TD>" &_
					"<TD WIDTH=65PX ALIGN=CENTER>" & rsSet("DEPARTMENT") & "</TD>" &_
					"<TD WIDTH=0PX ALIGN=CENTER>" & rsSet("GENDER") & "</TD>"&_
					"<TD WIDTH=0PX ALIGN=CENTER>" & rsSet("ETHNICITY")  & "</TD>" &_
					"<TD WIDTH=70PX ALIGN=CENTER>" & rsSet("DOB") & "</TD>"&_
					"<TD WIDTH=0PX ALIGN=CENTER>" & rsSet("SALARY") & "</TD>"&_
					"<TD WIDTH=38PX ALIGN=CENTER>" & rsSet("GRADE") & "</TD>"&_
                    "<TD WIDTH=80PX ALIGN=CENTER>" & rsSet("DESCRIPTION") & "</TD>"&_
                    "<TD WIDTH=38PX ALIGN=CENTER>" & rsSet("FTE") & "</TD>"&_
					"<TD WIDTH=75PX ALIGN=CENTER>" & rsSet("STARTDATE") & "</TD>"&_
					"<TD WIDTH=70PX ALIGN=CENTER>" & rsSet("ENDDATE") & "</TD>"

					str_data = str_data & "</TR></TBODY>"
				
					
				
				count = count + 1			
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=12 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'Quartely_Report.asp?bypass=1&page=1&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring &"'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'Quartely_Report.asp?bypass=1&page=" & prevpage & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='Quartely_Report.asp?bypass=1&page=" & nextpage & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='Quartely_Report.asp?bypass=1&page=" & intPageCount & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring &"'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"
			
		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<TFOOT><TR><TD COLSPAN=12 STYLE='FONT:16PX' ALIGN=CENTER>No records found for this date !!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR></TFOOT>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
									
			'Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
		

		
	End function


		// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=12 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	Function GetDepartmentReportVars()
	
				sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
						"			SUM(	CASE WHEN J.STARTDATE >= '" & fromDate & "' and  J.STARTDATE <= '" & theDate & "'  " &_
						"				THEN 1    " &_
						"				ELSE  0    " &_
						"    		END )AS STARTERS, " &_
						"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS MALETOTAL, " &_
						"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS FEMALETOTAL, " &_
						"			SUM(	CASE WHEN J.ENDDATE >= '" & fromDate & "' and J.ENDDATE <= '" & theDate & "' " &_ 
						"				THEN 1    " &_ 
						"				ELSE  0   " &_
						"    		END )AS LEAVERS " &_
						"FROM	E__EMPLOYEE E   " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL
						
			Call OpenRs(rsLoader, SQL)
				if (NOT rsLoader.EOF) then
			
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
						TotalSalary = FormatCurrency(rsLoader("SALARY"))
						TotalStaff		=	rsLoader("TOTALSTAFF")
						maletotal		=	rsLoader("MALETOTAL")
						femaletotal		=	rsLoader("FEMALETOTAL")
						NewStarters			=	rsLoader("STARTERS") 						
						Leavers				=	rsLoader("LEAVERS") 
				'==================================================================
				'= End Totals For department report
				'==================================================================	
				end if
			CloseRS(rsLoader)
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<TITLE>RSL Manager Business --> HR Tools --> Team</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
        .style1
        {
            width: 106px;
        }
        .RSLButton
        {
            margin-left: 107px;
        }
      
    </style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

	var FormFields = new Array()
	FormFields[0] = "txt_theDate|Date - As At |DATE|N"
	FormFields[1] = "sel_TEAM|Department|INT|N"
	
	function load_me(employee_id){
		
			location.href = "../myjob/erm.asp?employeeid=" + employee_id;
		
		}
	function quick_find(){
	
		location.href = "man_team.asp?name="+thisForm.findemp.value;
		
	}
	
	function BuildReport() {

		if (!checkForm()) return false;
		location.href = "Quartely_Report.asp?DEPT=" + thisForm.sel_TEAM.value + "&theDate=" + thisForm.txt_theDate.value + "&fromDate=" + thisForm.txt_fromDate.value
			
	}
	
	function PrintMe(){
	    location.href = "Quartely_Report.asp?DEPT=" + thisForm.sel_TEAM.value + "&theDate=" + thisForm.txt_theDate.value + "&fromDate=" + thisForm.txt_fromDate.value
	}
	
	function XLsMe(){

	    location.href = "Quartely_Report_xlsversion.asp?DEPT=" + thisForm.sel_TEAM.value + "&theDate=" + thisForm.txt_theDate.value + "&fromDate=" + thisForm.txt_fromDate.value + "&cc_sort=<%=orderby%>"
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
<form name = thisForm method=post>
  <table border="0" width=1010 cellpadding=2 cellspacing=2 height=40PX style='BORDER:1PX SOLID #133E71' bgcolor=beige>
    <tr>
     <!-- <td width="109" NOWRAP><b>From Date :</b></td>
      <td width="109" NOWRAP><b>To Date :</b></td>
      <td width="185"><b>Department :</b></td>
      <td width="584"><b>Department Information :</b></td>-->
    </tr>
    <tr>
      <td valign="top" NOWRAP>
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="style1"><strong>From : </strong></td>
            <td valign="top" NOWRAP><input type=text name="txt_fromDate" class="textbox" maxlength="10" style='width:100px;text-align:right' value="<%= fromDate %>"><img src="/js/FVS.gif" name="img_fromDate" width="15px" height="15px" border="0"></td>
          </tr>
          <tr>
            <td class="style1"><strong>To : </strong></td>
            <td valign="top" NOWRAP><input type=text name="txt_theDate" class="textbox" maxlength="10" style='width:100px;text-align:right' value="<%= theDate %>"><img src="/js/FVS.gif" name="img_theDate" width="15px" height="15px" border="0"></td>
          </tr>
          <tr>
            <td class="style1"><strong>Department : </strong></td>
            <td><%=selTeam%><img src="/js/FVS.gif" name="img_TEAM" width="15px" height="15px" border="0"> </td>
          </tr>
          
          <tr>
            <td height="5" class="style1"></td>
          </tr>
          <tr>
            <td colspan="2"><input name="Submit" type="button" class="RSLButton" onClick="BuildReport()" value="Build Report"></td>
          </tr>
        </table>
        <br>
      <td NOWRAP>
	  <div name="DeptInfo" id="DeptInfo">
	  	<table width="354"  border="0" cellpadding="0" cellspacing="0" style='BORDER:1PX SOLID #133E71'>
			  <tr>
			    <td colspan="5" height="10"></td>
		    </tr>
			  <tr>
			    <td width="12">&nbsp;</td>
			    <td width="88"><strong>Total Salary : </strong></td>
			    <td width="117"><%= TotalSalary %></td>
			    <td width="94"><strong>New Starters : </strong></td>
			    <td width="41"><%= NewStarters %></td>
			  </tr>
			   <tr>
			    <td colspan="5" height="10"></td>
		    </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td><strong>Total Staff : </strong></td>
			    <td><%= TotalStaff %> ( M: <%=maletotal%> / f: <%= femaletotal %> ) </td>
			    <td><strong>Leavers : </strong></td>
			    <td><%= Leavers %></td>
			  </tr>
			  <tr>
			    <td height="10" colspan="5"></td>
		    </tr>
		</table>
	</div>
	  </td>
    </tr>
  </table>
  <br>
  <TABLE WIDTH=1010 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=2 valign="bottom"><IMG NAME="tab_establishment" TITLE='<%=team_name%>' SRC="Images/tab_establishmentreport.gif" WIDTH=157 HEIGHT=20 BORDER=0></TD>
      <!--TD><IMG SRC="images/spacer.gif" WIDTH=656 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT><TABLE WIDTH=600 CELLPADDING=0 CELLSPACING=0>
          <TR>
            <TD width="72%" height="1" ALIGN=RIGHT>CONVERT TO PRINT FORMAT </TD>
            <TD width="5%" ALIGN=RIGHT><a href="Quartely_Report_Print.asp?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE desc")%><%=DEFAULTstring%>" target="_blank"><img src="../myImages/PrinterIcon.gif" alt="Print Arrears List" width="31" height="20" border="0" style="Cursor:hand" ></a></TD>
            <TD width="18%" ALIGN=RIGHT>CONVERT TO XLS </TD>
            <TD width="5%" ALIGN=RIGHT><img src="../customer/Images/excel.gif" alt="Excel Arrears List" width="29" height="29" border="0" style="Cursor:hand" onClick="XLsMe()"></TD>
          </TR>
        </TABLE></TD>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71 colspan=4><IMG SRC="images/spacer.gif" WIDTH=740 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=1010 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' COLSPAN=6> <B>Report : Establishment </B> </TD>
      <TD WIDTH=40 rowspan=2 ALIGN=left valign=bottom  CLASS='NO-BORDER'> <B>Salary</B></TD>
      <TD ALIGN=left WIDTH=78 CLASS='NO-BORDER' rowspan=2 valign=bottom>
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>
	  <B>Grade</B>
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>
	  </TD>
    </TR>
    <TR align="left" valign="bottom">
      <TD WIDTH=96 CLASS='NO-BORDER'><b>Name</b> </TD> 
      <TD WIDTH=101 CLASS='NO-BORDER'><b><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.JOBTITLEshort desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>Job Title<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.JOBTITLEshort  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a></b> </TD>
      <TD WIDTH=86 CLASS='NO-BORDER'>	  
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.DEPARTMENT desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>
	  <B>Dept.</B>
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.DEPARTMENT  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>
		</TD>
      <TD WIDTH=83 CLASS='NO-BORDER'><strong><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GENDER DESC")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>Gender<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GENDER ASC")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a></strong></TD>
      <TD WIDTH=92 CLASS='NO-BORDER'><strong><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ETHNICITY desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>Ethnicity<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ETHNICITY  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a></strong></TD>
      <TD WIDTH=63 CLASS='NO-BORDER'> <B><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("DOB desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>DOB</B><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("DOB  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a> </TD>
      <TD WIDTH=80 CLASS='NO-BORDER'> <B>Full/Part </B> </TD>
      <TD WIDTH=32 CLASS='NO-BORDER'> <B>FTE</B> </TD>
      <TD WIDTH=108 CLASS='NO-BORDER'>	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.STARTDATE desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>
	  <B>Start Date</B>
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.STARTDATE  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>
</TD>
      <TD WIDTH=89 CLASS='NO-BORDER'> <B>End Date </B> </TD>
    </TR>
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=12 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    </THEAD> <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

