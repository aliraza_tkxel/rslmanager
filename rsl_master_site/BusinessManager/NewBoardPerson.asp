<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim lstTitles
Dim lstEthnicities
Dim lstMaritalStatus
Dim l_sortcode
	l_sortcode = ""
Dim FullName
FullName = "<font color=""red"">New Member</font>"

Dim rsEmp, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
If (EmployeeID = "") Then EmployeeID = -1 End If

SQL = " SELECT	T.TITLEID AS TITLE, " &_
			"	ISNULL(E.FIRSTNAME,'')   AS FIRSTNAME, " &_
			"	ISNULL(E.LASTNAME,'')    AS LASTNAME, " &_
			"	ISNULL(A.ADDRESS1,'') AS ADDRESS1, " &_
			"	ISNULL(A.ADDRESS2,'') AS ADDRESS2 ," &_
			"	ISNULL(A.POSTALTOWN,'')  AS TOWN, " &_
			"	ISNULL(A.COUNTY,'')      AS COUNTY, " &_
			"	ISNULL(A.POSTCODE,'')    AS POSTCODE, " &_
			"	ISNULL(A.WORKDD,'N/A')      AS OFFICETEL, " &_
			"	ISNULL(A.WORKEXT,'N/A')     AS WORKEXT, " &_
			"	ISNULL(A.HOMETEL,'N/A')     AS HOMETEL, " &_
			"	ISNULL(A.MOBILE,'N/A')      AS MOBILE, " &_
			"	ISNULL(A.HOMEEMAIL,'N/A')   AS HOMEEMAIL, " &_
			"	ISNULL(A.WORKEMAIL,'N/A')   AS WORKEMAIL, " &_
			"	ISNULL(J.JOBTITLE,'N/A')    AS POSITION, " &_
			"	ISNULL(J.STARTDATE,'')   AS DATECOMMENCED, " &_
			"	J.ACTIVE as EMPLOYMENTSTSTUS , " &_
			"	DATEADD(YEAR, 3, J.STARTDATE) AS REVIEW, " &_
			"	case WHEN J.ACTIVE = 1  then 'ACTIVE' else 'INACTIVE' end EMPL_STATUS " &_
			" FROM E__EMPLOYEE E " &_
			"	LEFT JOIN G_TITLE T ON T.TITLEID = E.TITLE " &_
			"	LEFT JOIN E_CONTACT A ON A.EMPLOYEEID = E.EMPLOYEEID " &_
			"	LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
			" WHERE E.EMPLOYEEID = " & EmployeeID
Call OpenDB()
Call OpenRs(rsLoader, SQL)

If (NOT rsLoader.EOF) Then

	l_Title		 		= rsLoader("TITLE")
	l_FirstName			= rsLoader("FIRSTNAME")
	l_LastName			= rsLoader("LASTNAME")
	l_AddresS1			= rsLoader("ADDRESS1")
	l_Address2			= rsLoader("ADDRESS2")
	l_Town				= rsLoader("TOWN")
	l_County			= rsLoader("COUNTY")
	l_Postcode			= rsLoader("POSTCODE")
	l_OfficeTel			= rsLoader("OFFICETEL")
	l_HomeTel			= rsLoader("HOMETEL")
	l_Mobile			= rsLoader("MOBILE")
	'l_Fax				= rsLoader("FAX")
	l_OfficeEmail		= rsLoader("WORKEMAIL")
	l_HomeEmaiL 		= rsLoader("HOMEEMAIL")
	l_Position			= rsLoader("POSITION")
	l_DateCommenced 	= rsLoader("DATECOMMENCED")
	l_ReviewDate 		= rsLoader("REVIEW")
	l_EmploymentStatus 	= rsLoader("EMPLOYMENTSTSTUS")

	ACTION = "AMEND"
	FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"

End If

Call CloseRs(rsLoader)

Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "---", l_title, "width:50px", "textbox", " tabIndex='1'")
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Member Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "sel_TITLE|Title|SELECT|Y"
        FormFields[1] = "txt_FIRSTNAME|First Name|TEXT|Y"
        FormFields[2] = "txt_LASTNAME|Last Name|TEXT|Y"
        FormFields[3] = "txt_ADDRESS1|Address1|TEXT|N"
        FormFields[4] = "txt_ADDRESS2|Address2|TEXT|N"
        FormFields[5] = "txt_TOWN|Address2|TEXT|N"
        FormFields[6] = "txt_COUNTY|County|TEXT|N"
        FormFields[7] = "txt_POSTCODE|PostCode|TEXT|N"
        FormFields[8] = "txt_OFFICETEL|Office Telephone|TEXT|N"
        FormFields[9] = "txt_HOMETEL|Home Telephone|TEXT|N"
        FormFields[10] = "txt_MOBILE|Mobile|TEXT|N"
        FormFields[11] = "txt_FAX|Fax|TEXT|N"
        FormFields[12] = "txt_OFFICEEMAIL|Office Email|EMAIL|N"
        FormFields[13] = "txt_HOMEEMAIL|Home Email|EMAIL|N"
        FormFields[14] = "txt_POSITION|Position|TEXT|N"
        FormFields[15] = "txt_DATECOMMENCED|Date Commenced|DATE|Y"
        FormFields[16] = "sel_EMPLOYMENTSTATUS|Employment Status|TEXT|Y"

        function SaveForm() {
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide/NewBoardPerson_svr.asp"
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }		

    </script>
</head>
<body onload="initSwipeMenu(2);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="ServerSide/NewbOARDPerson_svr.asp">
    <table width="750" border="0" cellpadding="0" cellspacing="0" style="height:27px">
        <tr>
            <td rowspan="2">
                <img title="Personal Details" name="E_PD" id="E_PD" src="Images/1-open.gif" width="115" height="20"
                    border="0" alt="" />
            </td>
            <td rowspan="2">
                <img src="Images/TabEnd.gif" width="8" height="20" border="0" alt="Contacts" />
            </td>
            <td width="100%" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="../images/spacer.gif" width="53" height="1" alt="" /></td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="../images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="2" style="height:90%; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td nowrap="nowrap" width="110px">
                Title:&nbsp;<%=lstTitles%>
                <img src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                First Name:
                <input type="text" class="textbox" name="txt_FIRSTNAME" id="txt_FIRSTNAME" style="width: 132px" maxlength="30"
                    value="<%=l_firstname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap" width="85px">
                Office Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_OFFICETEL" id="txt_OFFICETEL" maxlength="40" value="<%=l_OFFICETEL%>"
                    tabindex="8" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_OFFICETEL" id="img_OFFICETEL" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Last Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="30" value="<%=l_lastname%>"
                    tabindex="3" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LASTNAME" id="img_LASTNAME" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Home Tel:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_HOMETEL" id="txt_HOMETEL" maxlength="40" value="<%=l_HOMETEL%>"
                    tabindex="8" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMETEL" id="img_HOMETEL" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td height="33">
                Address:
            </td>
            <td height="33">
                <input type="text" class="textbox200" name="txt_ADDRESS1" id="txt_ADDRESS1" value="<%=l_ADDRESS1%>"
                    tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS1" id="img_ADDRESS1" width="15px" height="15px" border="0" alt="" />
            </td>
            <td height="33">
                Mobile:
            </td>
            <td height="33">
                <input type="text" class="textbox200" name="txt_MOBILE" id="txt_MOBILE" value="<%=l_MOBILE%>" tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Address:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ADDRESS2" id="txt_ADDRESS2" value="<%=l_ADDRESS2%>"
                    tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ADDRESS2" id="img_ADDRESS2" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Fax:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_FAX" id="txt_FAX" maxlength="8" value="<%=l_FAX%>"
                    tabindex="12" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FAX" id="img_FAX" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Town:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_TOWN" id="txt_TOWN" value="<%=l_TOWN%>" tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_TOWN" id="img_TOWN" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Office Email:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_OFFICEEMAIL" id="txt_OFFICEEMAIL" maxlength="40" value="<%=l_OFFICEEMAIL%>"
                    tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_OFFICEEMAIL" id="img_OFFICEEMAIL" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                County:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" value="<%=l_COUNTY%>" tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Home Email:
            </td>
            <td align="right">
                <input type="text" class="textbox200" name="txt_HOMEEMAIL" id="txt_HOMEEMAIL" maxlength="40" value="<%=l_HOMEEMAIL%>"
                    tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_HOMEEMAIL" id="img_HOMEEMAIL" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Postcode:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" value="<%=l_POSTCODE%>"
                    tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px" border="0" alt="" />
            </td>
            <td>
                Position:
            </td>
            <td align="right">
                <input type="text" class="textbox200" name="txt_POSITION" id="txt_POSITION" maxlength="40" value="<%=l_POSITION%>"
                    tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_POSITION" id="img_POSITION" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
            </td>
            <td>
                Date Commenced:
            </td>
            <td align="right">
                <input type="text" class="textbox200" name="txt_DATECOMMENCED" id="txt_DATECOMMENCED" maxlength="40" value="<%=l_DATECOMMENCED%>"
                    tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DATECOMMENCED" id="img_DATECOMMENCED" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                Review Date:
            </td>
            <td align="right">
                <input type="text" class="textbox200" name="txt_REVIEWDATE" id="txt_REVIEWDATE" maxlength="40" readonly="readonly"
                    value="<%=l_REVIEWDATE%>" tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_REVIEWDATE" id="img_REVIEWDATE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                Status:
            </td>
            <td align="right">
                <select name="sel_EMPLOYMENTSTATUS" id="sel_EMPLOYMENTSTATUS" style="width: 200px" class="textbox200">
                    <option value="1" <%if l_EMPLOYMENTSTATUS = "1" then rw "selected" end if%>>Active</option>
                    <option value="0" <%if l_EMPLOYMENTSTATUS = "0" then rw "selected" end if%>>Inactive</option>
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMPLOYMENTSTATUS" id="img_EMPLOYMENTSTATUS" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td align="right">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
            <td align="right">
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="button" name="BtnSave" id="BtnSave" class="RSLButton" value=" SAVE" title="SAVE" onclick="SaveForm()" tabindex="14" style="cursor:pointer" />
            </td>
        </tr>
        <tr>
            <td height="100%">
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="iEmployeeCheck" id="iEmployeeCheck" style="display: none">
    </iframe>
</body>
</html>
