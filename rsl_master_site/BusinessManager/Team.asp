<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size

	Call getList(lst_director, "ISDIRECTOR", "D")
	Call getList(lst_manager, "ISMANAGER", "M")
	Call getTeams()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	' retrives team details from database and builds 'paged' report
	Function getTeams()

		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT 	T.TEAMNAME, T.TEAMID, " &_
					"			MAINFUNCTION = CASE  " &_
					"			WHEN LEN(T.MAINFUNCTION) < 40 THEN T.MAINFUNCTION " &_
					"			ELSE SUBSTRING(T.MAINFUNCTION, 1, 40) + '..' END, " &_
					"			T.MAINFUNCTION AS LONGMAINFUNCTION, " &_
					"			DESCRIPTION = CASE " &_
					"			WHEN T.DESCRIPTION IS NULL THEN 'N/A' " &_
					"			WHEN LEN(T.DESCRIPTION) < 40 THEN T.DESCRIPTION " &_
					"			ELSE SUBSTRING(T.DESCRIPTION, 1, 40) + '..' END, " &_
					"			T.DESCRIPTION AS LONGDESCRIPTION, " &_
					"			ISNULL(E1.FIRSTNAME + ' ' + E1.LASTNAME, 'N/A') AS DIRECTOR, " &_
					"			ISNULL(E2.FIRSTNAME + ' ' + E2.LASTNAME, 'N/A') AS MANAGER " &_
					"FROM	 	E_TEAM T " &_
					"			LEFT JOIN E__EMPLOYEE E1 ON T.DIRECTOR = E1.EMPLOYEEID " &_
					"			LEFT JOIN E__EMPLOYEE E2 ON T.MANAGER = E2.EMPLOYEEID " &_
					" WHERE T.ACTIVE=1 "&_
					"ORDER 		BY T.TEAMNAME "

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then

			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<tbody class=""CAPS"">"
			For intRecord = 1 to rsSet.PageSize

				team_name = rsSet("TEAMNAME")
				str_data = str_data & "<tr style=""cursor:pointer"">" &_
					"<td onclick=load_team(" & rsSet("TEAMID") & ") width=""150px"">" & rsSet("TEAMNAME") & "</td>" &_
					"<td onclick=load_team(" & rsSet("TEAMID") & ") width=""150px"">" & rsSet("DIRECTOR") & "</td>" &_
					"<td onclick=load_team(" & rsSet("TEAMID") & ") width=""150px"">" & rsSet("MANAGER") & "</td>" &_
					"<td onclick=load_team(" & rsSet("TEAMID") & ") width=""450px"" title=""" & rsSet("LONGDESCRIPTION") & """>" & rsSet("MAINFUNCTION") & "</td>" &_
					"<td style=""border-bottom:1px solid silver"" class=""DEL"" onclick=""del_team(" & rsSet("TEAMID") & ")"" title=""[DELETE] " & rsSet("TEAMNAME") & """ style=""cursor:pointer"">Del</td></tr>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""5"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='TEAM.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='TEAM.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='TEAM.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='TEAM.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""5"" align=""center""><b>No teams exist within the system</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""5"" align=""center"" style=""background-color:white"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
	
	' generates drop downs taking the table as an argument
	' criteria = 'manager' or 'director'
	Function getList(ByRef lst, criteria, ch_level)

		Dim strSQL, rsSet, int_lst_record

		int_lst_record = 0

		If ch_level = "D" Then
			strSQL  = "SELECT FIRSTNAME + ' ' + LASTNAME AS FULLNAME, E.EMPLOYEEID FROM E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
						" WHERE	" & criteria & " = 1 AND J.ACTIVE = 1 "

		    set rsSet = Server.CreateObject("ADODB.Recordset")
				rsSet.ActiveConnection = RSL_CONNECTION_STRING
				rsSet.Source = strSQL
				rsSet.CursorType = 0
				rsSet.CursorLocation = 2
				rsSet.LockType = 3
				rsSet.Open()
				lst = "<option value=''>Please select</option>"
				While (NOT rsSet.EOF)
					lst = lst & "<option value='" & rsSet("EMPLOYEEID") & "'" & extrabit & ">" & rsSet("FULLNAME") & "</option>"
					rsSet.MoveNext()
					int_lst_record = int_lst_record + 1
				Wend
				If int_lst_record = 0 then
					lst = "<option value=''>No Records Exist</option>"
				End If
				rsSet.close()
			Set rsSet = Nothing
		Else
			lst = "<option value=''>No Records Exist</option>"
		End If

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Team Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_TEAMNAME|Team Name|TEXT|Y"
        FormFields[1] = "sel_DIRECTOR|Director|SELECT|Y"
        FormFields[2] = "txt_MAINFUNCTION|Function|TEXT|Y"

        var theItems = new Array("txt_TEAMNAME", "sel_DIRECTOR", "sel_MANAGER", "txt_MAINFUNCTION");

        // called by save and amend buttons

        function click_save(str_action) {
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_team";
            document.RSLFORM.action = "serverside/team_srv.asp?path=" + str_action;
            document.RSLFORM.submit();
        }

        // called by delete button
        function del_team(int_id) {
            var answer
            answer = window.confirm("Are you sure you wish to delete this team")
            // if yes then send to server page to delete
            if (answer) {
                document.RSLFORM.target = "frm_team";
                document.RSLFORM.action = "serverside/team_srv.asp?path=del&team_id=" + int_id;
                document.RSLFORM.submit();
            }
        }

        // called when user clicks on a row
        function load_team(int_id) {
            swap_div(3);
            document.RSLFORM.target = "frm_team";
            document.RSLFORM.action = "serverside/team_srv.asp?path=load&team_id=" + int_id;
            document.RSLFORM.submit();
        }

        // swaps save button for amend button and vice versa
        // called by server side page
        function swap_button(int_which_one) { //   1 = show amend, 2 = show save
            if (int_which_one == 2) {
                document.getElementById("BTN_SAVE").style.display = "block"
                document.getElementById("BTN_AMEND").style.display = "none"
            }
            else {
                document.getElementById("BTN_SAVE").style.display = "none"
                document.getElementById("BTN_AMEND").style.display = "block"
            }
        }

        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        function swap_div(int_which_one) {
            if (int_which_one == 1) {
                document.getElementById("new_div").style.display = "none";
                document.getElementById("table_div").style.display = "block";
                document.getElementById("tab_team").src = 'images/tab_team-over.gif';
                document.getElementById("tab_new_team").src = 'images/tab_new_team-tab_team_over.gif';
                document.getElementById("tab_amend_team").src = 'images/tab_amend_team.gif';
            }
            else if (int_which_one == 2) {
                clear()
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_new_team").src = 'images/tab_new_team-over.gif';
                document.getElementById("tab_team").src = 'images/tab_team.gif';
                document.getElementById("tab_amend_team").src = 'images/tab_amend_team-tab_new_team.gif';
            }
            else {
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_team").src = 'images/tab_team.gif';
                document.getElementById("tab_new_team").src = 'images/tab_new_team.gif';
                document.getElementById("tab_amend_team").src = 'images/tab_amend_team-over.gif';
            }
            checkForm(false, true)
        }

        // clear form fields
        function clear() {
            document.getElementById("txt_TEAMNAME").value = "";
            document.getElementById("sel_DIRECTOR").value = "";
            document.getElementById("dvMANAGER").innerHTML = "<select name='sel_MANAGER' id='sel_MANAGER' class='textbox200'><option value=''>No Records Exist</option></select>";
            document.getElementById("txt_MAINFUNCTION").value = "";
            document.getElementById("txt_DESCRIPTION").value = "";
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_team" id="tab_team" onclick='swap_div(1)' style="cursor: pointer" src="images/tab_team-over.gif"
                    width="55" height="20" border="0" alt="Team" />
            </td>
            <td rowspan="2">
                <img name="tab_new_team" id="tab_new_team" onclick='swap_button(2);swap_div(2)' style="cursor: pointer"
                    src="images/tab_new_team-tab_team_over.gif" width="89" height="20" border="0" alt="New Team" />
            </td>
            <td rowspan="2">
                <img name="tab_amend_team" id="tab_amend_team" src="images/tab_amend_team.gif" width="112" height="20"
                    border="0" alt="Amend Team" />
            </td>
            <td>
                <img src="images/spacer.gif" width="494" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="494" height="1" alt="" /></td>
        </tr>
    </table>
    <div id="new_div" style="display: none">
        <table width="750" style="border-right: solid 1px #133e71" cellpadding="1" cellspacing="2"
            border="0" class="TAB_TABLE">
            <tr style="height: 3PX">
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Team Name
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_TEAMNAME" id="txt_TEAMNAME" maxlength="100" size="30" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_TEAMNAME" id="img_TEAMNAME" width="15px" height="15px" border="0" alt="" />
                </td>
                <td align="left">
                    Brief Description
                </td>
                <td rowspan="4" valign="top" align="left">
                    <textarea name="txt_DESCRIPTION" id="txt_DESCRIPTION" rows="5" cols="62" tabindex="1" style="border: 1px solid black;
                        overflow: hidden" class="textbox200"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_DESCRIPTION" id="img_DESCRIPTION" width="15px" height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td>
                    Team Director
                </td>
                <td>
                    <select name="sel_DIRECTOR" id="sel_DIRECTOR" class="textbox200">
                        <%=lst_director%>
                    </select>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_DIRECTOR" id="img_DIRECTOR" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Team Manager
                </td>
                <td valign="top">
                    <div id="dvMANAGER"><select name="sel_MANAGER" id="sel_MANAGER" class="textbox200">
                        <%=lst_manager%>
                    </select></div>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_MANAGER" id="img_MANAGER" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Main Function
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_MAINFUNCTION" id="txt_MAINFUNCTION" maxlength="100"
                        size="30" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_MAINFUNCTION" id="img_MAINFUNCTION" width="15px" height="15px" border="0" alt="" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
                <tr>
                    <td>
                        Active
                    </td>
                    <td>
                        <input type="checkbox" name="chk_ACTIVE" id="chk_ACTIVE" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="left">
                        <input type="button" name="BTN_SAVE" id="BTN_SAVE" value="Save" title="Save" class="rslbutton"
                            onclick="click_save('new')" style="display: block; cursor:pointer" />
                        <input type="button" name="BTN_AMEND" id="BTN_AMEND" value="Amend" title="Amend" class="rslbutton"
                            onclick="click_save('amend')" style="display: none; cursor:pointer" />
                    </td>
                </tr>
        </table>
    </div>
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="behavior: url(/Includes/Tables/tablehl.htc);
            border-collapse: collapse" slcolor='' border="1" hlcolor="STEELBLUE">
            <thead>
                <tr style="height: 3px; border-bottom: 1px solid #133e71">
                    <td colspan="5" class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td width="150px" class='TABLE_HEAD'>
                        <b>Team Name</b>
                    </td>
                    <td width="150px" class='TABLE_HEAD'>
                        <b>Team Director</b>
                    </td>
                    <td width="150px" class='TABLE_HEAD'>
                        <b>Team Manager</b>
                    </td>
                    <td width="150px" class='TABLE_HEAD'>
                        <b>Main Function</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD' colspan="5" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
    </div>
    <input type="hidden" name="hd_team_id" id="hd_team_id" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
