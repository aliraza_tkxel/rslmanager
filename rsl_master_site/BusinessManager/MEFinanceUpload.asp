<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<html>
    <head>
        <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
        <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
        <title>RSL Manager Business --> ME Finance Upload </title>
        <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    </head>
    <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="javascript" src="/js/general.js"></script>
    <script type="text/javascript" language="javascript" src="/js/menu.js"></script>
    <script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
    <script  type="text/javascript" language="javascript">
        function openDialog(form) {
            window.showModalDialog("MEFinanceReportFormat.asp", form, "dialogWidth:752px;dialogHeight:795px;center:yes"); 
        }
    </script>

    <body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages()" onunload="macGo()" style="margin:10 0 0 10;">
    <!--#include virtual="Includes/Tops/BodyTop_portfolioreport.asp" -->
        <table width="592px" border="0" cellpadding="0" cellspacing="0" style="height:26px">
            <tr>
                <td rowspan="2" width="133px">
                    <img src="images/tab_mefinanceupload.gif" width="150px" height="23px" border="0px" alt=""/>
                </td>
			    <td width="461px">
			        <img src="images/spacer.gif" width="200px" height="19px" alt=""/>
			    </td>
		    </tr>
		    <tr>
		        <td bgcolor="#133E71" width="400px"><img src="images/spacer.gif" width="200px" height="1px" alt=""/></td>
		    </tr>
		    <tr> 
			    <td colspan="2" style="border-left:1px solid #133e71; border-right:1px solid #133e71"> 
			        <img src="images/spacer.gif" width="53px" height="6px" alt=""/>
			    </td>
		    </tr>	
	    </table>
	    <div style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71;width:592px;height:225px;'>
	        <p style=" font-size:11px;font-family:Verdana, Arial, Helvetica;margin:0 0 0 5; ">
	            <b>Note: </b> Please make sure that the sheet name of the uploaded excel file is 
	                         <span style=" font-style:italic;">'Monthly Report'</span>.<br />
	                         <span style="padding-left:38px;">Please <a  href="javascript:openDialog(this.form);">click here</a> to the see the format of the report.</span> 
	        </p>
            <iframe id="ifmPlannedWork" src="/RSLMEFinance/MEF_Report_Upload.aspx" frameborder="0"  height="99%" width="99%"></iframe>
        </div>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    </body>
</html>