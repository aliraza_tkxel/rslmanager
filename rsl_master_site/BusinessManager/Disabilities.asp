<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim rsLoader, ACTION, lst_dis, lst_diff
	Dim strTable

	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
	If (EmployeeID = "") Then EmployeeID = -1 End If

	Call OpenDB()

	SQL = "SELECT DisDESC = CASE " &_
			"WHEN LEN(DESCRIPTION) " &_
			"< 30 THEN DESCRIPTION ELSE SUBSTRING(DESCRIPTION, 1, 30) + '...' " &_
			"END " &_
			", E_DIFFDIS.*, E_DIFFDISID.* FROM E_DIFFDIS INNER JOIN E_DIFFDISID ON E_DIFFDIS.DIFFDISID = E_DIFFDISID.DIFFDIS WHERE EMPLOYEEID = " & EmployeeID & " ORDER BY DESCRIPTION"

	Call OpenRs(rsLoader, SQL)

SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsName, SQL)

If NOT rsName.EOF Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")
	FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"
End If
Call CloseRs(rsName)

	strTable = "<table width=""220"" cellspacing=""0"" cellpadding=""1"">"
		strTable = strTable & "<thead>"
			strTable = strTable & "<tr>"
				strTable = strTable & "<td width=""200"" colspan=""2""><b>Disability/Learning Difficulty</b></td>"
			strTable = strTable & "</tr>"
			strTable = strTable & "<tr>"
				strTable = strTable & "<td colspan=""2"">"
				strTable = strTable & "<hr style=""border:1px dotted #133e71""/></td>"
			strTable = strTable & "</tr>"
	strTable = strTable & "</thead>"
	strTable = strTable & "<tbody>"

	If (Not rsLoader.EOF) Then
		While (NOT rsLoader.EOF)
			txtTitle = rsLoader("NOTES")
			If (txtTitle = "" OR isNull(txtTitle)) Then
				txtTitle = "No additional notes."
			End If
			strTable = strTable & "<tr>"
				strTable = strTable & "<td width=""200"" style=""cursor:pointer"" title='" & txtTitle & "'>" & rsLoader("DisDESC") & "</TD>"	
				strTable = strTable & "<td width=""20"" class=""del"" style=""cursor: pointer"" onclick=""DeleteMe(" & rsLoader("DISABILITYID") & ")"" title=""[DELETE] " & rsLoader("DESCRIPTION") & """>DEL</td>"
			strTable = strTable & "</tr>"
			rsLoader.moveNext
		Wend
	Else
			strTable = strTable & "<tr>"
				strTable = strTable & "<td colspan='2' align='center'>No Disability/Learning Difficulty</td>"
			strTable = strTable & "</tr>"
	End If
		strTable = strTable & "</tbody>"
		strTable = strTable & "<tfoot>"
			strTable = strTable & "<tr>"
				strTable = strTable & "<td colspan=""2"">"
				strTable = strTable & "<hr style=""border:1px dotted #133e71""/></td>"
			strTable = strTable & "</tr>"
		strTable = strTable & "</tfoot>"
	strTable = strTable & "</table>"

	Call BuildSelect(lst_dis, "sel_DISABILITY", "E_DIFFDIS WHERE DTYPE = 1", "DIFFDISID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lst_diff, "sel_DIFFICULTY", "E_DIFFDIS WHERE DTYPE = 2", "DIFFDISID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)

	Call CloseRs(rsLoader)

     Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED @EmployeeId = " & EmployeeID
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

	Call CloseDB()
%>
<%
Dim strfrmflds
Dim DataFields   (8)
Dim DataTypes    (8)
Dim ElementTypes (8)
Dim FormValues   (8)
Dim FormFields   (8)

	FormFields(0) = "txt_FNAME|TEXT"
	FormFields(1) = "txt_SNAME|TEXT"
	FormFields(2) = "txt_HOUSENUMBER|TEXT"
	FormFields(3) = "txt_STREET|TEXT"
	FormFields(4) = "txt_POSTALTOWN|TEXT"
	FormFields(5) = "txt_COUNTY|TEXT"
	FormFields(6) = "txt_POSTCODE|TEXT"
	FormFields(7) = "txt_HOMETEL|NUMBER"
	FormFields(8) = "txt_MOBILE|NUMBER"

	SQL = "SELECT * FROM E_NEXTOFKIN WHERE EMPLOYEEID = " & EmployeeID
	Call OpenDB()
	Call OpenRs(rsLoaded, SQL)
	strfrmflds = ""
	IF NOT rsLoaded.EOF Then
		ACTION = "AMEND"
		For i = 0 To UBound(FormFields)
			FormSplit = Split(FormFields(i), "|")
			FormSecondSplit = Split(FormSplit(0), "_")
			DataFields(i) = FormSecondSplit(1)
			strfrmflds = strfrmflds & "document.RSLFORM." & FormSplit(0) & ".value = """ & rsLoaded(DataFields(i)) & """;" & vbNewLine
		Next
	End If
	strfrmflds = strfrmflds
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
function DisLoaderFunction() {
	<%= strfrmflds %>
}

var FormFields = new Array();
FormFields[0] = "txt_FNAME|First Name|TEXT|  Y"
FormFields[1] = "txt_SNAME|Last Name|TEXT|Y"
FormFields[2] = "txt_HOUSENUMBER|House Number/Name|TEXT|N"
FormFields[3] = "txt_STREET|Street|TEXT|N"
FormFields[4] = "txt_POSTALTOWN|Postal Town|TEXT|N"
FormFields[5] = "txt_COUNTY|County|TEXT|N"
FormFields[6] = "txt_POSTCODE|Postcode|POSTCODE|N"
FormFields[7] = "txt_HOMETEL|Home Telephone|TELEPHONE|Y"
FormFields[8] = "txt_MOBILE|Mobile|TELEPHONE|N"	

function SaveForm(){
	if (!checkForm()){ 
    return false;
    }
	document.RSLFORM.target = ""
	document.RSLFORM.action = "ServerSide/Disabilities_svr.asp"
	document.RSLFORM.submit()
	}

function ResetForm(){
	document.RSLFORM.reset()
	}

function DeleteMe(THE_ID){
	document.getElementById("hid_DisType").value = THE_ID
	document.RSLFORM.target = "hiddenFrame"
	document.RSLFORM.action = "ServerSide/Disabilities_svr_AddDD.asp"
	document.getElementById("hid_Action_DiffDis").value = "DELETE"
	document.RSLFORM.submit()
	}

function AddDisability(THE_ID){
	if (document.getElementById("sel_DISABILITY").value == ""){
        alert("Please select disability type")
        return false;
     }
	document.getElementById("hid_DisType").value = 1
	document.RSLFORM.target = "hiddenFrame"
	document.RSLFORM.action = "ServerSide/Disabilities_svr_AddDD.asp"
	document.getElementById("hid_Action_DiffDis").value = "ADD"
    document.getElementById("hid_Action_DiffDis")
    document.getElementById("grpAdd").disabled = true 
    document.getElementById("grpAdd2").disabled = true 
    document.getElementById("myButton2").disabled = true 
    document.getElementById("myButtonReset2").disabled = true 
	document.RSLFORM.submit()
	}

function AddDifficulty(THE_ID){
	if (document.getElementById("sel_DIFFICULTY").value == ""){
        alert("Please select difficulty type")
        return false;
     } 
	document.getElementById("hid_DisType").value = 2
	document.RSLFORM.target = "hiddenFrame"
	document.RSLFORM.action = "ServerSide/Disabilities_svr_AddDD.asp"
	document.getElementById("hid_Action_DiffDis").value = "ADD"
    document.getElementById("grpAdd").disabled = true 
    document.getElementById("grpAdd2").disabled = true 
    document.getElementById("myButton2").disabled = true 
    document.getElementById("myButtonReset2").disabled = true 
	document.RSLFORM.submit()
	}

function limitText(textArea, length) {
    if (textArea.value.length > length) {
        textArea.value = textArea.value.substr(0,length);
    }
}
	// if action = 1 then clear partners, otherwise clear priority groups
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();DisLoaderFunction();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img name="E_PD" id="E_PD" src="Images/1-closed.gif" width="115" height="20" border="0"
                        alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" id="E_C" src="images/2-closed.gif" width="74" height="20" border="0"
                        alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" id="E_SQ" src="images/3-closed.gif" width="94" height="20" border="0"
                        alt="Qualifications" /></a>
            </td>
            <td rowspan="2" style='display: none'>
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" id="E_PE" src="images/4-closed.gif" width="86" height="20" border="0"
                        alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" id="E_DD" src="images/5-open.gif" width="70" height="20" border="0"
                        alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" id="E_JD" src="Images/6-previous.gif" width="52" height="20" border="0"
                        alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" id="E_B" src="images/7-closed.gif" width="82" height="20" border="0"
                        alt="Benefits" /></a>
            </td>
            <td width="265" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <form name="RSLFORM" method="post" action="ServerSide/Disabilities_svr.asp" target="hiddenFrame"
    style="margin: 0px">
    <table width="750" border="0" cellspacing="0" cellpadding="0" style="border-left: 1px solid #133E71;
        border-bottom: 1px solid #133E71; border-right: 1px solid #133E71">
        <tr>
            <td>
                &nbsp;
                <table cellspacing="2" style="height: 100%">
                    <tr>
                        <td width="20">
                        </td>
                        <td>
                            Disability or Health problem
                        </td>
                        <td>
                        </td>
                        <td>
                            Learning Difficulty
                        </td>
                        <td rowspan="4" valign="top" width="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                        </td>
                        <td>
                            <div id="dvDISABILITY">
                                <%=lst_dis%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DISABILITY" id="img_DISABILITY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                            <div id="dvDIFFICULTY">
                                <%=lst_diff%></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DIFFICULTY" id="img_DIFFICULTY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                        </td>
                        <td>
                            <textarea class="textbox200" name="txt_DISABILITYTEXT" id="txt_DISABILITYTEXT" rows="4"
                                cols="10"  maxlength="300" tabindex="4"></textarea>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <textarea class="textbox200" name="txt_DIFFICULTYTEXT" id="txt_DIFFICULTYTEXT" rows="4"
                                cols="10"  maxlength="300" tabindex="4"></textarea>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                        </td>
                        <td align="right">
                            <input type="button" name="grpAdd" id="grpAdd" onclick="AddDisability()" value=" Add "
                                title=" Add " class="RSLButton" />
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <input type="button" name="grpAdd2" id="grpAdd2" onclick="AddDifficulty()" value=" Add "
                                title=" Add " class="RSLButton" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <input type="hidden" name="hid_EMPLOYEEID" id="hid_EMPLOYEEID" value="<%=EmployeeID%>" />
                            <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                            <input type="hidden" name="hid_Action_DiffDis" id="hid_Action_DiffDis" value="<%=ACTION%>" />
                            <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />
                            <input type="hidden" name="hid_DisType" id="hid_DisType" value="" />
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" rowspan="5" style='border-left: 1px solid #133e71'>
                <table cellspacing="2">
                    <tr>
                        <td width="300">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <div id='ListRows'>
                                <%=strTable%></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='border-bottom: 1px solid #133E71;'>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            <br />
                            <b>Next of Kin Details</b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            First Name:
                        </td>
                        <td nowrap="nowrap">
                            <input type="text" class="textbox200" name="txt_FNAME" id="txt_FNAME" maxlength="50"
                                value="<%=l_fname%>" />&nbsp;
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_FNAME" id="img_FNAME" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_SNAME" id="txt_SNAME" maxlength="50"
                                value="<%=l_sname%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_SNAME" id="img_SNAME" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            House Number/Name:&nbsp;
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_HOUSENUMBER" id="txt_HOUSENUMBER"
                                maxlength="50" value="<%=l_housenumber%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_HOUSENUMBER" id="img_HOUSENUMBER" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Street:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_STREET" id="txt_STREET" maxlength="40"
                                value="<%=l_street%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STREET" id="img_STREET" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Postal Town:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_POSTALTOWN" id="txt_POSTALTOWN" maxlength="40"
                                value="<%=l_postaltown%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_POSTALTOWN" id="img_POSTALTOWN" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            County:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_COUNTY" id="txt_COUNTY" maxlength="30"
                                value="<%=l_county%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_COUNTY" id="img_COUNTY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Postcode:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_POSTCODE" id="txt_POSTCODE" maxlength="10"
                                value="<%=l_postcode%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_POSTCODE" id="img_POSTCODE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Home Tel:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_HOMETEL" id="txt_HOMETEL" maxlength="20"
                                value="<%=l_hometel%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_HOMETEL" id="img_HOMETEL" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            Mobile:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_MOBILE" id="txt_MOBILE" maxlength="30"
                                value="<%=l_mobile%>" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_MOBILE" id="img_MOBILE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td width="20">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div align="right">
                                <input class="RSLButton" name="myButton2" id="myButton2" type="button" value=" Save & Next "
                                    onclick="SaveForm()" tabindex="12" style="cursor: pointer" />
                                <input type="button" name="myButtonReset2" id="myButtonReset2" onclick="ResetForm()"
                                    value=" Reset " title=" Reset " class="RSLButton" style="cursor: pointer" />&nbsp;
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
    </table>  
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="hiddenFrame" id="hiddenFrame" style="display: none">
    </iframe>
</body>
</html>