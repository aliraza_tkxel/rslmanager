<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim isDirectorCode, director_sql

	' if the user is a director/ manager show only their team
	director_sql = ""		' default to empty
	isDirectorCode = Request("HFKS023")
	If isDirectorCode = "" Then isDirectorCode = 0 End If
	If isDirectorCode Then 
		director_sql =  get_director_sql() 
	End If 
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	Call getTeams()

	' build string to be used in 'IN 'statement in SQL containing IDs of directors teams
	Function get_director_sql()

		If Not ((Session("TeamName") = "Corporate Services") Or (Session("USERID") =  35)) Then

			Dim str_team_ids
			    str_team_ids = ""

			SQL = 	"SELECT distinct T.TEAMID  " &_
					"FROM E__EMPLOYEE E " &_
					"	INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
					"	LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM  " &_
					"WHERE	(LINEMANAGER = " & Session("userid") & " ) OR (DIRECTOR = " & Session("userid") & " OR MANAGER = " & Session("userid") & ") AND T.ACTIVE=1"
			Call OpenDB()
			Call OpenRS(rsSet, SQL)
			str_team_ids = str_team_ids & " AND T.TEAMID IN ("
			If Not rsSet.EOF Then
					while Not rsSet.EOF
						str_team_ids = str_team_ids & rsSet(0)
						rsSet.movenext()
						If Not rsSet.EOF Then str_team_ids = str_team_ids & ", " End If
					Wend
			Else
				str_team_ids = str_team_ids & "-1"
			End If
			str_team_ids = str_team_ids & ")"
			get_director_sql = str_team_ids
			Call CloseDB()
		End If

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0		
		str_data = ""
		strSQL = 	"DECLARE @BHSTART SMALLDATETIME, @BHEND   SMALLDATETIME  " &_
					" SELECT @BHSTART = ystart, @BHEND = yend FROM   f_fiscalyears ORDER  BY yrange ASC " &_
					" SELECT T.TEAMID,T.TEAMNAME, (SELECT Isnull(Cast(Count(*) AS NVARCHAR), '0') FROM   e_jobdetails WHERE  team = T.teamid AND active = 1 AND LINEMANAGER =" & Session("USERID")& ") AS TEAMNUM, " &_
					" (SELECT Isnull(Cast(Sum(holidayentitlementdays)AS NVARCHAR), '0') FROM   e_jobdetails WHERE  team = T.teamid AND active = 1 AND LINEMANAGER=" & Session("USERID")& ")      AS LEAVE, " &_
					" (SELECT Isnull(Cast(Sum(salary)AS NVARCHAR), '0') FROM   e_jobdetails WHERE  team = T.teamid AND active = 1 AND LINEMANAGER=" & Session("USERID")& ")      AS SALARY, " &_
					" Isnull(SickLeaveInfo.SicknessAbsense,0) as SICKNESSABSENCE,	Isnull(SickLeaveInfo.SicknessSalary,0) as SicknessSalary	   " &_
					" FROM   e_team T " &_
					" Outer Apply (Select SUM(calculatedSalary) as SicknessSalary,SUM(SICKNESSABSENCE) as SicknessAbsense,TEAMID FROM ( " &_
					" SELECT ((Isnull(jd.salary, 0)/52)/5)* Isnull(Sum(A.duration), 0) as calculatedSalary,  " &_
					"      Isnull(Sum(A.duration), 0) AS SICKNESSABSENCE, Isnull(jd.salary, 0) AS SALARY ,T.TEAMID " &_
					" FROM   e__employee E  " &_
					"      INNER JOIN e_journal J ON J.employeeid = E.employeeid  " &_
					"      INNER JOIN e_absence A ON A.journalid = J.journalid  " &_
					"		AND A.startdate BETWEEN @BHSTART AND @BHEND " &_
					"		AND A.absencehistoryid = (SELECT Max(absencehistoryid) FROM   e_absence WHERE  journalid = A.journalid) " &_
					"       LEFT JOIN e_jobdetails JD ON JD.employeeid = E.employeeid " &_
					"      LEFT JOIN e_team IT ON T.teamid = JD.team " &_
					" WHERE  itemnatureid = 1 AND A.itemstatusid NOT IN ( 20 ) AND duration IS NOT NULL AND T.TEAMID = IT.TEAMID AND JD.LINEMANAGER= " & Session("USERID") & " " &_
					" GROUP  BY IT.teamid, jd.Salary) AS Result Group By  Result.TEAMID )SickLeaveInfo WHERE  T.teamid <> 1 AND T.active = 1 AND" &_
					"(SELECT Isnull(Cast(Count(*) AS NVARCHAR), '0') FROM   e_jobdetails WHERE  team = T.teamid AND active = 1 AND LINEMANAGER ="& Session("USERID")&") >0" &_
					 director_sql  & " ORDER  BY T.TEAMNAME "					 	
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class=""CAPS"">"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 To rsSet.PageSize

				str_data = str_data & "<tr style=""cursor:pointer"" onclick=""load_myteam(" & rsSet("TEAMID") & "," & isDirectorCode & ")"">" &_
						"<td width=""290px"">" & rsSet("TEAMNAME") & "</td>" &_
						"<td align=""center"" width=""90px"">" & rsSet("TEAMNUM") &	"</td>" &_
						"<td align=""center"" width=""90px"">" & rsSet("LEAVE") & "</td>" & _
						"<td align=""center"" width=""90px"">" & rsSet("SICKNESSABSENCE") &"</td>"
					' MAKE SURE YOU DONT TRY TO DEVIDE BY ZERO
					If rsSet("TEAMNUM") > 0 then
						str_data = str_data & "<td align=""center"" width=""90px"">" & FormatNumber(IsNullNumber(rsSet("SicknessSalary"))) & "</td>"
					Else
						str_data = str_data & "<td align=""center"" width=""90px"">" & FormatNumber(IsNullNumber(rsSet("SicknessSalary"))) & "</td>"
					End If
						str_data = str_data & "<td align=""right"" width=""90px"">" & FormatCurrency(rsSet("SALARY")) & "</td>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			IF Not isDirectorCode = 1 Then			
			get_orphans()
			End If
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""7"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='ESTABLISHMENT.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='ESTABLISHMENT.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & (intRecordCount + 1) &_
			" <a href='ESTABLISHMENT.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='ESTABLISHMENT.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		If intRecord = 0 Then 
			str_data = "<tr><td colspan=""7"" align=""center""><b>No teams exist within the system</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""7"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function


	Function get_orphans()

		strSQL = 	"SELECT 	-1 AS TEAMID, 'No team' AS TEAMNAME,	COUNT(*) AS NOTEAMCOUNT " &_
					"FROM	 	E__EMPLOYEE E " &_
					"		 	LEFT JOIN E_JOBDETAILS T ON E.EMPLOYEEID = T.EMPLOYEEID " &_
					"WHERE		T.TEAM IS NULL"

		set rsOrphan = Server.CreateObject("ADODB.Recordset")
		rsOrphan.ActiveConnection = RSL_CONNECTION_STRING
		rsOrphan.Source = strSQL
		rsOrphan.CursorType = 3
		rsOrphan.CursorLocation = 3
		rsOrphan.Open()

		while not rsOrphan.EOF

			count = count + 1
			str_data = str_data & "<tr style=""cursor:pointer"" onclick=""load_myteam(" & rsOrphan("TEAMID") & ")"">" &_
					"<td width=""200px"" style=""color:blue"">" & rsOrphan("TEAMNAME") & "</td>" &_
					"<td align=""center"" width=""90px"">" & rsOrphan("NOTEAMCOUNT") &	"</td>" &_
					"<td colspan=""6"">&nbsp;</td></tr>"

			rsOrphan.movenext()

		Wend

		rsOrphan.close()
		Set rsOrphan = Nothing

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Establishment</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

        function load_myteam(team_id, isAdirector) {
            if (isAdirector == 1)
                location.href = "man_team.asp?bypass=1&team_id=" + team_id;
            else
                location.href = "myteam.asp?team_id=" + team_id;
        }
	
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_establishment" id="tab_establishment" title='Team Overview' src="images/establishment.gif"
                    width="115" height="20" border="0" alt="" />
            </td>
            <td>
                <img src="images/spacer.gif" width="635" height="19" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="635" height="1" border="0" alt="" /></td>
        </tr>
    </table>
    <table width="750" cellpadding="1" class="TAB_TABLE" cellspacing="2" style="border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="7">
        <thead>
            <tr>
                <td width="200px" class='TABLE_HEAD'>
                    <b>Team Name</b>
                </td>
                <td width="90px" align="center" class='NO-BORDER'>
                    <b>N� Emps</b>
                </td>
                <td width="90px" align="center" class='NO-BORDER'>
                    <b>Total Leave</b>
                </td>
                <td width="90px" align="center" class='NO-BORDER'>
                    <b>Sickness Absence</b>
                </td>
                <td width="90px" align="center" class='NO-BORDER'>
                    <b>Cost of Absence</b>
                </td>
                <td width="90px" align="center" class='NO-BORDER'>
                    <b>Total Salary</b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="7" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4"
        style="display: none"></iframe>
</body>
</html>
