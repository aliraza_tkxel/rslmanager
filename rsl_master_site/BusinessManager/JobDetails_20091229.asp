<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim rsLoader, ACTION, lstprobations, lstpartfulltimes, lstteams, lstpatchs, lstnotice, lstgrades, lstlinemanager, lstbuddy
	l_team = -1
	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
	if (EmployeeID = "") then EmployeeID = -1 end if
	
	OpenDB()

	SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsName, SQL)
	
	If NOT rsName.EOF Then
		l_lastname = rsName("LASTNAME")
		l_firstname = rsName("FIRSTNAME")	
		FullName = "<font color=blue>" & l_firstname & " " & l_lastname & "</font>"
	
		l_rolefile = rsName("ROLEPATH")
	    if isnull(l_rolefile) or l_rolefile = "" then
	        l_rolefile_display = "none"
	    else   
	        l_rolefile_display = "block"	
	        'l_rolefile = "RoleFiles/" & l_rolefile
		l_rolefile = l_rolefile
	    end if		
	
	
	End If
	Call CloseRs(rsName)
	
	SQL = "SELECT * FROM E_JOBDETAILS WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
	
		' tbl E_JOBDETAILS
		
		l_startdate = rsLoader("STARTDATE")
		l_enddate = rsLoader("ENDDATE")
		l_probationperiod = rsLoader("PROBATIONPERIOD")
		l_reviewdate = rsLoader("REVIEWDATE")
		l_jobtitle = rsLoader("JOBTITLE")
		l_refnumber = rsLoader("REFERENCENUMBER")
		l_detailsofrole = rsLoader("DETAILSOFROLE")	
		l_team = rsLoader("TEAM")						
		l_patch = rsLoader("PATCH")						
		l_linemanager = rsLoader("LINEMANAGER")						
		l_partfulltime = rsLoader("PARTFULLTIME")						
		l_hours = rsLoader("HOURS")						
		l_grade = rsLoader("GRADE")
		l_salary = rsLoader("SALARY")						
		l_taxoffice = rsLoader("TAXOFFICE")
		l_otrate = rsLoader("OTRATE")								
		l_employeelimit = rsLoader("EMPLOYEELIMIT")								
		l_taxcode = rsLoader("TAXCODE")						
		l_payrollnumber = rsLoader("PAYROLLNUMBER")						
		l_drivinglicensenumber = rsLoader("DRIVINGLICENSENUMBER")
	
		l_ninumber = rsLoader("NINUMBER")						
		l_holidaydays = rsLoader("HOLIDAYENTITLEMENTDAYS")						
		l_holidayhours = rsLoader("HOLIDAYENTITLEMENTHOURS")								
		l_appraisaldate = rsLoader("APPRAISALDATE")
	
		l_buddy = rsLoader("BUDDY")
		l_noticeperiod = rsLoader("NOTICEPERIOD")
		l_dateofnotice = rsLoader("DATEOFNOTICE")
		l_ismanager = rsLoader("ISMANAGER")
		l_isdirector = rsLoader("ISDIRECTOR")
		l_foreignnationalnumber = rsLoader("FOREIGNNATIONALNUMBER")
		l_officelocation=rsLoader("OFFICELOCATION")
		l_active = rsLoader("ACTIVE")
		l_point=-1
		if  not isnull(rsLoader("GRADEPOINT")) then
			l_point=rsLoader("GRADEPOINT")
		end if
		ACTION = "AMEND"
	end if
	
	'if(IsNumeric(l_jobtitle)=false) then
	    
	 '   SQL=" select jobtitleid from e_jobtitles " &_
	 '   	" where jobtitle like '%" & l_jobtitle  & "%'"
	 '   Call OpenRs(rsLoader, SQL)
	    
	 '   if (NOT rsLoader.EOF) then
	 '          Response.Write(l_jobtitle)
	 '          l_jobtitle= rsLoader("jobtitleid")
	           
	 '   end if
	    	
	'end if
	'Response.Write("jobtitle:" & l_jobtitle)
	
	CloseRs(rsLoader)
	
	
	Call BuildSelect(lstnotice, "sel_NOTICEPERIOD", "E_NOTICEPERIODS", "NOTICEPERIODID, DESCRIPTION", "NOTICEPERIODID", "Please Select", l_noticeperiod, NULL, "textbox200", "onchange='calc_notice()' TABINDEX=3")
	Call BuildSelect(lstprobations, "sel_PROBATIONPERIOD", "E_PROBATIONPERIOD", "PROBATIONPERIODID, DESCRIPTION", "PROBATIONPERIODID", "Please Select", l_probationperiod, NULL, "textbox200", " TABINDEX=1")
	Call BuildSelect(lstpartfulltimes, "sel_PARTFULLTIME", "E_PARTFULLTIME", "PARTFULLTIMEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_partfulltime, NULL, "textbox200", "onchange='pop_up()' TABINDEX=2")
	Call BuildSelect(lstteams, "sel_TEAM", "E_TEAM WHERE TEAMID <> 1 AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "Please Select", l_team, NULL, "textbox200", "onchange='change_team()' TABINDEX=1")
	Call BuildSelect(lstpatchs, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "Please Select", l_patch, NULL, "textbox200", " TABINDEX=1")
	Call BuildSelect(lstgrades, "sel_GRADE", "E_GRADE", "GRADEID, DESCRIPTION", "GRADEID", "Please Select", l_grade, NULL, "textbox200", " TABINDEX=2")
	Call BuildSelect(lstPoint, "sel_GRADEPOINT", "E_GRADE_POINT", "POINTID, POINTDESCRIPTION", "POINTID", "Please Select", l_point, NULL, "textbox200", " TABINDEX=4")
	Call BuildSelect(lstjobtitles, "sel_JOBTITLE", "E_JOBTITLES", "JOBTITLE, JOBTITLE", "JOBTITLE", "Please Select", l_jobtitle, NULL, "textbox200", " TABINDEX=1")
	Call BuildSelect(lstofficelocation, "sel_OFFICELOCATION", "G_OFFICE", "OFFICEID, DESCRIPTION", "OFFICEID", "Please Select", l_officelocation, NULL, "textbox200", " TABINDEX=1")
	Call getList(lstbuddy, "bud", l_buddy)
	Call getList(lstlinemanager, "man", l_linemanager)
	
	CloseDB()
	
	// generates drop downs taking the table as an argument
	// criteria = 'manager' or 'director'
	Function getList(ByRef lst, str_what, selectedValue)
		
		Dim strSQL, rsSet, int_lst_record
		
		int_lst_record = 0
		if str_what = "man" Then
			strSQL = 	"SELECT 	E.EMPLOYEEID, E.FIRSTNAME + ' ' + LASTNAME AS FULLNAME " &_
			"FROM 		E__EMPLOYEE E " &_
			"			INNER 	JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
			"WHERE		J.ACTIVE = 1 AND ((J.TEAM = " & l_team & ") OR (ISDIRECTOR = 1)) AND J.EMPLOYEEID <> " & EmployeeID & " AND " &_
			"			J.GRADE >= 6 " 
			
			strSQL =  " SELECT 	E.EMPLOYEEID, E.FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
                      " FROM 		E__EMPLOYEE E "&_
	                  "    INNER 	JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID "&_
                      " WHERE		J.ACTIVE = 1 AND ((ISMANAGER=1)OR (ISDIRECTOR = 1)) "&_
                      "    AND J.EMPLOYEEID <> " & EmployeeID & " AND J.GRADE >= 6  "
			
			//response.write strSQL
		else
			strSQL = 	"SELECT 	E.EMPLOYEEID, E.FIRSTNAME + ' ' + LASTNAME AS FULLNAME " &_
						"FROM	 	E__EMPLOYEE E " &_
						"			INNER 	JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
						"WHERE		J.ACTIVE = 1 AND J.TEAM = (SELECT TEAM FROM E_JOBDETAILS WHERE EMPLOYEEID = " & EmployeeID & ") AND E.EMPLOYEEID <> " & EmployeeID
		End If		
							
		Call OpenRs(rsSet, strSQL)

		if (isNull(selectedValue)) then
			selectedValue = ""
		end if
		lst = "<option value=''>Please select</option>"
		While (NOT rsSet.EOF)
			isSelected = ""
			if (CStr(rsSet("EMPLOYEEID")) = CStr(selectedValue)) then
				isSelected = " selected"
			end if
			lst = lst & "<option value='" & rsSet("EMPLOYEEID") & "' " & isSelected & ">" & rsSet("FULLNAME") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		CloseRs(rsSet)
		
		If int_lst_record = 0 then
				lst = "<option value=''>No Team Members/Directors Exist</option>"
		End If			
			
	End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

var FormFields = new Array();
	FormFields[0] = "txt_STARTDATE|Start Date|DATE|Y"
	FormFields[1] = "txt_ENDDATE|End Date|DATE|N"
	FormFields[2] = "sel_PROBATIONPERIOD|Review Date|SELECT|N"
	FormFields[3] = "txt_REVIEWDATE|Review Date|DATE|N"
	FormFields[4] = "sel_JOBTITLE|JOB TITLE|SELECT|N"
	FormFields[5] = "txt_REFERENCENUMBER|Reference Number|TEXT|N"
	FormFields[6] = "txt_DETAILSOFROLE|Details of Role|TEXT|N"	
	
	FormFields[7] = "sel_TEAM|Team|SELECT|Y"
	FormFields[8] = "sel_PATCH|Patch|SELECT|N"
	FormFields[9] = "sel_LINEMANAGER|Line Manager|SELECT|Y"	
	FormFields[10] = "sel_BUDDY|Buddy|SELECT|N"
			
	FormFields[11] = "sel_PARTFULLTIME|Part/Full Time|SELECT|Y"
	FormFields[12] = "txt_HOURS|Hours|FLOAT|N|1"	
	FormFields[13] = "sel_GRADE|Grade|SELECT|N"						
	FormFields[14] = "txt_SALARY|Salary|CURRENCY|N"						
	FormFields[15] = "txt_TAXOFFICE|Tax Office|TEXT|N"								
	FormFields[16] = "txt_TAXCODE|Tax Code|TEXT|N"						
	FormFields[17] = "txt_PAYROLLNUMBER|Payroll Number|TEXT|N"						
	FormFields[18] = "txt_DRIVINGLICENSENUMBER|Driving License Number|TEXT|N"						
	FormFields[19] = "txt_NINUMBER|National Insurance Number|TEXT|N"									
	FormFields[20] = "txt_FOREIGNNATIONALNUMBER|Foreign National Number|TEXT|N"						
	FormFields[21] = "txt_HOLIDAYENTITLEMENTDAYS|Holiday Entitlement|FLOAT|N|1"						
	FormFields[22] = "txt_APPRAISALDATE|Appraisal Date|DATE|N"						
	FormFields[23] = "rdo_ISMANAGER|Manager|RADIO|Y"
	FormFields[24] = "rdo_ISDIRECTOR|Director|RADIO|Y"		
	FormFields[25] = "sel_NOTICEPERIOD|Notice Period|SELECT|N"
	FormFields[26] = "txt_DATEOFNOTICE|Date of Notice|DATE|N"
	FormFields[27] = "rdo_ACTIVE|Active|RADIO|Y"
	FormFields[28] = "txt_EMPLOYEELIMIT|Employee Limit|CURRENCY|Y"
	FormFields[29] = "txt_HOLIDAYENTITLEMENTHOURS|Holiday Entitlement|FLOAT|N|2"						
	FormFields[30] = "txt_OTRATE|Overtime Rate|FLOAT|N|2"						
	FormFields[31] = "sel_GRADEPOINT|Point|SELECT|N"
    //FormFields[32] = "sel_OFFICELOCATION|Office Location|SELECT|N"

function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/JobDetails_svr.asp"
	RSLFORM.submit()
	}
	
function pop_up() {

	if (RSLFORM.sel_PARTFULLTIME.value == 2)
		pt.style.visibility = "visible";
	else
		pt.style.visibility = "hidden";

}

	function change_team(){
		
		if (RSLFORM.sel_TEAM.value != ""){
			RSLFORM.target = "frm_post";
			RSLFORM.action = "ServerSide/changeTeam_srv.asp?employeeid=<%=EmployeeID%>" 
			RSLFORM.submit();
			}	
	}
	
	function addDays(myDate,days) {
    
		return new Date(myDate.getTime() + days*24*60*60*1000);
	}
	
	function calc_notice(){
		
		var new_date
		if (RSLFORM.sel_NOTICEPERIOD.value != "" && RSLFORM.txt_DATEOFNOTICE.value != "")
			if (!isDate("txt_DATEOFNOTICE", "Date of Notice")){
				alert("Date of Notice is invalid -- Please re-enter");
				RSLFORM.txt_DATEOFNOTICE.select();
				return false;
				}
			else {
				var mydate, period
				mydate = new Date(conv_date(RSLFORM.txt_DATEOFNOTICE.value));
				period = parseInt(RSLFORM.sel_NOTICEPERIOD.value) * 7;
				new_date = addDays(mydate, period);
				new_date = pad(new_date.getDate()) + "/" + pad((new_date.getMonth() + 1)) + "/" + new_date.getFullYear();
				RSLFORM.txt_ENDDATE.value = new_date			 
			}
	}
	
	// pads the day and month figues for returning the proposed enddate
	function pad(num){
	
		if (num < 10)
			return new String("0" + num);
		else
			return num;
	}
	
	function conv_date(datum){
	
		var split_date, arr_months, long_date
		
		arr_months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		split_date = datum.split("/");
		return split_date[0] + " " + arr_months[(split_date[1]-1)] + " " + split_date[2];
		
	}
	//function check_rentrecoveryofficer(patch)
	//{
	 //   alert(patch)
	 //   if(document.getElementById("sel_JOBTITLE").value=="Rent Recovery Officer")
	 //   { 
	  //      if(patch!="")
	  //         { 
	   //         RSLFORM.target = "frm_post";
		//	    RSLFORM.action = "ServerSide/Check_RentRecoveryOfficer_srv.asp?patch="+patch; 
		//	    RSLFORM.submit();
	//		   }
	//		else
	//		return true; 
	//    }
	 //   else
	 //   return true;
	//}
function open_window(url,width,height)
{
window.open(url,'mywindow','width='+ width +  ',height=' +  height + ',toolbar=no, location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no, resizable=yes')


}	
		
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages();checkForm();pop_up()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width=750 border=0 cellpadding=0 cellspacing=0>
  <tr> 
    <td rowspan=2> <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details"><img title="Personal Details" name="E_PD" src="Images/1-closed.gif" width=115 height=20 border=0></a></td>
    <td rowspan=2> <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info"><img alt="Contacts" name="E_C" src="images/2-closed.gif" width=74 height=20 border=0></a></td>
    <td rowspan=2> <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications"><img alt="Qualifications" name="E_SQ" src="images/3-closed.gif" width=94 height=20 border=0></a></td>
    <td rowspan=2 style='display:none'> <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment"><img alt="Previous Employment" name="E_PE" src="images/4-closed.gif" width=86 height=20 border=0></a></td>
    <td rowspan=2> <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities"><img alt="Difficulties/Disabilities" name="E_DD" src="Images/5-closed.gif" width=70 height=20 border=0></a></td>
    <td rowspan=2> <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details"><img alt="Job Details" name="E_JD" src="Images/6-open.gif" width=52 height=20 border=0></a></td>
    <td rowspan=2> <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits"><img alt="Benefits" name="E_B"  src="Images/7-previous.gif" width=82 height=20 border=0></a></td>
    <td width=265 height=19 align="right"><%=FullName%></td>
  </tr>
  <tr> 
    <td bgcolor=#133E71> <img src="images/spacer.gif" width=53 height=1></td>
  </tr>
  <tr> 
    <td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="images/spacer.gif" width=53 height=6></td>
  </tr>
</table>
<TABLE cellspacing=2  BORDER=0 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="90%">
  <form name="RSLFORM" method="POST">
    <TR> 
      <TD nowrap >Start Date:</td>
      <td> 
        <input type="textbox" class="textbox200" name="txt_STARTDATE" maxlength=10 value="<%=l_startdate%>" TABINDEX=1>
      </td>
      <td><image src="/js/FVS.gif" name="img_STARTDATE" width="15px" height="15px" border="0"> 
      </TD>
      <TD nowrap width=85px>Full/Part Time:</TD>
      <TD> <%=lstpartfulltimes%></TD>
      <TD><image src="/js/FVS.gif" name="img_PARTFULLTIME" width="15px" height="15px" border="0"></TD>
      <TD id='pt' title='View shift details' width=100% style='visibility:hidden;cursor:hand;color:#133e71' align=center onclick='window.open("PartTimePopUp.asp?EMPLOYEEID=<%=EmployeeID%>", "_blank", "width=325,height=280, left=300,top=300");'><font color=blue>View</font></TD>
    </TR>
    <TR> 
      <TD nowrap>Patch:</TD>
      <TD> <%=lstpatchs%> </TD>
      <TD><image src="/js/FVS.gif" name="img_PATCH" width="15px" height="15px" border="0"></TD>
      <TD>Hours (per week):</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_HOURS" maxlength=10 value="<%=l_hours%>" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD NOWRAP>Review Date:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_REVIEWDATE" MAXLENGTH=10 value="<%=l_reviewdate%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_REVIEWDATE" width="15px" height="15px" border="0"></TD>
      <TD>Grade:</TD>
      <TD><%=lstgrades%> </TD>
      <TD><image src="/js/FVS.gif" name="img_GRADE" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Job Title:</TD>
      <!--<TD>
        <input type="textbox" class="textbox200" name="txt_JOBTITLE" MAXLENGTH=100 value="<%=l_jobtitle%>" TABINDEX=1>
        <%=lstjobtitles%>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_JOBTITLE" width="15px" height="15px" border="0"></TD>-->
    <TD>
        <%=lstJOBTITLES%>
      </TD>
     <TD><image src="/js/FVS.gif" name="img_JOBTITLE" width="15px" height="15px" border="0"></TD>
     
      <TD>Point:</TD>
      <TD>
        <%=lstPoint%>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_GRADEPOINT" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Ref Number:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_REFERENCENUMBER" MAXLENGTH=20 value="<%=l_refnumber%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_REFERENCENUMBER" width="15px" height="15px" border="0"></TD>
      <TD>Salary:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_SALARY" maxlength=15 value="<%=l_salary%>" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_SALARY" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD rowspan=3 valign=top>Details of Role:</TD>
      <TD rowspan=3 valign=top>
        <textarea class="textbox200" name="txt_DETAILSOFROLE" rows=6 style='overflow:hidden' TABINDEX=1><%=l_detailsofrole%></textarea>
      </TD>
      <TD rowspan=3 VALIGN=TOP><image src="/js/FVS.gif" name="img_DETAILSOFROLE" width="15px" height="15px" border="0"></TD>
      <TD NOWRAP>Tax Office:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_TAXOFFICE" maxlength=50 value="<%=l_taxoffice%>" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_TAXOFFICE" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Tax Code:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_TAXCODE" MAXLENGTH=50 value="<%=l_taxcode%>" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_TAXCODE" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Payroll Number:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_PAYROLLNUMBER" MAXLENGTH=30 value="<%=l_payrollnumber%>" style="text-transform:uppercase" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_PAYROLLNUMBER" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
    
       
          <TD  valign=top>Role File:</TD>
      <TD  valign=top>
      <a href="#" onClick="open_window('/BusinessManager/Popups/EmployeeRole.aspx?empid=<%=EmployeeID%>','400','250')">Upload Role File</a>
	  <a id="lnkRolefile" href="Rolefiles/<% = l_rolefile  %>" target="_blank" style="display:<% = l_rolefile_display    %>" >View File</a>      
      </TD><td></td>
      
    
      <TD>Driving Lic Number:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_DRIVINGLICENSENUMBER" MAXLENGTH=30 value="<%=l_drivinglicensenumber%>" style="text-transform:uppercase" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_DRIVINGLICENSENUMBER" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD nowrap title="The amount in pounds an employee is allowed to create repairs for before a manager has to authorise it.">Employee 
        Limit: </TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_EMPLOYEELIMIT" MAXLENGTH=50 value="<%=l_employeelimit%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_EMPLOYEELIMIT" width="15px" height="15px" border="0"></TD>
      <TD nowrap >NI Number: </TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_NINUMBER" MAXLENGTH=10 value="<%=l_ninumber%>" style="text-transform:uppercase" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_NINUMBER" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Team:</TD>
      <TD><%=lstteams%></TD>
      <TD><image src="/js/FVS.gif" name="img_TEAM" width="15px" height="15px" border="0"></TD>
      <TD nowrap title="(work permit)">Foreign National Number: </TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_FOREIGNNATIONALNUMBER" MAXLENGTH=50 value="<%=l_foreignnationalnumber%>" style="text-transform:uppercase" TABINDEX=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_FOREIGNNATIONALNUMBER" width="15px" height="15px" border="0"> </TD>
    </TR>
    <TR> 
      <TD nowrap>Line Manager:</TD>
      <TD>
        <Select class=textbox200 name=sel_LINEMANAGER TABINDEX=1>
          <%=lstlinemanager %> 
        </Select>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_LINEMANAGER" width="15px" height="15px" border="0"></TD>
      <TD>Holiday Entitlement:</TD>
      <TD>
       <input style='width:50px' type="textbox" class="textbox100" name="txt_HOLIDAYENTITLEMENTDAYS" maxlength=10 value="<%=l_holidaydays%>" TABINDEX=2>
		(days)
		<image src="/js/FVS.gif" name="img_HOLIDAYENTITLEMENTDAYS" width="15px" height="15px" border="0">
        <input style='width:50px' type="textbox" class="textbox100" name="txt_HOLIDAYENTITLEMENTHOURS" maxlength=10 value="<%=l_holidayhours%>" TABINDEX=2>
		(hrs)
      </TD>
      <TD><image src="/js/FVS.gif" name="img_HOLIDAYENTITLEMENTHOURS" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Place of Work:</TD>
      <TD> 
            <%=lstofficelocation %>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_OFFICELOCATION" width="15px" height="15px" border="0"></TD>
      <TD>Appraisal Date:</TD>
      <TD>  <input type="textbox" class="textbox200" name="txt_APPRAISALDATE" maxlength=10 value="<%=l_appraisaldate%>" TABINDEX=2>
        </TD>
      <TD><image src="/js/FVS.gif" name="img_APPRAISALDATE" width="15px" height="15px" border="0"> </TD>
    </TR>
    <TR> 
            <TD>Buddy:</TD>
      <TD> 
        <select class=textbox200 name=sel_BUDDY tabindex=1>
          <%=lstbuddy %> 
        </select>
      </TD>  
      <TD><image src="/js/FVS.gif" name="img_BUDDY" width="15px" height="15px" border="0"></TD>
      <TD>Team Manager:</TD>
      <TD> 
	  	 <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_ismanager = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
        <input type="RADIO" name="rdo_ISMANAGER" value="1" <%=yesChecked%> TABINDEX=2>
        YES 
        <input type="RADIO" name="rdo_ISMANAGER" value="0" <%=noChecked%> TABINDEX=2>
        NO
       </TD>
      <TD><image src="/js/FVS.gif" name="img_ISMANAGER" width="15px" height="15px" border="0"></TD>
    </TR>
    <tr>
        <TD>Overtime Rate :</TD>
      <TD><input type="textbox" class="textbox200" name="txt_OTRATE" maxlength=10 value="<%=l_otrate%>" TABINDEX=2></TD>
      <TD><image src="/js/FVS.gif" name="img_OTRATE" width="15px" height="15px" border="0"></TD>
    
    </tr>
    <tr> 
      <td colspan=5>
        <hr style='border:1px dotted #133e71'>
      </td>
    </tr>
    <TR> 
      <TD>Notice Period:</TD>
      <TD> <%=lstnotice%> </TD>
      <TD><image src="/js/FVS.gif" name="img_NOTICEPERIOD" width="15px" height="15px" border="0"></TD>
      <TD>Director:</TD>
      <TD> 
	   <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_isdirector = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
        <input type="RADIO" name="rdo_ISDIRECTOR" value="1" <%=yesChecked%> TABINDEX=2>
        YES 
        <input type="RADIO" name="rdo_ISDIRECTOR" value="0" <%=noChecked%> TABINDEX=2>
        NO 
        </TD>
      <TD><image src="/js/FVS.gif" name="img_ISDIRECTOR" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Date of Notice:</TD>
      <TD>
        <input type="textbox" class="textbox200" name="txt_DATEOFNOTICE" maxlength=10 value="<%=l_dateofnotice%>" onblur='calc_notice()' TABINDEX=3>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_DATEOFNOTICE" width="15px" height="15px" border="0"></TD>
      <TD>Active:</TD>
      <TD>
        <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_active = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
        <input type="RADIO" name="rdo_ACTIVE" VALUE="1" <%=yesChecked%> TABINDEX=4>
        YES 
        <input type="RADIO" name="rdo_ACTIVE" VALUE="0" <%=noChecked%> TABINDEX=4>
        NO 
      </TD>
      <td><image src="/js/FVS.gif" name="img_ACTIVE" width="15px" height="15px" border="0">     </TD>
    </TR>
    <TR> 
      <TD nowrap>Probation Period:</TD>
      <TD><%=lstprobations%></TD>
      <TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
      <TD>End Date:</TD>
      <TD > 
        <input type="textbox" class="textbox200" name="txt_ENDDATE" maxlength=10 value="<%=l_enddate%>" TABINDEX=4>
      </TD>
	  <td><image src="/js/FVS.gif" name="img_ENDDATE" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
	<tr>
		<td colspan="5" align="right">
			<input type=hidden name="hid_EmployeeID" value="<%=EmployeeID%>">
			<input type=button class="RSLButton" value=" SAVE & NEXT " onClick="SaveForm()" name="button">
       	 <input type=hidden name="hid_Action" value="<%=ACTION%>">
		</td>
	</tr>
    <TR height="100%"> 
      <TD height="100%">&nbsp;</TD>
    </TR>
  </FORM>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_post SRC="ServerSide/changeTeam_srv.asp?employeeid=<%=EmployeeID%>" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
