<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_director, str_data, active

	active = Request.QueryString("active")
	If active = "" Then active=1

	Call get_patch()

	Function get_patch()

		Dim strSQL, rsSet, cnt

		cnt = 0
		strSQL =    " SELECT J.JOBTITLEID,J.JOBTITLE,COUNT(E.JOBTITLE) AS JOBTITLENUM "&_ 
					" FROM E_JOBTITLES J "&_
					" INNER JOIN E_JOBDETAILS E ON E.JOBTITLE=J.JOBTITLE AND E.ACTIVE=" & ACTIVE & " "&_
					" GROUP BY J.JOBTITLEID,J.JOBTITLE "&_
					"ORDER BY J.JOBTITLE "

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()

		str_data = str_data & "<tbody>"
		while not rsSet.EOF

			str_data = str_data & "<tr style=""cursor:pointer"">" &_
								  "<td onclick=""load_JobTitle(" & rsSet("JOBTITLEID") & ")"" width=""20px"" align=""center"">" & cnt+1 & "</td>" &_
								  "<td onclick=""load_JobTitle(" & rsSet("JOBTITLEID") & ")"" width=""300px"">" & rsSet("JOBTITLE") & "</td>" &_
								  "<td onclick=""load_JobTitle(" & rsSet("JOBTITLEID") & ")"" width=""20px"" ALIGN=CENTER>" & rsSet("JOBTITLENUM") & "</td>" &_
								  "<td style=""border:none"" class=""DEL"" onclick=""del_JobTitle(" & rsSet("JOBTITLEID") & ")"" title=""[DELETE] " & rsSet("JOBTITLE") & """ style=""cursor:pointer"">Del</td></tr>"
			cnt = cnt + 1
			rsSet.movenext()

		wend

		If cnt = 0 Then 
			str_data = "<tr><td colspan=""2"" align=""center""><b>No patchs exist within the system</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing
		str_data = str_data & "</tbody>"
	End function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Job Title Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_JobTitle|JobTitle|TEXT|Y"

        var theItems = new Array("txt_JobTitle");

        function click_save(str_action) {
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_JobTitle";
            document.RSLFORM.action = "serverside/JobTitle_srv.asp?path=" + str_action;
            document.RSLFORM.submit();
        }

        function Reload() {
            var act;
            act = document.getElementById("ddActive").options[document.getElementById("ddActive").selectedIndex].value;
            document.RSLFORM.action = "JobTitle.asp?active=" + act;
            document.RSLFORM.submit();
        }
        // called by delete button
        function del_JobTitle(int_id) {
            var answer
            answer = window.confirm("Are you sure you wish to delete this Job Title")
            // if yes then send to server page to delete
            if (answer) {
                document.RSLFORM.target = "frm_JobTitle";
                document.RSLFORM.action = "serverside/JobTitle_srv.asp?path=del&JobTitle_id=" + int_id;
                document.RSLFORM.submit();
            }
        }

        // called when user clicks on a row
        function load_JobTitle(int_id) {
            document.RSLFORM.target = "frm_JobTitle";
            document.RSLFORM.action = "serverside/JobTitle_srv.asp?path=load&JobTitle_id=" + int_id;
            document.RSLFORM.submit();
        }

        // swaps save button for amend button and vice versa
        // called by server side page
        function swap_button(int_which_one) { //   1 = show amend, 2 = show save		
            if (int_which_one == 2) {
                document.getElementById("BTN_SAVE").style.display = "block"
                document.getElementById("BTN_AMEND").style.display = "none"
            }
            else {
                document.getElementById("BTN_SAVE").style.display = "none"
                document.getElementById("BTN_AMEND").style.display = "block"
            }
        }


        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        function swap_div(int_which_one) {
            if (int_which_one == 1) {
                document.getElementById("new_div").style.display = "none";
                document.getElementById("table_div").style.display = "block";
                document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle-over.gif';
                document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle-tab_jobtitle_ove.gif';
                document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle.gif';
            }
            else if (int_which_one == 2) {
                clear()
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle.gif';
                document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle-over.gif';
                document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle-tab_new_pat.gif';
            }
            else {
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle.gif';
                document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle.gif';
                document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle-over.gif';
            }
            checkForm()
        }

        function clear() {
            document.getElementById("txt_JobTitle").value = "";
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_JobTitle" id="tab_JobTitle" onclick="swap_div(1)" style="cursor: pointer" src="images/tab_jobtitle-over.gif"
                    width="55" height="20" border="0" alt="Job Title" />
            </td>
            <td rowspan="2">
                <img name="tab_new_JobTitle" id="tab_new_JobTitle" onclick="swap_div(2)" style="cursor: pointer" src="images/tab_new_jobtitle-tab_jobtitle_ove.gif"
                    width="89" height="20" border="0" alt="New Job Title" />
            </td>
            <td rowspan="2">
                <img name="tab_amend_JobTitle" id="tab_amend_JobTitle" src="images/tab_amend_jobtitle.gif" width="112" height="20"
                    border="0" alt="Amend Job Title" />
            </td>
            <td>
                <img src="images/spacer.gif" width="494" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="494" height="1" alt="" /></td>
        </tr>
    </table>
    <div id="new_div" style="display: none">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2">
            <thead>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td class='TABLE_HEAD' style="width: 50px;">
                        <b>Job Title</b>
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" class="textbox200" name="txt_JobTitle" id="txt_JobTitle" maxlength="100" size="50" />
                    </td>
                    <td nowrap="nowrap" width="50px">
                        <img src="/js/FVS.gif" name="img_JobTitle" id="img_JobTitle" width="15px" height="15px" border="0" alt="" />
                    </td>
                    <td nowrap="nowrap" width="100px">
                        <input type="button" name="BTN_SAVE" id="BTN_SAVE" value="Save" title="Save" class="rslbutton" onclick="click_save('new')" style="cursor:pointer" />
                        <input type="button" name="BTN_AMEND" id="BTN_AMEND" value="Amend" title="Amend" class="rslbutton"
                            onclick="click_save('amend')" style="display: none;cursor:pointer" />
                    </td>
                    <td width="100%">
                        &nbsp;
                    </td>
                </tr>
                </thead>
        </table>
    </div>
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="border-collapse: COLLAPSE;
            behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="1">
            <thead>
                <tr style="height: 3PX">
                    <td class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td class='TABLE_HEAD'>
                    </td>
                    <td class='TABLE_HEAD'>
                        <table border="2">
                            <tr>
                                <td class='TABLE_HEAD' width="100" align="right">
                                    <b>Job Type</b>
                                </td>
                                <td class='TABLE_HEAD'>
                                    <select id="ddActive" name="ddActive" class="textbox100" onchange="Reload();">
                                        <option value="1" <% if active=1 then response.write("selected")%>>Active</option>
                                        <option value="0" <% if active=0 then response.write("selected")%>>Inactive</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="20px" align="center" class='TABLE_HEAD'>
                        <b>ID</b>
                    </td>
                    <td width="100%" class='TABLE_HEAD'>
                        <b>Job Title</b>
                    </td>
                    <td width="20px" class='TABLE_HEAD'>
                        <b>Emps</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD' colspan="4" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
        <input type="hidden" name="hd_JobTitle_id" id="hd_JobTitle_id" />
    </div>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_JobTitle" id="frm_JobTitle" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>
