<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="Connections/db_connection.asp" -->
<%
Dim Session
Set Session = server.CreateObject("SessionMgr.Session2")

	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL, EMPSQL
	Dim PrevSelected, allSelected, EmployeeType,OrderBy 

	Call GetVariables()
	Call getTeams()
	'Call GetDepartmentReportVars()

	Function GetVariables()

		PrevSelected =  ""
		AllSelected = ""
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")

		If OrderBy <> "" Then
			orderSQL =  " ORDER BY  " & OrderBy
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End If

		If EmployeeType = "Previous" Then			
            EMPSQL =  " AND ( (J.ENDDATE IS NOT NULL AND (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') <= GETDATE())) OR J.ACTIVE IN (0)) "
		ElseIf EmployeeType = "All" Then
			EMPSQL =  " "
		Else
			EMPSQL =  " AND ((J.ENDDATE IS NULL OR (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') > GETDATE())) AND J.ACTIVE = 1) "
		End If

		If Dapartment <> "" Then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End If

		If theDate <> "" Then
			whereNeeded = 1
			If deptSQL <> "" Then
			    dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End If

		If whereNeeded = 1 Then
			whereSQL = " AND " & deptSQL & dateSQL
		End If

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		str_data = ""
		strSQL = 		"SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(EG.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE " &_
						"FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	lEFT JOIN E_GRADE EG ON EG.GRADEID=CAST(J.GRADE AS INT) " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL & orderSQL 

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		str_data = str_data &  "<tbody>"

		while not rsSet.eof

				str_data = str_data & 	"<tr>" &_
										"<td>" & rsSet("FULLNAME") & "</td>" &_
										"<td align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
										"<td align=""center"">" & rsSet("DEPARTMENT") & "</td>" &_
										"<td align=""center"">" & rsSet("GENDER") & "</td>"&_
										"<td align=""center"">" & rsSet("ETHNICITY")  & "</td>" &_
										"<td align=""center"">" & rsSet("DOB") & "</td>"&_
										"<td align=""center"">" & rsSet("SALARY") & "</td>"&_
										"<td align=""center"">" & rsSet("GRADE") & "</td>"&_
										"<td align=""center"">" & rsSet("STARTDATE") & "</td>"&_
										"<td align=""center"">" & rsSet("ENDDATE") & "</td>"
					str_data = str_data & "</tr>"

		rsSet.MoveNext()
		wend

		rsSet.Close()

		str_data = str_data &  "</tbody>"

	End function


	Function GetDepartmentReportVars()

				sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
						"			SUM(	CASE WHEN J.STARTDATE = '" & theDate & "'    " &_
						"				THEN 1    " &_
						"				ELSE  0    " &_
						"    		END )AS STARTERS, " &_
						"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS MALETOTAL, " &_
						"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS FEMALETOTAL, " &_
						"			SUM(	CASE WHEN J.ENDDATE = '" & theDate & "' " &_ 
						"				THEN 1    " &_ 
						"				ELSE  0   " &_
						"    		END )AS LEAVERS " &_
						"FROM	E__EMPLOYEE E   " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL  & EMPSQL 

				set rsLoader = Server.CreateObject("ADODB.Recordset")
				    rsLoader.ActiveConnection = RSL_CONNECTION_STRING
				    rsLoader.Source = strSQL
				    rsLoader.CursorType = 3
				    rsLoader.CursorLocation = 3
				    rsLoader.Open()

				If (NOT rsLoader.EOF) Then
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
					TotalSalary = FormatCurrency(rsLoader("SALARY"))
					TotalStaff	= rsLoader("TOTALSTAFF")
					maletotal	= rsLoader("MALETOTAL")
					femaletotal	= rsLoader("FEMALETOTAL")
					NewStarters	= rsLoader("STARTERS")
					Leavers		= rsLoader("LEAVERS")
				'==================================================================
				'= End Totals For department report
				'==================================================================	
				End If

				rsLoader.Close()

	End Function
%>
<html>
<head>
    <title>RSL Manager Business</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body bgcolor="#ffffff">
    <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                Name
            </td>
            <td>
                Job Title
            </td>
            <td>
                Dept.
            </td>
            <td>
                Gender
            </td>
            <td>
                Ethnicity
            </td>
            <td>
                DOB
            </td>
            <td>
                Salary
            </td>
            <td>
                Grade
            </td>
            <td>
                Start Date
            </td>
            <td>
                End Date
            </td>
        </tr>
        <%=str_data%>
    </table>
</body>
</html>
