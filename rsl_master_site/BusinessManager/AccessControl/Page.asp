<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
Moi = Request("hid_Me")
if (Moi = "") then Moi = -1 end if

mParent = Request.Form("hid_Parent")

OpenDB()
SQL = "SELECT P.*, M.PAGE AS MPAGE, M.DESCRIPTION AS MNAME, MO.DIRECTORY AS MODIRECTORY, MO.DESCRIPTION AS MODESCRIPTION FROM AC_PAGES P, AC_MENUS M, AC_MODULES MO WHERE P.MENUID = M.MENUID AND M.MODULEID = MO.MODULEID AND PAGEID = " & Moi
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then
	l_description = rsLoader("DESCRIPTION")
	l_active = rsLoader("ACTIVE")
	l_directory = rsLoader("MODIRECTORY")
	l_page = rsLoader("PAGE")
	l_pageid = rsLoader("PAGEID")						
	l_menuid = rsLoader("MENUID")
	l_ordertext = rsLoader("ORDERTEXT")
	l_link = rsLoader("LINK")

	lt_action = "Update Page"			
	lt_module = rsLoader("MODESCRIPTION")
	lt_directory = rsLoader("MODIRECTORY")
	lt_menu = rsLoader("MNAME")
	lt_mpage = rsLoader("MPAGE")
	lt_description = l_description
	lt_page = ", (" & rsLoader("PAGE") & ")"
	ACTION = "AMEND"
else
	CloseRs(rsLoader)
	SQL = "SELECT MENUID, M.PAGE AS MPAGE, M.DESCRIPTION AS MNAME, MO.DIRECTORY AS MODIRECTORY, MO.DESCRIPTION AS MODESCRIPTION FROM AC_MENUS M, AC_MODULES MO WHERE M.MODULEID = MO.MODULEID AND MENUID = " & mParent
	Call OpenRs(rsLoader, SQL)
	l_directory = rsLoader("MODIRECTORY")
	l_menuid = rsLoader("MENUID")

	lt_action = "New Page"			
	lt_module = rsLoader("MODESCRIPTION")
	lt_directory = rsLoader("MODIRECTORY")
	lt_menu = rsLoader("MNAME")
	lt_mpage = rsLoader("MPAGE")
	lt_description = l_description
	lt_page = ""
	lt_description = "..."
	lt_page = ""
end if

CloseRs(rsLoader)
CloseDB()
%>
<html>
<head>
<title>Menu Page</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>
var FormFields = new Array();
FormFields[0] = "txt_DESCRIPTION|Description|TEXT|Y"
FormFields[1] = "rdo_ACTIVE|Active|RADIO|Y"
FormFields[2] = "rdo_LINK|Menu Link|RADIO|Y"
FormFields[3] = "txt_PAGE|Page|TEXT|Y"
FormFields[4] = "txt_ORDERTEXT|Order|TEXT|N"
	
function SaveForm(){
	if (!checkForm()) return false;
	location.href = "ActionCompleted.asp"
	RSLFORM.submit()
	}

function DeleteItem(){
	RSLFORM.hid_ACTION.value = "DELETE"
	location.href = "ActionCompleted.asp"
	RSLFORM.submit();
	}

function ClearItem(el){
	document.getElementById(el).value = ""
	}
	
var TargetElement = ""

function getData(data){
	document.getElementById(TargetElement).value = data
	}

function OpenDirectory(el, MiniRoot, openType) {
	NewWindow = window.open('browser/browser.asp?FileFolders=' + openType + "&MiniRoot=" + MiniRoot,'browserWindow','width=600,height=400,scrollbars=yes,menu=no,')
	TargetElement = el
	}

function OpenPage(el, parDir, openType) {
	parVal = document.getElementById(parDir).value
	OpenDirectory(el, parVal, openType)
	}

function CancelAction(){
	location.href = "ActionCompleted.asp?Text="
	}
</script>
<body bgcolor="#FFFFFF" text="#000000">
<form name="RSLFORM" method="POST" action="ServerSide/Page_svr.asp" target="theSideBar">
<table align="center">
	<tr><td colspan=6>
	<!--Start of MiniTree -->

		<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk'>
			<tr><td><b><u><%=lt_action%></u></b></td></tr><tr><td>&nbsp;</td></tr>
			<tr><td valign=center>&nbsp;<%=lt_module%>, (<%=lt_directory%>)</td></tr>
			<tr><td valign=center><img src='/js/tree/img/empty.gif' width='18' height='18' align='absmiddle'><img src='/js/tree/img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<%=lt_menu%>, (<%=lt_mpage%>)</td></tr>
			<tr><td valign=center><img src='/js/tree/img/empty.gif' width='18' height='18' align='absmiddle'><img src='/js/tree/img/line.gif' width='18' height='18' align='absmiddle'></td></tr>
			<tr><td valign=center><img src='/js/tree/img/empty.gif' width='18' height='18' align='absmiddle'><img src='/js/tree/img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<%=lt_description%><%=lt_page%></td></tr>
			<tr><td>&nbsp;</td></tr>
		</table>
	
	<!--End of MiniTree -->	
	</td></tr>
	<tr><td>Name: </td><td><input type="TEXT" name="txt_DESCRIPTION" value="<%=l_description%>" class="textbox200"></td><td><img name="img_DESCRIPTION" src="/js/FVS.gif" width=15 height=15 border=0></TD></tr>
		<% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_active <> 1) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
	<tr><td>Active: </td><td><input type="RADIO" name="rdo_ACTIVE" value="1" <%=yesChecked%>> YES <input type="RADIO" name="rdo_ACTIVE" value="0" <%=noChecked%>> NO</td><td><img name="img_ACTIVE" src="/js/FVS.gif" width=15 height=15 border=0></TD></tr>
		<% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_link <> 1) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
	<tr><td>Menu Link: </td><td><input type="RADIO" name="rdo_LINK" value="1" <%=yesChecked%>> YES <input type="RADIO" name="rdo_LINK" value="0" <%=noChecked%>> NO</td><td><img name="img_LINK" src="/js/FVS.gif" width=15 height=15 border=0></TD></tr>
	<tr><td>Directory: </td><td><input type="TEXT" name="txt_DIRECTORY" value="<%=l_directory%>" class="textbox200" readonly></td></tr>
	<tr><td>Page: </td><td><input type="TEXT" name="txt_PAGE" value="<%=l_page%>" class="textbox200"></td><td><img name="img_PAGE" src="/js/FVS.gif" width=15 height=15 border=0></TD><td><input type="button" value="SET" class="RSLButtonSmall" onclick="OpenPage('txt_PAGE', 'txt_DIRECTORY', 3)"></td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_PAGE')"></td></tr>
	<tr><td>Order: </td><td><input type="TEXT" name="txt_ORDERTEXT" value="<%=l_ordertext%>" class="textbox200"></td><td><img name="img_ORDERTEXT" src="/js/FVS.gif" width=15 height=15 border=0></TD></tr>
	<tr><td colspan=2 align=right><input type="button" value=" SAVE " onclick="SaveForm()" class="RSLButton">
        <input type="button" value=" CANCEL " onClick="CancelAction()" class="RSLButton">
      </TD></TR>
	<tr><td colspan=2 align=right><input type="button" value=" DELETE " onclick="DeleteItem()" class="RSLButton"></TD></TR>	
</table>
<input type="hidden" name="hid_MENUID" VALUE="<%=l_menuid%>">
<input type="hidden" name="hid_PAGEID" VALUE="<%=l_pageid%>">
<input type="hidden" name="hid_ACTION" VALUE="<%=ACTION%>">
</form>
<!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
