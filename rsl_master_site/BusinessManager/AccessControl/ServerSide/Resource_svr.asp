<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (3)
Dim DataTypes    (3)
Dim ElementTypes (3)
Dim FormValues   (3)
Dim FormFields   (3)
UpdateID	  = "hid_RESOURCEID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "rdo_ACTIVE|TEXT"
FormFields(2) = "txt_PAGE|PATH"
FormFields(3) = "hid_PAGEID|TEXT"

Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO AC_RESOURCES " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO("Resource Created Successfully")
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE AC_RESOURCES " & strSQL & " WHERE RESOURCEID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO("Resource Updated Successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	dim rsRef
		
	SQL = "DELETE FROM AC_RESOURCES WHERE RESOURCEID = " & ID
	Conn.Execute SQL, recaffected
	GO("Resource Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "SiteTree.asp?Text=" & Text
End Function

TheAction = Request("hid_ACTION")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>