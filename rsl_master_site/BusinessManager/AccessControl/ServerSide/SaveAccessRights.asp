<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<body>
<%
EmployeeID = Request("EmployeeID")

' Open the database and delete all records for the employee
OpenDB()
Conn.Execute  "DELETE FROM AC_NOACCESS WHERE EMPLOYEEID = " & EmployeeID, recaffected

for each d in Request.Form
	' Obtain current item from the request collection
	currItem = Request.Form(d)
	' Check if the item has all of its child records checked or not (1)
	currArray = Split(currItem, "|||")
	if (UBound(currArray) = 1) then
		isPartial = 1
	else
		isPartial = 0
	end if
	' End of (1)
	
	' Aplit the actual data and get the level and itemid
	currSplits = Split(currArray(0), "_")
	
	' Build SQL and insert row into database
	InsertSQL = "INSERT INTO AC_NOACCESS (EMPLOYEEID, ACCESSLEVEL, ACCESSITEM, LIMITEDACCESS) " &_
				"VALUES (" & EmployeeID & "," & currSplits(0) & "," & currSplits(1) & "," & isPartial & ")"
	Conn.Execute  InsertSQL, recaffected
	
next
CloseDB()

Response.Redirect "ControlTree.asp?EmployeeID=" & EmployeeID & "&Text=" & Request("Text")
%>
</body>
</html>