<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (5)
Dim DataTypes    (5)
Dim ElementTypes (5)
Dim FormValues   (5)
Dim FormFields   (5)
UpdateID	  = "hid_PAGEID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "rdo_ACTIVE|TEXT"
FormFields(2) = "rdo_LINK|TEXT"
FormFields(3) = "txt_PAGE|PATH"
FormFields(4) = "txt_ORDERTEXT|TEXT"
FormFields(5) = "hid_MENUID|TEXT"

Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO AC_PAGES " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO("Page Created Successfully")
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE AC_PAGES " & strSQL & " WHERE PAGEID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO("Page Updated Successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	dim rsRef
	SQL = "SELECT RESOURCEID FROM AC_RESOURCES WHERE PAGEID = " & ID
	Call OpenRs(rsRef, SQL)
	if (NOT rsRef.EOF) then
		Call CloseRs(rsRef)
		GO("Page cannot be deleted because it has RESOURCES underneath it.")
	end if
	Call CloseRs(rsRef)
				
	SQL = "DELETE FROM AC_PAGES WHERE PAGEID = " & ID
	Conn.Execute SQL, recaffected
	GO("Page Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "SiteTree.asp?Text=" & Text
End Function

TheAction = Request("hid_ACTION")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>