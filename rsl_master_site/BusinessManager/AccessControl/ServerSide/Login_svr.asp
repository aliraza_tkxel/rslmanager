<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (6)
Dim DataTypes    (6)
Dim ElementTypes (6)
Dim FormValues   (6)
ReDim FormFields   (6)
UpdateID	  = "hid_LOGINID"
FormFields(0) = "txt_LOGIN|TEXT"
FormFields(1) = "txt_PASSWORD|TEXT"
FormFields(2) = "rdo_ACTIVE|TEXT"
FormFields(3) = "hid_EMPLOYEEID|NUMBER"
FormFields(4) = "cde_MODIFIEDBY|" & Session("UserID")
FormFields(5) = "cde_DATEMODIFIED|'" & FormatDateTime(date,1) & "'"
'Stick all items just for insert at the end then , these can be removed with a redim in the update
FormFields(6) = "cde_CREATEDBY|" & Session("UserID")

Function NewRecord ()
	ID = Request.Form(UpdateID)

	dim rsRef
	SQL = "SELECT LOGIN FROM AC_LOGINS WHERE LOGIN = '" & REQUEST("txt_LOGIN") & "'"
	Call OpenRs(rsRef, SQL)
	if (NOT rsRef.EOF) then
		Call CloseRs(rsRef)
		GO("The username you have entered is already in use. Please try another.")
	end if
	Call CloseRs(rsRef)
	
	Call MakeInsert(strSQL)
	SQL = "INSERT INTO AC_LOGINS " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO("Employee Login Created Successfully")
End Function

Function AmendRecord()
	ReDim Preserve FormFields(5) 'we dont want to update the created by user
	
	ID = Request.Form(UpdateID)	
	dim rsRef
	SQL = "SELECT LOGIN FROM AC_LOGINS WHERE LOGIN = '" & REQUEST("txt_LOGIN") & "' AND LOGINID <> " & ID
	rESPONSE.wRITE sql
	
	Call OpenRs(rsRef, SQL)
	if (NOT rsRef.EOF) then
		Call CloseRs(rsRef)
		GO("The username you have entered is already in use. Please try another.")
	end if
	Call CloseRs(rsRef)

	if (Request.Form("txt_PASSWORD") = "") then
		SQL = "UPDATE AC_LOGINS SET LOGIN = '" & Request.Form("txt_LOGIN") & "', ACTIVE = " & Request.Form("rdo_ACTIVE") & ", MODIFIEDBY = " & Session("UserID") & ", DATEMODIFIED = '" & FormatDateTime(date,0) & "' WHERE LOGINID = " & ID
	else
		Call MakeUpdate(strSQL)
		SQL = "UPDATE AC_LOGINS " & strSQL & " WHERE LOGINID = " & ID
	end if
	
	Conn.Execute SQL, recaffected
	GO("Employee Login Updated Successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	dim rsRef
		
	SQL = "DELETE FROM AC_LOGINS WHERE LOGINID = " & ID
	Conn.Execute SQL, recaffected
	GO("Employee Login Deleted Successfully")
End Function


Function UnlockAccount()
	ID = Request.Form(UpdateID)	
	dim rsRef
	SQL = "UPDATE dbo.AC_LOGINS SET  ISLOCKED = 0, THRESHOLD = 0 WHERE  LOGINID = " & ID
    	   	
	Conn.Execute SQL, recaffected
	GO("Account Unlocked Successfully")
End Function


Function GO(Text)
	CloseDB()
	Response.Redirect "../AccessControl.asp?EmployeeID=" & Request("hid_EMPLOYEEID") & "&Text=" & Text
End Function

TheAction = Request("hid_ACTION")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
    Case "Unlock"   UnlockAccount()
End Select
CloseDB()
%>