<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<head>
    <link rel="StyleSheet" href="/js/tree/dtree.css" type="text/css" />
    <script type="text/javascript" language="JavaScript" src="/js/tree/dtreeCheckbox.js"></script>
    <script type="text/javascript" language="JavaScript">
// The function begins the process of checking and un checking madness
function changeImage(iItem) {
	itemsrc = iItem.src
	elid = iItem.name
	//alert(elid)
	elid = elid.split(":::")
	//alert(elid[1])	
	elid = document.getElementById(elid[1])
	if (itemsrc.indexOf("Check_1.gif") != -1){
		iItem.src = "/js/tree/img/Check_3.gif"
		elid.checked = true;
		}
	else if (itemsrc.indexOf("Check_3.gif") != -1){		
		iItem.src = "/js/tree/img/Check_1.gif"
		elid.checked = false;
		}
	else if (itemsrc.indexOf("Check_2.gif") != -1){		
		iItem.src = "/js/tree/img/Check_3.gif"
		elid.checked = true;
		elid.style.backgroundColor = "silver"
		}
		CheckMad(elid)
	}
	
function CheckMad(cItem){
	initialStatus = true
	if (cItem.style.backgroundColor == "silver" || cItem.checked == true){
		cItem.checked = true
		cItem.style.backgroundColor = ""
		}
	else 
		initialStatus = false
	// (3) Removes the partially filled data bit from thew check box value
	var iValue = cItem.value
	var iArray = iValue.split("|||")
	cItem.value = iArray[0]
	// End of (3)
	SetChildren(cItem, initialStatus)
	CheckParents(cItem)
	}

// This function recursively calls itselef until it reeaches the top level. 
// It uses the SetParents() function to set the status of each level parent appropriately.
function CheckParents(iItem){
	if (iItem.id != "") {
		var obj = document.getElementsByName(iItem.id);
		SetParents(iItem)
		CheckParents(obj[0])
		}
	}

// This function sets the status and colour of all parent checkbox nodes.
// It does this by loooping thorugh the collection of each item and then storing the results
// then using the results to set the parent element appropriately.
function SetParents(iItem){
	var obj = document.getElementsByName(iItem.id);
	var checkedcount = false
	var notcheckedcount = false
	var mixedcount = false
	for (var i=0; i<obj.length; i++){
		if (obj[i].name != iItem.id){
			if (obj[i].checked == true && obj[i].style.backgroundColor == "")
				checkedcount = true
			else if (obj[i].checked == true && obj[i].style.backgroundColor == "silver")
				mixedcount = true
			else
				notcheckedcount = true
			}
		}
	if ((checkedcount && notcheckedcount) || mixedcount)
		SetItem(iItem, "silver", true)
	else if (checkedcount)
		SetItem(iItem, "", true)
	else if (notcheckedcount)
		SetItem(iItem, "", false)
	}

// This function Sets an individual checkbox items color status and value
// depending on values sent to it.
function SetItem(dItem, iColor, bool){
	document.getElementById(dItem.id).style.backgroundColor = iColor
	document.getElementById(dItem.id).checked = bool
	// (1) this section changes the value of the check name to state that its children are paritally filled
	// This is required so that the database can return the appropriate colour.
	if (iColor == "silver"){			
		var iValue = document.getElementById(dItem.id).value
		var iArray = iValue.split("|||")
		document.getElementById(dItem.id).value = iArray[0] + "|||" + 1
		}
	// This undoes what the top part does (1).
	else {
		var iValue = document.getElementById(dItem.id).value
		var iArray = iValue.split("|||")
		document.getElementById(dItem.id).value = iArray[0]			
		}
	}

// This function loops through the hierachy and sets the checkboxes to checked
// or not checked depending on the values sent to the function. The function is 
// recursive.
function SetChildren(iItem, bool){
	var obj = document.getElementsByName(iItem.name);
	if (obj.length > 1) {
		for (var i=0; i<obj.length; i++){
			if (obj[i].name != iItem.name) {
				obj[i].style.backgroundColor = ""
				obj[i].checked = bool;
				// (2) Removes the partially filled data bit from thew check box value
				var iValue = obj[i].value;
				var iArray = iValue.split("|||");
				obj[i].value = iArray[0];
				// End of (2)
				// Recursive call to itself...				
				SetChildren(obj[i], bool);
				}
			}
		}
	}

function OpenData(val){
	alert(val)
	}

function SubmitPage(){
	try {
		parent.document.getElementById("everything").innerHTML = "Please Wait Saving..."
		}
	catch (e){
		temp = ""
		}
	document.thisForm.submit();
	}

function SetText(){
	if ("<%=Request("Text")%>" != "") {
		try{
			parent.document.getElementById("everything").innerHTML = "Saved User Access<br>Rights Successfully"
			}
		catch (e){
			temp = ""
			}
		}
	}
    </script>
    <title>Server Access Control</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body marginheight="0" leftmargin="10" topmargin="10" marginwidth="0" bgcolor="beige"
    text="#000000" onload="SetText()" border="none" style="scrollbar-face-color: #133E71;
    scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71;
    scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
    <div class="dtree" style="height: 100%">
        <form name="thisForm" action="SaveAccessRights.asp?EmployeeID=<%=Request("EmployeeID")%>&Text=Save"
        method="post">
        <script type="text/javascript">
	<!--
	d = new dTree('d');
	d.config.useStatusText=true;
	d.add(0,-1,'Access Control');
	
	<%
	EMPLOYEEID = Request("EmployeeID")
	
	'this part sets the supplier filter if it is appropriate
	Call OpenDB()
	DefaultModuleList = ""
	SQL = "SELECT ORGID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EMPLOYEEID
	Call OpenRS(rsUserType, SQL)
	if (NOT rsUserType.EOF) then
		if (NOT isNull(rsUserType("ORGID")) AND rsUserType("ORGID") <> "") then
			SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SUPPLIERACCESSLIST'"
			Call OpenRs(rsDefault, SQL)
			if (NOT rsDefault.EOF) then
				DefaultModuleList = " AND MODULEID IN (" & rsDefault("DEFAULTVALUE") & ") "
			else
				DefaultModuleList = " AND MODULEID IN (-1) "		
			end if
			CloseRs(rsDefault)
		else
			'do not show contractor modules for internal users...
			SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SUPPLIERACCESSLIST'"
			Call OpenRs(rsDefault, SQL)
			if (NOT rsDefault.EOF) then
				DefaultModuleList = " AND MODULEID NOT IN (" & rsDefault("DEFAULTVALUE") & ") "
			end if
			CloseRs(rsDefault)
		end if
	end if
	Call CloseRs(rsUserType)
	Call CloseDB()
	'end of the supplier filter 

	Dim rstMenus, rstPages, rstBudgets, fundi, codei, budgeti, headi
	
	' Use a datashaping connection string to be used later.
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING
	
	' The sql below joins the tables AC_MODULES, AC_MENUS and AC_PAGES in a hierachial manner so that data can be retieved easily.
	' Join is as follows			AC_MODULES ---�
	'											  AC_MENUS ----� 
	'														   AC_PAGES 
	
	strSQL = "SHAPE {SELECT MODULEID, DESCRIPTION, ACCESSITEM, LIMITEDACCESS FROM AC_MODULES LEFT JOIN AC_NOACCESS ON MODULEID = ACCESSITEM AND ACCESSLEVEL = 1 AND EMPLOYEEID = " & EMPLOYEEID & " WHERE ACTIVE = 1 AND ALLACCESS = 0 " & DefaultModuleList & " ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "	APPEND((SHAPE {SELECT MENUID, DESCRIPTION, MODULEID, ACCESSITEM, LIMITEDACCESS FROM AC_MENUS LEFT JOIN AC_NOACCESS ON MENUID = ACCESSITEM AND ACCESSLEVEL = 2 AND EMPLOYEEID = " & EMPLOYEEID & " WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "		APPEND((SHAPE {SELECT PAGEID, DESCRIPTION, MENUID, ACCESSITEM, LIMITEDACCESS  FROM AC_PAGES LEFT JOIN AC_NOACCESS ON PAGEID = ACCESSITEM AND ACCESSLEVEL = 3 AND EMPLOYEEID = " & EMPLOYEEID & " WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION}) AS PAGES "&_
			 "	RELATE MENUID TO MENUID)) AS MENUS "&_
			 "RELATE MODULEID TO MODULEID)"
	
	' Open original recordset
	Set rst = Server.CreateObject("ADODB.Recordset")
	rst.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
	
	fundi = 1
	currenti = 5
	headparent = 0
	codeparent = 0
	budgetparent = 0
	
	' The initial while loops through the main recordset (containing the AC_MODULES data) 
	Do While Not rst.EOF
		ModuleID = rst("MODULEID")
		ModuleCode = "M" & ModuleID
		Response.Write "d.add(" & currenti & ",0,'" & rst("DESCRIPTION") & "','" & ModuleCode & "','','1_" & ModuleID & "','" &  rst("ACCESSITEM") & "','" &  rst("LIMITEDACCESS") & "','/js/tree/img/module.gif');" & VbCrLf
		currenti = currenti + 1
	
		' A new recordset reference is declared and is set to the children of the current reccord in 
		' AC_MODULES. The child records are from the AC_MENUS data
		Set rstMenus = rst("MENUS").Value
		if not rstMenus.EOF then
			headparent = currenti - 1
			Do While Not rstMenus.EOF
				MenuID = rstMenus("MENUID")
				Response.Write "d.add(" & currenti & "," & headparent & ",'" & rstMenus("DESCRIPTION") & "','" & ModuleCode & "_" & MenuID & "','" & ModuleCode & "','2_" & MenuID & "','" &  rstMenus("ACCESSITEM") & "','" &  rstMenus("LIMITEDACCESS") & "','/js/tree/img/menu.gif');" & VbCrLf
				currenti = currenti + 1			
				
				Set rstPages = rstMenus("PAGES").Value
				if not rstPages.EOF then
					codeparent = currenti - 1			
					Do While Not rstPages.EOF
						PageID = rstPages("PAGEID")
						Response.Write "d.add(" & currenti & "," & codeparent & ",'" & rstPages("DESCRIPTION") & "','" & ModuleCode & "_" & MenuID & "_" & PageID & "','" & ModuleCode & "_" & MenuID & "','3_" & PageID & "','" &  rstPages("ACCESSITEM") & "','" &  rstPages("LIMITEDACCESS") & "','/js/tree/img/page.gif');" & VbCrLf
						currenti = currenti + 1
						rstPages.MoveNext
					Loop
					currenti = currenti + 1				
				else
					codeparent = currenti - 1
					currenti = currenti + 1	
				end if
				
				rstMenus.MoveNext
			Loop
			currenti = currenti + 1		
		else
			headparent = currenti - 1
			currenti = currenti + 1	
		end if
		rst.MoveNext
	Loop
	%>		

	document.write(d);
	//-->
        </script>
        </form>
    </div>
</body>
</html>
