<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (6)
Dim DataTypes    (6)
Dim ElementTypes (6)
Dim FormValues   (6)
Dim FormFields   (6)
UpdateID	  = "hid_MENUID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "rdo_ACTIVE|TEXT"
FormFields(2) = "txt_PAGE|PATH"
FormFields(3) = "txt_MENUWIDTH|NUMBER"
FormFields(4) = "txt_SUBMENUWIDTH|NUMBER"
FormFields(5) = "txt_ORDERTEXT|TEXT"
FormFields(6) = "hid_MODULEID|TEXT"

Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO AC_MENUS " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO("Menu Created Successfully")
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE AC_MENUS " & strSQL & " WHERE MENUID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO("Menu Updated Successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	dim rsRef
	SQL = "SELECT PAGEID FROM AC_PAGES WHERE MENUID = " & ID
	Call OpenRs(rsRef, SQL)
	if (NOT rsRef.EOF) then
		Call CloseRs(rsRef)
		GO("Menu cannot be deleted because it has PAGES underneath it.")
	end if
	Call CloseRs(rsRef)
		
	SQL = "DELETE FROM AC_MENUS WHERE MENUID = " & ID
	Conn.Execute SQL, recaffected
	GO("Menu Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "SiteTree.asp?Text=" & Text
End Function

TheAction = Request("hid_ACTION")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>