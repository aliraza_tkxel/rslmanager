<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<head>
<link rel="StyleSheet" href="/js/tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/js/tree/dtree.js"></script>
<script language=javascript>
//iAction 1 = UPDATE
//		  2 = NEW

//Level   1 -- Modules
//		  2 -- Menus
//		  3 -- Pages

function CallUpdater(Level, Me, Parent, iAction){
/*	alert(Level)
	alert(Me)
	alert(Parent)
	alert(iAction)
	*/
	thisForm.hid_Level.value = Level
	thisForm.hid_Parent.value = Parent
	thisForm.hid_Me.value = Me		
	thisForm.hid_ACTION.value = ((iAction == 1)? "LOAD" : "NEW")
	switch (Level) {
		case 1: thisForm.action = "../Module.asp"; break;
		case 2: thisForm.action = "../Menu.asp"; break;
		case 3: thisForm.action = "../Page.asp"; break;						
		case 4: thisForm.action = "../Resource.asp"; break;								
		}
		thisForm.target = "MainFrame"
		thisForm.submit();
	}			
</script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<div class="dtree" style='height:100%'>
	<form name="thisForm" action="SaveAccessRights.asp" method="post">
		<input type="hidden" name="hid_ACTION" class="" value="">		
		<input type="hidden" name="hid_Level" class="" value="">		
		<input type="hidden" name="hid_Me" class="" value="">		
		<input type="hidden" name="hid_Parent" class="" value="">
	</form>										
<script type="text/javascript">
	<!--
	<% if (Request("Text") <> "") then %>
	parent.MainFrame.location.href = "../ActionCompleted.asp?Text=<%=Request("Text")%>" 
	<% end if %>
	d = new dTree('d');
	d.config.useStatusText=true;
	d.add(0,-1,'Access Control');
	
	<%
	dim rstMenus, rstPages, rstBudgets, fundi, codei, budgeti, headi
	
	' Use a datashaping connection string to be used later.
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING
	
	' The sql below joins the tables AC_MODULES, AC_MENUS and AC_PAGES in a hierachial manner so that data can be retieved easily.
	' Join is as follows			AC_MODULES ---�
	'											  AC_MENUS ----� 
	'														   AC_PAGES 
	
	strSQL = "SHAPE {SELECT MODULEID, DESCRIPTION, ACCESSITEM, LIMITEDACCESS FROM AC_MODULES LEFT JOIN AC_NOACCESS ON MODULEID = ACCESSITEM AND ACCESSLEVEL = 1  WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "	APPEND((SHAPE {SELECT MENUID, DESCRIPTION, MODULEID, ACCESSITEM, LIMITEDACCESS FROM AC_MENUS LEFT JOIN AC_NOACCESS ON MENUID = ACCESSITEM AND ACCESSLEVEL = 2 WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "		APPEND((SHAPE {SELECT PAGEID, DESCRIPTION, MENUID, ACCESSITEM, LIMITEDACCESS  FROM AC_PAGES LEFT JOIN AC_NOACCESS ON PAGEID = ACCESSITEM AND ACCESSLEVEL = 3 WHERE ACTIVE = 1 ORDER BY DESCRIPTION}) AS PAGES "&_
			 "	RELATE MENUID TO MENUID)) AS MENUS "&_
			 "RELATE MODULEID TO MODULEID)"

	strSQL = "SHAPE {SELECT MODULEID, DESCRIPTION FROM AC_MODULES WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "	APPEND((SHAPE {SELECT MENUID, DESCRIPTION, MODULEID FROM AC_MENUS WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "		APPEND((SHAPE {SELECT PAGEID, DESCRIPTION, MENUID FROM AC_PAGES WHERE ACTIVE = 1 ORDER BY ORDERTEXT, DESCRIPTION} "&_
			 "			APPEND((SHAPE {SELECT PAGEID, DESCRIPTION, RESOURCEID FROM AC_RESOURCES ORDER BY DESCRIPTION}) AS RESOURCES "&_
			 "		RELATE PAGEID TO PAGEID)) AS PAGES "&_
			 "	RELATE MENUID TO MENUID)) AS MENUS "&_
			 "RELATE MODULEID TO MODULEID)"

	
	' Open original recordset
	Set rst = Server.CreateObject("ADODB.Recordset")
	rst.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
	
	fundi = 1
	currenti = 5
	headparent = 0
	codeparent = 0
	budgetparent = 0
	
	' The initial while loops through the main recordset (containing the AC_MODULES data) 
	Do While Not rst.EOF
		ModuleID = rst("MODULEID")
		ModuleCode = "M" & ModuleID
		Response.Write "d.add(" & currenti & ",0,'" & rst("DESCRIPTION") & "','JavaScript:CallUpdater(1," & ModuleID & ",0,1)','','','/js/tree/img/module.gif');" & VbCrLf
		currenti = currenti + 1
	
		' A new recordset reference is declared and is set to the children of the current reccord in 
		' AC_MODULES. The child records are from the AC_MENUS data
		Set rstMenus = rst("MENUS").Value
		if not rstMenus.EOF then
			menuparent = currenti - 1
			Do While Not rstMenus.EOF
				MenuID = rstMenus("MENUID")
				Response.Write "d.add(" & currenti & "," & menuparent & ",'" & rstMenus("DESCRIPTION") & "','JavaScript:CallUpdater(2," & MenuID & "," & ModuleID & ",1)','','','/js/tree/img/menu.gif');" & VbCrLf
				currenti = currenti + 1			
				
				Set rstPages = rstMenus("PAGES").Value
				if not rstPages.EOF then
					pageparent = currenti - 1			
					Do While Not rstPages.EOF
						PageID = rstPages("PAGEID")
						Response.Write "d.add(" & currenti & "," & pageparent & ",'" & rstPages("DESCRIPTION") & "','JavaScript:CallUpdater(3," & PageID & "," & MenuID & ",1)','','','/js/tree/img/page.gif');" & VbCrLf
						currenti = currenti + 1

						Set rstResources = rstPages("RESOURCES").Value
						if not rstResources.EOF then
							resourceparent = currenti - 1
							Do While Not rstResources.EOF
								ResourceID = rstResources("RESOURCEID")
								Response.Write "d.add(" & currenti & "," & resourceparent & ",'" & rstResources("DESCRIPTION") & "','JavaScript:CallUpdater(4," & ResourceID & "," & PageID & ",1)','','','/js/tree/img/resource.gif');" & VbCrLf
								currenti = currenti + 1
								rstResources.MoveNext						
							Loop
						else
							resourceparent = currenti - 1
						end if
						Response.Write "d.add(" & currenti & "," & resourceparent & ",'<font color=blue>New Resource</font>','JavaScript:CallUpdater(4,0," & PageID & ",2)','','','/js/tree/img/resource.gif');" & VbCrLf
						currenti = currenti + 1	
								
						rstPages.MoveNext
					Loop
				else
					pageparent = currenti - 1
				end if
				Response.Write "d.add(" & currenti & "," & pageparent & ",'<font color=blue>New Page</font>','JavaScript:CallUpdater(3,0," & MenuID & ",2)','','','/js/tree/img/pageadd.gif');" & VbCrLf
				currenti = currenti + 1	
				rstMenus.MoveNext
			Loop
		else
			menuparent = currenti - 1
		end if
		Response.Write "d.add(" & currenti & "," & menuparent & ",'<font color=blue>New Menu</font>','JavaScript:CallUpdater(2,0," & ModuleID & ",2)','','','/js/tree/img/menu.gif');" & VbCrLf
		currenti = currenti + 1			
		rst.MoveNext
	Loop
	Response.Write "d.add(" & currenti & ",0,'<font color=blue>New Module</font>','JavaScript:CallUpdater(1,0,0,2)','','','/js/tree/img/module.gif');" & VbCrLf	
	%>		

	document.write(d);
	//-->
</script>
</div>
</body>
</html>
