<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (11)
Dim DataTypes    (11)
Dim ElementTypes (11)
Dim FormValues   (11)
Dim FormFields   (11)
UpdateID	  = "hid_MODULEID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "rdo_ACTIVE|TEXT"
FormFields(2) = "txt_DIRECTORY|PATH"
FormFields(3) = "txt_PAGE|PATH"
FormFields(4) = "txt_IMAGE|TEXT"
FormFields(5) = "txt_IMAGEOPEN|TEXT"
FormFields(6) = "txt_IMAGEMOUSEOVER|TEXT"
FormFields(7) = "txt_IMAGETAG|TEXT"	
FormFields(8) = "txt_IMAGETITLE|TEXT"	
FormFields(9) = "rdo_DISPLAY|TEXT"
FormFields(10) = "rdo_ALLACCESS|TEXT"
FormFields(11) = "txt_ORDERTEXT|TEXT"

Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO AC_MODULES " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO("Module Created Successfully")
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE AC_MODULES " & strSQL & " WHERE MODULEID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO("Module Updated Successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	dim rsRef
	SQL = "SELECT MENUID FROM AC_MENUS WHERE MODULEID = " & ID
	Call OpenRs(rsRef, SQL)
	if (NOT rsRef.EOF) then
		Call CloseRs(rsRef)
		GO("Module cannot be deleted because it has MENUS underneath it.")
	end if
	Call CloseRs(rsRef)
		
	SQL = "DELETE FROM AC_MODULES WHERE MODULEID = " & ID
	Conn.Execute SQL, recaffected
	GO("Module Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "SiteTree.asp?Text=" & Text
End Function

TheAction = Request("hid_ACTION")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>