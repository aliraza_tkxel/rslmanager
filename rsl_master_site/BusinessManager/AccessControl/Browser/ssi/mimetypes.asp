<%
' This file lists all the expected MIME types of files in the system, presented
' as the cases of a "Select" statement.

' HTML text files
Case "htm"
	Response.ContentType = "text/html"
Case "html"
	Response.ContentType = "text/html"

	' MS Excel files
Case "xls"
	Response.ContentType = "application/x-msexcel"
Case "xlt"
	Response.ContentType = "application/x-msexcel"

' MS PowerPoint files
Case "ppt"
	Response.ContentType = "application/x-mspowerpoint"
Case "ppa"
	Response.ContentType = "application/x-mspowerpoint"
Case "pps"
	Response.ContentType = "application/x-mspowerpoint"
Case "pot"
	Response.ContentType = "application/x-mspowerpoint"
Case "pwz"
	Response.ContentType = "application/x-mspowerpoint"

' MS Project Files
Case "mpp"
	Response.ContentType = "application/vnd.ms-project"
Case "mpx"
	Response.ContentType = "application/vnd.ms-project"
Case "mpw"
	Response.ContentType = "application/vnd.ms-project"
Case "mpt"
	Response.ContentType = "application/vnd.ms-project"

' MS Word files
Case "doc"
	Response.ContentType = "application/msword"
Case "dot"
	Response.ContentType = "application/msword"

' Plain text files
Case "txt"
	Response.ContentType = "text/plain"
' Prompts user to Save As
Case Else
	Response.ContentType = "x/unknown"
%>