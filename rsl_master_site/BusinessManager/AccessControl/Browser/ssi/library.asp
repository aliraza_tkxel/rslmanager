<%
'************* Constants: **************
Dim CR, browser, LastPrint

CR = vbCRLF

browser = Request.ServerVariables("HTTP_USER_AGENT")

LastPrint = ""
'************* Functions: **************

Function Print(thing)
	LastPrint = thing
	Response.Write(thing)
End Function

Function BaseDir(PathString)
	BaseDir = Replace(PathString,RootPath & "\","")
End Function

Function BackDir(PathString)
	If Right(PathString,1) = "\" Then
		PathString = Left(PathString,Len(PathString)-1)
	End If
	BackDir = Left(PathString,(InStrRev(PathString,"\")-1))
End Function

Function HideFile(FileName)
	Dim intFilter, intShow
	Dim arrHidden

	intShow = 0

	If InStr(strFileFilter,";") > 0 Then
		arrHidden = Split(strFileFilter,";")
	Else
		ReDim arrHidden(0)
		arrHidden(0) = "^*&"
	End If
	For intFilter = 0 To UBound(arrHidden)
'		Response.Write arrHidden(intFilter) & "<BR>"
		If UCase(Right(FileName,Len(arrHidden(intFilter)))) = UCase(arrHidden(intFilter)) Then
			If LCase(strHideType) = "inclusion" Then
				HideFile = "Y"
				Exit For
			End If
			intShow = intShow + 1
		End If
	Next
	If LCase(strHideType) = "exclusion" And intShow = 0 Then
		HideFile = "Y"
	End If

End Function

Function HideFolder(FolderName)
	strPrefixLength = Len(strDirFilter)
	If UCase(Left(FolderName,Len(strPrefixLength))) = UCase(strDirFilter) Then
		HideFolder = "Y"
	Else
		HideFolder = "N"
	End If
End Function

Function NumberTrim(FolderName)
	If InStr(FolderName,") ") Then
		FolderName = Replace(FolderName,") ","|")
		FolderName = Right(FolderName,Len(FolderName)-InStr(FolderName,"|"))
	End If
		NumberTrim = FolderName
End Function

Function SizeFormat(number)
	If number < 1000 Then
		SizeFormat = number & " Bytes"
	ElseIf number > 999 And number < 1000000 Then
		number = Round(number/1000)
		SizeFormat = number & " KB"
	ElseIf number > 1000000 Then
		number = Round(number/1000000)
		SizeFormat = number & " MB"
	End If
End Function
%>