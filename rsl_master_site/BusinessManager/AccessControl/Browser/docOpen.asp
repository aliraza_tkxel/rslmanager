<%@Language="VBScript"%>
<%Response.Buffer = True%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ROOTPATH'"
Call OpenRs(rsRoot, SQL)
if (NOT rsRoot.EOF) then
	TheUltimateRootPath = rsRoot("DEFAULTVALUE")
else
	Response.Clear
	Response.Write("You're not allowed to play there.")
	Response.End
end if
CloseDB()

FilePath = Server.Mappath(Request.QueryString("docPath"))
Response.Clear
Response.Write FilePath
'Response.Write InStr(FilePath, "Broadland Dev")
Response.End
If InStr(FilePath, "Broadland Dev") < 38 Then
	Response.Clear
	Response.Write("You're not allowed to play there.")
	Response.End
End If

FilePath = Request.QueryString("docPath")

If FilePath = "" Then
  Response.Clear
  Response.Write("No file specified.")
  Response.End
ElseIf InStr(FilePath, "..") > 0 Then
  Response.Clear
  Response.Write("Illegal folder location.")
  Response.End
ElseIf Len(FilePath) > 1024 Then
  Response.Clear
  Response.Write("Folder path too long.")
  Response.End
Else
  Call DownloadFile(FilePath)
End If

Private Sub DownloadFile(file)
  '--declare variables
  Dim strAbsFile
  Dim strFileExtension
  Dim objFSO
  Dim objFile
  Dim objStream
  '-- set absolute file location
  strRootString = TheUltimateRootPath
  strAbsFile = strRootString & "\" & file
  '-- create FSO object to check if file exists and get properties
  Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
  '-- check to see if the file exists
  If objFSO.FileExists(strAbsFile) Then
    Set objFile = objFSO.GetFile(strAbsFile)
      '-- first clear the response, and then set the appropriate headers
      Response.Clear
      '-- the filename you give it will be the one that is shown
      '   to the users by default when they save
      Response.AddHeader "Content-Disposition", "attachment; filename=" & objFile.Name
      Response.AddHeader "Content-Length", objFile.Size
      Response.ContentType = "application/octet-stream"
      Set objStream = Server.CreateObject("ADODB.Stream")
        objStream.Open
        '-- set as binary
        objStream.Type = 1
        Response.CharSet = "UTF-8"
        '-- load into the stream the file
        objStream.LoadFromFile(strAbsFile)
        '-- send the stream in the response
        Response.BinaryWrite(objStream.Read)
        objStream.Close
      Set objStream = Nothing
    Set objFile = Nothing
  Else  'objFSO.FileExists(strAbsFile)
    Response.Clear
    Response.Write("No such file exists.")
  End If
  Set objFSO = Nothing
End Sub

%>