<%@LANGUAGE=VBSCRIPT%>
<!--#include file="ssi/library.asp"-->
<!--#include virtual="Includes/Functions/LibFunctions.asp" -->
<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ROOTPATH'"
Call OpenRs(rsRoot, SQL)
if (NOT rsRoot.EOF) then
	TheUltimateRootPath = rsRoot("DEFAULTVALUE")
else
	Response.Redirect "BrowserError.asp"
end if
CloseDB()
' Web File Browser 1.1
' Author: Peter Brunone
' Release Date: September 19, 2000
' Contact: peterbrunone@aspalliance.com

'***********  The following are all the variables you need to set for your system.  **************************

' This variable should contain the name of the highest folder you want the users to access.  If they 
' try to go above this folder, the app will simply return to the default directory (this name).
strCeilingFolder = ""

' This section allows for development and production boxes with slightly different directory structures.  If they are the same, 
'  they must still be entered here.
'FileFolders = 1 - Only Directories, 2 - Only Files, 3 - Both
if (Request("FileFolders") <> "") then
	FileFolders = CInt(Request("FileFolders"))
	Session("FileFolders") = CInt(FileFolders)
else
	FileFolders = CInt(Session("FileFolders"))
end if

if (Request("MiniRoot") = "") then
	MiniRoot = Session("MiniRoot")
else
	if (Request("MiniRoot") = "ROOT") then
		MiniRoot = ""
	else
		MiniRoot = Request("MiniRoot")
	end if
	Session("MiniRoot") = MiniRoot
end if

'Response.Write Miniroot

AppendPath1 = ""
AppendPath2 = ""
if (MiniRoot <> "") then	
	AppendPath1 = MiniRoot & "/"
	AppendPath2 = "\" & MiniRoot
	AppendPath2 = Replace(AppendPath2, "/", "\")
end if

strDevBox = "" ' The machine name of the development box
strDevPath = "/" & AppendPath1 ' The virtual directory (under the web root) of the development box (a.k.a. the "ceiling")
' Example:  strDevPath = "/mydates/today/"
strLivePath = "/" & AppendPath1 ' The virtual directory of the production (live) box
' Example:  strLivePath = "/appointments/today/"

' These variables allow you to hide the directory structure from prying eyes, and at the same time make the interface neater by
'  removing the "deep" part of the path structure and displaying a more user-friendly directory name.
strRootString = TheUltimateRootPath & AppendPath2 ' The part of the physical path you'd like to hide
' Example: strRootString = "C:\Inetpub\wwwroot"
strBrowseString = "RSL Manager" & AppendPath2 ' The expression with which you'd like to replace it
' Example: strBrowseString = "\Users"

' Here you can specify prefixes for hidden files.  Simply add all extensions of file types that you wish to hide, delimited with a semicolon (;).
' The hide type can be specified as Inclusion (hide all types in strFileFilter) or Exclusion (hide all except types in strFileFilter).
'strFileFilter = "txt;gif"
strFileFilter = "asp;aspx;html;gif;jpg;jpeg;htm"
strHideType = "Exclusion"
' Example: strFileFilter = "asp;aspx;html;xls"
' Example: strHideType = "Inclusion"

'  Directories can now be filtered out as well, based on a leading character.
'   The default is an underscore (_), which effectively takes care of FrontPage folders.
strDirFilter = "_"

' The Delete switch turns the Delete File functionality on (Y) or off (N); it is off by default.
strDelete = "N"

'*********** The rest is functional code; don't change it unless you want to change the way it works.  ***********

Set fso = Server.CreateObject("Scripting.FileSystemObject")

If strServer = strDevBox Then
	RootPath = Server.Mappath(strDevPath)
Else
	RootPath = Server.Mappath(strLivePath)
End If
BrowsePath = RootPath

If (Request.Querystring("folder") <> "" And InStr(Request.Querystring("folder"),strCeilingFolder) > 0  And InStr(Request.QueryString("folder"),"..") <= 0) Then

	RootPath = strRootString & "\" & Request.Querystring("folder")
	'Response.Write RootPath
	BackPath = BackDir(RootPath)
	Set Dir = fso.GetFolder(RootPath)
	ParentDir = Dir.ParentFolder
	BrowsePath = RootPath

	If Request.QueryString("delete") <> "" Then
		DelDoc = Request.QueryString("docPath")
		Response.Write DelDoc
		If fso.FileExists(DelDoc) Then
			Set DelDoc = fso.GetFile(strRootString & "\" & Request.QueryString("docPath"))
			DelDoc.Delete True
		End If
	End If
End If

'Response.Write RootPath & "<BR>"
'Response.Write strRootString & "<BR>"
'Response.Write strBrowseString & "<BR>"
BrowsePath = Replace(RootPath,strRootString,strBrowseString)
%>
<HTML>
<HEAD>
<title>File Manager</title>
<!--#include file="ssi/header.jsinc"-->
<!--#include file="ssi/style.css"-->
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language=javascript>
function SetData(data){
	opener.getData(data)
	window.close()
	}

</script>
</HEAD>
<BODY BGCOLOR="#FFFFFF" onLoad="window.focus()">
<%
'Response.Write Request.ServerVariables("HTTP_USER_AGENT") & "<BR><BR>" & vbCRLF  ' Debugging

Response.Write "<TABLE WIDTH=""100%"" BORDER=0>" & vbCRLF

Response.Write "<TR><TD COLSPAN=6>Current Directory: " & BrowsePath & " &nbsp &nbsp &nbsp <BR><BR></TD></TR>"

If BackPath <> RootPath Then
	Response.Write "<TR><TD ALIGN=""LEFT"" VALIGN=""TOP""> <A HREF=""javascript:backUp();"">" &_
		"<IMG SRC=""images/parentdir.gif"" BORDER=0 ALIGN=""TOP""> Parent Directory</A></TD>" & vbCRLF
Else
	Response.Write "<TR><TD VALIGN=""TOP""> &nbsp </TD>" & vbCRLF
End If

If UCase(strDelete) = "Y" Then
	' Use this line to include the Delete function
	Response.Write "<TD ALIGN=""CENTER"" VALIGN=""TOP""><A HREF=""javascript:deleteSwitch('y')"">" &_
		"<IMG SRC=""images/deldown.gif"" BORDER=0 ALIGN=""TOP"" NAME=""deletepic""> Delete File</A></TD>" &_
		"<TD> &nbsp; <BR><BR></TD></TR>" & vbCRLF
Else
	' Use this line to exclude the Delete function
	Response.Write "<TD> &nbsp; <BR><BR></TD></TR>" & vbCRLF
End If
	
Set topfolder = fso.GetFolder(RootPath)

Response.Write "<TR><TD ALIGN=""LEFT"" width=""300""> &nbsp &nbsp &nbsp <B>Name</B></TD><TD> &nbsp </TD>" & vbCRLF &_
	"</TD><TD ALIGN=""RIGHT"" width=""50""><B>Size</B></TD><TD ALIGN=""RIGHT"" width=""100""><B>Modified</B></TD><td width=30 nowrap align=right>&nbsp;</td></TR>"

' Here the folder names in the current directory are read and formatted into a hyperlink list.
if (FileFolders = 1 OR FileFolders = 3) then

For Each Folder in topfolder.SubFolders

	FolderLink = NumberTrim(BaseDir(Folder))
	If HideFolder(FolderLink) <> "Y" Then
		FolderLink = Replace(FolderLink,rootpath,"")
		If InStr(browser,"MSIE") Then
			Folder = Replace(Folder,strRootString & "\","")
			Folder = Replace(Folder,"\","\\")
		
		End If
		Folder = Server.URLEncode(Folder)
		FolderLink = "<IMG SRC=""images/folder.gif"" BORDER=0 ALIGN=""TOP""> <A HREF=""javascript:linkPage('" & Folder & "')"">" & FolderLink & "</A>"
		ParentFolder = topfolder.ParentFolder
		if (FileFolders = 3) then		
			Response.Write "<TR><TD COLSPAN=4> &nbsp " & FolderLink & "</TD><TD align=right>&nbsp;</TD></TR>" & vbCRLF
		else
			Response.Write "<TR><TD COLSPAN=4> &nbsp " & FolderLink & "</TD><TD align=right><a href=""javascript:SetData('" & Folder & "')"">SELECT</a></TD></TR>" & vbCRLF
		end if
	End If
Next

end if

' This block prepares the RootPath for the Delete function if needed.
	If InStr(browser,"MSIE") Then
		RootDirPath = Replace(RootPath,strRootString & "\","")	
		RootDirPath = Replace(RootPath,"\","\\")
		RootDirPath = Server.URLEncode(RootDirPath)
	Else
		RootDirPath = Server.URLEncode(RootPath)
	End If

strRootString2 = Replace(strRootString, "\\", "\")
' Here the filenames in the current directory are read and formatted into a hyperlink list.
if (FileFolders = 2 OR FileFolders = 3) then

For Each File In topfolder.Files

	FileLink = BaseDir(File)
	FileLink = File
	FileLink = Replace(FileLink,RootPath & "\","")
	FileLink = BaseDir(FileLink)
		
	If HideFile(FileLink) <> "Y" Then
		Size = File.Size
		Size = SizeFormat(Size)
		Modified = File.DateLastModified
		File = Replace(File,strRootString & "\","")		
		If InStr(browser,"MSIE") Then
			File = Replace(File,"\","\\")
		End If
		File = Server.URLEncode(File)
		FileLink = "<IMG SRC=""images/file.gif"" BORDER=0 ALIGN=""TOP""> <A HREF=""javascript:openDocument('" & RootDirPath & "','" & Replace(AppendPath2,"\","\\") & "\\" & File & "','" & FileLink & "')""><font color=blue>" & FileLink & "</font></A>"
		Response.Write "<TR><TD NOWRAP> &nbsp " & FileLink & "</TD><TD> &nbsp </TD>" & vbCRLF &_
			"<TD ALIGN=""RIGHT"" NOWRAP>" & Size & "</TD><TD ALIGN=""RIGHT"" NOWRAP>" & Modified & "</TD><TD align=right><a href=""javascript:SetData('" & File & "')"">SELECT</a></TD></TR>" & vbCRLF
	End If
Next

end if

Response.Write "</TABLE>" & vbCRLF

RootPath = Replace(RootPath,strRootString & "\","")
BackPath = Replace(BackPath,strRootString & "\","")	
BackPath = Replace(BackPath,strRootString,"")		
%>
<FORM NAME="mainForm">
<INPUT TYPE="HIDDEN" NAME="rootPath" VALUE="<%=Server.URLEncode(RootPath)%>">
<INPUT TYPE="HIDDEN" NAME="backPath" VALUE="<%=Server.URLEncode(BackPath)%>">
<INPUT TYPE="HIDDEN" NAME="deleteIndicator" VALUE="no">
</FORM>
</BODY>
</HTML>
<%
Set browser = nothing

'LogArticleHit(68)
%>