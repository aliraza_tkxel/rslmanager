<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsUser
    UserID = Request("EmployeeID")

If (UserID = "") Then
	UserId = -1
End If

ACTION = "NEW"
Dim isLocked : isLocked = 0
unlock_disabled = "disabled=""disabled"""

SQL = "SELECT FIRSTNAME, LASTNAME, LOGINID, LOGIN, E.EMPLOYEEID, L.EXPIRES,L.ACTIVE, WORKEMAIL, HOMEEMAIL, O.NAME AS ORGNAME, ISNULL(ISLOCKED,0) AS ISLOCKED FROM E__EMPLOYEE E LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID LEFT JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID WHERE E.EMPLOYEEID = " & UserID
Call OpenDB()
Call OpenRs(rsUser, SQL)
If (NOT rsUser.EOF) Then
	firstname = rsUser("FIRSTNAME")
	lastname = rsUser("LASTNAME")
	loginid = rsUser("LOGINID")
	orgname = rsUser("ORGNAME")
	expirydate = rsUser("expires")
    isLocked = rsUser("ISLOCKED")
	If (NOT isNull(orgname) AND orgname <> "") Then
		orgtext = "<br/><br/><br/>Supplier: " & orgname
	End If
	If (isNull(loginid) OR loginid = "") then
		saveaccessrights = " disabled=""disabled"" "
		buttontext = " Create Login "
		username = rsUser("WORKEMAIL")
		If (username = "" OR isNull(username)) Then
			username = rsUser("HOMEEMAIL")
		End If
	Else
		username = rsUser("LOGIN")
		buttontext = " Update Login "
		ACTION = "AMEND"
		l_active = rsUser("ACTIVE")
	End If
Else
	firstname = "UNKNOWN"
	lastname = "USER"
	saveaccessrights = " disabled=""disabled"" "
	buttontext = " Disabled - Invalid User "" disabled "
End If
Call CloseRS(rsUser)
Call CloseDB()

Dim locked_disabled
locked_disabled = ""
if(isLocked) Then
    locked_disabled = "disabled=""disabled"""
    unlock_disabled = ""
End if

%>
<!DOCTYPE html>
<html>
<head>
    <title>RSL Manager Business --> Access Control --> Employee Access Control</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
            background-image: none !important;
        }
    </style>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script type="text/javascript" language="JavaScript">
var FormFields = new Array();
<% if (ACTION = "NEW") then %>
FormFields[0] = "txt_LOGIN|Username|SELECT|Y"
FormFields[1] = "txt_PASSWORD|Password|TEXT|Y"
FormFields[2] = "txt_PS2|Confirmation|TEXT|Y"
FormFields[3] = "rdo_ACTIVE|Active|RADIO|Y"
<% else %>
FormFields[0] = "txt_LOGIN|Username|SELECT|Y"
FormFields[1] = "txt_PASSWORD|Password|TEXT|N"
FormFields[2] = "txt_PS2|Confirmation|TEXT|N"
FormFields[3] = "rdo_ACTIVE|Active|RADIO|Y"
<% end if %>

function SaveForm(){
	if (!checkForm()) return false;
	if (document.getElementById("txt_PASSWORD").value != document.getElementById("txt_PS2").value) {
		ManualError('img_PASSWORD', 'Both password fields should match.',0)
		ManualError('img_PS2', 'Both password fields should match.',0)
		return false;
		}
	if (document.getElementById("txt_PASSWORD").value != "" && document.getElementById("txt_PS2").value != "" && document.getElementById("txt_PASSWORD").value.length < 8) {
		ManualError('img_PASSWORD', 'The password must be at least 8 characters long.',0)
		ManualError('img_PS2', 'The password must be at least 8 characters long.',0)
		return false;
		}
		//var passRegEx = new RegExp("^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$");
		var passRegEx = /^(?=.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*/;
		if (passRegEx.test(document.getElementById("txt_PASSWORD").value) == false)
		{
			ManualError('img_PASSWORD', 'The password must be at least 8 characters , contain at least one lower case letter, one upper case letter and one digit.',0)
			return false;
		}
	document.RSLFORM.submit()
	}
    function UnlockAccount()
    {
        document.getElementById("hid_ACTION").value = "Unlock"
        document.RSLFORM.submit()
    }

function SaveData(){
	try {
		parent.theSideBar.SubmitPage()
		}
	catch(e){
		alert("Please wait for the page to load.")
		}
	}

    $( document ).ready(function() {
       getPages("13");
       function showpanel() { 
      
      $('#links_19').css('background-color', '#ccc');
       }
        // use setTimeout() to execute
 setTimeout(showpanel, 1000);
});
</script>
<body>
    <!--<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">-->
    <!-- #include virtual="Includes/Tops/BodyTopNewLayout.asp" -->
    <table width="98%" border="0" cellspacing="0" cellpadding="0" class="jr-rightpanel">
        <tr>
            <td style="width: 98%; background-color: Black; height: 22px;">
                <span style="color: White; font-weight: bold; margin: 20px;">Employee Profiles</span>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2" style="border-right: 1px solid #7e729E" align="center"
                valign="top" class="iagManagerSmallBlk">
                <br />
                You are editing the User Access control for:<br />
                <br />
                <b>
                    <%=firstname%>&nbsp;<%=lastname%><%=orgtext%></b><br />
                <br />
                <form name="RSLFORM" method="post" action="ServerSide/Login_svr.asp">
                <table style="border: 1px solid #133E71; width: 450px">
                    <tr>
                        <td colspan="2">
                            The password expiry date for this user is:
                            <% = expirydate %>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Username:
                        </td>
                        <td>
                            <input type="text" name="txt_LOGIN" id="txt_LOGIN" value="<%=username%>" class="textbox200" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LOGIN" id="img_LOGIN" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <input type="password" name="txt_PASSWORD" id="txt_PASSWORD" class="textbox200" <%=locked_disabled %> />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_PASSWORD" id="img_PASSWORD" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Confirmation:
                        </td>
                        <td>
                            <input type="password" name="txt_PS2" id="txt_PS2" class="textbox200" <%=locked_disabled %> />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_PS2" id="img_PS2" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <% 
				yesChecked = "checked=""checked"""
				noChecked = ""
				if ACTION = "AMEND" then
					if (l_active <> 1) then
						yesChecked = ""
						noChecked = "checked=""checked"""
					end if
				end if
                    %>
                    <tr>
                        <td>
                            Active:
                        </td>
                        <td>
                            <input type="radio" name="rdo_ACTIVE" value="1" <%=yesChecked%> <%=locked_disabled %> />
                            YES
                            <input type="radio" name="rdo_ACTIVE" value="0" <%=noChecked%> <%=locked_disabled %> />
                            NO
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACTIVE" id="img_ACTIVE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <input type="button" onclick="SaveForm()" value="<%=buttontext%>" class="RSLButton"
                                style="cursor: pointer" <%=locked_disabled %> />
                            <input type="hidden" name="hid_EMPLOYEEID" id="hid_EMPLOYEEID" value="<%=UserID%>" />
                            <input type="hidden" name="hid_LOGINID" id="hid_LOGINID" value="<%=loginid%>" />
                            <input type="hidden" name="hid_ACTION" id="hid_ACTION" value="<%=ACTION%>" />
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <table style="border: 1px solid #133E71; padding: right: 20px; width: 450px">
                    <tr>
                        <td align="center" valign="top" style="margin-top: 20px; margin-bottom: 40px;">                            
                                <% if(not isLocked) Then %>
                                Account is <span style="color: Blue;">Active</span>
                                <% else %>
                                <img src="../../BHAIntranet/Images/icon_redlock.png" alt="lock icon" style="vertical-align:middle;padding-top: 5px;" />
                                Account is <span style="color: Red;">Locked</span>
                                <% End if %>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="padding-right: 20px;">
                            <input type="button" onclick="UnlockAccount()" value="Unlock Account" class="RSLButton"
                                style="cursor: pointer" <%=unlock_disabled %> />
                        </td>
                    </tr>
                </table>
                </form>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <div id="everything" style="width: 370px; color: Black; font-size: 20px">
                    <%=Server.HTMLEncode(Request("Text"))%>
                </div>
				<br />
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    <div id="Calendar1" style="background-color: white; position: absolute; left: 1px;
        top: 1px; width: 200px; height: 115px; z-index: 20; visibility: hidden">
    </div>
    <iframe src="/secureframe.asp" name="AccessControl" style="display: none"></iframe>
    <img src="/js/img/FVER.gif" width="1px" height="1px" name="FVER_Image" id="FVER_Image"
        style="visibility: hidden" alt="" />
    <img src="/js/img/FVEB.gif" width="1px" height="1px" name="FVEB_Image" id="FVEB_Image"
        style="visibility: hidden" alt="" />
    <img src="/js/img/FVS.gif" width="1px" height="1px" name="FVS_Image" id="FVS_Image"
        style="visibility: hidden" alt="" />
    <img src="/js/img/FVW.gif" width="1px" height="1px" name="FVW_Image" id="FVW_Image"
        style="visibility: hidden" alt="" />
    <img src="/js/img/FVTG.gif" width="1px" height="1px" name="FVTG_Image" id="FVTG_Image"
        style="visibility: hidden" alt="" />
</body>
</html>
<% 
	'ZZ_Time_4 = StopTimer(4) 
	'StartTimer (5)

'OpenDB()
'Conn.execute "INSERT INTO G_TIMING (PAGENAME, ACCESSCHECK, TOPCODE, MENUCODE, BODYCODE, ENDCODE)VALUES ('" & Request.ServerVariables("SCRIPT_NAME") & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "')"
'Response.Write "<div style='position:absolute;top:10;left:870;background-Color:#ffffff;border:1px dotted #133e71'>"
'Response.Write "<table><tr><td colspan=2 align=center nowrap><b>Page Loading Times</b></td></tr>"
'Response.Write "<tr><td nowrap>Access Check</td><td nowrap>" & ZZ_Time_1 & "</td></tr>"
'Response.Write "<tr><td>Top Code</td><td>" & ZZ_Time_2 & "</td></tr>"
'Response.Write "<tr><td>Menu Code</td><td>" & ZZ_Time_3 & "</td></tr>"
'Response.Write "<tr><td>Body Code</td><td>" & ZZ_Time_4 & "</td></tr>"'
'ZZ_Time_5 = StopTimer(5)
'Response.Write "<tr><td>End Code</td><td>" & ZZ_Time_5 & "</td></tr>"
'Response.Write "</table></div>"
'CloseDB()
%>