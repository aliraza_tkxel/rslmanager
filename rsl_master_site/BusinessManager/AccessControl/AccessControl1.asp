<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsUser
UserID = Request("EmployeeID")
if (UserID = "") then
	UserId = -1
end if

ACTION = "NEW"
OpenDB()
SQL = "SELECT FIRSTNAME, LASTNAME, LOGINID, LOGIN, E.EMPLOYEEID, L.ACTIVE, WORKEMAIL, HOMEEMAIL, O.NAME AS ORGNAME FROM E__EMPLOYEE E LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID LEFT JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID WHERE E.EMPLOYEEID = " & UserID
Call OpenRs(rsUser, SQL)
if (NOT rsUser.EOF) then
	firstname = rsUser("FIRSTNAME")
	lastname = rsUser("LASTNAME")
	loginid = rsUser("LOGINID")
	orgname = rsUser("ORGNAME")
	if (NOT isNull(orgname) AND orgname <> "") then
		orgtext = "<br><br><br>Supplier: " & orgname
	end if
	if (isNull(loginid) OR loginid = "") then
		saveaccessrights = " disabled"
		buttontext = " Create Login "
		username = rsUser("WORKEMAIL")
		if (username = "" OR isNull(username)) then
			username = rsUser("HOMEEMAIL")
		end if
	else
		username = rsUser("LOGIN")
		buttontext = " Update Login "
		ACTION = "AMEND"
		l_active = rsUser("ACTIVE")				
	end if
else
	firstname = "UNKNOWN"
	lastname = "USER"
	saveaccessrights = " disabled"
	buttontext = " Disabled - Invalid User "" disabled "
end if
Call CloseRS(rsUser)
CloseDB()
%>
<HTML>
<HEAD>
<title>RSL Manager Business --> Access Control --> Employee Access Control</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var FormFields = new Array();
<% if (ACTION = "NEW") then %>
FormFields[0] = "txt_LOGIN|Username|SELECT|Y"
FormFields[1] = "txt_PASSWORD|Password|TEXT|Y"
FormFields[2] = "txt_PS2|Confirmation|TEXT|Y"
FormFields[3] = "rdo_ACTIVE|Active|TEXT|Y"
<% else %>
FormFields[0] = "txt_LOGIN|Username|SELECT|Y"
FormFields[1] = "txt_PASSWORD|Password|TEXT|N"
FormFields[2] = "txt_PS2|Confirmation|TEXT|N"
FormFields[3] = "rdo_ACTIVE|Active|TEXT|Y"
<% end if %>

function SaveForm(){
	if (!checkForm()) return false;
	if (RSLFORM.txt_PASSWORD.value != RSLFORM.txt_PS2.value) {
		ManualError('img_PASSWORD', 'Both password fields should match.',0)
		ManualError('img_PS2', 'Both password fields should match.',0)		
		return false;
		}
	if (RSLFORM.txt_PASSWORD.value != "" && RSLFORM.txt_PS2.value != "" && RSLFORM.txt_PASSWORD.value.length < 5) {
		ManualError('img_PASSWORD', 'The password must be at least 5 characters long.',0)
		ManualError('img_PS2', 'The password must be at least 5 characters long.',0)		
		return false;
		}
	RSLFORM.submit()
	}

function SaveData(){
	try {
		parent.theSideBar.SubmitPage()
		}
	catch(e){
		alert("Please wait for the page to load.")
		}
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!-- #include virtual="Includes/Tops/BodyTopSpecial.asp" -->
  <TR> 
    <TD HEIGHT=100% STYLE='BORDER-LEFT:1PX SOLID #7E729E' width=300> 
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td style='border-top:1px solid #FFFFFF;border-right:1px solid #7E729E;' height=100% width=100% valign=top nowrap bgcolor=beige>
		  <% if (saveaccessrights <> " disabled") then %>
            <iframe name=theSideBar src="ServerSide/ControlTree.asp?EmployeeID=<%=UserID%>" id=theSideBar height=500 frameborder=none bordercolor=#FFFFFF></iframe> 
		  <% else %>
            <iframe name=theSideBar src="ServerSide/Beige.asp" id=theSideBar height=500 frameborder=none bordercolor=#FFFFFF></iframe> 
		  <% end if %>
          </td>
        </tr>
        <tr> 
          <TD height=1 BGCOLOR=#FFFFFF align=right class="iagManagerSmallWht" valign=middle></TD>
        </tr>
        <tr> 
          <TD height=28 BGCOLOR=#133e71 align=right class="iagManagerSmallWht" valign=middle><b>&nbsp;</b></TD>
        </tr>
      </table>
    </TD>
    <td HEIGHT=100% width=476> 
        <table cellspacing=0 cellpadding=0 height=100% width=100%>
          <tr> 
            <td width=100% colspan=2 STYLE='BORDER-RIGHT:1PX SOLID #7E729E' align=center valign=top class="iagManagerSmallBlk"> 
              <br>You are editing the User Access control for:<br><br><b><%=firstname%>&nbsp;<%=lastname%><%=orgtext%></b><br><br>
			  <table style='border:1px solid #133E71'>
			    <form name="RSLFORM" method="POST" action="ServerSide/Login_svr.asp">
                <tr> 
                  <td>Username:</td>
                  <td> 
                    <input type="text" name="txt_LOGIN" value="<%=username%>" class="textbox200">
                  </td>
				  <TD><image src="/js/FVS.gif" name="img_LOGIN" width="15px" height="15px" border="0"></TD>				  
                </tr>
                <tr> 
                  <td>Password:</td>
                  <td> 
                    <input type="password" name="txt_PASSWORD" class="textbox200">
                  </td>
				  <TD><image src="/js/FVS.gif" name="img_PASSWORD" width="15px" height="15px" border="0"></TD>				  
				 </tr>
				 <tr>
                  <td>Confirmation:</td>
                  <td> 
                    <input type="password" name="txt_PS2" class="textbox200">
                  </td>
				  <TD><image src="/js/FVS.gif" name="img_PS2" width="15px" height="15px" border="0"></TD>				  				  
                </tr>
				<% 
				yesChecked = "CHECKED"
				noChecked = ""
				if ACTION = "AMEND" then
					if (l_active <> 1) then
						yesChecked = ""
						noChecked = "CHECKED"
					end if
				end if
				%>				
				 <tr>
                  <td>Active:</td>
                  <td> 
                    <input type="radio" name="rdo_ACTIVE" value="1" <%=yesChecked%>> YES <input type="radio" name="rdo_ACTIVE" value="0" <%=noChecked%>> NO
                  </td>
				  <TD><image src="/js/FVS.gif" name="img_ACTIVE" width="15px" height="15px" border="0"></TD>				  				  
                </tr>				
                <tr> 
                  <td colspan=2 align=right> 
                    <input type="button" onclick="SaveForm()" value="<%=buttontext%>" class="RSLButton">
                    <input type="hidden" name="hid_EMPLOYEEID" value="<%=UserID%>">
                    <input type="hidden" name="hid_LOGINID" value="<%=loginid%>">					
                    <input type="hidden" name="hid_ACTION" value="<%=ACTION%>">					
                  </td>
                </tr>
				</form>
              </table>
              <br>
              <br>
              <br>
			  <% if (saveaccessrights <> " disabled") then %>
              To Update Access Rights click on the button below.<br>
              <br>
              <input type="button" value="Save Access Rights" class="RSLButton" onclick="SaveData()" <%=saveaccessrights%>>
			  <% end if %>
              <br>
              <br>
              <br>
              <br>
              <br>
              <font color=black style='font-size:20px'> 
              <div id=everything style='width:370px'><%=Server.HTMLEncode(Request("Text"))%></div>
              </font> 
			  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
			  </td>
          </tr>
          <tr> 
            <TD HEIGHT=44 width=100% align=right>&nbsp;</TD>
            <td rowspan=2><IMG SRC="/myImages/My113.gif" WIDTH=72 HEIGHT=72></td>
          </tr>
          <TR> 
            <TD height=28 BGCOLOR=#133e71 align=right class="RSLWhite" valign=middle><b>rslManager 
              is a Reidmark eBusiness System</b></TD>
          </TR>
        </table>
    </TD>
  </TR>
</TABLE>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<iframe name=AccessControl style='display:none'></iframe> 
</body>
</html>
<% 
	ZZ_Time_4 = StopTimer(4) 
	StartTimer (5)

OpenDB()
Conn.execute "INSERT INTO G_TIMING (PAGENAME, ACCESSCHECK, TOPCODE, MENUCODE, BODYCODE, ENDCODE)VALUES ('" & Request.ServerVariables("SCRIPT_NAME") & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "', '" & ZZ_Time_1 & "')"
Response.Write "<div style='position:absolute;top:10;left:870;background-Color:#ffffff;border:1px dotted #133e71'>"
Response.Write "<table><tr><td colspan=2 align=center nowrap><b>Page Loading Times</b></td></tr>"
Response.Write "<tr><td nowrap>Access Check</td><td nowrap>" & ZZ_Time_1 & "</td></tr>"
Response.Write "<tr><td>Top Code</td><td>" & ZZ_Time_2 & "</td></tr>"
Response.Write "<tr><td>Menu Code</td><td>" & ZZ_Time_3 & "</td></tr>"
Response.Write "<tr><td>Body Code</td><td>" & ZZ_Time_4 & "</td></tr>"
	ZZ_Time_5 = StopTimer(5)
Response.Write "<tr><td>End Code</td><td>" & ZZ_Time_5 & "</td></tr>"
Response.Write "</table></div>"
CloseDB()
%>
