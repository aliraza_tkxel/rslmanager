<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
lt_action = "New Module"

Moi = Request("hid_Me")
if (Moi = "") then Moi = -1 end if

OpenDB()

SQL = "SELECT * FROM AC_MODULES WHERE MODULEID = " & Moi
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then
	l_description = rsLoader("DESCRIPTION")
	l_active = rsLoader("ACTIVE")
	l_allaccess = rsLoader("ALLACCESS")	
	l_display = rsLoader("DISPLAY")	
	l_directory = rsLoader("DIRECTORY")	
	l_page = rsLoader("PAGE")
	l_image = rsLoader("IMAGE")
	l_imageopen = rsLoader("IMAGEOPEN")
	l_imagetag = rsLoader("IMAGETAG")
	l_imagetitle = rsLoader("IMAGETITLE")	
	l_imagemouseover = rsLoader("IMAGEMOUSEOVER")	
	l_moduleid = rsLoader("MODULEID")
	l_ordertext = rsLoader("ORDERTEXT")

	lt_action = "Update Module"							
	ACTION = "AMEND"
end if

CloseRs(rsLoader)
CloseDB()
%>
<html>
<head>
<title>Module Page</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidationExtras.js"></SCRIPT>
<script language=javascript>
var FormFields = new Array();
FormFields[0] = "txt_DESCRIPTION|Description|TEXT|Y"
FormFields[1] = "rdo_ACTIVE|Active|RADIO|Y"
FormFields[2] = "txt_DIRECTORY|Directory|TEXT|Y"
FormFields[3] = "txt_PAGE|Page|TEXT|I_Y"
FormFields[4] = "rdo_ALLACCESS|All Access|RADIO|Y"
FormFields[5] = "rdo_DISPLAY|Display|RADIO|Y"
FormFields[6] = "txt_IMAGE|Image|TEXT|I_Y"
FormFields[7] = "txt_IMAGEOPEN|Image Open|TEXT|I_Y"
FormFields[8] = "txt_IMAGEMOUSEOVER|Image Mouseover|TEXT|I_Y"	
FormFields[9] = "txt_IMAGETAG|Image Tag|TEXT|Y"	
FormFields[10] = "txt_IMAGETITLE|Image Title|TEXT|Y"	
FormFields[11] = "txt_ORDERTEXT|Order|TEXT|N"						

function ToggleDisplay(){
	if (document.getElementsByName("rdo_DISPLAY")[0].checked) EnableItems("3,8,6,7")
	else DisableItems("3,8,6,7")
	}
		
function SaveForm(){
	if (!checkForm()) return false;
	location.href = "ActionCompleted.asp"
	RSLFORM.submit()
	}

function DeleteItem(){
	RSLFORM.hid_ACTION.value = "DELETE"
	location.href = "ActionCompleted.asp"
	RSLFORM.submit();
	}
	
var TargetElement = ""

function getData(data){
	document.getElementById(TargetElement).value = data
	}

function OpenDirectory(el, MiniRoot, openType) {
	NewWindow = window.open('browser/browser.asp?FileFolders=' + openType + "&MiniRoot=" + MiniRoot,'browserWindow','width=600,height=400,scrollbars=yes,menu=no,')
	TargetElement = el
	}

function OpenPage(el, parDir, openType) {
	parVal = document.getElementById(parDir).value
	OpenDirectory(el, parVal, openType)
	}

function CancelAction(){
	location.href = "ActionCompleted.asp?Text="
	}
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="ToggleDisplay()">
<form name="RSLFORM" method="POST" action="ServerSide/Module_svr.asp" target="theSideBar">
  <table align="center">
    <tr>
      <td colspan=6> 
        <!--Start of MiniTree -->
        <table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk'>
          <tr>
            <td><b><u><%=lt_action%></u></b></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
        <!--End of MiniTree -->
      </td>
    </tr>
    <tr>
      <td>Name: </td>
      <td>
        <input type="TEXT" name="txt_DESCRIPTION" value="<%=l_description%>" class="textbox200">
      </td>
      <td><img name="img_DESCRIPTION" src="/js/FVS.gif" width=15 height=15 border=0></td>
    </tr>
    <% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_active <> 1) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
    <tr>
      <td>Active: </td>
      <td>
        <input type="RADIO" name="rdo_ACTIVE" value="1" <%=yesChecked%>>
        YES 
        <input type="RADIO" name="rdo_ACTIVE" value="0" <%=noChecked%>>
        NO</td>
      <td><img name="img_ACTIVE" src="/js/FVS.gif" width=15 height=15 border=0></td>
    </tr>
    <tr>
      <td>Directory: </td>
      <td>
        <input type="TEXT" name="txt_DIRECTORY" value="<%=l_directory%>" class="textbox200" readonly>
      </td>
      <td><img name="img_DIRECTORY" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_DIRECTORY', 'ROOT', 1)">
      </td>
    </tr>
    <tr>
      <td>Page: </td>
      <td>
        <input type="TEXT" name="txt_PAGE" value="<%=l_page%>" class="textbox200" readonly>
      </td>
      <td><img name="img_PAGE" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenPage('txt_PAGE', 'txt_DIRECTORY', 2)">
      </td>
    </tr>
    <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_allaccess = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
    <tr>
      <td>All Access: </td>
      <td>
        <input type="RADIO" name="rdo_ALLACCESS" value="1" <%=yesChecked%>>
        YES 
        <input type="RADIO" name="rdo_ALLACCESS" value="0" <%=noChecked%>>
        NO</td>
      <td><img name="img_ALLACCESS" src="/js/FVS.gif" width=15 height=15 border=0></td>
    </tr>	
    <% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_display = 0) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
    <tr>
      <td>Display: </td>
      <td>
        <input type="RADIO" name="rdo_DISPLAY" value="1" <%=yesChecked%> onClick="ToggleDisplay()">
        YES 
        <input type="RADIO" name="rdo_DISPLAY" value="0" <%=noChecked%> onClick="ToggleDisplay()">
        NO</td>
      <td><img name="img_DISPLAY" src="/js/FVS.gif" width=15 height=15 border=0></td>
    </tr>
    <tr>
      <td>Image: </td>
      <td>
        <input type="TEXT" name="txt_IMAGE" value="<%=l_image%>" class="textbox200" readonly>
      </td>
      <td><img name="img_IMAGE" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_IMAGE', 'myImages/Modules', 2)">
      </td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_IMAGE')"></td>
    </tr>
    <tr>
      <td>Image Open: </td>
      <td>
        <input type="TEXT" name="txt_IMAGEOPEN" value="<%=l_imageopen%>" class="textbox200" readonly>
      </td>
      <td><img name="img_IMAGEOPEN" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_IMAGEOPEN', 'myImages/Modules', 2)">
      </td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_IMAGEOPEN')"></td>
    </tr>
    <tr>
      <td>Mini Text: </td>
      <td>
        <input type="TEXT" name="txt_IMAGEMOUSEOVER" value="<%=l_imagemouseover%>" class="textbox200" readonly>
      </td>
      <td><img name="img_IMAGEMOUSEOVER" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_IMAGEMOUSEOVER', 'myImages/Modules', 2)">
      </td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_IMAGEMOUSEOVER')"></td>
    </tr>
    <tr>
      <td>Module Tag: </td>
      <td>
        <input type="TEXT" name="txt_IMAGETAG" value="<%=l_imagetag%>" class="textbox200" readonly>
      </td>
      <td><img name="img_IMAGETAG" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_IMAGETAG', 'myImages/tags', 2)">
      </td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_IMAGETAG')"></td>
    </tr>
    <tr>
      <td>Module Title: </td>
      <td>
        <input type="TEXT" name="txt_IMAGETITLE" value="<%=l_imagetitle%>" class="textbox200" readonly>
      </td>
      <td><img name="img_IMAGETITLE" src="/js/FVS.gif" width=15 height=15 border=0></td>
      <td>
        <input type="button" value="SET" class="RSLButtonSmall" onClick="OpenDirectory('txt_IMAGETITLE', 'myImages/titles', 2)">
      </td><td><input type="button" value="CLR" class="RSLButtonSmall" onclick="ClearItem('txt_IMAGETITLE')"></td>
    </tr>
    <tr>
      <td>Order: </td>
      <td>
        <input type="TEXT" name="txt_ORDERTEXT" value="<%=l_ordertext%>" class="textbox200">
      </td>
      <td><img name="img_ORDERTEXT" src="/js/FVS.gif" width=15 height=15 border=0></td>
    </tr>
    <tr>
      <td colspan=2 align=right>
        <input type="button" value=" SAVE " onClick="SaveForm()" class="RSLButton">
        <input type="button" value=" CANCEL " onClick="CancelAction()" class="RSLButton">		
      </td>
    </tr>
    <tr>
      <td colspan=2 align=right>
        <input type="button" value=" DELETE " onClick="DeleteItem()" class="RSLButton">
      </td>
    </tr>
  </table>
  <input type="hidden" name="hid_MODULEID" VALUE="<%=l_moduleid%>">
<input type="hidden" name="hid_ACTION" VALUE="<%=ACTION%>">
</form>
<!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
