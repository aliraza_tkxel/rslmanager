<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --&gt; HR Tools --&gt; Bank Holiday Management</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function load_BnkHoliday_Cal()
	        {
			RSLFORM.target = "frm_Holiday";
			// opens page in rsladdons as HolidayEntitlment is a virtual directory
			RSLFORM.action = "/HolidayEntitlement/bnk_hd_Calendar.aspx" 
			RSLFORM.submit();
			}

</script>


<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages();load_BnkHoliday_Cal();" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->


  <form name="RSLFORM" method="POST">
      <table>
          <tr>
                <td>
                    <iframe name="frm_Holiday" frameborder=0 width=400px height=400px style='display:Knone'></iframe> 
                </td>
                <td>
                    <div style='position:absolute;left:450;top:140;width:200px'>
                        <b><u>Bank Holidays</u></b><br><br>
                            This calendar allows you to view, add and delete the Bank Holidays for BHA and Meridian East staff. <br>
                         <br><font style='background-color:red'>&nbsp;&nbsp;</font> Bank Holiday<br> 
                         <br><font style='background-color:blue'>&nbsp;&nbsp;</font> Date selected
                         <br><br>
                                To change the selected month click on '<b>Month's Name</b>' to go 
                                backwards and forwards respectively. <br>
                                <br>To <b>add</b> a Bank Holiday, select the date from the calendar and press the Add button.
                         <br><br>To <b>delete</b> an existing Bank Holiday, select the date from the calendar and press the Delete button.
                         <br><br><b>NOTE</b>: If BHA team is selected, then adding or deleting a Bank Holiday will also affect the Meridian East Bank Holiday's. 
                    </div>
                </td>
           </tr>
       </table>   
    
  </FORM>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->

</BODY>
</HTML>
