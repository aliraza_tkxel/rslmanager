<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" xmlns:s="uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882" xmlns:html="http://www.w3.org/tr/rec-html40" result-ns="">

<xsl:template match="/"><xsl:apply-templates select="/xml/rs:data/z:row" /></xsl:template><xsl:template match="s:AttributeType"><xsl:choose><xsl:when test="@rs:hidden" /><xsl:when test="/xml/layout/field[@name $ieq$ context(-1)/@name]/@visible[text() $ieq$ 'false']"/></xsl:choose></xsl:template><xsl:template match="z:row"><xsl:for-each select="/xml/s:Schema/s:ElementType[@name='row']/s:AttributeType"><xsl:choose><xsl:when test="@rs:hidden" /><xsl:when test="/xml/layout/field[@name $ieq$ context(-1)/@name]/@visible[text() $ieq$ 'false']" /><xsl:otherwise>"<xsl:apply-templates select="context(-2)/@*[nodeName()=context(-1)/@name]" />",</xsl:otherwise></xsl:choose></xsl:for-each><br/></xsl:template>

<!-- whenever an element or attribute is found ("@*"), 
      it will run through this template -->
  <xsl:template match="@*">
<!-- depending on the format returned for the current item, 
              based on its node name, run its related Script function -->
    <xsl:choose>
      <xsl:when test="../../../s:Schema/s:ElementType/s:AttributeType[@name $ieq$ context(-2)/@name]/s:datatype/@dt:type [text() $ieq$ 'dateTime']"><xsl:eval>getDateOnly(this.text)</xsl:eval></xsl:when>
      <xsl:when test="../../../s:Schema/s:ElementType/s:AttributeType[@name $ieq$ context(-2)/@name]/s:datatype/@dt:type [text() $ieq$ 'date']"><xsl:eval>getDate(this.text)</xsl:eval></xsl:when>
      <xsl:otherwise><xsl:value-of select="text()" /></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:script>function getDateTime(value){ temp = new Date();
  temp.setYear(value.substr(0,4));
  temp.setMonth(value.substr(5,2));
  temp.setDate(value.substr(8,2));
  temp.setHours(value.substr(11,2));
  temp.setMinutes(value.substr(14,2));
  temp.setSeconds(value.substr(17,2));
  return temp.toLocaleString(); }
 function getDate(value) {
  temp = new Date();
  temp.setYear(value.substr(0,4));
  temp.setMonth(value.substr(5,2));
  temp.setDate(value.substr(8,2));
  return temp.toLocaleString().substr(0,10); }
 function getDateOnly(value) {
  temp = new Date();
  temp.setYear(value.substr(0,4));
  temp.setMonth(value.substr(5,2));
  temp.setDate(value.substr(8,2));
  return value.substr(8,2) + '/' + value.substr(5,2) + '/' + value.substr(0,4); }
</xsl:script>


</xsl:stylesheet>
