<!--#include file="../serverside/MD5.asp" -->
<%
	ReportId = Request.Querystring("Rid")
	if (NOT isNumeric(ReportId)) then
		ReportId = -1
	end if
	
	isReport = false

	TypeId = 3
	
	Call OpenDB()
	SQL = "SELECT RS.SAVENAME, RS.TYPEID, RS.DESCRIPTION, RSQ.REPORTQUERY, RS.LINEDATE FROM NS_REPORT_SAVE_QUERY RSQ " &_
			"INNER JOIN NS_REPORT_SAVE RS ON RS.SAVEREPORTID = RSQ.SAVEREPORTID " &_
			"WHERE RS.ORGID = " & "-1" & " AND RSQ.SAVEREPORTID = " & ReportId
'	rESPONSE.Write(SQL & "<br><BR>")
	Call OpenRs(rsQuery,SQL)
	if NOT rsQuery.EOF then
		isReport = true
		SQL = rsQuery("REPORTQUERY")
		TypeId = rsQuery("TYPEID")
		ReportName = rsQuery("SAVENAME")
		ReportDesc = rsQuery("DESCRIPTION")
		LineDateColumn = rsQuery("LINEDATE")
		if (NOT isNULL(ReportDesc) AND ReportDesc <> "") then
			ReportDesc = ReportDesc & "<BR>"
		end if
	end if
	Call CloseRs(rsQuery)
	
	'get the display column data and titles and sort columns, plus the widths
	DisplaySQL = "SELECT RS.SAVECOLUMNS, RT.GROUPED FROM NS_REPORT_SAVE RS INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID WHERE RS.SAVEREPORTID = " & ReportId
	Call OpenDB()
	Call OpenRs(rsSC, DisplaySQL)
	if NOT rsSC.EOF then
		SelectedList = rsSC("SAVECOLUMNS")
		isGrouped = rsSC("GROUPED")
	else
		SelectedList = -1
		isGrouped = -1
	end if
	Call CloseRs(rsSC)

	SQL_DATE = "SELECT ISNULL(COLUMN_CODE + ' ', '') + COLUMN_DISPLAY_NAME AS COLNAME, SQL_SELECT_COLUMN FROM NS_REPORT_COLUMN WHERE COLUMNID = " & LineDateColumn
	Call OpenRs(rsLD, SQL_DATE)
	if NOT rsLD.EOF then
		DateToUse = rsLD("SQL_SELECT_COLUMN")
		DateToUseName = rsLD("COLNAME")
	else
		DateToUse = "E.DATEADDED"
		DateToUseName = "NO2 Creation Date"
	end if
	
	MaxMinSQL = "SELECT MIN(" & DateToUse & ") AS MINDATE, MAX(" & DateToUse & ") AS MAXDATE " & SQL 
'	Response.Write MaxMinSQL
	Call OpenRs(rsMM, MaxMinSQL)
	if NOT rsMM.EOF then
		MinDate = rsMM("MINDATE")
		if isNull(MinDate) then MinDate = FormatDateTime(Now,2)
		MaxDate = rsMM("MAXDATE")
		if isNull(MaxDate) then MaxDate = FormatDateTime(Now,2)		
	else
		'use defaults
		MinDate = FormatDateTime(Now,2)
		MaxDate = FormatDateTime(Now,2)		
	end if
	Call CloseRs(rsMM)	
	
	DateDifference = DateDiff("m", MinDate, MaxDate) + 1

	MinMonth = Month(MinDate)	
	MinYear = Year(MinDate)
	MaxMonth = Month(MaxDate)
	MaxYear = Year(MaxDate)
	
	Set sd = Server.CreateObject("Scripting.Dictionary")

	Redim labels(DateDifference)
	
	tempMonth = MinMonth
	tempYear = MinYear
	for i=0 to DateDifference
		sd.Add tempMonth & "_" & tempYear, i
		labels(i) = MonthName(tempMonth, true) & " " & tempYear	
		tempMonth = tempMonth + 1
		if tempMonth = 13 then
			tempMonth = 1
			tempYear = tempYear + 1
		end if
	next

	'FIND THE COLUMN DETAILS AND THEN CREATE THE RESPECTIVE LOOP ITEMS.	
	DetailsSQL = "SELECT SQL_SELECT_COLUMN, ISNULL(COLUMN_CODE + ' ','') + COLUMN_DISPLAY_NAME AS COLUMN_TITLE, SQL_FILTER_COLUMN, FILTER_DATA_FOR_TABLE_OR_ARRAY, IS_TEXT_COMPARISON FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectedList
'	Response.Write DetailsSQL
	Call OpenRs(rsItem, DetailsSQL)
	if NOT rsItem.EOF then
		ColumnName = rsItem("COLUMN_TITLE")
		ColumnFilter = rsItem("SQL_FILTER_COLUMN")
		ColumnSelect = rsItem("SQL_SELECT_COLUMN")		
		ColumnFilters = rsItem("FILTER_DATA_FOR_TABLE_OR_ARRAY")
		FilterType = rsItem("IS_TEXT_COMPARISON")
		if FilterType = 1 then
			FilterText = "'"
		else
			FilterText = ""
		end if
	end if
	Call CloseRs(rsItem)	

'	if isNull(ColumnFilters) then
'		ItemCountSQL = "SELECT COUNT(*) AS TOTALITEMS FROM (SELECT " & ColumnFilter & " AS TOTALITEMS " & SQL & " GROUP BY " & ColumnFilter & ") VI"
		ItemCountSQL = "SELECT COUNT(DISTINCT " & ColumnSelect & ") AS TOTALITEMS " & SQL 		
'		Response.write 2
'	else
'		ItemSelectArray = Split(ColumnFilters, "|")
'		ItemCountSQL = "SELECT COUNT(*) AS TOTALITEMS FROM " & ItemSelectArray(0) & " " & ItemSelectArray(3)
'		Response.write 1
'	end if	
	Call OpenRs(rsC, ItemCountSQL)
	if NOT rsC.EOF then
		TotalItems = rsC("TOTALITEMS")
	else
		TotalItems = 0
	end if
	Call CloseRs(rsC)

	Redim data(DateDifference)
	
%>
<%
Set cd = CreateObject("ChartDirector.API")

'Create a XYChart object of size 300 x 280 pixels
Set c = cd.XYChart(760, 460, &Heeeeee, &H0, 1)

'Set the plotarea at (45, 30) and of size 200 x 200 pixels
PotentialSize = 40 * DateDifference
if PotentialSize > 600 then	PotentialSize = 600
if PotentialSize < 300 then 	PotentialSize = 300
Difference = CInt((600 - PotentialSize)/2)
Xstart = 85 + Difference

Call c.setPlotArea(Xstart, 70, PotentialSize, 220, &Hffffff, -1, -1, &Hc0c0c0, -1)

'Add a title to the chart
Call c.addTitle(ReportName, "arialbi.ttf", 15).setBackground(&Haaaaff)

'Add a title to the y axis
Call c.yAxis().setTitle("Count")

'Add a title to the x axis
Call c.xAxis().setTitle(DateToUseName)

'Add a legend box at (270, 75)
'Call c.addLegend(270, 75)

if TotalItems < 20 then
	'Add a legend box at (55, 25) (top of the chart) with horizontal layout. Use 8
	'pts Arial font. Set the background and border color to Transparent.
	ColumnName = "<*font=arialbd.ttf*>" & UCase(ColumnName) & "<*/*>"
	set lb = c.addLegend(25, 345, False, "", 8)
	Call lb.setBackground(cd.Transparent)
	Call lb.addKey2 (1, ColumnName, cd.Transparent)
end if

'Add a line layer to the chart
Set layer = c.addLineLayer2()

'Set the default line width to 2 pixels
Call layer.setLineWidth(2)

Dim ArrayText
ArrayText = ""

if TotalItems > 0 then
	
	ItemLoopCounter = 0

	'reset the data each loop
	for j=0 to Ubound(data)
		data(j) = 0
	next

	if isNull(ColumnFilters) then
		ChartSQL = "SELECT ISNULL(COUNT(*),0) AS GROUPEDCOUNT, " & ColumnSelect & " AS [NAME], MONTH(" & DateToUse & ") AS IMONTH, YEAR(" & DateToUse & ") AS IYEAR " & SQL & " GROUP BY " & ColumnSelect & ", YEAR(" & DateToUse & "), MONTH(" & DateToUse & ") ORDER BY [NAME] ASC"		
	else
		ChartSQL = "SELECT ISNULL(COUNT(*),0) AS GROUPEDCOUNT, " & ColumnSelect & " AS [NAME], MONTH(" & DateToUse & ") AS IMONTH, YEAR(" & DateToUse & ") AS IYEAR " & SQL & " GROUP BY " & ColumnSelect & ", YEAR(" & DateToUse & "), MONTH(" & DateToUse & ") ORDER BY [NAME] ASC"		
	end if
	
	LoopTotal = 0
	Call OpenRs(rsCH, ChartSQL)
	CurrentName = rsCH("NAME")
	if (isNull(CurrentName) OR CurrentName = "") then CurrentName = "NO VALUE"
	PreviousName = CurrentName
	while NOT rsCH.EOF
		CurrentName = rsCH("NAME")	
		if (isNull(CurrentName) OR CurrentName = "") then CurrentName = "NO VALUE"
		if (CStr(CurrentName) <> CStr(PreviousName)) then
			ItemLegendName = PreviousName & " (" & LoopTotal & ")"
			'add data set to chart
			Call layer.addDataSet(data, -1, ItemLegendName)
			ArrayText = ArrayText & "MultiArray [" & ItemLoopCounter & "] = new Array (" & Join(data, ",") & ",""" & Server.HTMLEncode(ItemLegendName) & """);" & VbCrLf
			LoopTotal = 0
			for j=0 to Ubound(data)
				data(j) = 0
			next
			PreviousName = CurrentName
			ItemLoopCounter = ItemLoopCounter + 1				
		end if
		rCount = rsCH("GROUPEDCOUNT") 
		key = rsCH("IMONTH") & "_" & rsCH("IYEAR")
		data(sd.item(key)) = rCount
		LoopTotal = LoopTotal + CLng(rCount)
		rsCH.moveNext	
	wend
	Call CloseRs(rsCH)

	if (ItemLoopCounter > 0) then
		ItemLegendName = PreviousName & " (" & LoopTotal & ")"
		'add data set to chart
		Call layer.addDataSet(data, -1, ItemLegendName)
		ArrayText = ArrayText & "MultiArray [" & ItemLoopCounter & "] = new Array (" & Join(data, ",") & ",""" & Server.HTMLEncode(ItemLegendName) & """);" & VbCrLf
		LoopTotal = 0
		for j=0 to Ubound(data)
			data(j) = 0
		next
		ItemLoopCounter = ItemLoopCounter + 1				
	end if
					
	'finally check for any empty values in the data
'	Dim NeedToAddToArray
'	NeedToAddToArray = false
'	Redim EmptyArray(DateDifference)

	'reset the data each loop
'	for j=0 to Ubound(EmptyArray)
'		EmptyArray(j) = 0
'	next
		
'	ExtraFilterPart = ""
'	if (FilterText = "'") then
'		ExtraFilterPart = " OR " & ColumnFilter & " = '' "
'	end if
'	ChartSQL = "SELECT ISNULL(COUNT(*),0) AS GROUPEDCOUNT, MONTH(" & DateToUse & ") AS IMONTH, YEAR(" & DateToUse & ") AS IYEAR " & SQL & " AND (" & ColumnFilter & " IS NULL " & ExtraFilterPart & ") GROUP BY YEAR(" & DateToUse & "), MONTH(" & DateToUse & ")"
'	LoopTotal = 0
'	Call OpenRs(rsEmpty, ChartSQL)
'	while NOT rsEmpty.EOF
'		rCount = rsEmpty("GROUPEDCOUNT") 
'		if rCount > 0 then
'			NeedToAddToArray = true
'		end if
'		key = rsEmpty("IMONTH") & "_" & rsEmpty("IYEAR")
'		EmptyArray(sd.item(key)) = rCount
'		LoopTotal = LoopTotal + CLng(rCount)
'		rsEmpty.moveNext	
'	wend
'	Call CloseRs(rsEmpty)
	
'	if NeedToAddToArray = true then
'		ItemLegendName = "NO VALUE (" & LoopTotal & ")"
'		Call layer.addDataSet(EmptyArray, -1, ItemLegendName)
'		ArrayText = ArrayText & "MultiArray [" & ItemLoopCounter & "] = new Array (" & Join(EmptyArray, ",") & ",""" & Server.HTMLEncode(ItemLegendName) & """);" & VbCrLf
'		TotalItems = TotalItems + 1
'	end if
	
end if

'ADD THESE AT THE END IN CASE THERE IS AN EMPTY ARRAY.
ArrayText = "MultiArray[" & TotalItems & "] = new Array(""" & Join(labels, """,""") & """);" & VbCrLf & ArrayText
ArrayText = "var MultiArray = new Array(" & TotalItems & ");" & VbCrLf & ArrayText

'make the max width 100px for the labels
Call c.xAxis().setLabelStyle().setMaxWidth(190)

'Display the trend line parameters as a text table formatted using CDML
Set textbox1 = c.addText(0, 30, "<*block*>Chart Generated : " & FormatDateTime(Now,1) & " " & FormatDateTime(Now,4) & ", By " & Replace(Session("svNAME"), "&nbsp;", " ") & "<*/*>", "arial.ttf", 8)

Set textbox2 = c.addText(0, 440, "<*block*>Created by RSL Manager. Chart Identifier : " & MD5(ReportID)& "<*/*>", "arial.ttf", 8)

'Use 10 pts Arial Bold/green (0x008000) font for the x axis labels. Set the
'label angle to 45 degrees.
Call c.xAxis().setLabelStyle("arial.ttf", 7, &00000).setFontAngle(45)

'Set the labels on the x axis.
Call c.xAxis().setLabels(labels)

'Create the image and save it in a temporary location
chart1URL = c.makeSession(Session, "chart1")

if TotalItems >= 20 then
	'Client side Javascript to show detail information "onmouseover"
	showText = "onmouseover='sh({x}, {dataSet});' "
	
	'Client side Javascript to hide detail information "onmouseout"
	hideText = "onmouseout='hi({x}, {dataSet});' "
else
	showText = ""
	hideText = ""
end if
'"title" attribute to show tool tip
toolTip = "title='{dataSetName} - {xLabel} : {value|0}'"

'Create an image map for the chart
imageMap = c.getHTMLImageMap("", "", showText & hideText & toolTip)
%>
