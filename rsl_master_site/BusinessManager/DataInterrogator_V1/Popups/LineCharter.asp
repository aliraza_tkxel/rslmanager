<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	ReportId = Request.Querystring("Rid")
	If (NOT isNumeric(ReportId)) Then
		ReportId = -1
	End If
%>
<!--#include file="lineserver.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator Chart</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function PrintChart() {
            document.getElementById("PrintDiv").style.display = "none";
            window.print();
            document.getElementById("PrintDiv").style.display = "block";
        }
    </script>
    <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="javascript">
    var TotalItems = <%=TotalItems%>;
    var Months = <%=DateDifference%>;

    <%=ArrayText%>

    function sh(i, j){
	    ItemSpan.innerHTML = MultiArray [j][Months+1]
	    DateSpan.innerHTML = MultiArray [TotalItems][i]
	    ValueSpan.innerHTML = MultiArray [j][i]
	    MouseOverDiv.style.visibility = "visible"
    }

    function hi (i, j){
	    MouseOverDiv.style.visibility = "hidden"
	}
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="preloadImages();window.focus()">
    <img src="sessionimage.asp?<%=chart1URL%>" border="0" usemap="#map1" alt="" />
    <map name="map1" id="map1">
        <%=imageMap%>
    </map>
    <div id="MouseOverDiv" style="visibility: hidden; padding: 4px; position: absolute;
        top: 380; left: 90; width: 350px; border: 1px solid blue; background-color: white">
        <span style="width: 60px"><strong>Item</strong>:</span><span id="ItemSpan"></span><br/>
        <span style="width: 60px"><strong>Period</strong>:</span><span id="DateSpan"></span><br/>
        <span style="width: 60px"><strong>Value</strong>:</span><span id="ValueSpan"></span><br/>
    </div>
    <div id="PrintDiv" style="position: absolute; top: 450; left: 700">
        <input type="button" class="iagButton" id="BtnPrint" name="BtnPrint" onclick="PrintChart()" value=" Print " />
    </div>
</body>
</html>
