function isUKPostCode(){
	elVal = document.getElementById("TheValue").value;
	elVal = TrimAll(elVal.toUpperCase());
	if (elVal == "") return false;
	result = elVal.match(/^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/g);
	if (!result) {
		alert("You must input a valid postcode.\nCorrect formats: [SW12 2EG][L4 5PQ][L17 2EA].");
		targetError("red");
		return false;	
		}
	else {
		elVal = String(elVal.replace(/\s/g,""));
		elValLen = elVal.length;
		elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		document.getElementById("TheValue").value = elVal;
		return true;
		}
	}

function targetError(iColor){
	document.getElementById("TheValue").style.color = iColor
	document.getElementById("TheValue").focus();
	document.getElementById("TheValue").select();	
	}

function filterText() { 
	elVal = document.getElementById("TheValue").value;
	if (elVal.match(/\<|\>|\"|\'|\%|\;|\&/)){
		elVal = elVal.replace(/\<|\>|\"|\'|\%|\;|\&/g,""); 
		document.getElementById("TheValue").value = elVal;
		}
	return true;		
	} 

function LTrimAll(str) {
	if (str==null){return str;}
	for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	return str.substring(i,str.length);
	}
	
function RTrimAll(str) {
	if (str==null){return str;}
	for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	return str.substring(0,i+1);
	}
	
function TrimAll(str) {
	return LTrimAll(RTrimAll(str));
	}

function isNumeric(){
	elVal = document.getElementById("TheValue").value;
	elVal = TrimAll(elVal);
	if (elVal == "") return false;	
	if ( parseFloat(elVal,10) == ( elVal*1) ){
		document.getElementById("TheValue").value = FormatNumber(elVal);
		return true;
		}
	else {
		alert("You must input a positive valid number.");
		targetError("red");
		return false;
		}
	}

function FormatNumber ( fPrice ) { 
   var sCurrency = "" + ( parseFloat(fPrice,10) + 0.00500000001 ); 
   var nPos = sCurrency.indexOf ( '.' ); 
   if ( nPos < 0 ){ 
      sCurrency += '.00'; 
   } 
   else { 
      sCurrency = sCurrency.slice ( 0, nPos + 3 ); 
      var nZero = 3 - ( sCurrency.length - nPos ); 
      for ( var i=0; i<nZero; i++ ) 
         sCurrency += '0'; 
   } 
   return sCurrency; 
}

function isDigit(num) {
	if (num.length>1){return false;}
	var string="1234567890";
	if (string.indexOf(num)!=-1){return true;}
	return false;
	}
	
function isInteger(){
	elVal = document.getElementById("TheValue").value;
	elVal = TrimAll(elVal);
	if (elVal == "") return false;		
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			alert("You must input a positive valid number.");
			targetError("red");
			return false;
			}
		}
	document.getElementById("TheValue").value = elVal;		
	return true;
	}

var dtCh= "/";
var minYear=1900;
var maxYear=2100;
	
function isIntegerScan(s){
	var i;
	for (i = 0; i < s.length; i++){   
		// Check that current character is number.
		var c = s.charAt(i);
		if (((c < "0") || (c > "9"))) return false;
	}
	// All characters are numbers.
	return true;
}
	
function stripCharsInBag(s, bag){
	var i;
	var returnString = "";
	// Search through string's characters one by one.
	// If character is not in bag, append to returnString.
	for (i = 0; i < s.length; i++){   
		var c = s.charAt(i);
		if (bag.indexOf(c) == -1) returnString += c;
	}
	return returnString;
}
	
function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}
		
function isDate(){
	dtStr = document.getElementById("TheValue").value;
	if (dtStr == "") return false;
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	STANDARD_ERROR = "You have entered an invalid date '" + dtStr + "'.\nPlease re-enter in the format dd/mm/yyyy."
	if (pos1==-1 || pos2==-1){
		alert(STANDARD_ERROR);
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert(STANDARD_ERROR);
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert(STANDARD_ERROR);
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert(STANDARD_ERROR);
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isIntegerScan(stripCharsInBag(dtStr, dtCh))==false){
		alert(STANDARD_ERROR);
		return false
	}
	return true
}	