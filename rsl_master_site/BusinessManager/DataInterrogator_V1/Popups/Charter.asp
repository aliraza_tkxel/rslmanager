<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	ReportId = Request.Querystring("Rid")
	If (NOT isNumeric(ReportId)) Then
		ReportId = -1
	End If
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator Chart</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function PrintChart() {
            document.getElementById("PrintDiv").style.display = "none";
            window.print();
            document.getElementById("PrintDiv").style.display = "block";
        }
    </script>
    <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="preloadImages();window.focus()">
    <img src="server.asp?Rid=<%=ReportId%>" alt="" />
    <div id="PrintDiv" style="position: absolute; top: 450; left: 700">
        <input type="button" class="iagButton" id="BtnPrint" name="BtnPrint" onclick="PrintChart()" value=" Print " />
    </div>
</body>
</html>
