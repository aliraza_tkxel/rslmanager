<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../serverside/MD5.asp" -->
<%
	ReportId = Request.Querystring("Rid")
	if (NOT isNumeric(ReportId)) then
		ReportId = -1
	end if
	
	isReport = false

	TypeId = 3
	
	Call OpenDB()
	SQL = "SELECT RS.SAVENAME, RS.TYPEID, RS.DESCRIPTION, RSQ.REPORTQUERY FROM NS_REPORT_SAVE_QUERY RSQ " &_
			"INNER JOIN NS_REPORT_SAVE RS ON RS.SAVEREPORTID = RSQ.SAVEREPORTID " &_
			"WHERE RS.ORGID = -1 AND RSQ.SAVEREPORTID = " & ReportId
	Call OpenRs(rsQuery,SQL)
	if NOT rsQuery.EOF then
		isReport = true
		SQL = rsQuery("REPORTQUERY")
		TypeId = rsQuery("TYPEID")
		ReportName = rsQuery("SAVENAME")
		ReportDesc = rsQuery("DESCRIPTION")
		if (NOT isNULL(ReportDesc) AND ReportDesc <> "") then
			ReportDesc = ReportDesc & "<BR>"
		end if
	end if
	Call CloseRs(rsQuery)
	
	'get the display column data and titles and sort columns, plus the widths
	DisplaySQL = "SELECT RS.SAVECOLUMNS, RT.GROUPED FROM NS_REPORT_SAVE RS INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID WHERE RS.SAVEREPORTID = " & ReportId
	Call OpenDB()
	Call OpenRs(rsSC, DisplaySQL)
	if NOT rsSC.EOF then
		SelectedList = rsSC("SAVECOLUMNS")
		isGrouped = rsSC("GROUPED")
	else
		SelectedList = -1
		isGrouped = -1
	end if
	Call CloseRs(rsSC)
	
	'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
	SelectArray = Split(SelectedList, ",")
	TotalColumns = Ubound(SelectArray)
	if (isGrouped = 1) then TotalColumns = TotalColumns + 1

	Redim DisplayTitles(TotalColumns)

	SelectSQL = ""
	for i=0 to Ubound(SelectArray)
		if (i > 0) then SelectSQL = SelectSQL & " UNION ALL "
		SelectSQL = SelectSQL & "SELECT '<font color=blue>' + ISNULL(COLUMN_CODE,'') + '</font> ' + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
	next
	
	column_counter = 0
	Call OpenRs(rsC,SelectSQL)
	while Not rsC.EOF
		DisplayTitles(column_counter) = rsC("COLUMN_TITLE")
		column_counter = column_counter + 1
		rsC.moveNext
	wend
	Call CloseRs(rsC)

	'ADD THE GROUP PART IF NECCESSARY
	if (isGrouped = 1) then
		DisplayTitles(column_counter) = "COUNT"
	end if	
	
	Redim data(30)
	Redim labels(30)
	ChartSQL = Replace(SQL, "SELECT", "SELECT TOP 31", 1, 1) & " ORDER BY GROUPCOUNT DESC"
	counter = 0
	LoopTotal = 0
	LoopValue = 0
	Call OpenRs(rsCH, ChartSQL)
	while NOT rsCH.EOF
		if (counter < 30) then
			data(counter) = rsCH(1) 
			LoopTotal = LoopTotal + CLng(rsCH(1))
			LabelForChart = Trim(rsCH(0))
			if (isNull(LabelForChart) OR LabelForChart = "") then LabelForChart = "NO VALUE"
			if (TypeId = 3) then
				labels(counter) = LabelForChart & "<*block*><*color=FF*> (" & data(counter) & ")<*/*>"
			else
				labels(counter) = LabelForChart
			end if
		else
			'this is the top min value which is not displayed
			LoopValue = rsCH(1)
		end if
		counter = counter + 1
		rsCH.moveNext	
	wend
	Call CloseRs(rsCH)

	TooManyItems = false
	'if the counter is greater than 30 then means too many items for the chart
	if (counter > 30) then
		TooManyItems = true		
		'have to the following replace as the statement afterwards creates a view
		'which needs each column to have a name and sometimes the first group does not.
		SQL = REPLACE(SQL, ", COUNT(*) AS GROUPCOUNT", " AS FIRSTGROUP, COUNT(*) AS GROUPCOUNT")
		'make a view for the data and get totals
		SQL = "SELECT COUNT(*) AS TOTALITEMS, SUM(GROUPCOUNT) AS TOTALCOUNT FROM (" & SQL & ") VIEW_TABLE"
		Call OpenRs(rsGroup, SQL)
		if (NOT rsGroup.EOF) then
			'remove the 30 items already displayed	
			TotalItems = rsGroup("TOTALITEMS") - 30
			'remove the total itemsalready displayed
			TotalCount = rsGroup("TOTALCOUNT") - LoopTotal	
		end if
		Call CloseRs(rsGroup)
	end if
	
	if (counter > 30) then
		'need to remove the last two items if the counter is greater than 30
		Redim Preserve data(counter-2)
		Redim Preserve labels(counter-2)	
	else
		Redim Preserve data(counter-1)
		Redim Preserve labels(counter-1)	
	end if
%>
<%
if (TypeId = 3) then
%>
<%



Set cd = CreateObject("ChartDirector.API")

'The data for the pie chart
'data = Array(25, 18, 15, 12, 8, 30, 35)

'The labels for the pie chart
'labels = Array("Labor", "Licenses", "Taxes", "Legal", "Insurance", _
 '   "Facilities", "Production")

'Create a PieChart object of size 360 x 300 pixels
Set c = cd.PieChart(760, 460, &Heeeeee, &H0, 1)

'Set the center of the pie at (180, 140) and the radius to 100 pixels
Call c.setPieSize(380, 230, 120)

'Add a title to the chart
Call c.addTitle(ReportName, "arialbi.ttf", 15).setBackground(&Haaaaff)

'Draw the pie in 3D
Call c.set3D()

'Use the side label layout method
Call c.setLabelLayout(cd.SideLayout)

'Set the label box the same color as the sector with a 1 pixel 3D border
Call c.setLabelStyle().setBackground(cd.SameAsMainColor, cd.Transparent, 1)

'Set the start angle to 135 degrees may improve layout when there are many small
'sectors at the end of the data array (that is, data sorted in descending
'order). It is because this makes the small sectors position near the horizontal
'axis, where the text label has the least tendency to overlap. For data sorted
'in ascending order, a start angle of 45 degrees can be used instead.
Call c.setStartAngle(135)

'Set the pie data and the pie labels
Call c.setData(data, labels)

'make the max width 100px for the labels
Call c.setLabelStyle("arial.ttf", 8).setMaxWidth(225)

'Display the trend line parameters as a text table formatted using CDML
Set textbox1 = c.addText(0, 30, "<*block*>Chart Generated : " & FormatDateTime(Now,1) & " " & FormatDateTime(Now,4) & ", By " & Session("NAME") & "<*/*>", "arial.ttf", 8)

Set textbox2 = c.addText(0, 440, "<*block*>Created by RSL Manager. Chart Identifier : " & MD5(ReportID)& "<*/*>", "arial.ttf", 8)

if TooManyItems = true then 
	Set textbox3 = c.addText(0, 43, "<*block*><*font=arial.ttf,color=FF0000*>NOTE: " & TotalItems & " items not displayed with total count of " & TotalCount & " where items are " & LoopValue & " or less.<*/*>", "arial.ttf", 8)
end if

'Call textbox.setBackground(&Hc0c0c0, 0, 1)


'Explode the 1st sector (index = 0)
'Call c.setExplode(1)

'output the chart
Response.ContentType = "image/gif"
Response.BinaryWrite c.makeChart2(cd.GIF)
Response.End

%>
<%
else
%>
<%
Set cd = CreateObject("ChartDirector.API")

'The data for the bar chart
'data = Array(85, 156, 179.5, 211, 123)

'The labels for the bar chart
'labels = Array("Mon", "Tue", "Wed", "Thu", "Fri")

'Create a XYChart object of size 300 x 280 pixels
Set c = cd.XYChart(760, 460, &Heeeeee, &H0, 1)

'Set the plotarea at (45, 30) and of size 200 x 200 pixels
PotentialSize = 40 * counter
if PotentialSize > 600 then	PotentialSize = 600
if PotentialSize < 300 then 	PotentialSize = 300
Difference = CInt((600 - PotentialSize)/2)
Xstart = 105 + Difference

Call c.setPlotArea(Xstart, 70, PotentialSize, 220)

'Add a title to the chart
Call c.addTitle(ReportName, "arialbi.ttf", 15).setBackground(&Haaaaff)

'Add a title to the y axis
Call c.yAxis().setTitle("Count")

'Add a title to the x axis
Call c.xAxis().setTitle("")

'Add a bar chart layer with green (0x00ff00) bars using the given data
'Call c.addBarLayer(data, &Hff00).set3D()

'Add a multi-color bar chart layer
Set layer = c.addBarLayer3(data)

'Set layer to 3D with 10 pixels 3D depth
Call layer.set3D(10)

'Set bar shape to circular (cylinder)
Call layer.setBarShape(cd.CircleShape)

'make the max width 100px for the labels
Call c.xAxis().setLabelStyle().setMaxWidth(190)

'Display the trend line parameters as a text table formatted using CDML
Set textbox1 = c.addText(0, 30, "<*block*>Chart Generated : " & FormatDateTime(Now,1) & " " & FormatDateTime(Now,4) & ", By " & Replace(Session("svNAME"), "&nbsp;", " ") & "<*/*>", "arial.ttf", 8)

Set textbox2 = c.addText(0, 440, "<*block*>Created by IAGmanager. Chart Identifier : " & MD5(ReportID)& "<*/*>", "arial.ttf", 8)

if TooManyItems = true then 
	Set textbox3 = c.addText(0, 43, "<*block*><*font=arial.ttf,color=FF0000*>NOTE: " & TotalItems & " items not displayed with total count of " & TotalCount & " where items are " & LoopValue & " or less.<*/*>", "arial.ttf", 8)
end if

'Use 10 pts Arial Bold/green (0x008000) font for the x axis labels. Set the
'label angle to 45 degrees.
Call c.xAxis().setLabelStyle("arial.ttf", 7, &00000).setFontAngle(45)

'Set the labels on the x axis.
Call c.xAxis().setLabels(labels)

'output the chart
Response.ContentType = "image/gif"
Response.BinaryWrite c.makeChart2(cd.GIF)
Response.End
%>
<%
end if
%>