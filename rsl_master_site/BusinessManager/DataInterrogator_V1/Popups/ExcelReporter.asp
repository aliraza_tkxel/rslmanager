<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator > Export to Excel</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
<%
	ReportId = Request("Rid")
	If (NOT isNumeric(ReportId)) Then
		ReportId = -1
	End If

	ReportId = CLng(ReportId)
	isReport = False

	SQL = "SELECT C.CATEGORY AS PROJECT, isnull(O.NAME,'RSL manager') AS ORGNAME, RS.SAVENAME, RS.DESCRIPTION, RSQ.REPORTQUERY, NEWID() AS UNIQID FROM NS_REPORT_SAVE_QUERY RSQ " &_
			"INNER JOIN NS_REPORT_SAVE RS ON RS.SAVEREPORTID = RSQ.SAVEREPORTID " &_
			 "INNER JOIN NS_GROUPPROJECT C ON C.GROUPPROJECTID = RS.PROJECTID " &_
			"left JOIN s_ORGANISATION O ON O.ORGID = RS.ORGID " &_
			"WHERE RS.ORGID = " & "-1" & " AND RSQ.SAVEREPORTID = " & ReportId
	Call OpenDB()
	Call OpenRs(rsQuery,SQL)
	If NOT rsQuery.EOF Then
		isReport = True
		SQL = rsQuery("REPORTQUERY")
		ReportName = rsQuery("SAVENAME")
		ReportDesc = rsQuery("DESCRIPTION")
		Project = rsQuery("PROJECT")
		Organisation = rsQuery("ORGNAME")
		NewID = rsQuery("UNIQID")
		NewID = Replace(Replace(NewID, "}", ""), "{", "")
	End If
	Call CloseRs(rsQuery)
%>
<%
	Set fso = CreateObject("Scripting.FileSystemObject")

	GP_curPath = Request.ServerVariables("PATH_INFO")
	GP_curPath = "/BusinessManager/DataInterrogator_v1/"
	GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
		GP_curPath = GP_curPath & "/"
	end if

	FullPath = Trim(Server.mappath(GP_curPath)) & "\ExportFiles\"
	FullPathForXSLFile = Trim(Server.mappath(GP_curPath)) & "\"
	FileName = "EXPORT-" & NewID
	Extension = ".csv"

	If (fso.FileExists(FullPath & FileName & Extension)) Then
		GP_FileExist = true
	End If
	if GP_FileExist then
		Begin_Name_Num = 0
		while GP_FileExist
			Begin_Name_Num = Begin_Name_Num + 1
			TempFileName = FileName & "_" & Begin_Name_Num
			GP_FileExist = fso.FileExists(FullPath & TempFileName & Extension)
		wend
		FileName = TempFileName
	end if
	'Response.Write FullPath & FileName & Extension & "<BR>"
	Set MyFile= fso.CreateTextFile(FullPath & FileName & Extension, True)
%>
<%
	'get the display column data and titles and sort columns, plus the widths
	DisplaySQL = "SELECT RS.SAVECOLUMNS, RT.GROUPED FROM NS_REPORT_SAVE RS " &_
				 "INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID " &_
				 "WHERE RS.SAVEREPORTID = " & ReportId
	Call OpenDB()
	Call OpenRs(rsSC, DisplaySQL)
	If NOT rsSC.EOF Then
		SelectedList = rsSC("SAVECOLUMNS")
		isGrouped = rsSC("GROUPED")
	Else
		SelectedList = -1
		isGrouped = -1
	End If
	Call CloseRs(rsSC)

	'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
	SelectArray = Split(SelectedList, ",")
	TotalColumns = Ubound(SelectArray)
	if (isGrouped = 1) then TotalColumns = TotalColumns + 1

	MyFile.Write """Name :"",,"" " & ReportName & """" & VbCrLf
	if (NOT isNULL(ReportDesc) AND ReportDesc <> "") then
		MyFile.Write """Description :"",,"" " & ReportDesc & """" & VbCrLf
	end if
	MyFile.Write """Project :"",,"" " & Project & """" & VbCrLf

	MyFile.Write """Date Created :"",,"" " & FormatDateTime(Now,1) & " " & FormatDateTime(Now,4) & """" & VbCrLf
	MyFile.Write """By :"",,"" " & Replace(Session("svNAME"), "&nbsp;", " ") & """" & VbCrLf
	MyFile.Write """From :"",,"" " & Organisation & """" & VbCrLf
	MyFile.Write """File Identifier :"",,"" " & NewID & """" & VbCrLf

	MyFile.Write VbCrLf & VbCrLf

	SelectSQL = ""
	for i=0 to Ubound(SelectArray)
		if (i > 0) then SelectSQL = SelectSQL & " UNION ALL "
		SelectSQL = SelectSQL & "SELECT SQL_SELECT_COLUMN, DISPLAY_WIDTH, ISNULL(COLUMN_CODE + ' ','') + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
	next
%>
<% 
	HeaderCount = 0
	Call OpenRs(rsC,SelectSQL)
	while Not rsC.EOF
		if (HeaderCount = 0) then
			MyFile.Write """" & rsC("COLUMN_TITLE") & """"
		else
			MyFile.Write ",""" & rsC("COLUMN_TITLE") & """"
		end if
		HeaderCount = 1
		rsC.moveNext
	wend
	Call CloseRs(rsC)

	'ADD THE GROUP PART IF NECCESSARY
	If (isGrouped = 1) Then
		MyFile.Write ",""COUNT"""
	End If

	MyFile.Write VbCrLf
%>
<%	'RW SQL
	'Response.end()
	Dim mydoc
	Set mydoc = Server.CreateObject("Microsoft.XMLDOM")
	mydoc.async = false

	Set xslFile = Server.CreateObject("Microsoft.XMLDOM")
	xslFile.Load FullPathForXSLFile & "\rs2txt.xsl"

	Call OpenRs(rstemp, SQL)
	rstemp.Save mydoc, 1

	'MyFile.Write replace(mydoc.transformNode(xslFile),",<br />",vbcrlf)

	DAR = replace(mydoc.transformNode(xslFile),Chr(13),"")
	DAR = replace(DAR,chr(9)," ")
	DAR = replace(DAR,chr(10)," ")
	DAR = replace(DAR,",<br />",vbcrlf)
	MyFile.Write DAR

	Set MyFile = Nothing

	SQL = "INSERT INTO NS_REPORT_EXPORT(REPORTID, FILECODE, ORGID, FULLPATH, FILENAME, CREATED, CREATEDBY) " &_
		"VALUES (" & ReportID & ", '" & NewID & "', " & "-1" & ", '" & FullPath & FileName & Extension & "', '" & FileName & Extension & "', GETDATE(), " & Session("USERID") & ")"
	Conn.Execute SQL

	Call CloseDB()

%>
    <script type="text/javascript" language="javascript">
        function ReportReady() {
            var Filename = "<%=GP_curPath%>ExportFiles/<%=FileName%><%=Extension%>"
            parent.progress_stop()
            parent.document.getElementById("parentbar").style.display = "none"
            parent.document.getElementById("titlebar").innerHTML = '<font style=\'font-size:12px;color:red\'><b style=\'color:black\'><u>File is ready for download.</u></b><br>Right click link below and select \'<b>Save target as...</b>\'<br>Select a location for the file<br>Select \'<b>All Files</b>\' for \'<b>Save as type</b>\'<br>Make sure the filename ends in \'<b>.csv</b>\' and not in \'<b>.xls</b>\'<br>Finally click \'<b>Save</b>\'.</font><br><br> <b><a href="' + Filename + '"><font color=blue>Right Click to Begin</font></a></b>'
        }
    </script>
</head>
<body onload="ReportReady()">
</body>
</html>
