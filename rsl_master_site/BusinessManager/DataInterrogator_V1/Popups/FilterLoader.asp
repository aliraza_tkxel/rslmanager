<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim FilterItem_EOF
' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - 

'
' ***************************************************************************************************

	' ***************************************************************************************************
	'	 A ) 	BRING BACK INFO ABOUT COLUMN YOU WANT TO FILTER ON
	' ***************************************************************************************************
         	
	FilterID = Request.QueryString("FilterID")
	
	' NOTE : THIS RECORDSET DOES NOT CLOSE UNTIL THE BOTTOM OF THE PAGE
	Call OpenDB()
	SQL = "SELECT * FROM NS_REPORT_COLUMN WHERE COLUMNID = " & Replace(FilterID, "'", "''")
	'rw SQL 
	Call OpenRs(rsFilterItem, SQL)
	
	if (NOT rsFilterItem.EOF) then
		FilterType = rsFilterItem("FILTER_TYPE")
		FilterItem_EOF = false
		
		' SET VARIABLES - THIS IS THE INFORMATION THAT IS ASSOCIATED WITH THE FILTER COLUMN
		FilterColumn = rsFilterItem("SQL_FILTER_COLUMN")
		
		'rw FilterColumn
		FilterName = rsFilterItem("COLUMN_CODE") & " " & rsFilterItem("COLUMN_DISPLAY_NAME")
		isText = rsFilterItem("IS_TEXT_COMPARISON")
		
		FILTER_DATA_FOR_TABLE_OR_ARRAY = rsFilterItem("FILTER_DATA_FOR_TABLE_OR_ARRAY")
		
		parent = rsFilterItem("PARENT")
		parentkey = rsFilterItem("PARENTKEY")
		 
		
	else
		FilterItem_EOF = true
	end if
	Call CloseRs(rsFilterItem)
	' ***************************************************************************************************
	'	 B ) 	DEPENDANTS - 	1	THERE ARE SOME COLUMNS THAT CANNOT BE SELECTED FOR FILTERING UNLESS
	'						 		ANOTHER COLUMN HAS BEEN SELECTED FIRST.
	'							2	THIS IS SO WE CAN FILTER PROPERLY ON OUR SQL - SO COLUMNID IS DEP ON PARENT
	'							3	THIS MEANS THAT A DROP DOWN MAY BE INFLUENCED BY THE DROP DOWN IN A DEPENDANT
	'								FILTER POP UP.
	'				
	'							EXAMPLE : 	ADDRESS 2 CANNOT BE SELECTED UNLESS ADDRESS 1 IS SELECTED
	'										COLUMN ID 	= 	ADDRESS 2
	'										PARENT		= 	ADDRESS 1
	' ***************************************************************************************************	

	JavaItem = ""
	JavaChildren = ""
	JavaParent = ""
	JavaParentKey = ""
	JavaItemDisplay = ""
	
	SQL = "SELECT COLUMNID, CHILDREN, ISNULL(COLUMN_CODE + ' ','') + COLUMN_DISPLAY_NAME AS DISPLAY_NAME, PARENT, PARENTKEY FROM NS_REPORT_HIERACHY"
	'RW  SQL & "<br>"
	Call OpenRs(rsH, SQL)
	while NOT rsH.EOF 
		if (JavaItem = "") then
			JavaItem = "'" & rsH("COLUMNID") & "'"
			JavaChildren = "'" & rsH("CHILDREN") & "'"
			JavaParent = "'" & rsH("PARENT") & "'"	
			JavaParentKey = "'" & rsH("PARENTKEY") & "'"
			JavaItemDisplay = "'" & rsH("DISPLAY_NAME") & "'"					
		else
			JavaItem = JavaItem & ",'" & rsH("COLUMNID") & "'"
			JavaChildren = JavaChildren & ",'" & rsH("CHILDREN") & "'"
			JavaParent = JavaParent & ",'" & rsH("PARENT") & "'"	
			JavaParentKey = JavaParentKey & ",'" & rsH("PARENTKEY") & "'"
			JavaItemDisplay = JavaItemDisplay & ",'" & rsH("DISPLAY_NAME") & "'"					
		end if
		rsH.moveNext
	wend
	Call CloseRs(rsH)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator > Filter</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
        <style type="text/css">
    body
    {
        background-color : #e0e0e0;
    }
    lscButtonSmall
    {
        font-family: tahoma;
        font-size: 10px;
        font-weight: normal;
        border: 1px solid black;
        background: #c0c0c0;
    }
</style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="Javascript" src="StandardFunctions.js" defer="defer"></script>
<script type="text/javascript" language="JavaScript" src="/js/calendarFunctions.js" defer="defer"></script>
<script type="text/javascript" language="JavaScript" defer="defer">

// BUILD THE ARRAY FOR THE DEPENDANTS (PART B)
var ArrayItem     	 = new Array(<%=JavaItem%>);
var ArrayParent   	 = new Array(<%=JavaParent%>);
var ArrayChildren    = new Array(<%=JavaChildren%>);
var ArrayParentKey   = new Array(<%=JavaParentKey%>);
var ArrayItemDisplay = new Array(<%=JavaItemDisplay%>);

<% if (FilterType = "ARRAY" OR FilterType = "TABLE") then %>
//THESE FUNCTIONS ARE FOR THE ARRAY AND TABLE TYPES.
function ADDITEM(){
	theItem = document.getElementById("FOptions").value
	if (theItem != "") {
		if (document.getElementById("FilterCode").value == "")
			document.getElementById("FilterCode").value = theItem
		else {
			tempStr = document.getElementById("FilterCode").value
			tempArray = tempStr.split("*|Z|*")
			matched = false
			for (i=0; i<tempArray.length; i++){
				if (tempArray[i] == theItem){
					matched = true
					break;
					}
				}
			if (matched == false) document.getElementById("FilterCode").value += "*|Z|*" + theItem		
			}
		document.getElementById("FOptions").selectedIndex = 0
		GenerateCode()		
		}
	}
	
function DELETEITEM(){
	theItem = document.getElementById("FOptions").value
	if (theItem != "") {
		newStr = ""
		tempStr = document.getElementById("FilterCode").value
		if (tempStr != "") {
			tempArray = tempStr.split("*|Z|*")
			for (i=0; i<tempArray.length; i++){
				if (tempArray[i] != theItem){
					if (newStr == "")
						newStr = tempArray[i]
					else
						newStr += "*|Z|*" + tempArray[i]
					}
				}
			}
		document.getElementById("FilterCode").value = newStr;
		document.getElementById("FOptions").selectedIndex = 0
		GenerateCode()
		}
	}

function GenerateCode(){
	codedata = document.getElementById("FilterCode").value
	if (codedata != "") {
		document.getElementById("FilterCode").disabled = true;	
		CodeArray = codedata.split("*|Z|*")
		//break the actual column filter item into how ever many components it is made of
		//the item will be separated by the '|'
		fcolArray = document.getElementById("FilterColumn").value.split("|")
		fname = document.getElementById("FilterName").value		
		colstr = ""
		namestr = ""
		for (j=0; j<CodeArray.length; j++){
			document.getElementById("FOptions").value = CodeArray[j]
			prettyname = document.getElementById("FOptions").options[document.getElementById("FOptions").selectedIndex].text
			if (colstr == "") {
				namestr += prettyname
				//next loop through the fcolArray and build the appropriate sql statement
				//this for loops needs and if statement for the first item which should not have an OR part.
				for (k=0; k<fcolArray.length; k++) {
					if (k == 0)
						colstr += "(" + fcolArray[k] + " = " + CodeArray[j] + ")"
					else
						colstr += " OR (" + fcolArray[k] + " = " + CodeArray[j] + ")"					
					}
				}
			else {
				namestr += " OR " + prettyname
				//next loop through the fcolArray and build the appropriate sql statement
				//this part does not need an if statement becuase all items have to be added with an OR statement
				for (k=0; k<fcolArray.length; k++) 
					colstr += " OR (" + fcolArray[k] + " = " + CodeArray[j] + ")"					
				}
			}
			namestr = " " + fname + " = (" + namestr + ")"
			colstr = " AND (" + colstr + ")"
		}
	else {
		namestr = ""
		colstr = ""
		}
	document.getElementById("FilterSQL").value = colstr
	document.getElementById("FilterText").value = namestr
	document.getElementById("FOptions").selectedIndex = 0	
	document.getElementById("FilterCode").disabled = false;
	}
// END ARRAY AND TABLE FUNCTIONS

<% Else %>	

//THESE FUNCTIONS ARE FOR THE NUMBER, DATE AND TEXT TYPES
function ADDTEXTITEM(){
	ItemData = document.getElementById("TheValue").value;
	if (ItemData != "" && CallValidationFunction()) {
		tempStr = document.getElementById("FilterCode").value
		if (tempStr == ""){
			SignStr = ""
			ValueStr = ""
			SignStr += document.getElementById("Sign").value
			ValueStr += ItemData
			}
		else {
			tempArray = tempStr.split("||SIGNVALUEDIVIDER||")
			SignStr = tempArray[0]
			ValueStr = tempArray[1]
			SignArray = SignStr.split(",")
			if (SignArray.length == 7) {
				alert ("Max Filter Items have been reached for this parameter.")
				}
			else {
				SignStr += "," + document.getElementById("Sign").value
				ValueStr += "," + ItemData
				}
			}
		document.getElementById("FilterCode").value = SignStr + "||SIGNVALUEDIVIDER||" + ValueStr
		}
	document.getElementById("TheValue").value = ""
	GenerateCode()
	}

function GenerateCode(){
	tempStr = document.getElementById("FilterCode").value
	if (tempStr != ""){
		isRealText = document.getElementById("isText").value
		AndOrText = document.getElementById("AndOr").value
		TextQuotes = ""
		if (isRealText == 1) 
			TextQuotes = "'"
		tempArray = tempStr.split("||SIGNVALUEDIVIDER||")
		SignStr = tempArray[0]
		ValueStr = tempArray[1]
		SignArray = SignStr.split(",")
		ValueArray = ValueStr.split(",")
		fcol = document.getElementById("FilterColumn").value
		fname = document.getElementById("FilterName").value		
		for (i=0; i<SignArray.length; i++){
			if (i == 0){
				LikeClause = ""
				if (SignArray[i] == "LIKE" || SignArray[i] == "NOT LIKE")
					LikeClause = "%"
				SQLCode = "(" + fcol + " " + SignArray[i] + " " + TextQuotes + LikeClause + ValueArray[i] + LikeClause + TextQuotes + ")"
				TableCode = "<div style='width:320px;height:90px;border:1px solid red;background-color:white;overflow:auto'><table class='textbox' style='border:none' cellspacing=0 cellpadding=1 border=0><tr><td width='260px'>" + fname + " " + SignArray[i] + " " + TextQuotes + ValueArray[i] + TextQuotes + "</td><td width='60px' onmouseover='HI(this)' onmouseout='BYE(this)' onclick='DELETEROW(" + i + ")' style='cursor:hand;text-align:center'>DELETE</td></tr>"
				PrettyCode = " " + fname + " " + SignArray[i] + " " + TextQuotes + ValueArray[i] + TextQuotes
				}
			else {
				LikeClause = ""
				if (SignArray[i] == "LIKE" || SignArray[i] == "NOT LIKE")
					LikeClause = "%"
				SQLCode += " " + AndOrText + " (" + fcol + " " + SignArray[i] + " " + TextQuotes + LikeClause + ValueArray[i] + LikeClause + TextQuotes + ")"
				TableCode += "<tr><td> " + AndOrText + " " + fname + " " + SignArray[i] + " " + TextQuotes + ValueArray[i] + TextQuotes + "</td><td onmouseover='HI(this)' onmouseout='BYE(this)' onclick='DELETEROW(" + i + ")' style='cursor:hand;text-align:center'>DELETE</td></tr>"
				PrettyCode += " " + AndOrText + " " + fname + " " + SignArray[i] + " " + TextQuotes + ValueArray[i] + TextQuotes
				}			
			}
		document.getElementById("FilterSQL").value = " AND (" + SQLCode + ")"
		document.getElementById("ListItems").innerHTML = TableCode + "</table></div>"
		document.getElementById("FilterText").value = PrettyCode		
		}
	else {
		document.getElementById("FilterSQL").value = ""
		document.getElementById("ListItems").innerHTML = "<div style='width:320px;height:90px;border:1px solid red;background-color:white;overflow:auto'>&nbsp;</div>"
		document.getElementById("FilterText").value = ""				
		}
	}

function DELETEROW(RowNumber){
	tempStr = document.getElementById("FilterCode").value
	if (tempStr != ""){
		tempArray = tempStr.split("||SIGNVALUEDIVIDER||")
		SignStr = tempArray[0]
		ValueStr = tempArray[1]
		SignArray = SignStr.split(",")
		ValueArray = ValueStr.split(",")
		NewSignStr = ""
		NewValueStr = ""
		if (SignArray.length > 1)
			for (i=0; i<SignArray.length; i++){
				if (i != RowNumber){
					if (NewSignStr == ""){
						NewSignStr = SignArray[i]
						NewValueStr = ValueArray[i]
						}
					else {
						NewSignStr += "," + SignArray[i]
						NewValueStr += "," + ValueArray[i]
						}
					}
				}
			if (NewSignStr != "") 
				document.getElementById("FilterCode").value = NewSignStr + "||SIGNVALUEDIVIDER||" + NewValueStr
		else
			document.getElementById("FilterCode").value = "";			
		}				
	GenerateCode()
	}
	
function HI(iRef){
	iRef.style.backgroundColor = "red"
	iRef.style.color = "#FFFFFF"	
	}

function BYE(iRef){
	iRef.style.backgroundColor = ""
	iRef.style.color = ""	
	}

function MakeOr(RefreshDisplay){
	document.getElementById("OrButton").style.backgroundColor = "blue"
	document.getElementById("OrButton").style.color = "white"	
	document.getElementById("AndButton").style.backgroundColor = "silver"		
	document.getElementById("AndButton").style.color = "black"			
	document.getElementById("AndOr").value = "OR"
	if (RefreshDisplay) GenerateCode()
	}	

function MakeAnd(RefreshDisplay){
	document.getElementById("OrButton").style.backgroundColor = "silver"
	document.getElementById("OrButton").style.color = "black"		
	document.getElementById("AndButton").style.backgroundColor = "blue"
	document.getElementById("AndButton").style.color = "white"						
	document.getElementById("AndOr").value = "AND"
	if (RefreshDisplay) GenerateCode()
	}	

//END OF NUMBER, DATE AND TEXT FUNCTIONS
<% End If %>

//CLEAR FUNCTION FOR ALL TYPES
function CLEARITEM(){
	document.getElementById("FilterCode").value = ""
	GenerateCode()
	}
//END OF CLEAR FUNCTIONS

function CheckChildren(){
	intFilter = <%=FilterID%>
	matchedIndex = -1;
	for (i=0; i<ArrayItem.length; i++){
		if (intFilter == ArrayItem[i]){
			matchedIndex = i;
			break;
			}
		}
	
	if (matchedIndex != -1 && ArrayChildren[matchedIndex] != "") {

		var ItemsToClear = ArrayChildren[matchedIndex].split(",")
		var ItemsToClearConfirm = new Array(ItemsToClear.length)
		var ThereIsToClear = false
		var TotalToClear = 0
		
		ConfirmationColumns = ""
		for (j=0; j<ItemsToClear.length; j++){
			if (pDocument.getElementById("Filter" + ItemsToClear[j]).value != "") {
				ItemsToClearConfirm[j] = 1
				TotalToClear ++;
				for (k=0; k<ArrayItem.length; k++){
					if (ItemsToClear[j] == ArrayItem[k]){
						ConfirmationColumns += ", " + ArrayItemDisplay[k];
						break;
						}
					}
				ThereIsToClear = true
				}
			else
				ItemsToClearConfirm[j] = 0
			}

		if (ThereIsToClear) {
			if (TotalToClear > 1) 
				answer = confirm("The fields ' " + ConfirmationColumns.substring(1,ConfirmationColumns.length) + " ' are dependant upon this selection. Therefore filters applied to these columns will be cleared. \nDo you wish to continue?\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")
			else
				answer = confirm("The field ' " + ConfirmationColumns.substring(1,ConfirmationColumns.length) + " ' is dependant upon this selection. Therefore filters applied to this column will be cleared. \nDo you wish to continue?\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.")			
			
			if (!answer) return false
			for (z=0; z<ItemsToClear.length; z++){
				if (ItemsToClearConfirm[z] == 1) ClearFilter(ItemsToClear[z])	
				}
			}

		}
		
		return true;
	}

//GLOBAL FILTER TRANSFERRAL FUNCTIONS AND SYNCRONISATION
function ApplyFilter(){

	if (!CheckChildren()) return false;
		
	FilterLine = document.getElementById("FilterID").value
	fCode = document.getElementById("FilterCode").value

	if (fCode != "") {
		fText = document.getElementById("FilterText").value
		fSQL = document.getElementById("FilterSQL").value
		fAndOr = document.getElementById("AndOr").value
		SendData = FilterLine + "[*<brkr>*]" + fCode + "[*<brkr>*]" + fText + "[*<brkr>*]" + fSQL + "[*<brkr>*]" + fAndOr 
		pDocument.getElementById("Filter" + FilterLine).value = SendData;
		pDocument.getElementById("ImgI" + FilterLine).src = pDocument.getElementById("filter").src				
		//pDocument.getElementById("TD" + FilterLine).className = "Sel"
		pDocument.getElementById("TD" + FilterLine).title = fText		
		}
	else {		
		ClearFilter(FilterLine);
		}
	window.close()
	}

function ClearThisFilter(iflne){
	if (!CheckChildren()) return false;
	ClearFilter(iflne)
	window.close()
	}

function ClearFilter(iflne){
	pDocument.getElementById("Filter" + iflne).value = "";	
	//pDocument.getElementById("TD" + FilterLine).className = "NotSel"
	pDocument.getElementById("ImgI" + iflne).src = pDocument.getElementById("cross").src				
	pDocument.getElementById("TD" + iflne).title = ""			
	}
	
var pDocument

function SynchronizeData(){

	pDocument = dialogArguments
	FilterLine = document.getElementById("FilterID").value
	FilterData = pDocument.getElementById("Filter" + FilterLine).value

	if (FilterData != "") {
		tmpStr = FilterData.split("[*<brkr>*]")
		fCodes = tmpStr[1]
<% if (FilterType <> "ARRAY" AND FilterType <> "TABLE") then %>
		fAndOr = tmpStr[4]
		if (fAndOr == "AND")
			MakeAnd(false)
		else if (fAndOr == "OR")
			MakeOr(false)
<% end if %>
		document.getElementById("FilterCode").value = fCodes
		
		}
	try {
		GenerateCode()
		}
	catch (e) {
		document.getElementById("FilterCode").value = "";
		alert("Unable to parse previous filters.\nPlease add the filters again.");
<% if (FilterType = "ARRAY" OR FilterType = "TABLE") then %>
		document.getElementById("FOptions").value = "";
<% end if %>		
		}
	}

<% 
if (FilterItem_EOF = false) then
 %>
function LoaderFunction(){
SynchronizeData()
}
<% Else
 %>
function LoaderFunction(){temp = ""}
<% End if %>
//END OF GLOBAL FILTER TRANSFERRAL FUNCTIONS AND SYNCRONISATION	

function CallValidationFunction(){
	if (<%
	if (FilterItem_EOF = false) then
		if (FilterType = "POSTCODE") then
			Response.Write "isUKPostCode()"
		elseif (FilterType = "TEXT") then
			Response.Write "filterText()"
		elseif (FilterType = "NUMERIC") then
			Response.Write "isNumeric()"
		elseif (FilterType = "INTEGER") then
			Response.Write  "isInteger()"
		elseif (FilterType = "DATE") then
			Response.Write  "isDate()"		
		else
			Response.Write "true" 
		end if
	end if
	%>) return true
	else return false;
	}
-->	
</script>
<body onload="LoaderFunction();window.focus()">
    <center>
        <form name="LSCFORM" method="post" action="FilterLoader.asp">
        <table cellpadding="6" cellspacing="0" align="center">
            <tr>
                <td>
                    <table width="368" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td bgcolor="#E0E0E0" height="200px" align="center" valign="top">
                                <%
Call OpenDB()
if (FilterItem_EOF = false) then

' ***************************************************************************************************
'	 C ) DISPLAY THE FILTER NAMING ON PAGE
' ***************************************************************************************************


	'DIPLSAY FILTER COL NAME AND IDS
	Response.Write "<input type=""hidden"" name=""FilterName"" id=""FilterName"" value=""" & FilterName & """ />"	
	Response.Write "<input type=""hidden"" name=""FilterID"" id=""FilterID"" value=""" & FilterID & """ />"
	Response.Write "<input type=""hidden"" name=""FilterColumn"" id=""FilterColumn"" value=""" & FilterColumn & """ />"

' ***************************************************************************************************
'	 D ) SET UP A DEFAULT FILTER SELECTION DEPENDING ON WHAT HAS BEEN SELECTED TO FILTER ON
'
'			DEFAULT SELECTIONS INCLUDE - POSTCODE, TEXT, NUMERIC, INTEGER, DATE
'
'
' ***************************************************************************************************

	'APART FROM THE ARRAY AND TABLE TYPE ALL OTHERS NEED THE FOLLOWING SETUP.
		' 	Javascript_Function 	- This is the javascript validation function we will apply to the input
		' 	SelectList				- This is the possible sql operators that we can use to compare the data to data in the filter column
		'	MAXLENGTH				- This is the total amount of characters allows in the input box for camparison
		'	DefaultAndOr			- This is what the drop down will be set to as default 
	
	
	if (FilterType = "POSTCODE") then
		Javascript_Function = " onblur=""isUKPostCode()"""
		SelectList = "LIKE,NOT LIKE,=,<>"
		MAXLENGTH = 10
		DefaultAndOr = "OR"
	elseif (FilterType = "TEXT") then
		Javascript_Function = " onblur=""filterText()"""
		SelectList = "LIKE,NOT LIKE,=,<>"
		MAXLENGTH = 30
		DefaultAndOr = "OR"				
	elseif (FilterType = "NUMERIC") then
		Javascript_Function = " onblur=""isNumeric()"""
		SelectList = ">=,<=,=,<>,>,<"
		MAXLENGTH = 15
		DefaultAndOr = "AND"				
	elseif (FilterType = "INTEGER") then
		Javascript_Function = " onblur=""isInteger()"""
		SelectList = ">=,<=,=,<>,>,<"		
		MAXLENGTH = 10
		DefaultAndOr = "AND"				
	elseif (FilterType = "DATE") then
		Javascript_Function = " onblur=""isDate()"""
		SelectList = ">=,<=,=,<>,>,<"
		MAXLENGTH = 10
		DefaultAndOr = "AND"
			
	end if


' ***************************************************************************************************
'	 E ) DIPLSAY FILTER BOX ONTO PAGE -  
'
'			WE HAVE 2 OPTIONS, IF ITS A DEPENDANT OPTION THEN WE USE A TABLE DATA FILLED DROPDOWN
'			ELSE , WE USE THE DEFAULT OPTIONS HIGHLIGHTED IN PART D
' ***************************************************************************************************


	' If the filter type is not one of the default options for filter such as >=,<> then it will be a drop down that is 
	'	populated by table data.
	if (FilterType = "ARRAY") then
		
		ArrayText = FILTER_DATA_FOR_TABLE_OR_ARRAY 
		ArrayData = Split(ArrayText, ",")
		
		Response.Write "<table class="""" style=""border-collapse:collapse"">"
		Response.Write "<tr><td style=""background-color:steelblue""><b><font color=""blue"" style=""text-transform:uppercase""><u>" & FilterName & "</u></font></b></td></tr>"
		Response.Write "<tr><td><b><i>Filter Options</i></b></td></tr>"
		
		' begin building drop down od selected items
		Response.Write "<tr><td><select name=""FOptions"" id=""FOptions"" class=""textbox"" style=""width:270px"">"
		Response.Write "<option value="""">Please Select</option>"
		
		' populate the dropdown box with all the table data that is required using the array
		if ArrayText <> "" AND NOT isNull(ArrayText) then
			For i=0 to UBound(ArrayData)
				CurrentItem = ArrayData(i)
				if (isText = 1) then
					CurrentItem = "'" & CurrentItem & "'"
				end if
				Response.Write "<option value=""" & CurrentItem & """>" & ArrayData(i) & "</option>"
			Next
		end if
		
		Response.Write "</select></td></tr>"
		' end building drop down od selected items
	
		' write buttons to screen to use , delete or just clear the filter sql from the input box
		Response.Write "<tr><td><b><i>Actions</i></b></td></tr><tr><td><input type=""hidden"" name=""AndOr"" id=""AndOr"" value=""OR"" />"
		Response.Write "<input type=""button"" name=""BtnAddItem"" id=""BtnAddItem"" value="" ADD "" onclick=""ADDITEM()"" class=""rslBUTTON"" />&nbsp;"
		Response.Write "<input type=""button"" name=""BtnDeleteItem"" id=""BtnDeleteItem"" value="" DELETE "" onclick=""DELETEITEM()"" class=""rslBUTTON"" />&nbsp;"
		Response.Write "<input type=""button"" name=""BtnClearItem"" id=""BtnClearItem"" value="" CLEAR "" onclick=""CLEARITEM()"" class=""rslBUTTON"" /></td></tr>"
						
	elseif (FilterType = "TABLE") then

		TableData = FILTER_DATA_FOR_TABLE_OR_ARRAY
		TableArray = Split(TableData, "|")		
		
		COL_ID = TableArray(1)
		COL_NAME = TableArray(2)
		TABLE_NAME = TableArray(0)
		WHERE_CLAUSE = TableArray(3)
		ORDER_COLUMN = TableArray(4)
		
		'**** IAG MANAGER ONLY CODE
		'SPECIAL ORGANISATION CHECK - THIS WOULD NOT BE USED IN RSL AS THIS A HARD CODED OPTION
		if (FilterID = 170) then
			if (WHERE_CLAUSE = "") then
				WHERE_CLAUSE_PREFIX = " WHERE "
			else
				WHERE_CLAUSE_PREFIX = " AND "			
			end if			

			'GET THE CURRENT PROJECT AND DISPLAY ORGANISATIONS ON THIS BASIS
			Project = Request("Project")
			CodeResult = -1
			SQL = "SELECT DISPLAYCODE FROM CAMPAIGN WHERE ENQID IN (" & Project	& ")"
			Call OpenRs(rsProject, SQL)
			if NOT rsProject.EOF then
				CodeResult = rsProject("DisplayCode")
			end if
			Call CloseRs(rsProject)
			
			WHERE_CLAUSE_ORG = "" 			
			IF NOT (isNull(CodeResult) OR CodeResult = CStr(-1)) THEN
				WHERE_CLAUSE_ORG = WHERE_CLAUSE_PREFIX & " ORGID IN (" & Replace(CodeResult,"a","") & ") " 
			END IF
			'END OF GET ORGANISATION/PROJECT LIST
			
			WHERE_CLAUSE = WHERE_CLAUSE & WHERE_CLAUSE_ORG
		end if


		' THE SELECTED COLUMN MAY BE DEPENDANT UPON ANOTHER COLUMN BEING SELECTED
		' THIS CODE GETS THE SELECTED SQL OF ITS DEPENDANT IE (COMPANY IN (3))
		'	IN IAG LOCATION AND ADVISOR ARE DEPENDANT UPON ORGANISATION SO
		'	THE SQL GENERATED HERE WOULD LOOK SOMEHTIN LIKE (WHERE LOC.ORGID IN (64))


		if (NOT isNull(parent) AND NOT isNull(parentkey) ) then
			parentKeys = Request("pt")
			if (parentKeys = "") then 
				parentKeys = "-999"
				
				SQL = "SELECT ISNULL(COLUMN_CODE + ' ','') + ':' + COLUMN_DISPLAY_NAME AS PARENTNAME FROM NS_REPORT_COLUMN WHERE COLUMNID = " & PARENT
				Call OpenRs(rsParent, SQL)
				ParentName = rsParent("PARENTNAME")
				Call CloseRs(rsParent)				
				
				ParentErrorMessage = "<font color='red'><b>Note : <br/><br/>This filter is dependant upon another filter being selected ('" & ParentName & "') </b></font>"
				
			end if
			
			

			WHERE_CLAUSE_PARENT = parentkey & " IN (" & parentKeys & ")"
						
			if (WHERE_CLAUSE = "") then
				WHERE_CLAUSE_PREFIX = " WHERE "
			else
				WHERE_CLAUSE_PREFIX = " AND "			
			end if				
			WHERE_CLAUSE_PARENT = WHERE_CLAUSE_PREFIX & WHERE_CLAUSE_PARENT

			' THE WHERE CLAUSE NOW BECOMES THE OLD ONE PLUS ANYTHING ADDED ON IN THIS PEICE OF CODE
			WHERE_CLAUSE = WHERE_CLAUSE & WHERE_CLAUSE_PARENT
			
		end if	
	
		SQL = "SELECT " & COL_ID & ", " & COL_NAME & " FROM " & TABLE_NAME & " " & WHERE_CLAUSE & " ORDER BY " & ORDER_COLUMN
	
		'Response.END
		
		Call OpenRs(rsList, SQL)
		Response.Write "<table class="""" style=""border-collapse:collapse"">"
		Response.Write "<tr><td style=""background-color:steelblue;height:20px;text-transform:uppercase;color:white""><b>&nbsp;" & FilterName & "</b></td></tr>"
		Response.Write "<tr><td><b><i>Filter Options</i></b></td></tr>"
		Response.Write "<tr><td><select name=""FOptions"" id=""FOptions"" class=""textbox"" style=""width:270px"">"
		Response.Write "<option value="""">Please Select</option>"
		
		' BUILD OPTION LIST FROM PREDEFINED TABLE IN DB AS INSTRUCTED IN THE COLUMN TABLE
		while NOT rsList.EOF
			CurrentItem = rsList(0) 
			ItemName = rsList(1)
			if (isText = 1) then
				CurrentItem = "'" & CurrentItem & "'"
			end if
			Response.Write "<option value=""" & CurrentItem & """>" & ItemName & "</option>"
			rsList.moveNext
		wend
		
		Call CloseRs(rsList)
		Response.Write "</select></td></tr>"
		Response.Write "<tr><td><b><i>Actions</i></b></td></tr><tr><td>"
		Response.Write "<input type=""button"" name=""BtnAddItem"" id=""BtnAddItem"" value="" ADD "" onclick=""ADDITEM()"" class=""rslBUTTON"" style=""cursor:pointer""><input type=""hidden"" name=""AndOr"" id=""AndOr"" value=""OR"" />&nbsp;"
		Response.Write "<input type=""button"" name=""BtnDeleteItem"" id=""BtnDeleteItem"" value="" DELETE "" onclick=""DELETEITEM()"" class=""rslBUTTON"" style=""cursor:pointer"" />&nbsp;"
		Response.Write "<input type=""button"" name=""BtnClearItem"" id=""BtnClearItem"" value="" CLEAR "" onclick=""CLEARITEM()"" class=""rslBUTTON"" style=""cursor:pointer"" /></td></tr>"				
	else
		
		'first build the options list from the variable declared previously IN PART D)
		OptionsArray = Split(SelectList, ",")
		OptionsData = ""
		for i=0 to UBound(OptionsArray)
			OptionsData = OptionsData & "<option value=""" & OptionsArray(i) & """>" & OptionsArray(i) & "</option>"
		next
		Response.Write "<table class="""" style=""border-collapse:collapse"">"
		Response.Write "<tr><td style=""background-color:steelblue;height:20px;text-transform:uppercase;color:white""><b>&nbsp;" & FilterName & "</b></td></tr>"
		Response.Write "<tr><td><b><i>Filter Options</i></b></td></tr>"
		Response.Write "<tr><td><select class=""textbox"" name=""Sign"" id=""Sign"" style=""width:100px"">" & OptionsData & "</select>"
		Response.Write "&nbsp;<input type=""text"" size=""27"" class=""textbox"" name=""TheValue"" id=""TheValue"" value="""" maxlength=""" & MAXLENGTH & """ " & Javascript_Function & " /></td></tr>"
		Response.Write "<tr><td><b><i>Actions</i></b></td></tr><tr><td>"
		Response.Write "<input type=""hidden"" name=""isText"" id=""isText"" value=""" & isText & """><input type=""hidden"" name=""AndOr"" id=""AndOr"" value=""" & DefaultAndOr & """ /><input type=""button"" value="" ADD "" onclick=""ADDTEXTITEM()"" class=""rslBUTTON"" style=""cursor:pointer"" />&nbsp;"
		Response.Write "<input type=""button"" value="" CLEAR "" onclick=""CLEARITEM()"" class=""rslBUTTON"" style=""cursor:pointer"" />&nbsp;"
		if (DefaultAndOr = "AND") then
			Response.Write "<input type=""button"" value="" OR "" id=""OrButton"" name=""OrButton"" onclick=""MakeOr(true)"" class=""rslBUTTON"" style=""background-color:silver;color:black; cursor:pointer"" />&nbsp;"						
			Response.Write "<input type=""button"" value="" AND "" id=""AndButton"" name=""AndButton"" onclick=""MakeAnd(true)"" class=""rslBUTTON"" style=""background-color:blue;color:white; cursor:pointer"" /></td></tr>"						
		else
			Response.Write "<input type=""button"" value="" OR "" id=""OrButton"" name=""OrButton"" onclick=""MakeOr(true)"" class=""rslBUTTON"" style=""background-color:blue;color:white; cursor:pointer"" />&nbsp;"						
			Response.Write "<input type=""button"" value="" AND "" id=""AndButton"" name=""AndButton"" onclick=""MakeAnd(true)"" class=""rslBUTTON"" style=""background-color:silver;color:black; cursor:pointer"" /></td></tr>"						
		end if		
		Response.Write "<tr><td><div id=""ListItems""></div></td></tr>"
	end if

end if

Call CloseDB()
                                %>
                                <tr>
                                    <td>
                                        <% if (FilterType = "ARRAY" OR FilterType = "TABLE") then 

' ***************************************************************************************************
'	 F ) SHOW FINAL BOXES AND BUTTONS 
'
' ***************************************************************************************************

                                        %>
                                        <b><i>Filter Text</i></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea name="FilterText" id="FilterText" cols="50" rows="7" class="textbox" style="border: 1p solid black"></textarea>
                                        <% Else
                                        %>
                                        <textarea name="FilterText" id="FilterText" cols="50" rows="5" class="textbox" style="display: none"></textarea>
                                        <% End if %>
                                        <textarea name="FilterCode" id="FilterCode" cols="50" rows="10" class="textbox" style="display: none"></textarea>
                                        <textarea name="FilterSQL" id="FilterSQL" cols="50" rows="10" class="textbox" style="display: none"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" name="BtnApplyFilter" id="BtnApplyFilter" value=" Apply Filter " onclick="ApplyFilter()" class="rslBUTTON" style="cursor:pointer" />
                                        <input type="button" name="BtnClearFilter" id="BtnClearFilter" value=" Clear Filter " onclick="ClearThisFilter(<%=FilterID%>)" class="rslBUTTON" style="cursor:pointer" />
                                    </td>
                                </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#e0e0e0">
                    <img src="/images/dispacer.gif" width="1" height="7" alt="" />
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px solid #003366">
                </td>
            </tr>
            <tr>
                <td height="24">
                    &nbsp;<%=ParentErrorMessage%>
                </td>
            </tr>
        </table>
        </td></tr></table></form>
    </center>
</body>
</html>
