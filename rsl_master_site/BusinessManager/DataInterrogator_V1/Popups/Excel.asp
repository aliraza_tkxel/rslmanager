<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <script language="JavaScript" src="barloader.js"></script>
    <title>Interrogator Export</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
</head>
<body onload="window.focus();" bgcolor="#bdbdbd" style="margin-left: 13px">
    <table border="0" cellspacing="1" cellpadding="4" width="100%" height="95%" bgcolor="#bdbdbd">
        <tr>
            <td height="95%" colspan="2" valign="top">
                <table style="border: 1px solid red" cellspacing="5" cellpadding="0" width="100%"
                    height="100%" bgcolor="#ffffff">
                    <tr>
                        <td valign="top">
                            <div id="titlebar" align="center">
                                <b>Excel file is being generated:</b></div>
                            <br />
                            <div id="parentbar" style="width: 300px; font-size: 8pt; padding: 2px; border: solid black 1px">
                                <div id="progressbar" style="width: 294px; overflow: hidden; color: #ffffff; background-color: #003366">
                                </div>
                            </div>
                            <script type="text/javascript" language="javascript">
                                progress_start()
                            </script>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <iframe src="ExcelReporter.asp?Rid=<%=Request("Rid")%>" width="1px" height="1px">
    </iframe>
</body>
</html>
