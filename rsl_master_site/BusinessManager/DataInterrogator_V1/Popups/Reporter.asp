<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 15

	sort_col = Request("data_grid_sort_col")
	sort_dir = Request("data_grid_sort_dir")

	Set objXML = Server.CreateObject("MSXML2.DOMDocument")

	data_grid_index = Request.Querystring("data_grid_index")
	if (data_grid_index <> "" AND isNumeric(data_grid_index)) then
		data_grid_index = CLng(data_grid_index)
	else
		data_grid_index = ""
	end if

	ReportId = Request.Querystring("Rid")
	if (NOT isNumeric(ReportId)) then
		ReportId = -1
	end if

	isReport = false

	Call OpenDB()
	SQL = "SELECT RS.SAVENAME, RS.DESCRIPTION, RSQ.REPORTQUERY FROM NS_REPORT_SAVE_QUERY RSQ " &_
			"INNER JOIN NS_REPORT_SAVE RS ON RS.SAVEREPORTID = RSQ.SAVEREPORTID " &_
			"WHERE RS.ORGID = " & "-1" & " AND RSQ.SAVEREPORTID = " & ReportId

	SQL = "SELECT RS.SAVENAME, RS.DESCRIPTION, RSQ.REPORTQUERY FROM NS_REPORT_SAVE_QUERY RSQ " &_
			"INNER JOIN NS_REPORT_SAVE RS ON RS.SAVEREPORTID = RSQ.SAVEREPORTID " &_
			"WHERE RSQ.SAVEREPORTID = " & ReportId


	Call OpenRs(rsQuery,SQL)

	if NOT rsQuery.EOF then
		isReport = true
		SQL = rsQuery("REPORTQUERY")
		ReportName = rsQuery("SAVENAME")
		ReportDesc = rsQuery("DESCRIPTION")
		if (NOT isNULL(ReportDesc) AND ReportDesc <> "") then
			ReportDesc = ReportDesc & "<BR>"
		end if
	end if

	Call CloseRs(rsQuery)

	if (sort_col <> "") then
		SQL = SQL & " ORDER BY " & sort_col & " " & sort_dir
		sortCol = "sortCol: """ & sort_col & ""","
		sortDir = "sortDir: """ & sort_dir & ""","
	end if
%>
<%
	'get the display column data and titles and sort columns, plus the widths
	DisplaySQL = "SELECT RS.SAVECOLUMNS, RT.GROUPED FROM NS_REPORT_SAVE RS INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID WHERE RS.SAVEREPORTID = " & ReportId

	Call OpenDB()
	Call OpenRs(rsSC, DisplaySQL)

	if NOT rsSC.EOF then
		SelectedList = rsSC("SAVECOLUMNS")
		isGrouped = rsSC("GROUPED")
	else
		SelectedList = -1
		isGrouped = -1
	end if
	Call CloseRs(rsSC)

	'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
	SelectArray = Split(SelectedList, ",")
	TotalColumns = Ubound(SelectArray)
	if (isGrouped = 1) then TotalColumns = TotalColumns + 1

	Redim DisplayWidths(TotalColumns)
	Redim DisplayTitles(TotalColumns)
	Redim SortColumns(TotalColumns)

	SelectSQL = ""
	for i=0 to Ubound(SelectArray)
		if (i > 0) then SelectSQL = SelectSQL & " UNION ALL "
		SelectSQL = SelectSQL & "SELECT SQL_SELECT_COLUMN, DISPLAY_WIDTH, '<font color=blue>' + ISNULL(COLUMN_CODE,'') + '</font> ' + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
	next

	column_counter = 0
	CounterWidth = 40
	TableWidth = 30
	Call OpenRs(rsC,SelectSQL)
	while Not rsC.EOF
		DW = rsC("DISPLAY_WIDTH")
		TableWidth = TableWidth + DW
		DisplayWidths(column_counter) = DW
		DisplayTitles(column_counter) = rsC("COLUMN_TITLE")
'		SortColumns(column_counter) = "[""" & rsC("SQL_SELECT_COLUMN") & """, true]"
		SortColumns(column_counter) = "[""(" & (column_counter+1) & ")"", true]"
		column_counter = column_counter + 1
		rsC.moveNext
	wend
	Call CloseRs(rsC)
	Call closeDB()

	'ADD THE GROUP PART IF NECCESSARY
	if (isGrouped = 1) then
		TableWidth = TableWidth + 100
		DisplayWidths(column_counter) = 100
		DisplayTitles(column_counter) = "COUNT"
		SortColumns(column_counter) = "[""GROUPCOUNT"", true]"
	end if

	SortColumnData = Join(SortColumns, ",")
'	Response.Write SortcolumnData
%>
<%
	'beginning of recordset setup for the actual query
	set rsSet = Server.CreateObject("ADODB.Recordset")
	rsSet.ActiveConnection = RSL_CONNECTION_STRING 
	'RW SQL	
	'Response.End()
	rsSet.Source = SQL
	rsSet.CursorType = 2
	rsSet.LockType = 1
	rsSet.CursorLocation = 3
	rsSet.Open()

	rsSet.PageSize = CONST_PAGESIZE
	my_page_size = CONST_PAGESIZE
	' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
	rsSet.CacheSize = rsSet.PageSize
	intPageCount = rsSet.PageCount
	intRecordCount = rsSet.RecordCount

	' Sort pages
	If intpage = 0 Then intpage = 1 End If
	' Just in case we have a bad request
	If intpage > intPageCount Then intpage = intPageCount End If
	If intpage < 1 Then intpage = 1 End If

	nextPage = intpage + 1
	If nextPage > intPageCount Then nextPage = intPageCount	End If
	prevPage = intpage - 1
	If prevPage <= 0 Then prevPage = 1 End If

	' double check to make sure that you are not before the start
	' or beyond end of the recordset.  If you are beyond the end, set
	' the current page equal to the last page of the recordset.  If you are
	' before the start, set the current page equal to the start of the recordset.

	If CLNG(intPage) > CLNG(intPageCount) Then intPage = intPageCount End If
	If CLNG(intPage) <= 0 Then intPage = 1 End If

	' Make sure that the recordset is not empty.  If it is not, then set the
	' AbsolutePage property and populate the intStart and the intFinish variables.
	If intRecordCount > 0 Then
		rsSet.AbsolutePosition = 1
		rsSet.AbsolutePage = intPage
		intStart = rsSet.AbsolutePosition
		If CLng(intPage) = CLng(intPageCount) Then
			intFinish = intRecordCount
		Else
			intFinish = intStart + (rsSet.PageSize - 1)
		End if
	End If

	count = 0

Function ReplaceBR(str)
    If str <> "" Then
        ReplaceBR = Replace(str,Chr(13)," ")
        ReplaceBR = Replace(ReplaceBR,Chr(10)," ")
        ReplaceBR = Replace(ReplaceBR,Chr(9)," ")
    Else
        ReplaceBR = ""
    End If
End Function
%>
<html>
<head>
    <title>Interrogator Report</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <style>
        .fixedTable
        {
            table-layout: fixed;
        }
        TD.cell
        {
            border-right: #b8b8b8 1px solid;
            padding-right: 0px;
            padding-left: 3px;
            font-size: 9px;
            padding-bottom: 1px;
            margin: 0px;
            overflow: hidden;
            line-height: 10px;
            padding-top: 1px;
            border-bottom: #b8b8b8 1px solid;
            font-family: arial, helvetica, sans-serif;
            height: 22px;
        }
        .first
        {
            border-left: #b8b8b8 1px solid;
        }
        .tableCellHeader
        {
            border-right: #b8b8b8 1px solid;
            padding-right: 0px;
            border-top: #b8b8b8 1px solid;
            padding-left: 3px;
            font-size: 9px;
            padding-bottom: 1px;
            padding-top: 1px;
            background-color: #cedebd;
            text-align: left;
            line-height: 10px;
            height: 22px;
        }
        #scrollerTip
        {
            border-top: 1px solid blue;
            border-bottom: 1px solid blue;
            background-color: beige;
        }
    </style>
    <script type="text/javascript" language="javascript" src="/js/prototype.js"></script>
    <script type="text/javascript" language="javascript" src="/js/rico.js"></script>
    <script type="text/javascript" language="javascript">
    function __debug(str) {
        new Insertion.Bottom( $('debug'), str + "</br>" );
        $('debug').scrollTop = 99999;
	    }

	var onloads = new Array();
	function bodyOnLoad() {
	for ( var i = 0 ; i < onloads.length ; i++ )
		onloads[i]();
		}
	onloads.push( registerAjaxStuff );

	theGrid = null;

	//Registers all the ajax engine items
	function registerAjaxStuff () { 
		//set the intial options
		<% if data_grid_index = "" then %>
		var opts = { 
   			prefetchBuffer: false, 
			onscroll: updateHeader,
			requestParameters : ['rid=<%=ReportId%>'],
			<%=sortCol%>
			<%=sortDir%>
			columns : [["ignore", false],<%=SortColumnData%>]                
			}
		<% else %>
		var opts = { 
   			prefetchBuffer: true, 
			onscroll: updateHeader,
			offset : <%=data_grid_index%>,
			requestParameters : ['rid=<%=ReportId%>'],
			<%=sortCol%>
			<%=sortDir%>
			columns : [["ignore", false],<%=SortColumnData%>]                
			}
		<% end if %>
	   //set the rico live grid
	   
	   theGrid = new Rico.LiveGrid( 
			'data_grid', 
			<%=CONST_PAGESIZE%>, 
			<%=intRecordCount%>, 
			'/BusinessManager/DataInterrogator_v1/Serverside/ReporterXML.asp', opts 
			); 
	   //register the search rows item
	   ajaxEngine.registerAjaxObject( 'configureSearchRows',   new SearchRowsConfigurator(theGrid) );				
	  
	   } 

	//Onscroll this updates the text for the row number and total rows
	function updateHeader( liveGrid, offset ) {
      	$('bookmark').innerHTML = "Records " + (offset+1) + " - " + (offset+liveGrid.metaData.getPageSize()) + " of " + liveGrid.metaData.getTotalRows();
      	var sortInfo = "";
	   
	    if (liveGrid.sortCol) {
        	sortInfo = "&data_grid_sort_col=" + liveGrid.sortCol + "&data_grid_sort_dir=" + liveGrid.sortDir;
      		}
		$('bookmark').href="/BusinessManager/DataInterrogator_v1/Popups/Reporter.asp" + "?data_grid_index=" + offset + "&Rid=<%=ReportId%>" + sortInfo;
		updateScrollerTip( liveGrid, offset );
		
		}	

	//This registers the search update from the responseXML
	SearchRowsConfigurator = Class.create();
	SearchRowsConfigurator.prototype = {
	
	   initialize: function(liveGrid) {
		 this.liveGrid = liveGrid;
	   },
	
	   ajaxUpdate: function(ajaxResponse) {
	   
		  var cell = ajaxResponse.getElementsByTagName("numResults")[0];
		  var numResults = cell.text != undefined ? cell.text : cell.textContent;
		  if ( this.liveGrid.metaData.getTotalRows() != numResults ) {
			 this.liveGrid.metaData.setTotalRows(numResults);
			 this.liveGrid.scroller.updateSize();
		  }
	   }
	}; 


   function updateScrollerTip( liveGrid, offset ) {

      var metaData = liveGrid.metaData;
      var grid     = liveGrid.table;
   
      var scrollerTip = $('scrollerTip');
      var range =  (offset+1);
      var range2 = (offset + metaData.getPageSize());
      scrollerTip.innerHTML = '<span class="scrollerTipSpan">' +
                                     range +
                                     //+ range + '&#45;' + range2 +   //IE6 went wacky on this??
                                    '</span>';
      // offset + (pagesize/2)
//	  alert("offset " + offset + "\nmetaData.getPageSize() " + metaData.getPageSize() + "\nmetaData.getTotalRows() " + metaData.getTotalRows() + "\ngrid.offsetHeight " + grid.offsetHeight + "\nmetaData.scrollArrowHeight " + metaData.scrollArrowHeight + "\nscrollerTip.offsetHeight " + scrollerTip.offsetHeight + "\nscrollerTip.offsetHeight " + scrollerTip.offsetHeight + "\nmetaData.scrollArrowHeight " + metaData.scrollArrowHeight)
      var topX = scrollerTip.offsetHeight + metaData.scrollArrowHeight-54;
	  				//(offset + metaData.getPageSize())/ metaData.getTotalRows()+//*
                     //(grid.offsetHeight - (metaData.scrollArrowHeight*2) - scrollerTip.offsetHeight/2) +
/* original
      var topX = (offset + metaData.getPageSize())/ metaData.getTotalRows()*
                     (grid.offsetHeight - (metaData.scrollArrowHeight*2) - scrollerTip.offsetHeight/2) +
					 scrollerTip.offsetHeight + metaData.scrollArrowHeight-18;
*/
      var scrollerWidth = 18;
      var padding = 4; 
      scrollerTip.style.top = topX + 'px';
      scrollerTip.style.left = grid.offsetLeft + grid.offsetWidth + scrollerWidth + padding - 20 + 'px';
//      scrollerTip.style.left = grid.offsetLeft + grid.offsetWidth + scrollerWidth + padding + 'px'; //original
      scrollerTip.style.visibility = 'visible';
   }	  						 	
    </script>
</head>
<body onload="javascript:bodyOnLoad();window.focus()" style="background-color: #FFFFFF">
    <font style='font-family: tahoma; font-size: 14px; border-bottom: 1px dotted block;
        font-weight: bold; text-transform: capitalize'>
        <%=ReportName%></font><br>
    <%=ReportDesc%><br>
    <a id="bookmark" style="font-size: 12px; margin-bottom: 3px">Records 1 -
        <%if inRecordCount < CONST_PAGESIZE then Response.Write intRecordCount else Response.Write CONST_PAGESIZE end if%>
        of
        <%=intRecordCount%></a>
    <table id="data_grid_header" class="fixedTable" cellspacing="0" cellpadding="0" style="width: <%=TableWidth%>px">
        <tr>
            <%	
	ColumnCount = rsSet.fields.count-1
	'loop through the tds and build a single row
	Response.Write "<th class=""first tableCellHeader"" style=""width:" & CounterWidth & "px"">#</th>"
	for j=0 to Columncount
		if j = 0 then
			Response.Write "<th class=""tableCellHeader"" style=""width:" & DisplayWidths(j) & "px"">" & DisplayTitles(j) & "</th>"
		else
			Response.Write "<th class=""tableCellHeader"" style=""width:" & DisplayWidths(j) & "px"">" & DisplayTitles(j) & "</th>"
		end if		
		
	next
	
            %>
        </tr>
    </table>
    <!--the Next div was addedby meto hold the scroller in place -->
    <div id="DivHolder" style='width: <%=TableWidth+40%>px; position: relative'>
        <div id="scrollerTip" style="visibility: hidden; position: absolute;">
        </div>
        <div id="viewPort" style="float: left; border-bottom: 1px solid silver;">
            <table id="data_grid" class="fixedTable" cellspacing="0" cellpadding="0" style="float: left;
                width: <%=TableWidth%>px; border-left: 1px solid #ababab">
                <tbody>
	<%
	If intRecordCount > 0 Then
	' Display the record that you are starting on and the record
	' that you are finishing on for this page by writing out the
	' values in the intStart and the intFinish variables.

	%>
	<%
	For intRecord = 1 to rsSet.PageSize

		Response.Write "<tr>" 
		Response.Write "<td class=""cell"" style=""width:" & CounterWidth & "px"">" & intRecord & "</td>"
		'loop through the tds and build a single row
		for j=0 to Columncount
			Response.Write "<td class=""cell"" style=""width:" & DisplayWidths(j) & "px"">" & ReplaceBR(rsSet(j)) & "&nbsp;</td>"
		next
		Response.Write "</tr>"

		count = count + 1
		rsSet.movenext()
		If rsSet.EOF Then Exit for

	Next

	'write some empty lines if the recordset is smaller than the page size
	if count < CONST_PAGESIZE then
		For i=count to CONST_PAGESIZE
			Response.Write "<tr>" 
			Response.Write "<td class=""cell"" style=""width:" & CounterWidth & "px"">&nbsp;</td>"
			'loop through the tds and build a single row
			for j=0 to Columncount
				Response.Write "<td class=""cell"" style=""width:" & DisplayWidths(j) & "px"">&nbsp;</td>"
			next
			Response.Write "</tr>" 
		next
	end if

	'finally insert dummy row
	Response.Write "<tr>"
	Response.Write "<td class=""cell"" style=""width=" & CounterWidth & "px""></td>"
	'loop through the tds and build a single row
	for j=0 to Columncount
		Response.Write "<td class=""cell"" style=""width:" & DisplayWidths(j) & "px""></td>"
	next
	Response.Write "</tr>" 

	Else

		'if no records then insert blank filled rows
		if count < CONST_PAGESIZE then
			For i=count to CONST_PAGESIZE
				Response.Write "<tr>"
				Response.Write "<td nowrap=""nowrap"" class=""cell"" style=""width:" & CounterWidth & "px"">&nbsp;</td>"
				'loop through the tds and build a single row
				for j=0 to Columncount
					Response.Write "<td nowrap=""nowrap"" class=""cell"" style=""width:" & DisplayWidths(j) & "px"">&nbsp;</td>"
				next
				Response.Write "</tr>"
			next
		end if

	End if

	rsSet.close()
	Set rsSet = Nothing
                    %>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
