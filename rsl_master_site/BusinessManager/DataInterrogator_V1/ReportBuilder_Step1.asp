<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - MULTIPLE PROJECT CHECK 
'									B - VARIABLE COLLECTION
'									C - DISPLAY OPTIONS ON SCREEN
'									D - VARIABLE STORAGE
'
' ***************************************************************************************************

					
Dim EmptyRecords , ErrorMessage

ProjectID = -1
CurrentMainMenu = 7
CurrentSubMenu = 4
HeaderPic = "nextstep"

Call OpenDB()
			SQL = "SELECT reportquery FROM NS_REPORT_SAVE_query WHERE ROWID = 62 " 
					Call OpenRs(rs1, SQL)
					if NOT rs1.EOF then
						'rw rs1("reportquery")
					end if
					Call CloseRs(rs1)
' ***************************************************************************************************
'	
'	PART A ) 	WE CHECK TO SEE IF THERE ARE ANY DUPLICATE PROJECTS IN THE PROJECT LIST. IF THERE
'				ARE NONE THEN WE CAN CARRY ON AS NORMAL. IF THERE ARE SOME THEN WE SHOW AN ERROR ON
'				PAGE AND HALT THE USER FROM CARRYING ON.
'
' ***************************************************************************************************

SQL = "SELECT GROUPPROJECTS FROM NS_PROJECT WHERE ACTIVE = 1"
Call OpenRs(rsT, SQL)
if not rsT.eof Then

	Data = rsT.getString(,,",",",","-1")
	
	DuplicateArray = Split(Data, ",")
	
	Set sd = Createobject("Scripting.Dictionary")
	FoundDuplicates = false
	
	for i=0 to Ubound(DuplicateArray)
		if sd.Exists(DuplicateArray(i)) then
			FoundDuplicates = true
			exit for
		else
			sd.add DuplicateArray(i), true
		end if
	next
	
else

	EmptyRecords = TRUE
	ErrorMessage = "There are currently no Modules to select from. <BR><b>Quote Reference DI-01-01</b>"

End If
Call CloseRs(rsT)
'end of PART A


' ***************************************************************************************************
'	
'	PART B ) 	WHAT WE DO IS COLLECT ALL THE MAIN INFORMATION THAT A REPORT CAN HOLD TO BRING BACK DATA
'				RELATED TO THAT REPORT. WE SAVE IT IN HIDDEN FIELDS AND THEN PASS THE INFORMATION FROM
'				PAGE TO PAGE.

'				NOTE : 	IF WE CREATE A NEW REPORT THEN WE CAN ONLY PASS THE VARIABLES THAT WE HAVE COLLECTED
'					  	UP TO THAT POINT. bUT IF THE BACK BUTTON IS PRESSED THEN WE STILL HAVE THE DATA STORED
'						BE IT IN A TEMP FASHION AS IT DOES NOT HIT THE DATABASE UNTIL THE END.
'
' ***************************************************************************************************

If (EmptyRecords = TRUE) then


else

		if (FoundDuplicates = false) then
		
			SaveReportID = Request.Form("hid_SaveReportID")
			ReportName = "Creating a new report..."
		
		
		
			'-----------------------------------------------------------------------------------------
			' NEW REPORT OR BACK BUTTON PRESSED - IF WE ARE CREATING A NEW REPORT THEN WE START HERE
			'-----------------------------------------------------------------------------------------
			
			'if the back button has been pressed then load from request object
			if (Request.Form("CLICKEDPREVIOUS") = "1" OR SaveReportID = "") then
				
				' THESE ARE ALL THE VARIABLES THAT WE PASS FROM PAGE TO PAGE IN OUR HIDDEN FIELDS.	
					
				' GROUP ID = THE FIRST STAGE SELECTION (IN IAG IT COULD BE ESF OR IN RSL IT COULD BE EMPLOYEE)
				' PROJECT ID = THIS IS THE PROJECT THAT RELATES TO OUT OF THE SCOPE OF DATA INTER. SO IF WE HAVE A PROJECTS TABLE IT WILL BE THE ID IN THERE)
				' REPORT ID - THIS IS THE ID OF THE SAVED COLLECTION OF COLUMNS AND FILTERS , IT IS THE MAIN KEY TO COMBINE EVERYTHING
				' REPORT NAME - THIS IS THE NAME OF THE REPORT
				' REPORT TYPE -  THIS TELLS US IF IT IS TO BE SHOWN AS ANY OF THE FOLLOWING (Grouped List,Pie Chart,Bar Chart,Line Chart)
				' SELECTED LIST - THIS IS A LIST OF ALL THE COLUMNS THAT HAVE BEEN SELECTED IN THE REPORT
				' LINE DATE ID - THIS TELLS US WHAT DATE COLUMN WE USE TO IDENTIFY THE MAIN DATE OF THE QUERY -  THIS WILL THEN BE USED WITH THE LINE CHART OPTION FOR DISPLAY
		
				GroupID = Request.Form("hid_Group")
				ProjectID = Request.Form("hid_Project")
				ReportID = Request.Form("hid_Report")
				ReportType = Request.Form("hid_ReportType")
				SelectedList = Request.Form("hid_SelectedList")
				FilterList = Request.Form("hid_FilterList")
				SaveReportID = Request.Form("hid_SaveReportID")
				LineDateID =  Request.Form("hid_LineDate")
				SaveTitle = Request.Form("hid_SaveTitle")
				SaveDescription =  Request.Form("hid_SaveDescription")
				ColumnCount  = Request.Form("hid_ColumnCount")
				' IF WE ARE WORKING ON A SAVED REPORT BUT HAVE ARRIVED AT THIS PAGE BECAUSE SOMEONE PRESSED THE BACK BUTTON
				' THEN WE STILL NEED TO SHOW THAT IT IS A SAVED PROJECT BY USING THE PROJECT NAME.
				if (SaveReportID <> "") then
					SQL = "SELECT SAVENAME FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
					Call OpenRs(rsR, SQL)
					if NOT rsR.EOF then
						ReportName = "Amending Report - " & rsR("SAVENAME")
					end if
					Call CloseRs(rsR)
					
				end if
				
			else
			
			'---------------------------------------------------------------------------------------------------
			' AMEND SAVED REPORT FIRST TIME VISIT TO PAGE - IF WE ARE AMENDING A SAVED REPORT THEN WE START HERE
			'---------------------------------------------------------------------------------------------------
				
				' -- WE GET ALL THE COLUMNS AND RELEVANT LINKS FROM THE MAIN TABLE TO USE LATER
				SQL = "SELECT GROUPID, PROJECTID, REPORTID, SAVENAME, TYPEID, LINEDATE, SAVECOLUMNS FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		
				Call OpenRs(rsR, SQL)
				if NOT rsR.EOF then
				
					' 	THESE ARE ALL THE VARIABLES THAT WE PASS FROM PAGE TO PAGE IN OUR HIDDEN FIELDS. 
					'	HERE THOUGH WE BUT THEM INTO VARS FROM THE DB AND THEN AT THE BOTTOM WE FILL THE HIDDEN VALUES.
					
					' GROUP ID = THE FIRST STAGE SELECTION (IN IAG IT COULD BE ESF OR IN RSL IT COULD BE EMPLOYEE)
					' PROJECT ID = THIS IS THE PROJECT THAT RELATES TO OUT OF THE SCOPE OF DATA INTER. SO IF WE HAVE A PROJECTS TABLE IT WILL BE THE ID IN THERE)
					' REPORT ID - THIS IS THE ID OF THE SAVED COLLECTION OF COLUMNS AND FILTERS , IT IS THE MAIN KEY TO COMBINE EVERYTHING
					' REPORT NAME - THIS IS THE NAME OF THE REPORT
					' REPORT TYPE -  THIS TELLS US IF IT IS TO BE SHOWN AS ANY OF THE FOLLOWING (Grouped List,Pie Chart,Bar Chart,Line Chart)
					' SELECTED LIST - THIS IS A LIST OF ALL THE COLUMNS THAT HAVE BEEN SELECTED IN THE REPORT
					' LINE DATE ID - THIS TELLS US WHAT DATE COLUMN WE USE TO IDENTIFY THE MAIN DATE OF THE QUERY -  THIS WILL THEN BE USED WITH THE LINE CHART OPTION FOR DISPLAY
					
					GroupID = rsR("GROUPID") 
					ProjectID = rsR("PROJECTID")
					ReportID = rsR("REPORTID")
					ReportName = "Amending Report - " & rsR("SAVENAME")
					ReportType = rsR("TYPEID")
					SelectedList = rsR("SAVECOLUMNS")
					LineDateID = rsR("LINEDATE")
					
					
					SQL = "SELECT FILTERID, FILTER_SQL, FILTER_TEXT, FILTER_CODES, FILTER_ANDOR FROM NS_REPORT_SAVE_FILTER WHERE SAVEREPORTID = " & SaveReportID & " ORDER BY ROWID ASC"
					Call OpenRs(rsF, SQL)
					FilterList = ""
					
					'------------------------------------------------------------------------------------
					' 	HERE WE ARE BUILDING AN ARRAY OF CODES SO THAT WE CAN LOOP THROUGH IT AND PICK OUT 
					'	WHAT FILTERS HAVE BEEN SELECTED ON A SAVED PROJECT.
					' 
					' WE USE THE FOLLOWING :	[<*FILTER_BREAKER*>] 	-  	??
					'							[*<brkr>*]				-	??
					'------------------------------------------------------------------------------------
					while NOT rsF.EOF
						if FilterList = "" then
							FilterList = rsF("FILTERID") & "[*<brkr>*]" & rsF("FILTER_CODES") & "[*<brkr>*]" & rsF("FILTER_TEXT") & "[*<brkr>*]" & rsF("FILTER_SQL") & "[*<brkr>*]" & rsF("FILTER_ANDOR")
						else
							FilterList = FilterList & "[<*FILTER_BREAKER*>]" & rsF("FILTERID") & "[*<brkr>*]" & rsF("FILTER_CODES") & "[*<brkr>*]" & rsF("FILTER_TEXT") & "[*<brkr>*]" & rsF("FILTER_SQL") & "[*<brkr>*]" & rsF("FILTER_ANDOR")
						end if
						rsF.moveNext
					wend
					
					Call CloseRs(rsF)
				else
					' IF WE GET TO THIS POINT THEN WE HAVE SOMEHOW LOST THE REPORT ID SO WE TREAT IT AS A NEW PROJECT IN THE NEXT STEPS.
					SaveReportID = ""	
				end if
				Call CloseRs(rsR)
			end if
		
		end if
		'end of PART B
End If

' end of empt records check
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="JavaScript">
    function ResetSelects() {
        for (i = 1; i < SelectListArray.length; i++)
            document.getElementById("sel_Project" + SelectListArray[i]).value = ""
    }

    function ResetOthers(iSel) {
        for (i = 1; i < SelectListArray.length; i++) {
            if (SelectListArray[i] != iSel)
                document.getElementById("sel_Project" + SelectListArray[i]).value = ""
        }
        ref = document.getElementsByName("Project")
        if (ref) {
            for (i = 0; i < ref.length; i++)
                ref[i].checked = false
        }
    }

    function PreviousStep() {
        location.href = "ReportList.asp"
    }

    function NextStep() {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }

        newProject = -1
        FoundChecked = false

        //loop through select boxes		
        for (i = 1; i < SelectListArray.length; i++) {
            if (document.getElementById("sel_Project" + SelectListArray[i]).value != "") {
                newGroup = SelectListArray[i];
                newProject = document.getElementById("sel_Project" + SelectListArray[i]).value;
                FoundChecked = true;
                break;
            }
        }

        //loop through check boxes
        for (i = 1; i < CheckListArray.length; i++) {
            if (document.getElementById("Project" + CheckListArray[i]).checked == true) {
                newGroup = CheckListArray[i];
                newProject = document.getElementById("Project" + CheckListArray[i]).value;
                FoundChecked = true;
                break;
            }
        }

        if (FoundChecked == false) {
            document.getElementById("ErrorDiv").innerHTML = "<font color=red>** : You must select a project to continue. <BR><BR>"
            document.getElementById("ErrorDiv").style.display = "block"
            return;
        }

        document.getElementById("hid_Group").value = newGroup;
        document.getElementById("hid_Project").value = newProject;

        document.ReportForm.action = "ReportBuilder_Step2.asp"
        document.ReportForm.submit()
    }

    //reinitialises any data which was previously selected
    var SynchronizeComplete = false
    function Synchronize() {
        previouslySelected = document.getElementById("hid_Project").value
        if (previouslySelected != "") {

            ref = document.getElementsByName("Project")
            if (ref) {
                for (i = 0; i < ref.length; i++) {
                    if (ref[i].value == previouslySelected) {
                        ref[i].checked = true;
                        break;
                    }
                }
            }

            for (j = 1; j < SelectListArray.length; j++) {
                ref = document.getElementById("sel_Project" + SelectListArray[j])
                for (i = 0; i < ref.options.length; i++) {
                    if (ref.options[i].value == previouslySelected) {
                        ref.selectedIndex = i;
                        break;
                    }
                }
            }

        }
        SynchronizeComplete = true
    }
</script>
<body onload="initSwipeMenu(0);preloadImages();Synchronize()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form target="" method="post" name="ReportForm" action="">
    <table style="height:90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff">
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 1 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td>
                            <font style="font-size: 16px; color: white; background-color: #236B8E; padding-left: 10px;
                                padding-right: 10px"><b>MODULE</b></font> &nbsp;&nbsp; Please select a project
                            from the list below:
                        </td>
                        <td width="132" align="right">
                            <input type="button" class="RSLButton" value=" Previous " name="BtnPrevious" id="BtnPrevious" onclick="PreviousStep()" title="Go to : Report List" style="cursor:pointer" />
                            <input type="button" class="RSLButton" value=" Next " name="BtnNext" id="BtnNext" onclick="NextStep()" title="Go to Step 2 :  Module Info" style="cursor:pointer" />
                        </td>
                        <td width="11">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <div id="ErrorDiv" style="display: none">
                </div>
                <table style="border-collapse: collapse">
                    <%
			GroupSelectList = ""
			GroupCheckList = ""
			
			If (EmptyRecords = TRUE) then
				Response.Write  "<br/><br/><center><font style=""color:red;font-size:16px;font-weight:bold"">" & ErrorMessage & "</font></center>"
			
			else
			
						if FoundDuplicates = true then
						
							Response.Write "<br/><br/><center><font style=""color:red;font-size:16px;font-weight:bold""> Database project configuration error </font></center>"
						
						else
							
							Call OpenDB()
							'------------------------------------------------------------------------------------
							' 	C) 	WE GET THE LIST OF PROJECTS TO SHOW ON SCREEN AND WORK OUT IF THE PROJECT HAS MUTIPLE CAMPAIGNS 
							'		(ALMOST LIKE SUB PROJECTS)
							'
							'		WE DO TWO THINGS THEN :
							'
							'		I) IF THERE ARE MUTIPLE PROJECTS THEN WE DISPLAY A DROP DOWN WITH THE SUB PROJECTS
							'
							'		II) IF NOT MULTIPLE PROJECTS THEN WE JUST SHOW THE RADIO CHOICES.
							'------------------------------------------------------------------------------------

							' temp measure - take this out when the access rights have been applied
							'if (	(session("userid") = 392) OR(session("userid") = 416) OR(session("userid") = 402) or (session("userid") = 343) or (session("TEAMNAME") = "Executive Services") or (session("userid") = 128) or (session("userid") = 357) or (session("userid") = 113) OR (session("userid") = 218) OR (session("userid") = 35) OR (session("userid") = 611) OR (session("userid") = 709) OR (session("userid") = 853) OR (session("userid") = 799) OR (session("userid") = 758)) THEN
							if (  (Session("TeamCode") = "HUM")  OR (session("TEAMNAME") = "Executive Services") or  (session("userid") = 35) ) THEN

								SQL = "SELECT GROUPID, GROUPNAME, GROUPPROJECTS, GROUPDESC, MULTIPLE FROM NS_PROJECT WHERE ACTIVE = 1 ORDER BY ORDERBY"
							ELSE
								SQL = "SELECT GROUPID, GROUPNAME, GROUPPROJECTS, GROUPDESC, MULTIPLE FROM NS_PROJECT WHERE ACTIVE = 1 AND GROUPID NOT IN (2) ORDER BY ORDERBY"
							END IF
							Call OpenRs(rsReport, SQL)
							while NOT rsReport.EOF
							
								'------------------------------------------------------------------------------------
								' I)  MUTIPLE PROJECTS 
								'------------------------------------------------------------------------------------
								if (rsReport("MULTIPLE") = 1) then
								
									Response.Write "<tr><td><b><u>" & rsReport("GROUPNAME") & "</u></b></td><td align=""right"">"
									ProjectList = rsReport("GROUPPROJECTS")
									GroupSelectList = GroupSelectList & "," & rsReport("GROUPID")
                    %>
                    <select name="sel_Project<%=rsReport("GROUPID")%>" id="sel_Project<%=rsReport("GROUPID")%>" class="iagManagerSmallblk" style="width: 200px"
                        onchange="ResetOthers(<%=rsReport("GROUPID")%>)">
                        <option value="">Please Select</option>
                        <%
											SQL = "SELECT GROUPPROJECTID, CATEGORY FROM NS_GROUPPROJECT WHERE GROUPPROJECTID IN (" & ProjectList & ") ORDER BY CATEGORY"
											Call OpenRs(rsP, SQL)
											While (NOT rsP.EOF)
												SelectedVar = ""
												if rsP("GROUPPROJECTID") = ProjectID then SelectedVar = " selected " 
                        %>
                        <option value="<%=(rsP.Fields.Item("GROUPPROJECTID").Value)%>" <%=SelectedVar%>>
                            <%=(rsP.Fields.Item("CATEGORY").Value)%></option>
                        <%
												rsP.MoveNext()
											Wend
											Call CloseRs(rsP)
                        %>
                    </select>
                    <%
									Response.Write "</td></tr>"
									Response.Write "<tr><td colspan=""2"" width=""400px"">" & rsReport("GROUPDESC") & "</td></tr>"
									Response.Write "<tr><td colspan=""2""><hr style=""border-top:1px dotted red;border-bottom:1px solid white""/></td></tr>"
						
								'------------------------------------------------------------------------------------
								' II)  SINGLE PROJECTS 
								'------------------------------------------------------------------------------------
								else
							
									' WE GET THE PROJECT NAME FROM THE CAMPAIGN TABLE 
									SQL = "SELECT GROUPPROJECTID, CATEGORY FROM NS_GROUPPROJECT WHERE GROUPPROJECTID IN (" & rsReport("GROUPPROJECTS") & ") ORDER BY CATEGORY"
									Call OpenRs(rsProjectName, SQL)
									ProjectName = "Unknown"
									
									' make sure there are entries in the grouped project table
									if NOT rsProjectName.EOF then
										
										ProjectName = rsProjectName("CATEGORY")
									
										GroupCheckList = GroupCheckList & "," & rsReport("GROUPID")			
										Response.Write "<tr><td><b><u>" & ProjectName & "</u></b></td><td rowspan=""2""><input type=""radio"" name=""Project"" id=""Project" & rsReport("GROUPID") & """ value=""" & rsReport("GROUPPROJECTS") & """ onclick=""ResetSelects()""></td></tr>"
										Response.Write "<tr><td width=""400px"">" & rsReport("GROUPDESC") & "</td></tr>"
										Response.Write "<tr><td colspan=""2""><hr style=""border-top:1px dotted #000033; border-bottom:1px solid white"" /></td></tr>"
									else
										' ERROR DI-01-02
										Response.Write "<tr><td><b><u>Cannot Find this module.</u></b></td><td rowspan=""2""></td></tr>"
										Response.Write "<tr><td width=""400px"">please contact the support team. A project cannot be found. <br/><b>Quote Reference DI-01-02</b></td></tr>"
										Response.Write "<tr><td colspan=""2""><hr style='border-top:1px dotted #000033; border-bottom:1px solid white"" /></td></tr>"
									
									end if
									Call CloseRs(rsProjectName)
									
								end if
								rsReport.moveNext
							wend
							Call CloseRs(rsReport)
							
							Call CloseDB()
							
						end if 
						' end duplicate check
			
				End If 
				' end empty record check
			
			'Call CloseDB()
                    %>
                </table>
                <script language="javascript" type="text/javascript">
			var SelectListArray = new Array(1<%=GroupSelectList%>)
			var CheckListArray = new Array(1<%=GroupCheckList%>)
			
			//'------------------------------------------------------------------------------------
			//' D)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
			//'------------------------------------------------------------------------------------
                </script>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=GroupID%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=ProjectID%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=ReportID%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=ReportType%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=ColumnCount%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=SelectedList%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=FilterList%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=SaveReportID%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=LineDateID%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=SaveTitle%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=SaveDescription%>" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
