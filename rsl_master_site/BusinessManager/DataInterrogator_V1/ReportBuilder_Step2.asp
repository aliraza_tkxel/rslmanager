<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - VALIDATE CHECK 
'									B - LABEL SETTING
'									C - DISPLAY OPTIONS ON SCREEN
'									D - VARIABLE STORAGE
'
' ***************************************************************************************************

CurrentMainMenu = 7
CurrentSubMenu = 4
HeaderPic = "nextstep"

' ***************************************************************************************************
'	 A ) 	CHECK IF STEP 1 HAS BEEN SKIPPED OR IF DATA HAS BEEN LOST ON THE WAY OVER TO STEP TWO AND REDIRECT THEM TO STEP 1
' ***************************************************************************************************
	if (Request.Form("hid_Project") = "") then Response.Redirect ("ReportBuilder_Step1.asp")
	
	
' ***************************************************************************************************
'	 B ) 	SET ALL THE LEBELLING AND NAMING FOR THE REPORT .
' ***************************************************************************************************
	
	Call OpenDB()
	
	SaveReportID = Request.Form("hid_SaveReportID")
	ReportName = "Creating a new report..."
	
	if (SaveReportID <> "") then
		SQL = "SELECT SAVENAME FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenRs(rsR, SQL)
		if NOT rsR.EOF then
			ReportName = "Amending Report - " & rsR("SAVENAME")
		end if
		Call CloseRs(rsR)
	end if
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="JavaScript">
    function PreviousStepNo(step) {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }
        document.ReportForm.action = "ReportBuilder_Step" + step + ".asp"
        document.ReportForm.target = ""
        document.ReportForm.submit()
    }


    function NextStep() {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }
        ref = document.getElementsByName("Report")
        FoundChecked = false
        if (ref.length) {
            for (i = 0; i < ref.length; i++) {
                if (ref[i].checked == true) {
                    document.getElementById("hid_Report").value = ref[i].value
                    FoundChecked = true
                }
            }
        }
        if (FoundChecked == false) {
            document.getElementById("ErrorDiv").innerHTML = "<font color='red'>** : You must select a report to continue. <br/><br/>"
            document.getElementById("ErrorDiv").style.display = "block"
            //alert("You must select a report to continue.")
            return;
        }
        document.ReportForm.action = "ReportBuilder_Step3.asp"
        document.ReportForm.submit()
    }

    //reinitialises any data which was previously selected
    var SynchronizeComplete = false
    function Synchronize() {
        previouslySelected = document.getElementById("hid_Report").value
        if (previouslySelected != "") {
            ref = document.getElementsByName("Report")
            if (ref.length) {
                for (i = 0; i < ref.length; i++) {
                    if (ref[i].value == previouslySelected) {
                        ref[i].checked = true
                        break
                    }
                }
            }
        }
        SynchronizeComplete = true
    }
</script>
<body onload="initSwipeMenu(0);preloadImages();Synchronize()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form target="" method="post" name="ReportForm" action="">
    <table style="height:90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">        
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 2 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td>
                            <font style="font-size: 16px; color: white; background-color: #236b8e; padding-left: 10px;
                                padding-right: 10px"><b>MODULE INFO</b></font> &nbsp;&nbsp; Please select a
                            report from the list below:
                        </td>
                        <td width="132" align="right">
                            <input type="button" class="RSLButton" value=" Previous " name="BtnPrevious" id="BtnPrevious" onclick="PreviousStepNo(1)"
                                title="Go to Step 1 : Module" style="cursor:pointer" />
                            <input type="button" class="RSLButton" value=" Next " name="BtnNext" id="BtnNext" onclick="NextStep()" title="Go to Step 3 : Columns" style="cursor:pointer" />
                        </td>
                        <td width="11">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <div id="ErrorDiv" style="display: none">
                </div>
                <table style='border-collapse: collapse'>
                    <%
			
			' *****************************************************************************************************************************
			'	C ) 	DISPLAY A LIST OF REPORTS THAT CAN BE ASSIGNED TO THE USER BUILT REPORT - THE USER WILL SELECT ONE BY RADIO OPTION
			' *****************************************************************************************************************************
				Call OpenDB()
				SQL = "	SELECT R.REPORTID, R.REPORTNAME, R.REPORTDESC FROM NS_REPORT R " &_
					  " 	INNER JOIN NS_REPORT_GROUP RG ON RG.REPORTID = R.REPORTID AND RG.GROUPID = " & Request.Form("hid_Group") & " " &_
					  "WHERE R.ACTIVE = 1 "
					  
				Call OpenRs(rsReport, SQL)
				If not rsReport.EOF then
					while NOT rsReport.EOF
						Response.Write "<tr><td><b><u>" & rsReport("REPORTNAME") & "</u></b></td><td rowspan=""2""><input type=""radio"" name=""Report"" value=""" & rsReport("REPORTID") & """></td></tr>"
						Response.Write "<tr><td width=""400px"">" & rsReport("REPORTDESC") & "</td></tr>"
						Response.Write "<tr><td colspan=""2""><hr style=""border-top:1px dotted #000033;border-bottom:1px solid white"" /></td></tr>"
						rsReport.moveNext
					wend
				else
					' ERROR DI-01-02
					Response.Write "<tr><td><b><u>Cannot Find any Module Info</u></b></td><td rowspan=""2""></td></tr>"
					Response.Write "<tr><td width=""400px"">Please contact the support team. A project cannot be found. <br/><b>Quote Reference DI-02-01</b></td></tr>"
					Response.Write "<tr><td colspan=""2""><hr style=""border-top:1px dotted #000033;border-bottom:1px solid white"" /></td></tr>"
				
				end if				
				Call CloseRs(rsReport)
				Call CloseDB()
				
			'------------------------------------------------------------------------------------
			' D)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
			'------------------------------------------------------------------------------------	
                    %>
                </table>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=Request.Form("hid_Group")%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=Request.Form("hid_Project")%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=Request.Form("hid_Report")%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=Request.Form("hid_ReportType")%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=Request.Form("hid_ColumnCount")%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=Request.Form("hid_SelectedList")%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=Request.Form("hid_FilterList")%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=Request.Form("hid_SaveReportID")%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=Request.Form("hid_LineDate")%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=Request.Form("hid_SaveTitle")%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=Request.Form("hid_SaveDescription")%>" />
                <input type="hidden" name="CLICKEDPREVIOUS" id="CLICKEDPREVIOUS" value="1" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>
        
    </table></form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
