<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - VALIDATE CHECK 
'									B - LABEL SETTING
'									C - SELECT DEPENDANT COLUMNS
'									D - GET LEVELS /  COLUMNS
'									E - DISPLAY DATA ON SCREEN
'									F - VARIABLE STORAGE
'
' ***************************************************************************************************

' ***************************************************************************************************
'	 A ) 	CHECK IF STEP 1 HAS BEEN SKIPPED OR IF DATA HAS BEEN LOST ON THE WAY OVER TO STEP TWO AND REDIRECT THEM TO STEP 1
' ***************************************************************************************************

	if (Request.Form("hid_Report") = "") then Response.Redirect ("ReportBuilder_Step1.asp")
	
	Call OpenDB()
	
' ***************************************************************************************************
'	 B ) 	SET ALL THE LEBELLING AND NAMING FOR THE REPORT .
' ***************************************************************************************************
	
	SaveReportID = Request.Form("hid_SaveReportID")
	ReportName = "Creating a new report..."
	
	if (SaveReportID <> "") then
		SQL = "SELECT SAVENAME FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenRs(rsR, SQL)
		if NOT rsR.EOF then
			ReportName = "Amending Report - " & rsR("SAVENAME")
		end if
		Call CloseRs(rsR)
	end if
	
	
' ***************************************************************************************************
'	 C ) 	DEPENDANTS - 	1	THERE ARE SOME COLUMNS THAT CANNOT BE SELECTED FOR FILTERING UNLESS
'						 		ANOTHER COLUMN HAS BEEN SELECTED FIRST.
'							2	THIS IS SO WE CAN FILTER PROPERLY ON OUR SQL - SO COLUMNID IS DEP ON PARENT
'							3	THIS MEANS THAT A DROP DOWN MAY BE INFLUENCED BY THE DROP DOWN IN A DEPENDANT
'								FILTER POP UP.
'				
'							EXAMPLE : 	ADDRESS 2 CANNOT BE SELECTED UNLESS ADDRESS 1 IS SELECTED
'										COLUMN ID 	= 	ADDRESS 2
'										PARENT		= 	ADDRESS 1
' ***************************************************************************************************
	
	JavaItem = ""
	JavaParent = ""
	JavaCount = 0
	SQL = "SELECT COLUMNID, PARENT FROM NS_REPORT_HIERACHY WHERE PARENT IS NOT NULL "

	Call OpenRs(rsH, SQL)
	while NOT rsH.EOF 
		if (JavaItem = "") then
			JavaItem =  "'" & rsH("COLUMNID") & "'"
			JavaParent = "'" & rsH("PARENT")		& "'"
		else
			JavaItem = JavaItem & ",'" &  rsH("COLUMNID") & "'" 
			JavaParent = JavaParent & ",'" & rsH("PARENT")	& "'"
				
		end if
			JavaCount = JavaCount + 1
		rsH.moveNext
	wend
	Call CloseRs(rsH)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .Cell
        {
            text-align: center;
            width: 20px;
            height: 20px;
            cursor: pointer;
            border-bottom: 1px solid Silver;
        }
        .TextDiv
        {
            position: relative;
            text-overflow: ellipsis;
            overflow: hidden;
            width: 135px;
            white-space: nowrap;
        }
        .TextTab
        {
            border-bottom: 1px solid Silver;
        }
        .TabSpacer
        {
            border-bottom: 1px solid Red;
            background-color: #bdbdbd;
        }
        .NewLevel
        {
            background-color: #a3a3a3;
            color: #ffffff;
            height: 20px;
            font-weight: bold;
        }
        #mylayerDiv
        {
            z-index: 1000;
            font-family: tahoma;
            font-size: 11px;
            color: Red;
            padding: 6px 6px;
            border: 1px black solid;
            position: absolute;
            left: -400;
            width: 331;
            background-color: #f5f5dc;
        }
    </style>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/DynLayer.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--
//Highlights the Row into a red colour
function HiLite(ColumnID){
	document.getElementById("TD" + ColumnID).style.backgroundColor = "#EEEEEE"
	document.getElementById("TD" + ColumnID).style.color = "#FFFFFF"	
	}

//UN-Highlights the Row into a purple colour
function UnHiLite(ColumnID){
	document.getElementById("TD" + ColumnID).style.backgroundColor = ""
	document.getElementById("TD" + ColumnID).style.color = ""	
	}

function CombineFilters(){
	document.getElementById("hid_FilterList").value = ""
	FilterData = ""
	ref = document.getElementsByName("iFilters")
	if (ref.length){
		SetInitial = false
		for (i=0;i<ref.length;i++){
			if (ref[i].value != ""){
				if (SetInitial == false) {
					FilterData = ref[i].value
					SetInitial = true
					}
				else {
					FilterData += "[<*FILTER_BREAKER*>]" + ref[i].value
					}
				}	
			}
		document.getElementById("hid_FilterList").value = FilterData
		}
	}	

function PreviousStep(){
	if (!SynchronizeComplete) {
		Synchronize()
		return;
		}
	CombineFilters()
	document.ReportForm.action = "ReportBuilder_Step5.asp"
	document.ReportForm.target = ""	
	document.ReportForm.submit()
	}
	
function PreviousStepNo(step){
	if (!SynchronizeComplete) {
		Synchronize()
		return;
		}
	CombineFilters()
	document.ReportForm.action = "ReportBuilder_Step" + step + ".asp"
	document.ReportForm.target = ""	
	document.ReportForm.submit()
	}

function NextStep(){
	if (!SynchronizeComplete) {
		Synchronize()
		return;
		}

	CombineFilters()
	document.ReportForm.action = "ReportBuilder_Step5.asp"
	document.ReportForm.target = ""
	document.ReportForm.submit()
	}

//Loads the appropriate Filter Options into dialog window
function LoadFilterPopup(iRef) {
	
	// WE USE THE DEPENDANT COLUMNS IN AN ARRAY HERE (SEE PART C)
	var ArrayItem      = new Array(<%=JavaItem%>);
	var ArrayParent    = new Array(<%=JavaParent%>);

	matchedIndex = -1;
	for (i=0; i<ArrayItem.length; i++){
		if (iRef == ArrayItem[i]){
			matchedIndex = i;
			break;
			}
		}

	// WE LOOK AT THE FILTER ID AND SEE IF THIS IS IN OUR DEPENDANT ARRAY LIST.
	var parentData = ""
	
	if (matchedIndex != -1){
	
		aParent = document.getElementById("Filter" + ArrayParent[matchedIndex]).value.split("[*<brkr>*]")
	
		if (aParent.length > 1) 
			aParent = aParent[1].split("*|Z|*")
		else
			aParent = ""
		
		// IF DEPENDANT COLUMN IS IN THE LIST WE ATTACH IT TO THIS STRING AND THEN WE POST IT TO THE POP UP FILTER BOX 
		parentData = "&pt="+aParent
	
		}
	window.showModalDialog("popups/FilterLoader.asp?FilterID=" + iRef + parentData + "&Project=<%=Request.Form("hid_Project")%>&Rand=" + new Date(), document, "dialogWidth=410px;dialogHeight=400px")
	//window.open("popups/FilterLoader.asp?FilterID=" + iRef + parentData + "&Project=<%=Request.Form("hid_Project")%>&Rand=" + new Date())

	}	

var	SynchronizeComplete = false
function Synchronize(){

	FilterData = document.getElementById("hid_FilterList").value
	if (FilterData != ""){
		FilterArray = FilterData.split("[<*FILTER_BREAKER*>]")
		for (i=0;i<FilterArray.length;i++){
			MiniArray = FilterArray[i].split("[*<brkr>*]")
			iObj = document.getElementsByName("TD" + MiniArray[0])			
			if (iObj.length){
				document.getElementById("ImgI" + MiniArray[0]).src = document.getElementById("filter").src			
				iObj[0].title = MiniArray[2]				
				document.getElementById("Filter" + MiniArray[0]).value = FilterArray[i]			
				}
			}
		}
	SynchronizeComplete = true
	}	
//-->
</script>
<script type="text/javascript" language="JavaScript">
<!--
    var TipReady = false

    function init() {
        mylayer = new DynLayer("mylayerDiv")
        mylayer.slideInit()
        TipReady = true
    }

    function getInfo(iID) {
        if (TipReady == true) {
            document.ReportForm.action = "Serverside/Information.asp?ID=" + iID
            document.ReportForm.target = "Report4Server<%=TimeStamp%>"
            document.ReportForm.submit()
        }
    }

    var TipShowing = false
    function showTip() {
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.moveTo(winW - 356, -250)
        mylayer.slideTo(winW - 356, 24 + document.body.scrollTop, 12, 20)
        TipShowing = true
    }

    function hideTip() {
        clearTimeout()
        mylayer.slideActive = false
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.slideTo(winW - 356, -250, 12, 20)
        TipShowing = false
    }

    function slideLeft() {
        if (TipShowing) {
            winW = document.body.offsetWidth - 20 + document.body.scrollLeft
            mylayer.slideTo(winW - 356, 24 + document.body.scrollTop, 12, 20)
        }
    }

    function setTimeoutRePosition() {
        if (TipShowing)
            setTimeout("slideLeft()", 200)
    }

    window.onresize = setTimeoutRePosition;
    window.onscroll = setTimeoutRePosition;
//-->
</script>
<body onload="initSwipeMenu(0);preloadImages();Synchronize();init()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <div id="mylayerDiv">
        <div id="TIP_MESSAGE">
        </div>
    </div>
    <form target="" method="post" name="ReportForm" action="">
    <table style="height:90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 4 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td width="378">
                            <font style="font-size: 16px; color: white; background-color: #236b8e; padding-left: 10px;
                                padding-right: 10px;"><b>FILTERS</b></font> &nbsp;&nbsp; Please apply any filters
                            which are required:
                        </td>
                        <td width="87">
                        </td>
                        <td width="267" align="right">
                            <input name="BtnPreviousStep1" id="BtnPreviousStep1" type="button" class="RSLButton" onclick="PreviousStepNo(1)"
                                value=" 1 " title="Go to Step 1 : Module" style="cursor:pointer" />
                            <input name="BtnPreviousStep2" id="BtnPreviousStep2" type="button" class="RSLButton" onclick="PreviousStepNo(2)"
                                value=" 2 " title="Go to Step 2 :  Module Info" style="cursor:pointer" />
                            <input name="BtnPreviousStep3" id="BtnPreviousStep3" type="button" class="RSLButton" value=" Previous " onclick="PreviousStepNo(3)"
                                title="Go to Step 3 : Columns" style="cursor:pointer" />
                            <input name="BtnNextStep" id="BtnNextStep" type="button" class="RSLButton" value=" Next " onclick="NextStep()" title="Go to Step 5 : Report Type" style="cursor:pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <table style="border-collapse: collapse" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table style="border-collapse: collapse" border="0" cellspacing="0" cellpadding="0">
                                <%
								
								Function CheckColumnCount()
									if (ColumnCount >= MaxPerColumn) then
										Response.Write "</table></td><td valign=""top""><table style=""border-collapse:collapse"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
										ColumnCount = 0
									end if
								End Function
								
								MaxPerColumn = 15
								
								Report = Request.Form("hid_Report")
								
								Call OpenDB()
								' ***************************************************************************************************
								'	 D ) 	GET THE LEVELS SO WE CAN ADD IN ALL THE ADDITIONAL COLUMNS THAT WE CAN CHOOSE FROM
								' ***************************************************************************************************
								
								
										SQL = "SELECT REPORT_LEVELS FROM NS_REPORT WHERE REPORTID = " & Report
										Call OpenRs(rsLevels, SQL)
										if (NOT rsLevels.EOF) then
											Levels = rsLevels("REPORT_LEVELS")
										else
											Levels = -1
										end if
										Call CloseRs(rsLevels)
										
										
								' ***************************************************************************************************
								'	 E ) 	WE GET ALL THE POSSIBLE COLUMNS THAT WE CAN USE IN THE USER BUILT REPORT AND DISPLAY THEM
								'
								' ***************************************************************************************************
								
								' NOTE : IAG SPECIFIC TABLE IN THIS SQL STATEMENT -  IAG_DEFAULTS
								
										' FILTER COLUMNS SQL -  THESE ARE ALL THE COLS WE CAN POSSIBLY FILTER BY
										SQL =  "SELECT RC.COLUMNID, RC.COLUMN_CODE, RC.COLUMN_DISPLAY_NAME, R.LEVELNAME, RC.CAN_BE_FILTERED, RC_PRNT.COLUMN_CODE AS PRNT_CODE " &_
											   "FROM NS_REPORT_COLUMN RC " &_ 
											   "LEFT JOIN NS_REPORT_COLUMN RC_PRNT ON RC_PRNT.COLUMNID = RC.PARENT " &_
											   "INNER JOIN NS_REPORT R ON R.REPORTID = RC.REPORT_LEVEL " &_
											   "INNER JOIN NS_REPORT_COLUMN_GROUP RCG ON RCG.COLUMNID = RC.COLUMNID AND RCG.GROUPID = " & Request.Form("hid_Group") & " " &_	   
											   "WHERE 	RC.ACTIVE = 1 AND RC.REPORT_LEVEL IN (" & Levels & ") " &_
											   "		AND RC.COLUMNID NOT IN (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'LOCALLSCFILTEREXCLUDES') "
										
										' WE DO THE ORDER BY SEPERATE SO WE CAN USE THE ABOVE SQL IN A COUNT SUB QUERY
										SQL_ORDER = "ORDER BY RC.REPORT_LEVEL ASC, RC.ORDERING ASC"
										
										' WE WANT TO DISPLAY THE ALL THE OPTIONS OVER 3 COLUMNS ON THE PAGE SO WE SEE HOPW MANY WE PUT IN EACH COLUMN  (DISPLAY PURPOSES)
										SQL_MAX = "SELECT COUNT(*) AS THECOUNT FROM (" & SQL & ") TMP"
										Call OpenRs(rsMax, SQL_MAX)
										if NOT rsMax.EOF then
											TempColumn = Round( (CDbl(rsMax("THECOUNT")) / 3) + 5.5)
											if MaxPerColumn < TempColumn then MaxPerColumn = TempColumn
										end if
										Call CloseRs(rsMax)
										
										SQL = SQL & SQL_ORDER
										
										Call OpenRs(rsReport, SQL)
										PreviousLevel = ""
										ColumnCount = 0
										while NOT rsReport.EOF
											
											ID = rsReport("COLUMNID")
											CurrentLevel = rsReport("LEVELNAME")
											
											Parent_Code = ""
											
											Parent_Code = rsReport("PRNT_CODE")
											if Parent_Code <> "" then
												Parent_Code = "<font class=""TextTab"" style=""color:red""><b>" & Parent_Code & "</b>:</font> "
											end if
											
											if (CurrentLevel <> PreviousLevel) then
												'this row makes sure the title is never put at the bottom of the list
												
												if (ColumnCount+1) = MaxPerColumn then 
													MaxPerColumn = MaxPerColumn - 1
													ColumnCount = ColumnCount + 1		
													CheckColumnCount()
												end if		
												
												Response.Write "<tr><td colspan=""4"" class=""NewLevel"">&nbsp;" & CurrentLevel & "</td></tr>" & vbCrLf
												ColumnCount = ColumnCount + 1	
												PreviousLevel = CurrentLevel
												CheckColumnCount()
											end if
											

											ListArray = split(Request.Form("hid_SelectedList"), ",")
											Line_Column_Color = ""
											for i = 0 to Ubound(ListArray)
												if (Cint(ListArray(i)) = cInt(ID)) then
													Line_Column_Color = "style='color:blue;'"
												end if										
											next				
																						
											
											' DISPLAY ALL THE COLUMNS THAT WE ARE TO FILTER BY USING RECORDS FROM COLUMN RECORDSET
											Response.Write "<tr title='" & rsReport("COLUMN_CODE") & " : " & rsReport("COLUMN_DISPLAY_NAME") & "'><td width=""30px"" class=""TextTab"">&nbsp;<strong>" & rsReport("COLUMN_CODE") & "</strong><input type=""hidden"" name=""iFilters"" id=""Filter"& ID & """></td><td class=""TextTab"" width=""135px"" " & Line_Column_Color & "><div class=""TextDiv"">&nbsp;" & Parent_Code & "" & rsReport("COLUMN_DISPLAY_NAME") & "</div></td>"
											if (rsReport("CAN_BE_FILTERED") = 1) then
												Response.Write "<td id=""TD"& ID & """ nowrap class=""Cell"" onmouseover=""HiLite('"& ID & "')"" onmouseout=""UnHiLite('"& ID & "')"" onclick=""LoadFilterPopup('" & ID & "')"" nowrap=""nowrap""><img id=""ImgI" & ID & """ name=""ImgI" & ID & """ src=""images/cross.gif"" border=""0"" width=""20"" height=""20"" alt=""""></td>"
											else
												Response.Write "<td id=""TD"& ID & """ nowrap class=""Cell"" style=""cursor:default"">&nbsp;</td>"	
											end if
										
											Response.Write "<td class=""Cell"" onclick=""getInfo(" & ID & ")"" style=""cursor:pointer""><img src=""images/info.gif"" width=""14"" height=""14"" alt=""Information"" /></td></tr>" & vbCrLf
											
											ColumnCount = ColumnCount + 1
											Call CheckColumnCount()
											rsReport.moveNext
										wend
										Call CloseRs(rsReport)
										Call CloseDB()
										
								'------------------------------------------------------------------------------------
								' F)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
								'------------------------------------------------------------------------------------	
								
                                %>
                            </table>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=Request.Form("hid_Group")%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=Request.Form("hid_Project")%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=Request.Form("hid_Report")%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=Request.Form("hid_ReportType")%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=Request.Form("hid_ColumnCount")%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=Request.Form("hid_SelectedList")%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=Request.Form("hid_FilterList")%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=Request.Form("hid_SaveReportID")%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=Request.Form("hid_LineDate")%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=Request.Form("hid_SaveTitle")%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=Request.Form("hid_SaveDescription")%>" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>
    </table>
    </form>
    <%
Response.Write "<img src=""images/cross.gif"" name=""cross"" id=""cross"" width=""1"" height=""1"" alt="""" />"
Response.Write "<img src=""images/tick.gif"" name=""tick"" id=""tick"" width=""1"" height=""1"" alt="""" />"
Response.Write "<img src=""images/filter.gif"" name=""filter"" id=""filter"" width=""1"" height=""1"" alt="""" />"
    %>
    <iframe name="Report4Server<%=TimeStamp%>" id="Report4Server<%=TimeStamp%>" style="display: none"
        src="/secureframe.asp"></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
