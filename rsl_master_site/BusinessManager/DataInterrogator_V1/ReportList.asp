<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="includes/Functions/TableBuilder.asp" -->
<%
	TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")

	CONST CONST_PAGESIZE = 16

	Dim PageName			 	 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 	 'the default order clause to be used, must be a number.
	Dim EmptyText			 	 'what to display if no records are returned
	Dim SQLCODE			     	 'the SQL CODE
	Dim ColumnLength 
		ColumnLength = 4
	ReDim TableTitles    (ColumnLength)	 'USED BY CODE
	ReDim DatabaseFields (ColumnLength)	 'USED BY CODE
	ReDim ColumnWidths   (ColumnLength)	 'USED BY CODE
	ReDim TDSTUFF        (ColumnLength)	 'USED BY CODE
	ReDim TDPrepared     (ColumnLength)	 'USED BY CODE
	ReDim ColData        (ColumnLength)	 'Syntax	[column title] | [database field] | [display length(px)] 
	ReDim SortASC        (ColumnLength)	 'All Items must be included, if a sort is not reuired for the field then put ""
	ReDim SortDESC       (ColumnLength)	 'All Array sizes must match
	ReDim TDFunc         (ColumnLength)	 'stores any functions that will be applied
	Dim SubQuery			 	 'used to get the name of the parent object
	Dim SubQueryTitle		 	 'the title of the parent object

	ColData (0) = "Name:|SAVENAME|"
	SortASC (0) = "SAVENAME ASC"
	SortDESC(0) = "SAVENAME DESC"
	TDSTUFF (0) = ""
	TDFunc  (0) = ""

	ColData (1) = "Organisation:|ORGNAME|190"
	SortASC (1) = "ORGNAME ASC"
	SortDESC(1) = "ORGNAME DESC"
	TDSTUFF (1) = ""
	TDFunc  (1) = ""

	ColData (2) = "Level:|LEVEL|120"
	SortASC (2) = "LEVEL ASC"
	SortDESC(2) = "LEVEL DESC"
	TDSTUFF (2) = ""
	TDFunc  (2) = ""

	ColData (3) = "Last Modified:|MODIFIED|140"
	SortASC (3) = "RS.MODIFIED ASC"
	SortDESC(3) = "RS.MODIFIED DESC"
	TDSTUFF (3) = ""
	TDFunc  (3) = ""

	ColData (4) = "By:|FULLNAME|120"
	SortASC (4) = "FULLNAME ASC"
	SortDESC(4) = "FULLNAME DESC"
	TDSTUFF (4) = ""
	TDFunc  (4) = ""

	PageName = "ReportList.asp"
	EmptyText = "No reports found in the system or filter(s) returned no results."
	DefaultOrderBy = SortDESC(3)
	RowClickColumn = " "" onclick=""""getSummary("" & rsSet(""SAVEREPORTID"") & "")"""" """
'	RowClickColumn = " "" onclick=""""load_me("" & rsSet(""SAVEREPORTID"") & "")"""" """


	Maximum_Table_Length = "100%"
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
	Call SetSort()

	Dim ORGANISATION, ORGANISATION_FILTER
		ORGANISATION_FILTER = ""
		ORGANISATION = Session("svOrgID")

	If ((ORGANISATION = "") OR (ISNULL(ORGANISATION))) Then
			ORGANISATION = -1
	Else
		If (IsInteger(ORGANISATION)) Then
			ORGANISATION = CInt(ORGANISATION)
		Else
			ORGANISATION = -1
		End if
	End if

		ORGANISATION_FILTER = ORGANISATION_FILTER & " AND RS.ORGID = " & ORGANISATION & " "

	NameFilter = ""
	if (Request("Name") <> "") then
		iName = Replace(Request("Name"), "'", "''")
		NameFilter = " AND (RS.SAVENAME LIKE '%" & iName & "%') "
	end if

	' temp measure - take this out when the access rights have been applied	
	'if ((session("userid") = 392) OR (session("userid") = 416) OR (session("userid") = 402) OR (session("userid") = 343) OR (session("TEAMNAME") = "Executive Services") OR (session("userid") = 128) OR (session("userid") = 357) OR (session("userid") = 113) OR (session("userid") = 218) OR (session("userid") = 35) OR (session("userid") = 611) OR (session("userid") = 289) or (session("userid") = 527) OR  (session("userid") = 362) OR (session("userid") = 709) OR (session("userid") = 853) OR (session("userid") = 799) OR (session("userid") = 758)) THEN
	if (  (Session("TeamCode") = "HUM")  OR (session("TEAMNAME") = "Executive Services") OR (session("userid") = 35)) THEN
	SQLCODE = "SELECT RS.SAVEREPORTID, RS.SAVENAME, RS.DESCRIPTION, ISNULL(O.NAME, 'RSL Manager') AS ORGNAME, RS.TYPEID, " &_
				"CASE WHEN ISGLOBAL = 1 THEN 'Standard' ELSE 'User Created' END AS TYPE, " &_
				"CASE WHEN RS.ORGID = -1 THEN 'RSL Manager'  ELSE 'Sub-Contractor' END AS LEVEL, " &_ 
				"C.FIRSTNAME + ' ' + C.LASTNAME AS FULLNAME, MODIFIED " &_
				"FROM NS_REPORT_SAVE RS " &_
				"	LEFT JOIN S_ORGANISATION O ON O.ORGID = RS.ORGID " &_
				"	INNER JOIN E__EMPLOYEE  C ON C.EMPLOYEEID = RS.MODIFIEDBY " &_
				"WHERE RS.ACTIVE = 1  " & NameFilter & ORGANISATION_FILTER &_
			  "ORDER BY " + Replace(orderBy, "'", "''") + ""

	ELSE

	SQLCODE = "SELECT RS.SAVEREPORTID, RS.SAVENAME, RS.DESCRIPTION, ISNULL(O.NAME, 'RSL Manager') AS ORGNAME, RS.TYPEID, " &_
				"CASE WHEN ISGLOBAL = 1 THEN 'Standard' ELSE 'User Created' END AS TYPE, " &_
				"CASE WHEN RS.ORGID = -1 THEN 'RSL Manager'  ELSE 'Sub-Contractor' END AS LEVEL, " &_ 
				"C.FIRSTNAME + ' ' + C.LASTNAME AS FULLNAME, MODIFIED " &_
				"FROM NS_REPORT_SAVE RS " &_
				"	LEFT JOIN S_ORGANISATION O ON O.ORGID = RS.ORGID " &_
				"	INNER JOIN E__EMPLOYEE  C ON C.EMPLOYEEID = RS.MODIFIEDBY " &_
				"WHERE RS.ACTIVE = 1  AND RS.PROJECTID <> 2 " & NameFilter & ORGANISATION_FILTER &_
			  "ORDER BY " + Replace(orderBy, "'", "''") + ""
	END IF

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1
		end if
	End If

	Call Create_Table()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .Header
        {
            background-color: Silver;
            border: 1px solid black;
            width: 90px;
            vertical-align: top;
        }
        .Data
        {
            border-bottom: 1px solid #cccccc;
        }
        #mylayerDiv
        {
            z-index: 1000;
            font-family: tahoma;
            font-size: 11px;
            color: Red;
            padding: 6px 6px;
            border: 1px black solid;
            position: absolute;
            left: -500;
            width: 431;
            background-color: #f5f5dc;
        }
        .lscButtonSmall
        {
            font-size: 9px;
            font-style: normal;
            background-color: #e3e3e3;
            border: 1px #F4F3F7 ridge;
            color: #000000;
            cursor: pointer;
            font-family: Tahoma, Arial;
            font-weight: normal;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/DynLayer.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript">
    var TipReady = false

    function init() {
        mylayer = new DynLayer("mylayerDiv")
        mylayer.slideInit()
        TipReady = true
    }

    function getSummary(RID) {
        if (TipReady == true) {
            document.LSCFORM.action = "Serverside/ReportSummary.asp?RID=" + RID
            document.LSCFORM.target = "ReportServer<%=TimeStamp%>"
            document.LSCFORM.submit()
        }
    }

    function DeleteMessage(MID) {
        document.getElementById("hid_MESSAGEID").value = MID
        document.LSCFORM.action = "/G_Messages/Serverside/DeleteMessage.asp"
        document.LSCFORM.target = "ReportServer<%=TimeStamp%>"
        document.LSCFORM.submit()
        hideTip()
    }

    var TipShowing = false
    function showTip() {
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.moveTo(winW - 456, -350)
        mylayer.slideTo(winW - 456, 24 + document.body.scrollTop, 12, 20)
        TipShowing = true
    }

    function hideTip() {
        clearTimeout()
        mylayer.slideActive = false
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.slideTo(winW - 456, -350, 12, 20)
        TipShowing = false
    }

    function slideLeft() {
        if (TipShowing) {
            winW = document.body.offsetWidth - 20 + document.body.scrollLeft
            mylayer.slideTo(winW - 456, 24 + document.body.scrollTop, 12, 20)
        }
    }

    function setTimeoutRePosition() {
        if (TipShowing)
            setTimeout("slideLeft()", 200)
    }

    window.onresize = setTimeoutRePosition;
    window.onscroll = setTimeoutRePosition;

    function RunReport(srid, typeid) {
        if (typeid == 5)
            window.open("Popups/LineCharter.asp?Rid=" + srid + "&Random=" + new Date(), "_blank", "height=500px,width=800px,resizable=yes,status=yes,scrollbars=yes")
        else if (typeid > 2)
            window.open("Popups/Charter.asp?Rid=" + srid + "&Random=" + new Date(), "_blank", "height=500px,width=800px,resizable=yes,status=yes,scrollbars=yes")
        else
            window.open("Popups/Reporter.asp?Rid=" + srid + "&Random=" + new Date(), "_blank", "height=480px,width=750px,resizable=yes,status=yes,scrollbars=yes")
    }


    function ExcelReport(srid) {
        window.open("Popups/Excel.asp?Rid=" + srid + "&Random=" + new Date(), "_blank", "height=170px,width=350px,resizable=yes,status=yes,scrollbars=no")
        document.getElementById("ExcelButton").disabled = true
        setTimeout("EnableButton()", 3000)
    }

    function EnableButton() {
        try {
            document.getElementById("ExcelButton").disabled = false
        }
        catch (e) {
            temp = 1
        }
    }

    function AmendReport(srid) {
        document.getElementById("hid_SaveReportID").value = srid
        document.LSCFORM.action = "ReportBuilder_Step1.asp"
        document.LSCFORM.target = ""
        document.LSCFORM.submit()
    }

    function DeleteReport(srid) {
        document.getElementById("hid_SaveReportID").value = srid
        answer = confirm("Are you sure you wish to delete this report?\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
        if (!answer) return false;
        document.LSCFORM.action = "Serverside/DeleteReport.asp"
        document.LSCFORM.target = ""
        document.LSCFORM.submit()
    }

    function DuplicateReport() {
        //document.getElementById("DuplicateTR").style.display = "block"
        // FireFox has an issue with the above and how to show the row in question. The below function takes care of the browser difference.
        ShowReportSummaryRow();
    }

    function SaveDuplicate(srid) {
        if (document.getElementById("DuplicateName").value.replace(" ", "") == "") {
            alert("Please enter a name for the duplicate report before you continue.");
            return false;
        }
        document.getElementById("hid_ReportName").value = document.getElementById("DuplicateName").value
        document.getElementById("hid_SaveReportID").value = srid
        document.LSCFORM.action = "ServerSide/DuplicateReport.asp"
        document.LSCFORM.target = ""
        document.LSCFORM.submit()
    }

    function UpdateView() {
        location.href = "ReportList.asp?Name=" + document.getElementById("txt_NAME").value
    }

    function NewReport() {
        location.href = "ReportBuilder_Step1.asp"
    }

    function ShowReportSummaryRow() {
        var DuplicateTR = document.getElementById('DuplicateTR');
        var status = DuplicateTR.style.display;
        if (status == 'table-row' || status == 'block') {
            DuplicateTR.style.display = 'none';
        }
        else {
            DuplicateTR.style.display = 'block';
            DuplicateTR.style.display = 'table-row';
        }
    }
</script>
<body onload="initSwipeMenu(0);preloadImages();init()" onunload="macGo()">
    <form name="LSCFORM" method="post" action="">
    <div id="mylayerDiv">
        <div id="TIP_MESSAGE">
        </div>
    </div>
    <input type="hidden" name="hid_ReportName" id="hid_ReportName" />
    <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" />
    </form>
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <center>
        <table style="margin-left: 3PX; border-top: 1px solid #000033" cellspacing="0" cellpadding="0"
            width="100%" bgcolor="#ffffff">
            <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                <td height="23">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                            <td>
                                &nbsp;&nbsp;<font style="font-size: 16px; color: white; background-color: #236b8e;
                                    padding-left: 10px; padding-right: 10px; vertical-align:bottom;"><b>RSL REPORTS LIST</b></font>
                            </td>
                            <td align="right">
                                &nbsp;
                            </td>
                            <td width="13">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="35">
                    <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                        border-right: 1px solid #000033; border-left: 1px solid #000033">
                        <tr>
                            <td width="447">
                                &nbsp;
                                <input name="BtnNewReport" id="BtnNewReport" type="button" class="rslButton" onclick="NewReport()" value="New Report" title="New Report" style="cursor:pointer" />
                            </td>
                            <td align="right">
                                <span class="silverbg">Search Name :
                                    <input type="text" name="txt_NAME" id="txt_NAME" class="textbox" value="<%=Server.HTMLEncode(iName)%>" />
                                </span><span class="silverbg">
                                    <input name="BtnGo" type="button" class="rslButton" onclick="UpdateView()" value=" GO " title="GO" style="cursor:pointer" />
                                </span>
                            </td>
                            <td width="10">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="border-top: 1px solid #000033" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <%=TheFullTable%>
                </td>
            </tr>
        </table>
    </center>
    <iframe name="ReportServer<%=TimeStamp%>" id="ReportServer<%=TimeStamp%>" style="display: none" src="/secureframe.asp">
    </iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
