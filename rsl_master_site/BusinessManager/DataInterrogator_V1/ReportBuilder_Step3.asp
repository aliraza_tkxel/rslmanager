<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - VALIDATE CHECK 
'									B - LABEL SETTING
'									C - DISPLAY OPTIONS ON SCREEN
'									D - VARIABLE STORAGE
'
' ***************************************************************************************************

' ***************************************************************************************************
'	 A ) 	CHECK IF STEP 1 HAS BEEN SKIPPED OR IF DATA HAS BEEN LOST ON THE WAY OVER TO STEP TWO AND REDIRECT THEM TO STEP 1
' ***************************************************************************************************
	
	if (Request.Form("hid_Report") = "") then Response.Redirect ("ReportBuilder_Step1.asp")
	
	' format a date to use later
	TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")


' ***************************************************************************************************
'	 B ) 	SET ALL THE LEBELLING AND NAMING FOR THE REPORT .
' ***************************************************************************************************
		
	Call OpenDB()
	
	SaveReportID = Request.Form("hid_SaveReportID")
	ReportName = "Creating a new report..."
	
	if (SaveReportID <> "") then
		SQL = "SELECT SAVENAME FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenRs(rsR, SQL)
		if NOT rsR.EOF then
			ReportName = "Amending Report - " & rsR("SAVENAME")
		end if
		Call CloseRs(rsR)
	end if

' ***************************************************************************************************
'	 C ) 	SET THE MAXIMUM NUMBER OF COLS TO USE IN THE USER BUILT REPORT
'
'			STEP 5 WAS STEP 3 AND WOULD PASS THIS PAGE A REPORT TYPE - NOT NEEDED NOW
'			WE WILL NEED TO NOW COUNT THE COLUMNS AND PASS THAT COUNT TO STAP FIVE
'
' ***************************************************************************************************

	'SQL = "SELECT MAX_SELECTABLE_COLUMNS FROM NS_REPORT_TYPE WHERE TYPEID = " & Request.Form("hid_ReportType")
	'Call OpenRs(rsR, SQL)
	'if NOT rsR.EOF then
	'	MaxColumns = rsR("MAX_SELECTABLE_COLUMNS")
	'else
	'	MaxColumns = 0
	'end if
	
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .Cell
        {
            text-align: center;
            width: 20px;
            height: 20px;
            cursor: pointer;
            border-bottom: 1px solid Silver;
        }
        .Number
        {
            text-align: right;
            font-size: 8px;
            width: 10px;
            height: 20px;
            border-bottom: 1px solid Silver;
        }
        .TextDiv
        {
            position: relative;
            text-overflow: ellipsis;
            overflow: hidden;
            width: 125px;
            white-space: nowrap;
        }
        .TextTab
        {
            border-bottom: 1px solid Silver;
        }
        .TabSpacer
        {
            border-bottom: 1px solid Red;
            background-color: #bdbdbd;
        }
        .NewLevel
        {
            background-color: #a3a3a3;
            color: #ffffff;
            height: 20px;
            font-weight: bold;
        }
        #mylayerDiv
        {
            z-index: 1000;
            font-family: tahoma;
            font-size: 11px;
            color: Red;
            padding: 6px 6px;
            border: 1px black solid;
            position: absolute;
            left: -400;
            width: 331;
            background-color: #f5f5dc;
        }
    </style>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/DynLayer.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
    //var MaxColumns = 999 <%=MaxColumns%>
    var ColumnsSelected = 0
    var SelectedList = ""

    //Highlights the Row into a red colour
    function HiLite(ColumnID) {
        document.getElementById("TD" + ColumnID).style.backgroundColor = "#EEEEEE"
        document.getElementById("TD" + ColumnID).style.color = "#FFFFFF"
    }
    //UN-Highlights the Row into a purple colour
    function UnHiLite(ColumnID) {
        document.getElementById("TD" + ColumnID).style.backgroundColor = ""
        document.getElementById("TD" + ColumnID).style.color = ""
    }

    function ToggleMe(ColumnID) {
        if (document.getElementById("TX" + ColumnID).value == "") {

            // USER CAN PICK AS MANY COLUMNS AS NEEDED NOW WE SELECT THE REPORT TYPE AFTERWARDS
            // WE HAVE USED 999 AS THE MAXIMUM COLUMNS ALLOWED
            //if (ColumnsSelected >= MaxColumns) {
            //	alert("The maximum number of columns that can be selected for this report is " + MaxColumns + ".")
            //	return false;
            //	}
            document.getElementById("ImgI" + ColumnID).src = document.getElementById("tick").src
            document.getElementById("TX" + ColumnID).value = ColumnID
            ColumnsSelected++;

            //add the item to the list
            SelList = document.getElementById("hid_SelectedList").value
            if (SelList == "")
                SelList = ColumnID.toString()
            else
                SelList += "," + ColumnID.toString()
            document.getElementById("hid_SelectedList").value = SelList
            DoNumbering(SelList)
        }
        else {
            document.getElementById("ImgI" + ColumnID).src = document.getElementById("cross").src
            document.getElementById("TX" + ColumnID).value = ""
            document.getElementById("TDN" + ColumnID).innerHTML = ""
            ColumnsSelected--;

            //remove item from the list
            SelList = document.getElementById("hid_SelectedList").value
            ListArray = SelList.split(",")
            NewList = ""
            if (ListArray.length) {
                for (i = 0; i < ListArray.length; i++) {
                    if (ListArray[i] != ColumnID) {
                        if (NewList == "")
                            NewList = ListArray[i]
                        else
                            NewList += "," + ListArray[i]
                    }
                }
            }
            document.getElementById("hid_SelectedList").value = NewList
            DoNumbering(NewList)
        }
    }

    function DoNumbering(iList) {
        if (iList == "") return false
        ListArray = iList.split(",")
        if (ListArray.length) {
            for (j = 0; j < ListArray.length; j++)
                document.getElementById("TDN" + ListArray[j]).innerHTML = j + 1
        }
    }



    function PreviousStepNo(step) {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }
        document.ReportForm.action = "ReportBuilder_Step" + step + ".asp"
        document.ReportForm.target = ""
        document.ReportForm.submit()
    }


    function NextStep() {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }

        if (document.getElementById("hid_SelectedList").value == "") {
            document.getElementById("ErrorDiv").innerHTML = "<font color=red>** : Please select at least one column to continue. <BR><BR>"
            document.getElementById("ErrorDiv").style.display = "block"
            //alert("Please select at least one column to continue.")
            return;
        }

        if (ColumnsSelected > 20) {
            document.getElementById("ErrorDiv").innerHTML = "<font color=red>** : The maximum number of columns that can be selected at any one time is 20 <BR><BR>"
            document.getElementById("ErrorDiv").style.display = "block"
            //alert("The maximum number of columns that can be selected at any one time is 20")
            return false
        }

        document.ReportForm.hid_ColumnCount.value = ColumnsSelected
        document.ReportForm.action = "ReportBuilder_Step4.asp"
        document.ReportForm.target = ""
        document.ReportForm.submit()
    }

    //loops through the whole list of previously sent data.
    //if any exist in this depending upon other creiteria then it is added, otherwise
    //the list is changed accordingly.
    var SynchronizeComplete = false
    function Synchronize() {

        SelList = document.getElementById("hid_SelectedList").value
        if (SelList != "") {
            SelArray = SelList.split(",")
            NewList = ""
            for (i = 0; i < SelArray.length; i++) {
                iObj = document.getElementsByName("TX" + SelArray[i])
                if (iObj.length) {
                    //if the number of columns is greater than what is allowed then break loop
                    //if (ColumnsSelected >= MaxColumns) {
                    //	break;
                    //	}
                    ColumnsSelected++;
                    iObj[0].value = SelArray[i]
                    document.getElementById("ImgI" + SelArray[i]).src = document.getElementById("tick").src
                    if (NewList == "")
                        NewList = SelArray[i]
                    else
                        NewList += "," + SelArray[i]
                }
                document.getElementById("hid_SelectedList").value = NewList
                DoNumbering(NewList)
            }
        }
        SynchronizeComplete = true
    }	
//-->
</script>
<script type="text/javascript" language="JavaScript">
<!--
    var TipReady = false

    function init() {
        mylayer = new DynLayer("mylayerDiv")
        mylayer.slideInit()
        TipReady = true
    }

    function getInfo(iID) {
        if (TipReady == true) {
            document.ReportForm.action = "Serverside/Information.asp?ID=" + iID
            document.ReportForm.target = "Report3Server<%=TimeStamp%>"
            document.ReportForm.submit()
        }
    }

    var TipShowing = false
    function showTip() {
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.moveTo(winW - 356, -250)
        mylayer.slideTo(winW - 356, 24 + document.body.scrollTop, 12, 20)
        TipShowing = true
    }

    function hideTip() {
        clearTimeout()
        mylayer.slideActive = false
        winW = document.body.offsetWidth - 20 + document.body.scrollLeft
        mylayer.slideTo(winW - 356, -250, 12, 20)
        TipShowing = false
    }

    function slideLeft() {
        if (TipShowing) {
            winW = document.body.offsetWidth - 20 + document.body.scrollLeft
            mylayer.slideTo(winW - 356, 24 + document.body.scrollTop, 12, 20)
        }
    }

    function setTimeoutRePosition() {
        if (TipShowing)
            setTimeout("slideLeft()", 200)
    }

    window.onresize = setTimeoutRePosition;
    window.onscroll = setTimeoutRePosition;
//-->
</script>
<body onload="initSwipeMenu(0);preloadImages();Synchronize();init()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <div id="mylayerDiv">
        <div id="TIP_MESSAGE">
        </div>
    </div>
    <form target="" method="post" name="ReportForm" action="">
    <table style="height: 90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 3 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td width="539">
                            <font style="font-size: 16px; color: white; background-color: #236b8e; padding-left: 10px;
                                padding-right: 10px"><b>COLUMNS</b></font> &nbsp;&nbsp; Please select the columns
                            you would like to display:
                        </td>
                        <td width="193" align="right">
                            <input name="BtnPreviousStep1" id="BtnPreviousStep1" type="button" class="RSLButton" onclick="PreviousStepNo(1)"
                                value=" 1 " title="Go to Step 1 : Module" style="cursor: pointer" />
                            <input name="BtnPreviousStep2" id="BtnPreviousStep2" type="button" class="RSLButton" onclick="PreviousStepNo(2)"
                                value=" Previous " title="Go to Step 2 :  Module Info" style="cursor: pointer" />
                            <input name="BtnNextStep" id="BtnNextStep" type="button" class="RSLButton" onclick="NextStep()" value=" Next "
                                title="Go to Step 4 : Filters" style="cursor: pointer" />
                        </td>
                        <td width="4">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <div id="ErrorDiv" style="display: none">
                </div>
                <table style="border-collapse: collapse" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <table style="border-collapse: collapse" border="0" cellspacing="0" cellpadding="0">
                                <%
							
							Function CheckColumnCount()
								if (ColumnCount >= MaxPerColumn) then
									Response.Write "</table></td><td valign=""top""><table style=""border-collapse:collapse"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
									ColumnCount = 0
								end if
							End Function
							
							MaxPerColumn = 15
							
							Report = Request.Form("hid_Report")
							
							' ***************************************************************************************************
							'	 D ) 	GET THE LEVELS SO WE CAN ADD IN ALL THE ADDITIONAL COLUMNS THAT WE CAN CHOOSE FROM
							' ***************************************************************************************************
									Call OpenDB()
									
									SQL = "SELECT REPORT_LEVELS FROM NS_REPORT WHERE REPORTID = " & Report
									Call OpenRs(rsLevels, SQL)
									if (NOT rsLevels.EOF) then
										Levels = rsLevels("REPORT_LEVELS")
									else
										Levels = -1
									end if
									Call CloseRs(rsLevels)
							
							' ***************************************************************************************************
							'	 E ) 	WE GET ALL THE POSSIBLE COLUMNS THAT WE CAN USE IN THE USER BUILT REPORT AND DISPLAY THEM
							'			
							' ***************************************************************************************************
							
									' SELECTABLE COLUMNS SQL -  THESE ARE ALL THE COLS WE CAN POSSIBLY SELECTABLE FROM 
											SQL = "SELECT RC.LINECHART, RC.CAN_BE_DISPLAYED, RC.COLUMNID, RC.COLUMN_CODE, RC.COLUMN_DISPLAY_NAME, R.LEVELNAME FROM NS_REPORT_COLUMN RC " &_ 
										   "INNER JOIN NS_REPORT R ON R.REPORTID = RC.REPORT_LEVEL " &_
										   "INNER JOIN NS_REPORT_COLUMN_GROUP RCG ON RCG.COLUMNID = RC.COLUMNID  AND RCG.GROUPID = " & Request.Form("hid_Group") & " " &_
										   "WHERE RC.ACTIVE = 1 AND RC.REPORT_LEVEL IN (" & Levels & ") "
										'RW SQL   
									' WE DO THE ORDER BY SEPERATE SO WE CAN USE THE ABOVE SQL IN A COUNT SUB QUERY
									SQL_ORDER = "ORDER BY RC.REPORT_LEVEL ASC, RC.ORDERING ASC"
									
									' WE WANT TO DISPLAY THE ALL THE OPTIONS OVER 3 COLUMNS ON THE PAGE SO WE SEE HOPW MANY WE PUT IN EACH COLUMN  (DISPLAY PURPOSES)
									SQL_MAX = "SELECT COUNT(*) AS THECOUNT FROM (" & SQL & ") TMP"
									Call OpenRs(rsMax, SQL_MAX)
									if NOT rsMax.EOF then
										TempColumn = Round( (CDbl(rsMax("THECOUNT")) / 3) + 5.5)
										if MaxPerColumn < TempColumn then MaxPerColumn = TempColumn
									end if
									Call CloseRs(rsMax)
							
									SQL = SQL & SQL_ORDER
							
								Call OpenRs(rsReport, SQL)
								PreviousLevel = ""
								ColumnCount = 0
								while NOT rsReport.EOF
								
									ID = rsReport("COLUMNID")
									CurrentLevel = rsReport("LEVELNAME")
									if (CurrentLevel <> PreviousLevel) then
										'this row makes sure the title is never put at the bottom of the list
										if (ColumnCount+1) = MaxPerColumn then 
											MaxPerColumn = MaxPerColumn - 1
											ColumnCount = ColumnCount + 1		
											CheckColumnCount()
										end if		
										Response.Write "<tr><td colspan=""5"" class=""NewLevel"">&nbsp;" & CurrentLevel & "</td></tr>"
										ColumnCount = ColumnCount + 1	
										PreviousLevel = CurrentLevel
										CheckColumnCount()
									end if
									
									'if (rsReport("CAN_BE_DISPLAYED") <> 1 OR (rsReport("LINECHART") <> 1 AND Request.Form("hid_ReportType") = 5 )) then
									'	Response.Write "<tr><td width=""30px"" class=""TextTab"">&nbsp;<b>" & rsReport("COLUMN_CODE") & "</b><input type=""hidden"" name=""iCells"" id=""TX"& ID & """></td><td width=""125px"" class=""TextTab""><div class='TextDiv'>&nbsp;" & rsReport("COLUMN_DISPLAY_NAME") & "</div></td><td id=""TDN"& ID & """ class=""Number"" nowrap></td><td id=""TD"& ID & """ class=""Cell"" nowrap=""nowrap"" style='cursor:default'>&nbsp;</td><td class=""Cell"" onclick=""getInfo(" & ID & ")""><img src=""images/info.gif"" alt=""Information"" width=""14"" height=""14"" /></td></tr>"
									'else
										Response.Write "<tr title=""" & rsReport("COLUMN_CODE") & " : " & rsReport("COLUMN_DISPLAY_NAME") & """><td width=""30px"" class=""TextTab"">&nbsp;<b>" & rsReport("COLUMN_CODE") & "</b><input type=""hidden"" name=""iCells"" id=""TX"& ID & """ /></td><td width=""125px"" class=""TextTab""><div class=""TextDiv"">&nbsp;" & rsReport("COLUMN_DISPLAY_NAME") & "</div></td><td id=""TDN"& ID & """ class=""Number"" nowrap=""nowrap""></td><td id=""TD"& ID & """ class=""Cell"" onmouseover=""HiLite('"& ID & "')"" onmouseout=""UnHiLite('"& ID & "')"" onclick=""ToggleMe('" & ID & "')"" nowrap=""nowrap""><img name=""ImgI" & ID & """ id=""ImgI" & ID & """ src=""images/cross.gif"" border=""0"" width=""20"" height=""20"" alt=""Status"" /></td><td class=""Cell"" onclick=""getInfo(" & ID & ")""><img src=""images/info.gif"" alt=""Information"" width=""14"" height=""14"" /></td></tr>"
									'end if	
									
									ColumnCount = ColumnCount + 1
									Call CheckColumnCount()
									rsReport.moveNext
									
								wend
								
								Call CloseRs(rsReport)
								Call CloseDB()						
								
							'------------------------------------------------------------------------------------
							' D)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
							'------------------------------------------------------------------------------------								
                            %>
                            </table>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=Request.Form("hid_Group")%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=Request.Form("hid_Project")%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=Request.Form("hid_Report")%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=Request.Form("hid_ReportType")%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=Request.Form("hid_ColumnCount")%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=Request.Form("hid_SelectedList")%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=Request.Form("hid_FilterList")%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=Request.Form("hid_SaveReportID")%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=Request.Form("hid_LineDate")%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=Request.Form("hid_SaveTitle")%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=Request.Form("hid_SaveDescription")%>" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>
    </table>
    </form>
    <%
    Response.Write "<img src=""images/cross.gif"" name=""cross"" id=""cross"" width=""1"" height=""1"" alt="""" />"
    Response.Write "<img src=""images/tick.gif"" name=""tick"" id=""tick"" width=""1"" height=""1"" alt="""" />"
    %>
    <iframe name="Report3Server<%=TimeStamp%>" id="Report3Server<%=TimeStamp%>" style="display: none"
        src="/secureframe.asp"></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
