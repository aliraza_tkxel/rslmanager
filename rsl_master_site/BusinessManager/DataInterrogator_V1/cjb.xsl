<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" xmlns:html="http://www.w3.org/tr/rec-html40" result-ns="">

<xsl:template match="/"><xsl:apply-templates select="/xml/rs:data/z:row" /></xsl:template><xsl:template match="s:AttributeType"><xsl:choose><xsl:when test="@rs:hidden" /><xsl:when test="/xml/layout/field[@name $ieq$ context(-1)/@name]/@visible[text() $ieq$ 'false']"/></xsl:choose></xsl:template><xsl:template match="z:row"><xsl:for-each select="/xml/s:Schema/s:ElementType[@name='row']/s:AttributeType"><xsl:choose><xsl:when test="@rs:hidden" /><xsl:when test="/xml/layout/field[@name $ieq$ context(-1)/@name]/@visible[text() $ieq$ 'false']" /><xsl:otherwise>"<xsl:apply-templates select="context(-2)/@*[nodeName()=context(-1)/@name]" />",</xsl:otherwise></xsl:choose></xsl:for-each><br/></xsl:template>

<!-- whenever an element or attribute is found ("@*"), 
      it will run through this template -->
  <xsl:template match="@*">
<!-- depending on the format returned for the current item, 
              based on its node name, run its related Script function -->
    <xsl:choose>
<!-- currency value -->
      <xsl:when test="/xml/layout/field[@name $ieq$ context(-2)/@name]/@format[text() $ieq$ 'currency']">
        <xsl:eval>getCurrency(this.text)</xsl:eval>
      </xsl:when>
<!-- datetime value -->
      <xsl:when test="/xml/layout/field[@name $ieq$ context(-2)/@name]/@format[text() $ieq$ 'dateTime']">
        <xsl:eval>getDateTime(this.text)</xsl:eval>
      </xsl:when>

<!-- date value -->
      <xsl:when test="/xml/layout/field[@name $ieq$ context(-2)/@name]/@format[text() $ieq$ 'date']">
        <xsl:eval>getDate(this.text)</xsl:eval>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="text()" /></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:script>function getDateTime(value){ temp = new Date();
  temp.setYear(value.substr(0,4));
  temp.setMonth(value.substr(5,2));
  temp.setDate(value.substr(8,2));
  temp.setHours(value.substr(11,2));
  temp.setMinutes(value.substr(14,2));
  temp.setSeconds(value.substr(17,2)); return
  temp.toLocaleString(); } function getCurrency(value){return
  formatNumber(value, "#,###.00 $");} function getDate(value){ temp
  = new Date(); temp.setYear(value.substr(0,4));
  temp.setMonth(value.substr(5,2));
  temp.setDate(value.substr(8,2)); return
  temp.toLocaleString().substr(0,10); }</xsl:script>
</xsl:stylesheet>
