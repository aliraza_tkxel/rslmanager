<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - VALIDATE CHECK 
'									B - LABEL SETTING
'									C - DISPLAY OPTIONS ON SCREEN
'									D - VARIABLE STORAGE
'
' ***************************************************************************************************

' ***************************************************************************************************
'	 A ) 	CHECK IF STEP 1 HAS BEEN SKIPPED OR IF DATA HAS BEEN LOST ON THE WAY OVER TO STEP TWO AND REDIRECT THEM TO STEP 1
' ***************************************************************************************************
	if (Request.Form("hid_Report") = "") then Response.Redirect ("ReportBuilder_Step1.asp")
	
	Call OpenDB()


' ***************************************************************************************************
'	 B ) 	SET ALL THE LEBELLING AND NAMING FOR THE REPORT .
' ***************************************************************************************************
	
	SaveReportID = Request.Form("hid_SaveReportID")
	ReportName = "Creating a new report..."
	
	if (SaveReportID <> "") then
		SQL = "SELECT SAVENAME FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenRs(rsR, SQL)
		if NOT rsR.EOF then
			ReportName = "Amending Report - " & rsR("SAVENAME")
		end if
		Call CloseRs(rsR)
	end if
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript"language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="JAVASCRIPT">
    var FoundChecked = false
    function SetHidden() {
        ref = document.getElementsByName("ReportType")
        if (ref.length) {
            for (i = 0; i < ref.length; i++) {
                if (ref[i].checked == true) {
                    document.getElementById("hid_ReportType").value = ref[i].value
                    FoundChecked = true
                }
            }
        }
    }

    function PreviousStepNo(step) {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }
        document.ReportForm.action = "ReportBuilder_Step" + step + ".asp"
        document.ReportForm.target = ""
        document.ReportForm.submit()
    }


    function NextStep() {
        if (!SynchronizeComplete) {
            Synchronize()
            return;
        }
        SetHidden()
        if (FoundChecked == false) {
            document.getElementById("ErrorDiv").innerHTML = "<font color=red>** : You must select a report type to continue. <br/><br/>"
            document.getElementById("ErrorDiv").style.display = "block"

            //	alert("You must select a report type to continue.")
            return;
        }
        //IF A LINE CHART MAKE SURE THEY SELECT A DATE FIELD
        if (document.getElementById("hid_ReportType").value == 5) {

            var e = document.getElementById("sel_LineDate");
            var sel_LineDate = e.options[e.selectedIndex].value;

            if (sel_LineDate == "") {
                document.getElementById("ErrorDiv").innerHTML = "<font color='red'>** : You must select a date that will be used by the line chart. <br/><br/>"
                document.getElementById("ErrorDiv").style.display = "block"
                //alert("You must select a date that will be used by the line chart.")
                return;
            }
            else
                document.getElementById("hid_LineDate").value = ReportForm.sel_LineDate.value
        }
        document.ReportForm.action = "ReportBuilder_Step6.asp"
        document.ReportForm.submit()
    }

    //reinitialises any data which was previously selected
    var SynchronizeComplete = false
    function Synchronize() {
        previouslySelected = document.getElementById("hid_ReportType").value
        if (previouslySelected != "") {
            ref = document.getElementsByName("ReportType")
            if (ref.length) {
                for (i = 0; i < ref.length; i++) {
                    if (ref[i].value == previouslySelected) {
                        ref[i].checked = true
                        break
                    }
                }
            }
        }
        SynchronizeComplete = true
    }
	
</script>
<body onload="initSwipeMenu(0);preloadImages();Synchronize()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form target="" method="post" name="ReportForm" action="">
    <table style="height:90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">        
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 5 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td>
                            <font style="font-size: 16px; color: white; background-color: #236b8e; padding-left: 10px;
                                padding-right: 10px"><b>REPORT TYPE</b></font>&nbsp;&nbsp;Please select a report
                            type:
                        </td>
                        <td width="267" align="right">
                            <input name="BtnPreviousStep1" id="BtnPreviousStep1" type="button" class="RSLButton" onclick="PreviousStepNo(1)"
                                value=" 1 " title="Go to Step 1 : Module" style="cursor:pointer" />
                            <input name="BtnPreviousStep2" id="BtnPreviousStep2" type="button" class="RSLButton" onclick="PreviousStepNo(2)"
                                value=" 2 " title="Go to Step 2 : Mudule Info" style="cursor:pointer" />
                            <input name="BtnPreviousStep3" id="BtnPreviousStep3" type="button" class="RSLButton" onclick="PreviousStepNo(3)"
                                value=" 3 " title="Go to Step 3 : Columns" style="cursor:pointer" />
                            <input name="BtnPreviousStep4" id="BtnPreviousStep4" type="button" class="RSLButton" onclick="PreviousStepNo(4)"
                                value=" Previous " title="Go to Step 4 : Filters" style="cursor:pointer" />
                            <input name="BtnNextStep" id="BtnNextStep" type="button" class="RSLButton" onclick="NextStep()" value=" Next "
                                title="Go to Step 6 : Save the Report" style="cursor:pointer" />
                        </td>
                        <td width="11">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <div id="ErrorDiv" style="display: none">
                </div>
                <table style="border-collapse: collapse">
                    <%
				
				' *****************************************************************************************************************************
				'	C ) DISPLAY A LIST OF DISPLAY OPTION THAT CAN BE ASSIGNED TO THE USER BUILT REPORT  (BAR CHART, LINE GRAPH ECT)
				' *****************************************************************************************************************************
						Call OpenDB()
					
						LineChartColumn = -1
						ReportLevels = -1
						
						'-----------------------------------------------------------------------------------------------
						'	GET THE MAIN COLUMN THAT IS USED FOR THE LINE CHART AS WELL AS THE DIFFERENT LEVELS IT APPLIES TO
						'------------------------------------------------------------------------------------------------
						SQL = "SELECT ISNULL(LINECHARTCOLUMN, -1) AS LINECHARTCOLUMN, REPORT_LEVELS FROM NS_REPORT WHERE REPORTID = " & Request.Form("hid_Report")
						Call OpenRs(rsL, SQL)
						if NOT rsL.EOF then
							LineChartColumn = rsL("LINECHARTCOLUMN")
							ReportLevels = rsL("REPORT_LEVELS")
						end if
						Call CloseRs(rsL)
						
						'-----------------------------------------------------------------------------------------------
						'	IF THERE IS NO COLUMN THEN WE SET THE TYPE TO 5 (dont show the line chart option)
						'------------------------------------------------------------------------------------------------
						if (LineChartColumn = -1) then
							ExtraSQL = " AND TYPEID <> 5 "
						end if
						
						'------------------------------------------------------------------------------------------------------------------
						'	WRITE ALL THE OPTIONS TO THE SCREEN THAT WE HAVE (Detailed List,Grouped List,Pie Chart,Bar Chart,Line Chart)
						'------------------------------------------------------------------------------------------------------------------
					
						SQL = "SELECT TYPEID, TYPENAME, TYPEDESC, MAX_SELECTABLE_COLUMNS FROM NS_REPORT_TYPE WHERE TYPEACTIVE = 1 " & ExtraSQL
						Call OpenRs(rsReport, SQL)
						while NOT rsReport.EOF
							
							Response.Write "<tr><td><b><u>" & rsReport("TYPENAME") & "</u></b></td><td rowspan=""2"">"

							IF (cInt(rsReport("MAX_SELECTABLE_COLUMNS")) >=  cInt(Request.Form("hid_ColumnCount"))) THEN
								Response.Write "<input type=""radio"" name=""ReportType"" value=""" & rsReport("TYPEID") & """  >"
							else
								Response.Write "<input type=""radio"" name=""ReportType"" value=""" & rsReport("TYPEID") & """ disabled=""disabled"" ><font style=""color:red"">Too many columns selected for this option.</font>"
							end if
							
							Response.Write "</td></tr>"
							Response.Write "<tr><td width=""400px"">" & rsReport("TYPEDESC") & "</td></tr>"
							if (LineChartColumn <> -1 AND rsReport("TYPEID") = 5) then
                    %>
                    <tr>
                        <td>
                            Select Date Parameter for Line Chart :
                            <select name="sel_LineDate" id="sel_LineDate" class="iagManagerSmallBlk" style="width: 150px">
                                <option value="">Please Select</option>
                                <%
					
						'------------------------------------------------------------------------------------------------------------------
						'	 	display an option box to show column to assign to the line chart
						'------------------------------------------------------------------------------------------------------------------
						
							SQL = "SELECT DISTINCT rc.ORDERING, RC.COLUMNID, ISNULL(RC.COLUMN_CODE + ' ','') + RC.COLUMN_DISPLAY_NAME AS COLNAME FROM NS_REPORT R " &_ 
									"INNER JOIN NS_REPORT_COLUMN RC ON RC.COLUMNID = R.LINECHARTCOLUMN " &_ 
									"WHERE R.REPORTID IN (" & ReportLevels & ") AND R.LINECHARTCOLUMN IS NOT NULL " &_
									"ORDER BY RC.ORDERING"	
							Call OpenRs(rsLD, SQL)
							while NOt rsLD.EOF
							
								isChecked = ""
								
								if (Cstr(Request.Form("hid_LineDate")) = Cstr(rsLD("COLUMNID"))) then
									isChecked = " selected"
								end if
								
								Response.Write "<option value=""" & rsLD("COLUMNID") &""" " & isChecked & ">" & rsLD("COLNAME") & "</option>"
								rsLD.moveNext
							wend
							Call CloseRs(rsLD)
                                %>
                            </select>
                        </td>
                    </tr>
                    <%		
						end if
						Response.Write "<tr><td colspan=""2""><hr style=""border-top:1px dotted #000033;border-bottom:1px solid white"" /></td></tr>"
						rsReport.moveNext
					wend
					Call CloseRs(rsReport)
					Call CloseDB()
				
				'------------------------------------------------------------------------------------
				' D)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
				'------------------------------------------------------------------------------------	
					
                    %>
                </table>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=Request.Form("hid_Group")%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=Request.Form("hid_Project")%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=Request.Form("hid_Report")%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=Request.Form("hid_ReportType")%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=Request.Form("hid_ColumnCount")%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=Request.Form("hid_SelectedList")%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=Request.Form("hid_FilterList")%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=Request.Form("hid_SaveReportID")%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=Request.Form("hid_LineDate")%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=Request.Form("hid_SaveTitle")%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=Request.Form("hid_SaveDescription")%>" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>        
       </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
