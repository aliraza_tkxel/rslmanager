<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	SaveReportID = Replace(Request.Querystring("RID"), "'", "''")
	If (NOT isNumeric(SaveReportID)) Then
		SaveReportID = -1
	Else
		SaveReportID = CInt(SaveReportID)
	End If

	Global = "No"

	If (SaveReportID <> "") Then
		SQL = "SELECT SAVENAME, DESCRIPTION, ISGLOBAL,ORGID FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenDB()
		Call OpenRs(rsR, SQL)
		If NOT rsR.EOF Then
			SaveName = rsR("SAVENAME")
			ReportDesc = rsR("DESCRIPTION")
			isGlobal = rsR("ISGLOBAL")
			orgid = rsR("orgid")
			If (isGlobal = 1) Then
				Global= "Yes"
			End If
		End If
		Call CloseRs(rsR)
		Call CloseDB()
	End If	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript">
<!--
    function ReturnData() {
        parent.document.getElementById("TIP_MESSAGE").innerHTML = document.getElementById("TIP_MESSAGE").innerHTML;
        parent.showTip();
    }
//-->
</script>
<body bgcolor="#bdbdbd" onload="ReturnData();">
    <div id="TIP_MESSAGE">
        <table width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td valign="top" class="Header">
                    Name :
                </td>
                <td class="Data">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <strong>
                                    <%=SaveName%></strong>
                            </td>
                            <td align="right" onclick="hideTip()" style="color: blue; cursor: pointer">
                                [X]
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" class="Header">
                    Description :
                </td>
                <td class="Data">
                    <%=ReportDesc%>&nbsp;
                </td>
            </tr>
            <%
ExportDisabled = ""
TypeId = -1
'get the display column data and titles and sort columns, plus the widths

'Rob's code prior to the projectid in NS_REPORT_SAVE being converted from an int to a comma separated field
DisplaySQL = "SELECT C.CATEGORY, RS.SAVECOLUMNS, RT.GROUPED, RT.TYPEID, RT.TYPENAME, R.REPORTNAME FROM NS_REPORT_SAVE RS " &_ 
				"INNER JOIN NS_GROUPPROJECT C ON C.GROUPPROJECTID = RS.PROJECTID " &_
				"INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID " &_
				"INNER JOIN NS_REPORT R ON R.REPORTID = RS.REPORTID " &_				
				"WHERE RS.ORGID = " & "-1" & " AND RS.SAVEREPORTID = " & SaveReportID

'The Join that links the NS_REPORT_SAVE to CAMPAIGN now handled by dbo.CONVERTCOMMA_TO_MANY_TO_ONE function which
'converts the comma separated projectid field to a table
 
DisplaySQL = "SELECT C.CATEGORY, RS.SAVECOLUMNS, RT.GROUPED, RT.TYPEID, RT.TYPENAME, R.REPORTNAME FROM NS_REPORT_SAVE RS " &_ 
				"INNER JOIN NS_GROUPPROJECT C ON C.GROUPPROJECTID IN " &_
					"(select projectid from dbo.CONVERTCOMMA_TO_MANY_TO_ONE() where projectid = RS.PROJECTID) " &_
				"INNER JOIN NS_REPORT_TYPE RT ON RT.TYPEID = RS.TYPEID " &_
				"INNER JOIN NS_REPORT R ON R.REPORTID = RS.REPORTID " &_				

				"WHERE  RS.SAVEREPORTID = " & SaveReportID

Call OpenDB()
Call OpenRs(rsSC, DisplaySQL)
If NOT rsSC.EOF Then
	SelectedList = rsSC("SAVECOLUMNS")
	TypeId = rsSC("TYPEID")
	If (TypeId = 5) Then ExportDisabled = " disabled"
	isGrouped = rsSC("GROUPED")
	Response.Write "<tr><td class=""Header"">Project : </td><td class=""Data"">" & rsSC("CATEGORY") & "</td></tr>"
	Response.Write "<tr><td class=""Header"">Report : </td><td class=""Data"">" & rsSC("REPORTNAME") & "</td></tr>"
	Response.Write "<tr><td class=""Header"">Type : </td><td class=""Data"">" & rsSC("TYPENAME") & "</td></tr>"
Else
	SelectedList = -1
	isGrouped = -1
End If
Call CloseRs(rsSC)
	
Response.Write "<tr><td class=""Header"">Columns : </td><td class=""Data"">" 

	'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
	SelectArray = Split(SelectedList, ",")
	TotalColumns = Ubound(SelectArray)
	If (isGrouped = 1) Then TotalColumns = TotalColumns + 1

	SelectSQL = ""
	For i=0 To Ubound(SelectArray)
		if (i > 0) Then SelectSQL = SelectSQL & " UNION ALL "
		SelectSQL = SelectSQL & "SELECT '<font color=blue>' + ISNULL(COLUMN_CODE,'') + '</font> ' + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
	Next

	counter = 1
	Call OpenRs(rsC,SelectSQL)
	While Not rsC.EOF
		If counter = 1 Then
			Response.Write rsC("COLUMN_TITLE")
		Else
			Response.Write ", " & rsC("COLUMN_TITLE")
		End If
		counter = counter + 1
		rsC.moveNext
	Wend
	Call CloseRs(rsC)

	If (isGrouped = 1) Then Response.Write ", COUNT" 

Response.Write "</td></tr>"


SQL = "SELECT FILTER_TEXT FROM NS_REPORT_SAVE_FILTER WHERE SAVEREPORTID = " & SaveReportID & " ORDER BY ROWID ASC"
Call OpenRs(rsF, SQL)
Response.Write "<tr><td class=""Header"">Filters : </td><td class=""Data"">" 
If NOT rsF.EOF Then
	count = 0
	While NOT rsF.EOF 
		If (count = 0) Then
			Response.Write "WHERE (" & rsF("FILTER_TEXT") & ")"
		Else
			Response.Write "<br/>AND (" & rsF("FILTER_TEXT") & ")"
		End If
		count = count + 1
		rsF.moveNext
	Wend
Else
	Response.Write "No Filter(s) applied."
End If
Response.Write "</td></tr>"

Call CloseDB()
            %>
            <tr>
                <td class="Header">
                    Actions :
                </td>
                <td class="Data">
                    <input type="button" name="BtnRunReport" id="BtnRunReport" value=" Run Report " class="lscButtonSmall" onclick="RunReport(<%=SaveReportID%>,<%=TypeId%>)" title="Run" style="cursor:pointer" />
                    <input type="button" name="ExcelButton" id="ExcelButton" value=" Export " class="lscButtonSmall" <%=ExportDisabled%> onclick="ExcelReport(<%=SaveReportID%>)" title="Export" style="cursor:pointer" />
                    <% if orgid = -1 then %>
                    <input type="button" name="BtnAmendReport" id="BtnAmendReport"  value=" Amend " class="lscButtonSmall" onclick="AmendReport(<%=SaveReportID%>)" title="Amend" style="cursor:pointer" />
                    <input type="button" name="BtnDeleteReport" id="BtnDeleteReport"  value=" Delete " class="lscButtonSmall" onclick="DeleteReport(<%=SaveReportID%>)" title="Delete" style="cursor:pointer" />
                    <input type="button" name="BtnDuplicateReport" id="BtnDuplicateReport"  value=" Duplicate " class="lscButtonSmall" onclick="DuplicateReport()" title="Duplicate" style="cursor:pointer" />
                    <% end if %>
                </td>
            </tr>
            <tr style="display: none" id="DuplicateTR">
                <td class="Header">
                    Duplicate Name :
                </td>
                <td>
                    <input maxlength="50" type="text" id="DuplicateName" name="DuplicateName" class="textbox"
                        style="width: 200px" />&nbsp;<input name="BtnSave" id="BtnSave" type="button" value=" Save " class="lscButtonSmall"
                            onclick="SaveDuplicate(<%=SaveReportID%>)" />
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
