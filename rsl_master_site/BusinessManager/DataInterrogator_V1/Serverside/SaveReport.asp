<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST ORGANISATION_FILTER_ID = 170

' ***************************************************************************************************
'
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - RETRIEVE VARIABLES
'									B - SAVE REPORT HEAD
'									C - BUILD QUERY
'									D - SAVE QUERY
'
' ***************************************************************************************************

' ***************************************************************************************************
' A)  RETRIEVE THE STORED VARIABLES THAT HAVE PASSED FROM STEP TO STEP
' ***************************************************************************************************

	ReportName = Replace(Request.Form("txt_ReportName"), "'", "''")
	ReportDesc = Replace(Request.Form("txt_ReportDesc"), "'", "''")
	isGlobal = Request.Form("rdo_Global")
	Group = Request.Form("hid_Group")
	Project = Request.Form("hid_Project")
	Report = Request.Form("hid_Report")
	ReportType = Request.Form("hid_ReportType")
	SelectedList = Request.Form("hid_SelectedList")
	FilterList = Request.Form("hid_FilterList")
	SaveReportID = Request.Form("hid_SaveReportID")
	LineDate = Request.Form("hid_LineDate")
	If (isNull(LineDate) OR LineDate = "") Then LineDate = "NULL"

	Call OpenDB()

' ***************************************************************************************************
' B)  SAVING THE REPORT
'	  2 ALTERNATIVES -	I) SAVE A NEW REPORT
'						II)SAVE AN AMENDED REPORT
'
' ***************************************************************************************************

'-----------------------------------------------------------------------------------------
'	I) SAVE A NEW REPORT - 	IF WE ARE SAVING THIS REPORT FOR THE FIRST TIME WE USE THIS
'							PART OF THE STATEMENT.
'-----------------------------------------------------------------------------------------


	If (SaveReportID = "") Then

		'Response.Write "New Save"

		SQL = "SET NOCOUNT ON; INSERT INTO NS_REPORT_SAVE (SAVENAME, DESCRIPTION, ISGLOBAL, ORGID, GROUPID, PROJECTID, REPORTID, TYPEID, SAVECOLUMNS, LINEDATE, CREATEDBY, CREATED, MODIFIEDBY, MODIFIED) VALUES " &_ 
				"('" & ReportName & "', '" & ReportDesc & "', 0,-1, " & Group & ", '" & Project & "', " & Report & ", " & ReportType & ", '" & SelectedList & "', " & LineDate & ", " & Session("USERID") & ", getdate(), " & Session("USERID") & ", getdate() );" &_
				"SELECT SCOPE_IDENTITY() AS NEWID; SET NOCOUNT OFF"
		Call OpenRs(rsIN, SQL)
		If NOT rsIN.EOF Then
			SaveReportID = rsIN("NEWID")
		End If
		Call CloseRs(rsIN)

	Else

'-----------------------------------------------------------------------------------------
'	II) SAVE AN AMENDED REPORT - 	IF WE ARE SAVING THIS REPORT AFTER AMENDING AN EXISTING
'									ONE THEN WE USE THIS SECTION OF CODE
'-----------------------------------------------------------------------------------------

		'Response.Write "Amended"

		SQL = "UPDATE NS_REPORT_SAVE SET SAVENAME = '" & ReportName & "', DESCRIPTION = '" & ReportDesc & "', ISGLOBAL = 0, GROUPID = " & Group & ", PROJECTID = '" & Project & "', REPORTID = " & Report & ", TYPEID = " & ReportType & ", " &_ 
				"SAVECOLUMNS = '" & SelectedList & "', LINEDATE = " & LineDate & ", MODIFIED = getdate(), MODIFIEDBY = " & Session("USERID") & " " &_ 
				"WHERE SAVEREPORTID = " & SaveReportID
		Conn.Execute SQL

		' WE DELETE THE ROW FROM THE QUERY TABLE AS WE WILL BE BUIDLING IT AGAIN FROM THE NEW PASSED VARIABLES
		SQL = "DELETE FROM NS_REPORT_SAVE_QUERY WHERE SAVEREPORTID = " & SaveReportID
		Conn.Execute SQL

		' WE DELETE THE ROW FROM THE FILTER TABLE AS WE WILL BE BUIDLING IT AGAIN FROM THE NEW PASSED VARIABLES
		SQL = "DELETE FROM NS_REPORT_SAVE_FILTER WHERE SAVEREPORTID = " & SaveReportID
		Conn.Execute SQL

	End If

' ***************************************************************************************************
' C)  BUILDING QUERY - WE BUILD THE QUERY STRING TO INSERT INTO THE REPORT QUERY TABLE
'
'		THIS COSISTS OF THE FOLLOWING PARTS 
'			I) 	BUILD THE COLUMNS
'			II) BUILD GROUP BY CLAUSE
'			III) BUILD THE FILTER
'			IIII) ANY ADDITONAL CLAUSES
'			IIIII) BUILD THE JOINS
'
' ***************************************************************************************************

'-----------------------------------------------------------------------------------------	
'	I)  BUILD THE COLUMNS -
'-----------------------------------------------------------------------------------------		

	' SELECT THE COLUMNS FOR THE QUERY
	'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
	SelectArray = Split(SelectedList, ",")
	SelectSQL = ""
	For i=0 To Ubound(SelectArray)
		If (i > 0) Then SelectSQL = SelectSQL & " UNION ALL "
		SelectSQL = SelectSQL & "SELECT COLUMNID,SQL_SELECT_COLUMN, INNER_JOIN, ISNULL(COLUMN_CODE,'') + ' ' + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
	Next
	
	Call OpenRs(rsC,SelectSQL)
	ColumnList = ""
	LeftList = "-99"  'dummy value required
	While Not rsC.EOF
		If ColumnList = "" Then
			ColumnList = rsC("SQL_SELECT_COLUMN")
			ColumnIDList = rsC("COLUMNID")
		Else
			ColumnList = ColumnList & ", " & rsC("SQL_SELECT_COLUMN")
			ColumnIDList = ColumnIDList & ", " &  rsC("COLUMNID")
		End If
		'THIS PART GETS ALL THE TABLE JOINS FOR ANY COLUMNS THAT WILL REQUIRE IT
		LeftJoin = rsC("INNER_JOIN")
		If (NOT isNull(LeftJoin) AND LeftJoin <> -1) Then
			LeftList = LeftList & "," & LeftJoin
		End If
		rsC.moveNext
	Wend
	Call CloseRs(rsC)

'-----------------------------------------------------------------------------------------
'	II)  ADD ON A GROUP BY CLAUSE - ONLY IF REQUIRED DEPENDING UPON THE REPORT TYPE
'-----------------------------------------------------------------------------------------

	'CHECK IF REPORT IS A GROUPED ONE.
	isGrouped = 0
	SQL = "SELECT GROUPED FROM NS_REPORT_TYPE WHERE TYPEID = " & ReportType
	Call OpenRs(rsGr, SQL)
	If NOT rsGr.EOF Then
		isGrouped = CInt(rsGr("GROUPED"))
	End If
	Call CloseRs(rsGr)

	'IF THE REPORT IS A LINE ONE THEN ADD THE RESPECTIVE GROUP BY BITS.
	GroupedClause = ""
	If (ReportType = 5) Then
		'GroupedClause = VbCrLf & " GROUP BY YEAR(DATEADDED), MONTH(DATEADDED) "
		ColumnList = " YEAR(DATEADDED), MONTH(DATEADDED), COUNT(*) AS GROUPCOUNT "
		ColumnList = ""
	ElseIf (isGrouped = 1) Then
		GroupedClause = VbCrLf & " GROUP BY " & ColumnList
		ColumnList = ColumnList & ", COUNT(*) AS GROUPCOUNT "
	End If

'-----------------------------------------------------------------------------------------
'	III)  BUILD THE FILTER QUERY 
'-----------------------------------------------------------------------------------------

	'BUILD THE FILTER QUERY
	FilterData = ""

	If (FilterList <> "") Then
		FilterArray = Split(FilterList, "[<*FILTER_BREAKER*>]")
		For i=0 to Ubound(FilterArray)
			MiniArray = Split(FilterArray(i), "[*<brkr>*]")
			If (isGrouped = 1 AND MiniArray(0) = ORGANISATION_FILTER_ID) Then
				'ignore item as the report is a global one, so we do not apply organisation filter
				vbYhjnIlopwe = 0
			Else
				SQL = "INSERT INTO NS_REPORT_SAVE_FILTER (SAVEREPORTID, FILTERID, FILTER_SQL, FILTER_TEXT, FILTER_CODES, FILTER_ANDOR) VALUES " &_ 
					  "(" & SaveReportID & ",'" & MiniArray(0) & "','" & Replace(MiniArray(3), "'", "''") & "','" & Replace(MiniArray(2), "'", "''") & "','" & Replace(MiniArray(1), "'", "''") & "','" & Replace(MiniArray(4), "'", "''") & "')"
				Conn.Execute SQL
				If (FilterData = "") Then
					FilterData = Mid(MiniArray(3),5) & VbCrLf
					FILTERCOLUMNS = MiniArray(0) 
				Else
					FilterData = FilterData & " " & MiniArray(3) & VbCrLf
					FILTERCOLUMNS = FILTERCOLUMNS & " , " & MiniArray(0)
				End If
			End If
		Next
		FilterArraySize = i
	End If


'-----------------------------------------------------------------------------------------
'	IIII)  ADD ADDITIONAL CLAUSES ONTO QUERY
'-----------------------------------------------------------------------------------------

	'GLOBAL PROJECT AND DATE CLAUSE
	If (Report > 1) Then
		'DateClause = " (E.DATEADDED >= '1 AUGUST 2004') "
		'Robs code which assumes the project clause is an integer
		'ProjectClause = " AND (E.CATEGORY = " & Project & " ) "

		'Chris code which assumes it is a comma separated field in now
		'uses an IN clause
		'ProjectClause = " AND (E.CATEGORY IN (" & Project & ") ) "

		If (FilterData <> "") Then
			'FilterData = DateClause & ProjectClause & " AND " & FilterData
			FilterData = FilterData
		Else
			FilterData = DateClause & ProjectClause
		End If
	Else
		DateClause = ""
		ProjectClause = ""
	End If

	'ADD A WHERE CLAUSE WHERE NECCESSARY.
	If (FilterData <> "") Then
		FilterData = "WHERE " & VbCrLf & FilterData
	End If

'-----------------------------------------------------------------------------------------	
'	IIIII)  BUILD ALL THE JOINS ONTO QUERY
'-----------------------------------------------------------------------------------------		


	'GET THE CORE INNER JOINS
	SQL = "SELECT INNER_JOINS FROM NS_REPORT WHERE REPORTID = " & Report
    Call OpenRs(rsReport, SQL) 
	if (NOT rsReport.EOF) then
		InnerJoins = rsReport("INNER_JOINS")
	end if

	SQL = "SELECT JOINSQL FROM NS_REPORT_INNER_JOIN WHERE JOINID IN (" & InnerJoins & ") ORDER BY JOINSORT"
	Call OpenRs(rsJ, SQL)
	InnerJoins = ""
	while NOT rsJ.EOF
		InnerJoins = InnerJoins & rsJ("JOINSQL") & " " & VbCrLf 
		rsJ.moveNext
	wend

	'-------------------------------------------------
	' BUILD THE INNER JOINS AND BUILD THE QUERY
	'-------------------------------------------------
	' ADDED 8 FEB 2007 - (1-4)

	' WE HAVE INSTANCES WHERE THE FILTER ID MAY NOT BE IN THE MASTER TABLE BUT THE ONE UNDER IT. tHIS MEANS THAT IF IT ISNT SELECTED AS A COLUMN
	' THEN NO JOIN WILL BE MADE FOR THE TABLE. WHAT WE HAVE DONE HERE IS INTRODUCE INNER JOINS THAT CAN ACTIVATE OTHER JOINS IF ITS REQUIRED.
	' eg. IF THERE IS NO FILTER THIS DOESNT NEED TO BE ADDED ONTO THE SQL.

	' 1. we get all the column ids - all columns for selected columns and columns for filter columns
	If FILTERCOLUMNS <> "" Then
		parent_join_list = FILTERCOLUMNS & "," & ColumnIDList
	Else
		parent_join_list = ColumnIDList
	End If

	' 2. we get a build a list of inner join id's using all the column ids
	SQL = 	"			SELECT INNER_JOIN_PARENT " &_
			"			FROM NS_REPORT_INNER_JOIN IJ " &_
			"				INNER JOIN NS_REPORT_COLUMN C ON C.INNER_JOIN = IJ.JOINID " &_
			"			WHERE COLUMNID IN (" & parent_join_list & ") AND INNER_JOIN_PARENT IS NOT NULL " 
	Call OpenRs(rsPJ, SQL)
	ParentJoins = ""
	While NOT rsPJ.EOF
		IF not ParentJoins <> "" Then
			ParentJoins = rsPJ("INNER_JOIN_PARENT")
		Else
			ParentJoins = ParentJoins & "," & rsPJ("INNER_JOIN_PARENT")
		End If
		rsPJ.moveNext
	Wend

	' 3. then we build a union to add onto the normal sql to add in all the inner joins and sort them as usual
	If ParentJoins <> "" Then
		filter_dependant_joins = 	"UNION " &_
									"SELECT JOINID,JOINSQL , JOINSORT " &_
									"FROM NS_REPORT_INNER_JOIN  " &_
									"WHERE JOINID IN (" & ParentJoins & ")"
	End If

	'4. FINALLY GET THE LEFT JOINS FOR ANY COLUMNS + (*new) add on any parent inner joins
	SQL = "SELECT JOINID,JOINSQL , JOINSORT FROM NS_REPORT_INNER_JOIN WHERE JOINID IN (" & LeftList & ") "  & filter_dependant_joins & " ORDER BY JOINSORT"
	Call OpenRs(rsLJ, SQL)
	LeftJoins = ""
	while NOT rsLJ.EOF
		LeftJoins = LeftJoins & rsLJ("JOINSQL") & " " & VbCrLf 
		rsLJ.moveNext
	wend

	' PUT IT ALL TOGETHER INTO ONE STRING
	If (ReportType = 5) Then
		FullSQLQuery = "FROM "  & VbCrLf & InnerJoins & " " & LeftJoins & " " & FilterData & GroupedClause
	Else
		FullSQLQuery = "SELECT "  & VbCrLf & ColumnList & " " & VbCrLf  & "FROM "  & VbCrLf & InnerJoins & " " & LeftJoins & " " & FilterData & GroupedClause	
	End If
	' END BUILD THE INNER JOINS AND BUILD THE QUERY


' ***************************************************************************************************
' D)  SAVE QUERY 
' ***************************************************************************************************


	SQL = "INSERT INTO NS_REPORT_SAVE_QUERY (SAVEREPORTID, REPORTQUERY) VALUES " &_ 
			"(" & SaveReportID & ", '" & Replace(FullSQLQuery, "'", "''") & "')"
	Conn.Execute SQL

	Call CloseDB()

	Response.Redirect "../ReportList.asp"
%>