<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	response.ContentType = "text/xml"

	Dim CONST_PAGESIZE 
	CONST_PAGESIZE = Request("page_size")
	
	sort_col = Request("sort_col")
	sort_dir = Request("sort_dir")
	
	Set objXML = Server.CreateObject("MSXML2.DOMDocument")
	
	Rid = Request("rid")
	oldRid = 1
	if (Rid = "") then
		oldRid = 2
		Rid = -300
	end if
	
	isReport = false
	
	Call OpenDB()
	SQL = "SELECT REPORTQUERY FROM NS_REPORT_SAVE_QUERY WHERE SAVEREPORTID = " & Rid
	Call OpenRs(rsQuery,SQL)
	if NOT rsQuery.EOF then
		isReport = true
		SQL = rsQuery("REPORTQUERY")
	end if
	Call CloseRs(rsQuery)
	
	if (sort_col <> "") then
		SQL = SQL & " ORDER BY " & sort_col & " " & sort_dir
	end if
	
'	Response.Write("<?xml version='1.0' encoding='ISO-8859-1' ?>")
	Response.Write "<ajax-response><response type='object' id='data_grid_updater'>"
	Response.Write "<rows update_ui='true'>"

		If Request.QueryString("page") = "" Then
			intpage = 1	
		Else
			if (IsNumeric(Request.QueryString("page"))) then
				intpage = CInt(Request.QueryString("page"))
			else
				intpage = 1			
			end if
		End If
	
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = MM_ContactManager_STRING			
		rsSet.Source = SQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		Offset = CLng(Request("offset"))+1
		If intRecordCount > 0 Then
			'rsSet.AbsolutePage = intPage
			rsSet.AbsolutePosition = Offset
			'intStart = rsSet.AbsolutePosition
			'If CInt(intPage) = CInt(intPageCount) Then
			'	intFinish = intRecordCount
			'Else
			'	intFinish = intStart + (rsSet.PageSize - 1)
			'End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.

		ColumnCount = rsSet.fields.count-1

		For intRecord = 1 to rsSet.PageSize

			
			Response.Write "<tr>" 
			'loop through the tds and build a single row
			Response.Write "<td>" & Offset & "</td>"
			for j=0 to Columncount
				theItem = rsSet(j)
				if isNull(theItem) then
					theItem = " "
				end if
				Response.Write "<td>" & Server.HTMLEncode(theItem) & "</td>"
				'Response.Write "<td convert_spaces=""true"">" & Server.HTMLEncode(theItem) & "</td>"
				'Response.Write "<td>ff" & data_grid_sort_col & "</td>"
			next
			Response.Write "</tr>" 
			
			Offset = Offset + 1	
			'count = count + 1
			rsSet.movenext()
			If rsSet.EOF Then Exit for
			
		Next

		End if
		
		rsSet.close()
		Set rsSet = Nothing

	Response.Write "</rows></response>" 
%>	
<response type="object" id="configureSearchRows">
	<numResults><%=intRecordCount%></numResults>
</response>
<%
	Response.Write "</ajax-response>"
%>