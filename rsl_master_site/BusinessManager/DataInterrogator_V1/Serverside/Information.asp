<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	ColumnID = Replace(Request.Querystring("ID"), "'", "''")
	If (NOT isNumeric(ColumnID)) Then
		ColumnID = -1
	Else
		ColumnID = CInt(ColumnID)
	End If

	SQL = "SELECT ISNULL('<FONT COLOR=BLUE>' + COLUMN_CODE + '</FONT> ', '') + COLUMN_DISPLAY_NAME AS FULLNAME,  COLUMN_DESCRIPTION FROM NS_REPORT_COLUMN WHERE COLUMNID = " & ColumnID
	Call OpenDB()
	Call OpenRs(rsR, SQL)
	If NOT rsR.EOF Then
		FullName = rsR("FULLNAME")
		Desc = rsR("COLUMN_DESCRIPTION")
	Else
		FullName = "Unknown"
		Desc = "No information available"
	End If
	Call CloseRs(rsR)
	Call closeDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript">
<!--
    function ReturnData() {
        parent.document.getElementById("TIP_MESSAGE").innerHTML = document.getElementById("TIP_MESSAGE").innerHTML;
        parent.showTip();
    }
//-->
</script>
<body bgcolor="#bdbdbd" onload="ReturnData();">
    <div id="TIP_MESSAGE">
        <table width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td class="Data" style="border-bottom: 1px dotted Silver">
                    <table cellpadding="0" cellspacing="0" width='100%'>
                        <tr>
                            <td>
                                <strong>
                                    <%=FullName%></strong>
                            </td>
                            <td align="right" onclick="hideTip()" style="color: blue; cursor: pointer">
                                [X]
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Data">
                    <%=Desc%>&nbsp;
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
