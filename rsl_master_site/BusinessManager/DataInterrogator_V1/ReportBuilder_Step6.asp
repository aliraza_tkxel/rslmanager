<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess =  true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
' ***************************************************************************************************
'	
'	PAGE INFO:	THIS PAGE ALLOWS THE USER TO SELECT WHAT PROJECT THEY WANT TO USE IN THEIR REPORT.
'
'		THIS PAGE CONSISTS OF STEPS A - VALIDATE CHECK 
'									B - LABEL SETTING
'									C - DISPLAY DATA ON-SCREEN
'									D - VARIABLE STORAGE
'
' ***************************************************************************************************

' ***************************************************************************************************
'	 A ) 	CHECK IF STEP 1 HAS BEEN SKIPPED OR IF DATA HAS BEEN LOST ON THE WAY OVER TO STEP TWO AND REDIRECT THEM TO STEP 1
' ***************************************************************************************************

	if (Request.Form("hid_Report") = "") then Response.Redirect ("ReportBuilder_Step1.asp")
	
	Call OpenDB()
	
	
' ***************************************************************************************************
'	 B ) 	SET ALL THE LEBELLING AND NAMING FOR THE REPORT .
' ***************************************************************************************************
	
	SaveReportID = Request.Form("hid_SaveReportID")
	ReportName = "Creating a new report..."
	SaveName = ""
	ReportDesc = ""
	
	if (SaveReportID <> "") then
		SQL = "SELECT SAVENAME, DESCRIPTION, ISGLOBAL FROM NS_REPORT_SAVE WHERE SAVEREPORTID = " & SaveReportID
		Call OpenRs(rsR, SQL)
		if NOT rsR.EOF then
			SaveName = rsR("SAVENAME")
			ReportDesc = rsR("DESCRIPTION")
			ReportName = "Amending Report - " & SaveName
		end if
		Call CloseRs(rsR)
	else
			SaveName  = Request.Form("hid_SaveTitle")
			ReportDesc = Request.Form("hid_SaveDescription")
	end if
	
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Data Interrogator</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        .Header 
        {
            vertical-align:top;
		    width: 100px;
		    background-color:#a3a3a3;
		    color:#FFFFFF;
		    font-weight:bold;
		}
        .Data 
        {
		    border-bottom:1px dotted silver;
		}
        .a{color:red}
        .b{color:green}
        .d{color:black}
    </style>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--
    var FormFields = new Array()
    FormFields[0] = "txt_ReportName|Report Name|TEXT|Y"
    FormFields[1] = "txt_ReportDesc|Report Description|TEXT|N"

    function PreviousStepNo(step) {
        document.getElementById("hid_SaveTitle").value = document.getElementById("txt_ReportName").value
        document.getElementById("hid_SaveDescription").value = document.getElementById("txt_ReportDesc").value
        document.ReportForm.action = "ReportBuilder_Step" + step + ".asp"
        document.ReportForm.target = ""
        document.ReportForm.submit()
    }


    function NextStep() {
        if (!checkForm()) return;
        document.ReportForm.action = "ServerSide/SaveReport.asp"
        document.ReportForm.submit()
    }

//-->
</script>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form target="" method="post" name="ReportForm" action="">
    <table style="height:90%; margin-left: 3px; border-top: 1px solid #000033; border-bottom: 1px solid #000033"
        cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">        
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="23">
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
                        <td>
                            &nbsp;&nbsp;<%=ReportName%>
                        </td>
                        <td align="right">
                            <strong style="font-size: 14px;">Step 6 of 6</strong>
                        </td>
                        <td width="10">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="35">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000033;
                    border-right: 1px solid #000033; border-left: 1px solid #000033">
                    <tr>
                        <td width="378">
                            <font style="font-size: 16px; color: white; background-color: #236b8e; padding-left: 10px;
                                padding-right: 10px"><b>SUMMARY</b></font>&nbsp;&nbsp;Please check your report
                            and give it a name:
                        </td>
                        <td width="87">
                        </td>
                        <td width="267" align="right">
                            <input name="BtnPreviousStep1" id="BtnPreviousStep1" type="button" class="RSLButton" onclick="PreviousStepNo(1)"
                                value=" 1 " title="Go to Step 1 : Module" style="cursor:pointer" />
                            <input name="BtnPreviousStep2" id="BtnPreviousStep2" type="button" class="RSLButton" onclick="PreviousStepNo(2)"
                                value=" 2 " title="Go to Step 2 :  Module Info" style="cursor:pointer" />
                            <input name="BtnPreviousStep3" id="BtnPreviousStep3" type="button" class="RSLButton" onclick="PreviousStepNo(3)"
                                value=" 3 " title="Go to Step 3 : Columns" style="cursor:pointer" />
                            <input name="BtnPreviousStep4" id="BtnPreviousStep4" type="button" class="RSLButton" onclick="PreviousStepNo(4)"
                                value=" 4 " title="Go to Step 4 : Filters" style="cursor:pointer" />
                            <input name="BtnPreviousStep5" id="BtnPreviousStep5" type="button" class="RSLButton" onclick="PreviousStepNo(5)"
                                value=" Previous " title="Go to Step 5 : Report Type" style="cursor:pointer" />
                            <input name="BtnNextStep" id="BtnNextStep" type="button" class="RSLButton" onclick="NextStep()" value=" Save "
                                style="background-color: #000033; color: #ffffff; cursor:pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-left: 10px; padding-right: 10px" height="100%">
                <table style="border-collapse: collapse">
                    <tr>
                        <td class="Header" style="vertical-align: middle">
                            NAME :
                        </td>
                        <td class="Data">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <input type="text" style="border: none; border-bottom: 1px solid #000033; font-family: tahoma;
                                            font-size: 12px" maxlength="50" size="50" name="txt_ReportName" id="txt_ReportName" value="<%=Server.HTMLEncode(SaveName)%>" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <img name="img_ReportName" id="img_ReportName" src="/js/FVS.gif" alt="Report Name" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="Header" style="vertical-align: top">
                            DESCRIPTION :
                        </td>
                        <td class="Data">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <textarea style="border: 1px solid #000033; font-family: tahoma; font-size: 12px"
                                            cols="90" rows="3" name="txt_ReportDesc" id="txt_ReportDesc"><%=Server.HTMLEncode(ReportDesc)%></textarea>
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <img name="img_ReportDesc" id="img_ReportDesc" src="/js/FVS.gif" alt="Report Description" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <%
				' ***************************************************************************************************
				'	 C ) 	IF REPORT IS ALREADY SAVED AND IS BEING AMENDED THEN WE BRING BACK SAVED DATA TO DIPLAY
				' ***************************************************************************************************
				
					'--------------------------------------------
					' I) PRE-SAVED REPORT NAME 
					'--------------------------------------------
					OpenDB()
					SQL = "SELECT REPORTNAME FROM NS_REPORT WHERE REPORTID = " & Request.Form("hid_Report")
					Call OpenRs(rsReport, SQL)
					if (NOT rsReport.EOF) then
						Response.Write "<tr><td class=""Header"">REPORT : </td><td class=""Data"">" & rsReport("REPORTNAME") & "</td></tr>"
					end if
					Call CloseRs(rsReport)
					
					'--------------------------------------------
					' II) PRE-SAVED REPORT TYPE (LIST,GRAPH) 
					'--------------------------------------------
					
					SQL = "SELECT GROUPED, TYPENAME FROM NS_REPORT_TYPE WHERE TYPEID = " & Request.Form("hid_ReportType")
					isGrouped = 0
					Call OpenRs(rsReport, SQL)
					if (NOT rsReport.EOF) then
						isGrouped = rsReport("GROUPED")
						Response.Write "<tr><td class=""Header"">TYPE : </td><td class=""Data"">" & rsReport("TYPENAME") & "</td></tr>"
					end if
					Call CloseRs(rsReport)
				
					Response.Write "<tr><td class=""Header"">COLUMNS : </td><td class=""Data"">" 
				
					'--------------------------------------------
					' III) DIPLSAY THE COLUMN LIST
					'--------------------------------------------
					
					'NEED TO DO IT THIS WAY SO THAT THE COLUMN ORDER IS PRESERVED
					SelectArray = Split(Request.Form("hid_SelectedList"), ",")
					TotalColumns = Ubound(SelectArray)
					if (isGrouped = 1) then TotalColumns = TotalColumns + 1
				
					SelectSQL = ""
					for i=0 to Ubound(SelectArray)
						if (i > 0) then SelectSQL = SelectSQL & " UNION ALL "
						SelectSQL = SelectSQL & "SELECT '<font color=blue>' + ISNULL(COLUMN_CODE,'') + '</font> ' + COLUMN_DISPLAY_NAME AS COLUMN_TITLE FROM NS_REPORT_COLUMN WHERE COLUMNID = " & SelectArray(i)
					next
				
					counter = 1
					Call OpenRs(rsC,SelectSQL)
					while Not rsC.EOF
						if counter = 1 then
							Response.Write rsC("COLUMN_TITLE")
						else
							Response.Write ", " & rsC("COLUMN_TITLE")
						end if
						counter = counter + 1
						rsC.moveNext
					wend
					Call CloseRs(rsC)
				
					if (isGrouped = 1) then Response.Write ", GROUPCOUNT" 
				
					Response.Write "</td></tr>"
				
					'--------------------------------------------
					' IIII) DISPLAY THE FILTERS
					'--------------------------------------------
					Response.Write "<tr><td class=""Header"">FILTERS : </td><td class=""Data"">" 
					FilterData = Request.Form("hid_FilterList")
					if (FilterData = "") then
						Response.Write "No Filters applied"
					else
						FilterArray = Split(FilterData, "[<*FILTER_BREAKER*>]")
						counter = 1
						for i=0 to Ubound(FilterArray)
							MiniArray = Split(FilterArray(i), "[*<brkr>*]")
							if (counter = 1) then
								Response.Write "WHERE (" & MiniArray(2) & ")"
							else
								Response.Write "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AND (" & MiniArray(2) & ")"
							end if
							counter = counter + 1
						next
					end if
					Response.Write "</td></tr>"
					
				'------------------------------------------------------------------------------------
				' D)  STORE THE VARIABLES THAT NEED TO BE PASSED FROM STEP TO STEP 
				'------------------------------------------------------------------------------------	
                    %>
                </table>
                <input type="hidden" name="hid_Group" id="hid_Group" value="<%=Request.Form("hid_Group")%>" />
                <input type="hidden" name="hid_Project" id="hid_Project" value="<%=Request.Form("hid_Project")%>" />
                <input type="hidden" name="hid_Report" id="hid_Report" value="<%=Request.Form("hid_Report")%>" />
                <input type="hidden" name="hid_ReportType" id="hid_ReportType" value="<%=Request.Form("hid_ReportType")%>" />
                <input type="hidden" name="hid_ColumnCount" id="hid_ColumnCount" value="<%=Request.Form("hid_ColumnCount")%>" />
                <input type="hidden" name="hid_SelectedList" id="hid_SelectedList" value="<%=Request.Form("hid_SelectedList")%>" />
                <input type="hidden" name="hid_FilterList" id="hid_FilterList" value="<%=Request.Form("hid_FilterList")%>" />
                <input type="hidden" name="hid_SaveReportID" id="hid_SaveReportID" value="<%=Request.Form("hid_SaveReportID")%>" />
                <input type="hidden" name="hid_LineDate" id="hid_LineDate" value="<%=Request.Form("hid_LineDate")%>" />
                <input type="hidden" name="hid_SaveTitle" id="hid_SaveTitle" value="<%=SaveName%>" />
                <input type="hidden" name="hid_SaveDescription" id="hid_SaveDescription" value="<%=ReportDesc%>" />
                <input type="hidden" name="CLICKEDPREVIOUS" id="CLICKEDPREVIOUS" value="1" />
            </td>
        </tr>
        <tr style="background-color: #236b8e; color: white; font-size: 14px; font-weight: bold;">
            <td height="10">
                &nbsp;&nbsp;Version
            </td>
        </tr>        
      </table>
    </form>
    <img src="/js/img/FVER.gif" width="1px" height="1px" name="FVER_Image" id="FVER_Image" style="visibility: hidden" alt="" />
    <img src="/js/img/FVEB.gif" width="1px" height="1px" name="FVEB_Image" id="FVEB_Image" style="visibility: hidden" alt="" />
    <img src="/js/img/FVS.gif" width="1px" height="1px" name="FVS_Image" id="FVS_Image" style="visibility: hidden" alt="" />
    <img src="/js/img/FVW.gif" width="1px" height="1px" name="FVW_Image" id="FVW_Image" style="visibility: hidden" alt="" />
    <img src="/js/img/FVTG.gif" width="1px" height="1px" name="FVTG_Image" id="FVTG_Image" style="visibility: hidden" alt="" />
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
