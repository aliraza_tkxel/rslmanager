<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccessControl.aspx.vb" Inherits="BHAIntranet_CustomPages_AccessControl" %>
<%@ Register TagPrefix="myControls" Namespace="CustomControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
        <link rel="stylesheet" type="text/css" runat="server"  id="emp_stylesheet" href="~/CSS/Employees/Employees.css" />
    <link rel="stylesheet" type="text/css" runat="server"  id="rsl_stylesheet" href="~/CSS/RSL.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Button cssclass="RSLButton" ID="btnSave" runat="server" Text="Save Changes" />
    <div>
 <myControls:MenuTreeView ID="tvwWebMenu" runat="server" ExpandDepth="1" ImageSet="XPFileExplorer" NodeIndent="25"  LeafNodeStyle-ChildNodesPadding="10" ShowLines="True" /> 
        </div>
    </form>
</body>
</html>
