Imports CustomControls
Partial Class BHAIntranet_CustomPages_AccessControl
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsNumeric(Request.QueryString("empid")) Then
            Throw New ArgumentException("Querystring must be numeric", "")
        End If

        If Not Page.IsPostBack Then

            ViewState("EmpID") = Request.QueryString("empid")
            Dim bllMenu As New bllMenu
            bllMenu.DataFieldID = "menuid"
            bllMenu.DataTextField = "menutitle"
            bllMenu.DataFieldParentID = "parentid"
            bllMenu.PopulateAccessControl(tvwWebMenu, True, ViewState("EmpID"))

        End If
    End Sub


    Public Function GetSelectedNodes(ByVal tvw As MenuTreeview)

        Dim strNodes As String = ""
        Dim node As New TreeNode
        For Each node In tvw.CheckedNodes
            strNodes = strNodes & node.Value & ","
        Next
        strNodes = strNodes.TrimEnd(",")
        Return strNodes
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Response.Write(GetSelectedNodes(tvwWebMenu))
        Dim bllPage As New bllPage
        bllPage.SetAccessControl(GetSelectedNodes(tvwWebMenu), ViewState("EmpID"))

        Dim strJS As String
        strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
        strJS = strJS & "window.close()" & Environment.NewLine

        strJS = strJS & "</script>" & Environment.NewLine
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close Window", strJS)
    End Sub
End Class
