<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EmployeeImage.aspx.vb" Inherits="BusinessManager_Popup_EmployeeImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Employee Image</title>
    <link rel="stylesheet" type="text/css" runat="server"  id="emp_stylesheet" href="~/CSS/Employees/Employees.css" />
    <link rel="stylesheet" type="text/css" runat="server"  id="rsl_stylesheet" href="~/CSS/RSL.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>Upload a new employee image</div>
        
        <asp:Label ID="lblValidation" runat="server" Text="" ForeColor="Red"></asp:Label><br />
        <asp:Label ID="lblFileImage" cssclass="genLabel" runat="server"  AssociatedControlID="filImage" Text="Image"></asp:Label>
        <asp:FileUpload cssclass="RSLButton" ID="filImage" runat="server" />
        <br />
        <div id="btn">
        <asp:Button cssclass="RSLButton" ID="btnUploadImage" runat="server" Text="Upload" />  
        </div>
    </div>
    </form>
</body>
</html>
