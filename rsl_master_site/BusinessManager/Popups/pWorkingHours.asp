<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	
	Dim employeeId, startdate ,mon, tue, wed, thu, fri,sat,sun,total, name , strTable, mon_history, tue_history, wed_history,thu_history,fri_history,total_history,name_history,start_history,end_history,history_record
	

	employeeId = Request("employeeId")
    strTable=""
    history_record = 1
	OpenDB()
	
    strSQL = "SELECT STARTDATE,MON,TUE,WED,THU,FRI,SAT,SUN,TOTAL,FIRSTNAME + ' ' + LASTNAME as NAME FROM E_WORKINGHOURS W LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=W.CREATEDBY WHERE W.EMPLOYEEID=" & employeeId
    Call OpenRs(rsSet, strSQL)
	
	if 	rsSet.EOF=False then
		mon = rsSet("MON")
		tue	= rsSet("TUE")
		wed = rsSet("WED")
		thu	= rsSet("THU")
		fri = rsSet("FRI")
		sat = rsSet("SAT")
		sun = rsSet("SUN")
		total = rsSet("TOTAL")
        name = rsSet("NAME")
        startdate = rsSet("STARTDATE")
	end if	
	CloseRs(rsSet)

    strSQL = " SELECT CONVERT(NVARCHAR(10),startdate,103) AS START,CONVERT(NVARCHAR(10),ENDDATE,103) AS [END], MON,TUE,WED,THU,FRI,SAT,SUN,TOTAL,ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'') as CREATEDBY,CTIMESTAMP AS CREATEDON " &_
             " FROM E_WORKINGHOURS_HISTORY WH  " &_
             " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID=WH.CREATEDBY  " &_
             " WHERE WH.EMPLOYEEID=" & employeeId & "" &_
             " AND WH.TOTAL!=0 "
    Call OpenRs(rsSet,strSQL)

   if rsSet.EOF= false then
    do until rsSet.EOF

        mon_history = rsSet("MON")
		tue_history	= rsSet("TUE")
		wed_history = rsSet("WED")
		thu_history	= rsSet("THU")
		fri_history = rsSet("FRI")
		total_history = rsSet("TOTAL")
        name_history = rsSet("CREATEDBY")
        created_on = rsSet("CREATEDON")
        start_history = rsSet("START")
        end_history = rsSet("END")
        
        strTable= strTable & "<tr><td class=""fsize"" align=""center"">" & start_history & "</td><td class=""fsize"" align=""center"">" & end_history & "</td><td class=""fsize"" align=""center"">" & mon_history & "</td><td class=""fsize"" align=""center"">" & tue_history & "</td><td class=""fsize"" align=""center"">" & wed_history & "</td><td class=""fsize"" align=""center"">" & thu_history & "</td><td class=""fsize"" align=""center"">" & fri_history & "</td><td class=""fsize"" align=""center"">" & total_history & "</td><td class=""style1"" align=""center"">" & name_history & "</td><td class=""style1"" align=""center"">" & created_on & "</td></tr>"
        rsSet.Movenext
    loop
   else
        history_record=0
   end if
	CloseRs(rsSet)
	CLoseDB()

%>
<html>
<head>
<title>ERM --> Employee Working Hours</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<style type="text/css">
.fsize
{
font-family:  Arial;
font-size: 13px;


}
    .style1
    {
        font-family: Arial;
        font-size: 13px;
        width: 105px;
    }
</style>
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	
</script> 
<body bgcolor="#FFFFFF" leftmargin="30" topmargin="20" marginwidth="0" marginheight="0" style="font-weight: 700; font-family: Arial; font-size: medium">
Current Working Pattern
<TABLE HEIGHT="100" CELLPADDING="2" CELLSPACING="0" border="2" bordercolor="gray" style="width: 700px" >
		<tr> 
		    
		    <th class="fsize" align="center">Start Date</th>
            <th class="fsize" align="center">Monday</th>
            <th class="fsize" align="center">Tuesday</th>
            <th class="fsize" align="center">Wednesday</th>
            <th class="fsize" align="center">Thursday</th>
            <th class="fsize" align="center">Friday</th>
            <th class="fsize" align="center">Saturday</th>
            <th class="fsize" align="center">Sunday</th>
            <th class="fsize" align="center">Total</th>
            <th class="style1" align="center">Created By</th>
        </tr>
        <tr> 
		    <td class="fsize" align="center"><%=startdate %></td>
            <td class="fsize" align="center"><%=mon %></td>
            <td class="fsize" align="center"><%=tue %></td>
            <td class="fsize" align="center"><%=wed %></td>
            <td class="fsize" align="center"><%=thu %></td>
            <td class="fsize" align="center"><%=fri %></td>
            <td class="fsize" align="center"><%=sat %></td>
            <td class="fsize" align="center"><%=sun %></td>
            <td class="fsize" align="center"><%=total %></td>
            <td class="style1" align="center"><%=name %></td>
        </tr>
  
</TABLE>  
<br /> <br /><br />
Previous Working Pattern(s)
<% If history_record = 1 then %>
       <TABLE HEIGHT="100" CELLPADDING="2" CELLSPACING="0" border="2" bordercolor="gray" style="width: 750px" >
		<tr> 
		      
		    <th class="fsize" align="center">Start</th>
            <th class="fsize" align="center">End</th>
            <th class="fsize" align="center">Monday</th>
            <th class="fsize" align="center">Tuesday</th>
            <th class="fsize" align="center">Wednesday</th>
            <th class="fsize" align="center">Thursday</th>
            <th class="fsize" align="center">Friday</th>
            <th class="fsize" align="center">Total</th>
            <th class="style1" align="center">Created By</th>
            <th class="style1" align="center">Created On</th>
            
        </tr> 
         <%= strTable%>
         </TABLE>
<% end if %>       
        <table height="40" WIDTH="700" border="0">

        <tr></tr>  
        <tr></tr>
        <tr>
            <td colspan="10" align="center">
                <input type="button" value="Close Window" class="RSLButton" onclick="window.close()"; />
            </td>
        </tr>
       
        </table>


</body>
</html>
