<%@  language="VBScript" codepage="65001" %>
<% 
Response.Expires = -1
Server.ScriptTimeout = 600
%>
<% Response.Buffer = true %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include file="../../freeaspupload.asp" -->
<%
	
	Dim employeeId, uploadsDirVar,salaryamendmentid
	

	employeeId = Request("empid")
    salaryamendmentid=Request("said")
     uploadsDirVar = Server.MapPath("/RoleFile/SalaryAmendments")
      
	if Request.ServerVariables("REQUEST_METHOD") = "POST" and employeeId > 0 then
     Dim strMsg 'As String
      if employeeId > 0 then
     
                Dim FSO
                Set FSO = CreateObject("Scripting.FileSystemObject")
                If NOT (FSO.FolderExists(uploadsDirVar)) Then
                    FSO.CreateFolder(uploadsDirVar)
                End If

                Dim fileName, fileSize, ks, i, fileKey, outFileName
        
                Set Upload = New FreeASPUpload
                Upload.SaveOne uploadsDirVar, 0, fileName, outFileName
                    SaveFiles = ""
                    ks = Upload.UploadedFiles.keys
                    if (UBound(ks) <> -1) then
                        SaveFiles = "<B>Files uploaded:</B> "
                        for each fileKey in Upload.UploadedFiles.keys
                            SaveFiles = SaveFiles & Upload.UploadedFiles(fileKey).FileName & " (" & Upload.UploadedFiles(fileKey).Length & "B) "
                        next
                    else
                        SaveFiles = "No file selected for upload or the file name specified in the upload form does not correspond to a valid file in the system."
                    end if 

                  If Err.number = 0 Then
                    strMsg = outFileName & " uploaded successfully."
                 
                   Set Conn = Server.CreateObject("ADODB.Connection")
                    Conn.Open(RSL_CONNECTION_STRING)

                    sExtension = FSO.GetExtensionName(outFileName)
                    ' strip the extension from file name
                    sName = Replace(outFileName, "." & sExtension, "")

                     SQLCODE = "INSERT INTO E_SalaryAmendment(EmployeeId,SalaryAmendmentFile,Status,AmendmentFileName)Values(" & employeeId & ",'" & outFileName & "',1,'" & sName & "') "  
                      Conn.Execute(SQLCODE)                       
%>
<script type="text/javascript">
    window.opener.location.reload(false);
    window.close();
</script>
<%                   
                    Else
                    strMsg = "An error occurred when uploading your file: " & Err.Description 
                  End If
               
          end if
    end if 

    if Request.ServerVariables("REQUEST_METHOD") = "POST" and salaryamendmentid > 0 then 
         
        Set Conn = Server.CreateObject("ADODB.Connection")
            Conn.Open(RSL_CONNECTION_STRING)
            SQLCODE = "UPDATE E_SalaryAmendment SET Status = 0 WHERE SalaryAmendmentId = " & salaryamendmentid                       
            Conn.Execute(SQLCODE)
        
 end if
%>
<html>
<head>
    <title>Upload Salary Amendment Letter </title>
    <script type="text/javascript">

        var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf", ".doc", ".docx", ".xls", ".xlsx"];
        function Validate(oForm) {
            var arrInputs = oForm.getElementsByTagName("input");
            for (var i = 0; i < arrInputs.length; i++) {
                var oInput = arrInputs[i];
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }

                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            return false;
                        }
                    }
                    else {
                        alert("Please select a file.");
                        return false;
                    }
                }
            }

            return true;
        }
    </script>
</head>
<body>
    <form enctype="multipart/form-data" action="SalaryAmendments.asp?empid=<%=employeeId%>"
    method="post" name="upload" onsubmit="return Validate(this);">
    <%=strMsg%>
    <div style="margin: 10px">
        Salary Amendment Letter:
        <input name="file" type="file" size="10"/>
        <input name="submit" type="submit" value="Upload" />
    </div>
    </form>
</body>
</html>
