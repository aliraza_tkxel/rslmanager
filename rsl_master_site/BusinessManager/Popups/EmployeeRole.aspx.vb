
Partial Class BusinessManager_Popups_EmployeeRole
    Inherits basepage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNumeric(Request.QueryString("empid")) Then
            Throw New ArgumentException("Querystring must be numeric", "")
        End If
        Dim intEmpID = CInt(Request.QueryString("empid"))
        ViewState("EmpID") = CInt(Request.QueryString("empid"))
    End Sub
    Protected Sub btnUploadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadFile.Click
        Try
            Dim bllEmp As New bllEmployees(ViewState("EmpID"))
            bllEmp.FileRole = filRole
            bllEmp.IsRoleMandatory = True
            bllEmp.Update()

            Dim strNewPath = "/RoleFile/" & bllEmp.RolePath

            Dim strJS As String
            strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
            strJS = strJS & "window.opener.document.getElementById('lnkRolefile').href='" & strNewPath & "'" & Environment.NewLine
            strJS = strJS & "window.opener.document.getElementById('lnkRolefile').style.display='" & "block" & "'" & Environment.NewLine
            strJS = strJS & "window.close()" & Environment.NewLine

            strJS = strJS & "</script>" & Environment.NewLine
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Change Link", strJS)


        Catch ex As Exception
            lblValidation.Text = ex.Message

        End Try
    End Sub


    Protected Sub btnDeleteFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteFile.Click
        Dim bllEmp As New bllEmployees()
        bllEmp.DeleteRoleFile(ViewState("EmpID"))

        Dim strJS As String
        strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
        strJS = strJS & "window.opener.document.getElementById('lnkRolefile').style.display='" & "none" & "'" & Environment.NewLine
        strJS = strJS & "window.close()" & Environment.NewLine

        strJS = strJS & "</script>" & Environment.NewLine
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Delete Link", strJS)
    End Sub
End Class
