
Partial Class BusinessManager_Popup_EmployeeImage
    Inherits basepage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsNumeric(Request.QueryString("empid")) Then
            Throw New ArgumentException("Querystring must be numeric", "")
        End If
        Dim intEmpID = CInt(Request.QueryString("empid"))
        ViewState("EmpID") = CInt(Request.QueryString("empid"))
 
    End Sub

    Protected Sub btnUploadImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadImage.Click
        Try
            Dim bllEmp As New bllEmployees(ViewState("EmpID"))
            bllEmp.FileImage = filImage
            bllEmp.IsImageMandatory = True
            bllEmp.Update()

            Dim strNewPath = "/EmployeeImage/" & bllEmp.ImagePath

            Dim strJS As String
            strJS = "<script language=""javascript"" type=""text/javascript"" defer=""defer"">" & Environment.NewLine
            strJS = strJS & "window.opener.document.getElementById('imgEmployee').src='" & strNewPath & "'" & Environment.NewLine
            strJS = strJS & "window.opener.document.getElementById('imgEmployee').style.display='" & "block" & "'" & Environment.NewLine
            'strJS = strJS & "window.close()" & Environment.NewLine

            strJS = strJS & "</script>" & Environment.NewLine
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Change Image", strJS)

        Catch ex As Exception
            lblValidation.Text = ex.Message

        End Try
    End Sub
End Class
