<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #Include virtual="Includes/Database/ADOVBS.INC" -->
<%
	//Get First name, surname and dob to check weather a similar user already exists in the database.
	FirstName = Replace(Request.Form("txt_FIRSTNAME"), "'", "''")
	LastName = Replace(Request.Form("txt_LASTNAME"), "'", "''")
	Dob = Replace(Request.Form("txt_DOB"), "'", "''")

	UserTitle = Request.Form("sel_TITLE")
	if (UserTitle = "" OR isNull(UserTitle)) then
		UserTitle = -1
	end if

	OpenDB()
	
	Dim rsTitle
	SQL = "SELECT DESCRIPTION FROM G_TITLE WHERE TITLEID = " & UserTitle
	Call OpenRs(rsTitle, SQL)
	
	if (NOT rsTitle.EOF) then
		UserTitleText = rsTitle("DESCRIPTION")
	else
		UserTitleText = ""
	end if
	CloseRs(rsTitle)

	Ethnicity = Request.Form("sel_ETHNICITY")
	if (Ethnicity = "" OR isNull(Ethnicity)) then
		Ethnicity = -1
	end if

	Dim rsEthnicity
	SQL = "SELECT DESCRIPTION FROM G_ETHNICITY WHERE ETHID = " & Ethnicity
	Call OpenRs(rsEthnicity, SQL)
	
	if (NOT rsEthnicity.EOF) then
		EthnicityText = rsEthnicity("DESCRIPTION")
	else
		EthnicityText = ""
	end if
	CloseRs(rsEthnicity)

	MaritalStatus = Request.Form("sel_MARITALSTATUS")
	if (MaritalStatus = "" OR isNull(MaritalStatus)) then
		MaritalStatus = -1
	end if

	Dim rsMarital
	SQL = "SELECT DESCRIPTION FROM G_MARITALSTATUS WHERE MARITALSTATUSID = " & MaritalStatus
	Call OpenRs(rsMarital, SQL)
	
	if (NOT rsMarital.EOF) then
		MaritalStatusText = rsMarital("DESCRIPTION")
	else
		MaritalStatusText = ""
	end if
	CloseRs(rsMarital)
		
	strEventQuery = "SELECT E.EMPLOYEEID, T.DESCRIPTION AS TITLE, FIRSTNAME, LASTNAME, GENDER, DOB, ET.DESCRIPTION AS ETHNICITY, M.DESCRIPTION AS MARITALSTATUS, GENDER, " &_
					"ADDRESS1, ADDRESS2, ADDRESS3, POSTALTOWN, POSTCODE, HOMETEL, HOMEEMAIL " &_
					"FROM E__EMPLOYEE E "&_
					"LEFT JOIN G_TITLE T ON E.TITLE = T.TITLEID " &_
					"LEFT JOIN G_ETHNICITY ET ON E.ETHNICITY = ET.ETHID " &_
					"LEFT JOIN G_MARITALSTATUS M ON E.MARITALSTATUS = M.MARITALSTATUSID " &_
					"LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
					"WHERE FIRSTNAME = '" & FirstName & "' AND LASTNAME = '" & LastName & "' AND DOB = '" & Dob & "'"
					
	dim mypage
	mypage = Request("page")
	if (mypage = "" or isNull(mypage)) then
		mypage = 1
	elseif (isNumeric(mypage)) then
		mypage = CInt(mypage)
	end if

	if mypage = 0 then mypage = 1 end if
	
	pagesize = 1
		
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Rs.PageSize = pagesize
	Rs.CacheSize = pagesize
	Rs.CursorLocation = adUseClient

	Rs.Open strEventQuery, Conn, adOpenForwardOnly, adLockReadOnly, adCmdText

	numpages = Rs.PageCount
	numrecs = Rs.RecordCount

' Just in case we have a bad request
	If mypage > numpages Then mypage = numpages 
	If mypage < 1 Then mypage = 1

	Dim nextpage, prevpage
	nextpage = mypage + 1
	if nextpage > numpages then 
		nextpage = numpages
	end if
	prevpage = mypage - 1
	if prevpage <= 0 then
		prevpage = 1
	end if
' This line sets the current page
	If Not Rs.EOF then
		Rs.AbsolutePage = mypage
	end if
	
	if (nextpage = 0) then nextpage = 1 end if
	if (numpages = 0) then numpages = 1 end if
%>
<html>
<head>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Business --> HR Tools --> Duplicate Employee?</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript"SRC="/js/menu.js"></SCRIPT>
<script language=javascript>
	function PageTraverse(thePage){
	thisForm.page.value = thePage;
	goDirect();
	}
	
function goDirect(){
	if (thisForm.page.value == "") {
		alert("You must enter a value before continuing...");
		return false;
		}
	if (isNaN(thisForm.page.value)){
		alert("Please enter a valid number before continuing...");
		return false;
		}
	if (thisForm.page.value <= 0){
		alert("Please enter a value greater than 0 before continuing...");
		return false;
		}		
	thisForm.action="EmployeeMatch.asp";
	thisForm.submit();
	}

function LoadEmployee(EmpID){
	thisForm.action = "NewEmployee.asp?EmployeeID=" + EmpID
	thisForm.target = ""
	thisForm.hid_ACTION.value = "";
	thisForm.submit();
	}
	
function SaveNewEmployee() {
	thisForm.action = "ServerSide/NewEmployee_svr.asp"
	thisForm.target = "";
	thisForm.hid_ACTION.value = "NEW";
	thisForm.submit();
	}

function HI(el){
	el.style.backgroundColor = "#133E71";
	el.style.color = "white"
	}	

function BYE(el){
	el.style.backgroundColor = "";
	el.style.color = "blue"	
	}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
This page shows any employees that match the employee data you just entered. If there is more than one match than you can traverse the employee list using the record pager below. If you believe the employee you are entering is the same as one from the database then you can use the previous employee to avoid duplication. If you believe that the employee you just entered does not match any of the employee records in the database then carry on creating a new employee by clicking on the respective 'GO' button.<br><br>
<%
if (NOT Rs.EOF) then
%>
            <table align=center style="border-collapse:collapse" border="1" cellpadding=1>
			  <form name="thisForm" method="post">
              <tr> 
                <td width=100px>&nbsp;</td>
			<td width=250px align=center onmouseover="HI(this)" onmouseout="BYE(this)" onclick="SaveNewEmployee()" style='cursor:hand;color:blue'><i><b>CREATE NEW EMPLOYEE</b></i></td>
			<td width=250px align=center onmouseover="HI(this)" onmouseout="BYE(this)" onclick="LoadEmployee(<%=Rs("EmployeeID")%>)" style='cursor:hand;color:blue'><i><b>LOAD PREVIOUS EMPLOYEE</b></i></td>
              </tr>
              <%
	
	RequestCollection = Array("sel_TITLE", "txt_FIRSTNAME", "txt_LASTNAME", "sel_GENDER", "txt_DOB", "sel_ETHNICITY", "sel_MARITALSTATUS", "ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN", "POSTCODE", "TELEPHONE", "EMAIL")
	DatabaseCollection = Array("TITLE", "FIRSTNAME", "LASTNAME", "GENDER", "DOB", "ETHNICITY", "MARITALSTATUS", "ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN", "POSTCODE", "HOMETEL", "HOMEEMAIL")
	ColumnNames = Array("Title", "First Name", "Last Name", "Gender", "Date of Birth", "Ethnicity", "MaritalStatus", "Address1", "Address2", "Address3", "Postal Town", "Postcode", "Telephone", "Email")
	ColumnType = Array("2", "1", "1", "1", "1", "2", "2", "4", "4", "4", "4", "4", "4", "4")

	FriendlyText = Array(UserTitleText, "", "", "", "", EthnicityText, MaritalStatusText, "", "", "", "", "", "", "")

	HiddenInputs = ""

	for i=0 to Ubound(RequestCollection)
		DatabaseValue = Rs(DatabaseCollection(i))
		if DatabaseValue = "" or isNull(DatabaseValue) then
			DatabaseValue = "---"
		end if
		if ColumnType(i) = "1" then
			Response.Write "<TR><TD>&nbsp;" & ColumnNames(i) & "&nbsp;:</TD><td bgcolor=beige nowrap>&nbsp;<input type=text class=""textbox200"" style=""width:246px;background-color:beige;border:none"" name=""" & RequestCollection(i) & """ value=""" & Request.Form(RequestCollection(i))& """ READONLY></TD><TD>&nbsp;" & DatabaseValue & "</TD></TR>"
		elseif ColumnType(i) = "2" then
			Response.Write "<TR><TD>&nbsp;" & ColumnNames(i) & "&nbsp;:</TD><td bgcolor=beige nowrap>&nbsp;<input type=text class=""textbox200"" style=""width:246px;background-color:beige;border:none"" name=""DisplayInput"" value=""" & FriendlyText(i) & """ READONLY><input type=hidden name=""" & RequestCollection(i) & """ value=""" & Request.Form(RequestCollection(i))& """></TD><TD>&nbsp;" & DatabaseValue & "</TD></TR>"		
		elseif ColumnType(i) = "4" then
			Response.Write "<TR><TD>4&nbsp;" & ColumnNames(i) & "&nbsp;:</TD><td bgcolor=#DDDDDD nowrap>&nbsp;<input type=text class=""textbox200"" style=""width:246px;background-color:#DDDDDD;border:none"" READONLY value=""Not Applicable""></TD><TD>&nbsp;" & DatabaseValue & "</TD></TR>"		
		else
			HiddenInputs = HiddenInputs & "<input type=hidden name=""" & RequestCollection(i) & """ value=""" & Request.Form(RequestCollection(i))& """>"			
		end if
	next
	
%>
	<tr>
	<td align=right colspan=2><input type=button class="RSLButton" value="  GO  " onclick="SaveNewEmployee()"></td>
	<td align=right><input type=button class="RSLButton" value="  GO  " onclick="LoadEmployee(<%=Rs("EmployeeID")%>)"></td>	
	</tr>
              
	<tr style='background-color:white' ><td colspan=2></td>
		<td align=center>
		<table ><tfoot><tr>
            <td align=right>
				<a href="Javascript:PageTraverse(1)"><b><font color=green>FIRST</font></b></a> 
                <a href="Javascript:PageTraverse(<%=prevpage%>)"><b><font color=green>PREV</font></b></a> 
                Record <%=mypage%> of <%=numpages%>. 
				<a href="Javascript:PageTraverse(<%=nextpage%>)"><b><font color=green>NEXT</font></b></a> 
				<a href="Javascript:PageTraverse(<%=numpages%>)"><b><font color=green>LAST</font></b></a>
			    <input type="hidden" name="page">
				<input type="hidden" name="EmployeeID">
			    <input type="hidden" name="hid_ACTION">
				<%=HiddenInputs%>					  
		   </td>
	    </tr></tfoot></table>
		</td>
	</tr>
<%	
	Rs.close
	Set Rs = Nothing
else	
%>
	  <TR>
		<td colspan=3 align=center>No Customers Found</td>
	  </TR>
<%
end if
%>
	</form>
</table>

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
