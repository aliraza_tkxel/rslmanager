<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	'Benefit select
	Dim lstbenefittype

	Dim rsLoader, ACTION,strTableButtons

	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
	If (EmployeeID = "") Then EmployeeID = -1 End If

	Call OpenDB()

	SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID

	Call OpenRs(rsName, SQL)

	If NOT rsName.EOF Then
		l_lastname = rsName("LASTNAME")
		l_firstname = rsName("FIRSTNAME")
		FullName = "<font color=blue>" & l_firstname & " " & l_lastname & "</font>"	
	End If
	Call CloseRs(rsName)

	SQL = "SELECT * FROM E_BENEFITS WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsLoader, SQL)

	If (NOT rsLoader.EOF) Then

		' tbl E_BENEFITS
		'Pension
		l_memnumber 	= rsLoader("MEMNUMBER")
		l_scheme 		= rsLoader("SCHEME")
		l_schemenumber 	= rsLoader("SCHEMENUMBER")
		l_salarypercent = FormatNumber(IsNullNumber(rsLoader("SALARYPERCENT")),0,-1,0,0)
		l_employeecontribution 	= FormatNumber(IsNullNumber(rsLoader("EMPLOYEECONTRIBUTION")),2,-1,0,0)
		l_employercontribution 	= FormatNumber(IsNullNumber(rsLoader("EMPLOYERCONTRIBUTION")),2,-1,0,0)
		l_avc 			= FormatNumber(IsNullNumber(rsLoader("AVC")),2,-1,0,0)
		l_contractedout = rsLoader("CONTRACTEDOUT")
		'General
		l_professionalfees 		= FormatNumber(IsNullNumber(rsLoader("PROFESSIONALFEES")),2,-1,0,0)
		l_telephoneallowance 	= FormatNumber(IsNullNumber(rsLoader("TELEPHONEALLOWANCE")),2,-1,0,0)
		l_firstaidallowance 	= FormatNumber(IsNullNumber(rsLoader("FIRSTAIDALLOWANCE")),2,-1,0,0)
		l_calloutallowance 		= FormatNumber(IsNullNumber(rsLoader("CALLOUTALLOWANCE")),2,-1,0,0)
		'Company Car
		l_carmake 				= rsLoader("CARMAKE")
		l_model 				= rsLoader("MODEL")
		l_listprice 			= FormatNumber(IsNullNumber(rsLoader("LISTPRICE")),2,-1,0,0)
		l_conthirecharge 		= FormatNumber(IsNullNumber(rsLoader("CONTHIRECHARGE")),2,-1,0,0)
		l_empcontribution 		= FormatNumber(IsNullNumber(rsLoader("EMPCONTRIBUTION")),2,-1,0,0)
		l_excesscontribution 	= FormatNumber(IsNullNumber(rsLoader("EXCESSCONTRIBUTION")),2,-1,0,0)
		l_carstartdate		    = rsLoader("CARSTARTDATE")
		l_co2emissions 		    = FormatNumber(IsNullNumber(rsLoader("CO2EMISSIONS")),2,-1,0,0)
		l_fuel 					= rsLoader("FUEL")
		l_compempcontribution 	= rsLoader("COMPEMPCONTRIBUTION")
		'Car Essential User
		l_carallowance 			= FormatNumber(IsNullNumber(rsLoader("CARALLOWANCE")),2,-1,0,0)
		l_carloanstartdate 		= rsLoader("CARLOANSTARTDATE")
		l_enginesize 			= FormatNumber(IsNullNumber(rsLoader("ENGINESIZE")),0,-1,0,0)
		l_loaninformation 		= rsLoader("LOANINFORMATION")
		l_value 				= FormatNumber(IsNullNumber(rsLoader("VALUE")),2,-1,0,0)
		l_term 					= FormatNumber(IsNullNumber(rsLoader("TERM")),0,-1,0,0)
		l_monthlyrepay 			= FormatNumber(IsNullNumber(rsLoader("MONTHLYREPAY")),2,-1,0,0)
		'Accomodation
		l_accomodationrent 		= FormatNumber(IsNullNumber(rsLoader("ACCOMODATIONRENT")),2,-1,0,0)
		l_counciltax 			= FormatNumber(IsNullNumber(rsLoader("COUNCILTAX")),2,-1,0,0)
		l_heating 				= FormatNumber(IsNullNumber(rsLoader("HEATING")),2,-1,0,0)
		l_linerental 			= FormatNumber(IsNullNumber(rsLoader("LINERENTAL")),2,-1,0,0)
		'Medical Insurance
		l_medicalorg 			= rsLoader("MEDICALORGID")
		l_taxablebenefit 		= FormatNumber(IsNullNumber(rsLoader("TAXABLEBENEFIT")),2,-1,0,0)
		l_annualpremium 		= FormatNumber(IsNullNumber(rsLoader("ANNUALPREMIUM")),2,-1,0,0)
		l_additionalmembers 	= rsLoader("ADDITIONALMEMBERS")
		l_additionalmembers2 	= rsLoader("ADDITIONALMEMBERS2")
		l_additionalmembers3 	= rsLoader("ADDITIONALMEMBERS3")
		l_additionalmembers4 	= rsLoader("ADDITIONALMEMBERS4")
		l_additionalmembers5 	= rsLoader("ADDITIONALMEMBERS5")
		l_drivinglicenceno 		= rsLoader("DRIVINGLICENCENO")
		l_motcertno 			= rsLoader("MOTCERTNO")
		l_insurance 			= rsLoader("INSURANCENO")
		l_insurancerenewaldate 	= rsLoader("INSURANCERENEWALDATE")
		l_groupschemeref        = rsLoader("GROUPSCHEMEREF")
		l_membershipno          = rsLoader("MEMBERSHIPNO")
		ACTION = "AMEND"

	End If

	Call CloseRs(rsLoader)
	Call BuildSelect(lstmedicalorg, "sel_MEDICALORGID", "E_MEDICALORG", "MEDICALORGID, MEDICALORGNAME", "MEDICALORGNAME", "Please Select", l_medicalorg, NULL, "textbox200", "" )
	Call BuildSelect(lstscheme, "sel_SCHEME", "G_SCHEME", "SCHEMEID, SCHEMEDESC", "SCHEMEDESC", "Please Select", l_scheme, NULL, "textbox200", " tabindex=2 " )
	Call BuildSelect(lstbenefittype, "sel_BENEFITTYPE", "E_BENTYPE", "BENEFITTYPEID, BENEFITTYPEDESC", "BENEFITTYPEDESC", "Please Select", l_benefittype, NULL, "textbox200", " onchange='OpenDiv()'" )
	

     Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED @EmployeeId = " & EmployeeID
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)
  

    Call CloseDB()

	If ACTION = "NEW" Then
		strButtonText 	= "SAVE"
		FormFunction= "SaveForm"
	Else
		strButtonText 	= "AMEND"
		FormFunction= "AmendForm"
	End If

	strTableButtons = "<tr>"&_
						"<td colspan=2>"&_
							"<table width='100%' border='0'>"&_
								"<tr> "&_
									"<td valign='top' align='right' nowrap='nowrap' width='100%'>"&_ 
										"<input type='button' class='RSLButton' value=' "&strButtonText&" '  title=' "&strButtonText&" ' onclick='{CorrectValidationFields();"&FormFunction&"()}' style='display:block;width:70px' />"&_
									"</td>"&_
									"<td nowrap='nowrap' align='right'>"&_
										"<input type='button' class='RSLButton' style='width:70px' value=' RESET ' title='RESET' onclick='ResetValues()' />"&_
									"</td>"&_
								"</tr>"&_
							"</table>"&_
						"</td>"&_
					"</tr>"
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidationExtras.js"></script>
    <script type="text/javascript" language="JavaScript">

        function OpenDiv() {
            DisplayDiv = document.getElementById("sel_BENEFITTYPE").value
            PreviousDiv = document.getElementById("hid_PreviousDiv").value
            var arrBenefitSection = new Array("Pension", "CompanyCar", "Accomodation", "General", "BUPA", "Essential_User");

            for (intDiv = 0; intDiv < (arrBenefitSection.length); intDiv++) {
                if ((intDiv) == DisplayDiv - 1)
                    document.getElementById("div_" + arrBenefitSection[intDiv] + "").style.display = "block";
                else
                    document.getElementById("div_" + arrBenefitSection[intDiv] + "").style.display = "none";
            }
            document.getElementById("hid_PreviousDiv").value = DisplayDiv
        }


        function ToggleCarLoan() {
            if (document.getElementsByName("rdo_LoanInformation")[0].checked == true)
                document.getElementById("tby_LoanInformation").style.display = "block"
            else {
                document.getElementById("txt_VALUE").value = ""
                document.getElementById("txt_TERM").value = ""
                document.getElementById("txt_MONTHLYREPAY").value = ""
                document.getElementById("tby_LoanInformation").style.display = "none"
            }
        }

        function ResetValues(BenefitType) {
            BenefitType = document.getElementById("sel_BENEFITTYPE").value
            switch (BenefitType) {
                case "1":
                    //Pension//
                    document.getElementById("txt_MEMNUMBER").value = ""
                    document.getElementById("sel_SCHEME").value = ""
                    document.getElementById("sel_SCHEMENUMBER").value = ""
                    document.getElementById("txt_SALARYPERCENT").value = ""
                    document.getElementById("txt_EMPLOYEECONTRIBUTION").value = ""
                    document.getElementById("txt_EMPLOYERCONTRIBUTION").value = ""
                    document.getElementById("txt_AVC").value = ""
                    document.getElementById("rdo_CONTRACTEDOUT").value = ""
                    break
                case "2":
                    //Company Car//
                    document.getElementById("txt_CARMAKE").value = ""
                    document.getElementById("txt_MODEL").value = ""
                    document.getElementById("txt_LISTPRICE").value = ""
                    document.getElementById("txt_CONTHIRECHARGE").value = ""
                    document.getElementById("txt_EMPCONTRIBUTION").value = ""
                    document.getElementById("txt_EXCESSCONTRIBUTION").value = ""
                    document.getElementById("txt_CARSTARTDATE").value = ""
                    document.getElementById("txt_CO2EMISSIONS").value = ""
                    document.getElementById("sel_FUEL").value = ""
                    document.getElementById("txt_COMPEMPCONTRIBUTION").value = ""
                    break
                case "3":
                    //Accomodation//
                    //alert("accom")
                    document.getElementById("txt_ACCOMODATIONRENT").value = ""
                    document.getElementById("txt_COUNCILTAX").value = ""
                    document.getElementById("txt_HEATING").value = ""
                    document.getElementById("txt_LINERENTAL").value = ""
                    //alert(FormFields)
                    break
                case "4":
                    //General//
                    document.getElementById("txt_PROFESSIONALFEES").value = ""
                    document.getElementById("txt_TELEPHONEALLOWANCE").value = ""
                    document.getElementById("txt_FIRSTAIDALLOWANCE").value = ""
                    document.getElementById("txt_CALLOUTALLOWANCE").value = ""
                    break
                case "5":
                    //Medical Insurance//
                    document.getElementById("sel_MEDICALORGID").value = ""
                    document.getElementById("txt_TAXABLEBENEFIT").value = ""
                    document.getElementById("txt_ANNUALPREMIUM").value = ""
                    document.getElementById("txt_ADDITIONALMEMBERS").value = ""
                    document.getElementById("txt_ADDITIONALMEMBERS2").value = ""
                    document.getElementById("txt_ADDITIONALMEMBERS3").value = ""
                    document.getElementById("txt_ADDITIONALMEMBERS4").value = ""
                    document.getElementById("txt_ADDITIONALMEMBERS5").value = ""
                    document.getElementById("txt_GROUPSCHEMEREF").value = ""
                    document.getElementById("txt_MEMBERSHIPNO").value = ""
                    document.getElementById("txt_DRIVINGLICENCENO").value = ""
                    document.getElementById("txt_MOTCERTNO").value = ""
                    document.getElementById("txt_INSURANCENO").value = ""
                    document.getElementById("txt_INSURANCERENEWALDATE").value = ""
                    break
                case "6":
                    //Essential User//
                    document.getElementById("txt_CARLOANSTARTDATE").value = ""
                    document.getElementById("txt_ENGINESIZE").value = ""
                    document.getElementById("txt_CARALLOWANCE").value = ""
                    if (document.getElementsByName("rdo_LoanInformation")[0].checked == true) {
                        document.getElementsByName("rdo_LoanInformation")[1].checked = true
                        document.getElementById("txt_VALUE").value = ""
                        document.getElementById("txt_TERM").value = ""
                        document.getElementById("txt_MONTHLYREPAY").value = ""
                        ToggleCarLoan()
                    }
                    break
            }
        }

        var FormFields = new Array();

        function CorrectValidationFields() {

            //Retrieve value from Benefit Type and validate fields according to value received.
            var DisplayDiv = document.getElementById("sel_BENEFITTYPE").value

            FormFields = new Array();

            switch (DisplayDiv) {
                case "1":
                    //Pension//
                    FormFields[0] = "txt_MEMNUMBER|Member Number|TEXT|N"
                    FormFields[1] = "sel_SCHEME|Scheme|SELECT|Y"
                    FormFields[2] = "txt_SCHEMENUMBER|Scheme Number|TEXT|N"
                    FormFields[3] = "txt_SALARYPERCENT|Salary Percentage|FLOAT|N"
                    FormFields[4] = "txt_EMPLOYEECONTRIBUTION|Employee Contribution|CURRENCY|N"
                    FormFields[5] = "txt_EMPLOYERCONTRIBUTION|Employer Contribution|CURRENCY|N"
                    FormFields[6] = "txt_AVC|AVC|CURRENCY|N"
                    FormFields[7] = "rdo_CONTRACTEDOUT|Contracted Out|RADIO|Y"
                    break
                case "2":
                    //Company Car//
                    FormFields[0] = "txt_CARMAKE|Car Make|TEXT|N"
                    FormFields[1] = "txt_MODEL|Car Model|TEXT|N"
                    FormFields[2] = "txt_LISTPRICE|List Price|CURRENCY|N"
                    FormFields[3] = "txt_CONTHIRECHARGE|Contract Hire Charge|CURRENCY|N"
                    FormFields[4] = "txt_EMPCONTRIBUTION|Employers Contribution|CURRENCY|N"
                    FormFields[5] = "txt_EXCESSCONTRIBUTION|Excess Contribution|CURRENCY|N"
                    // new fileds 5th oct 2004
                    FormFields[6] = "txt_CARSTARTDATE|Start Date|DATE|N"
                    FormFields[7] = "txt_CO2EMISSIONS|CO2 Emissions|FLOAT|N"
                    FormFields[8] = "sel_FUEL|Fuel|TEXT|N"
                    FormFields[9] = "txt_COMPEMPCONTRIBUTION|Employee Contribution|INTEGER|N"
                    FormFields[10] = "txt_DRIVINGLICENCENO|Driving Licence Number|TEXT|N"
                    FormFields[11] = "txt_MOTCERTNO|MOT Certificate Number|TEXT|N"
                    FormFields[12] = "txt_INSURANCENO|Insurance Number|TEXT|N"
                    FormFields[13] = "txt_INSURANCERENEWALDATE|Insurance Renewal Date|DATE|N"
                    break
                case "3":
                    //Accomodation//
                    FormFields[0] = "txt_ACCOMODATIONRENT|Accomodation Rent|CURRENCY|N"
                    FormFields[1] = "txt_COUNCILTAX|Council Tax|CURRENCY|N"
                    FormFields[2] = "txt_HEATING|Heating|CURRENCY|N"
                    FormFields[3] = "txt_LINERENTAL|Line Rental|CURRENCY|N"
                    break
                case "4":
                    //General//	
                    FormFields[0] = "txt_PROFESSIONALFEES|Professional Fees|CURRENCY|N"
                    FormFields[1] = "txt_TELEPHONEALLOWANCE|Telephone Allowance|CURRENCY|N"
                    FormFields[2] = "txt_FIRSTAIDALLOWANCE|First Aider Allowance|CURRENCY|N"
                    FormFields[3] = "txt_CALLOUTALLOWANCE|Call Out Allowance|CURRENCY|N"
                    break
                case "5":
                    //Medical Insurance//
                    FormFields[0] = "sel_MEDICALORGID|Medical Organisation|INTEGER|N"
                    FormFields[1] = "txt_TAXABLEBENEFIT|Taxable Benefit|CURRENCY|N"
                    FormFields[2] = "txt_ANNUALPREMIUM|Annual Premium|CURRENCY|N"
                    FormFields[3] = "txt_ADDITIONALMEMBERS|1st Additional Member|TEXT|N"
                    FormFields[4] = "txt_ADDITIONALMEMBERS2|2nd Additional Member|TEXT|N"
                    FormFields[5] = "txt_ADDITIONALMEMBERS3|3rd Additional Member|TEXT|N"
                    FormFields[6] = "txt_ADDITIONALMEMBERS4|4th Additional Member|TEXT|N"
                    FormFields[7] = "txt_ADDITIONALMEMBERS5|5th Additional Member|TEXT|N"
                    FormFields[7] = "txt_GROUPSCHEMEREF|Group Scheme Ref|TEXT|N"
                    FormFields[7] = "txt_MEMBERSHIPNO|Membership No|TEXT|N"
                    break
                case "6":
                    //Essential User//
                    FormFields[0] = "txt_ENGINESIZE|Engine Size|INTEGER|N"
                    FormFields[1] = "txt_CARALLOWANCE|Allowance|CURRENCY|N"
                    if (document.getElementsByName("rdo_LoanInformation")[0].checked == true) {
                        FormFields[2] = "txt_CARLOANSTARTDATE|Car Loan Start Date|DATE|N"
                        FormFields[3] = "txt_VALUE|Value �|CURRENCY|N"
                        FormFields[4] = "txt_TERM|Term|INTEGER|N"
                        FormFields[5] = "txt_MONTHLYREPAY|Monthly Repayments|CURRENCY|N"
                    }
                    break
                default:
                    FormFields[0] = "sel_BENEFITTYPE|Benefit Type|SELECT|Y"
                    break

            }
            //alert(FormFields)
        }

        function NotANumber(TextBox) {

            TextBoxValue = document.getElementById("" + TextBox + "").value

            if (isNaN(TextBoxValue) == true) {
                alert("Must be a numeric value")
                document.getElementById("" + TextBox + "").value = ""
                return false
            }
        }

        //	
        function CalculateMonthRepay() {
            document.getElementById("txt_MONTHLYREPAY").value = "";
            TotalValue = document.getElementById("txt_VALUE").value;
            Term = document.getElementById("txt_TERM").value;

            //if(NotANumber("txt_VALUE") == false)
            //	return false;
            //if(NotANumber("txt_TERM") == false)
            //	return false;

            if ((TotalValue != "") && (Term != "")) {
                SumResult = TotalValue / Term;
                if (isNaN(SumResult) == true)
                    document.getElementById("txt_MONTHLYREPAY").value = "Enter Integer Values"
                else
                    document.getElementById("txt_MONTHLYREPAY").value = FormatCurrency(SumResult, 2)
            }
            else
                document.getElementById("txt_MONTHLYREPAY").value = "";
        }

        function LoadForm(ID) {
            document.getElementById("hid_Action").value = "LOAD"
            document.getElementById("hid_WARRANTYID").value = ID
            document.RSLFORM.action = "ServerSide/Benefits_svr.asp"
            document.RSLFORM.target = "frm_ben"
            document.RSLFORM.submit()
        }


        function SaveForm() {
            if (!checkForm()) return false
            document.getElementById("hid_Action").value = "SAVE"
            document.RSLFORM.action = "ServerSide/Benefits_svr.asp"
            document.RSLFORM.target = "frm_ben"
            document.RSLFORM.submit()
        }

        function SetButtons(Code) {
            if (Code == "AMEND") {
                document.getElementById("AddButton").style.display = "none"
                document.getElementById("AmendButton").style.display = "block"
            }
            else {
                document.getElementById("AddButton").style.display = "block"
                document.getElementById("AmendButton").style.display = "none"
            }
        }

        function AmendForm() {
            document.getElementById("hid_Action").value = "AMEND"
            if (!checkForm()) return false;
            document.RSLFORM.action = "ServerSide/Benefits_svr.asp"
            document.RSLFORM.target = "frm_ben"
            document.RSLFORM.submit()
        }

    </script>
</head>
<body onload="initSwipeMenu(2);ToggleCarLoan()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img name="E_PD" id="E_PD" src="Images/1-closed.gif" width="115" height="20" border="0"
                        alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" id="E_C" src="images/2-closed.gif" width="74" height="20" border="0"
                        alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" id="E_SQ" src="images/3-closed.gif" width="94" height="20" border="0"
                        alt="Qualifications" /></a>
            </td>
            <td rowspan="2" style="display: none">
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" id="E_PE" src="images/4-closed.gif" width="86" height="20" border="0"
                        alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" id="E_DD" src="Images/5-closed.gif" width="70" height="20" border="0"
                        alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" id="E_JD" src="Images/6-closed.gif" width="52" height="20" border="0"
                        alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" id="E_B" src="Images/7-open.gif" width="82" height="20" border="0"
                        alt="Benefits" /></a>
            </td>
            <td width="265" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <table cellspacing="2" style="height: 330px; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">
        <tr>
            <td width="30%">
                Benefit Type
            </td>
            <td>
                <%=lstbenefittype%>
                <img src="/js/FVS.gif" name="img_BENEFITTYPE" width="15px" height="15px" border="0"
                    alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top: 1px solid #133E71;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <div id="div_Pension" style='display: none'>
                    <table width='100%'>
                        <tr>
                            <td nowrap="nowrap" colspan="2">
                                <b>PENSION</b>
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Membership Number:
                            </td>
                            <td>
                                <input type="text" name="txt_MEMNUMBER" id="txt_MEMNUMBER" class="textbox200" maxlength="20"
                                    value="<%=l_memnumber%>" />
                                <img src="/js/FVS.gif" name="img_MEMNUMBER" id="img_MEMNUMBER" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Scheme Name:
                            </td>
                            <td>
                                <%=lstscheme%><img src="/js/FVS.gif" name="img_SCHEME" id="img_SCHEME" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Scheme Number:
                            </td>
                            <td>
                                <input type="text" name="txt_SCHEMENUMBER" id="txt_SCHEMENUMBER" class="textbox200"
                                    maxlength="20" value="<%=l_schemenumber%>" />
                                <img src="/js/FVS.gif" name="img_SCHEMENUMBER" id="img_SCHEMENUMBER" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Salary %:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_SALARYPERCENT" id="txt_SALARYPERCENT"
                                    maxlength="10" value="<%=l_salarypercent%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_SALARYPERCENT" id="img_SALARYPERCENT" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Employee Contribution
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_EMPLOYEECONTRIBUTION" id="txt_EMPLOYEECONTRIBUTION"
                                    maxlength="10" value="<%=l_employeecontribution%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_EMPLOYEECONTRIBUTION" id="img_EMPLOYEECONTRIBUTION"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Employer Contribution
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_EMPLOYERCONTRIBUTION" id="txt_EMPLOYERCONTRIBUTION"
                                    maxlength="10" value="<%=l_employercontribution%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_EMPLOYERCONTRIBUTION" id="img_EMPLOYERCONTRIBUTION"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                AVC &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_AVC" id="txt_AVC" maxlength="10"
                                    value="<%=l_avc%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_AVC" id="img_AVC" width="15px" height="15px" border="0"
                                    alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Contracted Out:
                            </td>
                            <td>
                                <% 
					yesChecked = ""
					noChecked = ""
					if ACTION = "AMEND" then
						if (l_contractedout = 1) then
							yesChecked = "CHECKED"
							noChecked = ""
						elseif (l_contractedout = 0) then		
							yesChecked = ""
							noChecked = "CHECKED"
						end if
					end if
                                %>
                                <input type="radio" name="rdo_CONTRACTEDOUT" value="1" <%=yesChecked%> tabindex="1" />
                                YES
                                <input type="radio" name="rdo_CONTRACTEDOUT" value="0" <%=noChecked%> tabindex="1" />
                                NO
                                <img src="/js/FVS.gif" name="img_CONTRACTEDOUT" id="img_CONTRACTEDOUT" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_CompanyCar" style='display: none'>
                    <table width='100%'>
                        <tr>
                            <td colspan="2">
                                <b>COMPANY CAR</b>
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Start Date:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CARSTARTDATE" id="txt_CARSTARTDATE"
                                    maxlength="50" value="<%=l_carstartdate%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_CARSTARTDATE" id="img_CARSTARTDATE" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Make:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CARMAKE" id="txt_CARMAKE" maxlength="50"
                                    value="<%=l_carmake%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_CARMAKE" id="img_CARMAKE" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Model:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_MODEL" id="txt_MODEL" maxlength="50"
                                    value="<%=l_model%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_MODEL" id="img_MODEL" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                List Price &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_LISTPRICE" id="txt_LISTPRICE" maxlength="10"
                                    value="<%=l_listprice%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_LISTPRICE" id="img_LISTPRICE" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contract Hire Charge &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CONTHIRECHARGE" id="txt_CONTHIRECHARGE"
                                    maxlength="10" value="<%=l_conthirecharge%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_CONTHIRECHARGE" id="img_CONTHIRECHARGE" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Employers Contribution &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_EMPCONTRIBUTION" id="txt_EMPCONTRIBUTION"
                                    maxlength="10" value="<%=l_empcontribution%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_EMPCONTRIBUTION" id="img_EMPCONTRIBUTION" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Excess &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_EXCESSCONTRIBUTION" id="txt_EXCESSCONTRIBUTION"
                                    maxlength="10" value="<%=l_excesscontribution%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_EXCESSCONTRIBUTION" id="img_EXCESSCONTRIBUTION"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                CO2 Emissions :
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CO2EMISSIONS" id="txt_CO2EMISSIONS"
                                    maxlength="10" value="<%=l_co2emissions%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_CO2EMISSIONS" id="img_CO2EMISSIONS" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fuel:
                            </td>
                            <td>
                                <select name="sel_FUEL" id="sel_FUEL" class="textbox200">
                                    <option value="">Please select</option>
                                    <option value="Deisel" <%if l_fuel = "Deisel" then rw "selected" end if%>>Deisel</option>
                                    <option value="Petrol" <%if l_fuel = "Petrol" then rw "selected" end if%>>Petrol</option>
                                    <option value="LPG" <%if l_fuel = "LPG" then rw "selected" end if%>>LPG</option>
                                </select>
                                <img src="/js/FVS.gif" name="img_FUEL" id="img_FUEL" width="15px" height="15px" border="0"
                                    alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Employees Contribution &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_COMPEMPCONTRIBUTION" id="txt_COMPEMPCONTRIBUTION"
                                    maxlength="10" value="<%=l_compempcontribution%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_COMPEMPCONTRIBUTION" id="img_COMPEMPCONTRIBUTION"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Driving Licence Number :
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_DRIVINGLICENCENO" id="txt_DRIVINGLICENCENO"
                                    maxlength="10" value="<%=l_drivinglicenceno%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_DRIVINGLICENCENO" id="img_DRIVINGLICENCENO" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                MOT certificate Number :
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_MOTCERTNO" id="txt_MOTCERTNO" maxlength="10"
                                    value="<%=l_motcertno%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_MOTCERTNO" id="img_MOTCERTNO" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Policy Number :
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_INSURANCENO" id="txt_INSURANCENO"
                                    maxlength="10" value="<%=l_insuranceno%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_INSURANCENO" id="img_INSURANCENO" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Policy Renewal Date :
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_INSURANCERENEWALDATE" id="txt_INSURANCERENEWALDATE"
                                    maxlength="10" value="<%=l_insurancerenewaldate%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_INSURANCERENEWALDATE" id="img_INSURANCERENEWALDATE"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_General" style='display: none'>
                    <table width='100%'>
                        <tr>
                            <td nowrap="nowrap" colspan="2">
                                <b>GENERAL</b>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width='30%'>
                                Professional Fees &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_PROFESSIONALFEES" id="txt_PROFESSIONALFEES"
                                    maxlength="10" value="<%=l_professionalfees%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_PROFESSIONALFEES" id="img_PROFESSIONALFEES" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Telephone Allowance &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_TELEPHONEALLOWANCE" id="txt_TELEPHONEALLOWANCE"
                                    maxlength="10" value="<%=l_telephoneallowance%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_TELEPHONEALLOWANCE" id="img_TELEPHONEALLOWANCE"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                First Aider Allowance &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_FIRSTAIDALLOWANCE" id="txt_FIRSTAIDALLOWANCE"
                                    maxlength="10" value="<%=l_firstaidallowance%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_FIRSTAIDALLOWANCE" id="img_FIRSTAIDALLOWANCE" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Call Out Allowance &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CALLOUTALLOWANCE" id="txt_CALLOUTALLOWANCE"
                                    maxlength="10" value="<%=l_calloutallowance%>" tabindex="1" />
                                <img src="/js/FVS.gif" name="img_CALLOUTALLOWANCE" id="img_CALLOUTALLOWANCE" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_Essential_User" style='display: none'>
                    <table width='100%' border="0">
                        <tr>
                            <td colspan="2">
                                <b>CAR ESSENTIAL USER</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Engine Size:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ENGINESIZE" id="txt_ENGINESIZE" maxlength="10"
                                    value="<%=l_enginesize%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ENGINESIZE" id="img_ENGINESIZE" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Car Allowance &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_CARALLOWANCE" id="txt_CARALLOWANCE"
                                    maxlength="10" value="<%=l_carallowance%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_CARALLOWANCE" id="img_CARALLOWANCE" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Car Loan
                            </td>
                            <td>
                                <% 
						yesChecked = ""
						noChecked = "CHECKED"
						if ACTION = "AMEND" then
							if (l_loaninformation = 1) then
								yesChecked = "CHECKED"
								noChecked = ""
							end if
						end if
                                %>
                                <input type="radio" name="rdo_LoanInformation" value="1" <%=yesChecked%> onclick="ToggleCarLoan()"
                                    tabindex="1" />
                                YES
                                <input type="radio" name="rdo_LoanInformation" value="0" <%=noChecked%> onclick="ToggleCarLoan()"
                                    tabindex="1" />
                                NO
                                <img src="/js/FVS.gif" name="img_LoanInformation" id="img_LoanInformation" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tbody id="tby_LoanInformation" style="display: none">
                            <tr>
                                <td>
                                    Car Loan Start Date:
                                </td>
                                <td>
                                    <input type="text" class="textbox200" name="txt_CARLOANSTARTDATE" id="txt_CARLOANSTARTDATE"
                                        maxlength="10" value="<%=l_carloanstartdate%>" tabindex="2" />
                                    <img src="/js/FVS.gif" name="img_CARLOANSTARTDATE" id="img_CARLOANSTARTDATE" width="15px"
                                        height="15px" border="0" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Loan Value &pound;:
                                </td>
                                <td>
                                    <input type="text" class="textbox200" name="txt_VALUE" id="txt_VALUE" maxlength="10"
                                        value="<%=l_value%>" tabindex="2" onkeyup="CalculateMonthRepay()" />
                                    <img src="/js/FVS.gif" name="img_VALUE" id="img_VALUE" width="15px" height="15px"
                                        border="0" alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Term (months):
                                </td>
                                <td>
                                    <input type="text" class="textbox200" name="txt_TERM" id="txt_TERM" maxlength="10"
                                        value="<%=l_term%>" tabindex="2" onkeyup="CalculateMonthRepay()" />
                                    <img src="/js/FVS.gif" name="img_TERM" id="img_TERM" width="15px" height="15px" border="0"
                                        alt="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monthly Repayments &pound;
                                </td>
                                <td>
                                    <input type="text" class="textbox200" name="txt_MONTHLYREPAY" id="txt_MONTHLYREPAY"
                                        maxlength="10" value="<%=l_monthlyrepay%>" readonly="readonly" tabindex="2" />
                                    <img src="/js/FVS.gif" name="img_MONTHLYREPAY" id="img_MONTHLYREPAY" width="15px"
                                        height="15px" border="0" alt="" />
                                </td>
                            </tr>
                        </tbody>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_Accomodation" style='display: none'>
                    <table width='100%'>
                        <tr>
                            <td colspan="2">
                                <b>Accommodation</b>
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Rent &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ACCOMODATIONRENT" id="txt_ACCOMODATIONRENT"
                                    maxlength="10" value="<%=l_accomodationrent%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ACCOMODATIONRENT" id="img_ACCOMODATIONRENT" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Council Tax &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_COUNCILTAX" id="txt_COUNCILTAX" maxlength="10"
                                    value="<%=l_counciltax%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_COUNCILTAX" id="img_COUNCILTAX" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone Rental &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_LINERENTAL" id="txt_LINERENTAL" maxlength="10"
                                    value="<%=l_linerental%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_LINERENTAL" id="img_LINERENTAL" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Salary Deduction</b>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Heating &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_HEATING" id="txt_HEATING" maxlength="10"
                                    value="<%=l_heating%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_HEATING" id="img_HEATING" width="15px" height="15px"
                                    border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="div_BUPA" style='display: none'>
                    <table width='100%'>
                        <tr>
                            <td colspan="2">
                                <b>Medical Insurance</b>
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Name:
                            </td>
                            <td>
                                <%=lstmedicalorg%>
                                <img src="/js/FVS.gif" name="img_MEDICALORGID" id="img_MEDICALORGID" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'>
                                Annual Premium &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ANNUALPREMIUM" id="txt_ANNUALPREMIUM"
                                    maxlength="10" value="<%=l_annualpremium%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ANNUALPREMIUM" id="img_ANNUALPREMIUM" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Taxable Benefit &pound;:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_TAXABLEBENEFIT" id="txt_TAXABLEBENEFIT"
                                    maxlength="10" value="<%=l_taxablebenefit%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_TAXABLEBENEFIT" id="img_TAXABLEBENEFIT" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Additional Members:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ADDITIONALMEMBERS" id="txt_ADDITIONALMEMBERS"
                                    maxlength="10" value="<%=l_additionalmembers%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ADDITIONALMEMBERS" id="img_ADDITIONALMEMBERS" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ADDITIONALMEMBERS2" id="txt_ADDITIONALMEMBERS2"
                                    maxlength="10" value="<%=l_additionalmembers2%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ADDITIONALMEMBERS2" id="img_ADDITIONALMEMBERS2"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ADDITIONALMEMBERS3" id="txt_ADDITIONALMEMBERS3"
                                    maxlength="10" value="<%=l_additionalmembers3%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ADDITIONALMEMBERS3" id="img_ADDITIONALMEMBERS3"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ADDITIONALMEMBERS4" id="txt_ADDITIONALMEMBERS4"
                                    maxlength="10" value="<%=l_additionalmembers4%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ADDITIONALMEMBERS4" id="img_ADDITIONALMEMBERS4"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_ADDITIONALMEMBERS5" id="txt_ADDITIONALMEMBERS5"
                                    maxlength="10" value="<%=l_additionalmembers5%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_ADDITIONALMEMBERS5" id="img_ADDITIONALMEMBERS5"
                                    width="15px" height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Group Scheme Ref:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_GROUPSCHEMEREF" id="txt_GROUPSCHEMEREF"
                                    maxlength="10" value="<%=l_groupschemeref%>" tabindex="2" />
                                <img src="/js/FVS.gif" name="img_GROUPSCHEMEREF" id="img_GROUPSCHEMEREF" width="15px"
                                    height="15px" border="0" alt="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Membership No:
                            </td>
                            <td>
                                <input type="text" class="textbox200" name="txt_MEMBERSHIPNO" id="txt_MEMBERSHIPNO"
                                    maxlength="10" value="<%=l_membershipno%>" tabindex="2" />
                                <img alt="" src="/js/FVS.gif" name="img_MEMBERSHIPNO" id="img_MEMBERSHIPNO" width="15px"
                                    height="15px" border="0" />
                            </td>
                        </tr>
                        <tr>
                            <td height='100%'>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" height='100%'>
                &nbsp;
            </td>
        </tr>
        <%=strTableButtons%>
        <tr>
            <td>
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                <input type="hidden" name="txt_EMPLOYEEID" id="txt_EMPLOYEEID" value="<%=EmployeeID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="hidden" name="hid_PreviousDiv" id="hid_PreviousDiv" />
                <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />                
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <table width="750">
                    <tr>
                        <td valign="top" align="right">
                            <input type="button" class="RSLButton" value=" SAVE " title=" SAVE " onclick="{CorrectValidationFields();SaveForm()}"
                                style="cursor: pointer" />
                            <input type="button" class="RSLButton" value=" AMEND " title=" AMEND " onclick="{CorrectValidationFields();AmendForm()}"
                                name="AmendButton" style="display: none; width: 70px; cursor: pointer" />
                            <input type="button" class="RSLButton" value=" RESET " title=" RESET " onclick="ResetValues()"
                                style="width: 70px; cursor: pointer" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
   
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_ben" id="frm_ben" width="4" height="4" style="display: none">
    </iframe>
</body>
</html>