<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	PageName = "SicknessAbsence.asp"
	
	// only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	// redirect all director to a higher level showing their teams
	//if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TOTALDAYSABSENT,whereSQL, TOTALCOST,theFinalCost ,theToDate, OrderBy, EMPSQL ,EmployeeType,DEPTName
	
	theFinalCost = 0
	GetVariables()
	
	getTeams()
	opendb()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM WHERE TEAMID NOT IN (1) AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	GetDepartmentReportVars()

	closedb()
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

 	Function GetVariables()
		
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		theToDate = Request.QueryString("theTodate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")
		DEPTName=Request.QueryString("DEPTName")
		
		If OrderBy <> "" then
			orderSQL =  " ORDER BY  " & OrderBy & ",E.LASTNAME ASC, A.STARTDATE desc"
			
		else
			orderSQL = " ORDER BY E.LASTNAME ASC, A.STARTDATE desc "
		End if
		
		If Dapartment <> "" then
			deptSQL =  " AND T.TEAMID =  " & Dapartment & " "
		End if
		
		If EmployeeType = "Previous" then
			EMPSQL =  " AND( JD.ENDDATE IS not NULL OR JD.ACTIVE IN (0))"
		elseif EmployeeType = "All" then
			EMPSQL =  " "
		else 
			EMPSQL =  " AND (JD.ENDDATE IS NULL and JD.ACTIVE = 1)"
		End if
		
        If theDate="" and theToDate = "" then
            theDate = "1/" & month(Date) & "/" & year(Date)
            theToDate = FormatDateTime(Date,2)
        End if		
		
			
		dateSQL =  dateSQL & " AND ((a.STARTDATE BETWEEN '" & theDate & "' AND '" & theToDate & "') OR (ISNULL(a.RETURNDATE,CONVERT(DATE,GETDATE())) BETWEEN '" & theDate & "' AND '" & theToDate & "'))"
        
        DEFAULTstring = "&DEPT=" & Dapartment 
		DEFAULTstring= DEFAULTstring & "&DEPTName=" & DEPTName 
		DEFAULTstring= DEFAULTstring & "&thedate=" & theDate 
		DEFAULTstring= DEFAULTstring & "&theTodate=" & theToDate 
		DEFAULTstring= DEFAULTstring & "&EmployeeType=" & EmployeeType 
		
		whereSQL =  dateSQL & " " & deptSQL
	End Function
	
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 
								'"		(SELECT ISNULL(CAST(SUM(DURATION) AS NVARCHAR),'0') FROM E_JOURNAL EJ LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = EJ.EMPLOYEEID	LEFT JOIN E_ABSENCE A ON EJ.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = A.JOURNALID) WHERE ITEMNATUREID = 1 AND DURATION IS NOT NULL AND  EJ.EMPLOYEEID = E.EMPLOYEEID) AS SICKNESSABSENCE, " &_

		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT  E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.REASON, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM, " &_
						"		DBO.EMP_SICKNESS_DURATION(E.EMPLOYEEID,'" & theDate & "','" & theToDate & "',J.JOURNALID)  AS SICKNESSABSENCE, " &_
                        "			LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON, " &_
						"		jd.SALARY AS SALARY " &_						
						"	FROM E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID AND A.STARTDATE >= '1 JAN 2004' " &_
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = A.JOURNALID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "		LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
						"	WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20)  " &_
						"			AND DURATION IS NOT NULL  " &_
						" " & whereSQL & EMPSQL & orderSQL
						
						
		'"	LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND JNAL.CURRENTITEMSTATUSID = 5 AND JNAL.ITEMNATUREID = 2 " &_
		'rw strsql
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
			
				rec_is_true = 1
				'E.FIRSTNAME, E.LASTNAME, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.NOTES, 'No reason available.') AS REASON
				str_data = str_data & "<TBODY><TR STYLE='CURSOR:HAND' ONCLICK='load_me(" & rsSet("EMPLOYEEID") & ")'>" &_
					"<TD WIDTH=100PX>" & rsSet("LASTNAME") & "</TD>" &_
					"<TD WIDTH=106PX ALIGN=LEFT>" & rsSet("FIRSTNAME") & "</TD>" &_
					"<TD WIDTH=111PX ALIGN=LEFT>" & rsSet("TEAMNAME") & "</TD>" &_
					"<TD WIDTH=77PX ALIGN=CENTER>" & rsSet("STARTDATE") & "</TD>"&_
					"<TD WIDTH=117PX ALIGN=CENTER>" & rsSet("RETURNDATE")  & "</TD>" &_
					"<TD WIDTH=54PX ALIGN=CENTER>" & rsSet("SICKNESSABSENCE")  & "</TD>" &_
					"<TD WIDTH=216PX ALIGN=LEFT>" & rsSet("REASON") & "</TD>"
					
					If rsSet("TEAMNUM") > 0 then
						theFinalCost = theFinalCost +  (FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))  ) ))
						str_data = str_data & "<TD ALIGN=CENTER WIDTH=67PX>" & FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))  )) & "</TD>"
					Else
						theFinalCost = theFinalCost + (FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))) ))
						str_data = str_data & "<TD ALIGN=CENTER WIDTH=67PX>" & FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))) ) & "</TD>"
					
					End If
					
					str_data = str_data & "</TR></TBODY>"
				

				
				count = count + 1			
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=10 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'SicknessAbsenceAd.asp?bypass=1&page=1&name=" & searchName & "&team_id=" & team_id & DEFAULTstring& "&CC_Sort=" & OrderBy &"'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'SicknessAbsenceAd.asp?bypass=1&page=" & prevpage & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='SicknessAbsence.aspAd?bypass=1&page=" & nextpage & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='SicknessAbsence.aspAd?bypass=1&page=" & intPageCount & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy &"'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"
			
		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<TFOOT><TR><TD COLSPAN=10 STYLE='FONT:18PX' ALIGN=CENTER>No members exist within this team !!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR></TFOOT>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
									
			'Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
		

		
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=10 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	Function GetDepartmentReportVars()
		SQL = 		"SELECT (SELECT ISNULL(CAST(COUNT(*) AS FLOAT), 0) FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM, " &_
						"		DBO.EMP_SICKNESS_DURATION(E.EMPLOYEEID,'" & theDate & "','" & theToDate & "',J.JOURNALID)  AS SICKNESSABSENCE, " &_
						"		ISNULL(jd.SALARY,0) AS SALARY " &_						
						"	FROM E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID  " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID AND A.STARTDATE >= '1 JAN 2005' " &_
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = A.JOURNALID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
						"	WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20)  " &_
						"			AND DURATION IS NOT NULL  " &_
						" " & whereSQL & EMPSQL & orderSQL


			theFinalCost = 0
			Call OpenRs(rsLoader, SQL)
				if (NOT rsLoader.EOF) then
				while not rsLoader.eof
			
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
          			
					TOTALDAYSABSENT 	= TOTALDAYSABSENT + rsLoader("SICKNESSABSENCE")
					If rsLoader("TEAMNUM") > 0 then
						THECOST = ( (((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE")) )
						theFinalCost = theFinalCost + THECOST
					Else
						theFinalCost = theFinalCost + (((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE"))
						
					End If

				'==================================================================
				'= End Totals For department report
				'==================================================================	
				
				rsLoader.MoveNext()
				wend
				end if
			CloseRS(rsLoader)
			
	End Function

Select Case EmployeeType

	case "All" 
		AllSelected =  "selected"
	case "Previous" 
		PrevSelected =  "selected"
	case "Current" 
			CurrentSelected =  "selected"
	End Select

	sel_EmployeeType = 	 "<select name='sel_EmpType' class='textbox100'>" &_
				"<option value='Current' " & CurrentSelected & ">Current</option>" &_
				"<option value='Previous' " & PrevSelected & ">Previous</option>" &_
				"<option value='All' " & AllSelected & ">All</option>" &_
			 "</select>"
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<TITLE>RSL Manager Business --> HR Tools --> Team</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	FormFields[0] = "txt_theDate|Date - From|DATE|Y"
	FormFields[1] = "txt_theToDate|Date - To|DATE|Y"
	FormFields[2] = "sel_TEAM|Department|INT|N"
	//FormFields[3] = "sel_EmpType|EmployeeType|TEXT|N"
	
	function load_me(employee_id){
		
			location.href = "../myjob/erm.asp?employeeid=" + employee_id;
		
		}
	function quick_find(){
	
		location.href = "man_team.asp?name="+thisForm.findemp.value;
		
	}
	
	function BuildReport() {
	    if (!checkForm()) return false;
	    var querystring

	    quertstring = "?DEPT=" + thisForm.sel_TEAM.value
	    quertstring = quertstring +"&DEPTName=" + thisForm.sel_TEAM.options[thisForm.sel_TEAM.selectedIndex].text
	    quertstring = quertstring + "&theDate=" + thisForm.txt_theDate.value
	    quertstring = quertstring + "&theToDate=" + thisForm.txt_theToDate.value
	    quertstring = quertstring + "&EmployeeType=" + thisForm.sel_EmpType.value

	    location.href = "SicknessAbsenceAd.asp" + quertstring
			
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
<form name = thisForm method=post>
  <table border=0 width=900 cellpadding=2 cellspacing=2 height=40PX style='BORDER:1PX SOLID #133E71' bgcolor=beige>
    <tr>
      <td width="119" NOWRAP><b>From Date :</b></td>
      <td width="123"><strong>To Date : </strong></td>
      <td width="556"><b>Department :</b></td>
      <td width="419"><b>Department Information :</b></td>
    </tr>
    <tr>
      <td valign="top" NOWRAP><input type=text name="txt_theDate" value="<%=theDate%>" class="textbox" maxlength="10" style='width:100px;text-align:right'>
          <img src="/js/FVS.gif" name="img_theDate" width="15px" height="15px" border="0"></td>
      <td valign="top" NOWRAP> <input type=text name="txt_theToDate" value="<%=theToDate%>" class="textbox" maxlength="10" style='width:100px;text-align:right'>
          <img src="/js/FVS.gif" name="img_theToDate" width="15px" height="15px" border="0">  </td>   
      <td valign="top" NOWRAP><%=selTeam%><img src="/js/FVS.gif" name="img_TEAM" width="15px" height="15px" border="0"> <br>
          <br>
          
      <td NOWRAP rowspan="2"><div name="DeptInfo" id="DeptInfo">
          <table width="354"  border="0" cellpadding="0" cellspacing="0" style='BORDER:1PX SOLID #133E71'>
            <tr>
              <td colspan="5" height="10"></td>
            </tr>
            <tr>
              <td width="12">&nbsp;</td>
              <td width="116"><strong>Total Days Absent: </strong></td>
              <td width="75"><%= TOTALDAYSABSENT %></td>
              <td width="85"><strong>Total Cost  : � </strong></td>
              <td width="78"><%= FormatNumber(IsNullNumber(theFinalCost)) %></td>
            </tr>
            <tr>
              <td height="10" colspan="5"></td>
            </tr>
          </table>
      </div></td>
    </tr>
    <tr>
    <td width="119"><b>Employee Type :</b></td>
    </tr>
    <tr>
       <td valign="top" NOWRAP><%=sel_EmployeeType%>
       </td>
       <td colspan="2" align="center">
       <input name="Submit" type="button" class="RSLButton" onClick="BuildReport()" value="Build Report">
       </td>
    </tr>
  </table>
  <br>
<br>

  <TABLE WIDTH=900 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=2 valign="bottom"><IMG NAME="tab_establishment" TITLE='<%=team_name%>' SRC="Images/tab_establishmentreport.gif" WIDTH=157 HEIGHT=20 BORDER=0></TD>
      <!--TD><IMG SRC="images/spacer.gif" WIDTH=656 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT><TABLE WIDTH=600 CELLPADDING=0 CELLSPACING=0>
          <TR>
            <!--<TD width="72%" height="1" ALIGN=RIGHT>CONVERT TO PRINT FORMAT </TD>
            <TD width="5%" ALIGN=RIGHT><a href="SicknessAbsence_Print.asp?CC_Sort=<%=OrderBy%><%=theURL%><%=DEFAULTstring%>" target="_blank"><img src="../myImages/PrinterIcon.gif" alt="Print Arrears List" width="31" height="20" border="0" style="Cursor:hand" ></a></TD>-->
            <TD width="72%" height="1" ALIGN=RIGHT>&nbsp;</TD>
             <TD width="5%" ALIGN=RIGHT>&nbsp;</TD>
            <TD width="18%" ALIGN=RIGHT>CONVERT TO XLS </TD>
            <TD width="5%" ALIGN=RIGHT><a href="SicknessAbsence_XLS.asp?CC_Sort=<%=OrderBy%><%=theURL%><%=DEFAULTstring%>"><img src="../customer/Images/excel.gif" alt="Excel Arrears List" width="29" height="29" border="0" style="Cursor:hand" ></a></TD>
          </TR>
        </TABLE></TD>	
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71 colspan=4><IMG SRC="images/spacer.gif" WIDTH=740 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=900 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' COLSPAN=7> <B>Report : Sickness / Absence </B> </TD>
      <TD WIDTH=67 rowspan=2 ALIGN=left valign=bottom  CLASS='NO-BORDER'> <B>Cost</B></TD>
      </TR>
    <TR align="left" valign="bottom">
      <TD WIDTH=100 CLASS='NO-BORDER'><b>Surname</b></TD> 
      <TD WIDTH=106 CLASS='NO-BORDER'><b>First Name </b> </TD>
      <TD WIDTH=111 CLASS='NO-BORDER'>	  
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME desc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a>
	  <B>Dept.</B>
	  <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME  asc")%><%=DEFAULTstring%>" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>
		</TD>
      <TD WIDTH=77 CLASS='NO-BORDER'><B>Start Date</B></TD>
      <TD WIDTH=117 CLASS='NO-BORDER'><strong>End Date</strong></TD>
      <TD WIDTH=54 CLASS='NO-BORDER'><strong>Duration</strong></TD>
      <TD WIDTH=216 CLASS='NO-BORDER'> <B>Reason</B></TD>
      </TR>
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=10 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    </THEAD> <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

