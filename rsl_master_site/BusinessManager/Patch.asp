<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	' Call getList(lstType, "LSC_FundType", 0)  - Method of Calling the function	
	' lst										- String for select box
	' strTable									- Table name
	' ps										- 'Please Select' required - 1 = yes, 0 = no

	Dim lst_director, str_data

	Call getList(lst_director, "E__EMPLOYEE", 0)
	Call get_patch()

	Function get_patch()

		Dim strSQL, rsSet, cnt

		cnt = 0
		strSQL = 	"SELECT		P.PATCHID, " &_
					"			P.LOCATION, " &_
					"			(SELECT ISNULL(COUNT(*), 'N/A') FROM E_JOBDETAILS WHERE PATCH = P.PATCHID) AS PATCHNUM " &_
					"FROM		E_PATCH P " &_
					"ORDER 		BY LOCATION"

		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()

		str_data = str_data & "<tbody>"
		while not rsSet.EOF

			str_data = str_data & "<tr style=""cursor:pointer"">" &_
								  "<td onclick=""load_patch(" & rsSet("PATCHID") & ")"" width=""20px"" align=""center"">" & cnt+1 & "</td>" &_
								  "<td onclick=""load_patch(" & rsSet("PATCHID") & ")"" width=""300px"">" & rsSet("LOCATION") & "</td>" &_
								  "<td onclick=""load_patch(" & rsSet("PATCHID") & ")"" width=""20px"" align=""center"">" & rsSet("PATCHNUM") & "</td>" &_
								  "<td style=""border:none"" class=""DEL"" onclick=""del_patch(" & rsSet("PATCHID") & ")"" title=""[DELETE] " & rsSet("LOCATION") & """ style=""cursor:pointer"">Del</td></tr>"
			cnt = cnt + 1
			rsSet.movenext()

		wend

		If cnt = 0 Then 
			str_data = "<tr><td colspan=""2"" align=""center""><b>No patchs exist within the system</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing
		str_data = str_data & "</tbody>"
	End function


	Function getList(ByRef lst, strTable, ps)

		Dim strSQL, rsSet, cnt

			cnt = 0
			strSQL = "SELECT FIRSTNAME + ' ' + LASTNAME AS FULLNAME, * FROM " & strTable

		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()

		lst = "<option value=''>Please select</option>"
		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet("EMPLOYEEID") & "'" & extrabit & ">" & rsSet("FULLNAME") & "</option>"
			rsSet.MoveNext()
			cnt = cnt + 1
		Wend

		If cnt = 0 Then
			lst = "<option value=''>No Records Exist</option>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Patch Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array();
        FormFields[0] = "txt_LOCATION|Location|TEXT|Y"

        var theItems = new Array("txt_LOCATION");

        function click_save(str_action) {
            if (!checkForm()) return false;
            document.RSLFORM.target = "frm_patch";
            document.RSLFORM.action = "serverside/patch_srv.asp?path=" + str_action;
            document.RSLFORM.submit();
        }

        // called by delete button
        function del_patch(int_id) {
            var answer
            answer = window.confirm("Are you sure you wish to delete this patch")
            // if yes then send to server page to delete
            if (answer) {
                document.RSLFORM.target = "frm_patch";
                document.RSLFORM.action = "serverside/patch_srv.asp?path=del&patch_id=" + int_id;
                document.RSLFORM.submit();
            }
        }

        // called when user clicks on a row
        function load_patch(int_id) {
            document.RSLFORM.target = "frm_patch";
            document.RSLFORM.action = "serverside/patch_srv.asp?path=load&patch_id=" + int_id;
            document.RSLFORM.submit();
        }

        // swaps save button for amend button and vice versa
        // called by server side page
        function swap_button(int_which_one) { //   1 = show amend, 2 = show save
            if (int_which_one == 2) {
                document.getElementById("BTN_SAVE").style.display = "block"
                document.getElementById("BTN_AMEND").style.display = "none"
            }
            else {
                document.getElementById("BTN_SAVE").style.display = "none"
                document.getElementById("BTN_AMEND").style.display = "block"
            }
        }


        // swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
        function swap_div(int_which_one) {
            if (int_which_one == 1) {
                document.getElementById("new_div").style.display = "none";
                document.getElementById("table_div").style.display = "block";
                document.getElementById("tab_patch").src = 'images/tab_patch-over.gif';
                document.getElementById("tab_new_patch").src = 'images/tab_new_patch-tab_patch_ove.gif';
                document.getElementById("tab_amend_patch").src = 'images/tab_amend_patch.gif';
            }
            else if (int_which_one == 2) {
                clear()
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_patch").src = 'images/tab_patch.gif';
                document.getElementById("tab_new_patch").src = 'images/tab_new_patch-over.gif';
                document.getElementById("tab_amend_patch").src = 'images/tab_amend_patch-tab_new_pat.gif';
            }
            else {
                document.getElementById("new_div").style.display = "block";
                document.getElementById("table_div").style.display = "none";
                document.getElementById("tab_patch").src = 'images/tab_patch.gif';
                document.getElementById("tab_new_patch").src = 'images/tab_new_patch.gif';
                document.getElementById("tab_amend_patch").src = 'images/tab_amend_patch-over.gif';
            }
            checkForm()
        }

        function clear() {
            document.getElementById("txt_LOCATION").value = "";
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_patch" id="tab_patch" onclick="swap_div(1)" style="cursor: pointer"
                    src="images/tab_patch-over.gif" width="55" height="20" border="0" alt="Patch" />
            </td>
            <td rowspan="2">
                <img name="tab_new_patch" id="tab_new_patch" onclick="swap_div(2)" style="cursor: pointer"
                    src="images/tab_new_patch-tab_patch_ove.gif" width="89" height="20" border="0"
                    alt="New Patch" />
            </td>
            <td rowspan="2">
                <img name="tab_amend_patch" id="tab_amend_patch" src="images/tab_amend_patch.gif" width="112" height="20"
                    border="0" alt="Amend Patch" />
            </td>
            <td>
                <img src="images/spacer.gif" width="494" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="494" height="1" alt="" /></td>
        </tr>
    </table>
    <div id="new_div" style="display: none">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2">
            <thead>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td class='TABLE_HEAD'>
                        <b>Location</b>
                    </td>
                    <td nowrap>
                        <input type="textbox" class="textbox200" name="txt_LOCATION" id="txt_LOCATION" maxlength="100"
                            size="50" />
                    </td>
                    <td nowrap width="50px">
                        <img src="/js/FVS.gif" name="img_LOCATION" id="img_LOCATION" width="15px" height="15px"
                            border="0" alt="" />
                    </td>
                    <td nowrap width="100px">
                        <input type="bUtton" name="BTN_SAVE" id="BTN_SAVE" value="Save" title="Save" class="rslbutton"
                            onclick="click_save('new')" style="cursor: pointer">
                        <input type="button" name="BTN_AMEND" id="BTN_AMEND" value="Amend" title="Amend"
                            class="rslbutton" onclick="click_save('amend')" style="display: none; cursor: pointer">
                    </td>
                    <td width="100%">
                        &nbsp;
                    </td>
                </tr>
        </table>
    </div>
    <div id="table_div">
        <table width="750" class="TAB_TABLE" cellpadding="1" cellspacing="2" style="border-collapse: COLLAPSE;
            behavior: url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE" border="1">
            <thead>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD'>
                    </td>
                </tr>
                <tr>
                    <td width="20px" align="center" class='TABLE_HEAD'>
                        <b>ID</b>
                    </td>
                    <td width="100%" class='TABLE_HEAD'>
                        <b>Location</b>
                    </td>
                    <td width="20PX" class='TABLE_HEAD'>
                        <b>Emps</b>
                    </td>
                </tr>
                <tr style="height: 3px">
                    <td class='TABLE_HEAD' colspan="4" align="center" style="border-bottom: 2px solid #133e71">
                    </td>
                </tr>
            </thead>
            <%=str_data%>
        </table>
        <input type="hidden" name="hd_patch_id" id="hd_patch_id" />
    </div>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_patch" id="frm_patch" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
