<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PageName = "Establishment_report.asp"
	' only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	' redirect all director to a higher level showing their teams
	'if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL, EMPSQL
	Dim PrevSelected, allSelected, EmployeeType,OrderBy 

	Call GetVariables()
	Call getTeams()
	Call opendb()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM  WHERE TEAMID NOT IN (1,93) AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	Call GetDepartmentReportVars()
	Call closedb()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()

		PrevSelected =  ""
		AllSelected = ""
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")

		If OrderBy <> "" Then
			orderSQL =  " ORDER BY  " & OrderBy
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End If

		If EmployeeType = "Previous" Then
			EMPSQL =  " AND ( (J.ENDDATE IS NOT NULL AND (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') <= GETDATE())) OR J.ACTIVE IN (0)) "
		ElseIf EmployeeType = "All" Then
			EMPSQL =  " "
		Else
			EMPSQL =  " AND ((J.ENDDATE IS NULL OR (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') > GETDATE())) AND J.ACTIVE = 1) "

		End If

		If Dapartment <> "" Then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End If

		If theDate <> "" Then
			whereNeeded = 1
			If deptSQL <> "" Then
			    dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End If

		If whereNeeded = 1 Then
			whereSQL = " AND " & deptSQL & dateSQL
		End If

		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate & "&EmployeeType=" & EmployeeType

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	CASE WHEN LEN(ISNULL(J.JOBTITLE, 'N/A')) > 12   " &_
 						"		THEN LEFT(ISNULL(J.JOBTITLE, 'N/A'), 14) +  '...'   " &_
						"		ELSE  ISNULL(J.JOBTITLE, 'N/A')   " &_
						"	END AS JOBTITLEshort, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(EG.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE " &_
						" FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	lEFT JOIN E_GRADE EG ON EG.GRADEID=CAST(J.GRADE AS INT) " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL & orderSQL 

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick=""load_me(" & rsSet("EMPLOYEEID") & ")"">" &_
					"<td width=""120px"">" & rsSet("FULLNAME") & "</TD>" &_
					"<td width=""100px"" align=""left"" title='" & rsSet("JOBTITLE") & "'>" & rsSet("JOBTITLEshort") & "</td>" &_
					"<td width=""65px"" align=""center"">" & rsSet("DEPARTMENT") & "</td>" &_
					"<td align=""center"">" & rsSet("GENDER") & "</td>"&_
					"<td align=""center"">" & rsSet("ETHNICITY")  & "</td>" &_
					"<td width=""70px"" align=""center"">" & rsSet("DOB") & "</td>"&_
					"<td align=""center"">" & rsSet("SALARY") & "</td>"&_
					"<td width=""38px"" align=""center"">" & rsSet("GRADE") & "</td>"&_
					"<td width=""75px"" align=""center"">" & rsSet("STARTDATE") & "</td>"&_
					"<td width=""70px"" align=""center"">" & rsSet("ENDDATE") & "</td>"

					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""10"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='Establishment_Report.asp?bypass=1&page=1&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring &"'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='Establishment_Report.asp?bypass=1&page=" & prevpage & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='Establishment_Report.asp?bypass=1&page=" & nextpage & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='Establishment_Report.asp?bypass=1&page=" & intPageCount & "&name=" & searchName & "&cc_sort=" & orderby & "&team_id=" & team_id & DEFAULTstring &"'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<tfoot><tr><td colspan=""10"" style=""font:16px"" align=""center"">No records found for this date</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

			'Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records

	End function


	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""10"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function GetDepartmentReportVars()

			sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
					"			SUM(	CASE WHEN J.STARTDATE = '" & theDate & "'    " &_
					"				THEN 1    " &_
					"				ELSE  0    " &_
					"    		END )AS STARTERS, " &_
					"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 					"				THEN 1 " &_
					"				ELSE 0 " &_
        			"			END) AS MALETOTAL, " &_
					"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 					"				THEN 1 " &_
					"				ELSE 0 " &_
        			"			END) AS FEMALETOTAL, " &_
					"			SUM(	CASE WHEN J.ENDDATE = '" & theDate & "' " &_ 
					"				THEN 1    " &_ 
					"				ELSE  0   " &_
					"    		END )AS LEAVERS " &_
					"FROM	E__EMPLOYEE E   " &_
					"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
					"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
					"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
					" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL

		Call OpenRs(rsLoader, SQL)
		If (NOT rsLoader.EOF) Then
		'==================================================================
		'= Set Some Totals For department report
		'==================================================================
			TotalSalary = FormatCurrency(rsLoader("SALARY"))
			TotalStaff	= rsLoader("TOTALSTAFF")
			maletotal	= rsLoader("MALETOTAL")
			femaletotal	= rsLoader("FEMALETOTAL")
			NewStarters	= rsLoader("STARTERS")
			Leavers		= rsLoader("LEAVERS")
		'==================================================================
		'= End Totals For department report
		'==================================================================	
		End If
		Call CloseRS(rsLoader)
	End Function

	Select Case EmployeeType
		case "All"
			AllSelected =  "selected"
		case "Previous"
			PrevSelected =  "selected"
		case "Current"
			CurrentSelected =  "selected"
	End Select

	sel2 = 	 "<select name='sel_EmpType' id='sel_EmpType' class='textbox200'>" &_
				"<option value='Current' " & CurrentSelected & ">Current</option>" &_
				"<option value='Previous' " & PrevSelected & ">Previous</option>" &_
				"<option value='All' " & AllSelected & ">All</option>" &_
			 "</select>"
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()
        FormFields[0] = "txt_theDate|Date - As At|DATE|N"
        FormFields[1] = "sel_TEAM|Department|INT|N"

        function load_me(employee_id) {
            location.href = "../myjob/erm.asp?employeeid=" + employee_id;
        }

        function quick_find() {
            location.href = "man_team.asp?name=" + document.getElementById("findemp").value;
        }

        function BuildReport() {
            if (!checkForm()) return false;
            location.href = "Establishment_report.asp?DEPT=" + document.getElementById("sel_TEAM").value + "&theDate=" + document.getElementById("txt_theDate").value + "&EmployeeType=" + document.getElementById("sel_EmpType").value
        }

        function PrintMe() {

            var location = "Establishment_Report_Print.asp?DEPT=" + document.getElementById("sel_TEAM").value + "&theDate=" + document.getElementById("txt_theDate").value + "&EmployeeType=" + document.getElementById("sel_EmpType").value + "&cc_sort=<%=orderby%>"

            window.open(location,'_blank');
            //location.href = location;
        }

        function XLsMe() {
            location.href = "Establishment_report_xlsversion.asp?DEPT=" + document.getElementById("sel_TEAM").value + "&theDate=" + document.getElementById("txt_theDate").value + "&EmployeeType=" + document.getElementById("sel_EmpType").value + "&cc_sort=<%=orderby%>"
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
    <form name="thisForm" method="post" action="">
    <table border="0" width="900" cellpadding="2" cellspacing="2" style="height:40px; border: 1px solid #133e71"
        bgcolor="beige">
        <tr>
            <td width="109" nowrap="nowrap">
                <b>Date :</b>
            </td>
            <td width="185">
                <b>Department :</b>
            </td>
            <td width="584">
                <b>Department Information :</b>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <input type="text" name="txt_theDate" id="txt_theDate" class="textbox" maxlength="10"
                    style="width: 100px; text-align: right" value="<%= theDate %>" /><img src="/js/FVS.gif"
                        name="img_theDate" id="img_theDate" width="15px" height="15px" border="0" alt="" />
            </td>
            <td valign="top" nowrap="nowrap">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <%=selTeam%><img src="/js/FVS.gif" name="img_TEAM" id="img_TEAM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Employee Type</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= sel2 %>
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="Submit" id="Submit" type="button" class="RSLButton" onclick="BuildReport()"
                                value="Build Report" title="Build Report" style="cursor:pointer" />
                        </td>
                    </tr>
                </table>
            </td>
            <td nowrap="nowrap">
                <div id="DeptInfo">
                    <table width="354" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #133e71">
                        <tr>
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td width="12">
                                &nbsp;
                            </td>
                            <td width="88">
                                <strong>Total Salary : </strong>
                            </td>
                            <td width="117">
                                <%= TotalSalary %>
                            </td>
                            <td width="94">
                                <strong>New Starters : </strong>
                            </td>
                            <td width="41">
                                <%= NewStarters %>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Total Staff : </strong>
                            </td>
                            <td>
                                <%= TotalStaff %>
                                ( M:
                                <%=maletotal%>
                                / f:
                                <%= femaletotal %>
                                )
                            </td>
                            <td>
                                <strong>Leavers : </strong>
                            </td>
                            <td>
                                <%= Leavers %>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" colspan="5">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <table width="900" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="tab_establishment" id="tab_establishment" src="Images/tab_establishmentreport.gif"
                    width="157" height="20" border="0" alt="<%=team_name%>" />
            </td>
            <!--td><img src="images/spacer.gif" width="656" height="19" alt="" /></td-->
            <td align="right">
                <table width="600" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="72%" height="1" align="right">
                            CONVERT TO PRINT FORMAT
                        </td>
                        <td width="5%" align="right">
                            <!--<a href="Establishment_Report_Print.asp?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE desc")%><%=DEFAULTstring%>"
                                target="_blank">
                                

                            </a>-->

                            <img src="../myImages/PrinterIcon.gif" width="31" height="20" style="cursor: pointer" onclick="PrintMe()"
                                    border="0" alt="Print Arrears List" />
                        </td>
                        <td width="18%" align="right">
                            CONVERT TO XLS
                        </td>
                        <td width="5%" align="right">
                            <img src="../customer/Images/excel.gif" width="29" height="29" style="cursor: pointer"
                                onclick="XLsMe()" border="0" alt="Excel Arrears List" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4">
                <img src="images/spacer.gif" width="740" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="900" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE"
        border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' colspan="6">
                    <b>Report : Establishment </b>
                </td>
                <td width="40" rowspan="2" align="left" valign="bottom" class='NO-BORDER'>
                    <b>Salary</b>
                </td>
                <td align="left" width="78" class='NO-BORDER' rowspan="2" valign="bottom">
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a> <b>Grade</b> <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.GRADE asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none">
                                <img src="../myImages/sort_arrow_up_old.gif" width="17"
                                    height="16" alt="Sort Ascending" border="0" /></a>
                </td>
            </tr>
            <tr align="left" valign="bottom">
                <td width="96" class='NO-BORDER'>
                    <b>Name</b>
                </td>
                <td width="101" class='NO-BORDER'>
                    <b><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.JOBTITLEshort desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif"width="11"
                            height="12" border="0" alt="Sort Descending"  /></a>Job Title<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.JOBTITLEshort  asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_up_old.gif"
                                    width="17" height="16" border="0" alt="Sort Ascending" /></a></b>
                </td>
                <td width="86" class='NO-BORDER'>
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.DEPARTMENT desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a> <b>Dept.</b> <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.DEPARTMENT  asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none">
                                <img src="../myImages/sort_arrow_up_old.gif" width="17"
                                    height="16" border="0" alt="Sort Ascending" /></a>
                </td>
                <td width="83" class='NO-BORDER'>
                    <strong><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GENDER DESC")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>Gender<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("GENDER ASC")%><%=DEFAULTstring%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_up_old.gif"
                                    width="17" height="16" border="0" alt="Sort Ascending" /></a></strong>
                </td>
                <td width="92" class='NO-BORDER'>
                    <strong><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ETHNICITY desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11"
                            height="12" border="0" alt="Sort Descending" /></a>Ethnicity<a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("ETHNICITY  asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none"><img src="../myImages/sort_arrow_up_old.gif"
                                    width="17" height="16" border="0" alt="Sort Ascending" /></a></strong>
                </td>
                <td width="63" class='NO-BORDER'>
                    <b><a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("DOB desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11" height="12" border="0" alt="Sort Descending" /></a>DOB</b><a
                            href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("DOB  asc")%><%=DEFAULTstring%>"
                            style="text-decoration: none"><img src="../myImages/sort_arrow_up_old.gif" width="17"
                                height="16" border="0" alt="Sort Ascending" /></a>
                </td>
                <td width="108" class='NO-BORDER'>
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.STARTDATE desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" width="11" height="12" border="0" alt="Sort Descending" /></a>
                    <b>Start Date</b> <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("J.STARTDATE  asc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_up_old.gif" width="17" height="16" border="0" alt="Sort Ascending" /></a>
                </td>
                <td width="89" class='NO-BORDER'>
                    <b>End Date </b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="12" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4"
        style="display: none"></iframe>
</body>
</html>
