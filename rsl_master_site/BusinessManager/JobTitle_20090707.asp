<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim lst_director, str_data
	get_patch()
	
	Function get_patch()
		
		Dim strSQL, rsSet, cnt 
		
		cnt = 0
	    strSQL =    " SELECT J.JOBTITLEID,J.JOBTITLE,COUNT(E.JOBTITLE) AS JOBTITLENUM "&_ 
                    " FROM E_JOBTITLES J "&_
	                "     LEFT JOIN E_JOBDETAILS E ON E.JOBTITLE=J.JOBTITLE "&_
                    " GROUP BY J.JOBTITLEID,J.JOBTITLE "&_
                    "ORDER BY J.JOBTITLE "
		
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()
		
		str_data = str_data & "<TBODY>"
		while not rsSet.EOF
		
			str_data = str_data & "<TR STYLE='CURSOR:HAND'>" &_
								  "<TD ONCLICK=load_JobTitle(" & rsSet("JOBTITLEID") & ") WIDTH=20PX ALIGN=CENTER>" & cnt+1 & "</TD>" &_
								  "<TD ONCLICK=load_JobTitle(" & rsSet("JOBTITLEID") & ") WIDTH=300PX>" & rsSet("JOBTITLE") & "</TD>" &_
								  "<TD ONCLICK=load_JobTitle(" & rsSet("JOBTITLEID") & ") WIDTH=20PX ALIGN=CENTER>" & rsSet("JOBTITLENUM") & "</TD>" &_
								  "<TD STYLE='BORDER:NONE' CLASS='DEL' ONCLICK=del_JobTitle(" & rsSet("JOBTITLEID") & ")>Del</TD></TR>"
			cnt = cnt + 1
			rsSet.movenext()
		
		wend
		
		If cnt = 0 Then 
			str_data = "<TR><TD colspan=2 align=center><B>No patchs exist within the system !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		str_data = str_data & "</TBODY>"
	End function
	
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --&gt; HR Tools --&gt; Job Title Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_JobTitle|JobTitle|TEXT|Y"
	
	var theItems = new Array("txt_JobTitle");

	function click_save(str_action){
		if (!checkForm()) return false;
		
		RSLFORM.target = "frm_JobTitle";
		RSLFORM.action = "serverside/JobTitle_srv.asp?path=" + str_action;
		RSLFORM.submit();
	}

	// called by delete button
	function del_JobTitle(int_id){
		
		var answer
		answer = window.confirm("Are you sure you wish to delete this Job Title")
		
		// if yes then send to server page to delete
		if (answer){
			RSLFORM.target = "frm_JobTitle";
			RSLFORM.action = "serverside/JobTitle_srv.asp?path=del&JobTitle_id=" + int_id;
			RSLFORM.submit();
			}
	
	}
	
	// called when user clicks on a row
	function load_JobTitle(int_id){
			
			RSLFORM.target = "frm_JobTitle";
			RSLFORM.action = "serverside/JobTitle_srv.asp?path=load&JobTitle_id=" + int_id;
			RSLFORM.submit();	
	
	}
	
	// swaps save button for amend button and vice versa
	// called by server side page
	function swap_button(int_which_one){ //   1 = show amend, 2 = show save
		
		if (int_which_one == 2){
			document.getElementById("BTN_SAVE").style.display = "block"
			document.getElementById("BTN_AMEND").style.display = "none"
			}
		else {
			document.getElementById("BTN_SAVE").style.display = "none"
			document.getElementById("BTN_AMEND").style.display = "block"
			}
	}


	// swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
	function swap_div(int_which_one){
	
		if (int_which_one == 1) {
			new_div.style.display = "none";
			table_div.style.display = "block";
			document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle-over.gif';
			document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle-tab_jobtitle_ove.gif';
			document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle.gif';
			}
		else if (int_which_one == 2) {
			clear()
			new_div.style.display = "block";
			table_div.style.display = "none";
			document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle.gif';
			document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle-over.gif';
			document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle-tab_new_pat.gif';
			}
		else {
			new_div.style.display = "block";
			table_div.style.display = "none";
			document.getElementById("tab_JobTitle").src = 'images/tab_jobtitle.gif';
			document.getElementById("tab_new_JobTitle").src = 'images/tab_new_jobtitle.gif';
			document.getElementById("tab_amend_JobTitle").src = 'images/tab_amend_jobtitle-over.gif';
			}
		checkForm()
	}
	
	function clear(){
	
		RSLFORM.txt_JobTitle.value = "";
	
	}

	
// -->
</SCRIPT>
<!-- End Preload Script -->


<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post> 
<TITLE>Job Title Setup</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2>
				<IMG NAME="tab_JobTitle" ONCLICK='swap_div(1)' style='cursor:hand' SRC="images/tab_jobtitle-over.gif" WIDTH=55 HEIGHT=20 BORDER=0></TD>
		<TD ROWSPAN=2>
				<IMG NAME="tab_new_JobTitle" ONCLICK='swap_div(2)' style='cursor:hand' SRC="images/tab_new_jobtitle-tab_jobtitle_ove.gif" WIDTH=89 HEIGHT=20 BORDER=0></TD>
		<TD ROWSPAN=2>
				<IMG NAME="tab_amend_JobTitle" SRC="images/tab_amend_jobtitle.gif" WIDTH=112 HEIGHT=20 BORDER=0></TD>
		<TD>
			<IMG SRC="images/spacer.gif" WIDTH=494 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71>
			<IMG SRC="images/spacer.gif" WIDTH=494 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=new_div STYLE='DISPLAY:NONE'>
	<TABLE WIDTH=750 CLASS="TAB_TABLE" CELLPADDING=1 CELLSPACING=2>
		<THEAD><TR STYLE="HEIGHT:3PX"><TD CLASS='TABLE_HEAD'></TD></TR>
		<TR> 
		            <TD CLASS='TABLE_HEAD' style="width:50px;" ><B>Job Title</B></TD>
		<TD nowrap> 
		  	<input type="textbox" class="textbox200" name="txt_JobTitle" maxlength=100 size=50></td><td nowrap width=50px>
		    <image src="/js/FVS.gif" name="img_JobTitle" width="15px" height="15px" border="0"></td><td nowrap width=100px>			
			<input type="bUtton" name="BTN_SAVE" value="Save" class=rslbutton onclick="click_save('new')">
		 	<input type="button" id="BTN_AMEND" name='BTN_AMEND' value="Amend" class=rslbutton onclick="click_save('amend')" style='display:none'>
		</TD>
		<td width=100%>&nbsp;</td>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=table_div>
	<TABLE WIDTH=750 CLASS="TAB_TABLE" CELLPADDING=1 CELLSPACING=2 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=1>
		<THEAD><TR STYLE="HEIGHT:3PX"><TD CLASS='TABLE_HEAD'></TD></TR>
		<TR> 
		 <TD WIDTH=20PX ALIGN=CENTER CLASS='TABLE_HEAD'><B>ID</B></TD>
		 <TD WIDTH=100% CLASS='TABLE_HEAD'> <B>Job Title</B></TD>
		 <TD WIDTH=20PX CLASS='TABLE_HEAD'> <B>Emps</B></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=4 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<%=str_data%>
	</TABLE>
	<INPUT TYPE=HIDDEN NAME='hd_JobTitle_id'>
</DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_JobTitle width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

