<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->   
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="14">&nbsp;</td>
		<td>
		<table width="100%" border="0" cellspacing="5" cellpadding="0" class="rslmanager">
	 		<tr> 
        	<tr> 
				<td><b>Business</b></td><td>&nbsp;</td>
			</tr>
		  		<td width="35%"><img src="../myImages/HoldingGraphics/business.gif" width="295" height="283"></td>
          		<td width="65%" valign="top"> 
            		<p><br>
              The Business Module facilitates the business planning process, the 
              management of network teams, the performance of delivery partners 
              and contract payments and all required reporting.</p>
                  <p>Working alongside all other modules the Business Module ensures:</p>
                  <ul>
                    <li>Standard approach to business planning in accordance with 
                      approved formats</li>
                    <li>Standard delivery of business plan information to team 
                      members</li>
                    <li>Allocation of delivery targets and associated 
                      contract payments</li>
                   <li>Additional quality assurance performance monitoring</li>
                    <li>Efficient management of all team members and delivery 
                      partners </li>
                  </ul>
                    <p class="iagManagerSmallBlk"> <br /></p>
          		</td>
			</tr>
      	</table>
		</td>
	</tr>
	<tr>
		<td width="14">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
