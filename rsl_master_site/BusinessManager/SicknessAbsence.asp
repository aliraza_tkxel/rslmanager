<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PageName = "SicknessAbsence.asp"

	' only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	' redirect all director to a higher level showing their teams
	'if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, theToDate
	Dim searchName, searchSQL, orderSQL, DEFAULTstring
	Dim TOTALDAYSABSENT, whereSQL, TOTALCOST, theFinalCost, OrderBy, EMPSQL, EmployeeType, DEPTName

	theFinalCost = 0
	Call GetVariables()
	Call getTeams()
	Call OpenDB()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM WHERE TEAMID NOT IN (1) AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	Call GetDepartmentReportVars()
	Call CloseDB()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

 	Function GetVariables()

		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		theToDate = Request.QueryString("theTodate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")
		DEPTName=Request.QueryString("DEPTName")

		If OrderBy <> "" Then
			orderSQL = " ORDER BY " & OrderBy & ", E.LASTNAME ASC, A.STARTDATE DESC"
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC, A.STARTDATE DESC"
		End If
		
		If Dapartment <> "" Then
			deptSQL =  " AND T.TEAMID =  " & Dapartment & " "
		End If
		
		If EmployeeType = "Previous" Then
			EMPSQL =  " AND( JD.ENDDATE IS NOT NULL OR JD.ACTIVE IN (0))"
		ElseIf EmployeeType = "All" Then
			EMPSQL =  " "
		Else
			EMPSQL =  " AND (JD.ENDDATE IS NULL and JD.ACTIVE = 1)"
		End If


        If theDate="" And theToDate = "" Then
            theDate = "1/" & month(Date) & "/" & year(Date)
            theToDate = FormatDateTime(Date,2)
        End If

		'dateSQL =  dateSQL & " AND (a.STARTDATE BETWEEN '" & theDate & "' AND '" & theToDate & "')"
		'OR (ISNULL(a.RETURNDATE,CONVERT(DATE,GETDATE())) BETWEEN '" & theDate & "' AND '" & theToDate & "') or (a.STARTDATE < '" & theDate & "' AND ISNULL(A.RETURNDATE,CONVERT(DATE,GETDATE())) > '" & theToDate & "'))"

        DEFAULTstring = "&DEPT=" & Dapartment
		DEFAULTstring= DEFAULTstring & "&DEPTName=" & DEPTName
		DEFAULTstring= DEFAULTstring & "&thedate=" & theDate
		DEFAULTstring= DEFAULTstring & "&theTodate=" & theToDate
		DEFAULTstring= DEFAULTstring & "&EmployeeType=" & EmployeeType

		whereSQL =  dateSQL & " " & deptSQL

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord
		intRecord = 0
		str_data = ""
		strSQL = 		" SELECT Distinct E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, PFT.DESCRIPTION AS PARTFULLTIME, JD.STARTDATE AS EmployeeStartDate, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.REASON, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM " &_                        
                        " , CASE "                                                                            &_
                        "  WHEN A.returndate IS NULL AND GETDATE() < '" & theToDate & "' THEN "                          &_
						"        dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "' , GETDATE(), J.journalid) " &_
                        "  ELSE                                                                             " &_
						"       dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "',  '" & theToDate & "', J.journalid)   " &_
                        " END                                      AS SICKNESSABSENCE "                      &_
                        "		,	LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON " &_
						"		, JD.SALARY AS SALARY " &_
                        "       , JD.JOBTITLE " &_
						" FROM  E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_ 

                        "        AND ((A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' ) " &_
			            "                 OR (A.RETURNDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' AND A.STARTDATE < '" & theDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theToDate & "' AND A.RETURNDATE IS NULL)) " &_
                        "        AND (A.STARTDATE <= '" & theToDate & "' ) " &_    
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = J.JOURNALID AND J.EMPLOYEEID=E.EMPLOYEEID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "		LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
                        "       LEFT JOIN E_PARTFULLTIME PFT ON JD.PARTFULLTIME = PFT.PARTFULLTIMEID " &_
						" WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND  T.TEAMID <> 1 AND T.ACTIVE=1  " &_
						" " & whereSQL & EMPSQL & orderSQL
'Response.Write(strSQL)
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

				rec_is_true = 1
				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick='load_me(" & rsSet("EMPLOYEEID") & ")'>" &_
					"<td width=""100px"">" & rsSet("LASTNAME") & "</td>" &_
					"<td width=""106px"" align=""left"">" & rsSet("FIRSTNAME") & "</td>" &_
                    "<td align=""left"">" & rsSet("PARTFULLTIME") & "</td>" &_
                    "<td align=""left"">" & rsSet("EmployeeStartDate") & "</td>" &_
                    "<td align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
					"<td width=""111px"" align=""left"">" & rsSet("TEAMNAME") & "</td>" &_
					"<td width=""77px"" align=""center"">" & rsSet("STARTDATE") & "</td>"&_
					"<td width=""117px"" align=""center"">" & rsSet("RETURNDATE")  & "</td>" &_
					"<td width=""54px"" align=""center"">" & rsSet("SICKNESSABSENCE")  & "</td>" &_
					"<td width=""216px"" align=""left"">" & rsSet("REASON") & "</td>"

					If rsSet("TEAMNUM") > 0 Then
						' theFinalCost = theFinalCost +  (FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))  ) ))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))  )) & "</td>"
					Else
						' theFinalCost = theFinalCost + (FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))) ))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE"))) ) & "</td>"
					End If

					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""11"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='SicknessAbsence.asp?bypass=1&page=1&name=" & searchName & "&team_id=" & team_id & DEFAULTstring& "&CC_Sort=" & OrderBy &"'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='SicknessAbsence.asp?bypass=1&page=" & prevpage & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='SicknessAbsence.asp?bypass=1&page=" & nextpage & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='SicknessAbsence.asp?bypass=1&page=" & intPageCount & "&name=" & searchName & "&team_id=" & team_id & DEFAULTstring & "&CC_Sort=" & OrderBy &"'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then
			str_data = "<tfoot><tr><td colspan=""11"" style=""font:18px"" align=""center"">No records to display</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

			'Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""11"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function GetDepartmentReportVars()
		SQL = 		" SELECT Distinct E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, PFT.DESCRIPTION AS PARTFULLTIME, JD.STARTDATE AS EmployeeStartDate, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.REASON, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM " &_
                        " , CASE "                                                                            &_
                        "  WHEN A.returndate IS NULL AND GETDATE() < '" & theToDate & "' THEN "                          &_
						"        dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "' , GETDATE(), J.journalid) " &_
                        "  ELSE                                                                             " &_
						"       dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "',  '" & theToDate & "', J.journalid)   " &_
                        " END                                      AS SICKNESSABSENCE "                      &_
                        "		,	LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON " &_
						"		, JD.SALARY AS SALARY " &_
                        "       , JD.JOBTITLE " &_
						" FROM  E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_ 

                        "        AND ((A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' ) " &_
			            "                 OR (A.RETURNDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' AND A.STARTDATE < '" & theDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theToDate & "' AND A.RETURNDATE IS NULL)) " &_
                        "        AND (A.STARTDATE <= '" & theToDate & "' ) " &_    
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = J.JOURNALID AND J.EMPLOYEEID=E.EMPLOYEEID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "		LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
                        "       LEFT JOIN E_PARTFULLTIME PFT ON JD.PARTFULLTIME = PFT.PARTFULLTIMEID " &_
						" WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND  T.TEAMID <> 1 AND T.ACTIVE=1  " &_
						" " & whereSQL & EMPSQL & orderSQL

			theFinalCost = 0.0
            TOTALDAYSABSENT = 0


			Call OpenRs(rsLoader, SQL)			
				If (NOT rsLoader.EOF) Then
				While NOT rsLoader.eof
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
					TOTALDAYSABSENT = TOTALDAYSABSENT + rsLoader("SICKNESSABSENCE")
					'If rsLoader("TEAMNUM") > 0 Then
					'	THECOST = ( (((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE")) )
					'	theFinalCost = theFinalCost + THECOST
						'Response.Write(THECOST &" - theFinalCost -"& theFinalCost)
					'Else
						theFinalCost = theFinalCost + FormatNumber(IsNullNumber((((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE")))) 
					'End If
				'==================================================================
				'= End Totals For department report
				'==================================================================
				rsLoader.MoveNext()
				Wend
				End If
			Call CloseRS(rsLoader)

	End Function

Select Case EmployeeType

	case "All" 
		AllSelected =  "selected"
	case "Previous" 
		PrevSelected =  "selected"
	case "Current" 
			CurrentSelected =  "selected"
	End Select

	sel_EmployeeType = 	 "<select name=""sel_EmpType"" id=""sel_EmpType"" class=""textbox100"">" &_
				"<option value='Current' " & CurrentSelected & ">Current</option>" &_
				"<option value='Previous' " & PrevSelected & ">Previous</option>" &_
				"<option value='All' " & AllSelected & ">All</option>" &_
			 "</select>"	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        var FormFields = new Array()
        FormFields[0] = "txt_theDate|Date - From|DATE|Y"
        FormFields[1] = "txt_theToDate|Date - To|DATE|Y"
        FormFields[2] = "sel_TEAM|Department|INT|N"
        //FormFields[3] = "sel_EmpType|EmployeeType|TEXT|N"

        function load_me(employee_id) {
            location.href = "../myjob/erm.asp?employeeid=" + employee_id;
        }

        function quick_find() {
            location.href = "man_team.asp?name=" + thisForm.findemp.value;
        }

        function BuildReport() {
            if (!checkForm()) return false;
            var querystring
            quertstring = "?DEPT=" + thisForm.sel_TEAM.value
            quertstring = quertstring + "&DEPTName=" + thisForm.sel_TEAM.options[thisForm.sel_TEAM.selectedIndex].text
            quertstring = quertstring + "&theDate=" + thisForm.txt_theDate.value
            quertstring = quertstring + "&theToDate=" + thisForm.txt_theToDate.value
            quertstring = quertstring + "&EmployeeType=" + thisForm.sel_EmpType.value
            location.href = "SicknessAbsence.asp" + quertstring
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
    <form name="thisForm" method="post" action="">
    <table border="0" width="900" cellpadding="2" cellspacing="2" style="border: 1px solid #133e71; height:40px"
        bgcolor="beige">
        <tr>
            <td width="119" nowrap="nowrap">
                <b>From Date :</b>
            </td>
            <td width="123">
                <strong>To Date : </strong>
            </td>
            <td width="556">
                <b>Department :</b>
            </td>
            <td width="419">
                <b>Department Information :</b>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <input type="text" name="txt_theDate" id="txt_theDate" value="<%=theDate%>" class="textbox" maxlength="10"
                    style='width: 100px; text-align: right' />
                <img src="/js/FVS.gif" name="img_theDate" id="img_theDate" width="15px" height="15px" border="0" alt="" />
            </td>
            <td valign="top" nowrap="nowrap">
                <input type="text" name="txt_theToDate" id="txt_theToDate" value="<%=theToDate%>" class="textbox" maxlength="10"
                    style="width: 100px; text-align: right" />
                <img src="/js/FVS.gif" name="img_theToDate" id="img_theToDate" width="15px" height="15px" border="0" alt="" />
            </td>
            <td valign="top" nowrap="nowrap">
                <%=selTeam%><img src="/js/FVS.gif" name="img_TEAM" id="img_TEAM" width="15px" height="15px" border="0" alt="" />                
                <br />
                <br />
                </td>
                <td nowrap="nowrap" rowspan="2">
                    <div id="DeptInfo">
                        <table width="354" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #133e71">
                            <tr>
                                <td colspan="5" height="10">
                                </td>
                            </tr>
                            <tr>
                                <td width="12">
                                    &nbsp;
                                </td>
                                <td width="116">
                                    <strong>Total Days Absent: </strong>
                                </td>
                                <td width="75">
                                    <%= TOTALDAYSABSENT %>
                                </td>
                                <td width="85">
                                    <strong>Total Cost : � </strong>
                                </td>
                                <td width="78">
                                    <%= FormatNumber(IsNullNumber(theFinalCost)) %>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" colspan="5">
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
        </tr>
        <tr>
            <td width="119">
                <b>Employee Type :</b>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap">
                <%=sel_EmployeeType%>
            </td>
            <td colspan="2" align="center">
                <input name="Submit" type="button" class="RSLButton" onclick="BuildReport()" value="Build Report" title="Build Report" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table width="900" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img name="tab_establishment" title="<%=team_name%>" src="Images/tab_establishmentreport.gif"
                    width="157" height="20" border="0" alt="<%=team_name%>" />
            </td>
            <td align="right">
                <table width="600" cellpadding="0" cellspacing="0">
                    <tr>                       
                        <td width="72%" height="1" align="right">
                            &nbsp;
                        </td>
                        <td width="5%" align="right">
                            &nbsp;
                        </td>
                        <td width="18%" align="right">
                            CONVERT TO XLS
                        </td>
                        <td width="5%" align="right">
                            <a href="SicknessAbsence_XLS.asp?CC_Sort=<%=OrderBy%><%=theURL%><%=DEFAULTstring%>">
                                <img src="../customer/Images/excel.gif" width="29" height="29"
                                    border="0" alt="Excel Arrears List" style="cursor: pointer" /></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4">
                <img src="images/spacer.gif" width="740" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="900" cellpadding="1" cellspacing="2" class="TAB_TABLE" style="border-collapse: collapse;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="#4682b4"
        border="7">
        <thead>
            <tr>
                <td class='NO-BORDER' colspan="10">
                    <b>Report : Sickness / Absence </b>
                </td>
                <td width="67" rowspan="2" align="left" valign="bottom" class='NO-BORDER'>
                    <b>Cost</b>
                </td>
            </tr>
            <tr align="left" valign="bottom">
                <td width="100" class='NO-BORDER'>
                    <b>Surname</b>
                </td>
                <td width="106" class='NO-BORDER'>
                    <b>First Name</b>
                </td>
                <td class='NO-BORDER'>
                    <b>Full/Part Time</b>
                </td>
                <td class='NO-BORDER'>
                    <b>Employee Start Date</b>
                </td>
                <td class='NO-BORDER'>
                    <b>Job Title</b>
                </td>
                <td width="111" class='NO-BORDER'>
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" border="0" width="11"
                            height="12" alt="Sort Descending" /></a> <b>Dept.</b> <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME  asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none">
                                <img src="../myImages/sort_arrow_up_old.gif" border="0" width="17"
                                    height="16" alt="Sort Ascending" /></a>
                </td>
                <td width="77" class='NO-BORDER'>
                    <b>Start Date</b>
                </td>
                <td width="117" class='NO-BORDER'>
                    <strong>End Date</strong>
                </td>
                <td width="54" class='NO-BORDER'>
                    <strong>Duration</strong>
                </td>
                <td width="216" class='NO-BORDER'>
                    <b>Reason</b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="11" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
