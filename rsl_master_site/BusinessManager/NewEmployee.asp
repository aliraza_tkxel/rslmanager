<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim lstTitles
Dim lstEthnicities
Dim lstMaritalStatus
Dim l_sortcode
Dim FullName

	l_sortcode = ""
	FullName = "<font color=red>New Employee</font>"

Dim rsEmp, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
If (EmployeeID = "") Then EmployeeID = -1 End If

SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
Call OpenDB()
Call OpenRs(rsLoader, SQL)

If (NOT rsLoader.EOF) Then
	l_title = rsLoader("TITLE")
	l_lastname = rsLoader("LASTNAME")
	l_middlename = rsLoader("MIDDLENAME")
	l_firstname = rsLoader("FIRSTNAME")
	l_dob = rsLoader("DOB")
	l_gender = rsLoader("GENDER")
	l_maritalstatus = rsLoader("MARITALSTATUS")
	l_ethnicity = rsLoader("ETHNICITY")
	l_ETHNICITYOTHER = rsLoader("ETHNICITYOTHER")
	' Bank 1
	l_bank = rsLoader("BANK")
	l_sortcode = rsLoader("SORTCODE")
	l_accountnumber = rsLoader("ACCOUNTNUMBER")
	l_accountname = rsLoader("ACCOUNTNAME")
	l_rollnumber = rsLoader("ROLLNUMBER")
	' Bank 2
	l_bank2 = rsLoader("BANK2")
	l_sortcode2nd = rsLoader("SORTCODE2ND")
	l_accountnumber2 = rsLoader("ACCOUNTNUMBER2")
	l_accountname2 = rsLoader("ACCOUNTNAME2")
	l_rollnumber2 = rsLoader("ROLLNUMBER2")
	l_religion = rsLoader("RELIGION")
	l_sexualorientation = rsLoader("SEXUALORIENTATION")
	l_disability = rsLoader("DISABILITY")
	l_religionother = rsLoader("RELIGIONOTHER")
	l_sexualorientationother = rsLoader("SEXUALORIENTATIONOTHER")
	l_disabilityother = rsLoader("DISABILITYOTHER")
	l_profile = rsLoader("PROFILE")

	ACTION = "AMEND"
	FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"

	If NOT isNull(l_sortcode) Then
		If Replace(l_sortcode, " ", "") <> "" Then
			alldata = trim("" & Replace(l_sortcode, " ", "") & "")
			l_sortcode1	= left(alldata,2)
			l_sortcode2	= mid(alldata,3,2)
			l_sortcode3	= right(alldata,2)
		End If
	End If

	If NOT isNull(l_sortcode2nd) Then
		If Replace(l_sortcode2nd, " ", "") <> "" Then
			alldata2 = trim("" & Replace(l_sortcode2nd, " ", "") & "")
			l_sortcode21 = left(alldata2,2)
			l_sortcode22 = mid(alldata2,3,2)
			l_sortcode23 = right(alldata2,2)
		End If
	End If
End If

Call CloseRs(rsLoader)

Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "---", l_title, "width:50px", "textbox", "onchange='title_change()' tabIndex='1'")
Call BuildSelect(lstEthnicities, "sel_ETHNICITY", "G_ETHNICITY", "ETHID, DESCRIPTION", "DESCRIPTION", "Please Select", l_ethnicity, NULL, "textbox200", " onchange='Disable_Other_Boxes()' tabIndex='7'")
Call BuildSelect(lstMaritalStatus, "sel_MARITALSTATUS", "G_MARITALSTATUS", "MARITALSTATUSID, DESCRIPTION", "DESCRIPTION", "Please Select", l_maritalstatus, NULL, "textbox200", "tabIndex='6'")
Call BuildSelect(lstReligion, "sel_RELIGION", "G_RELIGION", "RELIGIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_Religion, NULL, "textbox200", "onchange='Disable_Other_Boxes()' tabIndex='1'")
Call BuildSelect(lstSexualOrientation, "sel_SEXUALORIENTATION", "G_SEXUALORIENTATION", "SEXUALORIENTATIONID, DESCRIPTION", "DESCRIPTION", "Please Select", l_SexualOrientation, NULL, "textbox200", "onchange='Disable_Other_Boxes()' tabIndex='1'")

SQL = "SELECT DISABILITYID, DESCRIPTION FROM G_DISABILITY "
	Call OpenRS(rsDisability, SQL)
	Disability_String = ""
    If Not rsDisability.EOF Then
        Disability_String = Disability_String & "<table>"
        DisLinkCount = 0
        ' Do until the end of all records
        Do Until rsDisability.EOF
            'change 5 to any number of columns you like
            If DisLinkCount Mod 3 = 0 Then
			    If rsDisability("DESCRIPTION") = "Other" Then
			   	    DisOtherID = DisLinkCount + 1
				    placeOnClick = " onClick='Disable_Other_Boxes()' "
				Else
				    placeOnClick = ""
			    End If
                If DisLinkCount <> 0 Then Disability_String = Disability_String & "</tr>"
                    Disability_String = Disability_String &"<tr><td width=""24""><input type=""checkbox"" name='chk_Disability" & (DisLinkCount + 1) & "' id='chk_Disability" & (DisLinkCount + 1) & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
                Else
                    Disability_String = Disability_String &"<td width=""24""><input type=""checkbox"" name='chk_Disability" & (DisLinkCount + 1) & "' id='chk_Disability" & (DisLinkCount + 1) & "' value='" & rsDisability("DISABILITYID") & "' " & placeOnClick & "></td><td width=""150"">" & rsDisability("DESCRIPTION") & "</td>"
                End If
            DisLinkCount = DisLinkCount + 1
        'loop till end of records
        rsDisability.MoveNext
        Loop
        Disability_String = Disability_String & "</tr></table>"
    Else
            'Write no records in there are no records
            Response.Write"Sorry, no teams were found!"
    End If


 Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED @EmployeeId = " & EmployeeID
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

    Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="JavaScript">

        var isNN = (navigator.appName.indexOf("Netscape") != -1);
        function autoTab(input, len, e) {

            var keyCode = (isNN) ? e.which : e.keyCode;
            var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];

            if (input.value.length >= len && !containsElement(filter, keyCode)) {
                input.value = input.value.slice(0, len);
                input.form[(getIndex(input) + 1) % input.form.length].focus();
            }

            function containsElement(arr, ele) {
                var found = false, index = 0;
                while (!found && index < arr.length)
                    if (arr[index] == ele)
                        found = true;
                    else
                        index++;
                return found;
            }

            function getIndex(input) {
                var index = -1, i = 0, found = false;
                while (i < input.form.length && index == -1)
                    if (input.form[i] == input) index = i;
                    else i++;
                return index;
            }
            return true;
        }

        var FormFields = new Array();
        FormFields[0] = "sel_TITLE|Title|SELECT|N"
        FormFields[1] = "txt_FIRSTNAME|First Name|TEXT|Y"
        FormFields[2] = "txt_LASTNAME|Last Name|TEXT|Y"
        FormFields[3] = "sel_ETHNICITY|Ethnicity|SELECT|N"
        FormFields[4] = "txt_DOB|Date of Birth|DATE|Y"
        FormFields[5] = "sel_GENDER|Gender|SELECT|N"
        FormFields[6] = "sel_MARITALSTATUS|Marital Status|SELECT|N"
        FormFields[7] = "txt_ACCOUNTNUMBER|Account Number|INTEGER|N"
        FormFields[8] = "txt_SORTCODE|Sort Code|INTEGER|N"
        FormFields[9] = "txt_ROLLNUMBER|Roll Number|TEXT|N"
        FormFields[10] = "sel_SEXUALORIENTATION| Sexual Orientation|SELECT|N"
        FormFields[11] = "sel_RELIGION| Religion|SELECT|N"
        FormFields[12] = "txt_SEXUALORIENTATIONOTHER|Other Sexual Orientation|TEXT|N"
        FormFields[13] = "txt_RELIGIONOTHER|Other Religion|TEXT|N"
        FormFields[14] = "txt_ETHNICITYOTHER|Other Ethnicity|TEXT|N"
        FormFields[15] = "txt_MIDDLENAME|Middle Name|TEXT|N"
        FormFields[16] = "txt_ACCOUNTNUMBER2|Account Number for Bank 2|INTEGER|N"
        FormFields[17] = "txt_SORTCODE2ND|Sort Code for Bank 2|INTEGER|N"
        FormFields[18] = "txt_ROLLNUMBER2|Roll Number for Bank 2|TEXT|N"

        function SaveForm(dupCheck) {

            document.getElementById("txt_SORTCODE").value = document.getElementById("txt_SORTCODE1").value.toString() + document.getElementById("txt_SORTCODE2").value.toString() + document.getElementById("txt_SORTCODE3").value.toString()
            document.getElementById("txt_SORTCODE2ND").value = document.getElementById("txt_SORTCODE21").value.toString() + document.getElementById("txt_SORTCODE22").value.toString() + document.getElementById("txt_SORTCODE23").value.toString()

            if (!checkForm()) return false;

            // validate bank 1
            if (document.getElementById("txt_SORTCODE").value.length != 6 && document.getElementById("txt_SORTCODE").value.length != 0) {
                ManualError("img_SORTCODE", "The Sort Code must consist of 6 numbers made up of 3 sets of 2.", 0)
                return false;
            }
            if (document.getElementById("txt_ACCOUNTNUMBER").value.length != 8 && document.getElementById("txt_ACCOUNTNUMBER").value.length != 0) {
                ManualError("img_ACCOUNTNUMBER", "The Account Number must consist of 8 numbers.", 0)
                return false;
            }

            // validate bank 2
            if (document.getElementById("txt_SORTCODE2ND").value.length != 6 && document.getElementById("txt_SORTCODE2ND").value.length != 0) {
                ManualError("img_SORTCODE2ND", "The Sort Code must consist of 6 numbers made up of 3 sets of 2.", 0)
                return false;
            }
            if (document.getElementById("txt_ACCOUNTNUMBER2").value.length != 8 && document.getElementById("txt_ACCOUNTNUMBER2").value.length != 0) {
                ManualError("img_ACCOUNTNUMBER2", "The Account Number must consist of 8 numbers.", 0)
                return false;
            }

            // do form stuff
            if (dupCheck && document.getElementById("hid_Action").value == "NEW") {
                document.RSLFORM.action = "ServerSide/EmployeeCheck.asp"
                document.RSLFORM.target = "iEmployeeCheck"
                document.RSLFORM.submit()
            }
            else {
                document.RSLFORM.action = "ServerSide/NewEmployee_svr.asp"
                document.RSLFORM.target = ""
                document.RSLFORM.submit()
            }
        }

        function DisplayMatches() {
            document.RSLFORM.action = "EmployeeMatch.asp"
            document.RSLFORM.target = ""
            document.RSLFORM.submit()
        }

        function title_change() {
            if (document.getElementById("sel_TITLE").value == 1)
                document.getElementById("sel_GENDER").selectedIndex = 2;
            else if ((document.getElementById("sel_TITLE").value == 5) || (document.getElementById("sel_TITLE").value == 6))
                document.getElementById("sel_GENDER").selectedIndex = 0;
            else
                document.getElementById("sel_GENDER").selectedIndex = 1;
        }


        function Disable_Other_Boxes() {
            var OptionSelectsArray = new Array("ETHNICITY", "RELIGION", "SEXUALORIENTATION")
            for (i = 0; i < OptionSelectsArray.length; i++) {
                str = "sel_" + OptionSelectsArray[i]
                none_option = document.getElementById("" + str + "").options[document.getElementById("" + str + "").selectedIndex].text
                sub_str = "txt_" + OptionSelectsArray[i] + "OTHER"
                if (none_option.indexOf('Other') > -1) {
                    document.getElementById("" + sub_str + "").disabled = false
                    document.getElementById("" + sub_str + "").style.backgroundColor = "white"
                }
                else {
                    document.getElementById("" + sub_str + "").disabled = true
                    document.getElementById("" + sub_str + "").value = ""
                    document.getElementById("" + sub_str + "").style.backgroundColor = "#AEB3BD"
                }
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(2);preloadImages();checkForm();Disable_Other_Boxes()"
    onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <% If ACTION = "NEW" Then %>
    <table width="750" border="0" cellpadding="0" cellspacing="0" style="height: 27px">
        <tr>
            <td rowspan="2">
                <img name="E_PD" id="E_PD" src="Images/1-open.gif" width="115" height="20" border="0"
                    alt="Personal Details" />
            </td>
            <td rowspan="2">
                <img src="Images/TabEnd.gif" width="8" height="20" border="0" alt="Contacts" />
            </td>
            <td width="100%" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <% Else %>
    <table width="750" border="0" cellpadding="0" cellspacing="0" style="height: 27px">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img name="E_PD" src="Images/1-open.gif" width="115" height="20" border="0" alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" src="Images/2-previous.gif" width="74" height="20" border="0" alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" src="images/3-closed.gif" width="94" height="20" border="0" alt="Qualifications" /></a>
            </td>
            <td rowspan="2" style="display: none">
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" src="images/4-closed.gif" width="86" height="20" border="0" alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" src="Images/5-closed.gif" width="70" height="20" border="0" alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" src="Images/6-closed.gif" width="52" height="20" border="0" alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" id="E_B" src="images/7-closed.gif" width="82" height="20" border="0"
                        alt="Benefits" /></a>
            </td>
            <td width="265" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <% End If %>
    <form name="RSLFORM" method="post" action="ServerSide/NewEmployee_svr.asp" style="margin: 0px">
    <table cellspacing="0" cellpadding="2" style="height: 90%; border-left: 1px solid #133E71;
        border-bottom: 1px solid #133E71; border-right: 1px solid #133E71" width="750">
        <tr>
            <td nowrap="nowrap" width="110">
                Title:&nbsp;<%=lstTitles%>
                <img src="/js/FVS.gif" name="img_TITLE" id="img_TITLE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                First Name:
                <input type="text" class="textbox" name="txt_FIRSTNAME" id="txt_FIRSTNAME" style="width: 132px"
                    maxlength="30" value="<%=l_firstname%>" tabindex="2" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_FIRSTNAME" id="img_FIRSTNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td rowspan="2">
                Profile:<br />
                <span style="color: red">This will appear on the intranet </span>
            </td>
            <td rowspan="3">
                <textarea name="txt_PROFILE" id="txt_PROFILE" rows="10" cols="10" style="border: solid 1px #133E71;
                    width: 200px; height: 75px"><%=l_profile%></textarea>
            </td>
            <td>
                &nbsp;
            </td>
            <td width="100%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Middle Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_MIDDLENAME" id="txt_MIDDLENAME" maxlength="30"
                    value="<%=l_middlename%>" tabindex="3" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MIDDLENAME" id="img_MIDDLENAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td height="33">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Last Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_LASTNAME" id="txt_LASTNAME" maxlength="30"
                    value="<%=l_lastname%>" tabindex="3" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_LASTNAME" id="img_LASTNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td height="33">
                Date of Birth:
            </td>
            <td height="33">
                <input type="text" class="textbox200" name="txt_DOB" id="txt_DOB" value="<%=l_dob%>"
                    tabindex="4" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_DOB" id="img_DOB" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Gender:
            </td>
            <td>
                <select class="textbox200" name="sel_GENDER" id="sel_GENDER" tabindex="5">
                    <option value="">Please Select</option>
                    <%		
		            If ACTION = "AMEND" Then
			            If (l_gender = "Female") Then
				            FemaleSelected = " selected"
			            ElseIf (l_gender = "Male") Then
				            MaleSelected = " selected"
			            ElseIf (l_gender = "Prefer not to say") Then
				            PNTSSelected = " selected"
			            End If
		            End If
                    %>
                    <option value="Female" <%=FemaleSelected%>>Female</option>
                    <option value="Male" <%=MaleSelected%>>Male</option>
                    <option value="Prefer not to say" <%=PNTSSelected%>>Prefer not to say</option>
                </select>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_GENDER" id="img_GENDER" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <strong>Bank Details 1 </strong>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                <strong>Bank Details 2 (Optional) </strong>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Bank:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_BANK" id="txt_BANK" maxlength="40"
                    value="<%=l_bank%>" tabindex="8" readonly="readonly" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BANK" id="img_BANK" width="15px" height="15px" border="0"
                    alt="" />
            </td>
            <td>
                Bank:
            </td>
            <td>
                <input name="txt_BANK2" id="txt_BANK2" type="text" class="textbox200" tabindex="8"
                    value="<%=l_bank2%>" maxlength="40" readonly="readonly" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_BANK2" id="img_BANK2" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td height="33">
                Sort Code:
            </td>
            <td height="33">
                <input type="text" class="textbox" name="txt_SORTCODE1" id="txt_SORTCODE1" value="<%=l_sortcode1%>"
                    readonly="readonly" size="2" maxlength="2" tabindex="9" onkeyup="return autoTab(this, 2, event);" />
                -
                <input type="text" class="textbox" name="txt_SORTCODE2" id="txt_SORTCODE2" value="<%=l_sortcode2%>"
                    readonly="readonly" size="2" maxlength="2" tabindex="10" onkeyup="return autoTab(this, 2, event);" />
                -
                <input type="text" class="textbox" name="txt_SORTCODE3" id="txt_SORTCODE3" value="<%=l_sortcode3%>"
                    readonly="readonly" size="2" maxlength="2" tabindex="11" />
                <input type="hidden" name="txt_SORTCODE" id="txt_SORTCODE" value="<%=l_sortcode%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SORTCODE" id="img_SORTCODE" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td height="33">
                Sort Code:
            </td>
            <td height="33">
                <input name="txt_SORTCODE21" type="text" class="textbox" id="txt_SORTCODE21" readonly="readonly"
                    tabindex="9" onkeyup="return autoTab(this, 2, event);" value="<%=l_sortcode21%>"
                    size="2" maxlength="2" />
                -
                <input name="txt_SORTCODE22" type="text" class="textbox" id="txt_SORTCODE22" readonly="readonly"
                    tabindex="10" onkeyup="return autoTab(this, 2, event);" value="<%=l_sortcode22%>"
                    size="2" maxlength="2" />
                -
                <input name="txt_SORTCODE23" type="text" class="textbox" id="txt_SORTCODE23" readonly="readonly"
                    tabindex="11" value="<%=l_sortcode23%>" size="2" maxlength="2" />
                <input type="hidden" name="txt_SORTCODE2ND" id="txt_SORTCODE2ND" value="<%=l_sortcode2ND%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SORTCODE2ND" id="img_SORTCODE2ND" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Acc Number:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ACCOUNTNUMBER" id="txt_ACCOUNTNUMBER"
                    maxlength="8" value="<%=l_accountnumber%>" readonly="readonly" tabindex="12" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER" id="img_ACCOUNTNUMBER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td>
                Acc Number:
            </td>
            <td>
                <input name="txt_ACCOUNTNUMBER2" type="text" class="textbox200" id="txt_ACCOUNTNUMBER2"
                    readonly="readonly" tabindex="12" value="<%=l_accountnumber2%>" maxlength="8" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNUMBER2" id="img_ACCOUNTNUMBER2" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Acc Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ACCOUNTNAME" id="txt_ACCOUNTNAME"
                    maxlength="40" value="<%=l_accountname%>" readonly="readonly" tabindex="13" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNAME" id="img_ACCOUNTNAME" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                Acc Name:
            </td>
            <td>
                <input name="txt_ACCOUNTNAME2" type="text" class="textbox200" id="txt_ACCOUNTNAME2"
                    readonly="readonly" tabindex="13" value="<%=l_accountname2%>" maxlength="40" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ACCOUNTNAME2" id="img_ACCOUNTNAME2" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                Roll Number:
            </td>
            <td>
                <input name="txt_ROLLNUMBER" type="text" class="textbox200" id="txt_ROLLNUMBER" tabindex="13"
                    value="<%=l_rollnumber%>" maxlength="40" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ROLLNUMBER" id="img_ROLLNUMBER" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td>
                Roll Number:
            </td>
            <td>
                <input name="txt_ROLLNUMBER2" type="text" class="textbox200" id="txt_ROLLNUMBER2"
                    tabindex="13" value="<%=l_rollnumber2%>" maxlength="40" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ROLLNUMBER2" id="img_ROLLNUMBER2" width="15px" height="15px"
                    border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Marital Status:
            </td>
            <td>
                <%=lstMaritalStatus%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_MARITALSTATUS" id="img_MARITALSTATUS" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Ethnicity:
            </td>
            <td>
                <%=lstEthnicities%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ETHNICITY" id="img_ETHNICITY" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input name="txt_ETHNICITYOTHER" type="text" class="textbox200" id="txt_ETHNICITYOTHER"
                    tabindex="2" maxlength="50" value="<%=l_ETHNICITYOTHER%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ETHNICITYOTHER" id="img_ETHNICITYOTHER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Religion :
            </td>
            <td>
                <%=lstReligion%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RELIGION" id="img_RELIGION" width="15px" height="15px"
                    border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input name="txt_RELIGIONOTHER" type="text" class="textbox200" id="txt_RELIGIONOTHER"
                    tabindex="2" maxlength="50" value="<%=l_Religionother%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_RELIGIONOTHER" id="img_RELIGIONOTHER" width="15px"
                    height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Sexual Orientation:
            </td>
            <td>
                <%=lstSexualOrientation%>
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SEXUALORIENTATION" id="img_SEXUALORIENTATION" width="15px"
                    height="15px" border="0" alt="" />
            </td>
            <td nowrap="nowrap">
                &gt; Other :
            </td>
            <td>
                <input name="txt_SEXUALORIENTATIONOTHER" type="text" class="textbox200" id="txt_SEXUALORIENTATIONOTHER"
                    tabindex="2" maxlength="50" value="<%=l_SexualOrientation%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_SEXUALORIENTATIONOTHER" id="img_SEXUALORIENTATIONOTHER"
                    width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
            <td align="right">
                <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="button" name="BtnSaveNext" id="BtnSaveNext" class="RSLButton" value=" SAVE & NEXT "
                    title=" SAVE & NEXT " onclick="SaveForm(true)" tabindex="14" style="cursor: pointer" />
                <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />                
            </td>
        </tr>
        <tr>
            <td height="100%">
                &nbsp;
            </td>
        </tr>
    </table>
    <% If ACTION <> "NEW" Then %>
    <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
        <table>
            <tbody>
                <tr>
                    <td>
                        Updated By:
                    </td>
                    <td>
                        <%=str_UpdatedBy %>
                    </td>
                </tr>
                <tr>
                    <td>
                        Updated On:
                    </td>
                    <td>
                        <%=str_UpdatedOn %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="clear: both">
    </div>
    <% End If %>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe name="iEmployeeCheck" id="iEmployeeCheck" src="ServerSide/EmployeeCheck.asp?emp=-1"
        style="display: none" height="4" width="4"></iframe>
</body>
</html>