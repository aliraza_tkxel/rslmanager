<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	CONST CONST_PAGESIZE = 19
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id
	Dim searchName, searchSQL
	
	searchName = Replace(Request("name"),"'","''")
	if searchName <> "" Then
		searchSQL = " AND (FIRSTNAME LIKE '%" & searchName & "%' OR LASTNAME LIKE '%" & searchName & "%') "
	Else
		searchName = ""
	End If

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Replace(Request.QueryString("page"),"'","''")
	End If
	
	If Not IsNumeric(team_id) Then
		str_data =  "<BR><BR><BR><TR><TD CLASS='ERROR_TEXT'>Management System Error -- 'No team id supplied, please contact administrator'</TD></TR>"
	elseif team_id = "-1" Then 
		get_orphan_peeps()
	else
		getTeams()
	end if
	
	Function isAbsent(int_employeeid)
		
		Dim str_isabsent 
		str_isabsent = "No"
		OpenDB()
		SQL = "SELECT J.* FROM 	E__EMPLOYEE E 	INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 WHERE 	E.EMPLOYEEID = " & int_employeeid
		Call OpenRS(rsSet,SQL)
		if not rsSet.EOF Then
			str_isabsent = "Yes"
		End If
		CloseDB()

		isAbsent = str_isabsent	
		
	End Function
	
	
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT 	T.TEAMNAME,E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
						"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE,  " &_
						"			ISNULL(CAST(G.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE,  " &_
						"			ISNULL(J.HOLIDAYENTITLEMENTDAYS,0) - ISNULL(SUM(A.DURATION),0) AS REMAININGLEAVE, " &_
						"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
						"			CASE WHEN ISNULL(SUM(AHOL.DURATION),0) = 0 THEN 'No' ELSE 'Yes' END AS HOLREQUEST " &_
						"FROM	 	E__EMPLOYEE E  " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM  " &_
						"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID  " &_
						"			LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND " &_
						"			JNAL.CURRENTITEMSTATUSID = 	5 AND " &_
						"			JNAL.ITEMNATUREID = 2  " &_				
						"			LEFT JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.STARTDATE >=  '1 '+ 'JAN' + ' ' + CAST(DATEPART (YYYY, GETDATE()) AS VARCHAR)  AND " &_
						"				A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID) " &_
						"			LEFT JOIN E_JOURNAL JHOL ON J.EMPLOYEEID = JHOL.EMPLOYEEID  " &_
						"		    AND JHOL.CURRENTITEMSTATUSID = 3 " &_
						"			LEFT JOIN E_NATURE N ON JHOL.ITEMNATUREID = N.ITEMNATUREID AND APPROVALREQUIRED = 1 " &_
						"			LEFT JOIN E_ABSENCE AHOL ON JHOL.JOURNALID = AHOL.JOURNALID AND " &_
						"			AHOL.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JHOL.JOURNALID) " &_
						"WHERE	 	J.LINEMANAGER = " & Session("userid") & " AND J.ACTIVE = 1  " & searchSQL &_
						"GROUP	 	BY J.EMPLOYEEID, HOLIDAYENTITLEMENTDAYS, E.FIRSTNAME, E.LASTNAME, J.JOBTITLE, G.[DESCRIPTION], " &_
						"			T.TEAMNAME, E.EMPLOYEEID, J.APPRAISALDATE " &_ 
						"ORDER		BY HOLREQUEST DESC "
		
		
		strSQL = 		"SELECT 	T.TEAMNAME,E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
						"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE,  " &_
						"			ISNULL(CAST(G.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE,  " &_
						"			ISNULL(J.HOLIDAYENTITLEMENTDAYS,0) - ISNULL(SUM(A.DURATION),0) AS REMAININGLEAVE, " &_
						"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
						"			CASE WHEN ISNULL(SUM(AHOL.DURATION),0) = 0 THEN 'No' ELSE 'Yes' END AS HOLREQUEST " &_
						"FROM	 	E__EMPLOYEE E  " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM  " &_
						"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID  " &_
						"			LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND " &_
						"			JNAL.CURRENTITEMSTATUSID = 	5 AND " &_
						"			JNAL.ITEMNATUREID = 2  " &_				
						"			LEFT JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.STARTDATE >=  '1 '+ 'JAN' + ' ' + CAST(DATEPART (YYYY, GETDATE()) AS VARCHAR)  AND " &_
						"				A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID) " &_
						"			LEFT JOIN (SELECT MAX(JOURNALID) JOURNALID ,EMPLOYEEID FROM E_JOURNAL EJ LEFT JOIN E_NATURE N ON EJ.ITEMNATUREID = N.ITEMNATUREID AND APPROVALREQUIRED = 1  WHERE CURRENTITEMSTATUSID = 3 GROUP BY EMPLOYEEID) JHOL ON J.EMPLOYEEID = JHOL.EMPLOYEEID " &_
						"			LEFT JOIN E_ABSENCE AHOL ON JHOL.JOURNALID = AHOL.JOURNALID AND " &_
						"			AHOL.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JHOL.JOURNALID) " &_
						"WHERE	 	J.LINEMANAGER = " & Session("userid") & " AND J.ACTIVE = 1  " & searchSQL &_
						"GROUP	 	BY J.EMPLOYEEID, HOLIDAYENTITLEMENTDAYS, E.FIRSTNAME, E.LASTNAME, J.JOBTITLE, G.[DESCRIPTION], " &_
						"			T.TEAMNAME, E.EMPLOYEEID, J.APPRAISALDATE " &_ 
						"ORDER		BY HOLREQUEST DESC "
    		    
		            
		
		//response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				
				team_name = rsSet("TEAMNAME")
				APPDATE = rsSet("APPRAISALDATE")
				IF (ISDATE(APPDATE)) THEN
					APPDATE = FormatDateTime(APPDATE, 2)
				END IF
				str_data = str_data & "<TBODY><TR STYLE='CURSOR:HAND' ONCLICK='load_me(" & rsSet("EMPLOYEEID") & ")'>" &_
					"<TD WIDTH=240PX>" & rsSet("FULLNAME") & "</TD>" &_
					"<TD WIDTH=240PX ALIGN=LEFT>" & rsSet("JOBTITLE") & "</TD>" &_
					"<TD WIDTH=75PX ALIGN=CENTER>" & rsSet("GRADE") & "</TD>" &_
					"<TD WIDTH=75PX ALIGN=CENTER>" & rsSet("REMAININGLEAVE") & "</TD>"
					if isAbsent(rsSet("EMPLOYEEID")) = "Yes" Then 
						str_data = str_data & "<TD WIDTH=70PX ALIGN=CENTER><font color=red>" & isAbsent(rsSet("EMPLOYEEID")) & "</FONT></TD>" 
					else
						str_data = str_data & "<TD WIDTH=70PX ALIGN=CENTER>" & isAbsent(rsSet("EMPLOYEEID")) & "</TD>"
					end if
					
					if rsSet("HOLREQUEST") = "Yes" Then 
						str_data = str_data & "<TD WIDTH=70PX ALIGN=CENTER><font color=red>" & rsSet("HOLREQUEST") & "</FONT></TD>" 
					else
						str_data = str_data & "<TD WIDTH=70PX ALIGN=CENTER>" & rsSet("HOLREQUEST") & "</TD>"
					end if
					str_data = str_data & "</TR></TBODY>"
				
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=8 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'my_staff.asp?page=1&name=" & searchName & "&team_id=" & team_id & "'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'my_staff.asp?page=" & prevpage & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='my_staff.asp?page=" & nextpage & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='my_staff.asp?page=" & intPageCount & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"
			
		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<TFOOT><TR><TD COLSPAN=8 STYLE='FONT:18PX' ALIGN=CENTER>No members exist within this team !!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR></TFOOT>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

	Function get_orphan_peeps()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT 	E.EMPLOYEEID, " &_
					"			FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
					"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, ISNULL(CAST(J.GRADE AS NVARCHAR), 'N/A') AS GRADE, " &_
					"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
					"			'N/A' AS PENSIONSCHEME " &_
					"FROM	 	E__EMPLOYEE E " &_
					"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
					"WHERE	 	J.TEAM IS NULL "
					
		//response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				
				APPDATE = rsSet("APPRAISALDATE")
				IF (ISDATE(APPDATE)) THEN
					APPDATE = FormatDateTime(APPDATE, 2)
				END IF
				str_data = str_data & "<TBODY><TR STYLE='CURSOR:HAND' ONCLICK='load_me(" & rsSet("EMPLOYEEID") & ")'>" &_
					"<TD WIDTH=240PX>" & rsSet("FULLNAME") & "</TD>" &_
					"<TD WIDTH=240PX ALIGN=LEFT>N/A</TD>" &_
					"<TD WIDTH=45PX ALIGN=CENTER>N/A</TD>" &_
					"<TD WIDTH=75PX ALIGN=CENTER>N/A</TD>" &_
					"<TD WIDTH=90PX ALIGN=CENTER>N/A</TD>" &_
					"<TD WIDTH=70PX ALIGN=CENTER>N/A</TD>" &_
					"</TR></TBODY>"
				
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=8 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A HREF = 'my_staff.asp?page=1'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'my_staff.asp?page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='my_staff.asp?page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='my_staff.asp?page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"
			
		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<TFOOT><TR><TD COLSPAN=8 STYLE='FONT:18PX' ALIGN=CENTER>No members exist within this team !!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR></TFOOT>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function


		// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=8 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --> HR Tools --> Team</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
		function load_me(employee_id){
		
			location.href = "../myjob/erm.asp?employeeid=" + employee_id;
		
		}
	function quick_find(){
	
		location.href = "my_staff.asp?name="+thisForm.findemp.value;
		
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=post>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=2><IMG NAME="tab_establishment" TITLE='<%=team_name%>' SRC="Images/tab_my_staff.gif" WIDTH=81 HEIGHT=20 BORDER=0></TD>
      <!--<TD ROWSPAN=2><IMG NAME="tab_establishment" SRC="Images/TabEnd.gif" WIDTH=8 HEIGHT=20 BORDER=0></TD>-->	  
      <!--TD><IMG SRC="images/spacer.gif" WIDTH=630 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT>
		<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0><TR>
				
            <TD ALIGN=RIGHT> 
              <input type='BUTTON' name='btn_FIND' title='Search for matches using both first and surname.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'>
              &nbsp; 
              <input type=text  size=18 name='findemp' class='textbox200'>
				</TD></TR>
			</TABLE>
		</TD>	
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71 colspan=4><IMG SRC="images/spacer.gif" WIDTH=669 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD WIDTH=240PX CLASS='NO-BORDER' COLSPAN=3> <B>Team: <%=team_name%></B> </TD>
      <TD ALIGN=CENTER WIDTH=90PX  CLASS='NO-BORDER' rowspan=2 valign=bottom> <B>Remaining Leave</B></TD>
      <TD ALIGN=CENTER WIDTH=90PX CLASS='NO-BORDER' rowspan=2 valign=bottom> <B>Absent</B> </TD>
    </TR>
    <TR> 
      <TD WIDTH=240PX CLASS='NO-BORDER'> <B>Name</B> </TD>
      <TD WIDTH=240PX ALIGN=LEFT CLASS='NO-BORDER'> <B>Job Title</B> </TD>
      <TD ALIGN=CENTER WIDTH=70PX CLASS='NO-BORDER'> <B>Grade</B> </TD>
      <TD ALIGN=CENTER WIDTH=70PX CLASS='NO-BORDER'> <B>Leave Request</B> </TD>
    </TR>
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=8 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    </THEAD> <%=str_data%> 
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

