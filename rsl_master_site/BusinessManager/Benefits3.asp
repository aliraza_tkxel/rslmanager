<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%

	//Benefit select//
	dim lstbenefittype

	Dim rsLoader, ACTION

	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
	if (EmployeeID = "") then EmployeeID = -1 end if
	
	OpenDB()
	
	SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsName, SQL)
	
	If NOT rsName.EOF Then
		l_lastname = rsName("LASTNAME")
		l_firstname = rsName("FIRSTNAME")	
		FullName = "<font color=blue>" & l_firstname & " " & l_lastname & "</font>"
	
	End If
	Call CloseRs(rsName)
	
	SQL = "SELECT * FROM E_BENEFITS WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsLoader, SQL)
	
	if (NOT rsLoader.EOF) then
		
		' tbl E_BENEFITS
		//Pension//
		l_pension = rsLoader("PENSION")
		l_salarypercent = rsLoader("SALARYPERCENT")
		l_contribution = rsLoader("CONTRIBUTION")
		l_avc = rsLoader("AVC")
		l_contractedout = rsLoader("CONTRACTEDOUT")
		l_professionalfess = rsLoader("PROFESSIONALFEES")
		l_telephoneallowance = rsLoader("TELEPHONEALLOWANCE")	
		l_bupa = rsLoader("BUPA")						
		l_bupaexcess = rsLoader("BUPAEXCESS")						
		l_firstaidallowance = rsLoader("FIRSTAIDALLOWANCE")						
		l_calloutallowance = rsLoader("CALLOUTALLOWANCE")						
		l_car = rsLoader("CAR")						
		l_cartype = rsLoader("CARTYPE")
		l_capacity = rsLoader("CAPACITY")						
		l_co2 = rsLoader("CO2")						
		l_listprice = rsLoader("LISTPRICE")						
		l_excesscontribution = rsLoader("EXCESSCONTRIBUTION")						
		l_carallowance = rsLoader("CARALLOWANCE")
	
		l_carloanstartdate = rsLoader("CARLOANSTARTDATE")						
		l_term = rsLoader("TERM")						
		l_value = rsLoader("VALUE")
		ACTION = "AMEND"
	end if
	
	CloseRs(rsLoader)
	
	Call BuildSelect(lstscheme, "sel_SCHEME", "G_SCHEME", "SCHEMEID, SCHEMEDESC", "SCHEMEDESC", "Please Select", b_schemeid, NULL, "textbox200", " TABINDEX=2 " )	
	Call BuildSelect(lstbenefittype, "sel_BENEFITTYPE", "E_BENTYPE", "BENEFITTYPEID, BENEFITTYPEDESC", "BENEFITTYPEDESC", "Please Select", b_benefitid, NULL, "textbox200", " onChange='OpenDiv()'" )	
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidationExtras.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

function OpenDiv()
{
	//Retrieve value from benefit type and display the correct Div according to Benefit Type Selected
	DisplayDiv = document.getElementById("sel_BENEFITTYPE").value
	
	var arrBenefitSection = new Array("Pension", "CompanyCar", "Accomodation", "General", "BUPA", "Essential_User");
	
	for(intDiv = 0 ; intDiv < (arrBenefitSection.length) ; intDiv++ )
	{
		if ((intDiv) == DisplayDiv - 1)
		{	
				document.getElementById("div_" + arrBenefitSection[intDiv] + "").style.display = "block";
		}
		else 
		{
				document.getElementById("div_" + arrBenefitSection[intDiv] + "").style.display = "none";
		}
	}
	
}


function TogglePension(){
	if (document.getElementsByName("rdo_PENSION")[0].checked) EnableItems("1,2,3,4")
	else DisableItems("1,2,3,4")
	}

function ToggleCar(){
	if (document.getElementsByName("rdo_CAR")[0].checked) EnableItems("12,13,14,15,16")
	else DisableItems("12,13,14,15,16")
	}

function ToggleBupa(){
	if (document.getElementsByName("rdo_BUPA")[0].checked) EnableItems("8")
	else DisableItems("8")
	}
	
function ToggleCarLoan()
{
	//alert(document.getElementsByName("rdo_LoanInformation")[0].checked)
	if (document.getElementsByName("rdo_LoanInformation")[0].checked == true)
		{
			document.getElementById("tby_LoanInformation").style.display = "block"
		}
	else
		{
			document.getElementById("tby_LoanInformation").style.display = "none"
		}
	
}

var FormFields = new Array();

function CorrectValidationFields()
{
	//Retrieve value from Benefit Type and validate fields according to value received.
	var DisplayDiv = document.getElementById("sel_BENEFITTYPE").value
	var FormFields = new Array();
	switch(DisplayDiv)
	{
	case "1":
		//Pension//
		FormFields[0] = "rdo_PENSION|Pension|RADIO|Y"
		FormFields[1] = "txt_SALARYPERCENT|Salary|CURRENCY|I_N"
		FormFields[2] = "txt_CONTRIBUTION|Contribution|CURRENCY|I_Y"
		FormFields[3] = "txt_AVC|AVC|CURRENCY|I_N"
		FormFields[4] = "rdo_CONTRACTEDOUT|Contracted Out|RADIO|I_Y"
		FormFields[5] = "txt_MEMNUMBER|Member Number|TEXT|N"
		FormFields[6] = "sel_SCHEME|Scheme|SELECT|N"
	break
	case "2":
		//Company Car//
		FormFields[0] = "txt_CARMAKE|Car Make|TEXT|N"
		FormFields[1] = "txt_MODEL|Car Model|TEXT|N"
		FormFields[2] = "txt_LISTPRICE|List Price|CURRENCY|I_Y"		
		FormFields[3] = "txt_CARTYPE|Car Type|TEXT|I_Y"
		FormFields[4] = "txt_CONTHIRECHARGE|Contract Hire Charge|CURRENCY|N"	
		FormFields[5] = "txt_EMPCONTRIBUTION|Employers Contribution|CURRENCY|N"								
		FormFields[6] = "txt_EXCESSCONTRIBUTION|Excess Contribution|CURRENCY|I_Y"
	break
	case "3":
		//Accomodation//
		FormFields[0] = "txt_AccomodationRent|Accomodation Rent|CURRENCY|N"
		FormFields[1] = "txt_CouncilTax|Council Tax|CURRENCY|N"
		FormFields[2] = "txt_Heating|Heating|CURRENCY|N"
		FormFields[3] = "txt_LineRental|Line Rental|CURRENCY|N"
	break
	case "4":
		//General//	
		FormFields[0] = "txt_PROFESSIONALFEES|Professional Fees|CURRENCY|N"
		FormFields[1] = "txt_TELEPHONEALLOWANCE|Telephone allowance|CURRENCY|N"	
		FormFields[2] = "rdo_BUPA|Team|RADIO|Y"
		FormFields[3] = "txt_BUPAEXCESS|Bupa Excess|CURRENCY|I_Y"
		FormFields[4] = "txt_FIRSTAIDALLOWANCE|First Aider Allowance|CURRENCY|N"	
		FormFields[5] = "txt_CALLOUTALLOWANCE|Call Out Allowance|CURRENCY|N"
	break
	case "5":
		//Bupa//
		FormFields[0] = "txt_AnnualPremium|Annual Premium|CURRENCY|N"
		FormFields[1] = "txt_AdditionalMembers|Term|TEXT|N"
	break
	case "6":
		//Essential User//
		FormFields[0] = "txt_CARALLOWANCE|Car Allowance|CURRENCY|N"						
		FormFields[1] = "txt_CARLOANSTARTDATE|Car Loan Start Date|DATE|N"						
		FormFields[2] = "txt_TERM|Term|INTEGER|N"
		FormFields[3] = "rdo_LOANINFORMATION|Loan|RADIO|I_Y"									
		if (document.getElementsByName("rdo_LoanInformation")[0].checked == true)
			{
				FormFields[4] = "txt_VALUE|Value �|CURRENCY|N"
				FormFields[5] = "txt_ENGINESIZE|Engine Size|INTEGER|N"
				FormFields[6] = "txt_TERM|Loan Term|INTEGER|N"
				FormFields[7] = "txt_MONTHLYREPAY|Monthly Repayments|CURRENCY|N"
			}
	break
	default :
			FormFields[0] = "sel_BENEFITTYPE|Benefit Type|SELECT|Y"
	break	
		
	}
		alert(FormFields)				
}	

function NotANumber(TextBox)
{
	
	TextBoxValue = document.getElementById("" + TextBox + "").value
	
	if(isNaN(TextBoxValue) == true )
	{
		alert("Must be a numeric value")
		document.getElementById("" + TextBox + "").value = ""
		return false
	}
}

//	
function CalculateMonthRepay()
{
	document.getElementById("txt_MONTHLYREPAY").value = "";
	
	TotalValue 	= document.getElementById("txt_VALUE").value;
	Term		= document.getElementById("txt_TERM").value;
	
	//if(NotANumber("txt_VALUE") == false)
	//	return false;
	//if(NotANumber("txt_TERM") == false)
	//	return false;
	
	if((TotalValue != "") &&  (Term != "") )
	{
		document.getElementById("txt_MONTHLYREPAY").value = TotalValue / Term;
	}
	else
		document.getElementById("txt_MONTHLYREPAY").value = "";
}
							


function LoadForm(ID){
	
		RSLFORM.hid_Action.value = "LOAD"
		RSLFORM.hid_WARRANTYID.value = ID
		RSLFORM.action = "ServerSide/Benefits_svr.asp"
		RSLFORM.target = "frm_ben"	
		RSLFORM.submit()
	}


	function SaveForm(){
		if (!checkForm()) return false
		RSLFORM.action = "ServerSide/Benefits_svr.asp"
		RSLFORM.target = "frm_ben"
		RSLFORM.submit()
		} 
	
	function SetButtons(Code){
	
		if (Code == "AMEND") {
			document.getElementById("AddButton").style.display = "none"
			document.getElementById("AmendButton").style.display = "block"		
			}
		else {
			document.getElementById("AddButton").style.display = "block"
			document.getElementById("AmendButton").style.display = "none"		
			}
	}
	
	function ResetForm(){
	
		RSLFORM.sel_WARRANTYTYPE.value = ""
		RSLFORM.sel_AREAITEM.value = ""
		RSLFORM.sel_CONTRACTOR.value = ""
		RSLFORM.txt_EXPIRYDATE.value = ""
		RSLFORM.hid_WARRANTYID.value = ""					
		RSLFORM.hid_Action.value = "NEW"
		SetButtons('NEW')
	}	

	function AmendForm(){
	
		RSLFORM.hid_Action.value = "AMEND"
		if (!checkForm()) return false;
		RSLFORM.action = "ServerSide/Benefits_svr.asp"
		RSLFORM.target = "frm_ben"
		RSLFORM.submit()
	}

	function DeleteMe(WarID){
		event.cancelBubble = true;
		answer = window.confirm("Are you sure you wish to delete this warranty?\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
		if (answer) {
			RSLFORM.hid_Action.value = "DELETE"
			RSLFORM.hid_WARRANTYID.value = WarID			
			RSLFORM.action = "ServerSide/warranty_svr.asp"
			RSLFORM.target = "frm_ben"
			RSLFORM.submit()
			}
		}
</SCRIPT>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method="post">
<table width=750 border=0 cellpadding=0 cellspacing=0>
	<tr> 
		
    <td rowspan=2> <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details"><img title="Personal Details" name="E_PD" src="Images/1-closed.gif" width=115 height=20 border=0></a></td>
		<td rowspan=2> <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info"><img alt="Contacts" name="E_C" src="images/2-closed.gif" width=74 height=20 border=0></a></td>
		<td rowspan=2> <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications"><img alt="Qualifications" name="E_SQ" src="images/3-closed.gif" width=94 height=20 border=0></a></td>
		<td rowspan=2> <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment"><img alt="Previous Employment" name="E_PE" src="images/4-closed.gif" width=86 height=20 border=0></a></td>
		<td rowspan=2> <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities"><img alt="Difficulties/Disabilities" name="E_DD" src="Images/5-closed.gif" width=70 height=20 border=0></a></td>
		<td rowspan=2> <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details"><img alt="Job Details" name="E_JD" src="Images/6-closed.gif" width=52 height=20 border=0></a></td>
		<td rowspan=2> <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits"><img alt="Benefits" name="E_B"  src="Images/7-open.gif" width=82 height=20 border=0></a></td>
		<td width=177 height=19 align="right"><%=FullName%></td>
	</tr>
	<tr> 
		<td bgcolor=#133E71> <img src="images/spacer.gif" width=53 height=1></td>
	</tr>
	<tr> 
		<td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="images/spacer.gif" width=53 height=6></td>
	</tr>
	</table>
	<TABLE cellspacing=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 >
 	<tr>
		<td width='30%'>Benefit Type</td>
		<td><%=lstbenefittype%><image src="/js/FVS.gif" name="img_BENEFITTYPE" width="15px" height="15px" border="0"></TD>
	</tr>
	</table>
		<div id="div_Pension" style='display:none'>
		<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' >
				<TR> 
					<TD nowrap colspan="2"><b>PENSION</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
				</TR>
				<tr>
					<td width='30%'>Membership Number:</td>
					<td><input type="text" name="txt_MEMNUMBER" class="textbox200" maxlength=20 value="<%=l_membernumber%>"></td>
				</tr>
				<tr>
					<td>Scheme Name:</td>
					<td><%=lstscheme%></td>
				</tr>
				<tr>
					<TD nowrap>Pension:</TD>
					<TD> 
					<% 
					yesChecked = "CHECKED"
					noChecked = ""
					if ACTION = "AMEND" then
						if (l_pension = 1) then
							yesChecked = "CHECKED"
							noChecked = ""
						elseif (l_car = 0) then
							yesChecked = ""
							noChecked = "CHECKED"
						end if
					end if
					%>
					<input type="RADIO" name="rdo_PENSION" value="1" <%=yesChecked%> onclick="TogglePension()" tabindex=1>
					YES 
					<input type="RADIO" name="rdo_PENSION" value="0" <%=noChecked%> onclick="TogglePension()" tabindex=1>
					NO </TD>
					<TD><image src="/js/FVS.gif" name="img_PENSION" width="15px" height="15px" border="0"></TD>
					
				</TR>
				<TR> 
					<TD nowrap>Salary %:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_SALARYPERCENT" maxlength=10 value="<%=l_salarypercent%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_SALARYPERCENT" width="15px" height="15px" border="0"></TD>
					
				</TR>
				<TR> 
					<TD nowrap>Contribution &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CONTRIBUTION" maxlength=10 value="<%=l_contribution%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_CONTRIBUTION" width="15px" height="15px" border="0"></TD>
				
				</TR>
				<TR> 
					<TD nowrap>AVC &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_AVC" maxlength=10 value="<%=l_avc%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_AVC" width="15px" height="15px" border="0"></TD>
					
				</TR>
				<TR> 
					<TD nowrap>Contracted Out:</TD>
					<TD> 
					<% 
					yesChecked = ""
					noChecked = ""
					if ACTION = "AMEND" then
						if (l_contractedout = 1) then
							yesChecked = "CHECKED"
							noChecked = ""
						elseif (l_contractedout = 1) then		
							yesChecked = ""
							noChecked = "CHECKED"
						end if
					end if
					%>
					<input type="RADIO" name="rdo_CONTRACTEDOUT" value="1" <%=yesChecked%>    tabindex=1>
					YES 
					<input type="RADIO" name="rdo_CONTRACTEDOUT" value="0" <%=noChecked%>    tabindex=1>
					NO </TD>
					<TD><image src="/js/FVS.gif" name="img_CONTRACTEDOUT" width="15px" height="15px" border="0"></TD>
					
				</TR>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>
				<TR> 
					<TD nowrap colspan="2">&nbsp; </TD>
					<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
				</TR>
			</table>
			</div>

			<div id="div_CompanyCar" style='display:none'>
			<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71'  >
				<tr>
					<TD colspan="2"><b>COMPANY CAR</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD width='30%'>Car:</TD>
					<TD> 
					<% 
					yesChecked = "CHECKED"
					noChecked = ""
					if ACTION = "AMEND" then
						if (l_car = 1) then
							yesChecked = "CHECKED"
							noChecked = ""
						elseif (l_car = 0) then
							yesChecked = ""
							noChecked = "CHECKED"
						end if
					end if
					%>
					<input type="RADIO" name="rdo_CAR" value="1" <%=yesChecked%> onclick="ToggleCar()" tabindex=2>
					YES 
					<input type="RADIO" name="rdo_CAR" value="0" <%=noChecked%> onclick="ToggleCar()" tabindex=2>
					NO </TD>
					<TD><image src="/js/FVS.gif" name="img_CAR" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>Make:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CARMAKE" maxlength=80 value="<%=l_carmake%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARTYPE" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>Model:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_MODEL" maxlength=10 value="<%=l_model%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CAPACITY" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>List Price &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_LISTPRICE" maxlength=10 value="<%=l_listprice%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_LISTPRICE" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>Contract Hire Charge &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CONTHIRECHARGE" maxlength=10 value="<%=l_conthirecharge%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CO2" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>Employers Contribution &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_EMPCONTRIBUTION" maxlength=10 value="<%=l_empcontribution%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CO2" width="15px" height="15px" border="0"></TD>
				</tr>
				<tr>
					<TD>Excess &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_EXCESSCONTRIBUTION" maxlength=10 value="<%=l_excesscontribution%>"    tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_EXCESSCONTRIBUTION" width="15px" height="15px" border="0"></TD>
				</tr>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>
			</table>
			</div>

			<div id="div_General"  style='display:none'>
			<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' >	
				<TR> 
					<TD nowrap colspan="2"><b>GENERAL</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
					<TD colspan="2">&nbsp;</TD>
				</TR>
				<TR> 
					<TD nowrap width='30%'>Professional Fees &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_PROFESSIONALFEES" maxlength=10 value="<%=l_professionalfess%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_PROFESSIONALFEES" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD nowrap>Telephone Allowance &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_TELEPHONEALLOWANCE" maxlength=10 value="<%=l_telephoneallowance%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_TELEPHONEALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD nowrap>BUPA:</TD>
					<TD> 
					<% 
					yesChecked = ""
					noChecked = "CHECKED"
					if ACTION = "AMEND" then
						if (l_bupa = 1) then
							yesChecked = "CHECKED"
							noChecked = ""
						end if
					end if
					%>
					<input type="RADIO" name="rdo_BUPA" value="1" <%=yesChecked%> onclick="ToggleBupa()" tabindex=1>
					YES 
					<input type="RADIO" name="rdo_BUPA" value="0" <%=noChecked%> onclick="ToggleBupa()" tabindex=1>
					NO </TD>
					<TD><image src="/js/FVS.gif" name="img_BUPA" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD nowrap>BUPA Excess &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_BUPAEXCESS" maxlength=10 value="<%=l_bupaexcess%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_BUPAEXCESS" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD nowrap>First Aider Allowance &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_FIRSTAIDALLOWANCE" maxlength=10 value="<%=l_firstaidallowance%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_FIRSTAIDALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD nowrap>Call Out Allowance &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CALLOUTALLOWANCE" maxlength=10 value="<%=l_calloutallowance%>" tabindex=1></TD>
					<TD><image src="/js/FVS.gif" name="img_CALLOUTALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>
				</table>
				</div>
	
			<div id="div_Essential_User"  style='display:none'>
			<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' >	
				<TR> 
					<TD colspan="2"><b>CAR ESSENTIAL USER</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD width='30%'>Car Allowance &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CARALLOWANCE" maxlength=10 value="<%=l_carallowance%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Car Loan Start Date:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_CARLOANSTARTDATE" maxlength=10 value="<%=l_carloanstartdate%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARLOANSTARTDATE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Engine Size:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_ENGINESIZE" maxlength=10 value="<%=l_engizesize%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_ENGINESIZE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Allowance &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_ALLOWANCE" maxlength=10 value="<%=l_allowance%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_ALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<tr>
					<TD nowrap>Car Loan &pound;:</TD>
						<TD> 
						<% 
						yesChecked = ""
						noChecked = "CHECKED"
						if ACTION = "AMEND" then
							if (l_carloan = 1) then
								yesChecked = "CHECKED"
								noChecked = ""
							end if
						end if
						%>
						  <input type="RADIO" name="rdo_LOANINFORMATION" value="1" <%=yesChecked%> onClick="ToggleCarLoan()" tabindex=1>
						  YES 
						  <input type="RADIO" name="rdo_LOANINFORMATION" value="0" <%=noChecked%> onclick="ToggleCarLoan()" tabindex=1>
						NO </TD>
						<TD><image src="/js/FVS.gif" name="img_LOANINFORMATION" width="15px" height="15px" border="0"></TD>
				</tr>
				<tbody id="tby_LoanInformation" name="tby_LoanInformation" style='display:none'>
					<TR> 
						<TD>Loan Value &pound;:</TD>
						<TD><input type="textbox" class="textbox200" name="txt_VALUE" id="txt_VALUE" maxlength=10 value="<%=l_value%>" tabindex=2 onkeyup="CalculateMonthRepay()"></TD>
						<TD><image src="/js/FVS.gif" name="img_VALUE" width="15px" height="15px" border="0"></TD>
					</TR>
					<TR> 
						<TD>Term:</TD>
						<TD><input type="textbox" class="textbox200" name="txt_TERM" id="txt_TERM" maxlength=10 value="<%=l_term%>" tabindex=2 onkeyup="CalculateMonthRepay()"></TD>
						<TD><image src="/js/FVS.gif" name="img_TERM" width="15px" height="15px" border="0"></TD>
					</TR>
					<TR> 
						<TD>Monthly Repayments &pound;</TD>
						<TD><input type="textbox" class="textbox200" name="txt_MONTHLYREPAY" id="txt_MONTHLYREPAY" maxlength=10 value="" readonly tabindex=2></TD>
						<TD><image src="/js/FVS.gif" name="img_MONTHLYREPAY" width="15px" height="15px" border="0"></TD>
					</TR>
				</tbody>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>	
				</table>
				</div>

			<div id="div_Accomodation"  style='display:none'>
			<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' >	
				<TR> 
					<TD colspan="2"><b>Accomodation</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD width='30%'>Rent &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_ACCOMODATIONRENT" maxlength=10 value="<%=l_accomodationrent%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Council Tax &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_COUNCILTAX" maxlength=10 value="<%=l_counciltax%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARLOANSTARTDATE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Heating &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_HEATING" maxlength=10 value="<%=l_heating%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_TERM" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Line Rental &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_LINERENTAL" maxlength=10 value="<%=l_linerental%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_VALUE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>
				</table>
				</div>
		
			<div id="div_BUPA"  style='display:none'>
			<table WIDTH=750 HEIGHT=300 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' >	
				<TR> 
					<TD colspan="2"><b>BUPA</b> </TD>
					<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					
        <TD width='30%'>Annual Premium &pound;:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_ANNUALPREMIUM" maxlength=10 value="<%=l_annualpremium%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARALLOWANCE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR> 
					<TD>Additional Members:</TD>
					<TD><input type="textbox" class="textbox200" name="txt_ADDITIONALMEMBERS" maxlength=10 value="<%=l_additionalmembers%>" tabindex=2></TD>
					<TD><image src="/js/FVS.gif" name="img_CARLOANSTARTDATE" width="15px" height="15px" border="0"></TD>
				</TR>
				<TR>
					<TD HEIGHT='100%'>&nbsp;</TD>
				</TR>
				</table>
				</div>
		<table>				
			<TR> 
				<TD valign=top > 
				<input type=hidden name="hid_EmployeeID" value="<%=EmployeeID%>">
				<input type=hidden name="hid_Action" value="<%=ACTION%>">
				<input type=button class="RSLButton" value=" SAVE & NEXT " onclick="{CorrectValidationFields();SaveForm()}">
        		<input type=button class="RSLButton" value=" AMEND " onClick="CorrectValidationFields();AmendForm()" name="AmendButton" style='display:none;width:70px'>
      </TD>
			<TD align=right valign=top> 
					<input type=button class="RSLButton" style='width:70px' value=" RESET " onclick="ResetForm()">
				</TD>
        		<td valign=top>
					<input type=button class="RSLButton" style='width:70px' value=" ADD " onclick="SaveForm()" name="AddButton">
      </TD>
    </TR>	
	
</TABLE>
  
  
 
        <input type=hidden name="hid_PropertyID" value="<%=PropertyID%>">
		<input type=hidden name="hid_WARRANTYID" value="<%=warrantyID%>">
        <input type=hidden name="hid_Action" value="<%=ACTION%>">
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_ben width=400px height=400px style='display:NONE'></iframe>
</BODY>
</HTML>

