<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim rsLoader, ACTION, lstprobations, lstpartfulltimes, lstteams, lstpatchs, lstnotice, lstgrades, lstlinemanager, lstbuddy
	Dim lstHolidayRule, lstHoliday , lstHolidayIndicator, l_workDays, l_totalleave_new, l_bankholiday_new, UserId, lstTrade,lstJOBTITLES,strDisplaySalaryAmendment

	l_team = -1
	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
	UserId= Session("UserId")
    
	If (EmployeeID = "") Then EmployeeID = -1 End If

	Call OpenDB()
	' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
	SQL = "SELECT dbo.E_WORK_DAYS_IN_WEEK(" & EmployeeID & ") AS WORKDAYS"
	Call OpenRs(rsDays, SQL)
		l_workDays = rsDays("WORKDAYS")
	Call CloseRs(rsDays)

	SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsName, SQL)
	If NOT rsName.EOF Then
		l_lastname = rsName("LASTNAME")
		l_firstname = rsName("FIRSTNAME")
		FullName =  l_firstname & " " & l_lastname 
		l_rolefile = rsName("ROLEPATH")
        l_employmentcontract = rsName("EmploymentContracts")
		If isnull(l_rolefile) Or l_rolefile = "" Then
			l_rolefile_display = "none"
		Else   
			l_rolefile_display = "block"	
			'l_rolefile = "RoleFiles/" & l_rolefile
			l_rolefile = l_rolefile
		End If	

        If isnull(l_employmentcontract) Or l_employmentcontract = "" Then
			l_employmentcontract_display = "none"
		Else   
			l_employmentcontract_display = "block"	
			'l_rolefile = "RoleFiles/" & l_rolefile
			l_employmentcontract = l_employmentcontract
		End If	
	End If
	Call CloseRs(rsName)
    '-----------------------------
    ' Salary Amendments
    '-----------------------------

    SQL = "Select * from E_SalaryAmendment where Status= 1 AND EmployeeId = " & EmployeeID 
    Call OpenRs(rsSalaryAmendment, SQL) 
    l_SalaryAmendment_display="none"  
    strDisplaySalaryAmendment = "<table border='0' width='100%'>"
	    If (NOT rsSalaryAmendment.EOF) Then
            while (NOT rsSalaryAmendment.EOF)				   
					    strDisplaySalaryAmendment = strDisplaySalaryAmendment & "<tr><td style=' width:70%;' ><a href='/Rolefile/SalaryAmendments/"& rsSalaryAmendment("SalaryAmendmentFile") &_
                         "' target='_blank'>  " & rsSalaryAmendment("AmendmentFileName")&_
                         "</a></td><td style=' width:30%;'><a href='JavaScript://' onclick='RemoveSalaryAmendment(" & rsSalaryAmendment("SalaryAmendmentId")& ")' style='text-decoration: none;color:Red; text-align:right;'>Remove </a></td></tr> "
				  
				    rsSalaryAmendment.moveNext()
				    
			    Wend
                 l_SalaryAmendment_display="block"  
        End If
        strDisplaySalaryAmendment = strDisplaySalaryAmendment & "</table>"
	Call CloseRs(rsSalaryAmendment)


    '-----------------------------
    'Auto BH adjustment calculation
    '-----------------------------

	Dim cmd 
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.ActiveConnection = RSL_CONNECTION_STRING
	cmd.CommandText = "E_HOLIDAYADJUSTMENTS"
	cmd.CommandTimeout = 5
	cmd.CommandType = 4 'same as adCmdStoredProc
	'cmd.Prepared = true
	
	' type = 3 Int, 4 Single, 16 TinyInt
	' direction  1 input, 2 output, 3, inputoutput, 4 returnval
	'
	'										  (name,						type,		direction,		size,		value)
	cmd.Parameters.Append(cmd.CreateParameter("@employeeId", 				3, 			1, 				11, 		EmployeeId))
	cmd.Parameters.Append(cmd.CreateParameter("@print", 					16, 		1, 				3, 			0))   
	cmd.Parameters.Append(cmd.CreateParameter("@fteholidayentitlement", 	3, 			1, 				11, 		27))   	
	cmd.Parameters.Append(cmd.CreateParameter("@AdjHours", 					4, 			4))
	cmd.Parameters.Append(cmd.CreateParameter("@AdjDays", 					4, 			4))    
	cmd.Parameters.Append(cmd.CreateParameter("@BHadj", 					4, 			4))    
	cmd.Parameters.Append(cmd.CreateParameter("@ProRataHours", 				4, 			4))    
	cmd.Parameters.Append(cmd.CreateParameter("@ProRataDays", 				4, 			4))    
	cmd.Parameters.Append(cmd.CreateParameter("@ProRataSameHours", 			4, 			4))   
	cmd.Execute()
     
     l_autobankholiday = cmd.parameters("@BHadj").value     
	 
	'-----------------------------
    'Auto BH adjustment calculation END
    '-----------------------------
    

	SQL = "SELECT * FROM E_JOBDETAILS WHERE EMPLOYEEID = " & EmployeeID
	Call OpenRs(rsLoader, SQL)

	If (NOT rsLoader.EOF) Then
		' tbl E_JOBDETAILS
		l_startdate = rsLoader("STARTDATE")
        l_alstartdate = rsLoader("ALSTARTDATE")
		l_enddate = rsLoader("ENDDATE")
		l_probationperiod = rsLoader("PROBATIONPERIOD")
		l_reviewdate = rsLoader("REVIEWDATE")
		l_jobtitle = rsLoader("JobRoleId")
		l_refnumber = rsLoader("REFERENCENUMBER")
		l_detailsofrole = rsLoader("DETAILSOFROLE")
		l_team = rsLoader("TEAM")
		l_patch = rsLoader("PATCH")
		l_linemanager = rsLoader("LINEMANAGER")
		l_partfulltime = rsLoader("PARTFULLTIME")
		l_hours = rsLoader("HOURS")
		l_grade = rsLoader("GRADE")
		l_salary = rsLoader("SALARY")
		l_taxoffice = rsLoader("TAXOFFICE")
		l_bankholiday = rsLoader("BANKHOLIDAY")
		l_employeelimit = rsLoader("EMPLOYEELIMIT")
		l_taxcode = rsLoader("TAXCODE")
		l_payrollnumber = rsLoader("PAYROLLNUMBER")
		l_drivinglicensenumber = rsLoader("DRIVINGLICENSENUMBER")
		l_ninumber = rsLoader("NINUMBER")
		l_holidaydays = rsLoader("HOLIDAYENTITLEMENTDAYS")
		l_holidayhours = rsLoader("HOLIDAYENTITLEMENTHOURS")
		l_totalleave = rsLoader("TOTALLEAVE")
		l_buddy = rsLoader("BUDDY")
		l_noticeperiod = rsLoader("NOTICEPERIOD")
		l_dateofnotice = rsLoader("DATEOFNOTICE")
		l_ismanager = rsLoader("ISMANAGER")
		l_isdirector = rsLoader("ISDIRECTOR")
		l_foreignnationalnumber = rsLoader("FOREIGNNATIONALNUMBER")
		l_officelocation=rsLoader("OFFICELOCATION")
		l_active = rsLoader("ACTIVE")
		l_point=-1
		If Not isnull(rsLoader("GRADEPOINT")) Then
			l_point=rsLoader("GRADEPOINT")
		End If
		l_holidayrule = rsLoader("HOLIDAYRULE")
		l_holidayIndicator = rsLoader("HOLIDAYINDICATOR")
		l_FTE = rsLoader("FTE")
		l_CARRYFORWARD = rsLoader("CARRYFORWARD")
        l_GSRENo = rsLoader("GSRENo")
        I_IsGasSafe = rsLoader("IsGasSafe")
        I_IsOftec = rsLoader("IsOftec")
		ACTION = "AMEND"
	End If
	Call CloseRs(rsLoader)


    SQL = "SELECT TradeId FROM E_TRADE WHERE EmpId = " & EmployeeID & " ORDER BY EmpTradeId ASC"
	Call OpenRs(rs, SQL)
    Counter = 0
    strDisplayTradeIds = ""
	    If (NOT rs.EOF) Then
            while (NOT rs.EOF)
				    If (Counter = 0) Then
					    strDisplayTradeIds = rs(0)
				    Else
					    strDisplayTradeIds = strDisplayTradeIds & "," & rs(0)
				    End If
				    rs.moveNext()
				    Counter = Counter + 1
			    Wend
        End If
	Call CloseRs(rs)

    SQL = "SELECT [DESCRIPTION] " &_
    "FROM E_TRADE et " &_
    "INNER JOIN g_trade gt ON et.TradeId = gt.TradeId " &_
    "WHERE et.EmpId = " & EmployeeID & " " &_
    "ORDER BY et.EmpTradeId"
    Call OpenRs(rsTrades, SQL)
    Counter = 0
    strDisplayTradeName = ""
	    If (NOT rsTrades.EOF) Then
            while (NOT rsTrades.EOF)
				    If (Counter = 0) Then
					    strDisplayTradeName = rsTrades(0)
				    Else
					    strDisplayTradeName = strDisplayTradeName & ", " & rsTrades(0)
				    End If
				    rsTrades.moveNext()
				    Counter = Counter + 1
			    Wend
        End If
	Call CloseRs(rsTrades)

	If action="AMEND" And l_holidayrule <> 13 Then
		strSQL = "EXEC E_HOLIDAY_DETAIL " & EmployeeID
		Call OpenRs (rsHols, strSQL)
		If NOT rsHols.EOF Then
			l_bankholiday_new = rsHols(4) 
			l_totalleave_new = rsHols(5)
			If (l_bankholiday_new <> l_bankholiday or l_totalleave_new <>  l_totalleave) Then
				l_bankholiday = l_bankholiday_new
				l_totalleave = l_totalleave_new
				SQL = "UPDATE E_JOBDETAILS SET BANKHOLIDAY=" & l_bankholiday & ",TOTALLEAVE=" & l_totalleave & " WHERE EMPLOYEEID = " & EmployeeID
				Conn.Execute SQL
			End If
			If l_totalleave = 0 Then l_totalleave= ""
		End If
		Call CloseRS(rsHols)
	End If


	Call BuildSelect(lstnotice, "sel_NOTICEPERIOD", "E_NOTICEPERIODS", "NOTICEPERIODID, DESCRIPTION", "NOTICEPERIODID", "Please Select", l_noticeperiod, NULL, "textbox200", "onchange='calc_notice()' tabindex=3")
	Call BuildSelect(lstprobations, "sel_PROBATIONPERIOD", "E_PROBATIONPERIOD", "PROBATIONPERIODID, DESCRIPTION", "PROBATIONPERIODID", "Please Select", l_probationperiod, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstpartfulltimes, "sel_PARTFULLTIME", "E_PARTFULLTIME", "PARTFULLTIMEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_partfulltime, NULL, "textbox75", "onchange='calculateHoliday();' tabindex=2")
	Call BuildSelect(lstteams, "sel_TEAM", "E_TEAM WHERE TEAMID <> 1 AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "Please Select", l_team, NULL, "textbox200", "onchange='change_team()' tabindex=1")
	Call BuildPatchDropdown(lstpatchs, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "Please Select","All", l_patch, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstgrades, "sel_GRADE", "E_GRADE where  ACTIVE=1", "GRADEID, DESCRIPTION", "GRADEID", "Please Select", l_grade, NULL, "textbox200", " tabindex=2")
	Call BuildSelect(lstPoint, "sel_GRADEPOINT", "E_GRADE_POINT", "POINTID, POINTDESCRIPTION", "POINTID", "Please Select", l_point, NULL, "textbox200", " tabindex=4")
	'Call BuildSelect(lstjobtitles, "sel_JobRoleId", "E_JOBTITLES", "JOBTITLE As JobTitleText, JOBTITLE", "JOBTITLE", "Please Select", l_jobtitle, NULL, "textbox200", " tabindex=1")
      '  response.Write(l_team)
     Call getJobRoleList(l_team,l_jobtitle)
    'Call BuildSelect(lstjobtitles, "sel_JobRoleId", "E_JOBROLE", " JobRoleId,JobeRoleDescription ", "JobeRoleDescription", "Please Select", l_jobtitle, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstofficelocation, "sel_OFFICELOCATION", "G_OFFICE", "OFFICEID, DESCRIPTION", "OFFICEID", "Please Select", l_officelocation, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstHolidayRule, "sel_HOLIDAYRULE", "E_HOLIDAYRULE WHERE EID NOT IN (6,7,8,9)", "EID, HOLIDAYRULE", "HOLIDAYRULE", "Please Select", l_holidayrule, NULL, "textbox200","onchange='calculateHoliday();' tabindex=2")
	Call BuildSelect(lstHoliday, "sel_HOLIDAY", "E_HOLIDAYRULE", "EID, HOLIDAY_DAYS", "HOLIDAYRULE", "Please Select", l_holiday, NULL, "textbox200", " TABINDEX=1")
	Call BuildSelect(lstHolidayIndicator, "sel_HOLIDAYINDICATOR", "E_HOLIDAYENTITLEMENT_INDICATOR", "SID, Indicator", "SID", "Please Select", l_holidayIndicator, NULL, "textbox75", "onchange='calculateHoliday();' tabindex=1")
	Call getList(lstbuddy, "bud", l_buddy)
	Call getList(lstlinemanager, "man", l_linemanager)
    Call BuildSelect(lstTrade, "sel_TRADE", "G_TRADE", "TradeId, Description", "Description", "Please Select", NULL, NULL, "textbox200", "tabindex=1")

	Call CloseDB()

    Function BuildPatchDropdown(ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS,PS2, isSelected, iStyle, iClass, iEvent)
		lst = "<select name='" & iSelectName & "' id='" & iSelectName & "' "
		If NOT isNull(iClass) Then
			lst = lst & " class='" & iClass & "' "
		End If
		If NOT isNull(iStyle) Then
			lst = lst & " style='" & iStyle & "' "
		End If
		If NOT isNull(iEvent) Then
			lst = lst & iEvent
		End If
		lst = lst & ">"

		Dim strSQL
			strSQL = "SELECT " & iCOLUMNS & " FROM " & iTABLE & " ORDER BY " & iORDERBY

		If (NOT isNull(PS)) Then
			lst = lst & "<option value=''>" & PS & "</option>"
            If NOT isNull(isSelected)  then
                if (CStr(isSelected) = "-1") then
                   optionSelected = " selected"
                end if
            End if
            lst = lst & "<option value='-1'" & optionSelected & ">" & PS2 & "</option>"
		End If

		Dim rsSet
		Call OpenRs (rsSet, strSQL)

		If NOT isNull(isSelected) Then' and isSelected <> "-1" Then
            While (NOT rsSet.EOF)
				lstid = rsSet(0)
				optionSelected = ""
				If (CStr(lstid) = CStr(isSelected)) Then
					optionSelected = " selected"
				End If
   	           lst = lst & "<option value='" & lstid & "'" & optionSelected & ">" & rsSet(1) & "</option>"
	
                rsSet.MoveNext()
			Wend
		Else
 			While (NOT rsSet.EOF)
				lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
				rsSet.MoveNext()
			Wend
		End If

		Call CloseRs(rsSet)

		lst = lst & "</select>"
	End Function

	' generates drop downs taking the table as an argument
	' criteria = 'manager' or 'director'
	Function getList(ByRef lst, str_what, selectedValue)

		Dim strSQL, rsSet, int_lst_record

		int_lst_record = 0

		If str_what = "man" Then
			strSQL = 	"SELECT 	E.EMPLOYEEID, E.FIRSTNAME + ' ' + LASTNAME AS FULLNAME "&_
						" FROM 		E__EMPLOYEE E "&_
						"    INNER 	JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID "&_
						" WHERE		J.ACTIVE = 1 AND ((ISMANAGER=1) OR (ISDIRECTOR = 1)) "&_
						"    AND J.EMPLOYEEID <> " & EmployeeID & " "
		Else
			strSQL = 	"SELECT 	E.EMPLOYEEID, E.FIRSTNAME + ' ' + LASTNAME AS FULLNAME " &_
						"FROM	 	E__EMPLOYEE E " &_
						"			INNER 	JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
						"WHERE		J.ACTIVE = 1 AND J.TEAM = (SELECT TEAM FROM E_JOBDETAILS WHERE EMPLOYEEID = " & EmployeeID & ") AND E.EMPLOYEEID <> " & EmployeeID
		End If

		Call OpenRs(rsSet, strSQL)

		If (isNull(selectedValue)) Then
			selectedValue = ""
		End If
		lst = "<option value=''>Please select</option>"
		While (NOT rsSet.EOF)
			isSelected = ""
			If (CStr(rsSet("EMPLOYEEID")) = CStr(selectedValue)) Then
				isSelected = " selected"
			End If
			lst = lst & "<option value='" & rsSet("EMPLOYEEID") & "' " & isSelected & ">" & rsSet("FULLNAME") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)

		If int_lst_record = 0 Then
				lst = "<option value=''>No Team Members/Directors Exist</option>"
		End If

	End Function
Function getJobRoleList(l_team,l_jobtitle)
	'Call OpenDB()
    ' response.Write(l_team)
    
     if l_team <>""  then
    
    sql = " Select E_JOBROLE.JobRoleId,JobeRoleDescription, E_JOBROLETEAM.isDeleted" &_
			" FROM  E_JOBROLE " &_
			"  INNER 	JOIN E_JOBROLETEAM ON E_JOBROLE.JobRoleId = E_JOBROLETEAM.JobRoleId  " &_
			" WHERE E_JOBROLETEAM.isDeleted = 0 AND TeamId = " & l_team
    
	selname = "<select name='sel_JobRoleId' id='sel_JobRoleId' class='textbox200' tabindex='1'>"
	Call build_Jobselect(lstJOBTITLES, sql, selname,l_jobtitle)
     
	'Call CloseDB()
    else
lstJOBTITLES="<select name='sel_JobRoleId' id='sel_JobRoleId' class='textbox200' tabindex='1'><option value=''>No Records Exist</option></select>"
    End If
	End Function
Function build_Jobselect(ByRef lst, ftn_sql, ftn_selname,l_jobtitle)
If (isNull(selectedValue)) Then
			selectedValue = ""
		End If
		Call OpenRs(rsSet, ftn_sql)
		lst = ftn_selname & "<option value=''>Please select</option>"
		While (NOT rsSet.EOF)
        isSelected = ""
         
			If (NOT isNull(l_jobtitle)) Then
                IF ( CStr(rsSet("JobRoleId")) = CStr(l_jobtitle)) Then
				isSelected = "selected=true"
                End If
			End If
             
			lst = lst & "<option value='" & rsSet("JobRoleId") & "'" & isSelected & ">" & rsSet("JobeRoleDescription") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend
		Call CloseRs(rsSet)
		lst = lst & "</select>"

		If int_lst_record = 0 Then
			lst = ftn_selname  & "<option value=''>No Records Exist</option></select>"
		End If

		'response.write lst

	End Function

%>
<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .style2
        {
            cursor: hand;
        }
        .style3
        {
            width: 123px;
        }
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
            background-image: none !important;
        }
        #details td
        {
            text-align: left;
            padding-left: 5px;
        }
    </style>
    <script src="/js/jquery-1.6.2.min.js" type="text/javascript"> </script>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"> </script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"> </script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"> </script>
    <script type="text/javascript" language="JavaScript" src="/js/ImageFormValidation.js"> </script>
    <script type="text/javascript" language="JavaScript">
        $(document).ready(function () {
            getPages("2")
            function showpanel() {

                $('#links_20').css('background-color', '#ccc');
            }
            // use setTimeout() to execute
            setTimeout(showpanel, 1000);
        });

        function RemoveSalaryAmendment(AmendmentId) {
            var answer = confirm('Are you sure you want to delete this Salary Amendment Letter?');
            if (answer) {
                var postData = JSON.stringify({
                });
                $.ajax({
                    type: "POST",
                    url: "/BusinessManager/Popups/SalaryAmendments.asp?said=" + AmendmentId,
                    data: postData,
                    contentType: "application/json",
                    dataType: "json",
                    statusCode: {
                        200: function () {                            
                            location.reload(true);
                        }
                    }
                });
                
            }
        }

        function AddContractor() {
            var temp2 = document.getElementById("sel_TRADE").value;
            if (temp2 == "") return true;
            var temp = document.getElementById("tbListNames").value + "";
            // if first entry in text area
            if (temp == "") {
                //document.getElementById("tbListNames").value = "a" + temp2 + "a";
                document.getElementById("tbListNames").value = "" + temp2 + "";
                PopulateContractor();
                return true;
            }
            var thearray = temp.split(",");
            for (var i = 0; i < thearray.length; i++) {
                //var temp3 = "a" + temp2 + "a";
                var temp3 = "" + temp2 + "";
                // if already in there
                if (temp3 == thearray[i]) {
                    return true;
                }
            }
            document.getElementById("tbListNames").value += "," + temp3;
            PopulateContractor();
        }

        function clearall() {
            document.getElementById("tbListNames").value = "";
            PopulateContractor();
        }

        function RemoveContractor() {
            var temp2 = document.getElementById("sel_TRADE").value;
            if (temp2 == "") return true;
            var temp = document.getElementById("tbListNames").value + "";
            if (temp == "") {
                PopulateContractor();
                return true;
            }
            var thearray = temp.split(",");
            for (var i = 0; i < thearray.length; i++) {
                //var temp3 = "a" + temp2 + "a";
                var temp3 = "" + temp2 + "";
                if (temp3 == thearray[i]) {
                    var newstring = ""
                    for (var j = 0; j < thearray.length; j++) {
                        if (j != i) {
                            if (newstring != "")
                                newstring += "," + thearray[j] + "";
                            else
                                newstring += "" + thearray[j] + "";
                        }
                    }
                    document.getElementById("tbListNames").value = newstring;
                    PopulateContractor();
                    return true;
                }
            }
        }

        function PopulateContractor() {
            document.getElementById("sel_TRADE").disabled = true;
            var mystring = ""
            var temp = document.getElementById("tbListNames").value + "";
            if (temp == "") {
                document.getElementById("taListNames").value = "";
                document.getElementById("sel_TRADE").disabled = false;
                return true;
            }
            var temp = temp.replace(/\a/g, " ");
            var thearray = temp.split(",");
            var mystring = new Array();
            for (var i = 0; i < thearray.length; i++) {
                document.getElementById("sel_TRADE").value = thearray[i];
                if (document.getElementById("sel_TRADE").selectedIndex == "")
                    mystring[i] = "Unknown Trade";
                else
                    mystring[i] = document.getElementById("sel_TRADE").options[document.getElementById("sel_TRADE").selectedIndex].text;
            }
            mystring = mystring.sort();
            mystring = mystring + "";
            mystring = mystring.replace(/\,/g, ",");
            document.getElementById("taListNames").value = mystring;
            document.getElementById("sel_TRADE").value = "";
            document.getElementById("sel_TRADE").disabled = false;
        }

        var FormFields = new Array();
        FormFields[0] = "txt_STARTDATE|Start Date|DATE|Y"
        FormFields[1] = "txt_ENDDATE|End Date|DATE|N"
        FormFields[2] = "sel_PROBATIONPERIOD|Review Date|SELECT|N"
        FormFields[3] = "txt_REVIEWDATE|Review Date|DATE|N"
        FormFields[4] = "sel_JobRoleId|JOBTITLE|SELECT|Y"
        FormFields[5] = "txt_REFERENCENUMBER|Reference Number|TEXT|N"
        FormFields[6] = "txt_DETAILSOFROLE|Details of Role|TEXT|N"
        FormFields[7] = "sel_TEAM|Team|SELECT|Y"
        FormFields[8] = "sel_PATCH|Patch|SELECT|N"
        FormFields[9] = "sel_LINEMANAGER|Line Manager|SELECT|Y"
        FormFields[10] = "sel_BUDDY|Buddy|SELECT|N"
        FormFields[11] = "sel_PARTFULLTIME|Part/Full Time|SELECT|Y"
        FormFields[12] = "txt_HOURS|Hours|FLOAT|Y|1"
        FormFields[13] = "sel_GRADE|Grade|SELECT|N"
        FormFields[14] = "txt_SALARY|Salary|CURRENCY|N"
        FormFields[15] = "txt_TAXOFFICE|Tax Office|TEXT|N"
        FormFields[16] = "txt_TAXCODE|Tax Code|TEXT|N"
        FormFields[17] = "txt_PAYROLLNUMBER|Payroll Number|TEXT|N"
        FormFields[18] = "txt_DRIVINGLICENSENUMBER|Driving License Number|TEXT|N"
        FormFields[19] = "txt_NINUMBER|National Insurance Number|TEXT|N"
        FormFields[20] = "txt_FOREIGNNATIONALNUMBER|Foreign National Number|TEXT|N"
        FormFields[21] = "txt_HOLIDAYENTITLEMENTDAYS|Holiday Entitlement|FLOAT|N|1"
        FormFields[22] = "txt_TOTALLEAVE|Total Leave|FLOAT|N|1"
        FormFields[23] = "rdo_ISMANAGER|Manager|RADIO|Y"
        FormFields[24] = "rdo_ISDIRECTOR|Director|RADIO|Y"
        FormFields[25] = "sel_NOTICEPERIOD|Notice Period|SELECT|N"
        FormFields[26] = "txt_DATEOFNOTICE|Date of Notice|DATE|N"
        FormFields[27] = "rdo_ACTIVE|Active|RADIO|Y"
        FormFields[28] = "txt_EMPLOYEELIMIT|Employee Limit|CURRENCY|Y"
        FormFields[29] = "txt_HOLIDAYENTITLEMENTHOURS|Holiday Entitlement|FLOAT|N|2"
        FormFields[30] = "txt_BANKHOLIDAY|Bank Holiday Adjustment|FLOAT|N|2"
        FormFields[31] = "sel_GRADEPOINT|Point|SELECT|N"
        FormFields[32] = "sel_HOLIDAYRULE|Holiday Rule|SELECT|Y"
        FormFields[33] = "txt_FTE|FTE|FLOAT|Y|2"
        FormFields[34] = "sel_HOLIDAYINDICATOR|Holiday Indicator|SELECT|Y"
        FormFields[35] = "txt_CARRYFORWARD|Carry Forward|FLOAT|N|2"
        FormFields[36] = "txt_ALSTARTDATE|AL Start Date|DATE|Y"
        FormFields[37] = "txt_GSRENo|Gas Safe Registered Engineer|TEXT|N"
        FormFields[38] = "rdo_IsGasSafe|Gas Safe|RADIO|Y"
        FormFields[39] = "rdo_IsOftec|OFTEC|RADIO|Y"



        //FormFields[32] = "sel_OFFICELOCATION|Office Location|SELECT|N"

        function SaveForm() {
            if (!checkForm()) return false;
            var jobSelect = document.getElementById("sel_JobRoleId");
            var selectedText = jobSelect.options[jobSelect.selectedIndex].text;
            document.getElementById("hdn_JOBTITLE").value = selectedText;
            //console.log(selectedText);
            //console.log(document.getElementById("hdn_JOBTITLE").value);
            document.RSLFORM.target = "";
            document.RSLFORM.action = "ServerSide/JobDetails_svr.asp"
            document.RSLFORM.submit()
        }

        function change_team() {
            //console.log(document.RSLFORM.sel_TEAM.value)
            //console.log(document.getElementById("sel_TEAM").value)
            //console.log(document.getElementById("sel_TEAM").options[document.getElementById("sel_TEAM").selectedIndex].value)
            if (document.getElementById("sel_TEAM").value != "") {
                document.RSLFORM.target = "frm_post";
                document.RSLFORM.action = "ServerSide/changeTeam_srv.asp?employeeid=<%=EmployeeID%>"
                document.RSLFORM.submit();
            }
        }
        function addDays(myDate, days) {
            return new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        }

        function Popup_Hours() {
            var holidayrule = RSLFORM.sel_HOLIDAYRULE[RSLFORM.sel_HOLIDAYRULE.selectedIndex].value;
            var partfull = RSLFORM.sel_PARTFULLTIME[RSLFORM.sel_PARTFULLTIME.selectedIndex].value;
            var startdate = document.getElementById("txt_STARTDATE").value;
            window.open("/HolidayEntitlement/WorkingHours.aspx?holidayrule=" + holidayrule + "&partfull=" + partfull + "&startdate=" + startdate + "&Userid=<%=USERID%>" + "&EMPLOYEEID=<%=EmployeeID%>", "_blank", "width=1100,height=550, left=300,top=300");
        }

        function viewEmpHours(empID) {
            window.open("Popups/pWorkingHours.asp?employeeid=" + empID, "WorkPattern", "height=450px,width=800px,scrollbars=yes,toolbar=no")
        }

        function calc_notice() {
            var new_date
            if ((document.getElementById("sel_NOTICEPERIOD").value != "") && (document.getElementById("txt_DATEOFNOTICE").value != ""))
                if (!isDate("txt_DATEOFNOTICE", "Date of Notice")) {
                    alert("Date of Notice is invalid -- Please re-enter");
                    document.getElementById("txt_DATEOFNOTICE").select();
                    return false;
                }
                else {
                    var mydate, period
                    mydate = new Date(conv_date(document.getElementById("txt_DATEOFNOTICE").value));
                    period = parseInt(document.getElementById("sel_NOTICEPERIOD").value);
                    new_date = addDays(mydate, period);
                    new_date = pad(new_date.getDate()) + "/" + pad((new_date.getMonth() + 1)) + "/" + new_date.getFullYear();
                    document.getElementById("txt_ENDDATE").value = new_date
                }
        }

        // pads the day and month figues for returning the proposed enddate
        function pad(num) {
            if (num < 10)
                return new String("0" + num);
            else
                return num;
        }

        function conv_date(datum) {
            var split_date, arr_months, long_date
            arr_months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            split_date = datum.split("/");
            return split_date[0] + " " + arr_months[(split_date[1] - 1)] + " " + split_date[2];
        }

        function open_window(url, width, height) {
            window.open(url, 'mywindow', 'width=' + width + ',height=' + height + ',toolbar=no, location=no,directories=no,status=yes,menubar=no,scrollbars=no,copyhistory=no, resizable=yes')
        }

        function JSlong_date(str_date) {
            var arrMon = new Array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            var date_array = str_date.split("/");
            return new String(Math.abs(date_array[0]) + " " + arrMon[parseInt(date_array[1], 10)] + " " + date_array[2])
        }

        function calculateRemainingDays() {
            aDate = new Date(Date.parse(JSlong_date(document.getElementById("txt_STARTDATE").value)));
            if (aDate.getMonth() >= 3) {
                var bYear = aDate.getFullYear() + 1;
            }
            else {
                var bYear = aDate.getFullYear();
            }
            var bEndDate = '1/4/' + bYear;
            bDate = new Date(Date.parse(JSlong_date(bEndDate)));
            var days_left = (bDate - aDate) / 86400000;
            return days_left;
        }

        function nextNearest(value, number) {
            var ceil = Math.ceil(value);
            var remainder = value % number;
            if (remainder > 0)
                value = value - remainder + number;
            return value;
        }

        function roundToHalf(value) {
            var converted = parseFloat(value); // Make sure we have a number 
            var decimal = (converted - parseInt(converted, 10));
            decimal = Math.round(decimal * 10);
            if (decimal == 5) { return (parseInt(converted, 10) + 0.5); }
            if ((decimal < 3) || (decimal > 7)) {
                return Math.round(converted);
            } else {
                return (parseInt(converted, 10) + 0.5);
            }
        }

        function calculateHoliday() {
            var holidayRule = document.getElementById("sel_HOLIDAYRULE").options[document.getElementById("sel_HOLIDAYRULE").selectedIndex].value;
            var fulltime = document.getElementById("sel_PARTFULLTIME").options[document.getElementById("sel_PARTFULLTIME").selectedIndex].value;
            var holiday = document.getElementById("sel_HOLIDAYRULE").options[document.getElementById("sel_HOLIDAYRULE").selectedIndex].value;
            var FTE = document.getElementById("txt_FTE").value;
            var days_left = calculateRemainingDays();
            var totalWorkDays = document.getElementById("txt_TOTALWORKDAYS").value;
            var totalHRS = document.getElementById("txt_HOURS").value;
            var holiday_proRata = (holiday / 365) * days_left;
            // BRS - MEARS HOLIDAY RULE
            if (holidayRule == '13') {
                document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = 0;
                document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = 0;
                document.getElementById("txt_TOTALLEAVE").value = 0;
                return;
            }
            if (fulltime == '1') {
                if (document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex == '') {
                    document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex = 1;
                }
                // Holiday Rule for Meridian East
                if (holidayRule == '6' || holidayRule == '7' || holidayRule == '8' || holidayRule == '9') {
                    // calculate number of days left
                    document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = roundToHalf(holiday_proRata); //nextNearest(holiday_proRata,0.5);
                    document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = '';
                }
                else {
                    document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex = 1;
                    document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = holiday;
                    document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = '';
                }
            }
            // means Part time
            else if (fulltime == '2') {
                if (document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex == '') {
                    document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex = 2;
                }
                var holIndicator = document.getElementById("sel_HOLIDAYINDICATOR").options[document.getElementById("sel_HOLIDAYINDICATOR").selectedIndex].value;
                // for part time holiday entitlement in days
                if (holIndicator == '1') {
                    //  for Part time Meridian East staff  holiday entitlement in dAYS
                    if (holidayRule == '6' || holidayRule == '7' || holidayRule == '8' || holidayRule == '9') {
                        document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = roundToHalf((holiday_proRata / 5) * totalWorkDays);
                        document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = '';
                    }
                    else {
                        document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = roundToHalf((holiday / 5) * totalWorkDays);
                        document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = '';
                    }
                }
                // for part time holiday entitlement in Hours
                else if (holIndicator == '2') {
                    // for Part time Meridian East staff  holiday entitlement in Hours
                    if (holidayRule == '6' || holidayRule == '7' || holidayRule == '8' || holidayRule == '9') {
                        document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = roundToHalf((holiday * (7.4 / 37) * totalHRS));
                        document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = '';
                    }
                    else {
                        document.getElementById("txt_HOLIDAYENTITLEMENTHOURS").value = roundToHalf((holiday * (7.4 / 37) * totalHRS));
                        document.getElementById("txt_HOLIDAYENTITLEMENTDAYS").value = '';
                    }
                }
            }
        }
        function ViewHistory() {
            window.open_window("/JobRoleAdministration/Views/Reports/JobRoleHistory.aspx?empId=<%=EmployeeID%>", "SalesPosting", "dialogHeight: 380px; dialogWidth: 640px; edge: Raised; center: Yes; help:Yes; resizable: Yes; status: Yes; scroll: Yes;menubar=no");
        }
    </script>
</head>
<!--<body> -->
<body>
    <!-- #include virtual="Includes/Tops/BodyTopNewLayout.asp" -->
    <table width="98%" border="0" cellspacing="0" cellpadding="0" class="jr-rightpanel">
        <tr>
            <td style="width: 98%; background-color: Black; height: 22px;">
                <span style="color: White; font-weight: bold; margin: 20px;">Employee Profiles:
                    <%=FullName%></span>
            </td>
        </tr>
        <tr>
            <td>
                <div class="tab-bar">
                    <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details" class="tab">
                        Personal Details </a><a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info"
                            class="tab">Contacts </a><a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>"
                                title="Skills & Qualifications" class="tab">Skills/ Quals </a><a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>"
                                    title="Difficulties/Disabilities" class="tab">Health</a> <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>"
                                        title="Job Details" class="tab-selected">Post</a> <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>"
                                            title="Benefits" class="tab">Benefits</a>
                    <div class="clear-tabs">
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="2" style="border-right: 1px solid #7e729E" align="center">
                <form name="RSLFORM" method="POST" action="" style="margin: 0px" id="RSLFORM">
                <table cellspacing="2" border="0" style="height: 100%; border: 1px solid #133E71;"
                    width="98%" id="details">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Team:
                        </td>
                        <td>
                            <%=lstteams%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TEAM" id="img_TEAM" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Job Role:
                        </td>
                        <td>
                            <div id="dvJOBTITLE">
                                <%=lstJOBTITLES%>
                            </div>
                            <input type="hidden" name="hdn_JOBTITLE" id="hdn_JOBTITLE" value="" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_JobRoleId" id="img_JobRoleId" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td>
                            <input type="button" value="View History" class="RSLButton" title="View History"
                                onclick='ViewHistory()' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="border: 1px dotted #133e71" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Start Date:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_STARTDATE" id="txt_STARTDATE" maxlength="10"
                                value="<%=l_startdate%>" tabindex="1" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td nowrap="nowrap" class="style3">
                            Patch:
                        </td>
                        <td>
                            <%=lstpatchs%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_PATCH" id="img_PATCH" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            AL Start Date:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_ALSTARTDATE" id="txt_ALSTARTDATE"
                                maxlength="10" value="<%=l_alstartdate%>" tabindex="1" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ALSTARTDATE" id="img_ALSTARTDATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td class="style3">
                            Grade:
                        </td>
                        <td>
                            <%=lstgrades%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_GRADE" id="img_GRADE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Review Date:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_REVIEWDATE" id="txt_REVIEWDATE" maxlength="10"
                                value="<%=l_reviewdate%>" tabindex="1" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REVIEWDATE" id="img_REVIEWDATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td class="style3">
                            Point:
                        </td>
                        <td>
                            <%=lstPoint%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_GRADEPOINT" id="img_GRADEPOINT" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Holiday Rule:
                        </td>
                        <td>
                            <%=lstHolidayRule%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_HOLIDAYRULE" id="img_HOLIDAYRULE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td nowrap="nowrap" class="style3">
                            Full/Part Time:
                        </td>
                        <td>
                            <%=lstpartfulltimes%>
                            <img src="/js/FVS.gif" name="img_PARTFULLTIME" id="img_PARTFULLTIME" width="15px"
                                height="15px" border="0" alt="" />
                            <%=lstHolidayIndicator%>
                            <img src="/js/FVS.gif" name="img_HOLIDAYINDICATOR" id="img_HOLIDAYINDICATOR" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Full Time Equivalent
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_FTE" id="txt_FTE" value="<%=l_FTE%>"
                                tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_FTE" id="img_FTE" width="15px" height="15px" border="0"
                                alt="" />
                        </td>
                        <td class="style3">
                            Hours (per week):
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_HOURS" id="txt_HOURS" maxlength="10"
                                value="<%=l_hours%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_HOURS" id="img_HOURS" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td id="TD2" title='View Working Hours' style='color: #133e71' align="center" onclick='viewEmpHours("<% =EmployeeId%>");'
                            class="style2">
                            <font color="blue">View</font>
                        </td>
                        <td id="TD1" title="Edit Working Hours" style="color: #133e71" align="center" onclick='Popup_Hours();'
                            class="style2">
                            <font color="red">Edit</font>
                        </td>
                        <!--<td id='pt' title='View shift details' width=100% style='visibility:hidden;cursor:hand;color:#133e71' align="center" onclick='window.open("PartTimePopUp.asp?EMPLOYEEID=<%=EmployeeID%>", "_blank", "width=325,height=280, left=300,top=300");'><font color=blue>View</font></td>-->
                    </tr>
                    <tr>
                        <td>
                            Ref Number:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_REFERENCENUMBER" id="txt_REFERENCENUMBER"
                                maxlength="20" value="<%=l_refnumber%>" tabindex="1" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_REFERENCENUMBER" id="img_REFERENCENUMBER" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            Salary:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_SALARY" id="txt_SALARY" maxlength="15"
                                value="<%=l_salary%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_SALARY" id="img_SALARY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="3" valign="top">
                            Details of Role:
                        </td>
                        <td rowspan="3" valign="top">
                            <textarea class="textbox200" name="txt_DETAILSOFROLE" id="txt_DETAILSOFROLE" rows="6"
                                cols="20" style="overflow: hidden" tabindex="1"><%=l_detailsofrole%></textarea>
                        </td>
                        <td rowspan="3" valign="top">
                            <img src="/js/FVS.gif" name="img_DETAILSOFROLE" id="img_DETAILSOFROLE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td nowrap="nowrap" title="(work permit)" class="style3">
                            Foreign National Number:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_FOREIGNNATIONALNUMBER" id="txt_FOREIGNNATIONALNUMBER"
                                maxlength="50" value="<%=l_foreignnationalnumber%>" style="text-transform: uppercase"
                                tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_FOREIGNNATIONALNUMBER" id="img_FOREIGNNATIONALNUMBER"
                                width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" class="style3">
                            Tax Office:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_TAXOFFICE" id="txt_TAXOFFICE" maxlength="50"
                                value="<%=l_taxoffice%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TAXOFFICE" id="img_TAXOFFICE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            Tax Code:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_TAXCODE" id="txt_TAXCODE" maxlength="50"
                                value="<%=l_taxcode%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TAXCODE" id="img_TAXCODE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Role File:
                        </td>
                        <td valign="top">
                            <a href="#" onclick="open_window('/BusinessManager/Popups/EmployeeRole.aspx?empid=<%=EmployeeID%>','400','250')">
                                Upload Role File</a> <a id="lnkRolefile" href="/Rolefile/<% = l_rolefile  %>" target="_blank"
                                    style="display: <% = l_rolefile_display    %>">View File</a>
                        </td>
                        <td>
                        </td>
                        <td class="style3">
                            Payroll Number:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_PAYROLLNUMBER" id="txt_PAYROLLNUMBER"
                                maxlength="30" value="<%=l_payrollnumber%>" style="text-transform: uppercase"
                                tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_PAYROLLNUMBER" id="img_PAYROLLNUMBER" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap" title="The amount in pounds an employee is allowed to create repairs for before a manager has to authorise it.">
                            Employee Limit:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_EMPLOYEELIMIT" id="txt_EMPLOYEELIMIT"
                                maxlength="50" value="<%=l_employeelimit%>" tabindex="1" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_EMPLOYEELIMIT" id="img_EMPLOYEELIMIT" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            Driving Lic Number:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DRIVINGLICENSENUMBER" id="txt_DRIVINGLICENSENUMBER"
                                maxlength="30" value="<%=l_drivinglicensenumber%>" style="text-transform: uppercase"
                                tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DRIVINGLICENSENUMBER" id="img_DRIVINGLICENSENUMBER"
                                width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Line Manager:
                        </td>
                        <td>
                            <div id="dvLINEMANAGER">
                                <select class="textbox200" name="sel_LINEMANAGER" id="sel_LINEMANAGER" tabindex="1">
                                    <%=lstlinemanager %>
                                </select></div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_LINEMANAGER" id="img_LINEMANAGER" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td nowrap="nowrap" class="style3">
                            NI Number:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_NINUMBER" id="txt_NINUMBER" maxlength="10"
                                value="<%=l_ninumber%>" style="text-transform: uppercase" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NINUMBER" id="img_NINUMBER" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Place of Work:
                        </td>
                        <td>
                            <%=lstofficelocation %>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_OFFICELOCATION" id="img_OFFICELOCATION" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            Holiday Entitlement:
                        </td>
                        <td>
                            <input style='width: 50px' type="text" class="textbox100" name="txt_HOLIDAYENTITLEMENTDAYS"
                                id="txt_HOLIDAYENTITLEMENTDAYS" maxlength="10" value="<%=l_holidaydays%>" tabindex="2" />
                            (days)
                            <img src="/js/FVS.gif" name="img_HOLIDAYENTITLEMENTDAYS" id="img_HOLIDAYENTITLEMENTDAYS"
                                width="15px" height="15px" border="0" alt="" />
                            <input style="width: 50px" type="text" class="textbox100" name="txt_HOLIDAYENTITLEMENTHOURS"
                                id="txt_HOLIDAYENTITLEMENTHOURS" maxlength="10" value="<%=l_holidayhours%>" tabindex="2" />
                            (hrs)
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_HOLIDAYENTITLEMENTHOURS" id="img_HOLIDAYENTITLEMENTHOURS"
                                width="15px" height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Buddy:
                        </td>
                        <td>
                            <div id="dvBUDDY">
                                <select class="textbox200" name="sel_BUDDY" id="sel_BUDDY" tabindex="1">
                                    <%=lstbuddy %>
                                </select>
                            </div>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_BUDDY" id="img_BUDDY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td class="style3">
                            Bank Holiday Adjustment:
                        </td>
                        <td>
                            <input style='width: 50px' type="text" class="textbox200" name="txt_BANKHOLIDAY"
                                id="txt_BANKHOLIDAY" maxlength="10" value="<%=l_bankholiday%>" tabindex="2" />
                            Auto:
                            <input style='width: 50px' type="text" class="textbox200" name="txt_BANKHOLIDAYAUTOADJ"
                                id="txt_BANKHOLIDAYAUTOADJ" maxlength="10" value="<%=l_autobankholiday%>" tabindex="2"
                                readonly />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_BANKHOLIDAY" id="img_BANKHOLIDAY" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Team Manager:
                        </td>
                        <td>
                            <% 
		        yesChecked = ""
		        noChecked = "CHECKED"
		        If ACTION = "AMEND" Then
			        If (l_ismanager = 1) Then
				        yesChecked = "CHECKED"
				        noChecked = ""
			        End If
		        End If
                            %>
                            <input type="radio" name="rdo_ISMANAGER" value="1" <%=yesChecked%> tabindex="2" />
                            YES
                            <input type="radio" name="rdo_ISMANAGER" value="0" <%=noChecked%> tabindex="2" />
                            NO
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ISMANAGER" id="img_ISMANAGER" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td class="style3">
                            Carry Forward:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_CARRYFORWARD" id="txt_CARRYFORWARD"
                                maxlength="10" value="<%=l_CARRYFORWARD%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_CARRYFORWARD" id="img_CARRYFORWARD" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Employment Contracts:
                        </td>
                        <td valign="top">
                            <a href="#" onclick="open_window('/BusinessManager/Popups/EmploymentContract.asp?empid=<%=EmployeeID%>','600','350')">
                                Upload Employment Contract</a> <a id="A1" href="/Rolefile/EmploymentContract/<% = l_employmentcontract  %>"
                                    target="_blank" style="display: <% = l_employmentcontract_display    %>">View File</a>
                        </td>
                        <td>
                        </td>
                        <td class="style3">
                            Total Leave:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_TOTALLEAVE" id="txt_TOTALLEAVE" maxlength="10"
                                value="<%=l_totalleave%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_TOTALLEAVE" id="img_TOTALLEAVE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Salary Amendments:
                        </td>
                        <td valign="bottom">
                            <a href="#" onclick="open_window('/BusinessManager/Popups/SalaryAmendments.asp?empid=<%=EmployeeID%>','600','350')">
                                Upload Salary Amendment Letter</a>
                        </td>
                        <td>
                        </td>
                        <td>
                            Gas Safe Reg Engineer:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_GSRENo" id="txt_GSRENo" maxlength="20"
                                value="<%=l_GSRENo%>" tabindex="2" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_GSRENo" id="img_GSRENo" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td rowspan="3" valign="top">
                            <div style="border: 1px solid rgb(19, 62, 113); min-height: 100px; width:200px; overflow:auto; max-height:120px; display: <% = l_SalaryAmendment_display %>">
                                <%=strDisplaySalaryAmendment %>
                            </div>
                        </td>
                        <td>
                        </td>
                        <td>
                            Trade:
                        </td>
                        <td>
                            <%=lstTrade%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_Trade" id="img_Trade" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                        <td rowspan="2">
                            <div style="padding-bottom: 10px">
                                <a href="JavaScript://" onclick="AddContractor()" style="text-decoration: none">Add</a></div>
                            <div style="padding-bottom: 10px">
                                <a href="JavaScript://" onclick="RemoveContractor()" style="text-decoration: none;
                                    color: Red">Remove</a></div>
                            <div style="padding-bottom: 10px">
                                <a href="JavaScript://" onclick="clearall()" style="text-decoration: none; color: Black">
                                    Clear</a></div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <textarea name="taListNames" id="taListNames" readonly="readonly" rows="4" cols="77"
                                class="textbox200"><%=strDisplayTradeName%></textarea>
                            <input type="hidden" name="tbListNames" id="tbListNames" value="<%=strDisplayTradeIds%>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            Certifications:
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Gas Safe
                                    </td>
                                    <td>
                                        <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (I_IsGasSafe = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
                                        %>
                                        <input type="radio" name="rdo_IsGasSafe" value="1" <%=yesChecked%> tabindex="2" />
                                        YES
                                        <input type="radio" name="rdo_IsGasSafe" value="0" <%=noChecked%> tabindex="2" />
                                        NO
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_IsGasSafe" id="img_IsGasSafe" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        OFTEC
                                    </td>
                                    <td>
                                        <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (I_IsOftec = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
                                        %>
                                        <input type="radio" name="rdo_IsOftec" value="1" <%=yesChecked%> tabindex="2" />
                                        YES
                                        <input type="radio" name="rdo_IsOftec" value="0" <%=noChecked%> tabindex="2" />
                                        NO
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_IsOftec" id="img_IsOftec" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <hr style="border: 1px dotted #133e71" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Notice Period:
                        </td>
                        <td>
                            <%=lstnotice%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_NOTICEPERIOD" id="img_NOTICEPERIOD" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            Director:
                        </td>
                        <td>
                            <% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_isdirector = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
                            %>
                            <input type="radio" name="rdo_ISDIRECTOR" value="1" <%=yesChecked%> tabindex="2" />
                            YES
                            <input type="radio" name="rdo_ISDIRECTOR" value="0" <%=noChecked%> tabindex="2" />
                            NO
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ISDIRECTOR" id="img_ISDIRECTOR" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date of Notice:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_DATEOFNOTICE" id="txt_DATEOFNOTICE"
                                maxlength="10" value="<%=l_dateofnotice%>" onblur="calc_notice()" tabindex="3" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_DATEOFNOTICE" id="img_DATEOFNOTICE" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            Active:
                        </td>
                        <td>
                            <% 
		        yesChecked = ""
		        noChecked = "CHECKED"
		        If ACTION = "AMEND" Then
			        If (l_active = 1) Then
				        yesChecked = "CHECKED"
				        noChecked = ""
			        End If
		        End If
                            %>
                            <input type="radio" name="rdo_ACTIVE" value="1" <%=yesChecked%> tabindex="4" />
                            YES
                            <input type="radio" name="rdo_ACTIVE" value="0" <%=noChecked%> tabindex="4" />
                            NO
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ACTIVE" id="img_ACTIVE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td nowrap="nowrap">
                            Probation Period:
                        </td>
                        <td>
                            <%=lstprobations%>
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_PROBATIONPERIOD" id="img_PROBATIONPERIOD" width="15px"
                                height="15px" border="0" alt="" />
                        </td>
                        <td class="style3">
                            End Date:
                        </td>
                        <td>
                            <input type="text" class="textbox200" name="txt_ENDDATE" id="txt_ENDDATE" maxlength="10"
                                value="<%=l_enddate%>" tabindex="4" />
                        </td>
                        <td>
                            <img src="/js/FVS.gif" name="img_ENDDATE" id="img_ENDDATE" width="15px" height="15px"
                                border="0" alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right;">
                            <br />
                            <input type="hidden" name="hid_EmployeeID" id="hid_EmployeeID" value="<%=EmployeeID%>" />
                            <input type="hidden" name="txt_TOTALWORKDAYS" id="txt_TOTALWORKDAYS" value="<%=l_workDays%>" />
                            <input type="button" name="button" id="button" class="RSLButton" value=" SAVE " title=" SAVE "
                                onclick="SaveForm()" style="cursor: pointer" />
                            <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                            <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />
                        </td>
                    </tr>
                    <tr id="hiddenRow" style="display: none">
                        <td colspan="5">
                            <%=lstHoliday%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" height="100%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <iframe name="frm_post" id="frm_post" width="4" height="4" style="display: none">
                </iframe>
                <br />
                </form>
            </td>
        </tr>
    </table>
    <!--<iframe name=frm_post SRC="ServerSide/changeTeam_srv.asp?employeeid=<%=EmployeeID%>" width=400px height=400px style='display:none'></iframe> -->
    <img src="/js/img/FVER.gif" width="1px" height="1px" name="FVER_Image" id="FVER_Image"
        alt="" />
    <img src="/js/img/FVEB.gif" width="1px" height="1px" name="FVEB_Image" id="FVEB_Image"
        alt="" />
    <img src="/js/img/FVS.gif" width="1px" height="1px" name="FVS_Image" id="FVS_Image"
        alt="" />
    <img src="/js/img/FVW.gif" width="1px" height="1px" name="FVW_Image" id="FVW_Image"
        alt="" />
    <img src="/js/img/FVTG.gif" width="1px" height="1px" name="FVTG_Image" id="FVTG_Image"
        alt="" />
</body>
</html>
