<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

  <!--#include virtual="ACCESSCHECK.asp" -->
  <html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>
        <meta http-equiv="page-enter" content="revealtrans(duration=0.000,transition=5)" />
        <meta http-equiv="page-exit" content="revealtrans(duration=0.000,transition=5)" />
        <title>mpi</title>
        <link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
    </head>
    <script type="text/javascript"   language="javascript" src="/js/preloader.js"></script>
    <script type="text/javascript"   language="javascript" src="/js/general.js"></script>
    <script type="text/javascript"   language="javascript" src="/js/menu.js"></script>
    <script type="text/javascript"   language="javascript" src="/js/formvalidation.js"></script>
    <script type="text/javascript"   language="javascript">
    <!--
	    function Go()
	    {
		    thisForm.submit();
	    }
    // -->
    </script>
    <!-- End Preload Script -->
    <body class='TA' style="background-color:White; margin:10 0 0 10;" onload="initSwipeMenu(3);preloadImages()" onunload="macGo()" >
        <!--#include virtual="Includes/Tops/BodyTop.asp" -->
        <form name="RSLForm" method="post"  action="" >
        <div style='border:1px solid black;padding:5 0 0 10;'>
            <p style=" font-weight:bold; text-decoration:underline;">MPI Reports</p>
            <ul>
                <li style="margin:7 0 7 0"><a href="arrearsmanagement.asp">MPI Arrears Management</a></li>
                <li style="margin:7 0 7 0"><a href="assetmanagement.asp">MPI Asset Management</a></li>
                <li style="margin:7 0 7 0"><a href="customers.asp">MPI Customers</a></li>
                <li style="margin:7 0 7 0"><a href="tenancylettings.asp">MPI Tenancy Lettings</a></li>
            </ul>
            <p style=" font-weight:bold; text-decoration:underline;">KPI Reports (Organisational)</p>
            <ul>
                <li style="margin:3 0 3 0"><a href="../KPI/KPI2_Organisational.asp">KPI</a></li>
                <li style="margin:3 0 3 0"><a href="../KPI/KPI2_Emergency.asp">KPI (Emergency Faults Counts)</a></li>
            </ul>
         </div>   
	     <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
        </form>
    </body>
  </html>
