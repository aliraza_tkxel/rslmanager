
<%
	' DONT EXECUTE IF INITIAL ENTRY TO PAGE
	If fiscalyear <> "" Then
	
		Dim lett, available, unavailable, pstotal
		OpenDB()	
		SQL = " EXEC MPI_MAIN_PROPERTYSTATUS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			lett 		= rsSet("LET")
			available 	= rsSet("AVAILABLE")
			unavailable = rsSet("UNAVAILABLE")
		End If
		CloseRs(rsSet)
		pstotal = lett + available + unavailable
		
		RW "<TR><TD>Total:</TD><TD><b>"&pstotal&"</b></TD><TD>Available</TD><TD><b>"&available&"</b></TD></TR>"
		RW "<TR><TD>Let:</TD><TD><b>"&lett&"</b></TD><TD>Unavailable</TD><TD><b>"&unavailable&"</b></TD></TR>"
		CloseDb()
		
	End If

 %>