
<%
	Function loadArrearsRecovery()
	
		Dim total, nulls
		nulls = 0
		total = 0
		
		' FIRST GET ANY ARREARS WITH NO ASSOCIATED ACTION (DUE TO BEING DEVELOPED LATER)
		SQL = " EXEC MPI_4_ARREARS_RECOVERY_NO_ACTION '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then nulls = rsSet(0) End If		
				
		SQL = " EXEC MPI_4_ARREARS_RECOVERY '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		strArrData = strArrData & "<TABLE WIDTH=250PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>"
		strArrData = strArrData & "<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Arrears Recovery	</b></TD></TR>"
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			strArrData = strArrData & "<TR><TD>" & rsSet("DESCRIPTION") & "</TD>"
			strArrData = strArrData & "<TD WIDTH=30PX ALIGN=RIGHT>" & rsSet("TOTAL") & "</TD></TR>"
			total = total + rsSet("TOTAL")
			rsSet.Movenext()
		Wend
		CloseRs(rsSet)
		strArrData = strArrData & "<TR><TD>No associated action</TD><TD ALIGN=RIGHT>" & nulls & "</TD></TR>"
		strArrData = strArrData & "<TR><TD>Total</TD><TD ALIGN=RIGHT>" & total & "</TD></TR></TABLE>"

	End Function

 %>