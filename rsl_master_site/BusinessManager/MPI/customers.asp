<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstScheme, lstAssetType, lstMonth, lstYear
	Dim fiscalyear, assettype, month, scheme
	' SERVICE CMOPLAINTS VARIABLES
	Dim strServiceComplaintsData, totalComplaints, overdue, withinTimeframe, performance
	' ASB VARIABLES
	Dim strASBData, asbTotalClosed, asbTotal, asbPerformance, asbByTypeTotalClosed, asbByTypeTotal
	
	OpenDB()
	
	' GET QUERYSTRING
	fiscalyear = ""
	If Request("sel_YEAR") <> "" Then fiscalyear = Request("sel_YEAR") End If
	assettype = ""
	If Request("sel_ASSETTYPE") <> "" Then assettype = Request("sel_ASSETTYPE") End If
	month = ""
	If Request("sel_MONTH") <> "" Then month = Request("sel_MONTH") End If
	scheme = ""
	If Request("sel_SCHEME") <> "" Then scheme = Request("sel_SCHEME") End If
	
	
	' GET SCHEME LIST
	Call BuildSelect(lstScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME", "SCHEMENAME", "All", scheme, NULL, "textbox200", " ")
	' GET ASSET TYPE LIST
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "All", assettype, NULL, "textbox200", "  ")
	' GET GET MONTH LIST
	Call BuildSelect(lstMonth, "sel_MONTH", "G_MONTH", "SHORTDESCRIPTION, DESCRIPTION", "MONTHID", "All", month, NULL, "textbox200", " style='width:180px' ")
	' GET YEAR LIST
	Call BuildSelect(lstYear, "sel_YEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2005'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select", fiscalyear, NULL, "textbox200", " tabindex=1 style='width:180px' ")			  
	
	' IF SUBMITTED BUILD DATA
	If fiscalyear <> "" Then
		Call loadServiceComplaints_1()
		Call loadServiceComplaints_2()
		Call loadServiceComplaints_3()
		Call ASB()
	End If
	
	CloseDB()
	
	Function ASB()
	
		Dim asbClosedArray, cnt
		cnt = 0
		Redim asbClosedArray(3)
		asbTotal = 0
		asbTotalClosed = 0
		asbPerformance = 0
		strASBData = ""
		
		' first get all closed asbs
		SQL = " EXEC MPI_3_ASB_CLOSED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			asbClosedArray(cnt) = rsSet("TOTAL")
			cnt = cnt + 1
			asbTotalClosed = asbTotalClosed + rsSet("TOTAL")
			rsSet.Movenext()
		Wend
		cnt = 0
		SQL = " EXEC MPI_3_ASB '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			strASBData = strASBData & "<TR><TD>&nbsp;&nbsp;&nbsp;" & rsSet("DESCRIPTION") & "</TD>"
			strASBData = strASBData & "<TD WIDTH=30PX ALIGN=RIGHT>" & rsSet("TOTAL") & "</TD><TD WIDTH=30PX ALIGN=RIGHT>" & asbClosedArray(cnt) & "</TD></TR>"
			asbTotal = asbTotal + rsSet("TOTAL")
			cnt = cnt + 1
			rsSet.Movenext()
		Wend
		CloseRs(rsSet)
	
		if asbTotal <> 0 Then asbPerformance = asbTotalClosed/asbTotal * 100 End If
		
		' NOW BY CATEGORY
		Dim asbClosedByTypeArray
		cnt = 0
		Redim asbClosedByTypeArray(10)
		asbByTypeTotal = 0
		asbByTypeTotalClosed = 0
		
		' first get all closed asbs
		SQL = " EXEC MPI_3_ASB_BY_CATEGORY_CLOSED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			asbClosedByTypeArray(cnt) = rsSet("TOTAL")
			cnt = cnt + 1
			asbByTypeTotalClosed = asbByTypeTotalClosed + rsSet("TOTAL")
			rsSet.Movenext()
		Wend

		strASBData = strASBData & "<TR BGCOLOR=SILVER><TD>Nature</TD><TD>Logged</TD><TD>Closed</TD></TR>"
		cnt = 0
		SQL = " EXEC MPI_3_ASB_BY_CATEGORY '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			strASBData = strASBData & "<TR><TD>&nbsp;&nbsp;&nbsp;" & rsSet("DESCRIPTION") & "</TD>"
			strASBData = strASBData & "<TD WIDTH=30PX ALIGN=RIGHT>" & rsSet("TOTAL") & "</TD><TD WIDTH=30PX ALIGN=RIGHT>" & asbClosedByTypeArray(cnt) & "</TD></TR>"
			asbByTypeTotal = asbByTypeTotal + rsSet("TOTAL")
			cnt = cnt + 1
			rsSet.Movenext()
		Wend
		CloseRs(rsSet)
	
	
	End Function

	
	Function loadServiceComplaints_1()
	
		totalComplaints = 0
		SQL = " EXEC MPI_3_COMPLAINTS_BY_ESCALATION '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then totalComplaints = rsSet("TOTAL") End IF
		strServiceComplaintsData = strServiceComplaintsData & "<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Service Complaints</b></TD></TR>"
		strServiceComplaintsData = strServiceComplaintsData & "<TR BGCOLOR=WHITESMOKE><TD>No of Complaints (a)</TD><TD ALIGN=RIGHT>"&totalComplaints&"</TD></TR>"
		Set rsSet = rsSet.NextRecordSet()
		While Not rsSet.EOF
			strServiceComplaintsData = strServiceComplaintsData & "<TR><TD>&nbsp;&nbsp;&nbsp;" & rsSet("DESCRIPTION") & "</TD>"
			strServiceComplaintsData = strServiceComplaintsData & "<TD WIDTH=30PX ALIGN=RIGHT>" & rsSet("TOTAL") & "</TD></TR>"
			rsSet.Movenext()
		Wend
		CloseRs(rsSet)

	End Function
	
	Function loadServiceComplaints_2()
	
		strServiceComplaintsData = strServiceComplaintsData & "<TR BGCOLOR=WHITESMOKE><TD COLSPAN=2>Complaints by type</TD></TR>"
		SQL = " EXEC MPI_3_COMPLAINTS_BY_TYPE '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			strServiceComplaintsData = strServiceComplaintsData & "<TR><TD>&nbsp;&nbsp;&nbsp;" & rsSet("DESCRIPTION") & "</TD>"
			strServiceComplaintsData = strServiceComplaintsData & "<TD ALIGN=RIGHT>" & rsSet("TOTAL") & "</TD></TR>"
			rsSet.Movenext()
		Wend
		' WRITE FINAL TOTAL AND CLOSE TABLE
		CloseRs(rsSet)

	End Function
	
	Function loadServiceComplaints_3()
	
		overdue = 0
		withinTimeframe = 0
		SQL = " EXEC MPI_3_COMPLAINTS_BY_TIME '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			 overdue		= rsSet("OVERDUE")
			 withinTimeframe = rsSet("EARLY")
		End If
		CloseRs(rsSet)
		' CALCULATE PERFORMANCE
		performance = 0
		If totalComplaints <> 0 Then performance = (withinTimeFrame/totalComplaints*100) End If

	End Function


	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>MPI</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
		
	function Go(){

		thisForm.submit();
	
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<FORM NAME=thisForm METHOD=POST ACTION="CUSTOMERS.ASP">
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR><TD WIDTH=5px ROWSPAN=20>&nbsp;</TD>
			<TD>
			<TABLE WIDTH=200px HEIGHT=55PX CELLPADDING=1 CELLSPACING=2 STYLE='BORDER:1PX SOLID BLACK' BORDER=0 VALIGN=TOP>
				<TR><TD COLSPAN=4><b><U>Property Status</U></b></TD></TR>
			<!--#include virtual="/BusinessManager/MPI/includes/propertystatus.asp" -->
				<TR><TD COLSPAN=4 HEIGHT=100%></TD></TR>
				</TABLE>
			</TD>
			<TD ALIGN=RIGHT>
				<TABLE WIDTH=400px CELLPADDING=1 CELLSPACING=2 BORDER=0>
					<TR><TD><b>Year:</b></TD><TD><%=lstYear%></TD><TD><b>Assettype:</b></TD><TD><%=lstAssetType%></TD></TR>
					<TR><TD><b>Month:</b></TD><TD><%=lstMonth%></TD><TD><b>Scheme:</b></TD><TD><%=lstScheme%></TD></TR>
				</TABLE>
			</TD>
			<TD WIDTH=5px>&nbsp;</TD>
		<TR><TD COLSPAN=2 ALIGN=RIGHT><input ONCLICK="Go()" type="button" class="RSLButton" name="btnGo" value=" Go ">&nbsp;&nbsp;</TD></TR>
		<% If fiscalyear <> "" Then %>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='SC_DIV'>
									<TABLE WIDTH=250PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
										<%=strServiceComplaintsData%>
										<TR><TD>Response Overdue</TD><TD ALIGN=RIGHT><%=overdue%></TD></TR>
										<TR><TD>Completed within timeframe (b)</TD><TD ALIGN=RIGHT><%=withinTimeFrame%></TD></TR>
										<TR><TD>% Performance</TD><TD ALIGN=RIGHT><%=FormatNumber(performance,2)%></TD></TR>
									</TABLE>
										</DIV></TD></TR>
				<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='ASB_DIV'>
								<TABLE WIDTH=400 CELLPADDING=3 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=3><b>Anti Social Behaviour</b></TD></TR>
									<TR><TD>Total number of cases</TD><TD ALIGN=RIGHT COLSPAN=2><%=asbTotal%></TD></TR>
									<TR><TD>Total Closed</TD><TD ALIGN=RIGHT COLSPAN=2><%=asbTotalClosed%></TD></TR>
									<TR><TD>% Performance</TD><TD ALIGN=RIGHT COLSPAN=2><%=FormatNumber(asbPerformance,2)%></TD></TR>
									<TR BGCOLOR=SILVER><TD>Nature</TD><TD>Logged</TD><TD>Closed</TD></TR>
										<%=strASBData%>
									</TABLE>
				</DIV></TD></TR>
		<% End If %>
	</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</FORM>
</BODY>
</HTML>
