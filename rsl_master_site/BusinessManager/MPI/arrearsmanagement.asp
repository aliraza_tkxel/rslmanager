<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/BusinessManager/MPI/includes/arrears.asp" -->
<%
	Dim lstScheme, lstAssetType, lstMonth, lstYear
	Dim fiscalyear, assettype, month, scheme
	' RENTINCOME VARIABLES
	Dim strRentIncomeData, total, potentialIncome
	' ARREARS VARIABLES
	Dim strArrearsData
	' PAYMENT AGREEMENT VARIABLES
	Dim paTotal, paCurrent, paPrevious, paOpen
	' RECEIPT VARIABLES
	Dim rTotal, rCurrent, rPrevious, rHB, rSupport, rAntHB, rAntSupport
	' ARREARS RECOVERY VARIABLES - MUST REMAIN AS IT IS USED IN INCLUDE FILE ABOVE
	Dim strArrData
	
	
	OpenDB()
	
	' GET QUERYSTRING
	fiscalyear = ""
	If Request("sel_YEAR") <> "" Then fiscalyear = Request("sel_YEAR") End If
	assettype = ""
	If Request("sel_ASSETTYPE") <> "" Then assettype = Request("sel_ASSETTYPE") End If
	month = ""
	If Request("sel_MONTH") <> "" Then month = Request("sel_MONTH") End If
	scheme = ""
	If Request("sel_SCHEME") <> "" Then scheme = Request("sel_SCHEME") End If
	
	
	' GET SCHEME LIST
	Call BuildSelect(lstScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME", "SCHEMENAME", "All", scheme, NULL, "textbox200", " ")
	' GET ASSET TYPE LIST
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "All", assettype, NULL, "textbox200", "  ")
	' GET GET MONTH LIST
	Call BuildSelect(lstMonth, "sel_MONTH", "G_MONTH", "SHORTDESCRIPTION, DESCRIPTION", "MONTHID", "All", month, NULL, "textbox200", " style='width:180px' ")
	' GET YEAR LIST
	Call BuildSelect(lstYear, "sel_YEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2005'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select", fiscalyear, NULL, "textbox200", " tabindex=1 style='width:180px' ")			  
	
	' IF SUBMITTED BUILD DATA
	If fiscalyear <> "" Then
		Call loadRentIncome()
		Call loadArrears()
		Call loadPaymentAgreements()
		Call loadReceipts()
		Call loadArrearsRecovery() ' FUNCTION LOCATED IN INCLUDE FILE
	End If
	
	CloseDB()

	Function loadReceipts()

		rTotal = 0
		rCurrent = 0
		rPrevious = 0
		rHB = 0
		rSupport = 0
		
		SQL = " EXEC MPI_4_RECEIPTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 1 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rTotal = Abs(rsSet(0)) End If
		SQL = " EXEC MPI_4_RECEIPTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 2 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rCurrent = Abs(rsSet(0)) End If
		SQL = " EXEC MPI_4_RECEIPTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 3 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rPrevious = Abs(rsSet(0)) End If
		SQL = " EXEC MPI_4_RECEIPTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 4 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rHB = Abs(rsSet(0)) End If		
		SQL = " EXEC MPI_4_RECEIPTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 5 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rSupport = Abs(rsSet(0)) End If		
		SQL = " EXEC MPI_4_RECEIPTS_ANTICIPATED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 1 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rAntHB = Abs(rsSet(0)) End If		
		SQL = " EXEC MPI_4_RECEIPTS_ANTICIPATED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&", 20 "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rAntSupport = Abs(rsSet(0)) End If		
		
		CloseRs(rsSet)		

	End Function
	
	Function loadPaymentAgreements()

		paTotal = 0 
		paCurrent = 0
		paPrevious = 0
		paOpen = 0
		
		SQL = " EXEC MPI_4_TOTAL_PAYMENTAGREEMENTS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then  paTotal = rsSet(0) End If
		SQL = " EXEC MPI_4_TOTAL_PAYMENTAGREEMENTS_CURRENT '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then  paCurrent = rsSet(0) End If
		SQL = " EXEC MPI_4_TOTAL_PAYMENTAGREEMENTS_PREVIOUS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then  paPrevious = rsSet(0) End If
		SQL = " EXEC MPI_4_TOTAL_PAYMENTAGREEMENTS_OPEN '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then  paOpen = rsSet(0) End If		
		
		CloseRs(rsSet)		

	End Function
	
	Function loadArrears()

		strArrearsData = ""

		SQL = " EXEC MPI_4_ARREARS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			strArrearsData = strArrearsData & "<TR><TD>Number of tenants in arrears</TD><TD ALIGN=RIGHT>"&rsSet(0)&"</TD><TD ALIGN=RIGHT>"&FormatCurrency(rsSet(1),2)&"</TD></TR>"
		End If
		SQL = " EXEC MPI_4_ARREARS_CURRENT '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			strArrearsData = strArrearsData & "<TR><TD>Current tenants</TD><TD ALIGN=RIGHT>"&rsSet(0)&"</TD><TD ALIGN=RIGHT>"&FormatCurrency(rsSet(1),2)&"</TD></TR>"
		End If
		SQL = " EXEC MPI_4_ARREARS_PREVIOUS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			strArrearsData = strArrearsData & "<TR><TD>Previous tenants</TD><TD ALIGN=RIGHT>"&rsSet(0)&"</TD><TD ALIGN=RIGHT>"&FormatCurrency(rsSet(1),2)&"</TD></TR>"
		End If
		CloseRs(rsSet)		

	End Function

	
	Function loadRentIncome()

		strRentIncomeData = ""
		total = 0
		strRentIncomeData = strRentIncomeData & "<TABLE WIDTH=350PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>"
		strRentIncomeData = strRentIncomeData & "<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Rent Income</b></TD></TR>"

		SQL = " EXEC MPI_4_RENTINCOME '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			potentialIncome = rsSet("RENTDUE")
			strRentIncomeData = strRentIncomeData & "<TR><TD>Let property potential income</TD><TD ALIGN=RIGHT>"&FormatCurrency(potentialIncome,2)&"</TD></TR>"
		End If
		total = total + rsSet("RENTDUE")
		Set rsSet = rsSet.NextRecordSet()
		If Not rsSet.EOF Then
			strRentIncomeData = strRentIncomeData & "<TR><TD>Available to let potential income</TD><TD ALIGN=RIGHT>"&FormatCurrency(rsSet("AVAILABLE"),2)&"</TD></TR>"
		End If
		total = total + rsSet("AVAILABLE")
		Set rsSet = rsSet.NextRecordSet()
		If Not rsSet.EOF Then
			strRentIncomeData = strRentIncomeData & "<TR><TD>Unavailable income loss</TD><TD ALIGN=RIGHT>"&FormatCurrency(rsSet("UNAVAILABLE"),2)&"</TD></TR>"
		End If
		total = total + rsSet("UNAVAILABLE")
		CloseRs(rsSet)		
		
		strRentIncomeData = strRentIncomeData & "<TR><TD>Maximum rent receivable</TD><TD ALIGN=RIGHT>"&FormatCurrency(total,2)&"</TD></TR>"
		strRentIncomeData = strRentIncomeData & "</TABLE>"

	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>MPI</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	
	function Go(){

		thisForm.submit();
	
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<FORM NAME=thisForm METHOD=POST ACTION="ARREARSMANAGEMENT.ASP">
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR><TD WIDTH=5px ROWSPAN=20>&nbsp;</TD>
			<TD>
			<TABLE WIDTH=200px HEIGHT=55PX CELLPADDING=1 CELLSPACING=2 STYLE='BORDER:1PX SOLID BLACK' BORDER=0 VALIGN=TOP>
				<TR><TD COLSPAN=4><b><U>Property Status</U></b></TD></TR>
			<!--#include virtual="/BusinessManager/MPI/includes/propertystatus.asp" -->
				<TR><TD COLSPAN=4 HEIGHT=100%></TD></TR>
				</TABLE>
			</TD>
			<TD ALIGN=RIGHT>
				<TABLE WIDTH=400px CELLPADDING=1 CELLSPACING=2 BORDER=0>
					<TR><TD><b>Year:</b></TD><TD><%=lstYear%></TD><TD><b>Assettype:</b></TD><TD><%=lstAssetType%></TD></TR>
					<TR><TD><b>Month:</b></TD><TD><%=lstMonth%></TD><TD><b>Scheme:</b></TD><TD><%=lstScheme%></TD></TR>
				</TABLE>
			</TD>
			<TD WIDTH=5px>&nbsp;</TD>
		<TR><TD COLSPAN=2 ALIGN=RIGHT><input ONCLICK="Go()" type="button" class="RSLButton" name="btnGo" value=" Go ">&nbsp;&nbsp;</TD></TR>
		<% If fiscalyear <> "" Then %>
			<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='ASB_DIV'>
										<%=strRentIncomeData%>
									</DIV></TD></TR>
			<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='CUSTOMER_DIV'>
								<TABLE WIDTH=400PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=3><b>Customer Stats</b></TD></TR>
									<%=strArrearsData%>
									<TR STYLE='HEIGHT:1PX'><TD COLSPAN=3 STYLE='BORDER-BOTTOM:1PX SOLID BLACK'></TD></TR>
									<TR><TD>Number of payment agreements</TD><TD ALIGN=RIGHT><%=paTotal%></TD><TD ALIGN=RIGHT></TD></TR>
									<TR><TD>Current customers</TD><TD ALIGN=RIGHT><%=paCurrent%></TD><TD ALIGN=RIGHT></TD></TR>
									<TR><TD>Previous customers</TD><TD ALIGN=RIGHT><%=paPrevious%></TD><TD ALIGN=RIGHT></TD></TR>
									<TR><TD>% Awaiting action</TD><TD ALIGN=RIGHT><%=paOpen%></TD><TD ALIGN=RIGHT></TD></TR>
									<TR STYLE='HEIGHT:1PX'><TD COLSPAN=3 STYLE='BORDER-BOTTOM:1PX SOLID BLACK'></TD></TR>
									<TR><TD>Total receipts</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rTotal,2)%></TD></TR>
									<TR><TD>Direct payments from current customers</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rCurrent,2)%></TD></TR>
									<TR><TD>Direct payments from previous customers</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rPrevious,2)%></TD></TR>
									<TR><TD>Housing benefit for current customers</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rHB,2)%></TD></TR>
									<TR><TD>Supporting people for current customers</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rSupport,2)%></TD></TR>
									<TR STYLE='HEIGHT:0PX'><TD COLSPAN=3 STYLE='BORDER-BOTTOM:1PX SOLID BLACK'></TD></TR>
									<TR><TD>Anticipated housing benefit</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rAntHB,2)%></TD></TR>
									<TR><TD>Anticipated supporting people</TD><TD ALIGN=RIGHT></TD><TD ALIGN=RIGHT><%=FormatCurrency(rAntSupport,2)%></TD></TR>
								</TABLE>
								<TABLE WIDTH=500PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=3><b>Financial Summary</b></TD></TR>
									<TR><TD>Let property potential summary - Total income received</TD><TD ALIGN=RIGHT><%=FormatCurrency(potentialIncome - rTotal,2)%></TD></TR>
								</TABLE>
							</DIV></TD></TR>
			<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='ASB_DIV'>
										<%=strArrData%>
									</DIV></TD></TR>
		<% End If %>
	</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</FORM>
</BODY>
</HTML>
