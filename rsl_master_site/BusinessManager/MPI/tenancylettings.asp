<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstScheme, lstAssetType, lstMonth, lstYear
	Dim fiscalyear, assettype, month, scheme
	' CUSTOMER VARIABLES
	Dim newTenancies, terminations, customers, tenancies, pendingTerminations, pendingLettings
	' VOIDs VARIABLES
	Dim voidValues
	
	OpenDB()
	
	' GET QUERYSTRING
	fiscalyear = ""
	If Request("sel_YEAR") <> "" Then fiscalyear = Request("sel_YEAR") End If
	assettype = ""
	If Request("sel_ASSETTYPE") <> "" Then assettype = Request("sel_ASSETTYPE") End If
	month = ""
	If Request("sel_MONTH") <> "" Then month = Request("sel_MONTH") End If
	scheme = ""
	If Request("sel_SCHEME") <> "" Then scheme = Request("sel_SCHEME") End If
	
	
	' GET SCHEME LIST
	Call BuildSelect(lstScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME", "SCHEMENAME", "All", scheme, NULL, "textbox200", " ")
	' GET ASSET TYPE LIST
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "All", assettype, NULL, "textbox200", "  ")
	' GET GET MONTH LIST
	Call BuildSelect(lstMonth, "sel_MONTH", "G_MONTH", "SHORTDESCRIPTION, DESCRIPTION", "MONTHID", "All", month, NULL, "textbox200", " style='width:180px' ")
	' GET YEAR LIST
	Call BuildSelect(lstYear, "sel_YEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2005'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select", fiscalyear, NULL, "textbox200", " tabindex=1 style='width:180px' ")			  
	
	' IF SUBMITTED BUILD DATA
	If fiscalyear <> "" Then
		Call loadCustomers()
		Call loadVoids()
	End If
	
	CloseDB()
	
	Function loadVoids()
	
		Dim vrSubscr
		' FIRST OF ALL GET THE COUNT OF VOID RANGES TO BE USED AS SUBSCRIPT FOR ARRAYS
		SQL = "SELECT COUNT(*) AS NUM FROM MPI_VOIDRANGE" 
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then vrSubscr = rsSet("NUM") Else vrSubscr = 0 End IF
		CloseRs(rsSet)
	
		' BUILD AND RESET VOID ARRAY
		ReDim voidValues(vrSubscr)
		For x = 0 To vrSubscr
			voidValues(x) = 0
		Next
		
		SQL = " EXEC MPI_2_VOIDS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF 
			voidValues(rsSet("RANGEID")) = rsSet("NUM")
			rsSet.movenext()
		Wend
		CloseRs(rsSet)		
	
	End Function
	
	Function loadCustomers()
	
		SQL = " EXEC MPI_2_NEWTENANCIES '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		'rw SQL
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then
			newTenancies		= rsSet("NEWTENANCIES")
			terminations		= rsSet("TERMINATIONS")
			customers			= rsSet("CUSTOMERS")
			tenancies			= rsSet("TENANCIES")
			pendingTerminations	= rsSet("PENDINGTERMINATIONS")
			pendingLettings		= rsSet("PENDINGLETTINGS")
			rsSet.Movenext()
		Else
			newTenancies		= 0
			terminations		= 0
			customers			= 0
			tenancies			= 0
			pendingTerminations	= 0
			pendingLettings		= 0

		End If
		CloseRs(rsSet)
	End Function 
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>MPI</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
		
	function process(){

		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/process_ApprovalList_srv.asp?";
		RSLFORM.submit();
	}
	
	function Go(){

		thisForm.submit();
	
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<FORM NAME=thisForm METHOD=POST ACTION="TENANCYLETTINGS.ASP">
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR><TD WIDTH=5px ROWSPAN=20>&nbsp;</TD>
			<TD>
			<TABLE WIDTH=200px HEIGHT=55PX CELLPADDING=1 CELLSPACING=2 STYLE='BORDER:1PX SOLID BLACK' BORDER=0 VALIGN=TOP>
				<TR><TD COLSPAN=4><b><U>Property Status</U></b></TD></TR>
			<!--#include virtual="/BusinessManager/MPI/includes/propertystatus.asp" -->
				<TR><TD COLSPAN=4 HEIGHT=100%></TD></TR>
			</TABLE>
			</TD>
			<TD ALIGN=RIGHT>
				<TABLE WIDTH=400px CELLPADDING=1 CELLSPACING=2 BORDER=0>
					<TR><TD><b>Year:</b></TD><TD><%=lstYear%></TD><TD><b>Assettype:</b></TD><TD><%=lstAssetType%></TD></TR>
					<TR><TD><b>Month:</b></TD><TD><%=lstMonth%></TD><TD><b>Scheme:</b></TD><TD><%=lstScheme%></TD></TR>
				</TABLE>
			</TD>
			<TD WIDTH=5px>&nbsp;</TD>
		<TR><TD COLSPAN=2 ALIGN=RIGHT><input ONCLICK="Go()" type="button" class="RSLButton" name="btnGo" value=" Go ">&nbsp;&nbsp;</TD></TR>
		<% If fiscalyear <> "" Then %>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='CUSTOMER_DIV'>
								<TABLE WIDTH=300PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Customers</b></TD></TR>
									<TR><TD>New tenancies created (a)</TD><TD ALIGN=RIGHT><%=newTenancies%></TD></TR>
									<TR><TD>Terminations (b)</TD><TD ALIGN=RIGHT><%=terminations%></TD></TR>
									<TR><TD>Number of customers</TD><TD ALIGN=RIGHT><%=customers%></TD></TR>
									<TR><TD>Number of tenancies</TD><TD ALIGN=RIGHT><%=tenancies%></TD></TR>
									<TR><TD>Terminations pending</TD><TD ALIGN=RIGHT><%=pendingTerminations%></TD></TR>
									<TR><TD>Lettings pending</TD><TD ALIGN=RIGHT><%=pendingLettings%></TD></TR>
									<TR><TD>Average turnaround</TD><TD ALIGN=RIGHT><%=(newTenancies-terminations)%></TD></TR>
							</TABLE>
							</DIV></TD></TR>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='CUSTOMER_DIV'>
								<TABLE WIDTH=300PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Void Properties</b></TD></TR>
									<TR><TD>Less than one month</TD><TD ALIGN=RIGHT><%=voidValues(1)%></TD></TR>
									<TR><TD>>= 1 month < 3 Months</TD><TD ALIGN=RIGHT><%=voidValues(2)%></TD></TR>
									<TR><TD>>= 3 months < 6 months</TD><TD ALIGN=RIGHT><%=voidValues(3)%></TD></TR>
									<TR><TD>>= 6 months < 12 Months</TD><TD ALIGN=RIGHT><%=voidValues(4)%></TD></TR>
									<TR><TD>>= 12 months</TD><TD ALIGN=RIGHT><%=voidValues(5)%></TD></TR>
							</TABLE>
							</DIV></TD></TR>
		<% End If %>
	</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</FORM>
</BODY>
</HTML>
