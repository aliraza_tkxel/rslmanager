<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lstScheme, lstAssetType, lstMonth, lstYear
	Dim fiscalyear, assettype, month, scheme
	Dim strExpenditureData
	' SCHEME VARIABLES
	Dim majorRepairsInProgress, majorRepairsCompleted, numberRecharges, valueRecharges, rechargeIncome
	' DAY TO DAY VARIABLES
	Dim dayToDayReported, dayToDayReportedAverage, dayToDayReportedCost, dayToDayReportedTotal, dayToDayReportedCostTotal
	Dim dayToDayCompleted, dayToDayCompletedAverage, dayToDayCompletedCost, dayToDayCompletedTotal, dayToDayCompletedCostTotal
	
	Redim dayToDayReported(3)
	Redim dayToDayReportedAverage(3)
	Redim dayToDayReportedCost(3)
	
	Redim dayToDayCompleted(3)
	Redim dayToDayCompletedAverage(3)
	Redim dayToDayCompletedCost(3)
	
	OpenDB()
	
	' GET QUERYSTRING
	fiscalyear = ""
	If Request("sel_YEAR") <> "" Then fiscalyear = Request("sel_YEAR") End If
	assettype = ""
	If Request("sel_ASSETTYPE") <> "" Then assettype = Request("sel_ASSETTYPE") End If
	month = ""
	If Request("sel_MONTH") <> "" Then month = Request("sel_MONTH") End If
	scheme = ""
	If Request("sel_SCHEME") <> "" Then scheme = Request("sel_SCHEME") End If
	
	
	' GET SCHEME LIST
	Call BuildSelect(lstScheme, "sel_SCHEME", "P_SCHEME", "SCHEMEID, SCHEMENAME", "SCHEMENAME", "All", scheme, NULL, "textbox200", " ")
	' GET ASSET TYPE LIST
	Call BuildSelect(lstAssetType, "sel_ASSETTYPE", "P_ASSETTYPE WHERE IsActive=1", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "All", assettype, NULL, "textbox200", "  ")
	' GET GET MONTH LIST
	Call BuildSelect(lstMonth, "sel_MONTH", "G_MONTH", "SHORTDESCRIPTION, DESCRIPTION", "MONTHID", "All", month, NULL, "textbox200", " style='width:180px' ")
	' GET YEAR LIST
	Call BuildSelect(lstYear, "sel_YEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2005'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select", fiscalyear, NULL, "textbox200", " tabindex=1 style='width:180px' ")			  
	
	' IF SUBMITTED BUILD DATA
	If fiscalyear <> "" Then
		Call loadExpenditure()
		Call loadScheme()
		Call loadDayToDay()
	End If
	
	CloseDB()
	
	Function loadDayToDay()
	
		' FIRST SET ALL VALUES IN ARRAY TO ZERO
		For x = 0 To 3
			dayToDayReported(x) = 0
			dayToDayReportedCost(x) = 0
			dayToDayReportedAverage(x) = 0
			
			dayToDayCompleted(x) = 0
			dayToDayCompletedCost(x) = 0			
		Next
		
		' RESET TOTAL VARIABLES
		dayToDayReportedTotal = 0
		dayToDayCompletedTotal = 0
		dayToDayReportedCostTotal = 0
		dayToDayCompletedCostTotal = 0
		
		' GET DATA FOR REPORTED REPAIRS
		SQL = " EXEC MPI_1_DAYTODAY_REPAIRS_REPORTED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			dayToDayReported(rsSet("SORTORDER")) = rsSet("LOGGED")
			dayToDayReportedCost(rsSet("SORTORDER")) = rsSet("COST")
			dayToDayReportedTotal = dayToDayReportedTotal + rsSet("LOGGED")
			dayToDayReportedCostTotal = dayToDayReportedCostTotal + rsSet("COST")
			rsSet.movenext()
		Wend
		Call CloseRs(rsSet)
		
		' CALCULATE AVERAGES IF TOTAL NOT ZERO
		If dayToDayReportedTotal <> 0 Then
			For x = 0 To 3
				dayToDayReportedAverage(x) = dayToDayReported(x)/dayToDayReportedTotal*100
			Next
		End If
	
		' GET DATA FOR COMPLETED REPAIRS
		SQL = " EXEC MPI_1_DAYTODAY_REPAIRS_COMPLETED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		While Not rsSet.EOF
			dayToDayCompleted(rsSet("SORTORDER")) = rsSet("COMPLETED")
			dayToDayCompletedCost(rsSet("SORTORDER")) = rsSet("COST")
			dayToDayCompletedTotal = dayToDayCompletedTotal + rsSet("COMPLETED")
			dayToDayCompletedCostTotal = dayToDayCompletedCostTotal + rsSet("COST")
			rsSet.movenext()
		Wend
		Call CloseRs(rsSet)
		
		' CALCULATE AVERAGES IF REPORTED NOT ZERO
		For x = 0 To 3
			If dayToDayReported(x) <> 0 Then
				dayToDayCompletedAverage(x) = dayToDayCompleted(x)/dayToDayReported(x)*100
			End If
		Next

	
	End Function
	
	Function loadScheme()
	
		' GET MAJOR REPAIRS IN PROGRESS
		SQL = " EXEC MPI_1_REPAIRS_IN_PROGRESS '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then majorRepairsInProgress = rsSet(0) Else majorRepairsInProgress = 0 End If
		Call CloseRs(rsSet)
		
		' GET MAJOR REPAIRS COMPLETED
		SQL = " EXEC MPI_1_REPAIRS_COMPLETED '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then majorRepairsCompleted = rsSet(0) Else majorRepairsCompleted = 0 End If
		Call CloseRs(rsSet)

		' GET TENANT RECHARGES
		SQL = " EXEC MPI_1_TENANT_RECHARGE '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then 
			numberRecharges = rsSet("NUMOFRECHARGES")
			valueRecharges	= rsSet("VALUEOFRECHARGES")
		Else 
			numberRecharges = 0
			valueRecharges	= 0
		End If
		Call CloseRs(rsSet)
		
		' GET MAJOR REPAIRS COMPLETED
		SQL = " EXEC MPI_1_TENANT_RECHARGE_INCOME '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then rechargeIncome = rsSet(0) Else rechargeIncome = 0 End If
		Call CloseRs(rsSet)

	End Function
	
	Function loadExpenditure()
	
		Dim costcentre, qtotal, ptotal
	
		strExpenditureData = "<TABLE CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>"	
		SQL = " EXEC MPI_1_EXPENDITURE '"&scheme&"', '"&assettype&"', '"&month&"', "&fiscalyear&" "
		Call OpenRs(rsSet, SQL)
		costcenter = rsSet("COSTCENTRE")
		While Not rsSet.EOF
			If costcentre <> rsSet("COSTCENTRE") Then ' IF NEW COSTCENTRE PUT TITLE BAR
				costcentre = rsSet("COSTCENTRE")
				strExpenditureData = strExpenditureData & "<TR BGCOLOR=SILVER><TD>" & rsSet("COSTCENTRE") & "</TD><TD>Queued & Ordered</TD><TD>Reconciled and Paid</TD></TR>"
			End If				
			strExpenditureData = strExpenditureData & "<TR><TD>" & rsSet("DESCRIPTION") & "</TD>"
			strExpenditureData = strExpenditureData & "<TD ALIGN=RIGHT>" & FormatCurrency(rsSet("QUEUEDORDERED"),2) & "</TD>"
			strExpenditureData = strExpenditureData & "<TD ALIGN=RIGHT>" & FormatCurrency(rsSet("TOTALPURCHASES"),2) & "</TD></TR>"
			qtotal = qtotal + rsSet("QUEUEDORDERED")
			ptotal = ptotal + rsSet("TOTALPURCHASES")
			rsSet.Movenext()
			If not rsSet.EOF Then ' IF NEW COSTCENTRE WRITE TOTALS
				If costcentre <> rsSet("COSTCENTRE") Then
					strExpenditureData = strExpenditureData & "<TR BGCOLOR=SILVER><TD></TD><TD ALIGN=RIGHT>" & FormatCurrency(qtotal,2) & "</TD><TD ALIGN=RIGHT>" & FormatCurrency(ptotal,2) & "</TD></TR>"
					qtotal = 0
					ptotal = 0
				End If				
			End If
		Wend
		' WRITE FINAL TOTAL AND CLOSE TABLE
		strExpenditureData = strExpenditureData & "<TR BGCOLOR=SILVER><TD></TD><TD ALIGN=RIGHT>" & FormatCurrency(qtotal,2) & "</TD><TD ALIGN=RIGHT>" & FormatCurrency(ptotal,2) & "</TD></TR>"
		strExpenditureData = strExpenditureData & "</TABLE>"
		CloseRs(rsSet)
	End Function 
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>MPI</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
		
	function Go(){

		thisForm.submit();
	
	}
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<FORM NAME=thisForm METHOD=POST ACTION="ASSETMANAGEMENT.ASP">
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR><TD WIDTH=5px ROWSPAN=20>&nbsp;</TD>
			<TD>
			<TABLE WIDTH=200px HEIGHT=55PX CELLPADDING=1 CELLSPACING=2 STYLE='BORDER:1PX SOLID BLACK' BORDER=0 VALIGN=TOP>
				<TR><TD COLSPAN=4><b><U>Property Status</U></b></TD></TR>
			<!--#include virtual="/BusinessManager/MPI/includes/propertystatus.asp" -->
				<TR><TD COLSPAN=4 HEIGHT=100%></TD></TR>
				</TABLE>
			</TD>
			<TD ALIGN=RIGHT>
				<TABLE WIDTH=400px CELLPADDING=1 CELLSPACING=2 BORDER=0>
					<TR><TD><b>Year:</b></TD><TD><%=lstYear%></TD><TD><b>Assettype:</b></TD><TD><%=lstAssetType%></TD></TR>
					<TR><TD><b>Month:</b></TD><TD><%=lstMonth%></TD><TD><b>Scheme:</b></TD><TD><%=lstScheme%></TD></TR>
				</TABLE>
			</TD>
			<TD WIDTH=5px>&nbsp;</TD>
		<TR><TD COLSPAN=2 ALIGN=RIGHT><input ONCLICK="Go()" type="button" class="RSLButton" name="btnGo" value=" Go ">&nbsp;&nbsp;</TD></TR>
		<% If fiscalyear <> "" Then %>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='EXP_DIV'><%=strExpenditureData%></DIV></TD></TR>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='SCH_DIV'>
								<TABLE WIDTH=300PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=2><b>Scheme</b></TD></TR>
									<TR><TD>Number of major repairs in progress</TD><TD ALIGN=RIGHT><%=majorRepairsInProgress%></TD></TR>
									<TR><TD>Number of completed major repairs</TD><TD ALIGN=RIGHT><%=majorRepairsCompleted%></TD></TR>
									<TR><TD>Number completed outside timeframe</TD><TD ALIGN=RIGHT></TD></TR>
									<TR><TD COLSPAN=2>&nbsp;</TD></TR>
									<TR><TD>Av time on change tenancy repair</TD><TD ALIGN=RIGHT></TD></TR>
									<TR><TD>Number of tenant recharges</TD><TD ALIGN=RIGHT><%=numberRecharges%></TD></TR>
									<TR><TD>Value of tenant recharges</TD><TD ALIGN=RIGHT><%=FormatCurrency(valueRecharges,2)%></TD></TR>
									<TR><TD>Recharge Income</TD><TD ALIGN=RIGHT><%=FormatCurrency(rechargeIncome,2)%></TD></TR>
							</TABLE>
							</DIV></TD></TR>
		<TR><TD HEIGHT=100PX COLSPAN=2><DIV ID='DD_DIV'>
								<TABLE WIDTH=700PX CELLPADDING=2 CELLSPACING=2 BORDER=1 STYLE='BORDER-COLLAPSE:COLLAPSE'>
									<TR BGCOLOR=SILVER><TD COLSPAN=7><b>Day To Day Repairs</b></TD></TR>
									<TR>
										<TD>Type</TD>
										<TD ALIGN=CENTER>No. Reported</TD>
										<TD ALIGN=CENTER>Cost</TD>
										<TD ALIGN=CENTER>No. Completed</TD>
										<TD ALIGN=CENTER>Cost</TD>
										<TD ALIGN=CENTER>Av. No. of Repairs</TD>
										<TD ALIGN=CENTER>% Against Portfolio</TD>
									</TR>
									<TR>
										<TD>Emergency</TD>
										<TD ALIGN=CENTER><%=dayToDayReported(1)%> (<%=FormatNumber(dayToDayReportedAverage(1),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayReportedCost(1),2)%></TD>
										<TD ALIGN=CENTER><%=dayToDayCompleted(1)%> (<%=FormatNumber(dayToDayCompletedAverage(1),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayCompletedCost(1),2)%></TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
									</TR>
									<TR>
										<TD>Urgent</TD>
										<TD ALIGN=CENTER><%=dayToDayReported(2)%> (<%=FormatNumber(dayToDayReportedAverage(2),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayReportedCost(2),2)%></TD>
										<TD ALIGN=CENTER><%=dayToDayCompleted(2)%> (<%=FormatNumber(dayToDayCompletedAverage(2),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayCompletedCost(2),2)%></TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
									</TR>
									<TR>
										<TD>Routine</TD>
										<TD ALIGN=CENTER><%=dayToDayReported(3)%> (<%=FormatNumber(dayToDayReportedAverage(3),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayReportedCost(3),2)%></TD>
										<TD ALIGN=CENTER><%=dayToDayCompleted(3)%> (<%=FormatNumber(dayToDayCompletedAverage(3),2)%>%)</TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayCompletedCost(3),2)%></TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
									</TR>
									<TR BGCOLOR=SILVER>
										<TD>&nbsp;</TD>
										<TD ALIGN=CENTER><%=dayToDayReportedTotal%></TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayReportedCostTotal,2)%></TD>
										<TD ALIGN=CENTER><%=dayToDayCompletedTotal%></TD>
										<TD ALIGN=CENTER><%=FormatCurrency(dayToDayCompletedCostTotal,2)%></TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
										<TD ALIGN=CENTER>&nbsp;</TD>
									</TR>
							</TABLE>
							</DIV></TD></TR>
		<% End If %>
	</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_slip width=300px height=300px style='display:none'></iframe>
</FORM>
</BODY>
</HTML>
