<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true
    Response.ContentType = "application/vnd.ms-excel"
%>
<!--#include virtual="ACCESSCHECK_export.asp" -->
<%
	PageName = "SicknessAbsence.asp"

	' only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	' redirect all director to a higher level showing their teams
	'if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, theToDate
	Dim searchName, searchSQL, orderSQL, DEFAULTstring
	Dim TOTALDAYSABSENT, whereSQL, TOTALCOST, theFinalCost, EMPSQL, EmployeeType, DEPTName

	Call GetVariables()
	Call getTeams()
	Call OpenDB()
	Call GetDepartmentReportVars()
	Call CloseDB()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()

		theDate = Request.QueryString("thedate")
		theToDate = Request.QueryString("theToDate")
		Dapartment = Request.QueryString("DEPT")

		If (Request.QueryString("EmployeeType") <> "") Then
			EmployeeType = Request.QueryString("EmployeeType")
		Else
			EmployeeType = "Current"
		End If

		OrderBy = Request.QueryString("CC_SORT")

		If(Request.QueryString("DEPTName")<>"") then
			DEPTName = Request.QueryString("DEPTName")
		Else
			DEPTName = "All"
		End If

		SortForPrintandXLS = OrderBy

		If OrderBy <> "" Then
			orderSQL = " ORDER BY " & OrderBy
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End If

		If Dapartment <> "" Then
			deptSQL = " AND T.TEAMID =  " & Dapartment & " "
		End If

		If EmployeeType = "Previous" Then
			EMPSQL = " AND (JD.ENDDATE IS NOT NULL OR JD.ACTIVE IN (0))"
		ElseIf EmployeeType = "All" Then
			EMPSQL = " "
		Else
			EMPSQL = " AND (JD.ENDDATE IS NULL AND JD.ACTIVE = 1)"
		End If

		If theDate = "" And theToDate = "" Then
			theDate = "1/" & month(Date) & "/" & year(Date)
			theToDate = FormatDateTime(Date,2)
		End If

		dateSQL =  dateSQL & " AND ((a.STARTDATE BETWEEN '" & theDate & "' AND '" & theToDate & "') OR (ISNULL(a.RETURNDATE,CONVERT(DATE,GETDATE())) BETWEEN '" & theDate & "' AND '" & theToDate & "') or (a.STARTDATE < '" & theDate & "' AND ISNULL(A.RETURNDATE,CONVERT(DATE,GETDATE())) > '" & theToDate & "'))"

		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate & "&theTodate=" & theToDate & "&EmployeeType=" & EmployeeType 
		whereSQL =  dateSQL & " " & deptSQL & " " & EMPSQL

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 		" SELECT Distinct E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, PFT.DESCRIPTION AS PARTFULLTIME, JD.STARTDATE AS EmployeeStartDate, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.REASON, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM " &_
                        " , CASE "                                                                            &_
                        "  WHEN A.returndate IS NULL AND GETDATE() < '" & theToDate & "' THEN "                          &_
						"        dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "' , GETDATE(), J.journalid) " &_
                        "  ELSE                                                                             " &_
						"       dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "',  '" & theToDate & "', J.journalid)   " &_
                        " END                                      AS SICKNESSABSENCE "                      &_
                        "		,	LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON " &_
						"		, JD.SALARY AS SALARY " &_
                        "       , JD.JOBTITLE " &_
						" FROM  E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_ 

                        "        AND ((A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' ) " &_
			            "                 OR (A.RETURNDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' AND A.STARTDATE < '" & theDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theToDate & "' AND A.RETURNDATE IS NULL)) " &_
                        "        AND (A.STARTDATE <= '" & theToDate & "' ) " &_    
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = J.JOURNALID AND J.EMPLOYEEID=E.EMPLOYEEID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "		LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
                        "       LEFT JOIN E_PARTFULLTIME PFT ON JD.PARTFULLTIME = PFT.PARTFULLTIMEID " &_
						" WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND  T.TEAMID <> 1 AND T.ACTIVE=1  " &_
						" " & whereSQL & EMPSQL & orderSQL

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		count = 0
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
    	theFinalCost = 0.0
        TOTALDAYSABSENT = 0

			while not rsSet.eof
				str_data = str_data & "<tbody><tr>" &_
					"<td width=""100px"">" & rsSet("LASTNAME") & "</td>" &_
					"<td width=""106px"" align=""left"">" & rsSet("FIRSTNAME") & "</td>" &_
                    "<td align=""left"">" & rsSet("PARTFULLTIME") & "</td>" &_
                    "<td align=""left"">" & rsSet("EmployeeStartDate") & "</td>" &_
					"<td align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
					"<td width=""111px"" align=""left"">" & rsSet("TEAMNAME") & "</td>" &_
                    "<td width=""77px"" align=""center"">" & rsSet("STARTDATE") & "</td>"&_
					"<td width=""117px"" align=""center"">" & rsSet("RETURNDATE")  & "</td>" &_
					"<td width=""54px"" align=""center"">" & rsSet("SICKNESSABSENCE")  & "</td>" &_
					"<td width=""216px"" align=""left"">" & rsSet("REASON") & "</td>"

					If rsSet("TEAMNUM") > 0 then
						thenum = IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE")))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(thenum) & "</td>"
					Else
						thenum  = IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE")))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(thenum) & "</td>"
					End If

					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				wend

			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

		' if no teams exist inform the user
		If count = 0 Then
			str_data = "<tfoot><tr><td colspan=""10"" style=""font:18px"" align=""center"">No records to display</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

			'Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""10"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

    Function GetDepartmentReportVars()
		SQL = 		" SELECT Distinct E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, PFT.DESCRIPTION AS PARTFULLTIME, JD.STARTDATE AS EmployeeStartDate, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.REASON, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM " &_
                        " , CASE "                                                                            &_
                        "  WHEN A.returndate IS NULL AND GETDATE() < '" & theToDate & "' THEN "                          &_
						"        dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "' , GETDATE(), J.journalid) " &_
                        "  ELSE                                                                             " &_
						"       dbo.Emp_sickness_duration(E.employeeid,'" & theDate & "',  '" & theToDate & "', J.journalid)   " &_
                        " END                                      AS SICKNESSABSENCE "                      &_
                        "		,	LTRIM(ISNULL(ER.DESCRIPTION,'') + ' ' + ISNULL(A.REASON, '')) AS REASON " &_
						"		, JD.SALARY AS SALARY " &_
                        "       , JD.JOBTITLE " &_
						" FROM  E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID " &_ 

                        "        AND ((A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' ) " &_
			            "                 OR (A.RETURNDATE >= '" & theDate & "' AND A.RETURNDATE <= '" & theToDate & "' AND A.STARTDATE < '" & theDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE >= '" & theDate & "' AND A.RETURNDATE >= '" & theToDate & "' ) " &_
			            "                 OR (A.STARTDATE <= '" & theToDate & "' AND A.RETURNDATE IS NULL)) " &_
                        "        AND (A.STARTDATE <= '" & theToDate & "' ) " &_    
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = J.JOURNALID AND J.EMPLOYEEID=E.EMPLOYEEID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "		LEFT JOIN E_ABSENCEREASON ER ON ER.SID = A.REASONID " &_
                        "       LEFT JOIN E_PARTFULLTIME PFT ON JD.PARTFULLTIME = PFT.PARTFULLTIMEID " &_
						" WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND  T.TEAMID <> 1 AND T.ACTIVE=1  " &_
						" " & whereSQL & EMPSQL & orderSQL

			theFinalCost = 0.0
            TOTALDAYSABSENT = 0


			Call OpenRs(rsLoader, SQL)			
				If (NOT rsLoader.EOF) Then
				While NOT rsLoader.eof
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
					TOTALDAYSABSENT = TOTALDAYSABSENT + rsLoader("SICKNESSABSENCE")
                    theFinalCost = theFinalCost + FormatNumber(IsNullNumber((((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE")))) 
				'==================================================================
				'= End Totals For department report
				'==================================================================
				rsLoader.MoveNext()
				Wend
				End If
			Call CloseRS(rsLoader)

	End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <form name="thisForm" method="post" action="">
    <div id="DeptInfo">
        <table width="354" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="5">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <b>From Date:</b>
                </td>
                <td>
                    <%=theDate %>
                </td>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td>
                    <b>To Date:</b>
                </td>
                <td>
                    <%=theToDate %>
                </td>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td>
                    <b>Employee Type:</b>
                </td>
                <td>
                    <%=EmployeeType %>
                </td>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td>
                    <b>Department:</b>
                </td>
                <td>
                    <%=DEPTName %>
                </td>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="5" height="10">
                </td>
            </tr>
            <tr>
                <td width="116">
                    <strong>Total Days Absent: </strong>
                </td>
                <td style="text-align: left;">
                    <%= TOTALDAYSABSENT %>
                </td>
                <td width="50">
                    &nbsp;
                </td>
                <td width="71">
                    <strong>Total Cost : </strong>
                </td>
                <td width="78">
                    <%= FormatNumber(IsNullNumber(theFinalCost)) %>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <table width="900" cellpadding="1" cellspacing="2" border="1">
        <thead>
            <tr align="left" valign="bottom">
                <td width="100">
                    <b>Surname</b>
                </td>
                <td width="106">
                    <b>First Name </b>
                </td>
                <td>
                    <b>Full/Part Time</b>
                </td>
                <td>
                    <b>Employee Start Date</b>
                </td>
                <td>
                    <b>Job Title</b>
                </td>
                <td width="111">
                    <b>Dept.</b>
                </td>
                <td width="77">
                    <b>Start Date</b>
                </td>
                <td width="117">
                    <strong>End Date </strong>
                </td>
                <td width="54">
                    <strong>Duration</strong>
                </td>
                <td width="216">
                    <b>Reason</b>
                </td>
                <td width="67" align="left" valign="bottom">
                    <b>Cost</b>
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
</body>
</html>
