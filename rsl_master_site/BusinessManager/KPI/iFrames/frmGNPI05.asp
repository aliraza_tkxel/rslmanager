<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================
	
		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue, divisor, opening_balance
		Dim A, B, C, D, E, F, G
		Dim ytd_let, ytd_available, ytd_unavailable, ytd_rentdue, ytd_initialrent, ytd_rebate, ytd_voidrent, ytd_payment
		
		Redim MONTHVALUES(MONTHCOUNT)
		Redim LET_VALUES(MONTHCOUNT)
		Redim AVAILABLE_VALUES(MONTHCOUNT)
		Redim UNAVAILABLE_VALUES(MONTHCOUNT)
		Redim RENTDUE_VALUES(MONTHCOUNT)
		Redim INITIALRENT_VALUES(MONTHCOUNT)
		Redim REBATE_VALUES(MONTHCOUNT)
		Redim VOID_RENT_VALUES(MONTHCOUNT)	
		Redim PAYMENT_VALUES(MONTHCOUNT)
		Redim KPI_VALUES(MONTHCOUNT)

	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	'===========================================================================================	
		Dim REQ_ASSETTYPE,REQ_PATCH
		REQ_ASSETTYPE = -1
		REQ_PATCH="18,25,26,27,28"
		
		If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Request("ASSETTYPE") End If
		If Request("PATCH") <> "" Then 	REQ_PATCH = Request("PATCH") End If
		
		selValue = Request("START")
		TID = Request("TOPICID")
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	

		if TID = "" Then TID = -1 End if
		
		total = 0
		ytd_let = 0
		ytd_available = 0
		ytd_unavailable = 0
		
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
				KPI_VALUES(x) = 0
		next
	
		' GET MONTHS PASSED FOR KPI CALCULATION
		If month(date) < 4 Then months_passed = month(date) + 9 Else months_passes = month(date) - 3 End If

		get_fiscal_boundary()
		Dmonth = -1
		DYear = -1
	
	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	


	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	
  	
		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If
	
	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	

		Call get_kpi_header(TID)
		Call get_opening_balance()
		Call build_fiscal_month_select(lstBox, Dmonth)
		Call get_monthly_property_count(2, LET_VALUES)
		Call get_monthly_property_count(1, AVAILABLE_VALUES)
		Call get_monthly_property_count(4, UNAVAILABLE_VALUES)
		Call get_rent_due(RENTDUE_VALUES ,0) 
		Call get_initial_rent(INITIALRENT_VALUES ,0) 
		Call get_rebate(REBATE_VALUES ,0)
		Call get_void_rent(VOID_RENT_VALUES)
		Call get_payments(PAYMENT_VALUES ,0)
		
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	
		
		for x = 0 to MONTHCOUNT 
				ytd_let = ytd_let + LET_VALUES(x)
				ytd_available = ytd_available + AVAILABLE_VALUES(x)
				ytd_unavailable = ytd_unavailable + UNAVAILABLE_VALUES(x)
				
				ytd_rentdue = ytd_rentdue + RENTDUE_VALUES(x)
				ytd_initialrent = ytd_initialrent + INITIALRENT_VALUES(x)
				ytd_rebate = ytd_rebate + Abs(REBATE_VALUES(x))
				ytd_voidrent = ytd_voidrent + VOID_RENT_VALUES(x)
				ytd_payment = ytd_payment + aBS(PAYMENT_VALUES(x))
				'rw "Month: " & X & " : M -" & testvar & " : P-" & ytd_unavailable & " <br>"
				TempD = RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - Abs(REBATE_VALUES(x))
				TempE = VOID_RENT_VALUES(x) - INITIALRENT_VALUES(x) + Abs(REBATE_VALUES(x))
				'If TempD = 0 Or TempE = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = aBS(PAYMENT_VALUES(x))/(TempD + TempE + opening_balance) * 100 End If
				If TempD = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = aBS(PAYMENT_VALUES(x))/(TempD) * 100 End If
		next
		
		'rw ytd_rentdue & "<BR>" & ytd_initialrent & "<BR>" & ytd_rebate
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
			
			' This is the values for the Period column
			Prop_let 				= ytd_let/divisor
			Available_tolet		 	= ytd_available/divisor
			Unavailable_tolet 		= ytd_unavailable/divisor
			Due 					= (ytd_rentdue + ytd_initialrent) - ytd_rebate
			VoidRent 				= ytd_voidrent - ytd_initialrent + ytd_rebate
			Collected_from_tenants 	= ytd_payment
			Amt_Before_April 		= opening_balance
			
			If Due = 0 Then KPI = 0 Else KPI = Collected_from_tenants/(Due + Amt_Before_April) * 100 End If
			' This is the values for the Month column
			mnth_Prop_let 				= "N/A"
			mnth_Available_tolet 		= "N/A"
			mnth_Unavailable_tolet 		= "N/A"
			mnth_Due 					= "N/A"
			mnth_VoidRent 				= "N/A"
			mnth_Collected_from_tenants = "N/A"
			mnth_Amt_Before_April		= "N/A"

		Else 
		
			'Next Calculate the Average up to the YTD figure as follows:
			CurrentMonth = 4 ' April
			MonthMatch = False
			Counter = 0
			
			Prop_let 				=  0
			Available_tolet 		=  0 
			Unavailable_tolet 		=  0 
			Due 					=  0
			VoidRent 				=  0
			Collected_from_tenants 	=  0
			Amt_Before_April		=  0

			Do while MonthMatch = False  
			
				Prop_let 				= Prop_let + LET_VALUES(CurrentMonth)
				'ytd_let/divisor
				Available_tolet		 	= Available_tolet + AVAILABLE_VALUES(CurrentMonth)
				Unavailable_tolet 		= Unavailable_tolet + UNAVAILABLE_VALUES(CurrentMonth)
				prd_rentdue				= prd_rentdue + RENTDUE_VALUES(CurrentMonth)
				prd_initialrent			= prd_initialrent + INITIALRENT_VALUES(CurrentMonth)
				prd_rebate 				= prd_rebate + Abs(REBATE_VALUES(CurrentMonth))
				prd_voidrent 			= prd_voidrent + VOID_RENT_VALUES(CurrentMonth)
				prd_payment 			= prd_payment + aBS(PAYMENT_VALUES(CurrentMonth))

				Collected_from_tenants 	= Collected_from_tenants + aBS(PAYMENT_VALUES(CurrentMonth))
				Amt_Before_April 		= 0
				
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop

			
				Prop_let 				= Prop_let / Counter
				Available_tolet		 	= Available_tolet / Counter
				Unavailable_tolet 		= Unavailable_tolet / Counter
				Due 					= (prd_rentdue + prd_initialrent) - prd_rebate
				VoidRent				= prd_voidrent - prd_initialrent + prd_rebate

				' This Is the values for the Period column
				If Due = 0 Then KPI = 0 Else KPI = Collected_from_tenants/(Due + Amt_Before_April) * 100 End If
				
				' This is the values for the Month column
				mnth_Prop_let 					= FormatNumber(LET_VALUES(DMonth)/divisor,2)
				mnth_Available_tolet 			= FormatNumber(AVAILABLE_VALUES(DMonth)/divisor,2)
				mnth_Unavailable_tolet 			= FormatNumber(UNAVAILABLE_VALUES(DMonth)/divisor,2)
				mnth_Due 						= (RENTDUE_VALUES(DMonth) + INITIALRENT_VALUES(DMonth)) - Abs(REBATE_VALUES(DMonth))
				mnth_VoidRent 					= FormatCurrency(VOID_RENT_VALUES(DMonth) - INITIALRENT_VALUES(DMonth) + Abs(REBATE_VALUES(DMonth)),2)
				mnth_Collected_from_tenants 	= aBS(PAYMENT_VALUES(DMonth))
				mnth_Amt_Before_April		 	= 0
				'If mnth_Due = 0 Then mnth_KPI = 0 Else mnth_KPI = mnth_Collected_from_tenants/(mnth_Due + mnth_Amt_Before_April) * 100 End If
				If mnth_Due = 0 Or mnth_VoidRent = o Then mnth_KPI = FormatNumber(0,2) Else			mnth_KPI = FormatNumber(mnth_Collected_from_tenants/(mnth_Due) * 100,2) End If
				
				' As we cant format the below because they had to be used for a calculation do them now
				mnth_Due						= FormatCurrency(mnth_Due,2)
				mnth_Collected_from_tenants		= FormatCurrency(mnth_Collected_from_tenants,2)
				mnth_Amt_Before_April			= FormatCurrency(0,2)
			
		End If
	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	

	'===========================================================================================
	'	Function List
	'===========================================================================================	

		' GET FIGURES FOR MONTHLY BREAKDOWN
		Function get_opening_balance()
		
			SQL = 	"SELECT ISNULL(SUM(J.AMOUNT),0) AS OPENINGBALANCE " &_
						" FROM F_RENTJOURNAL J " &_
						"	INNER JOIN C_TENANCY T ON T.TENANCYID = J.TENANCYID " &_
						"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
						"	INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID = P.DEVELOPMENTID " &_
						" WHERE J.ITEMTYPE = 9 AND P.ASSETTYPE IN (" & REQ_ASSETTYPE & ")" &_
					" AND PD.PATCHID IN (" & REQ_PATCH & ") AND J.TRANSACTIONDATE >= '" & FStart & "' AND J.TRANSACTIONDATE <= '" & FEnd & "' "
			'rw sql
			Call OpenRs(rsSet, SQL)
			IF not rsSet.EOF Then opening_balance = rsSet("OPENINGBALANCE") Else opening_balance = 0 End IF
				
		End Function
	
	'===========================================================================================
	'	End Function LIst
	'===========================================================================================	
	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI05.asp?ASSETTYPE=<%=REQ_ASSETTYPE%>&PATCH=<%=REQ_PATCH%>&START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&LEGEND=P&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="363" CELLPADDING=1 CELLSPACING=2>
				<TR><TD width="168" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="88" align="right"><strong>Month</strong></td>
			    <td width="91" align="right"><strong>Period</strong></td>
				</TR>
				<TR><TD COLSPAN=3><B>Average Properties</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(A) Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Prop_let%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(Prop_let,2)%></TD></TR>
				<TR><TD>(B) Available to let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Available_tolet%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(Available_tolet,2)%></TD></TR>
				<TR><TD>(C) Unvailable to let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Unavailable_tolet%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(Unavailable_tolet,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD COLSPAN=3><B>Rent</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(D) Due</TD>
				  <TD ALIGN=RIGHT><%=mnth_Due%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(Due,2)%></TD></TR>
				<TR><TD>(E) Void Rent</TD>
				  <TD ALIGN=RIGHT><%=mnth_VoidRent%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(VoidRent,2)%></TD></TR>
				<TR><TD>(F) Collected From Tenants</TD>
				  <TD ALIGN=RIGHT><%=mnth_Collected_from_tenants%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(Collected_from_tenants,2)%></TD></TR>
				<TR><TD>(G) Amount 'b/f' 1st April</TD>
				  <TD ALIGN=RIGHT><%=mnth_Amt_Before_April%></TD>
			    <TD ALIGN=RIGHT><%=FORMATCURRENCY(Amt_Before_April,2)%></TD></TR>
				<TR STYLE='HEIGHT:10PX'><TD COLSPAN=3></TD></TR>
				<TR><TD><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=mnth_KPI%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(KPI,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:10PX'><TD COLSPAN=3></TD></TR>
				<% if needtoshow = true then %>
				<TR><TD><SPAN STYLE='VISIBILITY:HIDDEN'>D = Total Income from A + B + C</SPAN></TD>
				  <TD></TD>
			    <TD></TD></TR>
				<TR><TD><SPAN STYLE='VISIBILITY:HIDDEN'>E = Total Income from B + C</SPAN></TD>
				  <TD></TD>
			    <TD></TD></TR>
				<TR STYLE='HEIGHT:10PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD>calculation: F/(D + G) x 100</TD>
				  <TD></TD>
			    <TD></TD></TR>
				<% end If %>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
