<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
    Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function

	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, KPI, KPIKPI, selValue, divisor
	Dim A, B, C, UN, AA, BB, CC, UUNN
	Dim av_cnt, let_cnt, un_let
	divisor = 1
	Redim LET_VALUES(MONTHCOUNT)
	Redim AVAILABLE_VALUES(MONTHCOUNT)
	Redim UNAVAILABLE_VALUES(MONTHCOUNT)
	Redim KPI_VALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	ytd_let = 0
	ytd_due = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) 			= 0
			UNAVAILABLE_VALUES(x) 	= 0
			AVAILABLE_VALUES(x) 	= 0
			LET_VALUES(x) 			= 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	'rESPONSE.wRITE fsTART
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'STARTDATE = Request("START")
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
		ytd = 1		
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
	End If
		
	If (Date > FEnd) then
		TheDate = CDate(FEnd)
		TheMonth = Month(TheDate)
		TheYear = Year(TheDate)
		TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
		TheEndDate = LastDayofMonth(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
	else
		TheDate = CDate(Date)
		TheMonth = Month(TheDate)
		TheYear = Year(TheDate)		
		TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
		TheEndDate = Day(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
	End If
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_ytd_count_av()
	Call get_ytd_count_let()
	Call get_ytd_count_un()
	Call get_month_start_av(AVAILABLE_VALUES)
	Call get_month_start_let(LET_VALUES)
	Call get_month_start_un(UNAVAILABLE_VALUES)

	If YTD = 1 Then		' YEAR TO DATE

		B = let_cnt
		C = av_cnt
		UN = un_let
		A = B + C + UN
				
		A = FormatNumber(A,2)
		B = FormatNumber(B) 
		C = FormatNumber(C,2)
		IF A = 0 THEN 
			KPI = "N/A" 
		ELSE
			KPI = C / A * 100
			KPI = FormatNumber(KPI,2)
		END IF
		AA = "N/A"
		BB = "N/A"
		CC = "N/A"
		KPIKPI = "N/A"

	Else 

		B = let_cnt
		C = av_cnt
		u = un_let
		A = B + C + UN
		
		KPI = C / A * 100
		A = FormatNumber(A,2)
		B = FormatNumber(B) 
		C = FormatNumber(C,2)
		
		KPI = FormatNumber(KPI,2)

		BB = LET_VALUES(DMonth)
		UUNN = AVAILABLE_VALUES(DMonth)		
		CC = UNAVAILABLE_VALUES(DMonth)
		AA = BB + CC + UUNN
		IF AA = 0 THEN 
			KPIKPI = "N/A" 
		ELSE
			KPIKPI = CC / AA * 100
			KPIKPI = FormatNumber(KPIKPI,2)
		END IF
		
		AA = FormatNumber(AA,2)
		BB = FormatNumber(BB,2)
		CC = FormatNumber(CC,2)
		
	End If

	' BUILD KPI ARRAY FOR GRAPH
	for X = 0 to MONTHCOUNT 
	
		G_B = LET_VALUES(X)
		G_C = AVAILABLE_VALUES(X)		
		G_UN = UNAVAILABLE_VALUES(X)
		G_A = G_B + G_C + G_UN

		IF G_A = 0 THEN 
			KPI_VALUES(X) = 0 
		ELSE
			KPI_VALUES(X) = G_C / G_A * 100
		END IF
	next
	if (Date > FEnd) then
		TheDate = CDate(FEnd)
		TheMonth = Month(TheDate)
		TheYear = Year(TheDate)
		TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
		TheEndDate = LastDayofMonth(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
	else
		TheDate = CDate(Date)
		TheMonth = Month(TheDate)
		TheYear = Year(TheDate)		
		TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
		TheEndDate = Day(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
	end if

	CloseDB()
	
	Function get_ytd_count_av()
	
		' MONTHLY RENTS
		SQL = "SELECT SUM(ISNULL(CNT,0)) AS CNT, SUM(ISNULL(RENT,0)) AS RENT FROM ( " &_
			 "SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_VOIDS M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"	AND ASSETTYPE = 1 AND M.STATUS = 1  " &_
			"UNION ALL " &_
			"SELECT 	-COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"		INNER JOIN F_RENTJOURNAL_VOIDS V ON V.PROPERTYID = M.PROPERTYID AND V.STATUS = 4 " &_
				"			AND V.TRANSACTIONDATE >= '" & TheStartDate & "' AND V.TRANSACTIONDATE <= '" & TheEndDate & "' " &_
				"WHERE	T.STARTDATE >= '" & TheStartDate & "' AND T.STARTDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.ITEMTYPE = 8 AND M.PAYMENTTYPE IS NULL AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	 COUNT(*) AS CNT, -(ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0)) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	T.ENDDATE >= '" & TheStartDate & "' AND T.ENDDATE <= '" & TheEndDate & "' AND " &_ 
				"		ASSETTYPE = 1 AND M.PAYMENTTYPE = 12 AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	-COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"		INNER JOIN F_RENTJOURNAL_VOIDS V ON V.PROPERTYID = M.PROPERTYID AND V.STATUS = 4 " &_
				"			AND V.TRANSACTIONDATE >= '" & TheStartDate & "' AND V.TRANSACTIONDATE <= '" & TheEndDate & "' " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.PAYMENTTYPE = 28 AND T.TENANCYTYPE IN (1,10)   ) MEGATABLE "

		'rw SQL		
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF then
			av_cnt = rsSet("CNT")
		end if
		CloseRs(rsSet)
		
	End Function

	Function get_month_start_av(ByRef AV_PROPS)
	
		Dim rents_cnt, initials_cnt, rebates_cnt, cancellations_cnt
		Dim rents, initials, rebates, cancellations
		
		' MONTHLY RENTS
		SQL = "SELECT 	MONTH(M.TRANSACTIONDATE) AS MNTH, COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_VOIDS M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		LEFT JOIN F_RENTJOURNAL R ON R.JOURNALID = M.JOURNALID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & FStart & "' AND M.TRANSACTIONDATE <= '" & FEnd & "'" &_
				"		AND ASSETTYPE = 1 AND M.STATUS = 1 " &_
				"GROUP BY MONTH(M.TRANSACTIONDATE)"
		'rw sql
		Call OpenRs(rsSet, SQL)
		while not rsSet.EOF 
'			Response.Write "m,onth " & rsSet("MNTH") & "  count " & rsSet("CNT") & "<BR>"
			AV_PROPS(rsSet("MNTH")) = rsSet("CNT")
			rsSet.Movenext()
		Wend		
		CloseRs(rsSet)
		
	End Function
	
	Function get_ytd_count_let()
	
		' MONTHLY RENTS
		SQL = "SELECT SUM(ISNULL(CNT,0)) AS CNT, SUM(ISNULL(RENT,0)) AS RENT FROM ( " &_
			 "SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_MONTHLY M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"	AND ASSETTYPE = 1 AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	T.STARTDATE >= '" & TheStartDate & "' AND T.STARTDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.ITEMTYPE = 8 AND M.PAYMENTTYPE IS NULL AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	 -COUNT(*) AS CNT, -(ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0)) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	T.ENDDATE >= '" & TheStartDate & "' AND T.ENDDATE <= '" & TheEndDate & "' AND " &_ 
				"		ASSETTYPE = 1 AND M.PAYMENTTYPE = 12 AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.PAYMENTTYPE = 28 AND T.TENANCYTYPE IN (1,10)   ) MEGATABLE "

		'rw SQL		
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF then
			let_cnt = rsSet("CNT")
		end if
		CloseRs(rsSet)
		
	End Function

	Function get_month_start_let(ByRef LET_PROPS)
	
		' MONTHLY RENTS
		SQL = "SELECT 	MONTH(M.TRANSACTIONDATE) AS MNTH, COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_MONTHLY M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		LEFT JOIN F_RENTJOURNAL R ON R.JOURNALID = M.JOURNALID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & FStart & "' AND M.TRANSACTIONDATE <= '" & FEnd & "'" &_
				"	AND ASSETTYPE = 1 AND T.TENANCYTYPE IN (1,10) " &_
				"GROUP BY MONTH(M.TRANSACTIONDATE)"
		'rw sql
		Call OpenRs(rsSet, SQL)
		while not rsSet.EOF 
'			Response.Write "m,onth " & rsSet("MNTH") & "  count " & rsSet("CNT") & "<BR>"
			LET_PROPS(rsSet("MNTH")) = rsSet("CNT")
			rsSet.Movenext()
		Wend		
		CloseRs(rsSet)
		
	End Function
	
	Function get_month_start_un(ByRef UN_PROPS)
	
		' MONTHLY RENTS
		SQL = "SELECT 	MONTH(M.TRANSACTIONDATE) AS MNTH, COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_VOIDS M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		LEFT JOIN F_RENTJOURNAL R ON R.JOURNALID = M.JOURNALID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & FStart & "' AND M.TRANSACTIONDATE <= '" & FEnd & "'" &_
				"	AND ASSETTYPE = 1 AND M.STATUS = 4 " &_
				"GROUP BY MONTH(M.TRANSACTIONDATE)"
		'rw sql
		Call OpenRs(rsSet, SQL)
		while not rsSet.EOF 
			UN_PROPS(rsSet("MNTH")) = rsSet("CNT")
			rsSet.Movenext()
		Wend		
		CloseRs(rsSet)
		
	End Function
	
	Function get_ytd_count_un()
	
		' MONTHLY RENTS
		SQL = "SELECT SUM(ISNULL(CNT,0)) AS CNT, SUM(ISNULL(RENT,0)) AS RENT FROM ( " &_
			 "SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_VOIDS M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"	AND ASSETTYPE = 1 AND M.STATUS = 4  " &_
			"UNION ALL " &_
			"SELECT 	-COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"		INNER JOIN F_RENTJOURNAL_VOIDS V ON V.PROPERTYID = M.PROPERTYID AND V.STATUS = 4 " &_
				"			AND V.TRANSACTIONDATE >= '" & TheStartDate & "' AND V.TRANSACTIONDATE <= '" & TheEndDate & "' " &_
				"WHERE	T.STARTDATE >= '" & TheStartDate & "' AND T.STARTDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.ITEMTYPE = 8 AND M.PAYMENTTYPE IS NULL AND T.TENANCYTYPE IN (1,10)  " &_
				"		   ) MEGATABLE "
		'rw sql
		Call OpenRs(rsSet, SQL)
		while not rsSet.EOF 
			un_let = rsSet("CNT")
			rsSet.Movenext()
		Wend		
		CloseRs(rsSet)
		
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="../../../css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="../../../js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../../../js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../../../js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../../../js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI09.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?LEGEND=�&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>&MINSCALE=60&MAXSCALE=61">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			
      <TABLE CELLPADDING=1 CELLSPACING=2>
        <TR STYLE='HEIGHT:15PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD STYLE='WIDTH:30PX' height="23"></TD>
          <TD height="23">Month&nbsp;&nbsp;&nbsp; 
            <SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>">
              <%=lstBox%>
            </SELECT>
          </TD>
          <TD height="23" align=center><b>Period</b></TD>
		  <TD>&nbsp;</TD>		  
          <TD height="23" align=center><b>YTD</b></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(A) Total No</TD>
          <TD ALIGN=RIGHT><%=AA%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=A%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(B) Let</TD>
          <TD ALIGN=RIGHT><%=BB%></TD>
		  <TD></TD>
          <TD ALIGN=RIGHT><%=B%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(C) Unavailable To Let</TD>
          <TD ALIGN=RIGHT><%=CC%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=C%></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD ALIGN=RIGHT><b>KPI = </b></TD>
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpikpi%></b></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpi%></b></TD>
        </TR>
        <TR STYLE='HEIGHT:85PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR STYLE='VISIBILITY:HIDDEN'>
          <TD></TD>
          <TD> calculation : C x (12/52)</TD>
          <TD></TD>
          <TD></TD>
        </TR>
      </TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>