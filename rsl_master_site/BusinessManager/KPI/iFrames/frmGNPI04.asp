<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->
<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================
	
		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue, ytd_props, asset_type
		Dim change_of_tenancy, day_to_day, services, decorating, gas, timber, total, assettype_sql
		
		' Set arrays all the same size
		Redim MONTHVALUES(MONTHCOUNT)
		Redim KPIVALUES(MONTHCOUNT)
		Redim LET_VALUES(MONTHCOUNT)
		Redim AVAILABLE_VALUES(MONTHCOUNT)
		Redim UNAVAILABLE_VALUES(MONTHCOUNT)
		
		Redim M_DAYTODAY(MONTHCOUNT)
		Redim M_CHANGEOFTENANCY(MONTHCOUNT)
		Redim M_DECORATING(MONTHCOUNT)
		Redim M_TIMBER(MONTHCOUNT)
		Redim M_GAS(MONTHCOUNT)
		Redim M_SERVICES(MONTHCOUNT)
		Redim M_TOTAL(MONTHCOUNT)
		
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================
	
	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
		Dim REQ_ASSETTYPE,REQ_PATCH
		REQ_ASSETTYPE = -1
		REQ_PATCH="18,25,26,27,28"
		
		If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Request("ASSETTYPE") End If
		If Request("PATCH") <> "" Then 	REQ_PATCH = Request("PATCH") End If
	
		selValue = Request("START")
		TID = Request("TOPICID")
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	
	OpenDB()
	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	
	
		' Set top id if empty
		if TID = "" Then TID = -1 End if
		' clear the total
		total = 0
		
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
				MONTHVALUES(x) = 0
				LET_VALUES(x) = 0
				AVAILABLE_VALUES(x) = 0
				UNAVAILABLE_VALUES(x) = 0
		next
		
		' GET MONTHS PASSED FOR KPI CALCULATION
		If month(date) < 4 Then 
			months_passed = month(date) + 9 
		Else 
			months_passed = month(date) - 3 
		End If
		
		get_fiscal_boundary()
		
		Dmonth = -1
		DYear = -1
		
	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
		
	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	

		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If
	
	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	

		Call get_kpi_header(TID)
		' 2 below are functions on this page
		Call get_monthly_breakdown()
		Call get_ytd_repair_cost()
		Call get_monthly_property_count(2, LET_VALUES)
		Call get_monthly_property_count(1, AVAILABLE_VALUES)
		Call get_monthly_property_count(4, UNAVAILABLE_VALUES)
		Call build_fiscal_month_select(lstBox, Dmonth)
	
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	

	' Standard Calculation : calculation: (YTD/No of Months) x 12/52
	
		for x = 0 to MONTHCOUNT 
			props = LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x)
			If (LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x)) = 0 Then
				KPIVALUES(x) = 0 
			Else
				KPIVALUES(x) = MONTHVALUES(x) / (LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x))
				'RW X & ".." & (LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x)) & "<br>"
			End If
		next

		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd)  Then
			If props <> 0 Then 
				props = (props / months_passed)
				kpi = (total/months_passed) * (12/52) / props
			Else
				props = 0
				kpi = 0
			End If
			prd_kpi					=  "N/A"  
			prd_day_to_day 			=  "N/A" 
			prd_change_of_tenancy 	=  "N/A" 
			prd_decorating 			=  "N/A" 
			prd_timber 				=  "N/A"  
			prd_gas 				=  "N/A"  
			prd_services			=  "N/A"  
			prd_total				=  "N/A"  
		Else 
			'Next Calculate the Average up to the YTD figure as follows:
			CurrentMonth = 4 ' April
			MonthMatch = False
			Counter = 0
			
			props 				=  0
			day_to_day 			=  0 
			change_of_tenancy 	=  0 
			decorating 			=  0
			timber 				=  0
			gas 				=  0
			services			=  0
			total				=  0
			
			Do while MonthMatch = False  
			
				props				=  props + LET_VALUES(CurrentMonth) + AVAILABLE_VALUES(CurrentMonth) + UNAVAILABLE_VALUES(CurrentMonth)
				day_to_day 			=  day_to_day + M_DAYTODAY(CurrentMonth) 
				change_of_tenancy 	=  change_of_tenancy + M_CHANGEOFTENANCY(CurrentMonth) 
				decorating 			=  decorating + M_DECORATING(CurrentMonth)
				timber 				=  timber + M_TIMBER(CurrentMonth) 
				gas 				=  gas + M_GAS(CurrentMonth) 
				services			=  services + M_SERVICES(CurrentMonth)
				total				=  total + M_TOTAL(CurrentMonth)
				
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
			Loop
					if props <> 0 Then
						props = (props / Counter)
						kpi = (total/Counter) * (12/52) / props
					Else
						props = 0
						kpi = 0
					End If 					
					prd_props 				=  (LET_VALUES(Dmonth) + AVAILABLE_VALUES(Dmonth) + UNAVAILABLE_VALUES(Dmonth)) 
					prd_day_to_day 			=  FormatCurrency(M_DAYTODAY(Dmonth) ,2)
					prd_change_of_tenancy 	=  FormatCurrency(M_CHANGEOFTENANCY(Dmonth) ,2) 
					prd_decorating 			=  FormatCurrency(M_DECORATING(Dmonth)  ,2)
					prd_timber 				=  FormatCurrency(M_TIMBER(Dmonth)  ,2)
					prd_gas 				=  FormatCurrency(M_GAS(Dmonth)  ,2)
					prd_services			=  FormatCurrency(M_SERVICES(Dmonth)  ,2)
					prd_total				=  FormatCurrency(M_TOTAL(Dmonth))
					
					if prd_props =  0 Then	
						prd_kpi = 0
					Else
						prd_kpi = ( (prd_total/(52/12)) / prd_props )
						prd_kpi = FormatCurrency(prd_kpi,2)
					End If
					
		End If
	
	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	
	
	'===========================================================================================
	'	Function List
	'===========================================================================================	

		' GET FIGURES FOR MONTHLY BREAKDOWN
		Function get_monthly_breakdown()
		
			SQL = 	"SELECT	MONTH(TAXDATE) AS MONTH, SUM(I.GROSSCOST) AS GROSS, " &_
					"		SUM(I.GROSSCOST) / (52/12) AS WEEKLYEXP " &_
					"FROM	F_PURCHASEITEM I " &_
					"		INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
					"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
					"		INNER JOIN P_WOTOREPAIR W ON I.ORDERITEMID = W.ORDERITEMID " &_
					"		INNER JOIN C_JOURNAL J ON J.JOURNALID = W.JOURNALID " &_
					"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
					"	    INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID = P.DEVELOPMENTID " &_
					"WHERE	I.PISTATUS > 9 AND I.EXPENDITUREID IN (24,25,26,27,28,29) AND P.ASSETTYPE IN (" & REQ_ASSETTYPE & ")" &_
					"		AND PD.PATCHID IN (" & REQ_PATCH & ") AND INV.TAXDATE >= '" & FStart & "' AND INV.TAXDATE <= '" & FEnd & "' " &_
					"GROUP	BY MONTH(TAXDATE) " &_
					"ORDER 	BY MONTH(TAXDATE) "
			'rw SQL & "<BR><BR>"
			Call OpenRs(rsSet, SQL)
			While not rsSet.EOF 
				MONTHVALUES(rsSet("MONTH")) = rsSet("WEEKLYEXP")
				rsSet.Movenext()
			Wend
		
		End Function
			
		' GET THE YEAR TO DATE REPAIR COSTS
		Function get_ytd_repair_cost()
		
			SQL = 	"SELECT	MONTH(TAXDATE) AS MONTH, I.EXPENDITUREID, SUM(I.GROSSCOST) AS GROSS " &_
					"FROM	F_PURCHASEITEM I " &_
					"		INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
					"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
					"		INNER JOIN P_WOTOREPAIR W ON I.ORDERITEMID = W.ORDERITEMID "&_
					"		INNER JOIN C_JOURNAL J ON J.JOURNALID = W.JOURNALID "&_
					"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID "&_
					"	    INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID = P.DEVELOPMENTID " &_
					"WHERE	I.PISTATUS > 9 AND I.ACTIVE = 1 AND I.EXPENDITUREID IN (24,25,26,27,28,29) AND P.ASSETTYPE IN (" & REQ_ASSETTYPE & ")" &_
					"		AND PD.PATCHID IN (" & REQ_PATCH & ") AND INV.TAXDATE >= '" & FStart & "' AND INV.TAXDATE <= '" & FEnd & "' " &_
					"GROUP	BY I.EXPENDITUREID, MONTH(TAXDATE) " &_
					"ORDER	BY I.EXPENDITUREID "
			'rw SQL
			Call OpenRs(rsSet, SQL)

			While not rsSet.EOF 
				If Cint(rsSet("EXPENDITUREID")) = 24 Then 
					day_to_day = day_to_day + rsSet("GROSS")
					'rw  day_to_day
					M_DAYTODAY( rsSet("MONTH")) = rsSet("GROSS")
				ElseIf Cint(rsSet("EXPENDITUREID")) = 25 Then 
					change_of_tenancy = change_of_tenancy + rsSet("GROSS")
					'rw change_of_tenancy
					M_CHANGEOFTENANCY(rsSet("MONTH")) = rsSet("GROSS")
				ElseIf Cint(rsSet("EXPENDITUREID")) = 26 Then 
					decorating = decorating + rsSet("GROSS")
					'rw decorating
					M_DECORATING(rsSet("MONTH")) = rsSet("GROSS")
				ElseIf Cint(rsSet("EXPENDITUREID")) = 27 Then 
					timber = timber + rsSet("GROSS")
					'RW timber
					M_TIMBER(rsSet("MONTH")) = rsSet("GROSS")
				ElseIf Cint(rsSet("EXPENDITUREID")) = 28 Then 
					gas = gas + rsSet("GROSS")
					M_GAS(rsSet("MONTH")) = rsSet("GROSS")
				ElseIf Cint(rsSet("EXPENDITUREID")) = 29 Then 
					services = services + rsSet("GROSS")
					M_SERVICES(rsSet("MONTH")) = rsSet("GROSS")
				End If
				total = total + rsSet("GROSS")
				M_TOTAL(rsSet("MONTH")) = M_TOTAL(rsSet("MONTH")) + rsSet("GROSS")
				rsSet.Movenext()
			Wend
		
		End Function
	
	'===========================================================================================
	'	End Function LIst
	'===========================================================================================	

	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style2 {font-size: .5}
-->
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI04.asp?ASSETTYPE=<%=REQ_ASSETTYPE%>&PATCH=<%=REQ_PATCH%>&START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&LEGEND=�&JAN=<%=KPIVALUES(1)%>&FEB=<%=KPIVALUES(2)%>&MAR=<%=KPIVALUES(3)%>&APR=<%=KPIVALUES(4)%>&MAY=<%=KPIVALUES(5)%>&JUN=<%=KPIVALUES(6)%>&JUL=<%=KPIVALUES(7)%>&AUG=<%=KPIVALUES(8)%>&SEP=<%=KPIVALUES(9)%>&OCT=<%=KPIVALUES(10)%>&NOV=<%=KPIVALUES(11)%>&DEC=<%=KPIVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="327" CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD width="15"></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD width="152" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="92" align="right"><strong>Period</strong></td>
			    <td width="48" align="right"><strong>Year</strong></td>
				</TR>
				<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
				<TR><TD></TD><TD><B>Average Properties</B></TD>
				  <TD ALIGN=RIGHT><%=FormatNumber(prd_props,2)%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(props,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD></TD></TR>
				<TR><TD></TD><TD COLSPAN=3><B>Reactive Repairs</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=4></TD></TR>
				<TR><TD></TD><TD>Change Of Tenancy</TD>
				  <TD ALIGN=RIGHT><%=prd_change_of_tenancy%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(change_of_tenancy,2)%></TD></TR>
				<TR><TD></TD><TD>Day to Day Repairs</TD>
				  
          <TD ALIGN=RIGHT><%=prd_day_to_day%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(day_to_day,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=4></TD></TR>
				<TR><TD></TD><TD COLSPAN=3><B>Cyclical Repairs</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=4></TD></TR>
				<TR><TD></TD><TD>Services</TD>
				  <TD ALIGN=RIGHT><%=prd_services%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(services,2)%></TD></TR>
				<TR><TD></TD><TD>Decorating</TD>
				  <TD ALIGN=RIGHT><%=prd_decorating%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(decorating,2)%></TD></TR>
				<TR><TD></TD><TD>Gas Services</TD>
				  <TD ALIGN=RIGHT><%=prd_gas%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(gas,2)%></TD></TR>
				<TR><TD></TD><TD>Timber Repairs</TD>
				  <TD ALIGN=RIGHT><%=prd_timber%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(timber,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=4></TD></TR>
				<TR><TD></TD><TD></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><%=prd_total%></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><%=FormatCurrency(total,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=4></TD></TR>
				<TR><TD></TD><TD ALIGN=RIGHT><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=prd_kpi%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatCurrency(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=4></TD></TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe  src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
