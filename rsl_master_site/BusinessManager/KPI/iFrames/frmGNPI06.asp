<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================

		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, KPI, selValue, divisor, opening_balance
		Dim A, B, C
		Dim ytd_rentdue, ytd_initialrent, ytd_rebate, ytd_payment, ytd_arrears
		Redim RENTDUE_VALUES(MONTHCOUNT)
		Redim INITIALRENT_VALUES(MONTHCOUNT)
		Redim REBATE_VALUES(MONTHCOUNT)
		Redim PAYMENT_VALUES(MONTHCOUNT)
		Redim KPI_VALUES(MONTHCOUNT)
		Redim ARREARS_VALUES(MONTHCOUNT)


	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	Dim REQ_ASSETTYPE,REQ_PATCH
	REQ_ASSETTYPE = -1
	REQ_PATCH="18,25,26,27,28"
	
	If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Request("ASSETTYPE") End If
	If Request("PATCH") <> "" Then 	REQ_PATCH = Request("PATCH") End If

	selValue = Request("START")
	TID = Request("TOPICID")
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	
	
		if TID = "" Then TID = -1 End if
	
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) = 0
		next
	

		get_fiscal_boundary()
		Dmonth = -1
		DYear = -1
	
	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	


	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	


		' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If

	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	

	
		Call get_kpi_header(TID)
		Call build_fiscal_month_select(lstBox, Dmonth)
		Call get_rent_due(RENTDUE_VALUES, 1) 
		Call get_rebate(REBATE_VALUES ,1) 
		Call get_initial_rent(INITIALRENT_VALUES, 1) 
		Call get_payments(PAYMENT_VALUES, 1)
	
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	
	
		for x = 0 to MONTHCOUNT 
				ytd_rentdue = ytd_rentdue + RENTDUE_VALUES(x)
				ytd_initialrent = ytd_initialrent + INITIALRENT_VALUES(x)
				ytd_rebate = ytd_rebate + Abs(REBATE_VALUES(x))
				ytd_payment = ytd_payment + aBS(PAYMENT_VALUES(x))
				ARREARS_VALUES(x) = (RENTDUE_VALUES(X) + INITIALRENT_VALUES(X) - Abs(REBATE_VALUES(x) ) - aBS(PAYMENT_VALUES(x)))
				ytd_arrears = ytd_arrears + ARREARS_VALUES(x)
				'If ytd_arrears = 0 Then 
				If ( RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - Abs(REBATE_VALUES(x) ) = 0) Then 
					KPI_VALUES(x) = 0 
				Else 
					KPI_VALUES(x) = ARREARS_VALUES(x) / ( ( RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - REBATE_VALUES(x) ) )  * 100 
				End If
		next

		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		' Important Part of the code :  
		'
		' Below determines what values to use, if a month is selected in the drop down
		' then the second part is used, but if YTD is chosen then the first part is used.
		'
		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
			
			prd_RentDue = (ytd_rentdue + ytd_initialrent - ytd_rebate)
			prd_RentCollected = ytd_payment
			prd_ArrearsAmount = ytd_arrears
			
			' Set KPI for the period
			If prd_ArrearsAmount = 0 or prd_RentDue = 0 Then 
				KPI = 0 
			Else 
				KPI = (prd_ArrearsAmount / prd_RentDue) * 100 
			End If
			
			' As we dont need the month vars, set them to N/A
			mnth_RentDue = "N/A"
			mnth_RentCollected = "N/A"
			mnth_ArrearsAmount = "N/A"
			
		Else 
		
			CurrentMonth = 4 ' April
			MonthMatch = False
			
			' Initialise the varibales for the loop
			Counter = 0
			prd_RentDue = 0
			prd_RentCollected = 0
			prd_ArrearsAmount = 0
			
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			' Important Loop :  
			'
			' This loop adds up the monthly figures for each variable and collates them to show the period figure.
			' The loop stops when the month chosen is equal to the var number 'CurrentMonth'.
			'
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			
			Do while MonthMatch = False  
			
				' The following three build monthly figures to work out the period calculation done outside the loop
				prd_RentDue = prd_RentDue + ( RENTDUE_VALUES(CurrentMonth) + INITIALRENT_VALUES(CurrentMonth) - Abs(REBATE_VALUES(CurrentMonth)))
				prd_RentCollected = prd_RentCollected + aBS(PAYMENT_VALUES(CurrentMonth))
				prd_ArrearsAmount = prd_ArrearsAmount + ARREARS_VALUES(CurrentMonth)
				
				'check if current month number in the loop is equal to the month chosen
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop
			
			' Set the KPI for the period
			If prd_ArrearsAmount = 0 Then KPI = 0 Else KPI = (prd_ArrearsAmount / prd_RentDue) * 100 End If
			
			' set the monthly values
			mnth_RentDue = (RENTDUE_VALUES(DMonth) + INITIALRENT_VALUES(DMonth) - - REBATE_VALUES(DMonth))
			mnth_RentCollected = aBS(PAYMENT_VALUES(DMonth))
			mnth_ArrearsAmount = ARREARS_VALUES(DMonth)
			
			' Set the KPI for the Month
			If mnth_ArrearsAmount = 0 or mnth_RentDue = 0 Then
			 	mnth_KPI = 0 
			Else 
				mnth_KPI = (mnth_ArrearsAmount / mnth_RentDue) * 100 
			End If
			
			' Format the variables after the kpi calculation is done
			mnth_KPI 			= FormatNumber(mnth_KPI,2)
			mnth_RentDue 		= FormatCurrency(mnth_RentDue,2)
			mnth_RentCollected	= FormatCurrency(mnth_RentCollected,2)
			mnth_ArrearsAmount 	= FormatCurrency(mnth_ArrearsAmount,2)
			
		End If

	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	

	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI06.asp?ASSETTYPE=<%=REQ_ASSETTYPE%>&PATCH=<%=REQ_PATCH%>&START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="357" CELLPADDING=1 CELLSPACING=2>
				<TR><TD width="121" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="120" align="right"><strong>Month</strong></td>
			    <td width="100" align="right"><strong>Period</strong></td>
				</TR>
				<TR><TD>(A) Rent Due</TD>
				  <TD ALIGN=RIGHT><%=mnth_RentDue%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(prd_RentDue,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(B) Rent Collected</TD>
				  <TD ALIGN=RIGHT><%=mnth_RentCollected%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(prd_RentCollected,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(C) Arrears</TD>
				  <TD ALIGN=RIGHT><%=mnth_ArrearsAmount%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(prd_ArrearsAmount,2)%></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD ALIGN=RIGHT><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=mnth_kpi%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:85PX'><TD COLSPAN=3></TD></TR>
				<% if showthi = true then %>
				<TR STYLE='VISIBILITY:HIDDEN'><TD> calculation : (C / A) x 100</TD>
				  <TD></TD>
			    <TD></TD></TR>
				<% end If %>
		  </TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
