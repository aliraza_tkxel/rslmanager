<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->
<%	
	Dim priorty, monthno, monthsql, cnt, total, prioritysql, timesql
	Dim MONTHNAMES
	Dim REQ_ASSETTYPE

	REQ_ASSETTYPE = -1
	If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Request("ASSETTYPE") End If

	priority = Request("PRIORITY")
	If Cint(priority) = 2 Then
		prioritysql = "	AND PRI.PRIORITYID IN (2)  "
		timesql		= "	AND (DATEDIFF(D, FL_LOG.SUBMITDATE, RP.LASTACTIONDATE)) > 7 "
		
	ElseIf Cint(priority) = 1 Then
		prioritysql = "	AND PRI.PRIORITYID IN (1)  "
		timesql		= "	AND (DATEDIFF(D, FL_LOG.SUBMITDATE, RP.LASTACTIONDATE)) > 28 "
	Else
		prioritysql = " AND	PRI.PRIORITYID IN (3,4)  "
		timesql		= "	AND (DATEDIFF(SS, FL_LOG.SUBMITDATE, RP.LASTACTIONDATE)) > 86400 "
	End If
		
	MONTHNAMES = aRRAY ("YTD","January","February","March","April","May","June","July","August","September","October","November","December")
	monthno = Request("MONTHNO")
	IF monthno = "-1" Then 
		monthno = 0
		monthsql = ""
	Else 
		monthsql = " AND MONTH(RP.LASTACTIONDATE) = " & monthno

	End If
	OpenDB()
	get_fiscal_boundary()
	cnt = 0
	total = 0
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<TITLE>Uncompleted Repairs</TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF style='background-image:none' TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" CLASS='TA'>
<BR><TABLE align=center WIDTH=95% CELLPADDING=1 CELLSPACING=2 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=steelblue>
	<THEAD>
		<TR><td COLSPAN=3><b><%=MONTHNAMES(monthno)%></b></td><TD colspan=2 align=center><B>Dates</B></TD><TD COLSPAN=2></TD></TR>
		<TR> 
			<TD><b>No</b></TD>
			<TD CLASS='TABLE_HEAD'><B>Supplier</B></TD>
			<TD CLASS='TABLE_HEAD'><B>Scheme</B></TD>
			<TD CLASS='TABLE_HEAD'>&nbsp;<B>Property</B></TD>
			<TD CLASS='TABLE_HEAD'>&nbsp;<B>Fault</B></TD>
		  	<TD STYLE='WIDTH:100px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Logged</B></TD>
		  	<TD STYLE='WIDTH:100px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Completed</B></TD>
		  	<TD  NOWRAP STYLE='WIDTH:30px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Days</B></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD  CLASS='TABLE_HEAD' COLSPAN=7 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR><TD COLSPAN=7>&nbsp;</TD></TR>
		<%
		
	
				
			SQL = 	"   SELECT	 PD.DEVELOPMENTNAME, CASE WHEN WO.PROPERTYID IS NULL THEN " &_
					"   CASE WHEN WO.DEVELOPMENTID IS NULL THEN 'Block: ' + B.BLOCKNAME  " &_
					"   ELSE	'Dev: ' + D.DEVELOPMENTNAME END " &_
					"   ELSE CAST(WO.PROPERTYID AS VARCHAR(100))	END AS PROPERTYID," &_
				    "	ISNULL(ISNULL(CAST(WO.PROPERTYID AS VARCHAR(100)), 'Block: ' + B.BLOCKNAME), 'Dev: ' + D.DEVELOPMENTNAME) AS PID,  " &_
					"	ISNULL(CONVERT(VARCHAR,  FL_LOG.SUBMITDATE, 103),'') AS CDATE," &_
					"	ISNULL(CONVERT(VARCHAR, RP.LASTACTIONDATE, 103),'') AS COMPLETIONDATE, " &_
					"	DATEDIFF(Day, FL_LOG.SUBMITDATE, RP.LASTACTIONDATE) AS DIFF,  " &_
					"	S.NAME, FL.DESCRIPTION, " &_
					"	CASE WHEN LEN(FL.DESCRIPTION) > 30 THEN LEFT(FL.DESCRIPTION,30) + '...' ELSE " &_
					"	FL.DESCRIPTION END AS REPAIR " &_
					"	FROM FL_FAULT_LOG FL_LOG  " &_
                    "  INNER JOIN FL_FAULT_JOURNAL FJ ON FJ.FAULTLOGID=FL_LOG.FAULTLOGID  " &_
                    "   INNER JOIN FL_FAULT FL ON FL.FAULTID=FL_LOG.FAULTID  " &_
                    "   INNER JOIN FL_FAULT_PRIORITY PRI ON PRI.PRIORITYID = FL.PRIORITYID " & prioritysql &_ 
                    "   INNER JOIN (SELECT MAX(CJOURNALID) AS CJOURNALID,FJOURNALID FROM FL_FAULTJOURNAL_TO_CJOURNAL GROUP BY FJOURNALID) FJ_CJ  ON FJ_CJ.FJOURNALID=FJ.JOURNALID  " &_
                    "   INNER JOIN C_JOURNAL J ON J.JOURNALID = FJ_CJ.CJOURNALID   " &_
                    "   INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
					"	LEFT JOIN P_WORKORDER WO ON W.WOID = WO.WOID " &_
					"	LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
					"	LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID  " &_
                    "   INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID  AND I.PITYPE = 2    " &_
                    "   LEFT JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID   " &_
                    "   LEFT JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID   " &_
                    "   INNER JOIN P__PROPERTY P ON P.PROPERTYID = FJ.PROPERTYID   " &_
                    "  INNER JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID = P.DEVELOPMENTID  " &_
					"	LEFT JOIN S_ORGANISATION S ON S.ORGID = FL_LOG.ORGID " &_
                    "    INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR GROUP BY JOURNALID) R ON R.JOURNALID = J.JOURNALID   " &_
                    "    INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID  AND RP.LASTACTIONDATE >= '" & FStart & "' AND RP.LASTACTIONDATE <= '" & FEnd & "' AND RP.ITEMSTATUSID=11 AND  RP.ITEMACTIONID IN (6,9,10,15)    " &_
                    "    WHERE 1=1  AND (P.ASSETTYPE IN (" & REQ_ASSETTYPE & ")) " & monthsql & timesql &_
                    " ORDER	BY S.NAME, FL_LOG.SUBMITDATE, DIFF, PROPERTYID "	


'rw prioritysql & "<BR>"

'rw monthsql & "<BR>"

'rw timesql & "<BR>"

'rw sql		
'response.End()
		Call OpenRs(rsSet, SQL)
		
		While not rsSet.EOF 
			cnt = cnt + 1
			Response.write "<TR><TD>" & CNT & "</TD>"
			Response.write "<TD NOWRAP>" & rsSet("NAME") & "</TD>"			
			Response.write "<TD NOWRAP>" & rsSet("DEVELOPMENTNAME") & "</TD>"
			Response.write "<TD NOWRAP>" & rsSet("PROPERTYID") & "</TD>"
			Response.write "<TD NOWRAP TITLE='" & rsSet("DESCRIPTION") & "'>" & rsSet("REPAIR") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("CDATE") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("COMPLETIONDATE") & "</TD>"
			Response.write "<TD ALIGN=RIGHT>" & rsSet("DIFF") & "</TD>"
			Response.write "</TR>"
			rsSet.Movenext()
			response.flush
		Wend		
		
		%>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
