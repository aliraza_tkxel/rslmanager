<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Dim monthno, monthsql, cnt, total, hasChild, strChild
	Dim MONTHNAMES
	
	MONTHNAMES = aRRAY ("YTD","January","February","March","April","May","June","July","August","September","October","November","December")
	monthno = Request("MONTHNO")
	IF monthno = "-1" Then 
		monthsql = " " 
		monthno = 0
	Else 
		monthsql = " AND MONTH(T.STARTDATE) = " & monthno 
	End If
	OpenDB()
	get_fiscal_boundary()
	cnt = 0
	total = 0

	Dim REQ_ASSETTYPE
	REQ_ASSETTYPE = -1
		If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Cstr(Request("ASSETTYPE")) End If
		
	' THERE IS ONE ASSETTYPE WHICH IS A COMBINATION OF TWO SO WE NEED TO SPLIT IF NECESSARY
	If InStr(REQ_ASSETTYPE, ",") > 0 Then
		Dim ASSETT
		ASSETT = Split(REQ_ASSETTYPE,",")
	Else
		ReDim ASSETT(3)
		ASSETT(0) = REQ_ASSETTYPE
		ASSETT(1) = REQ_ASSETTYPE
		ASSETT(2) = REQ_ASSETTYPE
	End If
	
	Function checkForRepairDetails(jid)
	
		SQL = "SELECT  WO.WOID, RP.TITLE, J.CREATIONDATE AS CREATED, " &_
				" ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) AS COMPLETIONDATE, " &_
				" APPROVED2.LASTACTIONDATE AS APPROVED, " &_
				" DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE))) AS TIMETAKEN_IN_DAYS, " &_
				" CASE WHEN (DATEDIFF(SS, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) <= 86400 " &_ 
				"  THEN 'YES' " &_ 
				"  ELSE 'NO' " &_
				"  END AS COMPLETED_ON_TIME , ORG.NAME " &_
				"FROM C_JOURNAL J " &_
				" LEFT JOIN C_MAXCOMPLETEDREPAIRDATE RC ON RC.JOURNALID = J.JOURNALID " &_
				" INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
				" LEFT JOIN P_WORKORDER WO ON WO.WOID = W.WOID " &_
				" LEFT JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				" LEFT JOIN S_ORGANISATION ORG ON ORG.ORGID = PO.SUPPLIERID " &_
				" INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID " &_
				" LEFT JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				" LEFT JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				" LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				" LEFT JOIN C_LASTCOMPLETEDACTION LC ON LC.JOURNALID = J.JOURNALID " &_
				" INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR GROUP BY JOURNALID, ITEMDETAILID) R ON R.JOURNALID = J.JOURNALID " &_
				" INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID " &_
				" INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR WHERE ITEMACTIONID = 9 GROUP BY JOURNALID, ITEMDETAILID) APPROVED1 ON APPROVED1.JOURNALID = J.JOURNALID " &_
				" INNER JOIN C_REPAIR APPROVED2 ON APPROVED1.REPAIRHISTORYID = APPROVED2.REPAIRHISTORYID " &_
				" INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = R.ITEMDETAILID " &_
				" INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY " &_
				"WHERE J.JOURNALID = " & jid
		Call OpenRs(rsSetInfo, SQL)
		If Not rsSetInfo.EOF Then
			While Not rsSetInfo.EOF 
				strChild = "<TR STYLE='DISPLAY:NONE' id='"&cnt&"'><TD COLSPAN=5 style='background-color:white'><TABLE style='background-color:white'><thead><TR><TD><b>WOID</b></TD><TD>" & rsSetInfo("WOID") & "</TD></TR><TR><TD><b>Title</b></TD><TD>" & rsSetInfo("TITLE") & "</TD></TR><TR><TD><b>Created</b></TD><TD>" & rsSetInfo("CREATED") & "</TD></TR><TR><TD><b>Approved</b></TD><TD>" & rsSetInfo("APPROVED") & "</TD></TR></thead></TABLE> </TD></TR>"
				rsSetInfo.Movenext()
				response.flush
			Wend
		End If
		CloseRs(rsSetInfo)
	
	End Function
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<TITLE>Relet Properties</TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		location.href = "frmGNPI11.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	}
	
	// open/close unavailable rows
	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" CLASS='TA'>
<BR><TABLE align=center WIDTH=95% CELLPADDING=1 CELLSPACING=0 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=steelblue>
	<THEAD>
		<TR><td COLSPAN=2><%=MONTHNAMES(monthno)%></td><TD colspan=2 align=center><B>Dates</B></TD><TD align=right></TD></TR>
		<TR> 
			<TD WIDTH=10%><b>No</b></TD>
			<TD WIDTH=40% CLASS='TABLE_HEAD'>&nbsp;<B>Address</B></TD>
		  	<TD WIDTH=15% CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Termination</B></TD>
		  	<TD WIDTH=15% CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Relet</B></TD>
		  	<TD WIDTH=20% NOWRAP STYLE='WIDTH:60px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Days</B></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD  CLASS='TABLE_HEAD' COLSPAN=5 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR><TD COLSPAN=5>&nbsp;</TD></TR>
		<%
		
		SQL = 	"SELECT 	HOUSENUMBER + ' ' +  P.ADDRESS1 AS ADDRESS, " &_	
				"			OLD.ENDDATE, T.STARTDATE, T.PROPERTYID, " &_
				"			DATEDIFF (D, OLD.ENDDATE, T.STARTDATE) AS DAYS " &_
				"FROM 		C_TENANCY T " &_
				"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"			INNER JOIN C_TENANCY OLD ON OLD.PROPERTYID = T.PROPERTYID AND OLD.ENDDATE <= T.STARTDATE " &_
				"WHERE		P.ASSETTYPE IN (" & REQ_ASSETTYPE & ") AND T.STARTDATE > '"&FStart&"' AND OLD.ENDDATE > '"&FStart&"'  " &_
				"			AND T.STARTDATE <= '"&FEnd&"' " & monthsql &_
				"ORDER		BY T.STARTDATE "
		'rw sql		
		'Call OpenRs(rsSet, SQL)
		
		set dbcmd= server.createobject("ADODB.Command")
		dbcmd.ActiveConnection=RSL_CONNECTION_STRING
		dbcmd.CommandType = 4
		dbcmd.CommandText = "KPI_RELETDAYS_DETAIL"
		dbcmd.Parameters.Refresh
		dbcmd.Parameters(1) = FStart
		dbcmd.Parameters(2) = FEnd
		dbcmd.Parameters(3) = Request("MONTHNO")
		dbcmd.Parameters(4) = ASSETT(0)
		dbcmd.Parameters(5) = ASSETT(1)
		dbcmd.Parameters(6) = ASSETT(2)
		set rsSet=dbcmd.execute

		While not rsSet.EOF 
			cnt = cnt + 1
			total = total + rsSet("DURATION")
			' CHECK TO SEE IF THIS ROW HAS ANY UNAVAILABLE PERIODS WHICH NEED TO BE ATTACHED TO CALCULATION
			' AND ALSO DISPLAYED
			hasChild = "No"
			strChild = ""
			'Call checkForUnavailablePeriods(rsSet("TERM"), rsSet("RELET"), rsSet("PROPERTYID"))
			If NOT isNull(rsSet("JOURNALID")) Then
				Response.write "<TR STYLE='CURSOR:HAND' BGCOLOR='BEIGE' ONCLICK=""toggle("&cnt&")""><TD>" & CNT & "</TD>"
				Call checkForRepairDetails (rsSet("JOURNALID"))
			Else
				Response.write "<TR><TD>" & CNT & "</TD>"
			End If
			Response.write "<TD>" & rsSet("DESCRIPTION") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("TERM") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("RELET") & "</TD>"
			Response.write "<TD ALIGN=RIGHT>" & rsSet("DURATION") & "</TD>"
			Response.write "</TR>"
			Response.Write strChild
			rsSet.Movenext()
			response.flush
		Wend		
		
		%>
		<TFOOT>
		<TR>
			<TD COLSPAN=3 STYLE='BORDER-top:2PX SOLID #133E71' align=right>&nbsp;</TD>
			<TD STYLE='BORDER-top:2PX SOLID #133E71' align=right><B>(F)&nbsp;<%=CNT%></B>&nbsp;</TD>
			<TD STYLE='BORDER-top:2PX SOLID #133E71' align=right><B>(D)&nbsp;<%=total%></B></TD>
		</TR>
		</TFOOT>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
