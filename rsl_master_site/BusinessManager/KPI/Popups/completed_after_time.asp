<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->
<%	
	Dim priorty, monthno, monthsql, cnt, total, prioritysql, timesql
	Dim MONTHNAMES
	Dim REQ_ASSETTYPE, ORGID
	
	REQ_PATCH="18,25,26,27,28"
	REQ_ASSETTYPE = -1
	
	If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Request("ASSETTYPE") End If
	If Request("CONTRACTORID") <> "" Then ORGID = Request("CONTRACTORID") End If
	If Request("PATCH") <> "" Then 	REQ_PATCH = Request("PATCH") End If

	priority = Request("PRIORITY")
	If Cint(priority) = 2 Then
		prioritysql = "	AND REP.PRIORITY IN ('C','D')  "
		timesql		= "	AND (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) > 7 "
		
		prioritysql_fl = "	AND PRIORITYID IN (2)  "
		timesql_fl		= "	AND (DATEDIFF(D, SUBMITDATE, CompletedDate)) > 7 "
		
	ElseIf Cint(priority) = 3 Then
		prioritysql = "	AND REP.PRIORITY IN ('E','F')  "
		timesql		= " AND (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) > 31 "
		
		prioritysql_fl = "	AND PRIORITYID IN (1)  "
		timesql_fl		= "	AND (DATEDIFF(D, SUBMITDATE, CompletedDate)) > 28 "
	Else
		prioritysql = "	AND REP.PRIORITY IN ('A','B')  "
		timesql		= "	AND (DATEDIFF(SS, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) > 86400 "
		
		prioritysql_fl = " AND	PRIORITYID IN (3,4)  "
		timesql_fl		= "	AND (DATEDIFF(SS, SUBMITDATE, CompletedDate)) > 86400 "
	End If
		
	MONTHNAMES = aRRAY ("YTD","January","February","March","April","May","June","July","August","September","October","November","December")
	monthno = Request("MONTHNO")
	IF monthno = "-1" Then 
		monthno = 0
		monthsql = ""
	Else 
		monthsql = " AND MONTH(ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE))) = " & monthno
		monthsql_fl = " AND MONTH(CompletedDate) = " & monthno

	End If
	
	IF ORGID <>"" THEN
		    ORGFILTER = " AND PO.SUPPLIERID =" & ORGID
            ORGFILTER_fl = " AND ORGID =" & ORGID
		ELSE
		    ORGFILTER = " AND 1=1 "
            ORGFILTER_fl = " AND 1=1 "
	END IF
	OpenDB()
	get_fiscal_boundary()
	cnt = 0
	total = 0
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<TITLE>Uncompleted Repairs</TITLE>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF style='background-image:none' TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" CLASS='TA'>
<BR><TABLE align=center WIDTH=98% CELLPADDING=1 CELLSPACING=2 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=steelblue>
	<THEAD>
		<TR><td COLSPAN=6><b><%=MONTHNAMES(monthno)%></b></td><TD colspan=2 align=center><B>Dates</B></TD><TD COLSPAN=2></TD></TR>
		<TR> 
			<TD><b>No</b></TD>
			<TD CLASS='TABLE_HEAD'><B>Supplier</B></TD>
            <TD CLASS='TABLE_HEAD'><B>Operative</B></TD>
			<TD CLASS='TABLE_HEAD'><B>Scheme</B></TD>
			<TD CLASS='TABLE_HEAD'>&nbsp;<B>Property</B></TD>
			<TD CLASS='TABLE_HEAD'>&nbsp;<B>Repair/Fault</B></TD>
		  	<TD STYLE='WIDTH:100px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Logged</B></TD>
		  	<TD STYLE='WIDTH:100px' CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Completed</B></TD>
		  	<TD  NOWRAP STYLE='WIDTH:30px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Days</B></TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'><TD  CLASS='TABLE_HEAD' COLSPAN=9 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR><TD COLSPAN=9>&nbsp;</TD></TR>
		<%
		' UNION command is use to merge OLD KPIS and FL KPIS
		
    sql  =  "   SELECT  DEVELOPMENTNAME , " &_
                "            PROPERTYID , " &_
                "            PROPERTYID AS PID , " &_
                "            ISNULL(CONVERT(VARCHAR, SUBMITDATE, 103), '') AS CDATE , " &_
                "            ISNULL(CONVERT(VARCHAR, SUBMITDATE, 103), '') AS LOGDATE , " &_
                "            ISNULL(CONVERT(VARCHAR, CompletedDate, 103), '') AS COMPLETIONDATE , " &_
                "            DATEDIFF(Day, SUBMITDATE, CompletedDate) AS DIFF , " &_
                "            ISNULL(SupplierName, '') AS NAME , " &_
                "            FaultDescription AS DESCRIPTION , " &_
                "             CASE WHEN LEN(FaultDescription) > 30 " &_
                "                  THEN LEFT(FaultDescription, 30) + '...' " &_
                "                  ELSE FaultDescription " &_
                "             END AS REPAIR, " &_
                "             Operative " &_
                "     FROM    FL_FAULT_LIST_COMPLETED_DETAILS " &_
                " WHERE 1=1 " &_
                " AND CompletedDate >= '" & FStart & "' AND CompletedDate <= '" & FEnd & "'" &_
				" AND PATCHID IN (" & REQ_PATCH & ") " &_
                " AND (ASSETTYPE IN (" & REQ_ASSETTYPE & ")) " & monthsql_fl & ORGFILTER_fl & timesql_fl & prioritysql_fl
    sql  = sql & " UNION ALL "
	sql = sql &	"SELECT	 DD.DEVELOPMENTNAME, CASE WHEN WO.PROPERTYID IS NULL THEN " &_
				"	   CASE WHEN WO.DEVELOPMENTID IS NULL THEN 'Block: ' + B.BLOCKNAME " &_
				"	   ELSE	'Dev: ' + D.DEVELOPMENTNAME END " &_
				"	   ELSE CAST(WO.PROPERTYID AS VARCHAR(100))	END AS PROPERTYID, " &_
				"		ISNULL(ISNULL(CAST(WO.PROPERTYID AS VARCHAR(100)), 'Block: ' + B.BLOCKNAME), 'Dev: ' + D.DEVELOPMENTNAME) AS PID,  " &_
				"		ISNULL(CONVERT(VARCHAR, J.CREATIONDATE, 103),'') AS CDATE,J.CREATIONDATE AS LOGDATE, " &_
				"		CONVERT(VARCHAR, ISNULL(RC.COMPLETIONDATE, ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)), 103) AS COMPLETIONDATE, " &_
				"		DATEDIFF(DAY, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE))) AS DIFF, " &_
				"		S.NAME, REP.DESCRIPTION, " &_
				"		CASE WHEN LEN(REP.DESCRIPTION) > 30 THEN LEFT(REP.DESCRIPTION,30) + '...' ELSE " &_
				"			REP.DESCRIPTION END AS REPAIR, " &_
                "       '' AS Operative " &_
				"FROM	C_JOURNAL J  " &_
				"		LEFT JOIN C_REPAIR_NOACCESSLIST AC ON AC.JOURNALID = J.JOURNALID " &_
				"		LEFT JOIN C_MAXCOMPLETEDREPAIRDATE RC ON RC.JOURNALID = J.JOURNALID  " &_
				"		INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " &_
				"		LEFT JOIN P_WORKORDER WO ON W.WOID = WO.WOID " &_
				"		LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_
				"		LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_
				"		INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID  " &_
				"	    LEFT JOIN F_PURCHASEORDER PO ON PO.ORDERID = I.ORDERID " &_
				"		LEFT JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID  " &_
				"		LEFT JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID  " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID  " &_
				"		LEFT JOIN C_LASTCOMPLETEDACTION LC ON LC.JOURNALID = J.JOURNALID  " &_
				"		INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR GROUP BY JOURNALID, ITEMDETAILID) R ON R.JOURNALID = J.JOURNALID  " &_
				"		INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID  " &_
				"		INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = R.ITEMDETAILID  " &_
				"		INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY  " &_
				"		LEFT JOIN S_ORGANISATION S ON S.ORGID = RP.CONTRACTORID " &_
				"		LEFT JOIN PDR_DEVELOPMENT DD ON DD.DEVELOPMENTID = P.DEVELOPMENTID " &_
				"WHERE	I.PITYPE = 2 AND REP.R_EXP_ID = 24  " & prioritysql &_
				"		AND D.PATCHID IN (" & REQ_PATCH & ")" &_
				"		AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) >= '" & FStart & "' AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) <= '" & FEnd & "'  " &_
				"		AND  RP.ITEMACTIONID IN (6,9,10,15) AND (P.ASSETTYPE IN (" & REQ_ASSETTYPE & ")) " & monthsql & ORGFILTER &  timesql &_
				"		AND AC.JOURNALID IS NULL AND J.ITEMNATUREID<>62 "   

		Call OpenRs(rsSet, SQL)

		While not rsSet.EOF 
			cnt = cnt + 1
			Response.write "<TR><TD>" & CNT & "</TD>"
			Response.write "<TD NOWRAP>" & rsSet("NAME") & "</TD>"
            Response.write "<TD NOWRAP>" & rsSet("OPERATIVE") & "</TD>"						
			Response.write "<TD NOWRAP>" & rsSet("DEVELOPMENTNAME") & "</TD>"
			Response.write "<TD NOWRAP>" & rsSet("PROPERTYID") & "</TD>"
			Response.write "<TD NOWRAP TITLE='" & rsSet("DESCRIPTION") & "'>" & rsSet("REPAIR") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("CDATE") & "</TD>"
			Response.write "<TD ALIGN=CENTER>" & rsSet("COMPLETIONDATE") & "</TD>"
			Response.write "<TD ALIGN=RIGHT>" & rsSet("DIFF") & "</TD>"
			Response.write "</TR>"
			rsSet.Movenext()
			response.flush
		Wend		
		
		%>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
