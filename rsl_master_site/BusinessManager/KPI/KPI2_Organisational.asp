<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	fiscalyear = Request("sel_FISCALYEAR")
	
	' SET ASSETTYPE VALUE
	assettype = 1
	patchid=""
	
	If Request("sel_ASSETTYPE") <> "" Then 	assettype = Request("sel_ASSETTYPE") End If
	If Request("sel_PATCH") <> "" Then 	patchid = Request("sel_PATCH") End If
	
	Call OpenDB()
	
	' SET YEAR VALUE
	if (fiscalyear = "") then
		today = FormatDateTime(Date, 1)
	
		SQL = "SELECT YRange FROM dbo.F_FiscalYears WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		Call OpenRs(rsFY, SQL)
		fiscalyear = rsFY("YRange")
		Call CloseRs(rsFY)
	end if

	Call BuildSelect(lstFY, "sel_FISCALYEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2004'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", fiscalyear, NULL, "textbox200", " tabindex=1 ")
	Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH WHERE PATCHID IN(18,25,26,27)", "PATCHID,LOCATION", "LOCATION", "All", patchid, NULL, "textbox200", " tabindex=1 ")			  			  
		
	' FOR SUPPORTED ASSET TYPE WE NEED TO INCLUDE 'SUPPORTED' AND 'SHELTERED' (APPARENTLY)
	SQL = "SELECT * FROM P_ASSETTYPE WHERE ASSETTYPEID IN (1,2,11) ORDER BY ASSETTYPEID "
	suppselected = ""
	gnselected = ""
	Call openRs(rsSet, SQL)
	lstAssetType = "<SELECT NAME='sel_ASSETTYPE' class='textbox200'><OPTION VALUE=''>Please Select</OPTION>"
	While Not rsSet.EOF
		if cint(assettype) = cint(1) Then gnselected = "selected" else suppselected = "selected" End If
		if Cint(rsSet("ASSETTYPEID")) = 1 Then
			lstAssetType = lstAssetType & "<OPTION VALUE='" & rsSet("ASSETTYPEID") & "' " & gnselected & " >" & rsSet("DESCRIPTION") & "</OPTION>"
		ElseIf Cint(rsSet("ASSETTYPEID")) = 2 Then
			lstAssetType = lstAssetType & "<OPTION VALUE='" & rsSet("ASSETTYPEID") & ",11,16' " & suppselected & " >" & rsSet("DESCRIPTION") & "</OPTION>"
		End If
		rsSet.Movenext()
	Wend 
		
	'Response.Write lstFY & " Asset Type : " & lstAssetType
	Call CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Portfolio--&gt; Main Details</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
<script language="JavaScript" type="text/javascript" >

	
	function show_pie(qtr1, qtr2, qtr3, qtr4, strname){
	
		window.showModelessDialog("popups/KPIPieChart.asp?qtr1="+qtr1+"&qtr2="+qtr2+"&qtr3="+qtr3+"&qtr4="+qtr4+"&strname="+strname+"&timestamp="+new Date(), "","dialogHeight: 340px; dialogWidth: 560px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
		//alert("popups/KPIPieChart.asp.asp?qtr1="+qtr1+"&qtr2="+qtr2+"&qtr3="+qtr3+"&qtr4="+qtr4);
	}

	function load_me(frmName, id){
	
		dataFrame.location.href = "/BusinessManager/kpi/iframes/"+frmName+".asp?ASSETTYPE=<%=assettype%>&PATCH=<%=patchid%>&TOPICID="+id + "&FY=<%=fiscalyear%>"
	
	}
	
	function printTheFrame(){
		printFrame(dataFrame)
		}
		
</script>
<body bgcolor=#FFFFFF onload="initSwipeMenu(4);preloadImages()" onunload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<br/>
<script language="javascript" src="/js/apymenu.js"  type="text/javascript"></script>
<script language="javascript" src="/js/print.js"  type="text/javascript"></script>
<script language="javascript"  type="text/javascript">

	var menuItems = [

	<%
	' Create a datashaping connection string to be used later.
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

	strSQL = "SHAPE {SELECT CATEGORYID, CATEGORY FROM KPI_CATEGORY WHERE ACTIVE = 1  ORDER BY CATEGORY} " &_
		 	"	APPEND ({SELECT TOPICID,CATEGORYID,TOPIC AS TOPIC,ACTIVE,FRAMENAME,DISCONTINUED FROM KPI_TOPIC WHERE ACTIVE = 1 AND DISCONTINUED IS NOT NULL ORDER BY TOPIC} AS KPIS " &_
	     	"	RELATE CATEGORYID TO CATEGORYID)"
	     	
	strSQL = "SHAPE {SELECT CATEGORYID, CATEGORY FROM KPI_CATEGORY WHERE ACTIVE = 1  ORDER BY CATEGORY} " &_
		 	"	APPEND ({SELECT TOPICID,CATEGORYID,TOPIC AS TOPIC,ACTIVE,FRAMENAME,DISCONTINUED FROM KPI_TOPIC WHERE ACTIVE = 1 AND TOPICID<=12 ORDER BY TOPIC} AS KPIS " &_
	     	"	RELATE CATEGORYID TO CATEGORYID)"
			
	' Open original recordset
	Set rst = Server.CreateObject("ADODB.Recordset")
	rst.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
	Dim headcount, topiccount
	headcount = rst.recordcount
	
	// THE PARAMETERS FOR THIS ARRAY ARE AS FOLLOWS :
	//	TITLE, JAVASCRIPT FUNCTION OR HYPERLINK, IMAGE1, IMAGE2, TOOL TIP, [CHILD INFO]
	
	While Not rst.EOF
		%>[<%
		Response.Write """" & rst("CATEGORY") & """" & "," & "" & "," & """""" & "," & """""" & "," & """Tool Tip""" & "," & """_blank""" 
		%>]<% 
		Response.Write "," & VbCrLf
		' Set object to child recordset and iterate through
		Set rstKpi = rst("KPIS").Value
		topiccount = rstKpi.recordcount
		if not rstKpi.EOF then
			while Not rstKpi.EOF
				%>[<%
				Response.Write """|" & rstKpi("TOPIC") & """" & "," & """javascript:load_me('"& rstKpi("FRAMENAME") &"', "& rstKpi("TOPICID") &")""" & "," & """""" & "," & """""" & "," & """Tool Tip"""
				%>]<%
				if cint(headcount) <> 0  and Cint(topiccount) <> 0 then Response.Write "," end if
				Response.Write VbcrLF
				topiccount = topiccount - 1 
				rstKpi.MoveNext
			wend
		end if
		headcount = headcount - 1
    	rst.MoveNext
	Wend
	%>
	
	];
	
//
apy_init();

</script>
<form name="thisform" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2">
	<tr style='height:20px' valign=top> 
		<td colspan="2" >
		    <label for="sel_FISCALYEAR" style="width:90px;" >Financial Year :</label> <%=lstFY%>
		    <label for="sel_ASSETTYPE" style="width:80px;margin-left:30px;">Asset Type :</label><%=lstAssetType%>
	    </td> 
	</tr>
	<tr>
	    <td colspan="2">
	        <label for="sel_PATCH"  style="width:90px;">Patch :</label> <%=lstPatch%>
	        <span style="margin-left:30px;">
	            <input tabindex="3" onclick="JAVASCRIPT:thisform.submit()" title='Apply Filters' class='rslbutton' type="submit" name="Submit" value=" Apply Filter "/>
	        </span>
	    </td>
	</tr>
	<tr style='height:5px'><td colspan="2"></td></tr>
	<tr> 
		<td colspan="2" style='height:380px'>
			<iframe src="/secureframe.asp" name="dataFrame" width="100%" height="100%" frameborder="0" style="border:solid 1px #133E71"></iframe>
		</td>
	</tr>
	<tr> 
		<td colspan="2" align=right>&nbsp;
		    <input type="button" value=" PRINT " class="RSLButton" onclick="printTheFrame()">
		 </td>
	</tr>

</table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name="Property_Server" width="400px" height="400px" style='display:none'></iframe> 
</body>
</html>

