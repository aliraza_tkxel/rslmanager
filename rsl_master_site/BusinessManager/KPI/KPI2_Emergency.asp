<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="Includes/kpifunctions.asp" -->


<%
	fiscalyear = Request("sel_FISCALYEAR")
	selValue = Request("selMonth")
	Call OpenDB()
	
	' SET YEAR VALUE
	if (fiscalyear = "") then
		today = FormatDateTime(Date, 1)
	
		SQL = "SELECT YRange,YStart,YEnd FROM dbo.F_FiscalYears WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		Call OpenRs(rsFY, SQL)
		fiscalyear = rsFY("YRange")
        STARTDATE = rsFY("YStart") & " 00:00:01"
        ENDDATE = rsFY("YEnd") & " 23:59:00"
		Call CloseRs(rsFY)
	end if

    Dmonth = -1
	DYear = -1

    if selValue <> "" then
        TempArray = Split(selValue,"_")
	    Dmonth = TempArray(0)
	    Dyear = TempArray(1)
        STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear & " 00:00:01")
	    ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear & " 23:59:00")
    end if
    
	Call BuildSelect(lstFY, "sel_FISCALYEAR", "F_FISCALYEARS WHERE YSTART > '31 mar 2013'", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", fiscalyear, NULL, "textbox200", " tabindex=1 ")
    call get_fiscal_yr_month(lstBox, Dmonth)

	
	sql = "exec KPI_EMERGENCY_COUNTS @StartDate ='" & startdate & "' ,@EndDate='" & endDate & "', @fiscalYear=" & fiscalyear
	Call OpenRs (rsCounts, sql)
    
    if Not rsCounts.EOF Then
    	ReportedFaults = rsCounts("ReportedFaults")
        CompletedFaults = rsCounts("CompletedFaults")
        CompletedOut = rsCounts("CompletedOut")
        NonCompleted = rsCounts("NonCompleted")
		Call CloseRs(rsCounts)
    end if
    Call CloseDB()

    Function get_fiscal_yr_month(ByRef lst, selectedMonth)

		SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & fiscalyear	

		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then
			FStart = rsSet(0) & " 00:00:01"
			FEnd 	= rsSet(1) & " 23:59:00"
            PS = CDate(FormatDateTime(rsSet("YSTART"),1))
		    PE = CDate(FormatDateTime(rsSet("YEND"),1))
    	end if
		CloseRs(rsSet)

        YearStart = Year(PS)
		YearEnd = Year(PE)
		MonthStart = Month(PS)
		MonthEnd = Month(PE)
		
		'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
		MonthC = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
		lst = "<option value=''>YTD</option>" 
		for i=1 to MonthC
			If cint(MonthStart) = cint(selectedMonth) then isSelected = "selected" Else isSelected = "" End If
			lst = lst & "<option value=""" & MonthStart & "_" & YearStart & """ "&isSelected&">" & MonthName(MonthStart) & " " & YearStart & "</option>"
			MonthStart = MonthStart + 1
			if (MonthStart = 13) then
				MonthStart = 1
				YearStart = YearStart + 1
			end if
		next
	
	End Function

   	
%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Portfolio--&gt; Main Details</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <style type="text/css">
        .style2
        {
            width: 100px;
        }
        .style1
        {
             width: 200px;
             font-weight:bold;
             font-family:Arial;
             font-size:12px;
        }     
           
    </style>
</head>
<script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>

<body bgcolor="#FFFFFF" onload="initSwipeMenu(4);preloadImages()" onunload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<br/>
<script language="javascript" src="/js/apymenu.js"  type="text/javascript"></script>
<script language="javascript" src="/js/print.js"  type="text/javascript"></script>

<form name="thisform" method="post">
<table width="60%" border="0" cellspacing="2" cellpadding="2">
	<tr style='height:20px' valign="top"> 
		<td class="style2">
		    <label for="sel_FISCALYEAR">Financial Year :</label> 
		</td>
        <td>
            <%=lstFY%>
        </td> 
	</tr>
    <tr>
        <td class="style2">Month</td>
        <td>
            <select name='selMonth' class='textbox'><%=lstBox%>
            </select>
         
        </td>
	</tr>			 
	<tr>
	    <td></td>
        <td>
            <input tabindex="3" onclick="JAVASCRIPT:thisform.submit()" title='Apply Filters' class='rslbutton' type="submit" name="Submit" value=" Apply Filter "/>
	    </td>
	</tr>
</table>

<table width="100%">
    <tr style='height:50px'> 
        <td></td>
    </tr>
    <tr>
        <td style='width:100'></td>
        <td>
            <table  width="70%" cellspacing="2" cellpadding="2" style="border:solid 1px #133E71" >

	            <tr style='height:5px;'>
                    <td colspan="2" align="center" style='font-weight:bold;font-family:Arial;text-decoration:underline;font-size:12px' >
                        Counts of Emergency Faults
                    </td>
                </tr>
	            <tr style='height:30px'> 
		            <td class="style1">
			            Reported Faults:
		            </td>
                    <td align="right"><%=ReportedFaults %></td>
	            </tr>
                <tr style='height:30px'> 
		            <td class="style1">
			            Completed Faults:
		            </td>
                    <td align="right"><%=CompletedFaults %></td>
	            </tr>
                <tr style='height:30px'> 
		            <td class="style1">
			            Completed Out of Timeframes:
		            </td>
                    <td align="right"><%=CompletedOut %></td>
	            </tr>
               <tr style='height:30px'> 
		            <td class="style1">
			            Not Completed:
		            </td>
                    <td align="right"><%=NonCompleted %></td>
	            </tr>
	          
            </table>
        </td>
    </tr>
</table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name="Property_Server" width="400px" height="400px" style='display:none'></iframe> 
</body>
</html>

