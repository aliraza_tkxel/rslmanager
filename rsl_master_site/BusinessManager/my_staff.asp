<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id
	Dim searchName, searchSQL

	searchName = Replace(Request("name"),"'","''")
	if searchName <> "" Then
		searchSQL = " AND (FIRSTNAME LIKE '%" & searchName & "%' OR LASTNAME LIKE '%" & searchName & "%') "
	Else
		searchName = ""
	End If

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Replace(Request.QueryString("page"),"'","''")
	End If

	Call OpenDB()
	If Not IsNumeric(team_id) Then
		str_data =  "<br/><br/><br/><tr><td class='ERROR_TEXT'>Management System Error -- 'No team id supplied, please contact administrator'</td></tr>"
	elseif team_id = "-1" Then 
		Call get_orphan_peeps()
	else
		Call getTeams()
	end if
	Call CloseDB()

	Function isAbsent(int_employeeid)

		Dim str_isabsent 
		str_isabsent = "No"
		SQL = "SELECT J.* FROM 	E__EMPLOYEE E 	INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 WHERE 	E.EMPLOYEEID = " & int_employeeid
		Call OpenRS(rsSet,SQL)
		if not rsSet.EOF Then
			str_isabsent = "Yes"
		End If
		isAbsent = str_isabsent	

	End Function


	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""

		strSQL = 		"SELECT 	T.TEAMNAME,E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
						"			ISNULL(JR.JobeRoleDescription, 'N/A') AS JOBTITLE,  " &_
						"			ISNULL(CAST(G.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE,  " &_
						"			ISNULL(J.HOLIDAYENTITLEMENTDAYS,0) - ISNULL(SUM(A.DURATION),0) AS REMAININGLEAVE, " &_
						"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
						"			CASE WHEN ISNULL(SUM(AHOL.DURATION),0) = 0 THEN 'No' ELSE 'Yes' END AS HOLREQUEST " &_
						"FROM	 	E__EMPLOYEE E  " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM  " &_
						"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID  " &_
						"			LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND " &_
						"			JNAL.CURRENTITEMSTATUSID = 	5 AND " &_
						"			JNAL.ITEMNATUREID = 2  " &_
						"			LEFT JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.STARTDATE >=  '1 '+ 'JAN' + ' ' + CAST(DATEPART (YYYY, GETDATE()) AS VARCHAR)  AND " &_
						"				A.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID) " &_
						"			LEFT JOIN (SELECT MAX(JOURNALID) JOURNALID ,EMPLOYEEID FROM E_JOURNAL EJ LEFT JOIN E_NATURE N ON EJ.ITEMNATUREID = N.ITEMNATUREID AND APPROVALREQUIRED = 1  WHERE CURRENTITEMSTATUSID = 3 GROUP BY EMPLOYEEID) JHOL ON J.EMPLOYEEID = JHOL.EMPLOYEEID " &_
						"			LEFT JOIN E_ABSENCE AHOL ON JHOL.JOURNALID = AHOL.JOURNALID AND " &_
						"			AHOL.ABSENCEHISTORYID = (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JHOL.JOURNALID) " &_
                        "           LEFT JOIN dbo.E_JOBROLE JR ON JR.JobRoleId = J.JobRoleId " &_
						"WHERE	 	J.LINEMANAGER = " & Session("userid") & " AND J.ACTIVE = 1  " & searchSQL &_
						"GROUP	 	BY J.EMPLOYEEID, HOLIDAYENTITLEMENTDAYS, E.FIRSTNAME, E.LASTNAME,  JR.JobeRoleDescription, G.[DESCRIPTION], " &_
						"			T.TEAMNAME, E.EMPLOYEEID, J.APPRAISALDATE " &_
						"ORDER		BY HOLREQUEST DESC "

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

				team_name = rsSet("TEAMNAME")
				APPDATE = rsSet("APPRAISALDATE")
				If (ISDATE(APPDATE)) Then
					APPDATE = FormatDateTime(APPDATE, 2)
				End If

				'--START OF COMMENT: GET REMAINING LEAVES OF EMPLOYEE----------------
				strSQL = "EXEC E_HOLIDAY_DETAIL " & rsSet("EMPLOYEEID")
				Call OpenRs (rsHols, strSQL)
				IF NOT rsHols.EOF Then
					days_hrs = rsHols(0)
					hols_available = rsHols(1)
				End If
				Call CloseRS(rsHols)
				'--END OF COMMENT--------------------------------------
				
				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick=""load_me(" & rsSet("EMPLOYEEID") & ")"">" &_
					"<td width=""240px"">" & rsSet("FULLNAME") & "</td>" &_
					"<td width=""240px"" align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
					"<td width=""75px"" align=""center"">" & rsSet("GRADE") & "</td>" &_
					"<td width=""75px"" align=""center"">" & hols_available & " " & days_hrs & "</td>"
					If isAbsent(rsSet("EMPLOYEEID")) = "Yes" Then 
						str_data = str_data & "<td width=""70px"" align=""center""><font color=""red"">" & isAbsent(rsSet("EMPLOYEEID")) & "</font></td>" 
					Else
						str_data = str_data & "<td width=""70px"" align=""center"">" & isAbsent(rsSet("EMPLOYEEID")) & "</td>"
					End If

					If rsSet("HOLREQUEST") = "Yes" Then
						str_data = str_data & "<td width=""70px"" align=""center""><font color=""red"">" & rsSet("HOLREQUEST") & "</font></td>" 
					Else
						str_data = str_data & "<td width=""70px"" align=""center"">" & rsSet("HOLREQUEST") & "</td>"
					End If
					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""8"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='my_staff.asp?page=1&name=" & searchName & "&team_id=" & team_id & "'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='my_staff.asp?page=" & prevpage & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='my_staff.asp?page=" & nextpage & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='my_staff.asp?page=" & intPageCount & "&name=" & searchName & "&team_id=" & team_id & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then
			str_data = "<tfoot><tr><td colspan=""8"" style=""font:18px"" align=""center"">No members exist within this team !!</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

	Function get_orphan_peeps()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT 	E.EMPLOYEEID, " &_
					"			FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
					"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, ISNULL(CAST(J.GRADE AS NVARCHAR), 'N/A') AS GRADE, " &_
					"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
					"			'N/A' AS PENSIONSCHEME " &_
					"FROM	 	E__EMPLOYEE E " &_
					"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
					"WHERE	 	J.TEAM IS NULL "

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End If
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize

				APPDATE = rsSet("APPRAISALDATE")
				If (ISDATE(APPDATE)) Then
					APPDATE = FormatDateTime(APPDATE, 2)
				End If
				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick=""load_me(" & rsSet("EMPLOYEEID") & ")"">" &_
					"<td width=""240px"">" & rsSet("FULLNAME") & "</td>" &_
					"<td width=""240px"" align=""left"">N/A</td>" &_
					"<td width=""45px"" align=""center"">N/A</td>" &_
					"<td width=""75px"" align=""center"">N/A</td>" &_
					"<td width=""90px"" align=""center"">N/A</td>" &_
					"<td width=""70px"" align=""center"">N/A</td>" &_
					"</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""8"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='my_staff.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='my_staff.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='my_staff.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='my_staff.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<tfoot><tr><td colspan=""8"" style=""font:18px"" align=""center"">No members exist within this team !!</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function


	'pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""8"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

        function load_me(employee_id) {
            //location.href = "../myjob/erm.asp?employeeid=" + employee_id;
            document.thisForm.employeeid.value = employee_id;
            document.thisForm.action = "../myjob/erm.asp";
            document.thisForm.submit();
        }

        function quick_find() {
            location.href = "my_staff.asp?name=" + thisForm.findemp.value;
        }

    </script>
</head>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_establishment" title='<%=team_name%>' src="Images/tab_my_staff.gif"
                    width="81" height="20" border="0" alt="" />
            </td>
            <td align="right">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right">
                                <input type="button" name="btn_FIND" id="btn_FIND" title="Search for matches using both first and surname."
                                class='RSLBUTTON' value="Quick Find" onclick="quick_find()" />
                            &nbsp;
                            <input type="text" size="18" name="findemp" id="findemp" class="textbox200" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4">
                <img src="images/spacer.gif" width="669" height="1"></td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE"
        border="7">
        <thead>
            <tr>
                <td width="240PX" class='NO-BORDER' colspan="3">
                    <b>Team:
                        <%=team_name%></b>
                </td>
                <td align="center" width="90px" class='NO-BORDER' rowspan="2" valign="bottom">
                    <b>Remaining Leave</b>
                </td>
                <td align="center" width="90px" class='NO-BORDER' rowspan="2" valign="bottom">
                    <b>Absent</b>
                </td>
            </tr>
            <tr>
                <td width="240px" class='NO-BORDER'>
                    <b>Name</b>
                </td>
                <td width="240px" align="left" class='NO-BORDER'>
                    <b>Job Title</b>
                </td>
                <td align="center" width="70px" class='NO-BORDER'>
                    <b>Grade</b>
                </td>
                <td align="center" width="70px" class='NO-BORDER'>
                    <b>Leave Request</b>
                </td>
            </tr>
            <tr style='height: 3px'>
                <td colspan="8" align="center" style='border-bottom: 2px solid #133e71'>
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    <input type="hidden" id="employeeid" name="employeeid" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
