<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, kpi, kpikpi, selValue, divisor
	Dim A, B, C, AA, BB, CC
	Dim ytd_let, ytd_due
	Redim LET_VALUES(MONTHCOUNT)
	Redim DUE_VALUES(MONTHCOUNT)
	Redim KPI_VALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	ytd_let = 0
	ytd_due = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) = 0
			LET_VALUES(x) = 0
			DUE_VALUES(x) = 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
	Else		' PERIOD FOR JUST ONE MONTH
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		divisor = 1
	End If
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_average_rent(2, LET_VALUES, DUE_VALUES)
	for x = 0 to MONTHCOUNT 		' POPULATE YTD VALUES
			ytd_let = ytd_let + LET_VALUES(x)
			ytd_due = ytd_due + DUE_VALUES(x)
			If LET_VALUES(x) = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = (DUE_VALUES(x) / LET_VALUES(x)) * 12 / 52 End If
	next

	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
		A = ytd_let/divisor
		B = ytd_due/divisor
		If A = 0 Then C = 0 Else C = (B / A) End If
		If C = 0 Then KPI = 0 Else KPI = C * 12 / 52 End If
		
		A = FormatNumber(A,2)
		B = FormatCurrency(B)
		C = FormatNumber(C,2)
		KPI = FormatNumber(KPI,2)
		AA = "N/A"
		BB = "N/A"
		CC = "N/A"
		KPIKPI = "N/A"
	Else 
		AA = LET_VALUES(DMonth)/divisor
		BB = DUE_VALUES(DMonth)
		If AA = 0 Then CC = 0 Else CC = (BB / AA) End If
		If CC = 0 Then KPIKPI = 0 Else KPIKPI = CC * 12 / 52 End If
		
		AA = FormatNumber(AA,2)
		BB = FormatCurrency(BB,2)
		CC = FormatNumber(CC,2)
		
		KPIKPI = FormatNumber(KPIKPI,2)
		
		'Next Calculate the Average up to the YTD figure as follows:
		CurrentMonth = 4 ' April
		MonthMatch = False
		A = 0
		B = 0
		Counter = 0
		
		Do while MonthMatch = False
			A = A + LET_VALUES(CurrentMonth)
			B = B +	DUE_VALUES(CurrentMonth)

			'Response.Write " Current Month " & CurrentMonth & " DMonth " & Dmonth & "<BR>"			
			if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true
			CurrentMonth = CurrentMonth + 1
			if (CurrentMonth = 13) then CurrentMonth = 1
			
			Counter = Counter + 1
			if (Counter = 20) then Exit Do
		Loop
		
		A = A/Counter
		B = B/Counter
		
		If A = 0 Then C = 0 Else C = (B / A) End If		
		If C = 0 Then KPI = 0 Else KPI = C * 12 / 52 End If

		A = FormatNumber(A,2)
		B = FormatCurrency(B)
		C = FormatNumber(C,2)
		KPI = FormatNumber(KPI,2)		
				
	End If
		
	CloseDB()
	
	Function get_average_rent(ByRef p_status, ByRef NO_PROPS, ByRef MONTH_RENT)
	
		SQL = 	"SELECT	COUNT(*)  AS MONTHENTRIES, " &_
				"		MONTH(DTIMESTAMP) AS MONTH, " &_
				"		SUM(AMOUNT) AS NOPROPS, " &_
				"		SUM(TOTALVALUE) AS RENT, " &_
				"		(SUM(AMOUNT)/COUNT(*) ) AS AVLETPERMONTH, " &_
				"		(SUM(TOTALVALUE)/SUM(AMOUNT) ) AS AVLETPERMONTH " &_
				"FROM 	P_DAILYSTATUS S " &_
				"WHERE	S.ASSETTYPEID = 1 AND S.STATUSID = " & p_status &_
				"		AND S.DTIMESTAMP >= '"&FStart&"' AND S.DTIMESTAMP <= '"&FEnd&"' " &_
				"		AND DAY(S.DTIMESTAMP) = 1 " &_
				"GROUP 	BY MONTH(DTIMESTAMP) " &_
				"ORDER   	BY MONTH(DTIMESTAMP) "
		rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			NO_PROPS(rsSet("MONTH")) = rsSet("NOPROPS")
			MONTH_RENT(rsSet("MONTH")) = rsSet("RENT")
			rsSet.Movenext()
		Wend
	
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI01.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?LEGEND=�&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>&MINSCALE=60&MAXSCALE=61">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			
      <TABLE CELLPADDING=1 CELLSPACING=2>
        <TR STYLE='HEIGHT:15PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD STYLE='WIDTH:30PX' height="23"></TD>
          <TD height="23">Month&nbsp;&nbsp;&nbsp; 
            <SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>">
              <%=lstBox%>
            </SELECT>
          </TD>
          <TD height="23" align=center><b>Period</b></TD>
		  <TD>&nbsp;</TD>		  
          <TD height="23" align=center><b>YTD</b></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(A) Average Properties</TD>
          <TD ALIGN=RIGHT><%=AA%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=A%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(B) Total Rent</TD>
          <TD ALIGN=RIGHT><%=BB%></TD>
		  <TD></TD>
          <TD ALIGN=RIGHT><%=B%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(C) Average Monthly Rent</TD>
          <TD ALIGN=RIGHT><%=CC%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=C%></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD ALIGN=RIGHT><b>KPI = </b></TD>
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpikpi%></b></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpi%></b></TD>
        </TR>
        <TR STYLE='HEIGHT:85PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR STYLE='VISIBILITY:HIDDEN'>
          <TD></TD>
          <TD> calculation : C x (12/52)</TD>
          <TD></TD>
          <TD></TD>
        </TR>
      </TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
