<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue
	Dim change_of_tenancy, day_to_day, services, decorating, gas, timber, total
	Redim MONTHVALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	total = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			MONTHVALUES(x) = 0
	next
	
	' GET MONTHS PASSED FOR KPI CALCULATION
	If month(date) < 4 Then months_passed = month(date) + 9 Else months_passes = month(date) - 3 End If
	
	OpenDB()

	get_fiscal_boundary()

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
	End If
	
	Call get_kpi_header(TID)
	get_monthly_breakdown()
	get_ytd_repair_cost()
	Call build_fiscal_month_select(lstBox)
	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd)  Then
		kpi = (total/months_passed) * (12/52)
	Else 
		kpi = (total/4)
	End If
	
	' GET FIGURES FOR MONTHLY BREAKDOWN
	Function get_monthly_breakdown()
	
		SQL = 	"SELECT	MONTH(TAXDATE) AS MONTH, SUM(I.GROSSCOST) AS GROSS, " &_
				"		SUM(I.GROSSCOST) / 4 AS WEEKLYEXP " &_
				"FROM	F_PURCHASEITEM I " &_
				"		INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				"		INNER JOIN P_WOTOREPAIR W ON I.ORDERITEMID = W.ORDERITEMID " &_
				"		INNER JOIN C_JOURNAL J ON J.JOURNALID = W.JOURNALID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"WHERE	I.PISTATUS > 9 AND I.EXPENDITUREID IN (24,25,26,27,28,29) " &_
				"		AND INV.TAXDATE >= '" & FStart & "' AND INV.TAXDATE <= '" & FEnd & "' " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) " &_
				"GROUP	BY MONTH(TAXDATE) " &_
				"ORDER 	BY MONTH(TAXDATE) "
		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			MONTHVALUES(rsSet("MONTH")) = rsSet("WEEKLYEXP")
			rsSet.Movenext()
		Wend
	
	End Function
		
	' GET THE YEAR TO DATE REPAIR COSTS
	Function get_ytd_repair_cost()
	
		SQL = 	"SELECT	I.EXPENDITUREID, SUM(I.GROSSCOST) AS GROSS " &_
				"FROM	F_PURCHASEITEM I " &_
				"		INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				"		INNER JOIN P_WOTOREPAIR W ON I.ORDERITEMID = W.ORDERITEMID "&_
				"		INNER JOIN C_JOURNAL J ON J.JOURNALID = W.JOURNALID "&_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID "&_
				"WHERE	I.PISTATUS > 9 AND I.EXPENDITUREID IN (24,25,26,27,28,29) " &_
				"		AND INV.TAXDATE >= '" & STARTDATE & "' AND INV.TAXDATE <= '" & ENDDATE & "' " &_
				"		AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL)  " &_
				"GROUP	BY I.EXPENDITUREID " &_
				"ORDER	BY I.EXPENDITUREID "
		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			If Cint(rsSet("EXPENDITUREID")) = 24 Then 
				day_to_day = rsSet("GROSS")
			ElseIf Cint(rsSet("EXPENDITUREID")) = 25 Then 
				change_of_tenancy = rsSet("GROSS")
			ElseIf Cint(rsSet("EXPENDITUREID")) = 26 Then 
				decorating = rsSet("GROSS")
			ElseIf Cint(rsSet("EXPENDITUREID")) = 27 Then 
				timber = rsSet("GROSS")
			ElseIf Cint(rsSet("EXPENDITUREID")) = 28 Then 
				gas = rsSet("GROSS")
			ElseIf Cint(rsSet("EXPENDITUREID")) = 25 Then 
				services = rsSet("GROSS")
			End If
			total = total + rsSet("GROSS")
			rsSet.Movenext()
		Wend
	
	End Function
	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI01.asp?START="+SD+"&TID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=MONTHVALUES(1)%>&FEB=<%=MONTHVALUES(2)%>&MAR=<%=MONTHVALUES(3)%>&APR=<%=MONTHVALUES(4)%>&MAY=<%=MONTHVALUES(5)%>&JUN=<%=MONTHVALUES(6)%>&JUL=<%=MONTHVALUES(7)%>&AUG=<%=MONTHVALUES(8)%>&SEP=<%=MONTHVALUES(9)%>&OCT=<%=MONTHVALUES(10)%>&NOV=<%=MONTHVALUES(11)%>&DEC=<%=MONTHVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD></TD></TR>
				<TR><TD></TD><TD COLSPAN=2><B>Reactive Repairs</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>Change Of Tenancy</TD><TD ALIGN=RIGHT><%=FormatCurrency(change_of_tenancy,2)%></TD></TR>
				<TR><TD></TD><TD>Day to Day Repairs</TD><TD ALIGN=RIGHT><%=FormatCurrency(day_to_day,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD COLSPAN=2><B>Cyclical Repairs</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>Services</TD><TD ALIGN=RIGHT><%=FormatCurrency(services,2)%></TD></TR>
				<TR><TD></TD><TD>Decorating</TD><TD ALIGN=RIGHT><%=FormatCurrency(decorating,2)%></TD></TR>
				<TR><TD></TD><TD>Gas Services</TD><TD ALIGN=RIGHT><%=FormatCurrency(gas,2)%></TD></TR>
				<TR><TD></TD><TD>Timber Repairs</TD><TD ALIGN=RIGHT><%=FormatCurrency(timber,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><%=FormatCurrency(total,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD ALIGN=RIGHT><b>KPI = </b></TD><TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatCurrency(kpi,2)%></b></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe src="/secureframe.asp" name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
