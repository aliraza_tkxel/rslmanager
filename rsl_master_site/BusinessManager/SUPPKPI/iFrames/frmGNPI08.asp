<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================

		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, kpi, selValue, divisor
		Dim A, B, C, D, E, F, G
		Dim ytd_let, ytd_available, ytd_unavailable, ytd_rentdue, ytd_initialrent, ytd_rebate, ytd_voidrent, ytd_payment
		
		Redim MONTHVALUES(MONTHCOUNT)
		Redim LET_VALUES(MONTHCOUNT)
		Redim AVAILABLE_VALUES(MONTHCOUNT)
		Redim UNAVAILABLE_VALUES(MONTHCOUNT)
		Redim RENTDUE_VALUES(MONTHCOUNT)
		Redim INITIALRENT_VALUES(MONTHCOUNT)
		Redim REBATE_VALUES(MONTHCOUNT)
		Redim VOID_RENT_VALUES(MONTHCOUNT)	
		Redim PAYMENT_VALUES(MONTHCOUNT)
		Redim KPI_VALUES(MONTHCOUNT)

	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	'===========================================================================================	
		Dim REQ_ASSETTYPE
		REQ_ASSETTYPE = -1
		If Request("ASSETTYPE") <> "" Then REQ_ASSETTYPE = Cint(Request("ASSETTYPE")) End If
		selValue = Request("START")
		TID = Request("TOPICID")

	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	

		if TID = "" Then TID = -1 End if
		total = 0
		ytd_let = 0
		ytd_available = 0
		ytd_unavailable = 0
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
				KPI_VALUES(x) = 0
		next
		
		get_fiscal_boundary()
		Dmonth = -1
		DYear = -1

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	

	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	

		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If

	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	

		Call get_kpi_header(TID)
		Call build_fiscal_month_select(lstBox, Dmonth)
		Call get_monthly_property_count(2, LET_VALUES)
		Call get_monthly_property_count(1, AVAILABLE_VALUES)
		Call get_monthly_property_count(4, UNAVAILABLE_VALUES)
		Call get_rent_due(RENTDUE_VALUES ,0) 
		Call get_initial_rent(INITIALRENT_VALUES ,0) 
		Call get_rebate(REBATE_VALUES ,0)
		Call get_void_rent(VOID_RENT_VALUES)
		Call get_payments(PAYMENT_VALUES ,0)
		
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	
	
		for x = 0 to MONTHCOUNT 
				
				ytd_let = ytd_let + LET_VALUES(x)
				ytd_available = ytd_available + AVAILABLE_VALUES(x)
				ytd_unavailable = ytd_unavailable + UNAVAILABLE_VALUES(x)
				ytd_rentdue = ytd_rentdue + RENTDUE_VALUES(x)
				
				ytd_initialrent = ytd_initialrent + INITIALRENT_VALUES(x)
				ytd_rebate = ytd_rebate + Abs(REBATE_VALUES(x))
				ytd_voidrent = ytd_voidrent + VOID_RENT_VALUES(x)
				ytd_payment = ytd_payment + aBS(PAYMENT_VALUES(x))
				
				TempD = RENTDUE_VALUES(x) + INITIALRENT_VALUES(x) - Abs(REBATE_VALUES(x))
				TempE = VOID_RENT_VALUES(x) - INITIALRENT_VALUES(x) + Abs(REBATE_VALUES(x))

				If TempD = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = (TempE/TempD) * 100 End If
				
		next
	
		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		' Important Part of the code :  
		'
		' Below determines what values to use, if a month is selected in the drop down
		' then the second part is used, but if YTD is chosen then the first part is used.
		'
		'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
			
			prd_Let = ytd_let/divisor
			prd_Available = ytd_available/divisor
			prd_Unavailable = ytd_unavailable/divisor
			prd_Due = (ytd_rentdue + ytd_initialrent) - ytd_rebate
			prd_Void = ytd_voidrent - ytd_initialrent + ytd_rebate
			'rw "(" & ytd_voidrent & " - " & ytd_initialrent & " + " & ytd_rebate & ")"
			prd_Collected = ytd_payment
			prd_Due_Void	= prd_Due + prd_Void

			' Set KPI for the period
			If prd_Due = 0 Then KPI = 0 Else KPI = (prd_Void / prd_Due) * 100 End If
			
			' As we dont need the month vars, set them to N/A
			mnth_Let 		= "N/A"
			mnth_Available	= "N/A"
			mnth_Unavailable= "N/A"
			mnth_Due 		= "N/A"
			mnth_Void 		= "N/A"
			mnth_Collected 	= "N/A"
		
		Else 
		
			CurrentMonth 	= 4 ' April
			MonthMatch 		= False
			
			' Initialise the varibales for the loop
			Counter 		= 0
			prd_Let 		= 0
			prd_Available 	= 0
			prd_Unavailable = 0
			prd_Due 		= 0
			prd_Void 		= 0
			prd_Collected 	= 0

			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			' Important Loop :  
			'
			' This loop adds up the monthly figures for each variable and collates them to show the period figure.
			' The loop stops when the month chosen is equal to the var number 'CurrentMonth'.
			'
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			
			Do while MonthMatch = False  
			
				' The following three build monthly figures to work out the period calculation done outside the loop
				void 	= void + VOID_RENT_VALUES(CurrentMonth)
				rent 	= rent + RENTDUE_VALUES(CurrentMonth)
				init 	= init + INITIALRENT_VALUES(CurrentMonth)
				rebate  = rebate + ABS(REBATE_VALUES(CurrentMonth))
				
				prd_Let 		= prd_Let 			+ LET_VALUES(CurrentMonth)
				prd_Available 	= prd_Available 	+ AVAILABLE_VALUES(CurrentMonth)
				prd_Unavailable = prd_Unavailable 	+ UNAVAILABLE_VALUES(CurrentMonth)
				
				prd_Collected 	= prd_Collected 	+ ABS(PAYMENT_VALUES(CurrentMonth))
				
				'check if current month number in the loop is equal to the month chosen
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop
			
			prd_Due 		= prd_Due			+ ((rent + init) - rebate)
			prd_Void 		= prd_Viod 			+ (void - init + rebate)

			prd_Due_Void	= prd_Due + prd_Void
			prd_Let 		= prd_Let / counter
			prd_Available	= prd_Available/ counter
			prd_Unavailable	= prd_Unavailable/ counter

			' Set KPI for the period after loop 
			If prd_Due = 0 Then KPI = 0 Else KPI = (prd_Void / prd_Due) * 100 End If
		
			mnth_Let = LET_VALUES(DMonth)/counter
			mnth_Available = AVAILABLE_VALUES(DMonth)/counter
			mnth_Unavailable = UNAVAILABLE_VALUES(DMonth)/counter
			mnth_Due = ((RENTDUE_VALUES(DMonth) + INITIALRENT_VALUES(DMonth)) - Abs(REBATE_VALUES(DMonth)))
			mnth_Void = VOID_RENT_VALUES(DMonth) - INITIALRENT_VALUES(DMonth) + Abs(REBATE_VALUES(DMonth))
			mnth_Collected = ABS(PAYMENT_VALUES(DMonth))
			mnth_G = opening_balance
			
			' set KPI for the month
			If mnth_Due = 0 Then mnth_KPI = 0 Else mnth_KPI = (mnth_Void / mnth_Due) * 100 End If
			
			mnth_KPI 		=  FormatNumber(mnth_KPI,2)
			mnth_Let 		=  FormatCurrency(mnth_Let,2)
			mnth_Available 	=  FormatCurrency(mnth_Available,2)
			mnth_Unavailable=  FormatCurrency(mnth_Unavailable,2)
			mnth_Due 		=  mnth_Due + mnth_Void
			
			mnth_Due_void	=  FormatCurrency(mnth_Due,2)
			mnth_Void		=  FormatCurrency(mnth_Void,2)
			mnth_Collected 	=  FormatCurrency(mnth_Collected,2)
			
		End If
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	

	
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI08.asp?ASSETTYPE=<%=REQ_ASSETTYPE%>&START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="357" CELLPADDING=1 CELLSPACING=2>
				<TR><TD width="154" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="89" align="right"><strong>Month</strong></td>
			    <td width="98" align="right"><strong>Period</strong></td>
				</TR>
				<TR><TD COLSPAN=3><B>Properties (Average)</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(A) Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Let%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Let,2)%></TD></TR>
				<TR><TD>(B) Available to Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Available%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Available,2)%></TD></TR>
				<TR><TD>(C) Unavailable to Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Unavailable%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Unavailable,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD COLSPAN=3><B>Rent</B></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(D) Available</TD>
				  <TD ALIGN=RIGHT><%=mnth_Due_Void%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(prd_Due_Void,2)%></TD></TR>
				<TR><TD>(E) Void Rent</TD>
				  <TD ALIGN=RIGHT><%=mnth_Void%></TD>
			    <TD ALIGN=RIGHT><%=FormatCurrency(prd_Void,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=mnth_kpi%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<% if showtemp = true then %>
				<TR STYLE='HEIGHT:30PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>D = Total Income from A + B + C</TD>
				  <TD></TD>
			    <TD></TD></TR>
				<TR><TD>E = Total Income from B + C</TD>
				  <TD></TD>
			    <TD></TD></TR>
				<TR STYLE='HEIGHT:10PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD>calculation: (E / D) x 100</TD>
				  <TD></TD>
			    <TD></TD></TR>
			<% end if %>
		</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
