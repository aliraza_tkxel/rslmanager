<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================	
	
		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, kpi, selValue, divisor, opening_balance
		Dim A, B, C
		Dim ytd_let, ytd_available, ytd_unavailable  
		Redim KPIVALUES(MONTHCOUNT)
		Redim LET_VALUES(MONTHCOUNT)
		Redim AVAILABLE_VALUES(MONTHCOUNT)
		Redim UNAVAILABLE_VALUES(MONTHCOUNT)
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	
		selValue = Request("START")
		TID = Request("TOPICID")
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	
		
		if TID = "" Then TID = -1 End if
		ytd_let = 0
		ytd_available = 0
		ytd_unavailable = 0
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
				KPIVALUES(x) = 0
		next
	
		get_fiscal_boundary()
		Dmonth = -1
		DYear = -1

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	

	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	

		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If
	
	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	
	

		Call get_kpi_header(TID)
		Call build_fiscal_month_select(lstBox, Dmonth)
		Call get_end_of_month_property_count(2, LET_VALUES)
		Call get_end_of_month_property_count(1, AVAILABLE_VALUES)
		Call get_end_of_month_property_count(4, UNAVAILABLE_VALUES)
		
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	

		for x = 0 to MONTHCOUNT 
			yr_TotalNumber = LET_VALUES(Month(Date())) + AVAILABLE_VALUES(Month(Date())) + UNAVAILABLE_VALUES(Month(Date()))
			yr_Let = LET_VALUES(Month(Date()))
			yr_Available = AVAILABLE_VALUES(Month(Date()))
			TempA = LET_VALUES(x) + AVAILABLE_VALUES(x) + UNAVAILABLE_VALUES(x)
			If TempA = 0 Then KPIVALUES(x) = 0 Else KPIVALUES(x) = (AVAILABLE_VALUES(x) / TempA) * 100 End If
		next
	
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
			
			prd_TotalNumber = yr_TotalNumber
			prd_Let = yr_Let
			prd_Available = yr_Available
			
			rw  prd_TotalNumber & "<BR>"
			rw prd_Let  & "<BR>"
			rw prd_Available 	 & "<BR>"	
			
			If prd_TotalNumber = 0 Then KPI = 0 Else KPI = (prd_Available / prd_TotalNumber) * 100 End If
			
		Else 
		
			CurrentMonth 	 = 4 ' April
			MonthMatch 		 = False
			
			' Initialise the varibales for the loop
			Counter 		= 0
			prd_TotalNumber = 0
			prd_Let  		= 0
			prd_Available 	= 0


			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			' Important Loop :  
			'
			' This loop adds up the monthly figures for each variable and collates them to show the period figure.
			' The loop stops when the month chosen is equal to the var number 'CurrentMonth'.
			'
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			Do while MonthMatch = False  
			
				' The following three build monthly figures to work out the period calculation done outside the loop
				'prd_TotalNumber = prd_TotalNumber +  LET_VALUES(CurrentMonth) + AVAILABLE_VALUES(CurrentMonth) + UNAVAILABLE_VALUES(CurrentMonth)
				Unav			= UNAVAILABLE_VALUES(CurrentMonth)
				prd_Let 		= LET_VALUES(CurrentMonth)
				prd_Available 	= AVAILABLE_VALUES(CurrentMonth)
				prd_TotalNumber = prd_Let + prd_Available + Unav
				
				'check if current month number in the loop is equal to the month chosen
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop

			
			' Calculate the period KPI
			If prd_TotalNumber = 0 Then KPI = 0 Else KPI = (prd_Available / prd_TotalNumber) * 100 End If

			' Calculate the monthly values
			mnth_TotalNumber = LET_VALUES(DMonth) + AVAILABLE_VALUES(DMonth) + UNAVAILABLE_VALUES(DMonth)
			mnth_Let 		 = LET_VALUES(DMonth)
			mnth_Available   = AVAILABLE_VALUES(DMonth)
			
			' calc the monthly KPI
			If mnth_TotalNumber = 0 Then mnth_KPI = 0 Else mnth_KPI = (mnth_Available / mnth_TotalNumber) * 100 End If
			
			'Format the monthly vars after calculation for accuracy
			mnth_KPI		 = FormatNumber(mnth_KPI,2)
			mnth_TotalNumber = FormatNumber(mnth_TotalNumber,2)
			mnth_Let		 = FormatNumber(mnth_Let,2)
			mnth_Available 	 = FormatNumber(mnth_Available,2)
			
		End If
		
	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	
	
	CloseDB()
	
	'===========================================================================================
	'	Function List
	'===========================================================================================	
		
		Function get_end_of_month_property_count(ByRef p_status, ByRef PROPERTY_COUNT_VALUES)
		
			SQL = 	"SELECT 	PMONTH, AMOUNT " &_
					"FROM 		P_DAILYSTATUS S " &_
					"			INNER JOIN   (SELECT MONTH(DTIMESTAMP)AS PMONTH, MAX(DTIMESTAMP) AS PRE " &_
					"			   FROM P_DAILYSTATUS WHERE  ASSETTYPEID = 1 AND STATUSID = " & Cint(p_status) &_
					"			   AND ASSETTYPEID = 1 AND STATUSID = " & Cint(p_status) &_
					"		      AND DTIMESTAMP >= '" & FStart & "' AND DTIMESTAMP <= '" & FEnd & "' " &_
					"			  GROUP BY MONTH(DTIMESTAMP))  PP ON PP.PRE = S.DTIMESTAMP " &_
					"WHERE	 S.DTIMESTAMP >= '" & FStart & "' AND S.ASSETTYPEID = 1 AND S.DTIMESTAMP <= '" & FEnd & "' AND S.STATUSID = " & Cint(p_status) &_
					"ORDER   BY MONTH(DTIMESTAMP) "
			'rw sql	
			Call OpenRs(rsSet, SQL)
			While not rsSet.EOF 
				PROPERTY_COUNT_VALUES(rsSet("PMONTH")) = rsSet("AMOUNT")
				rsSet.Movenext()
			Wend
		
		End Function
	
	'===========================================================================================
	'	End Function List
	'===========================================================================================	

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI09.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?JAN=<%=KPIVALUES(1)%>&FEB=<%=KPIVALUES(2)%>&MAR=<%=KPIVALUES(3)%>&APR=<%=KPIVALUES(4)%>&MAY=<%=KPIVALUES(5)%>&JUN=<%=KPIVALUES(6)%>&JUL=<%=KPIVALUES(7)%>&AUG=<%=KPIVALUES(8)%>&SEP=<%=KPIVALUES(9)%>&OCT=<%=KPIVALUES(10)%>&NOV=<%=KPIVALUES(11)%>&DEC=<%=KPIVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="353" CELLPADDING=1 CELLSPACING=2>
				<TR><TD width="161" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="86" align="right"><strong>Month</strong></td>
			    <td width="90" align="right"><strong>Period</strong></td>
				</TR>
				<TR><TD><b>Properties</b></TD>
				  <TD></TD>
			    <TD></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(A) Total Number</TD>
				  <TD ALIGN=RIGHT><%=mnth_TotalNumber%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_TotalNumber,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(B) Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Let%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(mnth_Let,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(C) Available To Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Available%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Available,2)%></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD ALIGN=RIGHT><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=mnth_kpi%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:55PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD> calculation : (C / A) x 100</TD>
				  <TD></TD>
			    <TD></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
