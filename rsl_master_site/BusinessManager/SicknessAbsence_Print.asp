<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PageName = "SicknessAbsence.asp"

	' only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	' redirect all director to a higher level showing their teams
	'if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, theToDate
	Dim searchName, searchSQL, orderSQL, DEFAULTstring
	Dim TOTALDAYSABSENT, whereSQL, TOTALCOST, theFinalCost


	Call GetVariables()
	Call getTeams()
	Call OpenDB()
	Call GetDepartmentReportVars()
	Call CloseDB()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()

		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		theToDate = Request.QueryString("theToDate")
		OrderBy = Request.QueryString("CC_SORT")
		SortForPrintandXLS = OrderBy

		If OrderBy <> "" Then
			orderSQL = " ORDER BY " & OrderBy
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End If

		If Dapartment <> "" Then
			deptSQL =  " AND T.TEAMID =  " & Dapartment & " "
		End If

		If theDate <> "" Then
			dateSQL =  dateSQL & " AND A.STARTDATE >=  '" & theDate & "' "
		End if

		If theToDate <> "" Then
			dateSQL =  dateSQL & " AND A.STARTDATE <=  '" & theToDate & "' "
		End If

		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate & "&theTodate=" & theToDate 

		whereSQL =  dateSQL & " " & deptSQL

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT  E.EMPLOYEEID, E.FIRSTNAME, E.LASTNAME, PFT.DESCRIPTION AS PARTFULLTIME, JD.STARTDATE AS EmployeeStartDate, T.TEAMNAME, A.STARTDATE, A.RETURNDATE, ISNULL(A.NOTES, 'No reason available.') AS REASON, " &_
						"		(SELECT ISNULL(CAST(COUNT(*) AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM, " &_
						"		(A.DURATION) AS SICKNESSABSENCE, " &_
						"		(SELECT ISNULL(CAST(SUM(SALARY)AS NVARCHAR), '0') FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS SALARY " &_
                        "       , JD.JOBTITLE " &_
						"FROM E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID AND A.STARTDATE >= '1 JAN 2005' " &_
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = A.JOURNALID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
                        "       LEFT JOIN E_PARTFULLTIME PFT ON JD.PARTFULLTIME = PFT.PARTFULLTIMEID " &_
						"WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND DURATION IS NOT NULL  " &_
						" " & whereSQL & orderSQL

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		count = 0
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.

			while not rsSet.eof
				str_data = str_data & "<tbody><tr>" &_
					"<td width=""100px"">" & rsSet("LASTNAME") & "</td>" &_
					"<td width=""106px"" align=""left"">" & rsSet("FIRSTNAME") & "</td>" &_
                    "<td align=""left"">" & rsSet("PARTFULLTIME") & "</td>" &_
					"<td align=""left"">" & rsSet("EmployeeStartDate") & "</td>" &_
                    "<td align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
                    "<td width=""111px"" align=""left"">" & rsSet("TEAMNAME") & "</td>" &_
					"<td width=""77px"" align=""center"">" & rsSet("STARTDATE") & "</td>"&_
					"<td width=""117px"" align=""center"">" & rsSet("RETURNDATE")  & "</td>" &_
					"<td width=""54px"" align=""center"">" & rsSet("SICKNESSABSENCE")  & "</td>" &_
					"<td width=""216px"" align=""left"">" & rsSet("REASON") & "</td>"

					If rsSet("TEAMNUM") > 0 then
						thenum = IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE")))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(thenum) & "</td>"
					Else
						thenum  = IsNullNumber((((rsSet("SALARY")/52) / 5) * rsSet("SICKNESSABSENCE")))
						str_data = str_data & "<td align=""center"" width=""67px"">" & FormatNumber(thenum) & "</td>"
					End If

					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				wend

			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

		' if no teams exist inform the user
		If count = 0 Then
			str_data = "<tfoot><tr><td colspan=""10"" style=""font:18px"" align=""center"">No records to display</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

			'Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records

	End function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""10"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function GetDepartmentReportVars()

		SQL = 		"SELECT (SELECT ISNULL(CAST(COUNT(*) AS FLOAT), 0) FROM E_JOBDETAILS WHERE TEAM = T.TEAMID AND ACTIVE = 1) AS TEAMNUM, " &_
						"		(A.DURATION) AS SICKNESSABSENCE, " &_
						"		jd.SALARY AS SALARY " &_						
						"FROM E__EMPLOYEE E " &_
						"		INNER JOIN E_JOURNAL J ON J.EMPLOYEEID = E.EMPLOYEEID  " &_
						"		INNER JOIN E_ABSENCE A ON A.JOURNALID = J.JOURNALID AND A.STARTDATE >= '1 JAN 2005' " &_
						"					  AND A.ABSENCEHISTORYID =	( " &_
						"									SELECT MAX(ABSENCEHISTORYID)  " &_
						"									FROM E_ABSENCE  " &_
						"									WHERE JOURNALID = A.JOURNALID " &_
						"									) " &_
						"		LEFT JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID " &_
						"		LEFT JOIN E_TEAM T ON T.TEAMID = JD.TEAM " &_
						"WHERE 	ITEMNATUREID = 1 AND  A.ITEMSTATUSID NOT IN (20) " &_
						"			AND DURATION IS NOT NULL  " &_
						" " & whereSQL & orderSQL

			theFinalCost = 0
			Call OpenRs(rsLoader, SQL)
				If (NOT rsLoader.EOF) Then
				While NOT rsLoader.eof
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
					TOTALDAYSABSENT = TOTALDAYSABSENT + rsLoader("SICKNESSABSENCE")
					If rsLoader("TEAMNUM") > 0 Then
						THECOST = ( (((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE")) )
						theFinalCost = theFinalCost + THECOST
					Else
						theFinalCost = theFinalCost + (((rsLoader("SALARY")/52) / 5) * rsLoader("SICKNESSABSENCE"))
					End If
				'==================================================================
				'= End Totals For department report
				'==================================================================
				rsLoader.MoveNext()
				Wend
				End If
			Call CloseRS(rsLoader)
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
    <table border="0" width="900" cellpadding="2" cellspacing="2" height="40px">
        <tr>
            <td width="584">
                <b>Department Information :</b>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <div id="DeptInfo">
                    <table width="354" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #133e71">
                        <tr>
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td width="12">
                                &nbsp;
                            </td>
                            <td width="116">
                                <strong>Total Days Absent: </strong>
                            </td>
                            <td width="75">
                                <%= TOTALDAYSABSENT %>
                            </td>
                            <td width="71">
                                <strong>Total Cost : </strong>
                            </td>
                            <td width="78">
                                <%= FORMATNUMBER(theFinalCost,2) %>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" colspan="5">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table width="900" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2"><img name="tab_establishment" title="<%=team_name%>" src="Images/tab_establishmentreport.gif" width="157" height="20" border="0" alt="" /></td>
            <td align="right" height="20" bgcolor="#fff">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4"><img src="images/spacer.gif" width="740" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="900" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: collapse;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE"
        border="7">
        <thead>
            <tr align="left" valign="bottom">
                <td width="100" class='NO-BORDER'>
                    <b>Surname</b>
                </td>
                <td width="106" class='NO-BORDER'>
                    <b>First Name</b>
                </td>
                <td class='NO-BORDER'>
                    <b>Full/Part Time</b>
                </td>
                <td class='NO-BORDER'>
                    <b>Employee Start Date</b>
                </td>
                <td width="106" class='NO-BORDER'>
                    <b>Job Title</b>
                </td>
                <td width="111" class='NO-BORDER'>
                    <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME desc")%><%=DEFAULTstring%>"
                        style="text-decoration: none">
                        <img src="../myImages/sort_arrow_down.gif" border="0" width="11"
                            height="12" alt="Sort Descending" /></a> <b>Dept.</b> <a href="<%=PageName%>?<%=theURL%>CC_Sort=<%=Server.UrlEncode("T.TEAMNAME  asc")%><%=DEFAULTstring%>"
                                style="text-decoration: none">
                                <img src="../myImages/sort_arrow_up_old.gif" border="0" width="17"
                                    height="16" alt="Sort Ascending" /></a>
                </td>
                <td width="77" class='NO-BORDER'>
                    <b>Start Date</b>
                </td>
                <td width="117" class='NO-BORDER'>
                    <strong>Returned to Work </strong>
                </td>
                <td width="54" class='NO-BORDER'>
                    <strong>Duration</strong>
                </td>
                <td width="216" class='NO-BORDER'>
                    <b>Reason</b>
                </td>
                <td width="67" align="left" valign="bottom" class='NO-BORDER'>
                    <b>Cost</b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="11" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
</body>
</html>
