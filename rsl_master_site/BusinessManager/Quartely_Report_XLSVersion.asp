<%
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="Connections/db_connection.asp" -->
<%
dim Session
set Session = server.CreateObject("SessionMgr.Session2")

	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, fromDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL, EMPSQL
	Dim PrevSelected, allSelected, EmployeeType,OrderBy 
	

		GetVariables()
		getTeams()
		'GetDepartmentReportVars()
	


	Function GetVariables()
		
		PrevSelected =  ""
		AllSelected = ""
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
        fromDate = Request.QueryString("fromDate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")
		
		If OrderBy <> "" then
			orderSQL =  " ORDER BY  " & OrderBy
		else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End if
		
			
		If Dapartment <> "" then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End if
		
		If theDate <> "" then
			whereNeeded = 1
			if deptSQL <> "" then
			dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End if

        If fromDate <> "" then
			whereNeeded = 1
            if deptSQL <> "" then
			fromdateSQL = fromdateSQL & " AND "
            end if
			fromdateSQL =  fromdateSQL & " (J.ENDDATE IS NULL) OR J.ENDDATE >=  '" & fromDate & "' "

		End if
		
		if whereNeeded = 1 then
			whereSQL = " AND " & deptSQL & fromdateSQL
		End If
			
	End Function
	
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 
	
		str_data = ""
		strSQL = 		"SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(EG.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE,J.FTE,PT.DESCRIPTION  " &_
						"FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	lEFT JOIN E_GRADE EG ON EG.GRADEID=CAST(J.GRADE AS INT) " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
                        "	LEFT JOIN E_PARTFULLTIME PT ON PT.PARTFULLTIMEID = J.PARTFULLTIME " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL & orderSQL 
								
		'response.write strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()


		str_data = str_data &  "<tbody>"
			
		while not rsSet.eof
		
				str_data = str_data & 	"<TR>" &_
										"<TD>" & rsSet("FULLNAME") & "</TD>" &_
										"<TD  ALIGN=LEFT>" & rsSet("JOBTITLE") & "</TD>" &_
										"<TD  ALIGN=CENTER>" & rsSet("DEPARTMENT") & "</TD>" &_
										"<TD  ALIGN=CENTER>" & rsSet("GENDER") & "</TD>"&_
										"<TD  ALIGN=CENTER>" & rsSet("ETHNICITY")  & "</TD>" &_
										"<TD  ALIGN=CENTER>" & rsSet("DOB") & "</TD>"&_
										"<TD  ALIGN=CENTER>" & rsSet("SALARY") & "</TD>"&_
										"<TD  ALIGN=CENTER>" & rsSet("GRADE") & "</TD>"&_
                                        "<TD  ALIGN=CENTER>" & rsSet("DESCRIPTION") & "</TD>"&_
                                        "<TD  ALIGN=CENTER>" & rsSet("FTE") & "</TD>"&_
										"<TD  ALIGN=CENTER>" & rsSet("STARTDATE") & "</TD>"&_
										"<TD  ALIGN=CENTER>" & rsSet("ENDDATE") & "</TD>"

					str_data = str_data & "</TR>"
								
		rsSet.MoveNext()
		wend
		
		rsSet.Close()
		
		
		str_data = str_data &  "</tbody>"
	End function
	
	Function GetDepartmentReportVars()
	
				sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
						"			SUM(	CASE WHEN J.STARTDATE >= '" & fromDate & "' and  J.STARTDATE <= '" & theDate & "'    " &_
						"				THEN 1    " &_
						"				ELSE  0    " &_
						"    		END )AS STARTERS, " &_
						"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS MALETOTAL, " &_
						"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS FEMALETOTAL, " &_
						"			SUM(	CASE WHEN J.ENDDATE >= '" & fromDate & "' and J.ENDDATE <= '" & theDate & "' " &_ 
						"				THEN 1    " &_ 
						"				ELSE  0   " &_
						"    		END )AS LEAVERS " &_
						"FROM	E__EMPLOYEE E   " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL  & EMPSQL 
						
				set rsLoader = Server.CreateObject("ADODB.Recordset")
				rsLoader.ActiveConnection = RSL_CONNECTION_STRING 			
				rsLoader.Source = strSQL
				rsLoader.CursorType = 3
				rsLoader.CursorLocation = 3
				rsLoader.Open()
				
				if (NOT rsLoader.EOF) then
			
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
						TotalSalary 		= 	FormatCurrency(rsLoader("SALARY"))
						TotalStaff			=	rsLoader("TOTALSTAFF")
						maletotal			=	rsLoader("MALETOTAL")
						femaletotal			=	rsLoader("FEMALETOTAL")
						NewStarters			=	rsLoader("STARTERS") 						
						Leavers				=	rsLoader("LEAVERS") 
				'==================================================================
				'= End Totals For department report
				'==================================================================	
				end if
				
				rsLoader.Close()
				
	End Function
%>
<HTML>
<HEAD>
<TITLE>RSL Manager Business</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<table width="800" border="0" cellspacing="0" cellpadding="0">
 
		<TR>
			<TD>Name</TD>
			<TD>Job Title</TD>
			<TD>Dept.</TD>
			<TD>Gender</TD>
			<TD>Ethnicity</TD>
			<TD>DOB</TD>
			<TD>Salary</TD>
			<TD>Grade</TD>
            <td>Full/Part</td>
            <td>FTE</td>
			<TD>Start Date</TD>
			<TD>End Date</TD>
		</TR>
		<%=str_data%>
</table>
</BODY>
</HTML>

