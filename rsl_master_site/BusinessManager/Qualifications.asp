<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
if (EmployeeID = "") then EmployeeID = -1 end if

Call OpenDB()

SQL = "SELECT * FROM E_QUALIFICATIONSANDSKILLS " &_
		"WHERE E_QUALIFICATIONSANDSKILLS.EMPLOYEEID = " & EmployeeID & " ORDER BY QUALIFICATIONDATE DESC"
Call OpenRs(rsLoader, SQL)

SQL = "SELECT * FROM E__EMPLOYEE " &_
		"WHERE E__EMPLOYEE.EMPLOYEEID = " & EmployeeID 

Call OpenRs(rsName, SQL)
If (NOT rsName.EOF) Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")
	FullName = "<font color=""blue"">" & l_firstname & " " & l_lastname & "</font>"
End If
Call CloseRS(rsName)

strTable = "<table cellspacing=""0"" cellpadding=""1"" style=""behavior:url(/Includes/Tables/tablehl.htc)"" slcolor='' hlcolor='STEELBLUE'>" &_
                "<thead>" &_
                "<tr>" &_
                    "<td width=""200px""><b>Subject</b></td>" &_
                    "<td width=""90px""><b>Result</b></td>" &_
                    "<td width=""80px""><b>Date</b></td>" &_
                "</tr>" &_
                "<tr>" &_
                    "<td colspan=""4"">" &_
                        "<hr style=""border:1px dotted #133E71"" /></td>" &_
                    "</td>" &_
                "</thead>" &_
                "<tbody>"
If (not rsLoader.EOF) Then
	While (NOT rsLoader.EOF) 
		special = rsLoader("SpecialisedTraining")
		titlebit = ""
		If (special <> "" AND Not isNull(special)) Then
			titlebit = " title='Specialised Training:" & special & "'"
		End If
		strTable = strTable & "<tr style=""cursor:pointer"">"
		strTable = strTable & "<td onclick=""EditMe(" & rsLoader("QUALIFICATIONSID") & ")"" valign=""top"" " & titlebit & ">" & rsLoader("Subject") & "</td>"	
		strTable = strTable & "<td valign=""top"">" & rsLoader("RESULT") & "</td>"	
		strTable = strTable & "<td valign=""top"">" & rsLoader("QUALIFICATIONDATE") & "</td>"
		strTable = strTable & "<td onclick='DeleteMe(" & rsLoader("QUALIFICATIONSID") & ")'><font color=""red"">DEL</font></td>"
		strTable = strTable & "</tr>"
		rsLoader.moveNext
	wend
Else
	strTable = strTable & "<tr>"
	strTable = strTable & "<td colspan=""3"" align=""center"">No Qualifications\Skills</td>"
	strTable = strTable & "</tr>"
End If
strTable = strTable & "</tbody>"
strTable = strTable & "<tfoot>"
strTable = strTable & "<tr>"
strTable = strTable & "<td colspan=""4"">"
strTable = strTable & "<hr style=""border:1px dotted #133e71"" />"
strTable = strTable & "</td>"
strTable = strTable & "</tr>"
strTable = strTable & "</tfoot>"
strTable = strTable & "</table>"
Call CloseRs(rsLoader)

Call BuildSelect(lstqualtype, "sel_EXISTPLANNED", "E_QUALIFICATIONTYPE", "QUALIFICATIONTYPEID, QUALIFICATIONTYPEDESC", "QUALIFICATIONTYPEDESC", "Please Select", NULL, NULL, "textbox200", " tabindex=""1"" " )
Call BuildSelect(lstquallevel, "sel_QualLevelID", "E_QUALIFICATIONLEVEL", "QualLevelId, QualLevelDesc", "QualLevelSortOrder ASC", "Please Select", NULL, NULL, "textbox200", " tabindex='2' " )

 Dim str_UpdatedBy, str_UpdatedOn, rsLastUpdated
    SQL_LastUpdated = "EXECUTE E_EMPLOYEE_AUDIT_GETLASTUPDATED @EmployeeId = " & EmployeeID
    
    str_UpdatedBy = "N/A"
    str_UpdatedOn = "N/A"

    SET rsLastUpdated = Conn.Execute (SQL_LastUpdated)

    if not rsLastUpdated.EOF Then
        str_UpdatedBy = rsLastUpdated("UpdatedBy")
        str_UpdatedOn = rsLastUpdated("UpdatedOn")
    End If

    Call CloseRs(rsLastUpdated)

Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_SUBJECT|Subject|TEXT|Y"
        FormFields[1] = "txt_RESULT|Result|TEXT|Y"
        FormFields[2] = "txt_QUALIFICATIONDATE|Date|DATE|Y"
        FormFields[3] = "txt_SPECIALISEDTRAINING|Specialised Training|TEXT|N"
        FormFields[4] = "txt_PROFESSIONALQUALIFICATION|Professional Qualification|TEXT|N"
        FormFields[5] = "txt_KEYCOMPETENCIES1|Key Competencies|TEXT|N"
        FormFields[6] = "txt_KEYCOMPETENCIES2|Key Competencies|TEXT|N"
        FormFields[7] = "txt_KEYCOMPETENCIES3|Key Competencies|TEXT|N"
        FormFields[8] = "txt_PERSONALBIO|Personal Biol|TEXT|N"
        FormFields[9] = "txt_CPD|CPD|TEXT|N"
        FormFields[10] = "txt_LANGUAGESKILLS|Language Skills|TEXT|N"
        FormFields[11] = "sel_EXISTPLANNED|Qualification Type|SELECT|Y"
        FormFields[12] = "sel_QualLevelID|Qualification Type|SELECT|Y"

        function SaveForm() {
            if (!checkForm()) return false;
            //document.RSLFORM.hid_Action.value = "NEW"	
            RSLFORM.submit()
        }

        function ResetForm() {
            document.RSLFORM.hid_QUALID.value = ""
            document.RSLFORM.hid_Action.value = "NEW"
            document.RSLFORM.myButton.value = " ADD "
            document.RSLFORM.reset()
        }

        function DeleteMe(THE_ID) {
            document.RSLFORM.hid_QUALID.value = THE_ID
            document.RSLFORM.hid_Action.value = "DELETE"
            document.RSLFORM.submit()
        }
        function EditMe(THE_ID) {
            document.RSLFORM.hid_QUALID.value = THE_ID
            document.RSLFORM.hid_Action.value = "LOAD"
            document.RSLFORM.submit()
        }
    </script>
    <script language="JavaScript" type="text/JavaScript">

        //Function to limit text entered into textarea 
        function limitText(textArea, length) {
            if (textArea.value.length > length) {
                textArea.value = textArea.value.substr(0, length);
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img name="E_PD" id="E_PD" src="Images/1-closed.gif" width="115" height="20" border="0"
                        alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" id="E_C" src="images/2-closed.gif" width="74" height="20" border="0"
                        alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" id="E_SQ" src="Images/3-open.gif" width="94" height="20" border="0"
                        alt="Qualifications" /></a>
            </td>
            <td rowspan="2" style="display: none">
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" id="E_PE" src="Images/4-previous.gif" width="86" height="20" border="0"
                        alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" id="E_DD" src="Images/5-previous.gif" width="70" height="20" border="0"
                        alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" id="E_JD" src="Images/6-closed.gif" width="52" height="20" border="0"
                        alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" id="E_B" src="images/7-closed.gif" width="82" height="20" border="0"
                        alt="Benefits" /></a>
            </td>
            <td width="265" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <form name="RSLFORM" method="post" action="ServerSide/Qualifications_svr.asp" target="hiddenFrame"
        style="margin: 0px">
        <table cellspacing="2" style="height: 90%; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71; border-right: 1px solid #133E71"
            width="750">
            <tr>
                <td nowrap="nowrap" valign="top">Qualification Type :
                </td>
                <td>
                    <%=lstqualtype%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_EXISTPLANNED" id="img_EXISTPLANNED" width="15px"
                        height="15px" border="0" alt="" />
                </td>
                <td width="100%" rowspan="17" valign="top">
                    <br />
                    <div id="ListRows">
                        <%=strTable%>
                    </div>
                    <div align="right">
                        <input type="button" value=" NEXT " title=" NEXT " class="RSLBUTTON" onclick="Javascript: location.href = 'PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>'"
                            tabindex="14" style="cursor: pointer" />
                    </div>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Qualification Level :
                </td>
                <td>
                    <%=lstquallevel%>
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_QualLevelID" id="img_QualLevelID" width="15px"
                        height="15px" border="0" alt=" PLANNED" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Subject:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_SUBJECT" id="txt_SUBJECT" maxlength="50"
                        value="" tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_SUBJECT" id="img_SUBJECT" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Result:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_RESULT" id="txt_RESULT" maxlength="10"
                        value="" tabindex="2" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_RESULT" id="img_RESULT" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">Date:
                </td>
                <td>
                    <input type="text" class="textbox200" name="txt_QUALIFICATIONDATE" id="txt_QUALIFICATIONDATE"
                        maxlength="10" value="" tabindex="3" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_QUALIFICATIONDATE" id="img_QUALIFICATIONDATE" width="15px"
                        height="15px" border="0" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Specialised Training:
                </td>
                <td>
                    <textarea class="textbox200" name="txt_SPECIALISEDTRAINING" id="txt_SPECIALISEDTRAINING"
                        rows="4" maxlength="499" onkeypress="limitText(this,499);" tabindex="4"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_SPECIALISEDTRAINING" id="img_SPECIALISEDTRAINING"
                        width="15px" height="15px" border="0" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Professional Qualification :
                </td>
                <td>
                    <input type="text" name="txt_PROFESSIONALQUALIFICATION" id="txt_PROFESSIONALQUALIFICATION"
                        class="textbox200" maxlength="200" tabindex="5" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_PROFESSIONALQUALIFICATION" id="img_PROFESSIONALQUALIFICATION"
                        width="15px" height="15px" border="0" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Key Competencies :
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES1" id="txt_KEYCOMPETENCIES1" class="textbox200"
                        maxlength="200" tabindex="6" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES1" id="img_KEYCOMPETENCIES1" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">&nbsp;
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES2" id="txt_KEYCOMPETENCIES2" class="textbox200"
                        maxlength="200" tabindex="7" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES2" id="img_KEYCOMPETENCIES2" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">&nbsp;
                </td>
                <td>
                    <input type="text" name="txt_KEYCOMPETENCIES3" id="txt_KEYCOMPETENCIES3" class="textbox200"
                        maxlength="200" tabindex="8" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_KEYCOMPETENCIES3" id="img_KEYCOMPETENCIES3" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr style='display: none'>
                <td nowrap="nowrap" valign="top">Personal Bio :
                </td>
                <td>
                    <textarea name="txt_PERSONALBIO" id="txt_PERSONALBIO" rows="4" class="textbox200"
                        maxlength="499" onkeypress="limitText(this,499);" tabindex="9"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_PERSONALBIO" id="img_PERSONALBIO" width="15px" height="15px"
                        border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">CPD :
                </td>
                <td>
                    <textarea name="txt_CPD" id="txt_CPD" rows="4" class="textbox200" maxlength="499"
                        onkeypress="limitText(this,499);" tabindex="10"></textarea>
                </td>
                <td valign="top">
                    <img src="/js/FVS.gif" name="img_CPD" id="img_CPD" width="15px" height="15px" border="0"
                        alt="" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" valign="top">Language Skills :
                </td>
                <td>
                    <input type="text" name="txt_LANGUAGESKILLS" id="txt_LANGUAGESKILLS" class="textbox200"
                        maxlength="200" tabindex="11" />
                </td>
                <td>
                    <img src="/js/FVS.gif" name="img_LANGUAGESKILLS" id="img_LANGUAGESKILLS" width="15px"
                        height="15px" border="0" alt="" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="hidden" name="hid_EMPLOYEEID" id="hid_EMPLOYEEID" value="<%=EmployeeID%>" />
                    <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                    <input type="hidden" name="hid_QUALID" id="hid_QUALID" value="" />
                    <input type="hidden" name="hid_LASTACTIONUSER" id="hid_LASTACTIONUSER" value="<%=Session("UserID")%>" />
                    <input type="button" name="myButton" id="myButton" class="RSLButton" value=" ADD "
                        title=" ADD " onclick="SaveForm()" style="cursor: pointer" tabindex="12" />
                    <input type="button" name="BtnRESET" id="BtnRESET" class="RSLButton" value=" RESET "
                        title=" RESET " onclick="ResetForm()" style="cursor: pointer" tabindex="13" />
                </td>
            </tr>
            <tr>
                <td height="100%">&nbsp;
                </td>
            </tr>
        </table>

        <div id="divUpdateTimeStamp" style="float: right; margin-right: 20px; margin-top: 10px;">
            <table>
                <tbody>
                    <tr>
                        <td>Updated By:
                        </td>
                        <td>
                            <%=str_UpdatedBy %>
                        </td>
                    </tr>
                    <tr>
                        <td>Updated On:
                        </td>
                        <td>
                            <%=str_UpdatedOn %>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="clear: both">
        </div>

    </form>
    <iframe src="/secureframe.asp" name="hiddenFrame" id="hiddenFrame" style="display: none"></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
