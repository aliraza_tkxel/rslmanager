<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

	ACTION = "NEW"
	EmployeeID = Request("EmployeeID")
if (EmployeeID = "") then EmployeeID = -1 end if

OpenDB()

SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsName, SQL)

If NOT rsName.EOF Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")	
	FullName = "<font color=blue>" & l_firstname & " " & l_lastname & "</font>"

End If
Call CloseRs(rsName)

SQL = "SELECT * FROM E_BENEFITS WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then
	
	' tbl E_BENEFITS

	l_pension = rsLoader("PENSION")
	l_salarypercent = rsLoader("SALARYPERCENT")
	l_contribution = rsLoader("CONTRIBUTION")
	l_avc = rsLoader("AVC")
	l_contractedout = rsLoader("CONTRACTEDOUT")
	l_professionalfess = rsLoader("PROFESSIONALFEES")
	l_telephoneallowance = rsLoader("TELEPHONEALLOWANCE")	
	l_bupa = rsLoader("BUPA")						
	l_bupaexcess = rsLoader("BUPAEXCESS")						
	l_firstaidallowance = rsLoader("FIRSTAIDALLOWANCE")						
	l_calloutallowance = rsLoader("CALLOUTALLOWANCE")						
	l_car = rsLoader("CAR")						
	l_cartype = rsLoader("CARTYPE")
	l_capacity = rsLoader("CAPACITY")						
	l_co2 = rsLoader("CO2")						
	l_listprice = rsLoader("LISTPRICE")						
	l_excesscontribution = rsLoader("EXCESSCONTRIBUTION")						
	l_carallowance = rsLoader("CARALLOWANCE")

	l_carloanstartdate = rsLoader("CARLOANSTARTDATE")						
	l_term = rsLoader("TERM")						
	l_value = rsLoader("VALUE")
	ACTION = "AMEND"
end if

CloseRs(rsLoader)

CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidationExtras.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function TogglePension(){
	if (document.getElementsByName("rdo_PENSION")[0].checked) EnableItems("1,2,3,4")
	else DisableItems("1,2,3,4")
	}

function ToggleCar(){
	if (document.getElementsByName("rdo_CAR")[0].checked) EnableItems("12,13,14,15,16")
	else DisableItems("12,13,14,15,16")
	}

function ToggleBupa(){
	if (document.getElementsByName("rdo_BUPA")[0].checked) EnableItems("8")
	else DisableItems("8")
	}

var FormFields = new Array();
FormFields[0] = "rdo_PENSION|Pension|RADIO|Y"
FormFields[1] = "txt_SALARYPERCENT|Salary|CURRENCY|I_N"
FormFields[2] = "txt_CONTRIBUTION|Contribution|CURRENCY|I_Y"
FormFields[3] = "txt_AVC|AVC|CURRENCY|I_N"
FormFields[4] = "rdo_CONTRACTEDOUT|Contracted Out|RADIO|I_Y"
	
FormFields[5] = "txt_PROFESSIONALFEES|Professional Fees|CURRENCY|N"
FormFields[6] = "txt_TELEPHONEALLOWANCE|Telephone allowance|CURRENCY|N"	
FormFields[7] = "rdo_BUPA|Team|RADIO|Y"
FormFields[8] = "txt_BUPAEXCESS|Bupa Excess|CURRENCY|I_Y"
FormFields[9] = "txt_FIRSTAIDALLOWANCE|First Aider Allowance|CURRENCY|N"	
FormFields[10] = "txt_CALLOUTALLOWANCE|Call Out Allowance|CURRENCY|N"
		
FormFields[11] = "rdo_CAR|Car|RADIO|Y"
FormFields[12] = "txt_CARTYPE|Car Type|TEXT|I_Y"	
FormFields[13] = "txt_CAPACITY|Capacity|TEXT|I_Y"						
FormFields[14] = "txt_CO2|CO2|TEXT|I_Y"						
FormFields[15] = "txt_LISTPRICE|List Price|CURRENCY|I_Y"								
FormFields[16] = "txt_EXCESSCONTRIBUTION|Excess Contribution|CURRENCY|I_Y"

FormFields[17] = "txt_CARALLOWANCE|Car Allowance|CURRENCY|N"						
FormFields[18] = "txt_CARLOANSTARTDATE|Car Loan Start Date|DATE|N"						
FormFields[19] = "txt_TERM|Term|TEXT|N"									
FormFields[20] = "txt_VALUE|Value �|CURRENCY|N"						

function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.submit()
	}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages();ToggleCar();TogglePension();ToggleBupa();checkForm()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width=750 border=0 cellpadding=0 cellspacing=0>
	<tr> 
		<td rowspan=2> <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details"><img title="Personal Details" name="E_PD" src="Images/1-closed.gif" width=115 height=20 border=0></a></td>
		<td rowspan=2> <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info"><img alt="Contacts" name="E_C" src="images/2-closed.gif" width=74 height=20 border=0></a></td>
		<td rowspan=2> <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications"><img alt="Qualifications" name="E_SQ" src="images/3-closed.gif" width=94 height=20 border=0></a></td>
		<td rowspan=2> <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment"><img alt="Previous Employment" name="E_PE" src="images/4-closed.gif" width=86 height=20 border=0></a></td>
		<td rowspan=2> <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities"><img alt="Difficulties/Disabilities" name="E_DD" src="Images/5-closed.gif" width=70 height=20 border=0></a></td>
		<td rowspan=2> <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details"><img alt="Job Details" name="E_JD" src="Images/6-closed.gif" width=52 height=20 border=0></a></td>
		<td rowspan=2> <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits"><img alt="Benefits" name="E_B"  src="Images/7-open.gif" width=82 height=20 border=0></a></td>
		<td width=177 height=19 align="right"><%=FullName%></td>
	</tr>
	<tr> 
		<td bgcolor=#133E71> <img src="images/spacer.gif" width=53 height=1></td>
	</tr>
	<tr> 
		<td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="images/spacer.gif" width=53 height=6></td>
	</tr>
	</table>
	<TABLE cellspacing=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="90%">
	<form name="RSLFORM" method="POST" ACTION="ServerSide/Benefits_svr.asp">
	<TR> 
		<TD nowrap colspan="2"><b>PENSION</b> </TD>
		<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
		<TD colspan="2"><b>COMPANY CAR</b> </TD>
		<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Pension:</TD>
		<TD> 
		<% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_pension = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			elseif (l_car = 0) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
		<input type="RADIO" name="rdo_PENSION" value="1" <%=yesChecked%> onclick="TogglePension()" tabindex=1>
		YES 
		<input type="RADIO" name="rdo_PENSION" value="0" <%=noChecked%> onclick="TogglePension()" tabindex=1>
		NO </TD>
		<TD><image src="/js/FVS.gif" name="img_PENSION" width="15px" height="15px" border="0"></TD>
		<TD>Car:</TD>
		<TD> 
		<% 
		yesChecked = "CHECKED"
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_car = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			elseif (l_car = 0) then
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
		<input type="RADIO" name="rdo_CAR" value="1" <%=yesChecked%> onclick="ToggleCar()" tabindex=2>
		YES 
		<input type="RADIO" name="rdo_CAR" value="0" <%=noChecked%> onclick="ToggleCar()" tabindex=2>
		NO </TD>
		<TD><image src="/js/FVS.gif" name="img_CAR" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Salary %:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_SALARYPERCENT" maxlength=10 value="<%=l_salarypercent%>" DISABLED tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_SALARYPERCENT" width="15px" height="15px" border="0"></TD>
		<TD>Car Type:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CARTYPE" maxlength=80 value="<%=l_cartype%>" DISABLED tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_CARTYPE" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Contribution &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CONTRIBUTION" maxlength=10 value="<%=l_contribution%>" DISABLED tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_CONTRIBUTION" width="15px" height="15px" border="0"></TD>
		<TD>Capacity:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CAPACITY" maxlength=10 value="<%=l_capacity%>" DISABLED tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_CAPACITY" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>AVC &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_AVC" maxlength=10 value="<%=l_avc%>" DISABLED tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_AVC" width="15px" height="15px" border="0"></TD>
		<TD>CO2:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CO2" maxlength=10 value="<%=l_co2%>" DISABLED tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_CO2" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Contracted Out:</TD>
		<TD> 
		<% 
		yesChecked = ""
		noChecked = ""
		if ACTION = "AMEND" then
			if (l_contractedout = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			elseif (l_contractedout = 1) then		
				yesChecked = ""
				noChecked = "CHECKED"
			end if
		end if
		%>
		<input type="RADIO" name="rdo_CONTRACTEDOUT" value="1" <%=yesChecked%> DISABLED tabindex=1>
		YES 
		<input type="RADIO" name="rdo_CONTRACTEDOUT" value="0" <%=noChecked%> DISABLED tabindex=1>
		NO </TD>
		<TD><image src="/js/FVS.gif" name="img_CONTRACTEDOUT" width="15px" height="15px" border="0"></TD>
		<TD>List Price:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_LISTPRICE" maxlength=10 value="<%=l_listprice%>" DISABLED tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_LISTPRICE" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap colspan="2">&nbsp; </TD>
		<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
		<TD>Excess Contribution &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_EXCESSCONTRIBUTION" maxlength=10 value="<%=l_excesscontribution%>" DISABLED tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_EXCESSCONTRIBUTION" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap colspan="2"><b>GENERAL</b> </TD>
		<TD><image src="/js/FVS.gif" name="img_PROBATIONPERIOD" width="15px" height="15px" border="0"></TD>
		<TD colspan="2">&nbsp;</TD>
	</TR>
	<TR> 
		<TD nowrap>Professional Fees &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_PROFESSIONALFEES" maxlength=10 value="<%=l_professionalfess%>" tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_PROFESSIONALFEES" width="15px" height="15px" border="0"></TD>
		<TD colspan="2"><b>CAR ESSENTIAL USER</b> </TD>
		<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Telephone Allowance &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_TELEPHONEALLOWANCE" maxlength=10 value="<%=l_telephoneallowance%>" tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_TELEPHONEALLOWANCE" width="15px" height="15px" border="0"></TD>
		<TD>Car Allowance &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CARALLOWANCE" maxlength=10 value="<%=l_carallowance%>" tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_CARALLOWANCE" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>BUPA:</TD>
		<TD> 
		<% 
		yesChecked = ""
		noChecked = "CHECKED"
		if ACTION = "AMEND" then
			if (l_bupa = 1) then
				yesChecked = "CHECKED"
				noChecked = ""
			end if
		end if
		%>
		<input type="RADIO" name="rdo_BUPA" value="1" <%=yesChecked%> onclick="ToggleBupa()" tabindex=1>
		YES 
		<input type="RADIO" name="rdo_BUPA" value="0" <%=noChecked%> onclick="ToggleBupa()" tabindex=1>
		NO </TD>
		<TD><image src="/js/FVS.gif" name="img_BUPA" width="15px" height="15px" border="0"></TD>
		<TD>Car Loan Start Date:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CARLOANSTARTDATE" maxlength=10 value="<%=l_carloanstartdate%>" tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_CARLOANSTARTDATE" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>BUPA Excess &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_BUPAEXCESS" maxlength=10 value="<%=l_bupaexcess%>" tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_BUPAEXCESS" width="15px" height="15px" border="0"></TD>
		<TD>Term:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_TERM" maxlength=10 value="<%=l_term%>" tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_TERM" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>First Aider Allowance &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_FIRSTAIDALLOWANCE" maxlength=10 value="<%=l_firstaidallowance%>" tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_FIRSTAIDALLOWANCE" width="15px" height="15px" border="0"></TD>
		<TD>Value &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_VALUE" maxlength=10 value="<%=l_value%>" tabindex=2></TD>
		<TD><image src="/js/FVS.gif" name="img_VALUE" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD nowrap>Call Out Allowance &pound;:</TD>
		<TD><input type="textbox" class="textbox200" name="txt_CALLOUTALLOWANCE" maxlength=10 value="<%=l_calloutallowance%>" tabindex=1></TD>
		<TD><image src="/js/FVS.gif" name="img_CALLOUTALLOWANCE" width="15px" height="15px" border="0"></TD>
		<TD>&nbsp;</TD>
		<TD>&nbsp; </TD>
		<TD><image src="/js/FVS.gif" name="img_HOURS" width="15px" height="15px" border="0"></TD>
	</TR>
	<TR> 
		<TD colspan=4></TD>
		<TD align=right> 
		<input type=hidden name="hid_EmployeeID" value="<%=EmployeeID%>">
		<input type=hidden name="hid_Action" value="<%=ACTION%>">
		<input type=button class="RSLButton" value=" SAVE & NEXT " onclick="SaveForm()">
		</TD>
	</TR>
	<TR height="100%"> 
		<TD height="100%">&nbsp;</TD>
	</TR>
</FORM>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
