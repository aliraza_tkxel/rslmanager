<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	OpenDB()
	'NEED TO FIND THE EMPLOYEE LIMIT FOR THE USER
	SQL = "SELECT ISNULL(EMPLOYEELIMIT,0) AS EMPLOYEELIMIT FROM E_JOBDETAILS WHERE EMPLOYEEID = " & SESSION("USERID")
	Call OpenRs(rsEmLimit, SQL)
	if (NOT rsEmLimit.EOF) then
		EmployeeLimit = rsEmLimit("EMPLOYEELIMIT")
		DisableAll = ""			
	else
		DisableAll = " disabled"
		EmployeeLimit = -1
	end if
	

	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	EmployeeLimit = 10000

	ColData(0)  = "Prop. Ref|PROPERTYID|80"
	SortASC(0) 	= "PROPRTYID ASC"
	SortDESC(0) = "PROPRTYID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Repair Detail|REPAIR|250"
	SortASC(1) 	= "REPAIR ASC"
	SortDESC(1) = "REPAIR DESC"	
	TDSTUFF(1)  = "" 
	TDFunc(1) = ""		

	ColData(2)  = "Element|REPAIRAREA|100"
	SortASC(2) 	= "REPAIRAREA ASC"
	SortDESC(2) = "REPAIRAREA DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Repair Item|MAINAREA|150"
	SortASC(3) 	= "MAINAREA ASC"
	SortDESC(3) = "MAINAREA DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Cost|QUOTEDCOST|70"
	SortASC(4) 	= "QUOTEDCOST ASC"
	SortDESC(4) = "QUOTEDCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	ColData(5)  = "Approve|EMPTY|50"
	SortASC(5) 	= ""
	SortDESC(5) = ""	
	TDSTUFF(5)  = " "" style='background-color:white' align='right'><input type=""""checkbox"""" name=""""Approve"""" value="""" "" & rsSet(""JOURNALID"") & "" """"><b"" "
	TDFunc(5) = ""		

	PageName = "QueuedRepairs.asp"
	EmptyText = "No Queued Repairs found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		'Call SetSort()

	SQLCODE ="SELECT J.JOURNALID, J.PROPERTYID, PO.QUOTEDCOST, '' as empty, " &_
			"REPAIR = CASE " &_
			"WHEN LEN(IT.DESCRIPTION) > 40 THEN LEFT(IT.DESCRIPTION,40) + '...' " &_
			"ELSE IT.DESCRIPTION " &_
			"END, " &_
			"IT.DESCRIPTION AS FULLREPAIR, " &_
			"AI.DESCRIPTION AS REPAIRAREA, " &_
			"A.DESCRIPTION AS MAINAREA " &_
			"FROM C_JOURNAL J " &_
			"INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
			"INNER JOIN R_ITEMDETAIL IT ON R.ITEMDETAILID = IT.ITEMDETAILID " &_ 
			"INNER JOIN R_AREAITEM AI ON AI.AREAITEMID = IT.AREAITEMID " &_
			"INNER JOIN R_AREA A ON A.AREAID = IT.AREAID " &_
			"INNER JOIN C_NATURE N ON J.ITEMNATUREID = N.ITEMNATUREID " &_
			"LEFT JOIN C_REPAIRPURCHASE RP ON RP.JOURNALID = J.JOURNALID " &_
			"LEFT JOIN F_PURCHASEORDER PO ON PO.ORDERID = RP.ORDERID " &_
			"WHERE R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR RR WHERE RR.JOURNALID = J.JOURNALID) " &_
			"AND J.CURRENTITEMSTATUSID = 12 AND PO.QUOTEDCOST <= " & EmployeeLimit

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --> Queued Repairs</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(Order_id){
	window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id, "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
	}

function ApproveItems(){
	thisForm.action="ServerSide/ApproveRepairs.asp"
	thisForm.method = "POST"
	thisForm.submit()
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=GET>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
<table width=750><tr><td align=right><input class="RSLButton" type=button onclick="ApproveItems()" value=" Approve Selected Items "></td></tr></table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>