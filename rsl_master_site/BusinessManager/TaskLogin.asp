<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CanAccessTaskManager = False
	
	Call OpenDB()
	SQL = "SELECT TASK_MANAGER_USER_ID FROM RSL_TASK_MANAGER_LINKAGE WHERE CAN_ACCESS_TASK_MANAGER = " & Session("UserID")
	Call OpenRs(rsTM, SQL)	

	TaskManagerUserID = -1
	TaskManagerOrg = -1	
	If NOT rsTM.EOF Then	
		CanAccessTaskManager = True
		TaskManagerUserID = rsTM("TASK_MANAGER_USER_ID")
		
		SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'TASK_MANAGER_ORGANISATION'"
		Call OpenRs(rsORG, SQL)
		if NOT rsORG.EOF then
			TaskManagerOrg = rsORG("DEFAULTVALUE")		
		end if
		Call CloseRs(rsORG)
	Else
		CanAccessTaskManager = False
	End If
	
	Call CloseRs(rsTM)
	Call CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script>
function openwindow(org, user){
	window.open("http://taskmanager.reidmark.com/ClientPortal/Serverside/QuickLogin.asp?oid="+org+"&uid="+user,"_blank","toolbar=no, status=no, width=560px, height=440px");
	}
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --> 
<br>
<% if CanAccessTaskManager = True AND TaskManagerOrg <> -1 then %>
&nbsp;&nbsp; To open TASKmanager click on the following link :  
<a href="javascript:openwindow(<%=TaskManagerOrg%>, <%=TaskManagerUserID%>)" class="RSLBlack"><strong><font color=#0099CC>TASK</font><font color=#0066CC>manager</font></strong></a>
<% else %>
&nbsp;&nbsp; You do not have access to the <strong><font color=#0099CC>TASK</font><font color=#0066CC>manager</font></strong> system. Please contact your company RSLmanager representative.
<% end if %>


<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

