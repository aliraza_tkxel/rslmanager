﻿<%@  language="VBScript" %>
<%Response.Buffer = False%>

<!--#include virtual="ACCESSCHECK.asp" -->
<%      if(Request.QueryString("txtSearch")="") then
                txtSearch=""
            else
                txtSearch=Request.QueryString("txtSearch")
                strFilterPg="txtSearch="+txtSearch+"&"
            end if        
            if(Request.QueryString("ddlYear")="") then
                ddlYear=0
            else
                ddlYear=Request.QueryString("ddlYear")
                strFilterPg=strFilterPg+"ddlYear=" + ddlYear+"&"
            end if
            'Setting Default Value for Month
            if(Request.QueryString("ddlMonth")="") then
                ddlMonth=0
            else
                ddlMonth=Request.QueryString("ddlMonth")
                 strFilterPg=strFilterPg+"ddlMonth=" + ddlMonth
            end if
   ' response.Write(strFilterPg)
     %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>RSL Login Audit Report</title>
    <link href="includes/AuditStyles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <script src="/js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">
	<!--
        function JumpPage() {
            iPage = document.getElementById("QuickJumpPage").value;
            if (iPage != "" && !isNaN(iPage))
                location.href = "Report_Login_Audit.asp" + "?page=" + iPage;
            else
                document.getElementById("QuickJumpPage").value = "";
        }
	-->
    </script>
</head>
<body class='TA' style="background-color: White; margin: 10 0 0 10;" onload="initSwipeMenu(3);"
    onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    
    <div class="rsl">
        <p class="Audittitle">
            <strong>Login Audit Report</strong>
        </p>
        <!--Form Starts Here-->
        <form name="frmSearch" method="get" action="Report_Login_Audit.asp">
        <div>
            <div class="aleft Audittitle label">
                Year :
            </div>
            <div class="aleft Audittitle">
                <select name="ddlYear" class="select">
                    <option value="0">All </option>
                    <% for i= 2005 to 2025
                    if(StrComp(ddlYear,i,0)=0) then
                      response.Write("<option value="&i&" selected>"&i&"</option>")
                    else
                      response.Write("<option value="&i&">"&i&"</option>")
                    end if 
                   Next %>
                </select>
            </div>
            <div class="aleft Audittitle label">
                Month :
            </div>
            <div class="aleft Audittitle">
                <select name="ddlMonth" class="select">
                    <option value='0'>All</option>
                    <% for i= 1 to 12
                    if(StrComp(ddlMonth,i,0)=0) then
                      response.Write("<option value="&i&" selected>"&MonthName(i)&"</option>")
                    else
                      response.Write("<option value="&i&">"&MonthName(i)&"</option>")
                    end if 
                   Next %>
                </select>
            </div>
            <div class="aleft Audittitle label">
                Name :
            </div>
            <div class="aleft">
                <input name="txtSearch" id="txtSearch" type="text" style="width: 150px;" value='<%= txtSearch%>' />
            </div>
            <div class="aright">
                <input name="btnSubmit" type="submit" class="submit" value="Submit" />
            </div>
            <div class="clear">
            </div>
        </div>
        </form>
        <!--Form End Here -->
        <div class="data">
            <%
            CONST CONST_PAGESIZE = 15

            Dim intPageCount	' The number of pages in the record set.
                Dim intRecordCount	' The number of records in the record set.
                Dim intPage			' The current page that we are on.
                Dim intRecord		' Counter used to iterate through the record set.
                Dim intStart		' The record that we are starting on.
                Dim intFinish		' The record that we are finishing on.
                Dim nextPage
                Dim prevPage
                Dim my_page_size
                Dim pageRecordCounter
                Dim MaxRowSpan
                Dim PageName

                MaxRowSpan = 5
                PageName = "Report_Login_Audit.asp"
                
                ' Check to see if there is value in the NAV querystring.  If there
    	        ' is, we know that the client is using the Next and/or Prev hyperlinks
	            ' to navigate the recordset.
            	If Request.QueryString("page") = "" Then
		           intpage = 1	
	            Else
		            if (IsNumeric(Request.QueryString("page"))) then
			            intpage = CInt(Request.QueryString("page"))
		            else
        		    	intpage = 1
		            end if
	            End If

            OpenDB()   
            
	  	    SQLLoginAudit = "EXECUTE SP_LOGIN_AUDIT_REPORT '" &txtSearch & "' ," & ddlYear & "," &  ddlMonth
 		 	            		
           'Open database
      	   
           Set rsAudit = Server.CreateObject("ADODB.Recordset")		   		
            rsAudit.ActiveConnection = Conn
			rsAudit.ActiveConnection.CommandTimeout = 60 	
		    rsAudit.Source = SQLLoginAudit
		    rsAudit.CursorType = 2
		    rsAudit.LockType = 1
			rsAudit.CursorLocation = 3
 		    rsAudit.open()

            rsAudit.PageSize = CONST_PAGESIZE
		    my_page_size = CONST_PAGESIZE

            ' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		    rsAudit.CacheSize = rsAudit.PageSize
		    intPageCount = rsAudit.PageCount
		    intRecordCount = rsAudit.RecordCount
            
            ' Sort pages
		    If intpage = 0 Then intpage = 1 End If
		    ' Just in case we have a bad request
		    If intpage > intPageCount Then intpage = intPageCount End If
		    If intpage < 1 Then intpage = 1 End If

		    nextPage = intpage + 1
		    If nextPage > intPageCount Then nextPage = intPageCount	End If
		    prevPage = intpage - 1
		    If prevPage <= 0 Then prevPage = 1 End If

		    ' double check to make sure that you are not before the start
		    ' or beyond end of the record set.  If you are beyond the end, set 
		    ' the current page equal to the last page of the record set.  If you are
		    ' before the start, set the current page equal to the start of the record set.
		    If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		    If CInt(intPage) <= 0 Then intPage = 1 End If

		    ' Make sure that the record set is not empty.  If it is not, then set the 
		    ' AbsolutePage property and populate the intStart and the intFinish variables.
		    If intRecordCount > 0 Then
			    rsAudit.AbsolutePage = intPage
			    intStart = rsAudit.AbsolutePosition
                    If CInt(intPage) = CInt(intPageCount) Then
				    intFinish = intRecordCount
			    Else
				    intFinish = intStart + (rsAudit.PageSize - 1)
			    End if
		    End If

           pageRecordCounter = intStart
           counter=1
           divcount=1
            if (Not rsAudit.EOF) then
            
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table">
                <tr class="head">
                    <td>
                        Date
                    </td>
                    <td>
                        Time
                    </td>
                    <td>
                        Name
                    </td>
                    <td>
                        Username
                    </td>
                    <td>
                        Logged In?
                    </td>
                </tr>
                <%
	    	    Do while  (NOT rsAudit.EOF) AND (pageRecordCounter <= intFinish) 
                %>
                <tr>
                    <td>
                        <%= rsAudit(0) %>
                    </td>
                    <td>
                        <%= rsAudit(1) %>
                    </td>
                    <td>
                        <%= rsAudit(2) %>
                    </td>
                    <td>
                        <%= rsAudit(3) %>
                    </td>
                    <td>
                        <% 
                         if(rsAudit(4)=True) then
                          response.Write("<span class='fail'>FAIL</span>")
                          if(rsAudit(5)=True) then 
                              response.Write("&nbsp;&nbsp;<img src='includes/lock.png' />")
                         'divcounter=divcount-4
                        %>
                        <!--<script type="text/javascript">
                         
                        // A $( document ).ready() block.
                        $( document ).ready(function() {
                            console.log("#div_"+<%'= divcounter%> );
    
                             $("#div_"+<%'= divcounter%>).show();
                        });
                        </script>-->
                        <%   
                           ' response.Write("&nbsp;<div id='div_'"&counter&" display><img src='includes/lock.png' /></div>")
                          '  counter=0
                          end if
                          'counter=counter+1 
                          'divcount=divcount+1
                        else
                          'counter=1
                          response.Write("Success")
                        end if
                   
                        %>
                    </td>
                </tr>
                <%
               
              'End Search  
              

              pageRecordCounter = pageRecordCounter + 1
             rsAudit.movenext
            Loop
            response.Write("<tfoot><tr><td colspan=""" & MaxRowSpan & """ style=""border-top:2px solid #133e71"" align=""center"">" &_
		            "<table cellspacing=""0"" cellpadding=""0"" width=""100%"" style=""border-width:0px""><thead><tr><td width=""100"" style=""border-width:0px""></td><td align=""center"" style=""border-width:0px"">"  &_
		            "<a href="" " & PageName & "?page=1&"&strFilterPg&""" title=""First""><b><font color=""blue"">First</font></b></a> "  &_
		            "<a href="" " & PageName & "?page=" & prevpage&"&"&strFilterPg & " "" title=""Prev""><b><font color=""blue"">Prev</font></b></a>"  &_
		            " Page " & intpage & " of " & intPageCount & ". Records: " & intStart & "  to " & intFinish &	" of " & intRecordCount   &_
		            " <a href="" " & PageName & "?page=" & nextpage&"&"&strFilterPg & " "" title=""Next""><b><font color=""blue"">Next</font></b></a>"  &_ 
		            " <a href="" " & PageName & "?page=" & intPageCount&"&"&strFilterPg & " "" title=""Last""><b><font color=""blue"">Last</font></b></a>" &_
		            "</td><td align=""right"" style=""border-width:0px"">Page:&nbsp;<input type=""text"" name=""QuickJumpPage"" id=""QuickJumpPage"" value="""" size=""2"" maxlength=""3"" class=""textbox"" style=""border:1px solid #133E71;font-size:11px"" />&nbsp;"  &_
		            "<input type=""button"" class=""RSLButtonSmall"" value=""GO"" onclick=""JumpPage()"" title=""GO"" style=""font-size:10px; cursor:pointer"">"  &_
		            "</td></tr></thead></table></td></tr></tfoot>")
        	'response.flush
		else
             response.Write("<span class='fail'>No record found</span>") 
	     end if
         CloseRs(rsAudit)
		 CloseDB()


                %>
            </table>
        </div>
        <div class="aright">
            <form name="frmXls" method="post" action="Generate_Excel_File.asp">
            <input type="hidden" name="txtSearch" value='<%=txtSearch %>' />
            <input type="hidden" name="txtYear" value='<%=ddlYear %>' />
            <input type="hidden" name="txtMonth" value='<%=ddlMonth %>' />
            <input name="" type="submit" class="export" value="Export to XLS" />
            </form>
        </div>
    </div>
    
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
