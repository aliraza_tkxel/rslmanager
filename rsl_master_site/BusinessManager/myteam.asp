<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	team_id = Request("team_id")

	If Not IsNumeric(team_id) Then
		str_data =  "<br/><br/><br/><tr><td class=""ERROR_TEXT"">Management System Error -- 'No team id supplied, please contact administrator'</td></tr>"
	ElseIf team_id = "-1" Then
		Call get_orphan_peeps()
	Else
		Call getTeams()
	End If

	Function isAbsent(int_employeeid)

		Dim str_isabsent
			str_isabsent = "No"
		Call OpenDB()
		SQL = "SELECT J.* FROM 	E__EMPLOYEE E INNER JOIN E_JOURNAL J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"AND J.ITEMNATUREID = 1 AND CURRENTITEMSTATUSID = 1 WHERE E.EMPLOYEEID = " & int_employeeid
		Call OpenRS(rsSet,SQL)
		If Not rsSet.EOF Then
			str_isabsent = "Yes"
		End If
		Call CloseDB()

		isAbsent = str_isabsent

	End Function


	Function getTeams()

		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
		strSQL =        "   DECLARE @TOIL_NATURE_ID INT	" &_
						" DECLARE @TIME_OFF_LIEU_NATURE_ID INT " &_
						"DECLARE @BHSTART SMALLDATETIME, @BHEND SMALLDATETIME " &_
						"SELECT	@BHSTART = YSTART, @BHEND = YEND  FROM  F_FISCALYEARS Order By YRANGE ASC " &_ 
						" SELECT   @TIME_OFF_LIEU_NATURE_ID=E_NATURE.ITEMNATUREID  FROM E_NATURE WHERE E_NATURE.DESCRIPTION = 'BRS Time Off in Lieu' " &_
						" SELECT  @TOIL_NATURE_ID= E_NATURE.ITEMNATUREID  FROM E_NATURE WHERE E_NATURE.DESCRIPTION = 'BRS TOIL Recorded' " &_
						" SELECT	T.TEAMNAME, E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"    ISNULL(CAST(G.DESCRIPTION AS NVARCHAR), 'N/A') AS GRADE, (J.BANKHOLIDAY +  J.CARRYFORWARD +  LEAVE.REMAININGLEAVE + (TOIL.TOIL -TIMEOFFINLIEU.TIMEOFFINLIEU )) AS REMAININGLEAVE, " &_
						"    ISNULL(CAST(J.APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
						"    CASE WHEN ISNULL(SUM(AHOL.DURATION), 0) = 0 THEN 'No' ELSE 'Yes' END AS HOLREQUEST   " &_
						"    FROM E__EMPLOYEE AS E  " &_
						"    LEFT JOIN E_JOBDETAILS AS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"    LEFT JOIN E_TEAM AS T ON T.TEAMID = J.TEAM  " &_
						"    LEFT JOIN E_GRADE AS G ON J.GRADE = G.GRADEID  " &_
						" Outer Apply   ( SELECT ISNULL(JD.HOLIDAYENTITLEMENTDAYS,0) - ISNULL(SUM(A.DURATION),0) As REMAININGLEAVE " &_
						" 						FROM 		E__EMPLOYEE EE   " &_
						" 						LEFT JOIN E_JOBDETAILS JD ON EE.EMPLOYEEID = JD.EMPLOYEEID " &_
						" 						LEFT JOIN E_JOURNAL JNAL ON JD.EMPLOYEEID = JNAL.EMPLOYEEID  AND JNAL.CURRENTITEMSTATUSID = 5 AND JNAL.ITEMNATUREID = 2   " &_
						" 						LEFT JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = ( " &_
						" 						SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID AND   " &_
						" 						A.STARTDATE BETWEEN @BHSTART AND @BHEND ) " &_
						" 						WHERE EE.EMPLOYEEID =  E.EMPLOYEEID GROUP BY JD.HOLIDAYENTITLEMENTDAYS )LEAVE " &_
						" Outer Apply (SELECT	 ISNULL(SUM(A.DURATION_HRS),0) as TOIL " &_
						" 			FROM  E_JOBDETAILS JD " &_
						" 					 INNER JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID   " &_
						" 					 INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = ( " &_
						" 					SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE WHERE JOURNALID = JNAL.JOURNALID " &_
						" 					AND A.STARTDATE BETWEEN @BHSTART AND @BHEND)  " &_
						" 			WHERE 	  JNAL.ITEMNATUREID = @TOIL_NATURE_ID And JD.EMPLOYEEID=E.EMPLOYEEID ) TOIL " &_
						" OUTER Apply (SELECT	 ISNULL(SUM(A.DURATION_HRS),0) AS TIMEOFFINLIEU FROM 	E_JOBDETAILS JD " &_
						" 					INNER JOIN E_JOURNAL JNAL ON JD.EMPLOYEEID = JNAL.EMPLOYEEID  AND JNAL.CURRENTITEMSTATUSID = 5 " &_
						" 					INNER JOIN E_ABSENCE A ON JNAL.JOURNALID = A.JOURNALID AND A.ABSENCEHISTORYID = ( " &_
						" 					SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE  WHERE JOURNALID = JNAL.JOURNALID  " &_
						" 					AND A.STARTDATE BETWEEN @BHSTART AND @BHEND)  " &_
						" 			WHERE 	JNAL.ITEMNATUREID = @TIME_OFF_LIEU_NATURE_ID AND JD.EMPLOYEEID =E.EMPLOYEEID)TIMEOFFINLIEU " &_
						" 	   LEFT JOIN E_JOURNAL AS JHOL ON ( J.EMPLOYEEID = JHOL.EMPLOYEEID AND JHOL.CURRENTITEMSTATUSID = 3 )  " &_
						" 	   LEFT JOIN E_NATURE AS N ON (JHOL.ITEMNATUREID = N.ITEMNATUREID AND N.APPROVALREQUIRED = 1) " &_
						" 	   LEFT JOIN E_ABSENCE AS AHOL ON ( JHOL.JOURNALID = AHOL.JOURNALID  " &_
						" 	   AND (AHOL.ABSENCEHISTORYID = ( SELECT MAX(ABSENCEHISTORYID) AS Expr1 FROM E_ABSENCE WHERE (JOURNALID = JHOL.JOURNALID) ) ) ) " &_
                        "    WHERE	(J.TEAM = " & team_id & ") AND  " &_
                        "            (J.ACTIVE = 1)  " &_
                        "   GROUP BY J.EMPLOYEEID, LEAVE.REMAININGLEAVE, E.FIRSTNAME, E.LASTNAME, J.JOBTITLE, J.GRADE, T.TEAMNAME, E.EMPLOYEEID, J.APPRAISALDATE, G.DESCRIPTION, " &_
			" J.BANKHOLIDAY,J.CARRYFORWARD , TOIL.TOIL  , TIMEOFFINLIEU.TIMEOFFINLIEU " &_
                        "    ORDER BY E.EMPLOYEEID "
		'Response.Write(strSQL)
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 To rsSet.PageSize

				team_name = rsSet("TEAMNAME")
				APPDATE = rsSet("APPRAISALDATE")
				If (ISDATE(APPDATE)) Then
					APPDATE = FormatDateTime(APPDATE, 2)
				End If
				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick=""load_me(" & rsSet("EMPLOYEEID") & ")"">" &_
					"<td width=""240px"">" & rsSet("FULLNAME") & "</td>" &_
					"<td width=""240px"" align=""left"">" & rsSet("JOBTITLE") & "</td>" &_
					"<td width=""45px"" align=""center"">" & rsSet("GRADE") & "</td>" &_
					"<td width=""75px"" align=""center"">" & rsSet("REMAININGLEAVE") & "</td>"
					If isAbsent(rsSet("EMPLOYEEID")) = "Yes" Then
						str_data = str_data & "<td width=""70px"" align=""center""><font color=""red"">" & isAbsent(rsSet("EMPLOYEEID")) & "</font></td>" 
					Else
						str_data = str_data & "<td width=""70px"" align=""center"">" & isAbsent(rsSet("EMPLOYEEID")) & "</td>"
					End If

					If rsSet("HOLREQUEST") = "Yes" Then
						str_data = str_data & "<td width=""70px"" align=""center""><font color=""red"">" & rsSet("HOLREQUEST") & "</font></td>" 
					Else
						str_data = str_data & "<td width=""70px"" align=""center"">" & rsSet("HOLREQUEST") & "</td>"
					End If
					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=8 STYLE='BORDER-TOP:2px solid #133e71' ALIGN=CENTER>" &_
			"<A HREF = 'MYTEAM.asp?page=1&team_id=" & team_id & "'><b><font color=BLUE>First</font></b></a> " &_
			"<A HREF = 'MYTEAM.asp?page=" & prevpage & "&team_id=" & team_id & "'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='MYTEAM.asp?page=" & nextpage & "&team_id=" & team_id & "'><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A HREF='MYTEAM.asp?page=" & intPageCount & "&team_id=" & team_id & "'><b><font color=BLUE>Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<tfoot><tr><td colspan=""8"" style=""font:18px"" align=""center"">No members exist within this team</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End Function

	Function get_orphan_peeps()

		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
		strSQL = 	"SELECT 	E.EMPLOYEEID, " &_
					"			FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
					"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, ISNULL(CAST(J.GRADE AS NVARCHAR), 'N/A') AS GRADE, " &_
					"			ISNULL(CAST( APPRAISALDATE AS NVARCHAR), 'N/A') AS APPRAISALDATE, " &_
					"			'N/A' AS PENSIONSCHEME " &_
					"FROM	 	E__EMPLOYEE E " &_
					"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
					"WHERE	 	J.TEAM IS NULL "

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount
		intRecordCount = rsSet.RecordCount

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End If
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 To rsSet.PageSize

				APPDATE = rsSet("APPRAISALDATE")
				If (ISDATE(APPDATE)) Then
					APPDATE = FormatDateTime(APPDATE, 2)
				End If
				str_data = str_data & "<tbody><tr style=""cursor:pointer"" onclick=""load_me(" & rsSet("EMPLOYEEID") & ")"">" &_
					"<td width=""240px"">" & rsSet("FULLNAME") & "</TD>" &_
					"<td width=""240px"" align=""left"">N/A</td>" &_
					"<td width=""45px"" align=""center"">N/A</td>" &_
					"<td width=""75px"" align=""center"">N/A</td>" &_
					"<td width=""90px"" align=""center"">N/A</td>" &_
					"<td width=""70px"" align=""center"">N/A</td>" &_
					"</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""8"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a href='MYTEAM.asp?page=1'><b><font color=""blue"">First</font></b></a> " &_
			"<a href='MYTEAM.asp?page=" & prevpage & "'><b><font color=""blue"">Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a href='MYTEAM.asp?page=" & nextpage & "'><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a href='MYTEAM.asp?page=" & intPageCount & "'><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<tfoot><tr><td colspan=""8"" style=""font:18px"" align=""center"">No members exist within this team</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function


	'pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""8"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

        function load_me(employee_id) {
            location.href = "newemployee.asp?employeeid=" + employee_id;
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="Establishment.asp">
                    <img src="Images/Establishment-Closed.gif" width="107" height="20" border="0" alt="Establishment" /></a>
            </td>
            <td rowspan="2">
                <img src="Images/tab_team-over.gif" width="55" height="20" border="0" alt="<%=team_name%>" />
            </td>
            <td rowspan="2">
                <img src="Images/TabEnd.gif" width="8" height="20" border="0" alt="" />
            </td>
            <td>
                <img src="images/spacer.gif" width="580" height="19" alt="" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4">
                <img src="images/spacer.gif" width="580" height="1" alt="" /></td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE"
        border="7">
        <thead>
            <tr>
                <td width="240px" class='NO-BORDER' colspan="3">
                    <b>Team:
                        <%=team_name%></b>
                </td>
                <td align="center" width="90px" class='NO-BORDER' rowspan="2" valign="bottom">
                    <b>Remaining Leave</b>
                </td>
                <td align="center" width="90px" class='NO-BORDER' rowspan="2" valign="bottom">
                    <b>Absent</b>
                </td>
            </tr>
            <tr>
                <td width="240px" class='NO-BORDER'>
                    <b>Name</b>
                </td>
                <td width="240px" align="left" class='NO-BORDER'>
                    <b>Job Title</b>
                </td>
                <td align="center" width="45px" class='NO-BORDER'>
                    <b>Grade</b>
                </td>
                <td align="center" width="70px" class='NO-BORDER'>
                    <b>Leave Request</b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="8" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" id="frm_team" width="4" height="4"
        style="display: none"></iframe>
</body>
</html>
