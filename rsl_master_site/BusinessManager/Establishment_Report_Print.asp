<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PageName = "Establishment_report.asp"
	' only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	' redirect all director to a higher level showing their teams
	'if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate
	Dim searchName, searchSQL,orderSQL,EMPSQL ,DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL,EmployeeType

	Call GetVariables()
	Call getTeams()
	Call opendb()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	Call GetDepartmentReportVars()
	Call closedb()

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()

		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")
    
		If OrderBy <> "" Then
			orderSQL =  " ORDER BY  " & OrderBy
		Else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End If
    
        If EmployeeType = "Previous" Then			
            EMPSQL =  " AND ((J.ENDDATE IS NOT NULL AND (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') <= GETDATE())) OR J.ACTIVE IN (0)) "
		ElseIf EmployeeType = "All" Then
			EMPSQL =  " "
		Else
			EMPSQL =  " AND ((J.ENDDATE IS NULL OR (CONVERT(SMALLDATETIME, J.ENDDATE+'6PM') > GETDATE())) AND J.ACTIVE = 1) "
		End If

		If Dapartment <> "" Then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End If

		If theDate <> "" Then
			whereNeeded = 1
			If deptSQL <> "" Then
			    dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End If

		If whereNeeded = 1 Then
			whereSQL = " AND " & deptSQL & dateSQL
		End If

		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate

	End Function

	Function getTeams()

		Dim strSQL, rsSet, intRecord

		intRecord = 0
		str_data = ""
		strSQL = 		" SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(J.GRADE AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE " &_
						"FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL & orderSQL


		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		If Not rsSet.EOF then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			while Not rsSet.EOF

				str_data = str_data & "<tbody><tr>" &_
					"<td width=""129px"">" & rsSet("FULLNAME") & "</td>" &_
					"<td width=""176px"" align=""left"" title='" & rsSet("JOBTITLE") & "'>" & rsSet("JOBTITLE") & "</td>" &_
					"<td width=""139px"" align=""center"">" & rsSet("DEPARTMENT") & "</td>" &_
					"<td width=""68px"" align=""center"">" & rsSet("GENDER") & "</td>"&_
					"<td width=""126px"" align=""center"">" & rsSet("ETHNICITY")  & "</td>" &_
					"<td width=""75px"" align=""center"">" & rsSet("DOB") & "</td>"&_
					"<td width=""78px"" align=""center"">" & rsSet("SALARY") & "</td>"&_
					"<td width=""65px"" align=""center"">" & rsSet("GRADE") & "</td>"&_
					"<td width=""102px"" align=""center"">" & rsSet("STARTDATE") & "</td>"&_
					"<td width=""78px"" align=""center"">" & rsSet("ENDDATE") & "</td>"

					str_data = str_data & "</tr></tbody>"

				count = count + 1
				rsSet.movenext()
				wend

			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()


		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<tfoot><tr><td colspan=""10"" style=""font:16px"" align=""center"">No records found for this date</td></tr>" &_
						"<tr style=""height:3px""><td></td></tr></tfoot>"
		End If

		rsSet.close()
		Set rsSet = Nothing

			'Next
			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records

	End function


	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""10"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function GetDepartmentReportVars()
	
				sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
						"			SUM(	CASE WHEN J.STARTDATE = '" & theDate & "'    " &_
						"				THEN 1    " &_
						"				ELSE  0    " &_
						"    		END )AS STARTERS, " &_
						"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS MALETOTAL, " &_
						"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS FEMALETOTAL, " &_
						"			SUM(	CASE WHEN J.ENDDATE = '" & theDate & "' " &_ 
						"				THEN 1    " &_ 
						"				ELSE  0   " &_
						"    		END )AS LEAVERS " &_
						"FROM	E__EMPLOYEE E   " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						"  WHERE TEAMID NOT IN (1) "  & whereSQL & EMPSQL
						
			Call OpenRs(rsLoader, sql)
                If (NOT rsLoader.EOF) Then
                '==================================================================
                '= Set Some Totals For department report
                '==================================================================
		                TotalSalary = FormatCurrency(rsLoader("SALARY"))
		                TotalStaff	= rsLoader("TOTALSTAFF")
		                maletotal	= rsLoader("MALETOTAL")
		                femaletotal	= rsLoader("FEMALETOTAL")
		                NewStarters	= rsLoader("STARTERS")
		                Leavers		= rsLoader("LEAVERS")
                '==================================================================
                '= End Totals For department report
                '==================================================================	
                End If
		Call CloseRS(rsLoader)
	End Function
	
	
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --> HR Tools --> Team</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
        .style1
        {
            color: #FF0000;
        }
    </style>
</head>
<body>
    <form name="thisForm" method="post" action="">
    <table border="0" width="900" cellpadding="2" cellspacing="2" style="height:40px">
        <tr>
            <td width="584">
                <b>Department Information :<span class="style1"> THIS PAGE IS UNDER DEVELOPMENT ( AS
                    OTHER PAGES HAVE CHANGED ASSOCIATED WITH THE DATA IN THIS REPORT ) </span></b>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <div name="DeptInfo" id="DeptInfo">
                    <table width="354" border="0" cellpadding="0" cellspacing="0" style='border: 1PX SOLID #133E71'>
                        <tr>
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td width="12">
                                &nbsp;
                            </td>
                            <td width="88">
                                <strong>Total Salary : </strong>
                            </td>
                            <td width="117">
                                <%= TotalSalary %>
                            </td>
                            <td width="94">
                                <strong>New Starters : </strong>
                            </td>
                            <td width="41">
                                <%= NewStarters %>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" height="10">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <strong>Total Staff : </strong>
                            </td>
                            <td>
                                <%= TotalStaff %>
                                ( M:
                                <%=maletotal%>
                                / f:
                                <%= femaletotal %>
                                )
                            </td>
                            <td>
                                <strong>Leavers : </strong>
                            </td>
                            <td>
                                <%= Leavers %>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" colspan="5">
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table width="1100" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img name="tab_establishment" id="tab_establishment" title='<%=team_name%>' src="Images/tab_establishmentreport.gif"
                    width="157" height="20" border="0" alt="" />
            </td>
            <!--td><img src="images/spacer.gif" width="656" height="19"></td-->
            <td align="right">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="1" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71" colspan="4">
                <img src="images/spacer.gif" width="940" height="1" alt="" />
            </td>
        </tr>
    </table>
    <table width="1100" cellpadding="1" cellspacing="2" class='TAB_TABLE' style="border-collapse: COLLAPSE;
        behavior: url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor="STEELBLUE"
        border="7">
        <thead>
            <tr align="left" valign="bottom">
                <td width="129" class='NO-BORDER'>
                    <b>Name</b>
                </td>
                <td width="176" class='NO-BORDER'>
                    <b>Job Title</b>
                </td>
                <td width="139" class='NO-BORDER'>
                    <b>Dept.</b>
                </td>
                <td width="68" class='NO-BORDER'>
                    <strong>Gender</strong>
                </td>
                <td width="126" class='NO-BORDER'>
                    <strong>Ethnicity</strong>
                </td>
                <td width="75" class='NO-BORDER'>
                    <b>DOB</b>
                </td>
                <td width="78" align="left" valign="bottom" class='NO-BORDER'>
                    <b>Salary</b>
                </td>
                <td align="left" width="65" class='NO-BORDER' valign="bottom">
                    <b>Grade</b>
                </td>
                <td width="102" class='NO-BORDER'>
                    <b>Start Date</b>
                </td>
                <td width="78" class='NO-BORDER'>
                    <b>End Date </b>
                </td>
            </tr>
            <tr style="height: 3px">
                <td height="3" colspan="12" align="center" style="border-bottom: 2px solid #133e71">
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    </form>
</body>
</html>
