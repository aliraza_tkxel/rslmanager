<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim exec_path 				'determines which way program flow will go based on value of
								'connection object			
	Dim users_exist				'tells client side whether or not patch will be deleted
	Dim error_flag, error_message

	Dim patch_id, location ' patch details gathered from client side or recordset

	exec_path = Request("path")
	patch_id = Request("patch_id")

	error_flag = false
	users_exist = false 	' yes you can delete patch

	'--------------MAIN PROGRAM EXECUTION-------------------'
	Call OpenDB()

	If exec_path = "new" Then
		new_patch()
	ElseIf exec_path = "amend" Then
		patch_id = Request.Form("hd_patch_id")
		edit_patch(patch_id)
	ElseIf exec_path = "del" Then
		del_patch(patch_id)
	Else 
		load_patch(patch_id)
	End If

	Call CloseDB()
	'--------------------END--------------------------------'

	' GATHER DATA FROM CLIENT SIDE FORM
	Function get_data()
		location = Request.Form("txt_LOCATION")
	End Function

	' CREATES NEW patch
	Function new_patch()
		Call get_data()
		conn.execute( "INSERT INTO E_PATCH (LOCATION) VALUES ('" & location & "')")
	End Function

	' DELETE patch ONLY IF NO USERS EXIST
	Function del_patch (the_id)

		Dim strSQL, cnt
			strSQL = "SELECT COUNT(*) AS EMPLOYEECOUNT FROM E_JOBDETAILS WHERE PATCH = " & the_id
		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()
				cnt = rsSet("EMPLOYEECOUNT")
			rsSet.close()
		Set rsSet  = Nothing

		' only delete if no employees are attachedto this patch
		If cnt > 0 Then
			users_exist = True
		Else
			conn.execute("DELETE E_PATCH WHERE patchID = " & the_id)
		End If

	End Function

	' UPDATE patch RECORD BASED ON VALUE IN patch_id
	Function edit_patch(the_id)

		' get form data
		Call get_data()
		Conn.execute( 	"UPDATE 	E_PATCH " &_ 
						"SET		LOCATION = '" & location & "'" &_
						"WHERE		PATCHID = " & the_id )

	End Function

	' GATHERS DATA FROM RECORDSET TO POPULATE FORM AND ALLOW EDITING ON CLIENT SIDE
	Function load_patch(the_id)

		Dim strSQL, cnt
			strSQL = "SELECT * FROM E_PATCH WHERE PATCHID = " & the_id
		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()
			If Not rsSet.EOF Then
				location = rsSet("LOCATION")
			Else
				error_flag = true
				error_message = "This patch no longer exists in the database :: Contact administrator "
			End If
		    rsSet.close()
		Set rsSet  = Nothing

	End Function
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Main Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript" defer="defer">

        // RETURN UPDATED TABLE TO CLIENT PAGE
        function returnData() {
            if ("<%=exec_path%>" == "load")
                load_patch();
            else if ("<%=exec_path%>" == "del")
                del_patch();
            else
                parent.location.replace("../patch.asp");
        }

        function load_patch() {
            parent.swap_button(1);
            parent.swap_div(3);
            parent.document.getElementById("txt_LOCATION").value = "<%=location%>";
            parent.document.getElementById("hd_patch_id").value = "<%=patch_id%>";
            parent.checkForm()
        }

        function del_patch() {
            if ("<%=users_exist%>" == "True")
                alert("This patch may not be deleted as it contains patch members");
            else
                parent.location.replace("../patch.asp");
        }

    </script>
</head>
<body onload="returnData()">
</body>
</html>
