<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim exec_path 				'determines which way program flow will go based on value of
								'connection object
	Dim users_exist				'tells client side whether or not patch will be deleted
	Dim error_flag, error_message

	Dim JobTitle_id, JobTitle ' patch details gathered from client side or recordset

	exec_path = Request("path")
	JobTitle_id = Request("JobTitle_id")

	error_flag = false
	users_exist = false 	' yes you can delete patch

	'--------------MAIN PROGRAM EXECUTION--------------------------------------'
	Call OpenDB()

	If exec_path = "new" Then 
		Call new_JobTitle()
	ElseIf exec_path = "amend" Then
		JobTitle_id = Request.Form("hd_JobTitle_id")
		'JobTitle=Request.Form("txt_JobTitle")
		edit_JobTitle(JobTitle_id)
	ElseIf exec_path = "del" Then
		Call del_JobTitle(JobTitle_id)
	Else
		Call load_JobTitle(JobTitle_id)
	End If

	Call CloseDB()
	'--------------------END--------------------------------'

	' GATHER DATA FROM CLIENT SIDE FORM
	Function get_data()	
		JobTitle = Request.Form("txt_JobTitle")
	End Function

	' CREATES NEW JobTitle
	Function new_JobTitle()
		Call get_data()
		conn.execute( "INSERT INTO E_JOBTITLES (JOBTITLE) VALUES ('" & JobTitle & "')")
	End Function

	' DELETE JobTitle ONLY IF NO USERS EXIST
	Function del_JobTitle (the_id)

		Dim strSQL, cnt
		strSQL =" SELECT COUNT(E.JOBTITLE) AS EMPLOYEECOUNT " &_
				" FROM E_JOBTITLES J "&_
				"    LEFT JOIN E_JOBDETAILS E ON E.JOBTITLE=J.JOBTITLE "&_
				" WHERE J.JOBTITLEID=" & the_id

		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()

			cnt = rsSet("EMPLOYEECOUNT")

			rsSet.close()
		Set rsSet  = Nothing

		' only delete if no employees are attachedto this patch
		If cnt > 0 Then
			users_exist = True
		Else
			conn.execute("DELETE E_JOBTITLES WHERE JOBTITLEID = " & the_id)
		End If

	End Function

	' UPDATE Job Title RECORD BASED ON VALUE IN JobTitle_id
	Function edit_JobTitle(the_id)
		Dim UPDATESQL
		' get form data
		Call get_data()
		UPDATESQL = " UPDATE E_JOBDETAILS "&_
					"	SET JOBTITLE='" & JobTitle & "'" &_
                    "	,LASTACTIONUSER=" & Session("UserID") & " " &_
                    "	,LASTACTIONTIME = GETDATE() " &_
					" WHERE JOBTITLE=(SELECT JOBTITLE FROM E_JOBTITLES WHERE JOBTITLEID = " & the_id & "); "&_
					" UPDATE E_JOBTITLES "&_
					"   SET	JOBTITLE = '" & JobTitle & "'" &_
					" WHERE	JOBTITLEID = " & the_id
		Conn.execute(UPDATESQL)

	End Function

	' GATHERS DATA FROM RECORDSET TO POPULATE FORM AND ALLOW EDITING ON CLIENT SIDE
	Function load_JobTitle(the_id)

		Dim strSQL, cnt
			strSQL = "SELECT JOBTITLEID,JOBTITLE FROM E_JOBTITLES WHERE JOBTITLEID = " & the_id
		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()
				If Not rsSet.EOF Then
					JobTitle = rsSet("JOBTITLE")
				Else
					error_flag = true
					error_message = "This Job Title no longer exists in the database :: Contact administrator "
				End If
		rsSet.close()
		Set rsSet  = Nothing

	End Function
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Main Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript" defer="defer">

        // RETURN UPDATED TABLE TO CLIENT PAGE
        function returnData() {
            if ("<%=exec_path%>" == "load")
                load_JobTitle();
            else if ("<%=exec_path%>" == "del")
                del_JobTitle();
            else
                parent.location.replace("../JobTitle.asp");
        }

        function load_JobTitle() {
            parent.swap_button(1);
            parent.swap_div(3);
            parent.document.getElementById("txt_JobTitle").value = "<%=JobTitle%>";
            parent.document.getElementById("hd_JobTitle_id").value = "<%=JobTitle_id%>";
            parent.checkForm()
        }

        function del_JobTitle() {
            if ("<%=users_exist%>" == "True")
                alert("This Job Title may not be deleted as it contains members");
            else
                parent.location.replace("../JobTitle.asp");
        }

    </script>
</head>
<body onload="returnData()">
</body>
</html>