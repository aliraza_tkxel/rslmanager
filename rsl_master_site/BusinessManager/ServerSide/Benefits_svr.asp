<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (48)
Dim DataTypes    (48)
Dim ElementTypes (48)
Dim FormValues   (48)
Dim FormFields   (48)
UpdateID	  = "hid_EmployeeID"
' BENEFITS
' FormFields(0) = "sel_BENEFITTYPE|SELECT"
' PENSION
FormFields(0) = "txt_MEMNUMBER|TEXT"
FormFields(1) = "sel_SCHEME|SELECT"
FormFields(2) = "txt_SALARYPERCENT|INTEGER"
FormFields(3) = "txt_EMPLOYEECONTRIBUTION|CURRENCY"
FormFields(4) = "txt_AVC|CURRENCY"
FormFields(5) = "rdo_CONTRACTEDOUT|INTEGER"
' Company Car
FormFields(6) = "txt_CARMAKE|TEXT"
FormFields(7) = "txt_MODEL|TEXT"
FormFields(8) = "txt_LISTPRICE|CURRENCY"
FormFields(9) = "txt_CONTHIRECHARGE|CURRENCY"
FormFields(10) = "txt_EMPCONTRIBUTION|CURRENCY"
FormFields(11) = "txt_EXCESSCONTRIBUTION|CURRENCY"
' GENERAL
FormFields(12) = "txt_PROFESSIONALFEES|CURRENCY"
FormFields(13) = "txt_TELEPHONEALLOWANCE|CURRENCY"
FormFields(14) = "txt_FIRSTAIDALLOWANCE|CURRENCY"
FormFields(15) = "txt_CALLOUTALLOWANCE|CURRENCY"
' Car Essential User
FormFields(16) = "txt_CARALLOWANCE|CURRENCY"
FormFields(17) = "txt_CARLOANSTARTDATE|TEXT"
FormFields(18) = "txt_ENGINESIZE|CURRENCY"
FormFields(19) = "rdo_LOANINFORMATION|INTEGER"
' ONLY IF YES TO LOAN INFORMATION
FormFields(20) = "txt_VALUE|CURRENCY"
FormFields(21) = "txt_TERM|INTEGER"
FormFields(22) = "txt_MONTHLYREPAY|CURRENCY"
' Accomodation
FormFields(23) = "txt_ACCOMODATIONRENT|CURRENCY"
FormFields(24) = "txt_COUNCILTAX|CURRENCY"
FormFields(25) = "txt_HEATING|CURRENCY"
FormFields(26) = "txt_LINERENTAL|CURRENCY"
' BUPA
FormFields(27) = "sel_MEDICALORGID|INTEGER"
FormFields(28) = "txt_TAXABLEBENEFIT|CURRENCY"
FormFields(29) = "txt_ANNUALPREMIUM|CURRENCY"
FormFields(30) = "txt_ADDITIONALMEMBERS|TEXT"
FormFields(31) = "txt_ADDITIONALMEMBERS2|TEXT"
FormFields(32) = "txt_ADDITIONALMEMBERS3|TEXT"
FormFields(33) = "txt_ADDITIONALMEMBERS4|TEXT"
FormFields(34) = "txt_ADDITIONALMEMBERS5|TEXT"
'FormFields(35) = "hid_EMPLOYEEID|NUMBER"
' NEW ADDITIONAL FIELDS AFTER 5TH OCT 2004
' PENSION
FormFields(35) = "txt_SCHEMENUMBER|TEXT"
FormFields(36) = "txt_EMPLOYERCONTRIBUTION|CURRENCY"
' COMPANY CAR
FormFields(37) = "txt_CARSTARTDATE|DATE"
FormFields(38) = "txt_CO2EMISSIONS|FLOAT"
FormFields(39) = "txt_FUEL|TEXT"
FormFields(40) = "txt_COMPEMPCONTRIBUTION|CURRENCY"
FormFields(41) = "txt_DRIVINGLICENCENO|TEXT"
FormFields(42) = "txt_MOTCERTNO|TXT"
FormFields(43) = "txt_INSURANCENO|TEXT"
FormFields(44) = "txt_INSURANCERENEWALDATE|DATE"
' NEW ADDITIONAL FIELDS AFTER 9TH JAN 2007
FormFields(45) = "txt_GROUPSCHEMEREF|TEXT"
FormFields(46) = "txt_MEMBERSHIPNO|TEXT"

' WE NEED EMPLOYEE ID 
FormFields(47) = "txt_EmployeeID|INTEGER"
FormFields(48) = "hid_LASTACTIONUSER|INTEGER"

Function LoadRecord()

	ID = Request.Form(UpdateID)
	strSQL = "SELECT * FROM E_BENEFITS WHERE EMPLOYEEID = " & ID

	' Get values for pension
	txt_MEMNUMBER
	sel_SCHEME
	txt_SCHEMENUMBER
	txt_SALARYPERCENT
	txt_EMPLOYEECONTRIBUTION
	txt_EMPLOYERCONTRIBUTION
	txt_AVC
	rdo_CONTRACTEDOUT
	' Get values for
	txt_CARMAKE
	txt_MODEL
	txt_LISTPRICE
	txt_CONTHIRECHARGE
	txt_EMPCONTRIBUTION
	txt_EXCESSCONTRIBUTION
	txt_CARSTARTDATE
	txt_CO2EMISSIONS
	sel_FUEL
	txt_COMPEMPCONTRIBUTION
	' Get values for
	txt_PROFESSIONALFEES
	txt_TELEPHONEALLOWANCE
	txt_FIRSTAIDALLOWANCE
	txt_CALLOUTALLOWANCE
	' Get values for
	txt_CARALLOWANCE
	txt_CARLOANSTARTDATE
	txt_ENGINESIZE
	rdo_LOANINFORMATION
	' Get values for
	txt_VALUE
	txt_TERM
	txt_MONTHLYREPAY
	' Get values for
	txt_ACCOMODATIONRENT
	txt_COUNCILTAX
	txt_HEATING
	txt_LINERENTAL
	' Get values for
	sel_MEDICALORGID
	txt_TAXABLEBENEFIT
	txt_ANNUALPREMIUM
	txt_ADDITIONALMEMBERS
	txt_ADDITIONALMEMBERS2
	txt_ADDITIONALMEMBERS3
	txt_ADDITIONALMEMBERS4
	txt_ADDITIONALMEMBERS5
	txt_GROUPSCHEMEREF
	txt_MEMBERSHIPNO

	txt_EmployeeID

'	GO()

End Function

Function NewRecord()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO E_BENEFITS " & strSQL & ""
	Conn.Execute SQL, recaffected
'	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
	Call MakeUpdate(strSQL)	
	SQL = "UPDATE E_BENEFITS " & strSQL & ",LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
'	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)
'	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
'	Conn.Execute SQL, recaffected
End Function

Function GO()
	'CloseDB()
	'Response.Redirect "../NewEmployee.asp?EmployeeID=" & ID
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "SAVE"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
Call CloseDB()
%>