<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/Functions/FormCollection.asp" -->
<%
	Dim strTable, lst_dis, lst_diff, TypeId

	Function LoadPreviousData()

	Dim UpdateID, RsID
		UpdateID = "hid_EMPLOYEEID"

		RsID = Request.Form(UpdateID)
		SQL = "SELECT DisDESC = CASE " &_
				"WHEN LEN(DESCRIPTION) " &_
				"< 30 THEN DESCRIPTION ELSE SUBSTRING(DESCRIPTION, 1, 30) + '...' " &_
				"END " &_
		", E_DIFFDIS.*, E_DIFFDISID.* FROM E_DIFFDIS INNER JOIN E_DIFFDISID ON E_DIFFDIS.DIFFDISID = E_DIFFDISID.DIFFDIS WHERE EMPLOYEEID = " & RsID & " ORDER BY DESCRIPTION"

		Call OpenRs(rsLoader, SQL)

		strTable = "<table width='220' cellspacing='0' cellpadding='1'>"
			strTable = strTable & "<thead>"
				strTable = strTable & "<tr>"
					strTable = strTable & "<td width='200' colspan='2'><b>Disability/Learning Difficulty</b></td>"
				strTable = strTable & "</tr>"
				strTable = strTable & "<tr>"
					strTable = strTable & "<td colspan='2'>"
					strTable = strTable & "<hr style='border:1px dotted #133e71'></td>"
				strTable = strTable & "</tr>"
		strTable = strTable & "</thead>"
		strTable = strTable & "<tbody>"
		
		If (Not rsLoader.EOF) Then
			While (NOT rsLoader.EOF)
				txtTitle = rsLoader("NOTES")
				If (txtTitle = "" OR isNull(txtTitle)) Then
					txtTitle = "No additional notes."
				End If
				strTable = strTable & "<tr>"
					strTable = strTable & "<td width='200' valign='top' style='cursor:pointer' title='" & txtTitle & "'>" & rsLoader("DisDESC") & "</td>"	
					strTable = strTable & "<td width='20' class='del' style='cursor: pointer' onClick='DeleteMe(" & rsLoader("DISABILITYID") & ")' title='[DELETE] " & rsLoader("DESCRIPTION") & "'>DEL</td>"
				strTable = strTable & "</tr>"
				rsLoader.moveNext
			Wend
		Else
				strTable = strTable & "<tr>"
					strTable = strTable & "<td colspan='2' align='center'>No Disability/Learning Difficulty</td>"
				strTable = strTable & "</tr>"
		End If
			strTable = strTable & "</tbody>"
			strTable = strTable & "<tfoot>"
				strTable = strTable & "<tr>"
					strTable = strTable & "<td colspan='2'>"
					strTable = strTable & "<hr style='border:1px dotted #133e71'/></td>"
				strTable = strTable & "</tr>"
			strTable = strTable & "</tfoot>"
		strTable = strTable & "</table>"

	'Unique Col = DISABILITYID

	Call CloseRs(rsLoader)

	'NEXT RELOAD THE SELECT LIST BOXES FOR EACH ITEM
	Call BuildSelect(lst_dis, "sel_DISABILITY", "E_DIFFDIS WHERE DTYPE = 1", "DIFFDISID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)
	Call BuildSelect(lst_diff, "sel_DIFFICULTY", "E_DIFFDIS WHERE DTYPE = 2", "DIFFDISID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", NULL)

	End Function

	Function del_Dis (RsID)
		RsID = Request.Form(UpdateID)
		conn.execute("DELETE E_DIFFDISID WHERE EMPLOYEEID = " & RsID)
	End Function

	Function DeleteRecord ()
		RsID = Request.Form("hid_DisType")
        SQL = "UPDATE E_DIFFDISID SET LASTACTIONUSER = " & Session("UserId") & ", LASTACTIONTIME = GETDATE() WHERE DISABILITYID = " & RsID & " "
        SQL = SQL & " DELETE E_DIFFDISID WHERE DISABILITYID = " & RsID
		conn.Execute(SQL)
		Call LoadPreviousData()
	End Function

	Function AddItem()
		TypeId = Request.Form("hid_DisType")
		If (NOT ISNULL(TypeId)) Then
			TypeId = CStr(TypeId)
		Else
			TypeId = -1
		End If
		if (TypeId = "1") Then
			DifDis = Request.Form("sel_DISABILITY")
			DifDistxt = Replace(Request.Form("txt_DISABILITYTEXT"),"'", "''")
		ElseIf (TypeId = "2") Then
			DifDis = Request.Form("sel_DIFFICULTY")
			DifDistxt = Replace(Request.Form("txt_DIFFICULTYTEXT"),"'", "''")
		End If
		
		Conn.Execute "INSERT INTO E_DIFFDISID (EMPLOYEEID, DIFFDIS, NOTES, LASTACTIONUSER, LASTACTIONTIME) VALUES (" & Request.Form("hid_EMPLOYEEID") & ", " & DifDis & ",'" & DifDisTxt & "'," & Session("UserID") & ",GETDATE())", recaffected
		Call LoadPreviousData()
	End Function

	Dim TheAction
		TheAction = Request("hid_Action_DiffDis")

	Call OpenDB()
	Select Case TheAction
		Case "ADD"		AddItem()
		Case "DELETE"   DeleteRecord()
	End Select
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager > Business Module > Employee > Disabilities</title>
    <script type="text/javascript" language="JavaScript">
    function ReturnData(){
	    parent.document.getElementById("ListRows").innerHTML = document.getElementById("ListRows").innerHTML;
    <% if (TypeId = "1") then %>
	    parent.document.getElementById("dvDISABILITY").innerHTML = document.getElementById("dvDISABILITY").innerHTML;
	    parent.document.getElementById("txt_DISABILITYTEXT").value = "";
        parent.document.getElementById("grpAdd").disabled = false ;
        parent.document.getElementById("grpAdd2").disabled = false ;
        parent.document.getElementById("myButton2").disabled = false ;
        parent.document.getElementById("myButtonReset2").disabled = false ;
    <% elseif (TypeId = "2") then %>
	    parent.document.getElementById("dvDIFFICULTY").innerHTML = document.getElementById("dvDIFFICULTY").innerHTML;
	    parent.document.getElementById("txt_DIFFICULTYTEXT").value = "";
        parent.document.getElementById("grpAdd").disabled = false ;
        parent.document.getElementById("grpAdd2").disabled = false ;
        parent.document.getElementById("myButton2").disabled = false ;
        parent.document.getElementById("myButtonReset2").disabled = false ;
    <% end if %>
	    }
    </script>
</head>
<body onload="ReturnData()">
    <div id="ListRows">
        <%=strTable%></div>
    <div id="dvDISABILITY">
        <%=lst_dis%></div>
    <div id="dvDIFFICULTY">
        <%=lst_diff%></div>
</body>
</html>