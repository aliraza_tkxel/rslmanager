<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (10)
Dim DataTypes    (10)
Dim ElementTypes (10)
Dim FormValues   (10)
Dim FormFields   (10)
UpdateID	  = "hid_EmployeeID"
FormFields(0) = "txt_FNAME|TEXT|"
FormFields(1) = "txt_SNAME|TEXT|"
FormFields(2) = "txt_HOUSENUMBER|TEXT"
FormFields(3) = "txt_STREET|TEXT"
FormFields(4) = "txt_POSTALTOWN|TEXT"
FormFields(5) = "txt_COUNTY|TEXT"
FormFields(6) = "txt_POSTCODE|TEXT"
FormFields(7) = "txt_HOMETEL|TEXT"
FormFields(8) = "txt_MOBILE|TEXT"
FormFields(9) = "hid_EMPLOYEEID|NUMBER"
FormFields(10) = "hid_LASTACTIONUSER|NUMBER"

Function NewRecord ()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)
	SQL = "INSERT INTO E_NEXTOFKIN " & strSQL & ""
	Conn.Execute SQL, recaffected
	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
	Call MakeUpdate(strSQL)
	SQL = "UPDATE E_NEXTOFKIN " & strSQL & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
End Function

Function GO()
	Call CloseDB()
	Response.Redirect "../JobDetails.asp?EmployeeID=" & ID
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
'CloseDB()
%>