<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim strTable
Dim strfrmflds
Dim DataFields   (4)
Dim DataTypes    (4)
Dim ElementTypes (4)
Dim FormValues   (4)
Dim FormFields   (4)
UpdateID	  = "hid_EMPLOYEEID"
UpdateID2     = "hid_PREVEMPID"
FormFields(0) = "txt_STARTDATE|DATE"
FormFields(1) = "txt_ENDDATE|DATE"
FormFields(2) = "txt_EMPLOYER|TEXT"
FormFields(3) = "txt_REASON|TEXT"
FormFields(4) = "hid_EMPLOYEEID|INT"

Function NewRecord ()
	
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO E_PREVIOUSEMPLOYMENT " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE E_PREVIOUSEMPLOYMENT " & strSQL & " WHERE PREVIOUSEMPLOYMENTID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO()
End Function

Function DeleteRecord()
	ID = Request.Form("hid_PREVEMPID")	
	
	SQL = "DELETE FROM E_PREVIOUSEMPLOYMENT WHERE PREVIOUSEMPLOYMENTID = " & ID
	Conn.Execute SQL, recaffected

	GO()	
End Function

Function GO()
	CloseDB()
	LoadPreviousData()
End Function

Function LoadPreviousData()
ID = Request.Form(UpdateID)
OpenDB()
SQL = "SELECT * FROM E_PREVIOUSEMPLOYMENT WHERE EMPLOYEEID = " & ID
Call OpenRs(rsLoader, SQL)

strTable = "<table cellspacing=0 cellpadding=1 style='behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE'><thead><tr><td width=190px><b>Employer</b></td><td width=80px><b>Start Date</b></td><td width=80px><b>End Date</b></td></tr><tr><td colspan=4><hr style='border:1px dotted #133E71'></td></td></thead><tbody>"
if (not rsLoader.EOF) then
	while (NOT rsLoader.EOF) 
		special = rsLoader("Reason")
		titlebit = ""
		if (special <> "" AND not isNull(special)) then
			titlebit = " title='Reasons for Leaving:" & special & "'"
		end if
		strTable = strTable & "<tr style='cursor:hand'><td onclick='EditMe(" & rsLoader("PREVIOUSEMPLOYMENTID") & ")' valign=top " & titlebit & ">" & rsLoader("Employer") & "</td>"	
		strTable = strTable & "<td valign=top>" & rsLoader("StartDate") & "</td>"	
		strTable = strTable & "<td valign=top>" & rsLoader("EndDate") & "</td><TD onclick='DeleteMe(" & rsLoader("PREVIOUSEMPLOYMENTID") & ")'><font color=red>DEL</font></TD></tr>"
		rsLoader.moveNext					
	wend
else
	strTable = strTable & "<tr><td colspan=3 align=center>No Previous Employers</td></tr>"
end if
strTable = strTable & "</tbody><tfoot><tr><td colspan=4><hr style='border:1px dotted #133e71'></td></tr></tfoot></table>"

CloseRs(rsLoader)
End Function


Function LoadRecord()
	ID = Request.Form("hid_PREVEMPID")
	SQL = "SELECT * FROM E_PREVIOUSEMPLOYMENT WHERE PREVIOUSEMPLOYMENTID = " & ID
	Response.Write SQL 
	Call OpenRs(rsLoaded, SQL)	
	strfrmflds = ""
	For i = 0 To UBound(FormFields)
		FormSplit = Split(FormFields(i), "|")
		FormSecondSplit = Split(FormSplit(0), "_")
		DataFields(i) = FormSecondSplit(1)
		strfrmflds = strfrmflds & "parent.RSLFORM." & FormSplit(0) & ".value = '" & rsLoaded(DataFields(i)) & "';" & vbNewLine
	Next
	strfrmflds = strfrmflds & "parent.RSLFORM." & UpdateID2 & ".value = '" & ID & "';" & vbNewLine
	strfrmflds = strfrmflds & "parent.RSLFORM.hid_Action.value = 'AMEND';" & vbNewLine
	strfrmflds = strfrmflds & "parent.RSLFORM.myButton.value = 'AMEND';" & vbNewLine
	strfrmflds = strfrmflds
	GO()	
End Function


TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	parent.ListRows.innerHTML = DataDiv.innerHTML;
	parent.ResetForm();
	<%= strfrmflds %>
	}
</script>
<body onload="ReturnData()">
	<div id=DataDiv><%=strTable%></div>
</body>
</html>