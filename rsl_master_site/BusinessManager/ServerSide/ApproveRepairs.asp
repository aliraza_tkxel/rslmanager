<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()
For each JOURNALID in Request.Form("Approve")

	//to begin update the main journal table as follows:
	strSQL = 	"UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & 2 & " WHERE JOURNALID = " & JOURNALID		
	Conn.Execute(strSQL)

	//get the last repair journal details as follows 
	SQL = "SELECT TOP 1 JOURNALID, ITEMDETAILID, TITLE, CONTRACTORID FROM C_REPAIR WHERE JOURNALID = " & JOURNALID & " ORDER BY REPAIRHISTORYID DESC"
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then
		itemdetail = rsLoader("ITEMDETAILID")
		contractorid = rsLoader("CONTRACTORID")
		entrytitle = rsLoader("TITLE")
	end if
	CloseRs(rsLoader)
	
	//next add new line to the repair journal saying that the repair has been authorised and is now assigned to a contractor
	strSQL = 	"INSERT INTO C_REPAIR " &_
				"(JOURNALID, ITEMSTATUSID, ITEMDETAILID, ITEMACTIONID, LASTACTIONUSER, CONTRACTORID, TITLE, NOTES) " &_
				"VALUES (" & JOURNALID & ", " & 2 & ", " & itemdetail & ", " & 2 & ", " & Session("USERID") & ", " & contractorid & ", '" & entrytitle & "', 'APPROVED BY " & SESSION("FIRSTNAME") & " " & SESSION("LASTNAME") & "')"
	Conn.Execute(strSQL)


	//THIS SECTION WILL UPDATE THE PREVIOUSLY CREATED PURCHASE ORDER, 
	'WE NEED TO GET SOME REPAIR DATA TO update purchase order WHICH HAPPENS NEXT
	str_sql = "SELECT ID.DESCRIPTION, ISNULL(COST,0) AS COST, ESTTIME_SEC FROM R_ITEMDETAIL ID LEFT JOIN R_PRIORITY PR ON ID.PRIORITY = PR.PRIORITYID WHERE ID.ITEMDETAILID = " & itemdetail
	Call OpenRs(rsSet, str_sql)
	If not rsSet.EOF Then
		EstimatedSeconds = rsSet("ESTTIME_SEC")
	else	
		EstimatedSeconds = ""
	end if
	CloseRs(rsSet)
		
	'GET THE CURRENT DATE FOR USE IN THE PURCHASE ORDER CREATION
	DateStamp = Now
	
	'GET THE ESTIMATED WORK COMPLETION DATE
	if (EstimatedSeconds = "") then
		DelDate = "NULL"
	else
		DelDate = DateAdd("s", EstimatedSeconds, DateStamp)
		DelDate = "'" & FormatDateTime(DelDate,1) & " " & FormatDateTime(DelDate, 3) & "'"
	end if
	
	'FORMAT THE CURRENT DATE WITH A TIME COMPONENT
	DateStamp = FormatDateTime(DateStamp,1) & " " & FormatDateTime(DateStamp, 3)

	SQL = "SELECT * FROM C_REPAIRPURCHASE WHERE JOURNALID = " & journalid
	Call OpenRs(rsExists, SQL)	
	ORDERID = rsExists("ORDERID")
	CloseRS(rsExists)
	
	SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 1, PODATE = '" & DateStamp & "', EXPDELDATE = " & DelDate & " WHERE ORDERID = " & ORDERID
	Conn.Execute (SQL)
	//END OF PURCHASE ORDER UPDATES

next
CloseDB()
Response.Redirect "../QueuedRepairs.asp"

%>