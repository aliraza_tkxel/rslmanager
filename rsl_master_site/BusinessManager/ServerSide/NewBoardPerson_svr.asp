<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%

	EmployeeID	  	= rq("hid_EmployeeID")
	Title 			= rq("sel_TITLE")
	FirstName 		= rq("txt_FIRSTNAME")
	LastName 		= rq("txt_LASTNAME")
	Address1 		= rq("txt_ADDRESS1")
	Address2 		= rq("txt_ADDRESS2")
	town 			= rq("txt_TOWN")
	County 			= rq("txt_COUNTY")
	PostCode 		= rq("txt_POSTCODE")
	OfficeTel 		= rq("txt_OFFICETEL")
	HomeTel 		= rq("txt_HOMETEL")
	Mobile 			= rq("txt_MOBILE")
	Fax 			= rq("txt_FAX")
	WorkEmail 	= rq("txt_OFFICEEMAIL")
	HomeEmail 		= rq("txt_HOMEEMAIL")
	JobTitle 		= rq("txt_POSITION")
	DateCommenced 	= rq("txt_DATECOMMENCED")
	EmploymentStatus= rq("sel_EMPLOYMENTSTATUS")

Function NewRecord ()
	
		SQL = "SET NOCOUNT ON INSERT INTO E__EMPLOYEE  (TITLE,FIRSTNAME,LASTNAME) VALUES (" &  Title & ",'" &  FirstName & "', " &"'" &  LastName & "') SELECT SCOPE_IDENTITY() AS NewID SET NOCOUNT OFF"
		rw sql & "<BR><BR><BR>"
		Call OpenRs (rsEmployee, SQL)
			EmployeeID = rsEmployee("NewID")
		Call CloseRs (rsEmployee)
		
		' UPDATE JOB TITLE
		SQL = "INSERT INTO E_JOBDETAILS (TEAM,EMPLOYEEID,JOBTITLE,STARTDATE,ACTIVE) VALUES (88," & EmployeeId & ",'" & JobTitle & "','" & DateCommenced & "'," & EmploymentStatus & " )"
		rw sql & "<BR><BR><BR>"
		Conn.Execute SQL
		
		' UPDATE OTHER CONTACT DETAILS
		SQL = "INSERT INTO  E_CONTACT (ADDRESS1,ADDRESS2,POSTALTOWN,COUNTY,POSTCODE,HOMETEL,MOBILE,WORKDD,HOMEEMAIL,WORKEMAIL,EMPLOYEEID) " &_
				"values( '" &  Address1 & "', '" &  Address2 & "','" &  Town & "','" &  County & "','" & PostCode  & "', '" &  HomeTel & "', " &_
				" '" &  Mobile & "',  '" &  OfficeTel & "', '" &  HomeEmail & "', '" &  WorkEmail & "'," & EmployeeId & ")"
		rw sql & "<BR><BR><BR>"
		Conn.Execute SQL
		GO()
	
End Function

Function AmendRecord()

			' UPDATE THEN NAMES
		SQL = "UPDATE E__EMPLOYEE  SET 	FIRSTNAME = '" &  FirstName & "', " &_
				"						LASTNAME= '" &  LastName & "' " &_
				" WHERE EMPLOYEEID = " & EmployeeId
		rw sql & "<BR><BR><BR>"
		Conn.Execute SQL

		' UPDATE JOB TITLE
		SQL = "UPDATE E_JOBDETAILS SET ACTIVE = " & EmploymentStatus & " ,JOBTITLE = '" & JobTitle & "',STARTDATE = '" & DateCommenced & "'  WHERE EMPLOYEEID = " & EmployeeId
		rw sql & "<BR><BR><BR>"
		
		Conn.Execute SQL
		
		' UPDATE OTHER CONTACT DETAILS
		SQL = "UPDATE E_CONTACT SET ADDRESS1 = '" &  Address1 & "', " &_
				"					ADDRESS2 = '" &  Address2 & "', " &_
				"					POSTALTOWN= '" &  Town & "', " &_
				"					COUNTY= '" &  County & "', " &_
				"					POSTCODE= '" & PostCode  & "', " &_
				"					HOMETEL='" &  HometEL & "', " &_
				"					MOBILE= '" &  Mobile & "', " &_
				"					WORKDD= '" &  OfficeTel & "', " &_
				"					HOMEEMAIL= '" &  HomeEmail & "', " &_
				"					WORKEMAIL = '" &  WorkEmail & "'"&_
				" WHERE EMPLOYEEID = " & EmployeeId
		
		Conn.Execute SQL
			rw sql & "<BR><BR><BR>"
	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)	
	
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
End Function

Function GO()
	CloseDB()
	Response.Redirect "../nEWbOARDpERSON.asp?EmployeeID=" & EmployeeId
End Function

TheAction = Request("hid_Action")

OpenDB()

Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>