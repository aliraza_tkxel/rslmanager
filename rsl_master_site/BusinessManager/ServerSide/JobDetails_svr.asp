<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID, bnk_hd, total_leave

Dim DataFields   (41)
Dim DataTypes    (41)
Dim ElementTypes (41)
Dim FormValues   (41)
Dim FormFields   (41)
UserId= Session("UserId")
   ' UserId="615"
UpdateID	  = "hid_EmployeeID"
FormFields(0) = "txt_STARTDATE|DATE"
FormFields(1) = "txt_ENDDATE|DATE"
FormFields(2) = "sel_PROBATIONPERIOD|NUMBER"
FormFields(3) = "txt_REVIEWDATE|DATE"
FormFields(4) = "hdn_JOBTITLE|TEXT"
FormFields(5) = "txt_REFERENCENUMBER|TEXT"
FormFields(6) = "txt_DETAILSOFROLE|TEXT"
FormFields(7) = "sel_TEAM|NUMBER"
FormFields(8) = "sel_PATCH|NUMBER"
FormFields(9) = "sel_LINEMANAGER|NUMBER"
FormFields(10) = "sel_PARTFULLTIME|NUMBER"
FormFields(11) = "txt_HOURS|FLOAT"
FormFields(12) = "sel_GRADE|TEXT"

FormFields(13) = "txt_SALARY|FLOAT"
FormFields(14) = "txt_TAXOFFICE|TEXT"
FormFields(15) = "txt_TAXCODE|TEXT"
FormFields(16) = "txt_PAYROLLNUMBER|TEXT"
FormFields(17) = "txt_DRIVINGLICENSENUMBER|TEXT"
FormFields(18) = "txt_NINUMBER|TEXT"
FormFields(19) = "txt_HOLIDAYENTITLEMENTDAYS|FLOAT"

FormFields(20) = "sel_NOTICEPERIOD|NUMBER"
FormFields(21) = "txt_DATEOFNOTICE|DATE"
FormFields(22) = "sel_BUDDY|NUMBER"
FormFields(23) = "rdo_ISMANAGER|NUMBER"
FormFields(24) = "rdo_ISDIRECTOR|NUMBER"
FormFields(25) = "rdo_ACTIVE|NUMBER"
FormFields(26) = "txt_FOREIGNNATIONALNUMBER|TEXT"

FormFields(27) = "hid_EMPLOYEEID|NUMBER"
FormFields(28) = "txt_EMPLOYEELIMIT|NUMBER"
FormFields(29) = "txt_HOLIDAYENTITLEMENTHOURS|FLOAT"
FormFields(30) = "sel_GRADEPOINT|INT"
FormFields(31) = "sel_OFFICELOCATION|NUMBER"
FormFields(32) = "sel_HOLIDAYRULE|NUMBER"
FormFields(33) = "txt_FTE|FTE|FLOAT"
FormFields(34) = "sel_HOLIDAYINDICATOR|NUMBER"
FormFields(35) = "txt_CARRYFORWARD|FLOAT"
FormFields(36) = "txt_ALSTARTDATE|DATE"
FormFields(37) = "txt_GSRENo|TEXT"

FormFields(38) = "rdo_IsGasSafe|NUMBER"
FormFields(39) = "rdo_IsOftec|NUMBER"
FormFields(40) = "sel_JobRoleId|NUMBER"
FormFields(41) = "hid_LASTACTIONUSER|INT"

Dim TradeIds
TradeIds = Request("tbListNames")

Function TradeUpsert()
    ID = Request.Form(UpdateID)
	' CALL TO STORED PROCEDURE TO INSERT THE TRADE IDs
	Set comm = Server.CreateObject("ADODB.Command")
	' setting stored procedure name
	comm.commandtext = "E_Trade_Insert"
	comm.commandtype = 4
	Set comm.activeconnection = Conn
	' setting input parameters for sproc used in delete + insert statements
      
    comm.parameters(1) = ID
	comm.parameters(2) = TradeIds
    comm.parameters(3) = UserId
	' executing sproc
	comm.execute
End Function


Function NewRecord()
	ID = Request.Form(UpdateID)
	Call MakeInsert(strSQL)
	SQL = "INSERT INTO E_JOBDETAILS " & strSQL & ""
	Conn.Execute SQL, recaffected
    Call TradeUpsert()
	Call UpdateTotalLeave()
	Call GO()
      Call UpdateEmployeeJobRoleTeamID()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)
    
	Call MakeUpdate(strSQL)       
	SQL = "UPDATE E_JOBDETAILS " & strSQL & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	Conn.Execute(SQL) , recaffected
    Call TradeUpsert()   
	SQL = "UPDATE AC_LOGINS SET ACTIVE = " & request("rdo_ACTIVE") & " WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL
    Call UpdateEmployeeJobRoleTeamID()
    Call UpdateTotalLeave()
	Call GO()
End Function

Function UpdateEmployeeJobRoleTeamID()
	JobRoleId = Request.Form("sel_JobRoleId")
    teamId = Request.Form("sel_TEAM")
    	 response.Write(teamId)
     if teamId <> "" AND JobRoleId  <> "" Then

    SQL = "  SELECT JobRoleTeamId FROM E_JOBROLETEAM Where JobRoleId =" & JobRoleId & " AND TeamId = " & teamId
	Call OpenRs(rsJobRoles, SQL)	
	jobRoleTeamId = rsJobRoles("JobRoleTeamId")    
   if jobRoleTeamId > 0 Then
   SQL = "UPDATE E__EMPLOYEE SET JobRoleTeamId = " & jobRoleTeamId & ", LASTACTIONUSER = " & UserId & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL
    SQL = "INSERT INTO E_EMPLOYEEJOBROLEHISTORY ( JobRoleTeamId, EmployeeId, CreatedDate, CreatedBy )" & strSQL & ""
	SQL = "SET NOCOUNT ON;INSERT INTO E_EMPLOYEEJOBROLEHISTORY  " &_
					"( JobRoleTeamId, EmployeeId, CreatedDate, CreatedBy )" &_
					" VALUES (" & jobRoleTeamId & ", " & ID& ", " & " GETDATE(),"& UserId & ")"
	 response.Write(SQL)							  
    Conn.Execute SQL
   End If   
	End If 
  
End Function

Function UpdateTotalLeave()

   ' If Request.Form("txt_BANKHOLIDAY") = "0" Or Request.Form("txt_BANKHOLIDAY") = "" Or Request.Form("txt_BANKHOLIDAY") = "0.00" Then
   ' Original If statement changed to allow update on full timers. 

   If Request.Form("txt_BANKHOLIDAY") = "" Then
        ID = Request.Form(UpdateID)
        strSQL = "EXEC E_HOLIDAY_DETAIL " & ID
  	    Call OpenRs (rsHols, strSQL)
		    If NOT rsHols.EOF Then
			    bnk_hd = rsHols(4)
			    total_leave = rsHols(5)
		    End If
		Call CloseRS(rsHols)
	    SQL = "UPDATE E_JOBDETAILS SET BANKHOLIDAY=" & bnk_hd & ",TOTALLEAVE=" & total_leave & ", LASTACTIONUSER = " & UserId & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	    Conn.Execute SQL
    Else
        Dim TotalLeaveAdj 
        If  Request.Form("txt_TOTALLEAVE") = "" Then
        TotalLeaveAdj = 0
        Else 
        TotalLeaveAdj = Request.Form("txt_TOTALLEAVE")
        End If 
        SQL = "UPDATE E_JOBDETAILS SET BANKHOLIDAY=" & Request.Form("txt_BANKHOLIDAY") & ",TOTALLEAVE=" & TotalLeaveAdj & ", LASTACTIONUSER = " & UserId & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	    Conn.Execute SQL
    End If

End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
End Function

Function GO()
	Response.Redirect "../JobDetails.asp?EmployeeID=" & ID
End Function

TheAction = Request("hid_Action")

Call OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
Call CloseDB()
%>