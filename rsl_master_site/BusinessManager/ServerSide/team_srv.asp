<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim exec_path 				'determines which way program flow will go based on value of
	Dim users_exist				'tells client side whether or not team will be deleted
	Dim team_exists				'tells client side whether or not team already exists
	Dim manager_exists			'tells client side whether or not the proposed manager is already a manager

	Dim error_flag, error_message, lst

	Dim team_id, team_name ,team_director, team_manager, main_function, brief_description,team_active 'team details gathered from client side or recordset

	exec_path = Request("path")
	team_id = Request("team_id")
	error_flag = false
	users_exist = false 	' yes you can delete team
	manager_exists = false  ' no this manager does not exist

	'--------------MAIN PROGRAM EXECUTION--------------------------------------'
	open_connection()

	If exec_path = "new" Then
		new_team()
	ElseIf exec_path = "amend" Then
		team_id = Request.Form("hd_team_id")
		edit_team(team_id)
	ElseIf exec_path = "del" Then
		del_team(team_id)
	Else
		load_team(team_id)
	End If

	close_connection()
	'--------------------END--------------------------------'

	' GATHER DATA FROM CLIENT SIDE FORM
	Function get_data()

		team_name = Replace(Request.Form("txt_TEAMNAME"), "'", "''")
		team_director = Request.Form("sel_DIRECTOR")
		team_manager = Request.Form("sel_MANAGER")
		If (team_manager = "") Then team_manager = 0 End If
		main_function = Replace(Request.Form("txt_MAINFUNCTION"), "'", "''")
		brief_description = Replace(Request.Form("txt_DESCRIPTION"), "'", "''")
		If(Request.Form("chk_ACTIVE")="on") Then
			team_active = "1"
		Else
			team_active = null
		End If
		If (brief_description = "") Then brief_description = null End If

	End Function

	'CREATES NEW TEAM
	Function new_team()

		Call get_data()
		If no_dupe_team() Then ' insert row
			conn.execute( "INSERT INTO E_TEAM (TEAMNAME, DIRECTOR, MANAGER, MAINFUNCTION, DESCRIPTION,ACTIVE) VALUES ('" &_
				team_name & "', " & team_director & ", " & team_manager & ", '" & main_function & "', '" & brief_description & "','" &  team_active & "')")
		End If

	End Function

	' DELETE TEAM ONLY IF NO USERS EXIST
	Function del_team (the_id)

		Dim strSQL, cnt
		strSQL = "SELECT COUNT(*) AS EMPLOYEECOUNT FROM E_JOBDETAILS WHERE TEAM = " & the_id

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()

		cnt = rsSet("EMPLOYEECOUNT")

		rsSet.close()
		Set rsSet  = Nothing

		' only delete if no employees are attachedto this team
		If cnt > 0 Then
			users_exist = true
		Else
			conn.execute("DELETE E_TEAM WHERE TEAMID = " & the_id)
		End If

	End Function

	' UPDATE TEAM RECORD BASED ON VALUE IN team_id
	Function edit_team(the_id)

		' get form data
		Call get_data()
		conn.execute( 	"UPDATE 	E_TEAM " &_ 
							"SET		TEAMNAME = '" & team_name & "'," &_
							"			MANAGER = " & team_manager & ", " &_
							"			DIRECTOR = " & team_director & ", " &_
							"			MAINFUNCTION = '" & main_function & "', " &_
							"			DESCRIPTION = '" & brief_description & "', " &_
							"			ACTIVE = '" & team_active & "' " &_
							"WHERE		TEAMID = " & the_id )

	End Function

	' GATHERS DATA FROM RECORDSET TO POPULATE FORM AND ALLOW EDITING ON CLIENT SIDE
	Function load_team(the_id)

		Dim strSQL, cnt
			strSQL = "SELECT * FROM E_TEAM WHERE TEAMID = " & the_id

		set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING
			rsSet.Source = strSQL
			rsSet.CursorType = 0
			rsSet.CursorLocation = 2
			rsSet.LockType = 3
			rsSet.Open()

			If Not rsSet.EOF Then

				team_name = rsSet("TEAMNAME")
				team_director = rsSet("DIRECTOR")
				team_manager = rsSet("MANAGER")
				main_function = rsSet("MAINFUNCTION")
				brief_description = rsSet("DESCRIPTION")
				team_active=rsSet("ACTIVE")
			Else
				error_flag = true
				error_message = "This team no longer exists in the database :: Contact administrator "			
			End If

			rsSet.close()
		Set rsSet  = Nothing

		Call build_select()

	End Function


	Function no_dupe_team()

		Dim team_cnt

		strSQL = "SELECT COUNT(*) AS TEAMCOUNT FROM E_TEAM WHERE TEAMNAME LIKE '%" & team_name & "%'"

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()

		team_cnt = rsSet("TEAMCOUNT")

		rsSet.close()

		Set rsSet  = Nothing

		' only delete if no employees are attachedto this team
		If team_cnt > 0 Then
			team_exists = true
			no_dupe_team = false
			exit function
		end if

		no_dupe_team = true

	End Function

	Function no_dupe_manager(int_limit)

		Dim manager_cnt

		strSQL = "SELECT COUNT(*) AS MANAGERCOUNT FROM E_TEAM WHERE MANAGER = " & team_manager

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()

		manager_cnt = rsSet("MANAGERCOUNT")
		rsSet.close()

		Set rsSet  = Nothing

		' only delete if no employees are attachedto this team
		If manager_cnt > int_limit Then
			manager_exists = true
			no_dupe_manager = false
			exit function
		End If

		no_dupe_manager = true

	End Function

	Function build_select()

		strSQL ="SELECT DISTINCT E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
				"E.EMPLOYEEID FROM 	E__EMPLOYEE E INNER JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"WHERE ISMANAGER = 1 AND J.ACTIVE = 1 AND J.TEAM = " & team_id

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = strSQL
		rsSet.CursorType = 0
		rsSet.CursorLocation = 2
		rsSet.LockType = 3
		rsSet.Open()

		lst = "<select name='sel_MANAGER' id='sel_MANAGER' class='textbox200'><option value=''>Please selects</option>"
		While (NOT rsSet.EOF)
			lst = lst & "<option value='" & rsSet("EMPLOYEEID") & "'" & extrabit & ">" & rsSet("FULLNAME") & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		Wend

		rsSet.close()

		lst = lst & "</select>"
		Set rsSet = Nothing

		If int_lst_record = 0 Then
			lst = "<select name='sel_MANAGER' class='textbox200'><option value=''>No Records Exist</option></select>"
		End If

		response.write lst

	End Function


	' CREATE AND OPEN CONNECTION OBJECT
	Function open_connection()

		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open RSL_CONNECTION_STRING

	End Function

	' CLOSE CONNECTION OBJECT
	Function close_connection()

		Conn.Close
		Set Conn = Nothing

	End Function
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Main Details</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript" defer="defer">

        // RETURN UPDATED TABLE TO CLIENT PAGE
        function returnData() {
            if ("<%=exec_path%>" == "load")
                load_team();
            else if ("<%=exec_path%>" == "del")
                del_team();
            else if ("<%=exec_path%>" == "new" || "<%=exec_path%>" == "amend")
                new_team();
            else
                parent.location.replace("../team.asp");
        }

        function load_team() {
            parent.document.getElementById("dvMANAGER").innerHTML = "<%=lst%>";
            parent.swap_button(1);
            parent.swap_div(3);
            parent.document.getElementById("txt_TEAMNAME").value = "<%=team_name%>";
            if ("<%=team_manager%>" != "0")
                parent.document.getElementById("sel_MANAGER").value = "<%=team_manager%>";
                parent.document.getElementById("sel_DIRECTOR").value = "<%=team_director%>";
                parent.document.getElementById("txt_MAINFUNCTION").value = "<%=main_function%>";
                parent.document.getElementById("txt_DESCRIPTION").value = txt_DESCRIPTION.value;
                parent.document.getElementById("hd_team_id").value = "<%=team_id%>";
            if ("<%=team_active%>" == "1")
                parent.document.getElementById("chk_ACTIVE").checked = "true";
                parent.checkForm()
        }

        function new_team() {
            if ("<%=team_exists%>" == "True")
                alert("This team already exists");
            else if ("<%=manager_exists%>" == "True")
                alert("The person you have chosen for manager already manages a team ");
            else
                parent.location.replace("../team.asp");
        }

        function del_team() {
            if ("<%=users_exist%>" == "True")
                alert("This team may not be deleted as it contains team members");
                parent.location.replace("../team.asp");
        }

    </script>
</head>
<body onload="returnData()">
    <textarea name="txt_DESCRIPTION" id="txt_DESCRIPTION"><%=brief_description%></textarea>
</body>
</html>
