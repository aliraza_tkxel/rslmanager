<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (25)
Dim DataTypes    (25)
Dim ElementTypes (25)
Dim FormValues   (25)
Dim FormFields   (25)
UpdateID	  = "hid_EmployeeID"
FormFields(0) = "sel_TITLE|INT"
FormFields(1) = "txt_FIRSTNAME|TEXT"
FormFields(2) = "txt_LASTNAME|TEXT"
FormFields(3) = "txt_BANK|TEXT"
FormFields(4) = "txt_DOB|DATE"
FormFields(5) = "txt_SORTCODE|TEXT"
FormFields(6) = "sel_GENDER|TEXT"
FormFields(7) = "txt_ACCOUNTNUMBER|TEXT"
FormFields(8) = "txt_ACCOUNTNAME|TEXT"
FormFields(9) = "sel_MARITALSTATUS|TEXT"
FormFields(10) = "sel_ETHNICITY|INT"

FormFields(11) = "sel_RELIGION|INT"
FormFields(12) = "sel_SEXUALORIENTATION|INT"
FormFields(13) = "hid_DISABILITY|TEXT"
	
FormFields(14) = "txt_RELIGIONOTHER|TEXT"
FormFields(15) = "txt_SEXUALORIENTATIONOTHER|TEXT"
FormFields(16) = "txt_MIDDLENAME|TEXT"
	
FormFields(17) = "txt_ROLLNUMBER|TEXT"
FormFields(18) = "txt_ETHNICITYOTHER|TEXT"

FormFields(19) = "txt_BANK2|TEXT"
FormFields(20) = "txt_SORTCODE2ND|TEXT"
FormFields(21) = "txt_ACCOUNTNUMBER2|TEXT"
FormFields(22) = "txt_ACCOUNTNAME2|TEXT"
FormFields(23) = "txt_ROLLNUMBER2|TEXT"
FormFields(24) = "txt_PROFILE|TEXT"
FormFields(25) = "hid_LASTACTIONUSER|INT"

Function NewRecord ()
	
	Call MakeInsert(strSQL)	
	SQL = "SET NOCOUNT ON INSERT INTO E__EMPLOYEE " & strSQL & " SELECT SCOPE_IDENTITY() AS NewID SET NOCOUNT OFF"
	Response.Write SQL
	Call OpenRs (rsEmployee, SQL)
		ID = rsEmployee("NewID")
	Call CloseRs (rsEmployee)
	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE E__EMPLOYEE " & strSQL & ", LASTACTIONTIME = GETDATE() WHERE EMPLOYEEID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)	
	
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
End Function

Function GO()
	CloseDB()
	Response.Redirect "../Contact.asp?EmployeeID=" & ID
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>