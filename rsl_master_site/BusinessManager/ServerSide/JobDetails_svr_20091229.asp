<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID

Dim DataFields   (33)
Dim DataTypes    (33)
Dim ElementTypes (33)
Dim FormValues   (33)
Dim FormFields   (33)

UpdateID	  = "hid_EmployeeID"
FormFields(0) = "txt_STARTDATE|DATE"
FormFields(1) = "txt_ENDDATE|DATE"
FormFields(2) = "sel_PROBATIONPERIOD|NUMBER"
FormFields(3) = "txt_REVIEWDATE|DATE"
FormFields(4) = "sel_JOBTITLE|TEXT"
FormFields(5) = "txt_REFERENCENUMBER|TEXT"
FormFields(6) = "txt_DETAILSOFROLE|TEXT"
FormFields(7) = "sel_TEAM|NUMBER"
FormFields(8) = "sel_PATCH|NUMBER"
FormFields(9) = "sel_LINEMANAGER|NUMBER"
FormFields(10) = "sel_PARTFULLTIME|NUMBER"
FormFields(11) = "txt_HOURS|FLOAT"
FormFields(12) = "sel_GRADE|TEXT"

FormFields(13) = "txt_SALARY|FLOAT"
FormFields(14) = "txt_TAXOFFICE|TEXT"
FormFields(15) = "txt_TAXCODE|TEXT"
FormFields(16) = "txt_PAYROLLNUMBER|TEXT"
FormFields(17) = "txt_DRIVINGLICENSENUMBER|TEXT"
FormFields(18) = "txt_NINUMBER|TEXT"
FormFields(19) = "txt_HOLIDAYENTITLEMENTDAYS|FLOAT"

FormFields(20) = "txt_APPRAISALDATE|DATE"

FormFields(21) = "sel_NOTICEPERIOD|NUMBER"
FormFields(22) = "txt_DATEOFNOTICE|DATE"
FormFields(23) = "sel_BUDDY|NUMBER"
FormFields(24) = "rdo_ISMANAGER|NUMBER"
FormFields(25) = "rdo_ISDIRECTOR|NUMBER"
FormFields(26) = "rdo_ACTIVE|NUMBER"
FormFields(27) = "txt_FOREIGNNATIONALNUMBER|TEXT"

FormFields(28) = "hid_EMPLOYEEID|NUMBER"
FormFields(29) = "txt_EMPLOYEELIMIT|NUMBER"
FormFields(30) = "txt_HOLIDAYENTITLEMENTHOURS|FLOAT"
FormFields(31) = "txt_OTRATE|FLOAT"
FormFields(32) = "sel_GRADEPOINT|INT"
FormFields(33) = "sel_OFFICELOCATION|NUMBER"
Function NewRecord ()
	ID = Request.Form(UpdateID)		
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO E_JOBDETAILS " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected	
	GO()
End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	
	SQL = "UPDATE E_JOBDETAILS " & strSQL & " WHERE EMPLOYEEID = " & ID
	
	Response.Write SQL
	Conn.Execute SQL, recaffected
	SQL = "UPDATE AC_LOGINS SET ACTIVE = " & request("rdo_ACTIVE") & " WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL
	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)	
	
	SQL = "DELETE FROM E__EMPLOYEE WHERE EMPLOYEEID = " & ID
	Conn.Execute SQL, recaffected
End Function

Function GO()
	CloseDB()
	Response.Redirect "../Benefits.asp?EmployeeID=" & ID
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>