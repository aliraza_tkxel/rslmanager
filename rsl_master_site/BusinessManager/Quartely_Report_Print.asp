<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	PageName = "Establishment_report.asp"
	// only allow managers access
	'if isManager() <> 1 Then response.redirect "../accessdenied.asp" end if
	// redirect all director to a higher level showing their teams
	//if (isDirector() Or isManager()) And Request("bypass") <> 1 Then response.redirect "establishment.asp?HFKS023=1" end if
	CONST CONST_PAGESIZE = 19
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim team_name, team_id, theDate, fromDate
	Dim searchName, searchSQL,orderSQL, DEFAULTstring
	Dim TotalSalary, TotalStaff, maletotal, femaletotal, NewStarters, Leavers,whereSQL,EmployeeType
	
	GetVariables()
	
	getTeams()
	opendb()
	Call BuildSelect(selTeam, "sel_TEAM", "E_TEAM  WHERE TEAMID NOT IN (1,93) AND ACTIVE=1", "TEAMID, TEAMNAME", "TEAMNAME", "All Departments", Request.QueryString("DEPT"), NULL, "textbox200", "onchange='' TABINDEX=2")
	GetDepartmentReportVars()
	closedb()
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If

	Function GetVariables()
		
		Dapartment = Request.QueryString("DEPT")
		theDate = Request.QueryString("thedate")
        fromDate = Request.QueryString("fromDate")
		OrderBy = Request.QueryString("CC_SORT")
		EmployeeType = Request.QueryString("EmployeeType")

		If OrderBy <> "" then
			orderSQL =  " ORDER BY  " & OrderBy
		else
			orderSQL = " ORDER BY E.LASTNAME ASC "
		End if
		
		
		
		If Dapartment <> "" then
			whereNeeded = 1
			deptSQL =  " T.TEAMID =  " & Dapartment & " "
		End if
		
		If theDate <> "" then
			whereNeeded = 1
			if deptSQL <> "" then
			dateSQL = dateSQL & " AND "
			End If
			dateSQL =  dateSQL & " J.STARTDATE <=  '" & theDate & "' "
		End if

        If fromDate <> "" then
			whereNeeded = 1
            if deptSQL <> "" then
			fromdateSQL = fromdateSQL & " AND "
            end if
			fromdateSQL =  fromdateSQL & " (J.ENDDATE IS NULL) OR J.ENDDATE >=  '" & fromDate & "' "

		End if
		
		if whereNeeded = 1 then
			whereSQL = " AND " & deptSQL & fromdateSQL
		End If
		
		DEFAULTstring = "&DEPT=" & Dapartment & "&thedate=" & theDate & "&fromdate=" & fromDate & "&EmployeeType=" & EmployeeType
	
	End Function
	
	Function getTeams()
		
		Dim strSQL, rsSet, intRecord 
		
		intRecord = 0
		str_data = ""
		strSQL = 		"SELECT 	E.EMPLOYEEID, " &_
						"	FIRSTNAME + ' ' + LASTNAME AS FULLNAME," &_
						"	ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
						"	CASE WHEN LEN(ISNULL(J.JOBTITLE, 'N/A')) > 12   " &_
 						"		THEN LEFT(ISNULL(J.JOBTITLE, 'N/A'), 14) +  '...'   " &_
						"		ELSE  ISNULL(J.JOBTITLE, 'N/A')   " &_
        				"	END AS JOBTITLEshort, " &_
						"	ISNULL(CAST(J.SALARY AS NVARCHAR),'N/A') AS SALARY," &_
						"	ISNULL(CAST(EG.[DESCRIPTION] AS NVARCHAR), 'N/A') AS GRADE, " &_
						"	ISNULL(ETH.DESCRIPTION, 'N/A') AS ETHNICITY, " &_
						"	ISNULL(E.GENDER,'N/A') AS GENDER, " &_
						"	E.DOB AS DOB, " &_
						"	ISNULL(T.TEAMNAME,'N/A') AS DEPARTMENT, " &_
						"	J.STARTDATE as STARTDATE, " &_
						"	J.ENDDATE as ENDDATE,J.FTE,PT.DESCRIPTION " &_
						"FROM	E__EMPLOYEE E   " &_
						"	LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"	lEFT JOIN E_GRADE EG ON EG.GRADEID=CAST(J.GRADE AS INT) " &_
						"	LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"	LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
                        "	LEFT JOIN E_PARTFULLTIME PT ON PT.PARTFULLTIMEID = J.PARTFULLTIME " &_ 

						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL& orderSQL 
						
		'"	LEFT JOIN E_JOURNAL JNAL ON J.EMPLOYEEID = JNAL.EMPLOYEEID AND JNAL.CURRENTITEMSTATUSID = 5 AND JNAL.ITEMNATUREID = 2 " &_

		'rw strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		If Not rsSet.EOF then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			while Not rsSet.EOF
				'& rsSet("JOBTITLE")
				'& rsSet("ETHNICITY")
				str_data = str_data & "<TBODY><TR>" &_
					"<TD WIDTH=129PX>" & rsSet("FULLNAME") & "</TD>" &_
					"<TD WIDTH=176PX ALIGN=LEFT title='" & rsSet("JOBTITLE") & "'>" & rsSet("JOBTITLE") & "</TD>" &_
					"<TD WIDTH=139PX ALIGN=CENTER>" & rsSet("DEPARTMENT") & "</TD>" &_
					"<TD WIDTH=68PX ALIGN=CENTER>" & rsSet("GENDER") & "</TD>"&_
					"<TD WIDTH=126PX ALIGN=CENTER>" & rsSet("ETHNICITY")  & "</TD>" &_
					"<TD WIDTH=75PX ALIGN=CENTER>" & rsSet("DOB") & "</TD>"&_
					"<TD WIDTH=78PX ALIGN=CENTER>" & rsSet("SALARY") & "</TD>"&_
					"<TD WIDTH=65PX ALIGN=CENTER>" & rsSet("GRADE") & "</TD>"&_
                    "<TD WIDTH=80PX ALIGN=CENTER>" & rsSet("DESCRIPTION") & "</TD>"&_
                    "<TD WIDTH=38PX ALIGN=CENTER>" & rsSet("FTE") & "</TD>"&_
					"<TD WIDTH=102PX ALIGN=CENTER>" & rsSet("STARTDATE") & "</TD>"&_
					"<TD WIDTH=78PX ALIGN=CENTER>" & rsSet("ENDDATE") & "</TD>"

					str_data = str_data & "</TR></TBODY>"
				
					
				
				count = count + 1			
				rsSet.movenext()
				wend

			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
						
		End If

		' if no teams exist inform the user
		If count = 0 Then 
			str_data = "<TFOOT><TR><TD COLSPAN=12 STYLE='FONT:16PX' ALIGN=CENTER>No records found for this date !!</TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR></TFOOT>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
									
			'Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
		

		
	End function


		// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=12 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	Function GetDepartmentReportVars()
	
				sql =   "SELECT 	ISNULL(SUM(ISNULL(J.SALARY,0)),0) AS SALARY, COUNT(*) AS TOTALSTAFF, " &_
						"			SUM(	CASE WHEN J.STARTDATE >= '" & fromDate & "' and  J.STARTDATE <= '" & theDate & "'  " &_
						"				THEN 1    " &_
						"				ELSE  0    " &_
						"    		END )AS STARTERS, " &_
						"			SUM(CASE WHEN E.GENDER = 'MALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS MALETOTAL, " &_
						"			SUM(CASE WHEN E.GENDER = 'FEMALE'  " &_
 						"				THEN 1 " &_
						"				ELSE 0 " &_
        				"			END) AS FEMALETOTAL, " &_
						"			SUM(	CASE WHEN J.ENDDATE >= '" & fromDate & "' and J.ENDDATE <= '" & theDate & "' " &_ 
						"				THEN 1    " &_ 
						"				ELSE  0   " &_
						"    		END )AS LEAVERS " &_
						"FROM	E__EMPLOYEE E   " &_
						"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID  " &_
						"			LEFT JOIN G_ETHNICITY ETH ON ETH.ETHID = E.ETHNICITY " &_
						"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_ 
						" WHERE TEAMID NOT IN (1) " & whereSQL & EMPSQL
						
			Call OpenRs(rsLoader, SQL)
				if (NOT rsLoader.EOF) then
			
				'==================================================================
				'= Set Some Totals For department report
				'==================================================================
						TotalSalary = FormatCurrency(rsLoader("SALARY"))
						TotalStaff		=	rsLoader("TOTALSTAFF")
						maletotal		=	rsLoader("MALETOTAL")
						femaletotal		=	rsLoader("FEMALETOTAL")
						NewStarters			=	rsLoader("STARTERS") 						
						Leavers				=	rsLoader("LEAVERS") 
				'==================================================================
				'= End Totals For department report
				'==================================================================	
				end if
			CloseRS(rsLoader)
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)"/>
<TITLE>RSL Manager Business --> HR Tools --> Team</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <style type="text/css">
        .style1
        {
            width: 106px;
        }
        .RSLButton
        {
            margin-left: 107px;
        }
      
    </style>
</HEAD>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF  MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name = thisForm method=post>
  <table border=0 width=900 cellpadding=2 cellspacing=2 height=40PX >
    <tr>
      <td width="584"><b>Department Information :<span class="style1"> THIS PAGE IS UNDER DEVELOPMENT ( AS OTHER PAGES HAVE CHANGED ASSOCIATED WITH THE DATA IN THIS REPORT ) </span></b></td>
    </tr>
    <tr>
      <td NOWRAP>
	  <div name="DeptInfo" id="DeptInfo">
	  	<table width="354"  border="0" cellpadding="0" cellspacing="0" style='BORDER:1PX SOLID #133E71'>
			  <tr>
			    <td colspan="5" height="10"></td>
		    </tr>
			  <tr>
			    <td width="12">&nbsp;</td>
			    <td width="88"><strong>Total Salary : </strong></td>
			    <td width="117"><%= TotalSalary %></td>
			    <td width="94"><strong>New Starters : </strong></td>
			    <td width="41"><%= NewStarters %></td>
			  </tr>
			   <tr>
			    <td colspan="5" height="10"></td>
		    </tr>
			  <tr>
			    <td>&nbsp;</td>
			    <td><strong>Total Staff : </strong></td>
			    <td><%= TotalStaff %> ( M: <%=maletotal%> / f: <%= femaletotal %> ) </td>
			    <td><strong>Leavers : </strong></td>
			    <td><%= Leavers %></td>
			  </tr>
			  <tr>
			    <td height="10" colspan="5"></td>
		    </tr>
		</table>
	</div>
	  </td>
    </tr>
  </table>
  <br>
<br>

  <TABLE WIDTH=1100 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD ROWSPAN=2><IMG NAME="tab_establishment" TITLE='<%=team_name%>' SRC="Images/tab_establishmentreport.gif" WIDTH=157 HEIGHT=20 BORDER=0></TD>
      <!--TD><IMG SRC="images/spacer.gif" WIDTH=656 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT><TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0>
          <TR>
            <TD height="1" ALIGN=RIGHT>&nbsp;</TD>
          </TR>
        </TABLE>
		</TD>	
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71 colspan=4><IMG SRC="images/spacer.gif" WIDTH=940 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=1100 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(../Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR align="left" valign="bottom">
      <TD WIDTH=129 CLASS='NO-BORDER'><b>Name</b> </TD> 
      <TD WIDTH=176 CLASS='NO-BORDER'><b>Job Title</b> </TD>
      <TD WIDTH=139 CLASS='NO-BORDER'>	  
	  <B>Dept.</B>
		</TD>
      <TD WIDTH=68 CLASS='NO-BORDER'><strong>Gender</strong></TD>
      <TD WIDTH=126 CLASS='NO-BORDER'><strong>Ethnicity</strong></TD>
      <TD WIDTH=75 CLASS='NO-BORDER'> <B>DOB</B> </TD>
      <TD WIDTH=78 ALIGN=left valign=bottom  CLASS='NO-BORDER'> <B>Salary</B></TD>
      <TD ALIGN=left WIDTH=65 CLASS='NO-BORDER' valign=bottom>
	    <B>Grade</B>
	    </TD>
		<TD WIDTH=78 CLASS='NO-BORDER'> <B>Full/Part </B> </TD>
		<TD WIDTH=35 CLASS='NO-BORDER'> <B>FTE </B> </TD>
      <TD WIDTH=102 CLASS='NO-BORDER'>	  <B>Start Date</B>
</TD>
      <TD WIDTH=78 CLASS='NO-BORDER'> <B>End Date </B> </TD>
    </TR>
    <TR STYLE='HEIGHT:3PX'> 
      <TD height="3" COLSPAN=12 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    </THEAD> <%=str_data%> 
  </TABLE>
</form>
</BODY>
</HTML>

