<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================

		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue
		Dim change_of_tenancy, day_to_day, services, decorating, gas, timber, total
		DIM TOTAL_REPAIRS ,TOTAL_ACHIEVED, TOTAL_NOTACHIVED, THE_KPI_FIG 
		Redim KPIVALUES(MONTHCOUNT)
		Redim INTFRAME(MONTHCOUNT)
		Redim TOTALREPAIRS(MONTHCOUNT)
		
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	
	
		selValue = Request("START")
		TID = Request("TOPICID")
		
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	
		
		if TID = "" Then TID = -1 End if
		total = 0
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
			KPIVALUES(MONTHCOUNT) = 0
			INTFRAME(MONTHCOUNT) = 0
			TOTALREPAIRS(MONTHCOUNT) = 0
		next
		
		' GET MONTHS PASSED FOR KPI CALCULATION
		If month(date) < 4 Then months_passed = month(date) + 9 Else months_passes = month(date) - 3 End If
	


		get_fiscal_boundary()
		
		Dmonth = -1
		DYear = -1
	
	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	

	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	

		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		End If
		
	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	
	
		Call get_kpi_header(TID)
		Call get_monthly_breakdown()
		Call build_fiscal_month_select(lstBox, Dmonth)
		
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	
	
		for x = 0 to MONTHCOUNT 
			ytd_repairs = ytd_repairs + TOTALREPAIRS(x)
			ytd_intframe = ytd_intframe + INTFRAME(x)
			If TOTALREPAIRS(x) = 0 Then KPIVALUES(x) = 0 Else KPIVALUES(X) = ( (INTFRAME(x) / TOTALREPAIRS(x) )* 100 ) End If
		next
	
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd)  Then
			prd_Repairs = ytd_repairs
			prd_inTimeFrame = ytd_intframe
			prd_outTimeFrame = ytd_repairs - ytd_intframe
			if prd_Repairs = 0 then kpi = 0 else kpi = ( (prd_inTimeFrame / prd_Repairs )* 100 ) end if
			
			mnth_Repairs 	  = "N/A"
			mnth_inTimeFrame  = "N/A"
			mnth_outTimeFrame = "N/A"
			prd_kpi 		  = "N/A"
						
		Else 
		
			CurrentMonth 	 = 4 ' April
			MonthMatch 		 = False
			
			' Initialise the varibales for the loop
			Counter 		 = 0
			prd_Repairs 	 = 0
			prd_inTimeFrame  = 0
			prd_outTimeFrame = 0


			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			' Important Loop :  
			'
			' This loop adds up the monthly figures for each variable and collates them to show the period figure.
			' The loop stops when the month chosen is equal to the var number 'CurrentMonth'.
			'
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			Do while MonthMatch = False  
			
				' The following three build monthly figures to work out the period calculation done outside the loop
				prd_Repairs = prd_Repairs + TOTALREPAIRS(CurrentMonth)
				prd_inTimeFrame = prd_inTimeFrame + INTFRAME(CurrentMonth)
				
				
				'check if current month number in the loop is equal to the month chosen
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop

			prd_outTimeFrame = prd_Repairs - prd_inTimeFrame
			if prd_Repairs = 0 then kpi = 0 else kpi = ( (prd_inTimeFrame / prd_Repairs )* 100 ) end if
			
			mnth_Repairs 	  = TOTALREPAIRS(Dmonth)
			mnth_inTimeFrame  = INTFRAME(Dmonth)
			mnth_outTimeFrame = TOTALREPAIRS(Dmonth) - INTFRAME(Dmonth)
			mnth_kpi = KPIVALUES(Dmonth)
			mnth_Kpi = FormatNumber(mnth_kpi,2)
		End If
		
	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	

	'===========================================================================================
	'	Function List
	'===========================================================================================	

	' GET FIGURES FOR MONTHLY BREAKDOWN
	Function get_monthly_breakdown()
	
		sql = 	"SELECT	COUNT(*) AS THECOUNT,MONTH(ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE))) AS MONTHNUMBER, " &_
				"	SUM(CASE WHEN (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) <= 31 " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS INTF, " &_
				"	SUM(CASE WHEN (DATEDIFF(D, J.CREATIONDATE, ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)))) > 31  " &_
				"	THEN 1 " &_
				"	ELSE 0 " &_
				"	END) AS OUTTF " &_
				"FROM	C_JOURNAL J " &_
				"	LEFT JOIN C_MAXCOMPLETEDREPAIRDATE RC ON RC.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = W.ORDERITEMID " &_
				"	LEFT JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = I.ORDERITEMID " &_
				"	LEFT JOIN F_INVOICE INV ON INV.INVOICEID = O.INVOICEID " &_
				"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"	LEFT JOIN C_LASTCOMPLETEDACTION LC ON LC.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN (SELECT JOURNALID, MAX(REPAIRHISTORYID) AS REPAIRHISTORYID, ITEMDETAILID FROM C_REPAIR GROUP BY JOURNALID, ITEMDETAILID) R ON R.JOURNALID = J.JOURNALID " &_
				"	INNER JOIN C_REPAIR RP ON RP.REPAIRHISTORYID = R.REPAIRHISTORYID " &_
				"	INNER JOIN R_ITEMDETAIL REP ON REP.ITEMDETAILID = R.ITEMDETAILID " &_
				"	INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = REP.PRIORITY " &_
				"WHERE	I.PITYPE = 2 AND REP.R_EXP_ID = 24 " &_
				"	AND REP.PRIORITY IN ('E','F') " &_
				"	AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) >= '" & FStart & "' AND ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE)) <= '" & FEnd & "' " &_
				"	AND RP.ITEMACTIONID IN (6,9,10,15) " &_
				"	AND P.ASSETTYPE = 1 " &_
				"GROUP 	BY MONTH(ISNULL(RC.COMPLETIONDATE,ISNULL(LC.LASTACTIONDATE,RP.LASTACTIONDATE))) "
		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			INTFRAME(rsSet("MONTHNUMBER")) = rsSet("INTF")
			TOTALREPAIRS(rsSet("MONTHNUMBER")) = rsSet("THECOUNT")
			rsSet.Movenext()
		Wend
	
	End Function
	'===========================================================================================
	'	End Function List
	'===========================================================================================	

	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI20.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
	function openuncompleted(monthno){
	
		window.open("../popups/uncompleted.asp?PRIORITY=3&FY=<%=Request("FY")%>&MONTHNO=" + monthno, "_blank", "TOP=100, LEFT=100, scrollbars=yes, height=410, width=770, status=no, resizable= Yes")	
	
	}
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&JAN=<%=KPIVALUES(1)%>&FEB=<%=KPIVALUES(2)%>&MAR=<%=KPIVALUES(3)%>&APR=<%=KPIVALUES(4)%>&MAY=<%=KPIVALUES(5)%>&JUN=<%=KPIVALUES(6)%>&JUL=<%=KPIVALUES(7)%>&AUG=<%=KPIVALUES(8)%>&SEP=<%=KPIVALUES(9)%>&OCT=<%=KPIVALUES(10)%>&NOV=<%=KPIVALUES(11)%>&DEC=<%=KPIVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
	  <TD WIDTH=49% VALIGN=TOP>
			<TABLE width="371" CELLPADDING=1 CELLSPACING=2>
              <TR>
                <TD width="204" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;
                    <SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>">
                      <%=lstBox%>
                  </SELECT></TD>
                <td width="68" align="right"><strong>Month</strong></td>
                <td width="83" align="right"><strong>Period</strong></td>
              </TR>
              <TR>
                <TD><b>Repairs</b></TD>
                <TD></TD>
                <TD></TD>
              </TR>
              <TR STYLE='HEIGHT:15PX'>
                <TD COLSPAN=3></TD>
              </TR>
              <TR>
                <TD valign="top">(A) Notified with 'ROUTINE' status (E/F) </TD>
                <TD ALIGN=RIGHT valign="top"><%=mnth_Repairs%></TD>
                <TD ALIGN=RIGHT valign="top"><%=prd_Repairs%></TD>
              </TR>
              <TR STYLE='HEIGHT:15PX'>
                <TD COLSPAN=3></TD>
              </TR>
              <TR>
                <TD>(B) Completed within timeframe </TD>
                <TD ALIGN=RIGHT><%=mnth_inTimeFrame%></TD>
                <TD ALIGN=RIGHT><%=prd_inTimeFrame%></TD>
              </TR>
              <TR STYLE='HEIGHT:15PX'>
                <TD COLSPAN=3></TD>
              </TR>
              <TR>
                <TD>(C) Completed after timeframe </TD>
                <TD ALIGN=RIGHT><%=mnth_outTimeFrame%></TD>
                <TD ALIGN=RIGHT><%=prd_outTimeFrame%></TD>
              </TR>
              <TR STYLE='HEIGHT:35PX'>
                <TD COLSPAN=3></TD>
              </TR>
              <TR>
                <TD ALIGN=RIGHT><b>KPI = </b></TD>
                <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><%=mnth_kpi%></TD>
                <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=Formatnumber(kpi)%></b></TD>
              </TR>
  		  	<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
			<TR STYLE='HEIGHT:35PX'>
          		<TD COLSPAN=3 ALIGN=RIGHT height="34"> 
            	<input type="button" name="btnVP" class="RSLButton" value="Outside Timeframe" onclick=openuncompleted(<%=Dmonth%>)>
			</TD>
			</TR>	
        </TABLE></TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
