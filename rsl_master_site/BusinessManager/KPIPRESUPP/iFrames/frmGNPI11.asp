<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	'===========================================================================================
	'	Declare Values
	'===========================================================================================

		Const MONTHCOUNT = 12
		
		Dim TID, STARTDATE, ENDDATE, lstBox, kpi, selValue, divisor
		Dim A, B, C
		Dim ytd_relet, ytd_available, ytd_days
		Redim KPI_VALUES(MONTHCOUNT)
		Redim AVAILABLE_VALUES(MONTHCOUNT)
		Redim RELET_COUNT(MONTHCOUNT)
		Redim DAYS(MONTHCOUNT)
	
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	

		selValue = Request("START")
		TID = Request("TOPICID")
		
	'===========================================================================================
	'	End Declare Values
	'===========================================================================================	

	OpenDB()

	'===========================================================================================
	'	Initialise variables
	'===========================================================================================	

		if TID = "" Then TID = -1 End if
		ytd_relet = 0
		' RESET VALUEARRAY
		for x = 0 to MONTHCOUNT 
				KPI_VALUES(x) = 0
				AVAILABLE_VALUES(x) = 0
				RELET_COUNT(x) = 0
				DAYS(x) = 0
		next
	
		get_fiscal_boundary()
		Dmonth = -1
		DYear = -1

	'===========================================================================================
	'	Request Values from form or page
	'===========================================================================================	

	'===========================================================================================
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'===========================================================================================	

		STARTDATE = Request("START")
		If STARTDATE = "" Then 
			STARTDATE = FStart 
			ENDDATE = FEnd
			divisor = DateDiff("m", FStart, date) + 1
		Else
			TempArray = Split(STARTDATE,"_")
			Dmonth = TempArray(0)
			Dyear = TempArray(1)
			STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
			ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
			divisor = 1
		End If
		
	'===========================================================================================
	'	End Choose Dates
	'===========================================================================================	
	
	'===========================================================================================
	'	Call Functions
	'===========================================================================================	

		Call get_kpi_header(TID)
		Call build_fiscal_month_select(lstBox, Dmonth)
		Call get_average_relet(2, RELET_COUNT, DAYS)
		Call get_monthly_property_count(1, AVAILABLE_VALUES)
		
	'===========================================================================================
	'	End Call Functions
	'===========================================================================================	
	
	'===========================================================================================
	'	Calculations area
	'===========================================================================================	
	
		for x = 0 to MONTHCOUNT 
			ytd_relet = ytd_relet + RELET_COUNT(x)
			ytd_days = ytd_days + DAYS(x)
			ytd_available = ytd_available + AVAILABLE_VALUES(x)
			If RELET_COUNT(x) = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = (DAYS(x) / RELET_COUNT(x)) End If
		next
	
		If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
			prd_Days = ytd_days
			prd_Available = ytd_available/divisor
			prd_Relet = ytd_relet
			If prd_Relet = 0 Then kpi = 0 Else kpi = (prd_Days / prd_Relet) End If
		Else 
		
			CurrentMonth 	 = 4 ' April
			MonthMatch 		 = False
			
			' Initialise the varibales for the loop
			Counter 	  = 0
			prd_Days 	  = 0
			prd_Available = 0
			prd_Relet	  = 0
			
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			' Important Loop :  
			'
			' This loop adds up the monthly figures for each variable and collates them to show the period figure.
			' The loop stops when the month chosen is equal to the var number 'CurrentMonth'.
			'
			'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
			Do while MonthMatch = False  
				'rw currentmonth & "<BR>"
				' The following three build monthly figures to work out the period calculation done outside the loop
				prd_Days 		= prd_Days + DAYS(CurrentMonth)
				prd_Available 	= prd_Available + AVAILABLE_VALUES(CurrentMonth)
				prd_Relet		= prd_Relet + RELET_COUNT(CurrentMonth)
				
				'check if current month number in the loop is equal to the month chosen
				if (CInt(CurrentMonth) = CInt(DMonth)) then MonthMatch = true 
				CurrentMonth = CurrentMonth + 1
				if (CurrentMonth = 13) then CurrentMonth = 1
				Counter = Counter + 1
				if (Counter = 20) then Exit Do
				
			Loop
			
			If prd_Relet = 0 Then kpi = 0 Else kpi = (prd_Days / prd_Relet) End If

			
			mnth_Days = DAYS(DMonth)
			mnth_Available = AVAILABLE_VALUES(DMonth)
			mnth_Relet = RELET_COUNT(DMonth)
			If mnth_Relet = 0 Then mnth_kpi = 0 Else mnth_kpi = (mnth_Days / mnth_Relet) End If
			
			mnth_Days 		= FormatNumber(DAYS(DMonth),2)
			mnth_Available 	= FormatNumber(AVAILABLE_VALUES(DMonth)/counter,2)
			mnth_Relet 		= FormatNumber(RELET_COUNT(DMonth),2)
			mnth_kpi   		= FormatNumber(mnth_kpi,2)
		End If
	
	'===========================================================================================
	'	End Calculations area
	'===========================================================================================	
	CloseDB()
	'===========================================================================================
	'	Function List
	'===========================================================================================	
	
	
		Function get_average_relet(ByRef p_status, ByRef RELET, ByRef DAYS)
			
			'rw FStart & "<BR>" & FEnd
			set dbcmd= server.createobject("ADODB.Command")
			dbcmd.ActiveConnection=RSL_CONNECTION_STRING
			dbcmd.CommandType = 4
			dbcmd.CommandText = "KPI_RELETDAYS_GROUPED"
			dbcmd.Parameters.Refresh
			dbcmd.Parameters(1) = FStart
			dbcmd.Parameters(2) = FEnd
			set rsSet=dbcmd.execute
			
			While not rsSet.EOF 
				RELET(rsSet("DMONTH")) = rsSet("RELET")
				DAYS(rsSet("DMONTH")) = rsSet("SUMDAYS")
				rsSet.Movenext()
			Wend
			
			CloseRs(rsSet)
			Set dbcmd = Nothing
			
		End Function
		
	'===========================================================================================
	'	End Function List
	'===========================================================================================	
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI11.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
	function openrelets(monthno){
	
		window.open("../popups/relet.asp?FY=<%=Request("FY")%>&MONTHNO=" + monthno, "_blank", "TOP=100, LEFT=200, scrollbars=yes, height=410, width=530, status=no, resizable= Yes")	
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE width="353" CELLPADDING=1 CELLSPACING=2>
				<TR><TD width="202" CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD>
				  <td width="70"><div align="right"></div></td>
			  <td width="65"><div align="right"></div></td>
				</TR>
				<TR><TD><b>Properties</b></TD>
				  <TD colspan="2"><input type="button" name="btnVP" class="RSLButton" value="View Properties" onclick=openrelets(<%=Dmonth%>)></TD>
			  </TR>
				<TR STYLE='HEIGHT:1PX;VISIBILITY:HIDDEN'><TD COLSPAN=3></TD></TR>
				<TR STYLE='VISIBILITY:HIDDEN'>
				  <TD>&nbsp;</TD>
				  <TD ALIGN=RIGHT><strong>Month</strong></TD>
				  <TD ALIGN=RIGHT><strong>Period</strong></TD>
			  </TR>
				<TR STYLE='VISIBILITY:HIDDEN'><TD>(E) Available To Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Available%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Available,2)%></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD>(F) Re Let</TD>
				  <TD ALIGN=RIGHT><%=mnth_Relet%></TD>
			    <TD ALIGN=RIGHT><%=FormatNumber(prd_Relet,2)%></TD></TR>
				<TR STYLE='HEIGHT:15PX'><TD COLSPAN=3></TD></TR>
				<TR STYLE='HEIGHT:35PX'><TD COLSPAN=3></TD></TR>
				<TR><TD ALIGN=RIGHT><b>KPI = </b></TD>
				  <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=mnth_kpi%></b></TD>
			    <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=FormatNumber(kpi,2)%></b></TD></TR>
				<TR STYLE='HEIGHT:45PX'><TD COLSPAN=3></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
