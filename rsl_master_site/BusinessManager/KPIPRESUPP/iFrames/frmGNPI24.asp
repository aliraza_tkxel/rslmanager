<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->

<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, months_passed, kpi, selValue, divisor, TotalEthnicityCount
	Dim change_of_tenancy, day_to_day, services, decorating, gas, timber, total, ytd_let
	Redim MONTHETHNICITY(MONTHCOUNT)
	Redim MONTHTOTAL(MONTHCOUNT)
	Redim MONTHVALUES(MONTHCOUNT)		
	Redim ET_Valid(15)
	Redim ET_Name(15)	
	Redim ET_Total(15)		
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	total = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			MONTHETHNICITY(x) = 0
			MONTHTOTAL(x) = 0			
	next
	
	' RESET VALUEARRAY
	for x = 1 to 15 
			ET_Valid(x) = 0
			ET_Total(x) = 0
	next

	' GET MONTHS PASSED FOR KPI CALCULATION
	If month(date) < 4 Then months_passed = month(date) + 9 Else months_passes = month(date) - 3 End If
	
	OpenDB()

	SQL = "SELECT ETHID, DESCRIPTION FROM G_ETHNICITY WHERE ETHID IN (1,2,3,4,5,6,7,8,9,10,11,12,15) ORDER BY DESCRIPTION"
	Call OpenRs(rsETH, SQL)
	while NOT rsETH.EOF
		ETID = rsEth("ETHID")
		ET_Valid(ETID) = 1
		ET_Name(ETID) = rsETH("DESCRIPTION")
		rsETH.moveNext
	wend
	Call CloseRs(rsETH)
	
	get_fiscal_boundary()
	
	Dmonth = -1
	DYear = -1
	
	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
		divisor = 1
	End If
	
	Call get_kpi_header(TID)
	get_monthly_breakdown()
	get_ytd_ethnicities()
	Call build_fiscal_month_select(lstBox, Dmonth)
	
	' GET FIGURES FOR MONTHLY BREAKDOWN
	Function get_monthly_breakdown()
	
		SQL = 	"SELECT 	MONTH(STARTDATE) AS DMONTH, COUNT(*)  AS LETTINGS " &_
				"FROM		C_TENANCY C " &_
				"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = C.PROPERTYID " &_
				"WHERE 		ASSETTYPE = 1 AND STARTDATE >= '"&FSTART&"' AND STARTDATE <= '"&FEND&"'	 " &_
				"GROUP 		BY MONTH(STARTDATE) "
				
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			MONTHTOTAL(rsSet("DMONTH")) = rsSet("LETTINGS")
			rsSet.Movenext()
		Wend

		SQL = 	"SELECT 	MONTH(C.STARTDATE) AS DMONTH, COUNT(DISTINCT C.TENANCYID) AS THECOUNT " &_
				"FROM		C_TENANCY C " &_
				"			INNER JOIN P__PROPERTY P ON P.PROPERTYID = C.PROPERTYID " &_
				"			INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = C.TENANCYID " &_
				"			INNER JOIN C__CUSTOMER CU ON CU.CUSTOMERID = CT.CUSTOMERID " &_
				"WHERE 		ASSETTYPE = 1 AND C.STARTDATE >= '" & FSTART & "' AND C.STARTDATE <= '" & FEND & "'" &_
				"			AND  CU.ETHNICORIGIN IN (1,2,3,4,5,6,7,8,9,10,11,12,15) " &_
				"GROUP 	BY MONTH(C.STARTDATE)" 
	'	RW SQL
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			MONTHETHNICITY(rsSet("DMONTH")) = rsSet("THECOUNT")
			rsSet.Movenext()
		Wend
			
		For i=1 to 12
			if (MONTHTOTAL(i) <> 0) then
				MONTHVALUES(i) = MONTHETHNICITY(i)/MONTHTOTAL(i)*100
			else
				MONTHVALUES(i) = 0
			end if
			ytd_let = ytd_let + MONTHTOTAL(i)
		next
	End Function
		
	' GET THE YEAR TO date ethncicities
	Function get_ytd_ethnicities()
	
		SQL = "SELECT COUNT(DISTINCT T.TENANCYID) AS THECOUNT, MONTH(T.STARTDATE),  ETHNICORIGIN FROM C_TENANCY T " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
		"WHERE T.STARTDATE >= '" & STARTDATE & "' AND T.STARTDATE <= '" & ENDDATE & "' " &_
			"AND ASSETTYPE = 1 " &_
			"AND C.ETHNICORIGIN IN (1,2,3,4,5,6,7,8,9,10,11,12,15) " &_
		"GROUP BY MONTH(T.STARTDATE), ETHNICORIGIN " 
		
		'RW sql
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			ET_Total(rsSet("ETHNICORIGIN")) = ET_Total(rsSet("ETHNICORIGIN")) + rsSet("THECOUNT")
			rsSet.Movenext()
		Wend
		Call CloseRs(rsSet)
		
		TotalEthnicityCount = 0
		for i=1 to 15
			if (ET_Valid(i) = 1) then
				'Response.Write i & " -- " & ET_Total(i)
				ET_Total(i) = ET_Total(i) '/divisor
				TotalEthnicityCount = TotalEthnicityCount + ET_Total(i)				
			end if
		next
	End Function

	If Cdate(STARTDATE) = Cdate(FStart) And Cdate(ENDDATE) = Cdate(FEnd) Then		' YEAR TO DATE
		A = ytd_let
	Else 
		A = MONTHTOTAL(DMonth)
	End If
	if (A = 0) then
		KPI = "N/A"
	else
		KPI = FormatNumber(TotalEthnicityCount/A * 100,2) & "%"
	end if
		
	'For i=1 To 12
	'	rw MONTHETHNICITY(i) & "<BR>"
	'Next	
		
	CloseDB()
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI24.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:5PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% valign=middle>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&JAN=<%=MONTHVALUES(1)%>&FEB=<%=MONTHVALUES(2)%>&MAR=<%=MONTHVALUES(3)%>&APR=<%=MONTHVALUES(4)%>&MAY=<%=MONTHVALUES(5)%>&JUN=<%=MONTHVALUES(6)%>&JUL=<%=MONTHVALUES(7)%>&AUG=<%=MONTHVALUES(8)%>&SEP=<%=MONTHVALUES(9)%>&OCT=<%=MONTHVALUES(10)%>&NOV=<%=MONTHVALUES(11)%>&DEC=<%=MONTHVALUES(12)%>">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			<TABLE CELLPADDING=1 CELLSPACING=2>
				<TR><TD STYLE='WIDTH:30PX'></TD><TD CLSPAN=2>Month&nbsp;&nbsp;&nbsp;<SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>"><%=lstBox%></SELECT></TD></TR>
				<TR STYLE='HEIGHT:3PX'><TD></TD></TR>
				<TR><TD></TD><TD><b>Properties</b></TD><TD></TD></TR>
				<TR STYLE='HEIGHT:5PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(A) Lettings</TD><TD ALIGN=RIGHT><%=FormatNumber(A,0)%></TD></TR>
				<TR STYLE='HEIGHT:3PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD><TD>(B) Ethnic Profile</TD><TD ALIGN=RIGHT></TD></TR>
				<TR STYLE='HEIGHT:3PX'><TD COLSPAN=3></TD></TR>
		<% 
		count = 0
		for i= 1 to 15 
			if (ET_Valid(i) = 1) then
				count = count + 1
		%>
				<TR><TD></TD><TD>&nbsp;&nbsp;<%=count%>&nbsp;<%=ET_Name(i)%></TD><TD ALIGN=RIGHT><%=FormatNumber(ET_Total(i),0)%></TD></TR>
		<% 
			end if
		next 
		%>
				<TR STYLE='HEIGHT:2PX'><TD COLSPAN=3></TD></TR>
				<TR><TD></TD>
          <TD ALIGN=RIGHT STYLE='VISIBILITY:HIDDEN'>calculation : (B / A) x 100 &nbsp;&nbsp;&nbsp;&nbsp;<b>KPI = </b></TD>
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=KPI%></b></TD></TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</BODY>
</HTML>
