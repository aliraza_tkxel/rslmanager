<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/kpifunctions.asp" -->
<%	
	Const MONTHCOUNT = 12
	
	Dim TID, STARTDATE, ENDDATE, lstBox, kpi, kpikpi, selValue, divisor
	Dim A, B, C, AA, BB, CC
	Dim ytd_let, ytd_due
	Dim final_cnt, final, YTD
	divisor = 1
	
	Redim NO_PROPS(MONTHCOUNT)
	Redim MONTH_RENT(MONTHCOUNT)
	Redim KPI_VALUES(MONTHCOUNT)
	
	selValue = Request("START")
	TID = Request("TOPICID")
	if TID = "" Then TID = -1 End if
	ytd_let = 0
	ytd_due = 0
	' RESET VALUEARRAY
	for x = 0 to MONTHCOUNT 
			KPI_VALUES(x) = 0
			NO_PROPS(x) = 0
			MONTH_RENT(x) = 0
	next
	
	OpenDB()

	get_fiscal_boundary()
	Dmonth = -1
	DYear = -1

	' DETERMINE WHICH DATES TO USE. EITHER FULL FISCAL BOUNDARY OR PARTICULAR MONTH BOUNDARY
	'STARTDATE = Request("START")
	STARTDATE = Request("START")
	If STARTDATE = "" Then 
		STARTDATE = FStart 
		ENDDATE = FEnd
		divisor = DateDiff("m", FStart, date) + 1
		ytd = 1		
	Else
		TempArray = Split(STARTDATE,"_")
		Dmonth = TempArray(0)
		Dyear = TempArray(1)
		STARTDATE = Cdate("1 " & MonthName(Dmonth) & " " & DYear)
		ENDDATE = Cdate(LastDayOfMonth(STARTDATE) & " " & MonthName(Dmonth) & " " & DYear)
	End If	
	
	Call get_kpi_header(TID)
	Call build_fiscal_month_select(lstBox, Dmonth)
	Call get_YTD_average_rent(2)
	Call get_MONTHLY_average_rent(2)
'	Call get_average_rent(2, NO_PROPS, MONTH_RENT)

	for x = 0 to MONTHCOUNT 		' POPULATE YTD VALUES
		ytd_let = ytd_let + NO_PROPS(x)
		ytd_due = ytd_due + MONTH_RENT(x)
		If NO_PROPS(x) = 0 Then KPI_VALUES(x) = 0 Else KPI_VALUES(x) = (MONTH_RENT(x) / NO_PROPS(x)) * 12 / 52 End If
	next

	If YTD = 1 Then		' YEAR TO DATE
		A = final_cnt 
		B = final
		If A = 0 Then C = 0 Else C = (B / A) End If
		If C = 0 Then KPI = 0 Else KPI = C * 12 / 52 End If
		
		A = FormatNumber(A,2)
		B = FormatCurrency(B) 
		C = FormatNumber(C,2)
		KPI = FormatNumber(KPI,2)
		AA = "N/A"
		BB = "N/A"
		CC = "N/A"
		KPIKPI = "N/A"
	Else 
		A = final_cnt 
		B = final
		If A = 0 Then C = 0 Else C = (B / A) End If
		If C = 0 Then KPI = 0 Else KPI = C * 12 / 52 End If
		
		A = FormatNumber(A,2)
		B = FormatCurrency(B) 
		C = FormatNumber(C,2)
		KPI = FormatNumber(KPI,2)

		AA = NO_PROPS(DMonth)
		BB = MONTH_RENT(DMonth)
		If AA = 0 Then CC = 0 Else CC = (BB / AA) End If
		If CC = 0 Then KPIKPI = 0 Else KPIKPI = CC * 12 / 52 End If
		
		AA = FormatNumber(AA,2)
		BB = FormatCurrency(BB,2)
		CC = FormatNumber(CC,2)
		
		KPIKPI = FormatNumber(KPIKPI,2)
		
	End If
		
	CloseDB()
	
	Function get_YTD_average_rent(p_status)
	
		Dim rents_cnt, initials_cnt, rebates_cnt, cancellations_cnt
		Dim rents, initials, rebates, cancellations
		
		if (Date > FEnd) then
			TheDate = CDate(FEnd)
			TheMonth = Month(TheDate)
			TheYear = Year(TheDate)
			TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
			TheEndDate = LastDayofMonth(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
		else
			TheDate = CDate(Date)
			TheMonth = Month(TheDate)
			TheYear = Year(TheDate)		
			TheStartDate = "1 " & MonthName(TheMonth) & " " & TheYear		
			TheEndDate = Day(TheDate) & " " & MonthName(TheMonth) & " " & TheYear		
		end if

		'rw TheStartDate & " -- " & TheEndDate 
		' MONTHLY RENTS
		SQL = "SELECT SUM(ISNULL(CNT,0)) AS CNT, SUM(ISNULL(RENT,0)) AS RENT FROM ( " &_
			 "SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_MONTHLY M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"	AND ASSETTYPE = 1 AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	T.STARTDATE >= '" & TheStartDate & "' AND T.STARTDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.ITEMTYPE = 8 AND M.PAYMENTTYPE IS NULL AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	 -COUNT(*) AS CNT, -(ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0)) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	T.ENDDATE >= '" & TheStartDate & "' AND T.ENDDATE <= '" & TheEndDate & "' AND " &_ 
				"		ASSETTYPE = 1 AND M.PAYMENTTYPE = 12 AND T.TENANCYTYPE IN (1,10)  " &_
			"UNION ALL " &_
			"SELECT 	COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_IR M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & TheStartDate & "' AND M.TRANSACTIONDATE <= '" & TheEndDate & "'" &_
				"		AND ASSETTYPE = 1 AND M.PAYMENTTYPE = 28 AND T.TENANCYTYPE IN (1,10)   ) MEGATABLE "

		'rw SQL		
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF then
			final_cnt = rsSet("CNT")
			final = rsSet("RENT")
		end if
		CloseRs(rsSet)
		
	End Function

	Function get_MONTHLY_average_rent(p_status)
	
		Dim rents_cnt, initials_cnt, rebates_cnt, cancellations_cnt
		Dim rents, initials, rebates, cancellations
		
		' MONTHLY RENTS
		SQL = "SELECT 	MONTH(M.TRANSACTIONDATE) AS MNTH, COUNT(*) AS CNT, ISNULL(SUM(M.RENT),0) + ISNULL(SUM(M.SERVICES),0) AS RENT " &_
				"FROM	F_RENTJOURNAL_MONTHLY M " &_
				"		INNER JOIN P_FINANCIAL F ON F.PROPERTYID = M.PROPERTYID " &_
				"		INNER JOIN P__PROPERTY P ON M.PROPERTYID = P.PROPERTYID " &_
				"		LEFT JOIN F_RENTJOURNAL R ON R.JOURNALID = M.JOURNALID " &_
				"		INNER JOIN C_TENANCY T ON M.TENANCYID = T.TENANCYID " &_
				"WHERE	M.TRANSACTIONDATE >= '" & FStart & "' AND M.TRANSACTIONDATE <= '" & FEnd & "'" &_
				"	AND ASSETTYPE = 1 AND T.TENANCYTYPE IN (1,10) " &_
				"GROUP BY MONTH(M.TRANSACTIONDATE)"
		'rw sql
		Call OpenRs(rsSet, SQL)
		while not rsSet.EOF 
'			Response.Write "m,onth " & rsSet("MNTH") & "  count " & rsSet("CNT") & "<BR>"
			NO_PROPS(rsSet("MNTH")) = rsSet("CNT")
			MONTH_RENT(rsSet("MNTH")) = rsSet("RENT")
			rsSet.Movenext()
		Wend		
		CloseRs(rsSet)
		
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script lanaguage=javascript>

	function redir(SD){
		
		location.href = "frmGNPI01.asp?START="+SD+"&TOPICID=<%=Request("TOPICID")%>&FY=<%=Request("FY")%>";
	
	}
	
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0">
<TABLE CELLPADDING=1 CELLSPACING=2 border=0 WIDTH=100% HEIGHT=360PX ALIGN=CENTER>
	<TR STYLE='HEIGHT:10PX'><TD></TD></TR>
	<TR VALIGN=TOP>
      <TD COLSPAN=3>&nbsp;&nbsp;<b style='font-size:13px'>Topic > <%=strCat%> > <%=strTopic%></B></TD>
	</TR>
	<TR><TD WIDTH=50% HEIGHT=90%>
			<img border=0 valign=center src="../serverside/srvChart.asp?FY=<%=Request("FY")%>&LEGEND=�&JAN=<%=KPI_VALUES(1)%>&FEB=<%=KPI_VALUES(2)%>&MAR=<%=KPI_VALUES(3)%>&APR=<%=KPI_VALUES(4)%>&MAY=<%=KPI_VALUES(5)%>&JUN=<%=KPI_VALUES(6)%>&JUL=<%=KPI_VALUES(7)%>&AUG=<%=KPI_VALUES(8)%>&SEP=<%=KPI_VALUES(9)%>&OCT=<%=KPI_VALUES(10)%>&NOV=<%=KPI_VALUES(11)%>&DEC=<%=KPI_VALUES(12)%>&MINSCALE=60&MAXSCALE=61">
		</TD>
		<TD WIDTH=1%></TD>
		<TD WIDTH=49% VALIGN=TOP>
			
      <TABLE CELLPADDING=1 CELLSPACING=2>
        <TR STYLE='HEIGHT:15PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD STYLE='WIDTH:30PX' height="23"></TD>
          <TD height="23">Month&nbsp;&nbsp;&nbsp; 
            <SELECT NAME='selMonth' class='textbox' ONCHANGE='redir(this.value)' value="<%=selValue%>">
              <%=lstBox%>
            </SELECT>
          </TD>
          <TD height="23" align=center><b>Period</b></TD>
		  <TD>&nbsp;</TD>		  
          <TD height="23" align=center><b>YTD</b></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(A) Properties</TD>
          <TD ALIGN=RIGHT><%=AA%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=A%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(B) Total Rent</TD>
          <TD ALIGN=RIGHT><%=BB%></TD>
		  <TD></TD>
          <TD ALIGN=RIGHT><%=B%></TD>
        </TR>
        <TR STYLE='HEIGHT:15PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>(C) Average Monthly Rent</TD>
          <TD ALIGN=RIGHT><%=CC%></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT><%=C%></TD>
        </TR>
        <TR STYLE='HEIGHT:35PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR>
          <TD></TD>
          <TD ALIGN=RIGHT><b>KPI = </b></TD>
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpikpi%></b></TD>
		  <TD></TD>		  
          <TD ALIGN=RIGHT STYLE='BORDER-BOTTOM:SOLID GREEN 2PX;BORDER-TOP:SOLID GREEN 2PX''><b><%=kpi%></b></TD>
        </TR>
        <TR STYLE='HEIGHT:85PX'>
          <TD COLSPAN=4></TD>
        </TR>
        <TR STYLE='VISIBILITY:HIDDEN'>
          <TD></TD>
          <TD> calculation : C x (12/52)</TD>
          <TD></TD>
          <TD></TD>
        </TR>
      </TABLE>
		</TD>
	</TR>
</TABLE>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ten width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>