<%
	Dim FStart, FEnd, str_SQL, strTopic, strCat
	
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function
	
	' GET FISCAL PERIOD
	Function get_fiscal_boundary()

		SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & Request("FY")		
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then 
			FStart 	= rsSet(0)
			FEnd 	= rsSet(1)
		end if
		CloseRs(rsSet)
	
	End Function	
	
	' GET KPI HEADER DETAILS
	Function get_kpi_header(topicID)
		
		str_SQL = " SELECT 	C.CATEGORY, T.TOPIC FROM 	KPI_TOPIC T " &_
				" INNER JOIN KPI_CATEGORY C ON T.CATEGORYID = C.CATEGORYID WHERE T.TOPICID = " & topicID
		Call OpenRs(rsSet, str_SQL)
		if not rsSet.EOF Then 
			strCat 	 = rsSet(0)
			strTopic = rsSet(1)
		End If
		CloseRs(rsSet)
		
	End Function
	
	Function build_fiscal_month_select(ByRef lst, selectedMonth)
	
		SQL = "SELECT * FROM F_FISCALYEARS WHERE YRANGE = " & Request("FY")		
		Call OpenRs(rsPeriod, SQL)
		PS = CDate(FormatDateTime(rsPeriod("YSTART"),1))
		PE = CDate(FormatDateTime(rsPeriod("YEND"),1))
		Call CloseRs(rsPeriod)

		YearStart = Year(PS)
		YearEnd = Year(PE)
		MonthStart = Month(PS)
		MonthEnd = Month(PE)
		
		'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
		MonthC = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
		lst = "<OPTION VALUE=''>YTD</OPTION>" 
		for i=1 to MonthC
			If cint(MonthStart) = cint(selectedMonth) then isSelected = "selected" Else isSelected = "" End If
			lst = lst & "<OPTION VALUE=""" & MonthStart & "_" & YearStart & """ "&isSelected&">" & MonthName(MonthStart) & " " & YearStart & "</OPTION>"
			MonthStart = MonthStart + 1
			if (MonthStart = 13) then
				MonthStart = 1
				YearStart = YearStart + 1
			end if
		next
	
	End Function

	Function get_monthly_property_count(ByRef p_status, ByRef PROPERTY_COUNT_VALUES)
	
		SQL = 	"SELECT	COUNT(*)  AS MONTHENTRIES, " &_
				"		MONTH(DTIMESTAMP) AS MONTH, " &_
				"		SUM(AMOUNT) AS NOPROPS, " &_
				"		SUM(TOTALVALUE) AS RENT, " &_
				"		(SUM(AMOUNT)/COUNT(*) ) AS AVPERMONTH " &_
				"FROM 	P_DAILYSTATUS S " &_
				"WHERE	S.ASSETTYPEID = 1 AND S.STATUSID = " & Cint(p_status) &_
				"		AND S.DTIMESTAMP >= '" & FStart & "' AND S.DTIMESTAMP <= '" & FEnd & "' " &_
				"GROUP 	BY MONTH(DTIMESTAMP) " &_
				"ORDER   BY MONTH(DTIMESTAMP) "
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("MONTH")) = rsSet("AVPERMONTH")
			rsSet.Movenext()
		Wend
	
	End Function
	
	Dim gn_only
	gn_only = " AND (P.ASSETTYPE = 1 OR P.ASSETTYPE IS NULL) "
	'gn_only = ""
	
	Function get_rent_due(ByRef PROPERTY_COUNT_VALUES, current_only) 
	
		SQL = "SELECT 	MONTH(RJ.TRANSACTIONDATE) AS DMONTH, ISNULL(SUM(RJ.AMOUNT),0) AS RENTDUE " &_
				"FROM 	F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " & ctsql &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE  STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND " &_
				"		PAYMENTTYPE IS NULL " & gn_only &_
				"		AND RJ.TRANSACTIONDATE >= '" & FStart & "' AND RJ.TRANSACTIONDATE <= '" & FEnd & "' " &_
				"GROUP	BY MONTH(RJ.TRANSACTIONDATE) " &_
				"ORDER 	BY MONTH(RJ.TRANSACTIONDATE)"
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("DMONTH")) = rsSet("RENTDUE")
			rsSet.Movenext()
		Wend
	
	End Function
	
	Function get_initial_rent(ByRef PROPERTY_COUNT_VALUES, current_only) 

		SQL = "SELECT 	MONTH(RJ.TRANSACTIONDATE) AS DMONTH, " &_
				"		ISNULL(SUM(RJ.AMOUNT),0) AS INITIAL " &_
				"FROM 	F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 	STATUSID NOT IN (1,4) AND ITEMTYPE = 8 " & gn_only &_
				"		AND RJ.TRANSACTIONDATE >= '" & FStart & "' AND RJ.TRANSACTIONDATE <= '" & FEnd & "' " &_
				"GROUP	BY MONTH(RJ.TRANSACTIONDATE) " &_
				"ORDER 	BY MONTH(RJ.TRANSACTIONDATE)"
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("DMONTH")) = rsSet("INITIAL")
			rsSet.Movenext()
		Wend
	
	End Function

	Function get_rebate(ByRef PROPERTY_COUNT_VALUES, current_only) 
	
		SQL = "SELECT 	MONTH(RJ.TRANSACTIONDATE) AS DMONTH, " &_
				"		ISNULL(SUM(RJ.AMOUNT),0) AS REBATE " &_
				"FROM 	F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 	STATUSID NOT IN (1,4) AND ITEMTYPE = 1 AND PAYMENTTYPE = 12 " & gn_only &_
				"		AND RJ.TRANSACTIONDATE >= '" & FStart & "' AND RJ.TRANSACTIONDATE <= '" & FEnd & "' " &_
				"GROUP	BY MONTH(RJ.TRANSACTIONDATE) " &_
				"ORDER 	BY MONTH(RJ.TRANSACTIONDATE)"
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("DMONTH")) = rsSet("REBATE")
			rsSet.Movenext()
		Wend
	
	End Function
	
	Function get_void_rent(ByRef PROPERTY_COUNT_VALUES) 
	
			SQL = "SELECT MONTH(DTIMESTAMP) AS DMONTH, SUM(TOTALVALUE) AS VOIDRENT " &_
					"FROM 	P_DAILYSTATUS S " &_
					"WHERE	DAY(DTIMESTAMP) = 1 AND ASSETTYPEID = 1 AND STATUSID IN (1,4) " &_
					"		AND S.DTIMESTAMP >= '" & FStart & "' AND S.DTIMESTAMP <= '" & FEnd & "' " &_
					"GROUP  BY DTIMESTAMP " &_
					"ORDER	BY MONTH(DTIMESTAMP) "
		'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("DMONTH")) = rsSet("VOIDRENT")
			rsSet.Movenext()
		Wend
	
	End Function
	
	Function get_payments(ByRef PROPERTY_COUNT_VALUES, current_only) 
	
		If Cint(current_only) = 1 Then ctsql = " AND (T.ENDDATE >= RJ.TRANSACTIONDATE OR T.ENDDATE IS NULL)  " Else ctsql = "" End If

		SQL = "SELECT 	MONTH(RJ.TRANSACTIONDATE) AS DMONTH, " &_
				"		ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTS " &_
				"FROM 	F_RENTJOURNAL RJ " &_
				"		INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " & ctsql &_
				"		INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"WHERE 	STATUSID NOT IN (1,4) AND ITEMTYPE IN (1) AND PAYMENTTYPE NOT IN (12) " & gn_only &_
				"		AND RJ.TRANSACTIONDATE >= '" & FStart & "' AND RJ.TRANSACTIONDATE <= '" & FEnd & "' " &_
				"GROUP 	BY MONTH(RJ.TRANSACTIONDATE) " &_
				"ORDER 	BY MONTH(RJ.TRANSACTIONDATE) "
	'rw sql		
		Call OpenRs(rsSet, SQL)
		While not rsSet.EOF 
			PROPERTY_COUNT_VALUES(rsSet("DMONTH")) = rsSet("PAYMENTS")
			rsSet.Movenext()
		Wend
	
	End Function
%>