<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	' GET FISCAL YEAR
	OpenDB()
		SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & REQUEST("FY")
	Call OpenRs(rsSet, SQL)
	if not rsSet.EOF Then FYear = year(rsSet(0)) & "/" & year(rsSet(1)) end if
	CloseRs(rsSet)
	CloseDB()
	
	Set cd = CreateObject("ChartDirector.API")
	'The data for the bar chart
	data = Array(Request("APR"),Request("MAY"),Request("JUN"),Request("JUL"),Request("AUG"),Request("SEP"),Request("OCT"),Request("NOV"),Request("DEC"),Request("JAN"),Request("FEB"),Request("MAR"))
	'The labels for the bar chart
	labels = Array("A","M","J","J","A","S","O","N","D","J","F","M")
	'Create a XYChart object of size 250 x 250 pixels
	Set c = cd.XYChart(355, 300)
	'Set the plotarea at (30, 20) and of size 200 x 200 pixels
	Call c.setPlotArea(40, 5, 310, 257)
	'Add a bar chart layer using the given data
	Call c.addBarLayer(data)
	'Set the x axis labels using the given labels
	Call c.xAxis().setLabels(labels)
	Call c.addText(1,95, Request("LEGEND"), "bold", 12)
	Call c.addText(140,280, FYear, "bold", 12)
	'output the chart
	Response.ContentType = "image/png"
	Response.BinaryWrite c.makeChart2(cd.GIF)
	Response.End
%>

