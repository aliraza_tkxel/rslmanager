<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc		   (3)
	Dim searchName, searchSQL
	
	searchName = Replace(Request("name"),"'","''")
	
	if searchName <> "" Then
		searchSQL = " AND (FIRSTNAME LIKE '%" & searchName & "%' OR LASTNAME LIKE '%" & searchName & "%') "
	Else
		searchName = ""
	End If

	ColData(0)  = "Name|FULLNAME|190"
	SortASC(0) 	= "FIRSTNAME ASC, LASTNAME ASC"
	SortDESC(0) = "FIRSTNAME DESC, LASTNAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""	

	ColData(1)  = "Team|TEAM|190"
	SortASC(1) 	= "TEAM ASC"
	SortDESC(1) = "TEAM DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Job Title|JOBTITLE|190"
	SortASC(2) 	= "JOBTITLE ASC"
	SortDESC(2) = "JOBTITLE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Grade|GRADE|80"
	SortASC(3) 	= "J.GRADE ASC"
	SortDESC(3) = "J.GRADE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	PageName = "UserList.asp"
	EmptyText = "No employees found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""EMPLOYEEID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE =	"SELECT FIRSTNAME + ' ' + LASTNAME AS FULLNAME, " &_
			"			TEAM = CASE  " &_
			"			WHEN T.TEAMID IS NULL THEN 'No Team' " &_
			"			ELSE T.TEAMNAME " &_						
			"			END, " &_
			"			ISNULL(O.NAME, '') AS ORGNAME, " &_			
			"			E.EMPLOYEEID, " &_
			"			ISNULL(J.JOBTITLE, 'N/A') AS JOBTITLE, " &_
			"			ISNULL(CAST (J.GRADE AS NVARCHAR), 'N/A') AS GRADE " &_
			"FROM	 	E__EMPLOYEE E " &_
			"			LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID " &_
			"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
			"			LEFT JOIN E_TEAM T ON T.TEAMID = J.TEAM " &_
			"			LEFT JOIN E_GRADE G ON J.GRADE = G.GRADEID " &_
			"			WHERE E.ORGID IS NULL " & searchSQL &_
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --> HR Tools --> Employee Profiles</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(employee_id){
	location.href = "AccessControl/AccessControl.asp?EmployeeId=" + employee_id;
	}
	
	function quick_find(){

		location.href = "userlist.asp?name="+thisForm.findemp.value;
		
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2><IMG NAME="tab_employees" TITLE='Employees' SRC="images/tab_employees.gif" WIDTH=94 HEIGHT=20 BORDER=0></TD>
		<!--TD><IMG SRC="images/spacer.gif" WIDTH=656 HEIGHT=19></TD-->
		<TD ALIGN=RIGHT>
		<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0><TR>
				<TD ALIGN=RIGHT>
					<INPUT TYPE='BUTTON' name='btn_FIND' title='Search for matches using both first and surname.' CLASS='RSLBUTTON' VALUE='Quick Find' ONCLICK='quick_find()'>
					&nbsp;<input type=text  size=18 name='findemp' class='textbox200'>
				</TD></TR>
			</TABLE>
		</TD>	
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=655 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>