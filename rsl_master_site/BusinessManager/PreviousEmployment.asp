<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION

ACTION = "NEW"
EmployeeID = Request("EmployeeID")
If (EmployeeID = "") Then EmployeeID = -1 End If

Call OpenDB()
SQL = "SELECT * FROM E_PREVIOUSEMPLOYMENT WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsLoader, SQL)

SQL = "SELECT * FROM E__EMPLOYEE WHERE EMPLOYEEID = " & EmployeeID
Call OpenRs(rsName, SQL)

If NOT rsName.EOF Then
	l_lastname = rsName("LASTNAME")
	l_firstname = rsName("FIRSTNAME")
	FullName = "<font color=blue>" & l_firstname & " " & l_lastname & "</font>"
End If
Call CloseRs(rsName)

strTable = strTable & "<table cellspacing=""0"" cellpadding=""1"" style=""behavior:url(/Includes/Tables/tablehl.htc)"" slcolor='' hlcolor='STEELBLUE'>"
strTable = strTable & "<thead>"
strTable = strTable & "<tr>"
strTable = strTable & "<td width=""190px""><b>Employer</b></td>"
strTable = strTable & "<td width=""80px""><b>Start Date</b></td>"
strTable = strTable & "<td width=""80px""><b>End Date</b></td>"
strTable = strTable & "</tr>"
strTable = strTable & "<tr>"
strTable = strTable & "<td colspan=""4"">"
strTable = strTable & "<hr style=""border:1px dotted #133E71"" />"
strTable = strTable & "</td>"
strTable = strTable & "</tr>"
strTable = strTable & "</thead>"
strTable = strTable & "<tbody>"
If (not rsLoader.EOF) Then
	while (NOT rsLoader.EOF)
		special = rsLoader("Reason")
		titlebit = ""
		If (special <> "" AND Not isNull(special)) Then
			titlebit = " title='Reasons for Leaving:" & special & "'"
		End If
		strTable = strTable & "<tr style=""cursor:pointer""><td onclick=""EditMe(" & rsLoader("PREVIOUSEMPLOYMENTID") & ")"" valign=""top"" " & titlebit & ">" & rsLoader("Employer") & "</td>"	
		strTable = strTable & "<td valign=""top"">" & rsLoader("StartDate") & "</td>"
		strTable = strTable & "<td valign=""top"">" & rsLoader("EndDate") & "</td>"
        strTable = strTable & "<td onclick=""DeleteMe(" & rsLoader("PREVIOUSEMPLOYMENTID") & ")""><font color=""red"">DEL</font></td>"
        strTable = strTable & "</tr>"
		rsLoader.moveNext
	wend
Else
	strTable = strTable & "<tr>"
    strTable = strTable & "<td colspan=""3"" align=""center"">No Previous Employers</td>"
    strTable = strTable & "</tr>"
End If
strTable = strTable & "</tbody>"
strTable = strTable & "<tfoot>"
strTable = strTable & "<tr>"
strTable = strTable & "<td colspan=""4"">"
strTable = strTable & "<hr style=""border:1px dotted #133e71"" />"
strTable = strTable & "</td>"
strTable = strTable & "</tr>"
strTable = strTable & "</tfoot>"
strTable = strTable & "</table>"
Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Business --&gt; HR Tools --&gt; Employee Setup</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/Calendar.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array();
        FormFields[0] = "txt_STARTDATE|Start Date|DATE|Y"
        FormFields[1] = "txt_ENDDATE|End Date|DATE|Y"
        FormFields[2] = "txt_EMPLOYER|Company name|TEXT|Y"
        FormFields[3] = "txt_REASON|Reasons for leaving|TEXT|N"

        function SaveForm() {
            if (!checkForm()) return false;
            //document.RSLFORM.hid_Action.value = "NEW"
            document.RSLFORM.submit()
        }

        function ResetForm() {
            document.RSLFORM.hid_PREVEMPID.value = ""
            document.RSLFORM.hid_Action.value = "NEW"
            document.RSLFORM.myButton.value = " ADD "
            document.RSLFORM.reset()
        }

        function DeleteMe(THE_ID) {
            document.RSLFORM.hid_PREVEMPID.value = THE_ID
            document.RSLFORM.hid_Action.value = "DELETE"
            document.RSLFORM.submit()
        }

        function EditMe(THE_ID) {
            document.RSLFORM.hid_PREVEMPID.value = THE_ID
            document.RSLFORM.hid_Action.value = "LOAD"
            document.RSLFORM.submit()
        }
    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages();checkForm()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <a href="NewEmployee.asp?EmployeeID=<%=EmployeeID%>" title="Personal Details">
                    <img title="Personal Details" name="E_PD" src="Images/1-closed.gif" width="115" height="20"
                        border="0" alt="Personal Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Contact.asp?EmployeeID=<%=EmployeeID%>" title="Contact Info">
                    <img name="E_C" src="images/2-closed.gif" width="74" height="20" border="0" alt="Contacts" /></a>
            </td>
            <td rowspan="2">
                <a href="Qualifications.asp?EmployeeID=<%=EmployeeID%>" title="Skills & Qualifications">
                    <img name="E_SQ" src="images/3-closed.gif" width="94" height="20"
                        border="0" alt="Qualifications" /></a>
            </td>
            <td rowspan="2">
                <a href="PreviousEmployment.asp?EmployeeID=<%=EmployeeID%>" title="Previous Employment">
                    <img name="E_PE" src="Images/4-open.gif" width="86" height="20"
                        border="0" alt="Previous Employment" /></a>
            </td>
            <td rowspan="2">
                <a href="Disabilities.asp?EmployeeID=<%=EmployeeID%>" title="Difficulties/Disabilities">
                    <img name="E_DD" src="Images/5-previous.gif" width="70"
                        height="20" border="0" alt="Difficulties/Disabilities" /></a>
            </td>
            <td rowspan="2">
                <a href="JobDetails.asp?EmployeeID=<%=EmployeeID%>" title="Job Details">
                    <img name="E_JD" src="Images/6-closed.gif" width="52" height="20"
                        border="0" alt="Job Details" /></a>
            </td>
            <td rowspan="2">
                <a href="Benefits.asp?EmployeeID=<%=EmployeeID%>" title="Benefits">
                    <img name="E_B" src="images/7-closed.gif" width="82" height="20" border="0" alt="Benefits" /></a>
            </td>
            <td width="177" height="19" align="right">
                <%=FullName%>
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="53" height="1" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="13" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="images/spacer.gif" width="53" height="6" alt="" />
            </td>
        </tr>
    </table>
    <form name="RSLFORM" method="post" action="ServerSide/PreviousEmployment_svr.asp" target="hiddenFrame">
    <table cellspacing="2" style="border-left: 1px solid #133E71; border-bottom: 1px solid #133E71;
        border-right: 1px solid #133E71" width="750">        
        <tr>
            <td nowrap="nowrap">
                Start Date:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_STARTDATE" id="txt_STARTDATE" maxlength="10" value="" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_STARTDATE" id="img_STARTDATE" width="15px" height="15px" border="0" alt="" />
            </td>
            <td width="100%" rowspan="10" valign="top">
                <br/>
                <div id="ListRows">
                    <%=strTable%></div>
                <div align="right">
                    <input type="button" value=" NEXT " class="RSLBUTTON" onclick="Javascript:location.href='disabilities.asp?EmployeeID=<%=EmployeeID%>'"
                        tabindex="14" />
                </div>
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                End Date:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_ENDDATE" id="txt_ENDDATE" maxlength="10" value="" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_ENDDATE" id="img_ENDDATE" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                Company Name:
            </td>
            <td>
                <input type="text" class="textbox200" name="txt_EMPLOYER" id="txt_EMPLOYER" maxlength="100" value="" />
            </td>
            <td>
                <img src="/js/FVS.gif" name="img_EMPLOYER" id="img_EMPLOYER" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td nowrap="nowrap" valign="top">
                Reasons for Leaving:
            </td>
            <td>
                <textarea class="textbox200" name="txt_REASON" id="txt_REASON" cols="10" rows="6"></textarea>
            </td>
            <td valign="top">
                <img src="/js/FVS.gif" name="img_REASON" id="img_REASON" width="15px" height="15px" border="0" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <input type="hidden" name="hid_EMPLOYEEID" id="hid_EMPLOYEEID" value="<%=EmployeeID%>" />
                <input type="hidden" name="hid_Action" id="hid_Action" value="<%=ACTION%>" />
                <input type="hidden" name="hid_PREVEMPID" id="hid_PREVEMPID" value="" />
                <input class="RSLButton" name="myButton" id="myButton" type="button" value=" ADD " onclick="SaveForm()"
                    tabindex="12" />
                <input type="button" class="RSLButton" value=" RESET " onclick="ResetForm()" tabindex="13" />
            </td>
        </tr>
        <tr>
            <td height="100%">
                &nbsp;
            </td>
        </tr>        
    </table>
    </form>
    <iframe src="/secureframe.asp" name="hiddenFrame" style='display: none'></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
