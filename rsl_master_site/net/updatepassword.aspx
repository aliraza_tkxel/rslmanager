<%@ Page Language="VB" AutoEventWireup="false" CodeFile="updatepassword.aspx.vb" Inherits="updatepassword" Trace="false" EnableEventValidation="false" viewStateEncryptionMode ="Never" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <link rel="stylesheet" type="text/css" href="update.css" />     

   
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>rslManager</title>
</head>
<body>
    <div id="header">
    
    </div>
    <div id="body">

    <div id="bodyinner">
        <div id="tab"></div>
        <div id="tabtitle">Log on</div>


    
    <form id="form1" runat="server">
    <div id="formgroup">
    Please update your password<br /><br />
            <asp:PlaceHolder  ID="plformgroup" runat="server">
        </asp:PlaceHolder> 
        <asp:Button ID="Button1" CssClass="RSLButton" runat="server" Text="Update" PostBackUrl="~\net\updatepassword.aspx" />
        <br /><br />
        <asp:Label ID="lblstatus" runat="server" BackColor="Transparent" ForeColor="Red"></asp:Label>
        <asp:ValidationSummary  ID="ValidationSummary1" runat="server" HeaderText="There were the following problems with your form:" />
    </div>
    </form>
       </div></div>
    <div id="footer">
    </div>
</body>
</html>
