Imports System.Data
Imports reidmark.platform.helpers.brokers.formbuilder
Imports reidmark.platform.security.passwordmanager



Partial Class updatepassword
    'Inherits System.Web.UI.Page
    'Inherits MSDN.SessionPage
    Inherits basepage

    Dim login As login.clsLogin

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim formbuilder As New clsformbuilder
        formbuilder.xmlconfig = "App_Data/updatelogin.xml"
        formbuilder.xmlcachename = "xml_update_login_config"
        formbuilder.buildform()
    End Sub
    Sub setsession(ByVal ds As DataSet)
        Dim isContractor As Integer
        Dim strTeamCode As String
        Dim rows As Data.DataRowCollection
        rows = ds.Tables(0).Rows()

        Session("FirstName") = rows(0).Item("FIRSTNAME")
        Session("LastName") = rows(0).Item("LASTNAME")
        Session("USERID") = rows(0).Item("EMPLOYEEID")
        Session("TeamCode") = rows(0).Item("TeamCode")

        strTeamCode = rows(0).Item("TEAMCODE")
        Session("TEAMNAME") = rows(0).Item("TEAMNAME")
        If Not IsDBNull(rows(0).Item("ORGID")) Then
            isContractor = rows(0).Item("ORGID")
        End If

        'Dim str_redirect As String

        If Not IsDBNull(rows(0).Item("ORGID")) Then
            Session("ORGID") = isContractor
            Response.Redirect("/MyWeb/MyWhiteboard.asp")
            'str_redirect = "/MyWeb/MyWhiteboard.asp"
        Else
            If strTeamCode = "PMA" Then
                Response.Redirect("/BoardMembers/BM.asp")
                'str_redirect = "/MyWeb/MyWhiteboard.asp"
            Else
                Response.Redirect("/MyWeb/MyWhiteboard.asp")
                'str_redirect = "/MyWeb/MyWhiteboard.asp"
            End If
        End If


        'Dim strScript As String = "<script language=JavaScript>"
        'strScript += "window.top.location=""" & str_redirect & """;"
        'strScript += "</script>"

        'Me.ClientScript.RegisterStartupScript( _
        '   GetType(String), "ShowInfoPage", strScript.ToString())





    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim settings As New ConnectionStringSettings
        settings = ConfigurationManager.ConnectionStrings("connRSL")
        Dim intstatus As Integer
        Dim logutil As LoginUtility = New LoginUtility
        Dim GenericObject As Object

        If Page.IsValid Then



            GenericObject = logutil.deserialize_object(Session("login").ToString)
            login = CType(GenericObject, login.clsLogin)

            login.xmlconfig = "App_Data/updatelogin.xml"
            login.xmlcachename = "xml_update_login_config"
            login.conn = settings.ConnectionString.ToString
            intstatus = login.update_login()

            If intstatus = 0 Then
                lblstatus.Text = "Your old password does not appear to be correct"
            Else
                lblstatus.Text = ""

                Dim ds As DataSet
                GenericObject = logutil.deserialize_object(Session("ds").ToString)
                ds = CType(GenericObject, DataSet)
                setsession(ds)
            End If

        End If
    End Sub
End Class
