<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="accessible.aspx.vb" Inherits="_Default" Trace="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>rslManager</title>
    <link rel="stylesheet" type="text/css" href="Stylesheet.css" />
     <link rel="stylesheet" type="text/css"  href="nostyle.css"/>
<script language="JavaScript" type="text/javascript">
<!--
var doAlerts=true;
function changeSheets(whichSheet){
  whichSheet=whichSheet-1;
  if(document.styleSheets){
    var c = document.styleSheets.length;
    //if (doAlerts) alert('Change to Style '+(whichSheet+1));
    for(var i=0;i<c;i++){
      if(i!=whichSheet){
        document.styleSheets[i].disabled=true;
      }else{
        document.styleSheets[i].disabled=false;
      }
    }
  }
}
//-->
</script>
</head>
<body>


    <div id="header">
    
    </div>
    <div id="body">

    <div id="bodyinner">
        <div id="tab"></div>
        <div id="tabtitle">Log on</div>


    <form id="form1" runat="server">
    <div id="formgroup">
        &nbsp;Please enter your designated user name and password.<br /><br />
   
   

            
        <asp:PlaceHolder ID="plformgroup" runat="server">
        </asp:PlaceHolder>
         <asp:Button ID="Button1" runat="server" Text="Login" CssClass="RSLButton"  /><br />
         <br />
    <asp:Label ID="lblstatus" runat="server" CssClass="pad"></asp:Label><asp:Panel ID="pnlexpiry" CssClass="pad" Visible="false" runat="server" Height="50px" Width="550px"><br />
      <asp:Label ID="lblexpiry" runat="server"></asp:Label>
        <br />
      <br /><asp:LinkButton ID="lnkexpiry" CssClass="linkbutton" runat="server" PostBackUrl="updatepassword.aspx">Click here to update your password.</asp:LinkButton>&nbsp;&nbsp;
      <br /><asp:Panel ID="pnlEnter" style="margin-top:10px" runat="server" Height="21px" Width="125px" Visible="False">
        <asp:LinkButton ID="lnkenter" Text="Click here to enter" CssClass="linkbutton" runat="server" CausesValidation="False"></asp:LinkButton></asp:Panel>
    </asp:Panel> 
     <asp:ValidationSummary ID="valSum" runat="server"
        HeaderText="There were the following problems:" CssClass="valsum" 
        />
   
    <br />       
        <asp:PlaceHolder ID="plupdatepassword" runat="server">
        </asp:PlaceHolder>  &nbsp;&nbsp;
       
    <div id="accessible" style="display:block">
    <a href="JavaScript:changeSheets(1)">Style One</a>
    <a href="JavaScript:changeSheets(2)">Style Two</a><br />
    </div>
        <br />
        <br />
        <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="/misplaced.asp">Forgot your password</asp:LinkButton>
        <br /><br />
        Do you need a login -
        <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="http://www.reidmark.com/contactus.asp">Click here</asp:LinkButton>
        - to request a login from our RSL Manager team
         </div> </form></div>
    </div>
    <div id="footer">
    </div>
</body>
</html>
