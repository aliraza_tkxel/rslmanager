<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->

<HTML>
<HEAD>
<TITLE>Messages</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
function DoFunction(){
	window.focus();
	<% if (Request("RELOAD") = "1") then %>
	try {
		openerfile = opener.location.href.toLowerCase()
		if (openerfile.indexOf("mywhiteboard.asp") != -1)
			opener.location.href="/BoardMembers/Messages.asp"
		if (openerfile.indexOf("contractorwhiteboard.asp") != -1)
			opener.location.href="/BoardMembers/Messages.asp"
		if (openerfile.indexOf("messages.asp") != -1)
			opener.location.href="/BoardMembers/Messages.asp"
		}
	catch (e){
		ignoreError = true
		}
	<% end if %>
	}
	
function CloseWindow(){

window.close()
}
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="DoFunction()">
<TABLE WIDTH=371 BORDER=0 CELLPADDING=0 CELLSPACING=0>
  <form name="RSLFORM" method="POST">
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
      <TD COLSPAN=21><IMG SRC="images/spacer.gif" WIDTH=359 HEIGHT=6 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=6 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=33><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=233 ALT=""></TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD COLSPAN=19 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=357 HEIGHT=1 ALT=""></TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD ROWSPAN=33><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=233 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=31 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=224 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=8 ALT=""></TD>
      <TD COLSPAN=17></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=8 ALT=""></TD>
      <TD ROWSPAN=31 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=224 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=29>&nbsp;</TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD COLSPAN=15 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=343 HEIGHT=1 ALT=""></TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD ROWSPAN=29>&nbsp;</TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=5 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=19 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=3 ALT=""></TD>
      <TD COLSPAN=2><IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=3 ALT=""></TD>
      <TD COLSPAN=5><IMG SRC="images/spacer.gif" WIDTH=92 HEIGHT=3 ALT=""></TD>
      <TD COLSPAN=6 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=217 HEIGHT=4 ALT=""></TD>
      <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=4 ALT=""></TD>
      <TD ROWSPAN=5 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=19 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=12 ALT=""></TD>
      <TD ROWSPAN=2><IMG SRC="images/im_envelope.gif" WIDTH=17 HEIGHT=12 ALT=""></TD>
      <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=8 HEIGHT=12 ALT=""></TD>
      <TD COLSPAN=5 ROWSPAN=3><IMG SRC="images/im_messagepad-.gif" WIDTH=92 HEIGHT=15 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD COLSPAN=2>&nbsp;</TD>
      <TD><IMG NAME="im_close" SRC="images/im_close.gif" WIDTH=37 HEIGHT=11 ALT="" style="cursor:hand" onClick="CloseWindow()"></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
      <TD COLSPAN=2><A HREF="#"
				ONMOUSEOVER="changeImages('im_close', 'images/im_close-im_closebutton_ove.gif', 'im_closebutton', 'images/im_closebutton-over.gif'); return true;"
				ONMOUSEOUT="changeImages('im_close', 'images/im_close.gif', 'im_closebutton', 'images/im_closebutton.gif'); return true;"> <IMG NAME="im_closebutton" SRC="images/im_closebutton.gif" WIDTH=11 HEIGHT=11 BORDER=0 ALT="" style="cursor:hand" onClick="CloseWindow()"></A></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=11 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
    </TR>
    <TR>
      <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=4 ALT=""></TD>
      <TD COLSPAN=2 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=4 ALT=""></TD>
      <TD COLSPAN=6 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=217 HEIGHT=4 ALT=""></TD>
      <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=4 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
    </TR>
    <TR>
      <TD COLSPAN=5><IMG SRC="images/spacer.gif" WIDTH=92 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD COLSPAN=15 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=343 HEIGHT=1 ALT=""></TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD COLSPAN=17><IMG SRC="images/spacer.gif" WIDTH=345 HEIGHT=11 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
    </TR>
    <TR>
      <TD COLSPAN=17 ROWSPAN=21 align="center" valign="middle" bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=4 ALT=""><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=3 ALT=""><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=2 ALT=""><%=Request("Text")%>
        <% if Request("Text") = "Message saved successfully" then %>
        <br>
        <br>
        <a href="PopMessage.asp"><font color=blue>Add Another</font></a>
      <% end if %></TD>
      <TD height="20"><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=76 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=2 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
    </TR>
    <TR>
      <TD height="1"><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4 ALT=""></TD>
    </TR>
    <TR>
      <TD></TD>
    </TR>
    <TR>
      <TD colspan="19"><IMG SRC="images/spacer.gif" WIDTH=11 HEIGHT=11 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
    </TR>
    <TR>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
      <TD COLSPAN=19 BGCOLOR=#669900></TD>
      <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
      <TD COLSPAN=21><!-- #include virtual="Includes/Bottoms/BlankBottom.html" --></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=6 ALT=""></TD>
    </TR>
    <TR>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=17 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=8 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=51 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=15 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=125 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=43 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=37 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=10 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
      <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
      <TD></TD>
    </TR>
  </FORM>
</TABLE>
<iframe src="/secureframe.asp"  name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

