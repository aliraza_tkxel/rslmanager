<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim rsTeams
OpenDB()
SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM ORDER BY TEAMNAME"
Call OpenRs(rsTeams, SQL)
%>
<HTML>
<HEAD>
<TITLE>Add Message</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL_green.css" type="text/css">
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidationExtras.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
window.focus();
	
var FormFields = new Array()
FormFields[0] = "txt_TITLE|Title|TEXT|Y"						
FormFields[1] = "txt_MESSAGE|Message|TEXT|Y"						
FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
FormFields[3] = "sel_MESSAGEFOR|For|SELECT|Y"		
FormFields[4] = "txt_EXPIRES|Expires|INTEGER|I_Y"		

function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/Message_svr.asp"
	RSLFORM.submit()
	}

function GetContractorContacts() {
	if (RSLFORM.sel_MESSAGEFOR.value != "ALLORG" && RSLFORM.sel_MESSAGEFOR.value != "") {
		RSLFORM.action = "Serverside/GetContractors.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else 
		SetExpiry()
	}

function LoadPrevious() {
	if (RSLFORM.sel_MESSAGEFOR.value == "PREVIOUS") {
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else
		SetExpiry()
	}
		
function GetEmployees(){
	if (RSLFORM.sel_TEAMS.value == "ALL") {
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value='ALL'>All Employees</option></select>"
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('4')
		}
	else if (RSLFORM.sel_TEAMS.value != "") {
		DisableItems('4')				
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()
		}
	else {
		DisableItems('4')								
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value=''>Please select a team</option></select>"
		}
	}

function SetExpiry(){
	if (RSLFORM.sel_MESSAGEFOR.value == "ALL" || RSLFORM.sel_MESSAGEFOR.value == "ALLORG") {
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('4')
		}
	else {
		DisableItems('4')
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		}
	}	
	
function CloseWindow(){

window.close()
}
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6>

<TABLE WIDTH=371 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<form name="RSLFORM" method="POST">
  <TR>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
    <TD COLSPAN=21><IMG SRC="images/spacer.gif" WIDTH=359 HEIGHT=6 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=6 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=33><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=233 ALT=""></TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=19 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=357 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD ROWSPAN=33><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=233 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=31 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=224 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=8 ALT=""></TD>
    <TD COLSPAN=17></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=8 ALT=""></TD>
    <TD ROWSPAN=31 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=224 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=29>&nbsp;</TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=15 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=343 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD ROWSPAN=29>&nbsp;</TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=5 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=19 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=2><IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=5><IMG SRC="images/spacer.gif" WIDTH=92 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=6 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=217 HEIGHT=4 ALT=""></TD>
    <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=4 ALT=""></TD>
    <TD ROWSPAN=5 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=19 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=12 ALT=""></TD>
    <TD ROWSPAN=2><IMG SRC="images/im_envelope.gif" WIDTH=17 HEIGHT=12 ALT=""></TD>
    <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=8 HEIGHT=12 ALT=""></TD>
    <TD COLSPAN=5 ROWSPAN=3><IMG SRC="images/im_messagepad-.gif" WIDTH=92 HEIGHT=15 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=2>&nbsp;</TD>
    <TD><IMG NAME="im_close" SRC="images/im_close.gif" WIDTH=37 HEIGHT=11 ALT="" style="cursor:hand" onClick="CloseWindow()"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
    <TD COLSPAN=2><IMG NAME="im_closebutton" SRC="images/im_closebutton.gif" WIDTH=11 HEIGHT=11 BORDER=0 ALT="" style="cursor:hand" onClick="CloseWindow()"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=11 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
  </TR>
  <TR>
    <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=4 ALT=""></TD>
    <TD COLSPAN=2 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=25 HEIGHT=4 ALT=""></TD>
    <TD COLSPAN=6 ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=217 HEIGHT=4 ALT=""></TD>
    <TD ROWSPAN=2><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=4 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5><IMG SRC="images/spacer.gif" WIDTH=92 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=15 BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=343 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=17><IMG SRC="images/spacer.gif" WIDTH=345 HEIGHT=11 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 ROWSPAN=3 bgcolor="#FFFFFF">Title</TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=2 ROWSPAN=3 bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD bgcolor="#FFFFFF" >&nbsp;</TD>
    <TD height="20" COLSPAN=8 align="left" bgcolor="#FFFFFF"><input name="txt_TITLE" type="text" class="textbox200" id="MessageTitle" maxlength="24" STYLE='WIDTH:280PX'></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
  </TR>
  <TR>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=55 HEIGHT=4 ALT=""></TD>
    <TD COLSPAN=10 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=4 ALT=""></TD>
    <TD COLSPAN=2 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=4 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 ROWSPAN=3 valign="top" bgcolor="#FFFFFF">Message</TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=2 ROWSPAN=3 bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=76 ALT=""></TD>
    <TD COLSPAN=8 align="left" bgcolor="#FFFFFF"><textarea name="txt_MESSAGE" rows="3" wrap="on" class="textbox200" STYLE="overflow:hidden;WIDTH:280PX;height:74"
								onKeyUp="if (this.value.length>500) { alert('Please enter no more than 500 chars'); this.value=this.value.substring(0,499) }"
								></textarea></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=76 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=76 ALT=""></TD>
  </TR>
  <TR>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=55 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=10 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=2 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=3 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 ROWSPAN=3 bgcolor="#FFFFFF">Team</TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD bgcolor="#FFFFFF" ><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=2 ROWSPAN=3 bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    <TD COLSPAN=8 align="left" bgcolor="#FFFFFF"><select name="sel_TEAMS" class="textbox200" onchange="GetEmployees()" STYLE='WIDTH:280PX'>
      <option value="">Please Select</option>
      <option value="ALL">All Teams</option>
      <%
									While (NOT rsTeams.EOF)
									%>
      <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
      <%
									  rsTeams.MoveNext()
									Wend
									If (rsTeams.CursorType > 0) Then
									  rsTeams.MoveFirst
									Else
									  rsTeams.Requery
									End If
									CloseRs(rsTeams)
									CloseDB()
									%>
      <option value='NOT'>No Team</option>
    </select></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=55 HEIGHT=2 ALT=""></TD>
    <TD COLSPAN=10 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=2 ALT=""></TD>
    <TD COLSPAN=2 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=2 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=2 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 ROWSPAN=3 bgcolor="#FFFFFF">Person</TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=2 ROWSPAN=3 bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_MESSAGEFOR" width="15px" height="15px" border="0"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    <TD COLSPAN=8 align="left" bgcolor="#FFFFFF"><select name="sel_MESSAGEFOR" class="textbox200" STYLE='WIDTH:280PX'>
      <option value="">Please select a team</option>
    </select></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=16 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=8 BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=283 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=55 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=10 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=285 HEIGHT=3 ALT=""></TD>
    <TD COLSPAN=2 bgcolor="#FFFFFF"><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=3 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=3 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=5 ROWSPAN=3 bgcolor="#FFFFFF">Expires</TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=51 HEIGHT=1 ALT=""></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD COLSPAN=7 ROWSPAN=3 bgcolor="#FFFFFF">&nbsp;&nbsp; Days 
      <input name="hid_CREATEDBY" type="hidden" value="<%= Session("UserID") %>">
      <input name="hid_ACTION" type="hidden" value="NEW">
      <input name="hid_ORGID" type="hidden" value=""></TD>
    <TD COLSPAN=2 ROWSPAN=3 bgcolor="#FFFFFF"><image src="/js/FVS.gif" name="img_EXPIRES" width="15px" height="15px" border="0"></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
    <TD align="left" bgcolor="#FFFFFF"><input type="text" name="txt_EXPIRES" value="Not Applicable" class="textbox200" STYLE='WIDTH:45PX' disabled></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=14 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD height="1" BGCOLOR=#FFFFFF></TD>
    <TD BGCOLOR=#FFFFFF><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=15 bgcolor="#FFFFFF">&nbsp;</TD>
    <TD COLSPAN=2 ROWSPAN=2 bgcolor="#FFFFFF">&nbsp;</TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4 ALT=""></TD>
  </TR>
  <TR>
    <TD COLSPAN=15 bgcolor="#FFFFFF"><IMG NAME="im_sendamessage" SRC="images/im_newmessage.gif" WIDTH=77 HEIGHT=11 ALT=""  style="cursor:hand" onClick="SaveForm()"> <IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""> <IMG NAME="im_rightbutton" SRC="images/im_rightbutton.gif" WIDTH=11 HEIGHT=11 BORDER=0 ALT="" style="cursor:hand" onClick="SaveForm()"></A></TD>
    <TD></TD>
  </TR>
  <TR>
    <TD colspan="19"><IMG SRC="images/spacer.gif" WIDTH=11 HEIGHT=11 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""></TD>
  </TR>
  <TR>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
    <TD COLSPAN=19 BGCOLOR=#669900></TD>
    <TD BGCOLOR=#669900><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=8 ALT=""></TD>
  </TR>
  <TR>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
    <TD COLSPAN=21>  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" --></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=6 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=6 ALT=""></TD>
  </TR>
  <TR>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=5 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=17 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=8 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=51 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=15 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=125 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=43 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=37 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=10 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=1 ALT=""></TD>
    <TD><IMG SRC="images/spacer.gif" WIDTH=6 HEIGHT=1 ALT=""></TD>
    <TD></TD>
  </TR>
  </FORM>
</TABLE>
<iframe src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

