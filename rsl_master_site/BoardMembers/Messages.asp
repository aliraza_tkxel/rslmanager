<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

ORGID = SESSION("ORGID")
		IF (ORGID = "") THEN
			ORGID = ""
		END IF

SQL = "SELECT  M.MESSAGEID, M.DATECREATED AS DATESENT, E.FIRSTNAME + ' ' + E.LASTNAME AS SENTBY ,M.MESSAGE, M.TITLE FROM dbo.G_MESSAGES M " &_
			" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY  " &_
			"WHERE " &_
			"((M.MESSAGEFOR = '" & Session("UserID") & "') " &_ 
			"OR (M.MESSAGEFOR = 'TE" & TheTeamID & "' AND M.EXPIRES >= GETDATE() AND M.ORGID IS NULL) " &_ 
			"OR (M.MESSAGEFOR = 'ALL' AND M.EXPIRES >= GETDATE() AND '" & ORGID & "1' = '1') " &_ 
			"OR (M.MESSAGEFOR = 'ALLORG' AND M.EXPIRES >= GETDATE() AND '" & ORGID & "1' <> '1') " &_ 
			"OR (M.MESSAGEFOR = 'TE" & TheTeamID & "' AND M.EXPIRES >= GETDATE() AND M.ORGID = '" & ORGID & "')) " &_ 
			"AND M.ACTIVE = 1 ORDER BY M.DATECREATED DESC, M.TITLE"

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMessages = Server.CreateObject("ADODB.Recordset")
rsMessages.ActiveConnection = RSL_CONNECTION_STRING
rsMessages.Source = SQL
rsMessages.CursorType = 0
rsMessages.CursorLocation = 2
rsMessages.LockType = 3
rsMessages.Open()
rsMessages_numRows = 0


	while not rsMessages.eof 

     	 ArchiveRow = 	ArchiveRow &  "<tr><td></td><td></td><td height='5'></td><td></td></tr>" &_
									 "<tr>" &_
									 "     <td width='10'></td>" &_
									 "     <td width='3'></td>" &_
									 "     <td width='200' height='16'><a href='#' onClick='MM_openBrWindow(""/MyTasks/PopMessageDisplay.asp?MessageID=" &  rsMessages("MessageID") & """,""YourMessage"",""width=400,height=300"")'><font color=black>" & rsMessages("Title")  & "</font></a></td>" &_
									 "     <td width='20' >" &ImageType & "</td>" &_
									 "     <td width='1' bgcolor='#C9CBC5'></td>" &_
									 "</tr>" &_
									 "<tr>" &_
									 "	<td width='10' height='1'></td>" &_
									 "	<td width='3' height='1'></td>" &_
									 "	<td width='200' height='1' bgcolor='#C9CBC5'></td>" &_
									 "	<td width='20' height='1'  bgcolor='#C9CBC5'></td>" &_
									 "	<td width='1' height='1'  bgcolor='#C9CBC5'></td>" &_
									 "</tr>"
						
		rsMessages.Movenext
	Wend
	
	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style2 {color: #FFFFFF}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<script language="javascript">

function CloseMessagePad(){

NewsBlock.style.display = "block";
MessageBlock.style.display = "none";
}

function OpenMessagePad(){

NewsBlock.style.display = "none";
MessageBlock.style.display = "block";
}

</script>
</head>

<body>
<table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
  <tr>
    <td colspan="3">
	<table width="736" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#FFFFFF">
        <td height="4">
		<table width="732" height="59" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="12" colspan="2">
			</td>
            </tr>
          <tr>
            <td width="13" height="27">&nbsp;</td>
            <td width="719" valign="bottom"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><IMG SRC="images/im_rteidmark.gif" WIDTH=88 HEIGHT=22 ALT=""></td>
                <td width="5">&nbsp;</td>
                <td width="183">&nbsp;</td>
                <td width="339" rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="191" rowspan="2"><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=29 ALT=""><IMG SRC="images/im_rslmanager.gif" WIDTH=163 HEIGHT=29 ALT=""></td>
                <td>&nbsp;</td>
                <td rowspan="2"><table width="98%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="18"></td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#C0C0C0">
					</td>
                  </tr>
                  <tr>
                    <td width="22">
					</td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right"><IMG SRC="images/im_strapline.gif" WIDTH=339 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="5" colspan="2"></td>
          </tr>
        </table></td>
        <td height="4">&nbsp;</td>
      </tr>
      <tr>
        <td height="4" width="733" bgcolor="#669900">
		</td>
        <td width="3" height="4"><IMG SRC="images/im_barend.gif" WIDTH=3 HEIGHT=4 ALT=""></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="617"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="613" height="23" bgcolor="#FFFFFF"><table width="607" height="32" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="11" height="27">&nbsp;</td>
                    <td width="426" valign="bottom"><img src="/BoardMembers/images/im_messagepad-.gif" width="92" height="15"></td>
                    <td width="170" align="right" valign="middle"> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backbutton.gif" width="11" height="11" border="0"></a> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backtomywhiteboard.gif" width="121" height="13" border="0"></a></td>
                  </tr>
                  <tr>
                    <td height="5" colspan="3"></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
          </table></td>
          <td width="121">
           <div id="id_card" name="id_card" width="117">
		   	<%=id_card_HTML%>
		   </div>
		  </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="234" valign="top"><table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1" bgcolor="#669900"></td>
              <td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><IMG SRC="images/im_archive_title.gif" WIDTH=47 HEIGHT=13 ALT=""></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
     	<%=ArchiveRow%>
        </table></td>
      </tr>
      <tr>
        <td height="9"></td>
      </tr>
    </table></td>
    <td width="14" height="380" valign="top">&nbsp;</td>
    <td width="488" valign="top"><table width="488" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><div id="NewsBlock" name="NewsBlock" style="display:block "><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td width="366" valign="top"><table width="366" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="364" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="6"></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="9">&nbsp;</td>
              <td width="115" valign="top"><table width="115" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="113" height="23" bgcolor="#FFFFFF"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="113" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=116 HEIGHT=4 ALT=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF">
					<table width="115" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=144 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						  	<!--#include file="includes/MessagePadAlert.asp" -->
						  </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table>
		</div>
		<div id="MessageBlock" name="MessageBlock" style="display:NONE ">
		<table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td valign="top"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="486" height="23" bgcolor="#FFFFFF"><table width="483" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="6">&nbsp;</td>
                                <td width="417"><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                                <td width="60"><div align="right" onClick="CloseMessagePad()"  style="cursor:hand"> <img src="/BoardMembers/images/im_close.gif" width="37" height="11"><img src="/BoardMembers/images/im_closebutton.gif"> </div></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="4"></td>
                  </tr>
                  <tr>
                    <td height="9"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=488 HEIGHT=4 ALT=""></td>
                      </tr>
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><table width="481" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=143 ALT=""></td>
                              <td valign="top" bgcolor="#FFFFFF"><!--#include file="includes/MessagePad.asp" -->
                              </td>
                            </tr>
                        </table></td>
                      </tr>
                    
                    </table></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table></td>
              </tr>
        </table>
		</div>
		</td>
      </tr>
      <tr>
        <td>&nbsp;		</td>
      </tr>
    </table></td>
  </tr>
    <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="17" colspan="3">  <table width="400" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="382" align="right" valign="middle"><span class="style3 style2">&copy;2005 RSLmanager is a Reidmark ebusiness system</span></td>
        <td valign="bottom" align="right" width="18"></td>
      </tr>
    </table></td>
  </tr> 
  <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="3" colspan="3"><img src="images/im_corner-42.gif" width=3 height=3 alt="" ></td>
  </tr> 
</table>
</body>
</html>
