<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
  <tr>
    <td>
		<table  border="0" cellspacing="0" cellpadding="0">
		   <tr>
			<td width="12"></td>
			<td width="1" bgcolor="#669900"></td>
			<td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5">&nbsp;</td>
                <td><IMG SRC="images/im_performance.gif" WIDTH=79 HEIGHT=13 ALT=""></td>
              </tr>
            </table></td>
			<td width="1" bgcolor="#669900"></td>
		  </tr>
		  <tr>
			<td width="12" height="1"></td>
			<td width="1" height="1"></td>
			<td width="220" height="1" bgcolor="#669900"></td>
			<td width="1" height="1"></td>
		  </tr>			
	  </table>
	</td>
  </tr>
  <tr>
    <td>
	<table  border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td></td>
	    <td></td>
	    <td height="5"></td>
	    <td></td>
	    </tr>
	  <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">No of Voids </td>
            <td width="75" align="right">00</td>
			<td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">Rent Value </td>
            <td width="75" align="right">00</td>
			<td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">Total Dev Progress </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">Value Void Rental </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">Av No Days to Relet </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">No of Vacancies </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">Total Salary </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="3">&nbsp;</td>
        <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="5">&nbsp;</td>
            <td width="134">No of Lets </td>
             <td width="75" align="right">00</td>
			 <td width="5">&nbsp;</td>
          </tr>
        </table></td>
        <td width="1" bgcolor="#C9CBC5"></td>
      </tr>
      <tr>
        <td width="10" height="1"></td>
        <td width="3" height="1"></td>
        <td width="220" height="1" bgcolor="#C9CBC5"></td>
        <td width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="9">
	</td>
  </tr>
  <tr>
    <td>
			<table  border="0" cellspacing="0" cellpadding="0">
		   <tr>
			<td width="12"></td>
			<td width="1" bgcolor="#669900"></td>
			<td width="220" height="23" bgcolor="#FFFFFF">
			  <table width="220" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5">&nbsp;</td>
                  <td><IMG SRC="images/im_boardpapers.gif" WIDTH=85 HEIGHT=13 ALT=""></td>
                </tr>
              </table></td>
			<td width="1" bgcolor="#669900"></td>
		  </tr>
		  <tr>
			<td width="12" height="1"></td>
			<td width="1" height="1"></td>
			<td width="220" height="1" bgcolor="#669900"></td>
			<td width="1" height="1"></td>
		  </tr>			 
		</table>	
	</td>
  </tr>
  <tr><td height="5" ></td></tr>
  <tr>
	  <td>
	  <table  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td></td>
			<td></td>
			<td height="5"></td>
			<td></td>
			</tr>
		  <tr>
			<td width="10">&nbsp;</td>
			<td width="3">&nbsp;</td>
			<td width="220" height="16">
				<table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5">&nbsp;</td>
				<td>AGM 2005 Minutes </td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_txt.gif" WIDTH=12 HEIGHT=16 ALT=""></td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_word.gif" WIDTH=15 HEIGHT=16 ALT=""></td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_pdf.gif" WIDTH=16 HEIGHT=16 ALT=""></td>
                <td width="6"></td>
              </tr>
            </table></td>
			<td width="1" bgcolor="#C9CBC5"></td>
		  </tr>
		  <tr>
			<td width="10" height="1"></td>
			<td width="3" height="1"></td>
			<td width="220" height="1" bgcolor="#C9CBC5"></td>
			<td width="1" height="1"></td>
		  </tr>
		  <tr>
			<td width="10">&nbsp;</td>
			<td width="3">&nbsp;</td>
			<td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr>
			  	<td width="5">&nbsp;</td>
                <td>AGM 2005 Agenda</td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_txt.gif" WIDTH=12 HEIGHT=16 ALT=""></td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_word.gif" WIDTH=15 HEIGHT=16 ALT=""></td>
                <td width="3"></td>
                <td width="16"><IMG SRC="images/im_pdf.gif" WIDTH=16 HEIGHT=16 ALT=""></td>
                <td width="6"></td>
              </tr>
            </table></td>
			<td width="1" bgcolor="#C9CBC5"></td>
		  </tr>
		  <tr>
			<td width="10" height="1"></td>
			<td width="3" height="1"></td>
			<td width="220" height="1" bgcolor="#C9CBC5"></td>
			<td width="1" height="1"></td>
		  </tr>
		    <tr><td height="5" ></td></tr>

		</table>
	  </td>
	  </tr>
  <tr>
    <td>
	<table  border="0" cellspacing="0" cellpadding="0">
		   <tr>
			<td width="12"></td>
			<td width="1"></td>
			<td width="220" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5" bgcolor="#B7BDAD"></td>
				<td width="155" bgcolor="#B7BDAD">Board Papers Archive </td>
                <td width="6" bgcolor="#B7BDAD"></td>
                <td width="51" bgcolor="#B7BDAD"><IMG SRC="images/im_enter.gif" WIDTH=40 HEIGHT=11 ALT=""></td>
                <td width="3"><IMG SRC="images/im_end.gif" WIDTH=3 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
		  </tr>
		  <tr>
			<td width="12" height="1"></td>
			<td width="1" height="1"></td>
			<td width="220" height="1"></td>
			<td width="1" height="1"></td>
		  </tr>			
	  </table>
	</td>
  </tr>
   <tr>
    <td height="9"><table  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12">&nbsp;</td>
        <td width="1"></td>
        <td width="220" ><table width="219" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>&nbsp;</td>
            <td rowspan="3"><IMG SRC="images/im_graph.gif" WIDTH=28 HEIGHT=30 ALT=""></td>
            <td>&nbsp;</td>
            <td width="8">&nbsp;</td>
            <td width="3">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#669900"><IMG SRC="images/spacer.gif" WIDTH=4 HEIGHT=25 ALT=""></td>
            <td colspan="3"><table width="188" border="0" cellspacing="0" cellpadding="0">
              <tr bgcolor="#669900">
                <td width="10" rowspan="2" bgcolor="#669900">&nbsp;</td>
                <td rowspan="2"><IMG SRC="images/im_latestfinancialinfo.gif" WIDTH=172 HEIGHT=25 ALT=""></td>
                <td width="3" height="22">&nbsp;</td>
              </tr>
              <tr bgcolor="#669900">
                <td width="3" align="right" valign="bottom" bgcolor="#DFEACA"><IMG SRC="images/im_corner-42.gif" WIDTH=3 HEIGHT=3 ALT=""></td>
              </tr>
            </table></td>
            </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td width="12" height="1"></td>
        <td width="1" height="1"></td>
        <td width="220" height="1"></td>
        <td width="1" height="1"></td>
      </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
