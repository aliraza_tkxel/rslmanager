<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
	

%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RSL Manager :: Board Members Area</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<script language="javascript">

function CloseMessagePad(){

NewsBlock.style.display = "block";
MessageBlock.style.display = "none";
}

function OpenMessagePad(){

NewsBlock.style.display = "none";
MessageBlock.style.display = "block";
}

function GoToKPI(TopicID,FY,TopicName){

document.location.href = "KPI.asp?TopicID=" + TopicID + "&FY=" + FY + "&TopicName=" + TopicName;
}

function GoArchive(){
document.location.href = "Messages.asp"
}

function NewMessage(){
document.location.href = "popmessage.asp"
}

function AmendProfile(){
RSLFORM.action = "includes/MyProfileAmend.asp"
RSLFORM.target = "MyProfile_frame"
RSLFORM.submit ()

}

</script> 
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style1 {font-weight: bold}
.style3 {color: #FFFFFF}
-->
</style>

</head>

<body>

<table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
<tr>
    <td colspan="3">
	<table width="736" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#FFFFFF">
        <td height="4"><div id="divLogout" style="position:relative;top:7px;left:695px;"><a href="/Logout/wash.asp">Logout</a></div>
		<table width="732" height="59" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="12" colspan="2" align="right"></td>
            </tr>
          <tr>
            <td width="13" height="27">&nbsp;</td>
            <td width="719" valign="bottom"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><IMG SRC="images/im_rteidmark.gif" WIDTH=88 HEIGHT=22 ALT=""></td>
                <td width="5">&nbsp;</td>
                <td width="183">&nbsp;</td>
                <td width="339" rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="191" rowspan="2"><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=29 ALT=""><IMG SRC="images/im_rslmanager.gif" WIDTH=163 HEIGHT=29 ALT=""></td>
                <td>&nbsp;</td>
                <td rowspan="2"><table width="98%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="18"></td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#C0C0C0">
					</td>
                  </tr>
                  <tr>
                    <td width="22">
					</td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right"><IMG SRC="images/im_strapline.gif" WIDTH=339 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="5" colspan="2"></td>
          </tr>
        </table></td>
        <td height="4">&nbsp;</td>
      </tr>
      <tr>
        <td height="4" width="733" bgcolor="#669900">
		</td>
        <td width="3" height="4"><IMG SRC="images/im_barend.gif" WIDTH=3 HEIGHT=4 ALT=""></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="617"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="613" height="23" bgcolor="#FFFFFF"><table width="440" height="32" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="12" height="27">&nbsp;</td>
                    <td width="428" valign="bottom"><IMG SRC="images/im_mywhiteboard.gif" WIDTH=126 HEIGHT=19 ALT=""></td>
                  </tr>
                  <tr>
                    <td height="5" colspan="2"></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
          </table></td>
          <td width="121">
           <div id="id_card" name="id_card" width="117">
			 <%= id_card_HTML %>
		   </div>
		  </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="234" valign="top"><table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1" bgcolor="#669900"></td>
              <td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><IMG SRC="images/im_performance.gif" WIDTH=79 HEIGHT=13 ALT=""></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td></td>
              <td></td>
              <td height="5"></td>
              <td></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(1,7,'GNPI01')" Title="GNPI01 : Average Weekly Rent">GNPI01 : Average Weekly Rent</a> </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(9,7,'GNPI18')" Title="GNPI18 : % of Emergency Repairs">GNPI18 : % of Emergency Repairs </a></td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(10,7,'GNPI19')" Title="GNPI19 : % of Urgent Repairs Within Target">GNPI19 : % of Urgent Repairs With...</a>  </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(11,7,'GNPI20')" Title="GNPI20 : % of Routine Repairs Within Target">GNPI20 : % of Routine Repairs Wit...</a>  </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(6,7,'GNPI09')" Title="GNPI09 : % of Dwellings Vacant and Available">GNPI09 : % of Dwellings Vacant an...</a> </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(7,7,'GNPI10')" Title="GNPI10 : % of Dwellings Vacant and Not Available">GNPI10 : % of Dwellings Vacant an...</a> </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(8,7,'GNPI11')" Title="GNPI11 : Average Letting Time for Dwellings">GNPI11 : Average Letting Time for ... </a> </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><a  style="CURSOR:HAND" onClick="GoToKPI(12,7,'GNPI24')" Title="GNPI24 : % of Lettings to BME households">GNPI24 : % of Lettings to BME Hou...
					</a>  </td>
                    <td width="5">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#C9CBC5"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="9"></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1" bgcolor="#669900"></td>
              <td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><IMG SRC="images/im_boardpapers.gif" WIDTH=112 HEIGHT=13 ALT=""></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="5" ></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td></td>
              <td></td>
              <td height="5"></td>
              <td></td>
            </tr>
            <tr>
              <td width="10">&nbsp;</td>
              <td width="3">&nbsp;</td>
              <td width="220" height="16"><!--#include file="includes/BoardDocs.asp" --></td>
              <td width="1"></td>
            </tr>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" ></td>
              <td width="1" height="1"></td>
            </tr>
            <tr>
              <td height="5" ></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1"></td>
              <td width="220" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5" bgcolor="#B7BDAD"></td>
                    <td width="155" bgcolor="#B7BDAD">Board Papers Archive </td>
                    <td width="6" bgcolor="#B7BDAD"></td>
                    <td width="51" bgcolor="#B7BDAD"><a href="BoardPapers.asp"><IMG SRC="images/im_enter.gif" WIDTH=40 HEIGHT=11 ALT=""  border="0"></a></td>
                    <td width="3"><IMG SRC="images/im_end.gif" WIDTH=3 HEIGHT=17 ALT=""></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="9"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12">&nbsp;</td>
              <td width="1"></td>
              <td width="220" ><table width="219" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>&nbsp;</td>
                    <td rowspan="3"><a href="financialInformation.asp"><IMG SRC="images/im_graph.gif" ALT="" WIDTH=28 HEIGHT=30 border="0"></a></td>
                    <td>&nbsp;</td>
                    <td width="8">&nbsp;</td>
                    <td width="3">&nbsp;</td>
                  </tr>
                  <tr>
                    <td bgcolor="#669900"><a href="financialInformation.asp"><IMG SRC="images/spacer.gif" ALT="" WIDTH=4 HEIGHT=25 border="0"></a></td>
                    <td colspan="3"><table width="188" border="0" cellspacing="0" cellpadding="0">
                        <tr bgcolor="#669900">
                          <td width="10" rowspan="2" bgcolor="#669900"><a href="financialInformation.asp"><IMG SRC="images/spacer.gif" ALT="" WIDTH=10 HEIGHT=20 border="0"></a></td>
                          <td rowspan="2"><a href="financialInformation.asp"><IMG SRC="images/im_latestfinancialinfo.gif" ALT="" WIDTH=172 HEIGHT=25 border="0"></a></td>
                          <td width="3" height="22">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#669900">
                          <td width="3" align="right" valign="bottom" bgcolor="#DFEACA"><IMG SRC="images/im_corner-42.gif" WIDTH=3 HEIGHT=3 ALT=""></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
    <td width="14" height="380" valign="top">&nbsp;</td>
    <td width="488" valign="top">
	<table width="488" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><div id="NewsBlock" name="NewsBlock" style="display:block "><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td width="366" valign="top"><table width="366" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="364" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_latestnews.gif" WIDTH=79 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="6"></td>
                  </tr>
                  <tr>
                    <td height="9"><!--#include file="includes/MyNews.asp" --></td>
                  </tr>
              </table></td>
              <td width="9">&nbsp;</td>
              <td width="115" valign="top"><table width="115" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="113" height="23" bgcolor="#FFFFFF"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="113" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=116 HEIGHT=4 ALT=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF">
					<table width="115" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=144 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						  	<!--#include file="includes/MessagePadAlert.asp" -->
						  </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table>
		</div>
		<div id="MessageBlock" name="MessageBlock" style="display:none ">
		<table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td valign="top"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="486" height="23" bgcolor="#FFFFFF"><table width="483" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="6">&nbsp;</td>
                                <td width="357"><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
								
                                <td width="120"><div align="right" onClick="CloseMessagePad()"  style="cursor:hand"> <img src="/BoardMembers/images/im_close.gif" width="37" height="11"><img src="/BoardMembers/images/im_closebutton.gif"> </div></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="4"></td>
                  </tr>
                  <tr>
                    <td height="9"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><table width="488" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="4"><img src="/BoardMembers/images/im_shadocorner.gif" width="4" height="4"></td>
                            <td><img src="/BoardMembers/images/im_shadotop-26.gif" width="484" height="4"></td>
                            </tr>
                        </table>                        </td>
                      </tr>
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><table width="481" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=143 ALT=""></td>
                              <td valign="top" bgcolor="#FFFFFF"><!--#include file="includes/MessagePad.asp" -->
                              </td>
                            </tr>
                        </table></td>
                      </tr>
                    
                    </table></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table></td>
              </tr>
        </table>
		</div>
		</td>
      </tr>
      <tr>
        <td>
		<table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td width="168" valign="top"><table width="168" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"><img src="/BoardMembers/images/spacer.gif" width="1" height="1"></td>
                          <td width="150" height="23" bgcolor="#FFFFFF">
						 <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="5">&nbsp;</td>
                              <td><IMG SRC="images/im_findacolleague.gif" WIDTH=104 HEIGHT=13 ALT=""></td>
                            </tr>
                          </table>
						  </td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><img src="images/im_shadotop-26.gif" width=169 height=4 alt=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><table width="169" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft-28.gif" WIDTH=4 HEIGHT=155 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						<!--#include file="includes/findcolleague.asp" -->
						  </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
              <td width="7">&nbsp;</td>
              <td width="313" valign="top">
			  <table width="313" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td>
					
					<table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="311" height="23" bgcolor="#FFFFFF"><table width="307" border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="6">&nbsp;</td>
                                <td width="148"><IMG SRC="images/im_findacolleague-22.gif" WIDTH=62 HEIGHT=13 ALT=""></td>
                                <td width="153" align="right"><img src="/BoardMembers/images/im_amendprofile.gif" width="81" height="11" onClick="AmendProfile()" style="cursor:hand"></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop-27.gif" WIDTH=314 HEIGHT=4 ALT=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><table width="313" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft-28.gif" WIDTH=4 HEIGHT=155 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						  <iframe name="MyProfile_frame"  id="MyProfile_frame" width="310" height="155" frameborder="0" src="/BoardMembers/includes/MyProfile.asp">MyProfile_frame</iframe>
						  </td>
                        </tr>
                    </table> 
					</td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr> 

    <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="17" colspan="3">  <table width="400" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="382" align="right" valign="middle"><span class="style3">&copy;2005 RSLmanager is a Reidmark ebusiness system</span></td>
        <td valign="bottom" align="right" width="18"></td>
      </tr>
    </table></td>
  </tr> 
  <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="3" colspan="3"><img src="images/im_corner-42.gif" width=3 height=3 alt="" ></td>
  </tr> 

</table>
</body>
</html>
