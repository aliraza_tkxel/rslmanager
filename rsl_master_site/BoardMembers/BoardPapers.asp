<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	dim SQLdocs, rsDocs, strDocArray, strDocIDArray
	SQLdocs = "SELECT DOCUMENTID,DOCNAME FROM DOC_DOCUMENT WHERE ORDERID IS NOT NULL ORDER BY ORDERID ASC"
	OpenDB()
	Call OpenRS(rsDocs, SQLdocs)
	do
		if rsDocs.eof then exit do
		strDocArray = strDocArray & "'" & replace(rsDocs("DOCNAME"),"'", "\'") & " (" & rsDocs("DOCUMENTID") & ")',"
		strDocIDArray = strDocIDArray & rsDocs("DOCUMENTID") & ","
		rsDocs.moveNext()
	loop
	strDocArray = strDocArray & "''" 
	strDocIDArray = strDocIDArray & "''"
	

	Call CloseRS(rsDocs)
	CloseDB()
  	
    Function stripHTML(strHTML)
    'Strips the HTML tags from strHTML

      Dim objRegExp, strOutput
      Set objRegExp = New Regexp

      objRegExp.IgnoreCase = True
      objRegExp.Global = True
      objRegExp.Pattern = "<(.|\n)+?>"

      'Replace all HTML tag matches with the empty string
      strOutput = objRegExp.Replace(strHTML, "")
      
      'Replace all < and > with &lt; and &gt;
      strOutput = Replace(strOutput, "<", "&lt;")
      strOutput = Replace(strOutput, ">", "&gt;")
      
      stripHTML = strOutput    'Return the value of strOutput

      Set objRegExp = Nothing
    End Function

	
'	while not rsDocs.eof 
'		filename = rsDocs("DOCUMENTFILE")
'		openDB()
'		thePath = GetFolderPath(rsDocs("FOLDERID"))
'		CloseDB()
'		docType = Right(filename,3)
'				select case lcase(DocType)
'					case "txt"
'						ImageType = "<IMG SRC='images/im_txt.gif' WIDTH=12 HEIGHT=16 ALT='' border='0'>"
'					case "doc"
'					ImageType = "<IMG SRC='images/im_word.gif' WIDTH=15 HEIGHT=16 ALT='' border='0'>"
'					case "pdf"
'						ImageType = "<IMG SRC='images/im_pdf.gif' WIDTH=16 HEIGHT=16 ALT='' border='0'>"
'					case "xls"
'						ImageType = "<IMG SRC='images/im_xls.gif' WIDTH=15 HEIGHT=16 ALT='' border='0'>"
'				end select 
'		rsDocs.Movenext
'	Wend
	
	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Board Papers</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
.style2 {color: #FFFFFF}

div.dragDropTarget	{
	margin-bottom:20;
	font-size:12;
	text-align:center;
	width:30;
	height:25;
	background:#669900;
	color:#ffffff;
	font-weight:bold;
	float:left;
	}
div.dragDropTargetText {
	background:#ffffff;
	color:#669900;
	font-weight:normal;
	width:300;
	height: 25;
	cursor: pointer;
}
div#DivHelp {
	display:none;
	text-align:left;
	position:absolute;
	top:370;
	left:250;
	background:#FFFFFF;
	border:1px solid #000000;
	width:300;
	height:150;
}
-->
</style>
<!-- Tree bits -->
    <link type="text/css" rel="stylesheet" href="/js/XTree/xtree2.css" />
    <script type="text/javascript" src="/js/XTree/xtree2.js"></script>
    <script type="text/javascript" src="/js/XTree/xmlextras.js"></script>
    <script type="text/javascript" src="/js/XTree/xloadtree2.js"></script>
    <script language=javascript>
    var rti;
    var CurrentSelected = ""
    function GO(iPage){
	   CurrentlySelected = tree.getSelected().getParent()
       location.href = iPage
	}
    		
	function DoNothing() { var a; }

    function ReloadMe(){
	    try {
		    if (CurrentlySelected != "")
			    CurrentlySelected.reload();
		    }
	    catch(e){
		    location.href = "BoardPapers.asp"	
		    }
	    }
    </script>
<!-- End tree bits -->
<script language="javascript">

function CloseMessagePad(){

NewsBlock.style.display = "block";
MessageBlock.style.display = "none";
}

function OpenMessagePad(){

NewsBlock.style.display = "none";
MessageBlock.style.display = "block";
}

// Drag drop functionality
	// This is a heineous and nasty implementation ... but it is as it is
	// Variables to hold when we start dragging
	var elementBeingDragged;
	var DragDropInProgress;
	var docArray = new Array(<%=strDocArray%>);
	var docIDArray = new Array(<%=strDocIDArray%>);
	docArray.length=6;	// Drop unwanted elements off the bottom
	docIDArray.length=6;
	DragDropInProgress=false;

	// Cancel events we don't want
	function cancelEvent() {
		window.event.returnValue = false;
	}

	// Register start of item being dragged
	function dragItem() {
		elementBeingDragged = event.srcElement;
		DragDropInProgress = true;
	}

	// Register Item Dropped
	function dropItem() {
		// only register drop if dropped onto a drag drop target
		if (event.srcElement.id.substring(0,10) == "DropTarget" && event.srcElement.id.length == 11) {
			var docID;
			var docName;
			docID = elementBeingDragged.parentElement.parentElement.id;				//parent because its a div in a div that has the id
			docName = elementBeingDragged.innerHTML + ' (' + docID + ')';
			if (elementBeingDragged.innerHTML == "") { docName = elementBeingDragged.parentElement.children(2).innerHTML + ' (' + docID + ')'; }

			// Is it already listed? If so get rid
			for (i=0;i<=docArray.length-1;i++){ if (docArray.slice(i,i+1) == docName) { docArray.splice(i,1); } } 
			for (i=0;i<=docIDArray.length-1;i++){ if (docIDArray.slice(i,i+1) == docID ) { docIDArray.splice(i,1); } } 

			// insert item into array at appropriate point
			var insertIntoIndex; 
			insertIntoIndex = event.srcElement.id.substring(event.srcElement.id.length-1,event.srcElement.id.length)-1;
			if (docArray.slice(insertIntoIndex,insertIntoIndex+1) == ''){
				// Empty slot ... put it in
				docArray[insertIntoIndex] = docName;
				docIDArray[insertIntoIndex] = docID;
			} else {
				// Move everything down by one and slot it in
				docArray.splice(insertIntoIndex, 0, docName);
				docIDArray.splice(insertIntoIndex, 0, docID);
			}
			var l; l = docArray.length;
			for (i=0;i<=l-1;i++){ if (docArray.slice(i,i+1) == '') { docArray.splice(i,1);i=-1;l=docArray.length; } } // Get rid of blanks
			l = docArray.length;
			for (i=0;i<=l-1;i++){ if (docIDArray.slice(i,i+1) == '') { docIDArray.splice(i,1);i=-1;l=docIDArray.length; } } // Get rid of blanks
			docArray.length=6;	// Drop unwanted elements off the bottom
			docIDArray.length=6;

			// reDraw controls
			document.getElementById(LastObjectChangedID).style.backgroundColor = LastObjectChangedBackground;
			rePrintDocArray();
		}
		// finalise elementBeingDragged variable
		elementBeingDragged = this;
		DragDropInProgress=false;
	}
	
	function rePrintDocArray(){
		//Clear DropTarget?Text so that it accurately reflects
		for (i=1;i<=6;i++){ document.getElementById('DropTarget'+i+'Text').innerText = ''; }
		var doc;
		for (doc in docArray) { 
			document.getElementById('DropTarget' + (parseInt(doc)+1) + 'Text').innerText = docArray[doc];	//+1 because 0 based array!  
		}
		
		var  divDocArray;
		divDocArray = document.getElementById('divDocArray');
		divDocArray.innerHTML = docArray;
		divDocArray.innerHTML = divDocArray.innerHTML.replace(/,/g,"<BR>");
	}

	var LastObjectChangedID;
	var LastObjectChangedBackground;
	function HighlightDropTarget(){
		if (event.srcElement.id.substring(0,10) == "DropTarget" && event.srcElement.id.length == 11) {
			if (LastObjectChangedID==undefined) { 
				LastObjectChangedID=event.srcElement.id;
				LastObjectChangedBackground = event.srcElement.style.backgroundColor;
			}
			if (LastObjectChangedID != event.srcElement.id && LastObjectChangedID != undefined){
				if (LastObjectChangedID != "") {
					document.getElementById(LastObjectChangedID).style.backgroundColor = LastObjectChangedBackground;
					LastObjectChangedID=event.srcElement.id;
					LastObjectChangedBackground = event.srcElement.currentStyle.backgroundColor;
				}
			}
			event.srcElement.style.background="#88A0B8";
		}
	}

	var HighlightedDropTargets;
	HighlightedDropTargets = new Array();
	function SelectDropTarget(){
		if (HighlightedDropTargets.length > 0) return;
		
		document.getElementById("btnRemoveSelection").style.display = "";
		document.getElementById("btnClearSelection").style.display = "";
	
		var DropTargetDiv;
		DropTargetDiv = event.srcElement;
		HighlightedDropTargets.push(DropTargetDiv)

		DropTargetDiv.style.background = "#88A0B8";

	}

	function ClearSelection(){
		document.getElementById("btnRemoveSelection").style.display = "none";
		document.getElementById("btnClearSelection").style.display = "none";

		for (var i=0;i<=HighlightedDropTargets.length-1;i++) { HighlightedDropTargets[i].style.background = "#669900"; }
		HighlightedDropTargets = new Array();
	}

	function RemoveHighlightedDocs(){
		document.getElementById("btnRemoveSelection").style.display = "none";
		document.getElementById("btnClearSelection").style.display = "none";

		for (var i=0;i<=HighlightedDropTargets.length-1;i++)
		{
			var DropTargetIndex = HighlightedDropTargets[i].id.substring(10,11);
			docArray.splice(DropTargetIndex-1, 1);
			docIDArray.splice(DropTargetIndex-1,1);

			HighlightedDropTargets[i].style.background = "#669900";
			
		}
		docArray.length=6; 				//reset the arrays
		docIDArray.length=6;
		HighlightedDropTargets = new Array();

		rePrintDocArray();
	}

	function SaveDocOrder(){
		// Pass the array of docIDs
		RSLFORM.docIDArray.value = docIDArray;
		RSLFORM.action = "serverside/BoardPapers_svr.asp"
		RSLFORM.target = "frmSaveTarget"
		RSLFORM.submit()
	}

	function ShowHideHelpDiv(){
		if (document.getElementById("DivHelp").style.display == "none") {
			document.getElementById("DivHelp").style.display="block" 
		} else {
			document.getElementById("DivHelp").style.display="none"
		}
	}
// End Drag drop functionality
</script>
</head>

<body onload="rePrintDocArray();" ondragstart="dragItem()" ondragenter="HighlightDropTarget();cancelEvent()" ondragover="HighlightDropTarget();cancelEvent()" ondrop="dropItem()">
<table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
  <tr>
    <td colspan="3">
	<table width="736" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#FFFFFF">
        <td height="4">
		<table width="732" height="59" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="12" colspan="2">
			</td>
            </tr>
          <tr>
            <td width="13" height="27">&nbsp;</td>
            <td width="719" valign="bottom"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><IMG SRC="images/im_rteidmark.gif" WIDTH=88 HEIGHT=22 ALT=""></td>
                <td width="5">&nbsp;</td>
                <td width="183">&nbsp;</td>
                <td width="339" rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="191" rowspan="2"><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=29 ALT=""><IMG SRC="images/im_rslmanager.gif" WIDTH=163 HEIGHT=29 ALT=""></td>
                <td>&nbsp;</td>
                <td rowspan="2"><table width="98%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="18"></td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#C0C0C0">
					</td>
                  </tr>
                  <tr>
                    <td width="22">
					</td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right"><IMG SRC="images/im_strapline.gif" WIDTH=339 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="5" colspan="2"></td>
          </tr>
        </table></td>
        <td height="4">&nbsp;</td>
      </tr>
      <tr>
        <td height="4" width="733" bgcolor="#669900">
		</td>
        <td width="3" height="4"><IMG SRC="images/im_barend.gif" WIDTH=3 HEIGHT=4 ALT=""></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="617"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="613" height="23" bgcolor="#FFFFFF"><table width="607" height="32" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="11" height="27">&nbsp;</td>
                    <td width="426" valign="bottom"><img src="/BoardMembers/images/im_boardpapers2.gif" width="126" height="19"></td>
                    <td width="170" align="right" valign="middle"> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backbutton.gif" width="11" height="11" border="0"></a> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backtomywhiteboard.gif" width="121" height="13" border="0"></a></td>
                  </tr>
                  <tr>
                    <td height="5" colspan="3"></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
          </table></td>
          <td width="121">
           <div id="id_card" name="id_card" width="117">
		 <%= id_card_HTML%>
			</div>
		  </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="234" valign="top"><table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1" bgcolor="#669900"></td>
              <td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><IMG SRC="images/im_archive_title.gif" WIDTH=47 HEIGHT=13 ALT=""></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>  
            <img name="web_folder" src="images/folder.png" width="1" height="1">
            <img name="web_openfolder" src="images/openfolder.png" width="1" height="1">
            <img name="web_file" src="images/file.png" width="1" height="1">
            <img name="web_lminus" src="images/Lminus.png" width="1" height="1">
            <img name="web_lplus" src="images/Lplus.png" width="1" height="1">
            <img name="web_tminus" src="images/Tminus.png" width="1" height="1">
            <img name="web_tplus" src="images/Tplus.png" width="1" height="1">
            <img name="web_i" src="images/I.png" width="1" height="1">
            <img name="web_l" src="images/L.png" width="1" height="1">
            <img name="web_t" src="images/T.png" width="1" height="1">
            <img name="web_base" src="images/base.gif" width="1" height="1">
            <div class="dtree">
            <script type="text/javascript"> 
                /// XP Look
                webFXTreeConfig.rootIcon		= web_folder.src;
                webFXTreeConfig.openRootIcon	= web_openfolder.src;
                webFXTreeConfig.folderIcon		= web_folder.src;
                webFXTreeConfig.openFolderIcon	= web_openfolder.src;
                webFXTreeConfig.fileIcon		= web_file.src;
                webFXTreeConfig.lMinusIcon		= web_lminus.src;
                webFXTreeConfig.lPlusIcon		= web_lplus.src;
                webFXTreeConfig.tMinusIcon		= web_tminus.src;
                webFXTreeConfig.tPlusIcon		= web_tplus.src;
                webFXTreeConfig.iIcon			= web_i.src;
                webFXTreeConfig.lIcon			= web_l.src;
                webFXTreeConfig.tIcon			= web_t.src;
    
                var rti;
                var tree = new WebFXTree("RSL Document Manager", "", "", web_base.src, web_base.src);
                <%
                SQL = "SELECT DISTINCT F.* FROM DOC_FOLDER F INNER JOIN DOC_DOCUMENT D ON (D.FOLDERID = F.FOLDERID AND DOCFOR LIKE '%," & Session("TeamCode") & ",%') WHERE PARENTFOLDER = -1 ORDER BY FOLDERNAME"
                Call OpenDB()
                Call OpenRs(rsCC, SQL)
                while NOT rsCC.EOF
		                Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("FOLDERNAME") & """, ""Treeload.asp?FLDID=" & rsCC("FOLDERID") & """, ""JavaScript:DoNothing();"", """"));"
	                rsCC.moveNext
                wend

                Call CloseRs(rsCC)
                Call CloseDB()
                %>

                tree.write()
                </script> 
            </div>
        </td>
      </tr>
      <tr>
        <td height="9"></td>
      </tr>
    </table></td>
    <td width="14" height="380" valign="top">&nbsp;</td>
    <td width="488" valign="top"><table width="488" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><div id="NewsBlock" name="NewsBlock" style="display:block "><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td width="366" valign="top"><table width="366" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="364" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><img src="images/im_boarddocumentswhiteboard.gif" border="0" /></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="6"></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="300" height="200">
						<div class="dragDropTargetText"><div id="DropTarget1" class="dragDropTarget" onclick="SelectDropTarget();">1</div><div id="DropTarget1Text"></div></div>
						<div class="dragDropTargetText"><div id="DropTarget2" class="dragDropTarget" onclick="SelectDropTarget();">2</div><div id="DropTarget2Text"></div></div>
						<div class="dragDropTargetText"><div id="DropTarget3" class="dragDropTarget" onclick="SelectDropTarget();">3</div><div id="DropTarget3Text"></div></div>
						<div class="dragDropTargetText"><div id="DropTarget4" class="dragDropTarget" onclick="SelectDropTarget();">4</div><div id="DropTarget4Text"></div></div>
						<div class="dragDropTargetText"><div id="DropTarget5" class="dragDropTarget" onclick="SelectDropTarget();">5</div><div id="DropTarget5Text"></div></div>
						<div class="dragDropTargetText"><div id="DropTarget6" class="dragDropTarget" onclick="SelectDropTarget();">6</div><div id="DropTarget6Text"></div></div>
						<div id="divDocArray" style="display:none;"></div>
					</td>
				</tr>
				<form name="RSLFORM" method="POST">
				<input type="hidden" name="docIDArray" value="">
				<input type="hidden" name="op" value="DocUpdate">
				<tr>
					<td align="right" width="300">
						<input type="button" class="RSLButton" id="btnRemoveSelection" style="display:none;" value=" Remove " onclick="RemoveHighlightedDocs();">
						<input type="button" class="RSLButton" id="btnClearSelection"  style="display:none;" value=" unSelect " onclick="ClearSelection();">
						<input type="button" class="RSLButton" value=" Save " onclick="SaveDocOrder();">
						<img src="images/im_info.gif" border="0" style="vertical-align:bottom;" onclick="ShowHideHelpDiv();" />
						<div id="DivHelp" >The above list determines the documents that are displayed on the Board Members Whiteboard. To amend the list, simply locate the document from within the folders on the left, and drag-and-drop it onto the required number. If there is already a document next to the selected number, the new document will replace it, and the existing document will move down to the next consecutive number. To delete a document from the list, click on the number next to that document, and a 'Remove' button will appear. Make sure you click the 'Save' button before returning to the Whiteboard. </div>
					</td>
				</tr>
				</form>
			  </table>			  
			  </td>
              <td width="9">&nbsp;</td>
              <td width="115" valign="top"><table width="115" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="113" height="23" bgcolor="#FFFFFF"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="113" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=116 HEIGHT=4 ALT=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF">
					<table width="115" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=144 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						  	<!--#include file="includes/MessagePadAlert.asp" -->
						  </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table>
		</div>
		<div id="MessageBlock" name="MessageBlock" style="display:NONE ">
		<table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td valign="top"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="486" height="23" bgcolor="#FFFFFF"><table width="483" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="6">&nbsp;</td>
                                <td width="417"><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                                <td width="60"><div align="right" onClick="CloseMessagePad()"  style="cursor:hand"> <img src="/BoardMembers/images/im_close.gif" width="37" height="11"><img src="/BoardMembers/images/im_closebutton.gif"> </div></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="4"></td>
                  </tr>
                  <tr>
                    <td height="9"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=488 HEIGHT=4 ALT=""></td>
                      </tr>
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><table width="481" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=143 ALT=""></td>
                              <td valign="top" bgcolor="#FFFFFF"><!--#include file="includes/MessagePad.asp" -->
                              </td>
                            </tr>
                        </table></td>
                      </tr>
                    
                    </table></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table></td>
              </tr>
        </table>
		</div>
		</td>
      </tr>
      <tr>
        <td>&nbsp;		</td>
      </tr>
    </table></td>
  </tr>
    <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="17" colspan="3">  <table width="400" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="382" align="right" valign="middle"><span class="style3 style2">&copy;2005 RSLmanager is a Reidmark ebusiness system</span></td>
        <td valign="bottom" align="right" width="18"></td>
      </tr>
    </table></td>
  </tr> 
  <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="3" colspan="3"><img src="images/im_corner-42.gif" width=3 height=3 alt="" ></td>
  </tr> 
</table>
<iframe src="/secureframe.asp" name="frmSaveTarget" style="display:none" src="about:blank"></iframe>
</body>
</html>
