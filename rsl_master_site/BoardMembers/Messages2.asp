<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Function stripHTML(strHTML)
	'Strips the HTML tags from strHTML
	
	  Dim objRegExp, strOutput
	  Set objRegExp = New Regexp
	
	  objRegExp.IgnoreCase = True
	  objRegExp.Global = True
	  objRegExp.Pattern = "<(.|\n)+?>"
	
	  'Replace all HTML tag matches with the empty string
	  strOutput = objRegExp.Replace(strHTML, "")
	  
	  'Replace all < and > with &lt; and &gt;
	  strOutput = Replace(strOutput, "<", "&lt;")
	  strOutput = Replace(strOutput, ">", "&gt;")
	  
	  stripHTML = strOutput    'Return the value of strOutput
	
	  Set objRegExp = Nothing
	End Function

	ORGID = SESSION("ORGID")
			IF (ORGID = "") THEN
				ORGID = ""
			END IF

	SQL = "SELECT  M.JOURNALID AS MESSAGEID,m.IMPORTANCEID ,M.DATECREATED AS DATESENT, E.FIRSTNAME + ' ' + E.LASTNAME AS SENTBY ,M.MESSAGE, M.TITLE FROM dbo.G_MESSAGEJOURNAL M " &_
				" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY  " &_
				"WHERE ( convert(datetime,convert(varchar,M.DATECREATED,103),103) = convert(datetime,convert(varchar,getdate(),103),103) ) AND (" &_
				"((M.MESSAGEFOR = '" & Session("UserID") & "') " &_ 
				"OR (M.MESSAGEFOR = 'TE" & TheTeamID & "' AND M.EXPIRES >= GETDATE() AND M.ORGID IS NULL) " &_ 
				"OR (M.MESSAGEFOR = 'ALL' AND M.EXPIRES >= GETDATE() AND '" & ORGID & "1' = '1') " &_ 
				"OR (M.MESSAGEFOR = 'ALLORG' AND M.EXPIRES >= GETDATE() AND '" & ORGID & "1' <> '1') " &_ 
				"OR (M.MESSAGEFOR = 'TE" & TheTeamID & "' AND M.EXPIRES >= GETDATE() AND M.ORGID = '" & ORGID & "')) " &_ 
				"AND M.ACTIVE = 1 AND M.READBY IS NULL) ORDER BY M.DATECREATED DESC, M.TITLE"


	Dim upperbound ' used to specify the random images on the news, when no news exists.
	upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
	set rsMessages = Server.CreateObject("ADODB.Recordset")
	rsMessages.ActiveConnection = RSL_CONNECTION_STRING
	rsMessages.Source = SQL
	rsMessages.CursorType = 0
	rsMessages.CursorLocation = 2
	rsMessages.LockType = 3
	rsMessages.Open()
	rsMessages_numRows = 0


	while not rsMessages.eof 
		datesent = rsMessages("DATESENT")
		FormatedDateSent = Day(datesent) & "/" & Month(datesent) & "/" & Year(datesent)
		Select Case rsMessages("IMPORTANCEID")
			Case "1" 
				 	importance_image = "<img src='/BoardMembers/images/importance.gif' width='4' height='10' title='High Importance'>"
			Case "2" 
				 	importance_image = "<img src='/BoardMembers/images/importance_medium.gif' width='4' height='10' title='Normal Importance'>"
			Case "3" 
			 		importance_image = "<img src='/BoardMembers/images/importance_low.gif' width='4' height='10' title='Low Importance'>"
		End select
		 
		main_mail = 	main_mail & "<tr> " &_
									"    <td align=center>" & importance_image & "</td> " &_
									"    <td align=center style='word-breal:break-all;cursor:hand'  onClick='ViewMessage(" &  rsMessages("MESSAGEID") & ");ShowDiv(""div_defaultlist"")'><img src='/BoardMembers/images/im_envelopec.gif' width='16' height='11'></td> " &_
									"    <td><input type='checkbox' id='check" & rsMessages("MESSAGEID") & "' name='check" & rsMessages("MESSAGEID") & "' value='" & rsMessages("MESSAGEID") & "' onClick='do_sum(" & rsMessages("MESSAGEID") & ")'></td> " &_
									"    <td style='word-breal:break-all;cursor:hand'  onClick='ViewMessage(" &  rsMessages("MESSAGEID") & ");ShowDiv(""div_defaultlist"")'>" & rsMessages("SENTBY") & "</td> " &_
									"    <td style='word-breal:break-all;cursor:hand'  onClick='ViewMessage(" &  rsMessages("MESSAGEID") & ");ShowDiv(""div_defaultlist"")'>" & rsMessages("Title") &  "</td> " &_
									"    <td title=" & datesent & " style='word-breal:break-all;cursor:hand'  onClick='ViewMessage(" &  rsMessages("MESSAGEID") & ");ShowDiv(""div_defaultlist"")'>" & FormatedDateSent &  "</td> " &_
									"  </tr> "
								
				
		
						
		rsMessages.Movenext
	Wend
	
	EMPLOY_FOLDER_SQL  = 	"SELECT  COUNT(*) AS MESSAGECOUNT ,E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS SENTBY  " &_
							"FROM dbo.G_MESSAGEJOURNAL M   " &_
							"	INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY   " &_
							"WHERE (M.MESSAGEFOR = ('" & Session("UserID") & "') ) and M.ACTIVE = 1 " &_
							"GROUP BY E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME "
	OpenDB()
	Call OpenRs(rsEmpFolders, EMPLOY_FOLDER_SQL)
	
	while not rsEmpFolders.eof 
			SENTBY = rsEmpFolders("SENTBY")
			EmployeeID = rsEmpFolders("EmployeeID")
			MESSAGECOUNT = rsEmpFolders("MESSAGECOUNT")
			Senders_folderTree =  Senders_folderTree & "<TR valign='middle'> " &_
													   " <TD width='5' align=center NOWRAP>&nbsp;</TD> " &_
													   " <TD align=center NOWRAP><img src='/BoardMembers/images/folder.gif' width='15' height='12'></TD> " &_
													   "  <td   style='word-breal:break-all;cursor:hand' onClick='setEmployeeFolderID(" & EmployeeID & ");ShowDiv(""div_EmployeeFolderList"");getSentList(""EmployeeFolder_MailItems"")'>&nbsp;" & SENTBY & " (" & MESSAGECOUNT & ")</TD> " &_
													   " </TR> " &_
													   "<TR valign='middle'><TD height='3' colspan='3' align=center NOWRAP></TD></TR>" 
	rsEmpFolders.Movenext
	Wend
	closeDB()
	
	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style2 {color: #FFFFFF}
.style3 {font-weight: bold}
.style4 {
	color: #6685A3;
	font-weight: bold;
}
.style6 {
	color: #336633;
	font-weight: bold;
}
.style7 {
	color: #CC6600;
	font-weight: bold;
}
.style8 {
	font-size: 16px;
	font-weight: bold;
	color: #FF0000;
}
.style9 {
	color: #FF0000;
	font-weight: bold;
}
.style14 {
	color: #0065FF;
	font-weight: bold;
}
.style15 {
	color: #000000;
	font-weight: bold;
}
-->
</style>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidationExtras.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<script language="javascript">

	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = ""

function CloseMessagePad(){

NewsBlock.style.display = "block";
MessageBlock.style.display = "none";
}

function OpenMessagePad(){

NewsBlock.style.display = "none";
MessageBlock.style.display = "block";
}

// below script has been taken from the pop up window for adding messages - my be edited since

var FormFields = new Array()
FormFields[0] = "txt_TITLE|Title|TEXT|Y"						
FormFields[1] = "txt_MESSAGE|Message|TEXT|Y"						
FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
FormFields[3] = "sel_MESSAGEFOR|For|SELECT|Y"		
FormFields[4] = "txt_EXPIRES|Expires|INTEGER|I_Y"		
FormFields[5] = "sel_OPENENDED|Open Ended|SELECT|Y"		

function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/Message_svr.asp"
	RSLFORM.submit()
	}

function GetContractorContacts() {
	if (RSLFORM.sel_MESSAGEFOR.value != "ALLORG" && RSLFORM.sel_MESSAGEFOR.value != "") {
		RSLFORM.action = "Serverside/GetContractors.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else 
		SetExpiry()
	}

function LoadPrevious() {
	if (RSLFORM.sel_MESSAGEFOR.value == "PREVIOUS") {
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else
		SetExpiry()
	}
		
function GetEmployees(){
	if (RSLFORM.sel_TEAMS.value == "ALL") {
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value='ALL'>All Employees</option></select>"
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('5')
		}
	else if (RSLFORM.sel_TEAMS.value != "") {
		DisableItems('5')				
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()
		}
	else {
		DisableItems('5')						

		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value=''>Please select a team</option></select>"
		}
	}

function SetExpiry(){
	if (RSLFORM.sel_MESSAGEFOR.value == "ALL" || RSLFORM.sel_MESSAGEFOR.value == "ALLORG") {
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('5')

		}
	else {
		DisableItems('5')
		
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		}
	RSLFORM.sel_OPENENDED.disabled = false;
	}	
	
function ShowDiv(theDiv){
	
	var FormFields2 = new Array("div_newmessage","div_messagelist", "div_sentlist", "div_EmployeeFolderList", "div_defaultlist")
	
	for (i=0; i < 5; i++){

		if (FormFields2[i]== theDiv )
			document.getElementById(FormFields2[i]).style.display = "block"
		else
			document.getElementById(FormFields2[i]).style.display = "none"
		}

	}
	
function ShowAndHideSearch(action){
	
	document.getElementById("div_findmessage").style.display = action

}

function SearchForm(){

	if (RSLFORM.searchSTR.value == "") {
		alert("Please enter your search text into the search For box");
		return false;
	}
		RSLFORM.action = "Serverside/find_Mail.asp"
	RSLFORM.target = "ServerFrame"
	RSLFORM.submit()
	ShowDiv("div_defaultlist")
	}

function getSentList(thePage){

	RSLFORM.action = "Serverside/" + thePage + ".asp"
	RSLFORM.target = "ServerFrame"
	RSLFORM.submit()

	}
	
function GoArchive() {
	
	RSLFORM.action = "Serverside/Message_Deleted.asp"
	RSLFORM.target = "ServerFrame"
	RSLFORM.submit()

	}	

function ViewMessage(messageID){
	
	RSLFORM.hid_MessageID.value = messageID
	RSLFORM.action = "Serverside/Message_Body.asp"
	RSLFORM.target = "ServerFrame"
	RSLFORM.submit()
	
	}
	
function setEmployeeFolderID(theID){
	RSLFORM.hid_EmployeeFolderID.value = theID
	}

function ReplyToMessage(){
	
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "serverside/Reply_svr.asp"
	RSLFORM.submit()
	}
	
function GoDelete(){

alert("Delete functionality to come!")
}
		
// ADD ID FROM IDLIST
function do_sum(int_num){
		
		if (document.getElementById("check" + int_num+"").checked == true){	
			if (int_initial == 0){ // first entry
				str_idlist = str_idlist + int_num.toString();
				}
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}
			
		document.getElementById("idlist").value = str_idlist;
	}
	
	// REMOVE ID FROM IDLIST
function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}

</script>
</head>

<body>
<table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
 <form name="RSLFORM" method="POST">
  <tr>
    <td>
	<table width="736" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#FFFFFF">
        <td height="4">
		<table width="732" height="59" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="12" colspan="2">
			</td>
            </tr>
          <tr>
            <td width="13" height="27">&nbsp;</td>
            <td width="719" valign="bottom"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><IMG SRC="images/im_rteidmark.gif" WIDTH=88 HEIGHT=22 ALT=""></td>
                <td width="5">&nbsp;</td>
                <td width="183">&nbsp;</td>
                <td width="339" rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="191" rowspan="2"><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=29 ALT=""><IMG SRC="images/im_rslmanager.gif" WIDTH=163 HEIGHT=29 ALT=""></td>
                <td>&nbsp;</td>
                <td rowspan="2"><table width="98%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="18"></td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#C0C0C0">
					</td>
                  </tr>
                  <tr>
                    <td width="22">
					</td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right"><IMG SRC="images/im_strapline.gif" WIDTH=339 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="5" colspan="2"></td>
          </tr>
        </table></td>
        <td height="4">&nbsp;</td>
      </tr>
      <tr>
        <td height="4" width="733" bgcolor="#669900">
		</td>
        <td width="3" height="4"><IMG SRC="images/im_barend.gif" WIDTH=3 HEIGHT=4 ALT=""></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="617"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="613" height="23" bgcolor="#FFFFFF"><table width="607" height="32" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="11" height="27">&nbsp;</td>
                    <td width="426" valign="bottom"><img src="/BoardMembers/images/im_messagepad-.gif" width="92" height="15"></td>
                    <td width="170" align="right" valign="middle"> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backbutton.gif" width="11" height="11" border="0"></a> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backtomywhiteboard.gif" width="121" height="13" border="0"></a></td>
                  </tr>
                  <tr>
                    <td height="5" colspan="3"></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
          </table></td>
          <td width="121">
          
		   	<%=id_card_HTML%>
		 
		  </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="380" valign="top"><table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
      <tr>
        <td width="160" valign="top"><table width="115" border="0" cellpadding="0" cellspacing="0" class="bgColour">
          <tr>
            <td><table width="161"  border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="1" bgcolor="#669900"></td>
                  <td width="159" height="23" bgcolor="#FFFFFF"><table width="110" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="5">&nbsp;</td>
                        <td><span class="style4"> Folders</span></td>
                      </tr>
                  </table></td>
                  <td width="1" bgcolor="#669900"></td>
                </tr>
                <tr>
                  <td width="1" height="1"></td>
                  <td width="159" height="1" bgcolor="#669900"></td>
                  <td width="1" height="1"></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="3"></td>
          </tr>
          <tr>
            <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=160 HEIGHT=4 ALT=""></td>
          </tr>
          <tr>
            <td valign="bottom" bgcolor="#FFFFFF"><table width="150" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=144 ALT=""></td>
                  <td valign="top" bgcolor="#FFFFFF"><TABLE CELLSPACING=0 CELLPADDING=0 valign=top  width=153>
                      
                      <TR valign="middle">
                        <TD height="5" colspan="3" align=right NOWRAP></TD>
                      </TR>
                      <TR valign="middle">
                        <TD align=center width=5 NOWRAP>&nbsp;</TD>
                        <TD align=center width=17 NOWRAP><img src="/BoardMembers/images/folder-inbox.gif" style='cursor:hand' width="15" height="12" onClick="ShowDiv('div_messagelist')"> </TD>
                        <td width="132"  style='word-breal:break-all;cursor:hand'  onClick="ShowDiv('div_messagelist')" >&nbsp;Todays Messages </TD>
                      </TR>
                       <TR valign="middle">
                        <TD height="5" colspan="3" align=right NOWRAP></TD>
                      </TR>
                      <TR valign="middle">
                        <TD width="5" align=center NOWRAP>&nbsp;</TD>
                        <TD width="17" align=center NOWRAP><img src="/BoardMembers/images/folder-sent.gif" width="15" height="12"></TD>
                        <td  style='word-breal:break-all;cursor:hand'  onClick="ShowDiv('div_sentlist'); getSentList('Message_Sent')" >&nbsp;Sent</TD>
                      </TR>
                       <TR valign="middle">
                        <TD height="5" colspan="3" align=right NOWRAP></TD>
                      </TR>
                      <TR valign="middle">
                        <TD width="5" align=center NOWRAP>&nbsp;</TD>
                        <TD width="17" align=center NOWRAP><img src="/BoardMembers/images/trash.gif" width="13" height="12"></TD>
                        <td  style='word-breal:break-all;cursor:hand' onClick="GoArchive()" >&nbsp;Trash</TD>
                      </TR>
                      <TR valign="middle">
                        <TD align=center NOWRAP>&nbsp;</TD>
                        <TD align=center NOWRAP>&nbsp;</TD>
                        <td  style='word-breal:break-all;cursor:hand' onClick="GoArchive()" ><input type="hidden" name="hid_EmployeeFolderID" value="">
						<input type="hidden" name="hid_MessageID" value=""></TD>
                      </TR>
                     
                     <%=Senders_folderTree%>
                     </TABLE></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td valign="bottom">&nbsp;</td>
          </tr>
        </table></td>
        <td width="14">&nbsp;</td>
        <td valign="top">
<div name="div_findmessage" id="div_findmessage" style="display:none">
          <table width="559" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                  <table width="557" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                    <tr>
                      <td valign="top"><table width="556" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                          <tr>
                            <td><table width="555"  border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td width="1" bgcolor="#669900"></td>
                                  <td width="553" height="23" align="left" bgcolor="#FFFFFF"><table width="545" height="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="6" align="right">&nbsp;</td>
                                        <td width="481" align="left"><span class="style4">Find Message </span></td>
                                        <td width="481" align="right"><img src="/BoardMembers/images/im_close.gif" width="37" height="11" style="cursor:hand" onClick="ShowAndHideSearch('none')" ><img src="/BoardMembers/images/im_closebutton.gif" width="11" height="11" style="cursor:hand" onClick="ShowAndHideSearch('none')"></td>
                                      </tr>
                                  </table></td>
                                  <td width="1" bgcolor="#669900"></td>
                                </tr>
                                <tr>
                                  <td width="1" height="1"></td>
                                  <td width="553" height="1" bgcolor="#669900"></td>
                                  <td width="1" height="1"></td>
                                </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="6"></td>
                          </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="411" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="6">&nbsp;</td>
                      <td width="25">&nbsp;</td>
                      <td width="28">&nbsp;</td>
                      <td width="7">&nbsp;</td>
                      <td width="282">&nbsp;</td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">Search For </td>
                      <td>&nbsp;</td>
                      <td><input name="searchSTR" type="text" class="textbox200" id="searchSTR" maxlength="24" style='WIDTH:280PX'></td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>* searches the From and Subject title fields only. </td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>Find Message<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT="" > <IMG NAME="im_rightbutton" SRC="images/im_rightbutton.gif" WIDTH=11 HEIGHT=11 BORDER=0 ALT="" style="cursor:hand" onClick="SearchForm()"></td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                </table>
	
				</td>
            </tr>
          </table></div>
		<div id="div_messagelist" name="div_messagelist" style="display:block ">
		<table width="559" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
                <table width="557" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td valign="top"><table width="556" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                        <tr>
                          <td><table width="555"  border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="1" bgcolor="#669900"></td>
                                <td width="553" height="23" align="right" bgcolor="#FFFFFF"><table width="550" height="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="295" align="left"><span class="style4">&nbsp;Todays Messages 
                                      <input name="idlist" type="hidden" id="idlist">
                                    </span></td>
                                    <td width="11" align="left"><span class="style6">|</span></td>
                                    <td width="28"><span class="style7"><img  style="cursor:hand" onClick="ShowDiv('div_newmessage')" src="/BoardMembers/images/im_envelopec.gif" width="16" height="11"></span></td>
                                    <td width="38"><span class="style6" style="cursor:hand" onClick="ShowDiv('div_newmessage')">New</span></td>
                                    <td width="15" align="left"><span class="style6"> |</span></td>
                                    <td width="28"><span class="style7"><img src="/BoardMembers/images/im_envelopec.gif" width="16" height="11" onClick="GoDelete()" style="cursor:hand"></span></td>
                                    <td width="48"><span class="style6"  onClick="GoDelete()" style="cursor:hand">Delete</span></td>
                                    <td width="16"><span class="style6">|</span></td>
                                    <td width="23"><span class="style7"><img style="cursor:hand" src="/BoardMembers/images/magnifier.gif" width="16" height="16" onClick="ShowAndHideSearch('block')"></span></td>
                                    <td width="48"><span class="style6"  style="cursor:hand" onClick="ShowAndHideSearch('block')">Find </span></td>
                                    </tr>
                                </table>
                                  </td>
                                <td width="1" bgcolor="#669900"></td>
                              </tr>
                              <tr>
                                <td width="1" height="1"></td>
                                <td width="553" height="1" bgcolor="#669900"></td>
                                <td width="1" height="1"></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="6"></td>
                        </tr>
                    </table></td>
                    </tr>
                </table>
              
    
                <table width="552" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="#E9F0DB">
                    <td width="18"><div align="center"><span class="style8"> <img src="/BoardMembers/images/importance.gif" width="4" height="10"></span></div></td>
                    <td width="27"><div align="center"><img src="/BoardMembers/images/im_envelopec.gif" width="16" height="11"></div></td>
                    <td width="23"><input type="checkbox" name="checkbox" value="checkbox"></td>
                    <td width="151" bgcolor="#E9F0DB">From</td>
                    <td width="262">Subject</td>
                    <td width="71" bgcolor="#C8D9A4">&nbsp;<img src="/BoardMembers/images/im_down.gif" width="11" height="11"> Date</td>
                    </tr>
                 <%=main_mail%>
                </table></td>
          </tr>
        </table>
		</div>
		<div id="div_sentlist" name="div_sentlist" style="display:NONE"><BR>Loading Sent List . . . . </div>
		<div id="div_defaultlist" name="div_defaultlist" style="display:NONE"><BR>Loading List . . . . </div>
		<div id="div_EmployeeFolderList" name="div_EmployeeFolderList" style="display:NONE"><BR>Loading List . . . . </div>
		<div name="div_newmessage" id="div_newmessage" style="display:none">
          <table width="559" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td>
                  <table width="557" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                    <tr>
                      <td valign="top"><table width="556" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                          <tr>
                            <td><table width="555"  border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td width="1" bgcolor="#669900"></td>
                                  <td width="553" height="23" align="left" bgcolor="#FFFFFF"><table width="491" height="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="10" align="right">&nbsp;</td>
                                        <td width="481" align="left"><span class="style4">New Message </span></td>
                                      </tr>
                                  </table></td>
                                  <td width="1" bgcolor="#669900"></td>
                                </tr>
                                <tr>
                                  <td width="1" height="1"></td>
                                  <td width="553" height="1" bgcolor="#669900"></td>
                                  <td width="1" height="1"></td>
                                </tr>
                            </table></td>
                          </tr>
                          <tr>
                            <td height="6"></td>
                          </tr>
                      </table></td>
                    </tr>
                  </table>
                  <table width="555" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="6">&nbsp;</td>
                      <td width="25">&nbsp;</td>
                      <td width="28">&nbsp;</td>
                      <td width="7">&nbsp;</td>
                      <td width="282">&nbsp;</td>
                      <td width="20">&nbsp;</td>
                      <td width="91">&nbsp;</td>
                      <td width="49">&nbsp;</td>
                      <td width="51">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">Title</td>
                      <td>&nbsp;</td>
                      <td><input name="txt_TITLE" type="text" class="textbox200" id="MessageTitle" maxlength="24" STYLE='WIDTH:280PX'></td>
                      <td><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></td>
                      <td>Importance</td>
                      <td><span class="style9">High</span></td>
                      <td><span class="style8"><img src="/BoardMembers/images/importance.gif" width="4" height="10">
                          <input name="IMPORTANCEID" type="radio" value="1">
</span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><span class="style14">Normal</span></td>
                      <td><span class="style8"><img src="/BoardMembers/images/importance_medium.gif" width="4" height="10">
                          <input name="IMPORTANCEID" type="radio" value="2" checked>
</span></td>
                    </tr>
                    <tr>
                      <td rowspan="4">&nbsp;</td>
                      <td colspan="2" rowspan="4" valign="top">Message</td>
                      <td rowspan="4">&nbsp;</td>
                      <td rowspan="4"><textarea name="txt_MESSAGE" rows="3" wrap="on" class="textbox200" STYLE="overflow:hidden;WIDTH:280PX;height:74"
								onKeyUp="if (this.value.length>500) { alert('Please enter no more than 500 chars'); this.value=this.value.substring(0,499) }"
								></textarea></td>
                      <td><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"></td>
                      <td>&nbsp;</td>
                      <td valign="top"><span class="style15">Low</span></td>
                      <td valign="top"><span class="style8"><img src="/BoardMembers/images/importance_low.gif" width="4" height="10">
                          <input name="IMPORTANCEID" type="radio" value="3">
</span></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                      <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>Open Ended </td>
                      <td colspan="2">
					  <select name="sel_OPENENDED" style="width:70" class="textbox200">
                        <option value="1" selected>Yes</option>
                        <option value="0">No</option>
                      </select>
					  <image src="/js/FVS.gif" name="img_OPENENDED" width="15px" height="15px" border="0"></td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">Team</td>
                      <td>&nbsp;</td>
                      <td><select name="sel_TEAMS" class="textbox200" onchange="GetEmployees()" STYLE='WIDTH:280PX'>
                        <option value="">Please Select</option>
                        <option value="ALL">All Teams</option>
                        <%
									Dim rsTeams
									OpenDB()
									SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM ORDER BY TEAMNAME"
									Call OpenRs(rsTeams, SQL)
						
									While (NOT rsTeams.EOF)
									%>
                        <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
                        <%
									  rsTeams.MoveNext()
									Wend
									If (rsTeams.CursorType > 0) Then
									  rsTeams.MoveFirst
									Else
									  rsTeams.Requery
									End If
									CloseRs(rsTeams)
									CloseDB()
									%>
                        <option value='NOT'>No Team</option>
                      </select></td>
                      <td><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">Person</td>
                      <td>&nbsp;</td>
                      <td><select name="sel_MESSAGEFOR" class="textbox200" STYLE='WIDTH:280PX'>
                        <option value="">Please select a team</option>
                      </select></td>
                      <td><image src="/js/FVS.gif" name="img_MESSAGEFOR" width="15px" height="15px" border="0"><image src="/js/FVS.gif" name="img_EXPIRES" width="15px" height="15px" border="0"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">Expires</td>
                      <td>&nbsp;</td>
                      <td><input type="text" name="txt_EXPIRES" value="Not Applicable" class="textbox200" STYLE='WIDTH:100PX' disabled>                        &nbsp;&nbsp; Days
                        <input name="hid_CREATEDBY" type="hidden" value="<%= Session("UserID") %>">
                        <input name="hid_ACTION" type="hidden" value="NEW">
                        <input name="hid_ORGID" type="hidden" value=""></td>
                      <td><image src="/js/FVS.gif" name="img_EXPIRES" width="15px" height="15px" border="0"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><IMG NAME="im_sendamessage" SRC="images/im_newmessage.gif" WIDTH=77 HEIGHT=11 ALT=""  style="cursor:hand" onClick="SaveForm()"> <IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=11 ALT=""> <IMG NAME="im_rightbutton" SRC="images/im_rightbutton.gif" WIDTH=11 HEIGHT=11 BORDER=0 ALT="" style="cursor:hand" onClick="SaveForm()"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                </table>
	
				</td>
            </tr>
          </table></div>
		  
		  </td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td valign="top">&nbsp; </td>
      </tr>
    </table>      </td>
    </tr>
    <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="17" bgcolor="#669900">  <table width="400" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="382" align="right" valign="middle"><span class="style3 style2">&copy;2005 RSLmanager is a Reidmark ebusiness system</span></td>
        <td valign="bottom" align="right" width="18"></td>
      </tr>
    </table></td>
  </tr> 
  <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="3"><img src="images/im_corner-42.gif" width=3 height=3 alt="" ></td>
  </tr> 
   </FORM>
</table>
<iframe name="ServerFrame" style='display:none'></iframe><!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
