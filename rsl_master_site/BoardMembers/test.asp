<html>
<head>
<script language="JavaScript1.2">
function compareDates (value1, value2) {
   var date1, date2;
   var month1, month2;
   var year1, year2;

   month1 = value1.substring (0, value1.indexOf ("-"));
   date1 = value1.substring (value1.indexOf ("-")+1, value1.lastIndexOf ("-"));
   year1 = value1.substring (value1.lastIndexOf ("-")+1, value1.length);

   month2 = value2.substring (0, value2.indexOf ("-"));
   date2 = value2.substring (value2.indexOf ("-")+1, value2.lastIndexOf ("-"));
   year2 = value2.substring (value2.lastIndexOf ("-")+1, value2.length);

   if (year1 > year2) return 1;
   else if (year1 < year2) return -1;
   else if (month1 > month2) return 1;
   else if (month1 < month2) return -1;
   else if (date1 > date2) return 1;
   else if (date1 < date2) return -1;
   else return 0;
} 
</script>
</head>

<body>
<form name="example">
<input type="text" name="date1"><br>
<input type="text" name="date2">
<input type="button" value="compare" onClick="alert (compareDates (date1.value, date2.value))">
</form>
</body>
</html>