<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim rsLoader

	EmployeeID = session("userid")
	if (EmployeeID = "") then EmployeeID = -1 end if
OpenDB()
	

	
	  SQL = " SELECT	T.TITLEID AS TITLEID, ISNULL(T.DESCRIPTION,'') AS TITLE, " &_
			"	ISNULL(E.FIRSTNAME,'')   AS FIRSTNAME, " &_
			"	ISNULL(E.LASTNAME,'')    AS LASTNAME, " &_
			"	(REPLACE((ISNULL(A.ADDRESS1,'') + ', ' + ISNULL(A.ADDRESS2,'')+ ', ' + ISNULL(A.ADDRESS3,'')),', , ', '')) AS ADDRESS, " &_
			"	ISNULL(A.POSTALTOWN,'')  AS TOWN, " &_
			"	ISNULL(A.COUNTY,'')      AS COUNTY, " &_
			"	ISNULL(A.POSTCODE,'')    AS POSTCODE, " &_
			"	ISNULL(A.WORKDD,'N/A')      AS OFFICETEL, " &_
			"	ISNULL(A.WORKEXT,'N/A')     AS WORKEXT, " &_
			"	ISNULL(A.HOMETEL,'N/A')     AS HOMETEL, " &_
			"	ISNULL(A.MOBILE,'N/A')      AS MOBILE, " &_
			"	ISNULL(A.HOMEEMAIL,'N/A')   AS HOMEEMAIL, " &_
			"	ISNULL(A.WORKEMAIL,'N/A')   AS WORKEMAIL, " &_
			"	ISNULL(J.JOBTITLE,'N/A')    AS POSITION" &_
			" FROM E__EMPLOYEE E " &_
			"	LEFT JOIN G_TITLE T ON T.TITLEID = E.TITLE " &_
			"	LEFT JOIN E_CONTACT A ON A.EMPLOYEEID = E.EMPLOYEEID " &_
			"	LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
			" WHERE E.EMPLOYEEID = " & EmployeeID
	
	'Response.write SQL
	Call OpenRs(rsLoader, SQL)
		
	if (NOT rsLoader.EOF) then
		l_titleID	= rsLoader("TITLEID")
		l_title		= rsLoader("TITLE")
		l_firstname	= rsLoader("FIRSTNAME")
		l_lastname	= rsLoader("LASTNAME")
		l_address	= rsLoader("ADDRESS")
		l_town		= rsLoader("TOWN")
		l_county	= rsLoader("COUNTY")
		l_postcode	= rsLoader("POSTCODE")
		l_officetel	= rsLoader("OFFICETEL")
		l_workext	= rsLoader("WORKEXT")
		l_hometel	= rsLoader("HOMETEL")
		l_mobile	= rsLoader("MOBILE")
		l_homeemail	= rsLoader("HOMEEMAIL")
		l_officeemail = rsLoader("WORKEMAIL")
		l_position	= rsLoader("POSITION") 

	end if
	
	CloseRs(rsLoader)

	Call BuildSelect(lstTitles, "sel_TITLE", "G_TITLE", "TITLEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_titleid, "width=140", "textbox200", " tabIndex='1'")

	
	CloseDB()
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
	

%>
<HTML>
<HEAD>
<TITLE>BM_Whiteboard</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<script language="javascript" >


	function SaveProfile(){

		RSLFORM.action = "../serverside/MyProfileAmend_svr.asp"
		RSLFORM.submit()
	
	}

</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
-->
</style>

</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="">
<table width="281" border="0" cellpadding="1" cellspacing="1">
                           <form name="RSLFORM" method=post> <tr>
                              <td width="3">&nbsp;</td>
                              <td colspan="2"><table width="269" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="3"><strong>Main Profile Details</strong></td>
                                </tr>
          						<tr>
                                    <td colspan="3" height="5">
									</td>
                                  </tr>
                                <tr>
                                  <td width="103">Title</td>
                                  <td width="4">:</td>
                                  <td width="160"><%=lstTitles%>
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME"></td>
                                </tr>
                                <tr>
                                  <td width="103">Firstname</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_FIRSTNAME" maxlength="50" TABINDEX=1 value='<%=l_firstname%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_FIRSTNAME"></td>
                                </tr>
                                <tr>
                                  <td width="103">Last Name</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_LASTNAME" maxlength="50" TABINDEX=1 value='<%=l_lastname%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_LASTNAME"></td>
                                </tr>
                                <tr>
                                  <td width="103">Position</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_POSITION" maxlength="50" TABINDEX=1 value='<%=l_Position%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_POSITION"></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3">
							  </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2"><table width="269" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="3"><strong>Address and Main Contact Details </strong></td>
                                </tr>
                                       <tr>
                                    <td colspan="3" height="5">
									</td>
                                  </tr>
                                <tr>
                                  <td width="103">Address </td>
                                  <td width="4">:</td>
                                  <td width="160"><input type="text" class="textbox200" name="txt_ADDRESS" maxlength="50" TABINDEX=1 value='<%=l_address%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_ADDRESS1"></td>
                                </tr>
                                <tr>
                                  <td width="103">Town</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_TOWN" maxlength="50" TABINDEX=1 value='<%=l_town%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_TOWN"></td>
                                </tr>
                                <tr>
                                  <td width="103">County</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_COUNTY" maxlength="50" TABINDEX=1 value='<%=l_county%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_COUNTY"></td>
                                </tr>
                                <tr>
                                  <td width="103">PostCode</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_POSTCODE" maxlength="50" TABINDEX=1 value='<%=l_postcode%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_POSTCODE"></td>
                                </tr>
                                <tr>
                                  <td width="103">Work Tel</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_WORKTEL" maxlength="50" TABINDEX=1 value='<%=l_officetel%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_WORKTEL"></td>
                                </tr>
                                <tr>
                                  <td width="103">Mobile</td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_MOBILE" maxlength="50" TABINDEX=1 value='<%=l_mobile%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_MOBILE"></td>
                                </tr>
                                <tr>
                                  <td width="103">Home Tel </td>
                                  <td width="4">:</td>
                                  <td><input type="text" class="textbox200" name="txt_HOMETEL" maxlength="50" TABINDEX=1 value='<%=l_hometel%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_HOMETEL"></td>
                                </tr>
                              </table></td>
                            </tr>
<tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="6" colspan="3">
							  </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2"><table width="269" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="3"><strong>Email Details </strong></td>
                                  </tr>
                                  <tr>
                                    <td colspan="3" height="5">
									</td>
                                  </tr>
                                  <tr>
                                    <td width="104">Office Email</td>
                                    <td width="4">&nbsp;</td>
                                    <td width="159"><input type="text" class="textbox200" name="txt_OFFICEEMAIL" maxlength="50" TABINDEX=1 value='<%=l_officeemail%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_OFFICEEMAIL"></td>
                                  </tr>
                                  <tr>
                                    <td width="104">Home Email</td>
                                    <td width="4">&nbsp;</td>
                                    <td><input type="text" class="textbox200" name="txt_HOMEEMAIL" maxlength="50" TABINDEX=1 value='<%=l_homeemail%>' style="width:140">
                                  <img src="/js/FVS.gif" width="15" height="15" name="img_HOMEEMAIL"></td>
                                  </tr>
                                </table></td>
                            </tr>
							<tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3">
							  </td>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2"><table width="269" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td colspan="3" height="5"></td>
                                </tr>
                                <tr>
                                  <td width="103">&nbsp;</td>
                                  <td width="4">&nbsp;</td>
                                  <td width="158"><input name="Submit" type="button" class="RSLButton" onClick="SaveProfile()" value="Save"></td>
                                </tr>
                              </table></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td width="119">&nbsp;</td>
                              <td width="149">&nbsp;</td>
                            </tr>
</FORM></table>
</BODY>
</HTML>