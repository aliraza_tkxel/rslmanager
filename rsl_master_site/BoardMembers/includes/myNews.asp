<%
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMyNews = Server.CreateObject("ADODB.Recordset")
rsMyNews.ActiveConnection = RSL_CONNECTION_STRING
rsMyNews.Source = "SELECT TOP 1 NEWSID, CONTENT, TITLE, NEWSIMAGE FROM G_NEWS WHERE NEWSFOR LIKE '%," & Session("TeamCode") & ",%' ORDER BY DATECREATED DESC, NEWSID DESC"
rsMyNews.CursorType = 0
rsMyNews.CursorLocation = 2
rsMyNews.LockType = 3
rsMyNews.Open()
rsMyNews_numRows = 0
%>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
	<style type="text/css">
<!--
.style1 {
	color: #0000CC;
	font-style: italic;
}
-->
    </style>
<table width="366" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="8">&nbsp;</td>
        <td width="224" valign="top">
		<%
			if (NOT rsMyNews.EOF) Then
				Response.Write "<b><font style='font-size:14px'>" & rsMyNews("Title") & "</font></b><br><br>"
				NewsContent = "" & rsMyNews("Content") & ""
				NewsContent = Left(stripHTML(NewsContent),100) & " ...."
			    Response.Write "<font style='font-size:10px' >" & NewsContent & "</font>"
			Else
		%>
            <font style='font-size:14px'>Main Headline</font><br>
            <br>
            <font style='font-size:10px'> RSL Manager delivers efficiency and performance to financial control, quality, communications... </font>
        <% 
			End If 
		%>
            <br>            <br>
        <%  if (NOT rsMyNews.EOF) Then %>
            <br>
            &nbsp;<a href='News.asp?NewsID=<%=rsMyNews("NEWSID")%>' class="style1"><font style='font-size:10px' ><img src="/BoardMembers/images/im_morenews.gif" width="66" height="11" border="0"></font></a>
        <%  end if%></td>
        <td width="5">&nbsp;</td>
        <td width="129" align="right" valign="top">
		<% if (NOT rsMyNews.EOF) Then 
					tempdata = rsMyNews("NewsImage") & ""
					if (rsMyNews("NewsImage") <> "") Then
						Response.Write "<image src='/News/NewsImages/" & rsMyNews("NewsImage") & "' width='100' height='100' hspace=0 valign=top align=right>"
					Else
						Randomize
						temp = Int((upperbound) * Rnd + 1)
						Response.Write "<image src='/myImages/news_image" & temp & ".gif' width='100px' height='100px' hspace=0 valign=top align=right alt='Default News Image'>"
					End If
				Else 
					Randomize
					temp = Int((upperbound) * Rnd + 1)
		%>
            <IMG SRC="images/im_picture.gif" WIDTH=100 HEIGHT=100 ALT="">          
		<% 
			End If 
		%>
		</td>
      </tr>
      <%
		rsMyNews.close()
		Set rsMyNews = Nothing
	%>
    </table>
	