<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim rsLoader

	EmployeeID = session("userid")
	if (EmployeeID = "") then EmployeeID = -1 end if
	
	OpenDB()
	
	  SQL = " SELECT	ISNULL(T.DESCRIPTION,'') AS TITLE, " &_
			"	ISNULL(E.FIRSTNAME,'')   AS FIRSTNAME, " &_
			"	ISNULL(E.LASTNAME,'')    AS LASTNAME, " &_
			"	(REPLACE((ISNULL(A.ADDRESS1,'') + ', ' + ISNULL(A.ADDRESS2,'')+ ', ' + ISNULL(A.ADDRESS3,'')),', , ', '')) AS ADDRESS, " &_
			"	ISNULL(A.POSTALTOWN,'')  AS TOWN, " &_
			"	ISNULL(A.COUNTY,'')      AS COUNTY, " &_
			"	ISNULL(A.POSTCODE,'')    AS POSTCODE, " &_
			"	ISNULL(A.WORKDD,'N/A')      AS OFFICETEL, " &_
			"	ISNULL(A.WORKEXT,'N/A')     AS WORKEXT, " &_
			"	ISNULL(A.HOMETEL,'N/A')     AS HOMETEL, " &_
			"	ISNULL(A.MOBILE,'N/A')      AS MOBILE, " &_
			"	ISNULL(A.HOMEEMAIL,'N/A')   AS HOMEEMAIL, " &_
			"	ISNULL(A.WORKEMAIL,'N/A')   AS WORKEMAIL, " &_
			"	ISNULL(J.JOBTITLE,'N/A')    AS POSITION, " &_
			"	ISNULL(J.STARTDATE,'')   AS DATECOMMENCED, " &_
			"	DATEADD(YEAR, 3, J.STARTDATE) AS REVIEW, " &_
			"	case WHEN J.ACTIVE = 1  then 'ACTIVE' else 'INACTIVE' end EMPL_STATUS " &_
			" FROM E__EMPLOYEE E " &_
			"	LEFT JOIN G_TITLE T ON T.TITLEID = E.TITLE " &_
			"	LEFT JOIN E_CONTACT A ON A.EMPLOYEEID = E.EMPLOYEEID " &_
			"	LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
			" WHERE E.EMPLOYEEID = " & EmployeeID
	
	'Response.write SQL
	Call OpenRs(rsLoader, SQL)
		
	if (NOT rsLoader.EOF) then
		
		l_title		= rsLoader("TITLE")
		l_firstname	= rsLoader("FIRSTNAME")
		l_lastname	= rsLoader("LASTNAME")
		l_address	= rsLoader("ADDRESS")
		l_town		= rsLoader("TOWN")
		l_county	= rsLoader("COUNTY")
		l_postcode	= rsLoader("POSTCODE")
		l_officetel	= rsLoader("OFFICETEL")
		l_workext	= rsLoader("WORKEXT")
		l_hometel	= rsLoader("HOMETEL")
		l_mobile	= rsLoader("MOBILE")
		l_homeemail	= rsLoader("HOMEEMAIL")
		l_officeemail = rsLoader("WORKEMAIL")
		l_position	= rsLoader("POSITION") 
		l_review 	= rsLoader("REVIEW")
		l_datecommenced	= rsLoader("DATECOMMENCED") 
		l_emp_status	= rsLoader("EMPL_STATUS") 

	end if
	
	CloseRs(rsLoader)
	CloseDB()
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
	

%>
<HTML>
<HEAD>
<TITLE>BM_Whiteboard</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<script language="javascript" >


function fill_id_card(){

	//parent.id_card.innerHTML = sendInfo.innerHTML
	//sendInfo.style.display = "none"
}

</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
-->
</style>

</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 onLoad="">
<table width="281" border="0" cellpadding="1" cellspacing="1">
                            <tr>
                              <td width="3">&nbsp;</td>
                              <td colspan="2"><%=l_title & " " & l_firstname & " " &  l_lastname%></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2"><%=l_position%></td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3">
							  </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td width="119"><%= l_address & "<BR>" & l_town & "<BR>" & l_county & "<BR>" & l_postcode%></td>
                              <td width="149" align="right" valign="bottom">
							  	<table width="133">
									<tr>
										<td width="44">Work:</td>
										<td width="86"><%=l_officetel%></td>
									</tr>
									<tr>
										<td>Mobile:</td>
										<td><%=l_mobile%></td>
									</tr>
									<tr>
										<td>Home:</td>
										<td><%=l_hometel%></td>
									</tr>
								</table>
                                </td>
                            </tr>
<tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="6" colspan="3">
							  </td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2">Office Email:<a href="mailto:<%=l_officeemail%>"><%=l_officeemail%></a></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td colspan="2">Home Email: <a href="mailto:<%=l_homeemail%>"><%=l_homeemail%></a></td>
                            </tr>
							<tr>
                              <td height="5" colspan="3"></td>
                            </tr>
                            <tr>
                              <td>
							  </td>
                              <td height="1" colspan="2" bgcolor="#009900">
							  </td>
                            </tr>
                            <tr>
                              <td height="5" colspan="3">
							  </td>
                            <tr>
                              <td>&nbsp;</td>
                              <td>Date Commenced: </td>
                              <td><%=l_datecommenced%></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>Review Date: </td>
                              <td><%=l_review%></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
</table>
</BODY>
</HTML>