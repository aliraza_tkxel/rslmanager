<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%


Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput,reply_row
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

MessageID =request("hid_MessageID")
		IF (MessageID = "") THEN
			MessageID = "0"
		END IF

 reply_row()

	Function reply_row()
	
		OPENDB()
			
		FolderID= doc_FOLDERID
		SQL = 	"SELECT M.DATECREATED AS MESSAGEDATE,M.MESSAGE, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME "&_
				"FROM G_MESSAGEHISTORY M " &_
				"	INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY " &_
				"WHERE JOURNALID = " & MessageID
		
		Call OpenRs(rsFolder, SQL)
		
		While not rsFolder.eof
		
				reply_row = reply_row & "       <tr bgcolor='#E9F0DB'><td colspan='5'>&nbsp;</td></tr><tr bgcolor=""#E9F0DB""  height='20'>" & vbCrLf
				reply_row = reply_row & "                    <td>&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                    <td bgcolor=""#669900"">&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                    <td colspan=""2"" bgcolor=""#669900""><font color=white><b>Reply: " & rsFolder("MESSAGEDATE") & " </b></font></td>" & vbCrLf
				reply_row = reply_row & "                    <td>&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                  </tr>" & vbCrLf
				reply_row = reply_row & "                  <tr bgcolor=""#E9F0DB"">" & vbCrLf
				reply_row = reply_row & "                    <td>&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                    <td bgcolor=""#BCD7B9"">&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                    <td colspan=""2""><table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">" & vbCrLf
				reply_row = reply_row & "                      <tr>" & vbCrLf
				reply_row = reply_row & "                        <td width=""29%"" bgcolor=""#BCD7B9"" valign='top'><BR>" & rsFolder("FULLNAME")  & " </td>" & vbCrLf
				reply_row = reply_row & "                        <td width=""71%"" bgcolor=""#DFEACA"" valign='top'><BR>" & rsFolder("MESSAGE")  & "<BR><BR></td>" & vbCrLf
				reply_row = reply_row & "                      </tr>" & vbCrLf
				reply_row = reply_row & "                    </table></td>" & vbCrLf
				reply_row = reply_row & "                    <td>&nbsp;</td>" & vbCrLf
				reply_row = reply_row & "                  </tr>" & vbCrLf
				reply_row = reply_row & "      				<tr bgcolor='#E9F0DB' height='20'><td colspan='5'>&nbsp;</td></tr>"
		
			
			rsFolder.movenext
		Wend
		Call CloseRs(rsFolder)
		CLOSEdb()
		
	End Function




SQL = 		" SELECT  	M.JOURNALID AS MESSAGEID,m.IMPORTANCEID ,M.DATECREATED AS DATESENT, E.FIRSTNAME + ' ' + E.LASTNAME AS SENTBY ,M.MESSAGE, M.TITLE FROM dbo.G_MESSAGEJOURNAL M " &_
			" 			INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY  " &_
			" WHERE  	M.ACTIVE = 1 AND M.JOURNALID = " & MessageID &_
			" ORDER 	BY M.DATECREATED DESC, M.TITLE "

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMessages = Server.CreateObject("ADODB.Recordset")
rsMessages.ActiveConnection = RSL_CONNECTION_STRING
rsMessages.Source = SQL
rsMessages.CursorType = 0
rsMessages.CursorLocation = 2
rsMessages.LockType = 3
rsMessages.Open()
rsMessages_numRows = 0


%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style3 {
	color: #669966;
	font-weight: bold;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">

	function SenddefaultList(){
	parent.div_defaultlist.innerHTML = div_defaultlist.innerHTML;
	}
	


</script>
</head>
<body onLoad="SenddefaultList()">
<div id="div_defaultlist" name="div_defaultlist" style="display:block ">
		<table width="559" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
                <table width="557" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td valign="top"><table width="556" border="0" cellpadding="0" cellspacing="0" class="bgColour">

                        <tr>
                          <td><table width="555"  border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="1" bgcolor="#669900"></td>
                                <td width="553" height="23" align="left" bgcolor="#FFFFFF"><table width="422" height="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td width="12" align="right">&nbsp;</td>
                                      <td width="410" align="left">&nbsp;</td>
                                  </tr>
                                </table></td>
                                <td width="1" bgcolor="#669900"></td>
                              </tr>
                              <tr>
                                <td width="1" height="1"></td>
                                <td width="553" height="1" bgcolor="#669900"></td>
                                <td width="1" height="1"></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="6"></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
                <table width="552" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="#E9F0DB">
                    <td width="19">&nbsp;</td>
                    <td width="9">&nbsp;</td>
                    <td width="53">&nbsp;</td>
                    <td width="460">&nbsp;</td>
                    <td width="11">&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>From</td>
                    <td><%=rsMessages("SENTBY")%></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Sent</td>
                    <td><%=rsMessages("DATESENT")%></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>To</td>
                    <td></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Subject</td>
                    <td><%=rsMessages("Title")%></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td bgcolor="#669966">&nbsp;</td>
                    <td height="20" colspan="2" bgcolor="#669966"><b><font color="#FFFFFF">Message</font></b></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td bgcolor="#DFEACA">&nbsp;</td>
                    <td colspan="2" bgcolor="#DFEACA"  valign='top'><BR><%=rsMessages("Message")%><BR><BR></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2" bgcolor="#E9F0DB"><span class="style3">Replys Below </span></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
				  <% rw reply_row%>
				  

                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><strong>To Reply</strong> to this message use the text box below </td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td height="5" colspan="5">&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><textarea name="txt_REPLY" rows="10" style="width:500"></textarea></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2"><input type="button" name="Submit" value="Reply" onClick="ReplyToMessage()"><input name="hid_THEMESSAGEID" type="hidden" value="<%=MessageID%>"></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr bgcolor="#E9F0DB">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                 <%=main_mail%>
                </table></td>
          </tr>
        </table>
		</div>
</body>
</html>
