<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%

UpdateID = "hid_MessageID"

	'==================================================================================
	'	Catch vars from submited page  - (message.asp)
	'==================================================================================

			Title 			= Request.Form("txt_TITLE")
			Message 		= Request.Form("txt_MESSAGE")
			Teams 			= Request.Form("sel_TEAMS")
			MessageFor 		= Request.Form("sel_MESSAGEFOR")
			
			OrgId 			= Request.Form("hid_ORGID")
			
			if OrgID = "" then OrgID = "NULL" end if
			
				if (theTeam = "ALL") then
					MessageForDatabase = "ALL"
				elseif (theTeam <> "" AND theEmployee = "ALL") then
					MessageForDatabase = "TE" & theTeam
				else
					MessageForDatabase = theEmployee
				end if
			
			CreatedBy 		= Request.Form("hid_CREATEDBY")
			expirydays 		= Request.Form("txt_EXPIRES")
			
				if (expirydays = "Not Applicable" OR expirydays = "" OR isNull(expirydays) OR NOT isNumeric(expirydays)) then
					ExpiryForDatabase = "NULL"
				else
					ExpiryForDatabase = "'" & FormatDateTime(DATEADD("d",DATE,expirydays),1) & "'"
				end if
			
			ExpiryDate 		= ExpiryForDatabase
			ImportanceID 	= Request.Form("IMPORTANCEID")
			OpenEnded 		= Request.Form("sel_OPENENDED")
			
	'==================================================================================
	'	End Catch vars from submited page  - (message.asp)
	'==================================================================================

			
	'==================================================================================
	'	Functions
	'==================================================================================
		
		Function NewRecord ()
			
		
			OpenDB()
			SQL = "SELECT TEAM FROM E_JOBDETAILS WHERE EMPLOYEEID = " & Session("USERID")
			Call OpenRs(rsMyTeamID,SQL)
			if (NOT rsMyTeamID.EOF) then
				TheTeamID = rsMyTeamID("TEAM")
			end if
			CloseRs(rsMyTeamID)
		
			Expires = ExpiryDate
			rw OrgID
			' INSERT INTO THE MESSAGE JOURNAL WHICH IS THE STARTING POINT FOR EVERY MESSAGE
			SQL =  	"INSERT INTO G_MESSAGEJOURNAL (TITLE, MESSAGE, ORGID, MESSAGEFOR, CREATEDBY,EXPIRES,IMPORTANCEID, OPENENDED,ACTIVE) " &_
					"VALUES ('"& Title& "','"& Message& "',"& OrgId & ","& MessageFor& ","& CreatedBy& ","& expires & ","& ImportanceID& "," & OpenEnded & ",1)"
			RW SQL & "<br><br>"
			Conn.Execute SQL, recaffected
			
			' INSERT INTO THE MESSAGE HISTORY WHICH IS THE FLOW OF REPLYS FOR EVERY MESSAGE
		
			SQL = 	"INSERT INTO G_MESSAGEHISTORY ( MESSAGE, ORGID, MESSAGEFOR, CREATEDBY, IMPORTANCEID, OPENENDED) " &_
					"VALUES ('"& Message& "',"& OrgId & ","& MessageFor& ","& CreatedBy& ","& ImportanceID& "," & OpenEnded & ")"
			RW SQL & "<br><br>"
			Conn.Execute SQL, recaffected
			
			TheOrg = SESSION("ORGID")
			if (isNull(TheOrg) OR TheOrg = "") then
				TheOrg = ""
			else
				TheOrg = CStr(TheOrg)
			end if
		
		'		( CStr(MessageForDatabase) = CStr(Session("UserID")) )
		'		OR ( MessageForDatabase = "ALL" AND TheOrg = "" AND TheRequestedOrg = "" )
		'		OR ( MessageForDatabase = CStr("TE" & TheTeamID) AND TheOrg = "" )
		'		OR ( MessageForDatabase = "ALLORG" AND TheRequestedOrg = "" AND TheOrg <> ""  )
		'		OR ( MessageForDatabase = "TE1" AND TheOrg = CStr(TheRequestedOrg) )
		
			if (( CStr(MessageForDatabase) = CStr(Session("UserID")) ) OR ( MessageForDatabase = "ALL" AND TheOrg = "" AND TheRequestedOrg = "" ) OR ( MessageForDatabase = CStr("TE" & TheTeamID) AND TheOrg = "" ) OR ( MessageForDatabase = "ALLORG" AND TheRequestedOrg = "" AND TheOrg <> ""  ) OR ( MessageForDatabase = "TE1" AND TheOrg = CStr(TheRequestedOrg) ) ) then
				AppendText = "&RELOAD=1"
			end if
			GO("Message saved successfully")
		End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	SQL = "UPDATE G_MESSAGEJOURNAL SET ACTIVE = 0 WHERE MESSAGEID = " & ID
	Conn.Execute SQL, recaffected
	AppendText = "&RELOAD=1"	
	GO("Message Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "../MESSAGEs2.asp?Text=" & Text & AppendText
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>