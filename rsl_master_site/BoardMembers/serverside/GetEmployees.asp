<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TeamID = CStr(Request.Form("sel_TEAMS"))
if (TeamID = "") then
	TeamID = "-1"
end if

IF (TeamID = "1") THEN
	SQL = "SELECT NAME, ORGID FROM S_ORGANISATION ORDER BY NAME"
	
	Dim rsOrgs
	OpenDB()
	Call OpenRs(rsOrgs, SQL)
	if (not rsOrgs.EOF) then
		OrgList = "<option value=''>Please Select</option>"
		if (Request("hid_TASK") <> "1") then	
			OrgList = OrgList & "<option value='ALLORG'>ALL Supplier Organisations</option>"
		end if
		while not rsOrgs.EOF
			OrgList = OrgList & "<option value='" & rsOrgs("ORGID") & "'>" & rsOrgs("NAME")& "</option>"
			rsOrgs.moveNext
		wend
	else
		OrgList = "<option value=''>No suppliers found...</option>"
	end if
	CloseRs(rsOrgs)
	CloseDB()
ELSE
	IF (TeamID = "NOT") THEN
		SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				" WHERE TEAM IS NULL AND L.ACTIVE = 1 AND J.ACTIVE = 1 ORDER BY FIRSTNAME, LASTNAME"
	ELSE
		SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " AND L.ACTIVE = 1 AND J.ACTIVE = 1 ORDER BY FIRSTNAME, LASTNAME"
	END IF
	
	Dim rsEmployees
	OpenDB()
	Call OpenRs(rsEmployees, SQL)
	if (not rsEmployees.EOF) then
		EmployeeList = "<option value=''>Please Select</option>"
		if (Request("hid_TASK") <> "1") then	
			EmployeeList = EmployeeList & "<option value='ALL'>ALL Team Members</option>"
		end if
		while not rsEmployees.EOF
			EmployeeList = EmployeeList & "<option value='" & rsEmployees("EMPLOYEEID") & "'>" & rsEmployees("FULLNAME")& "</option>"
			rsEmployees.moveNext
		wend
	else
		EmployeeList = "<option value=''>No employees found...</option>"
	end if
	CloseRs(rsEmployees)
	CloseDB()
END IF
%>			
<html>
<head>
<title>Server Employee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JAVASCRIPT">
function SetEmployees(){
<% IF (TeamID = "1") THEN %>
	<% if (Request("hid_TASK") = "1") then %>
		parent.RSLFORM.sel_ASSIGNTO.outerHTML = Employees.innerHTML
	<% else %>
		parent.RSLFORM.sel_MESSAGEFOR.outerHTML = Employees.innerHTML
	<% end if %>
<% ELSE %>
	<% if (Request("hid_TASK") = "1") then %>
		parent.RSLFORM.sel_ASSIGNTO.outerHTML = Employees.innerHTML
	<% else %>
		parent.RSLFORM.sel_MESSAGEFOR.outerHTML = Employees.innerHTML
	<% end if %>	
<% END IF %>
	}
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000" onload="SetEmployees()">
<div id="Employees">
<% IF (TeamID = "1") THEN %>
	<% if (Request("hid_TASK") = "1") then %>
	<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX' onchange="GetContractorContacts()">
	<% else %>
	<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:280PX' onchange="GetContractorContacts()">
	<% end if %>
<% ELSE %>
	<% if (Request("hid_TASK") = "1") then %>
	<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX'>
	<% else %>
	<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:280PX' onchange="SetExpiry()">
	<% end if %>
<% END IF %>
<%=EmployeeList%>
<%=OrgList%>
</select>
</div>
</body>
</html>
