<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim str_search, str_SQL, str_data, cnt
	
	str_search = Request.Form("txt_NAME")
	COUNT_SQL = "SELECT		COUNT(FIRSTNAME) AS THECOUNT "  &_
				"FROM 		E__EMPLOYEE E " &_
				"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"			LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
				"			LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
				"			LEFT JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID "&_		
				"WHERE		E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & str_search & "%'"&_
				"			AND TC.TEAMCODE LIKE '%," & Session("TeamCode") & ",%' "
	
	str_SQL = 	"SELECT		FIRSTNAME + ' ' + LASTNAME AS FULLNAME, FIRSTNAME,LASTNAME, " &_
				"			TEAMNAME = CASE O.NAME " &_				
				"			WHEN NULL THEN ISNULL(T.TEAMNAME, 'N/A') " &_								
				"			ELSE ISNULL(T.TEAMNAME, 'N/A') + ', <FONT COLOR=BLUE>' + O.NAME + '</FONT>' " &_								
				"			END, " &_																
				"			ISNULL(HOMETEL, 'N/A') AS HOMETEL," &_
				"			ISNULL(MOBILE, 'N/A') AS MOBILE," &_
				"			ISNULL(WORKDD, 'N/A') AS WORKDD," &_
				"			ISNULL(WORKEXT, 'N/A') AS WORKEXT," &_
				"			ISNULL(HOMEEMAIL, 'N/A') AS HOMEEMAIL," &_
				"			ISNULL(WORKEMAIL, 'N/A') AS WORKEMAIL " &_
				"FROM 		E__EMPLOYEE E " &_
				"			LEFT JOIN E_JOBDETAILS J ON E.EMPLOYEEID = J.EMPLOYEEID " &_
				"			LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
				"			LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
				"			LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID " &_			
				"			LEFT JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID "	&_		
				"WHERE		E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & str_search & "%'" &_
				"			AND TC.TEAMCODE LIKE '%" & Session("TeamCode") & "%' "&_		
				"ORDER		BY	E.FIRSTNAME, E.LASTNAME "
	response.write str_SQL
	OpenDB()
	
	get_colleague()
	
	CloseDB()
				
	Function get_colleague()
		Call OpenRs(rsCount, COUNT_SQL)
		if (NOT rsCount.EOF) then
			theCount = rsCount("THECOUNT")
		end if
		CloseRs(rsCount)
		
		Call OpenRs(rsSet, str_SQL)
		str_data = "<table cellspacing=0 cellpadding=0>"
		cnt = 0
		While not rsSet.EOF
			cnt = cnt + 1							
			
			str_data = str_data &_
					"<table cellpadding=0 cellspacing=0> " &_
					"	<TR> " &_
					"		<TD colspan=2 align=right><A NAME='L" & cnt & "'></A>&nbsp; <A HREF='#L" & cnt - 1 & "'><img src='images/im_UP.gif' BORDER=0></A>&nbsp;&nbsp;" & cnt & " of " & theCount & "&nbsp;&nbsp;<A CLASS='RSLBLACK' HREF='#L" & cnt + 1 & "'><img src='images/im_DOWN.gif' BORDER=0></A></TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD height=2></TD> " &_
					"		<TD height=2></TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD>First Name</TD> " &_
					"		<TD  NOWRAP>:&nbsp; " & Left(rsSet("FIRSTNAME"),30) & "</TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD>Surname</TD> " &_
					"		<TD  NOWRAP>:&nbsp; " & Left(rsSet("LASTNAME"),30) & "</TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD>Team</TD> " &_
					"		<TD>:&nbsp;" & rsSet("TEAMNAME") & "</TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD>Work DD</TD> " &_
					"		<TD>:&nbsp; " & rsSet("WORKDD") & "</TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD nowrap>Email&nbsp;</TD> " &_
					"		<TD>:&nbsp; <a href='mailto:" & rsSet("WORKEMAIL") & "'>" & rsSet("WORKEMAIL") & "</a></TD> " &_
					"	</TR> " &_
					"	<TR> " &_
					"		<TD>Mobile</TD> " &_
					"		<TD>:&nbsp; " & rsSet("MOBILE") & "</TD> " &_
					"  </TR> " &_
					"		<TR> " &_
					"			<TD>Work Ext</TD> " &_
					"			<TD>:&nbsp; " & rsSet("WORKEXT") & " </TD> " &_
					"		</TR> " &_
					"		<TR> " &_
					"			<TD>&nbsp;</TD> " &_
					"			<TD>&nbsp;</TD> " &_
					"		</TR> " &_
					"	</TABLE> " 
			rsSet.movenext()
		Wend
		str_data = str_data & 	"<TR><TD>&nbsp;</TD></TR></TABLE>"
		Call CloseRs(rsSet)	
	
	End function
	//<a href="mailto:iagsupport@reidmark.com">DSF</a>
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
<% if cnt = 0 then %>	
	parent.detail_div.innerHTML = Empty.innerHTML
<% else %>	
	parent.detail_div.innerHTML = NotEmpty.innerHTML
<% end if %>
	//parent.href="#L1"
	parent.detail_div.scrollTop = 0	
	}
</script>
<body onload="ReturnData()">
<div id="Empty">
	<TABLE cellspacing=0 cellpadding=0>
		<TR><TD>&nbsp;</TD></TR>
		<TR><TD height=2px></TD></TR>		
		<TR>
		  <TD>Firstname</TD><TD>:</TD></TR>
		<TR>
		  <TD>Surname</TD>
		  <TD>:</TD>
	  </TR>
		<TR><TD>Team</TD><TD>:</TD></TR>
		<TR><TD>Work DD</TD><TD>:</TD></TR>
		<TR>
		  <TD>Email </TD>
		  <TD>:&nbsp;</TD>
		</TR>
		<TR><TD>Mobile</TD><TD>:</TD></TR>
		<TR><TD>Work Ext</TD><TD>:</TD></TR>
		<TR><TD>&nbsp;</TD></TR>		
	</TABLE>
</div>
<div id="NotEmpty">
	<%=str_data%>
</div>
</body>
</html>