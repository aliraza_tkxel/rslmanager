<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
bypasssecurityaccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

ORGID = SESSION("ORGID")
		IF (ORGID = "") THEN
			ORGID = ""
		END IF

SQL = "SELECT  M.JOURNALID AS MESSAGEID,m.IMPORTANCEID ,M.DATECREATED AS DATESENT, E.FIRSTNAME + ' ' + E.LASTNAME AS SENTBY ,M.MESSAGE, M.TITLE FROM dbo.G_MESSAGEJOURNAL M " &_
			" INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = M.CREATEDBY  " &_
			" WHERE " &_
			"(M.CREATEDBY = '" & Session("UserID") & "') " &_ 
			"AND M.ACTIVE = 1 ORDER BY M.DATECREATED DESC, M.TITLE"

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMessages = Server.CreateObject("ADODB.Recordset")
rsMessages.ActiveConnection = RSL_CONNECTION_STRING
rsMessages.Source = SQL
rsMessages.CursorType = 0
rsMessages.CursorLocation = 2
rsMessages.LockType = 3
rsMessages.Open()
rsMessages_numRows = 0


	while not rsMessages.eof 
		datesent = rsMessages("DATESENT")
		FormatedDateSent = Day(datesent) & "/" & Month(datesent) & "/" & Year(datesent)
		Select Case rsMessages("IMPORTANCEID")
			Case "1" 
				 	importance_image = "<img src='/BoardMembers/images/importance.gif' width='4' height='10' title='High Importance'>"
			Case "2" 
				 	importance_image = "<img src='/BoardMembers/images/importance_medium.gif' width='4' height='10' title='Normal Importance'>"
			Case "3" 
			 		importance_image = "<img src='/BoardMembers/images/importance_low.gif' width='4' height='10' title='Low Importance'>"
		End select
		 
		main_mail = 	main_mail & "<tr> " &_
									"    <td align=center>" & importance_image & "</td> " &_
									"    <td align=center><img src='/BoardMembers/images/im_envelopec.gif' width='16' height='11'></td> " &_
									"    <td><input type='checkbox' name='checkbox' value='checkbox'></td> " &_
									"    <td>" & rsMessages("SENTBY") & "</td> " &_
									"    <td>" & rsMessages("Title") &  "</td> " &_
									"    <td title=" & datesent & ">" & FormatedDateSent &  "</td> " &_
									"  </tr> "
														
		rsMessages.Movenext
	Wend

	


	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	scrollbar-3dlight-color: #CCCCCC; 
	scrollbar-arrow-color: #839FAD; 
	scrollbar-base-color: #F7F8F8;
}
.style2 {color: #FFFFFF}
.style3 {font-weight: bold}
.style4 {
	color: #6685A3;
	font-weight: bold;
}
.style6 {
	color: #336633;
	font-weight: bold;
}
.style7 {
	color: #CC6600;
	font-weight: bold;
}
.style8 {
	font-size: 16px;
	font-weight: bold;
	color: #FF0000;
}
.style9 {color: #FF0000}
.style11 {color: #CC6600}
.style12 {color: #0099FF}
-->
</style>
<script language="JavaScript" type="text/JavaScript">

	function SendSentList(){
	
	parent.div_sentlist.innerHTML = div_sentlist.innerHTML;
	
	}

</script>
</head>

<body onLoad="SendSentList()">
<div id="div_sentlist" name="div_sentlist" style="display:block ">
		<table width="559" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
                <table width="557" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td valign="top"><table width="556" border="0" cellpadding="0" cellspacing="0" class="bgColour">

                        <tr>
                          <td><table width="555"  border="0" cellpadding="0" cellspacing="0">
                              <tr>
                                <td width="1" bgcolor="#669900"></td>
                                <td width="553" height="23" align="right" bgcolor="#FFFFFF"><table width="550" height="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="295" align="left"><span class="style4">Sent</span></td>
                                    <td width="11" align="left"><span class="style6">|</span></td>
                                    <td width="28"><span class="style7"><img  style="cursor:hand" onClick="ShowDiv('div_newmessage')" src="/BoardMembers/images/im_envelopec.gif" width="16" height="11"></span></td>
                                    <td width="38"><span class="style6" style="cursor:hand" onClick="ShowDiv('div_newmessage')">New</span></td>
                                    <td width="15" align="left"><span class="style6"> |</span></td>
                                    <td width="28"><span class="style7"><img src="/BoardMembers/images/im_envelopec.gif" width="16" height="11" onClick="GoDelete()" style="cursor:hand"></span></td>
                                    <td width="48"><span class="style6"  onClick="GoDelete()" style="cursor:hand">Delete</span></td>
                                    <td width="16"><span class="style6">|</span></td>
                                    <td width="23"><span class="style7"><img style="cursor:hand" src="/BoardMembers/images/magnifier.gif" width="16" height="16" onClick="ShowAndHideSearch('block')"></span></td>
                                    <td width="48"><span class="style6"  style="cursor:hand" onClick="ShowAndHideSearch('block')">Find </span></td>
                                  </tr>
                                </table></td>
                                <td width="1" bgcolor="#669900"></td>
                              </tr>
                              <tr>
                                <td width="1" height="1"></td>
                                <td width="553" height="1" bgcolor="#669900"></td>
                                <td width="1" height="1"></td>
                              </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="6"></td>
                        </tr>
                    </table></td>
                    </tr>
                </table>
              
    
                <table width="552" border="0" cellspacing="0" cellpadding="0">
                  <tr bgcolor="#E9F0DB">
                    <td width="18"><div align="center"><span class="style8"> <img src="/BoardMembers/images/importance.gif" width="4" height="10"></span></div></td>
                    <td width="27"><div align="center"><img src="/BoardMembers/images/im_envelopec.gif" width="16" height="11"></div></td>
                    <td width="23"><input type="checkbox" name="checkbox" value="checkbox"></td>
                    <td width="151">From</td>
                    <td width="262">Subject</td>
                    <td width="71" bgcolor="#C8D9A4">&nbsp;<img src="/BoardMembers/images/im_down.gif" width="11" height="11"> Date</td>
                    </tr>
                 <%=main_mail%>
                </table></td>
          </tr>
        </table>
		</div>
		
</body>
</html>
