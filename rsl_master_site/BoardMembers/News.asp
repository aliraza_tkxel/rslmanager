<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function

Dim NewsID
NewsID = Request("NewsID")

if NewsID <> "" then Specific_news_ID = " AND NEWSID = " & NewsID & " " End If

Dim upperbound ' used to specify the random images on the news, when no news exists.
upperbound = 4 ' format of images must be news_image[val].gif where [val] = the number, all imgs must be in /myImages/ directory
set rsMyNews = Server.CreateObject("ADODB.Recordset")
rsMyNews.ActiveConnection = RSL_CONNECTION_STRING
rsMyNews.Source = "SELECT top 1 NEWSID, CONTENT, TITLE, NEWSIMAGE FROM G_NEWS WHERE NEWSFOR LIKE '%," & Session("TeamCode") & ",%' " & Specific_news_ID & " ORDER BY DATECREATED DESC, NEWSID DESC"
rsMyNews.CursorType = 0
rsMyNews.CursorLocation = 2
rsMyNews.LockType = 3
rsMyNews.Open()
rsMyNews_numRows = 0

	Counter = 1
	while not rsMyNews.eof 
	
		 if Counter = 1 then
		 	
			 Main_TITLE	= rsMyNews("TITLE")
			 Main_NEWSID 	= rsMyNews("NEWSID")
			 Main_CONTENT 	= rsMyNews("CONTENT")
			 Main_NEWSIMAGE = rsMyNews("NEWSIMAGE")
			 
		 end if
		 

		counter = counter + 1 
		rsMyNews.Movenext
	Wend
	
	'===============================================================================================================
	'	Archive News list
	'===============================================================================================================	
	openDB()
	sql =  "SELECT NEWSID, CONTENT, TITLE, NEWSIMAGE FROM G_NEWS WHERE NEWSFOR LIKE '%," & Session("TeamCode") & ",%'  ORDER BY DATECREATED DESC, NEWSID DESC"
	Call openRs(rsMyNewsArchive,SQL)
	img  = "<img src='/BoardMembers/images/im_rightbutton.gif' width='11' height='11' align='right' valign='top'>"
	while not rsMyNewsArchive.eof 

	     	 if rsMyNewsArchive("NEWSID") = cInt(NewsID) then
			
			 	placeImageHere = img
			 else
			    placeImageHere = ""
			 end if
			 ArchiveRow = 	ArchiveRow &  	 "<tr><td></td><td></td><td height='5'></td><td></td></tr>" &_
											 "<tr>" &_
											 "     <td width='10'></td>" &_
											 "     <td width='3'></td>" &_
											 "     <td width='209' height='16'><a href='news.asp?newsID=" &  rsMyNewsArchive("NEWSID") & "'>" & rsMyNewsArchive("TITLE") & "</a></td>" &_
											 "    <td width='11' >" & placeImageHere & "</td>" &_
											 "     <td width='1' bgcolor='#C9CBC5'></td>" &_
											 "</tr>"&_
											 "<tr>" &_
											 "	   <td width='10' height='1'></td>" &_
											 "	   <td width='3' height='1'></td>" &_
											 "	   <td width='209' height='1' bgcolor='#C9CBC5'></td>" &_
											 "    <td width='11' bgcolor='#C9CBC5'></td>" &_
											 "	   <td width='1' height='1'></td>" &_
											 "</tr>"
						
	rsMyNewsArchive.Movenext
	Wend
	
	closeRs(rsMyNewsArchive)
	CloseDB()
	
	'===============================================================================================================
	'	end Archive News list
	'===============================================================================================================	

	
	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"	
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Untitled Document</title>
<link href="/css/RSL_Green.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
}
.style2 {color: #FFFFFF}
-->
</style>
</head>
<script language="javascript">

function CloseMessagePad(){

NewsBlock.style.display = "block";
MessageBlock.style.display = "none";
}

function OpenMessagePad(){

NewsBlock.style.display = "none";
MessageBlock.style.display = "block";
}

</script>
<body>
<table width="736" border="0" cellpadding="0" cellspacing="0" class="bgColour">
  <tr>
    <td colspan="3">
	<table width="736" border="0" cellspacing="0" cellpadding="0">
      <tr bgcolor="#FFFFFF">
        <td height="4">
		<table width="732" height="59" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="12" colspan="2">
			</td>
            </tr>
          <tr>
            <td width="13" height="27">&nbsp;</td>
            <td width="719" valign="bottom"><table width="724" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><IMG SRC="images/im_rteidmark.gif" WIDTH=88 HEIGHT=22 ALT=""></td>
                <td width="5">&nbsp;</td>
                <td width="183"></td>
                <td width="339" rowspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="191" rowspan="2"><IMG SRC="images/spacer.gif" WIDTH=24 HEIGHT=29 ALT=""><IMG SRC="images/im_rslmanager.gif" WIDTH=163 HEIGHT=29 ALT=""></td>
                <td>&nbsp;</td>
                <td rowspan="2"><table width="98%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="18"></td>
                  </tr>
                  <tr>
                    <td height="1" bgcolor="#C0C0C0">
					</td>
                  </tr>
                  <tr>
                    <td width="22">
					</td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right"><IMG SRC="images/im_strapline.gif" WIDTH=339 HEIGHT=17 ALT=""></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td height="5" colspan="2"></td>
          </tr>
        </table></td>
        <td height="4">&nbsp;</td>
      </tr>
      <tr>
        <td height="4" width="733" bgcolor="#669900">
		</td>
        <td width="3" height="4"><IMG SRC="images/im_barend.gif" WIDTH=3 HEIGHT=4 ALT=""></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3">      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="617"><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="613" height="23" bgcolor="#FFFFFF">                <table width="607" height="32" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="11" height="27">&nbsp;</td>
                    <td width="426" valign="bottom"><IMG SRC="images/im_news.gif" WIDTH=53 HEIGHT=19 ALT=""></td>
                    <td width="170" align="right" valign="middle"><a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backbutton.gif" width="11" height="11" border="0"></a> <a href="/BoardMembers/BM.asp"><img src="/BoardMembers/images/im_backtomywhiteboard.gif" width="121" height="13" border="0"></a></td>
                  </tr>
                  <tr>
                    <td height="5" colspan="3"></td>
                  </tr>
                </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
          </table></td>
          <td width="121">
           <div id="id_card" name="id_card" width="117">
		 		<%=id_card_HTML%>
		   </div>
		  </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td width="234" valign="top"><table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="12"></td>
              <td width="1" bgcolor="#669900"></td>
              <td width="220" height="23" bgcolor="#FFFFFF"><table width="220" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="5">&nbsp;</td>
                    <td><IMG SRC="images/im_archive_title.gif" WIDTH=47 HEIGHT=13 ALT=""></td>
                  </tr>
              </table></td>
              <td width="1" bgcolor="#669900"></td>
            </tr>
            <tr>
              <td width="12" height="1"></td>
              <td width="1" height="1"></td>
              <td width="220" height="1" bgcolor="#669900"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table  border="0" cellspacing="0" cellpadding="0">
     	<%=ArchiveRow%>
            <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="9"></td>
      </tr>
    </table></td>
    <td width="14" valign="top" height="380">&nbsp;</td>
    <td width="488" valign="top"><table width="488" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><div id="NewsBlock" name="NewsBlock" style="display:block "><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td width="366" valign="top"><table width="366" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="364" height="23" bgcolor="#FFFFFF"><table width="285" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_selectedarticle.gif" WIDTH=97 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="6"></td>
                  </tr>
                  <tr>
                    <td height="9"><table width="366" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="8">&nbsp;</td>
                        <td valign="top"> <% 
						if ( main_NewsId <> "") Then 
					tempdata = main_NewsImage & ""
					if (main_NewsImage <> "") Then
						Response.Write "<image src='/News/NewsImages/" & main_NewsImage & "' width='100' height='100' hspace=0 valign=top align=right>"
					Else
						Randomize
						temp = Int((upperbound) * Rnd + 1)
						Response.Write "<image src='/myImages/news_image" & temp & ".gif' width='100px' height='100px' hspace=0 valign=top align=right alt='Default News Image'>"
					End If
				Else 
					Randomize
					temp = Int((upperbound) * Rnd + 1)
		%>
                            <IMG SRC="images/im_picture.gif" WIDTH=190 HEIGHT=144 ALT="" valign="top" align="right">
                            <% 
			End If 
		%>           <%
			if (  main_NewsId <> "") Then
				Response.Write "<b><font style='font-size:14px'>" & Main_title & "</font></b><br><br>"
				Response.Write "<font style='font-size:10px' >" & Main_Content & "</font>"
			Else
		%>
                            <font style='font-size:14px'>Main Headline</font><br>
                            <br>
                            <font style='font-size:10px'> RSL Manager delivers efficiency and performance to financial control, quality, communications... </font>
                            <% 
			End If 
		%>
                        </td>
                        </tr>
                      <%
		rsMyNews.close()
		Set rsMyNews = Nothing
	%>
                    </table></td>
                  </tr>
              </table></td>
              <td width="9">&nbsp;</td>
              <td width="115" valign="top"><table width="115" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="113" height="23" bgcolor="#FFFFFF"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="5">&nbsp;</td>
                                <td><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="113" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="3"></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=116 HEIGHT=4 ALT=""></td>
                  </tr>
                  <tr>
                    <td valign="bottom" bgcolor="#FFFFFF">
					<table width="115" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=144 ALT=""></td>
                          <td valign="top" bgcolor="#FFFFFF">
						  	<!--#include file="includes/MessagePadAlert.asp" -->
						  </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td valign="bottom">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
        </table>
		</div>
		<div id="MessageBlock" name="MessageBlock" style="display:none ">
		<table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
            <tr>
              <td valign="top"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                  <tr>
                    <td><table  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1" bgcolor="#669900"></td>
                          <td width="486" height="23" bgcolor="#FFFFFF"><table width="483" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="6">&nbsp;</td>
                                <td width="417"><IMG SRC="images/im_messagepad.gif" WIDTH=84 HEIGHT=13 ALT=""></td>
                                <td width="60"><div align="right" onClick="CloseMessagePad()"  style="cursor:hand"> <img src="/BoardMembers/images/im_close.gif" width="37" height="11"><img src="/BoardMembers/images/im_closebutton.gif"> </div></td>
                              </tr>
                          </table></td>
                          <td width="1" bgcolor="#669900"></td>
                        </tr>
                        <tr>
                          <td width="1" height="1"></td>
                          <td width="220" height="1" bgcolor="#669900"></td>
                          <td width="1" height="1"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="4"></td>
                  </tr>
                  <tr>
                    <td height="9"><table width="488" border="0" cellpadding="0" cellspacing="0" class="bgColour">
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><IMG SRC="images/im_shadotop.gif" WIDTH=488 HEIGHT=4 ALT=""></td>
                      </tr>
                      <tr>
                        <td valign="bottom" bgcolor="#FFFFFF"><table width="481" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="4"><IMG SRC="images/im_shadoleft.gif" WIDTH=4 HEIGHT=143 ALT=""></td>
                              <td valign="top" bgcolor="#FFFFFF"><!--#include file="includes/MessagePad.asp" -->
                              </td>
                            </tr>
                        </table></td>
                      </tr>
                    
                    </table></td>
                  </tr>
                  <tr>
                    <td height="9">&nbsp;</td>
                  </tr>
              </table></td>
              </tr>
        </table>
		</div>
		</td>
      </tr>
      <tr>
        <td>&nbsp;		</td>
      </tr>
    </table></td>
  </tr>
    <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="17" colspan="3">  <table width="400" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="382" align="right" valign="middle"><span class="style3 style2">&copy;2005 RSLmanager is a Reidmark ebusiness system</span></td>
        <td valign="bottom" align="right" width="18"></td>
      </tr>
    </table></td>
  </tr> 
  <tr align="right" bgcolor="#669900" valign="bottom">
    <td height="3" colspan="3"><img src="images/im_corner-42.gif" width=3 height=3 alt="" ></td>
  </tr> 
</table>
</body>
</html>
