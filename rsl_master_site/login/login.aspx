<%@ Page Language="VB" debug="true" MasterPageFile="MasterPage/login.master" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" title="rslManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label id="lblValidation" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="pnlExpiry" Visible="false"  runat="server">
        <asp:Label ID="lblLoginStatus" runat="server" Text=""></asp:Label> &nbsp;
<asp:LinkButton  ID="lnkUpdate" runat="server">Update My Login</asp:LinkButton>&nbsp;
        <asp:LinkButton ID="lnkLogin" Visible="false" runat="server">Log Straight in </asp:LinkButton>     
    </asp:Panel>
    <asp:Panel ID="pnlLogin" runat="server" Visible="true"  >

    <br />  
    <asp:Label ID="lblUserName" cssclass="label"  runat="server" Text="User Name" AssociatedControlID="txtUserName"></asp:Label>
    <asp:TextBox ID="txtUserName" cssclass="textbox"  runat="server"></asp:TextBox>
    <asp:Label ID="lblPassword" cssclass="label"  runat="server" AssociatedControlID="txtPassword" Text="Password"></asp:Label>
    <asp:TextBox ID="txtPassword" TextMode="Password" cssclass="textbox"  runat="server"></asp:TextBox>  
    <asp:Button ID="btnLogin" runat="server" Text="Login"  CssClass="RSLButton" />    
    
    </asp:Panel>
    
   <asp:Panel ID="pnlUpdateLogin" runat="server" Visible="False"  >
       <asp:Label ID="lblUpdateUserName" cssclass="updatelabel" runat="server" AssociatedControlID="txtUpdateUserName"
           Text="Username"></asp:Label>
       <asp:TextBox cssclass="textbox" ID="txtUpdateUserName" runat="server"></asp:TextBox><br /><br />
       <asp:Label cssclass="updatelabel"  ID="lblUpdatePassword" runat="server" AssociatedControlID="txtUpdatePassword"
           Text="Password"></asp:Label>
       <asp:TextBox cssclass="textbox" TextMode="Password"   ID="txtUpdatePassword" runat="server"></asp:TextBox><br /><br />
       <asp:Label cssclass="updatelabel"  ID="lblUpdateNewPassword" runat="server" AssociatedControlID="txtUpdateNewPassword"
           Text="New Password"></asp:Label>
       <asp:TextBox cssclass="textbox" TextMode="Password"   ID="txtUpdateNewPassword" runat="server"></asp:TextBox><br /><br />
       <asp:Button ID="btnUpdatePassword" runat="server" Text="Update Password" CssClass="UpdateRSLButton" /></asp:Panel>

        <br />

	<br><div><script src=https://seal.verisign.com/getseal?host_name=crm.broadlandhousinggroup.org&size=M&use_flash=NO&use_transparent=NO&lang=en></script> </div>
    
	<br />
        <asp:Panel ID="pnlforgot" runat="server" Height="50px" Width="547px">
        <asp:LinkButton ID="lnkmisplaced" runat="server" PostBackUrl="/misplaced.asp">Forgot your password</asp:LinkButton>
        <br />

	<br />
        Do you need a login -
        <a href="mailto:david.hender@broadlandhousing.org">Click here</a>
        - to request a login from our RSL Manager team
        </asp:Panel>

</asp:Content>

