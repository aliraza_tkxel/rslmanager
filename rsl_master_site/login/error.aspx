<%@ Page Language="VB" AutoEventWireup="false" Debug="false" CodeFile="error.aspx.vb" Inherits="_Default" Trace="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>rslManager</title>
    <link rel="stylesheet" type="text/css" href="CSS/Stylesheet.css" />
     <link rel="stylesheet" type="text/css"  href="CSS/nostyle.css"/>
<script language="JavaScript" type="text/javascript">
<!--
var doAlerts=true;
function changeSheets(whichSheet){
  whichSheet=whichSheet-1;
  if(document.styleSheets){
    var c = document.styleSheets.length;
    //if (doAlerts) alert('Change to Style '+(whichSheet+1));
    for(var i=0;i<c;i++){
      if(i!=whichSheet){
        document.styleSheets[i].disabled=true;
      }else{
        document.styleSheets[i].disabled=false;
      }
    }
  }
}
//-->
</script>
</head>
<body>


    <div id="header">
    
    </div>
    <div id="body">

    <div id="bodyinner">
        <div id="tab"></div>
        <div id="tabtitle">Log on</div>


    <form id="form1" runat="server">
    <div id="formgroup" style="width: 664px">
        rslManager has reported an error. Please quote the following reference number when
        reporting the error. Error number
        <asp:Label ID="lblStatus" runat="server"></asp:Label></div> </form></div>
    </div>
    <div id="footer">
    </div>
</body>
</html>
