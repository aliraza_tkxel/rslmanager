Imports System.Data.SqlClient
Imports System.Data
Imports bllLogin
Imports System.Drawing.Color
Partial Class login
    'Inherits System.Web.UI.Page
    Inherits basepage
    Public Enum NavMode
        LoadPage = 1
        AlmostExpired = 2
        Expired = 3
        UpdatePassword = 4
    End Enum

    Public Sub uiController(ByVal controllerid As Integer)
        If controllerid = NavMode.LoadPage Then
            pnlLogin.Visible = True
            pnlExpiry.Visible = False
            Master.Page.Title = "rslManager"
        End If

        If controllerid = NavMode.AlmostExpired Then
            pnlExpiry.Visible = True
            pnlLogin.Visible = False
            pnlforgot.Visible = False
            lnkLogin.Visible = True
        End If

        If controllerid = NavMode.Expired Then
            pnlExpiry.Visible = True
            lnkLogin.Visible = False
            pnlforgot.Visible = False
            pnlLogin.Visible = False
        End If

        If controllerid = NavMode.UpdatePassword Then
            pnlExpiry.Visible = False
            pnlLogin.Visible = False
            pnlforgot.Visible = False
            pnlUpdateLogin.Visible = True
        End If

    End Sub



    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Try

            Dim bllLogin As New bllLogin
            bllLogin.UserName = txtUserName.Text
            bllLogin.Password = txtPassword.Text

            If bllLogin.Login = True Then
                lblValidation.Text = ""

                If bllLogin.LoginExpiryStatus = ExpiryType.Expired Then
                    lblLoginStatus.Text = "Your password has expired you have the following options: "
                    uiController(NavMode.Expired)
                    Session("bllLogin") = bllLogin
                ElseIf bllLogin.LoginExpiryStatus = ExpiryType.AlmostExpired Then
                    lblLoginStatus.Text = "Your password has almost expired you have the following options: "
                    uiController(NavMode.AlmostExpired)
                    Session("bllLogin") = bllLogin
                Else
                    bllLogin.SetSession(ASPSession)
                End If

            Else
                lblValidation.ForeColor = Red
                lblValidation.Text = "Your username and password do not appear to be correct"

            End If
        Catch ex As Exception
            	lblValidation.Text = ex.Message
		
        End Try

    End Sub


    Protected Sub lnkUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUpdate.Click
        uiController(NavMode.UpdatePassword)
        Dim bllLogin As New bllLogin
        bllLogin = CType(Session("bllLogin"), bllLogin)
        txtUpdateUserName.Text = bllLogin.UserName
        Session("bllLogin") = bllLogin
    End Sub

    Protected Sub btnUpdatePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePassword.Click
        lblValidation.Text = ""
        Try
            Dim bllLogin As New bllLogin
            bllLogin = CType(Session("bllLogin"), bllLogin)

            bllLogin.UserName = txtUpdateUserName.Text
            bllLogin.Password = txtUpdatePassword.Text
            bllLogin.NewPassword = txtUpdateNewPassword.Text

            Dim intUpdateStatus As Integer = bllLogin.UpdatePassword
            If intUpdateStatus = 0 Then
                lblValidation.ForeColor = Red
                lblValidation.Text = "The username and password do not seem to exist"
            Else
                bllLogin.SetSession(ASPSession)
            End If

        Catch ex As Exception
            lblValidation.ForeColor = Red
            lblValidation.Text = ex.Message
        End Try
    End Sub

    Protected Sub lnkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogin.Click
        Dim bllLogin As New bllLogin
        bllLogin = CType(Session("bllLogin"), bllLogin)
        bllLogin.SetSession(ASPSession)
    End Sub
End Class
