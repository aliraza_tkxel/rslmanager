Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class datahelper
    Shared Function getconn(ByVal strconn As String) As SqlConnection
        Dim conn As New SqlConnection(strconn)
        getconn = conn
    End Function

    Shared Function getStrConn(ByVal strconn As String) As String
        Dim settings As New ConnectionStringSettings
        settings = ConfigurationManager.ConnectionStrings(strconn)
        strconn = settings.ConnectionString.ToString
        Return strconn
    End Function
End Class
