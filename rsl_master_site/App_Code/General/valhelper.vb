Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.IO
Imports System.Guid
Imports System.Globalization

Public Class valhelper

    Shared Function Display(ByVal colError As Collection(Of String)) As String
        Dim strError As String = ""
        strError = strError & "<ul>"
        For Each item As String In colError
            strError = strError & "<li>" & item & "</li>"
        Next item
        strError = strError & "</ul>"
        Return strError
    End Function

    Shared Function validateEmail(ByVal strEmail As String) As Boolean
        If String.IsNullOrEmpty(strEmail) Then
            Return False
        Else
            Dim re As New Regex("(?<=(#|@))(?=\w+)\w+\b")
            If re.IsMatch(strEmail) Then
                Return True
            Else
                Return False
            End If
        End If

    End Function

    Shared Function validatePostcode(ByVal strPostcode As String) As Boolean
        Dim rePostcode As New Regex("^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$")
        strPostcode = Replace(strPostcode.ToUpper, " ", "")
        If rePostcode.IsMatch(strPostcode) Then
            Return True
        Else
            Return False
        End If


    End Function

    Shared Function validateDate(ByVal strDate As String) As Boolean

        Dim d As Date
        If Date.TryParse(strDate, d) Then
            Return True
        Else
            Return False
        End If

    End Function

    Shared Function validateValue(ByVal strData As String) As Boolean
        Dim iVal As Integer

        If String.IsNullOrEmpty(strData) Then Return False

        If Integer.TryParse(strData, iVal) Then
            If iVal = 0 Then
                Return False
            End If            

            Return True
        Else
            Return False
        End If
    End Function

    Shared Function validateCurrency(ByVal strData As String) As Boolean
        Dim dVal As Decimal
        Dim style As System.Globalization.NumberStyles
        Dim culture As System.Globalization.CultureInfo

        style = NumberStyles.AllowCurrencySymbol Or NumberStyles.AllowDecimalPoint Or NumberStyles.AllowThousands
        culture = CultureInfo.CreateSpecificCulture("en-GB")

        If Decimal.TryParse(strData, style, culture, dVal) Then            
            If dVal <= 999999 Then
                If dVal = 0 Then
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

        Return True
    End Function

    Shared Function convertToDate(ByVal strDate As String) As Nullable(Of Date)

        Dim d As Date
        If Date.TryParse(strDate, d) Then

        End If
        Return d

    End Function

    Shared Function validateLength(ByVal strData As String, ByVal intLength As Integer) As Boolean
        If Len(strData) > intLength Then
            Return False
        Else
            Return True
        End If


    End Function
    Shared Function validateIsEmpty(ByVal strData As String) As Boolean

        If String.IsNullOrEmpty(strData) Then
            Return True
        Else
            Return False
        End If


    End Function
    Shared Function validateLengthAndIsEmpty(ByVal strData As String, ByVal intLength As Integer) As Boolean
        If String.IsNullOrEmpty(strData) Then
            Return False
        Else
            If Len(strData) > intLength Then
                Return False
            Else
                Return True
            End If
        End If


    End Function


    Shared Function CheckRef(ByVal strData As String) As Nullable(Of Integer)
        strData = Regex.Replace(strData, "[a-z]", String.Empty)
        strData = Regex.Replace(strData, "[A_Z]", String.Empty)
        If IsNumeric(strData) Then
            Return CInt(strData)
        End If
    End Function
    Shared Function CheckPassword(ByVal strData As String, ByVal strLength As Integer) As Boolean
        Dim input As String = strData
        If input.Length < strLength Then
            Return False
        End If

        Dim chArray As Char()
        chArray = input.ToCharArray()
        Dim blnUcase As Boolean = False
        Dim blnLCase As Boolean = False
        Dim blnIsNum As Boolean = False
        Dim i As Integer
        For i = 0 To chArray.GetLength(0) - 1
            If Char.IsDigit(chArray(i)) Then
                blnIsNum = True
            End If

            If Char.IsLower(chArray(i)) Then
                blnLCase = True
            End If
            If Char.IsUpper(chArray(i)) Then
                blnUcase = True
            End If
        Next
        If blnUcase = True And blnLCase = True And blnIsNum = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Shared Function CheckSpaces(ByVal strData As String) As Boolean
        Dim chArray As Char()
        Dim input As String = strData
        chArray = input.ToCharArray()
        Dim blnHasSpaces As Boolean = False

        Dim i As Integer
        For i = 0 To chArray.GetLength(0) - 1

            If Char.IsWhiteSpace(chArray(i)) Then
                blnHasSpaces = True
            End If
        Next
        If blnHasSpaces = True Then
            Return True
        Else
            Return False
        End If
    End Function


End Class
