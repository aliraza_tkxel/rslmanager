Imports Microsoft.VisualBasic

Public Class sessionhelper

    Private Const s_uiControllerID As String = "uiControllerID"
    Private Const s_uiParentID As String = "ParentID"
    Private Const s_uiCurrentID As String = "uiCurrentID"
    Private Const s_uiDepth As String = "uiDepth"
    Private Const s_NewsID As String = "NewsID"
    Private Const s_SearchType As String = "SearchType"
    Private Const s_ViewApplicationID As String = "ViewApplicationID"


    Public Shared Property NewsID() As Integer
        Get
            Return HttpContext.Current.Session(s_NewsID)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_NewsID) = value
        End Set
    End Property




    Public Shared Property SearchType() As Integer
        Get
            Return HttpContext.Current.Session(s_SearchType)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_SearchType) = value
        End Set
    End Property
    Public Shared Property uiControllerID() As Integer
        Get
            Return HttpContext.Current.Session(s_uiControllerID)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_uiControllerID) = value
        End Set
    End Property

    Public Shared Property ParentID() As Integer
        Get
            Return HttpContext.Current.Session(s_uiParentID)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_uiParentID) = value
        End Set
    End Property
    Public Shared Property Depth() As Integer
        Get
            Return HttpContext.Current.Session(s_uiDepth)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_uiDepth) = value
        End Set
    End Property
    Public Shared Property CurrentID() As Integer
        Get
            Return HttpContext.Current.Session(s_uiCurrentID)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_uiCurrentID) = value
        End Set
    End Property
    Public Shared Property ViewApplicationID() As Integer
        Get
            Return HttpContext.Current.Session(s_ViewApplicationID)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session(s_ViewApplicationID) = value
        End Set
    End Property
End Class
