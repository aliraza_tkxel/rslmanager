Imports Microsoft.VisualBasic
Imports System.IO.Path
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.Guid

Public Class filehelper


    Shared Sub DeleteIfExists(ByVal imgUpload As FileUpload, ByVal strCurrentFile As String, ByVal strFullPath As String)
        Dim strDeletePath As String
        If imgUpload.HasFile Then
            If Not String.IsNullOrEmpty(strCurrentFile) Then
                strDeletePath = HttpContext.Current.Server.MapPath(strFullPath & strCurrentFile)
                If File.Exists(strDeletePath) Then
                    File.Delete(strDeletePath)
                End If
            End If
        End If
    End Sub

    Shared Function UploadImageFile(ByVal imgUpload As FileUpload, ByVal imgPath As String, ByVal intAspectRatio As Integer) As String
        Dim strNewFileName As String = ""
        Dim strEx As String

        strEx = GetExtension(imgUpload.FileName)
        strEx = strEx.ToLower

        Dim strTmpSaveAs As String = HttpContext.Current.Server.MapPath(imgPath & "tmp_" & NewGuid.ToString & strEx)
        strNewFileName = imgUpload.FileName
        Dim strSaveAs As String = HttpContext.Current.Server.MapPath(imgPath & strNewFileName)
        Dim strFileWithoutEx As String = GetFileNameWithoutExtension(imgUpload.FileName)

        Dim i As Integer

        imgUpload.SaveAs(strTmpSaveAs)
        imgUpload.Dispose()

        Dim OriginalBM As New Bitmap(strTmpSaveAs)

        Dim intCurrentWidth = OriginalBM.Width
        Dim intCurrentHeight = OriginalBM.Height


        Dim tempMultiplier As Double

        If intCurrentHeight > intCurrentWidth Then ' portrait  
            tempMultiplier = intAspectRatio / intCurrentHeight
        Else
            tempMultiplier = intAspectRatio / intCurrentWidth
        End If

        Dim NewSize As New Size(CInt(intCurrentWidth * tempMultiplier), CInt(intCurrentHeight * tempMultiplier))

        i = 1

        strSaveAs = HttpContext.Current.Server.MapPath(imgPath & strNewFileName)


        Dim tmpstr As String = ""



        Dim blnFile As Boolean = False
        If File.Exists(strSaveAs) Then
            blnFile = File.Exists(strSaveAs)

            Do Until blnFile = False
                strNewFileName = strFileWithoutEx & "_" & i & strEx
                strSaveAs = HttpContext.Current.Server.MapPath(imgPath & strNewFileName)
                blnFile = File.Exists(strSaveAs)

                i = i + 1
            Loop
        End If

        Dim ResizedBM As New Bitmap(OriginalBM, NewSize)

        If strEx = ".jpeg" Or strEx = ".jpg" Then
            ResizedBM.Save(strSaveAs, ImageFormat.Jpeg)
        Else
            ResizedBM.Save(strSaveAs, ImageFormat.Gif)
        End If

        ResizedBM.Dispose()
        OriginalBM.Dispose()
        File.Delete(strTmpSaveAs)

        Return strNewFileName

    End Function


    Shared Function UploadFile(ByVal filUp As FileUpload, ByVal imgPath As String) As String
        Dim strNewFileName As String = ""
        Dim strEx As String

        strEx = GetExtension(filUp.FileName)
        strEx = strEx.ToLower
        strNewFileName = filUp.FileName

        Dim strSaveAs As String = HttpContext.Current.Server.MapPath(imgPath & strNewFileName)
        Dim strFileWithoutEx As String = GetFileNameWithoutExtension(filUp.FileName)

        Dim i As Integer

        i = 1

        If File.Exists(strSaveAs) Then

            Do Until File.Exists(strSaveAs) = False

                strNewFileName = strFileWithoutEx & "_" & i & strEx
                strSaveAs = HttpContext.Current.Server.MapPath(imgPath & strNewFileName)
                i = i + 1
            Loop
        End If

        filUp.SaveAs(strSaveAs)
        filUp.Dispose()
        Return strNewFileName
    End Function


    Shared Function GetFileExt(ByVal sFileName As String) As String
        Dim P As Integer

        For P = Len(sFileName) To 1 Step -1
            'Find the last ocurrence of "." in the string
            If InStr(".", Mid(sFileName, P, 1)) Then Exit For
        Next

        GetFileExt = Right(sFileName, Len(sFileName) - P)

    End Function

    Shared Function GetDocImage(ByVal strFileName As String) As String
        Dim strPath As String = "/bhaintranet/images/defaults/"
        Dim strFile As String = ""
        Dim strExtension As String = GetFileExt(strFileName)


        Select Case strExtension
            Case "doc"
                strFile = "doc.gif"
            Case "docx"
                strFile = "doc.gif"
            Case "pdf"
                strFile = "pdf.gif"
            Case "txt"
                strFile = "txt.gif"
            Case "xls"
                strFile = "xls.gif"
            Case "xlsx"
                strFile = "xls.gif"
            Case "ppt"
                strFile = "ppt.gif"
            Case "pptx"
                strFile = "ppt.gif"
            Case "csv"
                strFile = "xls.gif"
            Case Else
                strFile = "page.gif"
        End Select

        Return strPath & "/" & strFile
    End Function
 

End Class
