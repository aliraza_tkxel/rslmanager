Imports Microsoft.VisualBasic
Imports FredCK.FCKeditorV2
Imports System.IO
Imports System.Web.HttpContext

Public Class formhelper
    Shared Sub ClearForm(ByVal ctlParent As Control)
        Dim ctl As Control
        Dim txt As TextBox
        Dim chk As CheckBox
        Dim drp As DropDownList
        Dim fck As FCKeditor
        Dim itmPleaseSelect As ListItem
        For Each ctl In ctlParent.Controls

            If TypeOf ctl Is FCKeditor Then
                fck = CType(ctl, FCKeditor)
                fck.Value = ""
            End If

            If TypeOf ctl Is TextBox Then
                txt = CType(ctl, TextBox)
                txt.Text = ""
            End If
            If TypeOf ctl Is CheckBox Then
                chk = CType(ctl, CheckBox)
                chk.Checked = False
            End If
            If TypeOf ctl Is DropDownList Then

                drp = CType(ctl, DropDownList)
                Dim blnHasPleaseSelect As Boolean = False
                For i As Integer = drp.Items.Count - 1 To 0 Step -1

                    Dim item As ListItem = drp.Items(i)
                    If item.Value = "" Then
                        blnHasPleaseSelect = True
                    End If

                Next

                If blnHasPleaseSelect = False Then
                    itmPleaseSelect = New ListItem("Please Select", "")
                    drp.Items.Insert(0, itmPleaseSelect)
                End If
                drp.SelectedIndex = 0
            End If

        Next
    End Sub

    Shared Sub LockForm(ByVal ctlParent As Control, ByVal bLock As Boolean)
        Dim ctl As Control
        Dim txt As TextBox
        Dim chk As CheckBox
        Dim drp As DropDownList

        For Each ctl In ctlParent.Controls
            If TypeOf ctl Is TextBox Then
                txt = CType(ctl, TextBox)
                txt.ReadOnly = bLock
            End If

            If TypeOf ctl Is CheckBox Then
                chk = CType(ctl, CheckBox)
                chk.Enabled = Not bLock
            End If

            If TypeOf ctl Is DropDownList Then

                drp = CType(ctl, DropDownList)
                drp.Enabled = Not bLock
            End If

        Next
    End Sub


    Shared Sub RemovePleaseSelect(ByVal ctlParent As Control)
        Dim ctl As Control
        Dim drp As DropDownList

        For Each ctl In ctlParent.Controls
            If TypeOf ctl Is DropDownList Then

                drp = CType(ctl, DropDownList)

                For i As Integer = drp.Items.Count - 1 To 0 Step -1

                    Dim item As ListItem = drp.Items(i)
                    If item.Value = "" Then
                        drp.Items.Remove(item)
                    End If

                Next
            End If

        Next
    End Sub

    Shared Function ProcessSelect(ByVal drpValue As String) As Nullable(Of Integer)
        Dim i As Nullable(Of Integer)

        If String.IsNullOrEmpty(drpValue) Then
            Return i
        Else
            Return CInt(drpValue)

        End If


    End Function
    Shared Sub ProcessCheckboxList(ByVal chklst As CheckBoxList, ByVal blnSelected As Boolean)

        For i As Integer = 0 To chklst.Items.Count - 1

            chklst.Items(i).Selected = blnSelected


        Next
    End Sub

    Shared Function TruncateText(ByVal sText As String, ByVal intTruncate As Integer) As String

        sText = sText.Replace("<br />", " #~BR~# ")
        sText = sText.Replace("</p>", " #~/P~# ")
        Dim stripped As String = Regex.Replace(sText, "<(.|\n)*?>", String.Empty)


        stripped = stripped.Replace(" #~/P~# ", "<br/>")
        stripped = stripped.Replace(" #~BR~# ", "<br/>")
        stripped = Left(stripped, intTruncate)
        Dim intLastSpace = stripped.LastIndexOf(" ")


        If (intLastSpace < Len(stripped) And intLastSpace > 0) Then
            stripped = Left(stripped, intLastSpace)
        End If
        Return "" & stripped & "....."

    End Function

    Shared Function GetFileSize(ByVal strUrl As String)
        Dim finfo As FileInfo

        Try
            finfo = New FileInfo(Current.Server.MapPath(strUrl))
            Dim lFileInBytes As Long = finfo.Length
            Dim lFileInKB As Long = finfo.Length / 1024

            GetFileSize = lFileInKB.ToString + " kb"
        Catch
            GetFileSize = "-"
        End Try
    End Function

End Class
