Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail


Public Class ErrorCollection
    Inherits Collection(Of String)

    Protected Overrides Sub InsertItem( _
        ByVal index As Integer, ByVal newItem As String)
        MyBase.InsertItem(index, newItem)
    End Sub

    Protected Overrides Sub SetItem(ByVal index As Integer, _
        ByVal newItem As String)

        Dim replaced As String = Items(index)
        MyBase.SetItem(index, newItem)

    End Sub

    Protected Overrides Sub RemoveItem(ByVal index As Integer)

        Dim removedItem As String = Items(index)
        MyBase.RemoveItem(index)

    End Sub

    Protected Overrides Sub ClearItems()
        MyBase.ClearItems()
    End Sub

End Class

Public Class ErrorHelper

    Shared Sub LogError(ByVal objErrorInfo As Exception, ByVal blnRedirect As Boolean)

        Dim strConn As String
        Dim settings As New ConnectionStringSettings
        settings = ConfigurationManager.ConnectionStrings("connRSL")

        strConn = settings.ConnectionString.ToString()
        Dim conn As New SqlConnection(strConn)
        conn.Open()
        Dim cmd As New SqlCommand("SP_NET_INSERTERROR", conn)
        cmd.CommandType = Data.CommandType.StoredProcedure

        Dim param As SqlParameter
        param = New SqlParameter("@ASPDescription", SqlDbType.VarChar, 2000)
        param.Value = objErrorInfo.Message.ToString
        cmd.Parameters.Add(param)

        param = New SqlParameter("@ASPCode", SqlDbType.VarChar, 100)
        param.Value = objErrorInfo.GetHashCode.ToString
        cmd.Parameters.Add(param)

        param = New SqlParameter("@ASPCategory", SqlDbType.VarChar, 100)
        param.Value = objErrorInfo.ToString
        cmd.Parameters.Add(param)


        param = New SqlParameter("@Filename", SqlDbType.VarChar, 100)
        param.Value = HttpContext.Current.Request.Url.ToString
        cmd.Parameters.Add(param)

        param = New SqlParameter("@Source", SqlDbType.VarChar, 2000)
        param.Value = objErrorInfo.ToString
        cmd.Parameters.Add(param)

        Dim strformcollection As String
        strformcollection = ""

        Dim loop1, loop2 As Integer
        Dim arr1(), arr2() As String

        Dim i As Integer
        Dim itemName As String
        Dim itemValue As String

        For i = 0 To HttpContext.Current.Request.Form.Count - 1
            itemName = HttpContext.Current.Request.Form.AllKeys(i)
            itemValue = HttpContext.Current.Request.Form.GetValues(i)(0)
            strformcollection += "<BR>" & itemName & " : " & itemValue
        Next


        param = New SqlParameter("@FORM", SqlDbType.VarChar, 1000)
        param.Value = strformcollection
        cmd.Parameters.Add(param)

        Dim strquerycollection As String
        strquerycollection = HttpContext.Current.Request.QueryString.ToString


        Dim qName As String
        Dim qValue As String

        For i = 0 To HttpContext.Current.Request.QueryString.Count - 1
            qName = HttpContext.Current.Request.QueryString.AllKeys(i)
            qValue = HttpContext.Current.Request.QueryString.GetValues(i)(0)
            strquerycollection += "<BR>" & qName & " : " & qValue
        Next


        param = New SqlParameter("@QUERYSTRING", SqlDbType.VarChar, 1000)
        param.Value = strquerycollection
        cmd.Parameters.Add(param)

        Dim strsessioncollection As String
        strsessioncollection = ""

        param = New SqlParameter("@SESSION", SqlDbType.VarChar, 1000)
        param.Value = strsessioncollection
        cmd.Parameters.Add(param)

        Dim strcookiescollection As String
        strcookiescollection = ""

        Dim MyCookieColl As HttpCookieCollection
        Dim MyCookie As HttpCookie

        MyCookieColl = HttpContext.Current.Request.Cookies
        ' Capture all cookie names into a string array.
        arr1 = MyCookieColl.AllKeys
        ' Grab individual cookie objects by cookie name     
        For loop1 = 0 To arr1.GetUpperBound(0)
            MyCookie = MyCookieColl(arr1(loop1))
            strcookiescollection += "Cookie: " & MyCookie.Name & "<br>"
            strcookiescollection += "Expires: " & MyCookie.Expires & "<br>"
            strcookiescollection += "Secure:" & MyCookie.Secure & "<br>"

            ' Grab all values for single cookie into an object array.
            arr2 = MyCookie.Values.AllKeys
            ' Loop through cookie value collection and print all values.
            For loop2 = 0 To arr2.GetUpperBound(0)
                strcookiescollection += "Value " & CStr(loop2) + ": " & HttpContext.Current.Server.HtmlEncode(arr2(loop2)) & "<br>"
            Next loop2
        Next loop1

        param = New SqlParameter("@COOKIES", SqlDbType.VarChar, 1000)
        param.Value = strcookiescollection
        cmd.Parameters.Add(param)


        Dim errorid_param As SqlParameter

        errorid_param = New SqlParameter("@ErrorID", SqlDbType.Int)
        errorid_param.Direction = ParameterDirection.Output
        cmd.Parameters.Add(errorid_param)

        Dim intreturnvalue As Integer
        Dim strerror As String
        strerror = ""
        strerror += "Error:<br> " & objErrorInfo.Message.ToString & "<br>"
        strerror += "Error Details:<br> " & objErrorInfo.ToString & "<br>"
        strerror += "Form Collection:<br> " & strformcollection & "<br><br>"
        strerror += "Querystring Collection:<br> " & strquerycollection & "<br><br>"
        strerror += "Cookie Collection:<br> " & strcookiescollection & "<br><br>"
        strerror += "Session Collection:<br> " & strsessioncollection & "<br><br>"

        HttpContext.Current.Response.Write(strerror)
        'HttpContext.Current.Response.End()


        Dim reader As SqlDataReader = cmd.ExecuteReader()


        intreturnvalue = errorid_param.Value


        Dim smtpClient As SmtpClient = New SmtpClient
        Dim message As MailMessage = New MailMessage
        Dim strMailText As String = "There was an error on the asp.net part of Broadland. The number was " & intreturnvalue.ToString

        Dim fromAddress As MailAddress = New MailAddress("jon.swain@reidmark.com", "Broadland Error")
        smtpClient.Host = "localhost"
        message.From = fromAddress
        message.To.Add("jon.swain@reidmark.com")
        message.Subject = "Broadland Error No " & intreturnvalue

        message.IsBodyHtml = True
        message.Body = strerror
        smtpClient.Send(message)
        If blnRedirect = True Then
            HttpContext.Current.Response.Redirect("/error.aspx?errorid=" & intreturnvalue.ToString)
        End If

    End Sub

End Class

