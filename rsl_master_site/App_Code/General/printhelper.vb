Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports System.Text
Imports System.Web.SessionState

Public Class PrintHelper
    Public Sub New()
    End Sub

    Public Shared Function GetPrintScript(ByVal strPrintPath As String) As String
        Dim strJavaScript As New StringBuilder

        strJavaScript.Append("window.open('" + strPrintPath + "'")
        strJavaScript.Append(",target='new', 'width=720,height=625,resizable=no,top=50,left=50,toolbar=yes,scrollbars=yes');")

        Return strJavaScript.ToString
    End Function

End Class
