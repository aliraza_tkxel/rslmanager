Imports Microsoft.VisualBasic
Namespace CustomControls

    Public Class CheckBoxWithValue
        Inherits WebControls.CheckBox
        Private _value As String
        Public Property Value() As String
            Get
                Return _value
            End Get
            Set(ByVal value As String)
                _value = value
            End Set
        End Property


        Protected Overrides Sub LoadViewState(ByVal state As Object)
            'Take Pair back 

            Dim arrState() As Object = CType(state, Object)
            Me.Value = CInt(arrState(1))

            'Pass base state to base class

            MyBase.LoadViewState(arrState(0))


        End Sub

        Protected Overrides Function SaveViewState() As Object
            Dim arrState(1) As Object
            arrState(0) = MyBase.SaveViewState()
            arrState(1) = Me.Value

            Return arrState
        End Function


    End Class


    Public Class MenuNode
        Inherits WebControls.TreeNode
        Private _NodeType As Integer
        Private _RootNodeType As Integer
        Private _NodeAction As Integer

        Public Property NodeType() As Integer
            Get
                Return _NodeType
            End Get
            Set(ByVal value As Integer)
                _NodeType = value
            End Set
        End Property

        Public Property RootNodeType() As Integer
            Get
                Return _RootNodeType
            End Get
            Set(ByVal value As Integer)
                _RootNodeType = value
            End Set
        End Property

        Public Property NodeAction() As Integer
            Get
                Return _NodeAction
            End Get
            Set(ByVal value As Integer)
                _NodeAction = value
            End Set
        End Property

        Private _myCustomStateString As String = ""

        Protected Overrides Sub LoadViewState(ByVal state As Object)
            'Take Pair back
            Dim arrState() As Object = CType(state, Object)
            Me.NodeType = CInt(arrState(1))
            Me.RootNodeType = CInt(arrState(2))
            Me.NodeAction = CInt(arrState(3))
            'Pass base state to base class
            'HttpContext.Current.Trace.Write(arrState(0).ToString & "test")
            MyBase.LoadViewState(arrState(0))
            'HttpContext.Current.Response.Write("<br>load-" & CInt(arrState(1)))

        End Sub

        Protected Overrides Function SaveViewState() As Object
            Dim arrState(3) As Object
            arrState(0) = MyBase.SaveViewState()
            arrState(1) = Me.NodeType
            arrState(2) = Me.RootNodeType
            arrState(3) = Me.NodeAction
            'HttpContext.Current.Response.Write("<br>save-" & CInt(arrState(1)))
            Return arrState
        End Function


    End Class


    Public Class MenuTreeview
        Inherits TreeView
        Protected Overrides Function CreateNode() As System.Web.UI.WebControls.TreeNode
            Return New MenuNode
        End Function
    End Class

End Namespace