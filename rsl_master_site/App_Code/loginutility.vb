Imports Microsoft.VisualBasic
Imports System.Data
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary



Public Class LoginUtility

    Public Function serialize_object(ByVal obj As Object) As String
        Dim ms As New MemoryStream
        Dim MyFormatter As New BinaryFormatter()
        MyFormatter.Serialize(ms, obj)

        Dim bytearray As Byte() = ms.ToArray()
        Dim str As String = Convert.ToBase64String(bytearray)
        Return str
    End Function

    Public Function deserialize_object(ByVal str As String) As Object
        Dim objFormatter As New BinaryFormatter
        Dim genericobject As New Object
        Dim arrBytData() As Byte = Convert.FromBase64String(str.ToString)
        Dim ms As MemoryStream = New MemoryStream(arrBytData)

        ms.Write(arrBytData, 0, arrBytData.Length)
        ms.Seek(0, SeekOrigin.Begin)
        genericobject = objFormatter.Deserialize(ms)
        Return genericobject


    End Function

End Class
