Imports Microsoft.VisualBasic
Imports dalPage
Imports dalPageTableAdapters
Imports dalMenuPage
Imports dalMenuPageTableAdapters
Imports System.IO
Imports System.Data
Imports dalPageTeam
Imports dalPageTeamTableAdapters
Imports dalPageEmployee
Imports dalPageEmployeeTableAdapters
Public Class bllPage

    Private m_PageID As Integer
    Private m_MenuID As Integer
    Private m_PageContent As String
    Private m_PageName As String
    Private m_PageTitle As String
    Private m_FileName As String
    Private m_Active As Boolean
    Private m_IsBespoke As Boolean
    Private m_MultiMenu As String
    Private m_BespokePath As String = HttpContext.Current.GetSection("web_manager")("bespoke_path")
    Private m_Path As String = HttpContext.Current.GetSection("web_manager")("template_driven_path")
    Private m_FooterID As Integer = CInt(HttpContext.Current.GetSection("web_manager")("footer_menuid"))
    Private m_SelectedTeams() As String
    Private m_SelectedEmps() As String
    Private m_IsDB As Boolean
    Private m_HasAccess As Boolean
    Private m_IsDefault As Boolean

    Public Property PageID() As Integer
        Get
            Return m_PageID
        End Get
        Set(ByVal value As Integer)
            m_PageID = value
        End Set
    End Property

    Public Property HasAccess() As Boolean
        Get
            Return m_HasAccess
        End Get
        Set(ByVal value As Boolean)
            m_HasAccess = value
        End Set
    End Property

    Public Property IsDefault() As Boolean
        Get
            Return m_IsDefault
        End Get
        Set(ByVal value As Boolean)
            m_IsDefault = value
        End Set
    End Property



    Public Property IsDB() As Boolean
        Get
            Return m_IsDB
        End Get
        Set(ByVal value As Boolean)
            m_IsDB = value
        End Set
    End Property

    Public Property MenuID() As Integer
        Get
            Return m_MenuID
        End Get
        Set(ByVal value As Integer)
            m_MenuID = value
        End Set
    End Property

    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set(ByVal value As Boolean)
            m_Active = value
        End Set
    End Property

    Public Property PageContent() As String
        Get
            Return m_PageContent
        End Get
        Set(ByVal value As String)
            m_PageContent = value
        End Set
    End Property

    Public Property PageTitle() As String
        Get
            Return m_PageTitle
        End Get
        Set(ByVal value As String)
            m_PageTitle = value
        End Set
    End Property

    Public Property PageName() As String
        Get
            Return m_PageName
        End Get
        Set(ByVal value As String)
            m_PageName = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return m_FileName
        End Get
        Set(ByVal value As String)
            m_FileName = value
        End Set
    End Property

    Public Property IsBespoke() As Boolean
        Get
            Return m_IsBespoke
        End Get
        Set(ByVal value As Boolean)
            m_IsBespoke = value
        End Set
    End Property

    Public Property MultiMenu() As String
        Get
            Return m_MultiMenu
        End Get
        Set(ByVal value As String)
            m_MultiMenu = value
        End Set
    End Property
    Public Property SelectedTeams() As Array
        Get
            Return m_SelectedTeams
        End Get
        Set(ByVal value As Array)
            m_SelectedTeams = value
        End Set
    End Property

    Public Property SelectedEmployees() As Array
        Get
            Return m_SelectedEmps
        End Get
        Set(ByVal value As Array)
            m_SelectedEmps = value
        End Set
    End Property

    Sub New()

    End Sub

    Sub New(ByVal PageID As Integer)
        GetPageByPageID(PageID)

    End Sub

    Public Sub New(ByVal PageName As String)
        Dim daPage As New I_PAGETableAdapter
        Dim intPage As Nullable(Of Integer)
        daPage.GetPageIDByPageName(PageName, intPage)

        If Not intPage.HasValue Then
            intPage = 0
        End If

        GetPageByPageID(intPage)
    End Sub

    Public Sub New(ByVal PageName As String, ByVal EmpID As Integer)

        Dim intAccessCount As Integer

        Dim daPage As New I_PAGETableAdapter
        Dim intPage As Nullable(Of Integer)
        daPage.GetPageIDByPageName(PageName, intPage)

        If Not intPage.HasValue Then
            intPage = 0
        End If

        daPage.CheckAccessRights(PageName, EmpID, intAccessCount)

        If intAccessCount > 0 Then
            m_HasAccess = True
        Else
            m_HasAccess = False
        End If

        GetPageByPageID(intPage)
    End Sub



    Public Sub GetPageByPageID(ByVal PageID As Integer)
        Dim daPage As New I_PAGETableAdapter
        Dim dtPage As New I_PAGEDataTable
        Dim drPage As I_PAGERow

        dtPage = daPage.GetPageByPageID(PageID)
        If dtPage.Count < 1 Then
            m_IsDB = False
        Else
            m_IsDB = True
            drPage = dtPage(0)
            m_PageID = drPage.PageID
            m_PageName = drPage.Name
            m_PageTitle = drPage.Title
            m_PageContent = drPage.PageContent
            m_MenuID = drPage.MenuID
            m_Active = drPage.Active
            m_FileName = drPage.FileName
            m_IsBespoke = drPage.IsBespoke
            'm_IsDefault = drPage.IsDefault
        End If

    End Sub

    Public Function UpdatePage(ByVal PageID As Integer) As Integer
        Dim daPage As New I_PAGETableAdapter
        Dim dtPage As New I_PAGEDataTable
        Dim drPage As I_PAGERow

        Dim errors As New ErrorCollection
        Dim intFileCount As Integer
        Dim strFileName As String

        If m_IsBespoke = True Then
            strFileName = m_FileName
            m_PageContent = ""

            If String.IsNullOrEmpty(strFileName) Then
                errors.Add("You must select a page")
            End If

        Else
            strFileName = CreateFilename(m_PageName)
        End If

        daPage.CheckFileName(strFileName, PageID, intFileCount)

        If intFileCount > 0 Then
            errors.Add("The Page Name must be unique")
        End If

        If m_PageTitle.Length >= 50 Then
            errors.Add("You cannot enter a Page Title of more than 50 characters")
        End If

        If String.IsNullOrEmpty(m_PageTitle) Then
            errors.Add("You must enter a Page Title")
        End If

        If m_PageName.Length >= 50 Then
            errors.Add("You cannot enter a Page Name of more than 50 characters")
        End If

        If String.IsNullOrEmpty(m_PageName) Then
            errors.Add("You must enter a Page Name")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        If String.IsNullOrEmpty(m_PageContent) Then
            m_PageContent = ""
        End If

        If PageID <= 0 Then
            drPage = dtPage.NewI_PAGERow
        Else
            dtPage = daPage.GetPageByPageID(PageID)
            drPage = dtPage(0)
        End If

        drPage.Name = m_PageName
        drPage.Title = m_PageTitle
        drPage.PageContent = m_PageContent
        drPage.MenuID = m_MenuID
        drPage.Active = m_Active
        drPage.FileName = strFileName
        drPage.IsBespoke = m_IsBespoke
        'drPage.IsDefault = m_IsDefault



        If PageID <= 0 Then
            dtPage.AddI_PAGERow(drPage)
        End If
        Dim intReturnValue As Integer
        intReturnValue = daPage.Update(drPage)

        'daPage.TMP_DEMO(343)

        Dim daMenuPage As New I_MENU_PAGETableAdapter
        Dim dtMenuPage As New I_MENU_PAGEDataTable



        If Not String.IsNullOrEmpty(m_MultiMenu) Then
            daMenuPage.SetMultiMenus(drPage.PageID, m_MultiMenu)
        End If
        If PageID <= 0 Then
            'HttpContext.Current.Response.Write(m_MenuID)
            'HttpContext.Current.Response.Write(drPage.PageID)
            Dim drMenuPage As I_MENU_PAGERow
            drMenuPage = dtMenuPage.NewI_MENU_PAGERow
            drMenuPage.MenuID = m_MenuID
            drMenuPage.PageID = drPage.PageID
            drMenuPage.Rank = 1
            dtMenuPage.AddI_MENU_PAGERow(drMenuPage)
            daMenuPage.Update(drMenuPage)

        End If
        Dim i As Integer

        If m_IsBespoke = False Then
            Dim strPath As String = HttpContext.Current.Server.MapPath(m_Path)
            Dim strFileToPaste As String = strPath & strFileName
            Dim strFiletoMove As String = ""
            Dim strFileToCopy As String

            strFileToCopy = strPath & "3tierWebTemplate.aspx"

            If m_PageID <= 0 Then
                File.Copy(strFileToCopy, strFileToPaste, True)
            Else
                File.Move(strFiletoMove, strFileToPaste)
            End If
        End If


        If m_IsDefault = True Then
            daPage.SetDefaultAccessControl(drPage.PageID)

        End If
    End Function


    Public Function DeleteByPageID(ByVal PageID As Integer) As Integer

        Dim daPage As New I_PAGETableAdapter
        Dim daMenuPage As New I_MENU_PAGETableAdapter
        Dim dtPage As New I_PAGEDataTable
        Dim drPage As I_PAGERow

        daMenuPage.DeleteByPageID(PageID)

        dtPage = daPage.GetPageByPageID(PageID)
        drPage = dtPage(0)
        drPage.Delete()

        Return daPage.Update(dtPage)

    End Function

    Function CreateFilename(ByVal theString As String) As String

        Dim strAlphaNumeric As String = "-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" 'Used to check for numeric characters.
        Dim i As Integer
        Dim CleanedString As String = ""
        theString = theString.Replace(" ", "-")
        Dim StrChar As String
        For i = 1 To Len(theString)
            StrChar = Mid(theString, i, 1)
            If InStr(strAlphaNumeric, StrChar) Then
                CleanedString = CleanedString & StrChar
            End If
        Next

        Return CleanedString & ".aspx"
    End Function


    Public Function GetPageList() As Array
        Dim strPath As String = HttpContext.Current.GetSection("web_manager")("bespoke_path")
        Dim di As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(strPath))
        Dim files() As System.IO.FileInfo = di.GetFiles("*.aspx")
        Return files

    End Function


    Public Function DoesPageHaveMultiMenus(ByVal PageID As Integer, ByVal WebsiteID As Integer) As Boolean
        Dim daPage As New I_PAGETableAdapter
        Dim intMenuCount As Integer
        daPage.MultiMenuCountByWebsiteID(WebsiteID, PageID, intMenuCount)

        If intMenuCount > 0 Then
            Return True
        Else
            Return False
        End If

    End Function


    Public Function GetSubMenu(ByVal PageID As Integer) As I_PAGEDataTable

        Dim daPage As New I_PAGETableAdapter
        Dim dtPage As New I_PAGEDataTable

        dtPage = daPage.GetSubMenu(PageID)
        Return dtPage

    End Function
    Public Function GetSubMenuByEmpID(ByVal PageID As Integer, ByVal EmpID As Integer) As I_PAGEDataTable

        Dim daPage As New I_PAGETableAdapter
        Dim dtPage As New I_PAGEDataTable

        dtPage = daPage.GetSubMenuByPageIDAndEmpID(PageID, EmpID)
        Return dtPage

    End Function
    Public Function GetMultiPageByMenuID(ByVal MenuID As Integer) As I_PAGEDataTable
        Dim daPage As New I_PAGETableAdapter
        Return daPage.GetMultiPageByMenuID(MenuID)

    End Function

    Public Sub SetPageRank(ByVal lst As ListBox)
        Dim daMenu As New I_MENU_PAGETableAdapter

        Dim item As ListItem
        Dim i As Integer = 0
        For Each item In lst.Items
            daMenu.UpdateMultiMenuRankByMultiMenuID(item.Value, i)
            i = i + 1
        Next

    End Sub

    Public Function GetAllowedPages(ByVal EmpID As Integer) As DataTable
        Dim daPage As New I_PAGETableAdapter
        Return daPage.GetAllowPages(EmpID)

    End Function

    Public Function SetAccessControl(ByVal Pages As String, ByVal EmpID As Integer) As Integer
        Dim daPage As New I_PAGETableAdapter
        Return daPage.SetAccessControl(Pages, EmpID)
    End Function


    Public Function GetOtherMenuByEmpID(ByVal MenuID As Integer, ByVal EmpID As Integer) As I_PAGEDataTable
        Dim daPage As New I_PAGETableAdapter
        Return daPage.GetOtherMenus(MenuID, EmpID)
    End Function


    Public Function GetFooter(ByVal EmpID As Integer) As I_PAGEDataTable
        Return GetOtherMenuByEmpID(m_FooterID, EmpID)
    End Function
End Class
