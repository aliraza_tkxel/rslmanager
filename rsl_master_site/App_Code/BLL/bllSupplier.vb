Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports dalSupplier
Imports dalSupplierTableAdapters

Public Class bllSupplier

    Private m_OrgID As Integer
    Private m_Name As String
    Private m_FullAddress As String
    Private m_Tel As String
    Private m_Fax As String
    Private m_Website As String


    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property

    Public Property OrgID() As Integer
        Get
            Return m_OrgID
        End Get
        Set(ByVal value As Integer)
            m_OrgID = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return m_FullAddress
        End Get
        Set(ByVal value As String)
            m_FullAddress = value
        End Set
    End Property

    Public Property Tel() As String
        Get
            Return m_Tel
        End Get
        Set(ByVal value As String)
            m_Tel = value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return m_Fax
        End Get
        Set(ByVal value As String)
            m_Fax = value
        End Set
    End Property

    Public Property Website() As String
        Get
            Return m_Website
        End Get
        Set(ByVal value As String)
            m_Website = value
        End Set
    End Property

    Public Property FullAddress() As String
        Get
            Return m_FullAddress
        End Get
        Set(ByVal value As String)
            m_Website = value
        End Set
    End Property
    Sub New()

    End Sub


    Sub New(ByVal OrgID As Integer)
        Dim daSupp As New S_ORGANISATIONTableAdapter
        Dim dtSupp As New S_ORGANISATIONDataTable
        Dim drSupp As S_ORGANISATIONRow

        dtSupp = daSupp.GetByOrgID(OrgID)
        drSupp = dtSupp(0)

        m_OrgID = drSupp.ORGID

        m_Name = drSupp.NAME
        Dim strBuilding As String = ""
        Dim strAddress1 As String = ""
        Dim strAddress2 As String = ""
        Dim strAddress3 As String = ""
        Dim strTown As String = ""
        Dim strCounty As String = ""
        Dim strPostcode As String = ""

        If Not drSupp.IsBUILDINGNAMENull Then
            strBuilding = drSupp.BUILDINGNAME & ", "
        End If

        If Not drSupp.IsADDRESS1Null Then
            strAddress1 = drSupp.ADDRESS1 & ", "
        End If

        If Not drSupp.IsADDRESS2Null Then
            strAddress2 = drSupp.ADDRESS2 & ", "
        End If

        If Not drSupp.IsADDRESS3Null Then
            strAddress3 = drSupp.ADDRESS3 & ", "
        End If

        If Not drSupp.IsTOWNCITYNull Then
            strTown = drSupp.TOWNCITY & ", "
        End If

        If Not drSupp.IsCOUNTYNull Then
            strCounty = drSupp.COUNTY & ", "
        End If

        If Not drSupp.IsPOSTCODENull Then
            strPostcode = drSupp.POSTCODE & ", "
        End If

        m_FullAddress = strBuilding & strAddress1 & strAddress2 & strAddress3 & strTown & strCounty & strPostcode

        If m_FullAddress.Length > 1 Then
            m_FullAddress = m_FullAddress.TrimEnd(", ")
        End If


        If Not IsDBNull(drSupp("FULLADDRESS")) Then
            m_FullAddress = drSupp("FULLADDRESS")
        End If


        If Not drSupp.IsTELEPHONE1Null Then
            m_Tel = drSupp.TELEPHONE1
        End If

        If Not drSupp.IsFAXNull Then
            m_Fax = drSupp.FAX
        End If

        If Not drSupp.IsWEBSITENull Then
            m_Website = drSupp.WEBSITE
        End If

        If Not drSupp.IsNAMENull Then
            m_Name = drSupp.NAME
        End If


    End Sub
    Public Function Search(ByVal OrgID As Nullable(Of Integer), ByVal Name As String) As S_ORGANISATIONDataTable
        Dim daSupp As New S_ORGANISATIONTableAdapter
        Return daSupp.Search(Name, OrgID)

    End Function
End Class
