Imports Microsoft.VisualBasic
Imports dalPage
Imports dalPageTableAdapters
Imports dalMenuPage
Imports dalMenuPageTableAdapters
Imports System.IO
Imports System.Data
Imports dalAlerts

Public Class bllAlerts

    Private m_AlertName As String    
    Private m_SOrder As Integer


    Public Function GetMyAlerts(ByVal sEmpID As String) As E_ALERTSDataTable
        Dim daAlerts As New dalAlertsTableAdapters.E_ALERTSTableAdapter

        Return daAlerts.GetAlertsByEmployee(sEmpID)
    End Function

    Public Function GetAlerts() As E_ALERTSDataTable
        Dim daAlerts As New dalAlertsTableAdapters.E_ALERTSTableAdapter

        ' return back all alerts        
        Return daAlerts.GetData()        
    End Function

End Class
