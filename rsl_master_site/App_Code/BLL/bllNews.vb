Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports dalNews
Imports dalNewsTableAdapters
Imports System.IO.Path
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.Guid

Public Class bllNews

    Private m_NewsID As Integer
    Private m_Headline As String
    Private m_DateCreated As Date
    Private m_Content As String
    Private m_Category As Integer
    Private m_Rank As Integer
    Private m_ImagePath As String
    Private m_HomeImagePath As String
    Private m_Active As Boolean
    Private m_ImageUpload As FileUpload
    Private m_HomeImage As FileUpload
    Private m_Homepage As Boolean
    Private m_TenantOnline As Boolean
    Sub New()

    End Sub
    Sub New(ByVal NewsID As Integer)
        GetByNewsID(NewsID)
    End Sub

    Public Sub GetTopNewsItem()
        Dim daNews As New I_NEWSTableAdapter
        Dim intTopNewsID As Integer
        daNews.GetTopNewsID(intTopNewsID)
        GetByNewsID(intTopNewsID)
    End Sub

    Public Function GetHomePage() As Boolean
        Dim daNews As New I_NEWSTableAdapter
        Dim intTopNewsID As Integer
        daNews.GetHomePageID(intTopNewsID)
        If intTopNewsID = 0 Then
            Return False
        Else
            GetByNewsID(intTopNewsID)

            Return True
        End If

    End Function


    Private Sub GetByNewsID(ByVal NewsID As Integer)
        Dim daNews As New I_NEWSTableAdapter
        Dim dtNews As New I_NEWSDataTable
        Dim drNews As I_NEWSRow

        dtNews = daNews.GetNewsByNewsID(NewsID)
        drNews = dtNews(0)

        m_NewsID = drNews.NewsID
        m_DateCreated = drNews.DateCreated

        m_Active = drNews.Active


        If drNews.IsHeadlineNull Then
            m_Headline = ""
        Else
            m_Headline = drNews.Headline
        End If

        If drNews.IsHomePageImageNull Then
            m_HomeImagePath = String.Empty
        Else
            m_HomeImagePath = drNews.HomePageImage
        End If


        If drNews.IsImagePathNull Then
            m_ImagePath = String.Empty
        Else
            m_ImagePath = drNews.ImagePath
        End If


        If drNews.IsNewsContentNull Then
            m_Content = ""
        Else
            m_Content = drNews.NewsContent
        End If

        If drNews.IsIsHomePageNull Then
            m_Homepage = False
        Else
            m_Homepage = drNews.IsHomePage
        End If

        If drNews.IsIsTenantOnlineNull Then
            m_TenantOnline = False
        Else
            m_TenantOnline = drNews.IsTenantOnline
        End If

    End Sub
    Public Function GetNewsStories() As I_NEWSDataTable
        Dim daNews As New I_NEWSTableAdapter
        Return daNews.GetData
    End Function

    Public Function GetHeadlines() As I_NEWSDataTable
        Dim daNews As New I_NEWSTableAdapter
        Return daNews.GetHeadlines
    End Function

    Public Property NewsID() As Integer
        Get
            Return m_NewsID
        End Get
        Set(ByVal value As Integer)
            m_NewsID = value
        End Set
    End Property

    Public Property Headline() As String
        Get
            Return m_Headline
        End Get
        Set(ByVal value As String)
            m_Headline = value
        End Set
    End Property

    Public ReadOnly Property DateCreated() As Date
        Get
            Return m_DateCreated
        End Get

    End Property

    Public Property Content() As String
        Get
            Return m_Content
        End Get
        Set(ByVal value As String)
            m_Content = value
        End Set
    End Property

    Public Property Category() As Integer
        Get
            Return m_Category
        End Get
        Set(ByVal value As Integer)
            m_Category = value
        End Set
    End Property

    Public ReadOnly Property ImagePath() As String
        Get
            Return m_ImagePath
        End Get
    End Property

    Public ReadOnly Property HomeImagePath() As String
        Get
            Return m_HomeImagePath
        End Get
    End Property
    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set(ByVal value As Boolean)
            m_Active = value
        End Set
    End Property

    Public Property TenantOnline() As Boolean
        Get
            Return m_TenantOnline
        End Get
        Set(ByVal value As Boolean)
            m_TenantOnline = value
        End Set
    End Property


    Public Property HomePage() As Boolean
        Get
            Return m_Homepage
        End Get
        Set(ByVal value As Boolean)
            m_Homepage = value
        End Set
    End Property
    Public Property ImageUpload() As FileUpload
        Get
            Return m_ImageUpload
        End Get
        Set(ByVal value As FileUpload)
            m_ImageUpload = value
        End Set
    End Property

    Public Property HomeImageUpload() As FileUpload
        Get
            Return m_HomeImage
        End Get
        Set(ByVal value As FileUpload)
            m_HomeImage = value
        End Set
    End Property
    Public Function UpdateNews(ByVal NewsID As Integer) As Integer
        Dim errors As New ErrorCollection
        Dim strEx As String = ""
        Dim strNewFileName As String = ""
        HttpContext.Current.Response.Write(m_ImageUpload.HasFile)
        If m_ImageUpload.HasFile Then

            strEx = GetExtension(m_ImageUpload.FileName)
            strEx = strEx.ToLower

            If strEx = ".gif" Or strEx = ".jpeg" Or strEx = ".jpg" Or strEx = ".png" Then
            Else
                errors.Add("You can only upload jpeg and gif files for the main image")
            End If

            If m_ImageUpload.FileBytes.Length > 1048576 Then
                errors.Add("You can only upload files less than 1 MB (megabyte) for the main image")
            End If
        End If


        If m_HomeImage.HasFile Then

            strEx = GetExtension(m_HomeImage.FileName)
            strEx = strEx.ToLower

            If strEx = ".gif" Or strEx = ".jpeg" Or strEx = ".jpg" Or strEx = ".png" Then
            Else
                errors.Add("You can only upload jpeg and gif files for the home page image")
            End If

            If m_HomeImage.FileBytes.Length > 1048576 Then
                errors.Add("You can only upload files less than 1 MB (megabyte) for the home page image")
            End If
        End If


        If String.IsNullOrEmpty(m_Headline) Then
            errors.Add("You must add a headline")
        End If

        If m_Headline.Length > 250 Then
            errors.Add("You cannot add a headline of more than 250 characters")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If



        Dim daNews As New I_NEWSTableAdapter
        Dim dtNews As New I_NEWSDataTable
        Dim drNews As I_NEWSRow
        Dim strImage As String = String.Empty
        Dim strHomeImage As String = String.Empty
        Dim ImagePath As String = "/BHAIntranetImagesNews/"


        If NewsID = 0 Then
            drNews = dtNews.NewI_NEWSRow
            drNews.DateCreated = Now
        Else
            dtNews = daNews.GetNewsByNewsID(NewsID)
            drNews = dtNews(0)

            If Not drNews.IsHomePageImageNull Then
                strHomeImage = drNews.HomePageImage
            End If

            If Not drNews.IsImagePathNull Then
                strImage = drNews.ImagePath
            End If
        End If



        If m_ImageUpload.HasFile Then

            If Not String.IsNullOrEmpty(strImage) Then
                filehelper.DeleteIfExists(m_ImageUpload, strImage, ImagePath)
            End If

            drNews.ImagePath = filehelper.UploadImageFile(m_ImageUpload, ImagePath, 100)
            m_ImagePath = Trim(drNews.ImagePath)
        End If
        If m_Homepage = True Then

            If m_HomeImage.HasFile Then

                If Not String.IsNullOrEmpty(strHomeImage) Then
                    filehelper.DeleteIfExists(m_HomeImage, strHomeImage, ImagePath)
                End If

                drNews.HomePageImage = filehelper.UploadImageFile(m_HomeImage, ImagePath, 150)
                m_HomeImagePath = Trim(drNews.HomePageImage)

            End If
        Else
            HttpContext.Current.Response.Write(strHomeImage & "test" & "<br>")
            If Not String.IsNullOrEmpty(strHomeImage) Then
                Dim tmpstr As String = HttpContext.Current.Server.MapPath(ImagePath) & strHomeImage
                HttpContext.Current.Response.Write(tmpstr)
                If File.Exists(tmpstr) Then
                    File.Delete(tmpstr)
                End If
            End If

            drNews.SetHomePageImageNull()
        End If
            drNews.Headline = m_Headline
            drNews.NewsContent = m_Content
            drNews.Active = m_Active
        drNews.IsHomePage = m_Homepage
        drNews.IsTenantOnline = m_TenantOnline



            If NewsID = 0 Then
                dtNews.AddI_NEWSRow(drNews)
            End If
        Dim intReturn As Integer = daNews.Update(dtNews)


        If m_Homepage = True Then
            daNews.SetIsHomePage(drNews.NewsID)
            HttpContext.Current.Response.Write("test" & drNews.NewsID)
        End If

        Return intReturn

    End Function

    Public Function DeleteNews(ByVal newsid As Integer) As Integer

        Dim daNews As New I_NEWSTableAdapter
        Dim dtNews As New I_NEWSDataTable
        Dim drNews As I_NEWSRow

        dtNews = daNews.GetNewsByNewsID(newsid)
        drNews = dtNews(0)
        drNews.Delete()
        Return daNews.Update(dtNews)

    End Function
End Class
