Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class bllIntranetLogin
    Inherits bllBaseLogin

    Public Function SetSession(ByVal objSession As MSDN.mySession, Optional ByVal isSso as Boolean = false) As Boolean

        Dim isContractor As Integer
        Dim strTeamCode As String
        Dim rows As Data.DataRowCollection
        rows = dsLogin.Tables(0).Rows()

        objSession("FirstName") = rows(0).Item("FIRSTNAME")
        objSession("LastName") = rows(0).Item("LASTNAME")
        objSession("USERID") = rows(0).Item("EMPLOYEEID")        
        objSession("TeamCode") = rows(0).Item("TeamCode")

        Me.FirstName = rows(0).Item("FIRSTNAME") ' objSession("FirstName")
        Me.LastName = rows(0).Item("LASTNAME") 'objSession("LastName")
        If isSso Then
            Me.ssoUserName = rows(0).Item("ssousername") 
        End If
          
        Me.StoreLoginHistory()

        If Not IsDBNull(rows(0).Item("ORGID")) Then
            objSession("OrgID") = rows(0).Item("ORGID")
        End If

        strTeamCode = rows(0).Item("TEAMCODE")
        objSession("TEAMNAME") = rows(0).Item("TEAMNAME")


        If Not IsDBNull(rows(0).Item("ORGID")) Then
            isContractor = rows(0).Item("ORGID")
        End If

        Dim bllEmp As New bllEmployees(rows(0).Item("EMPLOYEEID"))
        If bllEmp.Active Then
            objSession("LASTLOGGEDIN") = bllEmp.LastLoggedIn
            bllEmp.SetLastLoggedIn(rows(0).Item("EMPLOYEEID"))

            If objSession("TeamCode") = "SUP" Then
                ' if a contractor logged in then switch to old whiteboard
                HttpContext.Current.Response.Redirect("/MyWeb/MyWhiteboard.asp")
            ElseIf objSession("TeamCode") = "PMA" Then
                ' board member
                'HttpContext.Current.Response.Redirect("/BoardMembers/BM.asp")
                HttpContext.Current.Response.Redirect("/BHAIntranet/CustomPages/MyWhiteboard.aspx")
            Else
                ' everyone else
                HttpContext.Current.Response.Redirect("/BHAIntranet/CustomPages/MyWhiteboard.aspx")
            End If
            Return True
        Else
            ' employee is inactive
            Return False
        End If

    End Function

    Public Function getCredentialsByWorkEmail(ByVal workEmail As String) As Boolean
        Dim status As Boolean = False

        Dim connStrRSL As String = datahelper.getStrConn("connRSL")

        Dim connRSL As SqlConnection = datahelper.getconn(connStrRSL)
        connRSL.Open()

        Dim SQL = "SELECT * FROM dbo.E_CONTACT C, AC_LOGINS L WHERE C.EMPLOYEEID = L.EMPLOYEEID AND (C.WORKEMAIL = '" + workEmail.Replace("'", "''") + "')"

        Dim cmd As New SqlCommand(SQL, connRSL)

        Dim dr As SqlDataReader = cmd.ExecuteReader()

        If dr.Read Then

            If Not dr.IsDBNull(dr.GetOrdinal("LOGIN")) Then
                UserName = dr.GetString(dr.GetOrdinal("LOGIN"))
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("PASSWORD")) Then
                Dim b As Byte() = Convert.FromBase64String(dr.GetString(dr.GetOrdinal("PASSWORD")))

                Password = System.Text.Encoding.UTF8.GetString(b)
                'Password = dr.GetString(dr.GetOrdinal("PASSWORD"))
            End If

            status = True

        End If
        Return status
    End Function

End Class
