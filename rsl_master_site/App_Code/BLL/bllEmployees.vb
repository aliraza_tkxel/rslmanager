Imports Microsoft.VisualBasic
Imports dalEmployee
Imports dalEmployeeTableAdapters
Imports System.IO.Path
Imports System.IO
Imports System.Data

Public Class bllEmployees
    Private m_ImagePath As String
    Private m_RolePath As String
    Private m_fileRole As FileUpload
    Private m_fileImage As FileUpload
    Private m_EmpID As Integer
    Private m_ValidateRole As Boolean = False
    Private m_ValidateImage As Boolean = False
    Private m_FullName As String
    Private m_JobTitle As String
    Private m_StartDate As Date
    Private m_StartDate_Formatted As String
    Private m_About As String
    Private m_TeamName As String
    Private m_WorkDD As String
    Private m_Mobile As String
    Private m_WorkMobile As String
    Private m_Email As String
    Private m_OfficeLoc As String
    Private m_LineManager As String
    Private m_LineManagerId As Integer
    Private m_LastLoggedIn As String
    Private m_Profile As String
    Private m_Active As Integer
    Private m_TeamID As Integer



    Public ReadOnly Property EmpID() As Integer
        Get
            Return m_EmpID
        End Get
    End Property
    Public ReadOnly Property LineManager() As String
        Get
            Return m_LineManager
        End Get
    End Property

    Public ReadOnly Property LineManagerId() As Integer
        Get
            Return m_LineManagerId
        End Get
    End Property

    Public ReadOnly Property OfficeLocation() As String
        Get
            Return m_OfficeLoc
        End Get
    End Property

    Public ReadOnly Property Email() As String
        Get
            Return m_Email
        End Get
    End Property

    Public ReadOnly Property Mobile() As String
        Get
            Return m_Mobile
        End Get
    End Property

    Public ReadOnly Property WorkMobile() As String
        Get
            Return m_WorkMobile
        End Get
    End Property

    Public ReadOnly Property WorkDD() As String
        Get
            Return m_WorkDD
        End Get
    End Property

    Public Property ImagePath() As String
        Get
            Return m_ImagePath
        End Get
        Set(ByVal value As String)
            m_ImagePath = value
        End Set
    End Property

    Public Property RolePath() As String
        Get
            Return m_RolePath
        End Get
        Set(ByVal value As String)
            m_RolePath = value
        End Set
    End Property

    Public WriteOnly Property FileImage() As FileUpload
        Set(ByVal value As FileUpload)
            m_fileImage = value
        End Set
    End Property

    Public WriteOnly Property FileRole() As FileUpload
        Set(ByVal value As FileUpload)
            m_fileRole = value
        End Set
    End Property

    Public WriteOnly Property IsImageMandatory() As Boolean
        Set(ByVal value As Boolean)
            m_ValidateImage = value
        End Set
    End Property

    Public WriteOnly Property IsRoleMandatory() As Boolean
        Set(ByVal value As Boolean)
            m_ValidateRole = value
        End Set
    End Property

    Public ReadOnly Property FullName() As String
        Get
            Return m_FullName
        End Get
    End Property

    Public ReadOnly Property JobTitle() As String
        Get
            Return m_JobTitle
        End Get
    End Property

    Public ReadOnly Property StartDate() As String
        Get
            Return m_StartDate
        End Get
    End Property

    Public ReadOnly Property TeamName() As String
        Get
            Return m_TeamName
        End Get
    End Property

    Public ReadOnly Property About() As String
        Get
            Return m_About
        End Get
    End Property

    Public ReadOnly Property Profile() As String
        Get
            Return m_Profile
        End Get
    End Property

    Public Property LastLoggedIn() As String
        Get
            Return m_LastLoggedIn
        End Get
        Set(ByVal value As String)
            m_LastLoggedIn = value
        End Set
    End Property

    Public ReadOnly Property Active() As Integer
        Get
            Return m_Active
        End Get
    End Property

    Public ReadOnly Property TeamID() As Integer
        Get
            Return m_TeamID
        End Get
    End Property

    Public Function GetEmployeesByTeamID(ByVal TeamID As Integer) As E__EMPLOYEEDataTable
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.GetEmployeesByTeamID(TeamID)
    End Function

    Public Function GetEmployeesByTeamIDAndPageID(ByVal TeamID As Integer, ByVal PageID As Integer) As E__EMPLOYEEDataTable
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.GetEmployeesByTeamIDAndPageID(TeamID, PageID)
    End Function

    Public Function GetNumEmployeesBelowMe(ByVal iEmpId As Integer) As Integer
        Dim iRowCount As Integer = -1
        Dim daEmp As New E__EMPLOYEETableAdapter
        Dim dtEmp As E__EMPLOYEEDataTable
        dtEmp = daEmp.GetEmployeesByLineManagerID(iEmpId)

        If dtEmp IsNot Nothing Then
            iRowCount = dtEmp.Rows.Count
        End If

        Return iRowCount
    End Function

    Public Sub New()

    End Sub

    Private Function GetEmployeeByEmployeeID(ByVal EmpID As Integer) As Boolean
        ' Returns TRUE if employee is active within the database, otherwise returns FALSE
        Dim daEmp As New E__EMPLOYEETableAdapter
        Dim dtEmp As New E__EMPLOYEEDataTable
        Dim drEmp As E__EMPLOYEERow
        Dim strFirstName As String
        Dim strLastname As String

        m_EmpID = EmpID
        dtEmp = daEmp.GetByEmployeeID(EmpID)
        If dtEmp.Rows.Count > 0 Then
            drEmp = dtEmp(0)

            If drEmp.IsFIRSTNAMENull Then
                strFirstName = ""
            Else
                strFirstName = drEmp.FIRSTNAME
            End If

            If drEmp.IsLASTNAMENull Then
                strLastname = ""
            Else
                strLastname = drEmp.LASTNAME
            End If

            If IsDBNull(drEmp("WORKEMAIL")) Then
                m_Email = "N/A"
            Else
                m_Email = Trim(drEmp("WORKEMAIL"))
            End If

            If IsDBNull(drEmp("WORKDD")) Then
                m_WorkDD = "N/A"
            Else
                m_WorkDD = drEmp("WORKDD")
            End If

            If IsDBNull(drEmp("MOBILE")) Then
                m_Mobile = "N/A"
            Else
                m_Mobile = drEmp("MOBILE")
            End If

            If IsDBNull(drEmp("WORKMOBILE")) Then
                m_WorkMobile = "N/A"
            Else
                m_WorkMobile = drEmp("WORKMOBILE")
            End If

            If IsDBNull(drEmp("LINEMANAGERNAME")) Then
                m_LineManager = "N/A"
            Else
                m_LineManager = drEmp("LINEMANAGERNAME")
            End If

            If IsDBNull(drEmp("LINEMANAGER")) Then
                m_LineManagerId = Nothing
            Else
                m_LineManagerId = drEmp("LINEMANAGER")
            End If

            If IsDBNull(drEmp("OFFICEDESC")) Then
                m_OfficeLoc = "N/A"
            Else
                m_OfficeLoc = drEmp("OFFICEDESC")
            End If

            If drEmp.IsLASTLOGGEDINNull Then
                m_LastLoggedIn = "N/A"
            Else
                Dim dt As DateTime = drEmp.LASTLOGGEDIN
                m_LastLoggedIn = dt.ToString("dd MMMM yyyy HH:mm")
            End If

            m_FullName = strFirstName & " " & strLastname
            m_JobTitle = drEmp("JOBTITLE")
            m_StartDate = drEmp("STARTDATE")
            m_TeamName = drEmp("TEAMNAME")

            If drEmp.IsPROFILENull Then
                m_About = "N/A"
            Else
                m_About = drEmp.PROFILE
            End If



            If drEmp.IsIMAGEPATHNull Then
                m_ImagePath = Nothing
            Else
                m_ImagePath = Trim(drEmp.IMAGEPATH)
            End If

            If drEmp.IsROLEPATHNull Then
                m_RolePath = Nothing
            Else
                m_RolePath = Trim(drEmp.ROLEPATH)
            End If

            If drEmp.IsPROFILENull Then
                m_Profile = "N/A"
            Else
                m_Profile = Trim(drEmp.PROFILE)
            End If

            If IsDBNull(drEmp("TEAMID")) Then
                m_TeamID = Nothing
            Else
                m_TeamID = drEmp("TEAMID")
            End If

            m_Active = True
        Else
            ' employee not found, so inactive
            m_Active = False
        End If
    End Function


    Public Sub New(ByVal EmpID As Integer)
        GetEmployeeByEmployeeID(EmpID)
    End Sub

    Public Sub GetLatestEmployee()
        Dim daEmp As New E__EMPLOYEETableAdapter
        Dim intLatestEmpID As Integer = 0
        daEmp.GetLatestEmployeeID(intLatestEmpID)
        GetEmployeeByEmployeeID(intLatestEmpID)
    End Sub

    Public Function GetNewStarters() As DataTable
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.GetNewStarters()
    End Function

    Public Function Update() As Integer
        Dim errors As New ErrorCollection
        Dim strEx As String

        If Not IsNothing(m_fileImage) Then
            If m_ValidateImage = True Then

                If m_fileImage.HasFile Then

                    strEx = GetExtension(m_fileImage.FileName)
                    strEx = strEx.ToLower

                    If strEx = ".gif" Or strEx = ".jpeg" Or strEx = ".jpg" Then
                    Else
                        errors.Add("You can only upload jpeg and gif files for the Employee Image")
                    End If

                    If m_fileImage.FileBytes.Length > 1048576 Then
                        errors.Add("You can only upload files less than 1 MB (megabyte)")
                    End If

                Else
                    errors.Add("You must upload a file")
                End If
            End If
        End If

        If Not IsNothing(m_fileRole) Then
            If m_ValidateRole = True Then
                If m_fileRole.HasFile Then
                    strEx = GetExtension(m_fileRole.FileName)
                    strEx = strEx.ToLower

                    If strEx = ".pdf" Or strEx = ".doc" Then
                    Else
                        errors.Add("You can only upload pdf and Word files for the role type")
                    End If

                    If m_fileRole.FileBytes.Length > 1048576 Then
                        errors.Add("You can only upload files less than 1 MB (megabyte)")
                    End If
                Else
                    errors.Add("You must upload a file")
                End If
            End If
        End If


        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daEmp As New E__EMPLOYEETableAdapter
        Dim dtEmp As New E__EMPLOYEEDataTable
        Dim drEmp As E__EMPLOYEERow
        Dim strImagePath As String
        Dim strRolePath As String
        Dim strFullEmpImagePath = "/EmployeeImage/"
        Dim strFullRolePath = "/RoleFile/"

        Dim strDeletePath = ""

        dtEmp = daEmp.GetByEmployeeID(m_EmpID)
        drEmp = dtEmp(0)

        If drEmp.IsIMAGEPATHNull Then
            strImagePath = Nothing
        Else
            strImagePath = drEmp.IMAGEPATH
        End If

        If drEmp.IsROLEPATHNull Then
            strRolePath = Nothing
        Else
            strRolePath = drEmp.ROLEPATH
        End If

        If Not IsNothing(m_fileImage) Then
            If m_fileImage.HasFile Then

                If Not String.IsNullOrEmpty(strImagePath) Then
                    filehelper.DeleteIfExists(m_fileImage, strImagePath, strFullEmpImagePath)
                End If

                drEmp.IMAGEPATH = filehelper.UploadImageFile(m_fileImage, strFullEmpImagePath, 100)
                m_ImagePath = Trim(drEmp.IMAGEPATH)
            End If
        End If

        If Not IsNothing(m_fileRole) Then
            If m_fileRole.HasFile Then
                filehelper.DeleteIfExists(m_fileRole, strRolePath, strFullRolePath)
                drEmp.ROLEPATH = filehelper.UploadFile(m_fileRole, strFullRolePath)
                m_RolePath = Trim(drEmp.ROLEPATH)
            End If
        End If

        Return daEmp.Update(dtEmp)

    End Function

    Public Function DeleteRoleFile(ByVal EmpID As Integer) As Integer
        Dim daEmp As New E__EMPLOYEETableAdapter
        Dim dtEmp As New E__EMPLOYEEDataTable
        Dim drEmp As E__EMPLOYEERow

        Dim strRoleFilename As String
        Dim strRoleFullPath As String
        Dim strRolePath = "/RoleFile/"


        dtEmp = daEmp.GetByEmployeeID(EmpID)
        drEmp = dtEmp(0)

        If drEmp.IsROLEPATHNull Then
            strRoleFilename = Nothing
        Else
            strRoleFilename = drEmp.ROLEPATH
        End If
        drEmp.SetROLEPATHNull()

        strRoleFullPath = HttpContext.Current.Server.MapPath(strRolePath & strRoleFilename)

        If File.Exists(strRoleFullPath) Then
            File.Delete(strRoleFullPath)
        End If



        Return daEmp.Update(dtEmp)



    End Function

    Public Function GetByOrgID(ByVal OrgID As Integer) As E__EMPLOYEEDataTable
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.GetByOrgID(OrgID)
    End Function

    Public Function Search(ByVal strName As String) As E__EMPLOYEEDataTable
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.SearchByName(strName)
    End Function


    Public Function SetLastLoggedIn(ByVal EmpID As Integer) As Integer
        Dim daEmp As New E__EMPLOYEETableAdapter
        Return daEmp.SetLastLoggedIn(EmpID)

    End Function
End Class
