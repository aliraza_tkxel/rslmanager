Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports dalSuggestions
Imports dalSuggestionsTableAdapters
Imports System.Data
Public Class bllSuggestions
    Inherits baseBLL
    Private m_SuggestionID As Integer
    Private m_UserID As Integer
    Private m_Topic As String
    Private m_Suggestion As String
    Private m_DateSubmitted As Date
    Private m_Action As String
    Private m_TeamName As String
    Private m_EmpName As String

    Public ReadOnly Property TeamName() As String
        Get
            Return m_TeamName
        End Get
    End Property

    Public ReadOnly Property EmpName() As String
        Get
            Return m_EmpName
        End Get
    End Property

    Public Property SuggestionID() As Integer
        Get
            Return m_SuggestionID
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Public Property UserID() As Integer
        Get
            Return m_UserID
        End Get
        Set(ByVal value As Integer)
            m_UserID = value
        End Set
    End Property

    Public Property Topic() As String
        Get
            Return m_Topic
        End Get
        Set(ByVal value As String)
            m_Topic = value
        End Set
    End Property

    Public Property Suggestion() As String
        Get
            Return m_Suggestion
        End Get
        Set(ByVal value As String)
            m_Suggestion = value
        End Set
    End Property

    Public Property DateSubmitted() As DateTime
        Get
            Return m_DateSubmitted
        End Get
        Set(ByVal value As DateTime)
            m_DateSubmitted = value
        End Set
    End Property

    Public Property Action() As String
        Get
            Return m_Action
        End Get
        Set(ByVal value As String)
            m_Action = value
        End Set
    End Property
    Public Sub New()
        SetLocale()
    End Sub
    Public Sub New(ByVal SuggestionID As Integer)
        SetLocale()
        Dim daSugg As New I_SUGGESTIONSTableAdapter
        Dim dtSugg As New I_SUGGESTIONSDataTable
        Dim drSugg As I_SUGGESTIONSRow

        dtSugg = daSugg.GetBySuggestionID(SuggestionID)
        drSugg = dtSugg(0)

        m_SuggestionID = drSugg.SuggestionID
        m_UserID = drSugg.UserID
        m_DateSubmitted = drSugg.DateSubmitted

        Dim objTeam As Object = drSugg("TeamName")
        Dim objEmp As Object = drSugg("FullName")

        If IsDBNull(objTeam) Then
            m_TeamName = "N/A"
        Else
            m_TeamName = objTeam.ToString
        End If

        If IsDBNull(objEmp) Then
            m_EmpName = "N/A"
        Else
            m_EmpName = objEmp.ToString
        End If

        If drSugg.IsSuggestionNull Then
            m_Suggestion = ""
        Else
            m_Suggestion = drSugg.Suggestion
        End If

        If drSugg.IsTopicNull Then
            m_Topic = ""
        Else
            m_Topic = drSugg.Topic
        End If

        If drSugg.IsActionNull Then
            m_Action = ""
        Else
            m_Action = drSugg.Action
        End If

    End Sub
    Public Function Create() As Integer
        Dim errors As New ErrorCollection
        If String.IsNullOrEmpty(m_Suggestion) Then
            errors.Add("You must add a Suggestion")
        End If

        If m_Suggestion.Length > 1500 Then
            errors.Add("You cannot add a Suggestion of more than 1500 characters")
        End If
        If String.IsNullOrEmpty(m_Topic) Then
            errors.Add("You must add a Topic")
        End If

        If m_Topic.Length > 60 Then
            errors.Add("You cannot add a Topic of more than 50 characters")
        End If
        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daSugg As New I_SUGGESTIONSTableAdapter
        Dim dtSugg As New I_SUGGESTIONSDataTable
        Dim drSugg As I_SUGGESTIONSRow

        drSugg = dtSugg.NewI_SUGGESTIONSRow

        drSugg.Topic = m_Topic
        drSugg.Suggestion = m_Suggestion
        drSugg.UserID = m_UserID
        drSugg.DateSubmitted = Now
        dtSugg.AddI_SUGGESTIONSRow(drSugg)

        Return daSugg.Update(dtSugg)

    End Function

    Public Function Update(ByVal SuggestionID As Integer, ByVal Action As String) As Integer
        SetLocale()
        Dim daSugg As New I_SUGGESTIONSTableAdapter
        Dim dtSugg As New I_SUGGESTIONSDataTable
        Dim drSugg As I_SUGGESTIONSRow

        dtSugg = daSugg.GetBySuggestionID(SuggestionID)
        drSugg = dtSugg(0)
        drSugg.Action = Action
        Return daSugg.Update(dtSugg)
    End Function

    Public Function Search(ByVal strTopic As String, _
                            ByVal strDateFrom As String, _
                            ByVal strDateTo As String, _
                            ByVal intTeamID As Nullable(Of Integer)) As DataTable
        Dim errors As New ErrorCollection
        Dim dtDateFrom As Nullable(Of Date)
        Dim dtDateTo As Nullable(Of Date)


        If Not String.IsNullOrEmpty(strDateFrom) Then
            If Not IsDate(strDateFrom) Then
                errors.Add("The date from must be a valid date")
            Else
                dtDateFrom = valhelper.convertToDate(strDateFrom)
            End If
        End If

        If Not String.IsNullOrEmpty(strDateTo) Then
            If Not IsDate(strDateTo) Then
                errors.Add("The date to must be a valid date")
            Else
                dtDateTo = valhelper.convertToDate(strDateTo)
            End If
        End If


        If String.IsNullOrEmpty(strTopic) Or strTopic = "" Then
            strTopic = Nothing
        End If
        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daSugg As New I_SUGGESTIONSTableAdapter
        Return daSugg.Search(strTopic, dtDateTo, dtDateFrom, intTeamID)

    End Function

End Class
