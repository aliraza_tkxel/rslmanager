Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports dalKeyMessages
Imports dalKeyMessagesTableAdapters

Public Class bllKeyMessages
    Private m_KeyMessageID As Integer
    Private m_KeyMessage As String
    Public Property KeyMessageID() As Integer
        Get
            Return m_KeyMessageID
        End Get
        Set(ByVal value As Integer)
            m_KeyMessageID = value
        End Set
    End Property

    Public Property KeyMessage() As String
        Get
            Return m_KeyMessage
        End Get
        Set(ByVal value As String)
            m_KeyMessage = value
        End Set
    End Property

    Public Function GetKeyMessages() As I_KEYMESSAGESDataTable
        Dim daMessage As New I_KEYMESSAGESTableAdapter
        Return daMessage.GetData
    End Function

    Public Sub New()

    End Sub

    Public Sub New(ByVal MessageID As Integer)
        Dim daMessage As New I_KEYMESSAGESTableAdapter
        Dim dtMessage As New I_KEYMESSAGESDataTable
        Dim drMessage As I_KEYMESSAGESRow

        dtMessage = daMessage.GetMessageByMessageID(MessageID)
        drMessage = dtMessage(0)
        m_KeyMessageID = drMessage.KeyMessageID
        m_KeyMessage = drMessage.KeyMessageText

    End Sub
    Public Function GetRandom() As String
        Dim daMessage As New I_KEYMESSAGESTableAdapter
        Dim dtMessage As New I_KEYMESSAGESDataTable
        Dim drMessage As I_KEYMESSAGESRow

        dtMessage = daMessage.GetRandom
        If dtMessage.Rows.Count > 0 Then
            drMessage = dtMessage(0)
            Return drMessage.KeyMessageText
        Else
            Return "Broadland Intranet"
        End If

    End Function
    Public Function Update(ByVal MessageID As Integer) As Integer


        Dim errors As New ErrorCollection


        If String.IsNullOrEmpty(m_KeyMessage) Then
            errors.Add("You must add a message")
        End If

        'added 68 because the text editor add 8 characters before the first character
        If Trim(m_KeyMessage.Length) > 68 Then
            errors.Add("You cannot add a Message of more than 60 characters")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daMessage As New I_KEYMESSAGESTableAdapter
        Dim dtMessage As New I_KEYMESSAGESDataTable
        Dim drMessage As I_KEYMESSAGESRow

        If MessageID = 0 Then
            drMessage = dtMessage.NewI_KEYMESSAGESRow
        Else
            dtMessage = daMessage.GetMessageByMessageID(MessageID)
            drMessage = dtMessage(0)
        End If

        drMessage.KeyMessageText = m_KeyMessage

        If MessageID = 0 Then
            dtMessage.AddI_KEYMESSAGESRow(drMessage)
        End If

        Return daMessage.Update(dtMessage)
    End Function


    Public Function Delete(ByVal MessageID As Integer) As Integer
        Dim daMessage As New I_KEYMESSAGESTableAdapter
        Dim dtMessage As New I_KEYMESSAGESDataTable
        Dim drMessage As I_KEYMESSAGESRow

        dtMessage = daMessage.GetMessageByMessageID(MessageID)
        drMessage = dtMessage(0)
        drMessage.Delete()
        Return daMessage.Update(dtMessage)
    End Function
End Class
