Imports Microsoft.VisualBasic
Imports System.Data
Imports dalPosition
Imports dalPositionTableAdapters
Imports dalMenu
Imports dalMenuTableAdapters
Imports dalPage
Imports dalPageTableAdapters
Imports CustomControls

Public Class bllMenu

    Private m_MenuID As Integer
    Private m_PositionID As Integer
    Private m_ParentID As Integer
    Private m_Active As Boolean
    Private m_MenuTitle As String
    Private m_Depth As Integer

    Private m_DataFieldID As String
    Private m_DataFieldParentID As String
    Private m_DataTextField As String
    Private m_expanddepth As Integer
    Private m_webposition As Integer
    Private m_showcheckboxes As Boolean
    Private m_AccessControl As Boolean = False
    Private strMenu As String
    Private m_MenuType As MenuType
    Private m_SelectedPage As Integer = 0
    Private m_SelectedUser As Integer = 0

    Private m_BespokePath As String = HttpContext.Current.GetSection("web_manager")("bespoke_path")
    Private m_Path As String = HttpContext.Current.GetSection("web_manager")("template_driven_path")


    Public Enum MenuType
        Menu = 1
        Page = 2
        RankPage = 3
        RankFolder = 4
    End Enum

    Public Enum NodeAction
        Edit = 1
        Create = 2
        Rank = 3
    End Enum

    Public Property MenuID() As Integer
        Get
            Return m_MenuID
        End Get
        Set(ByVal value As Integer)
            m_MenuID = value
        End Set
    End Property

    Public Property PositionID() As Integer
        Get
            Return m_PositionID
        End Get
        Set(ByVal value As Integer)
            m_PositionID = value
        End Set
    End Property

    Public Property MenuTitle() As String
        Get
            Return m_MenuTitle
        End Get
        Set(ByVal value As String)
            m_MenuTitle = value
        End Set
    End Property

    Public Property ParentID() As Integer
        Get
            Return m_ParentID
        End Get
        Set(ByVal value As Integer)
            m_ParentID = value
        End Set
    End Property

    Public Property Active() As Boolean
        Get
            Return m_Active
        End Get
        Set(ByVal value As Boolean)
            m_Active = value
        End Set
    End Property


    Public Property DataFieldID() As String
        Get
            Return m_DataFieldID
        End Get
        Set(ByVal Value As String)
            m_DataFieldID = Value
        End Set
    End Property

    Public Property DataFieldParentID() As String
        Get
            Return m_DataFieldParentID
        End Get
        Set(ByVal Value As String)
            m_DataFieldParentID = Value
        End Set
    End Property

    Public Property DataTextField() As String
        Get
            Return m_DataTextField
        End Get
        Set(ByVal Value As String)
            m_DataTextField = Value
        End Set
    End Property

    Public Property ExpandDepth() As Integer
        Get
            Return m_expanddepth
        End Get
        Set(ByVal Value As Integer)
            m_expanddepth = Value
        End Set
    End Property

    Public Property SelectedPage() As Integer
        Get
            Return m_SelectedPage
        End Get
        Set(ByVal value As Integer)
            m_SelectedPage = value
        End Set
    End Property

    Public Property Depth() As Integer
        Get
            Return m_Depth
        End Get
        Set(ByVal value As Integer)
            m_Depth = value
        End Set
    End Property
    Public Sub PopulateAccessControl(ByVal tvw As MenuTreeview, ByVal checkboxmode As Boolean, ByVal EmpID As Integer)
        m_AccessControl = True
        m_SelectedUser = EmpID
        PopulateMenu(tvw, False, 1)
    End Sub

    Public Sub PopulateMenu(ByVal tvw As MenuTreeview, ByVal checkboxmode As Boolean, ByVal WebsiteID As Integer)


        m_showcheckboxes = checkboxmode
        tvw.Nodes.Clear()

        Dim daPosition As New I_POSITIONTableAdapter
        Dim dtPosition As New I_POSITIONDataTable
        dtPosition = daPosition.GetPositionByWebsiteID(WebsiteID, m_SelectedPage)

        Dim drPosition As I_POSITIONRow




        For Each drPosition In dtPosition
            Dim oNode As New MenuNode
            oNode.Text = drPosition.Position
            oNode.Value = drPosition("TOPMENUID")
            m_webposition = drPosition.PositionID
            oNode.ImageUrl = "/BHAIntranet/Images/WebManager/folder.gif"
            oNode.NavigateUrl = "javascript:alert('You cannot edit a root node')"

            If m_showcheckboxes = True And m_webposition <> 1 Then
                oNode.ShowCheckBox = True
                If drPosition("SELECTED") > 0 Then
                    oNode.Checked = True
                End If
            End If
            tvw.Nodes.Add(oNode)

            Dim dalMenu As New I_MENUTableAdapter
            Dim dtMenu As New I_MENUDataTable
            dtMenu = dalMenu.GetMenuByPositionID(drPosition.PositionID, m_SelectedPage)

            Dim dapage As New I_PAGETableAdapter
            Dim dtpage As New I_PAGEDataTable


            dtpage = dapage.GetPagesByPositionID(drPosition.PositionID, m_SelectedUser)
            'HttpContext.Current.Response.Write(m_SelectedUser)
            Dim otable As New DataTable
            otable = dalMenu.GetMenuByPositionID(drPosition.PositionID, m_SelectedPage)

            Dim ods As DataSet = New DataSet()
            ods.Tables.Add(otable.Copy())
            ods.Tables.Add(dtpage)

            'create a relation between the id column and parent column
            If ods.Relations.Contains("selfrefencerelation") = False Then
                ods.Relations.Add("selfrefencerelation", _
                    ods.Tables(0).Columns(m_DataFieldID), _
                    ods.Tables(0).Columns(m_DataFieldParentID))
            End If

            ods.Relations.Add("childpages", _
                    ods.Tables(0).Columns("menuid"), _
                    ods.Tables(1).Columns("menuid"))

            otable.Dispose()
            otable = Nothing

            'viewstate("treedata") = ods
            LoadTreeView(ods, oNode)

            ods.Dispose()
            ods = Nothing







        Next



    End Sub


    Private Sub LoadTreeView(ByVal oDS As DataSet, ByVal ParentNode As MenuNode)

        Dim oDataRow As DataRow
        For Each oDataRow In oDS.Tables(0).Rows
            'Find Root Node,A root node has ParentID NULL
            If oDataRow.IsNull(m_DataFieldParentID) Then
                'Create Parent Node and add to tree
                Dim oNode As New MenuNode

                oNode.Text = oDataRow(m_DataTextField).ToString()
                oNode.Value = oDataRow(m_DataFieldID).ToString()
                oNode.NodeType = MenuType.Menu
                oNode.NodeAction = NodeAction.Edit
                oNode.ImageUrl = "/BHAIntranet/Images/WebManager/folder.gif"
                'ParentNode.ChildNodes.Add(oNode)

                RecursivelyLoadTree(oDataRow, ParentNode)


            End If
        Next oDataRow


        oDS.Dispose()
        oDS = Nothing
    End Sub
    Private intMaxdepth As Integer
    Private Sub RecursivelyLoadTree(ByVal oDataRow As DataRow, _
    ByRef oNode As MenuNode)
        Dim oChildRow As DataRow
        'returns an array of DataRow objects representing the child view
        Dim intSubFolder As Integer = 0
        Dim intRowCount As Integer = 0
        Dim strCurrentMenuID As String = oDataRow("menuid").ToString
        Dim strHomePageMenu As String = HttpContext.Current.GetSection("web_manager")("home_menu_id").ToString
        Dim strTopMenu As String = HttpContext.Current.GetSection("web_manager")("top_menuid")


        For Each oChildRow In oDataRow.GetChildRows("SelfRefenceRelation")
            'Create child node and add to Parent
            intSubFolder = intSubFolder + 1



            Dim oChildNode As New MenuNode()
            oChildNode.Text = oChildRow(DataTextField).ToString()
            oChildNode.Value = oChildRow(DataFieldID).ToString()
            oChildNode.NodeType = MenuType.Menu
            oChildNode.NodeAction = NodeAction.Edit

            If m_showcheckboxes = True Then
                oChildNode.ShowCheckBox = True
                oChildNode.NavigateUrl = "#"
                If oChildRow("selected") > 0 Then
                    oChildNode.Checked = True
                End If
            Else
                oChildNode.ShowCheckBox = False
                oChildNode.NavigateUrl = ""
            End If
            oChildNode.ImageUrl = "/BHAIntranet/Images/WebManager/folder.gif"

            oNode.ChildNodes.Add(oChildNode)
            'Repeat for each child
            RecursivelyLoadTree(oChildRow, oChildNode)
        Next oChildRow


        If m_showcheckboxes = False Then
            intRowCount = 0
            For Each oChildRow In oDataRow.GetChildRows("ChildPages")

                Dim oChildNode As New MenuNode()
                oChildNode.Text = oChildRow("Title").ToString()
                oChildNode.Value = oChildRow("pageid").ToString()
                If oChildRow("ismulti") = 1 Then
                    oChildNode.NavigateUrl = "javascript:alert('You cannot edit a multmenu item')"
                    oChildNode.ImageUrl = "/BHAIntranet/Images/WebManager/multi.gif"
                Else
                    oChildNode.NavigateUrl = ""
                    oChildNode.ImageUrl = "/BHAIntranet/Images/WebManager/file.gif"
                    If m_AccessControl = True Then
                        oChildNode.ShowCheckBox = True
                        If oChildRow("selected") = 1 Then
                            oChildNode.Checked = True
                        End If
                    End If

                End If


                oChildNode.NodeType = MenuType.Page
                oChildNode.NodeAction = NodeAction.Edit

                oNode.ChildNodes.Add(oChildNode)
                intRowCount = intRowCount + 1
            Next




            Dim tvwndAddNode As New MenuNode()
            Dim tvwndAddFolderNode As New MenuNode()
            Dim tvwndAddPageRankNode As New MenuNode()
            Dim tvwndAddRankFolderNode As New MenuNode()

            Dim strText As String
            strText = ""
            If Not IsNothing(oNode) Then
                strText = oNode.Text
            End If


            If m_AccessControl = False Then

                If intSubFolder = 0 Then
                    tvwndAddNode.Text = "Add Page to " & strText
                    tvwndAddNode.NodeType = MenuType.Page
                    tvwndAddNode.NodeAction = NodeAction.Create
                    tvwndAddNode.Value = "0"
                    tvwndAddNode.ImageUrl = "/BHAIntranet/Images/WebManager/file_new.gif"
                    oNode.ChildNodes.Add(tvwndAddNode)
                End If

                ' don't allow addition of new menus for footer or header
                ' allow for main menu and anything below
                If (oNode.Depth > 0 And oNode.Depth < 2) Or (oNode.Value = strTopMenu) Then
                    tvwndAddFolderNode.Text = "Add Menu to " & strText
                    tvwndAddFolderNode.NodeType = MenuType.Menu
                    tvwndAddFolderNode.NodeAction = NodeAction.Create
                    tvwndAddFolderNode.Value = "1"
                    tvwndAddFolderNode.ImageUrl = "/BHAIntranet/Images/WebManager/folder_new.gif"
                    oNode.ChildNodes.Add(tvwndAddFolderNode)
                End If

                tvwndAddPageRankNode.Text = "Rank Pages in " & strText
                tvwndAddPageRankNode.NodeType = MenuType.RankPage
                tvwndAddPageRankNode.NodeAction = NodeAction.Edit
                tvwndAddPageRankNode.Value = "2"
                tvwndAddPageRankNode.ImageUrl = "/BHAIntranet/Images/WebManager/rank_file.gif"
                oNode.ChildNodes.Add(tvwndAddPageRankNode)


                tvwndAddRankFolderNode.Text = "Rank Folders in " & strText
                tvwndAddRankFolderNode.NodeType = MenuType.RankFolder
                tvwndAddRankFolderNode.NodeAction = NodeAction.Edit
                tvwndAddRankFolderNode.Value = "3"
                tvwndAddRankFolderNode.ImageUrl = "/BHAIntranet/Images/WebManager/rank.gif"
                oNode.ChildNodes.Add(tvwndAddRankFolderNode)
            End If
        End If
    End Sub



    Public Function UpdateMenu(ByVal MenuID As Integer) As Integer


        Dim errors As New ErrorCollection


        If m_MenuTitle.Length >= 50 Then
            errors.Add("You cannot enter a Menu Title of more than 50 characters")
        End If

        If String.IsNullOrEmpty(m_MenuTitle) Then
            errors.Add("You must enter a Menu Title")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If


        Dim daMenu As New I_MENUTableAdapter
        Dim dtMenu As New I_MENUDataTable
        Dim drMenu As I_MENURow


        If MenuID <= 0 Then
            drMenu = dtMenu.NewI_MENURow
        Else
            dtMenu = daMenu.GetMenuByMenuID(MenuID)
            drMenu = dtMenu(0)
        End If

        drMenu.MenuTitle = m_MenuTitle
        drMenu.ParentID = m_ParentID
        drMenu.Active = m_Active
        drMenu.Depth = m_Depth
        If MenuID <= 0 Then
            dtMenu.AddI_MENURow(drMenu)
        End If

        Dim intReturnValue As Integer = daMenu.Update(dtMenu)
        daMenu.SetPositionIDByMenuID(drMenu.MenuID)


        Return intReturnValue

    End Function

    Public Function DeleteMenu(ByVal MenuID As Integer) As Integer
        Dim errors As New ErrorCollection
        Dim daMenu As New I_MENUTableAdapter
        Dim dtMenu As New I_MENUDataTable
        Dim drMenu As I_MENURow
        Dim intCount As Integer

        daMenu.CheckDelete(MenuID, intCount)

        If intCount > 0 Then
            errors.Add("You cannot delete a menu when it contains pages or sub folders")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        dtMenu = daMenu.GetMenuByMenuID(MenuID)
        drMenu = dtMenu(0)
        drMenu.Delete()

        Return daMenu.Update(dtMenu)


    End Function


    Sub New(ByVal MenuID As Integer)
        Dim daMenu As New I_MENUTableAdapter
        Dim dtMenu As New I_MENUDataTable
        Dim drMenu As I_MENURow

        dtMenu = daMenu.GetMenuByMenuID(MenuID)
        drMenu = dtMenu(0)

        m_MenuID = drMenu.MenuID
        m_PositionID = drMenu.PositionID
        m_Active = drMenu.Active

        If drMenu.IsMenuTitleNull Then
            m_MenuTitle = ""
        Else
            m_MenuTitle = drMenu.MenuTitle
        End If

        If drMenu.IsParentIDNull Then
            m_ParentID = 0
        Else
            m_ParentID = drMenu.ParentID
        End If



    End Sub

    Sub New()

    End Sub


    Public Function GetTopLevelMenu() As I_MENUDataTable
        Dim intHomePageMenu As Integer = CInt(HttpContext.Current.GetSection("web_manager")("top_menuid"))
        Return GetMenuByParentID(intHomePageMenu)
    End Function



    Public Function GetMenuByParentID(ByVal ParentID As Integer) As I_MENUDataTable

        Dim daMenu As New I_MENUTableAdapter
        Dim dtMenu As New I_MENUDataTable
        dtMenu = daMenu.GetMenuByParentID(ParentID, m_BespokePath, m_Path)
        Return dtMenu
    End Function


    Public Function GetMenusByParentID(ByVal ParentID As Integer) As I_MENUDataTable

        Dim daMenu As New I_MENUTableAdapter
        Return daMenu.GetMenusByParentID(ParentID)

    End Function

    'Use this for Home Page
    Public Function GetTopLevelMenuByEmpID(ByVal EmpID As Integer) As I_MENUDataTable
        Dim intHomePageMenu As Integer = CInt(HttpContext.Current.GetSection("web_manager")("top_menuid"))
        Return GetMenuByParentIDAndEmpID(intHomePageMenu, EmpID)
    End Function

    Public Function GetMenuByParentIDAndEmpID(ByVal ParentID As Integer, ByVal EmpID As Integer) As I_MENUDataTable

        Dim daMenu As New I_MENUTableAdapter
        Dim dtMenu As New I_MENUDataTable
        dtMenu = daMenu.GetChildMenusByParentIDAndEmpID(ParentID, EmpID)
        Return dtMenu
    End Function








    Public Sub SetMenuByRank(ByVal lstRank As ListBox)
        Dim daMenu As New I_MENUTableAdapter

        Dim item As ListItem
        Dim i As Integer = 0
        For Each item In lstRank.Items
            daMenu.UpdateRankByMenuID(item.Value, i)
            i = i + 1
        Next

    End Sub

End Class
