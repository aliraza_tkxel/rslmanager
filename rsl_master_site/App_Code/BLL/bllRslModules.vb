Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports dalRSLModules


Public Class bllRSLModules

    Private m_AlertName As String
    Private m_SOrder As Integer


    Public Function GetRSLModules(ByVal sEmpID As String, ByVal bSortASC As Integer) As I_RSLMODULES_GETMODULELISTDataTable
        Dim daRSLModules As New dalRSLModulesTableAdapters.I_RSLMODULES_GETMODULELISTTableAdapter

        Return daRSLModules.GetData(Convert.ToInt32(sEmpID), bSortASC)
    End Function

    Public Function GetRSLMenus(ByVal sEmpID As String, ByVal iModuleId As Integer) As AC_MENULISTDataTable
        Dim daRSLModules As New dalRSLModulesTableAdapters.AC_MENULISTTableAdapter

        Return daRSLModules.GetData(Convert.ToInt32(sEmpID), iModuleId)
    End Function

    Public Function GetRSLPages(ByVal sEmpID As String, ByVal iModuleId As Integer) As AC_PAGELISTDataTable
        Dim daRSLModules As New dalRSLModulesTableAdapters.AC_PAGELISTTableAdapter

        Return daRSLModules.GetData(Convert.ToInt32(sEmpID), iModuleId)
    End Function

End Class
