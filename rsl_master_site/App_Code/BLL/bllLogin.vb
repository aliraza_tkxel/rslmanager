Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Public Class bllLogin
    Inherits bllBaseLogin

    Public Sub SetSession(ByVal objSession As MSDN.mySession)

        Dim isContractor As Integer
        Dim strTeamCode As String
        Dim rows As Data.DataRowCollection
        rows = dsLogin.Tables(0).Rows()

        objSession("FirstName") = rows(0).Item("FIRSTNAME")
        objSession("LastName") = rows(0).Item("LASTNAME")
        objSession("USERID") = rows(0).Item("EMPLOYEEID")
        objSession("TeamCode") = rows(0).Item("TeamCode")

        strTeamCode = rows(0).Item("TEAMCODE")
        objSession("TEAMNAME") = rows(0).Item("TEAMNAME")


        If Not IsDBNull(rows(0).Item("ORGID")) Then
            isContractor = rows(0).Item("ORGID")
        End If

        If Not IsDBNull(rows(0).Item("ORGID")) Then
            objSession("ORGID") = isContractor
            HttpContext.Current.Response.Redirect("/MyWeb/MyWhiteboard.asp")
        Else
            If strTeamCode = "PMA" Then
                'Response.write("board" & strTeamCode & "/BoardMembers/BM.asp")
                HttpContext.Current.Response.Redirect("/BoardMembers/BM.asp")
            Else
                'Response.write("standard" & strTeamCode)
                HttpContext.Current.Response.Redirect("/MyWeb/MyWhiteboard.asp")
            End If
        End If

    End Sub
End Class
