Imports Microsoft.VisualBasic
Imports dalJobs

Imports System.Data
Imports System.Data.SqlClient
Public Class bllJobs

#Region "Properties"
    Private _iTeamId As Integer
    Private _strDesc As String
    Private _iTypeId As Integer
    Private _strOtherType As String
    Private _strTitle As String
    Private _strRef As String
    Private _dtDatePosted As Date
    Private _iLocationId As Integer
    Private _strDuration As String
    Private _strStartDate As String
    Private _dtClosingDate As Date
    Private _strSalary As String
    Private _strContactName As String
    Private _iApplicationId As Integer
    Private _iJobId As Integer

    Public Property TeamId() As Integer
        Get
            Return _iTeamId
        End Get
        Set(ByVal value As Integer)
            _iTeamId = value
        End Set
    End Property

    Public Property TypeId() As Integer
        Get
            Return _iTypeId
        End Get
        Set(ByVal value As Integer)
            _iTypeId = value
        End Set
    End Property

    Public Property OtherJobType() As String
        Get
            Return _strOtherType
        End Get
        Set(ByVal value As String)
            _strOtherType = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return _strTitle
        End Get
        Set(ByVal value As String)
            _strTitle = value
        End Set
    End Property

    Public Property Desc() As String
        Get
            Return _strDesc
        End Get
        Set(ByVal value As String)
            _strDesc = value
        End Set
    End Property

    Public Property Ref() As String
        Get
            Return _strRef
        End Get
        Set(ByVal value As String)
            _strRef = value
        End Set
    End Property

    Public Property ContactName() As String
        Get
            Return _strContactName
        End Get
        Set(ByVal value As String)
            _strContactName = value
        End Set
    End Property

    Public Property Salary() As String
        Get
            Return _strSalary
        End Get
        Set(ByVal value As String)
            _strSalary = value
        End Set
    End Property

    Public Property Location() As Integer
        Get
            Return _iLocationId
        End Get
        Set(ByVal value As Integer)
            _iLocationId = value
        End Set
    End Property

    Public Property Duration() As String
        Get
            Return _strDuration
        End Get
        Set(ByVal value As String)
            _strDuration = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return _strStartDate
        End Get
        Set(ByVal value As String)
            _strStartDate = value
        End Set
    End Property

    Public Property ApplicationId() As Integer
        Get
            Return _iApplicationId
        End Get
        Set(ByVal value As Integer)
            _iApplicationId = value
        End Set
    End Property

    Public Property ClosingDate() As Date
        Get
            Return _dtClosingDate
        End Get
        Set(ByVal value As Date)
            _dtClosingDate = value
        End Set
    End Property

    Public Property JobId() As Integer
        Get
            Return _iJobId
        End Get
        Set(ByVal value As Integer)
            _iJobId = value
        End Set
    End Property
#End Region

    Public Sub New()

    End Sub

    Public Sub New(ByVal iJobId As Integer)
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Dim dt As dalJobs.I_JOBDataTable = da.GetJobById(iJobId)
        Dim dr As dalJobs.I_JOBRow
        If dt.Rows.Count > 0 Then
            dr = dt.Rows(0)

            _iTeamId = dr.TeamId
            _strDesc = dr.Description
            _iTypeId = dr.TypeId
            _strOtherType = dr.OtherType
            _strTitle = dr.Title
            _strRef = dr.Ref
            _dtDatePosted = dr.DatePosted
            _iLocationId = dr.LocationId
            _strDuration = dr.Duration
            _strStartDate = dr.StartDate
            _strSalary = dr.Salary
            _strContactName = dr.ContactName
            _dtClosingDate = dr.ClosingDate
            _iJobId = dr.JobId
            _iApplicationId = dr.ApplicationId
        End If
    End Sub



    Public Function GetJobById(ByVal iJobId As Integer) As dalJobs.I_JOBDataTable
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Return da.GetJobById(iJobId)
    End Function

    Public Function GetJobTypes() As I_JOB_TYPEDataTable
        Dim da As New dalJobsTableAdapters.I_JOB_TYPETableAdapter
        Return da.GetData()
    End Function

    Public Function GetAllJobs(ByVal bShowActive As Boolean) As I_JOBDataTable
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Return da.GetAllJobs(IIf(bShowActive, 1, 0))
    End Function

    Public Function GetJobsAsPagedDataSource(ByVal iTeamId As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal bShowActive As Boolean) As PagedDataSource
        Dim pagedData As New PagedDataSource
        If iTeamId > 0 Then
            pagedData.DataSource = GetJobsByTeamId(iTeamId, bShowActive).Rows
        Else
            pagedData.DataSource = GetAllJobs(bShowActive).Rows
        End If

        pagedData.AllowPaging = True
        pagedData.CurrentPageIndex = pageIndex
        pagedData.PageSize = pageSize

        Return pagedData
    End Function

    Public Function GetSearchedJobsPageDataSource(ByVal iTeamId As Integer, ByVal strTitle As String, ByVal iLocId As Nullable(Of Integer), ByVal iPostedIn As Nullable(Of Integer), ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal bShowActive As Boolean) As PagedDataSource
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Dim pagedData As New PagedDataSource

        pagedData.DataSource = da.GetSearchedJobs(strTitle, iLocId, iPostedIn, IIf(bShowActive, 1, 0)).Rows

        pagedData.AllowPaging = True
        pagedData.CurrentPageIndex = pageIndex
        pagedData.PageSize = pageSize

        Return pagedData
    End Function

    Public Function GetShortListAsPagedDataSource(ByVal iEmpId As Integer, ByVal pageIndex As Integer, ByVal pageSize As Integer) As PagedDataSource
        Dim pagedData As New PagedDataSource

        pagedData.DataSource = GetShortListByEmpId(iEmpId).Rows

        pagedData.AllowPaging = True
        pagedData.CurrentPageIndex = pageIndex
        pagedData.PageSize = pageSize

        Return pagedData
    End Function

    Public Function GetJobsByTeamId(ByVal iTeamId As Integer, ByVal bShowActive As Boolean) As I_JOBDataTable
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Return da.GetJobsByTeamId(iTeamId, IIf(bShowActive, 1, 0))
    End Function

    Public Function GetShortListByEmpId(ByVal iEmpId As Integer) As I_JOBDataTable
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        Return da.GetShortListByEmpId(iEmpId)
    End Function

    Public Sub PostJob()
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        da.Insert(TeamId, Desc, TypeId, OtherJobType, Title, Ref, Location, Duration, StartDate, ClosingDate, Salary, ContactName, ApplicationId)
    End Sub

    Public Sub AddToShortList(ByVal iEmpId As Integer, ByVal iJobId As Integer)
        Dim da As New dalJobsTableAdapters.I_JOB_SHORTLISTTableAdapter
        da.AddToShortList(iEmpId, iJobId)
    End Sub

    Public Sub RemoveFromShortList(ByVal iEmpId As Integer, ByVal iJobId As Integer)
        Dim da As New dalJobsTableAdapters.I_JOB_SHORTLISTTableAdapter
        da.RemoveFromShortList(iEmpId, iJobId)
    End Sub

    Public Sub SetActive(ByVal iJobId As Integer, ByVal bActive As Boolean)
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        da.SetActive(iJobId, IIf(bActive, 1, 0))
    End Sub

    Public Sub Update()
        Dim da As New dalJobsTableAdapters.I_JOBTableAdapter
        da.Update(TeamId, Desc, TypeId, OtherJobType, Title, Ref, Location, Duration, StartDate, Salary, ContactName, ApplicationId, ClosingDate, JobId)
    End Sub
End Class

Public Class bllJobsAccess

    Public Function UpdateOrderItemsWithTransaction(ByVal dtJobAccess As dalJobs.I_JOB_ACCESSDataTable) As Integer
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESSTableAdapter
        Return da.UpdateWithTransaction(dtJobAccess)
    End Function

    Public Function GetData() As dalJobs.I_JOB_ACCESSDataTable
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESSTableAdapter
        Return da.GetData()
    End Function

    Public Sub Clear()

        Using sqlconn As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            sqlconn.Open()

            Dim sqlcmd As New SqlCommand
            sqlcmd.Connection = sqlconn
            sqlcmd.CommandText = "I_JOB_ACCESS_TRUNCATE"
            sqlcmd.CommandType = CommandType.StoredProcedure
            sqlcmd.ExecuteNonQuery()

        End Using

    End Sub

    Public Function GetAccessRight(ByVal dt As dalJobs.I_JOB_ACCESSDataTable, ByVal iEmpId As Integer) As dalJobs.I_JOB_ACCESSRow
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESSTableAdapter
        Dim dr As dalJobs.I_JOB_ACCESSRow
        dt = da.GetById(iEmpId)
        If dt.Rows.Count > 0 Then
            dr = dt.Rows(0)
            Return dr
        Else
            Return Nothing
        End If
    End Function

    Public Function HasAccessRight(ByVal iEmpId As Integer) As Boolean
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESSTableAdapter
        Dim dt As New dalJobs.I_JOB_ACCESSDataTable

        dt = da.GetById(iEmpId)
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetTeams(ByVal iPageId As Integer) As dalJobs.I_JOB_ACCESS_TEAMSDataTable
        Dim da As New dalJobsTableAdapters.I_JOB_ACCESS_TEAMSTableAdapter
        Return da.GetData(iPageId)
    End Function

End Class

Public Class bllJobForms
    Private _sFormName As String

    Public ReadOnly Property FormName() As String
        Get
            Return _sFormName
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal iApplicationId As Integer)
        GetFormById(iApplicationId)
    End Sub

    Public Sub InsertBlankForm()
        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            sqlCon.Open()
            Dim strCmd As String = "I_JOB_FORMS_INSERT"
            Dim sqlCmd As New SqlCommand(strCmd, sqlCon)
            sqlCmd.CommandType = CommandType.Text
            sqlCmd.ExecuteNonQuery()

        End Using
    End Sub

    Public Function GetData() As DataTable
        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            sqlCon.Open()
            Dim strCmd As String = "I_JOB_FORMS_SELECT"
            Dim sqlCmd As New SqlCommand(strCmd, sqlCon)
            sqlCmd.CommandType = CommandType.StoredProcedure

            Dim dt As New DataTable
            dt.Load(sqlCmd.ExecuteReader())
            Return dt

        End Using

    End Function

    Public Sub DeleteForm(ByVal ApplicationId As Integer)
        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            sqlCon.Open()
            Dim strCmd As String = "I_JOB_FORMS_DELETE"
            Dim sqlCmd As New SqlCommand(strCmd, sqlCon)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.AddWithValue("ApplicationId", ApplicationId)
            sqlCmd.ExecuteNonQuery()
        End Using
    End Sub

    Public Sub UpdateForm(ByVal ApplicationId As Integer, ByVal Name As String, ByVal FormData As Byte())
        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            sqlCon.Open()
            Dim strCmd As String = "I_JOB_FORMS_UPDATE"

            Dim sqlCmd As New SqlCommand(strCmd, sqlCon)
            sqlCmd.CommandType = CommandType.StoredProcedure
            Dim spId As New SqlParameter("ApplicationId", SqlDbType.Int)
            spId.Value = ApplicationId
            sqlCmd.Parameters.Add(spId)

            Dim spName As New SqlParameter("Name", SqlDbType.NVarChar)
            spName.Value = Name
            sqlCmd.Parameters.Add(spName)

            sqlCmd.ExecuteNonQuery()
        End Using

    End Sub

    Public Sub GetFormById(ByVal iApplicationId As Integer)
        Using sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("connRSL").ConnectionString)

            Try
                sqlCon.Open()
                Dim strCmd As String = "I_JOB_FORMS_SELECTBYID"

                Dim sqlCmd As New SqlCommand(strCmd, sqlCon)
                sqlCmd.CommandType = CommandType.StoredProcedure
                Dim spId As New SqlParameter("ApplicationId", SqlDbType.Int)
                spId.Value = iApplicationId
                sqlCmd.Parameters.Add(spId)

                Dim sqr As SqlDataReader
                sqr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection)
                While sqr.Read
                    _sFormName = sqr("Name")
                End While
                sqr.Close()

            Catch ex As Exception

            End Try

        End Using
    End Sub
End Class