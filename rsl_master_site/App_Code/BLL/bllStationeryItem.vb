Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports dalStationery
Imports dalStationeryTableAdapters

Public Class bllStationeryItem

    Private mParentOrderID As Integer
    Private mOrderItemID As Integer
    Private mItemNo As String
    Private mPageNo As String
    Private mItemDesc As String
    Private mQuantity As Integer
    Private mSize As String
    Private mColour As String
    Private mCost As Decimal
    Private mPriorityId As Integer
    Private mStatusId As Integer
    Private mStatus As String
    Private mTotalCost As Decimal

    ' These should be the same as in the DB I_STATIONERY_ORDER_STATUS & I_STATIONERY_ORDER_ITEM_STATUS
    Public Enum OrderItemStatus
        Saved = 1
        Requested
        Ordered
        Rejected
        Received
    End Enum

    Public Function GetById(ByVal OrderId As Integer) As I_STATIONERY_ORDER_ITEMRow
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        Dim dt As New I_STATIONERY_ORDER_ITEMDataTable

        dt = da.GetItemById(OrderId)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function GetItemsByOrderID(ByVal iOrderID As Integer) As I_STATIONERY_ORDER_ITEMDataTable
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        Dim dt As New I_STATIONERY_ORDER_ITEMDataTable

        If iOrderID > 0 Then
            dt = da.GetItemsByOrderID(iOrderID)
            Return dt
        Else
            Return Nothing
        End If
    End Function

    Public Sub SetItemsRequested(ByVal iOrderID As Integer)
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        da.SetAllRequested(iOrderID)
    End Sub

    Public Sub Delete(ByVal iOrderItemId As Integer)
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        da.Delete(iOrderItemId)
    End Sub

    Public Property ParentOrderItemID() As Integer
        Get
            Return mParentOrderID
        End Get
        Set(ByVal value As Integer)
            mParentOrderID = value
        End Set
    End Property

    Public Property PageNo() As String
        Get
            Return mPageNo
        End Get
        Set(ByVal value As String)
            mPageNo = value
        End Set
    End Property

    Public Property OrderItemID() As Integer
        Get
            Return mOrderItemID
        End Get
        Set(ByVal value As Integer)
            mOrderItemID = value
        End Set
    End Property

    Public Property ItemNo() As String
        Get
            Return mItemNo
        End Get
        Set(ByVal value As String)
            mItemNo = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Description = mItemDesc
        End Get
        Set(ByVal value As String)
            mItemDesc = value
        End Set
    End Property

    Public Property Colour() As String
        Get
            Colour = mColour
        End Get
        Set(ByVal value As String)
            mColour = value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Quantity = mQuantity
        End Get
        Set(ByVal value As Integer)
            mQuantity = value
        End Set
    End Property

    Public Property Size() As String
        Get
            Size = mSize
        End Get
        Set(ByVal value As String)
            mSize = value
        End Set
    End Property

    Public Property Cost() As Decimal
        Get
            Cost = mCost
        End Get
        Set(ByVal value As Decimal)
            mCost = value
        End Set
    End Property

    Public Property PriorityId() As Integer
        Get
            PriorityId = mPriorityId
        End Get
        Set(ByVal value As Integer)
            mPriorityId = value
        End Set
    End Property

    Public Property StatusId() As Integer
        Get
            StatusId = mStatusId
        End Get
        Set(ByVal value As Integer)
            mStatusId = value
        End Set
    End Property

    Public ReadOnly Property Status() As String
        Get
            Status = mStatus
        End Get
    End Property

    Public ReadOnly Property TotalCost() As Decimal
        Get
            TotalCost = mTotalCost
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal iorderID As Integer)
        Dim taStationeryOrderItem As New I_STATIONERY_ORDER_ITEMTableAdapter
        Dim drStationeryOrderItem As I_STATIONERY_ORDER_ITEMRow
        Dim dtStationeryOrderItem As I_STATIONERY_ORDER_ITEMDataTable
        dtStationeryOrderItem = taStationeryOrderItem.GetItemById(iorderID)

        If dtStationeryOrderItem.Rows.Count > 0 Then
            drStationeryOrderItem = dtStationeryOrderItem.Rows(0)

            If drStationeryOrderItem IsNot Nothing Then
                With taStationeryOrderItem
                    mParentOrderID = drStationeryOrderItem.OrderId
                    mOrderItemID = drStationeryOrderItem.OrderItemId
                    mItemNo = drStationeryOrderItem.ItemNo
                    mPageNo = drStationeryOrderItem.PageNo
                    mItemDesc = drStationeryOrderItem.ItemDesc
                    mQuantity = drStationeryOrderItem.Quantity
                    mSize = drStationeryOrderItem.Size
                    mColour = drStationeryOrderItem.Colour
                    mCost = drStationeryOrderItem.Cost
                    mPriorityId = drStationeryOrderItem.PriorityId
                    mStatusId = drStationeryOrderItem.StatusId
                    mTotalCost = drStationeryOrderItem.TOTALCOST
                    mStatus = drStationeryOrderItem.OrderStatus
                End With
            End If
        End If
    End Sub

    Public Sub Update()
        Dim taStationeryOrderItem As New I_STATIONERY_ORDER_ITEMTableAdapter
        Dim taStationeryOrder As New I_STATIONERY_ORDERTableAdapter

        Try
            taStationeryOrderItem.Update(mParentOrderID, mPageNo, mItemNo, mItemDesc, mQuantity, mSize, mColour, mCost, mPriorityId, mStatusId, mOrderItemID)

            ' here we ensure that any update to the status of the order item propegates
            ' to the parent order status which has slightly different rules
            ' 1. If only 1 Order Item is set to Ordered then Order status is IN PROGRESS
            ' 2. If All Order Items are either Rejected or Received then Order status is COMPLETE
            Select Case mStatusId
                Case OrderItemStatus.Saved
                    ' saved by user

                Case OrderItemStatus.Requested
                    ' set on submission by user

                Case OrderItemStatus.Ordered
                    Dim bllStationeryOrder As New bllStationery
                    bllStationeryOrder.SetOrderInProgress(mParentOrderID)

                Case OrderItemStatus.Rejected, OrderItemStatus.Received
                    RefreshParentStatus(mParentOrderID)
            End Select

        Catch ex As Exception

        End Try
    End Sub

    Public Sub RefreshParentStatus(ByVal iOrderID As Integer)
        Dim bllStationeryOrder As New bllStationery
        bllStationeryOrder.RefreshOrderStatus(iOrderID)
    End Sub

    Public Sub Create()
        Dim taStationeryOrderItem As New I_STATIONERY_ORDER_ITEMTableAdapter
        taStationeryOrderItem.Insert(mParentOrderID, mPageNo, mItemNo, mItemDesc, mQuantity, mSize, mColour, mCost, mPriorityId, mStatusId)
    End Sub

    Public Function UpdateOrderItemsWithTransaction(ByVal dtOrderItems As dalStationery.I_STATIONERY_ORDER_ITEMDataTable) As Integer
        Dim da As New dalStationeryTableAdapters.I_STATIONERY_ORDER_ITEMTableAdapter
        Return da.UpdateWithTransaction(dtOrderItems)
    End Function

    Public Function CalcTotals(ByVal bIncRejected As Boolean, _
        ByVal dtDateReqTo As Nullable(Of DateTime), ByVal dtDateReqFrom As Nullable(Of DateTime), ByVal iOrderStatus As Nullable(Of Integer), _
        ByVal iTeamId As Nullable(Of Integer), ByVal iEmpId As Nullable(Of Integer), ByVal dcLessThan As Nullable(Of Decimal), ByVal dcGreaterThan As Nullable(Of Decimal)) As Decimal

        Dim dcTotal As Decimal
        Dim sqlCmd As New SqlCommand
        Dim con As New SqlConnection

        con.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
        If bIncRejected Then
            ' if we are to include rejected items in our report then call
            ' the appropriate Sproc
            sqlCmd.CommandText = "I_STATIONERY_ORDERITEM_SUM_FILTERED"
        Else
            sqlCmd.CommandText = "I_STATIONERY_ORDERITEM_SUM_FILTERED_EXCREJECTED"
        End If
        sqlCmd.CommandType = Data.CommandType.StoredProcedure


        With sqlCmd.Parameters
            Dim sqlParam1 As New SqlParameter
            sqlParam1.DbType = Data.DbType.DateTime
            sqlParam1.Direction = Data.ParameterDirection.Input
            sqlParam1.IsNullable = True
            sqlParam1.ParameterName = "DATEREQUESTEDFROM"
            sqlParam1.Value = IIf(dtDateReqFrom.HasValue, dtDateReqFrom, DBNull.Value)
            .Add(sqlParam1)

            Dim sqlParam2 As New SqlParameter
            sqlParam2.DbType = Data.DbType.DateTime
            sqlParam2.Direction = Data.ParameterDirection.Input
            sqlParam2.IsNullable = True
            sqlParam2.ParameterName = "DATEREQUESTEDTO"
            sqlParam2.Value = IIf(dtDateReqTo.HasValue, dtDateReqTo, DBNull.Value)
            .Add(sqlParam2)

            Dim sqlParam3 As New SqlParameter
            sqlParam3.DbType = Data.DbType.Int32
            sqlParam3.Direction = Data.ParameterDirection.Input
            sqlParam3.IsNullable = True
            sqlParam3.ParameterName = "ITEMSTATUS"
            sqlParam3.Value = IIf(iOrderStatus.HasValue, iOrderStatus, DBNull.Value)
            .Add(sqlParam3)

            Dim sqlParam4 As New SqlParameter
            sqlParam4.DbType = Data.DbType.Int32
            sqlParam4.Direction = Data.ParameterDirection.Input
            sqlParam4.IsNullable = True
            sqlParam4.ParameterName = "TEAM"
            sqlParam4.Value = IIf(iTeamId.HasValue, iTeamId, DBNull.Value)
            .Add(sqlParam4)

            Dim sqlParam5 As New SqlParameter
            sqlParam5.DbType = Data.DbType.Int32
            sqlParam5.Direction = Data.ParameterDirection.Input
            sqlParam5.IsNullable = True
            sqlParam5.ParameterName = "EMPLOYEE"
            sqlParam5.Value = IIf(iEmpId.HasValue, iEmpId, DBNull.Value)
            .Add(sqlParam5)

            Dim sqlParam6 As New SqlParameter
            sqlParam6.DbType = Data.DbType.Decimal
            sqlParam6.Direction = Data.ParameterDirection.Input
            sqlParam6.IsNullable = True
            sqlParam6.ParameterName = "LESSTHAN"
            sqlParam6.Value = IIf(dcLessThan.HasValue, dcLessThan, DBNull.Value)
            .Add(sqlParam6)

            Dim sqlParam7 As New SqlParameter
            sqlParam7.DbType = Data.DbType.Decimal
            sqlParam7.Direction = Data.ParameterDirection.Input
            sqlParam7.IsNullable = True
            sqlParam7.ParameterName = "GREATERTHAN"
            sqlParam7.Value = IIf(dcGreaterThan.HasValue, dcGreaterThan, DBNull.Value)
            .Add(sqlParam7)
        End With

        sqlCmd.Connection = con
        sqlCmd.Connection.Open()
        Dim Ret As Object
        Ret = sqlCmd.ExecuteScalar()
        If Ret IsNot DBNull.Value Then
            dcTotal = Convert.ToDecimal(Ret)
        Else
            dcTotal = 0
        End If
        Return dcTotal
    End Function

    Public Function GetFilteredReport(ByVal dtDateReqTo As Nullable(Of DateTime), ByVal dtDateReqFrom As Nullable(Of DateTime), ByVal iOrderStatus As Nullable(Of Integer), _
        ByVal iTeamId As Nullable(Of Integer), ByVal iEmpId As Nullable(Of Integer), ByVal dcLessThan As Nullable(Of Decimal), ByVal dcGreaterThan As Nullable(Of Decimal)) As DataTable

        Dim sqlAdpt As New SqlDataAdapter
        Dim sqlCon As New SqlConnection

        sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
        Dim sqlCmd As New SqlCommand("I_STATIONERY_ORDERITEM_REPORT_FILTERED", sqlCon)
        sqlCmd.CommandType = CommandType.StoredProcedure

        With sqlCmd.Parameters
            Dim sqlParam1 As New SqlParameter
            sqlParam1.DbType = Data.DbType.DateTime
            sqlParam1.Direction = Data.ParameterDirection.Input
            sqlParam1.IsNullable = True
            sqlParam1.ParameterName = "DATEREQUESTEDFROM"
            sqlParam1.Value = IIf(dtDateReqFrom.HasValue, dtDateReqFrom, DBNull.Value)
            .Add(sqlParam1)

            Dim sqlParam2 As New SqlParameter
            sqlParam2.DbType = Data.DbType.DateTime
            sqlParam2.Direction = Data.ParameterDirection.Input
            sqlParam2.IsNullable = True
            sqlParam2.ParameterName = "DATEREQUESTEDTO"
            sqlParam2.Value = IIf(dtDateReqTo.HasValue, dtDateReqTo, DBNull.Value)
            .Add(sqlParam2)

            Dim sqlParam3 As New SqlParameter
            sqlParam3.DbType = Data.DbType.Int32
            sqlParam3.Direction = Data.ParameterDirection.Input
            sqlParam3.IsNullable = True
            sqlParam3.ParameterName = "ITEMSTATUS"
            sqlParam3.Value = IIf(iOrderStatus.HasValue, iOrderStatus, DBNull.Value)
            .Add(sqlParam3)

            Dim sqlParam4 As New SqlParameter
            sqlParam4.DbType = Data.DbType.Int32
            sqlParam4.Direction = Data.ParameterDirection.Input
            sqlParam4.IsNullable = True
            sqlParam4.ParameterName = "TEAM"
            sqlParam4.Value = IIf(iTeamId.HasValue, iTeamId, DBNull.Value)
            .Add(sqlParam4)

            Dim sqlParam5 As New SqlParameter
            sqlParam5.DbType = Data.DbType.Int32
            sqlParam5.Direction = Data.ParameterDirection.Input
            sqlParam5.IsNullable = True
            sqlParam5.ParameterName = "EMPLOYEE"
            sqlParam5.Value = IIf(iEmpId.HasValue, iEmpId, DBNull.Value)
            .Add(sqlParam5)

            Dim sqlParam6 As New SqlParameter
            sqlParam6.DbType = Data.DbType.Decimal
            sqlParam6.Direction = Data.ParameterDirection.Input
            sqlParam6.IsNullable = True
            sqlParam6.ParameterName = "LESSTHAN"
            sqlParam6.Value = IIf(dcLessThan.HasValue, dcLessThan, DBNull.Value)
            .Add(sqlParam6)

            Dim sqlParam7 As New SqlParameter
            sqlParam7.DbType = Data.DbType.Decimal
            sqlParam7.Direction = Data.ParameterDirection.Input
            sqlParam7.IsNullable = True
            sqlParam7.ParameterName = "GREATERTHAN"
            sqlParam7.Value = IIf(dcGreaterThan.HasValue, dcGreaterThan, DBNull.Value)
            .Add(sqlParam7)
        End With

        sqlAdpt.SelectCommand = sqlCmd

        Dim dt As New DataTable
        sqlAdpt.Fill(dt)

        Return dt
    End Function
End Class
