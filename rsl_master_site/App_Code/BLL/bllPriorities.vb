Imports Microsoft.VisualBasic
Imports dalStationery
Imports dalStationeryTableAdapters

' priorities for Stationery orders
Public Class bllPriorities

    'Private mPriority As String
    Private mDataTable As I_STATIONERY_ORDER_PRIORITYDataTable

    Public Sub New()
        Dim daS As New I_STATIONERY_ORDER_PRIORITYTableAdapter
        mDataTable = daS.GetData()
    End Sub

    Public Function SelectAll() As I_STATIONERY_ORDER_PRIORITYDataTable
        Return mDataTable
    End Function

End Class
