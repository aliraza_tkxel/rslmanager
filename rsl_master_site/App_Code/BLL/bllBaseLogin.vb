Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.HttpContext
Imports MSDN.mySession
Public Class bllBaseLogin

#Region "Attributes"

    Private m_LoginSPName As String
    Private m_UpdateLoginSPName As String
    Private m_UserName As String
    Private m_ssoUserName As String
    Private m_Password As String
    Private m_FirstName As String
    Private m_LastName As String
    Private m_NewPassword As String
    Private m_DateExpires As DateTime
    Private m_ParamUserName As String
    Private m_ParamPassword As String
    Private m_ParamDateExpires As String
    Private m_ParamNewPassword As String
    Private m_IsLoggedIn As Boolean
    Private m_LoginExpired As Boolean
    Private m_ExpiryType As ExpiryType
    Private m_LoginLocked As Boolean
    Private m_LoginThreshold As Integer

    Public dsLogin As New DataSet
#End Region

    Public Enum ExpiryType
        Live = 1
        AlmostExpired = 2
        Expired = 3
    End Enum

    Public Property LoginExpiryStatus() As ExpiryType
        Get
            Return m_ExpiryType
        End Get
        Set(ByVal value As ExpiryType)
            m_ExpiryType = value
        End Set
    End Property

    Public ReadOnly Property LoginExpired() As Boolean
        Get
            Return m_LoginExpired
        End Get

    End Property


    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(ByVal value As String)
            m_UserName = value
        End Set
    End Property

    Public Property ssoUserName() As String
        Get
            Return m_ssoUserName
        End Get
        Set(ByVal value As String)
            m_ssoUserName = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(ByVal value As String)
            m_Password = value
        End Set
    End Property

    Public Property NewPassword() As String
        Get
            Return m_NewPassword
        End Get
        Set(ByVal value As String)
            m_NewPassword = value
        End Set
    End Property

    Public Property IsLoggedIn() As Boolean
        Get
            Return m_IsLoggedIn
        End Get
        Set(ByVal value As Boolean)
            m_IsLoggedIn = value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(ByVal value As String)
            m_FirstName = value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(ByVal value As String)
            m_LastName = value
        End Set
    End Property
    '<Track Login Failed Attempts>
    Public ReadOnly Property LoginLocked() As Boolean
        Get
            Return m_LoginLocked
        End Get

    End Property

    Public ReadOnly Property LoginThreshold() As Integer
        Get
            Return m_LoginThreshold
        End Get

    End Property
    Public Function Login() As Boolean

        Dim errors As New ErrorCollection
        Dim returnValue As Boolean = True

        If m_UserName.Length >= 50 Then
            errors.Add("You cannot enter a username of more than 50 characters")
        End If

        If m_UserName.Length = 0 Then
            errors.Add("You must enter a username")
        End If

        If m_Password.Length >= 50 Then
            errors.Add("You cannot enter a password of more than 50 characters")
        End If

        If m_Password.Length = 0 Then
            errors.Add("You must enter a password")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Try

            Dim strConnRSL As String = datahelper.getStrConn("connRSL")

            Dim connLogin As SqlConnection = datahelper.getconn(strConnRSL)
            connLogin.Open()

            Dim cmd As New SqlCommand(Current.GetSection("password_manager")("LoginSPName"), connLogin)
            cmd.CommandType = CommandType.StoredProcedure

            Dim param As New SqlParameter

            param = cmd.CreateParameter()
            param.ParameterName = Current.GetSection("password_manager")("ParamUserName")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = m_UserName
            cmd.Parameters.Add(param)

            param = New SqlParameter
            param = cmd.CreateParameter()
            param.ParameterName = Current.GetSection("password_manager")("ParamPassword")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = m_Password
            cmd.Parameters.Add(param)


            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(dsLogin)

            If dsLogin.Tables(0).Rows.Count > 0 Then
                m_DateExpires = dsLogin.Tables(0).Rows(0)(Current.GetSection("password_manager")("ParamDateExpires"))

                If m_DateExpires < Now Then
                    m_ExpiryType = ExpiryType.Expired
                Else
                    If m_DateExpires > Now And m_DateExpires < DateAdd(DateInterval.Day, 14, Now) Then
                        m_ExpiryType = ExpiryType.AlmostExpired
                    Else
                        m_ExpiryType = ExpiryType.Live
                    End If
                End If

                ' Return True
                'If Login Failed 
            Else
                '<Start Insert Failed Login History>
                Dim cmdThreshold As New SqlCommand("SP_NET_SETLOGIN_THRESHOLD", connLogin)
                cmdThreshold.CommandType = CommandType.StoredProcedure
                param = cmd.CreateParameter()
                param.ParameterName = "@username" 'Current.GetSection("password_manager")("ParamUserName")
                param.Direction = ParameterDirection.Input
                param.SqlDbType = SqlDbType.VarChar
                param.Value = Me.m_UserName
                cmdThreshold.Parameters.Add(param)
                da.SelectCommand = cmdThreshold
                da.Fill(dsLogin)

                ' SetLoginThreshold()

                If dsLogin.Tables(0).Rows.Count > 0 Then
                    m_LoginLocked = dsLogin.Tables(0).Rows(0)("Locked")
                    m_LoginThreshold = dsLogin.Tables(0).Rows(0)("Threshold")

                End If
                '<End Insert Failed Login History>
                returnValue = False
            End If
        Catch ex As Exception
            ErrorHelper.LogError(ex, True)

        End Try
        Return returnValue
    End Function

    Public Function LoginSso(ByVal secureKey As String) As Boolean

        Dim errors As New ErrorCollection
        Dim returnValue As Boolean = True

        'Can use this to check the key
        'If m_UserName.Length >= 50 Then
        '    errors.Add("You cannot enter a username of more than 50 characters")
        'End If
         
        'If errors.Count > 0 Then
        '    Dim strError As String = valhelper.Display(errors)
        '    Throw New ArgumentException(strError, "")
        'End If

        Try

            Dim strConnRSL As String = datahelper.getStrConn("connRSL")

            Dim connLogin As SqlConnection = datahelper.getconn(strConnRSL)
            connLogin.Open()

            Dim cmd As New SqlCommand("SP_SSO_Login", connLogin)
            cmd.CommandType = CommandType.StoredProcedure

            Dim param As New SqlParameter

            param = cmd.CreateParameter()
            param.ParameterName = "SecureKey"
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = secureKey
            cmd.Parameters.Add(param)
            
            Dim da As New SqlDataAdapter
            da.SelectCommand = cmd
            da.Fill(dsLogin)

            If dsLogin.Tables(0).Rows.Count > 0 Then
                'm_DateExpires = dsLogin.Tables(0).Rows(0)("isExpired")

                'If m_DateExpires < Now Then
                '    m_ExpiryType = ExpiryType.Expired
                'Else
                '    If m_DateExpires > Now And m_DateExpires < DateAdd(DateInterval.Day, 14, Now) Then
                '        m_ExpiryType = ExpiryType.AlmostExpired
                '    Else
                '        m_ExpiryType = ExpiryType.Live
                '    End If
                'End If

                ' Return True
                'If Login Failed 
            Else
                '<Start Insert Failed Login History>

                '<End Insert Failed Login History>
                returnValue = False
            End If
        Catch ex As Exception
            ErrorHelper.LogError(ex, True)

        End Try
        Return returnValue
    End Function

    Private Function ModifyPassword() As Integer
        Dim errors As New ErrorCollection


        If m_UserName.Length >= 50 Then
            errors.Add("You cannot enter a username of more than 50 characters")
        End If

        If m_UserName.Length = 0 Then
            errors.Add("You must enter a username")
        End If

        If m_Password.Length >= 50 Then
            errors.Add("You cannot enter a password of more than 50 characters")
        End If

        If m_Password.Length = 0 Then
            errors.Add("You must enter a password")
        End If

        If m_NewPassword.Length >= 50 Then
            errors.Add("You cannot enter a new password of more than 50 characters")
        End If

        If m_NewPassword.Length = 0 Then
            errors.Add("You must enter a new password")
        End If

        If m_NewPassword = m_Password Then
            errors.Add("The new password must be different from the old one")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Try
            Dim strConnRSL As String = datahelper.getStrConn("connRSL")


            Dim connLogin As SqlConnection = datahelper.getconn(strConnRSL)
            connLogin.Open()
            Dim cmd As New SqlCommand(Current.GetSection("password_manager")("UpdateLoginSPName"), connLogin)
            cmd.CommandType = CommandType.StoredProcedure

            Dim param As SqlParameter

            param = cmd.CreateParameter()
            param.ParameterName = Current.GetSection("password_manager")("UpdateParamUserName")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = m_UserName
            cmd.Parameters.Add(param)

            param = cmd.CreateParameter()
            param.ParameterName = Current.GetSection("password_manager")("UpdateParamPassword")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = m_Password
            cmd.Parameters.Add(param)

            param = cmd.CreateParameter()
            param.ParameterName = Current.GetSection("password_manager")("UpdateParamNewPassword")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = m_NewPassword
            cmd.Parameters.Add(param)

            'Dim retParam As SqlParameter = cmd.Parameters.Add("RetVal", SqlDbType.Int)
            Dim retParam As SqlParameter = cmd.CreateParameter()
            retParam.ParameterName = "@retStatus"
            retParam.SqlDbType = SqlDbType.Int
            retParam.Direction = ParameterDirection.Output
            cmd.Parameters.Add(retParam)

            cmd.ExecuteNonQuery()

            Dim intUpdateStatus As Integer = cmd.Parameters("@retStatus").Value

            '-- @retStatus:
            '-- 0 = No error occurred
            '-- 999 = An exception occurred during insert and/or update.
            '-- 10 = Username AND/OR do not match.
            '-- 11 = Password do not match, password history policy.

            Return intUpdateStatus
        Catch ex As Exception
            ErrorHelper.LogError(ex, True)
            Throw
        End Try
    End Function

    Private Sub sendNotification()
        Dim msg As New Web.Mail.MailMessage()
        msg.From = "rashid.khan@reidmark.com"
        msg.To = "rashid.khan@reidmark.com"
        'msg.Cc = "peter.thompson@reidmark.com"
        msg.Cc = "jon.swain@reidmark.com;Robert.Egan@reidmark.com"
        msg.Subject = "devteam password notification"
        msg.BodyFormat = Mail.MailFormat.Html
        msg.Priority = Mail.MailPriority.High
        Dim img As New Drawing.Bitmap(150, 25)
        Dim g As Drawing.Graphics = Drawing.Graphics.FromImage(img)
        g.Clear(Drawing.Color.White)
        g.DrawString(Me.Password, New Drawing.Font(Drawing.FontFamily.GenericMonospace, 10, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point), Drawing.Brushes.Black, 5, 5)
        img.Save("c:\windows\temp\devteam.jpg", Drawing.Imaging.ImageFormat.Jpeg)
        img.Dispose()
        g.Dispose()
        Dim ma As New Web.Mail.MailAttachment("c:\windows\temp\devteam.jpg", Mail.MailEncoding.Base64)
        msg.Attachments.Add(ma)
        msg.Body = "<p>The devteam password for <b>" + HttpContext.Current.Request.Url.Host + "</b> is changed</p>"
        msg.Body += "<p>The new password is attached as image file.</p>"
        msg.Body += "<p>Regards,</p>"
        msg.Body += "<p>RSLManager EMailBOT</p>"
        Web.Mail.SmtpMail.Send(msg)
    End Sub

    Public Function UpdatePassword() As Integer
        Dim i As Integer = ModifyPassword()
        If i = 0 Then
            If Me.UserName = "devteam" Then
                sendNotification()
            End If
        End If
        Return i
    End Function

    Public Sub StoreLoginHistory()


        Dim strConnRSL As String = datahelper.getStrConn("connRSL")

        Dim connLogin As SqlConnection = datahelper.getconn(strConnRSL)
        connLogin.Open()

        Dim cmd As New SqlCommand(Current.GetSection("password_manager")("StoreLoginSPName"), connLogin)
        cmd.CommandType = CommandType.StoredProcedure
        'cmd.ExecuteNonQuery()

        Dim param As SqlParameter

        

        param = cmd.CreateParameter()
        param.ParameterName = Current.GetSection("password_manager")("StoreParamFirstName")
        param.Direction = ParameterDirection.Input
        param.SqlDbType = SqlDbType.VarChar
        param.Value = Me.FirstName
        cmd.Parameters.Add(param)

        param = cmd.CreateParameter()
        param.ParameterName = Current.GetSection("password_manager")("StoreParamLastName")
        param.Direction = ParameterDirection.Input
        param.SqlDbType = SqlDbType.VarChar
        param.Value = Me.LastName
        cmd.Parameters.Add(param)

        If( m_UserName = "" )Then m_UserName = me.m_ssoUserName

        param = cmd.CreateParameter()
        param.ParameterName = Current.GetSection("password_manager")("ParamUserName")
        param.Direction = ParameterDirection.Input
        param.SqlDbType = SqlDbType.VarChar
        param.Value = Me.m_UserName
        cmd.Parameters.Add(param)


        cmd.ExecuteNonQuery()

    End Sub

    Public Sub SetLoginThreshold()
        Try
            Dim connLogin As SqlConnection = datahelper.getconn(datahelper.getStrConn("connRSL"))
            connLogin.Open()
            Dim cmd As New SqlCommand("SP_NET_SETLOGIN_THRESHOLD", connLogin)
            cmd.CommandType = CommandType.StoredProcedure

            Dim param As SqlParameter

            param = cmd.CreateParameter()
            param.ParameterName = "@username" 'Current.GetSection("password_manager")("ParamUserName")
            param.Direction = ParameterDirection.Input
            param.SqlDbType = SqlDbType.VarChar
            param.Value = Me.m_UserName
            cmd.Parameters.Add(param)

            '  Dim da As New SqlDataAdapter
            ' da.SelectCommand = cmd
            '  da.Fill(dsLogin)

        Catch ex As Exception
            ErrorHelper.LogError(ex, True)
        End Try
    End Sub
End Class
