Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data.SqlClient
Imports System.Data
Imports dalStationery
Imports dalStationeryTableAdapters

Public Class bllStationery

    Private mRequestedDate As Date
    Private mDescription As String
    Private mRequestedByEmpID As Integer
    Private mRequestedForEmpID As Integer
    Private mDeliveryLocation As String
    Private mOrderID As Integer
    Private mStatusID As Integer
    Private mStatus As String
    Private mTotalCost As Decimal

    Public Enum OrderStatus
        Saved = 1
        Requested
        InProgress
        Complete
    End Enum

    Public Function SelectById(ByVal OrderId As Integer) As I_STATIONERY_ORDERRow
        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable

        dt = da.SelectById(OrderId)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function GetAll() As I_STATIONERY_ORDERDataTable
        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable

        dt = da.GetData
        Return dt
    End Function

    Public Function GetAllByEmployee(ByVal iEmpID As Integer) As I_STATIONERY_ORDERDataTable
        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable

        dt = da.GetOrdersByEmpId(iEmpID)
        Return dt
    End Function

    Public Function GetAllByTeam(ByVal iTeamID As Integer) As I_STATIONERY_ORDERDataTable
        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable

        dt = da.GetOrdersByLineManagerId(iTeamID)
        Return dt
    End Function

    Public Function GetAllByLineManager(ByVal iEmpID As Integer) As I_STATIONERY_ORDERDataTable
        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable

        dt = da.GetOrdersByLineManagerId(iEmpID)
        Return dt
    End Function

    Public Function GetAllOrderItems(ByVal iOrderID As Integer) As I_STATIONERY_ORDER_ITEMDataTable
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        Dim dt As New I_STATIONERY_ORDER_ITEMDataTable

        If iOrderID > 0 Then
            dt = da.GetItemsByOrderID(iOrderID)
            Return dt
        Else
            Return Nothing
        End If
    End Function

    Public Function GetFilteredResultsByEmp(ByVal iEmpID As Integer, _
                                            ByVal strDateReqFrom As String, _
                                            ByVal strDateReqTo As String, _
                                            ByVal iOrderStatus As Nullable(Of Integer), _
                                            ByVal iTeamId As Nullable(Of Integer), _
                                            ByVal iEmpForId As Nullable(Of Integer), _
                                            ByVal strLessThan As String, _
                                            ByVal strGreaterThan As String) As I_STATIONERY_ORDERDataTable
        Try


            Dim da As New I_STATIONERY_ORDERTableAdapter
            Dim dt As New I_STATIONERY_ORDERDataTable

            Dim dateReqTo As Nullable(Of DateTime)
            Dim dateReqFrom As Nullable(Of DateTime)
            Dim orderStatus As Nullable(Of Integer)
            Dim dcLessThan As Nullable(Of Decimal)
            Dim dcGreaterThan As Nullable(Of Decimal)

            If Not String.IsNullOrEmpty(strDateReqFrom) Then
                dateReqFrom = valhelper.convertToDate(strDateReqFrom)
            End If

            If Not String.IsNullOrEmpty(strDateReqTo) Then
                dateReqTo = valhelper.convertToDate(strDateReqTo)
            End If

            If Not String.IsNullOrEmpty(strLessThan) Then
                dcLessThan = Convert.ToDecimal(strLessThan.Replace("�", ""))
            End If

            If Not String.IsNullOrEmpty(strGreaterThan) Then
                dcGreaterThan = Convert.ToDecimal(strGreaterThan.Replace("�", ""))
            End If

            orderStatus = iOrderStatus

            dt = da.GetFilteredResultsByEmp(iEmpID, dateReqFrom, datereqto, orderStatus, iTeamId, iEmpForId, dcLessThan, dcGreaterThan)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function GetFilteredResultsByLineManager(ByVal iEmpID As Integer, _
                                            ByVal strDateReqFrom As String, _
                                            ByVal strDateReqTo As String, _
                                            ByVal iOrderStatus As Nullable(Of Integer), _
                                            ByVal iTeamId As Nullable(Of Integer), _
                                            ByVal iEmpForId As Nullable(Of Integer), _
                                            ByVal strLessThan As String, _
                                            ByVal strGreaterThan As String) As I_STATIONERY_ORDERDataTable

        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable
        Dim dcLessThan As Nullable(Of Decimal)
        Dim dcGreaterThan As Nullable(Of Decimal)

        Dim dateReqFrom As Nullable(Of DateTime)
        Dim dateReqTo As Nullable(Of DateTime)
        Dim orderStatus As Nullable(Of Integer)

        If Not String.IsNullOrEmpty(strDateReqFrom) Then
            dateReqFrom = valhelper.convertToDate(strDateReqFrom)
        End If

        If Not String.IsNullOrEmpty(strDateReqTo) Then
            dateReqTo = valhelper.convertToDate(strDateReqTo)
        End If

        If Not String.IsNullOrEmpty(strLessThan) Then
            dcLessThan = Convert.ToDecimal(strLessThan.Replace("�", ""))
        End If

        If Not String.IsNullOrEmpty(strGreaterThan) Then
            dcGreaterThan = Convert.ToDecimal(strGreaterThan.Replace("�", ""))
        End If

        orderStatus = iOrderStatus

        dt = da.GetFilteredResultsByLineManagerId(iEmpID, dateReqFrom, dateReqTo, orderStatus, iTeamId, iEmpForId, dcLessThan, dcGreaterThan)
        Return dt
    End Function

    Public Function GetFilteredResultsAll(ByVal strDateReqFrom As String, _
                                            ByVal strDateReqTo As String, _
                                            ByVal iOrderStatus As Nullable(Of Integer), _
                                            ByVal iTeamId As Nullable(Of Integer), _
                                            ByVal iEmpForId As Nullable(Of Integer), _
                                            ByVal strLessThan As String, _
                                            ByVal strGreaterThan As String) As I_STATIONERY_ORDERDataTable

        Dim da As New I_STATIONERY_ORDERTableAdapter
        Dim dt As New I_STATIONERY_ORDERDataTable
        Dim dcLessThan As Nullable(Of Decimal)
        Dim dcGreaterThan As Nullable(Of Decimal)

        Dim dateReqFrom As Nullable(Of DateTime)
        Dim dateReqTo As Nullable(Of DateTime)
        Dim orderStatus As Nullable(Of Integer)

        If Not String.IsNullOrEmpty(strDateReqFrom) Then
            dateReqFrom = valhelper.convertToDate(strDateReqFrom)
        End If

        If Not String.IsNullOrEmpty(strDateReqTo) Then
            dateReqTo = valhelper.convertToDate(strDateReqTo)
        End If

        If Not String.IsNullOrEmpty(strLessThan) Then
            dcLessThan = Convert.ToDecimal(strLessThan.Replace("�", ""))
        End If

        If Not String.IsNullOrEmpty(strGreaterThan) Then
            dcGreaterThan = Convert.ToDecimal(strGreaterThan.Replace("�", ""))
        End If

        orderStatus = iOrderStatus

        dt = da.GetFilteredResultsAll(dateReqFrom, dateReqTo, orderStatus, iTeamId, iEmpForId, dcLessThan, dcGreaterThan)
        Return dt
    End Function

    Public Sub SetOrderRequested(ByVal iOrderID As Integer)
        Dim daOrder As New I_STATIONERY_ORDERTableAdapter

        daOrder.SetStatus(iOrderID, OrderStatus.Requested)
    End Sub

    Public Sub SetOrderInProgress(ByVal iOrderID As Integer)
        Dim daOrder As New I_STATIONERY_ORDERTableAdapter

        daOrder.SetStatus(iOrderID, OrderStatus.InProgress)
    End Sub

    Public Sub SetOrderCompleted(ByVal iOrderID As Integer)
        Dim daOrder As New I_STATIONERY_ORDERTableAdapter
        Dim daOrderItems As New I_STATIONERY_ORDER_ITEMTableAdapter

        ' on order completion we need ALL order items to be rejected or received
        ' so first check stationery order items
        daOrder.SetStatus(iOrderID, OrderStatus.Complete)
    End Sub


    Public Property RequestedByEmpID() As Integer
        Get
            Return mRequestedByEmpID
        End Get
        Set(ByVal value As Integer)
            mRequestedByEmpID = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Description = mDescription
        End Get
        Set(ByVal value As String)
            mDescription = value
        End Set
    End Property

    Public Property RequestedDate() As Date
        Get
            RequestedDate = mRequestedDate
        End Get
        Set(ByVal value As Date)
            mRequestedDate = value
        End Set
    End Property

    Public Property RequestedForEmpID() As Integer
        Get
            RequestedForEmpID = mRequestedForEmpID
        End Get
        Set(ByVal value As Integer)
            mRequestedForEmpID = value
        End Set
    End Property

    Public Property DeliveryLocation() As String
        Get
            DeliveryLocation = mDeliveryLocation
        End Get
        Set(ByVal value As String)
            mDeliveryLocation = value
        End Set
    End Property

    Public ReadOnly Property OrderID() As Integer
        Get
            OrderID = mOrderID
        End Get
    End Property

    Public Property StatusID() As Integer
        Get
            StatusID = mStatusID
        End Get
        Set(ByVal value As Integer)
            mStatusID = value
        End Set
    End Property

    Public ReadOnly Property Status() As String
        Get
            Status = mStatus
        End Get
    End Property

    Public ReadOnly Property TotalCost() As Decimal
        Get
            TotalCost = mTotalCost
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub Update(ByVal iOrderID As Integer)
        If iOrderID > 0 Then
            Dim taOrders As New I_STATIONERY_ORDERTableAdapter

            taOrders.Update(mDeliveryLocation, mDescription, iOrderID, mRequestedForEmpID)

        End If
    End Sub

    Public Sub New(ByVal orderID As Integer)
        Dim daS As New I_STATIONERY_ORDERTableAdapter
        Dim dtS As New I_STATIONERY_ORDERDataTable
        Dim drS As I_STATIONERY_ORDERRow
        dtS = daS.SelectById(orderID)
        drS = dtS(0)

        mOrderID = drS.OrderId
        mRequestedByEmpID = drS.RequestedByEmpId
        mRequestedForEmpID = drS.RequestedForEmpId
        mRequestedDate = drS.RequestedDate
        mDeliveryLocation = drS.DeliveryLocation
        mStatusID = drS.StatusId
        mStatus = drS.OrderStatus

        If drS.IsTotalCostNull Then
            mTotalCost = 0
        Else
            mTotalCost = drS.TotalCost
        End If

        If IsDBNull(drS.Description) Then
            mDescription = ""
        Else
            mDescription = drS.Description
        End If

        If IsDBNull(drS.RequestedDate) Then
            mRequestedDate = ""
        Else
            mRequestedDate = drS.RequestedDate
        End If
    End Sub

    Public Sub Create()
        Dim daS As New I_STATIONERY_ORDERTableAdapter
        daS.Insert(mRequestedDate, mDescription, mRequestedByEmpID, mRequestedForEmpID, mDeliveryLocation, 1)
    End Sub

    Public Function Delete(ByVal iOrderID As Integer) As Integer
        Dim taOrder As New I_STATIONERY_ORDERTableAdapter
        Dim drOrder As I_STATIONERY_ORDERRow

        ' delete order & order items
        drOrder = taOrder.SelectById(iOrderID).Rows(0)
        drOrder.Delete()

        taOrder.Update(drOrder)
    End Function

    Public Sub DeleteItem(ByVal iOrderItemID As Integer)
        Dim da As New I_STATIONERY_ORDER_ITEMTableAdapter
        da.Delete(iOrderItemID)
    End Sub

    Public Sub RefreshOrderStatus(ByVal iOrderID As Integer)
        Dim taStationeryOrderItem As New I_STATIONERY_ORDER_ITEMTableAdapter

        Dim bllStationeryOrder As New bllStationery(iOrderID)

        Select Case bllStationeryOrder.StatusID

            Case bllStationery.OrderStatus.Saved
                ' do nothing with status

            Case bllStationery.OrderStatus.Requested
                ' do nothing with status

            Case bllStationery.OrderStatus.InProgress
                If taStationeryOrderItem.GetNumItemsNotCompleted(iOrderID) = 0 Then
                    ' promote to completed if no uncompleted items
                    SetOrderCompleted(iOrderID)
                End If

            Case bllStationery.OrderStatus.Complete
                If taStationeryOrderItem.GetNumItemsNotCompleted(iOrderID) > 0 Then
                    ' demote to in progress if uncompleted items no exist
                    SetOrderInProgress(iOrderID)
                End If

        End Select

    End Sub

    Public Shared Function HasAccessToAllOrders(ByVal iEmpID As Integer) As Boolean
        ' returns true if logged in user is in the list of employees who belongs in any of the following job tiltes 	 'categories
        ' Facilities and Services Supervisor, Manager, Administrator (Trainee)
        Dim conConnection As New SqlConnection
        Dim cmdCommand As SqlCommand
        Dim bFound As Boolean
        Dim iResult As Boolean

        Try
            Dim prm As SqlParameter
            conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString

            cmdCommand = New SqlCommand("dbo.I_HasAccessToAllStationaryOrders", conConnection)
            cmdCommand.CommandType = Data.CommandType.StoredProcedure

            prm = New SqlParameter("@EmployeeID", Data.SqlDbType.Int)
            prm.Value = iEmpID

            cmdCommand.Parameters.Add(prm)

            cmdCommand.Connection.Open()
            iResult = Convert.ToBoolean(cmdCommand.ExecuteScalar)

            If iResult = True Then
                bFound = True

            End If

        Catch ex As Exception

        Finally
            conConnection.Close()
            conConnection.Dispose()
            cmdCommand.Dispose()
        End Try

        Return bFound

    End Function

End Class
