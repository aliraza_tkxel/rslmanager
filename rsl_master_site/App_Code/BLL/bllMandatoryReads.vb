Imports Microsoft.VisualBasic
Imports dalMandatoryRead
Imports dalMandatoryReadTableAdapters
Imports System.Data

Public Class bllMandatoryReads
    Inherits baseBLL
    Public Sub New()
        SetLocale()
    End Sub
    Public Function CreateRead(ByVal intDocumentID As Integer, ByVal intEmpID As Integer) As Integer
        Dim daMand As New I_MANDATORY_READTableAdapter
        Dim dtMand As New I_MANDATORY_READDataTable
        Dim drMand As I_MANDATORY_READRow

        drMand = dtMand.NewI_MANDATORY_READRow
        drMand.DOCUMENTID = intDocumentID
        drMand.EMPLOYEEID = intEmpID
        dtMand.AddI_MANDATORY_READRow(drMand)
        Return daMand.Update(dtMand)
    End Function

    Public Function GetMandatoryRead(ByVal intEmpID As Integer, ByVal intRowCount As Integer) As DataTable
        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.GetMandatoryReads(intEmpID, intRowCount)

    End Function
    Public Function GetLatest(ByVal intEmpID As Integer) As DataTable
        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.GetLatestByTeam(intEmpID)

    End Function

    Public Function GetTopLatestByTeam(ByVal intEmpID As Integer) As DataTable
        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.GetTopLatestByTeam(intEmpID)

    End Function

    Public Function GetMandatoryCount(ByVal intEmpID As Integer) As Integer
        Dim daMand As New I_MANDATORY_READTableAdapter
        Dim intMandCount As Integer
        daMand.GetMandatoryCount(intEmpID, intMandCount)
        Return intMandCount
    End Function

    Public Function GetReport(ByVal strExpiryFrom As String, _
                            ByVal strExpiryTo As String, _
                            ByVal strDateCreatedFrom As String, _
                            ByVal strDateCreatedTo As String, _
                            ByVal intTeamID As Nullable(Of Integer), _
                            ByVal strName As String) As DataTable

        Dim dtDateCreatedFrom As Nullable(Of Date)
        Dim dtDateCreatedTo As Nullable(Of Date)
        Dim dtDateExpiresFrom As Nullable(Of Date)
        Dim dtDateExpiresTo As Nullable(Of Date)

        Dim errors As New ErrorCollection

        If Not String.IsNullOrEmpty(strDateCreatedFrom) Then
            If Not IsDate(strDateCreatedFrom) Then
                errors.Add("The creation date from must be a valid date")
            Else
                dtDateCreatedFrom = valhelper.convertToDate(strDateCreatedFrom)
            End If
        End If

        If Not String.IsNullOrEmpty(strDateCreatedTo) Then
            If Not IsDate(strDateCreatedTo) Then
                errors.Add("The creation date to must be a valid date")
            Else
                dtDateCreatedTo = valhelper.convertToDate(strDateCreatedTo)
            End If
        End If


        If Not String.IsNullOrEmpty(strExpiryFrom) Then
            If Not IsDate(strExpiryFrom) Then
                errors.Add("The expiry date from must be a valid date")
            Else
                dtDateExpiresFrom = valhelper.convertToDate(strExpiryFrom)
            End If
        End If

        If Not String.IsNullOrEmpty(strExpiryTo) Then
            If Not IsDate(strExpiryTo) Then
                errors.Add("The expiry date to must be a valid date")
            Else
                dtDateExpiresTo = valhelper.convertToDate(strExpiryTo)
            End If
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.GetReport(intTeamID, dtDateCreatedFrom, dtDateCreatedTo, dtDateExpiresFrom, dtDateExpiresTo, strName)



    End Function

    Public Function SearchDocs(ByVal intEmpID As Integer, ByVal strSearch As String) As DataTable
        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.DocSearch(intEmpID, strSearch)
    End Function
    Public Function GetEmployeesToRead(ByVal intDocID As Integer) As DataTable
        Dim daMand As New I_MANDATORY_READTableAdapter
        Return daMand.GetEmployeesToRead(intDocID, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    End Function

    Public Shared Function getSCExportList(ByVal DOCUMENTID As Integer) As dalMandatoryRead.I_MANDATORY_READDataTable
        Return New dalMandatoryReadTableAdapters.I_MANDATORY_READTableAdapter().GetEmployeesToRead(DOCUMENTID, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

        ' Return New dalStartConsultationTableAdapters.TC_GETCUSTOMERCOMMUNICATIONLISTBYTYPETableAdapter().GetData(type, programmeid)
    End Function

    Public Shared Function ToCSV(ByVal dataTable As Data.DataTable) As String
        'create the stringbuilder that would hold our data 
        Dim sb As New StringBuilder()
        'check if there are columns in our datatable 
        If dataTable.Columns.Count <> 0 Then
            'loop thru each of the columns so that we could build the headers 
            'for each field in our datatable 
            For Each column As Data.DataColumn In dataTable.Columns
                'append the column name followed by our separator 
                sb.Append(column.ColumnName + ","c)
            Next
            'append a carriage return 
            sb.Append("" & Chr(13) & "" & Chr(10) & "")
            'loop thru each row of our datatable 
            For Each row As Data.DataRow In dataTable.Rows
                'loop thru each column in our datatable 
                For Each column As Data.DataColumn In dataTable.Columns
                    'get the value for tht row on the specified column 
                    ' and append our separator 
                    sb.Append("""" + row(column).ToString() + """" + ","c)
                Next
                'append a carriage return 
                sb.Append("" & Chr(13) & "" & Chr(10) & "")
            Next
        End If
        'return our values 
        Return sb.ToString()
    End Function
End Class


