﻿Imports System.Web
Imports dalEnviroTips
Imports dalEnviroTipsTableAdapters
Public Class bllEnviroTips
    Private m_EnviroTipsID As Integer
    Private m_EnviroTipsDescription As String

    Public Property EnviroTipsID() As Integer
        Get
            Return m_EnviroTipsID
        End Get
        Set(ByVal value As Integer)
            m_EnviroTipsID = value
        End Set
    End Property
    Public Property EnviroTipsDescription() As String
        Get
            Return m_EnviroTipsDescription
        End Get
        Set(ByVal value As String)
            m_EnviroTipsDescription = value
        End Set
    End Property
    Public Function GetEnviroTips() As I_ENVIROTIPSDataTable
        Dim daEnviroTips As New I_ENVIROTIPSTableAdapter
        Return daEnviroTips.GetData()
    End Function
    Public Sub New()

    End Sub
    Public Sub New(ByVal TipsId As Integer)
        Dim daEnviroTips As New I_ENVIROTIPSTableAdapter
        Dim dttnviroTips As New I_ENVIROTIPSDataTable
        Dim drEnviroTips As I_ENVIROTIPSRow

        dttnviroTips = daEnviroTips.GetTipByTipID(TipsId)
        drEnviroTips = dttnviroTips(0)
        m_EnviroTipsID = drEnviroTips.TipsId
        m_EnviroTipsDescription = drEnviroTips.TipsDescription
    End Sub
    Public Function Update(ByVal TipsId As Integer) As Integer
        Dim errors As New ErrorCollection


        If String.IsNullOrEmpty(m_EnviroTipsDescription) Then
            errors.Add("You must add a message")
        End If

        'added 68 because the text editor add 8 characters before the first character
        If Trim(m_EnviroTipsDescription.Length) > 158 Then
            errors.Add("You cannot add a Message of more than 150 characters")
        End If

        If errors.Count > 0 Then
            Dim strError As String = valhelper.Display(errors)
            Throw New ArgumentException(strError, "")
        End If

        Dim daEnviroTips As New I_ENVIROTIPSTableAdapter
        Dim dttnviroTips As New I_ENVIROTIPSDataTable
        Dim drEnviroTips As I_ENVIROTIPSRow

        If TipsId = 0 Then
            drEnviroTips = dttnviroTips.NewI_ENVIROTIPSRow
        Else
            dttnviroTips = daEnviroTips.GetTipByTipID(TipsId)
            drEnviroTips = dttnviroTips(0)
        End If


        drEnviroTips.TipsDescription = m_EnviroTipsDescription

        If TipsId = 0 Then
            dttnviroTips.AddI_ENVIROTIPSRow(drEnviroTips)
        End If

        Return daEnviroTips.Update(dttnviroTips)
    End Function
    Public Function Delete(ByVal TipsId As Integer) As Integer
        Dim daEnviroTips As New I_ENVIROTIPSTableAdapter
        Dim dttnviroTips As New I_ENVIROTIPSDataTable
        Dim drEnviroTips As I_ENVIROTIPSRow

        dttnviroTips = daEnviroTips.GetTipByTipID(TipsId)
        drEnviroTips = dttnviroTips(0)
        drEnviroTips.Delete()
        Return daEnviroTips.Update(dttnviroTips)
    End Function
End Class
