Imports Microsoft.VisualBasic
Imports System
Imports System.Xml
Imports System.Web
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Reflection
Imports System.Data
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports LoginUtility
Imports reidmark.platform.helpers.brokers.formbuilder
Imports reidmark.platform.security.passwordmanager.login


Partial Class _Default

    Inherits basepage

    Dim login As clsLogin

    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error



    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim formbuilder As New clsformbuilder
        formbuilder.xmlconfig = "App_Data/login_params_props.xml"
        formbuilder.xmlcachename = "xml_login_config"
        formbuilder.buildform()

        Dim settings As New ConnectionStringSettings
        settings = ConfigurationManager.ConnectionStrings("connRSL")


        login = New clsLogin
        login.xmlconfig = "App_Data/login_params_props.xml"
        login.xmlcachename = "xml_login_config"
        login.conn = settings.ConnectionString.ToString

        Dim logutil As LoginUtility = New LoginUtility
        Session("login") = logutil.serialize_object(login)

    End Sub

    Sub setsession(ByVal ds As DataSet)

        Dim isContractor As Integer
        Dim strTeamCode As String
        Dim rows As Data.DataRowCollection
        rows = ds.Tables(0).Rows()

        Session("FirstName") = rows(0).Item("FIRSTNAME")
        Session("LastName") = rows(0).Item("LASTNAME")
        Session("USERID") = rows(0).Item("EMPLOYEEID")
        Session("TeamCode") = rows(0).Item("TeamCode")

        strTeamCode = rows(0).Item("TEAMCODE")
        Session("TEAMNAME") = rows(0).Item("TEAMNAME")


        If Not IsDBNull(rows(0).Item("ORGID")) Then
            isContractor = rows(0).Item("ORGID")
        End If

        'Dim str_redirect As String




        If Not IsDBNull(rows(0).Item("ORGID")) Then
            	Session("ORGID") = isContractor
		Response.Redirect("/MyWeb/MyWhiteboard.asp")
        Else
            If strTeamCode = "PMA" Then
		Response.write ("board" &  strTeamCode &"/BoardMembers/BM.asp")
                Response.Redirect("/BoardMembers/BM.asp")
            Else
		Response.write ("standard" &  strTeamCode)
                Response.Redirect("/MyWeb/MyWhiteboard.asp")
            End If
        End If








    End Sub



    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ds As New DataSet
        Dim GenericObject As Object



        Dim logutil As LoginUtility = New LoginUtility
        GenericObject = logutil.deserialize_object(Session("login").ToString)
        login = CType(GenericObject, clsLogin)
        ds = login.check_login()

        Session("ds") = logutil.serialize_object(ds)
        Session("login") = logutil.serialize_object(login)

        lblstatus.Text = ""
        If Page.IsValid Then


            If login.status = 0 Then
                lblstatus.ForeColor = Drawing.Color.Red
                lblstatus.Text = "Your login has been unsuccessful."
                pnlexpiry.Visible = False

            Else
                loginform.Visible = False
                lblstatus.ForeColor = Drawing.Color.Black
                lblstatus.Text = "Your login has been successful. <br>"


                If login.expirydate < Now Then

                    pnlexpiry.Visible = True
                    lblexpiry.Text = "However your password has expired."
                    pnlforgot.Visible = False
                Else
                    If login.expirydate > Now And login.expirydate < DateAdd(DateInterval.Day, 14, Now) Then

                        pnlexpiry.Visible = True
                        lblexpiry.Text = "However your password is about to expire."
                        pnlEnter.Visible = True
                        pnlforgot.Visible = False

                    Else
                        setsession(ds)

                    End If
                End If
            End If
        End If
    End Sub


    Protected Sub lnkexpiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkexpiry.Click

    End Sub

    Protected Sub lnkenter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkenter.Click
        Dim ds As DataSet
        Dim logutil As LoginUtility = New LoginUtility
        Dim GenericObject As Object
        GenericObject = logutil.deserialize_object(Session("ds").ToString)
        ds = CType(GenericObject, DataSet)
        setsession(ds)
    End Sub
End Class
