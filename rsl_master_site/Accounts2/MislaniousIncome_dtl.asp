<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "Transaction Ref|REF|80"
	SortASC(0) 	= "REF ASC"
	SortDESC(0) = "REF DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "MIIncome(|)"
	
	ColData(1)  = "Bank|Bank|200"
	SortASC(1) 	= "NL_ACCOUNT_1.[DESC] ASC"
	SortDESC(1) = "NL_ACCOUNT_1.[DESC] DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "Paymenttype|Paymenttype|100"
	SortASC(2) 	= "F_PAYMENTTYPE.DESCRIPTION  ASC"
	SortDESC(2) = "F_PAYMENTTYPE.DESCRIPTION  DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Total Gross Amount|TotGross|90"
	SortASC(3) 	= "TotGross ASC"
	SortDESC(3) = "TotGross DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT"" "
	TDFunc(3) = "FormatCurrency(|)"		

	ColData(4)  = "Total Items|tcount|50"
	SortASC(4) 	= "tcount ASC"
	SortDESC(4) = "tcount DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""
	
	ColData(5)  = "Date|TXNDATE|80"
	SortASC(5) 	= "NL_JOURNALENTRY.TXNDATE ASC"
	SortDESC(5) = "NL_JOURNALENTRY.TXNDATE DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""
	
	
	PageName = "MislaniousIncome_dtl.asp"
	EmptyText = "No Relevant Miscellaneous Receipt found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""id"") &  "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	
	SQLCODE="SELECT   NL_Miscellaneous_HDR.ID as REF,  NL_ACCOUNT_1.[DESC] AS Bank, F_PAYMENTTYPE.DESCRIPTION AS Paymenttype, SUM(NL_Miscellaneous_dtl.Gross) AS TotGross, " &_
                      " COUNT(NL_Miscellaneous_HDR.ID) AS tcount, NL_Miscellaneous_HDR.ID, NL_JOURNALENTRY.TXNDATE " &_
			" FROM         NL_ACCOUNT NL_ACCOUNT_1 INNER JOIN " &_
                      " NL_Miscellaneous_HDR ON NL_ACCOUNT_1.ACCOUNTID = NL_Miscellaneous_HDR.Bank INNER JOIN " &_
                      " F_PAYMENTTYPE ON NL_Miscellaneous_HDR.PMethod = F_PAYMENTTYPE.PAYMENTTYPEID INNER JOIN " &_
                      " NL_ACCOUNT NL_ACCOUNT_2 INNER JOIN " &_
                      " NL_Miscellaneous_dtl ON NL_ACCOUNT_2.ACCOUNTID = NL_Miscellaneous_dtl.NlAccount ON  " &_
                      " NL_Miscellaneous_HDR.ID = NL_Miscellaneous_dtl.hdrid INNER JOIN " &_
                      " NL_JOURNALENTRY ON NL_Miscellaneous_HDR.TXNID = NL_JOURNALENTRY.TXNID " &_
			" GROUP BY NL_ACCOUNT_1.[DESC], F_PAYMENTTYPE.[DESCRIPTION ], NL_Miscellaneous_HDR.ID, NL_JOURNALENTRY.TXNDATE " &_
			"ORDER BY "& orderBy
	
	
	
	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	'OpenDB()
	
	'Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "NL_ACCOUNT", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	'CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Balance Transfer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	
	function SubmitPage(){
		location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  ;
		}

	function load_me(txnid){
		window.showModelessDialog("Popups/MIS.asp?txnid=" + txnid  + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
		}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="../Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>