<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="Calendar.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim mypage, numpages, prevpage, nextpage, numrecs, pagesize	

	Headers = Array("T.Date", "Item","Payment","Tenancy","Withdrawn","Paid In","&nbsp;")
	SortOrder = Array("RJ.TRANSACTIONDATE", "I.DESCRIPTION", "P.DESCRIPTION", "RJ.TENANCYID", "RJ.AMOUNT", "-RJ.AMOUNT", "")
	SortOrderAsc = Array("black","black","black","black","black","black","black","black","black")	
	SortOrderDesc = Array("black","black","black","black","black","black","black","black","black")
	HeaderWidths = Array("90", "190", "250","100", "90", "90", "")
			'
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal_debit = Request("detotal_debit")
	if detotal_debit = "" Then detotal_debit = 0 end if

	detotal_credit = Request("detotal_credit")
	if detotal_credit = "" Then detotal_credit = 0 end if

	selected = Request("idlist") // get selected checkboxes into an array
	selected_data = "," & Replace(selected, " ", "") & ","

	todate = Request("txt_TO")
	if todate = "" Then 
		to_sql = "" 
	else
		to_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(todate,1) & "' "
	end if

	fromdate = Request("txt_FROM")
	if fromdate = "" Then 
		from_sql = "" 
	else
		from_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) >= '" & FormatDateTime(fromdate,1) & "' "
	end if

	tenancy = Request("txt_TENANCYID")
	if tenancy = "" Then 
		tenancy_sql = "" 
	else
		tenancy_sql = " AND RJ.TENANCYID LIKE '%" & tenancy & "%' "
	end if
		
	localauthority = Request("sel_LA")
	if localauthority = "" Then 
		localauthority_sql = "" 
	else
		localauthority_sql = " AND LA.LOCALAUTHORITYID = " & localauthority & " "
	end if

	paymenttype = Request("sel_PAYMENTTYPE")
	if paymenttype = "" Then 
		paymenttype_sql = "" 
		inClause = "(3,7,17,18,22,25)"
	else
		inClause = "(" & paymenttype & ")"
		paymenttype_sql = " AND RJ.PAYMENTTYPE = " & paymenttype & " "
	end if
	
	orderBy = Request("orderBy")
	Call SetSort()
		
	TheNextPage_NoOrder = "txt_FROM=" & fromdate & "&txt_TO=" & todate & "&txt_TENANCYID=" & tenancy & "&sel_LA=" & localauthority & "&sel_PAYMENTTYPE=" & paymenttype
	TheNextPage = "orderBy=" & orderBy & "&" & TheNextPage_NoOrder
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CLng(mypage)
		end if
		
		Call TopBit()
		
		cnt = 0
		strSQL = 	"SELECT 	RJ.JOURNALID, RJ.TENANCYID, CONVERT(VARCHAR,RJ.TRANSACTIONDATE, 103) AS TRANSDATE, P.DESCRIPTION AS PAYMENTTYPE, " &_
					"			ISDEBIT, ISNULL(RJ.AMOUNT,0) AS AMOUNT, TS.DESCRIPTION AS STATUS, I.DESCRIPTION AS ITEMTYPE, " &_
					"			CASE WHEN LEN( LA.DESCRIPTION) > 22   " &_
					"				THEN LEFT(LA.DESCRIPTION , 17) +  '...'  "  &_
					"				ELSE  LA.DESCRIPTION " &_
					"			END AS LOCALAUTHORITY , LA.DESCRIPTION AS LOCALAUTHORITYTITLE " &_ 
					"FROM	 	F_RENTJOURNAL RJ " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = RJ.ITEMTYPE " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS TS ON TS.TRANSACTIONSTATUSID = RJ.STATUSID " &_
					"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
					"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
					"			INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
					"			LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = DEV.LOCALAUTHORITY " &_
					"WHERE		RJ.STATUSID <> 1  " &_
					" AND 	P.PAYMENTTYPEID IN " & inClause & " AND NOT EXISTS" &_
					"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
					"			" & from_sql & to_sql & localauthority_sql & tenancy_sql & paymenttype_sql & " " &_
					"ORDER		BY " & orderBy & ", RJ.JOURNALID ASC"
		response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 12
	'  		  PLACE THIS NEXT TO WHERE
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		Set regEx = New RegExp 

		
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if check box has been previously selected
			isselected = ""
			trstyle = ""
			JOURNALID = Rs("JOURNALID")
			ListSearch = "," & JOURNALID & ","
			Comparison = InStr(1, selected_data, ListSearch, 0)
			If NOT isNULL(Comparison) AND Comparison > 0  Then
					isselected = " checked "
					trstyle = " style='background-color:steelblue;color:white' "
			End If
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			paymenttype = Rs("PAYMENTTYPE")
			paymenttypetitle = ""
			if (NOT isNull(paymenttype)) then
				if (len(paymenttype) > 20) then
					paymenttypetitle = " title='" & paymenttype& "' "
					paymenttype = LEFT(paymenttype,20) & "..."
				end if
			end if
			 
			str_data = str_data & 	"<TR" & trstyle & "><TD>" & Rs("TRANSDATE") & "</TD>" &_
						"<TD>" & Rs("ITEMTYPE") & "</TD>" &_
						"<TD " & paymenttypetitle & ">" & paymenttype & "</TD>" &_
						"<TD>" & TenancyReference(Rs("TENANCYID")) & "</TD>" 
			amount = Rs("AMOUNT")
			if (amount < 0) then
				iType = 1
				amount = -amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				iType = 0						
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & JOURNALID & "' value='" & JOURNALID & "' onclick='do_sum("& JOURNALID &", " & FormatNumber(amount,2,-1,0,0)& "," & iType & ")'></TD>" &_
						"</TR>"
			
			Rs.movenext()
			End If
		Next

		Rs.close()
		Set Rs = Nothing
		
		Call BottomBit(4)	
	
	End function
	
	if (Request("RESET") = 1) then
%>	
	<!--#include file="Balance_Server.asp" -->
<%
	end if	
%>
<html>
<head>
	<!-- #include file="Balance_Server_JS.asp" -->
<script language=javascript>
function ReturnData(){
	<% if (Request("RESET") = 1) then %>
		parent.CalendarHolder.innerHTML = LoaderDivCalendar.innerHTML				
		parent.reset_client_totals()
		SendBalances()		
	<% end if %>
	parent.MainDiv.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
</head>
<body onload="ReturnData()">
<% if (Request("RESET") = 1) then %>
	<DIV id="LoaderDivCalendar">
	<!--#include file="Calendar_Create.asp" -->
	</DIV>
<% end if %>	
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>