<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="Calendar.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim mypage, numpages, prevpage, nextpage, numrecs, pagesize	

	Headers = Array("Transfer Date", "From", "To", "Reference", "Withdrawn","Paid In", "&nbsp;")
	SortOrder = Array("BT.TRANSFERDATE", "ACCOUNTFROM", "ACCOUNTTO", "REFF", "THEVALUE", "NEGATIVETHEVALUE", "")
	SortOrderAsc = Array("black","black","black","black","black","black","black","black","black")	
	SortOrderDesc = Array("black","black","black","black","black","black","black","black","black")		
	HeaderWidths = Array("145", "180", "180", "100", "100", "100", "")
			
	detotal_debit = Request("detotal_debit")
	if detotal_debit = "" Then detotal_debit = 0 end if

	detotal_credit = Request("detotal_credit")
	if detotal_credit = "" Then detotal_credit = 0 end if

	selected = Request("idlist") // get selected checkboxes into an array
	selected_data = "," & Replace(selected, " ", "") & ","


	todate = Request("txt_TO")
	if todate = "" Then 
		to_sql = "" 
	else
		to_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,BT.TRANSFERDATE,103),103) <= '" & FormatDateTime(todate,1) & "' "
	end if

	fromdate = Request("txt_FROM")
	if fromdate = "" Then 
		from_sql = "" 
	else
		from_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,BT.TRANSFERDATE,103),103) >= '" & FormatDateTime(fromdate,1) & "' "
	end if

	orderBy = Request("orderBy")
	Call SetSort()
	
	TheNextPage_NoOrder = "txt_FROM=" & fromdate & "&txt_TO=" & todate
	TheNextPage = "orderBy=" & orderBy & "&" & TheNextPage_NoOrder
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CLng(mypage)
		end if
		
		Call TopBit()
				
		cnt = 0
		strSQL = 	"SET NOCOUNT ON;DECLARE @CURRENTACCOUNT INT; " &_
					"SET @CURRENTACCOUNT = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTID'); " &_
					"SELECT BT.BLTID, BT.TRANSFERDATE, BT.REFF, BT.MEMO, TRANSFERDATE2 = CONVERT(VARCHAR, BT.TRANSFERDATE, 103), " &_
					"	   A1.ACCOUNTNUMBER + ' - ' + A1.[DESC] AS ACCOUNTFROM, " &_
					"	   A2.ACCOUNTNUMBER + ' - ' + A2.[DESC] AS ACCOUNTTO, " &_
					"	   BT.AMOUNT, " &_
					"	   THEVALUE = CASE WHEN BT.ACCOUNTFRM = @CURRENTACCOUNT THEN BT.AMOUNT " &_
					"	   ELSE -BT.AMOUNT END, " &_
					"	   NEGATIVETHEVALUE = CASE WHEN BT.ACCOUNTFRM = @CURRENTACCOUNT THEN -BT.AMOUNT " &_
					"	   ELSE BT.AMOUNT END " &_					
					"	FROM NL_BALANCETRANSFER BT " &_
					"	INNER JOIN NL_ACCOUNT A1 ON A1.ACCOUNTID = BT.ACCOUNTFRM " &_
					"	INNER JOIN NL_ACCOUNT A2 ON A2.ACCOUNTID = BT.ACCOUNTTO " &_
					"	WHERE (ACCOUNTFRM = @CURRENTACCOUNT OR ACCOUNTTO = @CURRENTACCOUNT) " &_
					"			AND NOT EXISTS (SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 4 AND RECCODE = BT.BLTID) " &_
					"			" & from_sql & to_sql & " " &_
					"ORDER		BY " & orderBy & ", BT.BLTID ASC"
		response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 12
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		Set regEx = New RegExp 

		
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if check box has been previously selected
			isselected = ""
			trstyle = ""
			BALANCETRANSFERID = Rs("BLTID")
			ListSearch = "," & BALANCETRANSFERID & ","
			Comparison = InStr(1, selected_data, ListSearch, 0)
			If NOT isNULL(Comparison) AND Comparison > 0  Then
					isselected = " checked "
					trstyle = " style='background-color:steelblue;color:white'"
			End If
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1

			accountfrom = Rs("accountfrom")
			accountfromtitle = ""
			if (Not isNull(Rs("accountfrom"))) then
				if (Len(Rs("accountfrom")) > 20) then
					accountfrom = left(Rs("accountfrom"),17) + "..."
					accountfromtitle = Rs("accountfrom")
				end if
			end if

			accountto = Rs("accountto")
			accounttotitle = ""
			if (Not isNull(Rs("accountto"))) then
				if (Len(Rs("accountto")) > 20) then
					accountto = left(Rs("accountto"),17) + "..."
					accounttotitle = Rs("accountto")
				end if
			end if
			
			str_data = str_data & 	"<TR " & trstyle & "><TD>" & Rs("TRANSFERDATE2") & "</TD>" &_
						"<TD title=""" & accountfromtitle & """>" &  accountfrom & "</TD>" &_
						"<TD title=""" & accounttotitle & """>" &  accountto & "</TD>" &_						
						"<TD title=""" & Rs("MEMO") & """>" & Rs("REFF") & "</TD>" 						
			amount = Rs("THEVALUE")
			if (amount < 0) then
				iType = 1
				amount = -amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				iType = 0						
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & BALANCETRANSFERID & "' value='" & BALANCETRANSFERID & "' onclick='do_sum("& BALANCETRANSFERID &"," & FormatNumber(amount,2,-1,0,0)& "," & iType & ")'></TD>" &_
						"</TR>"
			
			Rs.movenext()
			End If
		Next

		Rs.close()
		Set Rs = Nothing
		
		Call BottomBit(4)

	End function
	
	if (Request("RESET") = 1) then
%>	
	<!--#include file="Balance_Server.asp" -->
<%
	end if	
%>
<html>
<head>
	<!-- #include file="Balance_Server_JS.asp" -->
<script language=javascript>
function ReturnData(){
	<% if (Request("RESET") = 1) then %>
		parent.CalendarHolder.innerHTML = LoaderDivCalendar.innerHTML				
		parent.reset_client_totals()
		SendBalances()		
	<% end if %>
	parent.MainDiv.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
</head>
<body onload="ReturnData()">
<% if (Request("RESET") = 1) then %>
	<DIV id="LoaderDivCalendar">
	<!--#include file="Calendar_Create.asp" -->
	</DIV>
<% end if %>	
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>