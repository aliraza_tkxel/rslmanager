<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	Dim bt_id ,SQLFilter, insert_list, detotal_credit, detotal_debit, bank_statement
	Dim rec_type, method, TimeStamp, key_PaymentStartDate
	
	SQLFilter = " "
	
	' Get all the variables that are passed to this page
	
	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	
	'Set Statis variables
	
	rec_type = 7 ''This means these items are the CASH AND CHEQUE items
	method = 1 'Manual Validation
	TimeStamp = Now()
	
	if insert_list <> "" then
		OpenDB()
		InsertDebitCredit_Amount()
		CloseDB()
	End If
	
	'insert the debits and credits into table to balance the amounts
	Function InsertDebitCredit_Amount ()
		strSQL = 	"SET NOCOUNT ON;" &_	
					"	INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
					"	VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
					"	SELECT SCOPE_IDENTITY() AS BTID;"
	
		set rsSet = Conn.Execute(strSQL)
		bt_id = rsSet.fields("BTID").value
		rsSet.close()
		set rsSet = Nothing 
		
		Insert_PC_rec_Values()
	
	End Function

	
	Function Insert_PC_rec_Values()

		sel_split = Split(insert_list ,",") ' split selected string

		For each key in sel_split
			'SPLIT THE KEY

			SQL = "DECLARE @GROUPINGID INT; " &_
			  " INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
			  " SELECT 	" & bt_id & ", " & bank_statement & ", " & rec_type & ", CP.CASHPOSTINGID ,'" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID " &_ 
						" FROM	 F_CASHPOSTING CP  " &_
						"			INNER JOIN F_PAYMENTSLIP PS ON PS.PAYMENTSLIPID = CP.PAYMENTSLIPNUMBER " &_
						" WHERE NOT EXISTS" &_
						"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 7 AND RECCODE = CP.CASHPOSTINGID) " &_
						" AND PS.PAYMENTSLIPID = " & key & " "
			Conn.Execute(SQL)
		next
	End Function

	Response.Redirect "BankCCValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") & "&txt_SLIPNO=" & Request("txt_SLIPNO")
%>
