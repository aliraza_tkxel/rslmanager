<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	
	OpenDB()

	strSQL = 	"SET NOCOUNT ON;" &_	
		"INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
		"VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
		"SELECT SCOPE_IDENTITY() AS BTID;"

	set rsSet = Conn.Execute(strSQL)
	bt_id = rsSet.fields("BTID").value
	rsSet.close()
	set rsSet = Nothing 
	
	sel_split = Split(insert_list ,",") ' split selected string
	rec_type = 6 'This means these items are from the misc income THINGA MA BOB
	TimeStamp = Now()
	method = 1 'Manual Validation
	
	For each key in sel_split
		
		strSQL = "DECLARE @CURRENTACCOUNT INT; " &_
				"SET @CURRENTACCOUNT = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTID'); " &_
				"DECLARE @GROUPINGID INT; " &_
			  " INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
					"SELECT " & bt_id & ", " & bank_statement & ", " & rec_type & ", DTL.ID, '" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID FROM NL_MISCELLANEOUS_HDR HDR " &_
						" INNER JOIN NL_MISCELLANEOUS_DTL DTL ON DTL.HDRID = HDR.ID " &_      
						"	INNER JOIN NL_ACCOUNT NL1 ON NL1.ACCOUNTID = HDR.BANK " &_
						"	INNER JOIN NL_ACCOUNT NL2 ON NL2.ACCOUNTID = DTL.NLACCOUNT " &_
						"WHERE (NL1.ACCOUNTID = @CURRENTACCOUNT OR NL2.ACCOUNTID = @CURRENTACCOUNT) " &_					
						" AND HDR.ID = " & key & " AND NOT EXISTS " &_
						"	(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 6 AND RECCODE = DTL.ID) " 
		rw STRsql			
		Conn.Execute(strSQL)
		
	Next

	CloseDB()

	Response.Redirect "BankMIValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO")
%>
