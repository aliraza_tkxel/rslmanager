<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpeningBalance = Request("txt_STARTINGBALANCE")
	ClosingBalance = Request("txt_ENDINGBALANCE")
	StatementDate = Request("txt_STATEMENTDATE")
	
	Call OpenDB()	
	SQL = "SELECT STATEMENTDATE FROM F_BANK_STATEMENTS WHERE STATEMENTDATE = '" & FormatDateTime(StatementDate,1) & "'"
	Call OpenRs(rsExistsAlready, SQL)
	if (rsExistsAlready.EOF) then
		SQL = "SET NOCOUNT ON;INSERT INTO F_BANK_STATEMENTS (STATEMENTDATE, STARTINGBALANCE, ENDINGBALANCE) VALUES ( " &_
				"'" & FormatDateTime(StatementDate,1) & "'," & OpeningBalance & ", " & ClosingBalance & ");" &_
				"SELECT SCOPE_IDENTITY() AS BANKID;SET NOCOUNT OFF;"
		Call OpenRs(rsINS2, SQL)
		BSID = rsINS2("BANKID")
		Call CloseRs(rsINS2)
		Call CloseRs(rsExistsAlready)		
		Call CloseDB()
		Response.Redirect "QuickLoadAfterSave.asp?STATEMENTID=" & BSID & "&Date=" & StatementDate
	end if
	Call CloseRs(rsExistsAlready)		
	Call CloseDB()
%>
<html>
<head>
</head>
<script language="javascript">
function ReturnData(){
	alert("A statement for the date '<%=FormatDateTime(StatementDate,1)%>' already exists.\nContact Reidmark if you wish to amend the statement information.")
	}
</script>
<body onload="ReturnData()">
</body>
</html>