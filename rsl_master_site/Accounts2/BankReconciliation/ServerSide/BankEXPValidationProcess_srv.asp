<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	
	OpenDB()

	strSQL = 	"SET NOCOUNT ON;" &_	
		"INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
		"VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
		"SELECT SCOPE_IDENTITY() AS BTID;"

	set rsSet = Conn.Execute(strSQL)
	bt_id = rsSet.fields("BTID").value
	rsSet.close()
	set rsSet = Nothing 
	
	sel_split = Split(insert_list ,",") ' split selected string
	rec_type = 3 'This means these items are from the PURCHASE PAYMENT THINGA MA BOB
	TimeStamp = Now()
	method = 1 'Manual Validation
	
	For each key in sel_split
		
		strSQL = "DECLARE @GROUPINGID INT; " &_
			  " INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
					"SELECT " & bt_id & ", " & bank_statement & ", " & rec_type & ", PI.ORDERITEMID, '" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID FROM F_PURCHASEITEM PI " &_
						"INNER JOIN F_PURCHASEITEM_TO_CHQ TPIQ ON TPIQ.ORDERITEMID = PI.ORDERITEMID " &_
						"INNER JOIN F_PURCHASEITEM_CHQ PIQ ON PIQ.CHQID = TPIQ.CHQID " &_						
						" WHERE PIQ.CHQID = " & key & " AND NOT EXISTS " &_
						"	(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 3 AND RECCODE = PI.ORDERITEMID) " 
		rw STRsql			
		Conn.Execute(strSQL)
		
	Next

	CloseDB()

	Response.Redirect "BankEXPValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") & "&txt_CHEQUE=" & Request("txt_CHEQUE") 
%>	
