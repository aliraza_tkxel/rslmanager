<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/Calendar.asp" -->
<%
	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
	
	OpenDB()
	SQL = "SELECT PAYMENTTYPEID, DESCRIPTION FROM F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (3,7,17,18,22,25) ORDER BY DESCRIPTION"
	Call OpenRs (rsOptions, SQL)
	OptionString = ""
	while NOT rsOptions.EOF
		OptionString = OptionString & "<option value=""" & rsOptions("PAYMENTTYPEID") & """>" & rsOptions("DESCRIPTION") & "</OPTION>"
		rsOptions.moveNext
	wend
	Call CloseRs(rsOptions)
	
	Call BuildSelect(lst_paymentType, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (3,7,17,18,22,25,13,1,15,16) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "All", Null, "width:150px", "textbox", " disabled  onchange=""SingleChecking()"" ")
	Call BuildSelect(lst_paymentMethod, "sel_PAYMENTMETHOD", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (2,5,8,9) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "All", Null, "width:150px", "textbox", NULL)
	Call BuildSelect(lst_LA, "sel_LA", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION ", "DESCRIPTION", "Please Select", Null, "width:150px", "textbox200", "")
	CloseDB()	

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bank Reconciliation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function GetCalendar(Tdate){
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = "ServerSide/Calendar_svr.asp?Date=" + Tdate
		RSLFORM.submit();
//		var xh = new ActiveXObject("Microsoft.XMLHTTP");
//		var url = "/Finance/BankReconciliation/ServerSide/Calendar_Svr.asp?date=" + Tdate;
//		xh.open("POST",url,false);
//		xh.send();
//		CalendarHolder.innerHTML = xh.responseText;
		}

	function GetHelp(Tdate){
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = "ServerSide/Help.asp"
		RSLFORM.submit();
		}
		
	var FormFields = new Array();
	var TempToDates = new Array();
	var TempFromDates = new Array();
	var FieldNames = new Array();
	var str_idlist = ""
	var int_initial = 0;
	var detotal_debit = new Number();
	var detotal_credit = new Number();	

	FormFields[0] = "sel_VALIDATE|Validate|SELECT|Y"	
	FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|N"	
	FormFields[2] = "txt_FROM|Date From|DATE|N"	
	FormFields[3] = "txt_TO|Date To|DATE|N"			
	
	function process(){
		if (int_initial == 0) {
			alert("Please select at least one item to validate!")
			return false
			}
		answer = confirm("You are about to Validate a total of\n" + int_initial + " entries for the date : " + RSLFORM.txt_STATEMENTDATE.value + ".\n\nThe Withdrawn total is �" + FormatCurrencyComma(detotal_credit) + ".\nThe Paid In total is �" + FormatCurrencyComma(detotal_debit) + ".\nAre you sure you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")			
		if (!answer) return false;
		EL = RSLFORM.sel_VALIDATE.value
		if (EL == 3) {
			EL = RSLFORM.sel_PAYMENTTYPE.value					
			FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|Y"				
			}
		else
			FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|N"			
		if (!checkForm()) return false;
		if (RSLFORM.sel_VALIDATE.disabled == false){		
			RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
			RSLFORM.action = ProcessPage[EL] + "?detotal_credit=" + detotal_credit + "&detotal_debit=" + detotal_debit;
			RSLFORM.submit();
			}
		}
	
	function HideBalanceLines(iL){
		if (iL == 1){
			RSLFORM.txt_STARTINGBALANCE.readOnly = false
			RSLFORM.txt_ENDINGBALANCE.readOnly = false			
			Ref = document.getElementsByName("BB1")
			if (Ref.length) {
				for (i=0; i<Ref.length; i++)
					Ref[i].style.display = "none"
				} 
			document.getElementById("BB2").style.display = "block"
			}
		else {
			RSLFORM.txt_STARTINGBALANCE.readOnly = true
			RSLFORM.txt_ENDINGBALANCE.readOnly = true			
			document.getElementById("BB2").style.display = "none"
			Ref = document.getElementsByName("BB1")
			if (Ref.length) {
				for (i=0; i<Ref.length; i++)
					Ref[i].style.display = "block"
				} 
			}
		}
		 
	function NewDay(entDate){
		document.getElementById("SaveButton").disabled = false	
		HideBalanceLines(1)
		ResetBalances()
		RSLFORM.sel_VALIDATE.disabled = true
		reset_client_totals()
		MainDiv.innerHTML = ""
		RSLFORM.txt_STATEMENTDATE.value = entDate
		}
	
	function SaveDay(){
		FormFields.length = 0
		FormFields[0] = "txt_STARTINGBALANCE|Opening Balance|CURRENCY|Y"	
		FormFields[1] = "txt_ENDINGBALANCE|Closing Balance|CURRENCY|Y"	
		DidAnError = false
		if (!checkForm()) DidAnError = true
		FormFields[0] = "sel_VALIDATE|Validate|SELECT|Y"	
		FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|N"	
		FormFields[2] = "txt_FROM|Date From|DATE|N"	
		FormFields[3] = "txt_TO|Date To|DATE|N"			
		if (DidAnError == true) return false;

		RSLFORM.action = "serverside/BankStatement_Save.asp"
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.submit();
		document.getElementById("SaveButton").disabled = true
		}
			
	function LoadDay(StatementID){
		HideBalanceLines(2)		
		ResetBalances()
		RSLFORM.sel_VALIDATE.disabled = true
		reset_client_totals()
		MainDiv.innerHTML = ""
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.STATEMENTID.value = StatementID
		RSLFORM.action = "serverside/BankStatement_srv.asp"
		RSLFORM.submit();
		}
	
	function ResetBalances(){
		var BalanceList = new Array ("STATEMENTID", "txt_STARTINGBALANCE", "txt_STATEMENTDATE", "hid_STARTINGBALANCE", "hid_ENDINGBALANCE", "hid_DEBITLEFT", "hid_CREDITLEFT", "txt_DEBITLEFT", "txt_CREDITLEFT", "hid_TOTALBALANCE", "txt_TOTALBALANCE", "txt_ENDINGBALANCE")
		for (i=0; i<BalanceList.length; i++)
			document.getElementById(BalanceList[i]).value = ""
		}
			
	function ReinitializeBalances(){
		RSLFORM.txt_DEBITLEFT.value = FormatCurrencyComma(RSLFORM.hid_DEBITLEFT.value)
		RSLFORM.txt_CREDITLEFT.value = FormatCurrencyComma(RSLFORM.hid_CREDITLEFT.value)
		RSLFORM.txt_TOTALBALANCE.value = FormatCurrencyComma(RSLFORM.hid_TOTALBALANCE.value)
		}
	
	function ValidatedList(){
		if (RSLFORM.sel_VALIDATE.disabled == false)
			window.showModalDialog("Popups/ValidatedList.asp?STATEMENTID=" + RSLFORM.STATEMENTID.value  + "&STATEMENTDATE=" + RSLFORM.txt_STATEMENTDATE.value + "&Rand=" + new Date(), "RECONCILED", "dialogHeight: 350px; dialogWidth: 800px; status: No; resizable: No;");
		}
						
	function reset_client_totals(){
		RSLFORM.idlist.value = ""
		int_initial = 0;
		str_idlist = "";
		detotal_debit = 0;
		detotal_credit = 0;		
		}
	
	var GetPage = new Array()
	GetPage[1] = "serverside/BankRJValidation_svr.asp"
	GetPage[2] = "serverside/BankPOValidation_svr.asp"
	GetPage[3] = "serverside/BankHBValidation_svr.asp"
	GetPage[4] = "serverside/BankPCValidation_svr.asp"
	GetPage[5] = "serverside/BankDDValidation_svr.asp"
	GetPage[6] = "serverside/BankSPValidation_svr.asp"
	GetPage[7] = "serverside/BankEXPValidation_svr.asp"
	GetPage[8] = "serverside/BankCCValidation_svr.asp"
	GetPage[9] = "serverside/BankTRValidation_svr.asp"	
	GetPage[10] = "serverside/BankLAValidation_svr.asp"		
	GetPage[11] = "serverside/BankBTValidation_svr.asp"			
	GetPage[12] = "serverside/BankGRValidation_svr.asp"				
	GetPage[13] = "serverside/BankMIValidation_svr.asp"					
	
	var ProcessPage = new Array()
	ProcessPage[1] = "serverside/BankRJValidationProcess_srv.asp"
	ProcessPage[2] = "serverside/BankPOValidationProcess_srv.asp"
	ProcessPage[3] = "serverside/BankHBValidationProcess_srv.asp"
	ProcessPage[4] = "serverside/BankPCValidationProcess_srv.asp"
	ProcessPage[5] = "serverside/BankDDValidationProcess_srv.asp"
	ProcessPage[6] = "serverside/BankSPValidationProcess_srv.asp"
	ProcessPage[7] = "serverside/BankEXPValidationProcess_srv.asp"
	ProcessPage[8] = "serverside/BankCCValidationProcess_srv.asp"
	ProcessPage[9] = "serverside/BankTRValidationProcess_srv.asp"	
	ProcessPage[10] = "serverside/BankLAValidationProcess_srv.asp"		
	ProcessPage[11] = "serverside/BankBTValidationProcess_srv.asp"			
	ProcessPage[12] = "serverside/BankGRValidationProcess_srv.asp"				
	ProcessPage[13] = "serverside/BankMIValidationProcess_srv.asp"					
		
	var PopPage = new Array()
	PopPage[2] = "pInvoices.asp"
	PopPage[3] = "pHousingBenefit.asp"
	PopPage[4] = "pPaymentCards.asp"
	PopPage[5] = "pDirectDebits.asp"
	PopPage[6] = "pSupportPayment.asp"
	PopPage[8] = "pCashandCheque.asp"
	PopPage[9] = "pTenantRefund.asp"	
	PopPage[10] = "pLARefund.asp"		
	PopPage[11] = ""			
	PopPage[12] = ""				
	PopPage[13] = "pMIS.asp"					
	
	function GetList(){
		EL = RSLFORM.sel_VALIDATE.value
		if (EL == 3) {
			EL = RSLFORM.sel_PAYMENTTYPE.value					
			FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|Y"				
			}
		else
			FormFields[1] = "sel_PAYMENTTYPE|Payment Type|SELECT|N"
		if (!checkForm()) return false;
		reset_client_totals();
		ReinitializeBalances()
		if (RSLFORM.sel_VALIDATE.disabled == false){
			MainDiv.innerHTML = ""
			RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
			RSLFORM.action = GetPage[EL]
			RSLFORM.submit();
			}
		}
  
	// CALCULATE RUNNING TOTAL OF RENT JOURNAL SELECTIONS
	// LA_id has been included for housing benefit section only
	function do_sum(int_num, int_amount, type){
		type = parseInt(type,10)
		//alert(" Int Amount : " + int_amount + " Type " + type + " Debit Total : " + detotal_debit + " Credit Total " + detotal_credit)
		if (document.getElementById("chkpost" + int_num+"").checked == true){
			if (type == 1) 
				detotal_debit = detotal_debit + parseFloat(int_amount,10);
			else
				detotal_credit = detotal_credit + parseFloat(int_amount,10);			
			if (int_initial == 0) {// first entry
				str_idlist = str_idlist + int_num.toString();
				}
			else{
				str_idlist = str_idlist + "," + int_num.toString();
				}
			
			int_initial = int_initial + 1; // increase count of elements in string
			srcElem = window.event.srcElement;
			while (srcElem.tagName != "TR" && srcElem.tagName != "BODY")
				srcElem = srcElem.parentElement;
			if (srcElem.tagName == "TR"){
				srcElem.style.backgroundColor = "steelblue"
				srcElem.style.color = "white"
				}
			}
		else {
			if (type == 1) 
				detotal_debit = detotal_debit - parseFloat(int_amount,10);
			else
				detotal_credit = detotal_credit - parseFloat(int_amount,10);			
			int_initial = int_initial - 1;
			remove_item(int_num);

			srcElem = window.event.srcElement;
			while (srcElem.tagName != "TR" && srcElem.tagName != "BODY")
				srcElem = srcElem.parentElement;
			if (srcElem.tagName == "TR"){
				srcElem.style.backgroundColor = "white"
				srcElem.style.color = "black"
				}
			}

		CurrentBalance = parseFloat(document.getElementById("hid_TOTALBALANCE").value,10)
		
		CurrentDebits = parseFloat(document.getElementById("hid_DEBITLEFT").value,10)
		CurrentCredits = parseFloat(document.getElementById("hid_CREDITLEFT").value,10)
		if (type == 1) {
			document.getElementById("txt_POSTTOTALDEBIT").value = FormatCurrencyComma(detotal_debit);			
			document.getElementById("txt_DEBITLEFT").value = FormatCurrencyComma(CurrentDebits + detotal_debit);
			}
		else {
			document.getElementById("txt_POSTTOTALCREDIT").value = FormatCurrencyComma(detotal_credit);			
			document.getElementById("txt_CREDITLEFT").value = FormatCurrencyComma(CurrentCredits + detotal_credit);			
			}
		

		//document.getElementById("txt_TOTALBALANCE").value = FormatCurrencyComma(CurrentDebits + CurrentCredits - detotal_debit - detotal_credit)
		document.getElementById("txt_TOTALBALANCE").value = FormatCurrencyComma(CurrentBalance + detotal_debit - detotal_credit)		
		document.getElementById("idlist").value = str_idlist;
		}
		
	function TravelTo(NextPage){
		EL = RSLFORM.sel_VALIDATE.value	
		if (EL == 3) EL = RSLFORM.sel_PAYMENTTYPE.value		
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = GetPage[EL] + "?" + NextPage + "&detotal_debit=" + detotal_debit.toString() + "&detotal_credit=" + detotal_credit.toString()
		RSLFORM.submit();			
		}
		
	function PopList(ItemDate, Code){
		EL = RSLFORM.sel_VALIDATE.value
		if (EL == 3) EL = RSLFORM.sel_PAYMENTTYPE.value
		window.showModalDialog("Popups/" + PopPage[EL] + "?Date=" + ItemDate + "&Code=" + Code + " &Rand=" + new Date(), "TRANSACTIONS", "dialogHeight: 350px; dialogWidth: 800px; status: No; resizable: No;");
		}

	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){

		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
		document.getElementById("idlist").value = str_idlist;		
		}
	
	function RecType(){
		reset_client_totals()
		MainDiv.innerHTML = ""
		SetChecking();
		}

	function ShowError(){
		alert("WARNING: You should use the Rent Journal (Grouped) option to validate items of this type.\nThis option should only be used when it is neccessary to validate a single item from within\na group.\n\nNOTE: When validating single items, the grouped item count will decrease appropriately and\nthe value will change accordingly.")
		}

	function SingleChecking(){
		EL = RSLFORM.sel_PAYMENTTYPE.value
		switch (EL){
			case "1":
				ShowLines('4,6,7');
				ShowError()
				break;			
			case "13":				
				ShowLines('4,6,7');
				ShowError()					
				break;			
			case "16":			
				ShowLines('4,6,7');				
				ShowError()				
				break;			
			case "15":
				ShowLines('4,6,7');				
				ShowError()				
				break;			
			default:
				ShowLines('4,6');				
				break;			
			}
		}
		
	function GroupChecking(){
		FormFields.length = 4	
		reset_client_totals()
		MainDiv.innerHTML = ""		
		EL = RSLFORM.sel_PAYMENTTYPE.value
		switch (EL) {
			case "" :
				ShowLines('1');
				break;
			case "3" :
				ShowLines('4');				
				break;
			case "4" :
				ShowLines('1');				
				break;
			case "5" :
				ShowLines('1');				
				break;
			case "6" :
				ShowLines('1');				
				break;
			case "9" :
				ShowLines('3');				
				break;
			case "10" :
				ShowLines('3');				
				break;
			}
		}
	
	//the group	 and setchecking ids must be different, ignore the singlechecking ids
	function SetChecking(){
		FormFields.length = 4
		RSLFORM.sel_PAYMENTTYPE.disabled = true
		EL = RSLFORM.sel_VALIDATE.value
		if (EL == 3) {
			RSLFORM.sel_PAYMENTTYPE.outerHTML = GroupedPaymentTypes.value
			RSLFORM.sel_PAYMENTTYPE.disabled = false
			ShowLines('1');
			}			
		else {
			RSLFORM.sel_PAYMENTTYPE.outerHTML = SinglePaymentTypes.value		
			switch (EL) {
				case "" :
					ShowLines('1');
					break;
				case "1" :
					FormFields[FormFields.length] = "txt_TENANCYID|Tenancy Ref.|INTEGER|N"
					RSLFORM.sel_PAYMENTTYPE.disabled = false
					ShowLines('4,6');				
					break;
				case "2" :
					FormFields[FormFields.length] = "txt_CHEQUE|Cheque No.|TEXT|N"		
					ShowLines('2,3');				
					break;
				case "7" :
					FormFields[FormFields.length] = "txt_CHEQUE|Cheque No.|TEXT|N"		
					ShowLines('3');				
					break;
				case "8" :
					ShowLines('5');				
					break;
				case "11" :
					ShowLines('1');				
					break;
				case "12" :
					ShowLines('1');				
					break;
				case "13" :
					ShowLines('1');				
					break;
				}
			}
		}
				
	function ShowLines(WhichLines){
		for (i=1; i<8; i++)
			document.getElementById("Line" + i).style.display = "none"
			
		Ref = WhichLines.split(",")
		if (Ref.length){
			for (i=0; i<Ref.length; i++)
				document.getElementById("Line" + Ref[i]).style.display = "block"
			}
		}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<textarea id="SinglePaymentTypes" style='display:none'>
<SELECT NAME='sel_PAYMENTTYPE'  class='textbox' style='width:150px' disabled onchange="SingleChecking()">
<option value="">All</option>
<%=OptionString%>
<option value="13" style='background-color:red;color:white'>Payment Cards</option>			  	
<option value="1" style='background-color:red;color:white'>Housing Benefit</option>
<option value="16" style='background-color:red;color:white'>HB Back Payment</option>
<option value="15" style='background-color:red;color:white'>HB Over Payment</option>			  
</SELECT>
</textarea>
<textarea id="GroupedPaymentTypes" style='display:none'>
<SELECT NAME='sel_PAYMENTTYPE'  class='textbox' style='width:150px' disabled onchange="GroupChecking()">
	<option value=''>Please Select</OPTION>
    <option value="3">Housing Benefit</option>
    <option value="4">Payment Cards</option>
    <option value="5">Direct Debits</option>			  
    <option value="6">Support Payments/OverPayments</option>
    <option value="9">Tenant Refunds</option>			  
    <option value="10">LA Refunds</option>			  			  
</SELECT>
</textarea>

<form name=RSLFORM method=post>
<table width=100% cellspacing=0 cellpadding=2>
    <tr> 
      <td> 
<div id="CalendarHolder">
<!--#include file="ServerSide/Calendar_Create.asp" -->
</div>

</td><td>

	<TABLE BORDER=0 STYLE='BORDER:1PX SOLID SILVER;BACKGROUND-COLOR:BEIGE' CELLPADDING=2 CELLSPACING=0 height=161>
	  <TR bgcolor=#133e71>
            <TD colspan=2 style='color:white' height=20 align=center><b>Statement Information</b></TD>
	  </TR><TR>
            <TD>Date Selected : </TD>
            <TD> 
              <input type=text name="txt_STATEMENTDATE" class="textbox" readonly size=13 style='font-weight:bold;text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
            </TD>
	  </TR><TR> 
            <TD>Opening Balance : </TD>
            <TD> 
              <input type=text name="txt_STARTINGBALANCE" class="textbox" readonly size=16 style='text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
              <input type=hidden name="STATEMENTID">
              <input type=hidden value="" id="idlist" name="idlist">			  
              <input type=hidden name="hid_STARTINGBALANCE">
              <input type=hidden name="hid_ENDINGBALANCE">			  
              <input type=hidden name="hid_DEBITLEFT">
              <input type=hidden name="hid_CREDITLEFT">
              <input type=hidden name="hid_TOTALBALANCE">
              <input type=hidden name="TIMESTAMP" VALUE="<%=TIMESTAMP%>">			  
            </TD>
	  </TR><TR>
            <TD>Closing Balance : </TD>
            <TD> 
              <input type=text name="txt_ENDINGBALANCE" class="textbox" readonly size=16 style='text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
            </TD>
	  </TR><TR> 
			<td height=100%></td>
	  </TR><TR id="BB1">						
			<TD>Withdrawn :</TD>
            <TD> 
              <input type=text name="txt_CREDITLEFT" class="textbox" readonly size=16 style='text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
            </TD>
	  </TR><TR id="BB1">			
			<TD>Paid In :</TD>
            <TD> 
              <input type=text name="txt_DEBITLEFT" class="textbox" readonly size=16 style='text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
            </TD>
	  </TR><TR id="BB1">						
			<TD>Balance Left :</TD>
            <TD> 
              <input type=text name="txt_TOTALBALANCE" class="textbox" readonly size=16 style='text-align:right;border:none;border-bottom:1px solid silver;background-color:beige'>
            </TD>			
	  </TR><TR id="BB2" style='display:none'>						
			<TD></TD>
            <TD align=right> 
			<img src="/js/FVS.gif" name="img_STARTINGBALANCE" width=15 height=15>
			<img src="/js/FVS.gif" name="img_ENDINGBALANCE" width=15 height=15>			
              <input type=button class="RSLButton" value="SAVE" ONCLICK="SaveDay()" id="SaveButton">
            </TD>			
		</tr>	  
	</TABLE>

</TD><TD>

	<TABLE BORDER=0 STYLE='BORDER:1PX SOLID SILVER;BACKGROUND-COLOR:BEIGE' CELLPADDING=2 CELLSPACING=0 height=161>
		<tr bgcolor=#133e71> 
		  <td align=center height=20 style='color:white'><b>RECONCILIATION</b></td>
		</tr>
		<tr> 
		  <td nowrap><b>Validate : </b> &nbsp; <BR>
			<input type=hidden name="hid_PREVVALIDATE" value="1">
			<select name="sel_VALIDATE" class="textbox" style='width:150px' onChange="RecType()" disabled>
			  <option value="">Please Select</option>
			  <option value="1">Rent Journal (Single)</option>
			  <option value="3">Rent Journal (Grouped)</option>
			  <option value="8">Cash & Cheque Posting</option>			  				  
			  <option value="2">Invoice Payments</option>
		 	  <option value="7">Staff Expenses</option>
		 	  <option value="11">Bank Transfer</option>			  
		 	  <option value="12">Grants</option>			  			  
		 	  <option value="13">Miscellaneous Income</option>			  			  			  
			</select>
			<img src="/js/FVS.gif" name="img_VALIDATE" width=15 height=15>
		  </td>
		</tr><tr>
		  <td><b>Payment Type : </b><br><TABLE CELLSPACING=0 CELLPADDING=0><TR><TD>
			<SELECT NAME='sel_PAYMENTTYPE'  class='textbox' style='width:150px' disabled onchange="SingleChecking()">
			<option value="">Please Select</option>			  
			</SELECT>		  
		  </TD><TD>
			<img src="/js/FVS.gif" name="img_PAYMENTTYPE" width=15 height=15></TD></TR></TABLE>		  
		  </td>
		</tr>
		<tr>
		  <td><b>Date From : </b><br>
			<input type=TEXT name=txt_FROM class='TEXTBOX100' style="width:150px">
			<img src="/js/FVS.gif" name="img_FROM" width=15 height=15>
			</td>
		</tr><tr>
		  <td><b>Date To :</b><br>
			<input type=TEXT name=txt_TO class='TEXTBOX100' style="width:150px">
			<img src="/js/FVS.gif" name="img_TO" width=15 height=15>
			</td>
		</tr>
	</TABLE>

</TD><TD>

	<TABLE BORDER=0 STYLE='BORDER:1PX SOLID SILVER;BACKGROUND-COLOR:BEIGE' CELLPADDING=2 CELLSPACING=0 height=161 width=180>
		<tr bgcolor=#133e71> 
		  <td align=center height=20 style='color:white'><b>FILTERS</b></td>
		</tr>
		<tr id="Line1"> 
		  <td>
		     <br><b>NO FILTERS FOUND </b><br>
		  </td>
		</tr>
		<tr id="Line2" style='display:none'> 
		  <td>
		     <b>Payment Method : </b><br>
			 <%=lst_paymentMethod%> 
		  	 <image src="/js/FVS.gif" name="img_PAYMENTMETHOD" width="15px" height="15px" border="0"> 
		  </td>
		</tr>
		<tr id="Line3" style='display:none'>
		  <td> 
		    <b>Cheque No. :</b><br>
			<input type=TEXT name=txt_CHEQUE class='TEXTBOX' style='width:150px'>
			<image src="/js/FVS.gif" name="img_CHEQUE" width="15px" height="15px" border="0"> 
		  </td>
		</tr>		
		<tr id="Line4" style='display:none'> 
		  <td>
		  	<b>Local Authority :</b> <br><%=lst_LA%> 
		  	<image src="/js/FVS.gif" name="img_LA" width="15px" height="15px" border="0"> 
		  </td>
		</tr>
		<tr id="Line5" style='display:none'> 
		  <td> 
			<b>Slip No. :</b> <br>
			<input type=TEXT name=txt_SLIPNO class='TEXTBOX' style="width:150px">
			<image src="/js/FVS.gif" name="img_SLIPNO" width="15px" height="15px" border="0"> 
		  </td>
		</tr>
		<tr id="Line6" style='display:none'> 
		  <td> 
			<b>Tenancy Ref. :</b> <br>
			<input type=TEXT name=txt_TENANCYID class='TEXTBOX' style="width:150px">
			<image src="/js/FVS.gif" name="img_TENANCYID" width="15px" height="15px" border="0"> 
		  </td>
		</tr>		
		<tr id="Line7" style='display:none'> 
		  <td align=center> 
			<font color=red><b>WARNING!</b></font><br><a href="javascript:ShowError()"><font color=red>Click here for info.</font></a>
		  </td>
		</tr>		
		<tr> 
			<td height=100%></td>
		</tr>
		<tr> 
		  <td align=right> 
			<input type="button" value=" HELP " class="RSLButton"  onClick="GetHelp()">
			<input type="button" value=" LIST " class="RSLButton"  onClick="ValidatedList()">			
			<input type="button" value=" FIND " class="RSLButton"  onClick="GetList()">
			<image src="/js/FVS.gif" name="img_DUMMY" width="15px" height="15px" border="0"> 			
		  </td>
		</tr>
	  </table>
	
</TD></TR></TABLE>
		
<BR>
<DIV ID=MainDiv></DIV>

</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name="BankFrame<%=TIMESTAMP%>" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

