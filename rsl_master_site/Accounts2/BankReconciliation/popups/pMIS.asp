<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim totaldebit, totalcredit, DebitCreditCode

	Headers = Array("Date", "Ref.", "From", "To", "Slip", "Withdrawn","Paid In")
	SortOrder = Array("", "", "", "", "", "", "")
	HeaderWidths = Array("105", "100", "180", "180", "100", "100", "100")
			'
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		Call TopBit()
		
		cnt = 0
		strSQL =	"SET NOCOUNT ON;DECLARE @CURRENTACCOUNT INT; " &_
					"SET @CURRENTACCOUNT = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'BANKCURRENTACCOUNTID'); " &_ 
					"SELECT HDR.ID, HDR.MISDATE, NL1.FULLNAME,  NL2.FULLNAME AS FULLNAME2, HDR.ACCFROM, HDR.BSLIP, " &_
					"	THEVALUE = CASE WHEN NL1.ACCOUNTID =  @CURRENTACCOUNT THEN DTL.NETAMOUNT " &_
					"	ELSE -DTL.NETAMOUNT END " &_
					"FROM NL_MISCELLANEOUS_HDR HDR " &_
					"	INNER JOIN NL_MISCELLANEOUS_DTL DTL ON DTL.HDRID = HDR.ID " &_                            
					"	INNER JOIN NL_ACCOUNT NL1 ON NL1.ACCOUNTID = HDR.BANK " &_
					"	INNER JOIN NL_ACCOUNT NL2 ON NL2.ACCOUNTID = DTL.NLACCOUNT " &_
					"WHERE (NL1.ACCOUNTID = @CURRENTACCOUNT OR NL2.ACCOUNTID = @CURRENTACCOUNT) " &_
					"	AND HDR.ID = " & REQUEST("Date") & " "

		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
		
		debittotal = 0
		credittotal = 0	
		while NOT rs.EOF 
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			accountfrom = Rs("fullname")
			accountfromtitle = ""
			if (Not isNull(Rs("fullname"))) then
				if (Len(Rs("fullname")) > 20) then
					accountfrom = left(Rs("fullname"),17) + "..."
					accountfromtitle = Rs("fullname")
				end if
			end if

			accountto = Rs("FULLNAME2")
			accounttotitle = ""
			if (Not isNull(Rs("FULLNAME2"))) then
				if (Len(Rs("FULLNAME2")) > 20) then
					accountto = left(Rs("FULLNAME2"),17) + "..."
					accounttotitle = Rs("FULLNAME2")
				end if
			end if
						 
			str_data = str_data & "<TR><TD>" & Rs("MISDATE") & "</TD>" &_
						"<TD>" & MIIncome(Rs("ID")) & "</TD>" &_						
						"<TD title=""" & accountfromtitle & """>" &  accountfrom & "</TD>" &_
						"<TD title=""" & accounttotitle & """>" &  accountto & "</TD>" &_						
						"<TD TITLE=""" & Rs("ACCFROM") & """>" & Rs("BSLIP") & "</TD>"						
			amount = Rs("THEVALUE")
			if (amount < 0) then
				amount = -amount
				totaldebit = totaldebit + amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				totalcredit = totalcredit + amount
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
			str_data = str_data & "</TR>"
		Rs.movenext()
		Wend

		str_data = str_data & "<TR><TD colspan=5 align=right style='border-top:1px solid black'><b>Total</b></TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totalcredit) & "</b>&nbsp;</TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totaldebit) & "</b>&nbsp;</TD></TR>"					

		if (totaldebit-totalcredit > 0) then
			DebitCreditCode = "DR"
		else
			DebitCreditCode = "CR"
		end if
				
		str_data = str_data & "</TABLE>"
		Rs.close()
		Set Rs = Nothing

	End function
	
%>
<html>
<head><title>Invoice Payment List</title></head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language=javascript>
<!--
function CloseWindow(){
	window.close()
	}
	
function doPrint(){  
	 if(self.print){
	   	 document.getElementById("PrintButton").style.display = "none";
	   	 document.getElementById("CloseButton").style.display = "none";		 
		 self.print();  
		 self.close();  
		 return False;  
 		}  
	 else if (navigator.appName.indexOf(' Microsoft') != -1){  
		 VBPrint();  
		 self.close();  
		 }  
 	else{  
	 alert("To print this document, you will need to\nclick the Right Mouse Button and select\n' Print'");  
 	}  
 }  
//-->  	
</script>
<body leftmargin="0" topmargin="0" style='padding:10px'>
<object id="WBControl" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object> 
<script language="VBScript">
<!--  
 Sub VBPrint() On Error Resume next  
	 WBControl.ExecWB 6,1  
 End Sub  
//-->
</script>  

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td>
  	Below are the individual transaction items for the selected '<b>Miscellaneous Income</b>' transaction. The 'Miscellaneous Income' transaction contains one 
	or more items, which are displayed below. The chosen transaction(s) are based 
	on '<%=MIIncome(request("Date"))%>'. <br><br>Total Lines : <b><%=cnt%></b> with a total value of <b><%=FormatCurrency(abs(totaldebit-totalcredit))%></b>&nbsp;&nbsp;(<%=DebitCreditCode%>)</td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td><%=str_data%></td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td align="right"> 
	<input type="button" value="Print" class="RSLButton" onClick="doPrint()" id="PrintButton">
	<input type="button" value="Close" class="RSLButton" onClick="CloseWindow()" id="CloseButton">&nbsp;
  </td>
</tr>
</table>
</body>
</html>