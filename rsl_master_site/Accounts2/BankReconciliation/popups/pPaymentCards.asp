<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="../includes/BRFunctions.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim totaldebit, totalcredit, DebitCreditCode

	Headers = Array("Payment Card Date", "Item Type", "Payment Type", "Withdrawn", "Paid In")
	SortOrder = Array("", "","","", "", "")
	HeaderWidths = Array("160","150","250","90", "90", "")
			'
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		Call TopBit()
		
		cnt = 0
		strSQL = 	"SELECT 	RJ.JOURNALID, RJ.TENANCYID, CONVERT(VARCHAR,RJ.PAYMENTSTARTDATE, 103) AS TRANSDATE, P.DESCRIPTION AS PAYMENTTYPE, " &_
					"			ISDEBIT, ISNULL(RJ.AMOUNT,0) AS AMOUNT, TS.DESCRIPTION AS STATUS, I.DESCRIPTION AS ITEMTYPE " &_
					"FROM	 	F_RENTJOURNAL RJ " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = RJ.ITEMTYPE " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS TS ON TS.TRANSACTIONSTATUSID = RJ.STATUSID " &_
					"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
					"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
					"			INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
					"WHERE		RJ.STATUSID <> 1  " &_
					" 			AND P.PAYMENTTYPEID = 13 AND NOT EXISTS" &_
					"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
					"			AND RJ.PAYMENTSTARTDATE = '" & REQUEST("Date") & "' " &_
					" ORDER		BY RJ.JOURNALID ASC"
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()

		debittotal = 0
		credittotal = 0				
		while NOT rs.EOF 
			cnt = cnt + 1
			
			'START APPENDING EACH ROW TO THE TABLE
			paymenttype = Rs("PAYMENTTYPE")
			paymenttypetitle = ""
			if (NOT isNull(paymenttype)) then
				if (len(paymenttype) > 20) then
					paymenttypetitle = " title='" & paymenttype& "' "
					paymenttype = LEFT(paymenttype,20) & "..."
				end if
			end if
			 
			str_data = str_data & "<TR><TD>" & Rs("TRANSDATE") & "</TD>" &_
						"<TD>" & Rs("ITEMTYPE") & "</TD>" &_
						"<TD " & paymenttypetitle & ">" & paymenttype & "</TD>"
			amount = Rs("AMOUNT")
			if (amount < 0) then
				amount = -amount
				totaldebit = totaldebit + amount				
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				totalcredit = totalcredit + amount			
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
			str_data = str_data & "</TR>"
		Rs.movenext()
		Wend

		str_data = str_data & "<TR><TD colspan=3 align=right style='border-top:1px solid black'><b>Total</b></TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totalcredit) & "</b>&nbsp;</TD>" &_
					"<TD style='border-top:1px solid black;border-left:1px dotted #133e71' align=right><b>" & FormatCurrency(totaldebit) & "</b>&nbsp;</TD></TR>"					

		if (totaldebit-totalcredit > 0) then
			DebitCreditCode = "DR"
		else
			DebitCreditCode = "CR"
		end if
		
		str_data = str_data & "</TABLE>"
		Rs.close()
		Set Rs = Nothing

	End function
	
%>
<html>
<head><title>Payment Card List</title></head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language=javascript>
<!--
function CloseWindow(){
	window.close()
	}
	
function doPrint(){  
	 if(self.print){
	   	 document.getElementById("PrintButton").style.display = "none";
	   	 document.getElementById("CloseButton").style.display = "none";		 
		 self.print();  
		 self.close();  
		 return False;  
 		}  
	 else if (navigator.appName.indexOf(' Microsoft') != -1){  
		 VBPrint();  
		 self.close();  
		 }  
 	else{  
	 alert("To print this document, you will need to\nclick the Right Mouse Button and select\n' Print'");  
 	}  
 }  
//-->  	
</script>
<body leftmargin="0" topmargin="0" style='padding:10px'>
<object id="WBControl" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object> 
<script language="VBScript">
<!--  
 Sub VBPrint() On Error Resume next  
	 WBControl.ExecWB 6,1  
 End Sub  
//-->
</script>  

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
  <td>
  	Below are the individual transaction items for the selected '<b>Payment 
	Card</b>' transaction. The 'Payment Card' transaction contains one 
	or more items, which are displayed below. The chosen transaction(s) are based 
	on '<%=request("Date")%>'. <br><br>Total Lines : <b><%=cnt%></b> with a total value of <b><%=FormatCurrency(abs(totaldebit-totalcredit))%></b>&nbsp;&nbsp;(<%=DebitCreditCode%>)</td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td><%=str_data%></td>
</tr>
<tr> 
  <td>&nbsp;</td>
</tr>
<tr> 
  <td align="right"> 
	<input type="button" value="Print" class="RSLButton" onClick="doPrint()" id="PrintButton">
	<input type="button" value="Close" class="RSLButton" onClick="CloseWindow()" id="CloseButton">&nbsp;
  </td>
</tr>
</table>
</body>
</html>