<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Finance - Development Report</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/formValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function ReportOn(HEADID) {
	ReportFrame.location.href = "Serverside/DevelopmentReport_svr.asp?HEADID=" + HEADID
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopSpecial.asp" -->
  <TR> 
    <TD HEIGHT=100% STYLE='BORDER-LEFT:1PX SOLID #7E729E' width=301> 
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td style='border-top:1px solid #FFFFFF;border-right:1px solid #7E729E;background-color:beige' height=100% width=100% valign=top nowrap> 
		  <div style='overflow:auto;height:100%;padding-left:5px;scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;'>
		  <table>
		  <tr><td><br><b><u>DEVELOPMENTS</u></b></td></tr>
		  <%
		  Call OpenDB()
		  SQL = "SELECT HEADID, DESCRIPTION FROM F_HEAD WHERE COSTCENTREID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID') ORDER BY DESCRIPTION"
		  Call OpenRs(rsHE, SQL)
		  while NOT rsHE.EOF
		  	Response.Write "<TR onclick=""ReportOn(" & rsHE("HEADID") & ")""><TD style=""cursor:hand""><font color=blue>" & rsHE("DESCRIPTION") & "</font></TD></TR>"	
			rsHE.moveNext
		  wend
		  Call CloseRs(rsHE)
		  Call CloseDB()
		  %>
		  </table>
		  </div>
         </td>
        </tr>
        <tr> 
          <TD height=1 BGCOLOR=#FFFFFF align=right valign=middle></TD>
        </tr>
        <tr> 
          <TD height=28 BGCOLOR=#133e71 align=right valign=middle><b>&nbsp;</b></TD>
        </tr>
      </table>
    </TD>
    <td HEIGHT=100% width=476> 
      <form name=thisForm method=post>
        <table cellspacing=0 cellpadding=0 height=100% width=100%>
          <tr> 
            <td width=100% colspan=2 STYLE='BORDER-RIGHT:1PX SOLID #7E729E' align=center valign=top class="RSLBlack"> 
		<!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td  height=100% width=100% valign=top nowrap> 
            <iframe name=ReportFrame id=ReportFrame width=100% height=90% frameborder=none bordercolor=#FFFFFF></iframe> 
          </td>
        </tr>
	  </table>              
			  
			  </td>
          </tr>
          <tr> 
            <TD HEIGHT=44 width=100% align=right>&nbsp;</TD>
            <td rowspan=2><IMG SRC="/myImages/My113.gif" WIDTH=72 HEIGHT=72></td>
          </tr>
          <TR> 
            <TD height=28 BGCOLOR=#133e71 align=right class="RSLWhite" valign=middle><b>rslManager 
              is a Reidmark eBusiness System</b></TD>
          </TR>
        </table>
		<input type=hidden name=txt_dtStart>
		<input type=hidden name=txt_endDate> 
		<input type=hidden name=txt_endDate> 
		<input type=hidden name=txt_totValue>
		<input type=hidden name=txt_OPBalance>
		<input type=hidden name=hdn_OPBalance>
      </form>
    </TD>
  </TR>
</TABLE>
<iframe name=FB style='display:none;width:800px;height:500px'></iframe> 
</body>
</html>

