<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	OpenDB()
	
	Dim lstApprovedBy
	
	DISTINCT_SQL = " F_COSTCENTRE CC INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
					"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID "&_
					"INNER JOIN F_Head HD ON HD.HEADID = HE.HEADID "&_
					"WHERE (CC.ACTIVE = 1) AND (CC.COSTCENTREID = 9) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1)"
		
	Call BuildSelect(sel_Expenditure, "sel_Expenditure", DISTINCT_SQL, "DISTINCT EX.DESCRIPTION, EX.DESCRIPTION", "EX.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='DisplayLimit()' style='width:265px'")	
	CloseDB()
%>
<html>
<head>
<title>Expenditure Limits</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript src="/js/formValidation.js"></script>
<script language=javascript>

	var ItemArray = new Array(<%=ItemArray%>)

	var FormFields = new Array()
	FormFields[0] = "sel_TEAMS|Team|SELECT|Y"
	FormFields[1] = "sel_ASSIGNTO|Employee|SELECT|Y"
	FormFields[2] = "txt_LIMIT|Limit|CURRENCY|Y"

	function CreateLimit(){

		if (RSLFORM.sel_ASSIGNTO.value != ""){
		
			RSLFORM.hid_ACTION.value = "NEW"
			RSLFORM.action = "../Serverside/DevelopmentLimitList_svr.asp"
			RSLFORM.target = "ServerLimitFrame"
			RSLFORM.submit()
			
			}
		else alert("You have not selected an employee. Please select one from the Employee select box.")
			
		}

	function DeleteMe(T_ID, E_ID, DESC, LIMIT){

		RSLFORM.hid_ACTION.value = "DELETE"
		RSLFORM.hid_EMPLOYEELIMITID.value = T_ID
		RSLFORM.hid_EXPENDITUREID.value = E_ID
		RSLFORM.hid_ExpenditureName.value = DESC
		RSLFORM.hid_LIMIT.value = LIMIT
		RSLFORM.action = "../Serverside/DevelopmentLimitList_svr.asp"
		RSLFORM.target = "ServerLimitFrame"
		RSLFORM.submit()
		
		}

	function GetEmployees(){
	
		if (RSLFORM.sel_TEAMS.value != "") {
			RSLFORM.hid_ACTION.value = "1"
			RSLFORM.action = "../../MyTasks/Serverside/GetEmployeesForExpenditures.asp";
			RSLFORM.target = "ServerLimitFrame";
			RSLFORM.submit();
			}
		else {
			RSLFORM.sel_ASSIGNTO.outerHTML = "<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX'><option value=''>Please select a team</option></select>";
			}
		}
	
	function populatelistbox(values, thetext, which){
	
	values = values.replace(/\"/g, "");
	thetext = thetext.replace(/\"/g, "");
	values = values.split(";;");
	thetext = thetext.split(";;");
		
		if (which == 2)
			var selectlist = document.forms.RSLFORM.sel_HEADID;
		else 
			var selectlist = document.forms.RSLFORM.sel_Expenditure;	
			selectlist.length = 0;
	
			for (i=0; i<thetext.length;i++){
				var oOption = document.createElement("OPTION");
	
				oOption.text=thetext[i];
				oOption.value=values[i];
				selectlist.options[selectlist.length]= oOption;
				}
	}
	
	
	function GenerateExpenditures(){

		RSLFORM.hid_ACTION.value = "LOAD"
		RSLFORM.action = "../Serverside/DevelopmentLimitList_svr.asp"
		RSLFORM.target = "ServerLimitFrame"
		RSLFORM.submit()
		}
		
		function DisplayLimit(){
		RSLFORM.hid_ExpenditureName.value = RSLFORM.sel_Expenditure.options(RSLFORM.sel_Expenditure.selectedIndex).text
		RSLFORM.theAddButton.disabled = false
	}
	
</script>
</head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000">
<table HEIGHT=100% WIDTH=100% cellspacing=5>
  <tr> 
    <td> 
      <TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
        <tr> 
          <td valign="top" height="20"> 
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
              <TR> 
                <TD ROWSPAN=2 width="79"><img src="../Images/set%2Blimits.gif" width="115" height="20"></TD>
                <TD HEIGHT=19></TD>
              </TR>
              <TR> 
                <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <tr> 
          <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
            <table>
              <FORM name="RSLFORM" method="POST">
                <tr> 
                  <td colspan=3> 
                    <p><b>How to:<br>
                      </b> 1. Find the Employee<br>
                      2. Find the expenditure<br>
                      3. Enter a limit for expenditure<br>
                      4. Click ADD.</p>
                    
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td><b>Team :</b></td>
                  <td> 
                    <select name="sel_TEAMS" class="textbox200" onChange="GetEmployees()" STYLE='WIDTH:258PX'>
                      <option value="">Please Select</option>
                      <%
							Dim rsTeams
							OpenDB()
							SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM WHERE TEAMID <> 1 ORDER BY TEAMNAME"
							Call OpenRs(rsTeams, SQL)							
							While (NOT rsTeams.EOF)
							%>
                      <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
                      <%
							  rsTeams.MoveNext()
							Wend
							If (rsTeams.CursorType > 0) Then
							  rsTeams.MoveFirst
							Else
							  rsTeams.Requery
							End If
							CloseRs(rsTeams)
							CloseDB()
							%>
                      <option value='NOT'>No Team</option>
                    </select>
                  </td>
                  <TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>
                </tr>
                <tr> 
                  <td nowrap><b>Employee :</b></td>
                  <td> 
                    <select name="sel_ASSIGNTO" class="textbox200" STYLE='WIDTH:258PX' onChange="GenerateExpenditures()">
                      <option value="">Please select a team</option>
                    </select>
                  </td>
                  <TD><image src="/js/FVS.gif" name="img_ASSIGNTO" width="15px" height="15px" border="0"></TD>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr>
                  <td>Cost Centre:</td>
                  <td><b>Development</b></td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td>Expenditure:</td>
                  <td><%=sel_Expenditure%>
				  <input type="hidden" name="hid_ExpenditureName" value="">
				   </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr ID="showCheckbox"> 
                  <td nowrap>&nbsp;</td>
                  <td>&nbsp;</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr ID="showCheckbox"> 
                  <td nowrap>&nbsp;</td>
                  <td>(set limit accross all developments)</td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr ID="LimitRow"> 
                  <td nowrap><b>Limit : </b></td>
                  <td> 
                    <input type="text" class="textbox200" style='width:258px' name="txt_LIMIT">
                  </td>
                  <TD>&nbsp;</TD>
                </tr>
                <tr> 
                  <td colspan=2 align=right> 
                    <input name="whatAction" type='hidden' value="change" readOnly>
                    <input name="hid_EMPLOYEELIMITID" type="hidden" value="">
                    <input name="hid_EXPENDITUREID" type="hidden" value="">
					<input name="hid_LIMIT" type="hidden" value="">
                    <input name="hid_TASK" type="hidden" value="1">
                    <input name="hid_ACTION" type="hidden" value="1">
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                    <input type="button" onClick="window.close()" value=" CLOSE " class="RSLButton">
                    <input name="theAddButton" type="button" onclick="CreateLimit()" value=" ADD " class="RSLButton" disabled>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr> 
                  <td colspan=3><b>Employees with a purchase limit set for selected 
                    expenditure...</b></td>
                </tr>
                <tr> 
                  <td colspan=3 style='border-top:1px solid #133e71;'> 
                    <div id="MainDiv" style='overflow:auto;height:230;width:384' class="TA"> 
                      <table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' slcolor='' hlcolor='STEELBLUE' border=1 width=100% height=100%>
                        <tr><td align=center>Select an employee to view the list of selected expenditures.</td></tr>
						<tfoot>
						<tr><td height=100%></td></tr>
						</tfoot>
                      </table>
                    </div>
                  </td>
                </tr>
              </FORM>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<iframe name="ServerLimitFrame" style='display:none'></iframe> 
</body>
</html>
