<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%

	' Recordset to retrieve data that is relevant to HEAD ID from AmendScheme.asp
	OpenDB()
	SQL = "SELECT * FROM F_HEAD where HEADID = " & Request.Querystring("HDID")
	Call OpenRs(rsHeadDetails, SQL)
	DevelopmentName =  rsHeadDetails("DESCRIPTION")
	DevelopmentAllocation = rsHeadDetails("HEADALLOCATION")
	if (isNull(DevelopmentAllocation)) then DevelopmentAllocation = 0
	CloseRs(rsHeadDetails)

	'GET THE HEAD ID
	HDID = Request.Querystring("HDID")
	
	'THE TABLE F_DEVELOPMENTCODES STORES THE STATNDARD DEBITCODES FOR THE NOMINAL AND ALSO THE STANDARD
	'EXPENDITURE NAMES FOR A DEVELOPMENT.	
	'IT ALSO GETS THE AMOUNT SPENT AGAINST THE EXPENDITURES, SO THE USER CANNOT DECREASE THE BUDGET VALUE BELOW
	'THE MINIMUM AMOUNT
	OpenDB()
	SQL = "SELECT ISNULL(SPENT.TOTALPURCHASES,0) AS TOTALPURCHASES, DC.QBDEBITCODE, DC.QBCONTROLCODE, EX.ACTIVE, " &_
		  " CODEID, EX.EXPENDITUREID, ISNULL(DESCRIPTION, CODENAME) AS THEITEM, " &_
		  "ISNULL(EXPENDITUREALLOCATION, 0) AS EXPENDITUREALLOCATION FROM F_DEVELOPMENTCODES DC " &_
		  "LEFT JOIN F_EXPENDITURE EX ON DC.CODENAME = EX.DESCRIPTION AND EX.HEADID = "  & HDID & " " &_
		  "LEFT JOIN ( " &_
		  	"SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALPURCHASES, PI.EXPENDITUREID FROM F_PURCHASEITEM PI " &_ 
			"INNER JOIN F_EXPENDITURE EXI ON EXI.EXPENDITUREID = PI.EXPENDITUREID " &_
			"WHERE PI.ACTIVE = 1 AND EXI.HEADID = " & HDID & " " &_
			"GROUP BY PI.EXPENDITUREID " &_
			") SPENT ON SPENT.EXPENDITUREID = EX.EXPENDITUREID "
	Call OpenRs(rsCodes, SQL)
    TableString = ""
	ItemArray = ""
	while NOT rsCodes.EOF

		'If the expenditure id is null and active = 1 then check the checkbox
		CheckStatus = ""
		iF (NOT isNull(rsCodes("EXPENDITUREID"))) Then 
			if (rsCodes("ACTIVE") = True) then
				CheckStatus = "checked" 
			end if
		End if	
		ItemArray = ItemArray & """" & rsCodes("THEITEM") & ""","

		TableString = TableString & "<TR valign=middle>" &_
				 "<TD>" & rsCodes("THEITEM") &_
				 "<input type=hidden name=""txt_QBCONTROLCODE" 	& rsCodes("CodeID") & """ value=""" & rsCodes("QBCONTROLCODE") & """>" &_
				 "<input type=hidden name=""txt_QBDEBITCODE" & rsCodes("CodeID") & """ value=""" & rsCodes("QBDEBITCODE") & """>" &_
				 "<input type=hidden name=""txt_EXP" & rsCodes("CodeID")& """ value=""" & rsCodes("EXPENDITUREID") & """></TD>" &_
				 "<TD><input style=""text-align:right"" type=text class=""textbox"" name=""txt_Val" & rsCodes("CodeID") & """ value=""" & FormatNumber(rsCodes("EXPENDITUREALLOCATION"),2,-1,0,0) & """ onblur=""BlurTotal(" & rsCodes("CodeID") & ")"">" &_
				 "<input type=hidden name=""txt_Code" & rsCodes("CodeID") & """ value=""" & rsCodes("THEITEM") & """>" &_
				 "<input type=hidden name=""txt_TP" & rsCodes("CodeID") & """ value=""" & rsCodes("TOTALPURCHASES") & """>" &_				 
				 "&nbsp;<img name=""img_Val" & rsCodes("CodeID") & """ src=""/js/FVS.gif"" width=14 heiight=14></TD>" &_
				 "<TD><input type=""checkbox"" name=""iChecks"" value=""" & rsCodes("CodeID") & """ " & CheckStatus & " onclick=""BlurTotal(-1)""></TD>" &_
				 "</TR>"
		rsCodes.moveNext
	wend
	ItemArray = ItemArray & """"""
	CloseRs(rsCodes)
	CloseDB()
%>
<html>
<head>
<title>Amend Development</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript src="/js/formValidation.js"></script>
<script language=javascript>
var ItemArray = new Array(<%=ItemArray%>)

var FormFields = new Array()
FormFields[0] = "txt_SCHEME|Development Name|TEXT|Y"
FormFields[1] = "txt_RunningTotal|Total Development Cost|CURRENCY|Y"

function AmendScheme(){
	FormFields.length = 2
	
	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		for (i=0; i<CheckList.length; i++){
			FormFields[FormFields.length] = "txt_Val" + CheckList[i].value + "||TEXT|N"
			}
		}
	checkForm()

	FormFields.length = 2			
	if (CheckList.length){
		JCounter = 6
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked){
				FormFields[FormFields.length] = "txt_Val" + CheckList[i].value + "|" + ItemArray[i] + "|CURRENCY|Y"
				JCounter ++
				}
			}
		}

	if (!checkForm()) return false	

	if (CheckList.length){
		RunningTotal = 0
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked)
				RunningTotal += parseFloat(document.getElementById("txt_Val" + CheckList[i].value).value)
			}
		}
	document.getElementById("txt_RunningTotal").value = FormatCurrency(RunningTotal)
	
	RSLFORM.action = "../ServerSide/AmendScheme.asp"
	RSLFORM.target = "FB_DEV_4466"
	RSLFORM.submit()
	window.close()
	}

function Toggle() {
	if (document.getElementById("chkToggle").checked)
		checkStatus = true
	else
		checkStatus = false
			
	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		for (i=0; i<CheckList.length; i++){	
			CheckList[i].checked = checkStatus
			}
		}
	BlurTotal(-1)
	}

function BlurTotal(iType){
	if (iType != -1){
		GetMinimumAmount = parseFloat(document.getElementById("txt_TP" + iType).value,10)
		CurrentAmount = parseFloat(document.getElementById("txt_Val" + iType).value,10)		
		if (CurrentAmount < GetMinimumAmount){
			alert("The amount you entered is lower than the minimum value of " + FormatCurrency(GetMinimumAmount) + ".\nThis may be due to purchases which have already been made under this expenditure.\n\nThe value will be set to the minimum it can be.")
			document.getElementById("txt_Val" + iType).value = FormatCurrency(GetMinimumAmount)
			}
		}
	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		RunningTotal = 0
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked){
				if (!isNaN(document.getElementById("txt_Val" + CheckList[i].value).value)){
					RunningTotal += parseFloat(document.getElementById("txt_Val" + CheckList[i].value).value,10)
					document.getElementById("txt_Val" + CheckList[i].value).value = FormatCurrency(document.getElementById("txt_Val" + CheckList[i].value).value)
					}
				}
			}
		}
	document.getElementById("txt_RunningTotal").value =FormatCurrency(RunningTotal)
	}
</script>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000" style='padding-left:5px'>
<table HEIGHT=100% WIDTH=100% cellspacing=5>
  <tr>
    <td> 
      <TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
        <tr> 
          <td valign="top" height="20"> 
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
              <TR> 
                <TD ROWSPAN=2 width="79"><img src="../Images/tab_amendscheme.gif" width="122" height="20"></TD>
                <TD HEIGHT=19></TD>
              </TR>
              <TR> 
                <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <tr> 
          <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
            <table>
              <FORM name="RSLFORM" method="POST">
                <tr> 
                  <td colspan=2>Enter the development name and un-check any expenditures 
                    you do not wish to be created for the development. Finally 
                    enter a budget value for each expenditure item. 
                <tr>
                  <td colspan=2></td>
                </tr>
                <tr> 
                  <td colspan=2><b>Development Name:</b> 
                   
                 
                    <input type="text" value="<%=DevelopmentName%>" name="txt_SCHEME" class="textbox200" maxlength=50 style="width:220">
                    <img name="img_SCHEME" src="/js/FVS.gif" width=14 height=14></td>
                </tr>
              <tr> 
                  <td width="792"><b>Standard Expenditures for Development...</b>
                    <input type="checkbox" name="chkToggle" onclick="Toggle()" checked title="Toggle De/Select All">
                    &nbsp;&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=2 style='border-top:1px solid #133e71;border-bottom:1px solid #133e71'> 
                    <div style='overflow:auto;height:260;width:394' class="TA"> 
                      <table width=100% cellspacing=1 cellpadding=1>
                        <%=TableString%> 
                      </table>
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td width="792"><b>Total Predicted Dev. Cost:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type=hidden name="HDID" value="<%=Request("HDID")%>">
					<input type=hidden name="NODE" value="<%=Request("NODE")%>">					
                    <input type="text" class="textbox" name="txt_RunningTotal" readonly value="<%=FormatNumber(DevelopmentAllocation,2,-1,0,0)%>" style="text-align:right">
					<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                  </td>
                </tr>
                <tr> 
                  <td align=right>
                    <img name="img_RunningTotal" src="/js/FVS.gif" width=14 height=14>				  
                    <input type="button" onclick="AmendScheme()" value="AMEND" class="RSLButton">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;						
                  </td>
                </tr>				
              </FORM>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
