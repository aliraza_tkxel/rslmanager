<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% Response.Expires = 0 %>
<% 	bypasssecurityaccess = true 
	Dim quickfind
	quickfind = request("quickfind")
%>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Development Grants</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();
	function RowClick(DEVELOPMENTID)
	{
			location.href="GrantsLst.asp?DEVELOPMENTID=" + DEVELOPMENTID 
	

		
	}
	function process(){
		RSLFORM.target = "frm_grant";
		RSLFORM.action = "serverside/process_SatisfactionLetterList_srv.asp";
		RSLFORM.submit();
	}

	function GetList(){
		RSLFORM.target = "frm_grant";
		RSLFORM.action = "serverside/GrantDevelopmentList_srv.asp?developmentid=<%=DEVELOPMENTID%>&quickfind=<%=quickfind%>";
		RSLFORM.submit();
	}
	
	function quick_find(){
		RSLFORM.target = "frm_grant";
		RSLFORM.action = "serverside/GrantDevelopmentList_srv.asp?QUICKFIND=" + RSLFORM.quickfind.value;
		RSLFORM.submit();
	}	
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("idlist").value = str_idlist;
		
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	function open_me(WorkOrder_id){
		event.cancelBubble = true;
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = RSLFORM method=post>

	
  <TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2 width=745 align=center>
    <TR>
      <TD>Select a development below to record a grant payment receipt.</TD>
    </TR>
    <TR> 
      <TD align="right">
	  <input name="idlist" id="idlist" value="" TYPE="HIDDEN">
        <input type='BUTTON' name='btn_FIND' title='Search for matches using Supplier Name.' class='RSLBUTTON' value='Quick Find' onClick='quick_find()'>
        &nbsp;
        <input name="quickfind" id="quickfind" value="<%=request.form("quickfind")%>" TYPE="text" class="textbox200">
      </TD>
    </TR>
  </TABLE>
<CENTER>
<DIV ID=hb_div></DIV>
</CENTER>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_grant width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

