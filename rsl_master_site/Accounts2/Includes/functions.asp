<%

	Function get_currentaccount_opening_balance(YR)

		' GET TRANSACTION BALANCE UP TOAND NOT INCLUDING  STARTDAET
		SQL = "SELECT 	OPENINGBALANCE FROM RSL_DEFAULTS D " &_
				"		INNER JOIN NL_BANKACCOUNT B ON B.NOMINALACCOUNT = D.DEFAULTVALUE " &_
				"		INNER JOIN NL_BANKACCOUNT_OPENINGBALANCES BOB ON BOB.ACCOUNTID = B.ACCOUNTID AND BOB.YRANGE = "&YR&" " &_
				"WHERE 	DEFAULTNAME =  'BANKCURRENTACCOUNTID' "
		
		'rw sql
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			get_currentaccount_opening_balance = rsSet(0)
		End If
		CloseRs(rsSet)
		
	End Function
	
	Function get_cashbook_opening_balance(YR)

		Call get_lastyearend()

		if (Cint(YR) > Cint(LYE_FY)) then
			SQL_STARTDATE = LYE_STARTDATE
			SQL_ENDDATE = DateAdd("d", -1, PS)
			SQL = "SELECT ISNULL(SUM(BALANCE),0) AS BALANCE FROM ( " &_
				  "SELECT ISNULL(SUM(DEBIT),0) - ISNULL(SUM(CREDIT),0)  AS BALANCE FROM CASHBOOK_ALL A " &_
					"WHERE TRANSACTIONDATE >= '"&SQL_STARTDATE&" 00:00' AND TRANSACTIONDATE <= '"&SQL_ENDDATE&" 23:59' " &_
					"UNION ALL " &_
				  "SELECT 	BALANCE FROM RSL_DEFAULTS D " &_
					"		INNER JOIN NL_OPENINGBALANCES B ON B.ACCOUNTID = D.DEFAULTVALUE " &_
					"WHERE 	DEFAULTNAME =  'BANKCURRENTACCOUNTID' AND " &_
					"		B.YRANGE = "&LYE_FY&" " &_
					") AL"
		else		
			' GET TRANSACTION BALANCE UP TOAND NOT INCLUDING  STARTDAET
			SQL = "SELECT 	BALANCE FROM RSL_DEFAULTS D " &_
					"		INNER JOIN NL_OPENINGBALANCES B ON B.ACCOUNTID = D.DEFAULTVALUE " &_
					"WHERE 	DEFAULTNAME =  'BANKCURRENTACCOUNTID' AND " &_
					"		B.YRANGE = "&YR&" "
		end if
		
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			get_cashbook_opening_balance = rsSet(0)
		End If
		CloseRs(rsSet)
		
	End Function


	Function get_cashbook_balance(IPAGE, SD, ED, PSIZE)
		
		Dim b
		if (iPage < 1) then iPage = 1
		TOP_SQL = " TOP " & (PSIZE * (iPage-1)) & " " 

		' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
		SQL = 	"SELECT ISNULL(SUM(DEBIT) - SUM(CREDIT),0) AS BALANCE FROM " &_
				"	( SELECT " & TOP_SQL & "  ISNULL(DEBIT,0) AS DEBIT, ISNULL(CREDIT,0) AS CREDIT " &_
				"		FROM CASHBOOK_ALL " &_
				"		WHERE TRANSACTIONDATE >= '"&SD&"' AND TRANSACTIONDATE <= '"&ED&"' " &_
				"	ORDER BY TRANSACTIONDATE, THETYPE, THEID, RECCED) PAGER "
		
		Call OpenRS(rsSetGB, SQL)
		If Not rsSetGB.EOF Then b = rsSetGB("BALANCE") Else b = 0 End If
		get_cashbook_balance = b 	' BALANCE + OPENING BALANCE + PARTMONTH BALANCE
		CloseRs(rsSetGB)
		
	End Function

	Function get_statement_balance(IPAGE, SD, ED, PSIZE)
		
		Dim b
		if (iPage < 1) then iPage = 1
		TOP_SQL = " TOP " & (PSIZE * (iPage-1)) & " " 

		' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
		'SQL = 	"SELECT 	ISNULL(SUM(DEBIT),0) - ISNULL(SUM(CREDIT),0) AS BALANCE FROM " &_
		'		"(  SELECT " & TOP_SQL & "  CASE WHEN ISNULL(AMOUNT,0)  < 0 THEN  ABS(ISNULL(AMOUNT,0)) ELSE 0 END AS DEBIT, " &_
		'		"		CASE WHEN ISNULL(AMOUNT,0)  > 0 THEN  ABS(ISNULL(AMOUNT,0)) ELSE 0 END AS CREDIT " &_
		'		"		FROM BA_STATEMENT " &_
		'		"	WHERE TDATE >= '"&SD&"' AND TDATE <= '"&ED&"' " &_
		'		"	ORDER BY TDATE,  GROUPINGID ) PAGER "

		' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
		SQL = 	"SELECT ISNULL(SUM(DEBIT) - SUM(CREDIT),0) AS BALANCE FROM " &_
				"	( SELECT " & TOP_SQL & "  ISNULL(DEBIT,0) AS DEBIT, ISNULL(CREDIT,0) AS CREDIT " &_
				"		FROM BA_CASHBOOK " &_
				"		WHERE RECCED > -1 AND TRANSACTIONDATE >= '"&SD&"' AND TRANSACTIONDATE <= '"&ED&"' " &_
				"		ORDER BY TRANSACTIONDATE, THEID, THETYPE) PAGER "		
		'RW SQL & "<BR>"
		Call OpenRS(rsSetGB, SQL)
		If Not rsSetGB.EOF Then b = rsSetGB("BALANCE") Else b = 0 End If
		get_statement_balance = b 	' BALANCE + OPENING BALANCE + PARTMONTH BALANCE
		CloseRs(rsSetGB)
		
	End Function

%>