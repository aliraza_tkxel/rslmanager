<%
	Function get_deposit_cashbook_ob(ACCID)

		' GET TRANSACTION BALANCE UP TOAND NOT INCLUDING  STARTDAET
		SQL = "SELECT 	BALANCE FROM NL_OPENINGBALANCES " &_
				"WHERE	ACCOUNTID = " & ACCID & " AND TIMESTAMP >= '"&PS&"' AND TIMESTAMP <= '"&PE&"' "
		
		'rw sql
		Call OpenRS(rsOB, SQL)
		If Not rsOB.EOF Then
			get_deposit_cashbook_ob = rsOB(0)
		End If
		CloseRs(rsOB)
		
	End Function

	Function get_deposit_cashbook_balance(ACCID, IPAGE, SD, ED, PSIZE, RECCED)
		
		Dim b, rsDep
		if (iPage < 1) then iPage = 1 end if
		TOP_SQL = (PSIZE * (iPage-1))

		' GET CASHBOOK BALANCE FOR THIS ACCOUNT
		strSQL="EXEC NL_BA_CASHBOOK_DEPOSIT_SUM " & ACCID & ", " & TOP_SQL & ", '" & SD & "', '" & ED & "', " & RECCED
		Call OpenRs(rsDep, strSQL)
		If Not rsDep.EOF Then b = rsDep("BALANCE") Else b = 0 End If
		IF ISNULL(b) THEN b=0 END IF
		get_deposit_cashbook_balance = b 
		CloseRs(rsDep)
				
	End Function

	Function get_deposit_statement_ob(ACCID)

		' GET TRANSACTION BALANCE UP TO AND INCLUDING STARTDATE
		SQL = "SELECT 	BALANCE FROM NL_OPENINGBALANCES " &_
				"WHERE	ACCOUNTID = " & ACCID & " AND TIMESTAMP >= '"&PS&"' AND TIMESTAMP <= '"&PE&"' "
		
		'rw sql
		Call OpenRS(rsOB, SQL)
		If Not rsOB.EOF Then
			get_deposit_statement_ob = rsOB(0)
		End If
		CloseRs(rsOB)
		
	End Function

	Function get_deposit_statement_balance(ACCID, IPAGE, SD, ED, PSIZE)
		
		Dim b, rsDep
		if (iPage < 1) then iPage = 1 end if
		TOP_SQL = (PSIZE * (iPage-1))

		' GET CASHBOOK BALANCE FOR THIS ACCOUNT
		strSQL="EXEC NL_BA_STATEMENT_DEPOSIT_SUM " & ACCID & ", " & TOP_SQL & ", '" & SD & "', '" & ED & "' "
		Call OpenRs(rsDep, strSQL)
		If Not rsDep.EOF Then b = rsDep("BALANCE") Else b = 0 End If
		IF ISNULL(b) THEN b=0 END IF
		get_deposit_statement_balance = b 
		CloseRs(rsDep)
				
	End Function

%>