<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, optionvalues2, optiontext2
	Dim intReturn, theamounts, EmployeeLimits, Budget_Remaining, EXPID, HeadID, CCID
	
	strWhat = Request.form("IACTION")
	CURRENTITEMID = REQUEST("ORDERITEMID")
	'Default to '0' just incase no items are returned...
	Budget_Remaining = 0

	If strWhat = "change" Then 
		rePop() 
	End If

	Function rePop()
		HeadID = -1
		if(Request.Form("sel_HEAD") <> "") then HeadID = Request.Form("sel_HEAD")		

		if (HeadID = -1) then
			optionvalues = ""
			theamounts = "0"
			EmployeeLimits = "0"			
			optiontext = "Please Select a Head..."			
			Exit Function
		end if
		
		OpenDB()
		SQL = "SELECT DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (isNull(EXPENDITUREALLOCATION,0) - isNull(GROSSCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_				
				"left JOIN ( "&_
					"SELECT SUM(GROSSCOST) as GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM where F_PURCHASEITEM.ACTIVE = 1 AND F_PURCHASEITEM.ORDERITEMID <> " & CURRENTITEMID & " "&_
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID & ") "&_
				"AND EX.ACTIVE = 1 "&_
				"ORDER BY DESCRIPTION"
				
		Call OpenRs(rsExpenditure, SQL)		
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"		
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			EmployeeLimits = EmployeeLimits & ";;" & rsExpenditure.Fields.Item("LIMIT").Value			
			
			  count = count + 1
			  rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If

		SQLALLOC = "SELECT ISNULL(ISNULL(HEADALLOCATION,0) - ISNULL(EXPTOTAL,0),0) AS TOTAL FROM F_HEAD H " &_
				   "LEFT JOIN (SELECT HEADID, SUM(EXPENDITUREALLOCATION) AS EXPTOTAL FROM F_EXPENDITURE GROUP BY HEADID) E ON H.HEADID = E.HEADID " &_
				   "WHERE H.HEADID = " & HeadID & " "
		
		Call OpenRS(rsAlloc, SQLALLOC)
		Budget_Remaining = rsAlloc("TOTAL")
		CloseRs(rsAlloc)	
		
		CloseRs(rsExpenditure)
		CloseDB()		
	End Function


%>

<html>
<head>
<title>Expenditure Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>

	function loaded(){
		if ('<%=strWhat%>' == 'change'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>");
			parent.THISFORM.EXPENDITURE_LEFT_LIST.value = "<%=theamounts%>";
			parent.THISFORM.EMPLOYEE_LIMIT_LIST.value = "<%=EmployeeLimits%>";			
			}
		}
		
</script>

</body>
</html>

