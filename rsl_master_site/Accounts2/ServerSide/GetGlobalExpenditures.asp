<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, intReturn, theamounts, EmployeeLimits, Budget_Remaining
	
	strWhat = Request.form("IACTION")

	'Default to '0' just incase no items are returned...
	Budget_Remaining = 0

	If strWhat = "change" Then rePop() Else writeRecord() End If

	Function rePop()
		HeadID = -1
		if(Request.Form("sel_HeadID") <> "") then HeadID = Request.Form("sel_HeadID")		

		if (HeadID = -1) then
			optionvalues = ""
			theamounts = "0"
			EmployeeLimits = "0"			
			optiontext = "Please Select a Head..."			
			Exit Function
		end if
		
		OpenDB()
		SQL = "SELECT DESCRIPTION, EX.EXPENDITUREID "&_
				"FROM F_EXPENDITURE EX " &_
				"WHERE (HEADID = " & HeadID & ") "&_
				"AND EX.ACTIVE = 1 "&_
				"ORDER BY DESCRIPTION"
				
		Call OpenRs(rsExpenditure, SQL)		
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		EmployeeLimits = "0"		
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			
			  count = count + 1
			  rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If

		CloseDB()		
	End Function
	
%>

<html>
<head>
<title>Budget Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>

	function loaded(){
		if ('<%=strWhat%>' == 'change'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 1);
			}
		}
		
</script>

</body>
</html>

