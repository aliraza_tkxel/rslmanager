<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../NominalLedger/Includes/functions.asp" -->
<%
	if (Request("startdate") <> "") then
		SQL = "SELECT ISNULL(SUM(DEBIT),0) - ISNULL(SUM(CREDIT),0) AS BALANCE FROM BA_CASHBOOK WHERE RECCED = -1 " &_
				" AND TRANSACTIONDATE >= '" & Request("STARTDATE") & "' AND TRANSACTIONDATE <= '" & Request("ENDDATE") & "'"
		Call OpenDB()
		Call OpenRs(rsSet, SQL)
	
		if (NOT rsSet.EOF) then
			balance = rsSet("BALANCE")
		end if
		
		Call CloseRs(rsSet)
		Call CloseDB()
	end if
%>
<html>
<body topmargin=0 leftmargin=0 rightmargin=0 bottommargin=0 marginwidth=0 marginheight=0>
<% 	if (Request("startdate") <> "") then %>
<div align=right><font style='font-family:Tahoma;font-size:12px;'> Unreconciled Balance --> <%=FormatCurrency(balance)%></font></div>
<% else %>
<div align=right><font style='font-family:Tahoma;font-size:12px;'>Click the icon to show the cashbook unreconciled balances.</font></div>
<% end if %>
</body>
</html>