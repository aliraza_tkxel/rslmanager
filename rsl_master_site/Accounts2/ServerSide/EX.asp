<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText, ExpenditurePurchases
	Dim ProjectUsers, TimeCost, TimeMatched, fundid, HDID,  expname, startdate
	Dim expallocation, active, datecreated, EXID, totalallocated, totalfund, totalremaining, allocated
	Dim nominalcode, controlcode ,OpenBalance1, FY
	
Function GetFormFields ()
	expallocation = Request.Form("txt_expenditureallocation")
	if expallocation = "" then expallocation = null end if
	expname = Request.Form("txt_expenditurename")
	if expname = "" then expname = null end if	
	active = Request.Form("budgetactive")
	if active = "" then active = 0 end if	
	HDID = Request.Form("HDID")
	if HDID = "" then HDID = 0 end if
	nominalcode = Request.Form("txt_nominalcode")
	controlcode = Request.Form("txt_controlcode")
	OpenBalance1=Request.form("txt_OPBalance")
		
End Function

Function NewRecord ()
	GetFormFields()

	SQL = "SET NOCOUNT ON;INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREDATE, USERID, LASTMODIFIED, HEADID, QBDEBITCODE, QBCONTROLCODE,OPENBALANCE) " &_
			"VALUES ('" & expname & "', " &_
					"'" & Date & "', "&_					
					"" & Session("UserID") & ", "&_
					"'" & Date & "', "&_
					"" & HDID & ", "&_ 
					"'" & nominalcode & "', "&_ 
					"'" & controlcode & "',0 "&_ 
					");SELECT SCOPE_IDENTITY() AS NEWID; SET NOCOUNT OFF "
	Call OpenRs(rsINS, SQL)
	EXID = rsINS("NEWID")
	Call CloseRs(rsINS)

	SQL = "INSERT INTO F_EXPENDITURE_ALLOCATION (EXPENDITUREID, FISCALYEAR, EXPENDITUREALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE) " &_
			"VALUES (" & EXID & ", " &_
					"" & FY & ", "&_			
					"" & expallocation & ", "&_
					"'" & Date & "', "&_					
					"" & Session("UserID") & ", "&_
					"" & active & " "&_
					") "	
	Conn.Execute (SQL)	
End Function

Function UpdateRecord (theID)
	GetFormFields()

	SQL = "UPDATE F_EXPENDITURE " &_
			"SET DESCRIPTION = '" & expname & "', " &_
					"USERID = " & Session("UserID") & ", "&_
					"LASTMODIFIED = '" & Date & "', "&_
					"QBDEBITCODE = '" & nominalcode & "', "&_
					"QBCONTROLCODE = '" & controlcode & "', "&_
					"OPENBALANCE = " & OpenBalance1 & " "&_
					"WHERE EXPENDITUREID = " & theID
	Conn.Execute (SQL)
	
	SQL = "SELECT EXPENDITUREID FROM F_EXPENDITURE_ALLOCATION WHERE EXPENDITUREID = " & theID & " AND FISCALYEAR = " & FY	
	Call OpenRs(rsExists, SQL)
	if (rsExists.EOF) then
		SQL = "INSERT INTO F_EXPENDITURE_ALLOCATION (EXPENDITUREID, FISCALYEAR, EXPENDITUREALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE) " &_
				"VALUES (" & theID & ", " &_
						"" & FY & ", "&_			
						"" & expallocation & ", "&_
						"'" & Date & "', "&_					
						"" & Session("UserID") & ", "&_
						"" & active & " "&_
						") "	
		Conn.Execute (SQL)		
	else
		SQL = "UPDATE F_EXPENDITURE_ALLOCATION SET EXPENDITUREALLOCATION = " & expallocation & ", "&_
						"MODIFIEDBY = " & Session("UserID") & ", "&_
						"ACTIVE = " & active & ", "&_
						"MODIFIED = '" & Date & "' "&_
						"WHERE EXPENDITUREID = " & theID & " AND FISCALYEAR = " & FY
		Conn.Execute (SQL)	
	end if
	Call CloseRs(rsExists)
	
End Function

Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT ISNULL(EXA.ACTIVE,0) AS ACTIVE, EX.HEADID, QBDEBITCODE, QBCONTROLCODE, OPENBALANCE, DESCRIPTION, ISNULL(EXA.EXPENDITUREALLOCATION,0) AS EXPENDITUREALLOCATION  FROM F_EXPENDITURE EX LEFT JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR = " & FY & " WHERE EX.EXPENDITUREID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	expname 		= Rs("DESCRIPTION")
	expallocation 	= Rs("EXPENDITUREALLOCATION")
	nominalcode		= Rs("QBDEBITCODE")
	controlcode		= Rs("QBCONTROLCODE")
	OpenBalance1	= Rs("OPENBALANCE")
	if isNull(OpenBalance1) OR OpenBalance1 = "" then OpenBalance1=0
	OpenBalance1=formatnumber(OpenBalance1,2,-1,0,0)
	if (expallocation <> "") then
		expallocation = FormatNumber(expallocation,2,-1,0,0)
		DBL_expallocation = CDbl(expallocation)
	else 
		expallocation = "0.00"
		DBL_expallocation = CDbl(0)
	end if
	HDID = Rs("HEADID")
	active = Rs("ACTIVE")

	Rs.Close
	Set Rs = Nothing
	
	//find the total BUDGET VALUE
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT HE.HEADID, ISNULL(HEA.HEADALLOCATION,0) AS HEADALLOCATION FROM F_HEAD HE LEFT JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.FISCALYEAR = " & FY & " where HE.HEADID = " & HDID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("HEADALLOCATION"))
	totalfund = FormatNumber(Rs2("HEADALLOCATION"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT ISNULL(SUM(EXA.EXPENDITUREALLOCATION),0) AS TOTALALLOCATED from F_EXPENDITURE EX LEFT JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR = " & FY & " where EX.HEADID = " & HDID & " and EX.EXPENDITUREID <> " & EXID & " group by EX.HEADID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("TOTALALLOCATED")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_expallocation - CDbl(Rs3("TOTALALLOCATED"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = FormatNumber(DBL_totalfund - DBL_expallocation,2,-1,0,0)
	end if
	Rs3.Close
	Set Rs3 = Nothing

	SQL = "SELECT * FROM F_FISCALYEARS"
	Set Rs5 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SET NOCOUNT ON;DECLARE @YSTART SMALLDATETIME;" &_
					"DECLARE @YEND SMALLDATETIME; " &_ 
					"SET @YSTART = (SELECT YSTART FROM F_FISCALYEARS WHERE YRANGE = " & FY & ");" &_ 
					"SET @YEND = (SELECT YEND FROM F_FISCALYEARS WHERE YRANGE = " & FY & ");" &_ 					
					"SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALPURCHASES FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND EXPENDITUREID = " & theID & " AND PIDATE >= @YSTART AND PIDATE <= @YEND; "
	Set Rs5 = Conn.Execute(strEventQuery)
	ExpenditurePurchases = Rs5("TOTALPURCHASES")
	Rs5.Close
	Set Rs5 = Nothing
		
End Function	

Function GetHeadData (theID)
	expallocation = "0.00"
	DBL_expallocation = CDbl(0)	
	//find the total fund value
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT HE.HEADID, ISNULL(HEA.HEADALLOCATION,0) AS HEADALLOCATION FROM F_HEAD HE LEFT JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.FISCALYEAR = " & FY & " WHERE HE.HEADID = " & HDID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("headallocation"))
	totalfund = FormatNumber(Rs2("headallocation"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT ISNULL(SUM(EXPENDITUREALLOCATION),0) AS TOTALALLOCATED FROM F_EXPENDITURE EX LEFT JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR = " & FY & " WHERE EX.HEADID = " & HDID & " GROUP BY EX.HEADID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("TOTALALLOCATED")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_expallocation - CDbl(Rs3("TOTALALLOCATED"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = totalfund
	end if
	Rs3.Close
	Set Rs3 = Nothing
End Function

Function DisplayMiniTree(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT EX.DESCRIPTION AS EXPNAME, CC.DESCRIPTION AS COSTCENTRENAME, HD.DESCRIPTION AS HEADNAME FROM F_EXPENDITURE EX left join F_HEAD HD on HD.headid = EX.headid left join F_COSTCENTRE CC on CC.COSTCENTREID = HD.COSTCENTREID WHERE (EXPENDITUREID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Update Expenditure</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("HEADNAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("EXPNAME") & "</td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DisplayMiniTree2(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT CC.DESCRIPTION AS COSTCENTRENAME, HD.DESCRIPTION AS HEADNAME FROM F_HEAD HD left join F_COSTCENTRE CC on HD.COSTCENTREID = CC.COSTCENTREID WHERE (HD.HEADID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Add New Expenditure</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
	treeText = treeText & "<tr><td><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DelRecord (theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT Count(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE EXPENDITUREID = " & theID
	Set Rs4 = Conn.Execute(strEventQuery)
	ActualCount = Rs4("THECOUNT")
	Rs4.close()
	Set Rs4 = Nothing
			
	if (ActualCount = 0) then
		Conn.Execute ("DELETE FROM F_EXPENDITURE WHERE EXPENDITUREID = " & theID & ";")
		Conn.Execute ("DELETE FROM F_EXPENDITURE_ALLOCATION WHERE EXPENDITUREID = " & theID & ";")		
		treeText = "Expenditure deleted successfully."		
	else
		treeText = "Sorry, cannot delete the selected expenditure as it is has (" & ActualCount & ") purchase(s) assigned to it. You can set the expenditure in-active instead."
	end if

End Function

ACTION_TO_TAKE = Request("EX_A")
EXID = Request("EXID")

'make sure we get the currrent fiscal year variable one way or the other
FY = Request("FY")
if (FY = "") then
	FY = Request("hdn_FY")
end if

Response.Write("Fiscal Year is " & FY & "<br>")

OpenDB()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LFD") Then
	HDID = Request("HDID")
	fundid = Request("fundid")	
	GetHeadData(EXID)
	DisplayMiniTree2(HDID)	
ElseIf (ACTION_TO_TAKE = "L") Then
	GetData(EXID)
	DisplayMiniTree(EXID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(EXID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(EXID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
	if ("<%=ACTION_TO_TAKE%>" == "ADD"){
		//parent.refreshSideBar();
		parent.theSideBar.ReloadMe();
		parent.setText("New expenditure added successfully.",1);			
		}
	else if ("<%=ACTION_TO_TAKE%>" == "UPDATE"){
		//parent.refreshSideBar();
		parent.theSideBar.ReloadMe();
		parent.setText("Expenditure updated successfully.",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "LFD"){
		parent.ResetDiv('EXPENDITURE');
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("EXPENDITURE",1);
		parent.setCheckingArray('EXPENDITURE');
		parent.thisForm.HDID.value = "<%=HDID%>";		
		parent.thisForm.EXID.value = "<%=EXID%>";				
		parent.thisForm.EX_A.value = "ADD";
		parent.thisForm.txt_expenditureallocation.value = "<%=expallocation%>";
		parent.thisForm.Btotalallocated.value = "<%=allocated%>";
		parent.thisForm.Btotalremaining.value = "<%=totalremaining%>";
		parent.thisForm.Bmintotal.value = "0";						
		parent.thisForm.Btotalfund.value = "<%=totalfund%>";		
		parent.Budget.style.display = "block";
		}
	else if ("<%=ACTION_TO_TAKE%>" == "DELETE"){
		//parent.refreshSideBar();
		parent.theSideBar.ReloadMe();
		parent.setText("<%=treeText%>",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "L"){
		parent.NewItem(4);
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("EXPENDITURE",2);							
		parent.thisForm.txt_expenditureallocation.value = "<%=expallocation%>";
		parent.thisForm.txt_expenditurename.value = "<%=expname%>";
		parent.thisForm.EXID.value = "<%=EXID%>";						
		parent.thisForm.budgetcreated.value = "<%=startdate%>";		
		parent.thisForm.Btotalallocated.value = "<%=allocated%>";
		parent.thisForm.Bmintotal.value = "<%=ExpenditurePurchases%>";				
		parent.thisForm.Btotalremaining.value = "<%=totalremaining%>";
		parent.thisForm.Btotalfund.value = "<%=totalfund%>";		
		parent.thisForm.HDID.value = "<%=HDID%>";
		parent.thisForm.txt_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.txt_controlcode.value = "<%=controlcode%>";
		parent.thisForm.hid_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.hid_controlcode.value = "<%=controlcode%>";
		parent.thisForm.txt_OPBalance.value ="<%=OpenBalance1%>"
		parent.thisForm.hdn_OPBalance.value ="<%=OpenBalance1%>"
		if ("<%=active%>" == "True")
			parent.thisForm.budgetactive[0].checked = true;
		else
			parent.thisForm.budgetactive[1].checked = true;				
		parent.thisForm.EX_A.value = "UPDATE";
		parent.Budget.style.display = "block";
		parent.setCheckingArray('EXPENDITURE');
		}
	}
</script>
</body>
</html>
