<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim DataFields   (2)
Dim DataTypes    (2)
Dim ElementTypes (2)
Dim FormValues   (2)
Dim FormFields   (2)
UpdateID	  = "hid_EMPLOYEELIMITID"
FormFields(0) = "cde_EMPLOYEEID|" & Request("sel_ASSIGNTO")
FormFields(1) = "txt_LIMIT|TEXT"
FormFields(2) = "hid_EXPENDITUREID|TEXT"

Function NewRecord ()
	'CHECK IF an employee limit already exists for the expenditure if it does then delete it
	SQL = "DELETE FROM F_EMPLOYEELIMITS WHERE EMPLOYEEID = " & Request("sel_ASSIGNTO") & " AND EXPENDITUREID = " & Request("hid_EXPENDITUREID")
	Conn.Execute (SQL)
	
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO F_EMPLOYEELIMITS " & strSQL & " "
	Conn.Execute(SQL)

	LoadRecord()

End Function

Function AmendRecord()
	ID = Request.Form(UpdateID)	
	Call MakeUpdate(strSQL)
	SQL = "UPDATE F_EMPLOYEELIMITS " & strSQL & " WHERE EMPLOYEELIMITID = " & ID
	
	Conn.Execute SQL, recaffected
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	SQL = "DELETE FROM F_EMPLOYEELIMITS WHERE EMPLOYEELIMITID = " & ID
	Conn.Execute SQL, recaffected
	LoadRecord()	
End Function

Function LoadRecord()
	SQL = "SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, isnull(LIMIT,0) as limit, EMPLOYEELIMITID FROM F_EMPLOYEELIMITS EL INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = EL.EMPLOYEEID WHERE EXPENDITUREID = " & Request("hid_EXPENDITUREID") & " ORDER BY FULLNAME"
	Call OpenRs(rsEm, SQL)
	
	if (NOT rsEm.EOF) then
		TableString = ""
		while NOT rsEM.EOF 
			TableString = TableString & "<TR><TD>" & rsEm("FULLNAME") & "</TD><TD width=110px align=right>" & FormatCurrency(rsEm("LIMIT")) & "</td><TD align=center BGCOLOR=#ffffff width=30px nowrap style='cursor:hand' onclick=""DeleteMe(" & rsEm("EMPLOYEELIMITID")& ")""><font color=red>DEL</font></TD></TR>"
			rsEm.movenext
		wend
		TableString = TableString & "<TFOOT><TR><TD colspan=3 align=center height='100%'></TD></TR></TFOOT>"
	else
		TableString = "<TR><TD colspan=3 align=center>No Employee Limits Set</TD></TR>"
		TableString = TableString & "<TFOOT><TR><TD colspan=3 align=center height='100%'></TD></TR></TFOOT>"		
	end if
End Function


Function GO()
	CloseDB()
	'Response.Redirect "../CRM.asp?CustomerID=" & ID
End Function

TableString = ""

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head>
</head>
<script language="javascript">
function ReturnData(){
	parent.MainDiv.innerHTML = MainDiv.innerHTML
	}
</script>
<body onload="ReturnData()">
<div id="MainDiv">
<table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' SLCOLOR='' HLCOLOR='STEELBLUE' border=1 width=100% height=100%>
<%=TableString%>
</table>
</div>
</body>
</html>