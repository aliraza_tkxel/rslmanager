<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CostCentreID, strWhat, count, optionvalues, optiontext, intReturn, theamounts
	
	strWhat = Request.form("IACTION")
	response.write strWhat
	If strWhat = "gethead" Then rePop() End If
	
	Function rePop()
		CostCentreID = -1
		if(Request.Form("sel_COSTCENTRE") <> "") then CostCentreID = Request.Form("sel_COSTCENTRE")
		
		if (CostCentreID = -1) then
			optionvalues = ""
			optiontext = "Please Select a Cost Centre..."			
			Exit Function
		end if
		
		OpenDB()
		SQL = "SELECT DISTINCT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
			"WHERE HE.ACTIVE = 1 AND EX.ACTIVE = 1 AND HE.CostCentreID = " & CostCentreID
		
		Call OpenRs(rsHead, SQL)
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsHead.Fields.Item("HEADID").Value
			optiontext = optiontext & ";;" & theText
			
			  count = count + 1
			  rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Heads Are Setup..."
		End If

		CloseRs(rsHead)
		CloseDB()
	End Function
	
%>
<html>
<head>
<title>Head Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>


<script language=javascript>

	function loaded(){
		if ('<%=strWhat%>' == 'gethead'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 2);
			}
	}
		
</script>

</body>
</html>

