<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'"
Call OpenRs(rsDev, SQL)
if (NOt rsDev.eOF) then
	DevID = rsDev("DEFAULTVALUE")
else
	DevID = -1
end if
CloseRs(rsDev)
CloseDB()
%>
<html>
<head>
<link type="text/css" rel="stylesheet" href="/js/XTree/xtree2.css" />
<script type="text/javascript" src="/js/XTree/xtree2.js"></script>
<script type="text/javascript" src="/js/XTree/xmlextras.js"></script>
<script type="text/javascript" src="/js/XTree/xloadtree2.js"></script>
<script language=javascript>
var rti;
var CurrentSelected = ""
var CurrentDevelopmentPart = ""
function GO(iPage){
	CurrentlySelected = tree.getSelected()
	parent.FB_DEV_4466.location.href = iPage + "&Node=" + tree.getSelected().id
	}

function GOHE(iPage, iDev){
	CurrentDevelopmentPart = iDev
	CurrentlySelected = tree.getSelected()
	parent.FB_DEV_4466.location.href = iPage + "&Node=" + tree.getSelected().id
	}
	
function ReloadMe(){
	if (CurrentlySelected != "")
		CurrentlySelected.reload();
	}

function ReloadDevPart(){
	if (!isNaN(CurrentDevelopmentPart)) {
		alert("rti" + CurrentDevelopmentPart + ".reload()")
		eval("rti" + CurrentDevelopmentPart + ".reload()")
		}
	}
		
function PopScheme(){
	window.showModalDialog("../Popups/NewScheme.asp", "NewScheme", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}
function PopLimit1(){
	window.showModalDialog("../Popups/SetGlobalEmployeeLimits.asp", "Limits by User", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}
function PopLimit2(){
	window.showModalDialog("../Popups/SetDevelopmentLimits.asp", "Development Limits", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}	
</script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="0" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<img name="web_folder" src="images/folder.png" width="1" height="1">
<img name="web_openfolder" src="images/openfolder.png" width="1" height="1">
<img name="web_file" src="images/file.png" width="1" height="1">
<img name="web_lminus" src="images/Lminus.png" width="1" height="1">
<img name="web_lplus" src="images/Lplus.png" width="1" height="1">
<img name="web_tminus" src="images/Tminus.png" width="1" height="1">
<img name="web_tplus" src="images/Tplus.png" width="1" height="1">
<img name="web_i" src="images/I.png" width="1" height="1">
<img name="web_l" src="images/L.png" width="1" height="1">
<img name="web_t" src="images/T.png" width="1" height="1">
<img name="web_base" src="images/base.gif" width="1" height="1">
<img name="web_img1" src="img1.gif" width="1" height="1">
<img name="web_img2" src="img2.gif" width="1" height="1">
<img name="web_img3" src="img3.gif" width="1" height="1">
<img name="web_img4" src="img4.gif" width="1" height="1">
<img name="web_img5" src="img5.gif" width="1" height="1">
<img name="web_img6" src="img6.gif" width="1" height="1">
<img name="web_img7" src="img7.gif" width="1" height="1">
<div class="dtree" style='height:100%'>
<script type="text/javascript">

/// XP Look
webFXTreeConfig.rootIcon		= web_folder.src;
webFXTreeConfig.openRootIcon	= web_openfolder.src;
webFXTreeConfig.folderIcon		= web_folder.src;
webFXTreeConfig.openFolderIcon	= web_openfolder.src;
webFXTreeConfig.fileIcon		= web_file.src;
webFXTreeConfig.lMinusIcon		= web_lminus.src;
webFXTreeConfig.lPlusIcon		= web_lplus.src;
webFXTreeConfig.tMinusIcon		= web_tminus.src;
webFXTreeConfig.tPlusIcon		= web_tplus.src;
webFXTreeConfig.iIcon			= web_i.src;
webFXTreeConfig.lIcon			= web_l.src;
webFXTreeConfig.tIcon			= web_t.src;

var rti1, rti2, rti3, rti4, rti5, rti6, rti7, rti8, rti9, rti10;
var tree = new WebFXTree("RSL Finance Builder", "", "", web_base.src, web_base.src);
tree.add(new WebFXTreeItem("Limits by User", "javascript:PopLimit1()"));
tree.add(new WebFXTreeItem("Limit Across All Developments", "javascript:PopLimit2()"));

<%

SQL = "SELECT COSTCENTREID, DESCRIPTION FROM F_COSTCENTRE WHERE COSTCENTREID = " & DevID & " ORDER BY DESCRIPTION"
Call OpenDB()
Call OpenRs(rsCC, SQL)
while NOT rsCC.EOF
	Response.Write "tree.add(rti1 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (0 - 9)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=1"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti2 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (A - C)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=2"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti3 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (D - F)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=3"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti4 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (G - I)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=4"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti5 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (J - L)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=5"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"			
	Response.Write "tree.add(rti6 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (M - O)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=6"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti7 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (P - R)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=7"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti8 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (S - U)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=8"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti9 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (V - X)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=9"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	Response.Write "tree.add(rti10 = new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & "s  (Y - Z)"", ""TreeLoad.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&ALPHA=10"", ""JavaScript:GO('CC_DEV.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"			
	rsCC.moveNext
wend
Call CloseRs(rsCC)
Call CloseDB()
%>

tree.write()
</script>

</div>
</body>
</html>
