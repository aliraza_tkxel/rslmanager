<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'"
Call OpenRs(rsDev, SQL)
if (NOt rsDev.eOF) then
	DevID = rsDev("DEFAULTVALUE")
else
	DevID = -1
end if
CloseRs(rsDev)
CloseDB()

FY = Request("FY")
%>
<html>
<head>
<link type="text/css" rel="stylesheet" href="/js/XTree/xtree2.css" />
<script type="text/javascript" src="/js/XTree/xtree2.js"></script>
<script type="text/javascript" src="/js/XTree/xmlextras.js"></script>
<script type="text/javascript" src="/js/XTree/xloadtree2.js"></script>
<script language=javascript>
var rti;
var CurrentSelected = ""
function GO(iPage){
	CurrentlySelected = tree.getSelected().getParent()
	parent.FB.location.href = iPage + "&FY=<%=FY%>"
	}

function PopLimit1(){
	window.showModalDialog("../Popups/SetGlobalEmployeeLimits.asp", "Limits by User", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}
		
function ReloadMe(){
	if (CurrentlySelected != "")
		CurrentlySelected.reload();
	}
</script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="0" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<img name="web_folder" src="images/folder.png" width="1" height="1">
<img name="web_openfolder" src="images/openfolder.png" width="1" height="1">
<img name="web_file" src="images/file.png" width="1" height="1">
<img name="web_lminus" src="images/Lminus.png" width="1" height="1">
<img name="web_lplus" src="images/Lplus.png" width="1" height="1">
<img name="web_tminus" src="images/Tminus.png" width="1" height="1">
<img name="web_tplus" src="images/Tplus.png" width="1" height="1">
<img name="web_i" src="images/I.png" width="1" height="1">
<img name="web_l" src="images/L.png" width="1" height="1">
<img name="web_t" src="images/T.png" width="1" height="1">
<img name="web_base" src="images/base.gif" width="1" height="1">
<img name="web_img1" src="img1.gif" width="1" height="1">
<img name="web_img2" src="img2.gif" width="1" height="1">
<img name="web_img3" src="img3.gif" width="1" height="1">
<img name="web_img4" src="img4.gif" width="1" height="1">
<img name="web_img5" src="img5.gif" width="1" height="1">
<img name="web_img6" src="img6.gif" width="1" height="1">
<img name="web_img7" src="img7.gif" width="1" height="1">
<div class="dtree" style='height:100%'>
<script type="text/javascript">

/// XP Look
webFXTreeConfig.rootIcon		= web_folder.src;
webFXTreeConfig.openRootIcon	= web_openfolder.src;
webFXTreeConfig.folderIcon		= web_folder.src;
webFXTreeConfig.openFolderIcon	= web_openfolder.src;
webFXTreeConfig.fileIcon		= web_file.src;
webFXTreeConfig.lMinusIcon		= web_lminus.src;
webFXTreeConfig.lPlusIcon		= web_lplus.src;
webFXTreeConfig.tMinusIcon		= web_tminus.src;
webFXTreeConfig.tPlusIcon		= web_tplus.src;
webFXTreeConfig.iIcon			= web_i.src;
webFXTreeConfig.lIcon			= web_l.src;
webFXTreeConfig.tIcon			= web_t.src;

var rti;
var tree = new WebFXTree("RSL Finance Builder", "", "", web_base.src, web_base.src);
tree.add(new WebFXTreeItem("Limits by User", "javascript:PopLimit1()"));
<%

SQL = "SELECT CC.COSTCENTREID, DESCRIPTION, ISNULL(CCA.ACTIVE,0) AS ACTIVE FROM F_COSTCENTRE CC LEFT JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND FISCALYEAR = " & FY & " AND CCA.ACTIVE = 1 WHERE CC.COSTCENTREID <> " & DevID & " ORDER BY DESCRIPTION"
Call OpenDB()
Call OpenRs(rsCC, SQL)
while NOT rsCC.EOF
	if (rsCC("ACTIVE") = true) then
		Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & """, ""TL_F.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&FY=" & FY & """, ""JavaScript:GO('CC.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img1.src, web_img1.src));"
	else
		Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("DESCRIPTION") & """, ""TL_F.asp?TYPE=1&CCID=" & rsCC("CostCentreID") & "&FY=" & FY & """, ""JavaScript:GO('CC.asp?CCID=" & rsCC("CostCentreID") & "&CC_A=L')"", """", web_img7.src, web_img7.src));"
	end if	
	rsCC.moveNext
wend
Response.Write "tree.add(new WebFXLoadTreeItem(""New Cost Centre"", """", ""JavaScript:parent.NewItem(1)""));"	
Call CloseRs(rsCC)
Call CloseDB()
%>

tree.write()
</script>
</div>
</body>
</html>
