<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
'''''''''
'' Page Note: This is a copy of EmployeeLimit_srv.asp, changed to meet rash new functionality
'' 8/june/2004 - No longer returns the Employees, but now returns the expenditures
''''''''''

Dim ID
Dim DataFields   (2)
Dim DataTypes    (2)
Dim ElementTypes (2)
Dim FormValues   (2)
Dim FormFields   (2)

UpdateID	  = "hid_EMPLOYEELIMITID"
ExpenditureID = Request.Form("hid_EXPENDITUREID")
ExpenditureName = Request.Form("hid_ExpenditureName")		
EmpLimit = Request.Form("hid_LIMIT")	'This is WHEN DELETEION OF AN EXPEND IS ACTIVATED
ExpenditureLimit = Request.Form("txt_LIMIT")
AssignTo = Request("sel_ASSIGNTO")
FormFields(0) = "cde_EMPLOYEEID|" & Request("sel_ASSIGNTO")
FormFields(1) = "txt_LIMIT|TEXT"
FormFields(2) = "hid_EXPENDITUREID|TEXT"


	Function NewRecord ()
		rw " -------( "
		If ExpenditureID <> "" And AssignTo <> "" Then
		
			SQL = "SELECT * FROM F_EMPLOYEELIMITS WHERE EMPLOYEEID = " & AssignTo & " AND EXPENDITUREID = " & ExpenditureID
			Call OpenRs(rsCheck, SQL)
			rw SQL & "<br><br>"
			If NOT rsCheck.eof then
			DeleteRecord()
			End If
				
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO F_EMPLOYEELIMITS " & strSQL & " "
		Conn.Execute(SQL)
		rw SQL
		End If
		rw " )-------"
		LoadRecord()
		
	End Function

	Function AmendRecord()
	
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		SQL = "UPDATE F_EMPLOYEELIMITS " & strSQL & " WHERE EMPLOYEELIMITID = " & ID
		rw SQL & "<br><br>"
		Conn.Execute SQL, recaffected
	
	End Function
	
	Function DeleteRecord()
		
		SQL = "delete from f_employeelimits  " &_
				"where expenditureid = " & ExpenditureID & " and employeeid = " & AssignTo
				If NOT Request("hid_Action") = "NEW" then
					SQL = SQL &	" and limit = " & EmpLimit
				End If
		rw SQL & "<br><br>"
		Conn.Execute SQL, recaffected
		LoadRecord()	
	End Function


	' 8th June 2004 - Returns all the expenditures records that employees have
	' limits on, to the server frame in SetEmployeeLimts.asp
	Function LoadRecord()
		
		SQL =	"SELECT 	H.DESCRIPTION AS HEADDESC, " &_
					"F.DESCRIPTION AS descrip ,  " &_
					"	isnull(LIMIT,0) as limit,  " &_
					"	EMPLOYEELIMITID,  " &_
					"	F.EXPENDITUREID ,  " &_
					"	F.headid	 " &_
					"FROM F_EMPLOYEELIMITS EL  " &_
					"	INNER JOIN F_EXPENDITURE F ON F.EXPENDITUREID = EL.EXPENDITUREID  " &_
					"	INNER JOIN F_HEAD H ON H.HEADID = F.HEADID  " &_
					"WHERE 	EL.EMPLOYEEID = " & Request("sel_ASSIGNTO") &_
					"ORDER BY F.DESCRIPTION " 
					
		rw SQL & "<br><br>"
		Call OpenRs(rsEm, SQL)
		'"AND F.HEADID = " & Request("sel_HEADID") &_  (this has been taken out of sql if this line is viewed a week after 15/10/2004 then delete)
		'generate the html
		if (NOT rsEm.EOF) then

			TableString = ""
			tempDestinct = ""
				TableString = TableString & "<TR><TD><b>Head</b></TD><TD><b>Expenditure</b></TD><TD width=110px align=right><b>limit</td><TD align=center></TD></TR>"
			while NOT rsEM.EOF 
			
				TableString = TableString & "<TR><TD>" & rsEm("headdesc") & "</TD><TD>" & rsEm("descrip") & "</TD><TD width=110px align=right>" & FormatCurrency(rsEm("LIMIT")) & "</td><TD align=center BGCOLOR=#ffffff width=30px nowrap style='cursor:hand' onclick=""DeleteMe(" & rsEm("EMPLOYEELIMITID")& "," & rsEm("expenditureid")& ",'" & rsEm("descrip")&  "'," & rsEm("limit")& ")""><font color=red>DEL</font></TD></TR>"
			
			 rsEm.movenext
			wend
			TableString = TableString & "<TFOOT><TR><TD colspan=4 align=center height='100%'></TD></TR></TFOOT>"
		else
			TableString = "<TR><TD colspan=4 align=center>No Expenditures Assigned</TD></TR>"
			TableString = TableString & "<TFOOT><TR><TD colspan=4 align=center height='100%'></TD></TR></TFOOT>"		
		end if
		
	End Function

TableString = ""

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "GenExp"   LoadRecord()	
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>
<html>
<head>
</head>
<script language="javascript">
function ReturnData(){
	parent.MainDiv.innerHTML = MainDiv.innerHTML
	
	}
</script>
<body onload="ReturnData()">
<div id="MainDiv">
<table cellspacing=0 cellpadding=3 style='border-collapse:collapse;behavior:url(../../Includes/Tables/tablehl.htc);' SLCOLOR='' HLCOLOR='STEELBLUE' border=1 width=100% height=100%>
<%=TableString%>
</table>
</div>
</body>
</html>