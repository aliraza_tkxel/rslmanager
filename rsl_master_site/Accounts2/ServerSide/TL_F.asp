<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Response.ContentType = "text/xml"

FY = Request("FY")

Set objDom = Server.CreateObject("Microsoft.XMLDOM")
objDom.preserveWhiteSpace = True

if (Request("TYPE") = "1") then
	CCID = Request("CCID")
	IF (CCID = "") THEN CCID = -1
	SQL = "SELECT HE.HEADID, DESCRIPTION, COSTCENTREID, ISNULL(HEA.ACTIVE,0) AS ACTIVE FROM F_HEAD HE LEFT JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND FISCALYEAR = " & FY & " WHERE COSTCENTREID = " & CCID & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("DESCRIPTION")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the src attribute to the field node ***
		   Set objcolName = objDom.createAttribute("src")
		   objcolName.Text = "TL_F.asp?TYPE=2&HDID=" & rsHE("HEADID") & "&FY=" & FY
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GO('HD.asp?HDID=" & rsHE("HEADID") & "&HD_A=L')"
		   objRow.SetAttributeNode(objColName)

			if (rsHE("ACTIVE") = true) then
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img2.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img2.gif"
			   objRow.SetAttributeNode(objColName)
			else
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img6.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img6.gif"
			   objRow.SetAttributeNode(objColName)

			end if		

		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend

    'FINALLY ADD THE NEW OPTION AS FOLLOWS
    Set objRow = objDom.createElement("tree")

    '*** Append the text attribute to the field node ***
    Set objcolName = objDom.createAttribute("text")
    objcolName.Text = "New Head"
    objRow.SetAttributeNode(objColName)

    '*** Append the action attribute to the field node ***
    Set objcolName = objDom.createAttribute("action")
    objcolName.Text = "Javascript:GO('HD.asp?CCID=" & CCID & "&HD_A=LFD')"
    objRow.SetAttributeNode(objColName)

    objRoot.appendChild objRow
	
	Call CloseRs(rsHE)
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 
	
elseif (Request("TYPE") = "2") then

	HDID = Request("HDID")
	IF (HDID = "") THEN HDID = -1
	SQL = "SELECT EX.EXPENDITUREID, DESCRIPTION, EX.HEADID, ISNULL(EXA.ACTIVE,0) AS ACTIVE FROM F_EXPENDITURE EX LEFT JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR = " & FY & " WHERE EX.HEADID = " & HDID & " ORDER BY DESCRIPTION"
	Call OpenDB()
	Call OpenRs(rsEX, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot

		While NOT rsEX.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsEX("DESCRIPTION")
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the action attribute to the field node ***
		   Set objcolName = objDom.createAttribute("action")
		   objcolName.Text = "Javascript:GO('EX.asp?EXID=" & rsEX("EXPENDITUREID") & "&EX_A=L')"
		   objRow.SetAttributeNode(objColName)
	
			if (rsEX("ACTIVE") = true) then
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img4.gif"
			   objRow.SetAttributeNode(objColName)
			else
			   '*** Append the icon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("icon")
			   objcolName.Text = "img5.gif"
			   objRow.SetAttributeNode(objColName)
		
			   '*** Append the openIcon attribute to the field node ***
			   Set objcolName = objDom.createAttribute("openIcon")
			   objcolName.Text = "img5.gif"
			   objRow.SetAttributeNode(objColName)

			end if	
		   objRoot.appendChild objRow
	
			rsEX.moveNext()	
		Wend

    'FINALLY ADD THE NEW OPTION AS FOLLOWS
    Set objRow = objDom.createElement("tree")

    '*** Append the text attribute to the field node ***
    Set objcolName = objDom.createAttribute("text")
    objcolName.Text = "New Expenditure"
    objRow.SetAttributeNode(objColName)

    '*** Append the action attribute to the field node ***
    Set objcolName = objDom.createAttribute("action")
    objcolName.Text = "Javascript:GO('EX.asp?HDID=" & HDID & "&EX_A=LFD')"
    objRow.SetAttributeNode(objColName)

    objRoot.appendChild objRow

	Call CloseRs(rsEX)
	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 

end if
%>
