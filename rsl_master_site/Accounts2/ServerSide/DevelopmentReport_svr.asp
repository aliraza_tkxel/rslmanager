<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	StartZ = Timer 

	HEADID = REQUEST("HEADID")
	IF (HEADID = "") THEN HEADID = 100

	OpenDB()	
	SQL = "SELECT DESCRIPTION FROM F_HEAD WHERE HEADID = " & HEADID
	Call OpenRs(rsD, SQL)
	if (NOT rsD.EOF) then
		DevName = rsD("DESCRIPTION")
	end if
	Call CloseRs(rsD)
	
	SQL = "SELECT -ISNULL(SUM(AMOUNT),0) AS TOTALGRANT FROM P_DEVLOPMENTGRANT DG " &_
			"INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.DEVELOPMENTID = DG.DEVCENTREID " &_
			"WHERE DHL.HEADID = " & HEADID
	Call OpenRs(rsGR, SQL)
	GrantFigure = rsGR("TOTALGRANT")
	Call CloseRs(rsGR)
			
	SQL = "SELECT DRG.GROUPNAME, DC.CODENAME, ISNULL(OB.OPENBALANCE,0) + ISNULL(SUM(GROSSCOST),0) AS TOTAL FROM F_DEVELOPMENTCODES DC " &_
			"	INNER JOIN  F_DEVELOPMENT_REPORT_GROUPING DRG ON DRG.GROUPID = DC.GROUPID " &_
			"	LEFT JOIN F_EXPENDITURE EX ON EX.DESCRIPTION = DC.CODENAME AND EX.HEADID = " & HEADID & " " &_
			"	LEFT JOIN F_EXPENDITURE OB ON OB.DESCRIPTION = DC.CODENAME AND OB.HEADID = " & HEADID & " " &_
			"	LEFT JOIN F_PURCHASEITEM PI ON PI.EXPENDITUREID = EX.EXPENDITUREID AND EX.ACTIVE = 1 " &_
			"GROUP BY DC.CODENAME, DRG.GROUPNAME, OB.OPENBALANCE " &_
			"ORDER BY DRG.GROUPNAME, DC.CODENAME" 
	Call OpenRs(rsREP, SQL)
	
	ReportString = ""
	CurrentHeading = rsREP("GROUPNAME")
	FullReportString = ""
	SubTotal = 0
	FullTotal = 0
	DispCounter = 1
	while NOT rsREP.EOF
		Heading = rsREP("GROUPNAME")
		if (Heading <> CurrentHeading) then
			ReportString = "<TR style=""cursor:hand"" onclick=""toggle('TR" & DispCounter & "')""><TD><b>" & CurrentHeading & "</b></TD><TD align=right><b>" & FormatSign(FormatNumber(SubTotal)) & "</b></TD></TR>" & ReportString
			SubTotal = 0
			FullReportString =  FullReportString & ReportString
			ReportString = ""			
			CurrentHeading = Heading
			DispCounter = DispCounter + 1
		end if
		Amount =  rsREP("TOTAL")
		ReportString = ReportString & "<TR style=""display:none"" id=""TR" & DispCounter & """><TD>&nbsp;&nbsp;" & rsREP("CODENAME") & "</TD><TD align=right>" & FormatSign(FormatNumber(Amount)) & "</TD></TR>"
		SubTotal = SubTotal + Amount
		FullTotal = FullTotal + Amount
		rsREP.moveNext
	wend
	Call CloseRs(rsREP)	
	ReportString = "<TR style=""cursor:hand"" onclick=""toggle('TR" & DispCounter & "')""><TD><b>" & CurrentHeading & "</b></TD><TD align=right><b>" & FormatSign(FormatNumber(SubTotal)) & "</b></TD></TR>" & ReportString
	FullReportString =  FullReportString & ReportString	
	
	' 1 = negative , 2 = positive
	Function FormatSign(strNumber)
	
		If CDbl(strNumber) < 0 Then
			FormatSign = "(" & strNumber & ")"
			Exit Function
		Else
			FormatSign = strNumber
			Exit Function
		End If
		
	End Function
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<script type="text/javascript">
<!--
		
	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"

	function convertDate(strDate) {
	
		strDate = new String(strDate);
		arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		arrSplit = strDate.split("/");
		return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
	
	}

	function click_go(){
		location.href = "profitandloss.ASP?START="+document.getElementById("selMonth").value
		}

	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}

	// OPEN OR CLOSE ALL ROWS DEPENDENT ON STSATUS OF GLOBAL VARIABLES BELOW
	var current_rowStatus = new String("none")
	var next_rowStatus = new String("block")
	function toggleAll(){
		
		var coll = document.all.tags("TR");
			if (coll!=null)
				for (i=0; i<coll.length; i++) {
					if (coll[i].style.display == current_rowStatus)
						coll[i].style.display = next_rowStatus;
					}
		if (current_rowStatus == "none"){
			current_rowStatus = new String("block")
			next_rowStatus = new String("none")
			}
		else {
			current_rowStatus = new String("none")
			next_rowStatus = new String("block")
			}
	}

//-->
</script>
<STYLE TYPE="TEXT/CSS">
#TABLE1 TD {BORDER-BOTTOM:1PX DOUBLE SILVER	}
</STYLE>
<!--
<div style='position:absolute;top:80;left:430;z-Index:100'>
<%
Response.Write "Report Generated in : " & FormatNumber(Timer - StartZ,3) & " seconds"
%>
</div>
-->
<TABLE CELLPADDING=1 CELLSPACING=2 ALIGN=CENTER>
	<TR><TD  style='font-size:16px' ALIGN=CENTER><u><b><%=DevName%></b></u></TD></TR>		
<!--	<TR><TD  ALIGN=CENTER><%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD></TR>-->
	<TR><TD  ALIGN=CENTER><%=now%></TD></TR>
	<TR style='height:8px'><TD></TD></TR>
</TABLE>
<TABLE align=center ID="TABLE1" WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
<%=FullReportString%>
<TR><TD style='font-size:12px'><b>Development Costs</b></TD><TD style='font-size:12px' align=right><b><%=FormatSign(FormatNumber(FullTotal))%></b></TD></TR>
<TR><TD style='font-size:12px'><b>Grants</b></TD><TD style='font-size:12px' align=right><b><%=FormatSign(FormatNumber(GrantFigure))%></b></TD></TR>
<TR><TD style='font-size:12px'><b>BALANCE</b></TD><TD style='font-size:12px' align=right><b><%=FormatSign(FormatNumber(FullTotal+GrantFigure))%></b></TD></TR>
</TABLE>	
</BODY>
</HTML>

