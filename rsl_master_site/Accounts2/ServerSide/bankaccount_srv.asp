<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID, LoaderString
	Dim DataFields   (11)
	Dim DataTypes    (11)
	Dim ElementTypes (11)
	Dim FormValues   (11)
	Dim FormFields   (11)
	UpdateID	  = "hid_ACCOUNTID"
	FormFields(0) = "txt_ACCOUNTNAME|TEXT"
	FormFields(1) = "txt_BANK|TEXT"
	FormFields(2) = "txt_ADD1|TEXT"
	FormFields(3) = "txt_ADD2|TEXT"
	FormFields(4) = "txt_TOWN|TEXT"
	FormFields(5) = "txt_COUNTY|TEXT"
	FormFields(6) = "txt_POSTCODE|TEXT"
	FormFields(7) = "txt_ACCOUNTNO|TEXT"
	FormFields(8) = "txt_SORTCODE|TEXT"
	FormFields(9) = "sel_ACCOUNTTYPE|SELECT"
	FormFields(10) = "txt_OPENINGBALANCE|INT"
	FormFields(11) = "sel_NOMINALACCOUNT|SELECT"
	OpenDB()
	
	Dim nlaccountname, nlaccountid, bankaccounttype, bankaccountid
	
	TheAction = Request("hid_Action")
	Select Case TheAction
		Case "NEW"		NewRecord()
		Case "AMEND"	AmendRecord()
		Case "DELETE"   DeleteRecord()
		Case "LOAD"	    LoadRecord()
	End Select
	CloseDB()

	Function NewRecord ()
		ID = Request.Form(UpdateID)
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO NL_BANKACCOUNT " & strSQL & ""
		Response.Write SQL
		Conn.Execute SQL, recaffected
		GO()		
	End Function
	
	Function AmendRecord()

		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		SQL = "UPDATE NL_BANKACCOUNT " & strSQL & " WHERE ACCOUNTID = " & ID
		Response.Write SQL
		Conn.Execute SQL, recaffected
		GO()
	End Function
	
	Function DeleteRecord()
		
		ID = Request.Form(UpdateID)
		response.write "<BR> Update id is : "& ID	
		SQL = "DELETE FROM NL_BANKACCOUNT WHERE ACCOUNTID = " & ID
		Conn.Execute SQL, recaffected
		GO()	
			
	End Function
	
	Function LoadRecord()
		ID = Request.Form(UpdateID)
		LoaderString = ""
		'Call OpenRs(rsLoader, "SELECT * FROM NL_BANKACCOUNT WHERE ACCOUNTID = " & ID)
		SQL = "SELECT 	ACC.ACCOUNTID AS NLACC, ACC.ACCOUNTNUMBER + ' ' + ACC.NAME AS NLNAME, " &_
				"		ISNULL(A.ACCOUNTTYPE,-1) AS ACCOUNTTYPE,  " &_
				"		A.ACCOUNTNAME, T.BANKACCOUNTTYPE, A.*,  " &_
				"		A.BANK, A.ACCOUNTNO, A.SORTCODE, A.ADD1, A.ADD2, A.TOWN, A.COUNTY, A.POSTCODE " &_
				"FROM	NL_BANKACCOUNT A " &_
				"		INNER JOIN NL_BANKACCOUNTTYPE T ON A.ACCOUNTTYPE = T.BANKACCOUNTTYPEID " &_
				"		INNER JOIN NL_ACCOUNT ACC ON ACC.ACCOUNTID = A.NOMINALACCOUNT " &_
				"WHERE 	A.ACCOUNTID = "  & ID
		Call OpenRs(rsLoader, SQL)
		RW SQL
		if not (rsLoader.EOF) then
			nlaccountname = rsLoader("NLNAME")
			nlaccountid = rsLoader("NLACC")
			bankaccounttype = rsLoader("BANKACCOUNTTYPE")
			bankaccountid = rsLoader("ACCOUNTTYPE")
			for i=0 to Ubound(FormFields)
				FormArray = Split(FormFields(i), "|")
				TargetItem = FormArray(0)
				LoaderString = LoaderString & "parent.RSLFORM." & TargetItem & ".value = """ & rsLoader(RIGHT(TargetItem, Len(TargetItem)-4)) & """" & VbCrLf
			next
		end if
		CloseRs(rsLoader)
	End Function
	
	Function GO()
		'Response.Redirect "../BANKACCOUNT.ASP"	
	End Function
	
	
%>
<html>
<body>
<script language=javascript>
	<% if TheAction = "LOAD" then %>
		parent.RSLFORM.sel_NOMINALACCOUNT.options[parent.RSLFORM.sel_NOMINALACCOUNT.options.length] = new Option('<%=nlaccountname%>','<%=nlaccountid%>', true);
		<%IF CINT(bankaccountid) <> 2 Then %>
			parent.RSLFORM.sel_ACCOUNTTYPE.options[parent.RSLFORM.sel_ACCOUNTTYPE.options.length] = new Option('<%=bankaccounttype%>','<%=bankaccountid%>', true);
	<% end if %>	
		<%=LoaderString%>
		parent.split_sortcode();
		parent.RSLFORM.hid_ACTION.value = "AMEND"
		parent.checkForm();
		parent.swap_div(3);
	<% Else %>
		parent.location.reload();
	<% end if %>
	
</script>
</body>
</html>