<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../Includes/functions.asp" -->
<%
Dim startdate, enddate, credit_total, debit_total, str_data
Dim FY, PS, PE
Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
Call OpenDB()
Call get_year(Request("FY"))
Call get_lastyearend()
Call BuildSelect(lstFY, "FY", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 onchange=""JAVASCRIPT:THISFORM.submit()"" ")

'LYE_FY - THIS CONTAINS THE LAST FISCAL YEAR FOR WHICH THEIR IS OPENING BALANCES
'IF FY >= LYE_FY THEN USE THE STARTDATE OBTAINED FROM THIS OTHERWISE USE THE ORIGINAL START DATE
if (CInt(FY) > Cint(LYE_FY)) then
	SQL_STARTDATE = LYE_STARTDATE
Else
	SQL_STARTDATE = PS	
End If
%>
<%
If Request("END") <> "" Then enddate = Request("END") Else enddate = PE End If
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<link rel="stylesheet" href="/js/Tree/dtree.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Tree/Tree_TwoCols.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --><br>
<script type="text/javascript">
<!--
		
	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"

	function convertDate(strDate) {
		strDate = new String(strDate);
		arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		arrSplit = strDate.split("/");
		return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
		}

	function click_go(){
		if (!checkForm()) return false;	
		DateError = false
		if (convertDate(document.getElementById("txt_TO").value) > convertDate("<%=PE%>") ){
			ManualError("img_TO", "The TO date must be less than or equal to '<%=PE%>'.", 0)
			DateError = true
			}	
		if (convertDate(document.getElementById("txt_FROM").value) < convertDate("<%=PS%>") ){
			ManualError("img_FROM", "The FROM date must be greater than or equal to '<%=PS%>'.", 0)
			DateError = true
			}
		if (DateError == true) return false;
		
		location.href = "trialbalance.ASP?START="+document.getElementById("txt_FROM").value+"&END="+document.getElementById("txt_TO").value
		}

	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}

//-->
</script>
<STYLE TYPE="TEXT/CSS">
#TABLE1 TD {BORDER-BOTTOM:1PX DOUBLE SILVER	}
</STYLE>
<TABLE CELLPADDING=1 CELLSPACING=2 ALIGN=CENTER>
	<TR><TD style='font-size:16px' ALIGN=CENTER><b><U>Trial Balance</U></b></TD></TR>
	<TR><TD  ALIGN=CENTER><%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD></TR>
	<TR><TD  ALIGN=CENTER><%=now%></TD></TR>
	<TR style='height:8px'><TD></TD></TR>
</TABLE>
<TABLE align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0>
<FORM NAME="THISFORM" METHOD="POST" ACTION="TrialBalance.asp">
	<TR>
		<TD><b>PERIOD :</b>&nbsp;<%=lstFY%></TD>		
		<TD ALIGN=RIGHT>
			
			<B>From&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_FROM" maxlength=12 size=12 tabindex=3 value='<%=STARTDATE%>' READONLY>
			<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
			&nbsp;&nbsp;To&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TO" maxlength=12 size=12 tabindex=3 value='<%=ENDDATE%>'>
			<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()"></B><br>
		</TD>
	</TR>
</FORM>	
</table>
<TABLE ID="TABLE1" align=CENTER WIDTH=95% CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:0PX SOLID BLACK;border-collapse:collapse' border=0>
		<THEAD>
		<TR> 
		<TD STYLE='WIDTH:65px' CLASS='TABLE_HEAD'>&nbsp;<B>Account</B> </TD>
		  <TD ALIGN=RIGHT> <B>Debit</B> </TD>
		  <TD ALIGN=RIGHT> <B>Credit</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=3 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
		<TR style='background-color:white'><TD COLSPAN=3>&nbsp;</TD></TR>
<%
	' Use a datashaping connection string to be used later.
	RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING
	// SQL NEEDS OPTIMISING -- 
	strSQL = "SHAPE {SELECT 	ISNULL(TOPLEVEL.ACCOUNTID,-1) AS ACCOUNTID, " &_
			 " 					ISNULL(TOPLEVEL.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
			 "					LTRIM(ISNULL(TOPLEVEL.ACCOUNTNUMBER,'') + ' ' + ISNULL(TOPLEVEL.FULLNAME,'')) AS LEVEL1NAME, " &_
			 "					ISNULL(AT.DESCRIPTION,'N/A') AS ACCOUNTTYPE, " &_
			 "					ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0) AS DEBIT1, " &_
			 "					ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0) AS CREDIT1, " &_
			 "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) > 0 THEN " &_
			 "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS DEBIT, " &_	
			 "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) < 0 THEN " &_
			 "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS CREDIT " &_	
			 "					FROM 	NL_ACCOUNT TOPLEVEL " &_
			 "					LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
			 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY ACCOUNTID) DBNOCHILD " &_
			 "					  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
			 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY ACCOUNTID) CRNOCHILD " &_
			 "				 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
			 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY PARENTREFLISTID) DBCHILD " &_
			 "						ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
			 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY PARENTREFLISTID) CRCHILD " &_
			 "						  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
			 "WHERE			TOPLEVEL.PARENTREFLISTID IS NULL " &_
			 "ORDER 	BY ACCOUNTNUMBER} " &_
			" APPEND((SHAPE {SELECT 	A.PARENTREFLISTID, A.ACCOUNTID, A.ACCOUNTNUMBER, A.ACCOUNTNUMBER + ' ' + A.NAME AS LEVEL2NAME, " &_
			"							AT.DESCRIPTION AS ACCOUNTTYPE, " &_
			"							ISNULL(DR.AMOUNT,0) AS DEBIT1, ISNULL(CR.AMOUNT,0) AS CREDIT1, " &_
			"							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) > 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS DEBIT, " &_
			"							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) < 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS CREDIT " &_		
			"				FROM 	NL_ACCOUNT A " &_
			"						INNER JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = A.ACCOUNTTYPE " &_
			"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY ACCOUNTID) CR ON CR.ACCOUNTID = A.ACCOUNTID " &_
			"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&ENDDATE&" 23:59' GROUP BY ACCOUNTID) DR ON DR.ACCOUNTID = A.ACCOUNTID " &_
			"				WHERE	PARENTREFLISTID IS NOT NULL ORDER BY ACCOUNTNUMBER}) " &_
			" AS LEVEL2 RELATE ACCOUNTID TO PARENTREFLISTID)"
	
	'RW strSQL
	' Open original recordset
	credit_total = 0
	debit_total = 0
	Set rsLEVEL1 = Server.CreateObject("ADODB.Recordset")
	rsLEVEL1.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
	count = 0
	Do While Not rsLEVEL1.EOF
		'if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
		RW "<TR STYLE='CURSOR:HAND' ONCLICK=""toggle('"&rsLEVEL1("ACCOUNTNUMBER")&"')""><TD>"&rsLEVEL1("LEVEL1NAME")&"</TD>"
		If rsLEVEL1("DEBIT") = 0 Then
			RW "<TD ALIGN=RIGHT>&nbsp;</TD>"
		Else
			debit_total = debit_total + ABS(rsLEVEL1("DEBIT"))
			RW "<TD ALIGN=RIGHT>"&FormatNumber(ABS(rsLEVEL1("DEBIT")))&" </TD>"
		End If
		If rsLEVEL1("CREDIT") = 0 Then
			RW "<TD ALIGN=RIGHT>&nbsp;</TD>"
		Else
			credit_total = credit_total + ABS(rsLEVEL1("CREDIT"))
			RW "<TD ALIGN=RIGHT>"&FormatNumber(ABS(rsLEVEL1("CREDIT")))&"</TD>"
		End If
		RW "</tr>"
		' Set object to child recordset and iterate through
		Set rsLEVEL2 = rsLEVEL1("LEVEL2").Value
		if not rsLEVEL2.EOF then
			Do While Not rsLEVEL2.EOF
				RW "<TR STYLE='DISPLAY:NONE' id='"&rsLEVEL1("ACCOUNTNUMBER")&"'>"
				rw "<TD><i>&nbsp;&nbsp;&nbsp;"&rsLEVEL2("LEVEL2NAME")&"</i></TD>"
				If rsLEVEL2("DEBIT") = 0 Then
					RW "<TD ALIGN=RIGHT>&nbsp;</TD>"
				Else
					RW "<TD ALIGN=RIGHT><i>"&FormatNumber(ABS(rsLEVEL2("DEBIT")))&"</i></TD>"
				End If
				If rsLEVEL2("CREDIT") = 0 Then
					RW "<TD ALIGN=RIGHT>&nbsp;</TD>"
				Else
					RW "<TD ALIGN=RIGHT><i>"&FormatNumber(ABS(rsLEVEL2("CREDIT")))&"</i></TD>"
				End If
				RW "</tr>"
				rsLEVEL2.MoveNext
			Loop
		end if
		count=count+1
		rsLEVEL1.MoveNext
	Loop
	
%>
	<TR style='background-color:white'><TD COLSPAN=3>&nbsp;</TD></TR>
	<TR BGCOLOR='whitesmoke'><TD></TD><TD STYLE='BORDER SOLID 1PX BLACK' ALIGN=RIGHT><b><%= FormatCurrency(abs(debit_total)) %></b></TD><TD ALIGN=RIGHT STYLE='BORDER SOLID 1PX BLACK'><B><%= FormatCurrency(abs(credit_total)) %></b></TD></TR>
		<TR STYLE='HEIGHT:3PX'><TD COLSPAN=5></TD></TR>
</TABLE>	
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

