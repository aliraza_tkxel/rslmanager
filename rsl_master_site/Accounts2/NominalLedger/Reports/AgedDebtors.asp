<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	dim yRaange
	set rsFYear = Server.CreateObject("ADODB.Recordset")
	rsFYear.ActiveConnection = RSL_CONNECTION_STRING
	if (Request("sel_PERIOD") = "") then
		rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YSTART <= GETDATE() AND YEND >= GETDATE()"		
	else
		yRaange=Request("sel_PERIOD")
		rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YRANGE = " & REQUEST("sel_PERIOD")
	end if
	rsFYear.CursorType = 0
	rsFYear.CursorLocation = 2
	rsFYear.LockType = 3
	rsFYear.Open()
	if (NOT rsFYear.EOF) then
		
		YearRange = rsFYear("YRange")
		StartDate = rsFYear("YStart")
		EndDate = rsFYear("YEnd")		
	end if
	rsFYear.close

	PS = CDate(FormatDateTime(StartDate,1))
	PE = CDate(FormatDateTime(Enddate,1))


	Dim CONST_PAGESIZE
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance
	Dim viewItemSelectedAll,viewItemSelected1,viewItemSelected2,viewItemSelected3, StrSrch
	dim InvoiveAmtSum,paymentSum, OpeningBalanceSum, outstandingSum
	dim BollFlag
	BollFlag=false
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	viewItemSelectedAll=""
	viewItemSelected1=""
	viewItemSelected2=""
	viewItemSelected3=""

	
	
	if Request("SEARCHNAME")="" Then
		StrSrch=0
	else
		StrSrch=Request("SEARCHNAME")
	end if
	if StrSrch=0 then
		viewItemSelectedAll="SELECTED"
	end if

	if StrSrch=1 then
		viewItemSelected1="SELECTED"
	end if
	if StrSrch=2 then
		viewItemSelected2="SELECTED"
	end if
	if StrSrch=3 then
		viewItemSelected3="SELECTED"
	end if


	
	
	PageName = "AgedDebtors.asp"
	MaxRowSpan = 6
	OpenDB()
	get_tenant_balance()
	get_Sum()
	CloseDB()

	function get_sum()
		dim rsSet
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		strSQL="exec f_get_agedbalance_report 1," & YearRange
		'rw strSQL
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.CursorLocation = 3
		rsSet.Open()
		if not rsSet.eof then
			InvoiveAmtSum=rsSet("InvoiveAmtSum")
			paymentSum=rsSet("paymentSum")
			OpeningBalanceSum=rsSet("OpeningBalanceSum")
			outstandingSum=rsSet("outstandingSum")
		end if
		rsSet.close()
		Set rsSet = Nothing
	end function



	Function get_tenant_balance()
	
			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount
			'orderBy = "rslt.cname"
			'if (Request("CC_Sort") <> "") then orderBy = " " & Request("CC_Sort") end if
			intRecord = 0
			str_data = ""
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			strSQL="exec f_get_agedbalance_report 0," & YearRange
			'rw strSQL
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
		
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			
			
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
			
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			
			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<TBODY CLASS='CAPS'>"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize
						
						str_data = str_data & "<TR>" &_
										"<TD>" & rsSet("cat") & "</TD>" &_
										"<TD>" &  rsSet("cname") & "</TD>" &_
										"<TD align=right> " & FORMATNUMBER(rsSet("InvoiveAmt"),2) & "</TD>" &_
										"<TD align=right> " & FORMATNUMBER(rsSet("OpeningBalance"),2) & "</TD>" &_
										"<TD align=right>" & FORMATNUMBER(rsSet("payment"),2) & "</TD>" &_									
										"<TD ALIGN=RIGHT>" & FORMATNUMBER(rsSet("outstanding"),2) & "</TD>" &_
										"</TR>"
						count = count + 1
						rsSet.movenext()
						If rsSet.EOF Then Exit for 

					Next
					str_data = str_data & "</TBODY>"
	
					'ensure table height is consistent with any amount of records
					fill_gaps()
					
					' links
					str_data = str_data &_
					"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
					"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
					"<A HREF = '" & PageName & "?page=1&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>First</font></b></a> "  &_
					"<A HREF = '" & PageName & "?page=" & prevpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange  & "'><b><font color=BLUE>Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <A HREF='" & PageName & "?page=" & nextpage & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
					" <A HREF='" & PageName & "?page=" & intPageCount & "&SEARCHNAME="&StrSrch&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange  & "'><b><font color=BLUE>Last</font></b></a>"  &_
					"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
					"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
					"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
				
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Supplier Balances</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()

	FormFields[0] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function JumpPage(){
		
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "AgedDebtors.ASP?page="+iPage+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function click_go(){
		
		if (!checkForm()) return false;
		location.href = "AgedDebtors.ASP?page=1&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
		}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=RSLFORM method=post>

  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
	<TR>
	<TD COLSPAN=6 CLASS='TABLE_HEAD'><b>FISCAL YEAR</b>: <SELECT NAME="sel_PERIOD" CLASS="textbox" STYLE='WIDTH:170PX'>
    <%
		OpenDB()
		SQL = "SELECT * FROM F_FISCALYEARS ORDER BY YRANGE"
		Call OpenRs(rsTheYears, SQL)
		While (NOT rsTheYears.EOF)
			isSelected = ""
			if (CStr(YearRange) = CStr(rsTheYears.Fields.Item("YRANGE").Value)) Then
				isSelected = " selected"
			end if
	%>
     <OPTION VALUE="<%=(rsTheYears.Fields.Item("YRANGE").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YSTART").Value) & " - " & (rsTheYears.Fields.Item("YEND").Value)%></OPTION>
    <%
	  		rsTheYears.MoveNext()
		Wend
	%>
	
  </SELECT><!-- &nbsp;&nbsp;<b>View By</b>
   <SELECT name="viewby" CLASS="textbox">
   	<option value="0" <%= viewItemSelectedAll%> >All</option>
   	<option value="1" <%= viewItemSelected1%>>Employe</option>
   	<option value="2" <%= viewItemSelected2%>>Supplier</option>
   	<option value="3" <%= viewItemSelected3%>>Tenent</option>
   </select>-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	Pagesize&nbsp;
	<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
	<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
	<input type="button" id="BTN_GO" name"BTN_GO" value=" GO " class=rslbutton onClick="click_go()">
	</TD>
	</TR> 
    <TR> 
      <TD WIDTH=150PX>&nbsp;<B>Category</B></TD>
      <TD WIDTH=450PX>&nbsp;<B>Name</B></TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Invoice Amount</B>&nbsp;</TD>
	  <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Opening Balance</B>&nbsp;</TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Payments</B>&nbsp;</TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Balance</B>&nbsp;</TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=10 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
        <TR style='background-color:white'>
			<TD style='background-color:white;color:black' COLSPAN=2 ALIGN=RIGHT><b>Total : </b></TD>
			<TD style='background-color:white;color:black' ALIGN=RIGHT><b> <% =formatcurrency(InvoiveAmtSum)%> </b></TD>
			<TD style='background-color:white;color:black' ALIGN=RIGHT><b> <% =formatcurrency(OpeningBalanceSum)%> </b></TD>
			<TD style='background-color:white;color:black' ALIGN=RIGHT><b> <% =formatcurrency(paymentSum)%> </b></TD>
			<TD style='background-color:white;color:black' ALIGN=RIGHT><b> <% =formatcurrency(outstandingSum)%> </b></TD>
		</TR>
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>
						