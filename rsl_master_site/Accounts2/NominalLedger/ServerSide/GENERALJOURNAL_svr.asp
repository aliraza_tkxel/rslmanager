<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	Dim str_date, str_ref, str_details, int_account,  str_memo
	Dim general_journal_id, editseq, amount, cmdText, txnid	
	
	editseq = 0
	OpenDB()	
	
	str_date 	= Request.Form("txt_DATE")
	str_ref 	= Replace(Request.Form("txt_REF"), "'", "''")	
	str_details 	= Replace(Request.Form("txt_DETAILS"), "'", "''")
	str_memo	= Replace(Request.Form("txt_MEMO"), "'", "''")

	OpenDB()
	
	' INSERT ROW IUNTO NL_GENERALJOURNAL
	strSQL = "SET NOCOUNT ON;" &_	
		"INSERT INTO NL_GENERALJOURNAL " &_
		"(GJDATE, USERID) VALUES ('" & FormatDateTime(str_date) & "', " & SESSION("USERID") & " )" &_
		";SELECT SCOPE_IDENTITY() AS GJID"
	Call OpenRs(rsGJ, strSQL)
	general_journal_id = rsGJ("GJID")
	Call CloseRs(rsGJ) 
	
	' CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ENTRY
	Set comm = Server.CreateObject("ADODB.Command")
	comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
	comm.commandtype = 4
	Set comm.activeconnection = Conn
	comm.parameters(1) = 13
	comm.parameters(2) = Cint(general_journal_id)
	comm.parameters(3) = now 
	comm.parameters(4) = FormatDateTime(str_date)
	comm.parameters(5) = str_ref
	comm.parameters(6) = null		
	comm.execute
	txnid = comm.parameters(0)
	rw txnid & "<BR>"
	
	' CREATE JOURNALENTRY DETAIL LINES
	for each item in Request.Form("ID_ROW")	

		editseq = editseq + 1
		Data = Request.Form("iData" & item)
		DataArray = Split(Data, "||<>||")
		
		if Cdbl(DataArray(0)) > 0 Then 
			cmdText = "F_INSERT_NL_JOURNALENTRYDEBITLINE" 
			amount = DataArray(0)
		Else 
			cmdText = "F_INSERT_NL_JOURNALENTRYCREDITLINE" 
			amount = DataArray(1)
		End If
		
		comm.commandtext = cmdText
		comm.parameters(1) = Clng(txnid)
		comm.parameters(2) = Cint(DataArray(2))
		comm.parameters(3) = now 
		comm.parameters(4) = Cint(editseq)
		comm.parameters(5) = FormatDateTime(str_date)
		comm.parameters(6) = amount
		comm.parameters(7) = str_details
		comm.parameters(8) = str_memo
		If Cdbl(str_debit) > 0 Then comm.parameters(9) = null end if
		comm.execute

	next
	
	Set comm = Nothing
	
	CloseDB()

	Response.Redirect "/ACCOUNTS/NOMINALLEDGER/generaljournallist.asp"
%>

