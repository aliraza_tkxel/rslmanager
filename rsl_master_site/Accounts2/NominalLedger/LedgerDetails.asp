<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance
	Dim AccountNumber, FullName, AccountType, amitop
	Dim startdate, enddate, openingbalance, searchstring, searchstringSQL

	Dim PS, PE, FY
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
		
	Call OpenDB()

	FY = Request("FY")
	get_year(FY)					' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	get_account_details(Request("LID"))		' GET THE ACCOUNT HEADER INFORMATION
	
	If Request("START") <> "" Then startdate = Request("START") Else startdate = PS End If
	If Request("END") <> "" Then enddate = Request("END")Else enddate = PE End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 25 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("SEARCHSTRING") = "" Then SearchFactor = "" Else SearchFactor = "%" & Replace(Request("SEARCHSTRING"), "'", "''") & "%" End If
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	' BUILD PAGED REPORT
	Function get_breakdown()
		
		AccountID = Request("LID")
		Dim strSQL, rsSet, intRecord 

		str_data = ""
		set rsPager = Conn.execute("EXEC NL_LEDGERDETAILS " & CInt(intPage) & ", " & CONST_PAGESIZE & ", '" & FormatDateTime(STARTDATE,1)&  "', '" & FormatDateTime(enddate,1) & "', " & AccountID & ", '" & SearchFactor & "' ")

		intPageCount = rsPager("TOTALPAGES")
		intRecordCount = rsPager("TOTALROWS")
		intPage = rsPager("CURRENTPAGE")

		set rsSet = rsPager.NextRecordset()
		Set rsPager = Nothing
		
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' CALCULATE BALANCE FOR TOP OF PAGE
		'VERSION 2  REWRITE (ALI)
		'SEPARATED THE CALLS TO OPENING BALANCE, PAGE BALANCE AND MONTH BALANCE.
		'INCLUDE FUNCTIONS ALSO MODIFIED.
		if (SearchFactor = "") then
			Opening_Balance = get_opening_balance(AccountID, FY)
			If CDATE(PS) = CDATE(startdate) Then
				page_balance = get_balance(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, 1)
				month_balance = 0
			Else
				page_balance = get_balance(intPage, startdate, enddate, AccountID, CONST_PAGESIZE, 2)
				month_balance = get_balance(1000000, PS, DateAdd("d", -1, startdate), AccountID, CONST_PAGESIZE, 2)
			End If
			balance = Opening_Balance + month_balance + page_balance
			running_balance = balance
			' ENTER OPENING BALANCE		
		else
			Opening_Balance = 0
			page_balance = 0
			running_balance = 0
		end if
		
		Response.Write "<TBODY CLASS='CAPS'>"
		Response.Write "<TR style='background-color:thistle'><TD COLSPAN=5><B>Brought Forward</B></TD>" &_
			"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(balance,2),2)&"</B></TD></TR>" 

		count = 1

		while NOT rsSet.EOF
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
			str_data = "<TR VALIGN=TOP STYLE='CURSOR:HAND"&color&"' ONCLICK=""FindJournal(" & rsSet("TXNID") & ")"">" &_
				"<TD >&nbsp;" & rsSet("TXNDATE") & "</TD>" &_
				"<TD NOWRAP>" & rsSet("DESCRIPTION") & "</TD>" &_
				"<TD TITLE=""" & rsSet("MEMO") & """>" & rsSet("ITEMDESC") & "</TD>" 
				' SHOW DEBIT
			If rsSet("DEBIT") = 0 Then 
				str_data = str_data & "<TD NOWRAP></TD>" 
			Else
				running_balance = running_balance + cdbl(rsSet("DEBIT"))
				str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:RED'>"&FormatNumber(rsSet("DEBIT"),2)&"</TD>" 
			End If
			' SHOW CREDIT
			If rsSet("CREDIT") = 0 Then 
				str_data = str_data & "<TD NOWRAP></TD>" 
			Else
				running_balance = running_balance - cdbl(rsSet("CREDIT"))
				str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:BLUE'>"&FormatNumber(rsSet("CREDIT"),2)&"</TD>" 
			End If

			' SHOW RUNNING BALANCE
			str_data = str_data & "<TD NOWRAP ALIGN=RIGHT>"&FormatNumber(round(running_balance,2),2)&"</TD></TR>"
			Response.Write str_data
			count = count + 1
			rsSet.movenext()
		wend

		if (count = 1) then
			if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if		
			Response.Write "<TR style='" & color & "'><TD COLSPAN=6 align=center><B>No journal entries found.</B></TD></TR>"
			count = count + 1 
		end if
		
		'ensure table height is consistent with any amount of records
		fill_gaps()

		' END BALANCE
		Response.Write "<TR style='background-color:thistle'><TD COLSPAN=5><B></B></TD>" &_
		"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(running_balance,2),2)&"</B></TD></TR></TBODY>" 
		
		' LINKS
		Response.Write "<TFOOT><TR><TD COLSPAN=6 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
		"<A HREF = 'LEDGERDETAILS.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=1&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color=BLUE>First</font></b></a>&nbsp;" &_
		"<A HREF = 'LEDGERDETAILS.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color=BLUE>Prev</font></b></a>" &_
		" Page <b><FONT COLOR=RED>" & intpage & "</FONT></b> of " & intPageCount & ". Records: " & CONST_PAGESIZE * (intpage - 1) + 1 & "  to " & (intPage-1)*CONST_PAGESIZE+(count-1) &	" of " & intRecordCount &_
		"&nbsp;<A HREF='LEDGERDETAILS.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color=BLUE>Next</font></b></a>&nbsp;" &_ 
		"<A HREF='LEDGERDETAILS.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & intPageCount & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "'><b><font color=BLUE>Last</font></b></a>" &_			
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;<input type='button' name=btnGo value='GO' class='rslbutton' onclick='JumpPage()'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		
		rsSet.close()
		Set rsSet = Nothing
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt, innercount
		cnt = 0
		innercount = count
		tr_num = CONST_PAGESIZE - count
		while (cnt <= tr_num)
			if innercount mod 2 = 0 then color = "STYLE='background-color:beige'" else color = "" end if
			Response.Write "<TR "&color&"><TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD>" &_
								  "<TD>&nbsp;</TD><TD>&nbsp;</TD><TD>&nbsp;</TD></TR>"
			cnt = cnt + 1
			innercount = innercount + 1
		wend		
	End Function
	

%>
<html>
<head>
<title>Ledger Details - <%=FullName%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function JumpPage(){
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "LEDGERDETAILS.ASP?SEARCHSTRING="+document.getElementById("txt_LIKE").value+"&page="+iPage+"&START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value+"&LID=<%=Request("LID")%>&FY=<%=FY%>" 
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	
	
	function FindJournal(txnid){
		location.href = "LedgerItem.asp?page=<%=intPage%>&pagesize=<%=CONST_PAGESIZE%>&startdate=<%=startdate%>&enddate=<%=enddate%>&LID=<%=Request("LID")%>&FY=<%=FY%>&TXNID=" + txnid + "&SEARCHSTRING=<%=Request("SEARCHSTRING")%>"
		}
	

	function convertDate(strDate) {
		strDate = new String(strDate);
		arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
		arrSplit = strDate.split("/");
		return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
		}

	function click_go(){
		if (!checkForm()) return false;	
		DateError = false
		if (convertDate(document.getElementById("txt_TO").value) > convertDate("<%=PE%>") ){
			ManualError("img_TO", "The TO date must be less than or equal to '<%=PE%>'.", 0)
			DateError = true
			}	
		if (convertDate(document.getElementById("txt_FROM").value) < convertDate("<%=PS%>") ){
			ManualError("img_FROM", "The FROM date must be greater than or equal to '<%=PS%>'.", 0)
			DateError = true
			}
		if (DateError == true) return false;	
		location.href = "LEDGERDETAILS.ASP?SEARCHSTRING="+document.getElementById("txt_LIKE").value+"&page=1&START="+document.getElementById("txt_FROM").value+"&END="+document.getElementById("txt_TO").value+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&FY=<%=FY%>&LID=<%=Request("LID")%>"
		}

</SCRIPT>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="white" text="#000000" onload="window.focus()" class='ta'>
<form name = RSLFORM method=post> 
<TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=1 align=center>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Account :</b></TD><TD valign=middle >&nbsp;<%=AccountNumber %> - <%=FullName%>&nbsp;[<font color=blue><%=AccountType%></font>]<BR></TD>
		<TD ALIGN=RIGHT rowspan=2 valign=top><input type="BUTTON" name="btnBack" value="Summary" class=rslbutton onclick="location.href='Ledger.asp?LID=<%=Request("LID")%>&FY=<%=FY%>'" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
			<input type="BUTTON" name="btnClose" value="Close" class=rslbutton onclick="window.close()" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
		</TD>
	</TR>
	<TR>
		<TD style='border:1px solid black;background-color:thistle;color:white;height:15' width=100 valign=middle><b>&nbsp;Period :</b></TD><TD valign=middle>&nbsp;<%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%></TD>
	</TR>
</TABLE>
<br>
<TABLE align=center WIDTH=750 CELLPADDING=1 CELLSPACING=0 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=thistle>
		<THEAD><TR STYLE='HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71'><TD COLSPAN=6 CLASS='TABLE_HEAD'></TD></TR>
		<TR>
			<TD ALIGN=LEFT COLSPAN=6 CLASS='TABLE_HEAD'>
			<span title='Enter any keyword/reference code to search for'><B>&nbsp;Search&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_LIKE" maxlength=12 size=12 tabindex=3 value='<%=Request("SEARCHSTRING")%>'>
			<image src="/js/FVS.gif" name="img_LIKE" width="15px" height="15px" border="0"></B></span>
			&nbsp;&nbsp;Dates:&nbsp;&nbsp;<B>From&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_FROM" maxlength=12 size=12 tabindex=3 value='<%=STARTDATE%>'>
			<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
			&nbsp;&nbsp;To&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TO" maxlength=12 size=12 tabindex=3 value='<%=ENDDATE%>'>
			<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
			&nbsp;&nbsp;Pagesize&nbsp;
			<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()"></B>&nbsp;&nbsp;
			</TD>
		</TR>			
		<TR> 
		<TD STYLE='WIDTH:80px' NOWRAP CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD STYLE='WIDTH:35px' NOWRAP CLASS='TABLE_HEAD'> <B>Type</B> </TD>
		  <TD CLASS='TABLE_HEAD' WIDTH=360> <B>Description</B> </TD>
		  <TD STYLE='WIDTH:90px' NOWRAP CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Debit</B> </TD>
		  <TD STYLE='WIDTH:90px' NOWRAP CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Credit</B> </TD>
		  <TD STYLE='WIDTH:100px' NOWRAP CLASS='TABLE_HEAD' ALIGN=CENTER> <B>Balance</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=6 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
<%
get_breakdown()
Call CloseDB()
%>
	</TABLE>
</FORM>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
