<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include FILE="Includes/functions.asp" -->
<%

Dim FY, PS, PE
Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE
Call OpenDB()
Call get_year(Request("FY"))
Call get_lastyearend()
Call BuildSelect(lstFY, "FY", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, YEND, 103)", "YRANGE", "Please Select...", FY, NULL, "textbox200", " tabindex=1 onchange=""JAVASCRIPT:THISFORM.submit()"" ")

'LYE_FY - THIS CONTAINS THE LAST FISCAL YEAR FOR WHICH THEIR IS OPENING BALANCES
'IF FY >= LYE_FY THEN USE THE STARTDATE OBTAINED FROM THIS OTHERWISE USE THE ORIGINAL START DATE
if (CInt(FY) > Cint(LYE_FY)) then
	SQL_STARTDATE = LYE_STARTDATE
Else
	SQL_STARTDATE = PS	
End If
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<link rel="stylesheet" href="/js/Tree/dtree.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Tree/Tree_TwoCols.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function OpenWin(ACCNUM, ACCID){
		window.open("Ledger.asp?LID=" + ACCID + "&AN=" + ACCNUM + "&FY=<%=FY%>&Random=" + new Date(), "_blank", "TOP=100, LEFT=200, scrollbars=yes, height=590, width=790, status=YES, resizable= Yes")	
		}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" --><br>
<TABLE CELLPADDING=1 CELLSPACING=2 ALIGN=CENTER>
	<TR><TD style='font-size:16px' ALIGN=CENTER><b><U>Nominal Ledger</U></b></TD></TR>
	<TR><TD  ALIGN=CENTER><%=FormatDateTime(PS,1)%> - <%=FormatDateTime(PE,1)%></TD></TR>
	<TR><TD  ALIGN=CENTER><%=now%></TD></TR>
	<TR style='height:8px'><TD></TD></TR>
</TABLE>
<TABLE width=100%>
<form name="THISFORM" method="POST">
	<TR>
		<TD>[<a href="javascript:d.openAll()"><b style="color:tomato">OPEN ALL</b></a>]&nbsp;&nbsp;[<a href="javascript:d.closeAll()"><b style="color:tomato">CLOSE ALL</b></a>]&nbsp;&nbsp;[<a href="NominalLedger.asp"><b style="color:tomato">UPDATE</b></a>]</TD>
		<TD align=right><b>PERIOD :</b>&nbsp;<%=lstFY%>&nbsp;&nbsp;&nbsp;</TD>
	</TR>
</form>	
</TABLE>

<div class="dtree" style='height:100%'>

	<script type="text/javascript">
		<!--
		d = new dTree('d');
		d.config.useStatusText=true;
		d.config.useIcons=false;		
		d.add(0,-1,'<b style="color:steelblue">Account Number &nbsp;&nbsp; &nbsp;&nbsp; Account Name</b>','','','','<b style="color:steelblue">Account Type</b>&nbsp;&nbsp;','<b>Debit</b>','<b>Credit</b>');
<%
dim rsLEVEL2, rsLEVEL3, rsLEVEL1, credit_total, debit_total
credit_total = 0
debit_total = 0

' Use a datashaping connection string to be used later.
RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;" & RSL_CONNECTION_STRING
// SQL NEEDS OPTIMISING -- 
strSQL = "SHAPE {SELECT 	ISNULL(TOPLEVEL.ACCOUNTID,-1) AS ACCOUNTID, " &_
		 " 					ISNULL(TOPLEVEL.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
		 "					LTRIM(ISNULL(TOPLEVEL.ACCOUNTNUMBER,'') + ' ' + ISNULL(TOPLEVEL.FULLNAME,'')) AS LEVEL1NAME, " &_
		 "					ISNULL(AT.DESCRIPTION,'N/A') AS ACCOUNTTYPE, " &_
		 "					ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0) AS DEBIT1, " &_
		 "					ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0) AS CREDIT1, " &_
		 "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) > 0 THEN " &_
		 "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS DEBIT, " &_	
		 "					CASE WHEN (ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) < 0 THEN " &_
		 "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) ELSE 0 END AS CREDIT " &_	
		 "					FROM 	NL_ACCOUNT TOPLEVEL " &_
		 "					LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY ACCOUNTID) DBNOCHILD " &_
		 "					  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY ACCOUNTID) CRNOCHILD " &_
		 "				 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY PARENTREFLISTID) DBCHILD " &_
	  	 "						ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY PARENTREFLISTID) CRCHILD " &_
		 "						  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "WHERE			TOPLEVEL.PARENTREFLISTID IS NULL " &_
		 "ORDER 	BY ACCOUNTNUMBER} " &_
		" APPEND((SHAPE {SELECT 	A.PARENTREFLISTID, A.ACCOUNTID, A.ACCOUNTNUMBER, A.ACCOUNTNUMBER + ' ' + A.NAME AS LEVEL2NAME, " &_
		"							AT.DESCRIPTION AS ACCOUNTTYPE, " &_
		"							ISNULL(DR.AMOUNT,0) AS DEBIT1, ISNULL(CR.AMOUNT,0) AS CREDIT1, " &_
		"							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) > 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS DEBIT, " &_
		"							CASE WHEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) < 0 THEN (ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0)) ELSE 0 END AS CREDIT " &_		
		"				FROM 	NL_ACCOUNT A " &_
		"						INNER JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = A.ACCOUNTTYPE " &_
		"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY ACCOUNTID) CR ON CR.ACCOUNTID = A.ACCOUNTID " &_
		"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '"&SQL_STARTDATE&" 00:00' AND TXNDATE <= '"&PE&" 23:59' GROUP BY ACCOUNTID) DR ON DR.ACCOUNTID = A.ACCOUNTID " &_
		"				WHERE	PARENTREFLISTID IS NOT NULL ORDER BY ACCOUNTNUMBER}) " &_
		" AS LEVEL2 RELATE ACCOUNTID TO PARENTREFLISTID)"

'RW strSQL
' Open original recordset
Set rsLEVEL1 = Server.CreateObject("ADODB.Recordset")
rsLEVEL1.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING

currenti = 5
LEVEL2PARENT = 0

Do While Not rsLEVEL1.EOF

	Response.Write "d.add(" & currenti & ",0,'" & rsLEVEL1("LEVEL1NAME") & "','javascript:OpenWin(" & rsLEVEL1("ACCOUNTNUMBER") & "," & rsLEVEL1("ACCOUNTID") & ")','','','" & rsLEVEL1("ACCOUNTTYPE") & "','" & FormatNumber(ABS(rsLEVEL1("DEBIT"))) & "','" & FormatNumber(ABS(rsLEVEL1("CREDIT"))) & "');"
	currenti = currenti + 1
	credit_total = credit_total + rsLEVEL1("CREDIT")
	debit_total = debit_total + rsLEVEL1("DEBIT")

    ' Set object to child recordset and iterate through
    Set rsLEVEL2 = rsLEVEL1("LEVEL2").Value
    if not rsLEVEL2.EOF then
		LEVEL2PARENT = currenti - 1
        Do While Not rsLEVEL2.EOF
			Response.Write "d.add(" & currenti & "," & LEVEL2PARENT & ",'" & rsLEVEL2("LEVEL2NAME") & "','javascript:OpenWin(" & rsLEVEL2("ACCOUNTNUMBER") & "," & rsLEVEL2("ACCOUNTID") & ")','','','" & rsLEVEL2("ACCOUNTTYPE") & "','" & FormatNumber(ABS(rsLEVEL2("DEBIT"))) & "','" & FormatNumber(ABS(rsLEVEL2("CREDIT"))) & "');"
			currenti = currenti + 1			
            rsLEVEL2.MoveNext
        Loop
	end if
    rsLEVEL1.MoveNext
Loop

%>		
		document.write(d);
		//-->
	</script>
<TABLE BORDER=1 STYLE='DISPLAY:block'>
	<TR><TD STYLE='WIDTH:453PX'></TD><TD BGCOLOR='SILVER' STYLE='WIDTH:136PX;BORDER SOLID 1PX BLACK' ALIGN=RIGHT><b><%= FormatCurrency(abs(debit_total)) %></b></TD><TD BGCOLOR='SILVER' ALIGN=RIGHT STYLE='WIDTH:140PX;BORDER SOLID 1PX BLACK'><B><%= FormatCurrency(abs(credit_total)) %></b></TD></TR>
</TABLE>	
</div>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

