<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim clean_desc

	ColData(0)  = "Period|THEPERIOD|160"
	SortASC(0) 	= "FY.YRANGE ASC"
	SortDESC(0) = "FY.YRANGE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""
		
	ColData(1)  = "From Exp.|FROMEXP|120"
	SortASC(1) 	= "FROMEXP ASC"
	SortDESC(1) = "PO.FROMEXP DESC"
	TDSTUFF(1)  = " "" title=""""Head : "" & rsSet(""HD1DESC"") & "" &#13;Cost Centre : "" & rsSet(""CC1DESC"") & "" """" "" "
	TDFunc(1) = ""

	ColData(2)  = "To Exp.|TOEXP|120"
	SortASC(2) 	= "TOEXP ASC"
	SortDESC(2) = "TOEXP DESC"	
	TDSTUFF(2)  = " "" title=""""Head : "" & rsSet(""HD2DESC"") & "" &#13;Cost Centre : "" & rsSet(""CC2DESC"") & "" """" "" "
	TDFunc(2) = ""		

	ColData(3)  = "Amount|TRANSFERAMOUNT|100"
	SortASC(3) 	= "TRANSFERAMOUNT ASC"
	SortDESC(3) = "TRANSFERAMOUNT DESC"	
	TDSTUFF(3)  = " ""align='right'"" "
	TDFunc(3) = "FormatCurrency(|)"		

	ColData(4)  = "On|TIMESTAMP|80"
	SortASC(4) 	= "TIMESTAMP ASC"
	SortDESC(4) = "TIMESTAMP DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "By|FULLNAME|80"
	SortASC(5) 	= "FULLNAME ASC"
	SortDESC(5) = "FULLNAME DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""		

	PageName = "BudgetVirementList.asp"
	EmptyText = "No Relevant Transfers found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT EX1.DESCRIPTION AS TOEXP, EX2.DESCRIPTION AS FROMEXP, EV.TRANSFERAMOUNT, CONVERT(VARCHAR, EV.TIMESTAMP, 103) AS TIMESTAMP, " &_
				"HD1.DESCRIPTION AS HD1DESC, CC1.DESCRIPTION AS CC1DESC, HD2.DESCRIPTION AS HD2DESC, CC2.DESCRIPTION AS CC2DESC, " &_
				"UD.FIRSTNAME AS FULLNAME, CONVERT(VARCHAR, FY.YSTART, 103) + ' - ' + CONVERT(VARCHAR, FY.YEND, 103) AS THEPERIOD FROM F_EXPENDITURE_VIREMENT EV " &_
				"INNER JOIN F_FISCALYEARS FY ON FY.YRANGE = EV.FISCALYEAR " &_ 
				"INNER JOIN F_EXPENDITURE EX1 ON EX1.EXPENDITUREID = EV.TO_EXP " &_
				"INNER JOIN F_HEAD HD1 ON HD1.HEADID = EX1.HEADID " &_
				"INNER JOIN F_COSTCENTRE CC1 ON CC1.COSTCENTREID = HD1.COSTCENTREID " &_
				"INNER JOIN F_EXPENDITURE EX2 ON EX2.EXPENDITUREID = EV.FROM_EXP " &_							
				"INNER JOIN F_HEAD HD2 ON HD2.HEADID = EX2.HEADID " &_
				"INNER JOIN F_COSTCENTRE CC2 ON CC2.COSTCENTREID = HD2.COSTCENTREID " &_
				"INNER JOIN E__EMPLOYEE UD ON UD.EMPLOYEEID = EV.USERID " &_
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager - Budget Virement List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(Order_id){
	window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
	}

function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

// -->
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="iagManagerSmallBlk" width=750><tr>
<td width=100%>This page shows a list of all budget virements.</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>