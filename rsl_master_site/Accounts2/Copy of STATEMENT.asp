<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include FILE="NominalLedger/Includes/functions.asp" -->
<%

	Dim startdate, enddate, PS, PE
	
	get_year(6)
	If Request("START") = "" Then 
		startdate = PS
	Else
		startdate = Request("START")
	End If
	If Request("END") = "" Then 
		enddate = PE
	Else
		enddate = Request("END")
	End If
	If Request("PAGESIZE") = "" Then 
		CONST_PAGESIZE = 20 
	Else
		CONST_PAGESIZE = Request("PAGESIZE")
	End If
	
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size
	Dim nopounds
	Dim searchSQL,searchName
	Dim lstAccounts, balance, today, running_balance, current_day, prev_balance, end_balance
	
	OpenDB()
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	if cint(intPage) = 1 Then 
		balance = get_balance()
		running_balance = get_balance()
		'prev_balance = get_balance()
	Else
		balance = cdbl(Request("RB"))
		If Request("TT") = 1 Then
			running_balance = cdbl(Request("PB"))
		Else
			running_balance = cdbl(Request("RB"))
		End If
	End If

	'RESPONSE.WRITE "rb : " & running_balance
	'RESPONSE.WRITE "<BR>pr : " & prev_balance
	getStatement()
	
	Function get_balance()

		Dim b
		
		' GET OPENEING BALANCE
		SQL = "SELECT OPENINGBALANCE FROM NL_BANKACCOUNT WHERE ACCOUNTID = 1 "
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			b = rsSet(0)
		Else
			b = 0
		End If
		
		' GET TRANSACTION BALANCE UP TOAND NOT INCLUDING  STARTDAET
		SQL = "SELECT ISNULL(SUM(DEBIT) - SUM(CREDIT),0) AS BALANCE FROM ba_master_statement_all " &_
		" WHERE ENDDATE < '"&FormatDateTime(STARTDATE,1)&"' AND ENDDATE > '1/4/2004' "
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			b = rsSet(0) + b
		Else
			b = 0
		End If
		get_balance = b
		CloseRs(rsSet)
		
	End Function
	
	Function get_end_balance()
	
		SQL = "SELECT SUM(DEBIT) - SUM(CREDIT) AS BALANCE FROM ba_master_statement_all " &_
			" WHERE TRANSACTIONDATE <= '"&FormatDateTime(enddate,1)&"' "
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			end_balance = (rsSet(0) + balance)
		Else
			balance = 0
		End If
		CloseRs(rsSet)
	
	End Function

	
	' retrives team details from database and builds 'paged' report
	Function getStatement()
		
		Dim strSQL, rsSet, intRecord 
		
		' STORE PREVIOUS BALANCE BEFORE RUNNING BALANCE BECOMES UPDATED
		intRecord = 0
		str_data = ""
		'strSQL = 	"SELECT	ISNULL(CONVERT(NVARCHAR, RECDATE, 103) ,'') AS RECDATE, CODE, DESCRIPTION, CREDIT, DEBIT FROM ba_master_statement_all " &_
		'		" WHERE ENDDATE >= '"&FormatDateTime(STARTDATE,1)&"' AND ENDDATE <= '"&FormatDateTime(enddate,1)&"' ORDER BY ENDDATE "
		
		strSQL = 	"SELECT GROUPINGID, 'Rent Journal' as TYPE, COUNT(*) AS ITEMCOUNT,  'Support Payment' AS DESCRIPTION, " &_
					"		ISNULL(SUM(RJ.AMOUNT),0) AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"FROM F_RENTJOURNAL RJ  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1 " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID  " &_
					"		INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID  " &_
					"		INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID  " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					"WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' AND RJ.PAYMENTTYPE in (20,21) " &_
					"GROUP BY GROUPINGID, BS.STATEMENTDATE " &_
					"UNION ALL " &_
					"SELECT GROUPINGID, 'Rent Journal' as TYPE, COUNT(*) AS ITEMCOUNT,  'Housing Benefit' AS DESCRIPTION, " &_
					"		ISNULL(SUM(RJ.AMOUNT),0) AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"FROM F_RENTJOURNAL RJ  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1 " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID  " &_
					"		INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID  " &_
					"		INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID   " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					"	WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' AND RJ.PAYMENTTYPE IN (1,15,16) " &_
					"GROUP BY GROUPINGID,   BS.STATEMENTDATE " &_
					"UNION ALL " &_
					"SELECT GROUPINGID, 'Rent Journal' as TYPE, COUNT(*) AS ITEMCOUNT,  'Payment Slip' AS DESCRIPTION, " &_
					"		ISNULL(SUM(RJ.AMOUNT),0) AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"FROM F_RENTJOURNAL RJ  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1 " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID  " &_
					"		INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID  " &_
					"		INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID  " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					"WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' AND RJ.PAYMENTTYPE in (5,8) " &_
					"GROUP BY GROUPINGID,   BS.STATEMENTDATE	" &_
					"UNION ALL " &_
					"SELECT GROUPINGID, 'Rent Journal' as TYPE, COUNT(*) AS ITEMCOUNT,  P.DESCRIPTION, " &_
					"	ISNULL(SUM(RJ.AMOUNT),0) AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"	FROM F_RENTJOURNAL RJ  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND BR.RECTYPE = 1 " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID  " &_
					"		INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID  " &_
					"		INNER JOIN P_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID  " &_
					"		LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE  " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					" WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' AND PAYMENTTYPE NOT IN (1,5,8,15,16,20,21) " &_
					"GROUP BY GROUPINGID,  P.DESCRIPTION, BS.STATEMENTDATE " &_
					"UNION ALL " &_
					"SELECT GROUPINGID, 'Invoices' AS TYPE, BD.INVOICECOUNT,  " &_
					"	PT.DESCRIPTION AS PAYMENTTYPE, " &_ 
					"	BD.TOTALVALUE AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"	FROM F_BACSDATA BD  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND BR.RECTYPE = 2 " &_
					"		INNER JOIN F_POBACS PB ON PB.DATAID = BD.DATAID  " &_
					"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = PB.INVOICEID  " &_
					"		LEFT JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID  " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					" WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' " &_
					"	GROUP BY GROUPINGID, BD.INVOICECOUNT, BD.TOTALVALUE, PT.DESCRIPTION, BS.STATEMENTDATE  " &_
					"UNION ALL " &_					
					"SELECT GROUPINGID, 'Staff Expenses' AS TYPE, COUNT(*) AS THECOUNT,  " &_
					"	'' AS DESCRIPT, " &_ 
					"	SUM(PI.GROSSCOST) AS AMOUNT, ISNULL(CONVERT(NVARCHAR, BS.STATEMENTDATE, 103) ,'') AS STATEMENTDATE " &_
					"	FROM F_PURCHASEITEM PI  " &_
					"		INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = PI.ORDERITEMID AND BR.RECTYPE = 3 " &_
					"		INNER JOIN F_BANK_STATEMENTS BS ON BS.BSID = BR.BSID " &_
					" WHERE BS.STATEMENTDATE >= '"&STARTDATE&"' AND BS.STATEMENTDATE <= '"&ENDDATE&"' " &_
					"	GROUP BY GROUPINGID, BS.STATEMENTDATE " &_
					"ORDER BY STATEMENTDATE, GROUPINGID, P.DESCRIPTION " 
		
		'response.write strSQL					
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
				
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0	
		If intRecordCount > 0 Then
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
			For intRecord = 1 to rsSet.PageSize
			' ENTER OPENING BALANCE
				If intRecord = 1 And intPage = 1 Then
					str_data = str_data & "<TR>" &_
						"<TD COLSPAN=5><B>Opening Balance</B></TD>" &_
						"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(balance,2),2)&"</B></TD></TR>" 
						count = count + 1
 				Else	
					str_data = str_data & "<TR STYLE='CURSOR:HAND'>" &_
						"<TD NOWRAP>&nbsp;" & rsSet("STATEMENTDATE") & "</TD>" &_
						"<TD NOWRAP>" & rsSet("ITEMCOUNT") & "</TD>" &_
						"<TD NOWRAP>" & rsSet("DESCRIPTION") & "</TD>" 
						' SHOW DEBIT
						If rsSet("AMOUNT") > 0 Then 
							str_data = str_data & "<TD NOWRAP></TD>" 
						Else
							running_balance = running_balance + cdbl(ABS(rsSet("AMOUNT")))
							str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:BLACK'><B>"&FormatNumber(abs(rsSet("AMOUNT")),2)&"</b></TD>" 
						End If
						' SHOW CREDIT
						If rsSet("AMOUNT") < 0 Then 
							str_data = str_data & "<TD NOWRAP></TD>" 
						Else
							running_balance = running_balance - cdbl(ABS(rsSet("AMOUNT")))
							str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:blue'><B>"&FormatNumber(abs(rsSet("AMOUNT")),2)&"</b></TD>" 
						End If
						' SHOW BALANCE
						If count mod 5 <> 0 Then 
							str_data = str_data & "<TD NOWRAP></TD>" 
						Else
							my_balance = running_balance
							str_data = str_data & "<TD NOWRAP ALIGN=RIGHT>"&FormatNumber(round(my_balance,2),2)&"</TD>" 
							'str_data = str_data & "<TD NOWRAP></TD>" 
						End If
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
			End If
			Next
			'BALANCE AT BOTTOM PAGE
			str_data = str_data & "<TR>" &_
			"<TD COLSPAN=5><B></B></TD>" &_
			"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(running_balance,2),2)&"</B></TD></TR>" 
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			'str_data = str_data &_
			'"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			'"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
			'"<A HREF = 'CASHBOOK.asp?page=1&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&balance&"'><b><font color=BLUE>First</font></b></a> " &_
			'"<A HREF = 'CASHBOOK.asp?page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&prev_balance&"'><b><font color=BLUE>Prev</font></b></a>" &_
			'" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			'" <A HREF='CASHBOOK.asp?page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&running_balance&"'><b><font color=BLUE>Next</font></b></a>" &_ 
			'" <A HREF='CASHBOOK.asp?page=" & intPageCount & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&end_balance&"'><b><font color=BLUE>Last</font></b></a>" &_
			'"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			'"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			'"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
			"<A HREF = 'STATEMENT.asp?TT=1&page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&PB="&Request("PB")&"&RB="&Request("RB")&"'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='STATEMENT.asp?TT=2&page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&PB="&Request("RB")&"&RB="&running_balance&"'><b><font color=BLUE>Next</font></b></a>" &_ 
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			
		End If
		
		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TABLE><TR><TD COLSPAN=6 ALIGN=CENTER><B>No transactions exist for this account !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function
	
	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --&gt; Tools --&gt; Development Setup</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"

	// called by save and amend buttons
	function click_save(str_action){
		if (!checkForm()) return false;
		RSLFORM.target = "frm_bank_account";
		RSLFORM.hid_ACTION.value = str_action
		RSLFORM.action = "serverside/bankaccount_srv.asp"
		RSLFORM.submit();
	}

	
	function click_go(){
	
		if (!checkForm()) return false;
		location.href = "STATEMENT.asp?START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value
	
	
	}

</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = RSLFORM method=post> 
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR> 
	  <TD ROWSPAN=2> <IMG NAME="tab_dev" ONCLICK=location.href='bankaccount.asp' style='cursor:hand' SRC="img/account-closed.gif" WIDTH=73 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_dev_team" ONCLICK=location.href='bankaccount.asp?OPENDIV=2' style='cursor:hand' SRC="img/new-closed.gif" WIDTH=60 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_amend_dev" SRC="img/statement-open.gif" WIDTH=93 HEIGHT=20 BORDER=0></TD>
	  <TD height=19 align=right nowrap>&nbsp;</td>
	</TR>
	<TR> 
	  <TD BGCOLOR=#133E71 > <IMG SRC="images/spacer.gif" WIDTH=524 HEIGHT=1></TD>
	</TR>
</TABLE>
  <!-- End ImageReady Slices -->
<DIV ID=table_div>
	<TABLE WIDTH=750 CLASS='TAB_TABLE' CELLPADDING=1 CELLSPACING=2 STYLE='behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=STEELBLUE>
		<THEAD><TR STYLE='HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71'><TD COLSPAN=5 CLASS='TABLE_HEAD'></TD></TR>
		<TR>
			<TD ALIGN=RIGHT COLSPAN=7 CLASS='TABLE_HEAD'>
			<B>From&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_FROM" maxlength=12 size=12 tabindex=3 value='<%=STARTDATE%>'>
			<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
			&nbsp;&nbsp;To&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TO" maxlength=12 size=12 tabindex=3 value='<%=ENDDATE%>'>
			<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
			&nbsp;&nbsp;Pagesize&nbsp;
			<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()"></B>&nbsp;&nbsp;
			</TD>
		</TR>			
		
		<TR> 
		<TD WIDTH=90PX CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD WIDTH=30PX CLASS='TABLE_HEAD'> <B>No</B> </TD>
		  <TD WIDTH=290PX CLASS='TABLE_HEAD'> <B>Description</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Debit</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Credit</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Balance</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=7 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
	<%=str_data%>
	</TABLE>
</DIV>
<INPUT TYPE='HIDDEN' NAME='hid_ACCOUNTID'>
<INPUT TYPE='HIDDEN' NAME='hid_ACTION'>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_statement width=600px height=400px style='display:none'></iframe>
</BODY>
</HTML>

