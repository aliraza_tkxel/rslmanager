	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="Includes/functions.asp" -->
<!--#include file="NominalLedger/Includes/functions.asp" -->
<%
	StartZ = Timer 
	Dim STATDATE, ENDDATE

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim lst_director, str_data, count, my_page_size
	Dim nopounds
	Dim searchSQL,searchName
	Dim lstAccounts, running_balance, opening_balance, brought_forward, page_balance, month_balance

	Dim PS, PE, FY
	Dim LYE_FY, LYE_STARTDATE, LYE_ENDDATE

	Call OpenDB()
	FY = Request("FY")	
	Call get_year(FY)

	If Request("START") = "" Then STARTDATE = PS Else STARTDATE = Request("START")
	If Request("END") = "" Then ENDDATE = PE Else ENDDATE = Request("END")
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") 
		
	if (Request("SHOW") = "" OR Request("SHOW") = 1) then
		SQL_FILTER = ""
		Show1 = "checked"
		Show2 = ""
		ShowValue = 1
	else
		SQL_FILTER = " AND RECCED = -1 "
		Show1 = ""
		Show2 = "checked"
		ShowValue = 2		
	end if

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	getCashBook()
	
	// retrives team details from database and builds 'paged' report
	Function getCashBook()
		
		Dim strSQL, rsSet, intRecord 
		
		str_data = ""
		set rsPager = Conn.execute("EXEC F_CASHBOOK " & CInt(intPage) & ", " & CONST_PAGESIZE & ", '" & FormatDateTime(STARTDATE,1)&  "', '" & FormatDateTime(enddate,1) & "', '" & SearchFactor & "' ")

		intPageCount = rsPager("TOTALPAGES")
		intRecordCount = rsPager("TOTALROWS")
		intPage = rsPager("CURRENTPAGE")

		set rsSet = rsPager.NextRecordset()
		Set rsPager = Nothing
				
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		if (ShowValue = 1) then
			opening_balance = get_cashbook_opening_balance(FY)
			page_balance = get_cashbook_balance(intPage, STARTDATE, ENDDATE, CONST_PAGESIZE)
			If CDATE(PS) = CDATE(STARTDATE) Then
				month_balance = 0
			Else
				month_balance = get_cashbook_balance(1000000, PS, DateAdd("d", -1, STARTDATE), CONST_PAGESIZE)
			End If
			running_balance = page_balance + month_balance + opening_balance
		else
			Opening_Balance = 0
			page_balance = 0
			running_balance = 0
		end if
	
		count = 0	
		str_data = str_data & "<TBODY CLASS='CAPS'>"
		str_data = str_data & "<TR>" &_
		"<TD COLSPAN=5><B>Brought Forward</B></TD>" &_
		"<TD ALIGN=RIGHT><B>"&FormatNumber(running_balance,2)&"</B></TD><TD CLASS='TABLE_HEAD'>&nbsp;</TD></TR>" 
		
		while NOT rsSet.EOF
		' ENTER OPENING BALANCE
			str_data = str_data & "<TR ID=""TR"&count&""" STYLE='CURSOR:HAND;BACKGROUND-COLOR:WHITE;COLOR:BLACK' ONCLICK=""toggle_my_colour('TR"&count&"')"">" &_
				"<TD NOWRAP>&nbsp;" & rsSet("BOOKDATE") & "</TD>" &_
				"<TD NOWRAP>" & rsSet("ITEMCOUNT") & "</TD>" &_
				"<TD NOWRAP>" & rsSet("DESCRIPTION") & "</TD>" 
				' SHOW DEBIT
				If rsSet("DEBIT") = 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					running_balance = running_balance + cdbl(rsSet("DEBIT"))
					str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:BLACK'><B>"&FormatNumber(rsSet("DEBIT"),2)&"</b></TD>" 
				End If
				' SHOW CREDIT
				If rsSet("CREDIT") = 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					running_balance = running_balance - cdbl(rsSet("CREDIT"))
					str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:blue'><B>"&FormatNumber(rsSet("CREDIT"),2)&"</b></TD>" 
				End If
				' SHOW BALANCE
				If count mod 1 <> 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					str_data = str_data & "<TD NOWRAP ALIGN=RIGHT>"&FormatNumber(round(running_balance,2),2)&"</TD>" 
				End If
				If rsSet("RECCED") = -1 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					str_data = str_data & "<TD NOWRAP ALIGN=CENTER><img src=""Images/REC.gif""></TD>" 
				End If
			count = count + 1
			rsSet.movenext()
		Wend
		'BALANCE AT BOTTOM PAGE
		str_data = str_data & "<TR>" &_
		"<TD COLSPAN=5><B></B></TD>" &_
		"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(running_balance,2),2)&"</B></TD><TD>&nbsp;</TD></TR>" 
		str_data = str_data & "</TBODY>"

		' if no items exist inform the user
		If count = 0 Then 
			str_data = "<TABLE><TR><TD COLSPAN=6 ALIGN=CENTER><B>No transactions exist for the selected criteria !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
				
		'ensure table height is consistent with any amount of records
		fill_gaps()
		
		str_data = str_data &_
		"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
		"<A HREF = 'CASHBOOK.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=1&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "&SHOW=" & ShowValue & "'><b><font color=BLUE>First</font></b></a>&nbsp;" &_
		"<A HREF = 'CASHBOOK.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "&SHOW=" & ShowValue & "'><b><font color=BLUE>Prev</font></b></a>" &_
		" Page <b><FONT COLOR=RED>" & intpage & "</FONT></b> of " & intPageCount & ". Records: " & CONST_PAGESIZE * (intpage - 1) + 1 & "  to " & (intPage-1)*CONST_PAGESIZE+(count) &	" of " & intRecordCount &_
		"&nbsp;<A HREF='CASHBOOK.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "&SHOW=" & ShowValue & "'><b><font color=BLUE>Next</font></b></a>&nbsp;" &_ 
		"<A HREF='CASHBOOK.ASP?SEARCHSTRING="&Request("SEARCHSTRING")&"&page=" & intPageCount & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"&FY=" & FY & "&SHOW=" & ShowValue & "'><b><font color=BLUE>Last</font></b></a>" &_			
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;<input type='button' name=btnGo value='GO' class='rslbutton' onclick='JumpPage()'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			
		rsSet.close()
		Set rsSet = Nothing
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = CONST_PAGESIZE - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function

	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Portfolio --&gt; Cashbook</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"

	function click_go(){
		if (!checkForm()) return false;
		if (document.getElementsByName("txt_SHOW")[0].checked)
			ToShow = 1
		else
			ToShow = 2
		location.href = "cashbook.asp?FY=<%=FY%>&START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value+"&SHOW="+ToShow
	}

	function JumpPage(){
		if (document.getElementsByName("txt_SHOW")[0].checked)
			ToShow = 1
		else
			ToShow = 2
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "CASHBOOK.ASP?FY=<%=FY%>&page="+iPage+"&START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value+"&SHOW="+ToShow
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function toggle_my_colour(id){
	
		if (document.getElementById(id).style.backgroundColor == "white")
			document.getElementById(id).style.backgroundColor = "beige"
		else
			document.getElementById(id).style.backgroundColor = "white"
	
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = RSLFORM method=post> 
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR> 
	  <TD ROWSPAN=2> <IMG NAME="tab_dev" ONCLICK=location.href='bankaccount.asp' style='cursor:hand' SRC="img/account-closed.gif" WIDTH=73 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_dev_team" ONCLICK=location.href='bankaccount.asp?OPENDIV=2' style='cursor:hand' SRC="img/new-closed.gif" WIDTH=60 HEIGHT=20 BORDER=0></TD>
	  <TD ROWSPAN=2> <IMG NAME="tab_amend_dev" SRC="img/cashbook-open.gif" WIDTH=93 HEIGHT=20 BORDER=0></TD>
	  <TD height=19 align=right nowrap><IFRAME name="BalanceServer" height=22 width=500 scroll=no SRC="ServerSide/CashBookBalance.asp?Startdate=<%=STARTDATE%>&EndDate=<%=ENDDATE%>" frameborder=0></IFRAME></td>
	</TR>
	<TR> 
	  <TD BGCOLOR=#133E71 > <IMG SRC="images/spacer.gif" WIDTH=524 HEIGHT=1></TD>
	</TR>
</TABLE>
  <!-- End ImageReady Slices -->
<DIV ID=table_div>
	<TABLE WIDTH=750 CLASS='TAB_TABLE' CELLPADDING=1 CELLSPACING=2 STYLE='behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=THISTLE>
		<THEAD><TR STYLE='HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71'><TD COLSPAN=7 CLASS='TABLE_HEAD'></TD></TR>
		<TR>
			<TD ALIGN=RIGHT COLSPAN=7 CLASS='TABLE_HEAD'>
			Show: 
				All <input type=radio name="txt_SHOW" value=1 <%=Show1%>>
				Unreconciled <input type=radio name="txt_SHOW" value=2 <%=Show2%>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<B>From&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_FROM" maxlength=12 size=12 tabindex=3 value='<%=STARTDATE%>'>
			<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
			&nbsp;&nbsp;To&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TO" maxlength=12 size=12 tabindex=3 value='<%=ENDDATE%>'>
			<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
			&nbsp;&nbsp;Pagesize&nbsp;
			<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()"></B>&nbsp;&nbsp;
			</TD>
		</TR>			
		
		<TR> 
		<TD WIDTH=90PX CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD WIDTH=30PX CLASS='TABLE_HEAD'> <B>Count</B> </TD>
		  <TD WIDTH=290PX CLASS='TABLE_HEAD'> <B>Description</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Paid In</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Withdrawn</B> </TD>
		  <TD WIDTH=90PX CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Balance</B> </TD>
		  <TD CLASS='TABLE_HEAD'>&nbsp;</TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=7 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
	<%=str_data%>
	</TABLE>
</DIV>
<INPUT TYPE='HIDDEN' NAME='hid_ACCOUNTID'>
<INPUT TYPE='HIDDEN' NAME='hid_ACTION'>
</form>
<div style='position:absolute;top:80;left:430;z-Index:100'>
<%
Response.Write "Report Generated in : " & FormatNumber(Timer - StartZ,3) & " seconds"
%>
</div>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_statement width=600px height=400px style='display:none'></iframe>
</BODY>
</HTML>

