<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 17
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (2)	 'USED BY CODE
	Dim DatabaseFields (2)	 'USED BY CODE
	Dim ColumnWidths   (2)	 'USED BY CODE
	Dim TDSTUFF        (2)	 'USED BY CODE
	Dim TDPrepared	   (2)	 'USED BY CODE
	Dim ColData        (2)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (2)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (2)	 'All Array sizes must match	
	Dim TDFunc		   (2)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	'ColData(0)  = "DEVELOPMENT NAME|DEVELOPMENTNAME|90"
	'SortASC(0) 	= "DEVELOPMENTNAME ASC"
	'SortDESC(0) = "DEVELOPMENTNAME DESC"
	'TDSTUFF(0)  = ""
	'TDFunc(0) = ""

	ColData(0)  = "Form|NAME|300"
	SortASC(0) 	= "NAME ASC"
	SortDESC(0) = "NAME DESC"	
	TDSTUFF(0)  = ""
	TDFunc(0) = ""		

	ColData(1)  = "DATE|DATE|90"
	SortASC(1) 	= "DATE ASC"
	SortDESC(1) = "DATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = "formatdatetime(|)"
			
	ColData(2)  = "amount|amount|90"
	SortASC(2) 	= "amount ASC"
	SortDESC(2) = "amount DESC"	
	TDSTUFF(2)  = " "" ALIGN=RIGHT"" "
	TDFunc(2) = "FormatCurrency(|)"
	
			
	
	
	PageName = "GrantsLst.asp"
	EmptyText = "No Relevant Grants found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	
	'RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""ACCOUNTID"") & "","" & rsSet(""DEVCENTREID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	dim iSQlD,iSQlS
	dim rsSetD,rsSetS, DevcName, Total
	
	SQLCODE="select pd.DEVELOPMENTNAME as DEVELOPMENTNAME,a.NAME as Name, pdg.DATE Date,pdg.amount as amount, " &_
			"pdg.ACCOUNTID as ACCOUNTID,pdg.DEVCENTREID as DEVCENTREID " &_
			" from P_DEVLOPMENTGRANT pdg " &_
			" inner join P_DEVELOPMENT pd on pd.DEVELOPMENTID=pdg.DEVCENTREID " &_
			" inner join nl_account a on a.ACCOUNTID=pdg.ACCOUNTID " &_
			" where pdg.DEVCENTREID= " & request("DEVELOPMENTID") &_
			" ORDER BY " & orderBy
	
	iSQlD="select DEVELOPMENTNAME from P_DEVELOPMENT  where DEVELOPMENTID=" & request("DEVELOPMENTID") 
		
		 
	iSQlS="select isNull(sum(amount),0) as amount from P_DEVLOPMENTGRANT pdg " &_
		 " inner join P_DEVELOPMENT pd on pd.DEVELOPMENTID=pdg.DEVCENTREID " &_
         " where pdg.DEVCENTREID=" & request("DEVELOPMENTID") 
		
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
	
	Total=0
	OpenDB()
	Call OpenRS(rsSetD, iSQlD)
	DevcName= rsSetD("DEVELOPMENTNAME")

	closeRS(rsSetD)
	
	Call OpenRS(rsSetS, iSQlS)
	if not rsSetS.eof then
		Total=rsSetS("amount")
	end if
	closeRS(rsSets)
	'Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "NL_ACCOUNT", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Grant List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()




function Sumbitpage()
	{
		FormFields[0] = "sel_FROM|From|SELECT|Y"
		FormFields[1] = "txt_Date|Date|DATE|Y"
		FormFields[2] = "txt_amt|amt|CURRENCY|Y"
		FormFields[3] = "txt_Desc|Desc|TEXT|Y"
		if (!checkForm()) return false

	
	}



// -->
</SCRIPT>
<%
	opendb()
	Call BuildSelect(lstGRANTS, "sel_FROM", "nl_account where PARENTREFlistid=(select DEFAULTVALUE from rsl_defaults where DEFAULTNAME like 'GRANTID')", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", NULL)	
	closedb()
%>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="frm" method="post" action="ServerSide/Grand_srv.asp">
  <TABLE id="md" BORDER=0 width=83% CELLPADDING=2 CELLSPACING=2 HEIGHT=40PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
    <tr> 
      <td width=25%><B>From </b></td>
      <td></td>
      <td width="19%"><b>Date </b> </td>
      <td></td>
      <td width="17%"><b>Amount </b> </td>
      <td></td>
      <td width="39%"><b>Description </b> </td>
      <td></td>
    </tr>
    <tr> 
      <td width="25%"><%=lstGRANTS%> </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_FROM"></td>
      <td width="19%"> 
        <input type="text" name="txt_Date" value="<%=formatDateTime(Date,2)%>" class="textbox200" maxlength="12" style="width:75">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_Date"></td>
      <td width="17%"> 
        <input type="text" name="txt_amt"  class="textbox200" maxlength="12" style="width:75">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_amt"></td>
      <td width="39%"> 
        <input type="text" name="txt_Desc"  class="textbox200" maxlength="100" style="width:300">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_Desc"></td>
    <tr> 
      <td align ="right" colspan="8"> 
        <input type=reset name=reset class="RSLButton" value=Reset>
        &nbsp;
        <input type=submit name=sButton class="RSLButton" value=Submit onClick="return Sumbitpage()">
        <input type="hidden" name="devcentre" value =" <%=request("DEVELOPMENTID")%>" style="width:20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      </td>
    </tr>
  </table>
  <br>
  &nbsp; <b>Total grants for the development &nbsp;<i> "<%=DevcName%> 
  " </i> &nbsp;is : <%=FormatCurrency(Total)%> </b> 
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD>&nbsp;</TD>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71><IMG SRC="../Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <%=TheFullTable%> 
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
