<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->

<HTML>
<HEAD>
<TITLE>Messages</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
function DoFunction(){
	window.focus();
	<% if (Request("RELOAD") = "1") then %>
	try {
		openerfile = opener.location.href.toLowerCase()
		if (openerfile.indexOf("mywhiteboard.asp") != -1)
			opener.location.href="/MyWeb/MyWhiteboard.asp"
		if (openerfile.indexOf("contractorwhiteboard.asp") != -1)
			opener.location.href="/MyWeb/ContractorWhiteboard.asp"
		if (openerfile.indexOf("messages.asp") != -1)
			opener.location.href="/MyTasks/Messages.asp"
		}
	catch (e){
		ignoreError = true
		}
	<% end if %>
	}
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="DoFunction()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_pink_white_littl.gif" width="10" height="10" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
  </tr>
  <tr>
  	<td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71'>
	<table cellspacing=5 width=100%><tr><td>
		
		<table cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td colspan=2 align=left><img src="/myImages/My60_pink.gif" width="48" height="19">
			</tr>
			<tr bgcolor="#133e71"> 
			  <td height=19px class="RSLWhite">&nbsp;<b>Message</b></td>
			  <td class="RSLWhite" align=right>&nbsp;<%= Session("Firstname")%>&nbsp;</td>
			</tr>  
			<tr>
				<td colspan=2 valign=middle bgcolor="#f8f6f6" style='border:1px solid #133e71' align=center height=150px>
				&nbsp;
				<%=Request("Text")%>
				<% if Request("Text") = "Message saved successfully" then %>
				<br><br><a href="PopMessage.asp"><font color=blue>Add Another</font></a>
				<% end if %>
				</td>
			</tr>
		</table>
		
	</td></tr></table>
	</td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td width=67 align="right"><img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td>
  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

