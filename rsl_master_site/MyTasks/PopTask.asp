<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim rsTeams
OpenDB()
SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM WHERE TEAMID <> 1 AND ACTIVE=1 ORDER BY TEAMNAME"
Call OpenRs(rsTeams, SQL)
%>
<HTML>
<HEAD>
<TITLE>Add Task</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
window.focus();
	
var FormFields = new Array()
FormFields[0] = "txt_TITLE|Title|TEXT|Y"						
FormFields[1] = "txt_ACTION|Action|TEXT|Y"						
FormFields[2] = "txt_ACTIONDATE|Action Date|DATE|Y"
FormFields[3] = "sel_TEAMS|Team|SELECT|Y"
FormFields[4] = "sel_ASSIGNTO|Assign To|SELECT|Y"		


function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/Task_svr.asp"
	RSLFORM.submit()
	}

function GetEmployees(){
	if (RSLFORM.sel_TEAMS.value != "") {
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()
		}
	else {
		RSLFORM.sel_ASSIGNTO.outerHTML = "<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX'><option value=''>Please select a team</option></select>"
		}
	}

function GetContractorContacts() {
	if (RSLFORM.sel_ASSIGNTO.value != "ALL" && RSLFORM.sel_ASSIGNTO.value != "") {
		RSLFORM.action = "Serverside/GetContractors.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	}

function LoadPrevious() {
	if (RSLFORM.sel_ASSIGNTO.value == "PREVIOUS") {
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	}	
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6>
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_pink_white_littl.gif" width="10" height="10" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
  </tr>
  <tr>
  	<td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71'>
	<table cellspacing=5 width=100% style='border-collapse:collapse'><tr><td>
		
		<table cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td colspan=2 align=left><img src="/myImages/My28.gif" width="48" height="19">
			</tr>
			<tr bgcolor="#133e71"> 
			  <td height=19px class="RSLWhite">&nbsp;<b>Add Task</b></td>
			  <td class="RSLWhite" align=right>&nbsp;<%= Session("Firstname")%>&nbsp;</td>
			</tr>  
			<tr>
				<td colspan=2>
					<table width=100% cellspacing=2 cellpadding=0 style='border:1px solid #133e71' bgcolor="#f8f6f6">
						<tr>
							<td><b>Title</b></td>
	                        <td><input name="txt_TITLE" type="text" class="textbox200" id="MessageTitle" maxlength="24" STYLE='WIDTH:258PX'></td>
							<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td valign=top><b>Action </b></td>
                        	<td><textarea name="txt_ACTION" rows="3" wrap="on" class="textbox200" STYLE="overflow:hidden;WIDTH:258PX"
								onKeyUp="if (this.value.length>500) { alert('Please enter no more than 500 chars'); this.value=this.value.substring(0,499) }"
								></textarea></td>
							<TD><image src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0"></TD>								
						</tr>
						<tr>
							<td><b>Action Date</b></td>
	                        <td><input name="txt_ACTIONDATE" type="text" class="textbox200" id="MessageTitle" maxlength="10" STYLE='WIDTH:258PX'></td>
							<TD><image src="/js/FVS.gif" name="img_ACTIONDATE" width="15px" height="15px" border="0"></TD>							
						</tr>						
                        <tr>
							<td><b>Team</b></td>
                        	<td><select name="sel_TEAMS" class="textbox200" onchange="GetEmployees()" STYLE='WIDTH:258PX'>
								  <option value="">Please Select</option>
                                    <%
									While (NOT rsTeams.EOF)
									%>
                                    <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
                                    <%
									  rsTeams.MoveNext()
									Wend
									If (rsTeams.CursorType > 0) Then
									  rsTeams.MoveFirst
									Else
									  rsTeams.Requery
									End If
									CloseRs(rsTeams)
									CloseDB()
									%>
								  <option value='NOT'>No Team</option>
                            </select></td>
							<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td><b>Assign To</b></td>
                        	<td><select name="sel_ASSIGNTO" class="textbox200" STYLE='WIDTH:258PX'>
								  <option value="">Please select a team</option>
                            </select></td>
							<TD><image src="/js/FVS.gif" name="img_ASSIGNTO" width="15px" height="15px" border="0"></TD>							
						</tr>
						<tr><td COLSPAN=2 ALIGN=RIGHT>
							<input name="hid_TASK" type="hidden" value="1">
							<input name="hid_CREATEDBY" type="hidden" value="<%= Session("UserID") %>">
							<input name="hid_ACTION" type="hidden" value="NEW">							
							<input type="button" onClick="SaveForm()" class="RSLButton" value="Add Task" title='Add Task'>&nbsp;
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
		
	</td></tr></table>
	</td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>
  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

