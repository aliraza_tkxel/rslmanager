<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim rsTeams
OpenDB()
SQL = "SELECT TEAMID, TEAMNAME FROM E_TEAM WHERE TEAMID <> 1 AND ACTIVE=1 ORDER BY TEAMNAME"
Call OpenRs(rsTeams, SQL)
%>
<HTML>
<HEAD>
<TITLE>Add Message</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidationExtras.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
window.focus();
	
var FormFields = new Array()
FormFields[0] = "txt_TITLE|Title|TEXT|Y"						
FormFields[1] = "txt_MESSAGE|Message|TEXT|Y"						
FormFields[2] = "sel_TEAMS|Team|SELECT|Y"
FormFields[3] = "sel_MESSAGEFOR|For|SELECT|Y"		
FormFields[4] = "txt_EXPIRES|Expires|INTEGER|I_Y"		

function SaveForm(){
	if (!checkForm()) return false;
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/Message_svr.asp"
	RSLFORM.submit()
	}

function GetContractorContacts() {
	if (RSLFORM.sel_MESSAGEFOR.value != "ALLORG" && RSLFORM.sel_MESSAGEFOR.value != "") {
		RSLFORM.action = "Serverside/GetContractors.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else 
		SetExpiry()
	}

function LoadPrevious() {
	if (RSLFORM.sel_MESSAGEFOR.value == "PREVIOUS") {
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()		
		}
	else
		SetExpiry()
	}
		
function GetEmployees(){
	if (RSLFORM.sel_TEAMS.value == "ALL") {
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value='ALL'>All Employees</option></select>"
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('4')
		}
	else if (RSLFORM.sel_TEAMS.value != "") {
		DisableItems('4')				
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.action = "Serverside/GetEmployees.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()
		}
	else {
		DisableItems('4')								
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		RSLFORM.sel_MESSAGEFOR.outerHTML = "<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX'><option value=''>Please select a team</option></select>"
		}
	}

function SetExpiry(){
	if (RSLFORM.sel_MESSAGEFOR.value == "ALL" || RSLFORM.sel_MESSAGEFOR.value == "ALLORG") {
		RSLFORM.txt_EXPIRES.value = "";		
		EnableItems('4')
		}
	else {
		DisableItems('4')
		RSLFORM.txt_EXPIRES.value = "Not Applicable";		
		}
	}	
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6>
<table width=379 border="0" cellspacing="0" cellpadding="0">
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_pink_white_littl.gif" width="10" height="10" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=1></td>
  </tr>
  <tr>
  	<td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71'>
	<table cellspacing=5 width=100% border=0><tr><td>
		
		<table cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td colspan=2 align=left><img src="/myImages/My60_pink.gif" width="48" height="19">
			</tr>
			<tr bgcolor="#133e71"> 
			  <td height=19px class="RSLWhite">&nbsp;<b>Add Message</b></td>
			  <td class="RSLWhite" align=right>&nbsp;<%= Session("Firstname")%>&nbsp;</td>
			</tr>  
			<tr>
				<td colspan=2>
					<table width=100% cellspacing=2 cellpadding=0 style='border:1px solid #133e71' bgcolor="#f8f6f6">
						<tr>
							<td><b>Title</b></td>
	                        <td><input name="txt_TITLE" type="text" class="textbox200" id="MessageTitle" maxlength="24" STYLE='WIDTH:285PX'></td>
							<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td valign=top><b>Message </b></td>
                        	<td><textarea name="txt_MESSAGE" rows="3" wrap="on" class="textbox200" STYLE="overflow:hidden;WIDTH:285PX"
								onKeyUp="if (this.value.length>500) { alert('Please enter no more than 500 chars'); this.value=this.value.substring(0,499) }"
								></textarea></td>
							<TD><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"></TD>								
						</tr>
                        <tr>
							<td><b>Team</b></td>
                        	<td><select name="sel_TEAMS" class="textbox200" onchange="GetEmployees()" STYLE='WIDTH:285PX'>
								  <option value="">Please Select</option>
								  <option value="ALL">All Teams</option>								  
                                    <%
									While (NOT rsTeams.EOF)
									%>
                                    <option value="<%=(rsTeams.Fields.Item("TEAMID").Value)%>"><%=(rsTeams.Fields.Item("TEAMNAME").Value)%></option>
                                    <%
									  rsTeams.MoveNext()
									Wend
									If (rsTeams.CursorType > 0) Then
									  rsTeams.MoveFirst
									Else
									  rsTeams.Requery
									End If
									CloseRs(rsTeams)
									CloseDB()
									%>
								  <option value='NOT'>No Team</option>
                            </select></td>
							<TD><image src="/js/FVS.gif" name="img_TEAMS" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td><b>For</b></td>
                        	<td><select name="sel_MESSAGEFOR" class="textbox200" STYLE='WIDTH:285PX'>
								  <option value="">Please select a team</option>
                            </select></td>
							<TD><image src="/js/FVS.gif" name="img_MESSAGEFOR" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							
                        <td><b>Expires</b></td>
                        	<td><input type="text" name="txt_EXPIRES" value="Not Applicable" class="textbox200" STYLE='WIDTH:245PX' disabled> DAYS</td>
							<TD><image src="/js/FVS.gif" name="img_EXPIRES" width="15px" height="15px" border="0"></TD>							
						</tr>
						<tr><td COLSPAN=2 ALIGN=RIGHT>
							<input name="hid_CREATEDBY" type="hidden" value="<%= Session("UserID") %>">
							<input name="hid_ACTION" type="hidden" value="NEW">							
							<input name="hid_ORGID" type="hidden" value="">														
							<input type="button" onClick="SaveForm()" class="RSLButton" value="Add Message" title='Add Message'>&nbsp;
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
		
	</td></tr></table>
	</td>
  </tr>
  <tr> 
	  <td width=314 colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>
  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

