<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim rsTeams
MessageID = Request("MessageID")
if (MessageID = "") then
	MessageID = -1
end if

	OpenDB()
	SQL = "SELECT MESSAGEID, MESSAGE, M.CREATEDBY, M.TITLE, MESSAGEFOR, O.NAME AS ORGNAME, EXPIRES = CONVERT(VARCHAR, M.EXPIRES, 103), DATECREATED = CONVERT(VARCHAR, M.DATECREATED, 103), FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM G_MESSAGES M " &_
			"LEFT JOIN E__EMPLOYEE E ON M.CREATEDBY = E.EMPLOYEEID " &_
			"LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID " &_			
			"WHERE MESSAGEID = " & MessageID
	Call OpenRs(rsMessage, SQL)
	if (NOT rsMessage.EOF) then
		title = rsMessage("TITLE")
		message = rsMessage("MESSAGE")
		DateCreated = rsMessage("DATECREATED")
		SentBy = rsMessage("FULLNAME")
		MessageID = rsMessage("MESSAGEID")
		MessageFor = rsMessage("MESSAGEFOR")
		Organisation = "" & rsMessage("ORGNAME")
		ExpiryDate = rsMessage("EXPIRES")
		CreatedBy = rsMessage("CreatedBy")
		if (Organisation <> "") then
			SentBy = SentBy & ", <font color=blue>" & Organisation & "</font>"
		end if
		CloseRs(rsMessage)
		CloseDB()		
	else
		title = "No Message Found"
		message = "N/A"
		DateCreated = "N/A"
		SentBy = "N/A"
		MessageFor = "NA"
		ExpiryDate = "N/A"
		CreatedBy = -1
		MessageID = -1
	end if

%>
<HTML>
<HEAD>
<TITLE>Display Message</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<SCRIPT LANGUAGE="JAVASCRIPT" src="/js/FormValidation.js"></script>
<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
window.focus();

function DelMessage(){
	RSLFORM.target = "";
	RSLFORM.action = "ServerSide/Message_svr.asp"
	RSLFORM.submit()
	}	
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6>
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_pink_white_littl.gif" width="10" height="10" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
  </tr>
  <tr>
  	<td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71'>
	<table cellspacing=5 width=100%><tr><td>
		
		<table cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td colspan=2 align=left><img src="/myImages/My60_pink.gif" width="48" height="19">
			</tr>
			<tr bgcolor="#133e71"> 
			  <td height=19px class="RSLWhite">&nbsp;<b>Message</b></td>
			  <td class="RSLWhite" align=right>&nbsp;<%= Session("Firstname")%>&nbsp;</td>
			</tr>  
			<tr>
				<td colspan=2>
					<table width=100% cellspacing=2 cellpadding=0 style='border:1px solid #133e71' bgcolor="#f8f6f6">
						<tr>
							<td width=70px><b>Title</b></td>
	                        <td><%=title%></td>
							<TD><image src="/js/FVS.gif" name="img_TITLE" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td valign=top><b>Message </b></td>
                        	<td valign=top height=77px><%=message%></td>
							<TD><image src="/js/FVS.gif" name="img_MESSAGE" width="15px" height="15px" border="0"></TD>								
						</tr>
                        <tr>
							<td><b>Date Sent</b></td>
                        	<td><%=DateCreated%></td>
							<TD><image src="/js/FVS.gif" name="img_MESSAGEFOR" width="15px" height="15px" border="0"></TD>							
						</tr>
                        <tr>
							<td><b>Sent by</b></td>
                        	<td><%=sentby%></td>
							<TD><image src="/js/FVS.gif" name="img_MESSAGEFOR" width="15px" height="15px" border="0"></TD>							
						</tr>
						<% if (NOT isNumeric(MessageFor) AND CreatedBy = Session("USERID")) then %>
                        <tr>
							<td><b>Expires</b></td>
                        	<td><%=ExpiryDate%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="hid_MessageID" type="hidden" value="<%= MessageID %>">
							<input name="hid_ACTION" type="hidden" value="DELETE">							
							<input type="button" onClick="DelMessage()" class="RSLButton" value="Expire Message" title='Expire Message'>&nbsp;
							</td>
						</tr>
						<% elseif (isNumeric(MessageFor) AND CStr(MessageFor) = CStr(SESSION("USERID"))) then %>
						<tr><td COLSPAN=2 ALIGN=RIGHT>
							<input name="hid_MessageID" type="hidden" value="<%= MessageID %>">
							<input name="hid_ACTION" type="hidden" value="DELETE">							
							<input type="button" onClick="DelMessage()" class="RSLButton" value="Delete Message" title='Delete Message'>&nbsp;
						</td></tr>
						<% else %>
                        <tr>
							<td><b>Expires</b></td>
                        	<td><%=ExpiryDate%></td>
						</tr>
						<% end if %>
					</table>
				</td>
			</tr>
		</table>
		
	</td></tr></table>
	</td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" style='border-bottom:1px solid #133e71;border-left:1px solid #133e71' width=100%>
	  <!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
	  <a href="javascript:window.close()" class="RSLBlack">Close Window</a></td>
	  <td colspan=2 width=68 align="right">
	  	<table cellspacing=0 cellpadding=0 border=0>
			<tr><td width=1 style='border-bottom:1px solid #133E71'>
				<img src="/myImages/spacer.gif" width="1" height="68" /></td>
			<td>
				<img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td></tr></table></td>

  </tr>
</FORM>
</table>
<iframe  src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

