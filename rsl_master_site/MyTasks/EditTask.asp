<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include virtual="autologout.asp" -->
<!--#include file="../Connections/ContactManager.asp" -->

<%
	
	// converts date string to long format to ensure correct entry into database
	Function convDate(datum)
	
		Dim splitDate
		Dim ArrMonths 
		Dim longerDate
		ArrMonths = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
		splitDate = Split(datum, "/")
		convDate = splitDate(0) & " " & ArrMonths((splitDate(1)-1)) & " " & splitDate(2)
			
	End Function
	
	Dim DisplayLeave__UserID, taskID, taskTitle,taskAction, taskDate
			
	// declare variables
	taskID = Request.Querystring("TaskID")
	taskTitle = Request.Form("txtTitle")
	taskAction = Request.Form("txtAction")
	taskDate = Request.Form("txtDate")

	// query and receive current task details based on value query string
	set DisplayTask = Server.CreateObject("ADODB.Command")
	DisplayTask.ActiveConnection = MM_ContactManager_STRING
	DisplayTask.CommandText = "dbo.sprocDisplayTask"
	DisplayTask.Parameters.Append DisplayTask.CreateParameter("RETURN_VALUE", 3, 4)
	DisplayTask.Parameters.Append DisplayTask.CreateParameter("@TaskID", 3, 1, 4, Request.Querystring("TaskID"))
	DisplayTask.CommandType = 4
	DisplayTask.CommandTimeout = 0
	DisplayTask.Prepared = true
	set sprocDisplayTask = DisplayTask.Execute

	// if form has been submitted update table using stored procedure sprocUpdateTask
	IF Request.Form("todo")="submit" Then
		
		set AmendTask = Server.CreateObject("ADODB.Command")
		AmendTask.ActiveConnection = MM_ContactManager_STRING
		AmendTask.CommandText = "dbo.sprocUpdateTask"
		AmendTask.Parameters.Append AmendTask.CreateParameter("RETURN_VALUE", 3, 4)
		AmendTask.Parameters.Append AmendTask.CreateParameter("@TaskID", 3, 1, 4, Request.Querystring("TaskID"))
		AmendTask.Parameters.Append AmendTask.CreateParameter("@Title", 200, 1, 250, taskTitle)
		AmendTask.Parameters.Append AmendTask.CreateParameter("@Action", 200, 1, 250 , taskAction)
		AmendTask.Parameters.Append AmendTask.CreateParameter("@ActionDate", 135, 1, 4, taskDate)
		AmendTask.CommandType = 4
		AmendTask.CommandTimeout = 0
		AmendTask.Prepared = true
		AmendTask.Execute()
		Dim strRedirect
		strRedirect = "UserTaskSummary.asp?TaskSumID=" & Request.Querystring("TaskID")
		Response.Redirect strRedirect
	
	End IF
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>iag Tasks</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<!--#include file="menu/iagTASKS.INC"-->
<script language="JavaScript">
<!--
// validates then submits form
	function submit_onclick(){
	
		if (EditTask.txtTitle.value == "")
			alert("Please enter a task title")
		else if (EditTask.txtAction.value == "")
			alert("Please enter a task action")
		else {
				EditTask.todo.value = "submit";
				EditTask.action = "EditTask.asp?TaskID=<%=TaskID%>";
				EditTask.submit()
			}
	}

function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;|\(|\)|\&|\+|\-/g,""); 
	event.srcElement.value = strTemp;
} 	
//-->
</script>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<!--#include file="menu/BodyTop.html" -->
<table width="100%" border="0" cellpadding="0" cellspacing="5" bordercolor="#FFFFFF">
                    <tr> 
                      <td width="871" colspan="2" valign="top" class="iagManagerSmallBlk"> 
                        <table width=100% border="0" cellspacing="0" cellpadding="0">
							<tr> 
							  
                <td bgcolor="#F4F3F7" class="iagManagerbigblue"><strong>Amend 
                  selected task</strong></td>
							</tr>
                          <tr class="iagManagerSmallBlk"> 
                            <td valign="top"><table width="63%" border="0" cellspacing="1" cellpadding="1">
                                <form name="EditTask" method="post">
                                  <tr><td colspan=3 class="iagManagerSmallBlk"><br>Edit the details for the selected task and then click on 'Amend'.<br></td></tr>
								  <tr> 
                                    
                        <td width="41%" align="left" valign="top" class="iagManagerSmallBlk"><strong>Task 
                          Title </strong></td>
                                    <td width="4%"></td>
                                    <td width="55%" align="left" class="iagManagerSmallBlk"> 
                                      <a href="javascript:;" onClick="YY_Calendar('txtDate',390,120,'de','#FFFFFF','#7F819A','YY_calendar1')"> 
                                      <span class="iagManagerSmallBlk"><strong><font color=blue>Action 
                                      By Date</font></strong></span></a></td>
                                  </tr>
                                  <tr> 
                                    <td width="41%" align="left" valign="top" class="iagManagerSmallBlkBig"> 
                                      <input name="txtTitle" type="text" class="iagManagerSmallBlk" id="txtTitle2" value="<%= sprocDisplayTask.Fields.Item("TaskSum").Value %>" maxlength="24" size=30 onblur="RemoveBad()"> 
                                    </td>
                                    <td>&nbsp;</td>
                                    <td align="left" valign="top"> <input name="txtDate" type="text" class="iagManagerSmallBlk" id="txtDate3" value="<%= sprocDisplayTask.Fields.Item("ActionByDate").Value %>" readonly size=30></td>
                                  </tr>
                                  <tr> 
                                    <td colspan="3" align="left" valign="top" class="iagManagerSmallBlk"><span class="iagManagerSmallBlk"><strong>Action</strong> 
                                      </span> </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="3" align="left" valign="top"> 
                                      <textarea name="txtAction" cols="69" rows="4" class="iagManagerSmallBlk" STYLE="overflow:hidden" onKeyUp="if (this.value.length>200) { alert('Please enter no more than 200 chars'); this.value=this.value.substring(0,200) }"  onblur="RemoveBad()"><%= sprocDisplayTask.Fields.Item("Action").Value %></textarea></td></tr>
                                      <tr><td colspan=3 align=right><input name="Submit" type='button' class="iagButton" value="Amend" onclick="submit_onclick()">&nbsp;&nbsp;&nbsp;&nbsp;
                                      <input name="todo" type="hidden"> </td>
                                  </tr>
                                  <tr> 
                                    <td width="41%" align="left" valign="top" class="iagManagerSmallBlkBig">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="left">&nbsp; </td>
                                  </tr>
                                </form>
                              </table> </td>
                          </tr>
                        </table></td>
                                  <td width="227" align="right" valign="top"> 
                                    <!--#include virtual="/IagProfile/myTasksControlVer.asp" -->
                                  </td>						
                    </tr>
                  </table>
                  
<!--#include file="../include/BodyBottom.html" -->
</BODY>
</HTML>