<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID, AppendText
Dim DataFields   (4)
Dim DataTypes    (4)
Dim ElementTypes (4)
Dim FormValues   (4)
Dim FormFields   (4)
UpdateID = "hid_TASKID"
FormFields(0) = "txt_TITLE|TEXT"						
FormFields(1) = "txt_ACTION|TEXT"
FormFields(2) = "sel_ASSIGNTO|NUMBER"
FormFields(3) = "hid_CREATEDBY|NUMBER"
FormFields(4) = "txt_ACTIONDATE|DATE"

Function NewRecord ()
	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO G_TASKS " & strSQL & ""
	Response.Write SQL
	Conn.Execute SQL, recaffected
	if (CStr(Request("sel_ASSIGNTO")) = CStr(Session("UserID")))	 then
		AppendText = "&RELOAD=1"
	end if
	GO("Task saved successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "../TaskAction.asp?Text=" & Text & AppendText
End Function

Function CompleteRecord()
	ID = Request.Form("hid_TASKID")
	SQL = "Update G_TASKS Set CompletionNotes = '" & Replace(Request.Form("CompletionNotes"),"'", "''") & "', COMPLETED = 1 WHERE TASKID = " & ID
	Conn.Execute SQL, recaffected
	CloseDB()
	Response.Redirect "../UserTaskSummary.asp?TaskSumID=" & ID
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "COMPLETE"	CompleteRecord()	
End Select
CloseDB()
%>