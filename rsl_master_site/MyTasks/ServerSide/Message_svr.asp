<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID, AppendText
Dim DataFields   (5)
Dim DataTypes    (5)
Dim ElementTypes (5)
Dim FormValues   (5)
Dim FormFields   (5)
UpdateID = "hid_MessageID"
FormFields(0) = "txt_TITLE|TEXT"						
FormFields(1) = "txt_MESSAGE|TEXT"
theTeam = Request.Form("sel_TEAMS")
theEmployee = Request.Form("sel_MESSAGEFOR")
TheRequestedOrg = Request.Form("hid_ORGID")

if (theTeam = "ALL") then
	MessageForDatabase = "ALL"
elseif (theTeam <> "" AND theEmployee = "ALL") then
	MessageForDatabase = "TE" & theTeam
else
	MessageForDatabase = theEmployee
end if

FormFields(2) = "cde_MESSAGEFOR|'" & MessageForDatabase & "'"

FormFields(3) = "hid_CREATEDBY|NUMBER"
expirydays = Request.Form("txt_EXPIRES")
if (expirydays = "Not Applicable" OR expirydays = "" OR isNull(expirydays) OR NOT isNumeric(expirydays)) then
	ExpiryForDatabase = "NULL"
else
	ExpiryForDatabase = "'" & FormatDateTime(DATEADD("d",DATE,expirydays),1) & "'"
end if

FormFields(4) = "cde_EXPIRES|" & ExpiryForDatabase & ""
FormFields(5) = "hid_ORGID|NUMBER"

Function NewRecord ()
	OpenDB()
	SQL = "SELECT TEAM FROM E_JOBDETAILS WHERE EMPLOYEEID = " & Session("USERID")
	Call OpenRs(rsMyTeamID,SQL)
	if (NOT rsMyTeamID.EOF) then
		TheTeamID = rsMyTeamID("TEAM")
	end if
	CloseRs(rsMyTeamID)

	Call MakeInsert(strSQL)	
	SQL = "INSERT INTO G_MESSAGES " & strSQL & ""
	Response.Write MessageForDatabase
	Conn.Execute SQL, recaffected
	TheOrg = SESSION("ORGID")
	if (isNull(TheOrg) OR TheOrg = "") then
		TheOrg = ""
	else
		TheOrg = CStr(TheOrg)
	end if

'		( CStr(MessageForDatabase) = CStr(Session("UserID")) )
'		OR ( MessageForDatabase = "ALL" AND TheOrg = "" AND TheRequestedOrg = "" )
'		OR ( MessageForDatabase = CStr("TE" & TheTeamID) AND TheOrg = "" )
'		OR ( MessageForDatabase = "ALLORG" AND TheRequestedOrg = "" AND TheOrg <> ""  )
'		OR ( MessageForDatabase = "TE1" AND TheOrg = CStr(TheRequestedOrg) )

	if (( CStr(MessageForDatabase) = CStr(Session("UserID")) ) OR ( MessageForDatabase = "ALL" AND TheOrg = "" AND TheRequestedOrg = "" ) OR ( MessageForDatabase = CStr("TE" & TheTeamID) AND TheOrg = "" ) OR ( MessageForDatabase = "ALLORG" AND TheRequestedOrg = "" AND TheOrg <> ""  ) OR ( MessageForDatabase = "TE1" AND TheOrg = CStr(TheRequestedOrg) ) ) then
		AppendText = "&RELOAD=1"
	end if
	GO("Message saved successfully")
End Function

Function DeleteRecord()
	ID = Request.Form(UpdateID)	
	
	SQL = "UPDATE G_MESSAGES SET ACTIVE = 0 WHERE MESSAGEID = " & ID
	Conn.Execute SQL, recaffected
	AppendText = "&RELOAD=1"	
	GO("Message Deleted Successfully")
End Function

Function GO(Text)
	CloseDB()
	Response.Redirect "../MessageAction.asp?Text=" & Text & AppendText
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>