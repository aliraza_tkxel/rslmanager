<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
if (Request("hid_TASK") = "1") then
	OrgID = Request.Form("sel_ASSIGNTO")
else
	OrgID = Request.Form("sel_MESSAGEFOR")
end if

if (OrgID = "") then
	OrgID = -1
end if

SQL = "SELECT E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME, O.NAME AS ORGNAME FROM E__EMPLOYEE E INNER JOIN AC_LOGINS L ON E.EMPLOYEEID = L.EMPLOYEEID, E_JOBDETAILS J, E_TEAM T, S_ORGANISATION O " &_
		" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND O.ORGID = E.ORGID AND L.ACTIVE = 1 AND J.ACTIVE = 1 AND E.ORGID = " & OrgID & " ORDER BY FIRSTNAME, LASTNAME"


Dim rsOrgs
OpenDB()
Call OpenRs(rsOrgs, SQL)
if (not rsOrgs.EOF) then
	OrgList = "<option value=''>Please Select</option>"
	OrgList = OrgList & "<option value='PREVIOUS'>-&nbsp;List Previous Suppliers</option>"	
	if (Request("hid_TASK") <> "1") then
		OrgList = OrgList & "<option value='ALL'>&nbsp;+&nbsp;ALL '" & rsOrgs("ORGNAME") & "' Contacts</option>"
	end if
	OrgList = OrgList & "<option value=''>&nbsp;&nbsp;&nbsp;-&nbsp;Select a contact from below....</option>"		
	while not rsOrgs.EOF
		OrgList = OrgList & "<option value='" & rsOrgs("EMPLOYEEID") & "'>&nbsp;&nbsp;&nbsp;-&nbsp;" & rsOrgs("FULLNAME")& "</option>"
		rsOrgs.moveNext
	wend
else
	OrgList = "<option value=''>No Supplier Contact's found...</option>"
	OrgList = OrgList & "<option value='PREVIOUS'>-&nbsp;List Previous Suppliers</option>"	
end if
CloseRs(rsOrgs)
CloseDB()
%>			
<html>
<head>
<title>Server Employee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JAVASCRIPT">
function SetEmployees(){
<% if (Request("hid_TASK") = "1") then %>
	parent.RSLFORM.sel_ASSIGNTO.outerHTML = Employees.innerHTML
<% else %>
	parent.RSLFORM.sel_MESSAGEFOR.outerHTML = Employees.innerHTML
	parent.RSLFORM.hid_ORGID.value = "<%=OrgID%>"
<% end if %>
	}
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000" onload="SetEmployees()">
<div id="Employees">
<% if (Request("hid_TASK") = "1") then %>
<select name='sel_ASSIGNTO' class='textbox200' STYLE='WIDTH:258PX' onchange="LoadPrevious();">
<% else %>
<select name='sel_MESSAGEFOR' class='textbox200' STYLE='WIDTH:285PX' onchange="LoadPrevious();">
<% end if %>
<%=OrgList%>
</select>
</div>
</body>
</html>
