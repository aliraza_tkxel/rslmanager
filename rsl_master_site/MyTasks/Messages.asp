<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()
SQL = "SELECT TEAM FROM E_JOBDETAILS WHERE EMPLOYEEID = " & Session("USERID")
Call OpenRs(rsMyTeamID,SQL)
if (NOT rsMyTeamID.EOF) then
	TheTeamID = rsMyTeamID("TEAM")
end if
CloseRs(rsMyTeamID)
ORGID = SESSION("ORGID")
IF (ORGID = "") THEN
	ORGID = ""
END IF

set rs = Server.CreateObject("ADODB.Recordset")
rs.ActiveConnection = RSL_CONNECTION_STRING
rs.Source = "SELECT MESSAGEID, MESSAGE, M.TITLE, O.NAME AS ORGNAME, DATECREATED = CONVERT(VARCHAR, M.DATECREATED, 103), FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM G_MESSAGES M " &_
			"LEFT JOIN E__EMPLOYEE E ON M.CREATEDBY = E.EMPLOYEEID " &_
			"LEFT JOIN S_ORGANISATION O ON E.ORGID = O.ORGID " &_						
			"WHERE  " &_
			"((MESSAGEFOR = '" & Session("UserID") & "') " &_ 
			"OR (MESSAGEFOR = 'TE" & TheTeamID & "' AND EXPIRES >= GETDATE() AND M.ORGID IS NULL) " &_ 
			"OR (MESSAGEFOR = 'ALL' AND EXPIRES >= GETDATE() AND '" & ORGID & "1' = '1') " &_ 
			"OR (MESSAGEFOR = 'ALLORG' AND EXPIRES >= GETDATE() AND '" & ORGID & "1' <> '1') " &_ 
			"OR (MESSAGEFOR = 'TE" & TheTeamID & "' AND EXPIRES >= GETDATE() AND M.ORGID = '" & ORGID & "')) " &_ 
			"AND ACTIVE = 1 ORDER BY M.DATECREATED DESC, M.TITLE"
rs.CursorType = 0
rs.CursorLocation = 2
rs.LockType = 3
rs.Open()
rs_numRows = 0
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager --> Messages</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table cellspacing=5 cellpadding=0 class="RSLBlack">
      <tr>
        <td><br>
          You are viewing all your messages. Hover the mouse over the message title 
          to view the message, you can also click on the title to get a popup 
          box<br><br>
          <table class="RSLBlack" cellspacing=0>
            <tr>
              <td width=300px style='border-bottom:1px solid black'><b>Message</b></td>
              <td width=150px style='border-bottom:1px solid black'><b>Date Recieved</b></td>
              <td width=260px style='border-bottom:1px solid black'><b>Sent By</b></td>
            </tr>
            <%
if (Not rs.EOF) Then
while (not rs.EOF)
	sentby = rs("fullname")
	orgname = "" & rs("ORGNAME")
	if (orgname <> "") then
		sentby = sentby & ", <font color=blue>" & orgname & "</font>"
	end if
%>
            <TR>
              <TD title='<%= rs("Message") %>' style='word-breal:break-all' valign=top>&nbsp;&nbsp;<a href="#" onClick="MM_openBrWindow('/MyTasks/PopMessageDisplay.asp?MessageID=<%= rs("MessageID") %>','YourMessage','width=400,height=300')" class="RSLBlack"><font color=blue><%= rs("Title") %></font></a></TD>
              <td><%=rs("datecreated")%></td>
              <td><%=sentby%></td>
            </TR>
            <%
rs.moveNext()
Wend
Else
%>
            <TR>
              <TD colspan=3 align=center><font color=blue>You have no messages 
                waiting for you</font></td>
            </tr>
            <%
End if
%>
            <tr>
              <td  colspan=3 style='border-top:1px solid black'>&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
rs.Close()
%>
