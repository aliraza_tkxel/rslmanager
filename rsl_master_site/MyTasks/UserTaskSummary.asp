<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TaskID = -1
if (Request("TaskSumID") <> "") then TaskID = Replace(Request("TaskSumID"), "'", "''")
%>
<%
OpenDB()
Dim rsTasks
SQL = "SELECT COMPLETED, COMPLETIONNOTES, G.TITLE, TASKID, ACTION, CREATEDBY, DATECREATED = CONVERT(VARCHAR, DATECREATED, 103), ACTIONDATE = CONVERT(VARCHAR, ACTIONDATE, 103), E.FIRSTNAME + ' ' + E.LASTNAME AS ASSIGNEDBY FROM G_TASKS G " &_
		"LEFT JOIN E__EMPLOYEE E ON G.CREATEDBY = E.EMPLOYEEID WHERE ASSIGNTO = " & Session("UserID") & " AND ACTIVE = 1 AND TASKID = " & TaskID
Call OpenRs(rsTask, SQL)
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager --> Tasks</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script language="JavaScript" defer>
<!--
function CompleteTask(){
	RSLFORM.hid_ACTION.value = "COMPLETE"
	RSLFORM.submit()
	}
	
function Update(){
	location.href =  "EditTask.asp?TaskId=<%= Request.Querystring("TaskSumID") %>";;
}

function CompleteMilestone(){
	CompleteMe.style.visibility = "visible";
	}
	
function CancelComplete(){
	CompleteMe.style.visibility = "hidden";
	}

//-->
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
      <table border="0" cellspacing="5" cellpadding="0" class="RSLBlack">
        <tr> 
          <td valign="top"> 
            <form action="ServerSide/Task_svr.asp" method="POST" name="RSLFORM">
              <table border="0" cellspacing="1" cellpadding="1" class="RSLBlack" width=520px>
                <tr> 
                  <td bgcolor="#133e71" colspan=3 class="RSLWhite" height=20px><b>&nbsp;SELECTED TASK DETAILS</b></td>
                </tr>
                <% If Not rsTask.EOF Or Not rsTask.BOF Then %>
                <tr> 
                   <td><b>&nbsp;Title</b></td><td>:&nbsp;</td>
                   <td><%= rsTask.Fields.Item("TITLE").Value %></td>
                </tr>
                <tr> 
                    <td valign=top><b>&nbsp;Action</b></td><td>:&nbsp;</td>
                    <td><%= rsTask.Fields.Item("Action").Value %></td>
                </tr>
                <tr> 
                    <td><b>&nbsp;Action Date</b></td><td>:&nbsp;</td>
                    <td><%= rsTask.Fields.Item("ACTIONDATE").Value %></td>
                 </tr>
                 <tr> 
                    <td nowrap><b>&nbsp;Date Created&nbsp;</b></td><td>:&nbsp;</td>
                    <td width=100%><%= rsTask.Fields.Item("DATECREATED").Value %></td>
                 </tr>
                 <tr> 
                    <td><b>&nbsp;Assigned By</b></td><td>:&nbsp;</td>
                    <td><%= rsTask.Fields.Item("ASSIGNEDBY").Value%></td>
                 </tr>
				 <% if (rsTask("COMPLETED") = 1) then %>
			     <tr>		 
					 <td valign=top><b>&nbsp;Completion<br>&nbsp;Notes</b> </td><td valign=top>:&nbsp;</td>
					 <td valign=top><%=rsTask("COMPLETIONNOTES")%></td>
				 </tr>	 
				 <% else %>
                 <tr> 
                    <td colspan=3 nowrap align=right><input type='button' class="RSLButton" value=" Complete " onclick="CompleteMilestone()"/>
                    <%
					CREATOR = rsTask.Fields.Item("CREATEDBY").Value
					if (NOT isNull(CREATOR)) then
						CREATOR = CStr(CREATOR)
					end if
					alterable = ""
					if (CREATOR <> CStr(Session("UserID"))) then
						alterable = " disabled style='background-color:silver'"
					end if
					%>
                    <!--<input name="btnEdit" type='button' class="RSLButton" value=" Amend " onclick="Update()" <%=alterable%>/>-->
                    </td>
                 </tr>
                 <tr>
                    <td height=3px></td>
                 </tr>
				 <tbody id="CompleteMe" style="visibility:hidden">
                 <tr> 
                    <td valign=top><b>&nbsp;Completion<br>&nbsp;Notes</b> </td><td valign=top>:&nbsp;</td>
                    <td>
                          <textarea name="CompletionNotes" class="iagManagerSmallBlk" rows=5 cols=48 style="border:1px solid silver;background-color:beige" onKeyUp="if (this.value.length>1000) { alert('Please enter no more than 1000 chars'); this.value=this.value.substring(0,1000) }"></textarea>
                    </td>
                  </tr>
				  <tr>
                      <td colspan=3 align=right> 
					      <input type="hidden" name="hid_ACTION" value="COMPLETE">
					      <input type="hidden" name="hid_TASKID" value="<%=rsTask("TASKID")%>">
                          <input name="Submit" type='button' class="RSLButton" value="Confirm Completion" onclick="CompleteTask()" />
                          <input name="Cancel" type='button' class="RSLButton" value=" Cancel " onclick="CancelComplete()" />						  
                      </td>
				  </tr>
				  </tbody>
				  <% end if %>
                <% 
				End If ' end Not rsTask.EOF Or NOT rsTask.BOF 
				CloseRs(rsTask)
				%>
              </table>
            </form>
          </td>
          <td align="right" valign="top"> 
            <!--#include virtual="MyTasks/myTasksControlVer.asp" -->
          </td>
        </tr>
      </table>
      <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
