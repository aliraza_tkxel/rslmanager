<%
'Response.Buffer = True
'Response.ContentType = "application/vnd.ms-excel"
Response.Buffer = True
Response.ContentType = "application/x-msexcel"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->

<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include 
	'		sometimes kills the xls transfer

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count,HEADING,FilterDesc
		Dim COSTCENTREFilter,HeadFilter,selectedCC,selectedHead 
	    Dim FiscalYearFilter, FiscalYearFilter_Array, FiscalYearFilter_Start, FiscalYearFilter_End,selectedFiscalYear
	    Dim Total
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    'Get Record to display on xsl
	    OpenDB()
	    
	    HEADING="Expenditure Purchase List"
	    
	    orderBy=Request("orderBy")
	    
	    if(orderBy="") then
	        orderBy=" PO.ORDERID DESC"
	    end if
	    
	    FiscalYearFilter = ""
	    if (Request("sel_FISCALYEARFILTER") <> "") then
        	FiscalYearFilter_Array = Split(Request("sel_FISCALYEARFILTER"), ";", 2,1)
        	FiscalYearFilter_Start = FiscalYearFilter_Array(0)
        	FiscalYearFilter_End = FiscalYearFilter_Array(1)
            FiscalYearFilter = " AND PIDATE >= '" & FiscalYearFilter_Start & "' AND PIDATE <= '" & FiscalYearFilter_End & "' "
			selectedFiscalYear = FiscalYearFilter_Start & " - " & FiscalYearFilter_End
			FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Fiscal Year</b></td> " &_
                                      "   <td>" & selectedFiscalYear & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	     end if
	     	           
	    COSTCENTREFilter = ""
	    if (Request("sel_COSTCENTRE") <> "") then
		    COSTCENTREFilter = " AND CC.COSTCENTREID = " & Request("sel_COSTCENTRE") & " "
		    selectedCC=Request("selectedCC") 
		    FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Cost Centre</b></td> " &_
                                      "   <td>" & selectedCC & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	    end if
    		    
	    HeadFilter = ""
	    if (Request("sel_HEAD") <> "") then
			HeadFilter = " AND H.HEADID = " & Request("sel_HEAD") & " "	
		    selectedHead=Request("selectedHead") 
		    FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Head</b></td> " &_
                                      "   <td>" & selectedHead & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	    end if
	    	    
        if (Request("theTotal") <> "") then
           Total= Request("theTotal")
           FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Total</b></td> " &_
                                      "   <td>" & Total & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
        end if          
        strSQL =  " SELECT  ISNULL(EMP.FIRSTNAME + ' ' + EMP.LASTNAME,'') AS FULLNAME,  " &_
				  "        ISNULL(PO.ORDERID,'') AS ORDERID,ISNULL(PONAME,'') AS PONAME, " &_
		 		  "        ISNULL(PI.ORDERITEMID,'') AS ORDERITEMID, ISNULL(PI.ITEMNAME,'') AS ITEMNAME, ISNULL(PI.ITEMDESC,'') AS ITEMDESC, " &_
				  "	        PI.ITEMNAME  AS SHORTITEMNAME, " &_
				  "        PISTATUS = ISNULL(CASE PITYPE WHEN 5 THEN 'Paid <font color=red>Tenant Reimbursement</font>' WHEN 4 THEN 'Paid <font color=red>Staff Expense</font>'WHEN 3 THEN 'Paid <font color=red>Petty Cash</font>' " &_
				  "	                            ELSE PS.POSTATUSNAME END,''), " &_
				  "	        ISNULL(E.EXPENDITUREID,'') AS EXPENDITUREID, ISNULL(E.DESCRIPTION,'') AS EXPDESC, ISNULL(H.HEADID,'') AS HEADID,ISNULL(CC.COSTCENTREID,'') AS COSTCENTREID , " &_
				  "	        ISNULL(CONVERT(VARCHAR,PI.GROSSCOST,1),'') AS FORMATTEDCOST, " &_
				  "	        FORMATTEDPIDATE=ISNULL(CONVERT(VARCHAR,PIDATE,103),'') " &_
				  " FROM  F_PURCHASEITEM PI " &_
				  "	    LEFT JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_
				  "	    LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID  " &_
				  "	    LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  " &_
				  "	    INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = PI.EXPENDITUREID " &_
				  "	    INNER JOIN F_HEAD H ON H.HEADID = E.HEADID " &_
				  "	    INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID " &_
				  "	    LEFT JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = PO.USERID  " &_
				  " WHERE PI.PITYPE <> 6 AND PO.POSTATUS <> 0 AND PI.ACTIVE = 1 " & COSTCENTREFilter & HeadFilter & FiscalYearFilter &_
				  " Order By " + Replace(orderBy, "'", "''") + ""
	
						
        Server.ScriptTimeout=800
		Call OpenRs(rsSet, strSQL)
		
		while NOT rsSet.EOF 
			
	   		
		        str_data  =   str_data & "<tr>" &_
	                                     "  <td valign=""top"">" & rsSet("ORDERID") & "</td>" &_
				    		             "  <td>" & rsSet("FORMATTEDPIDATE") & "</td>" &_
			    	    	             "  <td align=""center"">" & rsSet("EXPDESC") & "</td>" &_
						                 "  <td>" & rsSet("SHORTITEMNAME") & "</td>" &_
						                 "  <td>" & rsSet("FORMATTEDCOST") & "</td>" &_
						                 "  <td>" & rsSet("PISTATUS") & "</td>" &_
						                 "</tr>" 

            
		        rsSet.moveNext()
			Response.Flush()
		wend
		
		CloseRs(rsSet)
		SET rsSet = Nothing
		
		CloseDB()
		
	
	'End Get Record to display
%>


<HTML>
<HEAD>
<TITLE>RSL Manager Finanace --> Expenditure Purchase List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<BODY bgcolor="#FFFFFF" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 border="1"  >
  <TR > 
    <TD  colspan="6"> 
      <div align="left"><font color="#0066CC"><b><font size="4"><%=HEADING%></font> 
        <%dim todaysDate
				 todaysDate=now()
				 response.write  todaysDate
				%>
        </b></font></div>
      </TD>
    </TR>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <%=FilterDesc%>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr> 
      <td>&nbsp;<b>Order No</b></td>
      <td>&nbsp;<b>Ordered</b>&nbsp;</td>
      <td>&nbsp;<b>Expenditure</b>&nbsp;</td>
      <td>&nbsp;<b>Item</b>&nbsp;</td>
      <td>&nbsp;<b>Cost</b>&nbsp;</td>
      <td>&nbsp;<b>Status</b>&nbsp;</td>
    </tr>
    <%=str_data%> 
    </TABLE>
</BODY>
</HTML>