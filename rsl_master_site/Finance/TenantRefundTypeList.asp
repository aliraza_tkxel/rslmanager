<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%

	SQLCODE = "SELECT " &_
			"CASE RJ.PAYMENTTYPE " &_
			"WHEN 24 THEN 'TENANTREFUNDCHEQUE.ASP' " &_
			"WHEN 19 THEN 'TENANTREFUNDLIST.ASP' " &_
			"END AS REDIR, ISNULL(SUM(AMOUNT),0) AS PAYMENTAMOUNT, " &_
			"COUNT(TR.JOURNALID) AS TOTALPO, RJ.PAYMENTTYPE,  PT.DESCRIPTION AS PAYMENTNAME " &_
			"FROM F_TENANTREFUNDS TR " &_
			"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
			"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
			"INNER JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
			"WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.TRANSACTIONDATE <= '" & REQUEST("PODATE")& "' " &_
			"GROUP BY RJ.PAYMENTTYPE, PT.DESCRIPTION "

	OpenDB()
	
	TableString = ""

	Call OpenRs(rsList, SQLCODE)
	if (NOT rsList.EOF) then
		while NOT rsList.EOF
			TableString = TableString & "<TR STYLE='CURSOR:HAND' ONCLICK=""GO('" & rsList("REDIR") & "')""><TD>" & rsList("PAYMENTNAME") & "</TD><TD>" & rsList("TOTALPO") & "</TD><TD align=right>" & FormatCurrency(rsList("PAYMENTAMOUNT")) & "</TD></TR>"
			rsList.moveNext
		wend
	else
		TableString = "<TR><TD COLSPAN=3 ALIGN=CENTER>No tenant refunds found.</TD></TR>"
	end if
	CloseRs(rsList)
	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Tenant Refunds List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}

function GO(ThePage){
	location.href = ThePage + "?PODATE=<%=Request("PODATE")%>" 
	}

function SaveResults(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "ServerSide/TenantRefundList_CSV.asp<%="?CC_Sort=" & orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value
		}
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack"><tr>
      <td> This page shows the tenant refunds which are <font color=red><b>ready 
        to be paid</b></font> grouped by the payment type. To process any individual 
        payment type click on it. This will take you to the respective processing 
        page.<br>
        <br>
</td></tr></table>

  <TABLE CELLPADDING=3 CELLSPACING=0 border=1 width=750 STYLE='BORDER-COLLAPSE:COLLAPSE;BEHAVIOR:URL(/INCLUDES/TABLES/TABLEHL.HTC)' HLCOLOR='STEELBLUE' SLCOLOR=''>
	<THEAD>
    <TR> 
      <TD style='border-bottom:1px solid #133e71'><b>Payment Type</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>Tenant Refund Count</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>Total Value</b></TD>
    </TR>
	</THEAD>
    <%=TableString%> 
  </TABLE>
</form>
<br>&nbsp<a href='TenantREfundDisplay.asp?date=<%=REQUEST("PODATE")%>'><font color=blue><b>BACK to Tenant Refund Calendar</b></font></a>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>