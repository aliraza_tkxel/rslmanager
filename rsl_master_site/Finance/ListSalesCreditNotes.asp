<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
    // ' Taken almost unchanged from Finance/SalesInvoice/SalesList.asp ' //

	dim CONST_PAGESIZE 
	CONST_PAGESIZE = 20
    if (Request("DataSetRows") <> "") then
			CONST_PAGESIZE = Clng(Request("DataSetRows"))
	end if
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim clean_desc
	
	ColData(0)  = "SCN No.|SCNID|90"
	SortASC(0) 	= "SCN.SCNID ASC"
	SortDESC(0) = "SCN.SCNID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "CreditNumber(|)"

	ColData(1)  = "Date|FORMATTEDSCNDATE|90"
	SortASC(1) 	= "SCNDATE ASC"
	SortDESC(1) = "SCNDATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Customer|CUSTOMER|120"
	SortASC(2) 	= "CUSTOMER ASC"
	SortDESC(2) = "CUSTOMER DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Type|CUSTOMERTYPE|90"
	SortASC(3) 	= "CUSTOMERTYPE ASC"
	SortDESC(3) = "CUSTOMERTYPE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Item|SONAME|175"
	SortASC(4) 	= "SONAME ASC"
	SortDESC(4) = "SONAME DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "Cost|FORMATTEDCOST|60"
	SortASC(5) 	= "TOTALCOST ASC"
	SortDESC(5) = "TOTALCOST DESC"	
	TDSTUFF(5)  = " ""align='right'"" "
	TDFunc(5) = "FormatCurrency(|)"		

	PageName = "SalesCreditNote.asp"
	EmptyText = "No Relevant Sales Credit Notes found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	//RowClickColumn = " "" TITLE="""""" & rsSet(""SOSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""SCNID"") & "")"""" """ 
	RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""SCNID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SOFilter = ""
	if (Request("SONumber") <> "") then
		if (isNumeric(Request("SONumber"))) then
			SOFilter = " AND (SO.SALEID = '" & Clng(Request("SONumber")) & "' OR SCN.SCNID = " & Clng(Request("SONumber")) & ")"
		end if
	end if

	SQLCODE="SET CONCAT_NULL_YIELDS_NULL OFF; " &_
            "SELECT  " &_
	        "    CUSTOMERTYPE =  " &_
	        "    CASE  " &_
		    "        WHEN TENANCYID IS NOT NULL THEN 'Tenant'  " &_
		    "        WHEN SUPPLIERID IS NOT NULL THEN 'Company'  " &_
		    "        WHEN SO.EMPLOYEEID IS NOT NULL THEN 'Employee'  " &_
		    "        WHEN SO.SALESCUSTOMERID IS NOT NULL THEN 'Sales Customer'  " &_
		    "        ELSE 'Unknown'  " &_
	        "    END,  " &_
	        "    CUSTOMER = " &_ 
	        "    CASE  " &_
		    "        WHEN TENANCYID IS NOT NULL THEN TEN.LIST  + ' (' + CAST(TENANCYID AS VARCHAR) + ')'  " &_
		    "        WHEN SUPPLIERID IS NOT NULL THEN O.NAME  " &_
		    "        WHEN SO.EMPLOYEEID IS NOT NULL THEN EFOR.FIRSTNAME + ' ' + EFOR.LASTNAME  " &_
		    "        WHEN SO.SALESCUSTOMERID IS NOT NULL THEN SC.CONTACT  " &_
		    "        ELSE 'Unknown'  " &_
	        "    END, " &_
	        "    E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, SCN.SCNID, " &_ 
	        "    CONVERT(VARCHAR,SI.TOTALCOST,1) AS FORMATTEDCOST, " &_
	        "    SS.SOSTATUSNAME, SONAME, " &_
	        "    FORMATTEDSCNDATE=CONVERT(VARCHAR,SCN.SCNDATE,103) " &_
            "FROM F_SALESCREDITNOTE SCN " &_
	        "    INNER JOIN F_SALESINVOICE SO ON SCN.INVOICEID = SO.SALEID " &_
	        "    INNER JOIN (SELECT SUM(ISNULL(GROSSCOST,0)) AS TOTALCOST, SCNID FROM F_SALESCREDITNOTEITEM GROUP BY SCNID) SI ON SI.SCNID = SCN.SCNID " &_
	        "    LEFT JOIN F_SOSTATUS SS ON SO.SOSTATUS = SS.SOSTATUSID " &_
	        "    LEFT JOIN S_ORGANISATION O ON SO.SUPPLIERID = O.ORGID " &_
	        "    LEFT JOIN E__EMPLOYEE EFOR ON EFOR.EMPLOYEEID = SO.EMPLOYEEID " &_
	        "    LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW TEN ON TEN.I = SO.TENANCYID " &_
	        "    LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SO.USERID " &_
	        "    LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SO.SALESCUSTOMERID " &_
			"WHERE SO.ACTIVE=1 " & SOFilter &_			
			"ORDER BY " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(SCN_id){
	window.showModelessDialog("Popups/SalesCreditNote.asp?SCNID=" + SCN_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
	}

function RemoveBad() { 
    strTemp = event.srcElement.value;
    strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
    event.srcElement.value = strTemp;
}

function SubmitPage(){
	if (isNaN(THISFORM.SONumber.value)){
		alert("When searching for a Sales Invoice you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
	}
	location.href = "<%=PageName & "?CC_Sort=" & orderBy%>&SONumber=" + THISFORM.SONumber.value + "&DataSetRows=" + THISFORM.DataSetRows.value
}
// -->
</SCRIPT>
<!-- End Preload Script -->

<table class="RSLBlack">
    <tr>
        <td><b>&nbsp;QUICK FIND Facility</b></td>
        <td><input type=text name=SONumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("SONumber"))%>" onBlur="RemoveBad()" style='width:135px;background-image:url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
        <td><select class="RSLBlack" name="DataSetRows"><option value="20">20 rows</option><option value="100">100 rows</option><option value="999">Max rows</option></select></td>
        <td><input type=button class="RSLButton" value=" Update Search " onClick="SubmitPage()"></td>

    </tr>
</table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>

