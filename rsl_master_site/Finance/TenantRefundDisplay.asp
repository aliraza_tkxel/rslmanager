<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/Calendar.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance - BACS Display</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='TenantRefundCSV.asp?pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	FutureCast = ""
	FutureSQL = ""

	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear		
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		RequestDate = CDate(StartOfMonthDate)				
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

		RealMonth = Month(Date)
		RealYear = Year(Date)	
		CurrentDate = CDate(Day(Date) & " " & MonthName(RealMonth) & " " & RealYear)				
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)	
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		CurrentDate = CDate(Day(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear)				
	End If
	
	MONTH_START_OF_REAL_DATE = CDate("1 " & MonthName(Month(Date)) & " " & Year(Date))
	MONTH_END_OF_REAL_DATE = CDate(LastDayOfMonth(Date) & " " & MonthName(Month(Date)) & " " & Year(Date))	

	'Response.Write "CurrentDate : " & CurrentDate & "<br/>"
	'Response.Write "MONTH_END_OF_REAL_DATE : " & MONTH_END_OF_REAL_DATE & "<br/><br/>"	
	if (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) then			
	'THIS IS THE BACS TODATE ACTUAL SQL
	SQL = "SELECT COUNT(TR.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS TOTALVALUE " &_
			"FROM F_TENANTREFUNDS TR " &_
			"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
			"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
			"WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.TRANSACTIONDATE <= '" & CurrentDate & "' " 

	'THIS IS THE BACS FUTURE ACTUAL SQL FOR THE CURRENT MONTH
	SQLFUTURE = "SELECT DAY(RJ.TRANSACTIONDATE) AS THEDAY, COUNT(TR.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS TOTALVALUE " &_
			"FROM F_TENANTREFUNDS TR " &_
			"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
			"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
			"WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.TRANSACTIONDATE > '" & CurrentDate & "' AND RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
			"GROUP BY DAY(RJ.TRANSACTIONDATE) "

	elseif (CDate(StartOfMonthDate) > MONTH_END_OF_REAL_DATE) then

	'THIS IS THE BACS FUTURE ACTUAL SQL FOR THE NEXT MONTH
	SQLFUTURE = "SELECT DAY(RJ.TRANSACTIONDATE) AS THEDAY, COUNT(TR.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS TOTALVALUE " &_
			"FROM F_TENANTREFUNDS TR " &_
			"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
			"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
			"WHERE TR.STATUS =0 AND TB.JOURNALID IS NULL AND RJ.TRANSACTIONDATE > '" & StartOfMonthDate & "' AND RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
			"GROUP BY DAY(RJ.TRANSACTIONDATE) "

	end if

	' Add items to the calendar
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			OpenDB()

			SQL_ALREADY_DONE = "SELECT DAY(TB.PROCESSDATE) AS THEDAY, COUNT(TR.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(AMOUNT),0) AS TOTALVALUE " &_
					"FROM F_TENANTREFUNDS TR " &_
					"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
					"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
					"WHERE TR.STATUS > 0 AND TB.JOURNALID IS NOT NULL AND TB.PROCESSDATE >= '" & StartOfMonthDate & "'AND TB.PROCESSDATE <= '" & EndOfMonthDate & "' " &_
					"GROUP BY DAY(TB.PROCESSDATE) "

			Call OpenRs(rsDone, SQL_ALREADY_DONE)
			while NOT rsDone.EOF
				'Response.Write "A"
				MyCalendar.Days(Cint(rsDone("THEDAY"))).AddActivity "<a href='TenantRefundTypeList_VIEW.asp?PODate=" & rsDone("THEDAY") & CurrentDateSmall & "' title='Total Processed Tenant Refunds --> " & rsDone("TOTALCOUNT") & ".  Total Value of Tenant Refunds --> " & FormatCurrency(rsDone("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDone("TOTALCOUNT") & " </b></small></font></a>", "steelblue"				
				rsDone.moveNext
			wend
			CloseRs(rsDone)
			
			if (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) then			
			
				'THIS SECTION DISPLAYS THE ACTUAL POS DUE
				Call OpenRs(rsDays, SQL)
				if (NOT rsDays.EOF) then
					InTheSystem = rsDays("TOTALCOUNT")
					MyCalendar.Days(Cint(Day(Date))).AddActivity "<a href='TenantRefundTypeList.asp?PODate=" & Day(Date) & CurrentDateSmall & "' title='Total Tenant Refunds --> " & rsDays("TOTALCOUNT") & ".  Total Value of Tenant Refunds --> " & FormatCurrency(rsDays("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDays("TOTALCOUNT") & " </b></small></font></a>", "RED"
				end if
				CloseRs(rsDays)									

			end if

			if ( (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) OR (CDate(StartOfMonthDate) > MONTH_END_OF_REAL_DATE) ) then

				'Response.Write "hello"
				'THIS SECTION DISPLAYS THE FUTURE POS DUE
				Call OpenRs(rsDays, SQLFUTURE)
				while (NOT rsDays.EOF)
					InTheSystem = rsDays("TOTALCOUNT")
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a title='Total Tenant Refunds --> " & rsDays("TOTALCOUNT") & ".  Total Value of Tenant Refunds --> " & FormatCurrency(rsDays("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDays("TOTALCOUNT") & " </b></small></font></a>", "LIGHTGREEN"
					rsDays.moveNext
				wend
				CloseRs(rsDays)									

			end if		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
                %>
                <div style='position: absolute; left: 500; top: 140; width: 200px'>
                    <b><u>Tenant Refunds</u></b><br/>
                    <br/>
                    This calendar shows the number of Tenant Refunds that are ready to be paid. To get
                    the financial value of these transactions hover over the respective number.
                    <br/>
                    <br/>
                    To change the selected month click on '<b><<</b>' or '<b>>></b>' to go backwards
                    and forwards respectively.
                    <br/>
                    <br/>
                    <a bgcolor="RED" style='background-color: red'>&nbsp;&nbsp;</a> Tenant Refunds Due
                    Payment.<br/>
                    <br/>
                    <a bgcolor="steelblue" style='background-color: steelblue'>&nbsp;&nbsp;</a> Tenant
                    Refunds Previously Paid.<br/>
                    <br/>
                    <a bgcolor="lightgreen" style='background-color: lightgreen'>&nbsp;&nbsp;</a> Tenant
                    Refunds Due In Future.<br/>
                    <br/>
                    To view processing options for the Tenant Refunds click on the red number.
                </div>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
