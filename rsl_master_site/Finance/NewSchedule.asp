<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="AccessCheck.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<HTML>
<HEAD>
<%
	CONST CONST_PAGESIZE = 15
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim clean_desc
	
	ColData(0)  = "Local Authority|LOCALAUTHORITY|190"
	SortASC(0) 	= "LA.DESCRIPTION ASC"
	SortDESC(0) = "LA.DESCRIPTION DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Schedule Date|SCHEDULEDATE|130"
	SortASC(1) 	= "SCHEDULEDATE ASC"
	SortDESC(1) = "SCHEDULEDATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Schedule Total|AMOUNT|120"
	SortASC(2) 	= "SCHEDULEDTOT ASC"
	SortDESC(2) = "SCHEDULEDTOT DESC"	
	TDSTUFF(2)  = " "" align='right' "" "
	TDFunc(2) = "FormatCurrency(|)"		

	ColData(3)  = "Housing Benefit|HBTOTAL|120"
	SortASC(3) 	= "HBTOTAL ASC"
	SortDESC(3) = "HBTOTAL DESC"	
	TDSTUFF(3)  = " "" align='right' "" "
	TDFunc(3) = "FormatCurrency(|)"		

	ColData(4)  = "Balance|BALANCE|120"
	SortASC(4) 	= "BALANCE ASC"
	SortDESC(4) = "BALANCE DESC"	
	TDSTUFF(4)  = " "" align='right' "" "
	TDFunc(4) = "FormatCurrency(|)"		

	ColData(5)  = "|DEL|30"
	SortASC(5) 	= ""
	SortDESC(5) = ""	
	TDSTUFF(5)  = " "" onclick=""""DeleteItem("" & rsSet(""SCHEDULEID"") & "")"""" "" "
	TDFunc(5) = ""		

	PageName = "NewSchedule.asp"
	EmptyText = "No Relevant HB Schedules found in the system!!!"
	DefaultOrderBy = SortDESC(1)
	RowClickColumn = "" 

	
	IF (rEQUEST("SEL_LAFILTER") <> "") THEN
		LASQL = " AND LA.LOCALAUTHORITYID = " & REQUEST("SEL_LAFILTER") & " "
		INNER_LASQL = " AND D.LOCALAUTHORITY = " & REQUEST("SEL_LAFILTER") & " "
	ELSE
		LASQL = ""
		INNER_LASQL = ""
	END IF
	
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE ="SELECT " &_	
	"S.SCHEDULEID, LA.DESCRIPTION AS LOCALAUTHORITY, S.SCHEDULEDATE, " &_
	"S.SCHEDULEDTOT AS AMOUNT, ISNULL(T.AMOUNT,0) AS HBTOTAL, " &_
	"'<font color=red>&nbsp;Del</font>' AS DEL, " &_
	"S.SCHEDULEDTOT + ISNULL(T.AMOUNT,0) AS BALANCE " &_
	"	FROM F_SCHEDULE S " &_
	"	INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = S.LOCAUTHORITY " & LASQL & " " &_
	"	LEFT JOIN " &_
	"		(SELECT ISNULL(SUM(AMOUNT),0) AS AMOUNT, SCHEDULEID " &_
	"			FROM F_RENTJOURNAL RJ " &_
	"				INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
	"				INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
	"				INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " & INNER_LASQL & " " &_
	"				INNER JOIN F_SCHEDULE S " &_
	"					ON CONVERT(VARCHAR,S.SCHEDULEDATE,103) = CONVERT(VARCHAR, RJ.TRANSACTIONDATE, 103) " &_
	"						AND D.LOCALAUTHORITY = S.LOCAUTHORITY " &_
	"				WHERE PAYMENTTYPE IN (1,15,16) AND STATUSID NOT IN (1,4) " &_
	"			GROUP BY S.SCHEDULEID " &_
	"		) T " &_
	"		ON T.SCHEDULEID = S.SCHEDULEID " &_
    "ORDER BY " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	

	Call Create_Table()





%>
<%
	OpenDB()




	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED 
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD 7"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)

'rw YearStartDate & "<BR>"
'rw YearendDate & "<BR>"
	Call CloseRs(rsTAXDATE)

	Call BuildSelect(lst_LA, "sel_LA", "F_HBINFORMATION HB INNER JOIN C_TENANCY CT ON HB.TENANCYID = CT.TENANCYID INNER JOIN P__PROPERTY P ON CT.PROPERTYID = P.PROPERTYID	INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID	INNER JOIN G_LOCALAUTHORITY G ON G.LOCALAUTHORITYID = D.LOCALAUTHORITY", " DISTINCT G.LOCALAUTHORITYID, G.DESCRIPTION", "G.DESCRIPTION", "Please Select", NULL, NULL, "textbox200",NULL)
	Call BuildSelect(lst_LAFilter, "sel_LAFILTER", "F_HBINFORMATION HB INNER JOIN C_TENANCY CT ON HB.TENANCYID = CT.TENANCYID INNER JOIN P__PROPERTY P ON CT.PROPERTYID = P.PROPERTYID	INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID	INNER JOIN G_LOCALAUTHORITY G ON G.LOCALAUTHORITYID = D.LOCALAUTHORITY", " DISTINCT G.LOCALAUTHORITYID, G.DESCRIPTION", "G.DESCRIPTION", "ALL Local Authorities", REQUEST("sel_LAFILTER"), NULL, "textbox200", " ONCHANGE=""FilterTable()"" ")
	CloseDB()
%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array();
	var txtAmtValue;
	var ActiveChkBoxid;
	var vre=0;
	
	FormFields[0] = "sel_LA|Local Authority|SELECT|Y"
	FormFields[1] = "txt_SCHEDULEDATE|Schedule Date|DATE|Y"	
	FormFields[2] = "txt_SCHEDULEDTOT|Schedule Total|CURRENCY|Y"
	var isNewSchedule;
	isNewSchedule=0;
	var chkAboxIndex;
	
	function save_form()
	{
		FormFields[0] = "sel_LA|Local Authority|SELECT|Y"
		FormFields[1] = "txt_SCHEDULEDATE|Schedule Date|DATE|Y"	
		FormFields[2] = "txt_SCHEDULEDTOT|Schedule Total|CURRENCY|Y"
		if (!checkForm()) return false;

	    	var YStart = new Date("<%=YearStartDate%>")
	    	var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    	var YEnd = new Date("<%=YearendDate%>")


	    	if  ( real_date(frm.txt_SCHEDULEDATE.value) < YStart || real_date(frm.txt_SCHEDULEDATE.value) > YEnd ) {
	        	// we're outside the grace period and also outside this financial year
               	//alert("Please enter a Schedule date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
                alert("The month is now locked down and entries can only be made for the current month");
                return false;
	    	}	


		frm.action = "serverside/CreateNewSchedule.asp"
		frm.submit();
	}

	function FilterTable(){
		frm.action = "NewSchedule.asp"
		frm.submit();	
		}
		
	function DeleteItem(SCID){
		answer = confirm("You are about to delete a Schedule.\nThis action cannot be undone.\n\nDo you wish to continue?\n\nClick 'OK' to continue\nClick 'CANCEL' to cancel")
		if (!answer) return false;
		frm.ScheduleID.value = SCID
		frm.action = "ServerSide/DeleteHBSchedule.asp"
		frm.submit()
		}


			
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
-->
</SCRIPT>
<BODY CLASS='TA' BGCOLOR=#ffffff ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT="0" TOPMARGIN="10" MARGINWIDTH="0" ><!--#include virtual="Includes/Tops/BodyTop.asp" -->    
<form name='frm' method='post' action='serverside/createNewSchedule.asp'>        
   <input type=hidden name="ScheduleID">
  <TABLE  width=750>
	
    <TR>
      <TD valign=top rowspan=2> 
         <TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>
				<TR> 
                      <TD rowspan=2><IMG height=20 src="images/tab_NewSchedule.gif" border=0></TD>
                      <TD><IMG id=IMG1 style="HEIGHT: 20px" height=19 src="images/spacer.gif" ></TD>
                </TR>
                <TR> 
                      <TD BGCOLOR=#133e71 colspan=2><IMG height=1  width=222 src ="images/spacer.gif" border=0></TD>
                </TR>
          </TABLE>
        <table cellspacing=0  cellpadding=3  style="BORDER-RIGHT: #133e71 1px solid; BORDER-LEFT: #133e71 1px solid; BORDER-BOTTOM: #133e71 1px solid">
            <TR>
            <td nowrap >Local Authority &nbsp;&nbsp;&nbsp;</td> 
            <td align=righ> <%=lst_LA%> </td>
            <td ><IMG height=15 src="/js/FVS.gif" width=15 name=img_LA></td>
          </tr>
          <tr> 
            <td >Schedule Date &nbsp;&nbsp;&nbsp; </td>
            <td align=right> 
              <INPUT NAME=txt_SCHEDULEDATE CLASS='TEXTBOX200' maxlength =10></td>
             <td><IMG height=15 src="/js/FVS.gif" width=15 border=0 name=img_SCHEDULEDATE></td>
	         </tr>
          <tr> 
            <td nowrap height="25" >Schedules Total &nbsp;&nbsp;&nbsp;</td>
            <td align=right>
			<INPUT NAME=txt_SCHEDULEDTOT CLASS='TEXTBOX200'></td>
            <td ><IMG height=15 src="/js/FVS.gif" width=15 border=0 name=img_SCHEDULEDTOT></td> 
          </tr>
          <tr> 
            <td  colspan=2 align=right > 
              <input type="button" value="Save" class="RSLButton"  onclick="save_form()" name="button">
								<input type="reset" value="Cancel" class="RSLButton"   name="btnCancel">
			</td>
		 </tr>
        </table>
	 </TD>
      <TD width=360 valign=middle nowrap> 
	  <%
	  if (Request("ERRCDE") = 1) then 
	  %>
	  <font color=red><b>A schedule for the date and local authority you entered already exists.<br></b></font>
	  <%
	  elseif (Request("SCS") = 1) then 
	  %>
	  <font color=blue><b>Schedule Saved Successfully.<br></b></font>
	  <%
	  elseif (Request("DEL") = 1) then 
	  %>
	  <font color=red><b>Schedule DELETED Successfully.<br></b></font>
	  <%	
	  end if
	  %>
	  This page allows you to create a New Schedule which can then be validated against using your Housing Benefit Schedules.<br>Please enter /select the appropriate values and then click 'SAVE'</TD>	 
	</TR>
	<TR><TD align=right valign=bottom><b>Filter To: </b> <%=lst_LAFilter%> <a href='HBValidation.asp'><font color=blue><b>Back to HB Validation</b></font></a></TD>
    </TR>
	<TR>
	  <TD colspan=2>
		<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
			<TR>
				<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
			</TR>
		</TABLE>	  
	  <%=TheFullTable%></TD>
	</TR>	
  </TABLE>

</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->

</BODY>
</HTML>
