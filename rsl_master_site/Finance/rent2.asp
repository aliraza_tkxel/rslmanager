<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
	Dim credit_string
	Dim debit_string
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	Const adUseClient = 3
					
	Call credit_account(4000, 120, 1)
	//Call credit_account(4030, 120, 1)
	'Call credit_account(4040, 10, 1)
	'Call credit_account(4050, 10, 1)
	'Call credit_account(4060, 20, 1)
	'Call credit_account(4070, 20, 1)
	'Call credit_account(4080, 20, 1)	
	
	Call debit_account(1600, 120, 0)

	Function debit_account(nominal_code, credit_amount, FQ_status)
		
		Dim oConnection
		Set oConnection = CreateObject("ADODB.Connection")
		oConnection.Open "DSN=RSLQB;"
		
		Dim nominal_code_table_listid 	
		
		ordernumber = 34
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient
	
		// get correct reference id from accounts table for 5630
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		debit_string = "INSERT INTO ""JOURNALENTRYDEBITLINE"" (" &_
								" ""REFNUMBER"", ""JOURNALDEBITLINEACCOUNTREFLISTID"", " &_
								" ""JOURNALDEBITLINEAMOUNT"", ""JOURNALDEBITLINEMEMO"", ""JournalDEBitLineEntityRefListID"",""JournalDEBitLineEntityRefFullName"", ""FQSAveTOCache"") " &_
								" VALUES (' " &_
								ordernumber & "', '" &_
								nominal_code_table_listid & "', " &_
								credit_amount &_
								",'Auto RSL Debit', 'A0000-1084200309','BHA'," & FQ_status & ")" & VbCrLf & VbCrLf
		response.write "<BR> 1st insert string = : " & debit_string						
		
		oConnection.execute(debit_string)
		oConnection.Close
		Set oConection = Nothing
	
	End Function	

	Function credit_account(nominal_code, credit_amount, FQ_status)

		Dim oConnection
		Set oConnection = CreateObject("ADODB.Connection")
		oConnection.Open "DSN=RSLQB;"
		
		Dim nominal_code_table_listid 	
		ordernumber = 36
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient

		// get correct reference id from accounts table for 5630
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		credit_string = "INSERT INTO ""JOURNALENTRYCREDITLINE"" (" &_
								" ""REFNUMBER"", ""JOURNALCREDITLINEACCOUNTREFLISTID"", " &_
								" ""JOURNALCREDITLINEAMOUNT"", ""JOURNALCREDITLINEMEMO"", ""JournalCREDitLineEntityRefListID"",""JournalCREDitLineEntityRefFullName"", ""FQSAveTOCache"") " &_
								" VALUES (' " &_
								ordernumber & "', '" &_
								nominal_code_table_listid & "', " &_
								credit_amount &_
								",'Auto RSL Credit', 'A0000-1084200309','BHA'," & FQ_status & ")" & VbCrLf & VbCrLf
		response.write "<BR> 1st insert string = : " & credit_string						
		oConnection.execute(credit_string)

		oConnection.Close
		Set oConection = Nothing

	End Function	

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bacs List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF>

</BODY>
</HTML>