<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	ColData(0)  = "Tenant Ref|TENANCYID|100"
	SortASC(0) 	= "T.TENANCYID ASC"
	SortDESC(0) = "T.TENANCYID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "TenancyReference(|)"

	ColData(1)  = "Account Holder|ACCOUNTNAME|230"
	SortASC(1) 	= "ACCOUNTNAME ASC"
	SortDESC(1) = "ACCOUNTNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Sort Code|SORTCODE|95"
	SortASC(2) 	= "SORTCODE ASC"
	SortDESC(2) = "SORTCODE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Acc No|ACCOUNTNUMBER|100"
	SortASC(3) 	= "ACCOUNTNUMBER ASC"
	SortDESC(3) = "ACCOUNTNUMBER DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Payment Amount|PAYMENTAMOUNT|150"
	SortASC(4) 	= "PAYMENTAMOUNT ASC"
	SortDESC(4) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	ColData(5)  = "Originator Number|ORIGINATOR|150"
	SortASC(5) 	= ""
	SortDESC(5) = ""	
	TDSTUFF(5)  = " ""align=center"" "
	TDFunc(5) = ""		

	PageName = "DDList.asp"
	EmptyText = "No Direct Debit Order found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("pROCESSINGdATE") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("pROCESSINGdATE")),2)
		RequiredDate = CDate(Request("pROCESSINGdATE"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ButtonDisabled = ""
	ORIGINATORNUMBER = "658670"
	
'	Response.Write ProcessingDate & "<BR>"
'	Response.Write RequiredDate & "<BR>"
'	Response.Write Date & "<BR>"	
	
	'two sql scripts are required, the one which appears next shows
	'the direct debits which will potentially come out on the specified date
	if (RequiredDate > Date) then
		ReturnType = "Forecast"
		SQLCODE = "SELECT T.TENANCYID, DD.ACCOUNTNAME, DD.SORTCODE, DD.ACCOUNTNUMBER, " &_
					"PAYMENTAMOUNT = CASE " &_
					"WHEN DD.INITIALDATE = '" & FormatDateTime(RequiredDate,1) & "' THEN DD.INITIALAMOUNT " &_
					"ELSE DD.REGULARPAYMENT " &_
					"END, " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
					"FROM F_DDSCHEDULE DD " &_
					"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
					"	WHERE DAY(PAYMENTDATE) = DAY('" & FormatDateTime(RequiredDate,1) & "') AND PERIODBALANCE <> 0 AND ISNULL(SUSPEND,0) <> 1  AND ISNULL(TEMPSUSPEND,0) <> 1 AND PAYMENTDATE <= '" & FormatDateTime(RequiredDate,1) & "' " &_
					"		AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''  " &_
					"		AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
					"		AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_					
				    "		ORDER BY " + Replace(orderBy, "'", "''") + ""
	else
		ExtraSQL = ""
		ReturnType = "Actual"		
		if (Request("DECLINED") = 1) then
			ExtraSQL = " AND RJ.STATUSID = 11 "
			ButtonDisabled = " disabled"
			ReturnType = "Subsequently Declined"			
		ELSE
			ExtraSQL = " AND RJ.STATUSID <> 11 "		
		END IF
		'the sql below shows the direct debit transactions which have occured.

		SQLCODE = "SELECT RJ.TENANCYID, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER, ISNULL(ABS(RJ.AMOUNT),0) AS PAYMENTAMOUNT, " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
					"FROM F_RENTJOURNAL RJ " &_
					"INNER JOIN F_RENTJOURNALDDSCHED RJD ON RJ.JOURNALID = RJD.JOURNALID " &_
					"INNER JOIN F_DDSCHEDULE DD ON RJD.DDSCHEDULEID = DD.DDSCHEDULEID " &_
					"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
					"	WHERE RJ.TRANSACTIONDATE = '" & FormatDateTime(RequiredDate,1) & "' " & ExtraSQL &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""
	end if				
	'rw SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Direct Debit List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

function cHECKpAGE(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "<%=PageName & "?CC_Sort=" & orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value
		}
	}

function SaveResults(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "ServerSide/DDList_CSV.asp<%="?CC_Sort=" & orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value
		}
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack" width=750><tr><td><b>&nbsp;DATE</b></td>
<td><input type=text name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>" style='width:135px;'></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="cHECKpAGE()" <%=ButtonDisabled%>>
</td><td><input type=button class="RSLButton" value=" Export to CSV " onclick="SaveResults()" <%=ButtonDisabled%>>
</td><td nowrap> &nbsp;&nbsp;Showing '<b><%=ReturnType%></b>' Direct Debits.
</td>
<td width=100% align=right>&nbsp<a href='DDDisplay.asp?date=<%=Request("pROCESSINGdATE")%>'><font color=blue><b>DD Calendar</b></font></a></td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>