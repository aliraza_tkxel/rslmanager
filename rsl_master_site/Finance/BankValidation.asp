<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
	Call BuildSelect(lst_paymentType, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID NOT IN (1,10,11,27,28,26,12,13,15,16) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "All", Null, "width:150px", "textbox200", NULL)

	Call BuildSelect(lst_paymentMethod, "sel_PAYMENTMETHOD", "F_POSTATUS WHERE POSTATUSID IN (10,11,12,14)", "POSTATUSID, POSTATUSNAME", "POSTATUSNAME", "All", Null, "width:150px", "textbox200", NULL)

	Call BuildSelect(lst_statement, "sel_STATEMENT", "F_BANK_STATEMENTS", "BSID, DATENAME(DAY,STARTDATE) + ' ' + DATENAME(MONTH,STARTDATE) + ' ' + DATENAME(YEAR,STARTDATE) + ' - ' + DATENAME(DAY,ENDDATE) + ' ' + DATENAME(MONTH,ENDDATE) + ' ' + DATENAME(YEAR,ENDDATE) AS DATEPERIOD ", "STARTDATE", "Please Select", Null, NULL, "textbox200", " onchange=""SelectStatement()"" ")

	Call BuildSelect(lst_LA, "sel_LA", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION ", "DESCRIPTION", "Please Select", Null, "width:150px", "textbox200", "")
	Call BuildSelect(lst_LA2, "sel_LA2", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION ", "DESCRIPTION", "Please Select", Null, "width:150px", "textbox200", "")
	Call BuildSelect(lst_LA3, "sel_LA3", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION ", "DESCRIPTION", "Please Select", Null, "width:150px", "textbox200", "")
	CloseDB()	
	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bank Validation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var TempToDates = new Array();
	var TempFromDates = new Array();
	var FieldNames = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal_debit = new Number();
	detotal_credit = new Number();	

	function SetChecking(){
	
		// These are the Abreviations for the different bank validation pages found at /serverside/Bank[Name]Validation_srv.asp
		FieldNames[0] = ""
		FieldNames[1] = "P"
		FieldNames[2] = "HB"
		FieldNames[3] = "PC"
		FieldNames[4] = "EX"
	
		EL = RSLFORM.sel_VALIDATE.value
		if (EL == 1) {
			FormFields[0] = "sel_PAYMENTTYPE|Payment Type|SELECT|N"
			FormFields[1] = "txt_FROM|From|DATE|N"
			FormFields[2] = "txt_TO|To Date|DATE|N"
			}
		else if (EL == 2) {
			FormFields[0] = "sel_PAYMENTMETHOD|Payment Method|SELECT|N"
			FormFields[1] = "txt_PFROM|From|DATE|N"
			FormFields[2] = "txt_PTO|To Date|DATE|N"
			FormFields[3] = "txt_CHEQUE|Cheque No.|TEXT|N"		
			}
		else if ((EL == 3)||(EL == 4)||(EL == 5)) {
			
			FormFields[0] = "txt_" + FieldNames[EL - 1] + "FROM|From|DATE|N"
			FormFields[1] = "txt_" + FieldNames[EL - 1] + "TO|To Date|DATE|N"
				
				if (EL == 5){
				FormFields[2] = "txt_" + FieldNames[EL - 1] + "CHEQUE|Cheque No|TEXT|N"
				}
				
			}
		}
				
	function process(){
		if (int_initial == 0) {
			alert("Please select at least one item to validate!")
			return false
			}
		answer = confirm("You are about to Validate a total of " + int_initial + " entries.\nThe debit value is �" + FormatCurrencyComma(detotal_debit) + ".\nThe credit value is �" + FormatCurrencyComma(detotal_credit) + ".\nAre you sure you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")			
		if (!answer) return false;
		EL = RSLFORM.sel_VALIDATE.value		
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action =ProcessPage[EL] + "?detotal_credit=" + detotal_credit + "&detotal_debit=" + detotal_debit;
		RSLFORM.submit();
		}

	function SelectStatement(){
		reset_client_totals()
		CurrentlySelected = RSLFORM.sel_STATEMENT.value
		ValSelected = RSLFORM.sel_VALIDATE.value
		RSLFORM.reset()
		RSLFORM.sel_STATEMENT.value	= CurrentlySelected
		RSLFORM.sel_VALIDATE.value = ValSelected
		ShowAppropriateLines()
		hb_div.innerHTML = ""
		RSLFORM.sel_VALIDATE.disabled = true
		if (RSLFORM.sel_STATEMENT.value != ""){
			RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
			RSLFORM.action = "serverside/BankStatement_srv.asp";
			RSLFORM.submit();
			RSLFORM.sel_VALIDATE.disabled = false
			}
		}
		
	function reset_client_totals(){
		RSLFORM.idlist.value = ""
		int_initial = 0;
		str_idlist = "";
		detotal_debit = 0;
		detotal_credit = 0;		
		}
	
	var GetPage = new Array("", "serverside/BankValidation_svr.asp", "serverside/BankPOValidation_svr.asp", "serverside/BankHBValidation_svr.asp", "serverside/BankPCValidation_svr.asp", "serverside/BankEXPENSESValidation_svr.asp")
	var ProcessPage = new Array("", "serverside/BankValidationProcess_srv.asp", "serverside/BankPOValidationProcess_srv.asp", "serverside/BankHBValidationProcess_srv.asp", "serverside/BankPCValidationProcess_srv.asp", "serverside/BankEXPENSESValidationProcess_srv.asp")	
	
	function submit_form(){
		SetChecking()
		if (RSLFORM.sel_STATEMENT.value == ""){
			alert("Please select a bank statement you wish to validate against.")
			return false
			}
		if (!checkForm()) return false;
		reset_client_totals();
		EL = RSLFORM.sel_VALIDATE.value
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = GetPage[EL]
		RSLFORM.submit();
		}
  
	// CALCULATE RUNNING TOTAL OF RENT JOURNAL SELECTIONS
	// LA_id has been included for housing benefit section only
	function do_sum(int_num, int_amount, type){

		if (document.getElementById("chkpost" + int_num+"").checked == true){
			if (type == 1) 
				detotal_debit = detotal_debit + parseFloat(int_amount,10);
			else
				detotal_credit = detotal_credit + parseFloat(int_amount,10);			
			if (int_initial == 0) {// first entry
				str_idlist = str_idlist + int_num.toString();
				}
			else{
				str_idlist = str_idlist + "," + int_num.toString();
				}
			
			int_initial = int_initial + 1; // increase count of elements in string
			srcElem = window.event.srcElement;
			while (srcElem.tagName != "TR" && srcElem.tagName != "BODY")
				srcElem = srcElem.parentElement;
			if (srcElem.tagName == "TR"){
				srcElem.style.backgroundColor = "steelblue"
				srcElem.style.color = "white"
				}
			}
		else {
			if (type == 1) 
				detotal_debit = detotal_debit - parseFloat(int_amount,10);
			else
				detotal_credit = detotal_credit - parseFloat(int_amount,10);			
			int_initial = int_initial - 1;
			remove_item(int_num);

			srcElem = window.event.srcElement;
			while (srcElem.tagName != "TR" && srcElem.tagName != "BODY")
				srcElem = srcElem.parentElement;
			if (srcElem.tagName == "TR"){
				srcElem.style.backgroundColor = "white"
				srcElem.style.color = "black"
				}
			}
		if (type == 1) 
			document.getElementById("txt_POSTTOTALDEBIT").value = FormatCurrencyComma(detotal_debit);
		else
			document.getElementById("txt_POSTTOTALCREDIT").value = FormatCurrencyComma(detotal_credit);
		CurrentCredits = parseFloat(document.getElementById("hid_RSLCREDITS").value,10)
		CurrentDebits = parseFloat(document.getElementById("hid_RSLDEBITS").value,10)
		
		document.getElementById("txt_RSLDEBITS").value = FormatCurrencyComma(CurrentDebits + detotal_debit)							
		document.getElementById("txt_RSLCREDITS").value = FormatCurrencyComma(CurrentCredits + detotal_credit)									
		document.getElementById("txt_RSLENDINGBALANCE").value = FormatCurrencyComma(parseFloat(document.getElementById("hid_STARTINGBALANCE").value,10) + (CurrentDebits + detotal_debit) - (CurrentCredits + detotal_credit))					
		document.getElementById("idlist").value = str_idlist;
		//if (int_initial >= 100)
			//alert("You have selected at least 100 items to validate.\n For performance reasons you should validate all currently stored entries.")
		}
		
	function TravelTo(NextPage){
		EL = RSLFORM.sel_VALIDATE.value	
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = GetPage[EL] + "?" + NextPage + "&detotal_debit=" + detotal_debit.toString() + "&detotal_credit=" + detotal_credit.toString()
		//+ "&selected=" + str_idlist.toString();	
		RSLFORM.submit();			
		}
		
	function getItems(ItemDate, LAuthID, DebitCredit){
	
	window.showModalDialog("Popups/pBankRecItems.asp?dateRange=" + ItemDate + "&LAuthID=" + LAuthID + "&ISDEBIT=" + DebitCredit , "Transactions", "dialogHeight: 350px; dialogWidth: 790px; status: No; resizable: No;");

		}

	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){

		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
		document.getElementById("idlist").value = str_idlist;		
		}
	
function WhichVal(){
	reset_client_totals()
	CurrentCredits = parseFloat(document.getElementById("hid_RSLCREDITS").value,10)
	CurrentDebits = parseFloat(document.getElementById("hid_RSLDEBITS").value,10)	
	document.getElementById("txt_RSLDEBITS").value = FormatCurrencyComma(CurrentDebits)							
	document.getElementById("txt_RSLCREDITS").value = FormatCurrencyComma(CurrentCredits)									
	document.getElementById("txt_RSLENDINGBALANCE").value = FormatCurrencyComma(parseFloat(document.getElementById("hid_STARTINGBALANCE").value,10) + (CurrentDebits) - (CurrentCredits))					

	hb_div.innerHTML = ""
	ShowAppropriateLines()
	}
	
	function new_Statement(){
	document.location ="addstatement.asp"
	
	}

function ShowAppropriateLines(){
	setDateRanges()
	document.getElementById("hid_PREVVALIDATE").value = RSLFORM.sel_VALIDATE.value
	GRef = document.getElementsByName("TRLine") 
	for (j=1; j<6; j++){
		GRef = document.getElementsByName("Line" + j) 	
		if (GRef.length){
			for (i=0; i<GRef.length; i++)
				GRef[i].style.display = "none"
			}
		}
	
	Ref = document.getElementsByName("Line" + document.getElementById("sel_VALIDATE").value)
	if (Ref.length){
		for (i=0; i<Ref.length; i++)
			Ref[i].style.display = "block"
		}
	}

	function setDateRanges(){
	ValidationType = parseInt(RSLFORM.hid_PREVVALIDATE.value,10)

	// List the types of validation names
	//To Dates
		TempToDates[0]	= RSLFORM.txt_TO
		TempToDates[1]	= RSLFORM.txt_PTO
		TempToDates[2]	= RSLFORM.txt_HBTO
		TempToDates[3]	= RSLFORM.txt_PCTO
		TempToDates[4]	= RSLFORM.txt_EXTO
	
	//From Dates	
		TempFromDates[0]	= RSLFORM.txt_FROM
		TempFromDates[1]	= RSLFORM.txt_PFROM
		TempFromDates[2]	= RSLFORM.txt_HBFROM
		TempFromDates[3]	= RSLFORM.txt_PCFROM
		TempFromDates[4]	= RSLFORM.txt_EXFROM
		
	if (RSLFORM.sel_VALIDATE.value == ""){
	ValidationType = 1
	}
	
	for (i = 0; i < 5; i++){
		if (i != ValidationType - 1){
			TempToDates[i].value = TempToDates[ValidationType - 1].value;
			TempFromDates[i].value = TempFromDates[ValidationType -1].value;
			}	
		}
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width=100% cellspacing=0 cellpadding=0>
  <form name=RSLFORM method=post>
    <tr>
      <td> 
        <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
          <TR> 
            <TD></TD>
          </TR>
        </TABLE>
        <TABLE BORDER=0 width=100% CELLPADDING=1 CELLSPACING=1 HEIGHT=30PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
          <TR> 
            <TD NOWRAP colspan=12 style='border-bottom:1px solid #133e71'> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="45%"><b>&nbsp;STATEMENT : </b>&nbsp;&nbsp;<%=lst_Statement%> 
                    <input type=hidden value="" id="idlist" name="idlist">
                  </td>
                  <td align="right" width="55%"> 
                    <input type="button" value="New Statement" class="RSLButton"  onClick="new_Statement()" name="button2">
                    &nbsp;&nbsp;&nbsp; </td>
                </tr>
              </table>
            </TD>
          </TR>
          <TR> 
            <TD><b>&nbsp;BANK : </b>&nbsp;</TD>
            <TD><b>Start : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_STARTINGBALANCE" style='text-align:right' class="textbox" readonly size=16>
              <input type=hidden name="hid_STARTINGBALANCE">
            </TD>
            <TD align=center><b>+</b></TD>
            <TD align=right><b>Debits : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_DEBITS" style='text-align:right' class="textbox" readonly size=16>
            </TD>
            <TD align=center><b>-</b></TD>
            <TD align=right><b>Credits : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_CREDITS" style='text-align:right' class="textbox" readonly size=16>
            </TD>
            <TD align=center><b>=</b></TD>
            <TD align=right><b>End : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_ENDINGBALANCE" style='text-align:right' class="textbox" readonly size=16>
            </TD>
          </TR>
          <TR> 
            <TD><b>&nbsp;RSL : </b>&nbsp;</TD>
            <td COLSPAN=2><i>Same as above</i></td>
            <TD align=center><b>+</b></TD>
            <TD align=right><b>Debits : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_RSLDEBITS" style='text-align:right' class="textbox" readonly size=16>
              <input type=hidden name="hid_RSLDEBITS">
            </TD>
            <TD align=center><b>-</b></TD>
            <TD align=right><b>Credits : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_RSLCREDITS" style='text-align:right' class="textbox" readonly size=16>
              <input type=hidden name="hid_RSLCREDITS">
            </TD>
            <TD align=center><b>=</b></TD>
            <TD align=right><b>End : </b>&nbsp;</TD>
            <TD>
              <input type=text name="txt_RSLENDINGBALANCE" style='text-align:right' class="textbox" readonly size=16>
              <input type=hidden name="hid_RSLENDINGBALANCE">
            </TD>
          </TR>
        </TABLE>
        <br>
        <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2 HEIGHT=40PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
          <TR> 
            <TD colspan=7 style='border-bottom:1px solid #133e71'> <b> Validatea 
              : </b> &nbsp; 
              <input type=hidden name="hid_PREVVALIDATE" value="1">
              <select name="sel_VALIDATE" class="textbox" ONCHANGE="WhichVal()" disabled>
                <option value="1">Rent Journal</option>
                <option value="2">Invoice Payments</option>
				<option value="3">Housing Benefit</option>
				<option value="4">Payement Cards</option>
				<option value="5">Staff Expenses</option>
              </select>
              &nbsp;&nbsp;Enter search criteria to bring back relevant <b>rent 
              journal</b> entries to validate your bank statement. </TD>
          </TR>
          <TR id="Line1" name="TRLine" style="display:block"> 
            <TD NOWRAP><b>Payment Type</b></TD>
            <TD><b>From</b></TD>
            <TD><b>To</b></TD>
            <TD><b>Debit/Credit</b></TD>
            <TD><b>Local Authority</b></TD>
            <TD><b>Slip No.</b></TD>
          </TR>
          <TR id="Line1" name="TRLine" style="display:block"> 
            <TD NOWRAP> <%=lst_paymentType%> <image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_FROM CLASS='TEXTBOX100' onblur="setDateRanges(1)" STYLE="width:80px">
              <image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0"> 
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_TO CLASS='TEXTBOX100' onblur="setDateRanges(1)" STYLE="width:80px">
              <image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP> 
              <select name="sel_TYPE" class="textbox">
                <option value="">All</option>
                <option value=1>Debit</option>
                <option value=2>Credit</option>
              </select>
            </TD>
            <TD NOWRAP> <%=lst_LA%></TD>
            <TD NOWRAP> 
              <input type=TEXT name=txt_SLIPNO class='TEXTBOX100' onBlur="RSLFORM.txt_PFROM.value = RSLFORM.txt_FROM.value" STYLE="width:60px">
            </TD>
            <TD WIDTH=100% NOWRAP> 
              <input type="button" value="Proceed" class="RSLButton"  onclick="submit_form()" "name="button">
            </TD>
          </TR>
          <TR id="Line2" name="TRLine" style="display:none"> 
            <TD NOWRAP><b>Payment Method</b></TD>
            <TD><b>From (PAYMENT)</b></TD>
            <TD><b>To (PAYMENT)</b></TD>
            <TD><b>Cheque No.</b></TD>
            <TD><b>Debit/Credit</b></TD>
          </TR>
          <TR id="Line2" name="TRLine" style="display:none"> 
            <TD NOWRAP> <%=lst_paymentMethod%> <image src="/js/FVS.gif" name="img_PAYMENTMETHOD" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_PFROM CLASS='TEXTBOX100' onblur="setDateRanges(2)">
              <image src="/js/FVS.gif" name="img_PFROM" width="15px" height="15px" border="0"> 
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_PTO CLASS='TEXTBOX100' onblur="setDateRanges(2)">
              <image src="/js/FVS.gif" name="img_PTO" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_CHEQUE CLASS='TEXTBOX100'>
              <image src="/js/FVS.gif" name="img_CHEQUE" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP> 
              <select name="sel_PTYPE" class="textbox">
                <option value="">All</option>
                <option value=1>Debit</option>
                <option value=2>Credit</option>
              </select>
            </TD>
            <TD WIDTH=100% NOWRAP> 
              <input type="button" value="Proceed" class="RSLButton"  onclick="submit_form()" "name="button">
            </TD>
          </TR>
		  		  <TR id="Line3" name="TRLine" style="display:none"> 
            <TD NOWRAP><b>From</b></TD>
            <TD><b>To</b></TD>
            <TD><b>Local Authority</b></TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
          </TR>
          <TR id="Line3" name="TRLine" style="display:none"> 
            <TD NOWRAP>
              <input type=TEXT name=txt_HBFROM class='TEXTBOX100'  style="width:80px" onBlur="setDateRanges(3)">
             <image src="/js/FVS.gif" name="img_HBFROM" width="15px" height="15px" border="0" > 
            </TD>
            <TD NOWRAP>
              <input type=TEXT name=txt_HBTO class='TEXTBOX100' onBlur="setDateRanges(3)" style="width:80px">
              <image src="/js/FVS.gif" name="img_HBTO" width="15px" height="15px" border="0"> 
            <TD NOWRAP><%=lst_LA2%></TD>
            <TD NOWRAP>
              <input type="button" value="Proceed" class="RSLButton"  onClick="submit_form()" "name="button3">
            </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD WIDTH=100% NOWRAP>&nbsp; </TD>
          </TR>
		  <TR id="Line4" name="TRLine" style="display:none"> 
            <TD NOWRAP><b>From</b></TD>
            <TD><b>To</b></TD>
            <TD><b>Local Authority</b></TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
          </TR>
          <TR id="Line4" name="TRLine" style="display:none"> 
            <TD NOWRAP>
              <input type=TEXT name=txt_PCFROM class='TEXTBOX100'  style="width:80px" onBlur="setDateRanges(4)">
             <image src="/js/FVS.gif" name="img_PCFROM" width="15px" height="15px" border="0" > 
            </TD>
            <TD NOWRAP>
              <input type=TEXT name=txt_PCTO class='TEXTBOX100' onBlur="setDateRanges(4)" style="width:80px">
              <image src="/js/FVS.gif" name="img_PCTO" width="15px" height="15px" border="0"> 
            <TD NOWRAP><%=lst_LA3%></TD>
            <TD NOWRAP>
              <input type="button" value="Proceed" class="RSLButton"  onClick="submit_form()" "name="button4">
            </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD WIDTH=100% NOWRAP>&nbsp; </TD>
          </TR>
		  <TR id="Line5" name="TRLine" style="display:none"> 
            <TD NOWRAP><b>From</b></TD>
            <TD><b>To</b></TD>
            <TD><b>Cheque No.</b></TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
            <TD>&nbsp;</TD>
          </TR>
          <TR id="Line5" name="TRLine" style="display:none"> 
            <TD NOWRAP>
              <input type=TEXT name=txt_EXFROM class='TEXTBOX100'  style="width:80px" onBlur="setDateRanges(5)">
             <image src="/js/FVS.gif" name="img_EXFROM" width="15px" height="15px" border="0" > 
            </TD>
            <TD NOWRAP>
              <input type=TEXT name=txt_EXTO class='TEXTBOX100' onBlur="setDateRanges(5)" style="width:80px">
              <image src="/js/FVS.gif" name="img_EXTO" width="15px" height="15px" border="0"> 
            <TD NOWRAP>
              <input type=TEXT name=txt_EXCHEQUE class='TEXTBOX100'>
			  <image src="/js/FVS.gif" name="img_EXCHEQUE" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP>
              <input type="button" value="Proceed" class="RSLButton"  onClick="submit_form()" "name="button5">
            </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD NOWRAP>&nbsp; </TD>
            <TD WIDTH=100% NOWRAP>&nbsp; </TD>
          </TR>
		  
        </TABLE>
        <BR>
        <DIV ID=hb_div></DIV>
      </td>
    </tr>	
  </form>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name="BankFrame<%=TIMESTAMP%>" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>

