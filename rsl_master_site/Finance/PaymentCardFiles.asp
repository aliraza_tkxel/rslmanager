<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
POFilter = ""
If (Request("UploadDate") <> "") Then
	ProcessingDate = FormatDateTime(CDate(Request("UploadDate")),2)
	RequiredDate = CDate(Request("UploadDate"))
Else
	ProcessingDate = FormatDateTime(CDate(Date),2)
	RequiredDate = Date
End If

SQL = "SELECT ISNULL(PROCESSVALUE,0) AS PROCESSVALUE_FORMATTED, PCC.DESCRIPTION, * FROM F_PAYMENTCARDFILES PCF LEFT JOIN F_PAYMENTCARDCODES PCC ON PCF.PAYMENTCODE = PCC.CODEID " & _
" AND '.' + RIGHT(PCF.[FILENAME], Len(PCF.[FILENAME]) - Charindex('.', PCF.[FILENAME])) = PCC.FileExtensions WHERE UPLOADDATE = '" & FormatDateTime(RequiredDate,1) & "' ORDER BY FILETYPE ASC, FILEID DESC"
Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	while (NOT rsLoader.EOF)
		TableString = TableString & "<tr><td><a target=""_blank"" href=""/PaymentCardFile/" & rsLoader("FILENAME") & """><font color=""blue"">" & rsLoader("FILENAME") & "</font></a></td><td>" & rsLoader("PROCESSCOUNT") & "</td><td align=""right"">" & FormatCurrency(rsLoader("PROCESSVALUE_FORMATTED")) & "</td><td>&nbsp;&nbsp;" & rsLoader("DESCRIPTION") & "</td><td>" & rsLoader("UPLOADDATE") & "</td></tr>"
		rsLoader.moveNext
	Wend
Else
	TableString = "<tr><td colspan=""5"" align=""center"">No Files found for the specified date</td></tr>"
End If
Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Payment Card File List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    You will find the Payment Card files you have uploaded listed below. To save any
    particular file right click on it and click 'save as'.<br />
    Any Payment Card data which was not entered into the system will appear in files
    with a BAD suffix. If you open a BAD file each item will have a relevant information
    next to it explaining why it was not entered into the system.<br />
    <br />
    The Files you are viewing are for the Date: <b>
        <%=FormatDateTime(RequiredDate,1)%></b>
    <br />
    <br />
    <table style='border-collapse: collapse' border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td width="200">
                <b>File Name</b>
            </td>
            <td width="100">
                <b>Entry Count</b>
            </td>
            <td width="100" align="right">
                <b>Entry Value</b>
            </td>
            <td width="150">
                <b>&nbsp;&nbsp;Data Type</b>
            </td>
            <td width="100">
                <b>Upload Date</b>
            </td>
        </tr>
        <%=TableString%>
    </table>
    <br />
    <br />
    &nbsp<a href='PaymentCardDisplay.asp?date=<%=Request("UploadDate")%>'><font color="blue"><b>BACK
        to Payment Card Upload</b></font></a>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
