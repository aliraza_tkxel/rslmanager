<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Reconciliation > Confirm</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></script>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border="0" cellspacing="5" cellpadding="0">
<FORM name=RSLFORM method=POST>
<TR><TD>
The purchase order you are attempting to view cannot be viewed or found for the following reasons:
<ul>
<li>You typed in an invalid address into the browser address bar directly.
<li>You amended a purchase item via the reconcile page which subsequently has become queued, so you can no longer reconcile the item.
<li>You amended an item which also became queued.
<li>You followed an invalid link in which case you should report this error to Reidmark Communications or an administrator within your team.
<li>The purchase order has been cancelled or removed in the time it took you to click on the link, which was suppose to take you to the reconcile page for the corresponding purchase order.
</ul>


</TD></TR>
</FORM>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
