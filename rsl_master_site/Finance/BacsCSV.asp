<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	POFilter = ""
	If (Request("PROCESSINGDATE") <> "") Then
		ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
		RequiredDate = CDate(Request("PROCESSINGDATE"))
	Else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	End If

    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(bd.COMPANYID,1) = '" & Company & "' "

	Call OpenDB()
	
	' SQL HAS BEEN AMENDED TO SPLIT CREDIT NOTES AND PURCHASE ORDERS
	SQL = "SELECT 	BD.DATAID, FILENAME, INVOICECOUNT, ISNULL(PO.POSUM,0) + ISNULL(CR.CRSUM,0) AS TOTALVALUE, ISNULL(PO.POCNT,0) AS POCNT, " &_
			"		ISNULL(PO.POSUM,0) AS POSUM, ISNULL(CR.CRCNT,0) AS CRCNT, ISNULL(CR.CRSUM,0) AS CRSUM, " &_
			"		BD.PROCESSDATE, PB.PROCESSDATE AS ACTUALDDDATE, CHEQUENUMBER, ISFILE " &_
			"FROM 	F_BACSDATA BD " &_
			"		INNER JOIN F_POBACS PB ON BD.DATAID = PB.DATAID " &_ 
			"		LEFT JOIN (SELECT COUNT(*) AS POCNT, SUM(GROSSCOST) AS POSUM, DATAID FROM F_INVOICE I INNER JOIN F_POBACS B  " &_
			"		      ON I.INVOICEID = B.INVOICEID WHERE ISCREDIT = 0 " &_
			"				GROUP BY B.DATAID) PO ON PO.DATAID = BD.DATAID " &_
			"		LEFT JOIN (SELECT COUNT(*) AS CRCNT, SUM(GROSSCOST) AS CRSUM, DATAID FROM F_INVOICE I INNER JOIN F_POBACS B " &_
			"		      ON I.INVOICEID = B.INVOICEID WHERE ISCREDIT = 1 " &_
			"			GROUP BY B.DATAID) CR ON CR.DATAID = BD.DATAID " &_
			"		WHERE BD.PROCESSDATE = '" & FormatDateTime(RequiredDate,1) & "' " & CompanyFilter &_
			"GROUP 	BY BD.DATAID, FILENAME, INVOICECOUNT, TOTALVALUE, BD.PROCESSDATE, PB.PROCESSDATE, " &_
			"		PB.CHEQUENUMBER, ISFILE, FILETYPE, POSUM, CRSUM, POCNT, CRCNT " &_
			"ORDER 	BY FILETYPE ASC, BD.DATAID DESC "
	'rw SQL
	totalvalue = 0
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then
		while (NOT rsLoader.EOF) 
			ISFILE = rsLoader("ISFILE")
			chequenumber = rsLoader("CHEQUENUMBER")
			totalvalue = totalvalue + CDBL(rsLoader("TOTALVALUE"))
			if (ISFILE = 1) then	' CSV FILE PAYMENT LISTING
				TableString = TableString & "<TR>" &_
					"<td><a target=""_blank"" href='/BacsFile/" & rsLoader("FILENAME") & "'><font color=""blue"">" & rsLoader("FILENAME") & "</font></a></td>" &_
					"<TD ALIGN=CENTER>" & rsLoader("POCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("POSUM")) & "</TD>" &_
					"<TD ALIGN=CENTER>" & rsLoader("CRCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("CRSUM")) & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD>" &_
					"<TD ALIGN=RIGHT>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"
			elseif (not isnull(chequenumber) and chequenumber <> "") then	' CHEQUE PAYMENT LISTING
				'TableString = TableString & "<TR><TD><font color=BLACK>Cheque Number : " & chequenumber & "</font></TD><TD>" & rsLoader("INVOICECOUNT") & "</TD><TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD><TD>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"
				TableString = TableString & "<TR>" &_
					"<TD><font color=BLACK>Cheque Number : " & chequenumber & "</font></TD>" &_
					"<TD ALIGN=CENTER>" & rsLoader("POCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("POSUM")) & "</TD>" &_
					"<TD ALIGN=CENTER>" & rsLoader("CRCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("CRSUM")) & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD>" &_
					"<TD ALIGN=RIGHT>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"
			else	' DD FILE PAYMENT LISTING
				TableString = TableString & "<TR>" &_
					"<TD><font color=green>DD Date : " &  rsLoader("ACTUALDDDATE") & "</font></TD></TD>" &_
					"<TD ALIGN=CENTER>" & rsLoader("POCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("POSUM")) & "</TD>" &_
					"<TD ALIGN=CENTER>" & rsLoader("CRCNT") & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("CRSUM")) & "</TD>" &_
					"<TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD>" &_
					"<TD ALIGN=RIGHT>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"
				
				'TableString = TableString & "<TR><TD><font color=green>DD Date : " &  rsLoader("ACTUALDDDATE") & "</font></TD><TD>" & rsLoader("INVOICECOUNT") & "</TD><TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD><TD>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"		
			end if		
			rsLoader.moveNext	
		wend
		TableString = TableString & "<TR><TD colspan=5 align=right style='border:none'>Total :</TD><TD bgcolor=gray align=right><b><font color=white>" & FormatCurrency(totalvalue) & "</font></b></TD></TR>"
	else
		TableString = "<TR><TD colspan=7 align=center>No data found for the specified date</TD></TR>"
	end if
	Call CloseRs(rsLoader)
	
	SQL = "SELECT BD.DATAID, FILENAME, INVOICECOUNT, TOTALVALUE, BD.PROCESSDATE, ISFILE FROM F_BACSDATA BD " &_
				"WHERE BD.PROCESSDATE = '" & FormatDateTime(RequiredDate,1) & "' AND BD.FILETYPE = 2 " & CompanyFilter &_
			"ORDER BY FILETYPE ASC, BD.DATAID DESC"
	bADTableString = ""
	totalvalue = 0
	Call OpenRs(rsLoader, SQL)
	if (NOT rsLoader.EOF) then
		while (NOT rsLoader.EOF) 
			totalvalue = totalvalue + CDBL(rsLoader("TOTALVALUE"))
			bADTableString = bADTableString & "<TR><TD><a target=""_blank"" href='/BacsFile/" & rsLoader("FILENAME") & "'><font color=blue>" & rsLoader("FILENAME") & "</font></a></TD><TD>" & rsLoader("INVOICECOUNT") & "</TD><TD align=right>" & FormatCurrency(rsLoader("TOTALVALUE")) & "</TD><TD>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</TD></TR>"
			rsLoader.moveNext	
		wend
		bADTableString = bADTableString & "<TR><TD colspan=5 align=right style='border:none'>Total :</TD><TD bgcolor=gray align=right><b><font color=white>" & FormatCurrency(totalvalue) & "</font></b></TD></TR>"
	else
		bADTableString = ""
	end if
	
	Call CloseRs(rsLoader)
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Payments Data List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    You will find information regarding payments that have been made below.<br/>
    To open a bacs file directly just click on the link. To save any particular bacs
    file right click on it and click 'save as'. Any bacs files which have a BAD suffix
    are files which contain invalid data that was taken from the original uploaded file.<br/>
    <br/>
    The data you are viewing is for the Date: <b>
        <%=FormatDateTime(RequiredDate,1)%></b>
    <br/>
    <br/>
    <table style='border-collapse: collapse' border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td width="250">
                <b>File Name/Cheque Number /DD Taken</b>
            </td>
            <td width="45">
                <b>Invoices</b>
            </td>
            <td width="100" align="right">
                <b>Invoice Value</b>
            </td>
            <td width="55" align="center">
                <b>Credits</b>
            </td>
            <td width="100" align="right">
                <b>Credit Value</b>
            </td>
            <td width="100" align="right">
                <b>Total Value</b>
            </td>
            <td width="80" align="right">
                <b>Process Date</b>
            </td>
        </tr>
        <%=TableString%>
    </table>
    <br/>
    <br/>
    <% IF bADTableString <> "" THEN %>
    <table style='border-collapse: collapse; color: red' border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td width="250">
                <b>Bad File Name</b>
            </td>
            <td width="150">
                <b>INVOICE Count</b>
            </td>
            <td width="100" align="right">
                <b>Total Value</b>
            </td>
            <td nowrap>
                <b>Process Date</b>
            </td>
        </tr>
        <%=bADTableString%>
    </table>
    <% end if %>
    <br/>
    <br/>
    &nbsp<a href='BacsDisplay.asp?date=<%=ProcessingDate%>&Company=<%=Company %>'><font color="blue"><b>BACK to
        Bacs Calendar</b></font></a>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
