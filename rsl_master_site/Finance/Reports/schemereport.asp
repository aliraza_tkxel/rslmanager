<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>  

<!--#include virtual="ACCESSCHECK.asp" -->
<% 'Setup Request Variables

	dim FY, GP
	FY = Request("FY")
	GP = Request("GP")
	if FY="" then FY = 8 'If nothing passed back then use previous fiscal year
	if GP="" then GP = 1 'If nothing passed back then use group by development

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>RSL Manager Finance --&gt; Scheme Report</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
        /* Page Specific Styles */
		.totalRow {
			font-weight: bold;
			display: block;
		}
		tr.totalRow td {
			border-bottom: solid 1px #f0f0f0;
		}

		tr.totalRow#GrandTotal td {
			border-bottom: solid 1px #f0f0f0;
			background-color: #f0f0f0;
		}

		.currency {
			text-align: right;
		}

		.detailrow {
			display: none;
		}

		.highlightedTotal {
			background-color: #ff0000;
		}

		div#filterDiv {
			background-color: #f0f0f0;
			width: 100%;
			border: solid 1px #000000;
		}
			div#filterDiv table td {
				margin: 0px;
				vertical-align: center;
				/* width: 12.5%; 
				*/
				height: 25px;
				padding-left: 10px;
			}
        
		thead {
			font-weight: bold;
			font-color: steelblue;
		}
    </style>
<script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
	var FormFields = new Array();
	var devArray = new Array();		// To help us with searching later

	var showDetailFlag = true;

	//document.getElementById("test").className+=" class2"
	function showDetail(devID){
		if (showDetailFlag == true){
			showDetailFlag = false;
			for(i = 0; i < document.all("DEV_" + devID).length; i++){
				if (document.all("DEV_" + devID)(i).style.display == "block") {
					setTimeout( 'document.all("DEV_' + devID + '")(' + i + ').style.display = "none";' , i * 20);
				} else {
					setTimeout( 'document.all("DEV_' + devID + '")(' + i + ').style.display = "block";' , i * 20);
				}
			}
			setTimeout( 'showDetailFlag = true;' , document.all("DEV_" + devID).length * 20);
		}
	}

	function search(searchText){
		if (event.keyCode != 13) return(false);

		var devLength = document.all.namedItem("DEV_TOTAL").length;
		for (k=0; k < devLength; k++){ 
			window.status = "Checking " + k + " of " + devLength;
			if (searchText == ""){
				setTimeout( 'document.all.namedItem("DEV_TOTAL")(' + k + ').style.display = "block";' , (k * 15) + 1000);
			} else {
				if (document.all.namedItem("DEV_TOTAL")(k).cells(0).innerHTML.indexOf(searchText) >= 0){
					document.all.namedItem("DEV_TOTAL")(k).onclick();
				} else {
					setTimeout( 'document.all.namedItem("DEV_TOTAL")(' + k + ').style.display = "none";' , (k * 15) + 1000);
				}		
			}
		}
		window.status = "Search complete";

	}

	function reloadPage() {
		window.location.href="SchemeReport.asp?FY=" + sel_FY.value + "&GP=" + sel_GP.value + "";
	}

	function exportReport() {

	}

	function printReport() {

	}

// -->
</script> 
</head> 
<body onload="initSwipeMenu(1);"  marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
	<% Call OpenDB() 
	   Call BuildSelect(lstFY, "sel_FY", "F_FISCALYEARS", "YRANGE, CAST(YEAR(YSTART) AS VARCHAR) + ' - ' + CAST(YEAR(YEND) AS VARCHAR) AS FY", "YRANGE", "Please Select...", FY, NULL, "textbox100", " onchange=""reloadPage();""")	
	%>

	<div id="filterDiv">
		<table cellspacing="0">
			<tr>
				<td>Fiscal Year</td><td><%=lstFY%></td>
				<td>Search</td><td><input type="text" class="textbox100" value="" onkeyup="search(this.value);"></td>
				<td>Grouped By</td><td><select name="sel_GP" class="textbox100" onchange="reloadPage();"><option value="1" <% if GP=1 then rw "Selected" %>>Development</option><option value="2" <% if GP=2 then rw "Selected" %>>Account</option></select></td>
				<td><!--<a href="#" onclick="exportReport();">Export</a>-->&nbsp;</td>
				<td><!--<a href="#" onclick="printReport();">Print</a>-->&nbsp;</td>
			</tr>
		</table>
	</div>
	<table id="dataTable" width="100%" style="behavior:url(/Includes/Tables/tablehl.htc);" slcolor="" hlcolor="steelblue">
	<thead><td>&nbsp;</td><td>&nbsp;</td><td width="15%" class="currency">Debit</td><td width="15%" class="currency">Credit</td></thead>
<%
	Dim rsSet
	Call OpenRS(rsSet, "EXEC F_SCHEME_REPORT @FISCALYEAR=" & FY & ", @GROUPEDBY=" & GP)

	Dim DevTotalDB, DevTotalCR
	DevTotalDB = 0 : DevTotalCR = 0

	if GP = 1 then 
		Dim DevTotalName, DevID

		Do While Not rsSet.EOF 

        DevID = rsSet("DEVELOPMENTID")
		DevTotalName = rsSet("DEVELOPMENTNAME")

			rw  "   <tr class=""detailrow"" id=""DEV_" & rsSet("DEVELOPMENTID") & """ onclick=""showDetail(" & DevID & ");"">" & vbCrLf
			rw  "       <td>" & rsSet("DEVELOPMENTNAME") & "</td>" & vbCrLf
			rw	"       <td>" & rsSet("ACCOUNTNUMBER") & "</td>" & vbCrLf
			rw	"       <td class=""currency"">" & FormatCurrency(rsSet("DEBIT")) & "</td>" & vbCrLf
			rw	"       <td class=""currency"">" & FormatCurrency(rsSet("CREDIT")) & "</td>" & vbCrLf
			rw  "   </tr>" & vbCrLf

			DevTotalDB = DevTotalDB + rsSet("DEBIT")
			DevTotalCR = DevTotalCR + rsSet("CREDIT")

			rsSet.MoveNext()

			if not(rsSet.EOF) then 
				if rsSet("DEVELOPMENTID") <> DevID then
					'Total Row!
					rw  "   <tr class=""totalRow"" id=""DEV_TOTAL"" onclick=""showDetail(" & DevID & ");"">" & vbCrLf
					rw  "       <td>" & DevTotalName & "</td>" & vbCrLf
					rw	"       <td>&nbsp;</td>" & vbCrLf
					rw	"       <td class=""currency"">" & FormatCurrency(DevTotalDB) & "</td>" & vbCrLf
					rw	"       <td class=""currency"">" & FormatCurrency(DevTotalCR) & "</td>" & vbCrLf
					rw  "   </tr>" & vbCrLf

					DevTotalDB = 0 : DevTotalCR = 0
					DevID = rsSet("DEVELOPMENTID")
					DevTotalName = rsSet("DEVELOPMENTNAME")
				end if
			end if

		Loop
	elseif GP = 2 then
		Dim AccTotalName, AccID
		AccID = rsSet("ACCOUNTNUMBER")
		AccTotalName = rsSet("ACCOUNTNUMBER")

		Do While Not rsSet.EOF 

			rw  "   <tr class=""detailrow"" id=""DEV_" & rsSet("ACCOUNTNUMBER") & """ onclick=""showDetail(" & AccID & ");"">" & vbCrLf
			rw  "       <td>" & rsSet("ACCOUNTNUMBER") & "</td>" & vbCrLf
			rw	"       <td>" & rsSet("DEVELOPMENTNAME") & "</td>" & vbCrLf
			rw	"       <td class=""currency"">" & FormatCurrency(rsSet("DEBIT")) & "</td>" & vbCrLf
			rw	"       <td class=""currency"">" & FormatCurrency(rsSet("CREDIT")) & "</td>" & vbCrLf
			rw  "   </tr>" & vbCrLf

			DevTotalDB = DevTotalDB + rsSet("DEBIT")
			DevTotalCR = DevTotalCR + rsSet("CREDIT")

			rsSet.MoveNext()

			if not(rsSet.EOF) then 
				if rsSet("ACCOUNTNUMBER") <> AccID then
					'Total Row!
					rw  "   <tr class=""totalRow"" id=""DEV_TOTAL"" onclick=""showDetail(" & AccID & ");"">" & vbCrLf
					rw  "       <td>" & AccTotalName & "</td>" & vbCrLf
					rw	"       <td>&nbsp;</td>" & vbCrLf
					rw	"       <td class=""currency"">" & FormatCurrency(DevTotalDB) & "</td>" & vbCrLf
					rw	"       <td class=""currency"">" & FormatCurrency(DevTotalCR) & "</td>" & vbCrLf
					rw  "   </tr>" & vbCrLf

					DevTotalDB = 0 : DevTotalCR = 0
					AccID = rsSet("ACCOUNTNUMBER")
					AccTotalName = rsSet("ACCOUNTNUMBER")
				end if
			end if

		Loop

	end if

	Call CloseRS(rsSet)
	Call CloseDB()
%> 
	</table> 

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
