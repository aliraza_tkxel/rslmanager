<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	set rsFYear = Server.CreateObject("ADODB.Recordset")
	rsFYear.ActiveConnection = RSL_CONNECTION_STRING
	if (Request("sel_PERIOD") <> "") then
		rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YRANGE = " & REQUEST("sel_PERIOD")
		rsFYear.CursorType = 0
		rsFYear.CursorLocation = 2
		rsFYear.LockType = 3
		rsFYear.Open()
		if (NOT rsFYear.EOF) then
			YearRange = rsFYear("YRange")
			StartDate = rsFYear("YStart")
			EndDate = rsFYear("YEnd")		
		end if
		rsFYear.close
	end if
	
	if (Request("sel_PERIOD") = "") then
		DATESQL = ""
	Else
		PS = CDate(FormatDateTime(StartDate,1)) & " 00:00 "
		PE = CDate(FormatDateTime(Enddate,1)) & " 23:59 "
		PB_DATESQL = " AND PB.PROCESSDATE >= '" & PS & "' AND PB.PROCESSDATE <= '" & PE & "' "
		INV_DATESQL = " AND INV.TAXDATE >= '" & PS & "' AND INV.TAXDATE <= '" & PE & "' "
	End If


	Dim CONST_PAGESIZE
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("SEARCHNAME") = "" Then 
		PSQL = "" 
	Else
		PSQL = " where O.NAME LIKE '%" & Request("SEARCHNAME") & "%' "
	End If

	If (Request("RDO_WHICH") = "" or Request("RDO_WHICH") = 1) then
		rdowhich2 = ""			
		rdowhich1 = " checked"
		rdowhich = 1
		ExtraSQL = ""
	else
		rdowhich2 = " checked"
		rdowhich1 = ""
		rdowhich = 2		
		IF PSQL = "" THEN
			ExtraSQL = " where ISNULL(TR,0) - ISNULL(TP,0) + isnull(CRECEIVED,0) + isnull(CPAID,0) <> 0 "
		else
			ExtraSQL = " and ISNULL(TR,0) - ISNULL(TP,0) + isnull(CRECEIVED,0) + isnull(CPAID,0) <> 0 "
		end if		
	end if
	
	'+ ISNULL(TC,0)
	
	PageName = "SUPPLIERBALANCE.bak.asp"
	MaxRowSpan = 4
	OpenDB()
	get_tenant_balance()
	balance = get_balance()
	CloseDB()

	Function get_balance()
	
		SQL = "SELECT " &_
			"isnull((SELECT SUM(ROUND(PI.GROSSCOST,2)) FROM F_INVOICE INV " &_
			"INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.INVOICEID = INV.INVOICEID " &_
			"INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = FOTI.ORDERITEMID " &_
			"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_ 					
			"WHERE iscredit=0 and CONFIRMED= 1 " & PB_DATESQL & " AND PAYMENTMETHOD IS NOT NULL),0)" &_
			"-" &_
			"isnull((SELECT SUM(ROUND(PI.GROSSCOST,2)) FROM F_PURCHASEITEM PI " &_
			"	INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = PI.ORDERITEMID " &_
			"	INNER JOIN F_INVOICE INV ON INV.INVOICEID = FOTI.INVOICEID " &_
			"WHERE iscredit=0 and PISTATUS >= 9 " & INV_DATESQL & " ),0)" &_
			"+" &_
			"		isnull((SELECT SUM(ROUND(PI.TOTALCOST,2)) FROM F_CREDITNOTE C " &_
			"			INNER JOIN ( " &_
			"			SELECT SUM(GROSSCOST) AS TOTALCOST, CNID " &_
			"			FROM F_PURCHASEITEM F " &_
			"				INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID " &_
			"			GROUP BY CNID) PI ON C.CNID = PI.CNID " &_
			"		WHERE C.ACTIVE = 1 AND C.CNSTATUS = 13),0) " 

		' changed by paul
		SQL = "SELECT " &_
				"(SELECT 	ISNULL(SUM(PI.GROSSCOST),0) AS TR  " &_
				"FROM 	F_PURCHASEITEM PI " &_
				"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID " &_
				"	INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = PI.ORDERITEMID " &_
				"	INNER JOIN F_INVOICE INV ON INV.INVOICEID = FOTI.INVOICEID " &_
				"WHERE 	ISCREDIT = 0 AND PISTATUS >= 9 AND PI.ACTIVE = 1 " &_
				")- " &_
				"(SELECT 	ISNULL(SUM(GROSSCOST),0) AS TP FROM F_INVOICE INV " &_
				"	INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
				"WHERE 	ISCREDIT=0 and CONFIRMED = 1 AND PAYMENTMETHOD IS NOT NULL " &_
				")+" &_
				"(SELECT 	ISNULL(SUM(PI.TOTALCOST),0) AS TC FROM F_CREDITNOTE C  " &_
				"	INNER JOIN (  " &_
				"SELECT 	SUM(GROSSCOST) AS TOTALCOST, CNID  " &_
				"FROM 	F_PURCHASEITEM F  " &_
				"	 	INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID  " &_
				"		GROUP 	BY CNID) PI ON C.CNID = PI.CNID  " &_
				"	WHERE 	C.ACTIVE = 1 )" &_
				"+" &_
				"(SELECT 	ABS(ISNULL(SUM(PI.TOTALCOST),0)) AS TC FROM F_CREDITNOTE C  " &_
				"	INNER JOIN (  " &_
				"SELECT 	SUM(GROSSCOST) AS TOTALCOST, CNID  " &_
				"FROM 	F_PURCHASEITEM F  " &_
				"	 	INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID  " &_
				"		GROUP 	BY CNID) PI ON C.CNID = PI.CNID  " &_
				"	WHERE 	C.ACTIVE = 1 AND C.CNSTATUS = 13	)"


' ADD THIS LINE IF YOU NEED TO INCLUDE THE OPENING BALANCE FROM THE NOMINAL ON THE TOTAL BALANCE			
' "+" &_
' "ISNULL((SELECT BALANCE FROM NL_OPENINGBALANCES WHERE YRANGE = " &  YearRange & " AND ACCOUNTID = (SELECT ACCOUNTID FROM NL_ACCOUNT WHERE ACCOUNTNUMBER = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'PURCHASECONTROLACCOUNTNUMBER'))),0) "
		'RW SQLX
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then get_balance = rsSet(0) Else get_balance = 0 End If
		CloseRs(rsSet)
	
	End Function

	Function get_tenant_balance()

		Dim orderBy, strSQL, rsSet, intRecord 
		orderBy = " O.NAME  "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
		
		intRecord = 0
		str_data = ""
		strSQL =   "SELECT O.ORGID, O.NAME, ISNULL(TR,0) - ISNULL(TP,0) + isnull(CRECEIVED,0) + isnull(CPAID,0) AS BALANCE, ISNULL(NEXTPAY.NEXTPAYDATE,'-') AS NEXTPAYDATE FROM S_ORGANISATION O " &_ 
				"LEFT JOIN " &_
					"(SELECT ISNULL(SUM(GROSSCOST),0) AS TP, SUPPLIERID FROM F_INVOICE INV " &_
					"	INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_ 					
					"WHERE ISCREDIT=0 and CONFIRMED = 1 " & PB_DATESQL & " AND PAYMENTMETHOD IS NOT NULL GROUP BY SUPPLIERID) PAYMENTS ON PAYMENTS.SUPPLIERID = O.ORGID " &_ 
				"LEFT JOIN " &_  
					"(SELECT ISNULL(SUM(PI.GROSSCOST),0) AS TR, PO.SUPPLIERID FROM F_PURCHASEITEM PI " &_  
					"	INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID " &_ 
					"	INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = PI.ORDERITEMID " &_  
					"	INNER JOIN F_INVOICE INV ON INV.INVOICEID = FOTI.INVOICEID " &_ 
					"WHERE ISCREDIT=0 AND PISTATUS >= 9 " & INV_DATESQL & " GROUP BY PO.SUPPLIERID) RECEIPTS ON RECEIPTS.SUPPLIERID = O.ORGID " &_ 
				"LEFT JOIN  " &_ 
					"	( " &_ 
					"	SELECT INV.SUPPLIERID, " &_ 
					"	CASE WHEN CONVERT(SMALLDATETIME, CONVERT(VARCHAR, GETDATE(), 103), 103) >= DATEADD(DAY, ISNULL(O.PAYMENTTERMS,0), MIN(INV.TAXDATE)) " &_
					"	THEN 'Due a Payment' ELSE CONVERT(VARCHAR, DATEADD(DAY, ISNULL(O.PAYMENTTERMS,0), MAX(INV.TAXDATE)), 103) END AS NEXTPAYDATE FROM F_INVOICE INV " &_
					"	INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID " &_
					"	LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
					"	WHERE CONFIRMED =1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL " &_
					"	GROUP BY INV.SUPPLIERID, O.PAYMENTTERMS " &_
					"	) NEXTPAY ON NEXTPAY.SUPPLIERID = O.ORGID " &_
					"LEFT JOIN " &_
					"		(SELECT ISNULL(SUM(PI.TOTALCOST),0) AS CRECEIVED, C.SUPPLIERID FROM F_CREDITNOTE C " &_
					"			INNER JOIN ( " &_
					"			SELECT SUM(GROSSCOST) AS TOTALCOST, CNID " &_
					"			FROM F_PURCHASEITEM F " &_
					"				INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID " &_
					"			GROUP BY CNID) PI ON C.CNID = PI.CNID " &_
					"		WHERE C.ACTIVE = 1 " &_
					"		GROUP BY C.SUPPLIERID) CREDITSR ON CREDITSR.SUPPLIERID = O.ORGID " &_
					"LEFT JOIN " &_
					"		(SELECT ABS(ISNULL(SUM(PI.TOTALCOST),0)) AS CPAID, C.SUPPLIERID FROM F_CREDITNOTE C " &_
					"			INNER JOIN ( " &_
					"			SELECT SUM(GROSSCOST) AS TOTALCOST, CNID " &_
					"			FROM F_PURCHASEITEM F " &_
					"				INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID " &_
					"			GROUP BY CNID) PI ON C.CNID = PI.CNID " &_
					"		WHERE C.ACTIVE = 1 AND C.CNSTATUS = 13 " &_
					"		GROUP BY C.SUPPLIERID) CREDITSP ON CREDITSP.SUPPLIERID = O.ORGID " & PSQL & ExtraSQL &_
					" ORDER BY O.NAME "
			
			'rw strSQL & "<BR>"
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				'rw intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				For intRecord = 1 to rsSet.PageSize
					NextPay = rsSet("NEXTPAYDATE")
					
						TotalTitle = ""
						if (NextPay = "-") then
							TotalPayable = "-"
							TotalTitle = ""						
						elseif (NextPay = "Due a Payment") then
							SQL = "SELECT isnull(SUM(GROSSCOST),0) AS TOTALPAYABLE, COUNT(*) AS TOTALCOUNT FROM F_INVOICE INV " &_
									"	INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID " &_
									"	LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
									"	WHERE CONFIRMED =1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL " &_
									" AND DATEADD(DAY, ISNULL(O.PAYMENTTERMS,0), INV.TAXDATE)  <= CONVERT(SMALLDATETIME, CONVERT(VARCHAR, GETDATE(), 103), 103) " &_
									" AND INV.SUPPLIERID = " & rsSet("ORGID")
							set rsSet2 = Server.CreateObject("ADODB.Recordset")
							rsSet2.ActiveConnection = RSL_CONNECTION_STRING 			
							rsSet2.Source = SQL
							rsSet2.CursorType = 3
							rsSet2.CursorLocation = 3
							rsSet2.Open()
							IF (not rsSet2.EOF) then
								TotalPayable = FormatNumber(rsSet2("TOTALPAYABLE"),2)
								TotalTitle = " title='" & rsSet2("TOTALCOUNT") & " invoices(s) '"
							end if
							rsSet2.close()
							Set rsSet2 = Nothing
						else
							SQL = "SELECT isnull(SUM(GROSSCOST),0) AS TOTALPAYABLE, COUNT(*) AS TOTALCOUNT FROM F_INVOICE INV " &_
									"	INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID " &_
									"	LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
									"	WHERE CONFIRMED =1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL " &_
									" AND DATEADD(DAY, ISNULL(O.PAYMENTTERMS,0), INV.TAXDATE)  <= '" & FormatDateTime(NextPay) & "' " &_
									" AND INV.SUPPLIERID = " & rsSet("ORGID")
							set rsSet2 = Server.CreateObject("ADODB.Recordset")
							rsSet2.ActiveConnection = RSL_CONNECTION_STRING 			
							rsSet2.Source = SQL
							rsSet2.CursorType = 3
							rsSet2.CursorLocation = 3
							rsSet2.Open()
							IF (not rsSet2.EOF) then
								TotalPayable = FormatNumber(rsSet2("TOTALPAYABLE"),2)
								TotalTitle = " title='" & rsSet2("TOTALCOUNT") & " invoices(s) '"
							end if
							rsSet2.close()
							Set rsSet2 = Nothing
						end if						
							
					str_data = str_data & "<TR><TD>" & rsSet("NAME") & "</TD>" &_
									"<TD>" & NextPay & "</TD>" &_
									"<TD align=right " & TotalTitle & ">" & TotalPayable & "</TD>" &_									
									"<TD ALIGN=RIGHT>" & FORMATNUMBER(rsSet("BALANCE"),2) & "</TD>" &_
									"</TR>"
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				str_data = str_data & "</TBODY>"

				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1&SEARCHNAME="&REQUEST("SEARCHNAME")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "&RDO_WHICH=" & rdowhich& "'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "&SEARCHNAME="&REQUEST("SEARCHNAME")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "&RDO_WHICH=" & rdowhich& "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "&SEARCHNAME="&REQUEST("SEARCHNAME")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "&RDO_WHICH=" & rdowhich& "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "&SEARCHNAME="&REQUEST("SEARCHNAME")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "&RDO_WHICH=" & rdowhich& "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
					
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Supplier Balances</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	FormFields[0] = "txt_NAME|Supplier|TEXT|N"
	FormFields[1] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function JumpPage(){
		if (document.getElementsByName("RDO_WHICH")[0].checked)
			rdowhich = 1
		else
			rdowhich = 2
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "SUPPLIERBALANCE.ASP?SEARCHNAME="+document.getElementById("txt_NAME").value+"&page="+iPage+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value+"&RDO_WHICH="+rdowhich
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function click_go(){
		if (document.getElementsByName("RDO_WHICH")[0].checked)
			rdowhich = 1
		else
			rdowhich = 2
		if (!checkForm()) return false;
		location.href = "SUPPLIERBALANCE.bak.ASP?SEARCHNAME="+document.getElementById("txt_NAME").value+"&page=1&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value+"&RDO_WHICH="+rdowhich
		}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=RSLFORM method=post>

  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
	<TR>
	<TD COLSPAN=6 CLASS='TABLE_HEAD'>
		<b>FISCAL YEAR</b>: 
		<SELECT NAME="sel_PERIOD" CLASS="textbox" STYLE='WIDTH:170PX'>
			<OPTION VALUE="" SELECTED>All</OPTION>
    <%
		OpenDB()
		SQL = "SELECT * FROM F_FISCALYEARS ORDER BY YRANGE"
		Call OpenRs(rsTheYears, SQL)
		While (NOT rsTheYears.EOF)
			isSelected = ""
			if Request("sel_PERIOD") <> "" THen
				If Cint(Request("sel_PERIOD")) = CInt(rsTheYears.Fields.Item("YRANGE").Value) Then
					isSelected = " SELECTED "
				Else
					isSelected = ""
				End IF
			End If			
	%>
     <OPTION VALUE="<%=(rsTheYears.Fields.Item("YRANGE").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YSTART").Value) & " - " & (rsTheYears.Fields.Item("YEND").Value)%></OPTION>
    <%
	  		rsTheYears.MoveNext()
		Wend
	%>
   </SELECT>
	<span title='Enter any supplier name to search for'><B>&nbsp;Supplier&nbsp;&nbsp;
	<input type="textbox" class="textbox" name="txt_NAME" maxlength=20 size=20 tabindex=3 value='<%=Request("SEARCHNAME")%>'>
	<image src="/js/FVS.gif" name="img_NAME" width="15px" height="15px" border="0"></B></span>
	ALL<INPUT TYPE=RADIO VALUE=1 NAME=RDO_WHICH <%=rdowhich1%>>
	With Balance<INPUT TYPE=RADIO VALUE=2 NAME=RDO_WHICH <%=rdowhich2%>>	
	Pagesize&nbsp;
	<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
	<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
	<input type="button" id="BTN_GO" name"BTN_GO" value=" GO " class=rslbutton onClick="click_go()">
	</TD>
	</TR> 
    <TR> 
      <TD WIDTH=450PX>&nbsp;<B>Supplier</B></TD>
      <TD WIDTH=150PX>&nbsp;<B>Next Pay Date</B>&nbsp;</TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Total Payable</B>&nbsp;</TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Balance</B>&nbsp;</TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=10 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
        <TR style='background-color:white'><TD style='background-color:white;color:black' COLSPAN=4 ALIGN=RIGHT><b>Balance : <%=FormatCurrency(balance)%></b></TD></TR>
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>