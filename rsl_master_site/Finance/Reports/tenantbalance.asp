<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	set rsFYear = Server.CreateObject("ADODB.Recordset")
	rsFYear.ActiveConnection = RSL_CONNECTION_STRING
	if (Request("sel_PERIOD") <> "") then
		rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YRANGE = " & REQUEST("sel_PERIOD")
		rsFYear.CursorType = 0
		rsFYear.CursorLocation = 2
		rsFYear.LockType = 3
		rsFYear.Open()
		if (NOT rsFYear.EOF) then
			YearRange = rsFYear("YRange")
			StartDate = rsFYear("YStart")
			EndDate = rsFYear("YEnd")		
		end if
		rsFYear.close
	end if
	
	if (Request("sel_PERIOD") = "") then
		DATESQL = ""
	Else
		PS = CDate(FormatDateTime(StartDate,1))
		PE = CDate(FormatDateTime(Enddate,1))
		DATESQL = "AND TRANSACTIONDATE <= '" & PE & "' "
	End If

	Dim CONST_PAGESIZE
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan, balance

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 20 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	If Request("SEARCHPROPERTY") = "" Then 
		PSQL = "" 
	Else
		PSQL = " AND T.PROPERTYID LIKE '%" & Request("SEARCHPROPERTY") & "%' "
	End If
	If Request("SEARCHTENANCY") = "" Then 
		TSQL = "" 
	Else
		TSQL = " AND R.TENANCYID = " & Request("SEARCHTENANCY") & " "
	End If
	PageName = "TENANTBALANCE.asp"
	MaxRowSpan = 4
	OpenDB()
	get_tenant_balance()
	balance = get_balance()
	CloseDB()

	Function get_balance()
	
		SQL = "SELECT ISNULL(SUM(R.AMOUNT),0) AS BALANCE " &_
					"FROM	F_RENTJOURNAL R " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID = R.TENANCYID " &_
					"WHERE	R.ITEMTYPE IN (1,8,9,10) AND R.STATUSID NOT IN (1,4) " & TSQL & PSQL & DATESQL
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then get_balance = rsSet(0) Else get_balance = 0 End If
		CloseRs(rsSet)
	
	End Function

	Function get_tenant_balance()

		Dim orderBy, strSQL, rsSet, intRecord 
		orderBy = " R.TENANCYID  "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
		
		intRecord = 0
		str_data = ""
		strSQL =   "SELECT 	R.TENANCYID, T.PROPERTYID, ISNULL(SUM(R.AMOUNT),0) AS BALANCE " &_
					"FROM	F_RENTJOURNAL R " &_
					"		INNER JOIN C_TENANCY T ON T.TENANCYID = R.TENANCYID " &_
					"WHERE	R.ITEMTYPE IN (1,8,9,10) AND R.STATUSID NOT IN (1,4) " & TSQL & PSQL & DATESQL &_
					"GROUP 	BY R.TENANCYID, PROPERTYID " &_
					"ORDER BY " & orderBy
		'response.write strSQL

			'rw strSQL & "<BR>"
			set rsSet = Server.CreateObject("ADODB.Recordset")
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			intPageCount = rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
	
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then
				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			If intRecordCount > 0 Then
			' Display the record that you are starting on and the record
			' that you are finishing on for this page by writing out the
			' values in the intStart and the intFinish variables.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
				' Iterate through the recordset until we reach the end of the page
				' or the last record in the recordset.
				For intRecord = 1 to rsSet.PageSize
					TenancyID = rsSet("TENANCYID")
					CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
							"FROM C_CUSTOMERTENANCY CT " &_
							"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
							"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
							"WHERE CT.TENANCYID = " & TenancyID
						
					Call OpenRs(rsCust, CustSQL)
					strUsers = ""
					CustCount = 0
					while NOT rsCust.EOF 
						if CustCount = 0 then
							strUsers = strUsers & "<SPAN STYLE='CURSOR:HAND' ONCLICK=""location.href='/customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'"">" & rsCust("FULLNAME") & "</SPAN>"
							CustCount = 1
						else
							strUsers = strUsers & ", " & "<SPAN STYLE='CURSOR:HAND' ONCLICK=""location.href='/customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'"">" & rsCust("FULLNAME") & "</SPAN>"
						end if					
						rsCust.moveNext()
					wend
					CloseRs(rsCust)
					Set rsCust = Nothing
	
					str_data = str_data & "<TR><TD>" & TenancyReference(rsSet("TENANCYID")) & "</TD>" &_
											"<TD>" & StrUsers & "</TD>" &_ 
											"<TD>" & rsSet("PROPERTYID") & "</TD>" &_
											"<TD ALIGN=RIGHT>" & FORMATNUMBER(rsSet("BALANCE"),2) & "</TD></TR>"
					count = count + 1
					rsSet.movenext()
					If rsSet.EOF Then Exit for
					
				Next
				str_data = str_data & "</TBODY>"

				'ensure table height is consistent with any amount of records
				fill_gaps()
				
				' links
				str_data = str_data &_
				"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
				"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1&SEARCHPROPERTY="&REQUEST("SEARCHPROPERTY")&"&SEARCHTENANCY="&REQUEST("SEARCHTENANCY")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>First</font></b></a> "  &_
				"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "&SEARCHPROPERTY="&REQUEST("SEARCHPROPERTY")&"&SEARCHTENANCY="&REQUEST("SEARCHTENANCY")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>Prev</font></b></a>"  &_
				" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "&SEARCHPROPERTY="&REQUEST("SEARCHPROPERTY")&"&SEARCHTENANCY="&REQUEST("SEARCHTENANCY")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
				" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "&SEARCHPROPERTY="&REQUEST("SEARCHPROPERTY")&"&SEARCHTENANCY="&REQUEST("SEARCHTENANCY")&"&PAGESIZE="&REQUEST("PAGESIZE")&"&sel_PERIOD=" & YearRange & "'><b><font color=BLUE>Last</font></b></a>"  &_
				"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
				"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
				"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
					
			rsSet.close()
			Set rsSet = Nothing
			
		End function
	
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Tenant Balances</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	FormFields[0] = "txt_PROPERTYID|PROPERTYID|TEXT|N"
	FormFields[1] = "txt_TENANCYID|TO|INTEGER|N"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function JumpPage(){
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "TENANTBALANCE.ASP?SEARCHPROPERTY="+document.getElementById("txt_PROPERTYID").value+"&SEARCHTENANCY="+document.getElementById("txt_TENANCYID").value+"&page="+iPage+"&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function click_go(){
		if (!checkForm()) return false;
		location.href = "TENANTBALANCE.ASP?SEARCHPROPERTY="+document.getElementById("txt_PROPERTYID").value+"&SEARCHTENANCY="+document.getElementById("txt_TENANCYID").value+"&page=1&PAGESIZE="+document.getElementById("txt_PAGESIZE").value+"&LID=<%=Request("LID")%>&sel_PERIOD="+document.getElementById("sel_PERIOD").value
		}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=RSLFORM method=post>

  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD>
	<TR>
	<TD COLSPAN=6 CLASS='TABLE_HEAD'>
		<b>FISCAL YEAR</b>: 
		<SELECT NAME="sel_PERIOD" CLASS="textbox" STYLE='WIDTH:170PX'>
			<OPTION VALUE="" SELECTED>All</OPTION>
    <%
		OpenDB()
		SQL = "SELECT * FROM F_FISCALYEARS ORDER BY YRANGE"
		Call OpenRs(rsTheYears, SQL)
		While (NOT rsTheYears.EOF)
			isSelected = ""
			if Request("sel_PERIOD") <> "" THen
				If Cint(Request("sel_PERIOD")) = CInt(rsTheYears.Fields.Item("YRANGE").Value) Then
					isSelected = " SELECTED "
				Else
					isSelected = ""
				End IF
			End If			
	%>
     <OPTION VALUE="<%=(rsTheYears.Fields.Item("YRANGE").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YSTART").Value) & " - " & (rsTheYears.Fields.Item("YEND").Value)%></OPTION>
    <%
	  		rsTheYears.MoveNext()
		Wend
	%>
   </SELECT>
	<span title='Enter any propertyid to search for'><B>&nbsp;Property&nbsp;&nbsp;
	<input type="textbox" class="textbox" name="txt_PROPERTYID" maxlength=12 size=12 tabindex=3 value='<%=Request("SEARCHPROPERTY")%>'>
	<image src="/js/FVS.gif" name="img_PROPERTYID" width="15px" height="15px" border="0"></B></span>
	<span title='Enter any tenancyid to search for'><B>&nbsp;Tenancy&nbsp;&nbsp;
	<input type="textbox" class="textbox" name="txt_TENANCYID" maxlength=10 size=10 tabindex=3 value='<%=Request("SEARCHTENANCY")%>'>
	<image src="/js/FVS.gif" name="img_TENANCYID" width="15px" height="15px" border="0"></B></span>
	&nbsp;&nbsp;Pagesize&nbsp;
	<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
	<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
	&nbsp;<input type="button" id="BTN_GO" name"BTN_GO" value=" GO " class=rslbutton onClick="click_go()">&nbsp;&nbsp;
	</TD>
	</TR> 
    <TR> 
      <TD WIDTH=100PX>&nbsp;<B>Tenancy</B></TD>
      <TD WIDTH=450PX>&nbsp;<B>Name</B>&nbsp;</TD>
      <TD WIDTH=100PX>&nbsp;<B>Property</B>&nbsp;</TD>
      <TD WIDTH=100PX ALIGN=RIGHT>&nbsp;<B>Balance</B>&nbsp;</TD>
	</TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=10 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
        <TR style='background-color:white'><TD style='background-color:white;color:black' COLSPAN=4 ALIGN=RIGHT><b>Balance : <%=FormatCurrency(balance)%></b></TD></TR>
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>