<%
	Function TopBit()
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" 
				
		for i=0 to Ubound(Headers)
			tWidth = "WIDTH=""" & HeaderWidths(i) & """"
			if (tWidth = "WIDTH=""""") then tWidth = ""
			if (SortOrder(i) <> "") then
				str_data = str_data &_
						"<TD " & tWidth & " height=20><B>" &_
						"<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " ASC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_up_" & SortOrderAsc(i) & ".gif"" border=""0"" alt=""Sort Ascending""></A>&nbsp;" &_
						Headers(i) &_
						"&nbsp;<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " DESC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_down_" & SortOrderDesc(i) & ".gif"" border=""0"" alt=""Sort Descending""></A>" &_
						"</B></TD>"  
			else
				str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" & Headers(i) & "</B></FONT></TD>" 
			end if
		next

		str_data = str_data & "</TR>"
	End Function
	
	Function BottomBit(ColSpan)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=10 WIDTH=100% ALIGN=CENTER valign=top><B>No matching records exist !!<input style='visibility:hidden' type='checkbox' name='dummyforheight'></B></TD></TR>"
			cnt = 1
		Else
			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=10 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE=' Validate Selected ' CLASS='rslbutton' ONCLICK='process()'>" &_
								"</TD></TR></TABLE>"
		End If

		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)
		
		str_data = str_data &_
			"<TR><TD COLSPAN=" & ColSpan & " align=right style='border-top:1px solid #133e71'><b>Total&nbsp;</b></TD>" &_
			"<TD style='border-top:1px solid #133e71'><INPUT READONLY style='text-align:right;font-weight:bold;border:none' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTALCREDIT CLASS='textbox100' VALUE='" & FormatNumber(detotal_credit,2) & "'></TD>" &_
			"<TD style='border-top:1px solid #133e71'><INPUT READONLY style='text-align:right;font-weight:bold;border:none' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTALDEBIT CLASS='textbox100' VALUE='" & FormatNumber(detotal_debit,2) & "'></TD>" &_
			"<td style='border-top:1px solid #133e71'>&nbsp;</td></TR>"

		str_data = str_data & "<tr bgcolor=black>" &_
			"	<td colspan=10 align=center height=22 valign=middle>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=black>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=1&" & Server.HTMLEncode(TheNextPage) & "')""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & prevpage & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & nextpage & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & numpages & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) TravelTo('page=' + document.getElementById('page').value + '&" & Server.HTMLEncode(TheNextPage) & "'); else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"
	End Function

	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=8 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function			
	
	Function SetSort()
		if (orderBy = "") then
			orderBy = SortOrder(0) & " ASC"
			SortOrderAsc(0) = "red"
		else
			Matched = false
			CompareOrderByArray = Split(orderBy, " ")
			CompareOrderBy = CompareOrderByArray(i)
			for i=0 to Ubound(SortOrder)
				RW CompareOrderBy & " -- " & SortOrder(i) & "<BR>"
				if (CompareOrderBy = SortOrder(i)) then
					if (CompareOrderBy & " ASC" = orderBy) then
						SortOrderAsc(i) = "red"
					else
						SortOrderDesc(i) = "red"
					end if
					Matched = true
					Exit For
				end if
			next
			if Matched = false then
				orderBy = SortOrder(0) & " ASC"
				SortOrderAsc(0) = "red"					
			end if
		end if
	End Function	
%>			