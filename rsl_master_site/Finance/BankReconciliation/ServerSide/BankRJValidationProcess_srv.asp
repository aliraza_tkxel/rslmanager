<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("STATEMENTID")
	
	OpenDB()

	strSQL = 	"SET NOCOUNT ON;" &_	
		"INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
		"VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
		"SELECT SCOPE_IDENTITY() AS BTID;"

	set rsSet = Conn.Execute(strSQL)
	bt_id = rsSet.fields("BTID").value
	rsSet.close()
	set rsSet = Nothing 
	
	sel_split = Split(insert_list ,",") ' split selected string
	rec_type = 1 'This means these items are from the rent journal
	TimeStamp = Now()
	method = 1 'Manual Validation
	
	For each key in sel_split
		
		strSQL = "DECLARE @GROUPINGID INT; " &_
			  "INSERT INTO F_BANK_GROUPING (RANDOM) VALUES (1); SET @GROUPINGID = SCOPE_IDENTITY(); " &_
			  "INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD, GROUPINGID) " &_
					"SELECT " & bt_id & ", " & bank_statement & ", " & rec_type & ", RJ.JOURNALID, '" & TimeStamp & "'," & Session("USERID") & "," & method & ", @GROUPINGID FROM F_RENTJOURNAL RJ " &_
						"WHERE JOURNALID = " & key & " AND NOT EXISTS " &_
						"	(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " 
		rw STRsql
		Conn.Execute(strSQL)
		
	Next

	CloseDB()

	Response.Redirect "BankRJValidation_svr.asp?RESET=1&Date=" & Request("txt_STATEMENTDATE") & "&STATEMENTID=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") & "&txt_TENANCYID=" & Request("txt_TENANCYID")& "&sel_LA=" & Request("sel_LA") & "&sel_PAYMENTTYPE=" & Request("sel_PAYMENTTYPE")	
%>
