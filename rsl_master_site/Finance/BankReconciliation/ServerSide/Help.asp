<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<script language=javascript>
function ReturnData(){
	parent.MainDiv.innerHTML = HelpDiv.innerHTML
	}
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="ReturnData()">
<div id="HelpDiv">
<div style='position:relative;height:280px;overflow:auto;border:1px solid gray;background-color:beige'>
<br>
<ul>
<li><a href="#calendar"><font color=blue>1. Calendar</font></a>
<li><a href="#Statement"><font color=blue>2. Statement</font></a>
<li><a href="#reconcile"><font color=blue>3. Reconciliation</font></a>
</ul>
<blockquote>
<a name=calendar><b>CALENDAR</b></a><br>The calendar allows you to create new statements and to load saved statements to reconcile items against. To create a new statement click on the appropriate day with a white coloured background. This will change the 'Statement Information' display, where you can enter the new statement details. Only the opening balance and closing balance is required. Once these have been entered click 'SAVE' to save the item.
<br><br>The calendar is colour coded as follows:
<ul>
	<li><span style='background-color:white;border:1px solid black'>&nbsp;&nbsp;&nbsp;&nbsp;</span> No statement exists.
	<li><span style='background-color:orange;border:1px solid black'>&nbsp;&nbsp;&nbsp;&nbsp;</span> represents dates which have a statement but have no items validated against them.
	<li><span style='background-color:red;border:1px solid black'>&nbsp;&nbsp;&nbsp;&nbsp;</span> represents dates which have a statement with items validated against them, but do not balance.
	<li><span style='background-color:green;border:1px solid black'>&nbsp;&nbsp;&nbsp;&nbsp;</span> represents dates which have a statement with items validated against them and also balance.	
</ul>

<a name=statement><b>STATEMENT</b></a><br>The statement information box displays information regarding the selected statement and the respective amounts which have been validated against it. The Withdrawn value is the total of all items that are validated in RSL that have an affect of removing money from the bank account. The Paid In value is the total of all items that are validated in RSL that have an affect of adding money to the bank account. The Balance Left is the difference remaining to get to a value of zero taking into account the opening and closing balances. This is calculated as follows: 
<br><br><center><i><b>Balance Left = (Opening Balance - Closing Balance) - Withdrawn + Paid In</b></i></center>
<br><br>The idea is to get the balance to go to zero which means the statement for the day has been fully validated/reconciled.

<br><br>

<a name=reconcile><b>RECONCILIATION</b></a><br>
      To validate/reconcile items you must first bring them back, this is done 
      by first selecting an appropriate statement, then you select what you want 
      to reconcile from the 'Validate' select box. Depending on what you select, 
      then appropriate filters will appear in the 'FILTER' box which can be used 
      to filter the result set. The Rent Journal items are split into two groups 
      Single and Grouped. The single items are just those items which will appear 
      indiviudally on the bank statement like standing orders. The grouped items 
      are those items which will appear grouped on the bank statement like Payment 
      Cards and Housing Benefit. Once the approprate items have been selected 
      click 'FIND' to bring back the results.  For grouped items there will always be a count of all the transactions involved. Clicking this figure will popup a window listing all the respective transactions.
	  <br><br>NOTE: Some items like payment cards have payments on the bank statement which will differ from the grouped values in RSL, this is due to the fact that some payment card file payments are split between two payments on the bank statement. Therefore you may have to validate some grouped items individually which can be done by going to the 'Rent Journal (Single)' selection.
	  
	  		
	  </blockquote>
</div>
</div>
</body>
</html>
