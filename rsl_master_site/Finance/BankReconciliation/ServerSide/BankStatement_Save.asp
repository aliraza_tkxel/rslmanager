<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpeningBalance = Request("txt_STARTINGBALANCE")
	ClosingBalance = Request("txt_ENDINGBALANCE")
	StatementDate = Request("txt_STATEMENTDATE")
	
	SQL = "SET NOCOUNT ON;INSERT INTO F_BANK_STATEMENTS (STATEMENTDATE, STARTINGBALANCE, ENDINGBALANCE) VALUES ( " &_
			"'" & FormatDateTime(StatementDate,1) & "'," & OpeningBalance & ", " & ClosingBalance & ");" &_
			"SELECT SCOPE_IDENTITY() AS BANKID;SET NOCOUNT OFF;"
	
	RW SQL
	
	Call OpenDB()
	Call OpenRs(rsINS2, SQL)
	BSID = rsINS2("BANKID")
'	RW BSID
	Call CloseRs(rsINS2)
	Call CloseDB()

	Response.Redirect "QuickLoadAfterSave.asp?STATEMENTID=" & BSID & "&Date=" & StatementDate
%>
