<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
L_ORDERID = 0
IF(Request("ORDERID") <> "" AND ISNUMERIC(Request("ORDERID"))) THEN L_ORDERID = Request("OrderID")
%>
<%
Dim IsShowAddNewPI
OpenDB()
ORDERID = Request("ORDERID")
EmployeeLimit = 0
IsShowAddNewPI = false

SQL = "SELECT BL.BLOCKNAME, SCH.SCHEMENAME, PO.ORDERID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, PO.POSTATUS, " &_
		"D.DEVELOPMENTNAME FROM F_PURCHASEORDER PO " &_
		"INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_
		"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID " &_		
		"LEFT JOIN P_BLOCK BL ON BL.BLOCKID = PO.BLOCKID " &_		
        "LEFT JOIN P_SCHEME SCH ON SCH.SCHEMEID = PO.SchemeId " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"WHERE ACTIVE = 1" &_
		" AND PO.ORDERID = " & ORDERID
        '"-- WHERE ACTIVE = 1 AND PO.POSTATUS NOT IN (0)" &_
		'"-- AND PO.POSTATUS < 9 OR PO.POSTATUS IN (17,18)"&_
'response.Write (SQL)		
Call OpenRs(rsDR, SQL)
if (NOT rsDR.EOF) then
	ORDERID = rsDR("ORDERID")
	PONUMBER = PurchaseNumber(rsDR("ORDERID"))
	PONAME = rsDR("PONAME")
	PODATE = FormatDateTime(rsDR("PODATE"),1)
	SUPPLIER = rsDR("NAME")
	SUPPLIERID = rsDR("SUPPLIERID")	
	POSTATUSNAME = rsDR("POSTATUSNAME")
	DEVELOPMENT = rsDR("DEVELOPMENTNAME")
	BLOCK = rsDR("BLOCKNAME")	
    SCHEME = rsDR("SCHEMENAME")
    POStatus = rsDR("POSTATUS")
else
	Response.Redirect ("PurchaseOrderNotFound.asp")
end if
Call CloseRs(rsDR)

    IsShowAddNewPI = "hidden"

    IF((POStatus = 0 OR POStatus = 1 OR POStatus = 3 OR POStatus = 18) AND (Request("ReturnTo") <> "1" AND Request("ReturnTo") <> "2" ) ) Then
        IsShowAddNewPI = "visible"
    END IF

SQL = "SELECT PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, PO.IsServiceChargePO," &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE FROM F_PURCHASEITEM PI " &_
        "INNER JOIN F_PURCHASEORDER PO on PO.ORDERID = PI.ORDERID " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
		"WHERE PI.ACTIVE = 1 AND PI.ORDERID = " & ORDERID & " ORDER BY ORDERITEMID ASC"
'response.Write(SQL)
PIString = ""
TotalNetCost = 0
TotalVAT = 0
TotalGrossCost = 0
TotalRows = 0

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF)
	
	OrderItemID = rsPI("ORDERITEMID")

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
    IsServiceChargePO = rsPI("IsServiceChargePO")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	DataStorage = "<INPUT TYPE=""HIDDEN"" NAME=""DS" & OrderItemID & """ VALUE=""" & NetCost & "|" & VAT & "|" & GrossCost & """>"
	
	PITYPE = rsPI("PITYPE")
	PISTATUS = rsPI("PISTATUS")

	RowsThatCanBeReconciled = RowsThatCanBeReconciled + 1
    
	if (PISTATUS =  16 OR PISTATUS =  20 ) then
		CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item has already been Declined/Cancelled and therefore cannot be amended."">"
	'elseif (PISTATUS = 0) then
		'CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be amended as it still waiting authorisation."">"	
	else
		CheckData = "<INPUT TYPE=""BUTTON"" VALUE=""AM"" TITLE=""To amend this Purchase Item click here."" onclick=""APO(" & OrderItemID & ",'" & IsServiceChargePO & "')"" class=""RSLBUTTON"">"
	end if
	
	PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD><TD>" & CheckData & "</TD></TR>"
	ItemDesc = rsPI("ITEMDESC")
	if ( NOT(ItemDesc = "" OR isNULL(ItemDesc)) ) then
		PIString = PIString & "<TR ID=""PIDESCS"" STYLE=""DISPLAY:NONE""><TD width=450><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"	
	end if
	rsPI.moveNext
wend

'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID
Call OpenRs(rsINV, SQL)
if (NOT rsINV.EOF) then
	InvoiceString = "<br><TABLE WIDTH=755 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
			"<TD WIDTH=200 HEIGHT=20><b>Invoice No:</b></TD><TD width=100><b>Tax Date:</b></TD><TD width=190><b>Status</b></TD><TD align=right width=80><b>Net (�):</b></TD><TD align=right width=75><b>VAT (�):</b></TD><TD align=right width=80><b>Gross (�):</b></TD><TD WIDTH=30></TD></TR>"
	while NOT rsINV.EOF
		InvoiceString = InvoiceString & "<TR><TD>" & rsINV("InvoiceNumber") & "</TD><TD>" & rsINV("TAXDATE") & "</TD><TD>" & rsINV("THESTATUS") & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("NETCOST"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("VAT"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("GROSSCOST"),2) & "</TD></TR>"
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</TABLE>"
end if
Call CloseRs(rsINV)

CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Finance --> Purchase Order --> Amend</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript" src="/js/preloader.js"></script>
<script language="JavaScript" src="/js/general.js"></script>
<script language="JavaScript" src="/js/menu.js"></script>
<script language="JavaScript">

    function ToggleDescs(){
        if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
            NewText = "HIDE DESCRIPTIONS"
            NewStatus = "block"
        }
        else {
            NewText = "SHOW DESCRIPTIONS"
            NewStatus = "none"
        }
        iDesc = document.getElementsByName("PIDESCS")
        if (iDesc.length){
            for (i=0; i<iDesc.length; i++)
                iDesc[i].style.display = NewStatus
        }
        document.getElementById("SHOWDESC").innerHTML = NewText
    }		
		
    function APO(OrderItemid, IsServiceChargePO){
        if(IsServiceChargePO == "True"){
            result = window.open("Popups/AmendPurchaseItemSC.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&OrderItemID=" + OrderItemid + "&Random=" + new Date(), "_blank", "Height=500px; Width= 400px; status: No; resizable: No;")
        }
        else{
            result = window.open("Popups/AmendPurchaseItem.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&OrderItemID=" + OrderItemid + "&Random=" + new Date(), "_blank", "Height=500px; Width= 400px; status: No; resizable: No;")
        }
    }

    function AddNewPIPopUp(){
        result = window.open("Popups/AddNewPurchaseItem.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&Random=" + new Date(), "_blank", "Height=500px; Width= 400px; status: No; resizable: No;")
    }

</script>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(0);preloadImages()" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table border="0" cellspacing="5" cellpadding="0">
        <form name="RSLFORM" id="RSLFORM" method="POST">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table style='border: 1PX SOLID BLACK' width="490PX">
                                    <tr>
                                        <td width="90PX">Order No:</td>
                                        <td><%=PONUMBER%></td>
                                    </tr>
                                    <tr>
                                        <td>Description:</td>
                                        <td><%=PONAME%></td>
                                    </tr>
                                    <tr>
                                        <td>Order Date:</td>
                                        <td><%=PODATE%></td>
                                    </tr>
                                    <tr>
                                        <td>Supplier:</td>
                                        <td><%=SUPPLIER%></td>
                                    </tr>
                                    <% IF DEVELOPMENT <> "" AND NOT ISNULL(DEVELOPMENT) THEN %>
                                    <tr>
                                        <td>Development:</td>
                                        <td><%=DEVELOPMENT%></td>
                                    </tr>
                                    <% END IF %>
                                    <% IF SCHEME <> "" AND NOT ISNULL(SCHEME) THEN %>
                                    <tr>
                                        <td>Scheme:</td>
                                        <td><%=SCHEME%></td>
                                    </tr>
                                    <% END IF %>
                                    <% IF BLOCK <> "" AND NOT ISNULL(BLOCK) THEN %>
                                    <tr>
                                        <td>Block:</td>
                                        <td><%=BLOCK%></td>
                                    </tr>
                                    <% END IF %>
                                    <tr>
                                        <td>Status:</td>
                                        <td><%=POSTATUSNAME%></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10">&nbsp;</td>
                            <td valign="top">
                                <table style='border: 1PX SOLID BLACK' width="255PX" height="100">
                                    <tr>
                                        <td height="100%" colspan="3" valign="top"><font color="red">Click on 
                    the 'AM' (which stands for Amend) button if applicable to 
                    update each respective purchase item.</font></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="2" width="755">
                        <tr bgcolor="#133e71" align="RIGHT" style='color: white'>
                            <td height="20" align="LEFT" width="450" nowrap>
                                <table cellspacing="0" cellpadding="0" width="450">
                                    <tr style='color: white'>
                                        <td><b>Item Name:</b></td>
                                        <td align="right" style='cursor: hand' onclick="ToggleDescs()"><b>
                                            <div id="SHOWDESC">SHOW DESCRIPTIONS</div>
                                        </b></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="40"><b>Code:</b></td>
                            <td width="80"><b>Net (�):</b></td>
                            <td width="75"><b>VAT (�):</b></td>
                            <td width="80"><b>Gross (�):</b></td>
                            <td width="30"></td>
                        </tr>
                        <%=PIString%>
                    </table>
                    <br>
                    <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="2" width="755">
                        <tr bgcolor="#133e71" align="RIGHT">
                            <td height="20" width="490" nowrap style='border: none; color: white'><b>TOTAL : &nbsp;</b></td>
                            <td width="80" bgcolor="white"><b><%=FormatNumber(TotalNetCost,2)%></b></td>
                            <td width="75" bgcolor="white"><b><%=FormatNumber(TotalVAT,2)%></b></td>
                            <td width="80" bgcolor="white"><b><%=FormatNumber(TotalGrossCost,2)%></b></td>
                            <td width="30" bgcolor="white">&nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <div style="text-align: right;">
                        <input type="button" name="AddNewPI" value=" ADD NEW PURCHASE ITEM " class="RSLBUTTON" style="visibility: <%=IsShowAddNewPI%>" onclick="AddNewPIPopUp()" tabindex="6">
                    </div>
                    <input type="HIDDEN" name="hid_PAGE" value="<%=Request("CurrentPage")%>">
                    <input type="HIDDEN" name="hid_ORDERID" value="<%=ORDERID%>">
                    <%=InvoiceString%>  
                </td>
            </tr>
        </form>
    </table>
    <iframe src="/secureframe.asp" name="PURCHASEAMENDFRAME123" style='display: NONE'></iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
