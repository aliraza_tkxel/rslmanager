<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
	
	Call GetCurrentYear()
	FY = GetCurrent_YRange
	
	Dim lstApprovedBy
	DISTINCT_SQL = "F_COSTCENTRE CC " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
            "WHERE CC.COSTCENTREID NOT IN (22) " &_ 
			"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"

		'	"WHERE CC.COSTCENTREID NOT IN (22,23) " &_ 
		'	"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"
	
	'		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND "
	
	Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:70' tabindex=5")
Call BuildSelect(lstCompany, "sel_COMPANY", "G_Company", "CompanyId, Description", "CompanyId", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeCostCentre()'  tabindex=5")

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	' THERE ARE 2 SP's FOR THIS a)GET_VALIDATION_PERIOD_NODEFAULT and b)GET_VALIDATION_PERIOD THE DEFAULT ONE 
	' HAS A WIDER DATE RANGE THAN THE CURRENT YEAR FOR TIDYING UP PURPOSES

	SQL = "EXEC GET_VALIDATION_PERIOD 9"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)
		
	CloseDB()
	
	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
%>
<HTML>
<HEAD>
<title>RSL Manager Finance - Create Credit Note</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
	var FormFields = new Array()
	
	function SetChecking(Type){
		FormFields.length = 0
		if (Type == 1) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
			FormFields[1] = "sel_HEAD|Head|SELECT|Y"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
			FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
			FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
			}
		else if (Type == 2){
			FormFields[0] = "txt_CNNAME|Name|TEXT|Y"
			FormFields[1] = "sel_SUPPLIER|Supplier|SELECT|Y"
			FormFields[2] = "txt_CNDATE|Order Date|DATE|Y"
			FormFields[3] = "txt_DELDATE|Expected Date|DATE|N"
			FormFields[4] = "txt_CNNOTES|Purchase Notes|TEXT|N"
			FormFields[5] = "txt_PONUMBER|Purchase Number|INTEGER|N"			}
		else if (Type == 3) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|N"
			FormFields[1] = "sel_HEAD|Head|SELECT|N"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|N"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|N"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|N"
			FormFields[6] = "txt_VAT|VAT|CURRENCY|N"
			FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|N"
			}
		}
			
	function PopulateListBox(values, thetext, which){
		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");

        if (which == -1)
            var selectlist = document.forms.THISFORM.sel_COSTCENTRE;
        else {

            if (which == 2)
                var selectlist = document.forms.THISFORM.sel_HEAD;
            else
                var selectlist = document.forms.THISFORM.sel_EXPENDITURE;

        }
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");

			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
	function Select_OnchangeCostCentre(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		//PopulateListBox("", "Waiting for Data...", -1);
		document.THISFORM.IACTION.value = "getcostcentre";
		document.THISFORM.action = "ServerSide/GetCostCentres.asp";
		document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		document.THISFORM.submit();
        }
        
	function Select_OnchangeFund(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		THISFORM.IACTION.value = "gethead";
		THISFORM.action = "ServerSide/GetHeads.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}
	
	function Select_OnchangeHead(){
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		document.getElementById("txt_EMLIMITS").value = "0.00";								
		PopulateListBox("", "Waiting for Data...", 1);	
		THISFORM.IACTION.value = "change";
		THISFORM.action = "ServerSide/GetExpenditures.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}

	function SetPurchaseLimits(){
		eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
		eEMLIMITS = eEMLIMITS.split(";;");
	
		document.getElementById("txt_EXPBALANCE").value = FormatCurrency(eAmounts[eIndex]);
		document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	
		}	
	
	var RowCounter = 0
	
	function AddRow(insertPos){
		SetChecking(1)			
		if (!checkForm()) return false
		GrossCost = document.getElementById("txt_GROSSCOST").value
		EmployeeLimit = document.getElementById("txt_EMLIMITS").value
		ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value
		SaveStatus = 1
		
		RowCounter++
		ItemRef = document.getElementById("txt_ITEMREF").value			
		ItemDesc = document.getElementById("txt_ITEMDESC").value			
		ExpenditureID = document.getElementById("sel_EXPENDITURE").value
		ExpenditureName = document.getElementById("sel_EXPENDITURE").options(document.getElementById("sel_EXPENDITURE").selectedIndex).text					
		NetCost = document.getElementById("txt_NETCOST").value			
		VAT = document.getElementById("txt_VAT").value											
		VatTypeID = document.getElementById("sel_VATTYPE").value			
		VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text					
		VatTypeCode = VatTypeName.substring(0,1).toUpperCase()					
        CompanyId = document.getElementById("sel_COMPANY").value	

		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows[i].id == "EMPTYLINE") {
				oTable.deleteRow(i)
				break;
				}
			}

		oRow = oTable.insertRow(insertPos)
		oRow.id = "TR" + RowCounter
		oRow.onclick = AmendRow
		oRow.style.cursor = "pointer"
		
		DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + ExpenditureID + "||<>||" + SaveStatus + "\">"
	
		AddCell (oRow, ItemRef + DATA, ItemDesc, "", "")
		AddCell (oRow, ExpenditureName, "", "", "")		
		AddCell (oRow, VatTypeCode, VatTypeName + " Rate", "center", "")	
		AddCell (oRow, FormatCurrencyComma(NetCost), "", "right", "")
		AddCell (oRow, FormatCurrencyComma(VAT), "", "right", "")
		AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "")
		DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
		AddCell (oRow, DelImage, "", "center", "#FFFFFF")

		SetTotals (NetCost, VAT, GrossCost)
		ResetData()
		return true;
		}
	
	function ResetData(){
		SetChecking(3)
		checkForm()
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Please Select a Cost Centre...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		ResetArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_COSTCENTRE")
		for (i=0; i<ResetArray.length; i++)
			document.getElementById(ResetArray[i]).value = ""
		document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		document.getElementById("txt_VAT").value = "0.00";	
		document.getElementById("AmendButton").style.display = "none"		
		document.getElementById("AddButton").style.display = "block"
		}
		
	function AmendRow(){
		//event.cancelBubble = true

        if (!evt)
            window.event.cancelBubble = true;
        else
            evt.stopPropagation();
		Ref = this.id
		RowNumber = Ref.substring(2,Ref.length)
		StoredData = document.getElementById("iDATA" + RowNumber).value
		StoredArray = StoredData.split("||<>||")
		
		THISFORM.IACTION.value = "LOADPREVIOUS";
		THISFORM.action = "ServerSide/GetExpenditures.asp?EXPID=" + StoredArray[6];
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		
		ReturnArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST")
		for (i=0; i<ReturnArray.length; i++)
			document.getElementById(ReturnArray[i]).value = StoredArray[i]
		document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
		//alert (document.getElementById("UPDATEID").value)
		document.getElementById("AddButton").style.display = "none"
		document.getElementById("AmendButton").style.display = "block"		
		}
	
	function UpdateRow(){
		sTable = document.getElementById("ItemTable")
		RowData = document.getElementById("UPDATEID").value
		RowArray = RowData.split("||<>||")
		MatchRow = RowArray[0]
		for (i=0; i<sTable.rows.length; i++){
			if (sTable.rows(i).id == "TR" + MatchRow) {
				sTable.deleteRow(i)
				SetTotals (-RowArray[1], -RowArray[2], -RowArray[3])				
				break;
				}
			}
		AddRow(i)		
		}
		
	function SetTotals(iNE, iVA, iGC) {
		totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
		totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
		totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

		document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		document.getElementById("hiVA").value = FormatCurrency(totalVAT)
		document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
		
		document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
		document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)						
		}
		
	function AddCell(iRow, iData, iTitle, iAlign, iColor) {
		oCell = iRow.insertCell()
		oCell.innerHTML = iData
		if (iTitle != "") oCell.title = iTitle
		if (iAlign != "") oCell.style.textAlign = iAlign
		if (iColor != "") oCell.style.backgroundColor = iColor
		}
	
	function DeleteRow(RowID,NE,VA,GR){
		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "TR" + RowID) {
				oTable.deleteRow(i)
				SetTotals (-NE, -VA, -GR)				
				break;
				}
			}
		if (oTable.rows.length == 1) {
			oRow = oTable.insertRow()
			oRow.id = "EMPTYLINE"
			oCell = oRow.insertCell()
			oCell.colSpan = 7
			oCell.innerHTML = "Please enter an item from above"
			oCell.style.textAlign = "center"
			}
		ResetData()
		}

	function SaveCreditNote(){
		ResetData()
		SetChecking(2)			
		if (!checkForm()) return false

		
		    var YStart = new Date("<%=YearStartDate%>")
		    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
		    var YEnd = new Date("<%=YearendDate%>")
		

		    if (!checkForm()) return false;
		    if  ( real_date(THISFORM.txt_CNDATE.value) < YStart || real_date(THISFORM.txt_CNDATE.value) > YEnd ) {
		     // we're outside the grace period and also outside this financial year
                    //alert("Please enter an Order Date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
                    alert("The month is now locked down and entries can only be made for the current month");

                    return false;                
			   
		    }	

				
		if (parseFloat(document.getElementById("hiGC").value) <= 0) {
			alert("Please enter some items before creating a credit note.")
			return false;
			}
			
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>"
		THISFORM.method = "POST"
		THISFORM.action = "ServerSide/CreateCreditNote_svr.asp"
		THISFORM.submit()
		}
				
	function real_date(str_date){
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		}					
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<iframe  src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style='display:none;width:400px'></iframe> 
<FORM NAME=THISFORM method=post>
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=755 border=0>
	<TR bgcolor=steelblue style='color:white'> 
		<TD colspan=6><b>CREDIT NOTE INFORMATION</b></TD>
	</TR>
	<TR><TD>Name : </TD><TD colspan=2><input name="txt_CNNAME" type="text" class="TEXTBOX200" size=50 maxlength=50 tabindex=1>&nbsp;<img src="/js/FVS.gif" width=15 height=15 name="img_CNNAME"></td><TD rowspan=3 valign=top NOWRAP>&nbsp;Notes : </TD>
		<TD rowspan=3><textarea name="txt_CNNOTES" cols=55 rows=5 class="TEXTBOX200"  tabindex=2 style='WIDTH:305;overflow:hidden;border:1px solid #133E71'></textarea></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_CNNOTES"></td></TR>
	<TR><TD>Supplier : </TD><TD colspan=2><%=lstSuppliers%>&nbsp;<img src="/js/FVS.gif" width=15 height=15 name="img_SUPPLIER"></td></TR>
	<TR><TD>Order Date : </TD><TD colspan=2><input name="txt_CNDATE" type="text" class="TEXTBOX200" tabindex=1 size=50 maxlength=10 value="<%=FormatDateTime(Date,2)%>">&nbsp;<img src="/js/FVS.gif" width=15 height=15 name="img_CNDATE"></td></tr>
	<TR>
      <TD>PoNumber : </TD>
      <TD colspan=2>
        <input name="txt_PONUMBER" type="text" class="TEXTBOX200" tabindex=2 maxlength=10 size=50>
		 <img src="/js/FVS.gif" width=15 height=15 name="img_PONUMBER">
      </td>  
      <TD style='visibility:hidden'>PoNumber</TD>
      <TD colspan=2 style='visibility:hidden'><input name="txt_DELDATE" type="text" class="TEXTBOX200" tabindex=2 maxlength=10 size=50>
      	<img src="/js/FVS.gif" width=15 height=15 name="img_DELDATE"></td>
    </tr>	
        <tr>
           <td>
                &nbsp;Company:
            </td>
            <td>
               <%=lstCompany%>
            </td>
          <td>
               &nbsp;
            </td>


        </tr>
</TABLE>
<br>		
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=755>
	<TR bgcolor=steelblue style='color:white'> 
		<TD colspan=9><b>NEW ITEM</b></TD>
	</TR>
	<TR>
		<TD>Item : </TD><TD><input name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex=3></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMREF"></td>
		<TD>&nbsp;Cost Centre : </TD><TD><%=lstCostCentres%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_COSTCENTRE"></td>	
		<TD>&nbsp;Net Cost : </TD><TD><input style='text-Align:right' name="txt_NETCOST" type="text"  class="textbox"  maxlength=20 size=11  onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()" tabindex=5></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_NETCOST"></td>			
	</TR>
	<TR>
		<TD rowspan=3 valign=top>Notes : </TD><TD rowspan=4><textarea tabindex=3 name="txt_ITEMDESC" rows=7 class="TEXTBOX200"  style='overflow:hidden;border:1px solid #133E71'></textarea></TD><td rowspan=3><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMDESC"></td>
		<TD>&nbsp;Head : </TD><TD>
							<select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()" tabindex=4>
                            <option value="">Please Select a Cost Centre...</option>
                          </select>
						  </TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_HEAD"></td>	
		<TD>&nbsp;Vat Type : </TD><TD><%=lstVAT%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_VATTYPE"></td>			
	</TR>
	<TR>
		<TD>&nbsp;Expenditure : </TD><TD>                        
						<select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()" tabindex=4>
                          <option value="">Please select a Head...</option>
                        </select>
						</TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_EXPENDITURE"></td>	
		<TD>&nbsp;VAT : </TD><TD><input style='text-Align:right' name="txt_VAT" type="text"  class="textbox"  maxlength=20 size=11  onBlur="TotalBoth()" onFocus="alignLeft()" VALUE="0.00" tabindex=5></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_VAT"></td>			
	</TR>
	<TR>
		<TD STYLE='VISIBILITY:HIDDEN'>&nbsp;Exp Balance : </TD><TD STYLE='VISIBILITY:HIDDEN'><input type="text" name="txt_EXPBALANCE" class="textbox200" tabindex=-1 readonly></TD><td></td>	
	    <TD>&nbsp;Total : </TD><TD><input style='text-Align:right' name="txt_GROSSCOST" type="text"  class="textbox"  maxlength=20 size=11 readonly tabindex=-1></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_GROSSCOST"></td>			
	</TR>
	<TR><TD></TD><TD></TD><td STYLE='VISIBILITY:HIDDEN'>&nbsp;Spend Limit : </td><td STYLE='VISIBILITY:HIDDEN'><input type="text" name="txt_EMLIMITS" class="textbox200" readonly tabindex=-1></td>
		<TD align=right colspan=3>
			<INPUT TYPE=HIDDEN NAME="IACTION">
			<INPUT TYPE=HIDDEN NAME="UPDATEID">			
			<INPUT TYPE=HIDDEN NAME="UPDATEID">						
		  	<INPUT TYPE=HIDDEN NAME="hid_TOTALCCLEFT">
		  	<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST">
		  	<INPUT TYPE=HIDDEN NAME="EMPLOYEE_LIMIT_LIST">						  
			<table cellspacing=0 cellpadding=0><tr>
			<td nowrap><input type=button Name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()" tabindex=6>&nbsp;</td>
			<td><input type=button Name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(-1)"  tabindex=6></td>
			<td><input type=button Name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateRow()" style="display:none" tabindex=6></td>
			</tr></table>			
		</TD>
	</tr>
</TABLE>		
<br>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=755 id="ItemTable" slcolor='' hlcolor="silver">
	<THEAD>
	<TR bgcolor=steelblue ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=left width=250 NOWRAP><b>Item Name:</b></TD>
	  <TD width=200 nowrap align="left"><b>Expenditure:</b></TD>
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=80 nowrap><b>Net (�):</b></TD>
	  <TD width=75 nowrap><b>VAT (�):</b></TD>
	  <TD width=80 nowrap><b>Gross (�):</b></TD>
	  <TD width=29 nowrap>&nbsp;  </TD>	  
	</TR>
	</THEAD>
	<TBODY>
	<TR ID="EMPTYLINE"><TD colspan=7 align="center">Please enter an item from above</TD></TR>
	</TBODY>
  </TABLE>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD height=20 width=490 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><input type="hidden" id="hiNC" value="0"><div id="iNC">0.00</div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><input type="hidden" id="hiVA" value="0"><div id="iVA">0.00</div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><input type="hidden" id="hiGC" value="0"><div id="iGC">0.00</div></b></TD>
	  <TD width=29 nowrap bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>
  <TABLE cellspacing=0 cellpadding=2 width=755>
	<TR ALIGN=RIGHT> 
	  <TD width=754 align=right nowrap>&nbsp;<input type="button" value=" CREATE CREDIT NOTE " class="RSLButton" onclick="SaveCreditNote()"></TD>		  
	</TR>
  </TABLE>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

