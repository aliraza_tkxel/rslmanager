<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)
	Dim clean_desc
	
	ColData(0)  = "Credit No|FORMATCNID|90"
	SortASC(0) 	= "C.CNID ASC"
	SortDESC(0) = "C.CNID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Received|FORMATTEDCNDATE|90"
	SortASC(1) 	= "CNDATE ASC"
	SortDESC(1) = "CNDATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "By|FULLNAME|120"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Item|CNNAME|175"
	SortASC(3) 	= "CNNAME ASC"
	SortDESC(3) = "CNNAME DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Cost|FORMATTEDCOST|60"
	SortASC(4) 	= "TOTALCOST ASC"
	SortDESC(4) = "TOTALCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	PageName = "CancelCreditNoteList.asp"
	EmptyText = "No Relevant Credit Notes found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""CNID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	CNFilter = ""
	if (Request("CNNumber") <> "") then
		if (isNumeric(Request("CNNumber"))) then
			CNFilter = " AND C.CNID = '" & Clng(Request("CNNumber")) & "' "
		end if
	end if
		Function PurchaseNumber(strWord)
		PurchaseNumber = "<b>PO</b> " & characterPad(strWord,"0", "l", 7)
	End Function	

	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " AND C.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	end if

    CompanyFilter = ""
    rqCompany = Request("COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " and isnull(c.COMPANYID,1) = '" & rqCompany & "' "
	end if

	SQLCODE = "SELECT 	C.CNID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, '<b>CN</B> ' + RIGHT('0000' + CAST(C.CNID AS VARCHAR),7) AS FORMATCNID, " &_
				"		C.ORDERID, O.NAME AS SUPPLIER, CONVERT(VARCHAR,ABS(PI.TOTALCOST),1) AS FORMATTEDCOST, " &_
				"		CNNAME, FORMATTEDCNDATE=CONVERT(VARCHAR,CNDATE,103) " &_
				"FROM 	F_CREDITNOTE C " &_
				"		INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, CNID FROM F_PURCHASEITEM F " &_
				"	   	INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID GROUP BY CNID) PI " &_
				"	     ON C.CNID = PI.CNID " &_
				"		LEFT JOIN S_ORGANISATION O ON C.SUPPLIERID = O.ORGID  " &_
				"		LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = C.USERID " &_
				"WHERE	C.ACTIVE = 1 AND C.CNSTATUS = 6 " & SupFilter & CNFilter & CompanyFilter &_
				" Order By " + Replace(orderBy, "'", "''") + ""
				
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	 Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:150px'")	
   
    CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Credit Note List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function load_me(Credit_id){
		location.href = "CancelCreditNote.asp?CNID=" + Credit_id
		}
	
	function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}
	
	function setSupplier(){
		thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
		}
	
	function SubmitPage(){
		if (isNaN(thisForm.CNNumber.value)){
			alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
			return false;
			}
        location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + thisForm.sel_SUPPLIER.value + "&CNNumber=" + thisForm.CNNumber.value + "&Company=" + thisForm.sel_COMPANY.value
		}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();setSupplier()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=CNNumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("CNNumber"))%>" onblur="RemoveBad()" style='width:135px'	></td>
<td><%=lstSuppliers%>
</td>
    <td><%=lstCompany%>
</td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>