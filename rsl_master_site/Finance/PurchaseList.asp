<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderDetail.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (10)	 'USED BY CODE
	Dim DatabaseFields (10)	 'USED BY CODE
	Dim ColumnWidths   (10)	 'USED BY CODE
	Dim TDSTUFF        (10)	 'USED BY CODE
	Dim TDPrepared	   (10)	 'USED BY CODE
	Dim ColData        (11)	 'Syntax	[column title] | [database field] | [display length(px)]
	Dim SortASC        (10)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (10)	 'All Array sizes must match
	Dim TDFunc		   (10)
	Dim clean_desc

	ColData(0)  = "Order No|ORDERID|90"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|80"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"
	TDSTUFF(1)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(1) = ""

	ColData(2)  = "By|FULLNAME|120"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"
	TDSTUFF(2)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(2) = ""

	ColData(3)  = "Supplier|SUPPLIER|120"
	SortASC(3) 	= "SUPPLIER ASC"
	SortDESC(3) = "SUPPLIER DESC"
	TDSTUFF(3)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(3) = ""

	ColData(4)  = "Name|PONAME|175"
	SortASC(4) 	= "PONAME ASC"
	SortDESC(4) = "PONAME DESC"
	TDSTUFF(4)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(4) = ""

	ColData(5)  = "STATUS|POSTATUSNAME|100"
	SortASC(5) 	= "PS.POSTATUSNAME ASC"
	SortDESC(5) = "PS.POSTATUSNAME DESC"
	TDSTUFF(5)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(5) = ""

    ColData(6)  = "Next Action For|NextActionFor|100"
	SortASC(6) 	= "NextActionFor ASC"
	SortDESC(6) = "NextActionFor DESC"
	TDSTUFF(6)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(6) = ""

	ColData(7)  = "Cost|FORMATTEDCOST|55"
	SortASC(7) 	= "TOTALCOST ASC"
	SortDESC(7) = "TOTALCOST DESC"
	TDSTUFF(7)  = " ""align='right' onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(7) = "FormatCurrency(|)"

    ColData(8)  = "Awaiting Invoice|AwaitingInvoice|100"
	SortASC(8) 	= "AwaitingInvoice ASC"
	SortDESC(8) = "AwaitingInvoice DESC"
	TDSTUFF(8)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(8) = ""
	'this Element use to display is disputed flag on purchase list must be last item on colData array
	ColData(9)  = "Is Invoice Disputed|IsInvoiceDisputed|100"
	SortASC(9) 	= "IsInvoiceDisputed ASC"
	SortDESC(9) = "IsInvoiceDisputed DESC"
	TDSTUFF(9)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(9) = ""
	

	PageName = "PurchaseList.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(1)
	'RowClickColumn = " "" title="""""" & rsSet(""POSTATUSNAME"") & """""" onclick=""""load_me("" & rsSet(""ORDERID"") & "")"""" """
	RowClickColumn = "  "" class='order-row' TITLE="""""" & rsSet(""POSTATUSNAME"") & """""""""
	
	Dim orderBy
	CURRENTPO = REQUEST("CPO")
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	If (Request("PONumber") <> "") Then
		If (isNumeric(Request("PONumber"))) Then
			POFilter = " AND PO.ORDERID = '" & Clng(Request("PONumber")) & "' "
		End If
	End If

	SupFilter = ""
	rqSupplier = Request("sel_SUPPLIER")
	If ( rqSupplier <> "") Then
		SupFilter = " AND PO.SUPPLIERID = '" & rqSupplier & "' "
	End If

    POStatusFilter = ""
	rqPOStatus = Request("sel_POSTATUS")
	If ( rqPOStatus <> "") Then
		POStatusFilter = " AND PO.POSTATUS = '" & rqPOStatus & "' "
	End If
	
   CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND isnull(PO.COMPANYID,1) = '" & rqCompany & "' "
	end if

	
	rqFiscalYear = ""
	rqFiscalYear = Request("sel_FISCALYEAR")
	If (rqFiscalYear <> "") Then
	    SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & rqFiscalYear
	    Call OpenDB()
	    Call OpenRs(rsFiscalYear, SQL)
	    If (NOT rsFiscalYear.EOF) Then
		    FiscalFilter = " AND (PO.PODATE >= '" & FormatDateTime(rsFiscalYear("YSTART"),1) & "' and PO.PODATE <= '" & FormatDateTime(rsFiscalYear("YEND"),1) & "')"
		End If
	    Call CloseRs(rsFiscalYear)
	    Call CloseDB()
	End If

    TeamIdFilter = ""
	rqTeamId = Request("sel_TEAMS")
	If ( rqTeamId <> "") Then
		TeamIdFilter = " AND T.TEAMID = '" & rqTeamId & "' "
	End If
	
	
	SQLCODE ="SET NOCOUNT ON DECLARE @Date AS DATE = GETDATE()" &_
"DECLARE @EmployeeName as varchar(100)= ''," &_
"		@PONumber as INT," &_
"		@Supplier as varchar(100)= ''," &_
"		@ITEMNAME as varchar(100)= ''," &_
"		@ITEMDESC as varchar(max)= ''," &_
"		@GROSSCOST as varchar(100)= ''," &_
"		@Status as varchar(20)= ''," &_
"		@RaisedBy as varchar(50)= ''," &_
"		@SendToEmpId as int " &_
"	create table #temp_POQueued" &_
"	(" &_
"		EmployeeName varchar(100)," &_
"		PONumber INT null," &_
"		Supplier varchar(100) null," &_
"		ITEMNAME varchar(100) null," &_
"		ITEMDESC varchar(max) null," &_
"		GROSSCOST varchar(100) null," &_
"		Status varchar(25) null," &_
"		RaisedBy varchar(50) null," &_
"		SendToEmpId int null," &_
"		ORDERITEMID int null" &_
"	)" &_
"	DECLARE EmpId_CURSOR  " &_
"	CURSOR FOR " &_
"	SELECT  Distinct EL.EMPLOYEEID" &_
"	FROM F_PURCHASEITEM PI " &_
"		INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID" &_
"		INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
"		INNER JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = EX.EXPENDITUREID" &_
"	WHERE PO.ACTIVE = 1 AND PO.POSTATUS = 0  " &_
"	OPEN EmpId_CURSOR " &_
"	FETCH NEXT FROM EmpId_CURSOR " &_
"	INTO @SendToEmpId" &_
"    WHILE @@FETCH_STATUS = 0 " &_
"   BEGIN " &_
"	INSERT INTO #temp_POQueued (EmployeeName, PONumber, Supplier, ITEMNAME, ITEMDESC, GROSSCOST, Status, RaisedBy, SendToEmpId, ORDERITEMID)" &_
"	SELECT  E.FIRSTNAME +' '+ E.LASTNAME as EmployeeName," &_
"	PO.ORDERID	as PONumber, " &_
"	 S.NAME as Supplier," &_
"	 PI.ITEMNAME , " &_
"	 PI.ITEMDESC, PI.GROSSCOST, 'Queued' as Status," &_
"	 RE.FIRSTNAME+' '+ RE.LASTNAME as RaisedBy, E.EMPLOYEEID, PI.ORDERITEMID" &_
"	FROM F_PURCHASEITEM PI " &_
"		INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID" &_
"		INNER JOIN E__EMPLOYEE RE ON RE.EMPLOYEEID = PO.USERID" &_
"		INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
"		JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = @SendToEmpId and " &_
"			( " &_
"				el.LIMIT > = PI.GROSSCOST AND " &_
"				( " &_
"					El.LIMIT >= PI.GROSSCOST And" &_
"					(select isnull(Max(LIMIT), 0) from F_EMPLOYEELIMITS where  " &_
"						EXPENDITUREID =  PI.EXPENDITUREID and LIMIT <  " &_
"							(select LIMIT from F_EMPLOYEELIMITS where  " &_
"								EXPENDITUREID =  PI.EXPENDITUREID and EMPLOYEEID = @SendToEmpId)) < Pi.GROSSCOST " &_
"				)  " &_
"				OR " &_
"				( El.LIMIT >= PI.GROSSCOST AND  Pi.GROSSCOST > " &_
"					    (select Isnull(( " &_
"						    select LIMIT  from " &_
"						        F_EMPLOYEELIMITS where  " &_
"							        EXPENDITUREID =   PI.EXPENDITUREID and LIMIT <=  " &_
"								        (select LIMIT from F_EMPLOYEELIMITS where  " &_
"									        EXPENDITUREID =   PI.EXPENDITUREID and EMPLOYEEID = @SendToEmpId) " &_
"						        group by LIMIT  " &_
"						        order by Limit desc " &_ 
"						        OFFSET (ABS(datediff(DD, PI.PIDATE, getDate()))/3 +1) ROWS FETCH NEXT (1) ROWS ONLY), 0) as LIMIT) " &_
"				)" &_
"				ANd PI.ORDERITEMID NOt IN (" &_
"				select PII.ORDERITEMID from F_PURCHASEITEM PII where " &_
"					el.LIMIT > = PI.GROSSCOST AND" &_
"					PII.GROSSCOST >= " &_
"						(select Max(LIMIT) from F_EMPLOYEELIMITS where " &_
"							EXPENDITUREID = PII.EXPENDITUREID and LIMIT <" &_
"								(select LIMIT from F_EMPLOYEELIMITS where " &_
"									EXPENDITUREID = PII.EXPENDITUREID and EMPLOYEEID = @SendToEmpId )) " &_
"				)" &_
"			) " &_
"		Inner Join E__EMPLOYEE E ON E.EMPLOYEEID = EL.EMPLOYEEID" &_
"		Inner JOIN E_CONTACT EC ON EC.EMPLOYEEID = E.EMPLOYEEID" &_
"		INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID" &_
"	WHERE PI.ACTIVE = 1 AND PISTATUS = 0 AND PI.ACTIVE = 1 AND PISTATUS = 0 " &_
"	ORDER BY ORDERITEMID ASC" &_
"		FETCH NEXT FROM EmpId_CURSOR INTO @SendToEmpId" &_
"    END " &_
"	CLOSE EmpId_CURSOR  " &_
"	DEALLOCATE EmpId_CURSOR " &_
"SELECT CASE When PO.IsAmend=1 THEN PS.POSTATUSNAME+'(R)' " &_
"ELSE PS.POSTATUSNAME "  &_
"END , "  &_
" CASE WHEN PS.POSTATUSNAME ='Invoice Received - In Dispute' THEN " &_
" 'block' ELSE 'none' end as IsInvoiceDisputed " &_
" ,E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " &_
" PO.ORDERID , "&_
" O.NAME AS SUPPLIER, CONVERT(VARCHAR,PI.TOTALCOST,1) AS FORMATTEDCOST," &_
" CASE When PO.IsAmend=1 THEN PS.POSTATUSNAME+'(R)' " &_
"ELSE PS.POSTATUSNAME "  &_
"END AS POSTATUSNAME"  &_
", PONAME,  " &_
"			CASE WHEN PS.POSTATUSNAME LIKE 'Paid' or ps.POSTATUSNAME LIKE 'Reconciled' THEN 'N/A' " &_
"				 WHEN PS.POSTATUSNAME LIKE 'Goods Ordered' OR PS.POSTATUSNAME LIKE 'Work Ordered' or PS.POSTATUSNAME LIKE 'Work Completed' THEN E.FIRSTNAME + ' ' + E.LASTNAME" &_
"				 WHEN PS.POSTATUSNAME LIKE 'Invoice Received'  THEN (Select  CASE WHEN Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) LIKE ' ' THEN E.FIRSTNAME + ' ' + E.LASTNAME ELSE Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) END" &_
"																	From" &_
"																		(" &_
"																			Select distinct PITEM2.orderid, " &_
"																				(" &_
"																					Select distinct ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME,' ') + ', ' AS [text()]" &_
"																					From F_PURCHASEITEM PITEM1" &_
"																					left join E__EMPLOYEE E on e.EMPLOYEEID=PITEM1.APPROVED_BY" &_
"																					Where PITEM1.ORDERID = PITEM2.ORDERID and PITEM1.PISTATUS = (SELECT POSTATUSID FROM F_POSTATUS WHERE POSTATUSNAME LIKE 'Invoice Received')" &_
"																					For XML PATH ('')" &_
"																				) [EmployeeNames]" &_
"																			From F_PURCHASEITEM PITEM2" &_
"																			where ORDERID=PO.ORDERID" &_
"																		) APPROVEDBY)" &_
"				 WHEN PS.POSTATUSNAME LIKE 'Invoice Received - In Dispute'  THEN (Select  CASE WHEN Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) LIKE ' ' THEN E.FIRSTNAME + ' ' + E.LASTNAME ELSE Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) END" &_
"																	From" &_
"																		(" &_
"																			Select distinct PITEM2.orderid, " &_
"																				(" &_
"																					Select distinct ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME,' ') + ', ' AS [text()]" &_
"																					From F_PURCHASEITEM PITEM1" &_
"																					left join E__EMPLOYEE E on e.EMPLOYEEID=PITEM1.APPROVED_BY" &_
"																					Where PITEM1.ORDERID = PITEM2.ORDERID and PITEM1.PISTATUS = (SELECT POSTATUSID FROM F_POSTATUS WHERE POSTATUSNAME LIKE 'Invoice Received - In Dispute')" &_
"																					For XML PATH ('')" &_
"																				) [EmployeeNames]" &_
"																			From F_PURCHASEITEM PITEM2" &_
"																			where ORDERID=PO.ORDERID" &_
"																		) APPROVEDBY)" &_
"				 WHEN PS.POSTATUSNAME LIKE 'Queued' THEN (Select  CASE WHEN Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) LIKE ' ' THEN E.FIRSTNAME + ' ' + E.LASTNAME ELSE Left(APPROVEDBY.[EmployeeNames],Len(APPROVEDBY.[EmployeeNames])-1) END" &_
"																	From" &_
"																		(" &_
"																			Select distinct PITEM2.PONumber, " &_
"																				(" &_
"																					Select distinct EmployeeName  + ', ' AS [text()]" &_
"																					From #temp_POQueued PITEM1" &_
"																					Where PITEM1.PONumber = PITEM2.PONumber" &_
"																					For XML PATH ('')" &_
"																				) [EmployeeNames]" &_
"																			From #temp_POQueued PITEM2" &_
"																			where PONumber=PO.ORDERID" &_
"																		) APPROVEDBY)" &_
"                WHEN PS.POSTATUSNAME LIKE 'Goods Approved' THEN ISNULL(O.NAME, 'N/A') "&_
"				 ELSE 'N/A'" &_
"				 END as NextActionFor, FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103), " &_
"                CASE WHEN PS.POSTATUSNAME LIKE 'Queued' or ps.POSTATUSNAME LIKE 'Goods Ordered' or ps.POSTATUSNAME LIKE 'Work Ordered' or ps.POSTATUSNAME LIKE 'Work Completed' or ps.POSTATUSNAME LIKE 'Goods Approved' " &_
"				 THEN 'Yes' " &_
"				 WHEN PS.POSTATUSNAME LIKE 'Invoice Received' OR PS.POSTATUSNAME LIKE 'Invoice Approved' or PS.POSTATUSNAME LIKE 'Paid' or PS.POSTATUSNAME LIKE 'Reconciled' or PS.POSTATUSNAME LIKE 'Part Paid' " &_
"				 THEN 'No' " &_
"				 ELSE 'N/A' " &_
"				 END as AwaitingInvoice " &_
"FROM F_PURCHASEORDER PO  " &_
"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID  " &_
"LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID  " &_
"LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  " &_
"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID  " &_
"LEFT JOIN E_JOBROLETEAM JT ON E.JobRoleTeamId = JT.JobRoleTeamId   " &_
"LEFT JOIN E_TEAM T ON JT.TeamId = T.TEAMID  " &_
"WHERE PO.POSTATUS IN (select POSTATUSID from F_POSTATUS where POSTATUSNAME in ('Goods Ordered',  " &_
"																				'Work Ordered', " &_
"																				'Invoice Received', " &_
																				"'Reconciled',  " &_
																				"'Queued',  " &_
																				"'Work Completed',  " &_
																				"'Paid',  " &_
																				"'Invoice Approved',  " &_
																				"'Invoice Received - In Dispute',  " &_
																				 "'Part Paid', 'Goods Approved')) AND PO.ACTIVE = 1 " & SupFilter & POFilter & FiscalFilter & POStatusFilter & CompanyFilter & TeamIdFilter &_
			" Order By " + Replace(orderBy, "'", "''") + "" &_
" DROP TABLE #temp_POQueued  "

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	 ' response.write(SQLCODE)
	 ' response.end
	 ' response.flush
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If
	Call Create_Table()

	Call OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", rqSupplier, NULL, "textbox200", " style=""width:150px"" ")
	Call BuildSelect(lstFiscalYear, "sel_FISCALYEAR", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, Yend, 103)", "CONVERT(VARCHAR, YSTART, 103)+ ' - ' + CONVERT(VARCHAR, Yend, 103)", "Please Select...", rqFiscalYear, NULL, "textbox200", " style=""width:150px"" ")
	Call BuildSelect(lstPOStatus, "sel_POSTATUS", "F_POSTATUS WHERE POSTATUSNAME IN ('Goods Ordered', 'Work Ordered','Invoice Received','Reconciled','Queued','Work Completed','Paid','Invoice Approved','Part Paid', 'Goods Approved','Invoice Received - In Dispute')", "POSTATUSID, POSTATUSNAME", "POSTATUSNAME", "Please Select...", rqPOStatus, NULL, "textbox200", " style=""width:150px"" ")
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:150px'")	
    Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.TEAMID <> 1 AND T.ACTIVE=1", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", rqTeamId, NULL, "textbox200", " style=""width:150px"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Purchase Order List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

     //function show_detail (order_id) {
	     //if (window.showmodelessdialog) {        // internet explorer
		   //  window.showmodaldialog("popups/purchaseorder.asp?orderid=" + order_id + "&random=" + new date(), "_blank", "dialogheight: 460px; dialogwidth: 850px; status: no; resizable: no;");
		  // } 
         //else {
        //     window.open ("popups/purchaseorder.asp?orderid=" + order_id + "&random=" + new date(), "","width=850px, height=460px, alwaysraised=yes, scrollbars=no");
      //   }
    // }

	function show_detail(Order_id) {
	
	if (window.showModelessDialog) {        // Internet Explorer
		window.showModalDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
	 } 
	else {
	window.open ("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "","width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
	}
	}
	
	
    function load_me(OID)
	{    
	var PONumber = document.getElementById("PONumber").value;
	var SupplierNumber = document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value;
	var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
	var FiscalYear = document.getElementById("sel_FISCALYEAR").options[document.getElementById("sel_FISCALYEAR").selectedIndex].value;
	var POStatusId = document.getElementById("sel_POSTATUS").options[document.getElementById("sel_POSTATUS").selectedIndex].value;
	var TeamId = document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value;
	if (isNaN(PONumber)) {
		alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.");
		return false;
	}
    setSupplier();
	//location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&sel_SUPPLIER="+"<%=Request("sel_SUPPLIER")%>";
	  location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&sel_SUPPLIER=" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + PONumber + "&sel_FISCALYEAR=" + FiscalYear + "&sel_POSTATUS=" + POStatusId + "&sel_TEAMS=" + TeamId;;
	//https://devcrm.broadlandhousinggroup.org/Finance/PurchaseList.asp?CC_Sort=PODATE+DESC&sel_Supplier=943&sel_COMPANY=1&PONumber=&sel_FISCALYEAR=1&sel_POSTATUS=7&sel_TEAMS=115
	setSupplier();
	}


    function RemoveBad() { 
        strTemp = event.srcElement.value;
        strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
        event.srcElement.value = strTemp;
    }

    function setSupplier() {
	    document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = '<%=Request("sel_SUPPLIER")%>';
        }

        function setCompany() {
	    document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value = '<%=Request("sel_COMPANY")%>';
	}

    function SubmitPage() {
        var PONumber = document.getElementById("PONumber").value;
        var SupplierNumber = document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value;
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
        var FiscalYear = document.getElementById("sel_FISCALYEAR").options[document.getElementById("sel_FISCALYEAR").selectedIndex].value;
        var POStatusId = document.getElementById("sel_POSTATUS").options[document.getElementById("sel_POSTATUS").selectedIndex].value;
        var TeamId = document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value;
	    if (isNaN(PONumber)) {
		    alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.");
		    return false;
	    }
	    
	    location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + PONumber + "&sel_FISCALYEAR=" + FiscalYear + "&sel_POSTATUS=" + POStatusId + "&sel_TEAMS=" + TeamId;;

	}

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages();setSupplier();HideOrderId() " onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <table class="RSLBlack">
         <tr>
            <td>
                <b>Order No: </b>
            </td>
            <td>
                <b>Fiscal Years: </b>
            </td>
             <td>
                <b>PO Status: </b>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="PONumber" id="PONumber" class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>"
                    onblur="RemoveBad()" style="width: 135px; background-image: url('img/CXfilter.gif');
                    background-repeat: no-repeat; background-attachment: fixed" />
            </td>
            <td>
                <%=lstfiscalyear%>
            </td>
            <td>
                <%=lstPOStatus%>
            </td>
        </tr>
	    <tr>
            <td>
                <b>Company: </b>
            </td>
            <td>
                <b>Suppliers: </b>
            </td>
            <td>
                <b>Directorate: </b>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <%=lstCompany%>
            </td>
            <td>
                <%=lstSuppliers%>
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <input type="button" class="RSLButton" value=" Update Search " title="Update Search" onclick="SubmitPage()" />
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="750" height="1" alt="" /></td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
	<script>
	function HideOrderId()
	{
	
	var rows= document.getElementById("MainTable").getElementsByTagName('tr');
	for(i =0;i<rows.length ;i++)
	{
	var orderId=document.getElementById('MainTable').getElementsByTagName('tr')[i].getElementsByTagName('td')[1];
	var status=document.getElementById('MainTable').getElementsByTagName('tr')[i].getElementsByTagName('td')[6];
	if(status != null){
	if (status.innerHTML=="Queued")
	{
	orderId.innerHTML="";
	}
	}
	}
	}
	</script>
</body>
</html>
