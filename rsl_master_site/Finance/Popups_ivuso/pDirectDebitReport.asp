<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<%
Dim sv_UserID 
sv_UserID = Session("UserID")

OpenDB()
Call BuildSelect(lst_action, "sel_ACTION", "c_journalaction", "journalactionID, DESCRIPTION", "DESCRIPTION", "Please Select", "", "width:150px;margin-left:17px;", "textbox200", "onChange=''")




CloseDB()	
%>

<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager --> Rent Increase </title>
<link rel="stylesheet" type="text/css" href="/css/RSLManager_greenBrand.css" />

<style  type="text/css" media="all">
    #main{width:100%}
    /*CSS for filter*/
    #filter {background-color: #FFFFFF;width:100%;margin-left:5px;padding-top:10px;float:left;margin-right:3px;}
    .heading {border:1px #133e71 solid;width:160px;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    #filter_head{width:291px;}
    #filter_headline{width:131px;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .filter_headlinediv{width:131px;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    #filter_body{width:291px;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-bottom:18px;margin-bottom:10px;}
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;font-size:13px;}
    .linkstylesmall{text-decoration: underline;color:blue;cursor:hand;font-size:10px;padding:0 5 0 5;}
    .boxstyle{border:1px solid #133e71;width:12px;height:1px;margin:5 5 0 10;}
    .dottedline{border-bottom:1px dotted #133e71; padding:0 0 0 0; margin:0 9 0 9;}
    .line{border-bottom:1px solid #133e71; padding:0 0 0 0; margin:0 9 0 2;}
    .dottedbox{border:1px dotted #133e71;}
    .dottedbox img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .elementlabel{font-size:10px;margin-left:10px;font-weight:bold;color:#133e71}
    .elementlabeldisabled{font-size:10px;margin-left:10px;font-weight:bold;color:gray}
    .margintop{margin:10 0 0 0;}
    /*CSS for content*/
    .content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;float:left;}
    .content_head{width:99%;}
    .heading_content {border:1px #133e71 solid;width:24%;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    .content_headline{width:76%;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .content_headlinediv{width:100%;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    .content_body{width:99%;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-left:10px;float:left;height:100%;}
    .heading2{font-weight:bold;color:#133e71;}
    .heading3{font-weight:bold;color:#133e71;font-size:12px;}
    .fonttwelevewithcolor{font-size:12;color:#133e71;} 
</style>


</head>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>

<script type="text/javascript" language="javascript">
  
 var FormFields = new Array()
 
 
 // This form submition is to bring back the list of previous uploaded files
 
		 function GetDetail()
		  {
		  
		  // declare all the feilds that need to be part of the form valifation
		 FormFields[0] = "txt_FROM|Search - From|DATE|N"
		 FormFields[1] = "txt_TO|Search - To|DATE|N"
		 FormFields[2] = "sel_ACTION|Search - Action|INT|N"
		 FormFields[3] = "txt_TENANCY|Search - Tenancy No|INT|N"
		
			// check all feilds pass validation
			if (!checkForm()) return false;	
			   
			// if pass validation then submit the form
			RSLFORM.target = "ServerFrame"
			RSLFORM.action = "DDMain.asp"
			RSLFORM.submit()
			return true;
		  }
  

</script>
<body>

<div id="maincontent" >
	<div id="topbar">
		<div id="topmenu">
			<div id="system_logo"></div>
			<div style="float:right;margin-right:4px;margin-top:10px;"><a class="lnklogout" href="#">Help</a> <a class="lnklogout"  href="#">Whiteboard</a> <a class="lnklogout" href="#" onClick="window.close()">Log Out</a></div>
			
			<div id="PageHeading" style="clear:right;float:right;margin-right:4px;margin-top:40px;">Finance</div>
			
		</div>
		
	</div>
	<div id="stripe"></div> 
	<br />



   <div id="main">
   
<form name = "RSLFORM" method="post" action="<%=MM_editAction%>">
 <div class="content" id="content2" >
        <div id="content_head2" class="content_head" >
            <div id="content_heading2" class="heading_content">Direct Debit Audit</div>
            <div id="content_headline2" class="content_headline">
                <div class="content_headlinediv" style="border-bottom:1px solid #133e71;"></div>
                <div class="content_headlinediv" style="border-right:1px solid #133e71;"></div>
            </div>
        </div>

        <div id="content_body2" class="content_body" style="height=300px">
<div >
<p style="margin-top:15px;">
<span class="elementlabel">From:</span>
<span class="elementlabel">
	<input type="text" value="" id="txt_FROM" name="txt_FROM" class="textbox100" style="margin-left:15px;" />
	<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
</span>
<span class="elementlabel">To:</span>
<span class="elementlabel">
	<input type="text" value="" id="txt_TO" name="txt_TO" class="textbox100" style="margin-left:15px;" />
	<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
</span>
<span class="elementlabel">&nbsp;</span>
<span class="elementlabel">Action:</span>
<span class="elementlabel">
	<%=lst_action%>
	<image src="/js/FVS.gif" name="img_ACTION" width="15px" height="15px" border="0">
</span>
<span class="elementlabel">Tenancy No:</span>
<span class="elementlabel">
	<input type="text" value="" id="txt_TENANCY" name="txt_TENANCY" class="textbox100" style="margin-left:15px;" />
	<image src="/js/FVS.gif" name="img_TENANCY" width="15px" height="15px" border="0">
</span>
<span class="elementlabel"><input name="button" type="button" class="RSLButton" onClick="GetDetail();" value="Search"/></span>
</p>

</div>
<iframe name="ServerFrame" frameborder="0" style=';display:knone;width=100%;height=100%' src="ddmain.asp"></iframe>

</div>
     </div>
 </div>
 <input type="hidden" id="hid_UserID" name="hid_UserID" value="<%=sv_UserID%>"/>
    <input type="hidden" name="MM_insert" value="true">
	
	
</div>
<br /> 
<div id="strapline">RSLmanager is a <a href="http://www.reidmark.com" target="_blank">Reidmark</a> ebusiness system � All rights reserved 2007</div>

</form>
<iframe name="ServerFrame" style='display:none' src="ddmain.asp"></iframe>
  <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>