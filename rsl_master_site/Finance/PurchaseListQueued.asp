<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderQueued.asp" -->
<%
	CONST CONST_PAGESIZE = 15
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
    Dim iResult
	Dim EnoughLimit
    ColData(0)  = "|EMPTY|85"
	SortASC(0) 	= ""
	SortDESC(0) = ""
	TDSTUFF(0)  = " ""ONCLICK=""""POP('"" & Encode(rsSet(""ORDERID"")) & ""')"""" style='color:blue'""  "
	TDFunc(0) = "MyView(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|80"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "By|FULLNAME|100"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"	
	TDSTUFF(2)  = " "" TITLE="""""" & rsSet(""FIRSTNAME"") & "" "" & rsSet(""LASTNAME"") & """""" "" "
	TDFunc(2) = ""		

	ColData(3)  = "Item|PONAME|275"
	SortASC(3) 	= "PONAME ASC"
	SortDESC(3) = "PONAME DESC"	
	TDSTUFF(3)  = " "" TITLE="""""" & rsSet(""PONOTES"") & """""" "" "
	TDFunc(3) = ""		

	ColData(4)  = "Cost|TOTALCOST|100"
	SortASC(4) 	= "SU.TOTALCOST ASC"
	SortDESC(4) = "SU.TOTALCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"

	ColData(5)  = "CheckBox|rsSet(""ORDERID"")|20"
	SortASC(5) 	= ""
	SortDESC(5) = ""
	TDSTUFF(5)  = " "" style='background-color:white' "" "
	TDFunc(5) = ""

	PageName = "PurchaseListQueued.asp"
	EmptyText = "No Queued Purchase Orders found in the system!!!"
	DefaultOrderBy = "PO.ORDERID DESC"
	RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	Dim SQL_show_all_pos
	EnoughLimit = "false"
	' SEE IF ANY OTHER PEOPLE CAN SEE ALL QUEUED PURCHASES
	Dim CANSEE
	CANSEE = 0
	OpenDB()
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " & SESSION("USERID") 
	Call openRs(rsSet, SQL)
	If Not rsSet.EOF Then 
		CANSEE = 1
	End If
	CloseRs(rsSet)
	CloseDB()

	' DETERMINE WETHER OR NOT TO SHOW ALL PURCHASE ORDERS ONLY IF TEAM IS IT OR LOGIN = KERRI
	'If Session("TeamCode") = "CORP" Or Cint(CANSEE) = 1 Then	
	'If Cint(CANSEE) = 1 Then	
	'	SQL_show_all_pos = ""
	'Else
	'	SQL_show_all_pos = " AND (SELECT COUNT(*) FROM F_PURCHASEITEM IT INNER JOIN F_EMPLOYEELIMITS LIM ON IT.EXPENDITUREID = LIM.EXPENDITUREID "
	'	SQL_show_all_pos=SQL_show_all_pos+"WHERE  LIM.EMPLOYEEID = " & Session("USERID") & " AND LIM.LIMIT >= IT.GROSSCOST) > 0 "	
	'	'SQL_show_all_pos=SQL_show_all_pos+ "WHERE ORDERID = PO.ORDERID AND LIM.EMPLOYEEID = " & Session("USERID") & " AND LIM.LIMIT >= IT.GROSSCOST) > 0 "
	'End If
	CURRENTPO=0
	if (REQUEST("CPO") <> "") then
	CURRENTPO = REQUEST("CPO")
	end if
	POFilter = ""
	if (Request("PONumber") <> "") then
		if (isNumeric(Request("PONumber"))) then
			POFilter = " AND PO.ORDERID = '" & Clng(Request("PONumber")) & "' "
		end if
	end if
	
	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " AND PO.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	end if
 
    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if

     SQLCODE =   " DECLARE @Date AS DATE = GETDATE() " & _
                 " SELECT DISTINCT " & _
                 "         PO.ORDERID, " & _
                 "         '' AS EMPTY, " & _
                 "         FORMATTEDPODATE = CONVERT(VARCHAR, PODATE, 103), " & _
                 "         PODATE, " & _
                 "         PS.POSTATUSNAME, " & _
                 "         E.FIRSTNAME, " & _
                 "         E.LASTNAME, " & _
                 "         PONAME, " & _
                 "         PONOTES, " & _
                 "         isnull(SU.TOTALCOST,0) as TOTALCOST, " & _
                 "         (LTRIM(RTRIM(E.FIRSTNAME)) + ' ' + LTRIM(RTRIM(E.LASTNAME))) AS FULLNAME  " & _
                 "   FROM " & _
                 "      F_PURCHASEORDER PO " & _
                 "      CROSS APPLY (SELECT SUM(GROSSCOST)AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) SU " & _
                 "      INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " & _
                 "      LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " & _
                 "      inner join F_PURCHASEITEM PI on PI.ORDERID = PO.ORDERID "
	
                 if (CANSEE = 0) then
				 if(Request("BUDGETFILLTER")="checked") then
				''''''
				 SQLCODE =   SQLCODE & _
                                
                                " JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = " & SESSION("USERID") & " " &_
						        " AND (  " &_
								"   ( " &_
								"	 El.LIMIT >= PI.GROSSCOST And " &_
								"	  (select isnull(Max(LIMIT), 0) from F_EMPLOYEELIMITS where  " &_
								"			EXPENDITUREID =  PI.EXPENDITUREID and LIMIT <  " &_
								"				(select LIMIT from F_EMPLOYEELIMITS where  " &_
								"					EXPENDITUREID =  PI.EXPENDITUREID and EMPLOYEEID = " & SESSION("USERID") & ")) < Pi.GROSSCOST " &_
								"   )  " &_
								"   OR " &_
								"   ( El.LIMIT >= PI.GROSSCOST AND  Pi.GROSSCOST > " &_ 
								"	    (select Isnull(( " &_
								"		    select LIMIT  from " &_
								"		        F_EMPLOYEELIMITS where  " &_
								"			        EXPENDITUREID =   PI.EXPENDITUREID and LIMIT <=  " &_
								"				        (select LIMIT from F_EMPLOYEELIMITS where  " &_
								"					        EXPENDITUREID =   PI.EXPENDITUREID and EMPLOYEEID = " & SESSION("USERID") & ") " &_
								"		        group by LIMIT  " &_
								"		        order by Limit desc  " &_
								"		        OFFSET (ABS(datediff(DD, PI.PIDATE, getDate()))/3 +1) ROWS FETCH NEXT (1) ROWS ONLY), 0) as LIMIT) " &_
								"   ) " &_
							    " ) "

				''''
				 else
				SQLCODE =   SQLCODE & _
								
								" JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = " & SESSION("USERID") & " " &_
								" AND El.LIMIT >= PI.GROSSCOST "
				 
				 	             end if

	             end if
                 
     SQLCODE =  SQLCODE & _
                "   WHERE ( PO.POTYPE = 1 OR PO.POTYPE = 2) " & _
                "         AND PO.ACTIVE = 1 AND PO.POSTATUS = 0 AND PI.ACTIVE = 1 And PI.PISTATUS = 0 " & SupFilter & CompanyFilter & POFilter & " " &_
                "   ORDER BY " + Replace(orderBy, "'", "''") & ""

  
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	' Response.Write( SQLCODE )
    ' Response.End()
	' Response.flush
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table(Request("BUDGETFILLTER"))

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select Supplier", NULL, NULL, "textbox200", " style='width:240px'")	
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", "onchange='SubmitPage()' style='width:240px'")	
	 
    CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Purchase Order List --> Queued</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" language="JavaScript">
<!--
    $(window).load(function () {

        var element=document.getElementById("CHECKITEM");
        if(<%=CURRENTPO%> >0 &&  $('#CHECKITEM').length > 0 )
        {
            var vis = "block";
            var checkBoxId = "CHECKORDER_<%=Request("CPO")%>";
		
            document.getElementById(checkBoxId).style.display = vis;
            if("<%=EnoughLimit%>" == "true")
            {
                $('#'+checkBoxId).attr('title', "You do not have enough spending power to authorise this item.");
                $('#'+checkBoxId).attr('Disabled', "Disabled");
            }
            var btnC = document.getElementById("tr_<%=Request("CPO")%>");
            btnC.onclick = null;
        }
        $(".CHECKITEM").prop("checked", $('#'+checkBoxId).prop("checked"));
	
        $('#tr_<%=Request("CPO")%>').on('click', 'input[type="checkbox"]', function (e) {
            console.log("checked");
	 
            $(".CHECKITEM").prop("checked", this.checked);	 
            // if (this.checked) {
	 
            //$('.CHECKITEM').attr('checked', true);
            //}
            //else{$('.CHECKITEM').attr('checked', false);}
        });
    });

    function load_me(OID) 
    {
        if (document.getElementById("budgetFillter").checked == true)
        {
            location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&BUDGETFILLTER=checked";
        }
        else
        {
            location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&BUDGETFILLTER=unchecked";
        }
    }

    function CancelBubble() {
        event.cancelBubble = true
    }

    function POP(Order_id) {
        event.cancelBubble = true
        var strContent;

        strContent = navigator.userAgent;
        strContent = strContent.toLowerCase();

        if (strContent.search("chrome") > 0) {
            var centerLeft = (screen.width / 2) - (440 / 2);
            var centerTop = (screen.height / 2) - (340 / 2);

            window.open('Popups/PurchaseOrder.asp?OrderID=' + Order_id + '&Encrypted=true&IncludePO=false&Random=' + new Date(), '_blank', 'height: 460px; width: 1000px; status: No; resizable: No;');
            return;
        }
        else {
            window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Encrypted=true&IncludePO=false&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
            //window.showModalDialog("/Customer/popups/cashposting.asp?customerid=<%=customer_id%>&tenancyid=" + MASTER_TENANCY_NUM, "", "dialogHeight: 340px; dialogWidth: 440px; edge: Raised; center: Yes; help: No; resizable: Yes; status: No; scroll: no");
            return;
        }

    }

    function RemoveBad() {
        strTemp = event.srcElement.value;
        strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g, "");
        event.srcElement.value = strTemp;
    }

    function setSupplier() {
        //document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = '<%=Request("sel_SUPPLIER")%>';
    }

    function SubmitPage() {
        if (document.getElementById("budgetFillter").checked == true)
        {
            thisForm.action = "PurchaseListQueued.asp?BUDGETFILLTER=checked";
            thisForm.method = "POST";
            thisForm.submit();
			
        }
        else{
            thisForm.action = "PurchaseListQueued.asp?BUDGETFILLTER=unchecked";
            thisForm.method = "POST";
            thisForm.submit();
			
        }
        //location.href = 'PurchaseListQueued.asp?sel_Company=' + document.getElementById("sel_COMPANY").value;
    }

    //function SubmitPage() {
    //    if (isNaN(thisForm.PONumber.value)) {
    //        alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
    //        return false;
    //    }
    //    location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>' + document.getElementById("sel_SUPPLIER").value + "&PONumber=" + thisForm.PONumber.value;
    //}

    function ApproveItems() {
        var btn = document.getElementById("btnApproveItem");
        btn.disabled = true;
        var authorizeItems = document.getElementsByClassName("CHECKITEM")
        var isAuthorized = false;
        for(i=0; i<authorizeItems.length; i++)
        {
            if(authorizeItems[i].checked == true)
            {
                thisForm.action = "ServerSide/ProcessPOS.asp?AUTHORIZE=1";
                thisForm.method = "POST";
                thisForm.submit();
                isAuthorized = true;
            }
        }
        btn.disabled = true;
        if(!isAuthorized) {
            alert("Please select item(s).");
            btn.disabled = false;
        }
    }

    function DeclineItems() {        
        var btn = document.getElementById("btnDeclineItems");
        btn.disabled = true;
        var authorizeItems = document.getElementsByClassName("CHECKITEM")
        var isAuthorized = false;
        for(i=0; i<authorizeItems.length; i++)
        {
            if(authorizeItems[i].checked == true)
            {
                thisForm.action = "ServerSide/ProcessPOS.asp?DECLINE=1";
                thisForm.method = "POST";
                thisForm.submit();
                isAuthorized = true;
            }
        }
        btn.disabled = true;
        if(!isAuthorized) {
            alert("Please select item(s).");
            btn.disabled = false;
        }
    }

    var ischecked = "";

    function change_show_all() {

        if (thisForm.showall.checked == true)
            ischecked = "checked";
        else
            ischecked = "no";
    }
    function checkMailSent() { 
	
        var noRecipient = "true";
        if (noRecipient == '<%=Request("noRecipient")%>'  ) {
            if(noRecipient == '<%=Session("noRecipient")%>')
            {
                alert("No contact details exist for this Supplier, and therefore they have not received an email notification that the Purchase Order has been approved. " +
				"Please contact the supplier directly with the Purchase Order details. " +
				"To update the Supplier records with contact details please contact a member of the Finance Team.");
				
                <% Session("noRecipient") = "false" %>
				
                }
        }
    }
    // -->
    </script>
</head>

<!-- End Preload Script -->
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages();setSupplier();checkMailSent()" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get">
        <table class="RSLBlack">
            <tr>
                <td colspan="2"><b>QUEUED PURCHASE ORDERS</b><br>
                    To authorize a purchase item check 
        the respective purchase item and click on 'AUTHORIZE'. To cancel any 
        purchase items check all respective purchase items and then click on 
        'DECLINE'. Purchase Items which are above your expenditure employee limit will have 
        a disabled checkbox next to them, so you will not be allowed to authorize them.
                </td>
            </tr>
            <tr>
                <td style="width: 1px;"><%=lstCompany %></td>
                <td>Budget Fillter<input type="checkbox" name="budgetFillter" id="budgetFillter" onclick="SubmitPage()" <%=Request("BUDGETFILLTER")%>>
                </td>
            </tr>
        </table>
        <table width="750" border="0" cellpadding="0" cellspacing="0">
            <tr>

                <td>&nbsp;</td>
            </tr>
            <tr>
                <td bgcolor="#133E71">
                    <img src="images/spacer.gif" width="750" height="1"></td>
            </tr>
        </table>

        <%=TheFullTable%>
        <table width="750">
            <tr>
                <td align="right">
                    <input id="btnDeclineItems" class="RSLButton" type="button" onclick="DeclineItems()" value=" DECLINE ">&nbsp;<input id="btnApproveItem" class="RSLButton" type="button" onclick="    ApproveItems()" value=" AUTHORIZE "></td>
            </tr>
        </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="400px" height="400px" style='display: none'></iframe>
</body>
</html>