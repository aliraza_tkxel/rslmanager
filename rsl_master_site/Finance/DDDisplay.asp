<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/Calendar.asp" -->
<%
SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YSTART <= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
Call OpenDB()
Call OpenRs(rsYear, SQL)
	YSTART = rsYear("YSTART")
	YEND = rsYear("YEND")
Call CloseRs(rsYear)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance - Direct Debit Display</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='DDCSV.asp?pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	FutureCast = ""
	FutureSQL = ""
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		
		RequestDate = CDate("1" & CurrentDateSmall)
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		

		CurrentDate = CDate(Day(Date) & " " & MonthName(Month(Date)) & " " & Year(Date))
		CurrentDate = FormatDateTime(CurrentDate,1)
		
		if (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) then
			FutureCast = "ThisMonth"
			FutureSQL = "AND DAY(PAYMENTDATE) > DAY('" & CurrentDate & "') "						
		else
			TempDate = DateAdd("m",1, Date)
			if (TheCurrentMonth = Month(TempDate) AND TheCurrentYear = Year(TempDate)) then
				FutureCast = "NextMonth"
				FutureSQL = "AND DAY(PAYMENTDATE) <= DAY('" & CurrentDate & "') "											 
			end if
		end if
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear					
		CurrentDate = FormatDateTime(Date,1)
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		FutureCast = "ThisMonth"
		FutureSQL = "AND DAY(PAYMENTDATE) > DAY('" & CurrentDate & "') " 
	End If

	'THIS IS THE FUTURE FORECASTING SQL
	FUTURESQL = "SELECT COUNT(DDSCHEDULEID) AS THECOUNT, DAY(PAYMENTDATE) AS THEDAY " &_
		"FROM F_DDSCHEDULE DD " &_
		"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
		"WHERE PERIODBALANCE <> 0 AND ISNULL(SUSPEND,0) <> 1  AND ISNULL(TEMPSUSPEND,0) <> 1 AND PAYMENTDATE <= '" & EndOfMonthDate & "' " &_
		"AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''  " &_
		"AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
		"AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_
		FutureSQL &_
		"GROUP BY DAY(PAYMENTDATE) "	
	'Response.Write FUTURESQL	&"<br/>"

	StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

	'THIS CALCULATES THE ACTUAL DIRECT DEBITS WHICH HAVE GONE THROUHG
	ACTUALSQL = "SELECT COUNT(RJ.JOURNALID) AS THECOUNT, DAY(RJ.TRANSACTIONDATE) AS THEDAY, RJ.STATUSID, ISNULL(SUM(RJ.AMOUNT),0) AS THEAMOUNT " &_
		"FROM F_RENTJOURNAL RJ " &_
		"INNER JOIN F_RENTJOURNALDDSCHED RJD ON RJ.JOURNALID = RJD.JOURNALID " &_
		"INNER JOIN F_DDSCHEDULE DD ON RJD.DDSCHEDULEID = DD.DDSCHEDULEID " &_
		"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
		"	WHERE RJ.TRANSACTIONDATE >= '" & StartOfMonthDate & "' AND RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
		"		GROUP BY DAY(RJ.TRANSACTIONDATE), RJ.STATUSID"


 
'RJ.STATUSID = 2 AND	
	'Response.Write ACTUALSQL			
	' Add items to the calendar
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			OpenDB()
			
			'THIS SECTION BUILDS THE FUTURE FORECASTS....
			if (FutureCast <> "") then
				Call OpenRs(rsDays, FUTURESQL)
				while (NOT rsDays.EOF) 
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='DDList.asp?pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "'><font color=blue><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "lightblue"
					rsDays.moveNext
				wend
				CloseRs(rsDays)			
			end if
			
			'THIS SECTION DISPLAYS THE ACTUAL DIRECT DEBITS IN THE SYSTEM
			Call OpenRs(rsDays, ACTUALSQL)
			while (NOT rsDays.EOF) 
				if (rsDays("STATUSID") = 11) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='DDList.asp?pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "&DECLINED=1' title='The count shows the number of direct debits which were subsequently declined. Direct Debit count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & ".'><font color=blue><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "red"
				else
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='DDList.asp?pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='Direct Debit count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & ".'><font color=blue><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "lightgreen"
				end if				
				rsDays.moveNext
			wend
			CloseRs(rsDays)									
		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
                %>
                <div style='position: absolute; left: 500; top: 140; width: 200px'>
                    <b><u>Direct Debits</u></b><br/>
                    <br/>
                    This calendar allows you to view any direct debits which are due or have been processed.
                    <br/>
                    <br/>
                    <font style='background-color: #90ee90'>&nbsp;&nbsp;</font> Processed Direct
                    Debits
                    <br/>
                    <br/>
                    <font style='background-color: red'>&nbsp;&nbsp;</font> Declined Direct Debits<br/>
                    <br/>
                    <font style='background-color: #8fd8d8'>&nbsp;&nbsp;</font> Forecast Direct Debits
                    <br/>
                    <br/>
                    To change the selected month click on '<b><<</b>' or '<b>>></b>' to go backwards
                    and forwards respectively.
                    <br/>
                    <br/>
                    The display shows a figure of how many direct debits will occur on relevant days.
                    To view these click on the figure which will take you to the Direct Debit List.
                    <br/>
                    <br/>
                    To view the CSV files for any particular day click on the month day number.
                    <br/>
                    <br/>
                    <b>NOTE</b>: The calendar will only forecast exactly one month into the future.
                </div>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="400px" height="400px" style='display: none'>
    </iframe>
</body>
</html>
