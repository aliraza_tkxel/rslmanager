<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bank Validation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();

	FormFields[0] = "txt_FROM|From|DATE|Y"
	FormFields[1] = "txt_TO|To|DATE|Y"
	FormFields[4] = "txt_DEBITS|Debits|CURRENCY|Y"
	FormFields[2] = "txt_CREDITS|Credits|CURRENCY|Y"
	FormFields[3] = "txt_ENDBALANCE|End Balance|CURRENCY|Y"			
	FormFields[5] = "txt_STARTINGBALANCE|Start|CURRENCY|Y"

	function new_Statement(){
		if (!checkForm()) return false;
		RSLFORM.target = "BankFrame<%=TIMESTAMP%>";
		RSLFORM.action = "serverside/AddStatement_srv.asp"	
		RSLFORM.submit();
		}
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<table width=100% cellspacing=0 cellpadding=0>
  <form name=RSLFORM method=post>
    <tr> 
      <td> 
        <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
          <TR> 
            <TD></TD>
          </TR>
        </TABLE>
        <TABLE BORDER=0 width=100% CELLPADDING=1 CELLSPACING=1 HEIGHT=30PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
          <TR> 
            <TD NOWRAP colspan=12 > 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <TR> 
                  <TD align=left width="48%" STYLE='BORDER-BOTTOM:1PX SOLID #133E71' height=20><b>&nbsp;Please set the dates for 
                    the new statement:</b></TD>
                  <TD width="52%" align="left" valign="middle"  STYLE='BORDER-BOTTOM:1PX SOLID #133E71'><b>From : </b>&nbsp;
                    <input type=text name="txt_FROM" class="textbox"  size=16>
				  <img name="img_FROM" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>					
					<input type=hidden name="hid_FROM">
                    &nbsp;<b>To :&nbsp; </b>
                    <input type=text name="txt_TO" class="textbox"  size=16>
				  <img name="img_TO" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>					
					<input type=hidden name="hid_TO">
                  </TD>
                </TR>
                <TR> 
                  <TD align=left colspan="2" > 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr bgcolor="#FFFFFF"> 
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr bgcolor="#FFFFFF"> 
                        <td>&nbsp;</td>
                        <td colspan="8">Next enter the following values that are 
                          shown on the bank statement.</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr bgcolor="#FFFFFF"> 
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr bgcolor="#FFFFFF"> 
                        <td><b>&nbsp;</b></td>
                        <td><b>Start : </b>&nbsp;</td>
                        <td> 
                          <input type=text name="txt_STARTINGBALANCE" style='text-align:right' class="textbox"  size=16>
						  <img name="img_STARTINGBALANCE" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>
                        </td>
                        <td align=center>&nbsp;</td>
                        <td align=right><b>Debits : </b>&nbsp;</td>
                        <td> 
                          <input type=text name="txt_DEBITS" style='text-align:right' class="textbox"  size=16>
						  <img name="img_DEBITS" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>
                          <input type=hidden name="hid_DEBITS">
                        </td>
                        <td align=center>&nbsp;</td>
                        <td align=right><b>Credits : </b>&nbsp;</td>
                        <td> 
                          <input type=text name="txt_CREDITS" style='text-align:right' class="textbox"  size=16>
						  <img name="img_CREDITS" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>
                          <input type=hidden name="hid_CREDITS">
                        </td>
                        <td align=center>&nbsp;</td>
                        <td align=right><b>End : </b>&nbsp;</td>
                        <td> 
                          <input type=text name="txt_ENDBALANCE" style='text-align:right' class="textbox"  size=16>
						  <img name="img_ENDBALANCE" SRC="/JS/FVS.GIF" WIDTH=15 HEIGHT=15>
                          <input type=hidden name="hid_ENDBALANCE">
                        </td>
                      </tr>
                      <tr bgcolor="#FFFFFF"> 
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align=center>&nbsp;</td>
                        <td align=right>&nbsp;</td>
                        <td > 
                          <input type="button" value="Add Statement" class="RSLButton"   onClick="new_Statement()" "name="button2">
                          <input type="button" value="Cancel" class="RSLButton"   onClick="javascript:location.href = 'BankValidation.asp'" "name="button22">
                        </td>
                      </tr>
                    </table>
                  </TD>
                </TR>
              </table>
            </TD>
          </TR>
        </TABLE>
      </td>
    </tr>
    <tr> 
      <td>&nbsp; </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </form>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name="BankFrame<%=TIMESTAMP%>" width=300px height=300px style='display:none'></iframe>
</BODY>
</HTML>

