<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

	CONST CONST_PAGESIZE = 15
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName
	Dim TheUser		 	' The Housing Officer ID - default is the session
	Dim TotalArrearsCases
	Dim ArrearActionCount
	Dim TotalArrearsForHO			' for housing officer
	Dim TotalPercentofArrearsForHO  ' for housing officer
	Dim ochecked, cchecked, ES
	Dim TotalArrears
	'Catch Housing Officer ID from dropdown
	If Request("WHICH") <> "" then
	
		If NOT Request("WHICH") = "All" then 
			theUser = cInt(Request("WHICH"))
		Else
			theUser = Request("WHICH")
		End If
		
	Else
		' This will be the initial value of the dropdown on loading of page
		theUser = Session("UserID")
	End If
	
	'Open database and create a dropdown compiled with the housing officers
	OpenDB()
	
		strSQL =	"SELECT T.TENANCYID, T.STARTDATE, P.HOUSENUMBER, P.ADDRESS1, P.ADDRESS2, P.TOWNCITY, P.POSTCODE, AA.DESCRIPTION AS ACTIONDESC " &_ 
				"FROM C_ARREARS A " &_ 
				"	INNER JOIN C_JOURNAL J ON J.JOURNALID = A.JOURNALID " &_ 
				"	INNER JOIN C_ARREARSACTION AA ON AA.ARREARACTIONID = A.ITEMACTIONID " &_ 
				"	INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = J.CUSTOMERID " &_ 
				"	INNER JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_ 
				"	INNER JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID " &_ 
				"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_ 
				"WHERE 	A.ARREARSHISTORYID = (SELECT MAX (ARREARSHISTORYID) FROM C_ARREARS WHERE JOURNALID = J.JOURNALID)  " &_ 
				"	AND T.ENDDATE IS NULL " & ES &_
				"ORDER BY "  & orderBy
	
	'SQL statement to show housing officers that are assigned to a property that have arreas
	SQL = "	SELECT E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME FROM E__EMPLOYEE E " &_
		 "INNER JOIN (SELECT DISTINCT P.HOUSINGOFFICER FROM P__PROPERTY P " &_
		 " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER " &_
		" WHERE E.EMPLOYEEID = P.HOUSINGOFFICER) A " &_
		" ON  E.EMPLOYEEID = A.HOUSINGOFFICER " 	
	Call OpenRs(rsHO, SQL)
	
	Sel_HousingOfficer = "<select name='WHICH' class='textbox200' onchange='' STYLE='WIDTH:190PX'>"
	Sel_HousingOfficer = Sel_HousingOfficer & "<option value=''>Please Select</option>"
	Dim isSelected
	
	While (NOT rsHO.EOF)
	
	   ' Set option to be selected in the housing officers dropdown

	  IF theUser = rsHO.Fields.Item("EMPLOYEEID").Value then 
	  isSelected = "selected"
	  end if
	  
	  Sel_HousingOfficer = Sel_HousingOfficer & "<option value='" & (rsHO.Fields.Item("EMPLOYEEID").Value) & "' " & IsSelected & " >"  & (rsHO.Fields.Item("FULLNAME").Value) & "</option>"	  
	  isSelected = ""
	  rsHO.MoveNext()
	  
		Wend
		If (rsHO.CursorType > 0) Then
		rsHO.MoveFirst
		Else
		rsHO.Requery
		End If
	 Sel_HousingOfficer = Sel_HousingOfficer & "<option value='All' "
	 if theUser = "All" then 
		  Sel_HousingOfficer = Sel_HousingOfficer & "selected"
	 End If
	 Sel_HousingOfficer = Sel_HousingOfficer & ">All</option>"
	 Sel_HousingOfficer = Sel_HousingOfficer & "</SELECT>"
	CloseRs(rsHO)
			
	CloseDB()
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	PageName = "ArrearsManagementReport.asp"
	MaxRowSpan = 6
	Dim orderBy
		orderBy = " T.TENANCYID DESC "
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")
	
	OpenDB()
	Dim CompareValue
	
	get_newtenants()
	
	CloseDB()

	Function get_newtenants()

		'get all previous reuqest items so we do not lose any
		theURL = theURLSET("cc_sort|page")

	' Total Arrears value for report and also use figure to find percentage for Housing Officer - HO
	strSQL = "SELECT ISNULL(SUM(ISNULL(A.AMOUNT,0)),0) AS TOTALARREARS " &_
				"FROM C_TENANCY T  " &_
				"	LEFT JOIN (	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID  " &_
				"			FROM F_RENTJOURNAL I_J  " &_
				"				LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID  " &_
				"				LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID " &_ 
				"				LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
				"			WHERE 	I_J.STATUSID NOT IN (1,4) " &_
				"				AND I_J.ITEMTYPE IN (1,8,9,10)  " &_
				"			GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID " &_
				"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  " &_
				"	LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID  " &_
				"WHERE 	T.ENDDATE IS NULL  "  
		
	Call OpenRs(rsTotalArrears, strSQL)
	TotalArrears = rsTotalArrears("TOTALARREARS")
	Call CloseRs(rsTotalArrears)

	

		THEWHICH = Request("WHICH")
		
		' This is the user id which
		IF THEWHICH = "" THEN 
		'	ES = " AND P.HOUSINGOFFICER = " & Session("USERID") 
		elseif  THEWHICH = "All" then
		ES =  ""
		else
		ES = " AND P.HOUSINGOFFICER = " & THEWHICH 
		
		End If
		Dim strSQL, rsSet, intRecord 
							
		intRecord = 0
		str_data = ""
		strSQL =	"SELECT E.EMPLOYEEID,E.FIRSTNAME,E.LASTNAME, " &_
					"	(SUM(ISNULL(A.AMOUNT,0))- SUM(ABS(ISNULL(B.AMOUNT,0))))  AS TOTALARREARS , " &_
					"	COUNT(A.TENANCYID) AS ARREARSCASESCOUNT " &_
					"FROM C_TENANCY T  " &_
					"	LEFT JOIN (	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID  " &_
					"			FROM F_RENTJOURNAL I_J  " &_
					"				LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID  " &_
					"				LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID  " &_
					"				LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID  " &_
					"			WHERE 	I_J.STATUSID NOT IN (1,4) " &_
					"				AND I_J.ITEMTYPE IN (1,8,9,10)  " &_
					"			GROUP BY I_J.TENANCYID ) A ON A.TENANCYID = T.TENANCYID "&_
						"		LEFT JOIN ( 	SELECT ISNULL(SUM(I_J.AMOUNT), 0) AS AMOUNT, I_J.TENANCYID    " &_
						"				FROM F_RENTJOURNAL I_J    " &_
						"					LEFT JOIN F_ITEMTYPE I_I ON I_J.ITEMTYPE = I_I.ITEMTYPEID    " &_
						"					LEFT JOIN F_PAYMENTTYPE I_P ON I_J.PAYMENTTYPE = I_P.PAYMENTTYPEID    " &_
						"					LEFT JOIN F_TRANSACTIONSTATUS I_S ON I_J.STATUSID = I_S.TRANSACTIONSTATUSID    " &_
						"				WHERE I_J.STATUSID IN (1) AND I_J.ITEMTYPE IN (1,8,9,10)    " &_
						"					AND PAYMENTTYPE = 1    " &_
						"				GROUP BY I_J.TENANCYID ) B ON B.TENANCYID = T.TENANCYID    " &_
					"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID "&_
					"	INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.HOUSINGOFFICER "&_
					"	LEFT JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID "&_
					"WHERE 	T.ENDDATE IS NULL "&_
					"GROUP BY  E.EMPLOYEEID,E.FIRSTNAME,E.LASTNAME"
			 ' rw  strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		' double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
				strUsers = ""
				CustCount = 0
			PrevTenancy = ""
			For intRecord = 1 to rsSet.PageSize
			HOTotalArrears = rsSet("TOTALARREARS")
			
			TotalPercentofArrearsForHO = (HOTotalArrears/TotalArrears * 100)
			

				SET rsCust = Nothing
				
				str_data = str_data & "<TR><TD>&nbsp;&nbsp;" & TenancyReference(rsSet("EMPLOYEEID")) & "</TD>" &_
					"<TD>&nbsp;&nbsp;" & rsSet("FIRSTNAME") & " " & rsSet("LASTNAME") & "</TD>" &_
					"<TD>&nbsp;&nbsp;" & rsSet("ARREARSCASESCOUNT") & "</TD>" &_
					"<TD>&nbsp;&nbsp;" & FormatNumber(TotalPercentofArrearsForHO,2) & " %</TD>" &_
					"<TD>&nbsp;&nbsp;" & FormatCurrency(HOTotalArrears,2) & "</TD>" &_
					"</TR>"
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit for
				
			Next
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
			"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
			" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		End If

		' if no teams exist inform the user
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " STYLE='FONT:12PX' ALIGN=CENTER>No tenancies found with a value greater than the specified arrears value</TD></TR>" 
			fill_gaps()						
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	
		
	End function
	

	Function WriteJavaJumpFunction()
		JavaJump = JavaJump & "<SCRIPT LANGAUGE=""JAVASCRIPT"">" & VbCrLf
		JavaJump = JavaJump & "<!--" & VbCrLf
		JavaJump = JavaJump & "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		JavaJump = JavaJump & "-->" & VbCrLf
		JavaJump = JavaJump & "</SCRIPT>" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customers --> Arrears List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function GO(){
	location.href = "<%=PageName%>?<%="CC_Sort=" & orderBy & "&COMPAREVALUE="%>&WHICH=" + thisForm.WHICH.value
	}

function PrintMe(){
	window.showModalDialog("PrintMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>", "Arrears List", "dialogHeight: 500px; dialogWidth: 800px; status: No; resizable: No;");
	}

function XLSMe(){
	window.open("XLSMe.asp?WHICH=" + thisForm.WHICH.value  + "&CC_Sort=<%=Request("cc_Sort")%>");
	}
	
// -->
</SCRIPT>
<%=WriteJavaJumpFunction%> 
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <td colspan=2> This report returns the 'Housing Officers' who are allocated 
        to arrears.&nbsp;<br>
        <br>
      </td>
    </tr>
    <TR> 
      <TD ROWSPAN=2 valign=bottom><IMG NAME="tab_Tenants" TITLE='Tenants' SRC="../Customer/Images/tab_arrears.gif" WIDTH=78 HEIGHT=20 BORDER=0></TD>
      <TD align=right>&nbsp; </TD>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71><IMG SRC="../Customer/images/spacer.gif" WIDTH=672 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 CLASS='TAB_TABLE' STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE BORDER=7>
    <THEAD> 
    <TR> 
      <TD CLASS='NO-BORDER' WIDTH=116><a href="<%=PageName%>?" style="text-decoration:none"><img src="../myImages/sort_arrow_up_old.gif" border="0" alt="Sort Ascending" width="17" height="16"></a>&nbsp;<b>Empl 
        No. </b><a href="../Customer/<%=PageName%>?<%=theURL%>CC_Sort=" style="text-decoration:none"><img src="../myImages/sort_arrow_down.gif" alt="Sort Descending" border="0" width="11" height="12"></a></TD>
      <TD CLASS='NO-BORDER' WIDTH=202>&nbsp;<b>Housing Officer Name</b></TD>
      <TD CLASS='NO-BORDER' WIDTH=128>&nbsp;<B>Arrears Cases</B>&nbsp;</TD>
      <TD CLASS='NO-BORDER' WIDTH=128><b>% of Total Arrears</b></TD>
      <TD CLASS='NO-BORDER' WIDTH=131>&nbsp;<B>Allocated Arrears</B>&nbsp;</TD>
    </TR>
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=7 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
  </TABLE>
  <table width="749">
    <TR> 
      <TD valign=top> 
        <table width="393">
          <tr bgcolor="#F9FCFF"> 
            <td width="251">Total Arrears Value:</td>
            <td width="130"> 
              <%
				rw FormatCurrency(TotalArrears  , 2)
				%>
            </td>
          </tr>
        </table>
      </TD>
    </TR>
  </table>

</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>	