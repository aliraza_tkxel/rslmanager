<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)
	Dim clean_desc
	Dim disablebutton
	
	ColData(0)  = "Order No|ORDERID|85"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPIDATE|80"
	SortASC(1) 	= "PIDATE ASC"
	SortDESC(1) = "PIDATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Expenditure|EXPDESC|110"
	SortASC(2) 	= "EXPDESC ASC"
	SortDESC(2) = "EXPDESC DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Item|SHORTITEMNAME|220"
	SortASC(3) 	= "SHORTITEMNAME ASC"
	SortDESC(3) = "SHORTITEMNAME DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Cost|FORMATTEDCOST|60"
	SortASC(4) 	= "GROSSCOST ASC"
	SortDESC(4) = "GROSSCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"
	
	ColData(5)  = "PO Status|PISTATUS|85"
	SortASC(5) 	= "PISTATUS ASC"
	SortDESC(5) = "PISTATUS DESC"	
	TDSTUFF(5)  = " "" NOWRAP align='right'"" "
	TDFunc(5) = ""		
	
	ColData(6)  = "Status|JOURNALSTATUS|85"
	SortASC(6) 	= "JOURNALSTATUS ASC"
	SortDESC(6) = "JOURNALSTATUS DESC"	
	TDSTUFF(6)  = " "" NOWRAP align='right'"" "
	TDFunc(6) = ""	

	PageName = "ExpenditurePurchaseList.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" TITLE="""""" & Server.HTMLEncode(rsSet(""ITEMNAME"")) & "": &#13; "" & Server.HTMLEncode(rsSet(""ITEMDESC"")) & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

    if(Request.QueryString("CC_Sort")<>"") then
        disablebutton=""
    else
        disablebutton="disabled"
    end if        
	
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	DIM COSTCENTREFilter
	COSTCENTREFilter = ""
	if (Request("sel_COSTCENTRE") <> "") then
		COSTCENTREFilter = " AND CC.COSTCENTREID = " & Request("sel_COSTCENTRE") & " "
	end if
	
	DIM HeadFilter 
	HeadFilter = ""
	if (Request("sel_HEAD") <> "") then
			HeadFilter = " AND H.HEADID = " & Request("sel_HEAD") & " "	
	end if

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if
	
	DIM FiscalYearFilter, FiscalYearFilter_Array, FiscalYearFilter_Start, FiscalYearFilter_End
	FiscalYearFilter = ""
	if (Request("sel_FISCALYEARFILTER") <> "") then
        	FiscalYearFilter_Array = Split(Request("sel_FISCALYEARFILTER"), ";", 2,1)
        	FiscalYearFilter_Start = FiscalYearFilter_Array(0)
        	FiscalYearFilter_End = FiscalYearFilter_Array(1)
            FiscalYearFilter = " AND PIDATE >= '" & FiscalYearFilter_Start & "' AND PIDATE <= '" & FiscalYearFilter_End & " 23:59 ' "
			'rw FiscalYearFilter
			selectedFiscalYear = FiscalYearFilter_Start & " - " & FiscalYearFilter_End
	end if 
		
'	DIM ExpFilter
'	ExpFilter = ""
'	if (Request("sel_Exp") <> "") then
'			ExpFilter = " AND E.EXPENDITUREID = " & Request("sel_Exp") & " "
'	end if

	SQLCODE = "SELECT ISNULL(EMP.FIRSTNAME + ' ' + EMP.LASTNAME,'') AS FULLNAME,  " &_
					"ISNULL(PO.ORDERID,'') AS ORDERID,ISNULL(PONAME,'') AS PONAME, " &_
					" ISNULL(PI.ORDERITEMID,'') AS ORDERITEMID, ISNULL(PI.ITEMNAME,'') AS ITEMNAME, ISNULL(PI.ITEMDESC,'') AS ITEMDESC, " &_
				"	ISNULL(CASE WHEN LEN(PI.ITEMNAME ) > 35   " &_
 				"			THEN LEFT(PI.ITEMNAME, 33) +  '...'   " &_
				"			ELSE  PI.ITEMNAME   " &_
        		"		END,'n/a') AS SHORTITEMNAME, " &_
				"   PISTATUS = ISNULL(CASE PITYPE WHEN 5 THEN 'Paid <font color=red>Tenant Reimbursement</font>' WHEN 4 THEN 'Paid <font color=red>Staff Expense</font>'WHEN 3 THEN 'Paid <font color=red>Petty Cash</font>' " &_
				"	ELSE PS.POSTATUSNAME END,''), " &_
				"	ISNULL(E.EXPENDITUREID,'') AS EXPENDITUREID, ISNULL(E.DESCRIPTION,'') AS EXPDESC, ISNULL(H.HEADID,'') AS HEADID,ISNULL(CC.COSTCENTREID,'') AS COSTCENTREID , " &_
				"	ISNULL(CONVERT(VARCHAR,PI.GROSSCOST,1),'') AS FORMATTEDCOST, " &_
				"	FORMATTEDPIDATE=ISNULL(CONVERT(VARCHAR,PIDATE,103),''), CS.DESCRIPTION as JOURNALSTATUS " &_
				"FROM  F_PURCHASEITEM PI " &_
				"	LEFT JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_
				  "     LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID  " &_
				  "     LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
				  "     LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID=P.DEVELOPMENTID "&_
                  "     LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
                  "     LEFT JOIN P_SCHEME SCH ON P.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
				"    LEFT JOIN dbo.P_WOTOREPAIR WOR ON WOR.ORDERITEMID=PI.ORDERITEMID  " &_
				"	LEFT JOIN dbo.C_JOURNAL CJ ON CJ.JOURNALID=WOR.JOURNALID  " &_
				"	LEFT JOIN dbo.C_STATUS CS ON CS.ITEMSTATUSID=CJ.CURRENTITEMSTATUSID  " &_
				"	LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID  " &_
				"	LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  " &_
				"	INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = PI.EXPENDITUREID " &_
				"     LEFT JOIN NL_ACCOUNT AC ON E.QBDEBITCODE  =AC.ACCOUNTNUMBER "&_
				"	INNER JOIN F_HEAD H ON H.HEADID = E.HEADID " &_
				"	INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID " &_
				"	LEFT JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = PO.USERID  " &_
				"WHERE PI.PITYPE <> 6 AND PO.POSTATUS <> 0 AND PI.ACTIVE = 1 " & COSTCENTREFilter & HeadFilter & FiscalYearFilter & CompanyFilter &_
				"Order By " + Replace(orderBy, "'", "''") + ""
	'rw SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	

		Call Create_Table()

	
	OpenDB()
	'Call BuildSelect(lstCostCentre, "sel_COSTCENTRE", "F_COSTCENTRE", "COSTCENTREID, DESCRIPTION", "DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " style='width:220px;margin:0 0 0 5;' onChange=""loadSelects('1')"" ")	
		
		IF COSTCENTREFilter <> "" THEN
			SQL = "SELECT E.DESCRIPTION as exp,H.DESCRIPTION as head ,CC.DESCRIPTION as costcentre " &_
						"FROM  F_EXPENDITURE E " &_
						"	INNER JOIN F_HEAD H ON H.HEADID = E.HEADID " &_
						"	INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID " &_
						"WHERE 1=1 " & COSTCENTREFilter & HeadFilter
			Call OpenRs(rsDESCRIPTIONS, SQL)
			If (NOT rsDESCRIPTIONS.EOF)  then
				selectedEXP = rsDESCRIPTIONS("EXP")
				selectedHEAD = rsDESCRIPTIONS("HEAD")
				selectedCC = rsDESCRIPTIONS("COSTCENTRE")
			end if	
			Call CloseRs(rsDESCRIPTIONS)
		END IF
		
			SQLTOTALCODE = "SELECT SUM(PI.GROSSCOST) AS THETOTAL " &_
				"FROM  F_PURCHASEITEM PI " &_
				"	LEFT JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_
				"	LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID  " &_
				"	LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  " &_
				"	INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = PI.EXPENDITUREID " &_
				"	INNER JOIN F_HEAD H ON H.HEADID = E.HEADID " &_
				"	INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID " &_
				"	LEFT JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = PO.USERID  " &_
				"WHERE PI.ACTIVE = 1 AND PO.POSTATUS <> 0 " & COSTCENTREFilter & HeadFilter & FiscalYearFilter
			'	rw SQLTOTALCODE
			Call OpenRs(rsTotal, SQLTOTALCODE)
				if  rsTotal("THETOTAL") <> "" then
					theTotal = " " & FormatCurrency(rsTotal("THETOTAL"),2)
				End If
			Call CloseRs(rsTotal)
			
		Call BuildSelect(lstFiscalYearFilter, "sel_FISCALYEARFILTER", "F_FISCALYEARS", "CONVERT(VARCHAR,YSTART,103) + ';' + CONVERT(VARCHAR,YEND,103) AS RANGE, CONVERT(VARCHAR,YSTART,103) + ' - ' + CONVERT(VARCHAR,YEND,103) AS DISPLAY", "DISPLAY", "Please Select...", NULL, NULL, "textbox200", " style='width:160px;padding:0 0 0 2;' onChange=""DisableEnableButton();"" ")
		Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:150px' onChange=""loadSelects('3')""")	
    
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Purchase Order List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
    function DisableEnableButton() {
        if (thisForm.sel_FISCALYEARFILTER.value != "") {
            thisForm.searchButton.disabled = false;
            //thisForm.exportButton.disabled = false;
        }
        else {
            thisForm.searchButton.disabled = true;
            //thisForm.exportButton.disabled = true;
        }
    }
    function load_me(Order_id) {

        if (isNaN(Order_id) || Order_id == 0) {
            alert("This item is a 'Petty Cash' item and has no purchase order associated with it.")
            return false
        }
        window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
    }

    function RemoveBad() {
        strTemp = event.srcElement.value;
        strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g, "");
        event.srcElement.value = strTemp;
    }

    function SubmitPage() {
        //if (isNaN(thisForm.PONumber.value)){
        //alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
        //	return false;
        //}
        location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + thisForm.sel_SUPPLIER.value + "&PONumber=" + thisForm.PONumber.value + "&sel_COMPANY=" + thisForm.sel_COMPANY.value + "&sel_COSTCENTRE=" + thisForm.sel_COSTCENTRE.value + "&sel_HEAD=" + thisForm.sel_HEAD.value + "&sel_FISCALYEARFILTER=" + thisForm.sel_FISCALYEARFILTER.value;
    }

    function loadSelects(selID) {
        thisForm.searchButton.disabled = true
        thisForm.hid_selID.value = selID;

        if (selID == "1")
            thisForm.sel_HEAD.disabled = false
        if (selID == "3")
            thisForm.sel_COSTCENTRE.disabled = false

        thisForm.target = "frm_team";
        thisForm.action = "serverside/Costcentre_Head_Exp__Selects.asp";
        thisForm.submit();
        thisForm.action = "";
    }

    function PopulateListBox(values, thetext, which) {

        values = values.replace(/\"/g, "");
        thetext = thetext.replace(/\"/g, "");
        values = values.split(";;");
        thetext = thetext.split(";;");
        var idname
        if (which == 1) {
            var selectlist = document.forms.thisForm.sel_HEAD;
            idname = "sel_HEAD";
        }
        else if (which == 3) {
            var selectlist = document.forms.thisForm.sel_COSTCENTRE;
            idname = "sel_COSTCENTRE";
        }
        else {
            var selectlist = document.forms.thisForm.sel_EXP;
            idname = "sel_EXP";
        }

        var listLength = 0
        //alert(selectlist.length);
        //selectlist.length = 0
        document.getElementById(idname).options.length = 0

        for (i = 0; i < thetext.length; i++) {
            var oOption = document.createElement("OPTION");

            oOption.text = thetext[i];
            oOption.value = values[i];
            selectlist.options[listLength] = oOption;
            listLength = listLength + 1
        }

    }

    /////////////////////////////////
    //	Opening data in XLS Below   //
    /////////////////////////////////
    function ExportMe() {
        var costcentreid, costcentre,company,companyid
        var headid, head
        var fiscalyearid, fiscalyear
        var total, querystring

        costcentreid = "<%=Request.QueryString("sel_COSTCENTRE")%>"
        costcentre = "<%=selectedCC%>"
        headid = "<%=Request.QueryString("sel_HEAD")%>"
        head = "<%=selectedHead%>"
        fiscalyearid = "<%=Request.QueryString("sel_FISCALYEARFILTER")%>"  
        Orderby = "<%=Request.QueryString("CC_Sort")%>"  
        //fiscalyear="<%=selectedFiscalYear%>"
        total = "<%=theTotal%>"
		companyid = "<%=Request.QueryString("sel_COMPANY")%>"
        querystring = ""

        //alert(Orderby+"\n"+costcentreid + "\t" + costcentre + "\n" +  headid + "\t" +   head + "\n" + fiscalyearid + "\t" + fiscalyear + "\n" + total); 

        if (total != "") {
            querystring = querystring + "?theTotal=" + total
        }

        if (Orderby != "") {
            querystring = querystring + "&orderBy=" + Orderby
        }

        if (fiscalyearid != "") {
            querystring = querystring + "&sel_FISCALYEARFILTER=" + fiscalyearid
            //querystring=querystring + "&selectedFiscalYear="+fiscalyear  
        }
        if (headid != "") {
            querystring = querystring + "&sel_HEAD=" + headid
            querystring = querystring + "&selectedHead=" + head
        }
        if (costcentreid != "") {
            querystring = querystring + "&sel_COSTCENTRE=" + costcentreid
            querystring = querystring + "&selectedCC=" + costcentre
        }
		if (companyid !=""){
		 querystring = querystring + "&sel_COMPANY=" + companyid
		}
        if (querystring != "")
            window.open("ExpenditurePurchaseList_xls.asp" + querystring)
        else
            window.open("ExpenditurePurchaseList_xls.asp")
    }
// -->
</SCRIPT>
<!-- End Preload Script -->      

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name = thisForm method=get>
  <table class="RSLBlack" border=0>
    <tr> 
      <td colspan="4">&nbsp;Please use the drop-down boxes below, to find 'Purchase Order Items' associated with individual expenditures.</td>
    </tr>
    <tr> 
      <td style="margin:0 0 0 0;padding:0 5 0 2;width:204px;">Period: <%=lstFiscalYearFilter%></td>
      <td style="margin:0 0 0 0;padding:0 0 0 0;">Cost Centre:
          <select name="sel_COSTCENTRE" id ="sel_COSTCENTRE" style='width:270px;margin-left:37px;' class=textbox200 disabled onChange="loadSelects('1');thisForm.searchButton.disabled = false">
          <option>Costcentre</option>
        </select>
      </td>
	  
    </tr>
    <tr>
        <td>Company <%=lstCompany%></td>
        <td>
        Head: 
        <select name="sel_HEAD" id="sel_HEAD" style='width:270px;margin-left:37px;' class=textbox200  disabled onChange="thisForm.searchButton.disabled = false">
          <option>Development</option>
        </select>
      </td>
      <td > 
        <input name="searchButton" type="button" class="RSLButton" value=" Search " onclick="SubmitPage()" disabled>
        <input name="exportButton" type="button" class="RSLButton" value=" Export " onclick="ExportMe()" <%=disablebutton%> />
        <input type="hidden" name="hid_selID">
        <input type="hidden" name="sel_SUPPLIER">
        <input type="hidden" name="PONumber">
      </td>
    </tr>
    </table>
    <table class="RSLBlack" border=0>
    <% IF selectedFiscalYear <> "" THEN %>
    <tr> 
      <td width="200" valign="top"><font color="#FF0000">&nbsp;<%=selectedFiscalYear%></font></td>
      <% IF selectedCC <> "" THEN %>
        <td width="200" rowspan="2">&nbsp;</td>
      <% ELSE %>
        <td width="150" rowspan="2"><font color="#FF0000"><%=theTotal%></font></td>
      <% END IF %>
    </tr>
    <% END IF %>
    <% IF selectedCC <> "" THEN %>
    <tr> 
      <td width="200" valign="top"><font color="#FF0000">&nbsp;<%=selectedCC%></font></td>
      <td width="200" valign="top"><font color="#FF0000"><%=selectedHead%></font></td>
      <td width="150" valign="top"><font color="#FF0000">Total:<%=theTotal%></font></td>
    </tr>
    <% END IF %>
  </table>
  <TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR> 
      <TD>&nbsp;</TD>
    </TR>
    <TR> 
      <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
    </TR>
  </TABLE>
  <%=TheFullTable%> 
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>