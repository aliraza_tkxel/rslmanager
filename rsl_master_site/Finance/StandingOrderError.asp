<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Standing Order Error</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<br><br>
An Error has occured processing your Standing Orders. This may be due to the following:<br><br>
<ul>
<%
ErrorCode = Server.HTMLEncode(Request("Error"))
if (ErrorCode = "ERR1") then
	Response.Write "<li>The file that was uploaded cannot be found."
elseif (ErrorCode = "ERR2") then	
	Response.Write "<li>The file that was uploaded has some missing/duplicate columns."
elseif (ErrorCode = "ERR3") then	
	Response.Write "<li>The file that was uploaded has duplicate columns."
else
	Response.Write "<li>No reason can be determined for failure."
end if
%>  
</ul>
<br><br>&nbsp<a href='SODisplay.asp?date=<%=Request("pROCESSINGdATE")%>'><font color=blue><b>BACK to Standing Order Upload</b></font></a>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>