<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	UserId=Session("UserID")
	Dim C_CNID, SUPPLIER_EXPPAYDATE
	C_CNID = ""
	if(Request.QueryString("CNID") <> "") then C_CNID = Request.QueryString("CNID")
	OpenDB()
	SQL = 	"SELECT C.CNID, '<b>CN</B> ' + RIGHT('0000' + CAST(C.CNID AS VARCHAR),7) AS FORMATCNID, " &_
			"		O.NAME, C.CNID, C.ORDERID, C.CNNAME, C.SUPPLIERID, " &_
			"		C.CNDATE, C.CNNOTES, ISNULL(O.NAME, 'SUPPLIER NOT ASSIGNED') AS SUPPLIER, " &_
			"		O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY, " &_
			"		O.TELEPHONE1, O.TELEPHONE2, O.FAX, PS.POSTATUSNAME	" &_
			"FROM 	F_CREDITNOTE C  " &_
			"		LEFT JOIN S_ORGANISATION O ON C.SUPPLIERID = O.ORGID  " &_
			"		INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = C.CNSTATUS	" &_
			"WHERE 	ACTIVE = 1 AND C.CNID = " & C_CNID

	Call OpenRs(rsSet, SQL)
	if (NOT rsSet.EOF) then
		CNID_INT	= rSset("CNID")
		CNID  		= rsSet("FORMATCNID") 	
		If (rsSet("ORDERID") <> "") Or (Not IsNull(rsSet("ORDERID"))) Then
			PONUMBER 	= PurchaseNumber(rsSet("ORDERID"))
		Else
			PONUMBER = ""
		End If
		CNNAME 		= rsSet("CNNAME")
		CNDATE 		= FormatDateTime(rsSet("CNDATE"),1)
		SUPPLIER	= rsSet("NAME")
		SUPPLIERID 	= rsSet("SUPPLIERID")	
		CNSTATUSNAME = rsSet("POSTATUSNAME")
		CNNOTES 	= rsSet("CNNOTES")
		
	end if
	Call CloseRs(rsSet)


	SQL = "SELECT PI.ORDERITEMID, PI.PAIDON, PI.ITEMNAME, INV.TAXDATE, PI.ITEMDESC, PI.PIDATE, " &_
			"ABS(PI.NETCOST) AS NETCOST, ABS(PI.VAT) AS VAT, V.VATNAME, ABS(PI.GROSSCOST) AS GROSSCOST, " &_
			"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE FROM F_PURCHASEITEM PI " &_
			"INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CR ON CR.ORDERITEMID = PI.ORDERITEMID " &_
			"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
			"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
			"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
			"LEFT JOIN F_ORDERITEM_TO_INVOICE OTOI ON OTOI.ORDERITEMID = PI.ORDERITEMID " &_
			"LEFT JOIN F_INVOICE INV ON INV.INVOICEID = OTOI.INVOICEID " &_
			"WHERE PI.ACTIVE = 1 AND CR.CNID = " & C_CNID  & " ORDER BY PI.ORDERITEMID DESC"
	
	PIString = ""
	TotalNetCost = 0
	TotalVAT = 0
	TotalGrossCost = 0
	TotalReconcilableNetCost = 0
	TotalReconcilableVAT = 0
	TotalReconcilableGrossCost = 0
	TotalRows = 0
	RowsThatCanBeCanceld = 0
	RowsThatWillBeCanceld = 0
	
	Call OpenRs(rsPI, SQL)		
	while (NOT rsPI.EOF) 
		
		OrderItemID = rsPI("ORDERITEMID")
	
		NetCost = rsPI("NETCOST")
		VAT = rsPI("VAT")
		GrossCost = rsPI("GROSSCOST")
		
		TotalNetCost = TotalNetCost + NetCost
		TotalVAT = TotalVAT + VAT
		TotalGrossCost = TotalGrossCost + GrossCost
		
		DataStorage = "<INPUT TYPE=""HIDDEN"" NAME=""DS" & OrderItemID & """ VALUE=""" & NetCost & "|" & VAT & "|" & GrossCost & """>"
		UserName_Reason = "<INPUT id=""UserName_Reason"" TYPE=""HIDDEN"" NAME=""DSUserName_Reason"" ""  >"
	
		PITYPE = rsPI("PITYPE")
		PISTATUS = rsPI("PISTATUS")
	
		RowsThatCanBeCanceld = RowsThatCanBeCanceld + 1

		CheckData = "<INPUT TYPE=""CHECKBOX"" NAME=""CHKS"" ID=""CHKS"" VALUE=""" & OrderItemID & """ CHECKED onclick=""UpdateCancelTotal()"">"
		RowsThatWillBeCanceld = RowsThatWillBeCanceld + 1
		TotalReconcilableNetCost = TotalReconcilableNetCost + NetCost
		TotalReconcilableVAT = TotalReconcilableVAT + VAT
		TotalReconcilableGrossCost = TotalReconcilableGrossCost + GrossCost
	
		'PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD><TD STYLE='VISIBILITY:HIDDEN'>" & CheckData & "</TD></TR>"
		PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT>" & DataStorage &UserName_Reason & rsPI("ITEMNAME") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD></TR>"
		ItemDesc = rsPI("ITEMDESC")
		if ( NOT(ItemDesc = "" OR isNULL(ItemDesc)) ) then
			PIString = PIString & "<TR ID=""PIDESCS"" STYLE=""DISPLAY:NONE""><TD width=410 colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"	
		end if
		rsPI.moveNext
	wend
	
	if (RowsThatWillBeCanceld = RowsThatCanBeCanceld) then
		DisplayText = "This Credit Note will become fully cancelled"
	else 
		if (RowsThatWillBeCanceld = 0) then
			DisplayText = "No rows have or can be selected therefore no cancellation will occur."
			DisabledButton = " disabled"
		elseif (RowsThatCanBeCanceld - RowsThatWillBeCanceld = 1) then
			DisplayText = "This Credit Noter will not be fully Cancelled as there is " & (RowsThatCanBeCanceld - RowsThatWillBeCanceld) & " item that has<br> not or can not be selected."
		else
			DisplayText = "This Credit Note will not be fully Cancelled as there are " & (RowsThatCanBeCanceld - RowsThatWillBeCanceld) & " items that have<br> not or can not be selected."
		end if						
	end if
	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Credit Note > Cancellation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 25%;
    top: 25%;
    width: 25%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0),0 6px 20px 0 rgba(0,0,0,0);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:0}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:0}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #000000;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var RowsThatWillBeCanceld = <%=RowsThatWillBeCanceld%>
	var RowsThatCanBeCanceld = <%=RowsThatCanBeCanceld%>
	
	function FormatCurrrenyComma(Figure){
	
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}

	function UpdateCancelTotal(){
		iChecks = document.getElementsByName("CHKS")
		NetValue = 0
		TheVAT = 0
		GrossValue = 0
		if (iChecks.length){
			RowsThatWillBeCanceld = 0
			for (i=0; i<iChecks.length; i++){
				if (iChecks[i].checked == true){
					ORDERITEMID = iChecks[i].value
					FieldData = document.getElementById("DS" + ORDERITEMID).value
					FieldArray = FieldData.split("|")
					NetValue += parseFloat(FieldArray[0])
					TheVAT += parseFloat(FieldArray[1])
					GrossValue += parseFloat(FieldArray[2])
					RowsThatWillBeCanceld++
					}
				}
			}
		document.getElementById("NET").innerHTML = FormatCurrrenyComma(NetValue)
		document.getElementById("VAT").innerHTML = FormatCurrrenyComma(TheVAT)
		document.getElementById("GROSS").innerHTML = FormatCurrrenyComma(GrossValue)				
		document.getElementById("CancelButton").disabled = false
		if (RowsThatWillBeCanceld == RowsThatCanBeCanceld)
			document.getElementById("INFO").innerHTML = "This Credit Note will become fully cancelled"
		else {
			if (RowsThatWillBeCanceld == 0) {
				document.getElementById("CancelButton").disabled = true
				document.getElementById("INFO").innerHTML = "No rows have or can be selected therefore no cancellation will occur."
				}
			else if (RowsThatCanBeCanceld - RowsThatWillBeCanceld == 1)
				document.getElementById("INFO").innerHTML = "This Credit Note will not be fully cancelled as there is " + (RowsThatCanBeCanceld - RowsThatWillBeCanceld) + " item that has not or can not be selected."
			else
				document.getElementById("INFO").innerHTML = "This Credit Note will not be fully cancelled as there are " + (RowsThatCanBeCanceld - RowsThatWillBeCanceld) + " items that have not or can not be selected."
			}						
		}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}		
		
	function Cancel(){
	
		if (! window.confirm("Are you sure you want to cancel this credit note ?"))
			return false;
		var ReasonPopUp = document.getElementById('ReasonPopUp');
		ReasonPopUp.style.display="block";
		document.getElementById("CancelButton").disabled = true

		}

	function UpdateChecks(){
		if (document.getElementById("MegaClick").checked == true)
			NewStatus = true
		else
			NewStatus = false
		ChkRef = document.getElementsByName("CHKS")
		if (ChkRef.length){
			for (i=0; i<ChkRef.length; i++){
				ChkRef[i].checked = NewStatus
				}
			}
		UpdateCancelTotal()
		}

<% if Request("ER12" & Replace(Date, "/", "")) = 1 then %>
	alert("A user is processing some invoices to be paid.\nPlease wait a few seconds before re-processing your actions.")
<% end if %>	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border="0" cellspacing="5" cellpadding="0">
<FORM name=RSLFORM method=POST>
<TR><TD>
<table cellspacing=0 cellpadding=0><tr><td>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=490PX>
		<TR>
			<TD WIDTH=90PX>Credit Note No:</TD><TD><%=CNID%></TD>
		</TR><TR>
			<TD WIDTH=90PX>PO No:</TD><TD><%=PONUMBER%></TD>
		</TR><TR>
			<TD>Description:</TD><TD><%=CNNAME%></TD>
		</TR><TR>		
			      <TD>Order Date:</TD>
                  <TD><%=CNDATE%></TD>
		</TR><TR>		
			<TD>Supplier:</TD><TD><%=SUPPLIER%></TD>						
		</TR>
	</TABLE>
</td><td width=10>&nbsp;</TD><td valign=top>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=255PX height=100>
		<TR>
			<TD height=100% colspan=3 VALIGN=TOP>
				<font color=red STYLE='FONT-SIZE:15'><div id="INFO"><%=DisplayText%></div></font>
			</TD>
		</TR>
	</TABLE>
</td></tr></table>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=410 NOWRAP><table cellspacing=0 cellpadding=0 width=410><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=80 nowrap><b>Net (�):</b></TD>
	  <TD width=75 nowrap><b>VAT (�):</b></TD>
	  <TD width=80 nowrap><b>Gross (�):</b></TD>
	  <!--TD width=30><input type=checkbox name="MegaClick" checked onclick="UpdateChecks()"></TD-->		  
	</TR>
	<%=PIString%> 
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	</TR>
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL CANCEL AMOUNT : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="NET"><%=FormatNumber(TotalReconcilableNetCost,2)%></div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><div id="VAT"><%=FormatNumber(TotalReconcilableVAT,2)%></div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="GROSS"><%=FormatNumber(TotalReconcilableGrossCost,2)%></div></b></TD>
 	</TR>
  </TABLE>
<BR>  
  <TABLE cellspacing=0 cellpadding=2 width=755>
	<TR ALIGN=RIGHT> 
	  <TD>
	  <input type="HIDDEN" name="hid_CNID" value="<%=CNID_INT%>">
	  <input type="BUTTON" id = "CancelButton" name="CancelButton" value=" CANCEL " onclick="Cancel()" class="RSLButton" <%=DisabledButton%>>
	  </TD>		  
	</TR>
  </TABLE>
</TD></TR>
</FORM>
</table>
<IFRAME src="/secureframe.asp" NAME="CANCELCREDITNOTE" STYLE='DISPLAY:NONE'></IFRAME>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<div id="ReasonPopUp" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
     <!--<span id= "close" class="close">&times;</span> -->
      <h2>Reasons for Cancellation</h2>
    </div>
    <div class="modal-body">
	<br>
	<select id="Reasons" onchange="OnReasonChange()">
		<option value="" disabled selected>Please Select</option>
		<option value="Incorrect Supplier">Incorrect Supplier</option>
		<option value="Credit Note duplicated">Credit Note duplicated</option>
		<option value="Credit Note no longer required">Credit Note no longer required</option>
		<option value="Credit Note raised in error">Credit Note raised in error</option>
</select>
<br><br>
<button id="saveReason" disabled onclick="CancelPO()">Save Reason</button>
<br>
    </div>
  </div>
</div>
<script>
var ReasonPopUp = document.getElementById('ReasonPopUp');
//var btn = document.getElementById("myBtn");
//var span = document.getElementById("close");
//span.onclick = function() {
//	document.getElementById("ReconcileButton").disabled=false;
//    modal.style.display = "none";
//	ReasonPopUp.style.display="none";
//	var inputs = document.querySelectorAll("input[type='checkbox']");
//for(var i = 0; i < inputs.length; i++) {
 //   inputs[i].disabled = false;   
//}
//}
function OnReasonChange()
{
var value =document.getElementById("Reasons").value;
if(value ==null)
document.getElementById("saveReason").disabled=true;
else 
document.getElementById("saveReason").disabled=false;
}

function CancelPO()
{	
	var reason=document.getElementById("Reasons").value;
	document.getElementById("UserName_Reason").value=reason+"|"+<%=UserId%>	
	document.getElementById("CancelButton").disabled = true
	document.getElementById("CancelButton").value = " Submitted "
	RSLFORM.action = "ServerSide/CancelCreditNote_svr.asp"
	RSLFORM.submit();
}
</script>
</BODY>
</HTML>
