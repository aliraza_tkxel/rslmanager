<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<script src="../js/jquery-1.6.2.min.js" type="text/javascript"></script>
<link href="../css/Finance/GoodsReceived.css" rel="stylesheet" type="text/css" />
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderUploadInvoice.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (7)	 'USED BY CODE
	Dim DatabaseFields (7)	 'USED BY CODE
	Dim ColumnWidths   (7)	 'USED BY CODE
	Dim TDSTUFF        (7)	 'USED BY CODE
	Dim TDPrepared	   (7)	 'USED BY CODE
	Dim ColData        (8)	 'Syntax	[column title] | [database field] | [display length(px)]
	Dim SortASC        (7)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (7)	 'All Array sizes must match
	Dim TDFunc		   (7)
    Dim ColSpan
	Dim clean_desc

	ColData(0)  = "Order #|ORDERID|100"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|80"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"
	TDSTUFF(1)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(1) = ""

	ColData(2)  = "By|FULLNAME|110"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"
	TDSTUFF(2)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(2) = ""

    ColData(3)  = "Supplier|SUPPLIER|130"
	SortASC(3) 	= "SUPPLIER ASC"
	SortDESC(3) = "SUPPLIER DESC"
	TDSTUFF(3)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(3) = ""

	ColData(4)  = "Item|PONAME|175"
	SortASC(4) 	= "PONAME ASC"
	SortDESC(4) = "PONAME DESC"
	TDSTUFF(4)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(4) = ""

	ColData(5)  = "Status|POSTATUSNAME|100"
	SortASC(5) 	= "PS.POSTATUSNAME ASC"
	SortDESC(5) = "PS.POSTATUSNAME DESC"
	TDSTUFF(5)  = " "" onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """
	TDFunc(5) = ""

	ColData(6)  = "Cost|FORMATTEDCOST|55"
	SortASC(6) 	= "TOTALCOST ASC"
	SortDESC(6) = "TOTALCOST DESC"
	TDSTUFF(6)  = " ""onclick=""""show_detail("" & rsSet(""ORDERID"") & "")"""" """ ' " ""align='right'"" "
	TDFunc(6) = "FormatCurrency(|)"

    ' leave it empty if we want to add a column with some button
    ColData(7) = "||55"
   

	PageName = "UploadInvoice.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"    
	DefaultOrderBy = SortDESC(0)
	'RowClickColumn = """ userId="""""" & rsSet(""USERID"") & """""" postatusid = """""" & rsSet(""POSTATUSID"") & """""" finalpostatusname = """""" & rsSet(""FINALPOSTATUSNAME"") & """"""   class='order-row' TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") &"""" """
  
	'RowClickColumn = """ userId="""""" & rsSet(""USERID"") & """""" orderid = """""" & rsSet(""ORDERID"") & """""" postatusid = """""" & rsSet(""POSTATUSID"") & """""" finalpostatusname = """""" & rsSet(""FINALPOSTATUSNAME"") & """"""   class='order-row' TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """
    'RowClickColumn = "  "" class='order-row' TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 
    RowClickColumn = "  "" class='order-row' TITLE="""""" & rsSet(""POSTATUSNAME"") & """""""""
    
    
	

    
    CURRENTPO = REQUEST("CPO")
	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	If (Request("PONumber") <> "") Then
		If (isNumeric(Request("PONumber"))) Then
			POFilter = " AND PO.ORDERID = '" & Clng(Request("PONumber")) & "' "
		End If
	End If

	SupFilter = ""
	rqSupplier = Request("sel_SUPPLIER")
	If ( rqSupplier <> "") Then
		SupFilter = " AND PO.SUPPLIERID = '" & rqSupplier & "' "
	End If

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if

	rqFiscalYear = ""
	rqFiscalYear = Request("sel_FISCALYEAR")
	If (rqFiscalYear <> "") Then
	    SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & rqFiscalYear
	    Call OpenDB()
	    Call OpenRs(rsFiscalYear, SQL)
	    If (NOT rsFiscalYear.EOF) Then
		    FiscalFilter = " AND (PO.PODATE >= '" & FormatDateTime(rsFiscalYear("YSTART"),1) & "' and PO.PODATE <= '" & FormatDateTime(rsFiscalYear("YEND"),1) & "')"
		End If
	    Call CloseRs(rsFiscalYear)
	    Call CloseDB()
	End If
	if(Request("BUDGETFILLTER")="checked") then			  
			  		SQLCODE =   "DECLARE @POSTATUSID INT  " & _
                "DECLARE @WOSTATUSID INT  " & _
              "DECLARE @FINALPOSTATUSNAME NVARCHAR(100)  " & _
               "DECLARE @FINALWOSTATUSNAME NVARCHAR(100)  " & _
              "Select @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
              "Select @WOSTATUSID = PS.POSTATUSID, @FINALWOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
              "SELECT DISTINCT PO.USERID, " & _
              "       PS.POSTATUSNAME, " & _ 
              "       E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " & _ 
              "       PO.ORDERID, " & _ 
              "       O.NAME AS SUPPLIER, " & _ 
              "       CONVERT(VARCHAR,ISNULL(PI.TOTALCOST,0),1) AS FORMATTEDCOST,  " & _ 
              "       ISNULL(PI.TOTALCOST,0) AS TOTALCOST,  " & _ 
              "       PS.POSTATUSNAME, " & _ 
              "       PONAME, " &_
              "       FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103), " & _ 
              "       CASE WHEN  PO.POTYPE = 2 THEN @WOSTATUSID ELSE @POSTATUSID END AS POSTATUSID, " & _
              "       CASE WHEN  PO.POTYPE = 2 THEN @FINALWOSTATUSNAME ELSE @FINALPOSTATUSNAME END AS FINALPOSTATUSNAME, " & _ 
              "       PODATE "&_
              " FROM F_PURCHASEORDER PO " &_
              " INNER JOIN (SELECT ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PIStatus IN (18) GROUP BY ORDERID) PI1 ON PI1.ORDERID = PO.ORDERID " &_
              " CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) PI " &_
              " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " &_ 
              " LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID " &_
              " LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
              " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
              " WHERE PO.ACTIVE = 1  " &_
              " AND PO.USERID=" & Session("UserID") & " " & SupFilter & CompanyFilter & POFilter & FiscalFilter & " and  po.POSTATUS  in (18) " &_
              " Order By " + Replace(orderBy, "'", "''") + ""

			  else
	SQLCODE =   "DECLARE @POSTATUSID INT  " & _
                "DECLARE @WOSTATUSID INT  " & _
              "DECLARE @FINALPOSTATUSNAME NVARCHAR(100)  " & _
               "DECLARE @FINALWOSTATUSNAME NVARCHAR(100)  " & _
              "Select @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
              "Select @WOSTATUSID = PS.POSTATUSID, @FINALWOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
              "SELECT DISTINCT PO.USERID, " & _
              "       PS.POSTATUSNAME, " & _ 
              "       E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " & _ 
              "       PO.ORDERID, " & _ 
              "       O.NAME AS SUPPLIER, " & _ 
              "       CONVERT(VARCHAR,ISNULL(PI.TOTALCOST,0),1) AS FORMATTEDCOST,  " & _ 
              "       ISNULL(PI.TOTALCOST,0) AS TOTALCOST,  " & _ 
              "       PS.POSTATUSNAME, " & _ 
              "       PONAME, " &_
              "       FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103), " & _ 
              "       CASE WHEN  PO.POTYPE = 2 THEN @WOSTATUSID ELSE @POSTATUSID END AS POSTATUSID, " & _
              "       CASE WHEN  PO.POTYPE = 2 THEN @FINALWOSTATUSNAME ELSE @FINALPOSTATUSNAME END AS FINALPOSTATUSNAME, " & _ 
              "       PODATE "&_
              " FROM F_PURCHASEORDER PO " &_
              " INNER JOIN (SELECT ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PIStatus IN (18) GROUP BY ORDERID) PI1 ON PI1.ORDERID = PO.ORDERID " &_
              " CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) PI " &_
              " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " &_ 
              " LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID " &_
              " LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
              " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
			  " INNER JOIN F_EMPLOYEELIMITS ex ON ex.EXPENDITUREID=PIApprovedFilter.EXPENDITUREID and ex.EMPLOYEEID = "& Session("UserID")&" " &_
              " WHERE PO.ACTIVE = 1  " &_
              " " & SupFilter & CompanyFilter & POFilter & FiscalFilter & " and  po.POSTATUS  in (18) " &_
              " Order By " + Replace(orderBy, "'", "''") + ""
			  
			  end if
'" WHERE PO.POSTATUS IN (0, 1, 2, 3, 7, 8, 17, 18) AND PO.ACTIVE = 1 " &_
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
'Response.Write( SQLCODE )
 '   Response.End()
	'Response.flush
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

	Call Create_Table()

	Call OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select Supplier", rqSupplier, NULL, "textbox200", " style=""width:240px"" ")
	Call BuildSelect(lstFiscalYear, "sel_FISCALYEAR", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, Yend, 103)", "CONVERT(VARCHAR, YSTART, 103)+ ' - ' + CONVERT(VARCHAR, Yend, 103)", "Please Select...", rqFiscalYear, NULL, "textbox200", " style=""width:180px"" ")
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Goods Received List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

    function show_detail(Order_id) {
	    if (window.showModelessDialog) {        // Internet Explorer
		    window.showModalDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
		  } 
        else {
            window.open ("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "","width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
        }
    }
    function load_me(OID)
	{    
    setSupplier();
	if(document.getElementById("budgetFillter").value!="on")
	location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&sel_SUPPLIER="+"<%=Request("sel_SUPPLIER")%>";
	else 
	location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&page=" & intpage%>&CPO=' + OID+"&sel_SUPPLIER="+"<%=Request("sel_SUPPLIER")%>"+"&BUDGETFILLTER=checked";
	
	setSupplier();
	}

 


    function RemoveBad() { 
        strTemp = event.srcElement.value;
        strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
        event.srcElement.value = strTemp;
    }

    function setSupplier() {
	    document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = "<%=Request("sel_SUPPLIER")%>";
	}
         function setCompany() {
            var company = '<%=Request("sel_COMPANY")%>';
            document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value = company;
          }
    function SubmitPage() {
    var PONumber = document.getElementById("PONumber").value;
    var SupplierNumber = document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value;
    var FiscalYear = document.getElementById("sel_FISCALYEAR").options[document.getElementById("sel_FISCALYEAR").selectedIndex].value;
	var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
    if (isNaN(PONumber)) {
		alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
		}
	if (document.getElementById("budgetFillter").checked == true)
		{
			location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + thisForm.PONumber.value  + "&sel_FISCALYEAR=" + FiscalYear+ "&BUDGETFILLTER=checked";
		}
	else
		{
			location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + thisForm.PONumber.value  + "&sel_FISCALYEAR=" + FiscalYear+ "&BUDGETFILLTER=unchecked";;
		}
	//location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + SupplierNumber + "&PONumber=" + PONumber + "&sel_FISCALYEAR=" + FiscalYear;
	//location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + thisForm.PONumber.value  + "&sel_FISCALYEAR=" + FiscalYear;;
	}

    $( document ).ready(function() {
        $("tr.order-row").each(function() {
            var self = $(this);
            self.attr('style','');
            var status = self.attr('title').toLowerCase();
            if (status == 'goods ordered' ||  status == 'invoice received' ||  status == 'invoice approved' ||  status == 'work ordered' ){ //||  status == 'invoice received' || status == 'queued'
               // self.append("<td STYLE='border: 1px solid black;'><input type='button' value='Goods Received' onclick='javascipt:updatePOStatus(this,0,0);'></td>");
               self.append("<td STYLE='border: 1px solid black;'></td>");
                
            } else {
               self.append("<td></td>");
            }
        });
    });
	function ShowPopUp(refNo,OrderName,ORDERID,OrignalGrossCost,COMPANYID)
	{
	debugger;
	var Order_id=ORDERID;


	//window.open ("Popups/UploadInvoicePopUp.asp?OrderID=305309-442878&RefNo=001&amount=100&name=Dining&COMPANYID=1208&ischild=1&updateparent=0&Random=" + new Date(), "","width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
	
	
	window.open ("Popups/UploadInvoicePopUp.asp?OrderID=" + Order_id + "&RefNo="+refNo+"&amount="+OrignalGrossCost+"&OrderName="+OrderName+"&COMPANYID="+COMPANYID+"&Random="+new Date(), "","width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
	
	//window.open ("Popups/UploadInvoicePopUp.asp?Random=" + new Date(), "","width=450px, height=460px, alwaysRaised=yes, scrollbars=no ");
	
	//alert(OrignalGrossCost);
	}
    function updatePOStatus(self,child,updateParent,totalRecord) {
        var parent = $(self).parents('.order-row');
       
        SetButtonState(true, totalRecord);
        var jqXHR = $.ajax({
              url: 'UpdatePOStatus.asp',
              type: 'get',
              //data: { orderid: parent.attr('orderid'), userId: parent.attr('userId'), postatusid: parent.attr('POSTATUSID'), postatusname: parent.attr('finalpostatusname'), ischild: child,updateParent: updateParent},
              data: { orderid: parent.attr('orderid'), userId: parent.attr('userId'), postatusid: 18, postatusname: parent.attr('finalpostatusname'), ischild: child,updateParent: updateParent},
              cache: false,
              success: function(data){ 
                parent.find("td:nth-child(5)").text(data)
                //alert(data);
                SubmitPage();
              },
           
              error: function(){
               // alert(jqXHR.responseText);
                alert('Error!');
              }
        });
    }
    function SetButtonState(diableState,totalRecord) {
           for (i = 1; i <= totalRecord; i++) {
                 
                if (document.getElementById("btn_approve" + i) != null) {
                  document.getElementById("btn_approve" + i).disabled = diableState;
                }
            }
        }

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages();setSupplier();setCompany()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <div id="LOADINGTEXT_BOTTOM">
    
    <table class="RSLBlack">
       <tr>
                <td></td>
                <td>
                    <b>Order No: </b>
                </td>
                <td>
                    <b></b>
                </td>
                <td>
                    <b> </b>
                </td>
                <td></td>
            </tr>
        <tr>
            <td>
            </td>
            <td>
                <input type="text" name="PONumber" id="PONumber" class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>"
                    onblur="RemoveBad()" style="width: 135px; background-image: url('img/CXfilter.gif');
                    background-repeat: no-repeat; background-attachment: fixed" />
            </td>
            <td>
              <input type="checkbox" name="budgetFillter" id="budgetFillter" onclick="SubmitPage()" <%=Request("BUDGETFILLTER")%>><b>Budget Fillter:<b>
            </td>
            <td>
                
            </td>
            <td>
                
            </td>
        </tr>
         <tr>
                <td></td>
                <td>
                    <b>Suppliers: </b>
                </td>
                <td>
                    <b>Company: </b>
                </td>
                <td>
                    <b>Fiscal Years: </b>
                </td>
                <td></td>
            </tr>
        <tr>
                <td></td>
                <td>
                    <%=lstSuppliers%>

                </td>
                <td>
                    <%=lstCompany%>
                </td>
                <td>
                    <%=lstfiscalyear%>
                </td>
                <td>
                     <input type="button" class="RSLButton" value=" Update Search " title="Update Search"
                    onclick="SubmitPage()" />
                </td>
            </tr>

    </table>
    </div>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="750" height="1" alt="" />
            </td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>