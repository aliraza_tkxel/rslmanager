<%
	' IPAGE	:	PAGENUMBER
	' SD	:	STARTDATE OF SQL
	' ED	:	ENDDATE OF SQL
	' ACCID	:	ACCOUNTID OF REQUIRED NOMINAL
	Function get_balance(IPAGE, SD, ED, ACCID, PSIZE)
		
		Dim b, ob
		TOP_SQL = " TOP " & (PSIZE * (iPage-1)) & " " 
		
		' CHECK FOR OFFICIAL OPENING BALANCES
		SQL = "SELECT ISNULL(SUM(DEBIT) - SUM(CREDIT),0) AS OB FROM ( " &_
				"		SELECT 	ISNULL(D.AMOUNT,0) AS DEBIT, 0 AS CREDIT, D.ACCOUNTID, A.PARENTREFLISTID, TXNID " &_
				"		FROM 	NL_JOURNALENTRYDEBITLINE D " &_
				"				INNER 	JOIN NL_ACCOUNT A ON A.ACCOUNTID = D.ACCOUNTID " &_
				"			  UNION ALL " &_
				"		SELECT 	0 AS DEBIT, ISNULL(C.AMOUNT,0) AS CREDIT, C.ACCOUNTID, A.PARENTREFLISTID, TXNID " &_
				"		FROM 	NL_JOURNALENTRYCREDITLINE C " &_
				"				INNER 	JOIN NL_ACCOUNT A ON A.ACCOUNTID = C.ACCOUNTID) UI " &_
				"		INNER JOIN NL_JOURNALENTRY J ON J.TXNID = UI.TXNID " &_
				"	WHERE (UI.ACCOUNTID = " & ACCID & " OR UI.PARENTREFLISTID = " & ACCID & ") AND J.TRANSACTIONTYPE = 9 "
		'RW SQL & "<br>"
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then ob = rsSet("OB") Else ob = 0 End If
		CloseRs(rsSet)

		' GET CARRIED FORWARD POSITION BASED ON PAGE VALUE
		SQL = 	"SELECT  	ISNULL(SUM(DB.AMOUNT),0) - ISNULL(SUM(CR.AMOUNT),0) AS BALANCE " &_
				"FROM 		NL_ACCOUNT A " &_
				"			LEFT JOIN (SELECT " & TOP_SQL & " AMOUNT, AD.ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE D " &_
				"		    	INNER JOIN NL_JOURNALENTRY J ON D.TXNID = J.TXNID " &_
				"				INNER JOIN NL_ACCOUNT AD ON AD.ACCOUNTID = D.ACCOUNTID " &_
				"		    	WHERE J.TRANSACTIONTYPE <> 9 AND J.TXNDATE >= '" & SD & " ' AND J.TXNDATE <= '" & ED & "' AND " &_
				"		   	    (AD.ACCOUNTID = " & ACCID & " OR AD.PARENTREFLISTID = " & ACCID & ") " &_
				"				ORDER BY D.TXNDATE ASC, LINEID ASC) DB " &_
				"			  	ON A.ACCOUNTID = DB.ACCOUNTID " &_
				"			LEFT JOIN (SELECT " & TOP_SQL & " AMOUNT, AC.ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE C " &_
				"			    INNER JOIN NL_JOURNALENTRY J ON C.TXNID = J.TXNID " &_
				"				INNER JOIN NL_ACCOUNT AC ON AC.ACCOUNTID = C.ACCOUNTID " &_					
				"			    WHERE J.TRANSACTIONTYPE <> 9 AND J.TXNDATE >= '" & SD & " ' AND J.TXNDATE <= '" & ED & "' AND " &_
				"		   	    (AC.ACCOUNTID = " & ACCID & " OR AC.PARENTREFLISTID = " & ACCID & ") " &_
				"				ORDER BY C.TXNDATE ASC, LINEID ASC) CR " &_
				"			  	ON A.ACCOUNTID = CR.ACCOUNTID " &_
				"WHERE		A.ACCOUNTID = " & ACCID
		'RW SQL & "<br>"
		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then b = rsSet("BALANCE") Else b = 0 End If
		get_balance = b + ob
		CloseRs(rsSet)
		
	End Function

	' GET ACCOUNT DETAILS
	' REQUIRES VARIABLES ACCOUNTNUMBER, FULLNAME, ACCOUNTTYPE TO BE DECLARED ON CALLING PAGE
	Function get_account_details(ACCID)
	
		' GET ACCOUNT DETAILS
		SQL = "SELECT 	ISNULL(A.FULLNAME,'N/A') AS FULLNAME, " &_
				"		ISNULL(A.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
				"		ISNULL(T.DESCRIPTION,'N/A') AS AC_TYPE " &_
				"FROM	NL_ACCOUNT A LEFT JOIN NL_ACCOUNTTYPE T ON A.ACCOUNTID = T.ACCOUNTTYPEID " &_
				"WHERE	A.ACCOUNTID = " & ACCID

		Call OpenRS(rsSet, SQL)
		If Not rsSet.EOF Then
			AccountNumber = rsSet("ACCOUNTNUMBER")
			FullName = rsSet("FULLNAME")
			AccountType = rsSet("AC_TYPE")
		End If
		CloseRs(rsSet)
		
	End Function 
	
	' GET THE START AND END DATE FOR THE CURRENT FISCAL YEAR
	' MUST DECLARE STARTDATE
	' MUST DECLARE ENDDATE
	Function get_year()

		set rsFYear = Server.CreateObject("ADODB.Recordset")
		rsFYear.ActiveConnection = RSL_CONNECTION_STRING
		today = FormatDateTime(Date, 1)
		rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
		rsFYear.CursorType = 0
		rsFYear.CursorLocation = 2
		rsFYear.LockType = 3
		rsFYear.Open()
		if (NOT rsFYear.EOF) then
			StartDate = rsFYear("YStart")
			EndDate = rsFYear("YEnd")		
		end if
		rsFYear.close
	
		PS = CDate(FormatDateTime(StartDate,1))
		PE = CDate(FormatDateTime(Enddate,1))
	
	End function
%>