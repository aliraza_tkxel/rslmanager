<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance </TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<link rel="stylesheet" href="/js/Tree/dtree.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Tree/Tree_TwoCols.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function OpenWin(ACCNUM, ACCID){
	//alert ("Ledger.asp?LID=" + ACCID + "&AN=" + ACCNUM + "&Random=" + new Date())
	window.open("Ledger.asp?LID=" + ACCID + "&AN=" + ACCNUM + "&Random=" + new Date(), "_blank", "TOP=100, LEFT=200, scrollbars=yes, height=610, width=750, status=YES, resizable= Yes")	
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<div class="dtree" style='height:100%'>

	<script type="text/javascript">
		<!--

<% if Request("BAL") = 1 then %>
	alert("Ledger Balances updated successfully.")
<% end if %>
		d = new dTree('d');
		d.config.useStatusText=true;
		d.config.useIcons=false;		
					
		d.add(0,-1,'<b style="color:steelblue">Account Number &nbsp;&nbsp; &nbsp;&nbsp; Account Name</b>','','','','<b style="color:steelblue">Account Type</b>&nbsp;&nbsp;[<a href="javascript:d.openAll()"><b style="color:silver">open all</b></a>][<a href="javascript:d.closeAll()"><b style="color:silver">close all</b></a>][<a href="NominalLedgerSetup	.asp"><b style="color:silver">UPDATE</b></a>]','<b>Balance</b>');
<%
dim rsLEVEL2, rsLEVEL3, rsLEVEL1

' Use a datashaping connection string to be used later.
RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

strSQL = "SHAPE {SELECT 	ISNULL(TOPLEVEL.ACCOUNTID,'N/A') AS ACCOUNTID, " &_
		 " 					ISNULL(TOPLEVEL.ACCOUNTNUMBER,'N/A') AS ACCOUNTNUMBER, " &_
		 "					LTRIM(ISNULL(TOPLEVEL.ACCOUNTNUMBER,'') + ' ' + ISNULL(TOPLEVEL.FULLNAME,'')) AS LEVEL1NAME, " &_
		 "					ISNULL(AT.DESCRIPTION,'N/A') AS ACCOUNTTYPE, " &_
		 "					ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0) AS DEBIT, " &_
		 "					ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0) AS CREDIT, " &_
		 "					(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) AS BALANCE " &_
		 "					FROM 	NL_ACCOUNT TOPLEVEL " &_
		 "					LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE GROUP BY ACCOUNTID) DBNOCHILD " &_
		 "					  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE GROUP BY ACCOUNTID) CRNOCHILD " &_
		 "				 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID GROUP BY PARENTREFLISTID) DBCHILD " &_
	  	 "						ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "					LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID GROUP BY PARENTREFLISTID) CRCHILD " &_
		 "						  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "WHERE			TOPLEVEL.PARENTREFLISTID IS NULL " &_
		 "ORDER 	BY LEVEL1NAME} " &_
		" APPEND((SHAPE {SELECT 	A.PARENTREFLISTID, A.ACCOUNTID, A.ACCOUNTNUMBER, A.ACCOUNTNUMBER + ' ' + A.NAME AS LEVEL2NAME, " &_
		"							AT.DESCRIPTION AS ACCOUNTTYPE, " &_
		"							ISNULL(DR.AMOUNT,0) AS DEBIT, ISNULL(CR.AMOUNT,0) AS CREDIT, " &_
		"							ISNULL(DR.AMOUNT,0) - ISNULL(CR.AMOUNT,0) AS BALANCE " &_
		"				FROM 	NL_ACCOUNT A " &_
		"						INNER JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = A.ACCOUNTTYPE " &_
		"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE GROUP BY ACCOUNTID) CR ON CR.ACCOUNTID = A.ACCOUNTID " &_
		"						LEFT JOIN (SELECT SUM(AMOUNT) AS AMOUNT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE GROUP BY ACCOUNTID) DR ON DR.ACCOUNTID = A.ACCOUNTID " &_
		"				WHERE	PARENTREFLISTID IS NOT NULL ORDER 	BY FULLNAME}) " &_
		" AS LEVEL2 RELATE ACCOUNTID TO PARENTREFLISTID)"

' Open original recordset
Set rsLEVEL1 = Server.CreateObject("ADODB.Recordset")
rsLEVEL1.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING

currenti = 5
LEVEL2PARENT = 0

Do While Not rsLEVEL1.EOF

	Response.Write "d.add(" & currenti & ",0,'" & rsLEVEL1("LEVEL1NAME") & "','javascript:OpenWin(" & rsLEVEL1("ACCOUNTNUMBER") & "," & rsLEVEL1("ACCOUNTID") & ")','','','" & rsLEVEL1("ACCOUNTTYPE") & "','<b>" & FormatCurrency(rsLEVEL1("BALANCE")) & "</b>');"
	currenti = currenti + 1
	
    ' Set object to child recordset and iterate through
    Set rsLEVEL2 = rsLEVEL1("LEVEL2").Value
    if not rsLEVEL2.EOF then
		LEVEL2PARENT = currenti - 1
        Do While Not rsLEVEL2.EOF
			Response.Write "d.add(" & currenti & "," & LEVEL2PARENT & ",'" & rsLEVEL2("LEVEL2NAME") & "','javascript:OpenWin(" & rsLEVEL2("ACCOUNTNUMBER") & "," & rsLEVEL2("ACCOUNTID") & ")','','','" & rsLEVEL2("ACCOUNTTYPE") & "','" & FormatCurrency(rsLEVEL2("BALANCE")) & "');"
			currenti = currenti + 1			
			
            rsLEVEL2.MoveNext
        Loop
	end if
    rsLEVEL1.MoveNext
Loop

%>		

		document.write(d);
		//-->
	</script>

</div>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

