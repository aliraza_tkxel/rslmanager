<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	set rsFYear = Server.CreateObject("ADODB.Recordset")
	rsFYear.ActiveConnection = RSL_CONNECTION_STRING
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
	rsFYear.CursorType = 0
	rsFYear.CursorLocation = 2
	rsFYear.LockType = 3
	rsFYear.Open()
	if (NOT rsFYear.EOF) then
		StartDate = rsFYear("YStart")
		EndDate = rsFYear("YEnd")		
	end if
	rsFYear.close


	PS = CDate(FormatDateTime(StartDate,1))
	PE = CDate(FormatDateTime(Enddate,1))
	
	OpenDB()
	SQL = "SELECT isnull(OpenBalance,0) as ob, AT.DESCRIPTION, ParentRefFullName, ACCOUNTNUMBER, FULLNAME FROM NL_ACCOUNT A INNER JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = A.ACCOUNTTYPE WHERE LISTID = " & Request("LID")		
	Call OpenRS(rsNL, SQL)
	if (NOT rsNL.EOF) then
		AccountNumber = rsNL("ACCOUNTNUMBER")
		FULLNAME = rsNL("FULLNAME")
		AccountType = rsNL("DESCRIPTION")
		OpeningBalance = CDbl(rsNL("ob"))
	END IF
	CloseRs(rsNL)
	CloseDB()	

	Set Rs = Server.Createobject("ADODB.Recordset")
	Set dbcmd = Server.Createobject("ADODB.Command")

	dbcmd.ActiveConnection = RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "RSL_ALLTRANSACTIONS"
'	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = Request("LID")
	dbcmd.Parameters(2) = Request("AN")	
	dbcmd.Parameters(3) = FormatDateTime(StartDate,1)
	dbcmd.Parameters(4) = FormatDateTime(Enddate,1)		
	Set Rs = dbcmd.execute

		
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000" onload="window.focus()">
<%=AccountNumber %> - <%=FULLNAME%> [<font color=blue><%=AccountType%></font>]<BR>
<br>
Period : <%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%><BR><BR>
<TABLE border=1 style="TABLE-LAYOUT:fixed; border-collapse:collapse;" >
  <COL width=100>  <COL width=100>  <COL width=200>  <COL width=100>  <COL width=100>  <COL width=100>

<TR><TD colspan=5 align=right WIDTH=500 NOWRAP>Opening Balance:&nbsp;</TD><TD width=100 align=right><b><%= FormatCurrency(OpeningBalance)%></b></TD></TR>
<TR STYLE='background-color:black;color:white;font-weight:bold'><TD width=100>TRANS ID</TD><TD WIDTH=100>Date</TD><TD NOWRAP WIDTH=200>Item</TD><TD width=100>Increase</TD><TD width=100>Decrease</TD><TD width=100>Running Balance</TD></TR>
<%
	RunningBalance = OpeningBalance
	While NOT Rs.EOF
		if (CStr(Rs("TCODE")) = "CR") then
			DeCredit = Rs("Amount")
			DeDebit = ""
			Credit = Credit + DeCredit
			RunningBalance = RunningBalance - DeCredit
			DeCredit = FormatCurrency(DeCredit)			
		else
			DeDebit =  Rs("Amount")
			DeCredit = ""
			Debit = Debit + DeDebit
			RunningBalance = RunningBalance + DeDebit			
			DeDebit = FormatCurrency(DeDebit)
		end if
		
		Response.Write "<TR><TD>" & Rs("IDVAL") & "</TD><TD>" & Rs("NDATE") & "</TD><TD>" & LEFT(Rs("DESCRIPTION"),40) & "</TD><TD ALIGN=RIGHT>" & DeDebit & "</TD><TD ALIGN=RIGHT><FONT COLOR=RED>" & DeCredit & "</FONT></TD><TD ALIGN=RIGHT>" & FormatCurrency(RunningBalance) & "</TD></TR>"
		Rs.moveNext
	Wend	
	Rs.close()
	Set Rs = Nothing
	Set dbcmd = Nothing

%>
<TR><TD colspan=5 align=right STYLE='background-color:black;color:white;font-weight:bold'>End Balance:&nbsp;</TD><TD width=100 align=right><b><%= FormatCurrency(RunningBalance)%></b></TD></TR>
</TABLE>
</body>
</html>
