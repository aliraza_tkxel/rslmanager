<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<% Session.LCID = 2057 %>
<html>
<head>
<link rel="StyleSheet" href="/js/Tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/js/Tree/dtree.js"></script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<div class="dtree" style='height:100%'>

	<script type="text/javascript">
		<!--
		d = new dTree('d');
		d.config.useStatusText=true;
		d.config.useIcons=false;		
					
		d.add(0,-1,'<b>Nominal Ledger</b>');
<%
dim rsLEVEL2, rsLEVEL3, rsLEVEL1

' Use a datashaping connection string to be used later.
RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

strSQL = "SHAPE {SELECT ACCOUNTTYPEID, DESCRIPTION AS LEVEL1NAME FROM NL_ACCOUNTTYPE ORDER BY LEVEL1NAME} " &_
		 "	APPEND((SHAPE {SELECT LISTID, ACCOUNTNUMBER + ' ' + FULLNAME AS LEVEL2NAME, ACCOUNTTYPE FROM NL_ACCOUNT WHERE PARENTREFLISTID IS NULL ORDER BY LEVEL2NAME} " &_
		 "		APPEND({SELECT LISTID, ACCOUNTNUMBER + ' ' + FULLNAME AS LEVEL3NAME, PARENTREFLISTID FROM NL_ACCOUNT WHERE PARENTREFLISTID IS NOT NULL ORDER BY LEVEL3NAME} AS LEVEL3 " &_
		 "		RELATE LISTID TO PARENTREFLISTID)) AS LEVEL2 " &_
	     "	RELATE ACCOUNTTYPEID TO ACCOUNTTYPE)"

' Open original recordset
Set rsLEVEL1 = Server.CreateObject("ADODB.Recordset")
rsLEVEL1.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING

currenti = 5
LEVEL2PARENT = 0
LEVEL3PARENT = 0

Do While Not rsLEVEL1.EOF

	Response.Write "d.add(" & currenti & ",0,'" & rsLEVEL1("LEVEL1NAME") & "','LEVEL1.asp?L1ID=" & rsLEVEL1("ACCOUNTTYPEID") & "&L1_A=L','','FB');"
	currenti = currenti + 1
	
    ' Set object to child recordset and iterate through
    Set rsLEVEL2 = rsLEVEL1("LEVEL2").Value
    if not rsLEVEL2.EOF then
		LEVEL2PARENT = currenti - 1
        Do While Not rsLEVEL2.EOF
			Response.Write "d.add(" & currenti & "," & LEVEL2PARENT & ",'" & rsLEVEL2("LEVEL2NAME") & "','LEVEL2.asp?L2ID=" & rsLEVEL2("LISTID") & "&L2_A=L','','FB');"
			currenti = currenti + 1			
			
			Set rsLEVEL3 = rsLEVEL2("LEVEL3").Value
			if not rsLEVEL3.EOF then
				LEVEL3PARENT = currenti - 1			
				Do While Not rsLEVEL3.EOF
					Response.Write "d.add(" & currenti & "," & LEVEL3PARENT & ",'" & rsLEVEL3("LEVEL3NAME") & "','LEVEL3.asp?L3ID=" & rsLEVEL3("LISTID") & "&L3_A=L','','FB');"
					currenti = currenti + 1
					rsLEVEL3.MoveNext
				Loop
				Response.Write "d.add(" & currenti & "," & LEVEL3PARENT & ",'<font color=blue>New Sub Item</font>','LEVEL3.asp?L2ID=" & rsLEVEL2("LISTID") & "&L3_A=LFD','','FB');"
				currenti = currenti + 1				
			else
				LEVEL3PARENT = currenti - 1
				Response.Write "d.add(" & currenti & "," & LEVEL3PARENT & ",'<font color=blue>New Sub Item</font>','LEVEL3.asp?L2ID=" & rsLEVEL2("LISTID") & "&L3_A=LFD','','FB');"
				currenti = currenti + 1	
			end if
			
            rsLEVEL2.MoveNext
        Loop
		Response.Write "d.add(" & currenti & "," & LEVEL2PARENT & ",'<font color=blue>New Item</font>','LEVEL2.asp?L1ID=" & rsLEVEL1("ACCOUNTTYPEID") & "&L2_A=LFD','','FB');"
		currenti = currenti + 1		
    else
		LEVEL2PARENT = currenti - 1
		Response.Write "d.add(" & currenti & "," & LEVEL2PARENT & ",'<font color=blue>New Item</font>','LEVEL2.asp?L1ID=" & rsLEVEL1("ACCOUNTTYPEID") & "&L2_A=LFD','','FB');"
		currenti = currenti + 1	
	end if
    rsLEVEL1.MoveNext
Loop
Response.Write "d.add(" & currenti & ",0,'<font color=blue>New Type</font>','javascript:parent.NewItem(1)');"

%>		

		document.write(d);
		//-->
	</script>

</div>
</body>
</html>
