<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function

	Dim treeText, ExpenditurePurchases
	Dim ProjectUsers, TimeCost, TimeMatched, fundid, HDID,  expname, startdate
	Dim expallocation, active, datecreated, EXID, totalallocated, totalfund, totalremaining, allocated
	Dim nominalcode, controlcode
	
Function GetFormFields ()
	expallocation = Request.Form("txt_expenditureallocation")
	if expallocation = "" then expallocation = null end if
	expname = Request.Form("txt_expenditurename")
	if expname = "" then expname = null end if	
	active = Request.Form("budgetactive")
	if active = "" then active = 0 end if	
	HDID = Request.Form("HDID")
	if HDID = "" then HDID = 0 end if
	nominalcode = Request.Form("txt_nominalcode")
	controlcode = Request.Form("txt_controlcode")	
End Function

Function NewRecord ()
	GetFormFields()

	SQL = "INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREALLOCATION, EXPENDITUREDATE, USERID, ACTIVE, LASTMODIFIED, HEADID, QBDEBITCODE, QBCONTROLCODE) " &_
			"VALUES ('" & expname & "', " &_
					"" & expallocation & ", "&_
					"'" & Date & "', "&_					
					"" & Session("UserID") & ", "&_
					"" & active & ", "&_
					"'" & Date & "', "&_
					"" & HDID & ", "&_ 
					"" & nominalcode & ", "&_ 
					"" & controlcode & ") "

	Response.Write SQL
		
	Conn.Execute (SQL)	
End Function

Function UpdateRecord (theID)
	GetFormFields()

	SQL = "UPDATE F_EXPENDITURE " &_
			"SET DESCRIPTION = '" & expname & "', " &_
					"EXPENDITUREALLOCATION = " & expallocation & ", "&_
					"USERID = " & Session("UserID") & ", "&_
					"ACTIVE = " & active & ", "&_
					"LASTMODIFIED = '" & Date & "', "&_
					"QBDEBITCODE = " & nominalcode & ", "&_
					"QBCONTROLCODE = " & controlcode & " "&_
					"WHERE EXPENDITUREID = " & theID

	Response.Write SQL
		
	Conn.Execute (SQL)	
End Function

Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT * FROM F_EXPENDITURE WHERE EXPENDITUREID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	expname 		= Rs("DESCRIPTION")
	expallocation 	= Rs("EXPENDITUREALLOCATION")
	nominalcode		= Rs("QBDEBITCODE")
	controlcode		= Rs("QBCONTROLCODE")
	
	if (expallocation <> "") then
		expallocation = FormatNumber(expallocation,2,-1,0,0)
		DBL_expallocation = CDbl(expallocation)
	else 
		expallocation = "0.00"
		DBL_expallocation = CDbl(0)
	end if
	HDID = Rs("HEADID")
	active = Rs("ACTIVE")

	Rs.Close
	Set Rs = Nothing
	
	//find the total BUDGET VALUE
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select HEADID, HEADALLOCATION from F_HEAD where HEADID = " & HDID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("HEADALLOCATION"))
	totalfund = FormatNumber(Rs2("HEADALLOCATION"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select sum(EXPENDITUREALLOCATION) as TOTALALLOCATED from F_EXPENDITURE where HEADID = " & HDID & " and EXPENDITUREID <> " & EXID & " group by HEADID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("TOTALALLOCATED")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_expallocation - CDbl(Rs3("TOTALALLOCATED"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = FormatNumber(DBL_totalfund - DBL_expallocation,2,-1,0,0)
	end if
	Rs3.Close
	Set Rs3 = Nothing

	Set Rs5 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALPURCHASES FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND EXPENDITUREID = " & theID & " "
	Set Rs5 = Conn.Execute(strEventQuery)
	ExpenditurePurchases = Rs5("TOTALPURCHASES")
	Rs5.Close
	Set Rs5 = Nothing
		
End Function	

Function GetHeadData (theID)
	expallocation = "0.00"
	DBL_expallocation = CDbl(0)	
	//find the total fund value
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select headid, headallocation from F_HEAD where headid = " & HDID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("headallocation"))
	totalfund = FormatNumber(Rs2("headallocation"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select sum(EXPENDITUREALLOCATION) as TOTALALLOCATED from F_EXPENDITURE where HEADID = " & HDID & " group by HEADID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("TOTALALLOCATED")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_expallocation - CDbl(Rs3("TOTALALLOCATED"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = totalfund
	end if
	Rs3.Close
	Set Rs3 = Nothing
End Function

Function DisplayMiniTree(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT EX.DESCRIPTION AS EXPNAME, CC.DESCRIPTION AS COSTCENTRENAME, HD.DESCRIPTION AS HEADNAME FROM F_EXPENDITURE EX left join F_HEAD HD on HD.headid = EX.headid left join F_COSTCENTRE CC on CC.COSTCENTREID = HD.COSTCENTREID WHERE (EXPENDITUREID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Update Expenditure</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("HEADNAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("EXPNAME") & "</td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DisplayMiniTree2(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT CC.DESCRIPTION AS COSTCENTRENAME, HD.DESCRIPTION AS HEADNAME FROM F_HEAD HD left join F_COSTCENTRE CC on HD.COSTCENTREID = CC.COSTCENTREID WHERE (HD.HEADID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Add New Expenditure</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
	treeText = treeText & "<tr><td><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DelRecord (theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT Count(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE EXPENDITUREID = " & theID
	Set Rs4 = Conn.Execute(strEventQuery)
	ActualCount = Rs4("THECOUNT")
	Rs4.close()
	Set Rs4 = Nothing
			
	if (ActualCount = 0) then
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Set Rs = Conn.Execute ("DELETE FROM F_EXPENDITURE WHERE EXPENDITUREID = " & theID & ";")
		treeText = "Expenditure deleted successfully."		
	else
		treeText = "Sorry, cannot delete the selected expenditure as it is has (" & ActualCount & ") purchase(s) assigned to it. You can set the expenditure in-active instead."
	end if

End Function

ACTION_TO_TAKE = Request("EX_A")
EXID = Request("EXID")

OpenDB()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LFD") Then
	HDID = Request("HDID")
	fundid = Request("fundid")	
	GetHeadData(EXID)
	DisplayMiniTree2(HDID)	
ElseIf (ACTION_TO_TAKE = "L") Then
	GetData(EXID)
	DisplayMiniTree(EXID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(EXID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(EXID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
	if ("<%=ACTION_TO_TAKE%>" == "ADD"){
		parent.refreshSideBar();
		parent.setText("New expenditure added successfully.",1);			
		}
	else if ("<%=ACTION_TO_TAKE%>" == "UPDATE"){
		parent.refreshSideBar();
		parent.setText("Expenditure updated successfully.",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "LFD"){
		parent.ResetDiv('EXPENDITURE');
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("EXPENDITURE",1);
		parent.setCheckingArray('EXPENDITURE');
		parent.thisForm.HDID.value = "<%=HDID%>";		
		parent.thisForm.EXID.value = "<%=EXID%>";				
		parent.thisForm.EX_A.value = "ADD";
		parent.thisForm.txt_expenditureallocation.value = "<%=expallocation%>";
		parent.thisForm.Btotalallocated.value = "<%=allocated%>";
		parent.thisForm.Btotalremaining.value = "<%=totalremaining%>";
		parent.thisForm.Bmintotal.value = "0";						
		parent.thisForm.Btotalfund.value = "<%=totalfund%>";		
		parent.Budget.style.display = "block";
		}
	else if ("<%=ACTION_TO_TAKE%>" == "DELETE"){
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);	
		}
	else if ("<%=ACTION_TO_TAKE%>" == "L"){
		parent.NewItem(4);
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("EXPENDITURE",2);							
		parent.thisForm.txt_expenditureallocation.value = "<%=expallocation%>";
		parent.thisForm.txt_expenditurename.value = "<%=expname%>";
		parent.thisForm.EXID.value = "<%=EXID%>";						
		parent.thisForm.budgetcreated.value = "<%=startdate%>";		
		parent.thisForm.Btotalallocated.value = "<%=allocated%>";
		parent.thisForm.Bmintotal.value = "<%=ExpenditurePurchases%>";				
		parent.thisForm.Btotalremaining.value = "<%=totalremaining%>";
		parent.thisForm.Btotalfund.value = "<%=totalfund%>";		
		parent.thisForm.HDID.value = "<%=HDID%>";
		parent.thisForm.txt_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.txt_controlcode.value = "<%=controlcode%>";
		parent.thisForm.hid_nominalcode.value = "<%=nominalcode%>";
		parent.thisForm.hid_controlcode.value = "<%=controlcode%>";
		if ("<%=active%>" == "True")
			parent.thisForm.budgetactive[0].checked = true;
		else
			parent.thisForm.budgetactive[1].checked = true;				
		parent.thisForm.EX_A.value = "UPDATE";
		parent.Budget.style.display = "block";
		parent.setCheckingArray('EXPENDITURE');
		}
	}
</script>
</body>
</html>
