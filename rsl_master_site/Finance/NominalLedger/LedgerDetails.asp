<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="INCLUDES/functions.asp" -->
<%
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim balance, today, running_balance, current_day, prev_balance, end_balance
	Dim AccountNumber, FullName, AccountType, PS, PE, amitop
	Dim startdate, enddate, ob
	
	amitop = Request("AMITOP")
	If Request("START") = "" Then startdate = PS Else startdate = Request("START") End If
	If Request("END") = "" Then enddate = PE Else enddate = Request("END") End If
	If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 25 Else CONST_PAGESIZE = Request("PAGESIZE") End If

	OpenDB()

	get_year()					' GET THE START AND END DATES FOR THE CURRENT FISCAL YEAR
	get_account_details(Request("LID"))		' GET THE ACCOUNT HEADER INFORMATION
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intPage = 1	
	Else
		intPage = Request.QueryString("page")
	End If
	
	get_breakdown()				' BUILD BREAKDOWN

	' BUILD PAGED REPORT
	Function get_breakdown()
		
		Dim strSQL, rsSet, intRecord 
		
		' STORE PREVIOUS BALANCE BEFORE RUNNING BALANCE BECOMES UPDATED
		intRecord = 0
		str_data = ""
		strSQL = "SELECT 	J.TXNID, J.TXNDATE, T.DESCRIPTION, J.TRANSACTIONTYPE, ISNULL(J.DESCRIPTION,'') AS JDESC, " &_
				"			U.DEBIT, U.CREDIT, U.DESCRIPTION AS ITEMDESC, U.MEMO  " &_
				"FROM		NL_JOURNALENTRY J " &_
				"			INNER JOIN NL_TRANSACTIONTYPE T ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE " &_
				"			LEFT JOIN (SELECT TXNID, AMOUNT AS DEBIT, 0 AS CREDIT, ACCOUNTID, DESCRIPTION, MEMO FROM NL_JOURNALENTRYDEBITLINE " &_
				"		     UNION ALL " &_
				"		      SELECT TXNID, 0 AS DEBIT, AMOUNT AS CREDIT, ACCOUNTID, DESCRIPTION, MEMO FROM NL_JOURNALENTRYCREDITLINE) U " &_
				"			   ON J.TXNID = U.TXNID " &_
				"WHERE	U.ACCOUNTID = "&Request("LID")&" AND J.TXNDATE >= '" & FormatDateTime(STARTDATE,1)&  "' AND J.TXNDATE <= '" & FormatDateTime(enddate,1) & "' AND TRANSACTIONTYPE <> 9 " &_
				"ORDER	BY J.TXNDATE " 
		'response.write strSQL					
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
				
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If
	
		' CALCULATE BALANCE FOR TOP OF PAGE
		'balance = get_balance(intPage)
		balance = get_balance(intPage, startdate, enddate, Request("LID"), CONST_PAGESIZE)
		running_balance = balance

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		count = 0	
		str_data = str_data & "<TR style='background-color:thistle'>" &_
			"<TD COLSPAN=5><B>Brought Forward</B></TD>" &_
			"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(balance,2),2)&"</B></TD></TR>" 
		count = count + 1
		If intRecordCount > 0 Then
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			str_data = str_data & "<TBODY CLASS='CAPS'>"
			For intRecord = 1 to rsSet.PageSize
			' ENTER OPENING BALANCE
				if count mod 2 = 0 then color = ";background-color:beige" else color = "" end if
				str_data = str_data & "<TR STYLE='CURSOR:HAND"&color&"'>" &_
					"<TD NOWRAP>&nbsp;" & rsSet("TXNDATE") & "</TD>" &_
					"<TD NOWRAP>" & rsSet("DESCRIPTION") & "</TD>" &_
					"<TD NOWRAP TITLE='" & 	rsSet("MEMO") & "'>" & rsSet("ITEMDESC") & "</TD>" 
					' SHOW DEBIT
				If rsSet("DEBIT") = 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					If count = 1 Then prev_balance = running_balance End If
					running_balance = running_balance + cdbl(rsSet("DEBIT"))
					str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:BLACK'><B>"&FormatNumber(rsSet("DEBIT"),2)&"</b></TD>" 
				End If
				' SHOW CREDIT
				If rsSet("CREDIT") = 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					If count = 1 Then prev_balance = running_balance End If
					running_balance = running_balance - cdbl(rsSet("CREDIT"))
					str_data = str_data & "<TD ALIGN=RIGHT NOWRAP STYLE='COLOR:blue'><B>"&FormatNumber(rsSet("CREDIT"),2)&"</b></TD>" 
				End If
				' SHOW BALANCE
				If count mod 5 <> 0 Then 
					str_data = str_data & "<TD NOWRAP></TD>" 
				Else
					my_balance = running_balance
					str_data = str_data & "<TD NOWRAP ALIGN=RIGHT>"&FormatNumber(round(my_balance,2),2)&"</TD>" 
				End If
				count = count + 1
				rsSet.movenext()
				If rsSet.EOF Then Exit For End If
			Next
			'BALANCE AT BOTTOM PAGE
			'ensure table height is consistent with any amount of records
			str_data = str_data & "<TR style='background-color:thistle'>" &_
			"<TD COLSPAN=5><B></B></TD>" &_
			"<TD ALIGN=RIGHT><B>"&FormatNumber(Round(running_balance,2),2)&"</B></TD></TR>" 
			str_data = str_data & "</TBODY>"
			
			' links
			'str_data = str_data &_
			'"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			'"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
			'"<A HREF = 'CASHBOOK.asp?page=1&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&balance&"'><b><font color=BLUE>First</font></b></a> " &_
			'"<A HREF = 'CASHBOOK.asp?page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&prev_balance&"'><b><font color=BLUE>Prev</font></b></a>" &_
			'" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			'" <A HREF='CASHBOOK.asp?page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&running_balance&"'><b><font color=BLUE>Next</font></b></a>" &_ 
			'" <A HREF='CASHBOOK.asp?page=" & intPageCount & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&RB="&end_balance&"'><b><font color=BLUE>Last</font></b></a>" &_
			'"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			'"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
			'"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=6 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>" &_			
			"<A HREF = 'LEDGERDETAILS.ASP?page=" & prevpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"'><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <A HREF='LEDGERDETAILS.ASP?page=" & nextpage & "&START="&startdate&"&END="&enddate&"&PAGESIZE="&CONST_PAGESIZE&"&LID="&Request("LID")&"'><b><font color=BLUE>Next</font></b></a>" &_ 
			"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
			"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
		Else
			fill_gaps()	
		End If
		
		' if no teams exist inform the user
		'If intRecord = 0 Then 
		'	str_data = "<TABLE><TR><TD COLSPAN=6 ALIGN=CENTER><B>No transactions exist for this account !!</B></TD></TR>" &_
		'				"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		'End If
		
		rsSet.close()
		Set rsSet = Nothing
	End function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=6 ALIGN=CENTER STYLE='BACKGROUND-COLOR:WHITE'>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	End Function
	
	CloseDB()
	
%>
<html>
<head>
<title>Monthly Breakdown</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array();
	FormFields[0] = "txt_FROM|FROM|DATE|Y"
	FormFields[1] = "txt_TO|TO|DATE|Y"
	FormFields[2] = "txt_PAGESIZE|Page Size|INTEGER|Y"
	
	function save_balance(){
	
		//alert("whheh");
		//document.all.item("hd_" + <%=intPage%>).value = <%=topbalance%>;

	
	}
	
	function click_go(){
	
		if (!checkForm()) return false;
		//alert("LEDGERDETAILS.asp?START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value)
		location.href = "LEDGERDETAILS.asp?START="+RSLFORM.txt_FROM.value+"&END="+RSLFORM.txt_TO.value+"&PAGESIZE="+RSLFORM.txt_PAGESIZE.value+"&LID=<%=REQUEST("LID")%>"
	}

</SCRIPT>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="white" text="#000000" onload="window.focus()" class='ta'>
<form name = RSLFORM method=post> 
<TABLE WIDTH=690 CELLPADDING=1 CELLSPACING=1 align=center>
	<TR>
		<TD><%=AccountNumber %> - <%=FullName%> [<font color=blue><%=AccountType%></font>]<BR></TD>
		<TD ALIGN=RIGHT><input type="BUTTON" name="btnBack" value="Back" class=rslbutton onclick="location.href='Ledger.asp?LID=<%=Request("LID")%>'" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
			<input type="BUTTON" name="btnClose" value="Close" class=rslbutton onclick="window.close()" STYLE='BACKGROUND-COLOR:SILVER;COLOR:WHITE;BORDER:SOLID 1PX BLACK'>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=2>Period : <%=FormatDateTime(StartDate,1)%> - <%=FormatDateTime(EndDate,1)%><BR><BR></TD>
	</TR>
</TABLE>
<br>
<TABLE align=center WIDTH=690 CELLPADDING=1 CELLSPACING=0 STYLE='border:solid 1px black;behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse' slcolor='' border=1 hlcolor=thistle>
		<THEAD><TR STYLE='HEIGHT:3PX;BORDER-BOTTOM:1PX SOLID #133E71'><TD COLSPAN=6 CLASS='TABLE_HEAD'></TD></TR>
		<TR>
			<TD ALIGN=RIGHT COLSPAN=6 CLASS='TABLE_HEAD'>
			<B>From&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_FROM" maxlength=12 size=12 tabindex=3 value='<%=STARTDATE%>'>
			<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
			&nbsp;&nbsp;To&nbsp;&nbsp;
			<input type="textbox" class="textbox" name="txt_TO" maxlength=12 size=12 tabindex=3 value='<%=ENDDATE%>'>
			<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
			&nbsp;&nbsp;Pagesize&nbsp;
			<input type="textbox" class="textbox" name="txt_PAGESIZE" maxlength=3 size=5 tabindex=3 value="<%=CONST_PAGESIZE%>">
			<image src="/js/FVS.gif" name="img_PAGESIZE" width="15px" height="15px" border="0">
			&nbsp;&nbsp;	
			<input type="button" id="BTN_GO" name"BTN_GO" value="Proceed" class=rslbutton onClick="click_go()"></B>&nbsp;&nbsp;
			</TD>
		</TR>			
		<TR> 
		<TD STYLE='WIDTH:65px' CLASS='TABLE_HEAD'>&nbsp;<B>Date</B> </TD>
		  <TD STYLE='WIDTH:35px' CLASS='TABLE_HEAD'> <B>Type</B> </TD>
		  <TD CLASS='TABLE_HEAD'> <B>Description</B> </TD>
		  <TD STYLE='WIDTH:75px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Debit</B> </TD>
		  <TD STYLE='WIDTH:75px' CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Credit</B> </TD>
		  <TD CLASS='TABLE_HEAD' ALIGN=RIGHT> <B>Balance</B> </TD>
		</TR>
		<TR STYLE='HEIGHT:3PX'>	<TD  CLASS='TABLE_HEAD' COLSPAN=6 ALIGN=CENTER STYLE='BORDER-bottom:2PX SOLID #133E71'></TD></TR></THEAD>
	<%=str_data%>
	</TABLE>
</FORM>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>
