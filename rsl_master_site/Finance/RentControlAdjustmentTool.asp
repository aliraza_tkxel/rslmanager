<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_post, lst_paymenttype
	
	OpenDB()
	
	Call BuildSelect(lst_post, "sel_DIRECTPOSTITEMS", " F_DIRECTPOSTITEMS WHERE ITEMID BETWEEN 56 and 67 ", "ITEMID, ITEMNAME", "ITEMNAME", "Please Select", 0, null, "textbox200", "ONCHANGE='get_items()' disabled")
	
	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED 
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	SQL = "EXEC GET_VALIDATION_PERIOD"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	CurrentDate = FormatDateTime(Date(),1)
	Call CloseRs(rsTAXDATE)
	   
	CloseDB()	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Direct Posting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var is_a_valid_tenancy = 0

	function SetForSave(){
		// SET APPROPRIATE FORM FIELDS
		FormFields.length = 0		
		FormFields[0] = "hid_TENANTNUMBER|TENANTNUMBER|INTEGER|N"
		FormFields[1] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
		FormFields[2] = "sel_DIRECTPOSTITEMS|DIRECTPOSTITEMS|SELECT|Y"
		FormFields[3] = "hid_TENANTNUMBER1|TENANTNUMBER1|INTEGER|N" //"txt_CLAIMNUMBER|CLAIMNUMBER|TEXT|N"
	 	FormFields[4] = "txt_PROCESSDATE|Processing Date|DATE|Y"
		if (dates_required == 1){
			FormFields[5] = "txt_FROM|FROM|DATE|Y"
			FormFields[6] = "txt_TO|TO|DATE|Y"
			}
		else {
			FormFields[5] = "txt_FROM|FROM|DATE|N"
			FormFields[6] = "txt_TO|TO|DATE|N"
			}
		}

	function ATTACHITEM (JournalID, JournalValue, PaymentStartDate, PaymentEndDate,IsDebit,DebitId,CreditId){
	    RSLFORM.txt_AMOUNT.value = FormatCurrency(JournalValue);
	    RSLFORM.hid_AMOUNT.value=   FormatCurrency(JournalValue);
		RSLFORM.hid_ATTACHID.value = JournalID
		RSLFORM.txt_AMOUNT.readOnly = true
		RSLFORM.txt_FROM.value=PaymentStartDate;
		RSLFORM.txt_TO.value=PaymentEndDate;
		RSLFORM.hid_ISDEBIT.value=IsDebit;
		RSLFORM.hid_DEBITID.value=DebitId
		RSLFORM.hid_CREDITID.value=CreditId
		RSLFORM.Detach.style.visibility = "visible";					
		}
	
	function ATTACHBANK(BankID){
		RSLFORM.hid_ATTACHID.value = BankID	
		}

	function ATTACHCUSTOMER(CustomerID){
		RSLFORM.hid_ATTACHID.value = CustomerID	
		}
		
	function DETACHITEM(){
		RSLFORM.txt_AMOUNT.readOnly = false
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";
		RSLFORM.hid_ATTACHID.value = "";
		RSLFORM.txt_FROM.value="";
		RSLFORM.txt_TO.value="";															
		ref = document.getElementsByName("attach")
		if (ref.length){
			for (i=0; i<ref.length; i++){
				ref[i].checked = false
				}
			}
		}
				
	function save_form(){
		SetForSave()
		if (!checkForm()) return false;

		// MAKE SURE ORDER DATE IS IN CURRENT FISCAL YEAR
		var YStart = new Date("<%=YearStartDate%>")
		var YEnd = new Date("<%=YearendDate%>")
		var CurrentDate =new Date("<%=CurrentDate%>")
		if ( (real_date(RSLFORM.txt_PROCESSDATE.value) < YStart) || (real_date(RSLFORM.txt_PROCESSDATE.value) > YEnd)){
			alert("Please enter an payment date for the current financial year\nBetween '<%=YearStartDate%>' and '<%=YearendDate%>'");
			return false;
		}
		if(real_date(RSLFORM.txt_PROCESSDATE.value) < CurrentDate )
		{
		    alert("Please enter a date greater then current date.");
		    return false;
		}
    	// END ------------------------------------------
      if(FormatCurrency(RSLFORM.txt_AMOUNT.value) > FormatCurrency(RSLFORM.hid_AMOUNT.value))  
        {
            alert("Please enter a correct amount");
            return;
        }

		var is_returnable = RSLFORM.sel_DIRECTPOSTITEMS.value;

		tenantNumber = RSLFORM.txt_TENANTSEARCH.value
		tenantNumber1 = RSLFORM.txt_TENANTSEARCH1.value
		PostingType  = RSLFORM.sel_DIRECTPOSTITEMS[ RSLFORM.sel_DIRECTPOSTITEMS.selectedIndex].text;
		ProcessDate	 = RSLFORM.txt_PROCESSDATE.value
		
		if (RSLFORM.txt_FROM.value != ""){
			FromToString = "\n\n Date From : " + RSLFORM.txt_FROM.value + "\n\n Date To : " +  RSLFORM.txt_TO.value
			}
		else {
			FromToString = ""
			}
		
		Amount		 = RSLFORM.txt_AMOUNT.value
		alert("Misposted Tenant: " + tenantNumber + " \n\nTenant to be posted : " + tenantNumber1 + " \n\nPosting Type : " + PostingType  + " \n\nProcess Date : " + ProcessDate + FromToString + " \n\nAmount : " + Amount + " \n\n\n\n Please confirm that you have checked all the submitted information is correct.")

		if (confirm("Please confirm that you have checked all the submitted information is correct.")){
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait posting...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		RSLFORM.action = "serverside/RentControlAdjustmentTool_srv.asp";
		RSLFORM.btn_POST.disabled = true;		
		RSLFORM.submit();
		}
	}
	
	// 1 = called by tenant number 2 = callled by claim number
	function get_tenant(int_which){
		// SET APPROPRIATE FORMM FIELDS
		FormFields.length = 0
		if (int_which == 1)
			FormFields[0] = "txt_TENANTSEARCH|Tenant Number|INTEGER|Y"
			
		else 
			FormFields[0] = "txt_TENANTSEARCH1|Tenant Number|INTEGER|Y"
			
		if (!checkForm()) return false;
		
		ACCOUNT_DIV.innerHTML = "";
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait finding tenant...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		if (int_which == 1)
		   RSLFORM.action = "serverside/RentControlAdjustmentTool_srv.asp?action=GET&which=1";
		else
		   RSLFORM.action = "serverside/RentControlAdjustmentTool_srv.asp?action=GET&which=2"; 
		
		RSLFORM.txt_AMOUNT.readOnly = false
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";											
		RSLFORM.hid_ATTACHID.value = "";													
		RSLFORM.submit();
		
	
	}
	var dates_required = 0 //used to determine whether or not to force from and to dates
	
	// retrieve items from this rent account if cheque or DD is selected 
	function get_items(){
	    	
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT>";		
		
		var is_returnable = RSLFORM.sel_DIRECTPOSTITEMS.value;
			
		dates_required = 1;
		
		RSLFORM.txt_FROM.value = "";
		RSLFORM.txt_TO.value = "";
		SetForSave()
		checkForm()
        
		RSLFORM.txt_AMOUNT.readOnly = false		
		RSLFORM.txt_AMOUNT.value = " ";
		RSLFORM.hid_ATTACHID.value = "";
		RSLFORM.hid_TENANTNUMBER.value=	RSLFORM.txt_TENANTSEARCH.value
        RSLFORM.hid_TENANTNUMBER1.value=	RSLFORM.txt_TENANTSEARCH1.value											
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_PROCESSDATE.value="<%=Date()%>";
		parent.ACCOUNT_DIV.innerHTML=""
	
	
		 if ((is_returnable == 66) && is_a_valid_tenancy == 1){
		    STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait getting tenant details...</B></FONT>";
			STATUS_DIV.style.visibility = "visible";
			RSLFORM.target = "frm_direct";
			RSLFORM.action = "serverside/get_rent_control_adjustment_accountbalance.asp";
			RSLFORM.submit();
		}
		else if ((is_returnable == 56 || is_returnable == 57 || is_returnable == 58 || is_returnable == 59 || is_returnable == 60 || is_returnable == 61 || is_returnable == 62 || is_returnable == 63 || is_returnable == 64 || is_returnable == 65 || is_returnable == 67) && is_a_valid_tenancy == 1){
			STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait retrieving items...</B></FONT>";
			STATUS_DIV.style.visibility = "visible";
			RSLFORM.target = "frm_direct";
			RSLFORM.action = "serverside/get_rent_control_adjustment_items.asp?posting_type="+is_returnable;
			
			RSLFORM.submit();
		}
		else 
			ACCOUNT_DIV.innerHTML = ""; 
	}
			
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
		
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
	<TABLE BORDER=0 width=100% CELLPADDING=0 CELLSPACING=0>
		<TR>
			
      <TD>Enter tenant number to bring back correct tenant, then attach appropriate direct posting. If you choose 'DD Unpaid' or
	  		'Cheque' unpaid you can then attach the payment to a specific item. All HB entries require a date range to be entered.
	  </TD>
		</TR>
		<TR style=height:15px><TD></td></TR>
	</TABLE>
			
    <TABLE BORDER=0 width=90% CELLPADDING=0 CELLSPACING=0 align=center>
        <form name = RSLFORM method=post>
            <tr>
                <td width=10%>
                    <table border=0>
                        <TR> 
                            <TD nowrap><B>Misposted Tenant:</B></TD>
                            <TD colspan=1><INPUT TYPE=TEXT NAME=txt_TENANTSEARCH CLASS='TEXTBOX100'>
				                <image src="/js/FVS.gif" name="img_TENANTSEARCH" width="15px" height="15px" border="0">
				                <INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_FINDT' VALUE=' Find ' onclick='get_tenant(1)'>
			                </TD>

                		</TR>
		                <TR> 
                            <TD nowrap><B>Tenant to be posted:</B></TD>
                            <TD colspan=1><INPUT TYPE=TEXT NAME=txt_TENANTSEARCH1 CLASS='TEXTBOX100'  disabled>
				                <image src="/js/FVS.gif" name="img_TENANTSEARCH1" width="15px" height="15px" border="0">
				                <INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_FINDT1' VALUE=' Find ' onclick='get_tenant(2)' disabled>
			                </TD>
                		</TR>
		                <TR> 
		  	                 <TD nowrap><B>Posting Type :</B></TD>
                             <TD ><%=lst_post%>
				                       <image src="/js/FVS.gif" name="img_DIRECTPOSTITEMS" width="15px" height="15px" border="0">
			                  </TD>
		                 </TR>
		                 <TR> 
		  	                  <TD nowrap><B>Amount :</B></TD>
                              <TD nowrap colspan=1>
				                    <INPUT TYPE=TEXT NAME=txt_AMOUNT CLASS='TEXTBOX100' disabled= "disabled">
				                    <image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0">
				                    <INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_POST' VALUE=' Post ' DISABLED ONCLICK="save_form()">
				                    <INPUT TYPE=hidden NAME=hid_AMOUNT>
				                    <INPUT TYPE=hidden NAME=hid_TENANTNUMBER>
				                    <INPUT TYPE=hidden NAME=hid_TENANTNUMBER1>
				                    <INPUT TYPE=hidden NAME=hid_ATTACHID>
				                    <INPUT TYPE=hidden NAME=hid_ISDEBIT>
				                    <INPUT TYPE=hidden NAME=hid_CREDITID>
				                    <INPUT TYPE=hidden NAME=hid_DEBITID>				
				                    <image src="/js/FVS.gif" name="img_TENANTNUMBER" width="15px" height="15px" border="0">
				                    <image src="/js/FVS.gif" name="img_TENANTNUMBER1" width="15px" height="15px" border="0">
				                    <input type=button name=Detach value="DETACH" class="RSLButton" style='color:red;visibility:hidden' onClick="DETACHITEM()">
				              </TD>
		                 </TR>
		                 <TR> 
	                          <TD nowrap><b>Process Date :</b></TD>
		  	                  <TD nowrap colspan=1>
				                        <INPUT TYPE=TEXT NAME=txt_PROCESSDATE CLASS='TEXTBOX100' disabled= "disabled">
				                        <image src="/js/FVS.gif" name="img_PROCESSDATE" width="15px" height="15px" border="0">
			                  </TD>				
		                </TR>
		                <TR><TD><b>Correction Period From :</b></TD>
		                     <TD>
		                        <INPUT TYPE=TEXT NAME=txt_FROM CLASS='TEXTBOX100' disabled= "disabled">
				                <img src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0">
				             </TD>
				         </TR>
				         <tr>        
		  	                  <td><b>Correction Period To :</b></td>
		  	                  <td>
				                    <input type="text" name="txt_TO" class="TEXTBOX100" disabled= "disabled" />
				                    <img alt="" src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0" />
			                   </td>				
		                 </tr>

		        </table>    
		   </td>
		   <td width="100%" height="100%">
		        <table height="100%" border=0 width="100%">
		                <tr>	
			                    <TD valign=top STYLE='BORDER:1PX SOLID #133E71 right'  width=90% bgcolor=beige rowspan=1 height=50%>
			                        <div id=details style="height:50px">&nbsp;</div>
			                    </TD>
			            </tr>
			            <tr><td>&nbsp;</td></tr>
		                <tr>			  
		                        <TD valign=top STYLE='BORDER:1PX SOLID #133E71 right'  width=90% bgcolor=beige rowspan=1 height=50%>
			                        <div id=details1 style="height:50px">&nbsp;</div>
			                    </TD>
			             </tr>
			            

			    </table>
		     </td>
		</tr>
		<TR> 
		    <TD colspan=1> Click here to redirect page after submission: 
                <input type="checkbox" name="redir" value="1">
			</TD>
			<td><DIV ID=STATUS_DIV STYLE='VISIBILITY:HIDDEN'><FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT></DIV></td>
		</TR>
		<TR>
			<TD colspan=2 height=200px VALIGN=TOP>	
				<DIV ID=ACCOUNT_DIV STYLE='BORDER:1PX SOLID #133E71;background-color:beige;OVERFLOW:auto;height:100px;height:100%' class="TA">
				</DIV>
			</TD>
		</TR>
      </form>
    </TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_direct width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>
