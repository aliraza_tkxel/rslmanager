<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/CalendarForStandingOrders.asp" -->
<%
SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YSTART <= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
Call OpenDB()
Call OpenRs(rsYear, SQL)
	YSTART = rsYear("YSTART")
	YEND = rsYear("YEND")
Call CloseRs(rsYear)
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance - Standing Order Display</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">
        function getFileExtension(filePath) { //v1.0
            fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length) : filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length));
            return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
        }

        function checkFileUpload(form, extensions) { //v1.0
            document.MM_returnValue = true;
            if (extensions && extensions != '') {
                for (var i = 0; i < form.elements.length; i++) {
                    field = form.elements[i];
                    if (field.type.toUpperCase() != 'FILE') continue;
                    if (extensions.toUpperCase().indexOf(getFileExtension(field.value).toUpperCase()) == -1) {
                        ManualError('img_FILENAME', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                    }
                }
            }
            ManualError('img_FILENAME', '', 3);
            return true;
        }

        function BtnSubmit() {
            result = checkFileUpload(RSLFORM, 'csv');
            if (document.RSLFORM.FILENAME.value == "") {
                ManualError('img_FILENAME', 'You must select a file to upload.', 1);
                return false;
            }
            if (!result) return false;
            document.RSLFORM.submit();
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='StandingOrderFiles.asp?pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))

		RequestDate = CDate("1" & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear)

		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)

		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear							
	End If

	StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

	'THIS CALCULATES THE STANDING ORDERS WHICH HAVE GONE THROUGH
	ACTUALSQL = "SELECT COUNT(RJ.JOURNALID) AS THECOUNT, DAY(RJ.TRANSACTIONDATE) AS THEDAY, RJ.STATUSID, ISNULL(SUM(RJ.AMOUNT),0) AS THEAMOUNT " &_
		"FROM F_RENTJOURNAL RJ " &_
		"	WHERE RJ.PAYMENTTYPE = 3 AND RJ.TRANSACTIONDATE >= '" & StartOfMonthDate & "' and RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
		"		GROUP BY DAY(RJ.TRANSACTIONDATE), RJ.STATUSID"

	' Add items to the calendar
	OpenDB()
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			'THIS SECTION DISPLAYS THE STANDING ORDERS IN THE SYSTEM
			Call OpenRs(rsDays, ACTUALSQL)
			while (NOT rsDays.EOF) 
				if (rsDays("STATUSID") = 11) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='SOList.asp?PROCESSINGDATE=" & rsDays("THEDAY") & CurrentDateSmall & "&DECLINED=1' title='The count shows the number of standing orders which were subsequently declined. Standing Order Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=blue><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "red"
				else
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='SOList.asp?PROCESSINGDATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='Standing Order Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=blue><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "lightgreen"
				end if				
				rsDays.moveNext
			wend
			CloseRs(rsDays)									
		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
	CloseDB()
                %>
                <div style='position: absolute; left: 500; top: 140; width: 200px'>
                    <b><u>Standing Orders</u></b><br/>
                    <br/>
                    This calendar allows you to view any standing orders which have been processed.
                    <br/>
                    <br/>
                    <font style='background-color: #90EE90'>&nbsp;&nbsp;</font> Processed Standing
                    Orders
                    <br/>
                    <br/>
                    <font style='background-color: red'>&nbsp;&nbsp;</font> Declined Standing Orders<br/>
                    <br/>
                    <br/>
                    To change the selected month click on '<b><<</b>' or '<b>>></b>' to go backwards
                    and forwards respectively.
                    <br/>
                    <br/>
                    The display shows a figure of how many standing orders have been recieved on each
                    day.
                    <br/>
                    <br/>
                    To view files which have been previously uploaded click on the corresponding red
                    day numbers.
                    <form method="post" name="RSLFORM" action="ServerSide/StandingOrderUpload.asp?GP_upload=true"
                    enctype="multipart/form-data">
                    <table border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td>
                                <b>Upload Standing Order CSV File</b>
                            </td>
                            </tr>
                            <tr>
                                    <td>
                                        <input type="file" size="30" name="FILENAME" id="FILENAME" class="RSLButton" style="background-color: white;
                                            color: black" />&nbsp;
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_FILENAME" id="img_FILENAME" width="15px" height="15px" border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <input type="button" name="Submit" value="Upload File" title="Upload File" onclick="BtnSubmit()" class="RSLButton" />&nbsp;
                                    </td>
                                </tr>
                    </table>
                    </form>
                </div>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
