<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	SQLCODE = "SELECT CASE  RJ.PAYMENTTYPE " &_
			"WHEN 14 THEN 'LACHEQUE.ASP' " &_
			"WHEN 23 THEN 'LALIST.ASP' " &_
			"END AS REDIR, " &_
			"CASE  RJ.PAYMENTTYPE " &_
			"WHEN 14 THEN 'Cheque' " &_
			"WHEN 23 THEN 'BACS' " &_
			"END AS PAYMENTNAME, " &_
			"COUNT(RJ.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT " &_
			"FROM F_RENTJOURNAL RJ " &_
			"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
			"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
			"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
			"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
			"INNER JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
			"WHERE RJ.PAYMENTTYPE IN (14,23) AND LB.PROCESSDATE = '" & REQUEST("PODATE") & "' " &_
			"GROUP BY RJ.PAYMENTTYPE"			

	OpenDB()
	
	TableString = ""

	Call OpenRs(rsList, SQLCODE)
	if (NOT rsList.EOF) then
		while NOT rsList.EOF
			TableString = TableString & "<TR STYLE='CURSOR:HAND' ONCLICK=""GO('" & rsList("REDIR") & "')""><TD>" & rsList("PAYMENTNAME") & "</TD><TD>" & rsList("TOTALCOUNT") & "</TD><TD align=right>" & FormatCurrency(rsList("PAYMENTAMOUNT")) & "</TD></TR>"
			rsList.moveNext
		wend
	else
		TableString = "<TR><TD COLSPAN=3 ALIGN=CENTER>No payable LA Refunds found.</TD></TR>"
	end if
	CloseRs(rsList)
	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> LA Refund List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}

function GO(ThePage){
	location.href = ThePage + "?VIEW=1&PODATE=<%=Request("PODATE")%>" 
	}

function SaveResults(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "ServerSide/LAList_CSV.asp<%="?CC_Sort=" & orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value
		}
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack"><tr>
      <td> This page shows the LA Refunds which have <font color=blue><b>already 
        been paid</b></font> grouped by the payment type. To view the payments 
        made for any individual payment type click on it. <br>
        <br>
</td></tr></table>

  <TABLE CELLPADDING=3 CELLSPACING=0 border=1 width=750 STYLE='BORDER-COLLAPSE:COLLAPSE;BEHAVIOR:URL(/INCLUDES/TABLES/TABLEHL.HTC)' HLCOLOR='STEELBLUE' SLCOLOR=''>
	<THEAD>
    <TR> 
      <TD style='border-bottom:1px solid #133e71'><b>Payment Type</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>LA Refund Count</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>Total Value</b></TD>
    </TR>
	</THEAD>
    <%=TableString%> 
  </TABLE>
</form>
<br>&nbsp<a href='LADisplay.asp?date=<%=REQUEST("PODATE")%>'><font color=blue><b>BACK to LA Refund Calendar</b></font></a>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>