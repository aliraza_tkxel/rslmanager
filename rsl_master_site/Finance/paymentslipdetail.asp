<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match
	Dim TDFunc         (5)	 'stores any functions that will be applied		
	
	ColData(0)  = "Date|CREATIONDATE|90"
	SortASC(0) 	= "CP.CREATIONDATE ASC "
	SortDESC(0) = "CP.CREATIONDATE DESC "
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Office|OFFICE|140"
	SortASC(1) 	= "OFFICE ASC "
	SortDESC(1) = "OFFICE DESC "	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Name|THENAME|260"
	SortASC(2) 	= "THENAME ASC"
	SortDESC(2) = "THENAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""
	
	ColData(3)  = "Item|ITEMTYPE|100"
	SortASC(3) 	= "ITEMTYPE ASC"
	SortDESC(3) = "ITEMTYPE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""
	
	ColData(4)  = "Cash/Chq|PAYMENTTYPE|100"
	SortASC(4) 	= "PAYMENTTYPE ASC"
	SortDESC(4) = "PAYMENTTYPE DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "Amount|AMOUNT|90"
	SortASC(5) 	= "AMOUNT ASC"
	SortDESC(5) = "AMOUNT DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = "FormatNumber(|)"
	
	PageName = "paymentslipdetail.asp"
	EmptyText = "No matching cash postings found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SQLCODE =	"SELECT " &_
				"			THENAME = CASE " &_ 
				"			WHEN CASHPOSTINGORIGIN = 1 THEN '<B>Ten</B>: ' + CAST(CP.TENANCYID AS VARCHAR)" &_
				"			WHEN CASHPOSTINGORIGIN = 2 THEN '<B>Emp</B>: ' + E.FIRSTNAME + ' ' + E.LASTNAME " &_
				"			WHEN CASHPOSTINGORIGIN = 3 THEN '<B>Sup</B>: ' + ORG.NAME " &_
				"			WHEN CASHPOSTINGORIGIN = 4 THEN '<B>S.Cus</B>: ' + SC.CONTACT " &_									
				"			END, " &_
				"			ISNULL(CONVERT(NVARCHAR, CP.CREATIONDATE, 103) ,'') AS CREATIONDATE, " &_
				"			ISNULL(O.DESCRIPTION,'') AS OFFICE, " &_
				"			ISNULL(CP.RECEIVEDFROM,'') AS RECEIVEDFROM, " &_
				"			ISNULL(I.DESCRIPTION,'') AS ITEMTYPE, " &_
				"			ISNULL(P.DESCRIPTION,'') AS PAYMENTTYPE, " &_
				"			ISNULL(CP.AMOUNT, 0) AS AMOUNT, " &_
				"			ISNULL(CP.CHQNUMBER,'') AS CHQNUMBER, " &_
				"			ISNULL(PAYMENTSLIPNUMBER,'') AS SLIPNUM " &_
				"FROM	 	F_CASHPOSTING CP " &_
				"			LEFT JOIN G_OFFICE O ON CP.OFFICE = O.OFFICEID " &_
				"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = CP.PAYMENTTYPE " &_
				"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = CP.ITEMTYPE " &_
				"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = CP.TENANCYID AND CASHPOSTINGORIGIN = 2 " &_  
				"			LEFT JOIN S_ORGANISATION ORG ON ORG.ORGID = CP.TENANCYID AND CASHPOSTINGORIGIN = 3 " &_ 
				"			LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = CP.TENANCYID AND CASHPOSTINGORIGIN = 4 " &_  									
				"WHERE		CP.PAYMENTSLIPNUMBER = '" & Request("slipid") & "'" &_
				"ORDER		BY " & orderBy 
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Payment Slip Detail</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(slip_id){
	location.href = "paymentslip.asp";
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2>
			<IMG NAME="paymentslip" style='cursor:hand' onclick="location.href='PaymentSlip.asp?page=<%=Request("SlipPage")%>&txt_SLIPNUMBER=<%=Request("SlipFilter")%>&CC_Sort=<%=Request("SlipSort")%>'"  TITLE='Payment Slip' SRC="images/ttps.gif" WIDTH=102 HEIGHT=20 BORDER=0>
		</TD>
		<TD ROWSPAN=2>
			<IMG NAME="tab_employees" TITLE='Cash Postings' SRC="images/csh.gif" WIDTH=161 HEIGHT=20 BORDER=0>
		</TD>	
		<TD><IMG SRC="images/spacer.gif" WIDTH=487 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=487 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>