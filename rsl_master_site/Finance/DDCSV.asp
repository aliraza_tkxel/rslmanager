<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
POFilter = ""
If (Request("PROCESSINGDATE") <> "") Then
	ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
	RequiredDate = CDate(Request("PROCESSINGDATE"))
Else
	ProcessingDate = FormatDateTime(CDate(Date),2)
	RequiredDate = Date
End If

SQL = "SELECT * FROM F_DDFILES WHERE FILEDATE = '" & FormatDateTime(RequiredDate,1) & "' ORDER BY FILETYPE ASC, FILEID DESC"
Call OpenDB()
Call OpenRs(rsLoader, SQL)
If (NOT rsLoader.EOF) Then
	while (NOT rsLoader.EOF)
		TableString = TableString & "<tr><td><a target=""_blank"" href=""/DDFile/" & rsLoader("FILENAME") & """><font color=""blue"">" & rsLoader("FILENAME") & "</font></a></td><td>" & rsLoader("FILECOUNT") & "</td><td align=""right"">" & FormatCurrency(rsLoader("FILEAMOUNT")) & "</td><td>&nbsp;&nbsp;" & rsLoader("PROCESSDATE") & "</td></tr>"
		rsLoader.moveNext
	Wend
Else
	TableString = "<tr><td colspan=""4"" align=""center"">No Files found for the specified date</td></tr>"
End If
Call CloseRs(rsLoader)
Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> CSV File List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    You will find the files you have created listed below. To save any particular file
    right click on it and click 'save as'.<br/>
    To open the file directly just click on the link. Any files which have a BAD suffix
    are files which have incorrect or missing data.<br/>
    The Files you are viewing are for the Date: <b>
        <%=FormatDateTime(RequiredDate,1)%></b>
    <br/>
    <br/>
    <table style='border-collapse: collapse' border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td width="200">
                <b>File Name</b>
            </td>
            <td width="100">
                <b>DD Count</b>
            </td>
            <td width="100" align="right">
                <b>DD Total</b>
            </td>
            <td width="100">
                <b>&nbsp;&nbsp;Process Date</b>
            </td>
        </tr>
        <%=TableString%>
    </table>
    <br/>
    <br/>
    &nbsp<a href='DDDisplay.asp?date=<%=Request("PROCESSINGDATE")%>'><font color="blue"><b>BACK
        to DD Calendar</b></font></a>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
