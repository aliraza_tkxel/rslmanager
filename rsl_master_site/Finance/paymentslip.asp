<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match
	Dim TDFunc         (3)	 'stores any functions that will be applied		

	ColData(0)  = "Date|THEDATE|100"
	SortASC(0) 	= "CP.CREATIONDATE ASC "
	SortDESC(0) = "CP.CREATIONDATE DESC "
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Taken By|FULLNAME|190"
	SortASC(1) 	= "FIRSTNAME ASC, LASTNAME ASC "
	SortDESC(1) = "FIRSTNAME DESC, LASTNAME DESC "	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Slip Number|SLIPNUMBER|190"
	SortASC(2) 	= "SLIPNUMBER ASC"
	SortDESC(2) = "SLIPNUMBER DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""
	
	ColData(3)  = "Slip Total|SLIPTOTAL|190"
	SortASC(3) 	= "SLIPTOTAL ASC"
	SortDESC(3) = "SLIPTOTAL DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = "FormatNumber(|)"		


	PageName = "paymentslip.asp"
	EmptyText = "No paymentslips found in the system or filter returned zero results!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""PAYMENTSLIPID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SlipFilter = ""
	if (Request("txt_SLIPNUMBER") <> "") Then
		SlipFilter = " WHERE CP.SLIPNUMBER LIKE '%" & Replace(Request("txt_SLIPNUMBER"), "'", "''") & "%' "
	end if

	SQLCODE =	"SELECT 	CP.PAYMENTSLIPID, ISNULL(CONVERT(NVARCHAR, CP.CREATIONDATE, 103) ,'') AS THEDATE, " &_
				"			LTRIM(ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'')) AS FULLNAME, " &_
				"			ISNULL(CP.SLIPNUMBER,'') AS SLIPNUMBER, " &_
				"			ISNULL(CP.SLIPTOTAL,0) AS SLIPTOTAL " &_
				"FROM	 	F_PAYMENTSLIP CP " &_
				"			LEFT JOIN E__EMPLOYEE E ON CP.USERID = E.EMPLOYEEID "  & SlipFilter &_
				"ORDER		BY " & orderBy 

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Payment Slips</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()
FormFields[0] = "txt_SLIPNUMBER|Slip Number|TEXT|N"

function load_me(slip_id){
	location.href = "PaymentSlipDetail.asp?slipid=" + slip_id + "&SlipPage=<%=intpage%>&SlipSort=<%=orderby%>&SlipFilter=<%=Request("txt_SLIPNUMBER")%>";
	}

function FilterPaymentSlips(){
	if (!checkForm()) return false
	thisForm.action = "PaymentSlip.asp"
	thisForm.method = "post"
	thisForm.submit()
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD ROWSPAN=2><IMG NAME="paymentslip" TITLE='Payment Slips' SRC="images/payment.gif" WIDTH=109 HEIGHT=20 BORDER=0></TD>
		<TD align=right><b>Slip Number</b> : <input type="textbox" class="textbox" name="txt_SLIPNUMBER" value="<%=Request("txt_SLIPNUMBER")%>"><img name="img_SLIPNUMBER" src="/js/img/FVS.gif" width=15 height=15><input type=button value=" SUBMIT " class="RSLButton" onclick="FilterPaymentSlips()"></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=641 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>