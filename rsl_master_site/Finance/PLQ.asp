<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderQueued.asp" -->
<%
	CONST CONST_PAGESIZE = 17
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	ColData(0)  = "Order No|ORDERID|85"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|80"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "By|FIRSTNAME|100"
	SortASC(2) 	= "FIRSTNAME ASC"
	SortDESC(2) = "FIRSTNAME DESC"	
	TDSTUFF(2)  = " "" TITLE="""""" & rsSet(""FIRSTNAME"") & "" "" & rsSet(""LASTNAME"") & """""" "" "
	TDFunc(2) = ""		

	ColData(3)  = "Item|PONAME|275"
	SortASC(3) 	= "PONAME ASC"
	SortDESC(3) = "PONAME DESC"	
	TDSTUFF(3)  = " "" TITLE="""""" & rsSet(""PONOTES"") & """""" "" "
	TDFunc(3) = ""		

	ColData(4)  = "Cost|TOTALCOST|100"
	SortASC(4) 	= "SU.TOTALCOST ASC"
	SortDESC(4) = "SU.TOTALCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"

	ColData(5)  = "|EMPTY|20"
	SortASC(5) 	= ""
	SortDESC(5) = ""
	TDSTUFF(5)  = " "" style='background-color:white' "" "
	TDFunc(5) = ""

	PageName = "PLQ.asp"
	EmptyText = "No Queued Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	CURRENTPO = REQUEST("CPO")
	
	POFilter = ""
	if (Request("PONumber") <> "") then
		POFilter = " AND PO.ORDERID = '" & Request("PONumber") & "' "
	end if
	
	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " AND PO.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	end if

	SQLCODE = "SELECT PO.ORDERID, '' AS EMPTY, FORMATTEDPODATE = CONVERT(VARCHAR, PODATE, 103), PODATE, PS.POSTATUSNAME, E.FIRSTNAME, E.LASTNAME, PONAME, PONOTES, SU.TOTALCOST " &_
			"FROM F_PURCHASEORDER PO " &_
			"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) SU ON SU.ORDERID = PO.ORDERID " &_
			"INNER JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " &_
			"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_			
			"WHERE PO.POSTATUS = 0 AND PO.ACTIVE = 1 " & SupFilter & POFilter &_			
			"ORDER BY " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Purchase Order List --> Queued</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(Order_id){
	window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
	}

function CancelBubble(){
	event.cancelBubble = true
	}

function ClickChange(OID){
	event.cancelBubble = true
	location.href = "<%=PageName & "?CC_Sort=" & orderBy& "page=" & intpage%>&CPO=" + OID
	}
		
function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}

function setSupplier(){
	thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
	}

function SubmitPage(){
	if (isNaN(thisForm.PONumber.value)){
		alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
		}
	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value + "&PONumber=" + thisForm.PONumber.value
	}

function ApproveItems(){
	thisForm.action="ServerSide/ProcessPOS.asp?AUTHORIZE=1"
	thisForm.method = "POST"
	thisForm.submit()
	}

function DeclineItems(){
	thisForm.action="ServerSide/ProcessPOS.asp?DECLINE=1"
	thisForm.method = "POST"
	thisForm.submit()
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();setSupplier()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack">
<tr>
      <td>This list shows all queued purchase orders. To authorize them check 
        the respective purchase orders and click on 'AUTHORIZE'. To cancel any 
        purchase orders check all respective purchase orders and then click on 
        'DECLINE'. Purchase Orders which are above your employee limit will have 
        no checkbox next to them, so you will not be allowed to authorize them.</td>
    </tr>
</table>
<table class="RSLBlack">
<tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=PONumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>" onblur="RemoveBad()" style='width:135px;background-image:url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
<td><%=lstSuppliers%>
</td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr>
</table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
<table width=750><tr><td align=right><input class="RSLButton" type=button onclick="DeclineItems()" value=" DECLINE ">&nbsp;<input class="RSLButton" type=button onclick="ApproveItems()" value=" AUTHORIZE "></td></tr></table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>