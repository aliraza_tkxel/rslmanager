<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
Dim TaxDate, InvoiceNnumber, ReconcileDate
L_ORDERID = 0
Session("SupplierReturenId")=Request("suppId")
IF(Request("ORDERID") <> "" AND ISNUMERIC(Request("ORDERID"))) THEN L_ORDERID = Request("OrderID")
%>
<%
OpenDB()

EmployeeLimit = 0

SQL = "SELECT PO.POSTATUS, PO.ORDERID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, " &_
		"D.DEVELOPMENTNAME, S.CISCATEGORY AS CISCATEGORY, C.DESCRIPTION as COMPANY, isnull(c.companyid,1) AS COMPANYID FROM F_PURCHASEORDER PO " &_
		"INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_
        " LEFT JOIN G_COMPANY C ON C.COMPANYID = PO.COMPANYID " &_
		"LEFT JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS IN (1,2,5,7) GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_		
		"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID " &_		
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"WHERE ACTIVE = 1 AND PO.ORDERID = " & L_ORDERID
'response.Write (SQL)
'response.End()
Call OpenRs(rsDR, SQL)
if (NOT rsDR.EOF) then
    POSTATUS = rsDR("POSTATUS")
    ' if PO status is not invoice recieved than do not allow it to be reconciled.
    'if (POSTATUS <> 9) THEN
    '    Response.Redirect ("InvoiceNotReceived.asp")
    'End IF
	ORDERID = rsDR("ORDERID")
	PONUMBER = PurchaseNumber(rsDR("ORDERID"))
	PONAME = rsDR("PONAME")
	PODATE = FormatDateTime(rsDR("PODATE"),1)
	SUPPLIER = rsDR("NAME")
	SUPPLIERID = rsDR("SUPPLIERID")	
	POSTATUSNAME = rsDR("POSTATUSNAME")
	DEVELOPMENT = rsDR("DEVELOPMENTNAME")
    CISCATEGORY = rsDR("CISCATEGORY")
    COMPANY = rsDR("COMPANY")
	COMPANYID = rsDR("COMPANYID")
else
	Response.Redirect ("PurchaseOrderNotFound.asp")
end if
Call CloseRs(rsDR)


SQL = "SELECT PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, PO.IsServiceChargePO, " &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE, ISNULL(Invoice.image_url,'') AS image_url FROM F_PURCHASEITEM PI " &_
        "INNER JOIN F_PURCHASEORDER PO on PO.ORDERID = PI.ORDERID " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
        "Outer APPLY (SELECT TOP 1 image_url AS image_url FROM F_PURCHASEITEM_LOG PIL WHERE PIL.ORDERITEMID = PI.ORDERITEMID AND PIL.image_url IS NOT NULL ORDER BY PIL.TIMESTAMP DESC) Invoice " &_
	    "WHERE PI.ACTIVE = 1 AND PI.ORDERID = " & ORDERID & " ORDER BY ORDERITEMID ASC"
'response.Write (SQL)
'response.End()
PIString = ""
TotalNetCost = 0
TotalVAT = 0
TotalGrossCost = 0
TotalReconcilableNetCost = 0
TotalReconcilableVAT = 0
TotalReconcilableGrossCost = 0
TotalRows = 0
RowsThatCanBeReconciled = 0
RowsThatWillBeReconciled = 0

Call OpenRs(rsPI, SQL)		
SQL="Select OrderItemId from F_PurchaseItem where OrderId="&ORDERID
Call OpenRs_RecCount(rsTotalPIs, SQL)	
remainPIs = rsTotalPIs.RecordCount
Call CloseRs(rsTotalPIs)
InvoiceDoc=""

while (NOT rsPI.EOF) 
	
	OrderItemID = rsPI("ORDERITEMID")

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
    IsServiceChargePO = rsPI("IsServiceChargePO")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
       
	if(len(rsPI("image_url"))>1) then
     InvoiceDoc = "<a href='"&replace(rsPI("image_url"),"","%20") &"' target='_blank'><img src='../../IMAGES/IconAttachment.gif' border=0></a>"
    else
      InvoiceDoc = "No Attachment"
   
    end if
	DataStorage = "<INPUT TYPE=""HIDDEN"" NAME=""DS" & OrderItemID & """ id=""DS" & OrderItemID & """ VALUE=""" & NetCost & "|" & VAT & "|" & GrossCost & """>"
	
	PITYPE = rsPI("PITYPE")
	PISTATUS = rsPI("PISTATUS")
'response.Write(PISTATUS)
	RowsThatCanBeReconciled = RowsThatCanBeReconciled + 1

	if (PISTATUS = 9 or PISTATUS = 10 or PISTATUS = 11 or PISTATUS = 12 or PISTATUS = 13 or PISTATUS = 14) then
		CheckData = "<IMG SRC=""/myImages/tick.gif"" title=""This item has already been reconciled/paid."">"
		RowsThatCanBeReconciled = RowsThatCanBeReconciled - 1	
        remainPIs = remainPIs-1 	
	elseif (PISTATUS = 0 or PISTATUS = 1) then
		'response.Write (OrderItemID)
        CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as it still waiting authorisation."">"	
	   ' remainPIs = remainPIs + 1
	    
    'elseif (PISTATUS = 1 or PISTATUS = 7) then
'		CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as it still waiting approval."">"	
'	   remainPIs = remainPIs + 1
    elseif (PITYPE = 2 AND PISTATUS <> 3 AND  PISTATUS <> 7 AND PISTATUS <> 5 AND PISTATUS <>17 AND PISTATUS <> 18) then
		CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as it has not been completed yet."">"
       ' remainPIs = remainPIs +1 
	else
		
        SQL = "Select OrderItemId from F_PURCHASEITEM where OrderItemId ="& OrderItemID &" and PIStatus In (17)"

        Call OpenRs_RecCount(rsPILog, SQL)
        'response.Write (SQL)
        totalPIs = rsPILog.RecordCount
        if (totalPIs>0) then
            CheckData = "<INPUT TYPE=""CHECKBOX"" NAME=""CHKS"" ID=""CHKS"" VALUE=""" & OrderItemID & """ CHECKED onclick=""UpdateReconcileTotal();UpdateInvAndTaxDate("&OrderItemID&",this"&")"">"
		    RowsThatWillBeReconciled = RowsThatWillBeReconciled + 1
		    TotalReconcilableNetCost = TotalReconcilableNetCost + NetCost
		    TotalReconcilableVAT = TotalReconcilableVAT + VAT
		    TotalReconcilableGrossCost = TotalReconcilableGrossCost + GrossCost
        else
        '   remainPIs = remainPIs + 1
           CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as either Goods or Invoice has not been approved yet."">" 
        end if
	end if

	if (PISTATUS = 9 or PISTATUS = 11 or PISTATUS = 12 or PISTATUS = 13) then
		AmendData = "<IMG SRC=""/myImages/tick.gif"" title=""This item has already been reconciled/paid and therefore cannot be amended."">"
	elseif (PISTATUS = 0) then
		AmendData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be amended as it still waiting authorisation."">"	
	else
		AmendData = "<INPUT TYPE=""BUTTON"" VALUE=""AM"" TITLE=""To amend this Purchase Item click here."" onclick=""APO(" & OrderItemID & ",'" & IsServiceChargePO & "')"" class=""RSLBUTTON"">"
	end if
	'//Fetching Invoice No & Tax Date
   '  SQL = "SELECT TaxDate,InvoiceNumber From F_INVOICE Where ORDERID = " & ORDERID & " Order by InvoiceId Desc"
    'If (len(Request("txt_INVOICENUMBER"))>0 and len(Request("txt_TAXDATE"))>0) then
        SQL = "SELECT RECONCILEDATE, TaxDate,InvoiceNumber From F_INVOICE Where ORDERITEMID = " & OrderItemID & " Order by InvoiceId Desc"
    'end if
    'response.Write(SQL)
	Call OpenRs(rsDR, SQL)
    if (NOT rsDR.EOF ) then
        ReconcileDate = rsDR("RECONCILEDATE") 
        TaxDate = rsDR("TaxDate")
        InvoiceNumber = rsDR("InvoiceNumber")
       
    End if
	Call CloseRs(rsDR)
    PIString = PIString & "<TR ALIGN=RIGHT ><TD ALIGN=LEFT><input type='hidden' id='Inv_Tax_"&OrderItemID&"' value='"&InvoiceNumber&"|"&TaxDate&"'>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD><TD>" & AmendData & "</TD><TD>" & CheckData & "</TD><TD ALIGN=LEFT>"&InvoiceDoc&"</TD></TR>"
	ItemDesc = rsPI("ITEMDESC")
	if ( NOT(ItemDesc = "" OR isNULL(ItemDesc)) ) then
		PIString = PIString & "<TR ID=""PIDESCS"" STYLE=""DISPLAY:NONE""><TD width=410 colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"	
	end if
	rsPI.moveNext
   
wend

if (RowsThatWillBeReconciled = RowsThatCanBeReconciled) then
	DisplayText = "This Purchase Order will become fully reconciled"
else 
	if (RowsThatWillBeReconciled = 0) then
		DisplayText = "No rows have or can be selected therefore no Reconciliation will occur."
		DisabledButton = " disabled"
	elseif (RowsThatCanBeReconciled - RowsThatWillBeReconciled = 1) then
		DisplayText = "This Purchase Order will not be fully Reconciled as there is " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " item that has<br> not or can not be selected."
	else
		DisplayText = "This Purchase Order will not be fully Reconciled as there are " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " items that have<br> not or can not be selected."
	end if						
end if


'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
'SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID & " AND NETCOST<>0 AND GROSSCOST<>0 Order by InvoiceId Desc "
SQL ="SELECT INV.InvoiceNumber,INV.TAXDATE,PI.NETCOST,PI.VAT,PI.GROSSCOST,PI.ITEMNAME,PI.PISTATUS FROM F_INVOICE INV" &_
" INNER JOIN F_PURCHASEITEM PI ON INV.ORDERITEMID = PI.ORDERITEMID" &_
" WHERE INV.ORDERID = " & ORDERID & " Order by INV.InvoiceId"
'response.Write (SQL)
Call OpenRs(rsINV, SQL)

'Fetching PO Total 
if (NOT rsINV.EOF) then
	InvoiceString = "<br><TABLE WIDTH=755 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
			"<TD WIDTH=80 HEIGHT=20><b>Invoice No:</b></TD><TD WIDTH=70 HEIGHT=20><b>Invoice No:</b></TD><TD width=100><b>Invoice Date:</b></TD><TD width=180><b>Status</b></TD><TD align=right width=80><b>Net (�):</b></TD><TD align=right width=75><b>VAT (�):</b></TD><TD align=right width=80><b>Gross (�):</b></TD><TD WIDTH=30></TD></TR>"
	
	while NOT rsINV.EOF
	  	'InvoiceString = InvoiceString & "<TR><TD>" & rsINV("InvoiceNumber") & "</TD><TD>" & rsINV("TAXDATE") & "</TD><TD>" & rsINV("THESTATUS") & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("NETCOST"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("VAT"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("GROSSCOST"),2) & "</TD></TR>"
		InvStatus = "Waiting Confirmation" 
		if(rsINV("PISTATUS") = 18 or rsINV("PISTATUS") = 17) then
		 InvStatus ="Confirmed"		
		end if
		if (rsINV("PISTATUS") = 9 or rsINV("PISTATUS") = 10 or rsINV("PISTATUS") = 11 or rsINV("PISTATUS") = 12 or rsINV("PISTATUS") = 13)Then
		InvStatus ="Paid"
		end if
		
		InvoiceString = InvoiceString & "<TR><TD>" & rsINV("ITEMNAME") & "</TD><TD>" & rsINV("InvoiceNumber") & "</TD><TD>" & rsINV("TAXDATE") & "</TD><TD>" & InvStatus & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("NETCOST"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("VAT"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("GROSSCOST"),2) & "</TD></TR>"
		
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</TABLE>"
end if
Call CloseRs(rsINV)

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)
	
	
	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	' THERE ARE 2 SP's FOR THIS a)GET_VALIDATION_PERIOD_NODEFAULT and b)GET_VALIDATION_PERIOD THE DEFAULT ONE 
	' HAS A WIDER DATE RANGE THAN THE CURRENT YEAR FOR TIDYING UP PURPOSES
	SQL = "EXEC GET_VALIDATION_PERIOD 10 , " & COMPANYID

	Call OpenRs(rsTAXDATE, SQL)	
	If (NOT rsTAXDATE.EOF) then
	        
	   if (Not isEmpty(rsTAXDATE("YSTART")) and rsTAXDATE("YSTART")<>"") then
	      YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	   end if 
	   if (Not isEmpty(rsTAXDATE("YEND")) and rsTAXDATE("YEND")<>"" ) then
	      YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	   end if 
	End if  
    Call CloseRs(rsTAXDATE)
    '//Fetching Default Invoice# and Tax Date
   SQL = "SELECT RECONCILEDATE, TaxDate,InvoiceNumber From F_INVOICE Where ORDERID = " & ORDERID & " Order by InvoiceId Desc"
   Call OpenRs(rsInvoice, SQL)
   if (NOT rsInvoice.EOF) then
    
        ReconcileDate = rsInvoice("RECONCILEDATE")
        TaxDate = rsInvoice("TaxDate")
        InvoiceNumber = rsInvoice("InvoiceNumber")

        if ((ReconcileDate = "" OR isNULL(ReconcileDate)) ) then
    
                Dim dd, mm, yy, dtsnow

                dtsnow = Now()

                'Individual date components
                dd = Right("00" & Day(dtsnow), 2)
                mm = Right("00" & Month(dtsnow), 2)
                yy = Year(dtsnow)

                ReconcileDate = dd & "/" & mm & "/" & yy
                
           End if
    End if
	Call CloseRs(rsInvoice)
    
CloseDB()

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge" />
    <title>Reconciliation > Confirm</title>
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/RSL.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="javascript" src="/js/general.js"></script>
    <script type="text/javascript" language="javascript" src="/js/menu.js"></script>
    <script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" language="javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array()
        FormFields[0] = "txt_INVOICENUMBER|Invoice Number|TEXT|Y"
        FormFields[1] = "txt_TAXDATE|Invoice Date|DATE|Y"
        FormFields[2] = "txt_RECONCILEDATE|Reconcile Date|DATE|Y"

        var RowsThatWillBeReconciled = <%=RowsThatWillBeReconciled%>
        var RowsThatCanBeReconciled = <%=RowsThatCanBeReconciled%>

	
                function FormatCurrrenyComma(Figure) {
                    NewFigure = FormatCurrency(Figure)
                    if ((Figure >= 1000 || Figure <= -1000)) {
                        var iStart = NewFigure.indexOf(".");
                        if (iStart < 0)
                            iStart = NewFigure.length;

                        iStart -= 3;
                        while (iStart >= 1) {
                            NewFigure = NewFigure.substring(0, iStart) + "," + NewFigure.substring(iStart, NewFigure.length)
                            iStart -= 3;
                        }
                    }
                    return NewFigure
                }

        function UpdateReconcileTotal() {
            iChecks = document.getElementsByName("CHKS")

            NetValue = 0
            TheVAT = 0
            GrossValue = 0
            if (iChecks.length) {
                RowsThatWillBeReconciled = 0
                for (i = 0; i < iChecks.length; i++) {
                    if (iChecks[i].checked == true) {
                        ORDERITEMID = iChecks[i].value
                        FieldData = document.getElementById("DS" + ORDERITEMID).value
                        FieldArray = FieldData.split("|")
                        NetValue += parseFloat(FieldArray[0])
                        TheVAT += parseFloat(FieldArray[1])
                        GrossValue += parseFloat(FieldArray[2])
                        RowsThatWillBeReconciled++
                    }

                }
            }
            document.getElementById("NET").innerHTML = FormatCurrrenyComma(NetValue)
            document.getElementById("VAT").innerHTML = FormatCurrrenyComma(TheVAT)
            document.getElementById("GROSS").innerHTML = FormatCurrrenyComma(GrossValue)
            document.getElementById("ReconcileButton").disabled = false
            if (RowsThatWillBeReconciled == RowsThatCanBeReconciled)
                document.getElementById("INFO").innerHTML = "This Purchase Order will become fully reconciled"
            else {
                if (RowsThatWillBeReconciled == 0) {
                    document.getElementById("ReconcileButton").disabled = true
                    document.getElementById("INFO").innerHTML = "No rows have or can be selected therefore no Reconciliation will occur."
                }
                else if (RowsThatCanBeReconciled - RowsThatWillBeReconciled == 1)
                    document.getElementById("INFO").innerHTML = "This Purchase Order will not be fully Reconciled as there is " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " item that has<br> not or can not be selected."
                else
                    document.getElementById("INFO").innerHTML = "This Purchase Order will not be fully Reconciled as there are " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " items that have<br> not or can not be selected."
            }
        }

        function ToggleDescs() {
            if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
                NewText = "HIDE DESCRIPTIONS"
                NewStatus = "block"
            }
            else {
                NewText = "SHOW DESCRIPTIONS"
                NewStatus = "none"
            }
            iDesc = document.getElementsByName("PIDESCS")
            if (iDesc.length) {
                for (i = 0; i < iDesc.length; i++)
                    iDesc[i].style.display = NewStatus
            }
            document.getElementById("SHOWDESC").innerHTML = NewText
        }




        function Reconcile() {


            var YStart = new Date("<%=YearStartDate%>")
            var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
            var YEnd = new Date("<%=YearendDate%>")
            var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
            var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>")

            if (!checkForm()) return false;
            if (real_date(document.getElementById("txt_RECONCILEDATE").value) < YStart || real_date(document.getElementById("txt_RECONCILEDATE").value) > YEnd) {
                // we're outside the grace period and also outside this financial year
                //alert("Please enter an Invoice Date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
                alert("The month is now locked down and entries can only be made for the current month for Date of Reconcilialtion");
                return false;
            }

            var ChkRef = document.getElementsByName("CHKS");
            var remainingPIs = document.getElementById("hid_RemainPIs").value;
            //	alert("before" + remainingPIs);
            for (i = 0; i < ChkRef.length; i++) {
                if (ChkRef[i].checked) {
                    remainingPIs = remainingPIs - 1
                }
            }
            // alert("after" + remainingPIs); 
            document.getElementById("hid_RemainPIs").value = remainingPIs

            document.getElementById("ReconcileButton").disabled = true
            document.getElementById("ReconcileButton").value = " Submitted "

            RSLFORM.action = "ServerSide/ReconcileConfirm_svr.asp"
            RSLFORM.submit();
        }

        function real_date(str_date) {

            var datearray;
            var months = new Array("nowt", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
            str_date = new String(str_date);
            datearray = str_date.split("/");

            return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);

        }

        function load_me(Order_id) {
            window.showModelessDialog("Popups/PartReconcile.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 500px; dialogWidth: 580px; status: No; resizable: No;")
        }

        function UpdateChecks() {

            var var_TaxDate = ""
            var var_ReconcileDate = ""
            var var_InvoiceNumber = ""
            if (document.getElementById("MegaClick").checked == true) {
                NewStatus = true
                var_TaxDate = '<% =TaxDate%>'
                var_ReconcileDate = '<% =ReconcileDate%>'
                var_InvoiceNumber = '<% =InvoiceNumber%>'
            }
            else {
                NewStatus = false
            }

            document.getElementById("txt_INVOICENUMBER").value = var_InvoiceNumber;
            document.getElementById("txt_TAXDATE").value = var_TaxDate;
            document.getElementById("txt_RECONCILEDATE").value = var_ReconcileDate;
            ChkRef = document.getElementsByName("CHKS")
            if (ChkRef.length) {
                for (i = 0; i < ChkRef.length; i++) {
                    ChkRef[i].checked = NewStatus

                }
            }

            UpdateReconcileTotal()
        }

        function APO(OrderItemid, IsServiceChargePO){

            //if (window.showModelessDialog) {        // Internet Explorer
            //       result = window.showModelessDialog("Popups/AmendPurchaseItem.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&ReturnTo=1&OrderItemID=" + OrderItemid + "&Random=" + new Date(), document.getElementById("ReconcileButton"), "dialogHeight: 500px; dialogWidth: 354px; status: No; resizable: No;");
            // }
            //else {
            if(IsServiceChargePO == "True"){
                result = window.open("Popups/AmendPurchaseItemSC.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&ReturnTo=1&OrderItemID=" + OrderItemid + "&Random=" + new Date(),document.getElementById("ReconcileButton"), "Height=500px; Width= 400px; status: No; resizable: No; alwaysRaised=yes; scrollbars=no");
            }
            else{
                result = window.open("Popups/AmendPurchaseItem.asp?OrderID=<%=ORDERID%>&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&ReturnTo=1&OrderItemID=" + OrderItemid + "&Random=" + new Date(), document.getElementById("ReconcileButton"), "Height=500px; Width= 400px; status: No; resizable: No; alwaysRaised=yes; scrollbars=no");
                //}
            }
        }

        function showCisApprovedalert(f_CisCategory) {
            if (f_CisCategory == "Gross" || f_CisCategory == "Higher Rate" || f_CisCategory == "Standard") {
                $("#CISApprovedSupplier").dialog({
                    title: "CIS Approved Supplier",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: 200,
                    position: {
                        my: "center",
                        at: "center",
                        of: $("body"),
                        within: $("body")
                    },
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
        }
        		
        <% if Request("ER12" & Replace(Date, "/", "")) = 1 then %>
                alert("A user is processing some invoices to be paid.\nPlease wait a few seconds before re-processing your actions.")
                <% end if %>	
                    function UpdateInvAndTaxDate(OderItemId, field) {
                        var txtInvAndTax = document.getElementById("Inv_Tax_" + OderItemId);

                        if (field.checked) {
                            InvoiceInfo = txtInvAndTax.value.split("|");
                            document.getElementById("txt_INVOICENUMBER").value = InvoiceInfo[0];
                            document.getElementById("txt_TAXDATE").value = InvoiceInfo[1];
                            document.getElementById("txt_RECONCILEDATE").value = InvoiceInfo[2];
                }
                else {
                            document.getElementById("txt_INVOICENUMBER").value = "";
                            document.getElementById("txt_TAXDATE").value = "";
                            document.getElementById("txt_RECONCILEDATE").value = "";
                }


                }
    </script>
  <script>
  $(function() {
    $( "#txt_TAXDATE" ).datepicker({ dateFormat: 'dd/mm/yy' });
	$( "#txt_RECONCILEDATE" ).datepicker({ dateFormat: 'dd/mm/yy' });
  });
  </script>
</head>
<body bgcolor="#FFFFFF" onload="showCisApprovedalert(<%="'"+CISCATEGORY+"'" %>);initSwipeMenu(0);preloadImages()"
    onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">

    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table border="0" cellspacing="4" cellpadding="0">
        <form name="RSLFORM" method="post">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table style='border: 1PX SOLID BLACK' width="475px">
                                <tr>
                                    <td width="90PX">
                                        Order No:
                                    </td>
                                    <td>
                                        <%=PONUMBER%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Description:
                                    </td>
                                    <td>
                                        <%=PONAME%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Order Date:
                                    </td>
                                    <td>
                                        <%=PODATE%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Supplier:
                                    </td>
                                    <td>
                                        <%=SUPPLIER%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Development:
                                    </td>
                                    <td>
                                        <%=DEVELOPMENT%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Status:
                                    </td>
                                    <td>
                                        <%=POSTATUSNAME%>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        Company:
                                    </td>
                                    <td>
                                        <%=COMPANY%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10">
                            &nbsp;
                        </td>
                        <td valign="top">
                            <table style='border: 1PX SOLID BLACK' width="255PX" height="100">
                                <tr>
                                    <td width="90PX" nowrap>
                                        Invoice No:
                                    </td>
                                    <td width="120" nowrap>
                                        <input type="text" name="txt_INVOICENUMBER" maxlength="20" class="textbox" id="txt_INVOICENUMBER" value="<%=InvoiceNumber %>" />
                                    </td>
                                    <td width="40" nowrap>
                                        <img src="/js/FVS.gif" name="img_INVOICENUMBER" width="15" height="15"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="90PX">
                                        Invoice Date:
                                    </td>
                                    <td width="120" nowrap>
                                        <input type="text" name="txt_TAXDATE" id="txt_TAXDATE" maxlength="10" class="textbox" value="<%=TaxDate %>"  " />
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_TAXDATE" width="15" height="15">
                                    </td>
                                </tr>
								<tr>
                                    <td width="90PX">
                                        Date Of Reconciliation:
                                    </td>
                                    <td width="120" nowrap>
                                        <input type="text" name="txt_RECONCILEDATE" id="txt_RECONCILEDATE" maxlength="10" class="textbox" value="<%=ReconcileDate %>"   />
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_RECONCILEDATE" width="15" height="15">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="100%" colspan="3">
                                        <font color="red">
                                            <div id="INFO">
                                                <%=DisplayText%></div>
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br>
                <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="2" width="755">
                    <tr bgcolor="#133e71" align="RIGHT" style='color: white'>
                        <td height="20" align="LEFT" width="370" nowrap>
                            <table cellspacing="0" cellpadding="0" width="370">
                                <tr style='color: white'>
                                   
                                    <td width="100px">
                                        <b>Item Name:</b>
                                    </td>
                                    <td align="right" style='cursor: hand' onclick="ToggleDescs()">
                                        <b>
                                            <div id="SHOWDESC">
                                                SHOW DESCRIPTIONS</div>
                                        </b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="40" nowrap>
                            <b>Code:</b>
                        </td>
                        <td width="80" nowrap>
                            <b>Net (�):</b>
                        </td>
                        <td width="75" nowrap>
                            <b>VAT (�):</b>
                        </td>
                        <td width="80" nowrap>
                            <b>Gross (�):</b>
                        </td>
                        <td width="30">
                            &nbsp;
                        </td>
                        <td width="30">
                            <input type="checkbox" name="MegaClick" id="MegaClick"  checked onclick="UpdateChecks()">
                        </td>
                         <td><b>Invoice:</b>
                                    </td>
                    </tr>
                    <%=PIString%>
                </table>
                <br>
                <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="2" width="755">
                    <tr bgcolor="#133e71" align="RIGHT">
                        <td height="20" width="450" nowrap style='border: none; color: white'>
                            <b>TOTAL : &nbsp;</b>
                        </td>
                        <td width="80" nowrap bgcolor="white">
                            <b>
                                <%=FormatNumber(TotalNetCost,2)%></b>
                        </td>
                        <td width="75" nowrap bgcolor="white">
                            <b>
                                <%=FormatNumber(TotalVAT,2)%></b>
                        </td>
                        <td width="80" nowrap bgcolor="white">
                            <b>
                                <%=FormatNumber(TotalGrossCost,2)%></b>
                        </td>
                        <td width="30" bgcolor="white">
                            &nbsp;
                        </td>
                        <td width="30" bgcolor="white">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <br>
                <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="2" width="755">
                    <tr bgcolor="#133e71" align="RIGHT">
                        <td height="20" width="450" nowrap style='border: none; color: white'>
                            <b>TOTAL RECONCILABLE : &nbsp;</b>
                        </td>
                        <td width="80" nowrap bgcolor="white">
                            <b>
                                <div id="NET">
                                    <%=FormatNumber(TotalReconcilableNetCost,2)%></div>
                            </b>
                        </td>
                        <td width="75" nowrap bgcolor="white">
                            <b>
                                <div id="VAT">
                                    <%=FormatNumber(TotalReconcilableVAT,2)%></div>
                            </b>
                        </td>
                        <td width="80" nowrap bgcolor="white">
                            <b>
                                <div id="GROSS">
                                    <%=FormatNumber(TotalReconcilableGrossCost,2)%></div>
                            </b>
                        </td>
                        <td width="30" bgcolor="white">
                            &nbsp;
                        </td>
                        <td width="30" bgcolor="white">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <br>
                <table cellspacing="0" cellpadding="2" width="755">
                    <tr align="RIGHT">
                        <td>
                            <input type="HIDDEN" name="hid_PAGE" value="<%=Request("CurrentPage")%>">
                            <input type="HIDDEN" name="hid_ORDERID" value="<%=ORDERID%>">
                            <input type="HIDDEN" name="hid_SUPPLIERID" value="<%=SUPPLIERID%>">
                            <input type="Hidden" id="hid_RemainPIs" name="hid_RemainPIs" value="<%=remainPIs%>">
                            
                            <input type="HIDDEN" name="hid_RECONCILE" value="RECONCILE">
                            <input type="BUTTON" name="ReconcileButton" value=" RECONCILE " onclick="Reconcile()" id="ReconcileButton" class="RSLButton" <%=DisabledButton%>>
                        </td>
                    </tr>
                </table>
                <%=InvoiceString%>
            </td>
        </tr>
        </form>
    </table>
    <iframe src="/secureframe.asp" name="PURCHASERECONCILEFRAME123" style='display: NONE'>
    </iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <div id="CISApprovedSupplier" title="CIS Approved Supplier" style="display: none;">
        <p>
            Category -
            <%=CISCATEGORY%>
        </p>
    </div>
</body>
</html>
