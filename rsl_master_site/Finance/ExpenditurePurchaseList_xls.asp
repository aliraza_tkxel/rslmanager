<%
Response.Buffer = True
ByPassSecurityAccess = true
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition","attachment; filename=ExpenditurePurchaseList.xls"
%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_export.asp" -->

<%
	'****** NOTE IF STOPS WORKING
	'		take away the accesscheck include above and put in a connection string as it seems that the include 
	'		sometimes kills the xls transfer

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		' Declare all of the variables that will be used in the page.
		Dim str_data, count,HEADING,FilterDesc
		Dim COSTCENTREFilter,HeadFilter,selectedCC,selectedHead
	    Dim FiscalYearFilter, FiscalYearNLFilter, FiscalYearFilter_Array, FiscalYearFilter_Start, FiscalYearFilter_End,selectedFiscalYear
	    Dim Total
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End Declare Vars
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    'Get Record to display on xsl
	    OpenDB()
	    
	    HEADING="Expenditure Purchase List"
	    
	    orderBy=Request("orderBy")
	    
	    if(orderBy="") then
	        orderBy=" PO.ORDERID DESC"
	    end if
	     CompanyFilter = ""
			rqCompany = Request("sel_COMPANY")
			if (rqCompany <> "") then
				CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
			end if
	    FiscalYearFilter = ""
	    if (Request("sel_FISCALYEARFILTER") <> "") then
        	FiscalYearFilter_Array = Split(Request("sel_FISCALYEARFILTER"), ";", 2,1)
        	FiscalYearFilter_Start = FiscalYearFilter_Array(0)
        	FiscalYearFilter_End = FiscalYearFilter_Array(1)
            FiscalYearFilter = " AND PIDATE >= '" & FiscalYearFilter_Start & "' AND PIDATE <= '" & FiscalYearFilter_End & " 23:59 ' "
            FiscalYearNLFilter = " AND J.TXNDATE >= '" & FiscalYearFilter_Start & "' AND J.TXNDATE <= '" & FiscalYearFilter_End & " 23:59 ' "
			selectedFiscalYear = FiscalYearFilter_Start & " - " & FiscalYearFilter_End
			FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Fiscal Year</b></td> " &_
                                      "   <td>" & selectedFiscalYear & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	     end if
	     	           
	    COSTCENTREFilter = ""
	    if (Request("sel_COSTCENTRE") <> "") then
		    COSTCENTREFilter = " AND CC.COSTCENTREID = " & Request("sel_COSTCENTRE") & " "
		    selectedCC=Request("selectedCC") 
		    FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Cost Centre</b></td> " &_
                                      "   <td>" & selectedCC & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	    end if
    		    
	    HeadFilter = ""
	    if (Request("sel_HEAD") <> "") then
			HeadFilter = " AND H.HEADID = " & Request("sel_HEAD") & " "	
		    selectedHead=Request("selectedHead") 
		    FilterDesc = FilterDesc & " <tr> " &_
                                      "   <td>&nbsp;<b>Head</b></td> " &_
                                      "   <td>" & selectedHead & "</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      "   <td>&nbsp;</td> " &_
                                      " </tr>"
	    end if
	    	    
        strSQL =  " SELECT ISNULL(EMP.FIRSTNAME + ' ' + EMP.LASTNAME,'') AS FULLNAME,  " &_
				  "        ISNULL(PO.ORDERID,'') AS ORDERID,ISNULL(PONAME,'') AS PONAME, " &_
		 		  "        ISNULL(PI.ORDERITEMID,'') AS ORDERITEMID, ISNULL(PI.ITEMNAME,'') AS ITEMNAME, ISNULL(PI.ITEMDESC,'') AS ITEMDESC, " &_
				  "	        PI.ITEMNAME  AS SHORTITEMNAME, " &_
				  "        PISTATUS = ISNULL(CASE PITYPE WHEN 5 THEN 'Paid <font color=red>Tenant Reimbursement</font>' WHEN 4 THEN 'Paid <font color=red>Staff Expense</font>'WHEN 3 THEN 'Paid <font color=red>Petty Cash</font>' " &_
				  "	                            ELSE PS.POSTATUSNAME END,''), " &_
				  "	        ISNULL(E.EXPENDITUREID,'') AS EXPENDITUREID, ISNULL(E.DESCRIPTION,'') AS EXPDESC, ISNULL(H.HEADID,'') AS HEADID,ISNULL(CC.COSTCENTREID,'') AS COSTCENTREID , " &_
				  "	        ISNULL(CONVERT(VARCHAR,PI.NETCOST,1),'') AS NETCOST, " &_
                  "	        ISNULL(CONVERT(VARCHAR,PI.VAT,1),'') AS VAT, " &_
                  "	        ISNULL(CONVERT(VARCHAR,PI.GROSSCOST,1),'') AS GROSSCOST, " &_
				  "	        FORMATTEDPIDATE=ISNULL(CONVERT(VARCHAR,PIDATE,103),''),CS.DESCRIPTION AS JOURNALSTATUS,O.NAME AS SUPPLIER,ISNULL(P.HOUSENUMBER,'')+ ' ' +  ISNULL(P.ADDRESS1,'') + ' ' +  ISNULL(P.ADDRESS2,'') + ' ' + ISNULL(P.TOWNCITY,'') + ' ' + ISNULL(P.POSTCODE,'') AS PROPADDRESS, SCH.SCHEMECODE,AC.ACCOUNTNUMBER  " &_
				  " FROM  F_PURCHASEITEM PI " &_
				  "	    LEFT JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_
				  "     LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID  " &_
				  "     LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
				  "     LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID=P.DEVELOPMENTID "&_
                  "     LEFT JOIN P_BLOCK BL ON BL.BLOCKID = P.BLOCKID  "&_ 
                  "     LEFT JOIN P_SCHEME SCH ON P.SchemeId = SCH.SCHEMEID OR BL.SCHEMEID = SCH.SCHEMEID "&_ 
				  "     LEFT JOIN dbo.P_WOTOREPAIR WOR ON WOR.ORDERITEMID=PI.ORDERITEMID  " &_
				  "	    LEFT JOIN dbo.C_JOURNAL CJ ON CJ.JOURNALID=WOR.JOURNALID  " &_
				  "	    LEFT JOIN dbo.C_STATUS CS ON CS.ITEMSTATUSID=CJ.CURRENTITEMSTATUSID  " &_
				  "	    LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID  " &_
				  "	    LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID  " &_
				  "	    INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = PI.EXPENDITUREID " &_
                  "     LEFT JOIN NL_ACCOUNT AC ON E.QBDEBITCODE  =AC.ACCOUNTNUMBER "&_
				  "	    INNER JOIN F_HEAD H ON H.HEADID = E.HEADID " &_
				  "	    INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = H.COSTCENTREID " &_
				  "	    LEFT JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = PO.USERID  " &_
				  " WHERE PI.PITYPE <> 6 AND PO.POSTATUS <> 0 AND PI.ACTIVE = 1 " & COSTCENTREFilter & HeadFilter & FiscalYearFilter & CompanyFilter
				  
		if (Request("sel_COSTCENTRE") = "9") then		
			strSQL = strSQL &_
			" UNION ALL " &_
			" SELECT e.FIRSTNAME + ' ' + e.LASTNAME AS FULLNAME, null as ORDERID, '' as PONAME, null as ORDERITEMID, ITEMNAME, null as ITEMDESC, ITEMNAME AS SHORTITEMNAME "&_
			" 	,'General Journal' AS PISTATUS, null AS EXPENDITUREID, NLName AS EXPDESC, null AS HEADID, null AS COSTCENTREID "&_
			" 	, null AS NETCOST, null as VAT, GROSSCOST, FORMATTEDPIDATE, null AS JOURNALSTATUS, '' AS SUPPLIER, '' AS PROPADDRESS, null AS SCHEMECODE, u.ACCOUNTNUMBER "&_
			" FROM NL_JOURNALENTRY J  "&_
			"	INNER JOIN P_SCHEME ps on ps.SCHEMEID = j.SchemeId "&_
			"	INNER JOIN PDR_DEVELOPMENT pd on pd.DEVELOPMENTID = ps.DEVELOPMENTID "&_
			"	INNER JOIN F_HEAD h on h.DESCRIPTION = pd.DEVELOPMENTNAME "&_
			" 	LEFT JOIN NL_TRANSACTIONTYPE T  ON T.TRANSACTIONTYPEID = J.TRANSACTIONTYPE  "&_
			" 	INNER JOIN NL_GENERALJOURNAL gj on gj.GJID = j.TRANSACTIONID "&_
			" 	LEFT JOIN E__EMPLOYEE e on e.EMPLOYEEID = gj.USERID "&_
			" 	INNER JOIN ( "&_
			" 		SELECT TXNID, LINEID, jedl.DESCRIPTION AS ITEMNAME, amount AS GROSSCOST, NLA1.NAME AS NLName, NLA1.ACCOUNTNUMBER, CONVERT(VARCHAR,jedl.TXNDATE,103) AS FORMATTEDPIDATE "&_
			" 			FROM NL_JOURNALENTRYDEBITLINE JEDL    "&_
			" 				INNER JOIN NL_ACCOUNT NLA1   ON NLA1.ACCOUNTID = JEDL.ACCOUNTID AND (NLA1.ACCOUNTID = 100 OR NLA1.PARENTREFLISTID = 100)  "&_
			" 		UNION ALL  "&_
			" 		SELECT TXNID, LINEID, jecl.DESCRIPTION AS ITEMNAME, -amount AS GROSSCOST, NLA2.NAME AS NLName, NLA2.ACCOUNTNUMBER, CONVERT(VARCHAR,jecl.TXNDATE,103) AS FORMATTEDPIDATE "&_
			" 			FROM NL_JOURNALENTRYCREDITLINE JECL    "&_
			" 				INNER JOIN NL_ACCOUNT NLA2   ON NLA2.ACCOUNTID = JECL.ACCOUNTID AND (NLA2.ACCOUNTID = 100 OR NLA2.PARENTREFLISTID = 100)  "&_
			" 		) U ON J.TXNID = U.TXNID  "&_
			" WHERE TRANSACTIONTYPE = 13 and h.HEADID = " & Request("sel_HEAD") & FiscalYearNLFilter
		end if

		strSQL = strSQL & " Order By ORDERID DESC"
		'rw strSQL
		'response.end	
        Server.ScriptTimeout= 120 
		Call OpenRs(rsSet, strSQL)
		
		Total = 0
		while NOT rsSet.EOF 
			Total = Total + rsSet("GROSSCOST")
          
	        rsSet.moveNext()
		wend
		if Total > 0 then
		rsSet.moveFirst()
		end if
        FilterDesc = FilterDesc & " <tr> " &_
                                  "   <td>&nbsp;<b>Total</b></td> " &_
                                  "   <td>" & FormatCurrency(Total) & "</td> " &_
                                  "   <td>&nbsp;</td> " &_
                                  "   <td>&nbsp;</td> " &_
                                  "   <td>&nbsp;</td> " &_
                                  "   <td>&nbsp;</td> " &_
                                  " </tr>"
		
%>
	<HTML>
	<HEAD>
	<TITLE>RSL Manager Finanace --> Expenditure Purchase List </TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<BODY bgcolor="#FFFFFF" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
	  <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=2 border="1"  >
	  <TR > 
		<TD  colspan="11"> 
		  <div align="left"><font color="#0066CC"><b><font size="4"><%=HEADING%></font> 
			<%dim todaysDate
					 todaysDate=now()
					 response.write todaysDate
					%>
			</b></font></div>
		  </TD>
		</TR>
		<tr>
			<td colspan="11">&nbsp;</td>
		</tr>
		<%=FilterDesc%>
		<tr>
			<td colspan="11">&nbsp;</td>
		</tr>
		<tr> 
		  <td>&nbsp;<b>Order No</b></td>
		  <td>&nbsp;<b>Ordered</b>&nbsp;</td>
		  <td>&nbsp;<b>Expenditure</b>&nbsp;</td>
		  <td>&nbsp;<b>Item</b>&nbsp;</td>
		  <td>&nbsp;<b>Net Cost</b>&nbsp;</td>
          <td>&nbsp;<b>VAT</b>&nbsp;</td>
          <td>&nbsp;<b>Gross Cost</b>&nbsp;</td>
		  <td>&nbsp;<b>PO Status</b>&nbsp;</td>
		  <td>&nbsp;<b>Status</b>&nbsp;</td>
		  <td>&nbsp;<b>Supplier</b>&nbsp;</td>
		  <td>&nbsp;<b>Property Address</b>&nbsp;</td>
		  <td>&nbsp;<b>Scheme Code</b>&nbsp;</td>
		  <td>&nbsp;<b>NL Account</b>&nbsp;</td>
		  
		</tr>
	<%	
				while NOT rsSet.EOF 
				        response.flush
						response.Write("<tr>")
						response.Write("<td valign=""top"">" & rsSet("ORDERID") & "</td>")
						response.Write("<td>" & rsSet("FORMATTEDPIDATE") & "</td>")
						response.Write("<td align=""center"">" & rsSet("EXPDESC") & "</td>")
						response.Write("<td>" & rsSet("SHORTITEMNAME") & "</td>")
						response.Write("<td>" & rsSet("NETCOST") & "</td>")
                        response.Write("<td>" & rsSet("VAT") & "</td>")
                        response.Write("<td>" & rsSet("GROSSCOST") & "</td>")
						response.Write("<td>" & rsSet("PISTATUS") & "</td>")
						response.Write("<td>" & rsSet("JOURNALSTATUS") & "</td>")
						response.Write("<td>" & rsSet("SUPPLIER") & "</td>")
						response.Write("<td>" & rsSet("PROPADDRESS") & "</td>")
						response.Write("<td>" & rsSet("SCHEMECODE") & "</td>")
                        response.Write("<td>" & rsSet("ACCOUNTNUMBER") & "</td>")
						response.Write("</tr>")
          
		        rsSet.moveNext()
		wend
		CloseRs(rsSet)
		SET rsSet = Nothing
		
		CloseDB()
%>
		</TABLE>
	</BODY>
	</HTML>



