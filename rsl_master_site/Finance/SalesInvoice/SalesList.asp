<%@language="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim clean_desc
	
	ColData(0)  = "Sale No|SALEID|90"
	SortASC(0) 	= "SO.SALEID ASC"
	SortDESC(0) = "SO.SALEID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "SaleNumber(|)"

	ColData(1)  = "Date|FORMATTEDSODATE|90"
	SortASC(1) 	= "SODATE ASC"
	SortDESC(1) = "SODATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Customer|CUSTOMER|120"
	SortASC(2) 	= "CUSTOMER ASC"
	SortDESC(2) = "CUSTOMER DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Type|CUSTOMERTYPE|90"
	SortASC(3) 	= "CUSTOMERTYPE ASC"
	SortDESC(3) = "CUSTOMERTYPE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Item|SONAME|175"
	SortASC(4) 	= "SONAME ASC"
	SortDESC(4) = "SONAME DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "Cost|FORMATTEDCOST|60"
	SortASC(5) 	= "TOTALCOST ASC"
	SortDESC(5) = "TOTALCOST DESC"	
	TDSTUFF(5)  = " ""align='right'"" "
	TDFunc(5) = "FormatCurrency(|)"		

	PageName = "SalesList.asp"
	EmptyText = "No Relevant Sales Invoices found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" TITLE="""""" & rsSet(""SOSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""SALEID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SOFilter = ""
	if (Request("SONumber") <> "") then
		if (isNumeric(Request("SONumber"))) then
			SOFilter = " AND SO.SALEID = '" & Clng(Request("SONumber")) & "' "
		end if
	end if

        CompanyFilter = ""
    rqCompany = Request("COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " and SO.COMPANYID = '" & rqCompany & "' "
	end if

	
	SQLCODE ="SET CONCAT_NULL_YIELDS_NULL OFF;SELECT CUSTOMERTYPE = CASE WHEN TENANCYID IS NOT NULL THEN 'Tenant' WHEN SUPPLIERID IS NOT NULL THEN 'Company' WHEN SO.EMPLOYEEID IS NOT NULL THEN 'Employee' WHEN SO.SALESCUSTOMERID IS NOT NULL THEN 'Sales Customer' ELSE 'Unknown' END, " &_
			"CUSTOMER = CASE WHEN TENANCYID IS NOT NULL THEN TEN.LIST  + ' (' + CAST(TENANCYID AS VARCHAR) + ')' WHEN SUPPLIERID IS NOT NULL THEN O.NAME WHEN SO.EMPLOYEEID IS NOT NULL THEN EFOR.FIRSTNAME + ' ' + EFOR.LASTNAME WHEN SO.SALESCUSTOMERID IS NOT NULL THEN SC.CONTACT ELSE 'Unknown' END, " &_
			"E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, SO.SALEID, CONVERT(VARCHAR,SI.TOTALCOST,1) AS FORMATTEDCOST, SS.SOSTATUSNAME, SONAME, " &_
			"FORMATTEDSODATE=CONVERT(VARCHAR,SODATE,103) FROM F_SALESINVOICE SO " &_
			"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, SALEID FROM F_SALESINVOICEITEM WHERE ACTIVE = 1 GROUP BY SALEID) SI ON SI.SALEID = SO.SALEID " &_
			"LEFT JOIN F_SOSTATUS SS ON SO.SOSTATUS = SS.SOSTATUSID " &_
			"LEFT JOIN S_ORGANISATION O ON SO.SUPPLIERID = O.ORGID " &_
			"LEFT JOIN E__EMPLOYEE EFOR ON EFOR.EMPLOYEEID = SO.EMPLOYEEID " &_			
			"LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW TEN ON TEN.I = SO.TENANCYID " &_						
			"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SO.USERID " &_						
			"LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SO.SALESCUSTOMERID " &_									
			"WHERE SO.ACTIVE = 1 " & SOFilter & CompanyFilter &_			
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

        Call OpenDB()
Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
   	Call CloseDB()

%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Finance --> Sales Invoice List</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>
<script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
<script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
<script type="text/javascript" language="JavaScript">
<!--
function load_me(Sale_id){

    var strContent;

	strContent = navigator.userAgent;
	strContent = strContent.toLowerCase();
    
    if (strContent.search("chrome") > 0){
		var centerLeft = (screen.width/2)-(440/2);
		var centerTop = (screen.height/2)-(340/2);
        
		window.open("../SalesInvoice/Popups/SalesInvoice.asp?SaleID=" + Sale_id + "&Random=" + new Date(), "","Height=440px, Width=750px, left=" + centerLeft + ", top="+ centerTop +",edge=Raised, center=Yes, help= No, resizable= Yes, status= No, scroll= no");
		return;
	}
	else{
	    window.showModelessDialog("../SalesInvoice/Popups/SalesInvoice.asp?SaleID=" + Sale_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
		return;
	}
}

function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

function SubmitPage(){
	if (isNaN(thisForm.SONumber.value)){
		alert("When searching for a Sales Invoice you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
		}
    location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&SONumber=" + thisForm.SONumber.value + "&COMPANY=" + document.getElementById("sel_COMPANY").value;
	}
// -->
</script>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack"><tr><td><%=lstCompany%></td><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=SONumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("SONumber"))%>" onblur="RemoveBad()" style='width:135px;background-image:url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG src="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>