<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim ID
Dim Text
TotalFields = 20
ReDim DataFields   (TotalFields)
ReDim DataTypes    (TotalFields)
ReDim ElementTypes (TotalFields)
ReDim FormValues   (TotalFields)
ReDim FormFields   (TotalFields)
UpdateID	  = "hid_SCID"
FormFields(0) = "txt_DESCRIPTION|TEXT"
FormFields(1) = "txt_CREDITLIMIT|NUMBER"
FormFields(2) = "txt_PAYMENTTERMS|NUMBER"
FormFields(3) = "txt_CONTACT|TEXT"
FormFields(4) = "txt_ORGANISATION|TEXT"
FormFields(5) = "txt_ADDRESS1|TEXT"
FormFields(6) = "txt_ADDRESS2|TEXT"
FormFields(7) = "txt_ADDRESS3|TEXT"
FormFields(8) = "txt_TOWNCITY|TEXT"
FormFields(9) = "txt_COUNTY|TEXT"
FormFields(10) = "txt_POSTCODE|TEXT"
FormFields(11) = "txt_TELEPHONE1|TEXT"
FormFields(12) = "txt_TELEPHONE2|TEXT"
FormFields(13) = "txt_FAX|TEXT"
FormFields(14) = "txt_WEBSITE|TEXT"
FormFields(15) = "cde_MODIFIED|'" & FormatDateTime(Date,1) & " " & FormatDateTime(Date,4) & "'"
FormFields(16) = "cde_MODIFIEDBY|" & Session("USERID")
FormFields(17) = "cde_CREATED|'" & FormatDateTime(Date,1) & " " & FormatDateTime(Date,4) & "'"
FormFields(18) = "cde_CREATEDBY|" & Session("USERID")
FormFields(19) = "rdo_CUSACTIVE|NUMBER" 
FormFields(20) = "sel_COMPANYID|NUMBER" 
Function NewRecord ()
	
	Call MakeInsert(strSQL)	
	SQL = "SET NOCOUNT ON; INSERT INTO F_SALESCUSTOMER " & strSQL & "; SELECT SCOPE_IDENTITY() AS NEWID; SET NOCOUNT OFF"
	Call OpenRs (rsCompany, SQL)
	ID = rsCompany("NewID")
	Call CloseRs (rsCompany)
	GO()
End Function

Function AmendRecord()

	ID = Request.Form(UpdateID)
	Redim Preserve FormFields(20)
	Call MakeUpdate(strSQL)
	SQL = "UPDATE F_SALESCUSTOMER " & strSQL & " WHERE SCID = " & ID
 
	Conn.Execute SQL, recaffected

	GO()
End Function

Function DeleteRecord(Id, theID)
	ID = Request.Form(UpdateID)	
	
	SQL = "DELETE FROM F_SALESCUSTOMER WHERE SCID = '" & ID & "'" 
	Conn.Execute SQL, recaffected
End Function

Function GO()
	CloseDB()
	Response.Redirect "../SalesCustomerList.asp?"
End Function

TheAction = Request("hid_Action")

OpenDB()
Select Case TheAction
	Case "NEW"		NewRecord()
	Case "AMEND"	AmendRecord()
	Case "DELETE"   DeleteRecord()
	Case "LOAD"	    LoadRecord()
End Select
CloseDB()
%>