<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, intReturn, theamounts
	
	OpenDB()	
	
	Dim SOName
	SOName = ""
	if(Request.Form("txt_SONAME") <> "") then SOName = Replace(Request.Form("txt_SONAME"), "'", "''")

	Dim TAXDate
	TAXDate = ""
	if(Request.Form("txt_TAXDATE") <> "") then TAXDate = FormatDateTime(Request.Form("txt_TAXDATE"),1)
	
	Dim SONOTES
	SONOTES = ""
	if(Request.Form("txt_SONOTES") <> "") then SONOTES = Replace(Request.Form("txt_SONOTES"), "'", "''")
	
	Dim STYPE
	STYPE = "NULL"
	if(Request.Form("hid_TYPE") <> "") then STYPE = Request.Form("hid_TYPE")

    Dim CompanyId
	CompanyId = "NULL"
	if(Request.Form("sel_COMPANY") <> "") then CompanyId = Request.Form("sel_COMPANY")

	Tenancy = "NULL"
	Employee = "NULL"
	Supplier = "NULL"		
	SalesCustomer = "NULL"
	
	IF (STYPE = 1) THEN
		if(Request.Form("hid_CODE") <> "") then Tenancy = Request.Form("hid_CODE")
	ELSEIF (STYPE = 2) THEN
		if(Request.Form("hid_CODE") <> "") then Employee = Request.Form("hid_CODE")
	ELSEIF (STYPE = 3) THEN
		if(Request.Form("hid_CODE") <> "") then Supplier = Request.Form("hid_CODE")
	ELSEIF (STYPE = 4) THEN
		if(Request.Form("hid_CODE") <> "") then SalesCustomer = Request.Form("hid_CODE")
	END IF

	SQL = "SET NOCOUNT ON;INSERT INTO F_SALESINVOICE (SONAME, SODATE, SONOTES, USERID, SUPPLIERID, EMPLOYEEID, TENANCYID, SALESCUSTOMERID, ACTIVE, SOTYPE, SOSTATUS, COMPANYID) VALUES " &_
			"('" & SOName & "', '" & TAXDate & "', '" & SONOTES & "', " & Session("USERID") & ", " & Supplier & ", " &_
			" " & Employee & ", " & Tenancy & ", " & SalesCustomer & ", 1, 1, 1, " & CompanyId & ");SELECT SCOPE_IDENTITY() AS SALEID;SET NOCOUNT OFF"
	OpenDB()
	Call OpenRs(rsSALE, SQL)		
	SaleID = rsSALE("SALEID")
	CloseRs(rsSALE)

	for each item in Request.Form("ID_ROW")	
		Data = Request.Form("iData" & item)
		DataArray = Split(Data, "||<>||")
		
		ItemRef = Replace(DataArray(0), "'", "''")
		ItemDesc = Replace(DataArray(1), "'", "''")
		VatTypeID = DataArray(2)
		NetCost = DataArray(3)
		VAT = DataArray(4)
		GrossCost = DataArray(5)
		SaleCat = DataArray(6)
		SaveStatus = DataArray(7)										
		RentJournal_id = "NULL"

		'IF A TENANT PUT A TENANT RECHARGE LINE ON TO THE RENT JOURNAL FOR EVERY ITEM.
		if (STYPE = 1) then
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL (TRANSACTIONDATE, PAYMENTTYPE, ITEMTYPE, TENANCYID, AMOUNT, STATUSID) VALUES (" &_
					"'" & TAXDate & "', NULL, 5, " & Tenancy & ", " & GrossCost & ",2);" &_
				"SELECT SCOPE_IDENTITY() AS JOURNALID;"
			set rsRentJourn = Conn.Execute(SQL)
			RentJournal_id = rsRentJourn("JOURNALID")
		end if
		
		SQL = "INSERT INTO F_SALESINVOICEITEM (SALEID, SALESCATID, ITEMNAME, ITEMDESC, SIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, SITYPE, SISTATUS, RENTJOURNALID) VALUES " &_
				"(" & SaleID & ", " & SaleCat & ", '" & ItemRef & "', '" & ItemDesc & "', '" & TAXDate & "', " &_
				" " & NetCost & ", " & VatTypeID & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 1, 1, " & RentJournal_id & ")"
		Conn.Execute SQL
	next
	
	Conn.Execute "EXEC NL_SALESORDER " & SaleID
	
        SQL="EXEC F_SALES_LEDGER_BALANCE " & Request.Form("hid_TYPE") & " , " & Request.Form("hid_CODE") & " , "  & GrossCost & " "
	Conn.Execute(SQL)

	
	CloseDB()

	Response.Redirect "../SalesList.asp"										
%>

