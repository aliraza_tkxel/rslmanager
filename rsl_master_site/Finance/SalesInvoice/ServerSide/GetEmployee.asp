<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()

	SQL = "SELECT E.EMPLOYEEID, GT.DESCRIPTION AS TITLE, E.FIRSTNAME, E.LASTNAME, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.POSTALTOWN, P.POSTCODE, P.COUNTY FROM E__EMPLOYEE E " &_
			"INNER JOIN E_CONTACT P ON P.EMPLOYEEID = E.EMPLOYEEID " &_
			"LEFT JOIN G_TITLE GT ON E.TITLE = GT.TITLEID " &_
			"WHERE E.EMPLOYEEID = " & Request("EID")

	Call OpenRS(rsEmployee, SQL)

	count = 1
	IF NOT rsEmployee.EOF THEN
		
		StringName = ""
		Title = rsEmployee("TITLE")
		if (Title <> "" AND NOT isNull(Title)) then
			StringName = StringName & Title & " "
		end if
		FirstName = rsEmployee("FIRSTNAME")
		if (FirstName <> "" AND NOT isNull(FirstName)) then
			StringName = StringName & FirstName & " "
		end if
		LastName = rsEmployee("LASTNAME")
		if (LastName <> "" AND NOT isNull(LastName)) then
			StringName = StringName & LastName & " "
		end if
		
		RemainingAddressString = ""
		AddressArray = Array("ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsEmployee(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
			end if
		next
		FullAddress = "" & StringName & "<BR>" & RemainingAddressString					
	else
		FullAddress = "Details Could not be found!<br>Contact Support Team immediately."
	end if
	
	Call CloseDB()

	Response.Write FullAddress
%>