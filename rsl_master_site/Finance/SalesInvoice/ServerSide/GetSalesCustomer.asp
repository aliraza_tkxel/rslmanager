<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()

	SQL = "SELECT O.SCID, O.CONTACT, O.ORGANISATION, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM F_SALESCUSTOMER O " &_
			"WHERE SCID = " & Request("CID") & " AND CUSACTIVE=1"


	Call OpenRS(rsCompany, SQL)

	IF (not rsCompany.EOF) then
		StringFullName = rsCompany("CONTACT")
	
		RemainingAddressString = ""
		AddressArray = Array("ORGANISATION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCompany(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
			end if
		next
		FullAddress = "" & StringFullName & "<BR>" & RemainingAddressString			
	else
		FullAddress = "Details Could not be found!<br>Contact Support Team immediately."
	end if
	
	Call CloseDB()

	Response.Write FullAddress
%>