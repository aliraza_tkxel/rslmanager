<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()

	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			"WHERE T.TENANCYID = " & Request("TID")

	Call OpenRS(rsCust, SQL)

	count = 1
	while NOT rsCust.EOF
		
		StringName = ""
		Title = rsCust("TITLE")
		if (Title <> "" AND NOT isNull(Title)) then
			StringName = StringName & Title & " "
		end if
		FirstName = rsCust("FIRSTNAME")
		if (FirstName <> "" AND NOT isNull(FirstName)) then
			StringName = StringName & FirstName & " "
		end if
		LastName =rsCust("LASTNAME")
		if (LastName <> "" AND NOT isNull(LastName)) then
			StringName = StringName & LastName & " "
		end if
		
		if (count = 1) then
			StringFullName = StringName
			count = 2
		else
			StringFullName = StringFullName & "& " & StringName
		end if
		rsCust.moveNext
	wend
	if (count = 2) then
		rsCust.moveFirst()
		housenumber = rsCust("housenumber")
		if (housenumber = "" or isNull(housenumber)) then
			housenumber = rsCust("flatnumber")
		end if
		if (housenumber = "" or isNull(housenumber)) then
			FirstLineOfAddress = rsCust("Address1")
		else
			FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
		end if

		RemainingAddressString = ""
		AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCust(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
			end if
		next
		tenancyref = rsCust("TENANCYID")
	end if
	
	FullAddress = "" & StringFullName & "<BR>" & "" & FirstLineOfAddress & "<BR>" &	RemainingAddressString			

	Call CloseDB()

	Response.Write FullAddress
%>