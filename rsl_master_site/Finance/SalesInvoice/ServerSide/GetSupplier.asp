<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Call OpenDB()

	SQL = "SELECT O.ORGID, O.NAME, O.BUILDINGNAME, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM S_ORGANISATION O " &_
			"WHERE ORGID = " & Request("CID")

	Call OpenRS(rsCompany, SQL)

	IF (not rsCompany.EOF) then
		StringFullName = rsCompany("NAME")
	
		RemainingAddressString = ""
		AddressArray = Array("BUILDINGNAME", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCompany(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
			end if
		next
		FullAddress = "" & StringFullName & "<BR>" & RemainingAddressString			
	else
		FullAddress = "Details Could not be found!<br>Contact Support Team immediately."
	end if
	
	Call CloseDB()

	Response.Write FullAddress
%>