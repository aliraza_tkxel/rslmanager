<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

tenantname = Replace(Request.Form("txt_TENANTNAME"), "'", "''")
	if (tenantname = "") then 
		tenantsql = ""
	else
    'LastNameSql = " AND (CNGV.List LIKE '%" & LastName & "%') "
		tenantsql = " AND (CNGV.List LIKE '%" & tenantname & "%') "
	end if

	tenancyid = Replace(Request.Form("txt_TENANCYID"), "'", "''")
	if (tenancyid = "") then 
		tenancysql = ""
	else
		tenancysql = " AND SD.TENANCYID = '" & tenancyid & "' "
	end if

	organisation = Replace(Request.Form("txt_ORGANISATION"), "'", "''")
	if (organisation = "") then 
		organisationsql = ""
	else
		organisationsql = " AND O.NAME LIKE '%" & organisation & "%' "
	end if

	salescustomer = Replace(Request.Form("txt_SALESCUSTOMER"), "'", "''")
	if (salescustomer = "") then 
		salescustomersql = ""
	else
		salescustomersql = " AND (SC.CONTACT LIKE '%" & salescustomer & "%' OR SC.ORGANISATION LIKE '%" & salescustomer & "%') "
	end if
	
	employee = Replace(Request.Form("txt_EMPLOYEE"), "'", "''")
	if (employee = "") then 
		employeesql = ""
	else
		employeesql = " AND (E.FIRSTNAME + ' ' + E.LASTNAME LIKE '%" & employee & "%' OR E.FIRSTNAME LIKE '%" & employee & "%' OR E.LASTNAME LIKE '%" & employee & "%') "
	end if

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND (SC.COMPANYID = '" & rqCompany & "' )"
	end if
	
	WhereStart = ""
	if (organisationsql <> "" OR employeesql <> "" OR tenancysql <> "" OR salescustomersql <> ""  OR tenantsql <> "" or CompanyFilter <> ""  ) then
		WhereStart = " WHERE 1=1  "
	end if
	
	OpenDB()
	MaxRecords = 300
	SQL = "SET CONCAT_NULL_YIELDS_NULL OFF;SELECT TOP " & MaxRecords & " " &_
			"	DESCRIPTION = CASE " &_
			"	WHEN SD.TENANCYID IS NOT NULL THEN 'ONCLICK=""Load(1,' + CAST(SD.TENANCYID AS VARCHAR) + ')""><TD width=110px>Tenancy</TD><TD>' + CAST(SD.TENANCYID AS VARCHAR) + ' : ' + CNGV.LIST " &_
			"	WHEN SUPPLIERID IS NOT NULL THEN 'ONCLICK=""Load(3,' + CAST(SD.SUPPLIERID AS VARCHAR) + ')""><TD width=110px>Supplier</TD><TD>' + O.NAME " &_
			"	WHEN SD.EMPLOYEEID IS NOT NULL THEN 'ONCLICK=""Load(2,' + CAST(SD.EMPLOYEEID AS VARCHAR) + ')""><TD width=110px>Employee</TD><TD>' + E.FIRSTNAME + ' ' + E.LASTNAME " &_
			"	WHEN SD.SALESCUSTOMERID IS NOT NULL THEN 'ONCLICK=""Load(4,' + CAST(SD.SALESCUSTOMERID AS VARCHAR) + ')""><TD width=110px>Sales Customer</TD><TD>' + SC.CONTACT " &_			
			"	ELSE '<TR><TD colspan=2 align=center>IT NOT WORKING' " &_
			"	END " &_
			"FROM " &_
			"( " &_
			"	SELECT TENANCYID, SUPPLIERID, EMPLOYEEID, SALESCUSTOMERID FROM F_SALESINVOICE " &_
			"	WHERE TENANCYID IS NOT NULL OR SUPPLIERID IS NOT NULL OR EMPLOYEEID IS NOT NULL OR SALESCUSTOMERID IS NOT NULL " &_
			"	UNION " &_
			"	SELECT TENANCYID, SUPPLIERID, EMPLOYEEID, SALESCUSTOMERID FROM F_SALESPAYMENT " &_
			"	WHERE TENANCYID IS NOT NULL OR SUPPLIERID IS NOT NULL OR EMPLOYEEID IS NOT NULL OR SALESCUSTOMERID IS NOT NULL " &_
			") SD " &_
			"	LEFT JOIN C_TENANCY T ON T.TENANCYID = SD.TENANCYID " &_
			"	LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = SD.TENANCYID " &_
			"	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SD.EMPLOYEEID " &_
			"	LEFT JOIN S_ORGANISATION O ON O.ORGID = SD.SUPPLIERID " &_
			"	LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SD.SALESCUSTOMERID AND CUSACTIVE=1 " &_			
			WhereStart & organisationsql & tenancysql & employeesql & salescustomersql  & tenantsql & CompanyFilter & " ORDER BY NAME, DESCRIPTION"

	'Response.Write SQL
	Call OpenRs(rsSearch, SQL)
	

%>
<html>
<head></head>
<SCRIPT LANGUAGE=JAVASCRIPT>
    function Load(TYPE, ID) {
        parent.location.href = "../SalesAccount.asp?ACCID=" + ID + "&ACCTYPE=" + TYPE
    }
</SCRIPT>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body leftmargin=0 topmargin=0 marginheight=0 marginwidth=0>
<table cellspacing=0 cellpadding=0 height=100%><tr><td valign=top height=20>
<table>
	<tr>
		<td width=110px nowrap style='color:#133E71'><b>Type:</b></td>
		<td width=260px nowrap style='color:#133E71'><b>Desc:</b></td>
		<td width=30></td>
	</tr>
	<tr style='line-height:2'>
		<td colspan=4 height=2 style='line-height:2'><hr height=2 style='border:1px dotted #133E71'></td>
	</tr>
</table>
</td></tr>
<tr><td height=100% valign=top>
<div style='height:100%;overflow:auto' class='TA'>
<table style='behavior:url(/Includes/Tables/tablehl.htc)' hlcolor=#133e71 WIDTH=393>
<%
	COUNT = 0 
	if (not rsSearch.EOF) then

		while NOT rsSearch.EOF
			DESC = rsSearch("DESCRIPTION")

			Response.Write "<TR style='cursor:hand;line-height:2' " & DESC & " </TD></TR>" 

			'onclick=""LoadCompany(" & COMPANYID & ")""><TD width=240px>" & rsSearch("NAME") & "</TD><TD width=160px>" & rsSearch("TRADE") & "</TD></TR>"
			COUNT = COUNT + 1	
			rsSearch.moveNext
		wend
	else
		Response.Write "<TR><TD colspan=2 align=center>No Records Found</td></TR>"		
	end if
	if (COUNT = MaxRecords) then
		Response.Write "<TR><TD colspan=2 align=center>Maximum number of records returned.<br>To Filter the list apply more filters.</TD></TR>"
	end if

	CloseRs(rsSearch)
	CloseDB()
%>
</table>
</div>
</td></tr>
<tr><td height=20>
<table WIDTH=408>
<tfoot>
	<tr style='line-height:2'>
		<td colspan=4 height=2 style='line-height:2'><hr height=2 style='border:1px dotted #133E71'></td>
	</tr>
	<TR><td colspan=4 align=center>TOTAL RECORDS RETURNED : <%=COUNT%></TD></TR>
</tfoot>	
</table>				
</td></tr></table>	
</body>
</html>