<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

Call BuildSelect(lstCATS, "sel_SALESCAT", "F_SALESCAT WHERE SALESCATID <> 1", "SALESCATID, DESCRIPTION", "DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " tabindex=4")
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:115' tabindex=5")
Call BuildSelect(lstCompany, "sel_COMPANY", "G_Company", "CompanyId, Description", "CompanyId", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeCostCentre()'  tabindex=5")


	SQL = "EXEC GET_VALIDATION_PERIOD 18"
	Call OpenRs(rsTAXDATE, SQL)	
	    YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	    YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

CloseDB()

TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
%>
<html>
<head>
    <title>RSL Manager Finance - Create Purchase Order</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
</head>
<script language="JavaScript" src="/js/preloader.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/general.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/menu.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/FormValidation.js" type="text/javascript"></script>
<script language="JavaScript" src="/js/financial.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
    function LoadCustomer(CID, TID) {
        THISFORM.hid_CODE.value = TID
        THISFORM.hid_TYPE.value = 1

        var xh;

        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xh = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xh = new ActiveXObject("Microsoft.XMLHTTP");
        }

        //var xmlobj = new ActiveXObject("Microsoft.XMLDOM")
        var url = "/Finance/SalesInvoice/ServerSide/GetCustomer.asp?CID=" + CID + "&TID=" + TID;
        var nodeMap
        xh.open("POST", url, false);
        xh.send();
        CustomerInfo.innerHTML = xh.responseText;
        //xmlobj.async = false;
    }

    function LoadEmployee(EID) {
        THISFORM.hid_CODE.value = EID
        THISFORM.hid_TYPE.value = 2

        var xh;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xh = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xh = new ActiveXObject("Microsoft.XMLHTTP");
        }

        //var xmlobj = new ActiveXObject("Microsoft.XMLDOM")
        var url = "/Finance/SalesInvoice/ServerSide/GetEmployee.asp?EID=" + EID;
        var nodeMap
        xh.open("POST", url, false);
        xh.send();
        CustomerInfo.innerHTML = xh.responseText;
        //xmlobj.async = false;
    }

    function LoadCompany(CID) {
        THISFORM.hid_CODE.value = CID
        THISFORM.hid_TYPE.value = 3

        var xh;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xh = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xh = new ActiveXObject("Microsoft.XMLHTTP");
        }

        //var xmlobj = new ActiveXObject("Microsoft.XMLDOM")
        var url = "/Finance/SalesInvoice/ServerSide/GetSupplier.asp?CID=" + CID;
        var nodeMap
        xh.open("POST", url, false);
        xh.send();
        CustomerInfo.innerHTML = xh.responseText;
        //xmlobj.async = false;
    }

    function LoadSalesCustomer(CID) {
        THISFORM.hid_CODE.value = CID
        THISFORM.hid_TYPE.value = 4


        var xh;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xh = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xh = new ActiveXObject("Microsoft.XMLHTTP");
        }

        //var xmlobj = new ActiveXObject("Microsoft.XMLDOM")
        var url = "/Finance/SalesInvoice/ServerSide/GetSalesCustomer.asp?CID=" + CID;
        var nodeMap
        xh.open("POST", url, false);
        xh.send();
        CustomerInfo.innerHTML = xh.responseText;
        //xmlobj.async = false;
    }

    function GetInfo(I) {
        if (I == 1)
            window.open("SearchPopups/SearchTenant.asp?Random=" + new Date(), "_blank", "height= 540, width= 460, status= no, resizable= yes")
        else if (I == 2)
            window.open("SearchPopups/SearchEmployee.asp?Random=" + new Date(), "_blank", "height= 540, width= 460, status= no, resizable= yes")
        else if (I == 3)
            window.open("SearchPopups/SearchSupplier.asp?Random=" + new Date(), "_blank", "height= 540, width= 460, status= no, resizable= yes")
        else if (I == 4)
            window.open("SearchPopups/SearchSalesCustomer.asp?Random=" + new Date(), "_blank", "height= 540, width= 460, status= no, resizable= yes")
    }

    var FormFields = new Array()

    function SetChecking(Type) {
        FormFields.length = 0
        if (Type == 1) {
            FormFields[0] = "sel_SALESCAT|Category|SELECT|Y"
            FormFields[1] = "txt_ITEMREF|Item Ref|TEXT|Y"
            FormFields[2] = "txt_ITEMDESC|Item Description|TEXT|N"
            FormFields[3] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
            FormFields[4] = "txt_VAT|VAT|CURRENCY|Y"
            FormFields[5] = "txt_NETCOST|Net Cost|CURRENCY|Y"
        }
        else if (Type == 2) {
            FormFields[0] = "txt_SONAME|Name|TEXT|Y"
            FormFields[1] = "hid_TYPE|Customer|TEXT|Y"
            FormFields[2] = "txt_TAXDATE|Tax Date|DATE|Y"
            FormFields[3] = "txt_SONOTES|Sales Notes|TEXT|N"
        }
        else if (Type == 3) {
            FormFields[0] = "sel_SALESCAT|Category|SELECT|n"
            FormFields[1] = "txt_ITEMREF|Item Ref|TEXT|N"
            FormFields[2] = "txt_ITEMDESC|Item Description|TEXT|N"
            FormFields[3] = "txt_GROSSCOST|Gross Cost|CURRENCY|N"
            FormFields[4] = "txt_VAT|VAT|CURRENCY|N"
            FormFields[5] = "txt_NETCOST|Net Cost|CURRENCY|N"
        }
    }

    var RowCounter = 0

    function AddRow(insertPos) {
        SetChecking(1)
        if (!checkForm()) return false
        GrossCost = document.getElementById("txt_GROSSCOST").value

        SaveStatus = 1

        RowCounter++
        ItemRef = document.getElementById("txt_ITEMREF").value
        ItemDesc = document.getElementById("txt_ITEMDESC").value
        SalesCatID = document.getElementById("sel_SALESCAT").value
        SalesCatName = document.getElementById("sel_SALESCAT").options(document.getElementById("sel_SALESCAT").selectedIndex).text
        NetCost = document.getElementById("txt_NETCOST").value
        VAT = document.getElementById("txt_VAT").value
        VatTypeID = document.getElementById("sel_VATTYPE").value
        VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text
        VatTypeCode = VatTypeName.substring(0, 1).toUpperCase()
        CompanyId = document.getElementById("sel_COMPANY").value	

        oTable = document.getElementById("ItemTable")
        for (i = 0; i < oTable.rows.length; i++) {
            if (oTable.rows(i).id == "EMPTYLINE") {
                oTable.deleteRow(i)
                break;
            }
        }

        oRow = oTable.insertRow(insertPos)
        oRow.id = "TR" + RowCounter
        oRow.onclick = AmendRow
        oRow.style.cursor = "hand"

        DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + SalesCatID + "||<>||" + CompanyId + "||<>||" + SaveStatus + "\">"

        AddCell(oRow, ItemRef + DATA, ItemDesc, "", "")
        AddCell(oRow, SalesCatName, "", "", "")
        AddCell(oRow, VatTypeCode, VatTypeName + " Rate", "center", "")
        AddCell(oRow, FormatCurrencyComma(NetCost), "", "right", "")
        AddCell(oRow, FormatCurrencyComma(VAT), "", "right", "")
        AddCell(oRow, FormatCurrencyComma(GrossCost), "", "right", "")
        DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
        AddCell(oRow, DelImage, "", "center", "#FFFFFF")

        SetTotals(NetCost, VAT, GrossCost)
        ResetData()
    }

    function ResetData() {
        SetChecking(3)
        checkForm()

        ResetArray = new Array("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_SALESCAT")
        for (i = 0; i < ResetArray.length; i++)
            document.getElementById(ResetArray[i]).value = ""
        document.getElementById("sel_VATTYPE").selectedIndex = 0;
        document.getElementById("txt_VAT").value = "0.00";
        document.getElementById("AmendButton").style.display = "none"
        document.getElementById("AddButton").style.display = "block"
    }

    function AmendRow() {
        event.cancelBubble = true
        Ref = this.id
        RowNumber = Ref.substring(2, Ref.length)
        StoredData = document.getElementById("iDATA" + RowNumber).value
        StoredArray = StoredData.split("||<>||")

        ReturnArray = new Array("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST", "sel_SALESCAT")
        for (i = 0; i < ReturnArray.length; i++)
            document.getElementById(ReturnArray[i]).value = StoredArray[i]
        document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
        document.getElementById("AddButton").style.display = "none"
        document.getElementById("AmendButton").style.display = "block"
    }

    function UpdateRow() {
        sTable = document.getElementById("ItemTable")
        RowData = document.getElementById("UPDATEID").value
        RowArray = RowData.split("||<>||")
        MatchRow = RowArray[0]
        for (i = 0; i < sTable.rows.length; i++) {
            if (sTable.rows(i).id == "TR" + MatchRow) {
                sTable.deleteRow(i)
                SetTotals(-RowArray[1], -RowArray[2], -RowArray[3])
                break;
            }
        }
        AddRow(i)
    }

    function SetTotals(iNE, iVA, iGC) {
        totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
        totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
        totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

        document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
        document.getElementById("hiVA").value = FormatCurrency(totalVAT)
        document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)

        document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
        document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
        document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)
    }

    function AddCell(iRow, iData, iTitle, iAlign, iColor) {
        oCell = iRow.insertCell()
        oCell.innerHTML = iData
        if (iTitle != "") oCell.title = iTitle
        if (iAlign != "") oCell.style.textAlign = iAlign
        if (iColor != "") oCell.style.backgroundColor = iColor
    }

    function DeleteRow(RowID, NE, VA, GR) {
        oTable = document.getElementById("ItemTable")
        for (i = 0; i < oTable.rows.length; i++) {
            if (oTable.rows(i).id == "TR" + RowID) {
                oTable.deleteRow(i)
                SetTotals(-NE, -VA, -GR)
                break;
            }
        }
        if (oTable.rows.length == 1) {
            oRow = oTable.insertRow()
            oRow.id = "EMPTYLINE"
            oCell = oRow.insertCell()
            oCell.colSpan = 7
            oCell.innerHTML = "Please enter an item from above"
            oCell.style.textAlign = "center"
        }
        ResetData()
    }
    function real_date(str_date) {
        var datearray;
        var months = new Array("nowt", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
        str_date = new String(str_date);
        datearray = str_date.split("/");
        return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
    }

    function SavePurchaseOrder() {

        this.disabled = true;
        var YStart = new Date("<%=YearStartDate%>")
        var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
        var YEnd = new Date("<%=YearendDate%>")
        var YStartPlusTime = new Date('<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>')
        var YEndPlusTime = new Date('<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>')

        if (real_date(THISFORM.txt_TAXDATE.value) < YStart || real_date(THISFORM.txt_TAXDATE.value) > YEnd) {
             alert("The month is now locked down and entries can only be made for the current month");

            this.disabled = false;
            return false;
        }		

        ResetData()
        SetChecking(2)
        if (!checkForm()) return false
        if (parseFloat(document.getElementById("hiGC").value) <= 0) {
            alert("Please enter some items before creating a invoice.")
            return false;
        }
        THISFORM.target = ""
        THISFORM.method = "POST"
        THISFORM.action = "ServerSide/CreateSalesInvoice_svr.asp"
        THISFORM.submit()
    }		
</script>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(3);preloadImages();" onunload="macGo()"
    style="margin-left: 10; margin-top: 10;">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <iframe src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style='display: NONE'>
    </iframe>
    <form name="THISFORM" method="post">
    <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="3" width="755">
        <tr bgcolor="steelblue" style='color: white'>
            <td colspan="6">
                <b>SALES INVOICE INFORMATION</b>
            </td>
        </tr>
        <tr>
            <td>
                Name :
            </td>
            <td>
                <input name="txt_SONAME" type="text" class="TEXTBOX200" size="50" maxlength="50"
                    tabindex="1" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_SONAME" />
            </td>
            <td rowspan="3" valign="top" align="center">
                <img src="/Customer/Images/T.gif" width="16" height="16" onclick="GetInfo(1)" style='cursor: hand'
                    alt="Tenant" /><br />
                <br />
                <img src="/Finance/SalesInvoice/images/employe_image.gif" width="21" height="21"
                    onclick="GetInfo(2)" style='cursor: hand' alt="Employee" /><br />
                <br />
                <img src="/Finance/SalesInvoice/images/truck_image.gif" width="21" height="21" onclick="GetInfo(3)"
                    style='cursor: hand' alt="Supplier" /><br />
                <br />
                <img src="/Finance/SalesInvoice/images/im_salescustomer.gif" width="15" height="16"
                    onclick="GetInfo(4)" style='cursor: hand' alt="Sales Customer" /><br />
            </td>
            <td rowspan="3" valign="middle" width="100%">
                <input type="hidden" name="hid_TYPE" /><input type="hidden" name="hid_CODE" />
                <div id="CustomerInfo" style='border: 1px dotted #133e71; background-color: beige;
                    width: 310; height: 107; padding: 10px'>
                </div>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_TYPE" />
            </td>
        </tr>
        <tr>
            <td nowrap valign="top">
                Sales Notes :
            </td>
            <td>
                <textarea name="txt_SONOTES" cols="55" rows="5" class="TEXTBOX200" tabindex="1" style='overflow: hidden;
                    border: 1px solid #133E71'></textarea>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_SONOTES" />
            </td>
        </tr>
        <tr>
            <td>
                Tax Date :
            </td>
            <td>
                <input name="txt_TAXDATE" type="text" class="TEXTBOX200" tabindex="1" size="50" maxlength="10"
                    value="<%=FormatDateTime(Date,2)%>" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_TAXDATE" />
            </td>
        </tr>
         <tr>
            <td>
                Company :
            </td>
            <td>
                <%=lstCompany%>
            </td>
            <td>
               
            </td>
        </tr>
    </table>
    <br />
    <table style='border: 1PX SOLID BLACK' cellspacing="0" cellpadding="3" width="755">
        <tr bgcolor="steelblue" style='color: white'>
            <td colspan="9">
                <b>NEW ITEM</b>
            </td>
        </tr>
        <tr>
            <td>
                Item :
            </td>
            <td>
                <input name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex="3" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMREF" />
            </td>
            <td nowrap>
                &nbsp;Net Price :
            </td>
            <td>
                <input style='text-align: right' name="txt_NETCOST" type="text" class="textbox" maxlength="20"
                    size="20" onblur="TotalBoth();ResetVAT()" onfocus="alignLeft()" tabindex="5" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_NETCOST" />
            </td>
            <td width="100%" rowspan="5" valign="top">
                <div style='border: 1px dotted #133e71; background-color: beige; height: 107; padding: 10px'>
                    <li>Enter name of item
                        <li>Enter a brief description
                            <li>Select the category for the item
                                <li>Enter the cost/charge for the item
                                    <li>Select the appropriate VAT type
                                        <li>Click 'ADD' to save to list
                                            <li>
                    Repeat for other items
                </div>
            </td>
        </tr>
        <tr>
            <td rowspan="3" valign="top">
                Notes :
            </td>
            <td rowspan="3">
                <textarea tabindex="3" name="txt_ITEMDESC" rows="5" class="TEXTBOX200" style='overflow: hidden;
                    border: 1px solid #133E71'></textarea>
            </td>
            <td rowspan="3">
                <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMDESC" />
            </td>
            <td nowrap>
                &nbsp;Vat Type :
            </td>
            <td>
                <%=lstVAT%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_VATTYPE" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;VAT :
            </td>
            <td>
                <input style='text-align: right' name="txt_VAT" type="text" class="textbox" maxlength="20"
                    size="20" onblur="TotalBoth()" onfocus="alignLeft()" value="0.00" tabindex="5" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_VAT" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Total :
            </td>
            <td>
                <input style='text-align: right' name="txt_GROSSCOST" type="text" class="textbox"
                    maxlength="20" size="20" readonly="readonly" tabindex="-1" />
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;Category :
            </td>
            <td>
                <%=lstCATS%>
            </td>
            <td>
                <img src="/js/FVS.gif" width="15" height="15" name="img_SALESCAT" />
            </td>
            <td align="right" colspan="2">
                <input type="hidden" name="IACTION" />
                <input type="hidden" name="UPDATEID" />
                <input type="hidden" name="EXPENDITURE_LEFT_LIST" />
                <input type="hidden" name="EMPLOYEE_LIMIT_LIST" />
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td nowrap>
                            <input type="button" name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()"
                                tabindex="6" />&nbsp;
                        </td>
                        <td>
                            <input type="button" name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(-1)"
                                tabindex="6" />
                        </td>
                        <td>
                            <input type="button" name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateRow()"
                                style="display: none" tabindex="6" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table style='border: 1PX SOLID BLACK; behavior: url(/Includes/Tables/tablehl.htc)'
        cellspacing="0" cellpadding="3" width="755" id="ItemTable" slcolor='' hlcolor="silver">
        <thead>
            <tr bgcolor="steelblue" align="right" style='color: white'>
                <td height="20" align="left" width="250" nowrap>
                    <b>Item Name:</b>
                </td>
                <td width="200" nowrap align="left">
                    <b>Category:</b>
                </td>
                <td width="40" nowrap>
                    <b>Code:</b>
                </td>
                <td width="80" nowrap>
                    <b>Net (�):</b>
                </td>
                <td width="75" nowrap>
                    <b>VAT (�):</b>
                </td>
                <td width="80" nowrap>
                    <b>Gross (�):</b>
                </td>
                <td width="29" nowrap>
                    &nbsp;
                </td>
            </tr>
        </thead>
        <tbody>
            <tr id="EMPTYLINE">
                <td colspan="7" align="center">
                    Please enter an item from above
                </td>
            </tr>
        </tbody>
    </table>
    <table style='border: 1PX SOLID BLACK; border-top: none' cellspacing="0" cellpadding="2"
        width="755">
        <tr bgcolor="steelblue" align="RIGHT">
            <td height="20" width="490" nowrap style='border: none; color: white'>
                <b>TOTAL : &nbsp;</b>
            </td>
            <td width="80" nowrap bgcolor="white">
                <b>
                    <input type="hidden" id="hiNC" value="0" /><div id="iNC">
                        0.00</div>
                </b>
            </td>
            <td width="75" nowrap bgcolor="white">
                <b>
                    <input type="hidden" id="hiVA" value="0" /><div id="iVA">
                        0.00</div>
                </b>
            </td>
            <td width="80" nowrap bgcolor="white">
                <b>
                    <input type="hidden" id="hiGC" value="0" /><div id="iGC">
                        0.00</div>
                </b>
            </td>
            <td width="29" nowrap bgcolor="white">
                &nbsp;
            </td>
        </tr>
    </table>
    <br>
    <table cellspacing="0" cellpadding="2" width="755">
        <tr align="right">
            <td width="754" align="right" nowrap>
                &nbsp;<input type="button" value=" CREATE INVOICE " class="RSLButton" onclick="SavePurchaseOrder()" />
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
