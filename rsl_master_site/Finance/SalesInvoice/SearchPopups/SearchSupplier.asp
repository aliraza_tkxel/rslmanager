<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
OpenDB()
Call BuildSelect(lstTrades, "sel_TRADE", "S_COMPANYTRADE", "TRADEID, DESCRIPTION", "DESCRIPTION", "Please Select", l_trade, NULL, "textbox200", " TABINDEX=2" )
CloseDB()
%>
<html>
<head>
    <title>Find Customer</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script language="JavaScript" src="/js/general.js" type="text/javascript"></script>
    <script language="JavaScript" src="/js/FormValidation.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        //var Ref = dialogArguments;

        var FormFields = new Array();
        FormFields[0] = "txt_NAME|Company Name|TEXT|N"
        FormFields[1] = "sel_TRADE|Trade|SELECT|N"

        function SearchNow() {
            if (!checkForm()) return false
            RSLFORM.action = "SupplierSearch_svr.asp"
            RSLFORM.target = "SearchResults"
            RSLFORM.submit()
            RSLFORM.submit()
            return false
        }

        var ReadyBool = false
        function Ready() {
            ReadyBool = true
        }

        function Results() {
            if (!ReadyBool) return false
            document.getElementById("SearchDiv").style.display = "none"
            document.getElementById("SearchResults").style.display = "block"
            document.getElementById("tab_search").src = "../images/im_findanorganisation.gif"
            document.getElementById("tab_results").src = "../images/im_searchresults_o-05.gif"
        }

        function Search() {
            if (!ReadyBool) return false
            document.getElementById("SearchDiv").style.display = "block"
            document.getElementById("SearchResults").style.display = "none"
            document.getElementById("tab_search").src = "../images/im_findanorganisation_d.gif"
            document.getElementById("tab_results").src = "../images/im_searchresults_o.gif"
        }		
    </script>
</head>
<body bgcolor="#FFFFFF" text="#000000" style='padding-left: 10px; padding-top: 10px'
    onload="Ready()">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" style='cursor: hand'>
                <img name="tab_search" id="tab_search" src="../images/im_findanorganisation_d.gif"
                    width="138" height="20" border="0" onclick="Search()">
            </td>
            <td rowspan="2" style='cursor: hand'>
                <img name="tab_results" id="tab_results" src="../images/im_searchresults_o.gif" width="121"
                    height="20" border="0" onclick="Results()">
            </td>
            <td>
                <img src="/images/spacer.gif" width="74" height="19">
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="166" height="1">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/images/spacer.gif" width="53" height="6">
            </td>
        </tr>
    </table>
    <div id="SearchDiv">
        <form name="RSLFORM" id="RSLFORM" method="post">
        <table width="425" height="365" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
            border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
            <tr>
                <td nowrap width="105px">
                    Trade:
                </td>
                <td nowrap>
                    <%=lstTrades%>
                </td>
                <td nowrap>
                    <image src="/js/FVS.gif" name="img_TRADE" width="15px" height="15px" border="0">
                </td>
                <td width="100%">
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                </td>
            </tr>
            <tr>
                <td nowrap>
                    Company Name:
                </td>
                <td nowrap>
                    <input type="text" name="txt_NAME" id="txt_NAME" class="textbox200" maxlength="50">
                </td>
                <td nowrap>
                    <image src="/js/FVS.gif" name="img_NAME" width="15px" height="15px" border="0">
                </td>
            </tr>
            <tr>
                <td nowrap colspan="2" align="right">
                    <input type="hidden" name="DONTDOIT" value="1">
                    <input type="hidden" name="ADVANCED">
                    <input type="reset" value=" RESET " class="RSLButton">
                    <input type="button" onclick="SearchNow();Results()" value=" SEARCH " class="RSLButton">
                </td>
            </tr>
            <tr>
                <td height="100%">
                </td>
            </tr>
        </table>
        </form>
    </div>
    <div id="SearchResults" style='display: none'>
        <table width="425" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
            border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
            <tr>
                <td nowrap valign="top">
                    <iframe src="/secureframe.asp" name="SearchResults" height="360" width="100%" frameborder="0"></iframe>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
