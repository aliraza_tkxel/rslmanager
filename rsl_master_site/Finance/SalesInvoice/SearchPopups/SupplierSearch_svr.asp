<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	companyname = Replace(Request.Form("txt_NAME"), "'", "''")
	if (companyname = "") then 
		namesql = ""
	else
		namesql = " AND O.NAME LIKE '%" & companyname & "%' "
	end if

	trade = Replace(Request.Form("sel_TRADE"), "'", "''")
	if (trade = "") then 
		tradesql = ""
	else
		tradesql = " AND O.TRADE = " & trade & " "
	end if

	OpenDB()
	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " ISNULL(T.DESCRIPTION, '<I>Unknown</I>') AS TRADE, O.NAME, O.ORGID " &_
			"FROM S_ORGANISATION O " &_
			"LEFT JOIN S_COMPANYTRADE T ON O.TRADE = T.TRADEID " &_
			"WHERE 1 = 1 " & tradesql & namesql & " ORDER BY NAME, T.DESCRIPTION"

	Call OpenRs(rsSearch, SQL)
	

%>
<html>
<head>
    <title></title>
    <script language="javascript" type="text/javascript">
        function DisplayCompany(OrgID) {
            event.cancelBubble = true
            parent.location.href = "../Company.asp?OrgID=" + OrgID
        }

        function LoadCompany(CompanyID) {
            opener = parent.opener;
            //parent.Ref.LoadCompany(CompanyID);
            opener.LoadCompany(CompanyID);
            parent.close();
        }
    </script>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <table cellspacing="0" cellpadding="0" height="100%">
        <tr>
            <td valign="top" height="20">
                <table>
                    <tr>
                        <td width="230px" nowrap style='color: #133E71'>
                            <b>Company Name:</b>
                        </td>
                        <td width="140px" nowrap style='color: #133E71'>
                            <b>Trade:</b>
                        </td>
                        <td width="30">
                        </td>
                    </tr>
                    <tr style='line-height: 2'>
                        <td colspan="4" height="2" style='line-height: 2'>
                            <hr height="2" style='border: 1px dotted #133E71'>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="100%" valign="top">
                <div style='height: 100%; overflow: auto' class='TA'>
                    <table style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#133e71" width="393">
                        <%
	COUNT = 0 
	if (not rsSearch.EOF) then
		while NOT rsSearch.EOF
			COMPANYID = rsSearch("ORGID")
			Response.Write "<TR style='cursor:hand' onclick=""LoadCompany(" & COMPANYID & ")""><TD height=20 width=240px>" & rsSearch("NAME") & "</TD><TD width=160px>" & rsSearch("TRADE") & "</TD></TR>"
			COUNT = COUNT + 1	
			rsSearch.moveNext
		wend
	else
		Response.Write "<TR><TD colspan=4 align=center>No Records Found</td></TR>"		
	end if
	if (COUNT = MaxRecords) then
		Response.Write "<TR><TD colspan=4 align=center>Maximum number of records returned.<br>To Filter the list apply more filters.</TD></TR>"
	end if
	CloseRs(rsSearch)
	CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="408">
                    <tfoot>
                        <tr style='line-height: 2'>
                            <td colspan="4" height="2" style='line-height: 2'>
                                <hr height="2" style='border: 1px dotted #133E71'>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
