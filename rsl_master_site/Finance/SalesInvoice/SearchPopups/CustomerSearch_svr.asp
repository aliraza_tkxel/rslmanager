<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	'This filters the tenancy type (the checkboxes next to the key)
	isTennant = Request.Form("chk_show_Tenant")
	isFormerTennant = Request.Form("chk_show_PrevTenant")
	isProspective = Request.Form("chk_show_Prospective")
	isStakeholder = Request.Form("chk_show_Stakeholder")
	isGeneral = Request.Form("chk_show_General")
	isJoint = Request.Form("chk_show_Joint")

	Dim strCustomerType, strTenancyType, strJointTenancy, commaCount
	commaCount = 1
	
	' DETERMINE TENANCY TYPE STRING
	if (isTennant = "1" AND isFormerTennant = "1") then 
		strTenancyType = " " 
	ElseIf (isFormerTennant = "1") then 
		strTenancyType = " AND CT.ENDDATE IS NOT NULL " 
	ElseIf (isTennant = "1") then 
		strTenancyType = " AND CT.ENDDATE IS NULL "
	End If
	
	' DETERMINE CUSTOMER TYPE STRING
	strCustomerType = " AND C.CUSTOMERTYPE IN ( "
	If (isProspective = "1") Then 
		strCustomerType = strCustomerType & " 1 "
		commaCount = commaCount + 1
	End If
	if (isStakeholder = "1") Then
		If commaCount > 1 Then
			strCustomerType = strCustomerType & ", 4 "
			commaCount = commaCount + 1
		Else
			strCustomerType = strCustomerType & " 4 "
			commaCount = commaCount + 1
		End If
	End If
	if(isGeneral = "1") Then
		If commaCount > 1 Then
			strCustomerType = strCustomerType & ", 5 "
			commaCount = commaCount + 1
		Else
			strCustomerType = strCustomerType & " 5 "
			commaCount = commaCount + 1
		End If
	End If
	If commaCount > 1 Then
		strCustomerType = strCustomerType & " ) "
	Else
		strCustomerType = " "
	End If
	
	'rw NotInString
	' DETERMINE JOINT TENANCY STRING
	if (isJoint = "1") then 
		strJointTenancy = " AND (SELECT COUNT(CUSTOMERTENANCYID) FROM C_CUSTOMERTENANCY WHERE ENDDATE IS NULL AND " &_
				" TENANCYID = T.TENANCYID) > 1 " 
	End If
	' End Of customer type filter
	
	
	lastname = Replace(Request.Form("txt_LASTNAME"), "'", "''")
	if (lastname = "") then 
		namesql = ""
	else
		namesql = " AND C.LASTNAME LIKE '%" & lastname & "%' "
	end if

	customerid = Replace(Request.Form("txt_CUSTOMERID"), "'", "''")
	if (customerid = "") then 
		customersql = ""
	else
		customersql = " AND C.CUSTOMERID = " & customerid & " "
	end if
	
	isOrgisIndivd = Replace(Request.Form("rdo_CustomerType"), "'", "''")
	if  (isOrgisIndivd = "3") then 
	isOrgsql = " AND C.ORGFLAG = 1 "
	elseif (isOrgisIndivd = "2") then
		isOrgsql = " AND C.ORGFLAG not in (1) "
	Else
			isOrgsql = ""
	end if
	
	Initial = Replace(Request.Form("sel_Initial"), "'", "''")
	if (Initial = "") then 
		Initialsql = ""
	else
		Initialsql = " AND C.TITLE = " & Initial & "	 "
	end if

	address = Replace(Request.Form("txt_ADDRESS"), "'", "''")
	if (address = "") then 
		addresssql = ""
	else
		addresssql = " AND ((P.ADDRESS1 LIKE '%" & address & "%' OR P.ADDRESS2 LIKE '%" & address & "%' OR P.ADDRESS3 LIKE '%" & address & "%' OR P.HOUSENUMBER + ' ' + P.ADDRESS1 LIKE '%" + address + "%')  OR" &_
				" (A.ADDRESS1 LIKE '%" & address & "%' OR A.ADDRESS2 LIKE '%" & address & "%' OR A.ADDRESS3 LIKE '%" & address & "%' OR A.HOUSENUMBER + ' ' + A.ADDRESS1 LIKE '%" + address + "%')) "
	end if	

	towncity = Replace(Request.Form("txt_TOWNCITY"), "'", "''")
	if (towncity = "") then 
		towncitysql = ""
	else
		towncitysql = " AND (P.TOWNCITY LIKE '%" & towncity & "%') "
	end if	

	tenancyid = Replace(Request.Form("txt_TENANCYID"), "'", "''")
	if (tenancyid = "") then 
		tenancysql = ""
	else
		tenancysql = " AND T.TENANCYID = " & tenancyid & " "
	end if

	OpenDB()
	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " C.CUSTOMERID, T.TENANCYID, G.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, CT.ENDDATE," &_
			"	ISNULL(P.HOUSENUMBER + ' ' + P.ADDRESS1, A.HOUSENUMBER + ' ' + A.ADDRESS1) AS FULLADDRESS, " &_
			"   (	SELECT COUNT(CUSTOMERTENANCYID) " &_
			"	 	FROM C_CUSTOMERTENANCY " &_
			"    	WHERE ENDDATE IS NULL AND TENANCYID = T.TENANCYID) AS TENANCYCOUNT , " &_
			"	 C.CUSTOMERTYPE " &_
			" FROM C__CUSTOMER C " &_
			"	LEFT JOIN C_CUSTOMERTENANCY CT ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"	LEFT JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
			"	LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"	LEFT JOIN C_ADDRESS A ON C.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 " &_						
			"	LEFT JOIN G_TITLE G ON C.TITLE = G.TITLEID " &_									
			"WHERE 1 = 1 " & tenancysql & customersql & namesql & towncitysql &_
							addresssql & Initialsql & isOrgsql & strJointTenancy & strCustomerType & strTenancyType &_
			"ORDER BY LASTNAME, CT.ENDDATE, FULLADDRESS, TITLE"
	Call OpenRs(rsSearch, SQL)
	'rw sql

%>
<html>
<head>
    <title></title>
    <script language="javascript" type="text/javascript">

        function DisplayCustomer(CustID) {
            event.cancelBubble = true;
            window.open("/Customer/PopUps/Customer_Details.asp?CustomerID=" + CustID, "_blank", "width=400,height=335");
        }

        function LoadCustomerInfo(CustID, TenancyID) {
            opener = parent.opener;
            opener.LoadCustomer(CustID, TenancyID);
            parent.close();
        }
    </script>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onload="parent.Results()">
    <table cellspacing="0" cellpadding="0" height="100%">
        <tr>
            <td valign="top" height="20">
                <table>
                    <tr>
                        <td width="130px" nowrap style='color: #133E71'>
                            <b>Last Name:</b>
                        </td>
                        <td width="240px" nowrap style='color: #133E71'>
                            <b>Address:</b>
                        </td>
                        <td width="30">
                        </td>
                    </tr>
                    <tr style='line-height: 2'>
                        <td colspan="4" height="2" style='line-height: 2'>
                            <hr height="2" style='border: 1px dotted #133E71'>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="100%" valign="top">
                <div style='height: 100%; overflow: auto' class='TA'>
                    <table style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#133e71" width="393">
                        <%
	COUNT = 0 
	if (not rsSearch.EOF) then
		while NOT rsSearch.EOF
			CUSTID = rsSearch("CUSTOMERID")
			title = rsSearch("TITLE")
			CustomerName = rsSearch("LASTNAME")
			EndDate = rsSearch("ENDDATE")
			TENANCYCOUNT = rsSearch("TENANCYCOUNT")
			CustomerType= rsSearch("CUSTOMERTYPE")
			TENANCYID = rsSearch("TENANCYID")
			if (isNULL(TENANCYID)) then TENANCYID = -1
			
			if (NOT isNull(title)) then
				CustomerName = title & " " & CustomerName
			end if
			RW "<TR style='cursor:hand' onclick=""LoadCustomerInfo(" & CUSTID & ", " & TENANCYID & ")""><TD width=124px>" & CustomerName & "</TD><TD width=210px>" & rsSearch("FULLADDRESS") & "</TD><TD width=66 style='background-color:#FFFFFF' align='right'>" 		
			
			'Done by jimmy to show images of the type of customer
			Dim ImageArray
			ReDim ImageArray(6)
			ImageArray(1) = "pt" 'Prospective
			ImageArray(2) = "t"  'Tennant
			ImageArray(3) = "ft" 'Former Tenanat
			ImageArray(4) = "sh" 'Stakeholder
			ImageArray(5) = "g"  'general
			
			if (CustomerType <> "" AND CustomerType <> 2  And CustomerType <> 3) Then
				rw "<img src='/Customer/Images/"  & ImageArray(CustomerType) &  ".gif' border=0> "
			Else
				If isnull(rsSearch("ENDDATE")) Then
					rw "<img src='/Customer/Images/"  & ImageArray(2) &  ".gif' border=0> "
				Else
					rw "<img src='/Customer/Images/"  & ImageArray(3) &  ".gif' border=0> "
				End If		
			End If
			
			' Is this a joint tennancy
			IF TENANCYCOUNT > 1 then
				RW "<img src='/Customer/Images/JT.gif' border=0> "
			End If
					
			RW "<a onclick=""DisplayCustomer(" & CUSTID & ")""><img src='/myImages/info.gif' border=0></a></td></TR>"
			COUNT = COUNT + 1	
			rsSearch.moveNext
		wend
	else
		Response.Write "<TR><TD colspan=4 align=center>No Records Found</td></TR>"		
	end if
	if (COUNT = MaxRecords) then
		Response.Write "<TR><TD colspan=4 align=center>Maximum number of records returned.<br>To Filter the list apply more filters.</TD></TR>"
	end if
	CloseRs(rsSearch)
	CloseDB()
                        %>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td height="20">
                <table width="408">
                    <tfoot>
                        <tr style='line-height: 2'>
                            <td colspan="4" height="2" style='line-height: 2'>
                                <hr height="2" style='border: 1px dotted #133E71'>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                TOTAL RECORDS RETURNED :
                                <%=COUNT%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
