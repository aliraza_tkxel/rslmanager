<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	companyname = Replace(Request.Form("txt_NAME"), "'", "''")
	if (companyname = "") then 
		namesql = ""
	else
		namesql = " AND O.ORGANISATION LIKE '%" & companyname & "%' "
	end if

	CN = Replace(Request.Form("txt_CONTACTNAME"), "'", "''")
	if (CN = "") then 
		CNsql = ""
	else
		CNsql = " AND O.CONTACT LIKE '%" & CN & "%' "
	end if

	OpenDB()
	MaxRecords = 300
	SQL = "SELECT TOP " & MaxRecords & " ISNULL(O.ORGANISATION, '<I>N/A</I>') AS ORGANISATION, O.CONTACT, O.SCID " &_
			"FROM F_SALESCUSTOMER O " &_
			"WHERE 1 = 1 AND CUSACTIVE=1 " & CNsql & namesql & " ORDER BY O.ORGANISATION, O.CONTACT"

	Call OpenRs(rsSearch, SQL)
	

%>
<html>
<head></head>
<SCRIPT LANGUAGE=JAVASCRIPT>
function DisplayCompany(CompanyID){
    event.cancelBubble = true;
    //This code is commented as there is no such file exists in current implementation and the code is causing error at runtime
    //window.open("../Company.asp?CompanyID=" + CompanyID;, "_blank", "width=400,height=335")    
    //parent.location.href = "../Company.asp?CompanyID=" + CompanyID;
	}

	function LoadSalesCustomer(SCID) {
	    opener = parent.opener;
        //parent.Ref.LoadSalesCustomer(SCID);
	    opener.LoadSalesCustomer(SCID);
	    parent.close();
	}
</SCRIPT>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body leftmargin=0 topmargin=0 marginheight=0 marginwidth=0>
<table cellspacing="0" cellpadding="0" height="100%"><tr><td valign="top" height="20">
<table>
	<tr>
		<td width=180px nowrap style='color:#133E71'><b>Company Name:</b></td>
		<td width=190px nowrap style='color:#133E71'><b>Contact Name:</b></td>
		<td width=30></td>
	</tr>
	<tr style='line-height:2'>
		<td colspan=4 height=2 style='line-height:2'><hr height=2 style='border:1px dotted #133E71'></td>
	</tr>
</table>
</td></tr>
<tr><td height=100% valign=top>
<div style='height:100%;overflow:auto' class='TA'>
<table style='behavior:url(/Includes/Tables/tablehl.htc)' hlcolor=#133e71 WIDTH=393>
<%
	COUNT = 0 
	if (not rsSearch.EOF) then
		while NOT rsSearch.EOF
			SCID = rsSearch("SCID")
			Response.Write "<TR style='cursor:hand' onclick=""LoadSalesCustomer(" & SCID & ")""><TD height=20 width=180px>" & rsSearch("ORGANISATION") & "</TD><TD width=190px>" & rsSearch("CONTACT") & "</TD></TR>"
			COUNT = COUNT + 1	
			rsSearch.moveNext
		wend
	else
		Response.Write "<TR><TD colspan=4 align=center>No Records Found</td></TR>"		
	end if
	if (COUNT = MaxRecords) then
		Response.Write "<TR><TD colspan=4 align=center>Maximum number of records returned.<br>To Filter the list apply more filters.</TD></TR>"
	end if
	CloseRs(rsSearch)
	CloseDB()
%>
</table>
</div>
</td></tr>
<tr><td height=20>
<table WIDTH=408>
<tfoot>
	<tr style='line-height:2'>
		<td colspan=4 height=2 style='line-height:2'><hr height=2 style='border:1px dotted #133E71'></td>
	</tr>
	<TR><td colspan=4 align=center>TOTAL RECORDS RETURNED : <%=COUNT%></TD></TR>
</tfoot>	
</table>				
</td></tr></table>	
</body>
</html>