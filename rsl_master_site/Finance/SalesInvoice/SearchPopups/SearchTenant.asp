<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
OpenDB()
Call BuildSelect(CustomerTitle, "sel_Initial", "G_TITLE ", "TITLEID, DESCRIPTION", "DESCRIPTION", "All", NULL, NULL, "textbox200", "tabIndex='2'")
CloseDB()
%>
<html>
<head>
    <title>Find Customer</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
<script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
<script language="JavaScript" type="text/javascript">
    //var Ref = dialogArguments;

    var FormFields = new Array();
    FormFields[0] = "txt_LASTNAME|Last Name|TEXT|N"
    FormFields[1] = "txt_CUSTOMERID|Customer Number|INTEGER|N"
    FormFields[2] = "txt_TENANCYID|Tenancy Number|INTEGER|N"
    FormFields[3] = "txt_ADDRESS|Address|TEXT|N"
    FormFields[4] = "txt_TOWNCITY|Town / City|TEXT|N"


    function setValuesOfChecks(checkAction) {
        if ((checkAction == "Joint") && (RSLFORM.chk_show_Tenant.checked == false) && (RSLFORM.chk_show_Joint.checked == true)) {
            RSLFORM.chk_show_Tenant.checked = true
        }
    }

    function SearchNow() {
        if (!checkForm()) return false

        RSLFORM.action = "CustomerSearch_svr.asp"
        RSLFORM.target = "SearchResults"
        RSLFORM.submit()
        return false
    }

    function SearchDisplay(theType) {
        if (theType == "3") {
            RSLFORM.txt_TENANCYID.disabled = true
            RSLFORM.txt_CUSTOMERID.disabled = true
            RSLFORM.txt_TENANCYID.value = ""
            RSLFORM.txt_CUSTOMERID.value = ""
        }
        else {
            RSLFORM.txt_TENANCYID.disabled = false
            RSLFORM.txt_CUSTOMERID.disabled = false
        }
    }

    var ReadyBool = false
    function Ready() {
        ReadyBool = true
    }

    function Results() {
        if (!ReadyBool) return false
        document.getElementById("SearchDiv").style.display = "none"
        document.getElementById("SearchResults").style.display = "block"
        document.getElementById("tab_search").src = "../images/im_findatenant.gif"
        document.getElementById("tab_results").src = "../images/im_searchresults_o-05.gif"
    }

    function Search() {
        if (!ReadyBool) return false
        document.getElementById("SearchDiv").style.display = "block"
        document.getElementById("SearchResults").style.display = "none"
        document.getElementById("tab_search").src = "../images/im_findatenant_o.gif"
        document.getElementById("tab_results").src = "../images/im_searchresults_o.gif"
    }
    function chk_show_PrevTenant_onclick() {

    }

</script>
<body bgcolor="#FFFFFF" text="#000000" style='padding-left: 10px; padding-top: 10px'
    onload="Ready()">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td rowspan="2" style='cursor: hand'>
                    <img name="tab_search" id="tab_search" src="../images/im_findatenant_o.gif" width="104" height="20"
                        border="0" onclick="Search()" alt="im_findatenant_o.gif" />
                </td>
                <td rowspan="2" style='cursor: hand'>
                    <img name="tab_results" id="tab_results" src="../images/im_searchresults_o.gif" width="121" height="20"
                        border="0" onclick="Results()" alt="im_searchresults_o.gif" />
                </td>
                <td>
                    <img src="/IMAGES/spacer.gif" width="74" height="19" alt="spacer.gif" />
                </td>
            </tr>
            <tr>
                <td bgcolor="#133E71">
                    <img src="/images/spacer.gif" width="200" height="1" alt="spacer.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                    <img src="/images/spacer.gif" width="53" height="6" alt="spacer.gif" />
                </td>
            </tr>
        </tbody>
    </table>
    <div id="SearchDiv">
        <form name="RSLFORM" method="post" action="">
        <table width="425" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
            border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
            <tbody>
                <tr>
                    <td nowrap="nowrap" width="105px">
                        Last Name:
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" name="txt_LASTNAME" id="txt_LASTNAME" class="textbox200" maxlength="100" />
                    </td>
                    <td nowrap="nowrap">
                        <img src="/js/FVS.gif" name="img_LASTNAME" width="15px" height="15px" border="0"
                            alt="FVS.gif" />
                    </td>                    
                </tr>
                <tr>
                    <td nowrap="nowrap">
                        Title:
                    </td>
                    <td nowrap="nowrap">
                        <%=CustomerTitle%>
                    </td>
                    <td nowrap="nowrap">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" colspan="3" style='visibility: hidden'>
                        All
                        <input type="radio" name="rdo_CustomerType" value="1" checked="checked" onclick="SearchDisplay(1)" />
                        Individual
                        <input type="radio" name="rdo_CustomerType" value="2" onclick="SearchDisplay(2)" />
                        Organisation
                        <input type="radio" name="rdo_CustomerType" value="3" onclick="SearchDisplay(3)"
                            disabled="disabled" />
                    </td>                    
                </tr>
                <tr>
                    <td nowrap="nowrap" colspan="3" height="10">
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" colspan="3" bgcolor="#133E71" height="1">
                    </td>
                </tr>
                <tr bgcolor="#F3F7FE">
                    <td nowrap="nowrap" colspan="3" height="10">
                    </td>
                </tr>
                <tr bgcolor="#F3F7FE">
                    <td nowrap="nowrap">
                        Address:
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" name="txt_ADDRESS" id="txt_ADDRESS" class="textbox200" maxlength="50" />
                    </td>
                    <td nowrap="nowrap">
                        <img src="/js/FVS.gif" name="img_ADDRESS" width="15px" height="15px" border="0" alt="FVS.gif" />
                    </td>                    
                </tr>
                <tr bgcolor="#F3F7FE">
                    <td nowrap="nowrap">
                        Town / City:
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" name="txt_TOWNCITY" id="txt_TOWNCITY" class="textbox200" maxlength="50" />
                    </td>
                    <td nowrap="nowrap">
                        <img src="/js/FVS.gif" name="img_TOWNCITY" width="15px" height="15px" border="0"
                            alt="FVS.gif" />
                    </td>                    
                </tr>
                <tr bgcolor="#F3F7FE" id="TypeCust1">
                    <td nowrap="nowrap">
                        Customer No:
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" name="txt_CUSTOMERID" id="txt_CUSTOMERID" class="textbox200" maxlength="50" />
                    </td>
                    <td nowrap="nowrap">
                        <img src="/js/FVS.gif" name="img_CUSTOMERID" width="15px" height="15px" border="0"
                            alt="FVS.gif" />
                    </td>                    
                </tr>
                <tr bgcolor="#F3F7FE" id="TypeCust">
                    <td nowrap="nowrap" width="105px" bgcolor="#F3F7FE">
                        Tenancy No:
                    </td>
                    <td nowrap="nowrap">
                        <input type="text" name="txt_TENANCYID" id="txt_TENANCYID" class="textbox200" maxlength="50" />
                    </td>
                    <td nowrap="nowrap">
                        <img src="/js/FVS.gif" name="img_TENANCYID" id="img_TENANCYID" width="15px" height="15px" border="0"
                            alt="FVS.gif" />
                    </td>                    
                </tr>
                <tr bgcolor="#F3F7FE" id="TypeCust2">
                    <td>
                        <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                    </td>
                    <td nowrap="nowrap" colspan="2">
                        <table width="200" border="0" cellpadding="2" cellspacing="0" style="border-top: 1px solid #133E71;
                            border-right: 1px solid #133E71; border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
                            <tr align="center">
                                <td nowrap="nowrap" colspan="3">
                                    <b><i>Key: </i></b>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/T.gif" width="16" height="16" alt="T.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    Tenant
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_Tenant" id="chk_show_Tenant" value="1" checked="checked" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/FT.gif" width="16" height="16" alt="FT.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    PreviousTenant
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_PrevTenant" id="chk_show_PrevTenant" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/JT.gif" width="16" height="16" alt="JT.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    Joint Tenancy
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_Joint" id="chk_show_Joint" value="1" onclick="setValuesOfChecks('Joint')" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/PT.gif" width="16" height="16" alt="PT.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    Prospective Tenant
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_Prospective" id="chk_show_Prospective" value="1" disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/SH.gif" width="16" height="16" alt="SH.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    Stakeholder
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_Stakeholder" id="chk_show_Stakeholder" value="1" disabled="disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" width="29" align="center">
                                    <img src="/Customer/Images/G.gif" width="16" height="16" alt="G.gif" />
                                </td>
                                <td nowrap="nowrap" width="110">
                                    General
                                </td>
                                <td nowrap="nowrap" width="49" align="right">
                                    <input type="checkbox" name="chk_show_General" id="chk_show_General" value="1" disabled="disabled" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" colspan="2" align="right" bgcolor="#F3F7FE">
                        <input type="hidden" name="DONTDOIT" value="1" />
                        <input type="hidden" name="ADVANCED" />
                        <input type="reset" value=" RESET " class="RSLButton" />
                        <input type="button" onclick="SearchNow();Results()" value=" SEARCH " class="RSLButton" />
                    </td>
                    <td bgcolor="#F3F7FE">
                    </td>                    
                </tr>
            </tbody>
        </table>
        </form>
    </div>
    <div id="SearchResults" style='display: none'>
        <table width="425" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
            border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
            <tr>
                <td nowrap="nowrap" valign="top">
                    <iframe src="/secureframe.asp" name="SearchResults" height="360" width="100%" frameborder="0">
                    </iframe>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
