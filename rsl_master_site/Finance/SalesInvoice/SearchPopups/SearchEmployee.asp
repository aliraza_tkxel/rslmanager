<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
SQL = "SELECT E.EMPLOYEEID, LTRIM(RTRIM(ISNULL(J.JOBTITLE, '<i>Unknown</i>'))) AS JOBTITLE, FIRSTNAME, LASTNAME, G.DESCRIPTION AS TITLE FROM E__EMPLOYEE E " &_
		" LEFT JOIN G_TITLE G ON G.TITLEID = E.TITLE " &_
		" LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
		" WHERE J.ACTIVE = 1 " &_
		" ORDER BY FIRSTNAME, LASTNAME "
DataString = ""
Call OpenDB()
Call OpenRs(rsEmp, SQL)
if (NOT rsEmp.EOF) then
	while NOT rsEmp.EOF
		//THIS PARTS CHECKS WHETHER THE TITLE IS EMPTY, OTHERWISE IT ADDS A SPACE ON THE END
		'iTitle = rsEmp("TITLE")
		'if (isNull(iTitle) OR iTitle = "") then
		'	iTitle = ""
		'else
		'	iTitle = iTitle & " "
		'end if
		JobTitle =  rsEmp("JOBTITLE")
		if (JobTitle = "") then
			JobTitle = "<i>Unknown</i>"
		end if
		JobTitleDesc = ""
		if (LEN(JobTitle) > 20) then
			JobTitleDesc = " title=""" & JobTitle & """ "
			JobTitle = Left(JobTitle,17) & "..."
		end if
		DataString = DataString & "<TR style='cursor:hand' onclick=""LoadEmployee(" & rsEmp("EMPLOYEEID") & ")""><TD height=20px width=210px>" & rsEmp("FIRSTNAME") & " " & rsEmp("LASTNAME") & "</TD><TD width=130px " & JobTitleDesc & ">" & JobTitle & "</TD></TR>"
		rsEmp.moveNext
	wend
else
	DataString = "<TR><TD colspan=2 align=center>No Employees Found</TD></TR>"
end if
Call CloseRs(rsEmp)
Call CloseDB()
%>
<html>
<head>
    <title>Employee List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
    <script language="javascript" type="text/javascript">
        //var Ref = dialogArguments;

        function LoadEmployee(EmployeeID) {
            //Ref.LoadEmployee(EmployeeID)
            opener.LoadEmployee(EmployeeID)
            window.close()
        }
    </script>
</head>
<body bgcolor="#FFFFFF" text="#000000" style='padding-left: 10px; padding-top: 10px'>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" style='cursor: hand'>
                <img name="tab_results" src="../images/im_employees.gif" width="94" height="20" border="0"
                    onclick="Results()" />
            </td>
            <td>
                <img src="/images/spacer.gif" width="74" height="19">
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="331" height="1">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-left: 1px solid #133e71; border-right: 1px solid #133e71">
                <img src="/images/spacer.gif" width="53" height="6">
            </td>
        </tr>
    </table>
    <table width="425" border="0" cellpadding="2" cellspacing="0" style="border-right: 1px solid #133E71;
        border-left: 1px solid #133E71; border-bottom: 1px solid #133E71">
        <tr>
            <td nowrap valign="top" height="360px">
                <div>
                    <table cellspacing="0" cellpadding="0" height="100%">
                        <tr>
                            <td valign="top" height="20">
                                <table>
                                    <tr>
                                        <td width="230px" nowrap style='color: #133E71'>
                                            <b>Name:</b>
                                        </td>
                                        <td width="150px" nowrap style='color: #133E71'>
                                            <b>Job Title:</b>
                                        </td>
                                        <td width="10">
                                        </td>
                                    </tr>
                                    <tr style='line-height: 2'>
                                        <td colspan="4" height="2" style='line-height: 2'>
                                            <hr height="2" style='border: 1px dotted #133E71'>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="100%" valign="top">
                                <div style='height: 100%; overflow: auto' class='TA'>
                                    <table style='behavior: url(/Includes/Tables/tablehl.htc)' hlcolor="#133e71" width="393">
                                        <%=DataString%>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="20">
                                <table width="408">
                                    <tfoot>
                                        <tr style='line-height: 2'>
                                            <td colspan="4" height="2" style='line-height: 2'>
                                                <hr height="2" style='border: 1px dotted #133E71'>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                TOTAL RECORDS RETURNED :
                                                <%=COUNT%>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
