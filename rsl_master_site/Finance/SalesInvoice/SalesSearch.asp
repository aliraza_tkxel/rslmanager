<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
    Call OpenDB()
    Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
   	Call CloseDB()
    %>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Sales Account Search</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

    var FormFields = new Array();
    FormFields[0] = "txt_TENANCYID|Tenancy No.|INTEGER|N";
    FormFields[1] = "txt_EMPLOYEE|Employee|TEXT|N";
    FormFields[2] = "txt_ORGANISATION|Organisation|TEXT|N";
    FormFields[3] = "txt_SALESCUSTOMER|Sales Customer|TEXT|N";

    function SearchNow() {
        //if (!checkForm()) return false;
        RSLFORM.action = "ServerSide/SalesSearch_svr.asp";
        RSLFORM.target = "SearchResults";
        RSLFORM.submit();
        //RSLFORM.submit();
        return false;
    } 

</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(4);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form name="RSLFORM" method="post">
<table cellspacing=0 cellpadding=0 height=100%><tr><td valign=top>

  <table border=0 cellpadding=0 cellspacing=0>
    <tr> 
            <td rowspan=2 style='cursor:hand'> <img name="tab_standardsearch" src="images/tab_findasalesaccount.gif" width=154 height=20 border=0 onClick="StandardSearch()"></td>
      <td> <img src="/myImages/spacer.gif" width=89 height=19></td>
    </tr>
    <tr> 
      <td bgcolor=#133E71> <img src="images/spacer.gif" width=176 height=1></td>
    </tr>
    <tr> 
      <td colspan=3 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="/myImages/spacer.gif" width=53 height=6></td>
    </tr>
  </table>
  <table width="233" border="0" cellpadding="2" cellspacing="0" style=" border-right: 1px solid #133E71; border-LEFT: 1px solid #133E71;  border-BOTTOM: 1px solid #133E71 ">
  <tr> 
            <td nowrap width=105px>Tenant Name:</td>
            <td nowrap> 
			<input type="text" name="txt_TENANTNAME" value="" class="textbox200">
	        </td>
      <td  nowrap> <image src="/js/FVS.gif" name="img_TENANTNAME" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
            <td nowrap width=105px>Tenancy ID:</td>
            <td nowrap> 
			<input type="text" id="txt_TENANCYID" name="txt_TENANCYID" value="" class="textbox200">
	        </td>
      <td  nowrap> <image src="/js/FVS.gif" id="txt_TENANCYID" name="img_TENANCYID" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
            <td nowrap width=105px>Employee:</td>
            <td nowrap> 
			<input type="text" id="txt_EMPLOYEE" name="txt_EMPLOYEE" value="" class="textbox200">
	        </td>
      <td  nowrap> <image src="/js/FVS.gif" id="txt_EMPLOYEE" name="img_EMPLOYEE" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
            <td nowrap width=105px>Organisation:</td>
            <td nowrap> 
			<input type="text" id="txt_ORGANISATION" name="txt_ORGANISATION" value="" class="textbox200">
	        </td>
      <td  nowrap> <image src="/js/FVS.gif" id="txt_ORGANISATION" name="img_ORGANISATION" width="15px" height="15px" border="0"> 
      </td>
    </tr>
    <tr> 
            <td nowrap width=105px>Sales Customer:</td>
            <td nowrap> 
			<input type="text" id="txt_SALESCUSTOMER" name="txt_SALESCUSTOMER" value="" class="textbox200">
	        </td>
      <td  nowrap> <image src="/js/FVS.gif" id="txt_SALESCUSTOMER" name="img_SALESCUSTOMER" width="15px" height="15px" border="0"> 
      </td>
    </tr>	
    <tr> 
            <td nowrap width=105px>Company:</td>
            <td nowrap> 
			<%=lstCompany%>
	        </td>
      <td  nowrap>
      </td>
    </tr>	

      
    <tr> 
      <td nowrap colspan=2 ALIGN=RIGHT> 
        <input type="hidden" name="DONTDOIT" value="1">
	  
        <input type="reset" value=" RESET " class="RSLButton">
        <input type="button" onclick="SearchNow()" value=" SEARCH " class="RSLButton">
      </td>
    </tr>
  </table>

</td><td>&nbsp;&nbsp;
</td><td valign=top height=100%>

  <table border=0 cellpadding=0 cellspacing=0>
    <tr> 
            <td rowspan=2> <img name="tab_main_details" src="Images/tab_search_results.gif" width=122 height=20 border=0></td>
      <td> <img src="/myImages/spacer.gif" width=298 height=19></td>
    </tr>
    <tr> 
      <td bgcolor=#133E71> <img src="images/spacer.gif" width=298 height=1></td>
    </tr>
    <tr> 
      <td colspan=3 style="border-left:1px solid #133e71; border-right:1px solid #133e71"><img src="/myImages/spacer.gif" width=53 height=6></td>
    </tr>
  </table>
  <table height=90% width="420" border="0" cellpadding="2" cellspacing="0" style=" border-right: 1px solid #133E71; border-LEFT: 1px solid #133E71;  border-BOTTOM: 1px solid #133E71 ">
    <tr> 
      <td nowrap height="100%" valign=top>
	  <iframe src="/secureframe.asp" name="SearchResults" height="400" width="100%" frameborder=0></iframe>
	  </td>
    </tr>
   </table>
   
</td></tr></table>
  
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name="Property_Server" width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
