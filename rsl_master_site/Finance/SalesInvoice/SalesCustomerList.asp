<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)
	Dim clean_desc
	
	ColData(0)  = "SC Ref|SCID|90"
	SortASC(0) 	= "SCID ASC"
	SortDESC(0) = "SCID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "SalesCustomerReference(|)"

	ColData(1)  = "Contact|CONTACT|150"
	SortASC(1) 	= "CONTACT ASC"
	SortDESC(1) = "CONTACT DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Company|ORGANISATION|150"
	SortASC(2) 	= "ORGANISATION ASC"
	SortDESC(2) = "ORGANISATION DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Address 1|ADDRESS1|90"
	SortASC(3) 	= "ADDRESS1 ASC"
	SortDESC(3) = "ADDRESS1 DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Town/City|TOWNCITY|175"
	SortASC(4) 	= "TOWNCITY ASC"
	SortDESC(4) = "TOWNCITY DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""		

	ColData(5)  = "PC|POSTCODE|60"
	SortASC(5) 	= "POSTCODE ASC"
	SortDESC(5) = "POSTCODE DESC"	
	TDSTUFF(5)  = ""
	TDFunc(5) = ""		

	PageName = "SalesCustomerList.asp"
	EmptyText = "No sales customers found or filter returned zero results!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""SCID"") & "")"""" """ 

    CompanyFilter = ""
    rqCompany = Request("COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " and COMPANYID = '" & rqCompany & "' "
	end if

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	ScFilter = ""
	if (Request("SC") <> "") then
		ScFilter = " and (CONTACT LIKE '%" & Replace(Request("SC"), "'", "''") & "%' OR ORGANISATION LIKE '%" & Replace(Request("SC"), "'", "''") & "%' )"
	end if
	
	SQLCODE ="SELECT SCID, CONTACT, ORGANISATION, ADDRESS1, TOWNCITY, POSTCODE FROM F_SALESCUSTOMER where 1=1 " & CompanyFilter & ScFilter  & " Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
    Call Create_Table()
    Call OpenDB()
Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
   	Call CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Sales Customers</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(SCID){
	location.href = "SalesCustomer.asp?ID=" + SCID
	}

function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}

function SubmitPage(){
    location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&SC=" + thisForm.SC.value + "&COMPANY=" + document.getElementById("sel_COMPANY").value;
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack"><tr><td><%=lstCompany%></td><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=SC class="RSLBlack" value="<%=Server.HTMLEncode(Request("SC"))%>" onblur="RemoveBad()" style='width:135px;background-image:url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>