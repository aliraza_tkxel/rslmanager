<%@  language="VBscript" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	AccountType = Request("ACCTYPE")
	AccountID = Request("ACCID")

	Call OpenDB()
	
	IF AccountType = 1 THEN

		SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DEscriptION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
				"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
				"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
				"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & AccountID
	
		Call OpenRS(rsCust, SQL)
		if (rsCust.EOF) then 'MOST PROBABLY A PREVIOUS TENANT, THEREFORE WE NEED TO AMEND THE SQL STATEMENT TO
			Call CloseRs(rsCust)	
			SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DEscriptION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID AND CT.ENDDATE = T.ENDDATE " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					"WHERE T.TENANCYID = " & AccountID
			Call OpenRS(rsCust, SQL)
		End if
		
		count = 1
		while NOT rsCust.EOF
			
			StringName = ""
			Title = rsCust("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsCust("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName =rsCust("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			if (count = 1) then
				StringFullName = StringName
				count = 2
			else
				StringFullName = StringFullName & "& " & StringName
			end if
			rsCust.moveNext
		wend
		if (count = 2) then
			rsCust.moveFirst()
			housenumber = rsCust("housenumber")
			if (housenumber = "" or isNull(housenumber)) then
				housenumber = rsCust("flatnumber")
			end if
			if (housenumber = "" or isNull(housenumber)) then
				FirstLineOfAddress = rsCust("Address1")
			else
				FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
			end if
	
			RemainingAddressString = ""
			AddressName = Array("Address 2", "Address 3", "Town", "Postcode", "County")
			AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsCust(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "<tr><td bgcolor=beige><b>" & AddressName(i) & "</b></td><td>" & temp & "</td></tr>"
				end if
			next
			tenancyref = rsCust("TENANCYID")
		end if
		
		FirsttableLine = "<tr><td bgcolor=beige width=120 style='border-top:none'><b>TENANCY</b></td><td style='border-top:none'> " & TenancyReference(tenancyref) & "</b></td>"
		OthertableLines = "<tr><td bgcolor=beige><b>Name</b></td><td>" & StringFullName & "</td></tr><tr><td bgcolor=beige><b>Address 1</b></td><td>" & "" & FirstLineOfAddress & "</td></tr>" & RemainingAddressString			

	ELSEIF AccountType = 4 THEN
	
		SQL = "SELECT O.SCID, O.CONTACT, O.CREDITLIMIT, O.ORGANISATION, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM F_SALESCUSTOMER O " &_
				"WHERE SCID = " & AccountID
	
		Call OpenRS(rsSC, SQL)

		IF (not rsSC.EOF) then
			StringFullName = rsSC("CONTACT")
		
			RemainingAddressString = ""
			AddressName = Array("Company", "Address 1", "Address 2", "Address 3", "Town", "Postcode", "County", "Credit Limit")
			AddressArray = Array("ORGANISATION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY", "CREDITLIMIT")
			for i=0 to Ubound(AddressArray)
				temp = rsSC(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "<tr><td bgcolor=beige><b>" & AddressName(i) & "</b></td><td>" &  temp & "</td></tr>"
				end if
			next
			FirsttableLine = "<tr><td bgcolor=beige width=120 style='border-top:none'><b>Sales Customer</b> </td><td style='border-top:none'> " & StringFullName & "</td></tr>"
			OthertableLines = RemainingAddressString			
		else
			FirsttableLine = "<tr><td bgcolor=beige width=120><b>Sales Customer</b> </td><td>Details Could not be found! Contact Support Team immediately</td></tr>"
		end if	

	ELSEIF AccountType = 3 THEN
	
		SQL = "SELECT O.ORGID, O.NAME, O.BUILDINGNAME, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM S_ORGANISATION O " &_
				"WHERE ORGID = " & AccountID
	
		Call OpenRS(rsCompany, SQL)

		IF (not rsCompany.EOF) then
			StringFullName = rsCompany("NAME")
		
			RemainingAddressString = ""
			AddressName = Array("Building Name", "Address 1", "Address 2", "Address 3", "Town", "Postcode", "County")
			AddressArray = Array("BUILDINGNAME", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsCompany(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "<tr><td bgcolor=beige><b>" & AddressName(i) & "</b></td><td>" &  temp & "</td></tr>"
				end if
			next
			FirsttableLine = "<tr><td bgcolor=beige width=120 style='border-top:none'><b>Organisation</b> </td><td style='border-top:none'> " & StringFullName & "</td></tr>"
			OthertableLines = RemainingAddressString			
		else
			FirsttableLine = "<tr><td bgcolor=beige width=120><b>Organisation</b> </td><td>Details Could not be found! Contact Support Team immediately</td></tr>"
		end if	
	
	ELSEIF AccountType = 2 THEN

		SQL = "SELECT E.EMPLOYEEID, GT.DEscriptION AS TITLE, E.FIRSTNAME, E.LASTNAME, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.POSTALTOWN, P.POSTCODE, P.COUNTY FROM E__EMPLOYEE E " &_
				"INNER JOIN E_CONTACT P ON P.EMPLOYEEID = E.EMPLOYEEID " &_
				"LEFT JOIN G_TITLE GT ON E.TITLE = GT.TITLEID " &_
				"WHERE E.EMPLOYEEID = " & AccountID
	
		Call OpenRS(rsEmployee, SQL)
	
		count = 1
		IF NOT rsEmployee.EOF THEN
			
			StringName = ""
			Title = rsEmployee("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsEmployee("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName = rsEmployee("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			RemainingAddressString = ""
			AddressName = Array("Address 1", "Address 2", "Address 3", "Town", "Postcode", "County")
			AddressArray = Array("ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsEmployee(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "<tr><td bgcolor=beige><b>" & AddressName(i) & "</b></td><td>" & temp & "</td></tr>"
				end if
			next
			FirsttableLine = "<tr><td bgcolor=beige width=120 style='border-top:none'><b>Employee</b></td><td style='border-top:none'>" & StringName & "</td></tr>"
			OthertableLines = RemainingAddressString					
		else
			FirsttableLine = "<tr><td bgcolor=beige width=120><b>Employee</b></td><td>Details Could not be found! Contact Support Team immediately.</td></tr>"		
			FullAddress = "<b>EMPLOYEE</b><br>Details Could not be found!<br>Contact Support Team immediately."
		end if			
	
	ELSE
		FirsttableLine = "<tr><td bgcolor=beige width=120><b>NAME</b></td><td>Details Could not be found! Contact Support Team immediately.</td></tr>"		
	END IF

	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --&gt; Sales Account</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script language="Javascript" type="text/javascript" src="/js/preloader.js"></script>
<script language="Javascript" type="text/javascript" src="/js/general.js"></script>
<script language="Javascript" type="text/javascript" src="/js/menu.js"></script>
<script language="Javascript" type="text/javascript" src="/js/FormValidation.js"></script>
<script language="Javascript" type="text/javascript" src="/js/Loading.js"></script>
<script language="Javascript" type="text/javascript">
    function post_cash() {
        var strcontent, popupWidth, popupHeight, popupLeft, popupTop;

        strcontent = navigator.userAgent;
        strcontent = strcontent.toLowerCase();

        popupWidth = 440;
        popupHeight = 340;
        popupLeft = (screen.width / 2) - (popupWidth / 2);
        popupTop = (screen.height / 2) - (popupHeight / 2);

        if (strcontent.search("chrome") > 0) {
            window.open("popups/cashposting.asp?AccountType=<%=AccountType%>&AccountID=<%=AccountID%>", "SalesPosting", "height=" + popupHeight + "px; width=" + popupWidth + "px; left=" + popupLeft + "px; top=" + popupTop + "px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
            return;
        }
        else {
            window.showModalDialog("popups/cashposting.asp?AccountType=<%=AccountType%>&AccountID=<%=AccountID%>", "SalesPosting", "dialogheight: " + popupHeight + "px; dialogwidth: " + popupWidth + "px; dialogleft: " + popupLeft + "px; dialogtop: " + popupTop + "px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
            return;
        }
    }

    function show_statement() {
        var strcontent, popupWidth, popupHeight, popupLeft, popupTop;

        strcontent = navigator.userAgent;
        strcontent = strcontent.toLowerCase();

        popupWidth = 840;
        popupHeight = 600;
        popupLeft = (screen.width / 2) - (popupWidth / 2);
        popupTop = (screen.height / 2) - (popupHeight / 2);

        if (strcontent.search("chrome") > 0) {
                       window.open("../../AgedDebtor/Bridge.aspx?accountId=<%=AccountID%>&accountType=<%=AccountType%>", "SalesPosting", "height=" + popupHeight + "px, width=" + popupWidth + "px, left=" + popupLeft + "px, top=" + popupTop + "px,edge=Raised, center=Yes, help= No, resizable= Yes, status= No, scroll= no");
            return;
        }
        else {
            window.showModalDialog("../../AgedDebtor/Bridge.aspx?accountId=<%=AccountID%>&accountType=<%=AccountType%>", "SalesPosting", "dialogheight: " + popupHeight + "px; dialogwidth: " + popupWidth + "px; dialogleft: " + popupLeft + "px; dialogtop: " + popupTop + "px;  edge: Raised; center: Yes; help: No; resizable: No; status: No; scroll: no");
            return;
        }
    }

    function refresh_account() {
        SALES_BOTTOM_FRAME.location.href = "iFrames/iSalesAccount.asp?ACCTYPE=<%=AccountType%>&ACCID=<%=AccountID%>"
    }
</script>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(4);preloadImages()" onunload="macGo()"
    style="margin-left: 10px; margin-top: 10px;">
    <!--#include virtual="Includes/Tops/bodyTop.asp" -->
    <form name="RSLFORM" method="post" action="SalesAccount.asp">
    <table style="width: 750px" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2">
                <img alt="" name="img1" src="images/tab_accountdetails.gif" width="123px" height="20px"
                    border="0" />
            </td>
            <td height="19">
                <img alt="" src="images/spacer.gif" width="29" height="19" />
            </td>
        </tr>
        <tr>
            <td bgcolor="#133e71">
                <img alt="" src="images/spacer.gif" width="627" height="1" />
            </td>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse; border-left: '1px solid #133E71';
        border-right: '1px solid #133E71'; border-bottom: '1px solid #133E71'; width: 750px;
        height: 150px;" cellspacing="0" cellpadding="3">
        <%=FirsttableLine%>
        <%=OthertableLines%>
        <tr>
            <td bgcolor="beige" height="100%">
            </td>
            <td height="100%">
            </td>
        </tr>
    </table>
    <img src="/images/spacer/gif" height="2" width="100">
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td rowspan="2" valign="bottom">
                <img alt="" name="img8" src="images/tab_salesaccount.gif" width="115" height="20"
                    border="0" align="bottom" />
            </td>
            <td align="right">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="20%">
                        </td>
                        <td>
                            <img alt="" name="cpost" src="images/cpost.gif" width="27" height="22" title='Post cash/chq to this tenancy'
                                style='cursor: hand' onclick='post_cash()' <%=disa_bled%> />
                        </td>
                        <td width="9%">
                            <img alt="" name="crefresh" src="images/refresh.gif" width="27" height="20" title='Refresh Account'
                                style='cursor: hand' onclick='refresh_account()' <%=disa_bled%> />
                        </td>
                        <td width="9%">
                            <input type="button" value="Statement" style="width: 80px; height: 20px; cursor: hand;
                                background-color: Gray; border: none; text-decoration: 'none'; color: White;"
                                onclick='show_statement()' />
                        </td>
                        <td align="right" width="96%">
                            <b><font color='#133e71' style='font-size: 14px'>Balance:&nbsp;</font></b>
                            <input type="text" value='<%=ACCOUNT_BALANCE%>' size="12" readonly name='SalesBalance'
                                style='border: 1PX SOLID black; font-size: 14px; color: BLUE; font-weight: bold;
                                text-align: right' />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#004376" colspan="2">
                <img alt="" src="images/spacer.gif" width="635" height="1" />
            </td>
        </tr>
    </table>
    <div id="BOTTOM_DIV" style="display: BLOCK; overflow: hidden">
        <table style="width: 750px; height: 260px; border-right: 'SOLID 1PX #133E71';" cellpadding="1"
            cellspacing="0" border="0" class="TAB_table">
            <tr>
                <td>
                    <iframe name="SALES_BOTTOM_FRAME" src="iFrames/iSalesAccount.asp?ACCTYPE=<%=AccountType%>&ACCID=<%=AccountID%>"
                        width="100%" height="100%" frameborder="0" style="border: 0"></iframe>
                </td>
            </tr>
        </table>
    </div>
    <div id="BOTTOM_DIV_LOADER" style="display: none; overflow: hidden; width: 750px;
        height: 260px">
        <table style="border-right: solid 1px #133E71; width: 750px; height: 210px;" cellpadding="10"
            cellspacing="10" border="0" class="TAB_table">
            <tr>
                <td style='color: SILVER; font-size: 20PX' align="left" valign="middle">
                    <b>
                        <div id="LOADINGTEXT_BOTTOM">
                        </div>
                    </b>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <!--#include virtual="Includes/Bottoms/bodyBottom.asp" -->
</body>
</html>
