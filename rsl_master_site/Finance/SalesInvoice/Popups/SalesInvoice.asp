<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim L_SALEID
L_SALEID = ""
if(Request.QueryString("SaleID") <> "") then L_SALEID = Request.QueryString("SaleID")

OpenDB()


SQL = "SELECT SO.SALEID, SO.SONAME, SO.SUPPLIERID, SO.SALESCUSTOMERID, SS.SOSTATUSNAME, SODATE, SO.SONOTES, SO.TENANCYID, SO.EMPLOYEEID " &_
		"FROM F_SALESINVOICE SO " &_
		"INNER JOIN F_SOSTATUS SS ON SS.SOSTATUSID = SO.SOSTATUS " &_
		"WHERE ACTIVE = 1 AND SO.SALEID = " & L_SALEID

Call OpenRs(rsPP, SQL)
if (NOT rsPP.EOF) then
	SALEID = rsPP("SALEID")
	SONUMBER = SaleNumber(rsPP("SALEID"))
	SONAME = rsPP("SONAME")
	SODATE = FormatDateTime(rsPP("SODATE"),1)

	SOSTATUSNAME = rsPP("SOSTATUSNAME")
	SONOTES = rsPP("SONOTES")

'	SUPPLIER = rsPP("NAME")
	SUPPLIERID = rsPP("SUPPLIERID")	
	TENANCYID = rsPP("TENANCYID")
	EMPLOYEEID = rsPP("EMPLOYEEID")
	SALESCUSTOMERID = rsPP("SALESCUSTOMERID")
	
	IF NOT isNull(TENANCYID) THEN

		SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
				"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
				"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
				"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & TENANCYID
	
		Call OpenRS(rsCust, SQL)
		if (rsCust.EOF) then 'MOST PROBABLY A PREVIOUS TENANT, THEREFORE WE NEED TO AMEND THE SQL STATEMENT TO
			Call CloseRs(rsCust)	
			SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID AND CT.ENDDATE = T.ENDDATE " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					"WHERE T.TENANCYID = " & TENANCYID
			Call OpenRS(rsCust, SQL)
		End if	

		count = 1
		while NOT rsCust.EOF
			
			StringName = ""
			Title = rsCust("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsCust("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName =rsCust("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			if (count = 1) then
				StringFullName = StringName
				count = 2
			else
				StringFullName = StringFullName & "& " & StringName
			end if
			rsCust.moveNext
		wend
		if (count = 2) then
			rsCust.moveFirst()
			housenumber = rsCust("housenumber")
			if (housenumber = "" or isNull(housenumber)) then
				housenumber = rsCust("flatnumber")
			end if
			if (housenumber = "" or isNull(housenumber)) then
				FirstLineOfAddress = rsCust("Address1")
			else
				FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
			end if
	
			RemainingAddressString = ""
			AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "COUNTY","POSTCODE" )
			for i=0 to Ubound(AddressArray)
				temp = rsCust(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			tenancyref = rsCust("TENANCYID")
		end if
		
		FullAddress = "" & StringFullName & "<BR>" & "" 		
        Address = 	FirstLineOfAddress & "<BR>" &	RemainingAddressString
        TenancyRefrenceId = "<b>TENANT : " & TenancyReference(TENANCYID) & "</b>"
	ELSEIF NOT isNull(SALESCUSTOMERID) THEN
	
		SQL = "SELECT O.SCID, O.CONTACT, O.ORGANISATION, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM F_SALESCUSTOMER O " &_
				"WHERE SCID = " & SALESCUSTOMERID
	
		Call OpenRS(rsCompany, SQL)

		IF (not rsCompany.EOF) then
			StringFullName = rsCompany("CONTACT")
		
			RemainingAddressString = ""
			AddressArray = Array("ORGANISATION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY","COUNTY", "POSTCODE" )
			for i=0 to Ubound(AddressArray)
				temp = rsCompany(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>SALES CUSTOMER</b> <br>" & StringFullName 
            Address = 	RemainingAddressString	
            TenancyRefrenceId = ""
		else
			FullAddress = "<b>SALES CUSTOMER</b> <br> Details Could not be found!<br>Contact Support Team immediately."
            TenancyRefrenceId = ""
		end if	

	ELSEIF NOT isNull(SUPPLIERID) THEN
	
		SQL = "SELECT O.ORGID, O.NAME, O.BUILDINGNAME, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM S_ORGANISATION O " &_
				"WHERE ORGID = " & SUPPLIERID
	
		Call OpenRS(rsCompany, SQL)

		IF (not rsCompany.EOF) then
			StringFullName = rsCompany("NAME")
		
			RemainingAddressString = ""
			AddressArray = Array("BUILDINGNAME", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY","COUNTY", "POSTCODE" )
			for i=0 to Ubound(AddressArray)
				temp = rsCompany(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>ORGANISATION</b> <br> " & StringFullName 
            Address = 	RemainingAddressString		
		else
			FullAddress = "<b>ORGANISATION</b> <br> Details Could not be found!<br>Contact Support Team immediately."
		end if	
	
	ELSEIF NOT isNull(EMPLOYEEID) THEN

		SQL = "SELECT E.EMPLOYEEID, GT.DESCRIPTION AS TITLE, E.FIRSTNAME, E.LASTNAME, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.POSTALTOWN, P.POSTCODE, P.COUNTY FROM E__EMPLOYEE E " &_
				"INNER JOIN E_CONTACT P ON P.EMPLOYEEID = E.EMPLOYEEID " &_
				"LEFT JOIN G_TITLE GT ON E.TITLE = GT.TITLEID " &_
				"WHERE E.EMPLOYEEID = " & EMPLOYEEID
	
		Call OpenRS(rsEmployee, SQL)
	
		count = 1
		IF NOT rsEmployee.EOF THEN
			
			StringName = ""
			Title = rsEmployee("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsEmployee("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName = rsEmployee("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			RemainingAddressString = ""
			AddressArray = Array("ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN","COUNTY", "POSTCODE" )
			for i=0 to Ubound(AddressArray)
				temp = rsEmployee(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>EMPLOYEE</b><br>" & StringName	
            Address = 	RemainingAddressString				
		else
			FullAddress = "<b>EMPLOYEE</b><br>Details Could not be found!<br>Contact Support Team immediately."
		end if			
	
	ELSE
		FullAddress = "Unable to idetify account details<br>Please contact your administrator<br>or software vendor."
	END IF
	
end if
Call CloseRs(rsPP)

SQL = "SELECT SC.DESCRIPTION AS SALESCAT, SI.ITEMNAME, SI.ITEMDESC, SI.SIDATE, SI.NETCOST, SI.VAT, V.VATNAME, SI.GROSSCOST, " &_
		"SI.SISTATUS, SI.SITYPE, SS.SOSTATUSNAME, V.VATCODE FROM F_SALESINVOICEITEM SI " &_
		"INNER JOIN F_SALESCAT SC ON SC.SALESCATID = SI.SALESCATID " &_
		"INNER JOIN F_VAT V ON V.VATID = SI.VATTYPE " &_
		"INNER JOIN F_SOSTATUS SS ON SS.SOSTATUSID = SI.SISTATUS " &_
		"WHERE SI.ACTIVE = 1 AND SALEID = " & L_SALEID & " ORDER BY SALEITEMID DESC"

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF) 
	
	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	'turned vat display ON ------------off
	PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT width=260 style='font-size:12px;'>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD style='font-size:12px;' align=center title='" & rsPI("VATNAME") & "' nowrap width=>" & rsPI("VATCODE") & "</TD><TD style='font-size:12px; font-family:Arial' nowrap width=80>" & FormatNumber(NetCost,2) & "</TD><TD style='font-size:12px;' nowrap width=75>" & FormatNumber(VAT,2) & "</TD><TD style='font-size:12px;' nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD><TD style='font-size:12px;' nowrap width=5></TD></TR>"
	'PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT width=500>" & DataStorage & rsPI("ITEMNAME") &  " (" & rsPI("SALESCAT") & ")</TD><TD nowrap width=135>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5></TD></TR>"
	
	ITEMDESC = rsPI("ITEMDESC")
	if (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
		PIString = PIString & "<TR  id=""PIDESCS"" STYLE='DISPLAY:none' valign=top><TD valign=top colspan=3><I>" & rsPI("SALESCAT") & ":" & rsPI("ITEMDESC") & "</I></TD></TR>"
	end if
	rsPI.moveNext
wend
PIString = PIString & "<TR><TD height='100%'></TD></TR>"
CloseRs(rsPI)

CloseDB()
%>
<HTML>
<HEAD>
<title>&nbsp;</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

    function PrintThePP() {
        document.getElementById("PrintButton").style.visibility = "hidden";
        document.getElementById("SHOWDESC").style.visibility = "hidden";
        ReturnBack = 0
        if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
            ToggleDescs()
            ReturnBack = 1
        }
        window.print();
        if (ReturnBack == 1) ToggleDescs()
        document.getElementById("PrintButton").style.visibility = "visible";
        document.getElementById("SHOWDESC").style.visibility = "visible";
    }

    function ToggleDescs() {
        if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
            NewText = "HIDE DESCRIPTIONS"
            NewStatus = "block"
        }
        else {
            NewText = "SHOW DESCRIPTIONS"
            NewStatus = "none"
        }
        iDesc = document.getElementsByName("PIDESCS")
        if (iDesc.length) {
            for (i = 0; i < iDesc.length; i++)
                iDesc[i].style.display = NewStatus
        }
        document.getElementById("SHOWDESC").innerHTML = NewText
    }			
//-->
</script>

<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<style>
.setFont
    {
        font-size:12px;
       font-family: 'Arial';
    }
</style>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" CLASS='TA'>
<table cellspacing=0 cellpadding=0 width="644" ALIGN=CENTER>
  <tr><td width=10></td><td>

      <TABLE BORDER=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=644>
        <TR> 
          <TD width=100% nowrap valign=top height=100> <b><font style='font-size:12px'>SALES INVOICE</font></b><BR><font style='font-size:12px'> <%=FullAddress%> </font> </TD>
          <TD ALIGN=RIGHT VALIGN=TOP width=134><IMG SRC="/myImages/BHG_Logo_Image.PNG" width="134" height="106">
          <BR><font style='font-size:12px'> <%=TenancyRefrenceId%> </font> 
          </TD>
        </TR>
      </TABLE>
<BR>
<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=644PX>
	<TR><TD class="setFont" WIDTH=90PX>Name:</TD><TD class="setFont" style="font-size:12px"><%=sONAME%></TD>
	</TR>
    <TR><TD class="setFont" WIDTH=90PX style="vertical-align: top;" >Address:</TD><TD class="setFont" style="font-size:12px"><%=Address%></TD>
	</TR>
	<TR><TD class="setFont" WIDTH=90PX>Invoice No:</TD><TD class="setFont"><%=SONUMBER%></TD></TR>
	<TR><TD class="setFont">Tax Date:</TD><TD class="setFont"><%=SODATE%></TD><TD rowspan=2 valign=bottom align=right><input type=Button name="PrintButton" class="RSLButton" value=" PRINT " onClick="PrintThePP()"></td></TR>
	<TR><TD class="setFont">Status:</TD><TD class="setFont"><%=SOSTATUSNAME%></TD></TR>
</TABLE>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=644>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=340 NOWRAP><table cellspacing=0 cellpadding=0 width=340><tr style='color:white'><td class="setFont"><b>Item Name:</b></td><td align=right class="setFont" style='cursor:hand' onClick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
<!--turned  on-->	
	 <TD class="setFont" width=><b>Code:</b></TD>
	  <TD class="setFont" width=><b>Net (�):</b></TD>
	  <TD class="setFont" width=><b>VAT (�):</b></TD>
 
	  <TD class="setFont" width=135><b>Amount (�):</b></TD>
	  <TD  class="setFont" width=5><%=PIString%></TD>		  
	</TR>
	
  </TABLE>
  <TABLE class="setFont" width="644" cellpadding=2 cellspacing=0 STYLE='BORDER:1PX SOLID BLACK;border-top:none' >
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD class="setFont" height=20 width=339 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>

	  <TD class="setFont" width=114 bgcolor=white nowrap><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD class="setFont" width=74 bgcolor=white nowrap><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  
	  <TD class="setFont" width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD class="setFont" width=15 bgcolor=white nowrap></TD>		  
	</TR>
  </TABLE>
<%=InvoiceString%>  

<TABLE class="setFont" cellspacing=0 cellpadding=2 width=644>
	<TR><TD class="setFont"><b>Invoice Notes :</b> </TD></TR>
	<TR><TD  style='border:1px solid #133e71;HEIGHT:60 ;valign=top ;width=644px; font-size:12px'><%=SONOTES%>&nbsp;</TD></TR>
</TABLE>

<div align=center style='width:644'>
  <table class="setFont" width="100%" border="0">
    <tr>
      <td class="setFont"><b>Registered Office</b> Broadland Housing Association Limited, NCFC Jarrold Stand, Carrow Road, Norwich, NR1 1HU<br />Tel: 01603 750200, Fax: 01603 750222</td>
    </tr>
    <tr>
      <td class="setFont">VAT No. <SPAN lang="en-us">927 5127 17</SPAN> </td>
    </tr>
  </table>
</div>
</td></tr></table>

</BODY>
</HTML>
