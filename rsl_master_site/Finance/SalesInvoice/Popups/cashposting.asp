<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim str_SQL, lst_itemtype, lst_paymenttype,lst_salesinvoice, lst_office, employee_name, employee_id, cashposting_id, tenancy_id, theaction
	Dim customer_id,itemtypelist,query_SQL,SIBalance,str_SIBalance
		'SIBalance=1
	accountid = Request("accountid")
	accounttype = Request("accounttype")
	theaction = Request("action")
    saleid=Request("saleid")
	if(accounttype=1) then
	    itemtypelist="5,13"
	else
	    itemtypelist="11"
	end if    
	
	OpenDB()
	
		SQL = "EXEC GET_VALIDATION_PERIOD_NODEFAULT"
	Call OpenRs(rsTAXDATE, SQL)	
   ' Response.Write rsTAXDATE("YSTART")
    ''' uncomment on server and comment for local

	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)

	
	If theaction = "submit" Then
		do_insert()
        elseif theaction = "calculate" Then       
        calculateSIBalance(saleid)
	End If

	'str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")
    str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")

	// get user details
	Call OpenRs(rsSet, str_SQL)	
	If not rsSet.EOF Then 
		employee_id = rsSet("EMPLOYEEID")
		employee_name = rsSet("FULLNAME")
	Else
		employee_name = ""
	End If
    if (AccountType = 1) then	
            searchCriteria="TenancyId"
    elseif (AccountType = 2) then
			searchCriteria = "EMPLOYEEID"
    elseif (AccountType = 3) then
		    searchCriteria = "SUPPLIERID"
	elseif (AccountType = 4) then
			searchCriteria = "SALESCUSTOMERID"
		
		end if
query_SQL = "F_SALESINVOICEITEM WHERE SALEID IN (SELECT SALEID FROM F_SALESINVOICE WHERE " & searchCriteria & " = " & accountid & " AND ACTIVE=1) GROUP BY DBO.F_SALESINVOICEITEM.SALEID "
''' BuildSelect function exist in Include/Functions/LibFunction.asp file
''' this function build a dropdown from data base
''' its parameters are (ByRef lst, iSelectName, iTABLE, iCOLUMNS, iORDERBY, PS, isSelected, iStyle, iClass, iEvent)
	CloseRs(rsSet)
	Call BuildSelect(lst_itemtype, "sel_ITEMTYPE", "F_ITEMTYPE WHERE ITEMTYPEID IN (" & itemtypelist & ")", "ITEMTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)
	Call BuildSelect(lst_paymenttype, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (5,8,25,92,93,95) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "ONCHANGE='pay_change()'")
	Call BuildSelect(lst_salesinvoice, "sel_SALEINVOICE", query_SQL, "SALEID,'SI '+ convert (varchar,SALEID) + '  (�'+convert (varchar,SUM(GROSSCOST))+')' AS TOTALCOST", "SALEID", "Please Select", 0, NULL, "textbox200", "ONCHANGE='calculate_SIBalance()'")
    CloseDB()

	Function do_insert()
		AccountType = Request.Form("hid_ACCOUNTTYPE")
		AccountID = Request.Form("hid_ACCOUNTID")
		
		//if the accounttype is 1 then stick the row into the rent journal, as a trigger will send it over to 
		//sales payment table automatically.
		if (AccountType = 1) then	
			// make journal entry
			'ItemType = 5
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL  " &_
					"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
					" VALUES (" &_
							AccountID & ", '" &_
								Request.Form("txt_CREATIONDATE") & "', " &_
								Request.Form("sel_ITEMTYPE") & ", " &_
								Request.Form("sel_PAYMENTTYPE") & ", -" &_
								Request.Form("txt_AMOUNT") & ", " &_
								2 &_
							  ");SELECT SCOPE_IDENTITY() AS JOURNALID;"
							  
			set rsRentJourn = Conn.Execute(SQL)
			Journal_id = rsRentJourn("JOURNALID")
          SQL ="  Update F_SALESPAYMENT set SaleId= "& Request.Form("sel_SALEINVOICE") &"where journalid ="&Journal_id&" and tenancyId ="&AccountID
      Conn.Execute(SQL)
			rsRentJourn.close()
			Set rsRentJourn = Nothing
         '''Insert into F_SALESINVOICE_PAYMENT new table created
			
		else
			'ItemType = 11
			if (AccountType = 3) then
				InsertColumn = "SUPPLIERID"
			elseif (AccountType = 4) then
				InsertColumn = "SALESCUSTOMERID"
			elseif (AccountType = 2) then
				InsertColumn = "EMPLOYEEID"
			end if
           


			SQL = "SET NOCOUNT ON;INSERT INTO F_SALESPAYMENT  " &_
					"(" & InsertColumn & ", TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID,SaleId ) " &_
					" VALUES (" &_
								AccountID & ", '" &_
								FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
								Request.Form("sel_ITEMTYPE") & ", " &_
								Request.Form("sel_PAYMENTTYPE") & ", -" &_
								Request.Form("txt_AMOUNT") & ", " &_
								2&","&_
                                Request.Form("sel_SALEINVOICE") &_
							  ");"
                
                              'FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
				Response.Write SQL	  
			Conn.Execute(SQL)
			
			SQL2 = "SELECT SCOPE_IDENTITY() AS ROWID;"
					
			set rsRentJourn = Conn.Execute(SQL2)
			Journal_id = rsRentJourn("ROWID")			
			rsRentJourn.close()
			Set rsRentJourn = Nothing
			
			SQL="EXEC F_SALES_LEDGER_BALANCE " & AccountType & " , " & AccountID & " , "  & Request.Form("txt_AMOUNT") & " "
			Conn.Execute(SQL)
			
		end if
	SQL = "SET NOCOUNT ON;INSERT INTO F_SALESINVOICE_PAYMENT  " &_
					"(SALEID, CREATEDBY, AMOUNTPAID, ACCOUNTTIMESTAMP) " &_
					" VALUES (" &_
								 Request.Form("sel_SALEINVOICE") & ", " &_
                                Session("USERID") & ", " &_
                                 Request.Form("txt_AMOUNT") & ",' " &_			
								 Request.Form("txt_CREATIONDATE") & "' " &_
								
							  ");"  'Session("USERID") & ", " &_
				Response.Write 		SQL  
			 Conn.Execute(SQL)			
		chqnumber = Request.Form("txt_CHQNUMBER")
		if (chqnumber = "") then
			chqnumber = "NULL"
		else
			chqnumber = "'" & Replace(chqnumber, "'", "''") & "'"
		end if		
		
		SQL = "set nocount on;INSERT INTO F_CASHPOSTING  " &_
				"(CREATEDBY, CREATIONDATE, RECEIVEDFROM, AMOUNT, OFFICE, ITEMTYPE, PAYMENTTYPE, TENANCYID, CHQNUMBER, JOURNALID, CUSTOMERID, CASHPOSTINGORIGIN) " &_
				" VALUES (" &_	
					Request.Form("txt_CREATEDBY")& ", '" &_
					Request.Form("txt_CREATIONDATE")& "', " &_
                  "''," &_
					
					Request.Form("txt_AMOUNT")& ", " &_
					 "''," &_
					Request.Form("sel_ITEMTYPE") & ", " &_
					Request.Form("sel_PAYMENTTYPE") & ", " &_
					AccountID & ", " &_
					chqnumber & ", " &_
					Journal_id & ", " &_
					"NULL," & AccountType &_
				");SELECT SCOPE_IDENTITY() AS CASHPOSTINGID;"

		'Response.Write SQL

		set rsSet = Conn.Execute(SQL)
		cashposting_id = rsSet("CASHPOSTINGID")
				
		response.redirect "../iframes/iSalesAccount.asp?ACCTYPE=" & AccountType & "&ACCID=" & AccountID
		
	End Function

'calculate the SI balance accourding to the requirement	
Function calculateSIBalance(SaleId)
SQL = "SELECT SUM(GROSSCOST) AS INVOICEAMOUNT FROM F_SALESINVOICEITEM WHERE SALEID = "&SaleId&" GROUP BY DBO.F_SALESINVOICEITEM.SALEID "
	Call OpenRs(rsINVOICEAMOUNT, SQL)	
	INVOICEAMOUNT = rsINVOICEAMOUNT("INVOICEAMOUNT")
SQL = "SELECT SUM(AMOUNTPAID) AS PAIDAMOUNT FROM F_SALESINVOICE_PAYMENT WHERE SALEID =  "&SaleId&" GROUP BY DBO.F_SALESINVOICE_PAYMENT.SALEID"
	Call OpenRs(rsAMOUNTPaid, SQL)
    if rsAMOUNTPaid.EOF = False Then
    PAIDAMOUNT = rsAMOUNTPaid("PAIDAMOUNT")
    else
    PAIDAMOUNT=0
    end if	
	 
 SIBalance=INVOICEAMOUNT - PAIDAMOUNT
 str_SIBalance= "�"& SIBalance

End Function


%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>Cash Posting</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript" src="/js/preloader.js"></script>
<script language="JavaScript" src="/js/general.js"></script>
<script language="JavaScript" src="/js/menu.js"></script>
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript" src="/js/financial.js"></script>
<script src="../../../js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script language="JAVASCRIPT">

	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}	
	var FormFields = new Array()
	FormFields[0] = "txt_EMPLOYEENAME|CREATEDBY|TEXT|Y"
	FormFields[1] = "txt_CREATIONDATE|CREATIONDATE|DATE|Y"
	FormFields[2] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
	FormFields[3] = "sel_PAYMENTTYPE|PAYMENTTYPE|SELECT|Y" 
	FormFields[4] = "sel_SALEINVOICE|SALEINVOICE|SELECT|Y" 
 	FormFields[5] = "txt_CHQNUMBER|Local Office|SELECT|N"	
FormFields[6] = "sel_ITEMTYPE|ITEMTYPE|SELECT|Y" 
	function SaveForm(){
		
		FormFields[5] = "txt_CHQNUMBER|cheque number|TEXT|N"
		if (RSLFORM.sel_PAYMENTTYPE.value == "8" && RSLFORM.txt_CHQNUMBER.value == ""){
			//ManualError("img_CHQNUMBER", "Please enter a number for the cheque.", 0);
		 	FormFields[5] = "txt_CHQNUMBER|cheque number|TEXT|Y"				
			}
		if (!checkForm()) return false
		if (parseFloat(RSLFORM.txt_AMOUNT.value) < 0){
			ManualError("img_AMOUNT", "The amount figure must be greater than 0.", 0)
			return false
			}

				
	<% if LockdownStatus = "ON" then %>
		var YStart = new Date("<%=YearStartDate%>")
		var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
		var YEnd = new Date("<%=YearendDate%>")
		var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
		var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 2, YearStartDate),1) %>") 

		if (!checkForm()) return false;
		if  ( real_date(RSLFORM.txt_CREATIONDATE.value) < YStart || real_date(RSLFORM.txt_CREATIONDATE.value) > YEnd ) {
			// outside current financial year
			if ( TodayDate <= YEndPlusTime ) {
				// inside grace period
				if ( real_date(RSLFORM.txt_CREATIONDATE.value) < YStartPlusTime || real_date(RSLFORM.txt_CREATIONDATE.value) > YEnd) {
					// outside of last and this financial year
					alert("Please enter a tax date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
					return false;
				} else {
					// date must be ok?
				}
			} else {
				// we're outside the grace period and also outside this financial year
				alert("Please enter a tax date for this financial year\n(i.e. between '<%=YearStartDate%>' and '<%=YearendDate%>')");
				return false;                
			}
		}	
	<% end if %>

			
		RSLFORM.target = "SALES_BOTTOM_FRAME"
		RSLFORM.action = "cashposting.asp?action=submit"
		//document.getElementById("btn_POST").disabled = true
		RSLFORM.submit()
		window.close()
		}
	
	function pay_change(){
	    var CHQROW;
        CHQROW = document.getElementById("CHQROW");
		if (RSLFORM.sel_PAYMENTTYPE.value == "8")
			CHQROW.style.display = "table-row";
		else
			CHQROW.style.display = "none";
	
	}
    /// On Change sel_SALEINVOICE dropdown calculate the SaleInvice balance according to calculation formula
    ///this function post the form with sale id,action= calculate, account type and accountid. it will effect the calculateSIBalance asp method.

    function calculate_SIBalance(){	
		var e = document.getElementById("sel_SALEINVOICE");
        var strsaleid= e.options[e.selectedIndex].value;   
        if(strsaleid >0) 
        {   
        var url = "../iFrames/iRechargeStatement.asp?action=calculate&saleid="+strsaleid+"&AccountType="+<%=accounttype %>+"&AccountID="+<%=accountid %>
        loadSIBalance(url)          
        }
        else
        {
        document.getElementById('txt_SIBALANCE').value="";
        }
	}

 function loadSIBalance(url) {          
        $.ajax({
            type: "GET",
            url:  url,
            //data: postData,
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
               // $(loading).show();
            },
            complete: function () {
               // $(loading).hide();
            },

            success: function (result) {
               document.getElementById('txt_SIBALANCE').value="�"+result;
              $('#hid_SI_BALANCE').val(result);
            }
        });
    }

/// On blur check amount is greater than the existing balance then display a message.
 function check_amount(){	
 var  amount=document.getElementById('txt_AMOUNT').value;
  var  value=document.getElementById('hid_SI_BALANCE').value;
		if (amount > value)  
        {
       console.log(value+"   true="+amount);  
        MSG.innerHTML="<p><b>The payment amount entered is higher than the outstanding balance of the selected Sales Invoice. Select 'Post' if you are happy to continue.<b></p>"
		}
        else
        {
		 MSG.innerHTML=""
		}
	}
/// get value from query string 
	function getQuerystring(name) {
    var hash = document.location.hash;
    var match = "";
    if (hash != undefined && hash != null && hash != '') {
        match = RegExp('[?&]' + name + '=([^&]*)').exec(hash);
    }
    else {
        match = RegExp('[?&]' + name + '=([^&]*)').exec(document.location.search);
    }

    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

/// On page load reselect the sale invice drop down.
///
$(document).ready(function() {

var saleid=getQuerystring("saleid");
 if (saleid!= null)
 {
 var ddl = document.getElementById('sel_SALEINVOICE');
var opts = ddl.options.length;
for (var i=0; i<opts; i++){
    if (ddl.options[i].value == saleid){
        ddl.options[i].selected = true;
        break;
    }
}
	
}
});
</script>

<body bgcolor="#FFFFFF" marginheight="0" leftmargin="0" topmargin="0" marginwidth="0"
    style='padding: 10px' >
    <form name="RSLFORM" method="POST">
    <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top" height="20">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td rowspan="2" width="79">
                            <img src="../Images/tab_cheque_cash.gif" width="173" height="20">
                        </td>
                        <td height="19">
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#133E71">
                            <img src="images/spacer.gif" height="1">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71;
                border-bottom: 1px solid #133E71'>
                <br>
                <table cellspacing="1" cellpadding="2" style='border: 1px solid #133E71' width="90%"
                    align="CENTER" border="0">
                    <tr>
                        <td>
                            Taken By
                        </td>
                        <td>
						  <input type="hidden" id="hid_SI_BALANCE"  name="hid_SI_BALANCE" value="" />
                            <input type="TEXT" name="txt_EMPLOYEENAME" id="txt_EMPLOYEENAME" class='TEXTBOX200' value="<%=employee_name%>" readonly />
                            <input type="TEXT" name="txt_CREATEDBY" id="txt_CREATEDBY" class='TEXTBOX200' style='display: NONE'
                                value="<%=employee_id%>" />
                            <image src="/js/FVS.gif" name="img_EMPLOYEENAME" width="15px" height="15px" border="0" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sales Invoice
                        </td>
                        <td>
                            <%=lst_salesinvoice%>
                            <image src="/js/FVS.gif" name="img_SALEINVOICE" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date
                        </td>
                        <td>
                            <input type="TEXT" name="txt_CREATIONDATE" id="txt_CREATIONDATE" class='TEXTBOX200' value="<%=DATE%>">
                            <image src="/js/FVS.gif" name="img_CREATIONDATE" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Item Type
                        </td>
                        <td>
                            <%=lst_itemtype%>
                            <image src="/js/FVS.gif" name="img_ITEMTYPE" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Payment Type
                        </td>
                        <td>
                            <%=lst_paymenttype%>
                            <image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SI Balance
                        </td>
                        <td>
                            <input type="TEXT" name="txt_SIBALANCE" id="txt_SIBALANCE" class='TEXTBOX200' value=" <%Response.Write str_SIBalance%>" readonly="readonly"  >
                            <image src="/js/FVS.gif" name="img_SIBALANCE" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr id='CHQROW' style='display: NONE'>
                        <td>
                            Chq. Number
                        </td>
                        <td>
                            <input type="TEXT" name="txt_CHQNUMBER" id="txt_CHQNUMBER" class='TEXTBOX200' maxlength="29">
                            <image src="/js/FVS.gif" name="img_CHQNUMBER" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Amount
                        </td>
                        <td>
                            <input type="TEXT" name="txt_AMOUNT" id="txt_AMOUNT" class='TEXTBOX200' onblur='check_amount()'>
                            <image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0">
                        </td>
                    </tr>
                    <tr ><td colspan="2" align="center" id="MSG" style=' color:Red;' ></td></tr>
                </table>
                <br>
                <table cellspacing="2" cellpadding="2" style='border: 1px solid #133E71' width="90%"
                    align="CENTER" border="0">
                    <tr>
                        <td colspan="2" align="CENTER">
                            <input type="BUTTON" name="btn_POST" class='RSLBUTTON' value=' Post ' onclick='SaveForm()'>
                            <input type="BUTTON" name="btn_CANCEL" class='RSLBUTTON' value='Close' onclick='window.close()'>
                        </td>
                    </tr>
                </table>
                <input type="HIDDEN" name="hid_ACCOUNTID" value="<%=accountid%>">
                <input type="HIDDEN" name="hid_ACCOUNTTYPE" value="<%=accounttype%>">
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
    <iframe src="/secureframe.asp" name="frm_ct_srv" width="400px" height="400px" style='display: none'>
    </iframe>
</body>
</html>