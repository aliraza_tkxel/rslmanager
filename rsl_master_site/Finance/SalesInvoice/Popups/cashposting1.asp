<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%	
	Dim str_SQL, lst_itemtype, lst_paymenttype, lst_office, employee_name, employee_id, cashposting_id, tenancy_id, theaction
	Dim customer_id
		
	accountid = Request("accountid")
	accounttype = Request("accounttype")
	theaction = Request("action")
	
	OpenDB()
	If theaction = "submit" Then
		do_insert()
	End If

	str_SQL = "SELECT LTRIM(ISNULL(FIRSTNAME,'') + ' ' + ISNULL(LASTNAME,'')) AS FULLNAME, EMPLOYEEID FROM E__EMPLOYEE WHERE EMPLOYEEID = " & Session("USERID")

	// get user details
	Call OpenRs(rsSet, str_SQL)	
	If not rsSet.EOF Then 
		employee_id = rsSet("EMPLOYEEID")
		employee_name = rsSet("FULLNAME")
	Else
		employee_name = ""
	End If
	CloseRs(rsSet)
	
	Call BuildSelect(lst_paymenttype, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (5,8) ", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", "ONCHANGE='pay_change()'")
	Call BuildSelect(lst_office, "sel_OFFICE", "G_OFFICE", "OFFICEID, DESCRIPTION", "DESCRIPTION", "Please Select", 0, NULL, "textbox200", NULL)
	CloseDB()
	
	Function do_insert()
		AccountType = Request.Form("hid_ACCOUNTTYPE")
		AccountID = Request.Form("hid_ACCOUNTID")
		
		//if the accounttype is 1 then stick the row into the rent journal, as a trigger will send it over to 
		//sales payment table automatically.
		if (AccountType = 1) then	
			// make journal entry
			ItemType = 5
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL  " &_
					"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
					" VALUES (" &_
								AccountID & ", '" &_
								FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
								ItemType & ", " &_
								Request.Form("sel_PAYMENTTYPE") & ", -" &_
								Request.Form("txt_AMOUNT") & ", " &_
								2 &_
							  ");SELECT SCOPE_IDENTITY() AS JOURNALID;"
							  
			set rsRentJourn = Conn.Execute(SQL)
			Journal_id = rsRentJourn("JOURNALID")
			rsRentJourn.close()
			Set rsRentJourn = Nothing
		else
			ItemType = 11
			if (AccountType = 3) then
				InsertColumn = "SUPPLIERID"
			elseif (AccountType = 4) then
				InsertColumn = "SALESCUSTOMERID"
			elseif (AccountType = 2) then
				InsertColumn = "EMPLOYEEID"
			end if
			
			SQL = "SET NOCOUNT ON;INSERT INTO F_SALESPAYMENT  " &_
					"(" & InsertColumn & ", TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID ) " &_
					" VALUES (" &_
								AccountID & ", '" &_
								FormatDateTime(Request.Form("txt_CREATIONDATE"),1) & "', " &_
								ItemType & ", " &_
								Request.Form("sel_PAYMENTTYPE") & ", -" &_
								Request.Form("txt_AMOUNT") & ", " &_
								2 &_
							  ");SELECT SCOPE_IDENTITY() AS JOURNALID;"
							  
			set rsRentJourn = Conn.Execute(SQL)
			Journal_id = rsRentJourn("JOURNALID")
			rsRentJourn.close()
			Set rsRentJourn = Nothing
		end if
				
		chqnumber = Request.Form("txt_CHQNUMBER")
		if (chqnumber = "") then
			chqnumber = "NULL"
		else
			chqnumber = "'" & Replace(chqnumber, "'", "''") & "'"
		end if		
		
		SQL = "set nocount on;INSERT INTO F_CASHPOSTING  " &_
				"(CREATEDBY, CREATIONDATE, RECEIVEDFROM, AMOUNT, OFFICE, ITEMTYPE, PAYMENTTYPE, TENANCYID, CHQNUMBER, JOURNALID, CUSTOMERID, CASHPOSTINGORIGIN) " &_
				" VALUES (" &_	
					Request.Form("txt_CREATEDBY")& ", '" &_
					Request.Form("txt_CREATIONDATE")& "', '" &_
					Replace(Request.Form("txt_RECEIVEDFROM"), "'", "''") & "', " &_
					Request.Form("txt_AMOUNT")& ", " &_
					Request.Form("sel_OFFICE") & ", " &_
					ItemType & ", " &_
					Request.Form("sel_PAYMENTTYPE") & ", " &_
					AccountID & ", " &_
					chqnumber & ", " &_
					Journal_id & ", " &_
					"NULL," & AccountType &_
				");SELECT SCOPE_IDENTITY() AS CASHPOSTINGID;"

		Response.Write SQL

		set rsSet = Conn.Execute(SQL)
		cashposting_id = rsSet("CASHPOSTINGID")
				
		response.redirect "../iframes/iSalesAccount.asp?ACCTYPE=" & AccountType & "&ACCID=" & AccountID
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Cash Posting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT">
	
	var FormFields = new Array()
	FormFields[0] = "txt_EMPLOYEENAME|CREATEDBY|TEXT|Y"
	FormFields[1] = "txt_CREATIONDATE|CREATIONDATE|DATE|Y"
	FormFields[2] = "txt_RECEIVEDFROM|RECEIVEDFROM|TEXT|Y"
	FormFields[3] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
	FormFields[4] = "sel_PAYMENTTYPE|PAYMENTTYPE|SELECT|Y"
 	FormFields[5] = "sel_OFFICE|Local Office|SELECT|Y"
 	FormFields[6] = "txt_CHQNUMBER|Local Office|SELECT|N"	

	function SaveForm(){
		
		FormFields[6] = "txt_CHQNUMBER|cheque number|TEXT|N"
		if (RSLFORM.sel_PAYMENTTYPE.value == "8" && RSLFORM.txt_CHQNUMBER.value == ""){
			//ManualError("img_CHQNUMBER", "Please enter a number for the cheque.", 0);
		 	FormFields[6] = "txt_CHQNUMBER|cheque number|TEXT|Y"				
			}
		if (!checkForm()) return false
		if (parseFloat(RSLFORM.txt_AMOUNT.value) < 0){
			ManualError("img_AMOUNT", "The amount figure must be greater than 0.", 0)
			return false
			}
		RSLFORM.target = "SALES_BOTTOM_FRAME"
		RSLFORM.action = "cashposting.asp?action=submit"
		document.getElementById("btn_POST").disabled = true
		RSLFORM.submit()
		window.close()
		}
	
	function pay_change(){
	
		if (RSLFORM.sel_PAYMENTTYPE.value == "8")
			CHQROW.style.display = "block";
		else
			CHQROW.style.display = "none";
	
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0" style='padding:10px'>
<form name="RSLFORM" method="POST">
	<TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
		<tr> 
			<td valign="top" height="20">
				<TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
					<TR>						
						<TD ROWSPAN=2 width="79"><img src="../Images/tab_cheque_cash.gif" width="173" height="20"></TD>
						<TD HEIGHT=19></TD>
					</TR>
					<TR>
						<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<tr> 
			<td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
				<br>
				<table cellspacing=1 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0>  
					<TR>
						<TD>Taken By</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_EMPLOYEENAME CLASS='TEXTBOX200' VALUE="<%=employee_name%>">
							<INPUT TYPE=TEXT NAME=txt_CREATEDBY CLASS='TEXTBOX200' STYLE='DISPLAY:NONE' VALUE=<%=employee_id%>>
							<image src="/js/FVS.gif" name="img_EMPLOYEENAME" width="15px" height="15px" border="0">
						</TD>
					</TR>
						<TR>
						<TD>Office</TD>
						<TD>
							<%=lst_office%>
							<image src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0">
						</TD>
					</TR>
				
					<TR>
						<TD>Date</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_CREATIONDATE CLASS='TEXTBOX200' VALUE="<%=DATE%>">
							<image src="/js/FVS.gif" name="img_CREATIONDATE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Received From</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_RECEIVEDFROM CLASS='TEXTBOX200' MAXLENGTH=99>
							<image src="/js/FVS.gif" name="img_RECEIVEDFROM" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Payment Type</TD>
						<TD>
							<%=lst_paymenttype%>
							<image src="/js/FVS.gif" name="img_PAYMENTTYPE" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR ID='CHQROW' STYLE='DISPLAY:NONE'>
						<TD>Chq. Number</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_CHQNUMBER CLASS='TEXTBOX200' MAXLENGTH=29>
							<image src="/js/FVS.gif" name="img_CHQNUMBER" width="15px" height="15px" border="0">
						</TD>
					</TR>
					<TR>
						<TD>Amount</TD>
						<TD>
							<INPUT TYPE=TEXT NAME=txt_AMOUNT CLASS='TEXTBOX200' ONBLUR='checkForm(true)'>
							<image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0">
						</TD>
					</TR>
				</TABLE>
				<br>
				<table cellspacing=2 cellpadding=2 style='border:1px solid #133E71' WIDTH=90% ALIGN=CENTER border=0> 
					<TR>
						<TD COLSPAN=2 ALIGN=CENTER>
						<INPUT TYPE=BUTTON NAME=btn_POST CLASS='RSLBUTTON' VALUE=' Post ' ONCLICK='SaveForm()'>
						<INPUT TYPE=BUTTON NAME=btn_CANCEL CLASS='RSLBUTTON' VALUE='Close' onclick='window.close()'>
						</TD>
					</TR>
				</table>
				<INPUT TYPE=HIDDEN NAME=hid_ACCOUNTID VALUE=<%=accountid%>>
				<INPUT TYPE=HIDDEN NAME=hid_ACCOUNTTYPE VALUE=<%=accounttype%>>
			</td>
		</tr>
	</table>
</form>
<!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
<iframe name=frm_ct_srv width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>