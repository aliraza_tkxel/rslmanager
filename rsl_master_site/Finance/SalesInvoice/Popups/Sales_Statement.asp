<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<HTML>
	<HEAD>
		<TITLE>Customer Info:</TITLE>
		<% ByPassSecurityAccess = true %>
		<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
		<!--#include file="../../../Customer/iFrames/iHB.asp" -->
		<%
	
	Dim cnt,Balance, customer_id, str_journal_table, str_journal_table_PREVIOUS, str_color, ACCOUNT_BALANCE, ACCOUNT_BALANCE_PREVIOUS, HBPREVIOUSLYOWED

	CurrentDate = CDate("1 jan 2004") 
	TheCurrentMonth = DatePart("m", CurrentDate)
	TheCurrentYear = DatePart("yyyy", CurrentDate)
	TheStartDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
	
	PreviousDate = DateAdd("m", -1, CurrentDate)
	ThePreviousMonth = DatePart("m", PreviousDate)
	ThePreviousYear = DatePart("yyyy", PreviousDate)
	ThePreviousDate = "1 " & MonthName(ThePreviousMonth) & " " & ThePreviousYear
	
	'REsponse.Write ThePreviousDate
	OpenDB()
	customer_id = Request("customerid")
	tenancy_id = Request("tenancyid")
	if (tenancy_id = "") then
		Call OpenRs(rsSet, "SELECT TENANCYID FROM C_CUSTOMERTENANCY WHERE CUSTOMERID = " & customer_id)
		If Not rsSet.EOF Then 
			tenancy_id = rsSet(0)
		Else
			tenancy_id = -1
		End If
		CloseRs(rsSet)
	end if
		
	build_journal()
	
	'HBOWED = TOTAL_HB_OWED_THIS_DATE(customer_id, tenancy_id)
	
	Function build_journal()
		
		cnt = 0
		dim debit,credit
					
		strSQL = 	"F_SALESSTATEMENT  " & Request.QueryString("ACCTYPE")  & ", " & Request.QueryString("ACCID")
		'rw strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
		cnt =cnt+ 1	
			
			
			str_journal_table = str_journal_table & 	"<TR>" &_
															"<TD width='100' class='style3'>" & rsSet("TDATE") & "</TD>" &_
															"<TD width='150' class='style3'>" & rsSet("PAYMENTTYPE") & "</TD>" &_
															"<TD width='110' class='style3'>" & rsSet("STATUS") & "</TD>" &_
															"<TD width='100' class='style3' align=right>" & rsSet("DEBIT") & "</TD>" &_
															"<TD width='100' class='style3' align=right>" & rsSet("CREDIT") & "</TD>" &_
															"<TD width='100' class='style3' ALIGN=RIGHT>" & FormatCurrency(rsSet("RUNNINGBALANCE"))  & "</TD>" 
			
			if cnt=1 then 
				Balance=FormatCurrency(rsSet("RUNNINGBALANCE"))
			end if
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist.</TD></TR>"
		End If
		

		

	End Function

	SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
			"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
			"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
			"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
			"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & tenancy_id
	
	HBOWED = 0

	Call OpenRS(rsCust, SQL)

	count = 1
	while NOT rsCust.EOF
		'This will get the hbowed for each customer
		HBOWED = HBOWED + TOTAL_HB_OWED_THIS_DATE(rsCust("CUSTOMERID"), tenancy_id)	
		
		StringName = ""
		Title = rsCust("TITLE")
		if (Title <> "" AND NOT isNull(Title)) then
			StringName = StringName & Title & " "
		end if
		FirstName = rsCust("FIRSTNAME")
		if (FirstName <> "" AND NOT isNull(FirstName)) then
			StringName = StringName & FirstName & " "
		end if
		LastName =rsCust("LASTNAME")
		if (LastName <> "" AND NOT isNull(LastName)) then
			StringName = StringName & LastName & " "
		end if
		
		if (count = 1) then
			StringFullName = StringName
			count = 2
		else
			StringFullName = StringFullName & "& " & StringName
		end if
		rsCust.moveNext
	wend
	if (count = 2) then
		rsCust.moveFirst()
		housenumber = rsCust("housenumber")
		if (housenumber = "" or isNull(housenumber)) then
			housenumber = rsCust("flatnumber")
		end if
		if (housenumber = "" or isNull(housenumber)) then
			FirstLineOfAddress = rsCust("Address1")
		else
			FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
		end if

		RemainingAddressString = ""
		AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
		for i=0 to Ubound(AddressArray)
			temp = rsCust(AddressArray(i))
			if (temp <> "" or NOT isNull(temp)) then
				RemainingAddressString = RemainingAddressString &  "<TR><TD nowrap style='font-size:12px'>" & temp & "</TD></TR>"
			end if
		next
		tenancyref = rsCust("TENANCYID")
	end if
	
	FullAddress = "<TR><TD nowrap style='font-size:12px'>" & StringFullName & "</TD></TR>" &_
				"<TR><TD nowrap style='font-size:12px'>" & FirstLineOfAddress & "</TD></TR>" &_
				RemainingAddressString				


	
%>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="/css/RSL.css" type="text/css">
			<style type="text/css"> <!-- .style3 {font-family: Verdana, Arial, Helvetica, sans-serif}
	.style7 {font-family: Arial, Helvetica, sans-serif; font-size: 12; font-weight: bold; }
	.style8 {font-family: Verdana, Arial, Helvetica, sans-serif}
	--></style>
			<script lanaguage="javascript">
function PrintMe(){
	document.getElementById("PrintButton").style.visibility = "hidden"
	print()
	document.getElementById("PrintButton").style.visibility = "visible"	
	}
			</script>
	</HEAD>
	<body bgcolor="#ffffff" MARGINTOP="0" MARGINHEIGHT="0" TOPMARGIN="6" onLoad="window.focus()"
		class='ta'>
		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="center">
		<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="110" valign="top">
					<table width="100%">
						<tr>
							<td nowrap valign="top" width="165">
								<table width="163">
									<tr>
										<td nowrap><u><span class="style3"><b><font color="#133e71" style="FONT-SIZE:14px">Sales 
															Statement</font></b></span></u><span class="style3"><br>
											</span><br>
										</td>
									</tr>
									<tr>
										<td nowrap><font style="FONT-SIZE:12px;FONT-FAMILY:Arial, Verdana, Helvetica, sans-serif"><span class="style3">Tenancy Ref: <b><%=TenancyReference(tenancyref)%></b></span></font></td>
									</tr>
									<%=FullAddress%>
								</table>
							</td>
							<td align="right" width="525" valign="top">&nbsp;
							</td>
							<td align="right" valign="top" width="150"><img src="/myImages/LOGOLETTER.gif"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" height="171">
					<table width="100%" cellpadding="1" cellspacing="0" style="BEHAVIOR:url(../../../Includes/Tables/tablehl.htc);BORDER-COLLAPSE:collapse"
						slcolor='' border="0" >
						<thead>
							<tr valign="top">
								<td colspan="4" height="20"><b><font color='#133e71' style='FONT-SIZE:14px'><span class="style3">Date</span>:
											<%=MonthName(DatePart("m", Date)) & " " & DatePart("yyyy", Date)%>
										</font></b>
								</td>
								<td colspan="5" height="20">
									<input type="button" value=" Print Statement" onClick="PrintMe()" class="RSLButton" name="PrintButton">
								</td>
							</tr>
							<tr valign="top">
								<td width="100"  STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;'><span class="style7"><font color='#133e71'>Date</font></span></td>
								<td width="150" STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;'><span class="style7"><font color='#133e71'> Item Type</font></span></td>
								<td width="110" STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;'><span class="style7"><font color='#133e71'>Payment Type</font></span></td>
								<td width="100" STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;' align="right"><span class="style7"><font color='#133e71'>Debit</font></span></td>
								<td width="100" STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;' align="right"><span class="style7"><font color='#133e71'>Credit</font></span></td>
								<td width="100" STYLE='BORDER-top:1px solid #133E71;BORDER-bottom:1px solid #133E71;'  align="right"><span class="style7" ><font color="#133e71">Balance</font></span></td>
							</tr>
							<tr style='HEIGHT:7px'>
								<td colspan="6"></td>
							</tr>
						</thead>
						<tbody>
							<%=str_journal_table%>
						</tbody><tfoot>
							<%If HBOWED <> 0 then %>
							<tr valign="top">
								<td height="20" colspan="5" align="right"><span class="style7"><b></b><font color='red' style='FONT-SIZE:14px'></font><font color='red' style='FONT-SIZE:12px'></font></span><font color='red' style='FONT-SIZE:14px'>&nbsp;</font></td>
								<td height="20" colspan="2" align="right">&nbsp;</td>
							</tr>
							<%Else %>
							<tr valign="top">
								<td colspan="4" height="20" align="right"><span class="style3"></span></td>
								<td align="right"><span class="style3"></span></td>
								<td align="right"><span class="style3"></span></td>
								<td align="right"><span class="style3"></span></td>
							</tr>
							<%End If %>
							<tr valign="top">
								<td height="20" STYLE='BORDER-top:1px solid #133E71;' colspan="5" align="right"><span class="style7"><b><font color='#133e71' style='FONT-SIZE:12px'>Balance 
												:&nbsp;</font></b></span></td>
								<td height="20" STYLE='BORDER-top:1px solid #133E71;' colspan="2" align="right"><span class="style7"><b><font color='#133e71' style='FONT-SIZE:12px'><%=BALANCE%></font></b></span></td>
							</tr>
						</tfoot>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="bottom" align="right" height="100%">
					<table width="100%" align="right">
						<tr>
							<td width="69%" valign="top"><span class="style3">Please <span class="style8">contact</span> your housing officer <%= FullName%> to discuss further.<br>
                <br>
                </span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width="69%" valign="top">&nbsp;</td>
							<td width="31%">
								<div align="right" class="style3">&nbsp;16 The Croft</div>
							</td>
						</tr>
						<tr>
							<td width="69%" valign="top">&nbsp;</td>
							<td>
								<div align="right" class="style3">&nbsp;Stockbridge Village</div>
							</td>
						</tr>
						<tr>
							<td width="69%" valign="top">&nbsp;</td>
							<td align="right">
								&nbsp;<font face="Arial, Helvetica, sans-serif">L28 1NR</font></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</td>
		</tr>
		</table>
	</body>
</HTML>
