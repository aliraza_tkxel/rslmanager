<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim rsLoader, ACTION
Dim rq_Active,rq_Inactive

ACTION = "NEW"
SCID = Request("ID")
if (SCID = "") then SCID = -1 end if

rq_Active = "CHECKED"
rq_Inactive = ""

OpenDB()
l_dt = "New Sales Customer"
l_reference = "<b>Auto Generated</b>"
SQL = "SELECT * FROM F_SALESCUSTOMER WHERE SCID = " & SCID 
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then

	l_dt = "Update Sales Customer"
	l_organisation = rsLoader("ORGANISATION")
	l_address1 = rsLoader("ADDRESS1")
	l_address2 = rsLoader("ADDRESS2")
	l_address3 = rsLoader("ADDRESS3")	
	l_towncity = rsLoader("TOWNCITY")
	l_county = rsLoader("COUNTY")
	l_postcode = rsLoader("POSTCODE")
	l_telephone1 = rsLoader("TELEPHONE1")
	l_telephone2 = rsLoader("TELEPHONE2")	
	l_fax = rsLoader("FAX")						
	l_website = rsLoader("WEBSITE")						
	l_contact = rsLoader("CONTACT")
	l_creditlimit = rsLoader("CREDITLIMIT")
	if (not ISNULL(l_creditlimit) AND l_creditlimit <> "") then
		l_creditlimit = FormatNumber(l_creditlimit,2,-1,0,0)
	end if
	l_paymentterms = rsLoader("PAYMENTTERMS")
	l_reference = SalesCustomerReference(rsLoader("SCID"))
	l_description = rsLoader("DESCRIPTION")

    ' if the organisation is active radio button will set it to active
	l_cusactive= rsLoader("CUSACTIVE")
    l_company = rsLoader("companyid")
    if l_cusactive = true then
	    rq_Active = "CHECKED" 
	    rq_Inactive = ""
    else  
	   rq_Inactive = "CHECKED"
	    rq_Active =""
	end if

	ACTION = "AMEND"
end if

Call BuildSelect(lstCompany, "sel_COMPANYID", "G_Company", "CompanyId, Description", "CompanyId", null, l_company, NULL, "textbox200", "  ")


CloseRs(rsLoader)
CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Sales --&gt; General Customer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var FormFields = new Array();
FormFields[0] = "txt_ADDRESS1|Address 1|TEXT|Y"
FormFields[1] = "txt_ADDRESS2|Address 2|TEXT|N"
FormFields[2] = "txt_ADDRESS3|Address 3|TEXT|N"
FormFields[3] = "txt_TOWNCITY|TOWNCITY|TEXT|Y"
FormFields[4] = "txt_COUNTY|County|TEXT|N"
FormFields[5] = "txt_POSTCODE|Postcode|POSTCODE|Y"
FormFields[6] = "txt_ORGANISATION|Organisation|TEXT|Y"
FormFields[7] = "txt_TELEPHONE1|Telephone 1|TELEPHONE|N"
FormFields[8] = "txt_TELEPHONE2|Telephone 2|TELEPHONE|N"
FormFields[9] = "txt_FAX|Fax|TELEPHONE|N"
FormFields[10] = "txt_WEBSITE|Web Site|TEXT|N"
FormFields[11] = "txt_CONTACT|Contact|TEXT|Y" 
FormFields[12] = "txt_CREDITLIMIT|Credit Limit|CURRENCY|N" 
FormFields[13] = "txt_PAYMENTTERMS|Payment Terms|INTEGER|N" 
FormFields[14] = "txt_DESCRIPTION|Description|TEXT|N" 

function SaveForm(){
	//if (!checkForm()) return false;
	RSLFORM.submit()
	}

function limitText(textArea, length, errorTag) {
    if (textArea.value.length > length) {
        textArea.value = textArea.value.substr(0,length);
		ManualError(errorTag, "This field has been truncated, the maximum number of characters allowed is 1000.", 2)
    }
}	
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width=750 border=0 cellpadding=0 cellspacing=0>
  <tr> 
    <td rowspan=2> <img name="tab_main_details" src="images/tab_salescustomer.gif" width=123 height=20 border=0></td>
    <td align=right height=19><font color=red><%=l_dt%></font></td>
  </tr>
  <tr> 
    <td bgcolor=#133E71> <img src="/myImages/spacer.gif" width=619 height=1></td>
  </tr>
  <tr> 
    <td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> 
      <img src="/myImages/spacer.gif" width=53 height=6></td>
  </tr>
</table>
<TABLE cellspacing=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="90%">
  <form name="RSLFORM" method="POST" ACTION="ServerSide/SalesCustomer_svr.asp">
    <TR> 
      <TD nowrap width=85px>Contact:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_CONTACT" maxlength=20 value="<%=l_contact%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_CONTACT" width="15px" height="15px" border="0"></TD>
      <TD nowrap>Reference:</td>
      <td> 
        <%=l_reference%>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_REFERENCE" width="15px" height="15px" border="0"></TD>
      <TD width=100%>&nbsp;</TD>
    </TR>
    <TR> 
      <TD nowrap>Organisation:</td>
      <td> 
        <input type="textbox" class="textbox200" name="txt_ORGANISATION" maxlength=50 value="<%=l_organisation%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_ORGANISATION" width="15px" height="15px" border="0"></TD>
      <TD nowrap width=85px>Telephone 1:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_TELEPHONE1" maxlength=20 value="<%=l_telephone1%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_TELEPHONE1" width="15px" height="15px" border="0"></TD>
      <TD width=100%>&nbsp;</TD>
    </TR>
    <TR> 
      <TD nowrap>Address 1:</td>
      <td> 
        <input type="textbox" class="textbox200" name="txt_ADDRESS1" maxlength=50 value="<%=l_address1%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_ADDRESS1" width="15px" height="15px" border="0"></TD>
      <TD nowrap width=85px>Telephone 2:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_TELEPHONE2" maxlength=20 value="<%=l_telephone2%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_TELEPHONE2" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD width=90px>Address 2:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_ADDRESS2" maxlength=40 value="<%=l_address2%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_ADDRESS2" width="15px" height="15px" border="0"></TD>
      <TD>Fax:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_FAX" maxlength=20 value="<%=l_fax%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_FAX" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD width=90px>Address 3:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_ADDRESS3" maxlength=50 value="<%=l_address3%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_ADDRESS3" width="15px" height="15px" border="0"></TD>
      <TD>Web Site:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_WEBSITE" maxlength=200 value="<%=l_website%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_WEBSITE" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD NOWRAP>Town / City:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_TOWNCITY" MAXLENGTH=40 value="<%=l_towncity%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_TOWNCITY" width="15px" height="15px" border="0"></TD>

      <TD>Credit Limit:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_CREDITLIMIT" maxlength=200 value="<%=l_creditlimit%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_CREDITLIMIT" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>County:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_COUNTY" MAXLENGTH=30 value="<%=l_county%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_COUNTY" width="15px" height="15px" border="0"></TD>

      <TD nowrap>Payment Terms:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_PAYMENTTERMS" maxlength=200 value="<%=l_paymentterms%>" tabindex=2>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_PAYMENTTERMS" width="15px" height="15px" border="0"></TD>
    </TR>
    <TR> 
      <TD>Postcode:</TD>
      <TD> 
        <input type="textbox" class="textbox200" name="txt_POSTCODE" MAXLENGTH=10 value="<%=l_postcode%>" TABINDEX=1>
      </TD>
      <TD><image src="/js/FVS.gif" name="img_POSTCODE" width="15px" height="15px" border="0"></TD>

       <td nowrap>Status of Customer:</td>
       <td nowrap>
          <input name="rdo_CUSACTIVE" id="chk1"  value="1" type="radio"  <%=rq_Active%> /> <i>Active</i>
           <input name="rdo_CUSACTIVE" id="chk2" value="0" type="radio" <%=rq_Inactive%> /><i>Inactive</i>
       </td> 
       <td><image src="/js/FVS.gif" name="img_CUSACTIVE" width="15px" height="15px" border="0"></td>
      
    </TR>
    <tr> 
      <td nowrap valign=top> Company:</td>
      <td nowrap colspan=4> 
        <%=lstCompany%>
      </td>
      <td valign=top></td>
    </tr>
    <tr> 
      <td nowrap valign=top> Description:</td>
      <td nowrap colspan=4> 
        <textarea name="txt_DESCRIPTION" class="textbox" rows=8 cols=100 TABINDEX=3 onblur="limitText(this,3000,'img_DESCRIPTION')"><%=l_description%></TEXTAREA>
      </td>
      <td valign=top><image src="/js/FVS.gif" name="img_DESCRIPTION" width="15px" height="15px" border="0"></td>
    </tr>
    <TR> 
      <TD colspan=4></TD>
      <TD align=right> 
        <input type=hidden name="hid_SCID" value="<%=SCID%>">
        <input type=hidden name="hid_Action" value="<%=ACTION%>">
		<% IF ACTION = "AMEND" THEN %>
        <input type=button class="RSLButton" value=" UPDATE " onclick="SaveForm()">
		<% ELSE %>
        <input type=button class="RSLButton" value=" SAVE " onclick="SaveForm()">
		<% END IF %>
      </TD>
    </TR>
    <TR height="100%"> 
      <TD height="100%">&nbsp;</TD>
    </TR>
  </FORM>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
