<%@  language="VBscript" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	Dim cnt, str_journal_table, status_sql, theURL
	Dim mypage, orderBy, str_table_bottom, acctype, accid, SalesBalance

	acctype = Request("ACCTYPE")
	accid = Request("ACCID")
		
	OpenDB()
	build_salesaccount()
	get_balance()
	CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	Function get_balance()
		strSQL = "F_SALESACCOUNT_BALANCE " & acctype & ", " & accid	
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_StrING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
		if (NOT rsSet.EOF) then
			SalesBalance = FormatCurrency(rsSet("BALANCE"))
		end if
		rsSet.close()
	
	End Function
	
	Function build_salesaccount()
		cnt = 0
		PageName = "iSalesAccount.asp"
		theURL = theURLSET("cc_sort|page")

		Const adCmdText = &H0001
		
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CInt(mypage)
		end if
			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 13

		strSQL = "F_SALESACCOUNT " & acctype & ", " & accid
	
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_StrING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
			
		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize
	
		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF then
			rsSet.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if
				
		str_journal_table = ""
		For i=1 to pagesize
			If NOT rsSet.EOF Then
				cnt = cnt + 1
				str_journal_table = str_journal_table &_
					"<tr>" &_
						"<td style='width: 10%;' class='UNDRLNE'>" & rsSet("tdATE") & "</td>" &_
						"<td style='width: 30%;' class='UNDRLNE'>" & rsSet("DESC") & "</td>" &_
						"<td style='width: 20%;' class='UNDRLNE'>" & rsSet("PAYMENTTYPE") & "</td>" &_
						"<td style='width: 10%;' align='right' class='UNDRLNE'>" & rsSet("DEBIT") & "</td>" &_
						"<td style='width: 10%;' align='right' class='UNDRLNE'>" & rsSet("CREDIT") & "</td>" &_
						"<td style='width: 20%;' align='right' class='UNDRLNE'>" & rsSet("RUNNINGBALANCE") & "</td>" &_												
					"<tr>"
				rsSet.movenext()
			End If
		Next
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<tr><td HEIGHT=18 COLSPAN=8 align=CENTER>No repair work order entries exist for selected criteria.</td></tr>"
		End If

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<TFOOT><tr><td HEIGHT='100%'></td></tr><tr><td COLSPAN=8 align=CENTER HEIGHT=20>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%' style='BORDER-TOP:1px solid;'><Thead><tr style='BACKGROUND-COLOR:BEIGE;'><td WIDTH=100></td><td align=CENTER>"  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
		" Page <font color=red><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b>Next</font></b></a>"  &_ 
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b>Last</font></b></a>"  &_
		"</td><td align='right' WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
		"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
		"</td></tr></Thead></TABLE></td></tr></TFOOT>" 
		
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealtrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealtrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance -- > Sales Account</title>
    <meta http-equiv="content-Type" content="text/html; charset=iso-8859-1"/>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
    <style type="TEXT/CSS">
<!--
.UNDRLNE {border-bottom:1px solid #e3e3e3}
-->
</style>
</head>
<script language="Javascript" type="text/javascript" src="/js/general.js"></script>
<script language="Javascript" type="text/javascript">
	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=<%=orderBy%>&CWO=" + WOID + "&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value
	}

	function SORTPAGE(SORT){
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=" + SORT + "&CWO=<%=CurrentWorkOrder%>&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value		
	}

	function ShowInvoice(Sale_id){
   		var strContent;

		strContent = navigator.userAgent;
		strContent = strContent.toLowerCase();

		if (strContent.search("chrome") > 0){
			var centerLeft = (screen.width/2)-(440/2);
			var centerTop = (screen.height/2)-(340/2);

			window.open("../Popups/SalesInvoice.asp?SaleID=" + Sale_id + "&Random=" + new Date(), "","Height=440px, Width=750px, left=" + centerLeft + ", top="+ centerTop +",edge=Raised, center=Yes, help= No, resizable= Yes, status= No, scroll= no");
			return;
		}
		else{
		    window.showModelessDialog("../Popups/SalesInvoice.asp?SaleID=" + Sale_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
			return;
		}
	}
	
	function ShowCreditNote(SCN_id){
        var strContent;

		strContent = navigator.userAgent;
		strContent = strContent.toLowerCase();

		if (strContent.search("chrome") > 0){
			var centerLeft = (screen.width/2)-(440/2);
			var centerTop = (screen.height/2)-(340/2);

			window.open("../../Popups/SalesCreditNote.asp?SCNID=" + SCN_id + "&Random=" + new Date(),"","Height=440px, Width=750px, left=" + centerLeft + ", top="+ centerTop +",edge=Raised, center=Yes, help= No, resizable= Yes, status= No, scroll= no");
			return;
		}
		else{
		    window.showModelessDialog("../../Popups/SalesCreditNote.asp?SCNID=" + SCN_id + "&Random=" + new Date(),"_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
			return;
		}
    }
		
	function DoSync(){
		parent.RSLFORM.SalesBalance.value = "<%=SalesBalance%>"
		}
				
	<%= WriteJavaJumpFunction() %>	
</script>
<body style="background-color: #FFFFFF; margin-top: 0; margin-left: 0; margin-'right': 0;"
    class="TA" onload="DoSync();parent.STOPLOADER('BOTTOM');">
    <table cellpadding="0" cellspacing="0" style="border-collapse: collapse; height: 100%;
        width: 100%;" border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: beige; height:20px; ">
                <td style="border-bottom: 1px solid; width: 10%;">
                    <b>Date</b>
                </td>
                <td style="border-bottom: 1px solid; width: 30%;">
                    <b>Description</b>
                </td>
                <td style="border-bottom: 1px solid; width: 20%;">
                    <b>Type</b>
                </td>
                <td style="border-bottom: 1px solid; width: 10%;" align='right'>
                    <b>Debit</b>
                </td>
                <td style="border-bottom: 1px solid; width: 10%;" align='right'>
                    <b>Credit</b>
                </td>
                <td style="border-bottom: 1px solid; width: 20%;" align='right'>
                    <b>Balance</b>
                </td>
            </tr>
            <tr>
                <td height="2">
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="8">
                    <div style='overflow: auto; height: 205'>
                        <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                            <%=str_journal_table%>
                        </table>
                    </div>
                </td>
            </tr>
            <%=str_table_bottom%>
        </tbody>
    </table>
</body>
</html>
