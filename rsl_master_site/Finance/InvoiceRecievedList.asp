<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
''''''''''''
'' PAGE NOTE: This is a list of all the repairs that an invoice has been recieved for
''
'' NOTE: Pages to be aware of: InvoiceRecievedList.asp 
''							   InvoiceRecieved_srv.asp 
''						       InvoiceRecieved.asp
''''''''''''
bypasssecurityaccess = true

Call OpenDB()
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select Supplier", rqSupplier, NULL, "textbox200", " style=""width:240px"" ")
Call BuildSelect(lstFiscalYear, "sel_FISCALYEAR", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, Yend, 103)", "CONVERT(VARCHAR, YSTART, 103)+ ' - ' + CONVERT(VARCHAR, Yend, 103)", "Please Select...", rqFiscalYear, NULL, "textbox200", " style=""width:180px"" ")
Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
	
Call CloseDB()
 
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Invoice Received</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
     <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script src="../js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function load_me(OID, page) {
           
            GetListWrtChild(OID,page);
           
        }
        function show_detail(Order_id) {
            if (window.showModelessDialog) {        // Internet Explorer
                window.showModalDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
            }
            else {
                window.open("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "", "width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
            }
        }

	</script>
    <script type="text/javascript" language="JavaScript">
<!--
        var FormFields = new Array();
        var str_idlist, int_initial;
        int_initial = 0;
        str_idlist = "";
        detotal = new Number();

        FormFields[0] = "txt_FROM|From Date|DATE|Y"
        FormFields[1] = "sel_OFFICE|Local Office|SELECT|N"
        FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|N"
        FormFields[3] = "txt_TO|To Date|DATE|Y"

        function updatePOStatus(p_orderid, p_userId, p_postatusid, p_postatusname, child, updateParent,totalRecord) {
            p_postatusname = p_postatusname.replace(/&/g, ' ');
			p_orderid = p_orderid.toString();
            p_userId = p_userId.toString();
            p_postatusid = p_postatusid.toString();
            p_postatusname = p_postatusname.replace("_", " ").toString();
            SetButtonState(true, totalRecord);
           
            $.ajax({
                url: 'UpdatePOStatus.asp',
                type: 'get',
                data: { orderid: p_orderid, userId: p_userId, postatusid: p_postatusid, postatusname: p_postatusname, ischild: child, updateParent: updateParent },
                cache: false,
                success: function(data) {
                GetList();
                SetButtonState(false,totalRecord);    
                },
                error: function() {
                SetButtonState(false, totalRecord); 
                alert('Unable to update status due to internal server error!');
                    
                }
            });
        }
        function SetButtonState(diableState,totalRecord) {
            for (i = 1; i <= totalRecord; i++) {
                if (document.getElementById("btn_approve" + i) != null) {
                    document.getElementById("btn_approve" + i).disabled = diableState;
                }
            }
        }
        function GetList() {
            RSLFORM.target = "frm_slip";
            var POId = '<%=REQUEST("CPO") %>';
            if (POId.length <= 0) {
                POId = 0;
            }
            var page = '<%=REQUEST("page") %>';
          /*  var sel_Supplier = document.getElementById("sel_SUPPLIER");
            var supplier = sel_Supplier.options[sel_Supplier.selectedIndex].value;
           */
            parent.hb_div.innerHTML = "<span style='color: silver; font-size: 20px; font-weight: bold;' align='center'>Please Wait, Page is Loading...</span>";
            RSLFORM.action = "/finance/serverside/InvoiceRecieved_srv.asp?CPO=" + POId + "&page=" + page;  //+"&sel_SUPPLIER=" + supplier;
            RSLFORM.submit();
                    
             
        }
        function GetListWrtChild(OrderId,currentPage) {
            RSLFORM.target = "frm_slip";
     
            var page = '<%=REQUEST("page") %>';
            var sel_Supplier = document.getElementById("sel_SUPPLIER");
            var supplier = sel_Supplier.options[sel_Supplier.selectedIndex].value;

            var sel_Company = document.getElementById("sel_COMPANY");
            var company = sel_Company.options[sel_Company.selectedIndex].value;

            parent.hb_div.innerHTML = "<span style='color: silver; font-size: 20px; font-weight: bold;' align='center'>Please Wait, Page is Loading...</span>";
            RSLFORM.action = "/finance/serverside/InvoiceRecieved_srv.asp?CPO=" + OrderId + "&page=" + currentPage + "&sel_SUPPLIER=" + supplier + "&sel_COMPANY=" + company;
            RSLFORM.submit();
   
        }
        

        // CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
        function do_sum(int_num) {

            if (document.getElementById("chkpost" + int_num + "").checked == true) {
                //detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
                if (int_initial == 0) // first entry
                    str_idlist = str_idlist + int_num.toString();
                else
                    str_idlist = str_idlist + "," + int_num.toString();
                int_initial = int_initial + 1; // increase count of elements in string

            }
            else {
                //detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
                int_initial = int_initial - 1;

                remove_item(int_num);
            }

            //document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);

            document.getElementById("idlist").value = str_idlist;
        }

        // REMOVE ID FROM IDLIST
        function remove_item(to_remove) {

            var stringsplit, newstring, index, cnt, nowt;
            stringsplit = str_idlist.split(","); // split id string
            cnt = 0;
            newstring = "";
            nowt = 0;

            for (index in stringsplit)
                if (to_remove == stringsplit[index])
                    nowt = nowt;
                else {
                    //alert("keeping  "+stringsplit[index]);
                    if (cnt == 0)
                        newstring = newstring + stringsplit[index].toString();
                    else
                        newstring = newstring + "," + stringsplit[index].toString();
                    cnt = cnt + 1;
                }
            str_idlist = newstring;
        }

        function open_me(WorkOrder_id) {
            event.cancelBubble = true;
            //alert("../Finance/Popups/Purchaseorder_jim.asp?WORKOrderID=" + WorkOrder_id )
            window.showModelessDialog("../Finance/Popups/Purchaseorder_jim.asp?WorkOrderID=" + WorkOrder_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;");
        }

        function setSupplier() {
            var suppiler = '<%=Request("sel_SUPPLIER")%>';
           // alert("get"+suppiler);
            document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = suppiler;
         }

         function setCompany() {
            var company = '<%=Request("sel_COMPANY")%>';
            document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value = company;
          }
// -->
    </script>
	

	
</head>
<%
PageName = "InvoiceRecievedList.asp"
 %>
<!-- End Preload Script -->

 <body class='TA' bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages();GetList();setSupplier();setCompany()"
    onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
  
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="RSLFORM" method="post" action="">
        <table class="RSLBlack">
            <tr>
                <td></td>
                <td>
                    <b>Order No: </b>
                </td>
                <td>
                    <b></b>
                </td>
                <td>
                    <b> </b>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="text" name="PONumber" id="PONumber" class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>"
                        style="width: 135px; background-image: url('img/CXfilter.gif'); background-repeat: no-repeat; background-attachment: fixed" />
                </td>
                <td>
                   
                </td>
                <td>
                   
                </td>
                <td>
                   
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <b>Suppliers: </b>
                </td>
                <td>
                    <b>Company: </b>
                </td>
                <td>
                    <b>Fiscal Years: </b>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <%=lstSuppliers%>

                </td>
                <td>
                    <%=lstCompany%>
                </td>
                <td>
                    <%=lstfiscalyear%>
                </td>
                <td>
                     <input type="button" class="RSLButton" value=" Update Search " title="Update Search"
                        onclick="GetList()" />
                </td>
            </tr>
        </table>
        <div id="BOTTOM_DIV_LOADER" style="display: none; overflow: hidden; width: 750px; height: 210px;">

        <table width="750" border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td style="color: silver; font-size: 20px; font-weight: bold" align="left" valign="middle">&nbsp; <div id="LOADINGTEXT_BOTTOM">Please Wait, Page is Loading...</div>
                             
                </td>
            </tr>
            <tr>
                <td bgcolor="#133E71">
                    <img src="images/spacer.gif" width="750" height="1" alt="" />
                </td>
            </tr>
        </table>
        </div>
    <br />
    <div id="hb_div">
    </div>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_slip" width="800px" height="400px" style='display: none'>
    </iframe>
</body>
</html>
