<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
 
<%
 
    Dim  company 
company = Request("sel_COMPANY")
    if company = "" then company = "1" 
    Dim OptionVals 
    Dim StartMonthDateSet
	Dim DayLimit 
	
	OpenDB()
	
	SQL = 	" SELECT DEFAULTVALUE FROM dbo.RSL_DEFAULTS WHERE DEFAULTNAME = 'MONTHLYLOCKDOWNLIMIT' " 
    Call OpenRs(rsLoader, SQL) 
	
    if (NOT rsLoader.EOF) then
         DayLimit  = rsLoader("DEFAULTVALUE")
    end if
	 
	
    if (Day(now) < (DayLimit - 1)) then
        StartMonthDateSet = DateAdd("m",-1, now)
    else
        StartMonthDateSet =  now
    end if

    For i = 0 To 11

           OptionVals = OptionVals & "<option value='"  & Month(DateAdd("m",i,StartMonthDateSet)) & "-" & Year(DateAdd("m",i,StartMonthDateSet)) & "'>" & MonthName( Month(DateAdd("m",i,StartMonthDateSet))) & " " & Year(DateAdd("m",i,StartMonthDateSet)) & "</option>,"
          '  response.write MonthName(i, True) & "-" & Year(Now)
    next
     
 

     
	strROWS = strROWS  & "<tr>"
	strROWS = strROWS  & "<td><b>Module</B></td>"
	strROWS = strROWS  & "<td><font color=green><b>Sub Module</B></font></td>"
	strROWS = strROWS  & "<td><b>Description</B></td>"
    strROWS = strROWS  & "<td><b>Month</B></td>"
	strROWS = strROWS  & "<td><b>Days Extended By</b></td>"
	strROWS = strROWS  & "<td align='centre'><b>Active</b></td>"
	strROWS = strROWS  & "<tr>"

OpenDB()


    sql="DECLARE @LOCKDOWNID int " &_
        "DECLARE @COMPANYID int " &_
        "SET @COMPANYID =  " & Company &_
        " DECLARE MY_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY " &_
        "FOR  " &_
        "SELECT DISTINCT LOCKDOWNID FROM AC_FINANCIALLOCKDOWN WHERE DeletedFlag = 0 and CompanyId = @COMPANYID "  &_
        "OPEN MY_CURSOR FETCH NEXT FROM MY_CURSOR INTO @LOCKDOWNID WHILE @@FETCH_STATUS = 0 " &_
        "BEGIN  " &_
	     "   EXEC [GET_VALIDATION_PERIOD_AUTOUPDATE] @LOCKDOWNID , @COMPANYID " &_
         "   FETCH NEXT FROM MY_CURSOR INTO @LOCKDOWNID " &_
        "END " &_
        "CLOSE MY_CURSOR DEALLOCATE MY_CURSOR"
    conn.Execute(sql)

	SQL = 	" select  FL.LOCKDOWNID,fl.DESCRIPTION as page,M.DESCRIPTION as module, FL.SUBMODULEDESCRIPTION, FL.EXTENDEDDAYS,FL.ACTIVE, FL.EXTENDEDMONTH, FL.EXTENDEDYEAR " &_
		"from AC_FINANCIALLOCKDOWN FL " &_
		"	INNER JOIN AC_MODULES M ON M.MODULEID = FL.MODULE " &_
        "	where fl.deletedflag = 0  and FL.CompanyId = " & company &_
		" ORDER BY LOCKDOWNID "
			
Call OpenRs(rsLoader, SQL)

if (NOT rsLoader.EOF) then
while NOT rsLoader.EOF

	last_module = l_Module
	last_Submodule = l_SubModule
 			
	l_Module 			= rsLoader("MODULE")
	l_LockDownID	 		= rsLoader("LOCKDOWNID")
	l_PageDecription		= rsLoader("page")
	l_SubModule			= rsLoader("SUBMODULEDESCRIPTION")
    l_ExtendedMonth			= rsLoader("EXTENDEDMONTH")
    l_ExtendedYear			= rsLoader("EXTENDEDYEAR")
	l_ExtendedDays			= rsLoader("EXTENDEDDAYS")
	l_Active			= rsLoader("ACTIVE")

	tmp_module = l_Module
	if l_Module = last_module then l_Module = "" end if

	tmp_SubModule = l_SubModule
	if l_SubModule = last_SubModule then l_SubModule = "" end if

	if l_Active = 1 then CheckBox = "CHECKED" else CheckBox ="" end if
    OptionVals_this = ""
    OptionValsCopy = OptionVals
    IF (Instr(OptionValsCopy, l_ExtendedMonth & "-" & l_ExtendedYear) > 0) THEN  
        OptionValsCopy = Replace(OptionValsCopy,"value='" & l_ExtendedMonth & "-" & l_ExtendedYear & "'"," SELECTED value='" & l_ExtendedMonth & "-" & l_ExtendedYear & "'")
    'ELSE 
      ' OptionVals_this = "<option SELECTED value='"  & l_ExtendedMonth & "-" & l_ExtendedYear & "'>" & MonthName(l_ExtendedMonth) & " " & l_ExtendedYear & "</option>,"
    END IF 
    OptionVals_this = OptionVals_this & OptionValsCopy

          

	strROWS = strROWS  & "<tr>"
	strROWS = strROWS  & "<td><b>" & l_Module & "</B></td>"
	strROWS = strROWS  & "<td><font color=green>" & l_SubModule & "</font></td>"
	strROWS = strROWS  & "<td>" & l_PageDecription & "<input type='hidden' name='hdnName" & l_LockDownID & "' value='" & l_PageDecription & "'></td>"
   strROWS = strROWS  & "<td><select name='sel_ExtendedMonthYear" & l_LockDownID & "'>" & OptionVals_this & "</td>"
	strROWS = strROWS  & "<td><input type=text name='txt_ExtendedDays" & l_LockDownID & "' value='" & l_ExtendedDays & "' class='textbox' maxlength=10 style='width:100px;text-align:right' onblur='ValidateDays(this, hdnName" & l_LockDownID & ".value);'></td>"
	strROWS = strROWS  & "<td align='centre'><INPUT TYPE='CHECKBOX' NAME='CHKS" & l_LockDownID & "' ID='CHKS" & l_LockDownID & "' VALUE='1' " & CHECKBOX & " ></td>"
	strROWS = strROWS  & "<tr>"

	l_module = tmp_module
	l_SubModule = tmp_Submodule

	rsLoader.MoveNext				
wend	


	strROWS = strROWS  & "<tr>"
	strROWS = strROWS  & "<td></td>"
	strROWS = strROWS  & "<td></td>"
	strROWS = strROWS  & "<td></td>"
	strROWS = strROWS  & "<td></td>"
	strROWS = strROWS  & "<td><input type=button class=RSLButton value=' SAVE ' onclick='SaveForm()'  ></td>"
	strROWS = strROWS  & "<tr>"

end if

CloseRs(rsLoader)

 Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", Company, null, "textbox200", " onchange=""JAVASCRIPT:CompanyChange()"" style='width:200px'")	
	
 
CloseDB()


%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --&gt; Tools --&gt; Monthly Lockdown Tool</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT> 
<SCRIPT LANGUAGE="JavaScript">
<!-- 


    function SaveForm() {


        RSLFORM.action = "ServerSide/FincancialYearLockdown_svr.asp"
        RSLFORM.target = "svrFrame"
        RSLFORM.submit()

    }

    function SaveLimit() {


        RSLFORM.action = "ServerSide/FincancialYearLockdown_svr.asp?LimitUpdate=1"
        RSLFORM.target = "svrFrame"
        RSLFORM.submit()

    }

    function CompanyChange() {

        var COMPANYID = RSLFORM.sel_COMPANY.value;
        RSLFORM.action = "FinancialYearLockdown.asp?Company=" + COMPANYID;
        RSLFORM.target = "_self"
        RSLFORM.submit()

    }



    function ValidateDays(txtVal, title) {

        if (txtVal.value != "" && parseInt(txtVal.value) > <%=DayLimit %>) {
            alert(title + ": You can only enter a miximum of <%=DayLimit%> days to extend for the financial lockdown.");
            txtVal.value = "0";
            return false;
        }

    }


//-->	
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width=750 border=0 cellpadding=0 cellspacing=0 HEIGHT=27>
	<tr> 
		<td rowspan=2></td>
		    <td rowspan=2> </td>
		<td width=100% height=19 align="right"></td>
	</tr>
	<tr> 
		<td bgcolor=#133E71> <img src="images/spacer.gif" width=53 height=1></td>
	</tr>
	<tr> 
		<td colspan=13 style="border-left:1px solid #133e71; border-right:1px solid #133e71"> <img src="images/spacer.gif" width=53 height=6></td>
	</tr>
</table>

<TABLE cellspacing=0 CELLPADDING=2 style='border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 >
	<form name="RSLFORM" method="POST" >
		<TR> 
		    <td colspan=2 > <%=lstCompany%> </td>
		    <TD HEIGHT=100% colspan=4 align="right">  </td>
	    </TR>
        <TR> 
		<td colspan=2 ></td>
		<TD HEIGHT=100% colspan=4 align="right">
		Monthly Lockdown max entension of <input type="text" class="textbox200" style='width:22px' name="txt_LIMIT" value="<%=DayLimit%>">
		Days <input type=button class=RSLButton value=' UPDATE ' onclick='SaveLimit()'  > </td>
	</TR>
	<TR> 
		<TD HEIGHT=100% colspan=5>Enter the number of days you would like to set the lockdown after the selected month and click active tick box to activate this.<BR><BR></TD>
	</TR>
<%=StrRows%>

	<TR> 
		<TD HEIGHT=100% colspan=5><div name="messages" id="messages"></div></TD>
	</TR>
	</FORM>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=svrFrame width=400px height=400px style='display:block'>
</BODY>
</HTML>