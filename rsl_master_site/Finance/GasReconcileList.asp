<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (2)	 'USED BY CODE
	Dim DatabaseFields (2)	 'USED BY CODE
	Dim ColumnWidths   (2)	 'USED BY CODE
	Dim TDSTUFF        (2)	 'USED BY CODE
	Dim TDPrepared	   (2)	 'USED BY CODE
	Dim ColData        (2)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (2)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (2)	 'All Array sizes must match	
	Dim TDFunc		   (2)

	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Batch ID|BATCHID|90"
	SortASC(0) 	= "INV.BATCHID ASC"
	SortDESC(0) = "INV.BATCHID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "BatchNumber(|)"

	'ColData(1)  = "CONTRACT NAME|CONTRACTNAME|130"
	'SortASC(1) 	= "CONTRACTNAME ASC"
	'SortDESC(1) = "CONTRACTNAME DESC"	
	'TDSTUFF(1)  = ""
	'TDFunc(1) = ""		

	ColData(1)  = "ORGNAME|ORGNAME|175"
	SortASC(1) 	= "ORGNAME ASC"
	SortDESC(1) = "ORGNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "BATCHVALUE|BATCHVALUE|110"
	SortASC(2) 	= "BATCHVALUE ASC"
	SortDESC(2) = "BATCHVALUE DESC"	
	TDSTUFF(2)  = " ""align='right'"" "
	TDFunc(2) = "FormatCurrency(|)"
	
	
	PageName = "GasReconcileList.asp"
	EmptyText = "No Relevant Batch Id's found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" TITLE="""""" & rsSet(""BATCHID"") & """""" ONCLICK=""""load_me("" & rsSet(""BATCHID"") & "","" & rsSet(""SID"") & "")"""" """ 
    RowClickColumn = " "" TITLE="""""" & rsSet(""BATCHID"") & """""" ONCLICK=""""load_me("" & rsSet(""BATCHID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	BatchIdFilter = ""
	if (Request("BatchId") <> "") then
		if (isNumeric(Request("BatchId"))) then
			BatchIdFilter = " AND INV.BATCHID = '" & CLng(Request("BatchId")) & "' "
		end if
	end if
	
	Dim PostBack
	
	'Defines the selection of reconcile status
	Dim ReconcileSelect,UnReconcileSelected,AllSelected
	
	PostBack=Request("post_back")
	
	
	if(PostBack<>"") then
	    
	    SupFilter = ""
	    SupplierId=Request("sel_SUPPLIER")
	    if (SupplierId <> "") then
		    SupFilter = " AND SCOPE.ORGID = '" & SupplierId & "' "
	    end if
	
        ReconcileFilter = ""
	    if (Request("sel_reconcile_status") <> "") then
	        if(Request("sel_reconcile_status")="0") then
    	    	ReconcileFilter = " AND ISNULL(UNREC.UNREC_COUNT,0)>=1 "
    	    	UnReconcileSelected="selected=selected"
	        Elseif(Request("sel_reconcile_status")="1") then	
	            ReconcileFilter = " AND ISNULL(REC.REC_COUNT,0)>=1 AND ISNULL(UNREC.UNREC_COUNT,0)=0 "
	            ReconcileSelect="selected=selected"
	        End If
        Else
	        ReconcileFilter=""
	        AllSelected="selected=selected"
	    end if
	else
	    ReconcileFilter = "	AND ISNULL(UNREC.UNREC_COUNT,0)>=1 "
	    SupplierId=1270
		SupFilter = " AND SCOPE.ORGID = '" & SupplierId & "' "   
		UnReconcileSelected="selected=selected"
        
	end if
	
	
				
	
	SQLCODE= "  SELECT INV.BATCHID,ORG.[NAME] AS ORGNAME,SUM(ISNULL(COSTPERAPPLIANCE,0)) AS BATCHVALUE  " &_ 
		     "	FROM GS_INVOICE INV " &_ 
		     "	    INNER JOIN C_REPAIR REPAIR ON REPAIR.REPAIRHISTORYID= INV.REPAIRHISTORYID " &_ 
		     "	    INNER JOIN S_SCOPE SCOPE ON SCOPE.SCOPEID=REPAIR.SCOPEID " &_ 
		     "	    INNER JOIN S_ORGANISATION ORG ON ORG.ORGID=SCOPE.ORGID " &_ 
		     "      LEFT JOIN (SELECT BATCHID,COUNT(ISNULL(RECONCILED,0))AS UNREC_COUNT   FROM GS_INVOICE WHERE ISNULL(RECONCILED,0)=0  GROUP BY BATCHID) UNREC ON UNREC.BATCHID=INV.BATCHID " &_
		     "      LEFT JOIN (SELECT BATCHID,COUNT(ISNULL(RECONCILED,0)) AS  REC_COUNT FROM GS_INVOICE WHERE ISNULL(RECONCILED,0)=1  GROUP BY BATCHID) REC ON REC.BATCHID=INV.BATCHID "&_
		     "  WHERE 1 = 1 " & SupFilter & BatchIdFilter & ReconcileFilter  &_
		     " GROUP BY INV.BATCHID,ORG.[NAME] " &_	
		     " Order By " + Replace(orderBy, "'", "''") + ""

        
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	

	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", SupplierId, NULL, "textbox200", " style='width:200px'")	
	CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
        <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
        <title>RSL Manager Finance --> Purchase Order List</title>
        <link rel="stylesheet" href="/css/RSL.css" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    </head>
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">
    <!--
            function load_me(Batch_id)
            {
	            location.href = "GasReconcileConfirm.asp?BatchID=" + Batch_id + "&CurrentPage=<%=intpage%>";
	        }

            function RemoveBad() 
            { 
               strTemp = event.srcElement.value;
               strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
               event.srcElement.value = strTemp;
            }

            function setSupplier()
            {
	          thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
	        }

            function SubmitPage()
            {
	            if (isNaN(thisForm.BatchId.value))
	            {
		            alert("When searching for a Batch Id you only need to enter the last identifiable digits.\nFor example to search for the Batch Id 'IDO00000345' enter '345'.")
		            return false;
		        }
            	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value + "&BatchId=" + thisForm.BatchId.value +"&sel_reconcile_status=" + thisForm.sel_reconcile_status.value +"&post_back=1" 
	        }
    // -->
    </script>
    <!-- End Preload Script -->
    <body style="background-color:#FFFFFF;margin:10px 0px 0px 10px;"  onload="initSwipeMenu(3);preloadImages();" onunload="macGo()">
        <!--#include virtual="Includes/Tops/BodyTop.asp" -->  
        <form action="" id="thisForm" name="thisForm" method="post">
            <p style="margin:5px 0px 5px 5px;">Click on the batch that you would like to reconcile. </p>
            <table class="RSLBlack">
                <tr>
                    <td><b>&nbsp;QUICK FIND Facility</b></td>
                    <td><input type="text" name="BatchId" class="RSLBlack" value="<%=Server.HTMLEncode(Request("BatchId"))%>" onblur="RemoveBad()" style='width:135px;'/></td>
                    <td><%=lstSuppliers%></td>
                    <td>    
                        <select id="sel_reconcile_status" class="textbox200" style="width:150px">
                            <option <%=AllSelected%> value="" >All</option>   
                            <option <%=UnReconcileSelected%> value="0" >Not Reconciled</option>
                            <option <%=ReconcileSelect%> value="1" >Reconciled</option>  
                        </select> 
                    </td>
                    <td><input type="button" class="RSLButton" value="Update Search" onclick="SubmitPage()"/></td>
                </tr>
            </table>
            <table style="width:75px;border-width:0px"  cellpadding="0" cellspacing="0">
	            <tr><td>&nbsp;</td></tr>
	            <tr>
		            <td style="background-color:#133E71;" ><img alt="" src="images/spacer.gif" width="750" height="1"/></td>
	            </tr>
            </table>
            <%=TheFullTable%>
        </form>
        <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
        <iframe id="frm_team" name="frm_team" width="400px" height="400px" style='display:none'></iframe>
    </body>
</html>