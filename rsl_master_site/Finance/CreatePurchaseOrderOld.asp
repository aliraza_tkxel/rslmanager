<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

Dim lstApprovedBy
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
		"WHERE (CC.ACTIVE = 1) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1)" &_
		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND "

Call BuildSelect(lstCostCentres, "sel_CostCentre", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' style='width:265px'")
Call BuildSelect(lstDevelopments, "sel_DEVELOPMENT", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_developmentid, NULL, "textbox200", " style='width:265px'" )
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:265px'")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' style='width:265px'")

CloseDB()
	
%>
<%

		
%>
<HTML>
<HEAD>
<title>RSL Manager Finance - Create Purchase Order</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language='JavaScript' defer>
<!--
var FormFields = new Array()

FormFields[0] = "sel_CostCentre|Cost Centre|SELECT|Y"
FormFields[1] = "sel_HEADID|Head|SELECT|Y"
FormFields[2] = "sel_Expenditure|Expenditure|SELECT|Y"
FormFields[3] = "sel_SUPPLIER|Supplier|SELECT|Y"
FormFields[4] = "txt_ITEMREF|Item Ref|TEXT|Y"
FormFields[5] = "txt_ITEMDESC|Item Description|TEXT|N"
FormFields[6] = "txt_PODATE|Order Date|DATE|Y"
FormFields[7] = "txt_QUOTEDCOST|Gross Cost|CURRENCY|Y"
FormFields[8] = "txt_DELDATE|Delivery Date|DATE|N"
FormFields[9] = "txt_ORDEREDBY|Ordered By|TEXT|Y"
FormFields[10] = "txt_DelDetail|Deleivery Details|TEXT|N"
FormFields[11] = "txt_VAT|VAT|CURRENCY|Y"
FormFields[12] = "txt_NETCOST|Net Cost|CURRENCY|Y"

		function TotalBoth(){
		event.srcElement.style.textAlign = "right"
		if (!checkForm(true)) {
			document.getElementById("txt_QUOTEDCOST").value = ""
			return false
			}
		document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))
		}

	function SetVat(){
		if (document.getElementById("sel_VATTYPE").value == 0 || document.getElementById("sel_VATTYPE").value == 3){
			document.getElementById("txt_VAT").value = "0.00"
			document.getElementById("txt_VAT").readOnly = true
			}
		else if (document.getElementById("sel_VATTYPE").value == 1){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(20)
			}
		else if (document.getElementById("sel_VATTYPE").value == 2){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(5)
			}			
		document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
		}

	function AddVAT(val){
		if (isNumeric("txt_NETCOST", "")){
			//VAT = FormatCurrency(parseFloat(document.getElementById("txt_NETCOST").value) /100 * val)
			VAT = new Number (document.getElementById("txt_NETCOST").value /100 * val)
			VAT = round(round(VAT,4),2)
			document.getElementById("txt_VAT").value = VAT
			document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
			}
		}

	function round(number,X) {
	// rounds number to X decimal places, defaults to 2
		X = (!X ? 2 : X);
		return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
	}


	function ResetVAT(){
		if (document.getElementById("sel_VATTYPE").value == 1) AddVAT(20)
		if (document.getElementById("sel_VATTYPE").value == 2) AddVAT(5)		
		}		


	function populatelistbox(values, thetext, which){
	values = values.replace(/\"/g, "");
	thetext = thetext.replace(/\"/g, "");
	values = values.split(";;");
	thetext = thetext.split(";;");
		if (which == 2)
			var selectlist = document.forms.thisform.sel_HEADID;
		else 
			var selectlist = document.forms.thisform.sel_Expenditure;	
			selectlist.length = 0;
	
			for (i=0; i<thetext.length;i++){
				var oOption = document.createElement("OPTION");
	
				oOption.text=thetext[i];
				oOption.value=values[i];
				selectlist.options[selectlist.length]= oOption;
				}
	}
	
	function Select_OnchangeFund(){
		doc = document.all;
		populatelistbox("", "Waiting for Data...", 2);
		doc["maxValueHead"].value = "0.00";				
		doc["txt_EMLIMITS"].value = "0.00";						
		populatelistbox("", "Please Select a Head...", 1);
		doc["maxValue"].value = "0.00";
		thisform.whatAction.value = "gethead";
		thisform.action = "ServerSide/GetHeads.asp";
		thisform.target = "POFrame";
		thisform.submit();
		}
	
	function Select_OnchangeHead(){
		doc = document.all;
		doc["maxValue"].value = "0.00";
		doc["txt_EMLIMITS"].value = "0.00";								
		populatelistbox("", "Waiting for Data...", 1);	
		thisform.whatAction.value = "change";
		thisform.action = "ServerSide/GetExpenditures.asp";
		thisform.target = "POFrame";
		thisform.submit();
		}
	
	function setRemaining(){
	doc = document.all;
	eIndex = doc["sel_Expenditure"].selectedIndex;
	eAmounts = doc["theremainingamounts"].value;
	eAmounts = eAmounts.split(";;");
	eEMLIMITS = doc["theEmployeeLimits"].value;
	eEMLIMITS = eEMLIMITS.split(";;");

	doc["maxValue"].value = FormatCurrency(eAmounts[eIndex]);
	doc["txt_EMLIMITS"].value = FormatCurrency(eEMLIMITS[eIndex]);	
	}
	
	function dosubmit(){
		if (!checkForm()) return false		
		doc = document.all;		
		if (thisform.sel_Expenditure.value != "") {
			if (parseFloat(doc["txt_QUOTEDCOST"].value,10) > parseFloat(doc["maxValue"].value,10)) {
				alert("The Gross Cost is more expensive than the balance remaining in the selected expenditure.\n Please decrease the Gross Cost to continue...");
				return false;
				}
			}
		else {
			if (parseFloat(doc["txt_QUOTEDCOST"].value,10) > parseFloat(doc["maxValueHead"].value,10)) {
				alert("The Gross Cost is more expensive than the balance remaining in the selected head.\n Please decrease the Gross Cost to continue...");
				return false;
				}
			}

		QuePO = ""
		if (parseFloat(doc["txt_QUOTEDCOST"].value,10) > parseFloat(doc["txt_EMLIMITS"].value,10)) {
			result = confirm("The cost of this purchase is greater than your spending limit.\nThe purchase will be placed in a queue for an employee with a greater spending limit to authorize.\nDo you wish to continue?\nClick on 'OK' to continue.\nClick on 'CANCEL' to cancel.");
			if (!result) return false;
			QuePO = "?QUE=1"
			}
		
		thisform.whatAction.value = "recordPO";
		thisform.action = "ServerSide/CreatePurchaseOrder_svr.asp" + QuePO;
		thisform.target = "";
		thisform.submit();
		}

//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="3" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                    <tr> 
                      <td colspan="2" valign="top" class="RSLBlack"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr class="RSLBlack"> 
                            <td width="98%" valign="top" align=center>                 
                              <form name="thisform" method="post" action="/iagFinance/Purchasing.asp">
                                <table width="95%" border="0" cellspacing="0" cellpadding="0" height="25">
                                  <tr> 
                                    <td colspan=5 class="RSLBlack">
									<br>
									<b><u>Create Purchase Order</u></b>
									<br>1. To create a purchase order select all appropriate fields and then click on '<b>Create Purchase Order</b>'.
				                    <br>
                    <br>
                  </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="2"> 
                                      
                    <table border="0" cellpadding="1" cellspacing="0" class="RSLBlack">
                      <tr> 
                        <td>Cost Centre :</td>
                        <td> <%=lstCostCentres%> </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_CostCentre"></td>
						<td colspan=2>YOUR SPENDING LIMIT: � <input type="text" name="txt_EMLIMITS" class="RSLBlack" style='text-Align:top;border:none' readonly></td>
                      </tr>
                      <tr> 
                        <td>Head :</td>
                        <td> 
                          <select id="sel_HEADID" name="sel_HEADID" class="RSLBlack" style='width:265px' onchange="Select_OnchangeHead()">
                            <option value="">Please Select a Cost Centre...</option>
                          </select>
                        </td>
                        <td nowrap><img src="/js/FVS.gif" width=15 height=15 name="img_HEADID">&nbsp;&nbsp;</td>
                        <td class="RSLBlack" nowrap colspan=2>
                          Balance remaining in selected head : � 
                          <input type=text name=maxValueHead class="RSLBlack" style='text-Align:top;border:none' readonly>
                        </td>						
                      </tr>
                      <td>Expenditure :</td>
                      <td> 
                        <select id="sel_Expenditure" name="sel_Expenditure" class="RSLBlack" style='width:265px' onchange="setRemaining()">
                          <option value="">Please select a Head...</option>
                        </select>
                      </td>
                      <td><img src="/js/FVS.gif" width=15 height=15 name="img_Expenditure"></td>
                        <td  class="RSLBlack" nowrap colspan=2>
                          Balance remaining in expenditure : � 
                          <input type=text name=maxValue class="RSLBlack" style='text-Align:top;border:none' readonly>
                        </td>					  
                      </tr>
                      <tr class="RSLBlack"> 
                        <td colspan=5 valign=middle> 
                          <hr size="1">
                        </td>
                      </tr>
                      <tr> 
                        <td>SUPPLIER :</td>
                        <td valign="top"> <%=lstSuppliers%> </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_SUPPLIER"></td>
                        <td nowrap rowspan=8 valign=top>Description&nbsp;:<br>
                          <textarea class="RSLBlack" name=txt_ITEMDESC cols=55 rows=5 maxlength=300 style='overflow:hidden;border:1px solid #133E71;background-color:beige'></textarea>						
							<br>Delivery Info :<br>
                          <textarea name="txt_DelDetail" cols=55 rows=5 class="RSLBlack"  style='overflow:hidden;border:1px solid #133E71;background-color:beige'></textarea>						  
					  </td>
                        <td rowspan=4><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMDESC"></td>
                      </tr>
                      <tr> 
                        <td>Item :</td>
                        <td> 
                          <input name="txt_ITEMREF" type="text" class="RSLBlack" maxlength="50" size=50>
                        </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMREF"></td>
                      </tr>
                      <tr> 
                        <td>Order Date :</td>
                        <td> 
                          <input name="txt_PODATE" type="text" class="RSLBlack" size=50 value="<%=FormatDateTime(Date,2)%>">
                        </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_PODATE"></td>
                      </tr>
                      <tr> 
                        <td nowrap>Delivery Date :</td>
                        <td> 
                          <input name="txt_DELDATE" type="text" class="RSLBlack" size=50>
                        </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_DELDATE"></td>
                      </tr>
                      <tr> 
                        <td>Ordered By :</td>
                        <td> 
                          <input name="txt_NAMEORDEREDBY" type="text" class="RSLBlack" value ="<%= Session("FirstName") & " " & Session("LastName")%>" readonly size=50 style='border:1px solid black'>
                          <input name="txt_ORDEREDBY" type="hidden" value ="<%= session("UserID")%>">
                        </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_ORDEREDBY"></td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_DelDetail"></td>
					  </tr>
					  <tr><td>Development :</td><td><%=lstDevelopments%></td><td></td></tr>					  
					   <tr><td colspan=4><br></td></tr>
                       <tr> 
									<td>Net Cost :</td>
									<td align=right> 
									  <input style='text-Align:right' name="txt_NETCOST" type="text"  class="RSLBlack"  maxlength=49 size=50  onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()">
									</td>
									<td><img src="/js/FVS.gif" width=15 height=15 name="img_NETCOST"></td>						
								  </tr>
								  <tr> 
									<td>VAT Type :</td>
									<td align=right> 
										<%=lstVAT%> 
									</td>
									<td><img src="/js/FVS.gif" width=15 height=15 name="img_VATYPE"></td>
									<td></td>						
								  </tr>
								  <tr> 
									<td >VAT :</td>
									<td align=right> 
									  <input style='text-Align:right' name="txt_VAT" type="text"  class="RSLBlack"   maxlength=49 size=50  onBlur="TotalBoth()" onFocus="alignLeft()" readonly value="0">
									</td>
									<td><img src="/js/FVS.gif" width=15 height=15 name="img_VAT"></td>
									<td></td>						
								  </tr>
								  <tr>
								  <td class="RSLBlack">Gross :</td>
								  
                        <td align=right>
	                          <input name="txt_QUOTEDCOST" type="text" class="RSLBlack" size=50 maxlength=20 style='text-align:right;border:1px solid #133E71;background-color:beige' readonly>
                        </td>
									<td><img src="/js/FVS.gif" width=15 height=15 name="img_QUOTEDCOST"></td>
								  </tr>
					  <!--tr> 
                        <td>Gross Cost :</td>
                        <td> 
                          <input name="txt_QUOTEDCOST" type="text" class="RSLBlack" onBlur="checkForm()" onFocus="alignLeft()" size=50 maxlength=20 style='text-align:right;border:1px solid #133E71;background-color:beige'>
                        </td>
                        <td><img src="/js/FVS.gif" width=15 height=15 name="img_QUOTEDCOST"></td>
					</tr-->
					<tr>
                        <td align=right colspan=3> 
                          <input name="whatAction" type='hidden' value="change" readOnly>
                          <input type=hidden name=theremainingamounts>
                          <input type=hidden name=theEmployeeLimits>						  
                          <input name=thebutton type='button' class="RSLButton" value="Create Purchase Order" onClick="dosubmit()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          </td>												
                      </tr>
                      <tr> 
                      </tr>					  
                    </table>
                                    </td>
                                  </tr>
                                </table>
                                </form>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=POFrame style='display:none'></iframe> 
</BODY>
</HTML>

