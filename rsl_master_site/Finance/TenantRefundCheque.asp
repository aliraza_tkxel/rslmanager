<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	
	CONST CONST_PAGESIZE = 19
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	ColData(0)  = "Tenancy|TENANCYID|80"
	SortASC(0) 	= "T.TENANCYID ASC"
	SortDESC(0) = "T.TENANCYID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "TenancyReference(|)"

	ColData(1)  = "Name|FULLNAME|250"
	SortASC(1) 	= "FIRSTNAME ASC, LASTNAME ASC"
	SortDESC(1) = "FIRSTNAME DESC, LASTNAME DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "Amount|PAYMENTAMOUNT|100"
	SortASC(2) 	= "PAYMENTAMOUNT ASC"
	SortDESC(2) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(2)  = " ""align='right'"" "
	TDFunc(2) = "FormatCurrency(|)"		

	ColData(3)  = "Count|TOTALPO|40"
	SortASC(3) 	= ""
	SortDESC(3) = ""	
	TDSTUFF(3)  = " ""align='right'"" "
	TDFunc(3) = ""

	if (Request("VIEW") = 1) then
		ColData(4)  = "Cheque Number|EMPTY|100"
		SortASC(4) 	= ""
		SortDESC(4) = ""	
		TDSTUFF(4)  = " "" onclick=""""CancelBubble()"""" align=RIGHT>"" & rsSet(""INPUTBOX"") & ""<b"" "
		TDFunc(4) = ""
	ELSE
		ColData(4)  = "Cheque Number|EMPTY|100"
		SortASC(4) 	= ""
		SortDESC(4) = ""	
		TDSTUFF(4)  = " "" onclick=""""CancelBubble()"""" style='background-color:white' align=left>"" & rsSet(""INPUTBOX"") & ""<b"" "
		TDFunc(4) = ""
	END IF

	ColData(5)  = "|EMPTY|50"
	SortASC(5) 	= ""
	SortDESC(5) = ""	
	TDSTUFF(5)  = " "" onclick=""""CancelBubble()"""" style='background-color:white' align=center>"" & rsSet(""CHECKBOX"") & ""<b"" "
	TDFunc(5) = ""
	
	PageName = "TenantRefundCheque.asp"
	EmptyText = "No Tenant Refunds found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PODate") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("PODate")),2)
		RequiredDate = CDate(Request("PODate"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ORIGINATORNUMBER = "658670"
	
'	Response.Write ProcessingDate & "<BR>"
'	Response.Write RequiredDate & "<BR>"
'	Response.Write Date & "<BR>"	
	
	if (Request("VIEW") = 1) then
		SQLCODE = "SELECT '' AS EMPTY, T.TENANCYID, ISNULL(SUM(AMOUNT),0) AS PAYMENTAMOUNT, REPLACE(ISNULL(G.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME,   " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(TR.JOURNALID) AS TOTALPO, " &_
				"TB.CHEQUENUMBER AS INPUTBOX, " &_
				"'' AS CHECKBOX " &_				
				"FROM F_TENANTREFUNDS TR " &_
				"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = TR.TENANCYID " &_				
				"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = TR.CUSTOMERID " &_												
				"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_													
				"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
				"	WHERE TR.STATUS > 0 AND RJ.PAYMENTTYPE = 24 AND CONVERT(DATETIME,CONVERT(VARCHAR,TB.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY T.TENANCYID, TR.CUSTOMERID, G.DESCRIPTION, C.FIRSTNAME, C.LASTNAME, " &_
				"		TB.CHEQUENUMBER	 " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""				
	else
		SQLCODE = "SELECT '' AS EMPTY, T.TENANCYID, ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, REPLACE(ISNULL(G.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME, " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(TR.JOURNALID) AS TOTALPO, " &_
				"'<input type=""text"" name=""txt_TEXT' + CAST(T.TENANCYID AS VARCHAR) + '---' + CAST(TR.CUSTOMERID AS VARCHAR) + '"" value="""" CLASS=""TEXTBOX200"" MAXLENGTH=20><img src=""/js/FVS.gif"" width=15 height=15 name=""img_TEXT' + CAST(T.TENANCYID AS VARCHAR) + '---' + CAST(TR.CUSTOMERID AS VARCHAR) + '"">' AS INPUTBOX, " &_
				"'<input type=""HIDDEN"" name=""hid_CO' + CAST(T.TENANCYID AS VARCHAR) + '---' + CAST(TR.CUSTOMERID AS VARCHAR) + '"" value=""' + CAST(COUNT(TR.JOURNALID) AS VARCHAR) + '""><input type=""HIDDEN"" name=""hid_VA' + CAST(T.TENANCYID AS VARCHAR) + '---' + CAST(TR.CUSTOMERID AS VARCHAR) + '"" value=""' + CAST(ISNULL(SUM(RJ.AMOUNT),0) AS VARCHAR) + '""><input type=""CHECKBOX"" name=""CHECKITEMS"" value=""' + CAST(T.TENANCYID AS VARCHAR) + '---' + CAST(TR.CUSTOMERID AS VARCHAR) + '"">' AS CHECKBOX " &_				
				"FROM F_TENANTREFUNDS TR " &_
				"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = TR.TENANCYID " &_				
				"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = TR.CUSTOMERID " &_								
				"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_									
				"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
				"	WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.PAYMENTTYPE = 24 AND CONVERT(DATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY T.TENANCYID, TR.CUSTOMERID, G.DESCRIPTION, C.FIRSTNAME, C.LASTNAME " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""				
	end if	
	'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Tenant Refunds List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function CancelBubble(){
	event.cancelBubble = true
	}

var FormFields = new Array()

function SendCheques() {
	chkitems = document.getElementsByName("CHECKITEMS")
	FormFields.length = 0
	iCounter = 0
	TotalValue = 0
	TotalCount = 0
	if (chkitems.length){
		for (j=0; j<chkitems.length; j++) {
			ManualError ("img_TEXT" + chkitems[j].value, "", 3)
			}
			
		for (i=0; i<chkitems.length; i++) {
			if (chkitems[i].checked) {
				FormFields[iCounter] = "txt_TEXT" + chkitems[i].value + "|Cheque Number|TEXT|Y"
				TotalCount += parseInt(document.getElementById("hid_CO" + chkitems[i].value).value)
				TotalValue += parseFloat(document.getElementById("hid_VA" + chkitems[i].value).value)
				iCounter ++
				}
			}
		}
	if (TotalCount == 0) {
		alert("No lines have been selected. To select a line click on the\nrespective checkbox and enter a cheque number next to\nit. Otherwise the row will not be processed.")
		return false
		} 
		
	if (!checkForm()) return false
	result = confirm("You have selected '" + TotalCount + "' Tenant Refund(s) to be processed at a total value of '�" + FormatCurrency(TotalValue) +"'.\nDo you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")
	if (!result) return false
	thisForm.action = "ServerSide/TenantRefundList_Cheque.asp?pROCESSINGdATE=<%=Request("PODate")%>"
	thisForm.submit()
	}

<% if (Request("ERR97" & Replace(Date, "/", "")) = 1) then %>
alert ("Some entries were not processed as more Tenant Refunds have been entered on to the system.\nPlease check the data and re-process as required.")
<% end if %>
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table width=750 class="RSLBlack"><tr>
<td> <b>&nbsp;Tenant Refund Cheque List</b>
<input type=HIDDEN name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>">
</td>
<td align=right>
&nbsp<a href='TenantRefundDisplay.asp?date=<%=ProcessingDate%>'><font color=blue><b>BACK to Tenant Refund Calendar</b></font></a>
</td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
<% if (Request("VIEW") <> 1) then %>
<table width=750><tr><td align=right>&nbsp;<input class="RSLButton" type=button onclick="SendCheques()" value=" AUTHORIZE "></td></tr></table>
<% END IF %>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>