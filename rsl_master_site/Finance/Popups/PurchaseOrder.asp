<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<link href="../../css/Finance/Popups/PurchaseOrder/PurchaseOrder.css" rel="stylesheet" type="text/css" />
<%
Dim L_ORDERID, SUPPLIER_EXPPAYDATE, PO_VISIBLE,noOfItems

L_ORDERID = ""
TEMP_ORDERID = "" 
PO_VISIBLE = true
PO_ENCRYPTED = false 
noOfItems=0
if(Request.QueryString("OrderID") <> "") then L_ORDERID = Request.QueryString("OrderID")
TEMP_ORDERID = Request.QueryString("OrderID")

if(Request.QueryString("Encrypted") <> "") then PO_ENCRYPTED = CBool(Request.QueryString("Encrypted"))
if PO_ENCRYPTED = true then 
    L_ORDERID = Decode(TEMP_ORDERID)
else
    L_ORDERID = TEMP_ORDERID
end if


if(Request.QueryString("IncludePO") <> "") then PO_VISIBLE = Request.QueryString("IncludePO")

OpenDB()

SQL = "SELECT  BL.BLOCKNAME, SCH.SCHEMENAME, PO.ORDERID, WO.WOID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, PO.PONOTES + ',</br> ' + PI.ITEMDESC AS PONOTES, EXPPODATE, " &_
		"ISNULL(S.NAME, 'SUPPLIER NOT ASSIGNED') AS SUPPLIER, S.PAYMENTTERMS, S.ADDRESS1, S.ADDRESS2, S.ADDRESS3, S.TOWNCITY, S.POSTCODE, S.COUNTY, S.TELEPHONE1, S.TELEPHONE2, S.FAX, " &_
		"C.FIRSTNAME + ' ' + C.LASTNAME AS CUSTOMERNAME, P.PROPERTYID, P.FLATNUMBER, P.HOUSENUMBER, P.ADDRESS1 AS PADDRESS1, P.ADDRESS2 AS PADDRESS2, P.ADDRESS3 AS PADDRESS3, P.TOWNCITY AS PTOWNCITY, P.POSTCODE AS PPOSTCODE, P.COUNTY AS PCOUNTY, ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '') AS ORDEREDBY, " &_		
		"D.DEVELOPMENTNAME, WO.BLOCKID, WO.SCHEMEID, BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost, COM.DESCRIPTION AS COMPANY FROM F_PURCHASEORDER PO " &_
        "LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " &_
		"LEFT JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_			
		"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = WO.CUSTOMERID " &_								
		"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_
        "LEFT JOIN P_BLOCK BL ON BL.BLOCKID IN(PO.BLOCKID, WO.BLOCKID,P.BLOCKID) " &_
        "LEFT JOIN P_SCHEME SCH ON SCH.DEVELOPMENTID IN(PO.DEVELOPMENTID, WO.DEVELOPMENTID,P.DEVELOPMENTID) OR SCH.SchemeId IN (PO.SchemeId, WO.SchemeId,P.SCHEMEID) " &_
        "LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = SCH.DEVELOPMENTID " &_
        "LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
        "LEFT JOIN G_COMPANY COM ON COM.COMPANYID = PO.COMPANYID " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"INNER JOIN F_PURCHASEITEM PI ON PI.ORDERID = PO.ORDERID " &_
		"WHERE PO.ACTIVE = 1 AND PO.ORDERID = " & L_ORDERID

SQLMAX = "SELECT MAX(PROCESSDATE) AS LASTPAYMENTDATE FROM F_POBACS PB " &_
			"INNER JOIN F_INVOICE INV ON INV.INVOICEID = PB.INVOICEID " &_
			"WHERE INV.ORDERID = " & L_ORDERID
LASTPAYMENT = ""
Call OpenRs(rsLAST, SQLMAX)
if (NOT rsLAST.EOF) then
	LASTPAYMENT = rsLAST("LASTPAYMENTDATE")
end if
Call CloseRs(rsLAST)

Dim WOID, PROPERTYID
	
Call OpenRs(rsPP, SQL)
if (NOT rsPP.EOF) then
	ORDERID = rsPP("ORDERID")
    PONUMBER = ""

    if PO_VISIBLE = true then 
        PONUMBER = PurchaseNumber(rsPP("ORDERID"))
    end if
	
    PONAME = rsPP("PONAME")
	PODATE = FormatDateTime(rsPP("PODATE"),1)
	if (rsPP("EXPPODATE") <> "" AND NOT isNULL(rsPP("EXPPODATE"))) then
		EXPPODATE = FormatDateTime(rsPP("EXPPODATE"),1)
	end if
	SUPPLIER = rsPP("NAME")
	SUPPLIERID = rsPP("SUPPLIERID")	
	POSTATUSNAME = rsPP("POSTATUSNAME")
	DEVELOPMENT = rsPP("DEVELOPMENTNAME")
	BLOCK = rsPP("BLOCKNAME")
    SchemeName = rsPP("SCHEMENAME")
    BTC = rsPP("BTC")
    BCount = rsPP("BCount")
    BPost = rsPP("BPost")
	PROPERTYID = rsPP("PROPERTYID")
    BLOCKID = rsPP("BLOCKID")
    SCHEMEID = rsPP("SCHEMEID")
	PONOTES = rsPP("PONOTES")
	ORDEREDBY = rsPP("ORDEREDBY")
    WOID = rsPP("WOID")
    COMPANY = rsPP("COMPANY")




    ' ADDING BLOCKID IF SCHEMEID IS AVAILABLE TO GET DETAILS OF ADDRESS
    
	' THIS IS FOR THE WORKING OF EXPECTED PAYMENTDATE
	SUPPLIER_EXPPAYDATE = rsPP("PAYMENTTERMS")	
	
	AddressArray = Array("Supplier", "Address1", "Address2", "Address3", "TownCity", "PostCode", "County")
	AddressString = ""
	For i=0 to ubound(AddressArray)
		if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			AddressString = AddressString & rsPP(AddressArray(i)) & "<br>"
		end if
	next
	AddressString = AddressString & "<br>"
	
	ContactArray = Array("Telephone1", "Telephone2", "Fax")
	ContactString = ""
	For i=0 to ubound(ContactArray)
		if (rsPP(ContactArray(i)) <> "" AND NOT ISNULL(rsPP(ContactArray(i)))) THEN
			ContactString = ContactString & "<i>" & ContactArray(i) & ":</i> " & rsPP(ContactArray(i)) & "<br>"
		end if
	next
	if (ContactString <> "") then
		ContactString = ContactString & "<br>"
	end if

	'if a repair get the property address

    Dim rsForPropertyAddress
    rsForPropertyAddress = rsPP
    '//Getting Scheme Name for Scheme POs
     
    if (NOT ISNULL(PROPERTYID) and( StrComp(UCase(PONAME),"SCHEME/BLOCK REACTIVE REPAIR WORK ORDER")<>0 OR StrComp(UCase(PONAME),"SCHEME BLOCK PURCHASE ORDER")<>0 )) then
		'THIS SECTION WILL GET THE PROPERTY ADDRESS
		
		PAddressArray = Array("Housenumber","PAddress1", "PAddress2", "PAddress3", "PTownCity", "PPostCode", "PCounty")		
		PAddressString = ""
		For i=0 to ubound(PAddressArray)
			if (rsPP(PAddressArray(i)) <> "" AND NOT ISNULL(rsPP(PAddressArray(i)))) THEN
                If PAddressArray(i) = "Housenumber" THEN
                    PAddressString = PAddressString & rsPP(PAddressArray(i)) & ","
                Else
				    PAddressString = PAddressString & rsPP(PAddressArray(i)) & "<br/>"
                End If
			end if
		next
		PAddressString = PAddressString & "<br/>"
	end if
	
    If ( ( StrComp(UCase(PONAME),"SCHEME/BLOCK REACTIVE REPAIR WORK ORDER")=0 ) OR ( StrComp(UCase(PONAME),"CYCLICAL WORKS ORDER")=0) OR (StrComp(UCase(PONAME),"SCHEME BLOCK PURCHASE ORDER")=0 ) and Not IsNull(ORDERID) ) Then
        
        '------------------ GETTING SCHEME ID AND DEVELOPMENT ----------------
        if(ISNULL(SCHEMEID)) THEN
				
            SchemePOSQL =  "Select PS.SCHEMENAME,PS.SCHEMEID as sid,PA_I.ItemName, PD.DEVELOPMENTNAME as pdev from CM_ContractorWork CW " &_
							"JOIN CM_ServiceItems SI ON  CW.ServiceItemId = SI.ServiceItemId "&_
						"Left JOIN PA_ITEM PA_I ON PA_I.ItemId = si.ItemId "&_
						" JOIN P_SCHEME PS ON ps.SchemeId = si.SCHEMEID "&_
                        "left join PDR_Development PD on PS.DEVELOPMENTID = PD.DEVELOPMENTID "&_
						" Where CW.PurchaseOrderId ="&ORDERID
             Call OpenRs(rsSchemeName, SchemePOSQL)
			 'Response.write(SchemePOSQL)
			 'response.end
			 'response.flush
             If (NOT rsSchemeName.EOF) Then
                 'SchemeName = rsSchemeName("SCHEMENAME")
                 SCHEMEID = rsSchemeName("sid")
                 DEVELOPMENT = rsSchemeName("pdev")
				 ITEMNAME =rsSchemeName("ItemName")
             End if
        ELSE
            SchemePOSQL = " Select PS.SCHEMENAME,PS.SCHEMEID as sid , PD.DEVELOPMENTNAME as pdev from P_SCHEME PS " &_
            " left join PDR_Development PD on PS.DEVELOPMENTID = PD.DEVELOPMENTID " &_
            "Where SCHEMEID = "&SCHEMEID
            Call OpenRs(rsSchemeName, SchemePOSQL)
             If (NOT rsSchemeName.EOF) Then
                 'SchemeName = rsSchemeName("SCHEMENAME")
                 SCHEMEID = rsSchemeName("sid")
                 DEVELOPMENT = rsSchemeName("pdev")
             End if
        END IF

        '-------------------------- GETTING BLOCK ID AND ADDRESS
        if((ISNULL(BLOCKID) OR BLOCKID < 1 ) AND (NOT ISNULL(SCHEMEID))) then

        SQL = "select top 1 BL.blockid as blId,BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost from P_BLOCK as BL where schemeid=" &SCHEMEID
        Call OpenRs(rsPP, SQL)
        if (NOT rsPP.EOF) then
            BLOCKID = rsPP("blId")
            BTC = rsPP("BTC")
            BCount = rsPP("BCount")
            BPost = rsPP("BPost")
        end if
        AddressArray = Array( "BTC", "BPost","BCount")
	    PAddressString = ""
	    CountSQL = "select count(BL.blockid)as Total from P_BLOCK as BL where schemeid=" &SCHEMEID 'get count of blocks associated with a scheme
        Call OpenRs(rsRows, CountSQL)
	    For i=0 to ubound(AddressArray)
		    if (rsRows("Total")>0)THEN 
			    PAddressString = PAddressString & rsPP(AddressArray(i)) & "<br>"
		    end if
	    next
        PAddressString = PAddressString & "<br>"
        
        ELSE 
            AddressArray = Array( "BTC", "BPost","BCount")
	        PAddressString = ""
	        For i=0 to ubound(AddressArray)
		        if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			        PAddressString = PAddressString & rsPP(AddressArray(i)) & "<br>"
		        end if
	        next
            PAddressString = PAddressString & "<br>"
        END IF

    End If
    

else
	Response.Redirect ("Purchase Order NOt Found Page.asp")
end if
Call CloseRs(rsPP)

SQL = "SELECT CWD.CycleDate,PI.ORDERITEMID, PI.PAIDON, PI.ITEMNAME, INV.TAXDATE, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, " &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE, ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, '') AS APPROVEDBY, C.DESCRIPTION AS PURCHASEORDER, H.DESCRIPTION AS PURCHASEHEAD, EX.DESCRIPTION AS PURCHASEEXPEND " &_ 
        "FROM F_PURCHASEITEM PI " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
        "INNER JOIN F_HEAD H ON EX.HEADID = H.HEADID " &_
		"INNER JOIN F_COSTCENTRE C ON C.COSTCENTREID = H.COSTCENTREID " &_	
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
		"left join CM_ContractorWorkDetail CWD on CWD.PurchaseOrderItemId = PI.ORDERITEMID "&_
		"LEFT JOIN F_ORDERITEM_TO_INVOICE OTOI ON OTOI.ORDERITEMID = PI.ORDERITEMID " &_
		"LEFT JOIN F_INVOICE INV ON INV.INVOICEID = OTOI.INVOICEID " &_
        "LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PI.APPROVED_BY " &_
		"WHERE PI.ACTIVE = 1 AND pi.ORDERID = " & L_ORDERID & " ORDER BY PI.ORDERITEMID DESC"

Call OpenRs(rsPI, SQL)	
noOfItems = 0	
while (NOT rsPI.EOF) 

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
    PURCHASEORDER = rsPI("PURCHASEORDER")
    PURCHASEHEAD  = rsPI("PURCHASEHEAD")
    PURCHASEEXPEND = rsPI("PURCHASEEXPEND")

	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
    ApprovedBy = rsPI("APPROVEDBY")
	if(NetCost>0 and GrossCost>0 ) then
	    PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT width=360>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center title='" & rsPI("VATNAME") & "' nowrap width=40>" & rsPI("VATCODE") & "</TD><TD nowrap width=151>" & ApprovedBy & "</TD><TD nowrap width=80>" & FormatNumber(NetCost,2) & "</TD><TD nowrap width=75>" & FormatNumber(VAT,2) & "</TD><TD nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5></TD></TR>"
	    ITEMDESC = rsPI("ITEMDESC")
		CycleDate = rsPI("CycleDate")
		if (CycleDate <>"") then
		 PIString = PIString & "<TR id=""PIDESCS" & noOfItems & """ STYLE='DISPLAY:NONE'><TD colspan=5>" & PURCHASEORDER & " > " & PURCHASEHEAD & " > " &  PURCHASEEXPEND & "<br /><I>" & rsPI("ITEMDESC") & "</I><br /><I>Cycle Date: "&rsPI("CycleDate")&"</I></TD></TR>"
	    elseif (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
		    PIString = PIString & "<TR id=""PIDESCS" & noOfItems & """ STYLE='DISPLAY:NONE'><TD colspan=5>" & PURCHASEORDER & " > " & PURCHASEHEAD & " > " &  PURCHASEEXPEND & "<br /><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"
        else
    	    PIString = PIString & "<TR id=""PIDESCS" & noOfItems & """ STYLE='DISPLAY:NONE'><TD colspan=5>" & PURCHASEORDER & " > " & PURCHASEHEAD & " > " &  PURCHASEEXPEND & "</TD></TR>"
	    end if
        noOfItems = noOfItems + 1
    end if	
	rsPI.moveNext
wend
CloseRs(rsPI)

'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
SQL = "SELECT INV.*,THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE INV  WHERE INV.ORDERID = " & ORDERID
SQL = "SELECT  INV.*,THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END, " &_
		" PI.PAIDON, ISNULL(PI.NETCOST,0.00) AS NETCOST, ISNULL(PI.VAT,0.00) AS VAT, ISNULL(PI.GROSSCOST ,0.00) AS GROSSCOST" &_
		" FROM F_INVOICE INV " &_
		"	LEFT JOIN F_ORDERITEM_TO_INVOICE OT ON OT.INVOICEID = INV.INVOICEID " &_
		"	LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = OT.ORDERITEMID " &_
		" WHERE INV.ORDERID = " & L_ORDERID & " AND INV.NETCOST<>0 AND INV.GROSSCOST<>0"

Call OpenRs(rsINV, SQL)
if (NOT rsINV.EOF) then
	iTGC = 0
	iTNC = 0
	iTVA = 0
'	showTotal = 0
	InvoiceString = "<br><TABLE WIDTH=791 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
			"<TD WIDTH=150 HEIGHT=20 nowrap><b>Invoice No:</b></TD><TD nowrap width=134><b>Tax Date:</b></TD><TD nowrap width=134><b>Due Date:</b></TD><TD nowrap width=133><b>Pay Date:</b></TD><TD nowrap align=right width=80><b>Net (�):</b></TD><TD nowrap align=right width=75><b>VAT (�):</b></TD><TD nowrap align=right width=80><b>Gross (�):</b></TD><TD WIDTH=5></TD></TR>"
	while NOT rsINV.EOF
		TaxDate = rsINV("TAXDATE")
		duedate = DateAdd("d",SUPPLIER_EXPPAYDATE,TaxDate)
		paymentdate = rsINV("PAIDON")
		iVAT = rsINV("VAT")
		iNetCost = rsINV("NETCOST")
		iGrossCost = rsINV("GROSSCOST")
		iTGC = iTGC + iGrossCost
		iTNC = iTNC + iNetCost
		iTVA = iTVA + iVAT
		'if(iNetCost>0 and iGrossCost>0 ) then
		 InvoiceString = InvoiceString & "<TR><TD WIDTH=150 nowrap>" & rsINV("InvoiceNumber") & "</TD><TD nowrap width=134>" & rsINV("TAXDATE") & "</TD></TD><TD nowrap width=134>" & DueDate & "</TD><TD nowrap width=133>" & Paymentdate & "</TD><TD ALIGN=RIGHT nowrap width=80>" & FormatNumber(iNetCost,2) & "</TD><TD ALIGN=RIGHT nowrap width=75>" & FormatNumber(iVAT,2) & "</TD><TD ALIGN=RIGHT nowrap width=80>" & FormatNumber(iGrossCost,2) & "</TD><TD WIDTH=5></TD></TR>"
		' showTotal = 1
		'end if
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</TABLE>"
   'if(showTotal = 1) then
    InvoiceString = InvoiceString & "<TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=791>"
	InvoiceString = InvoiceString & "<TR bgcolor=steelblue ALIGN=RIGHT> "
	  InvoiceString = InvoiceString & "<TD height=20 width=400 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>"
      InvoiceString = InvoiceString & "<TD width=151 bgcolor=white nowrap></TD>"
	  InvoiceString = InvoiceString & "<TD width=80 bgcolor=white nowrap><b>" & FormatNumber(iTNC,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=75 bgcolor=white nowrap><b>" & FormatNumber(iTVA,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=80 bgcolor=white nowrap><b>" & FormatNumber(iTGC,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=5 bgcolor=white></TD>"
	 InvoiceString = InvoiceString & "</TR>"
     InvoiceString = InvoiceString & "</TABLE>"
  'end if
end if
Call CloseRs(rsINV)

CloseDB()
%>
<HTML>
<HEAD>
<title>&nbsp;</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

    function PrintThePP() {
        document.getElementById("PrintButton").style.visibility = "hidden";
        document.getElementById("SHOWDESC").style.visibility = "hidden";
        ReturnBack = 0
        if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
            ToggleDescs()
            ReturnBack = 1
        }
        window.print();
        if (ReturnBack == 1) ToggleDescs()
        document.getElementById("PrintButton").style.visibility = "visible";
        document.getElementById("SHOWDESC").style.visibility = "visible";
    }

    function ToggleDescs(noOfItems) {
	
        if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS") {
            NewText = "HIDE DESCRIPTIONS"
            NewStatus = "block"
        }
        else {
            NewText = "SHOW DESCRIPTIONS"
            NewStatus = "none"
        }
        for (a = 0; a < noOfItems; a++) {
            var pItemId = "PIDESCS" + a
            document.getElementById(pItemId).style.display = NewStatus
            iDesc = document.getElementById(pItemId)
            if (iDesc.length) {
            for (i = 0; i < iDesc.length; i++)
                iDesc[i].style.display = NewStatus
        }
        }
        document.getElementById("SHOWDESC").innerHTML = NewText
    }			
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" CLASS='TA' style="background-image:none">
<table cellspacing=0 cellpadding=0 width="791" ALIGN=CENTER>
  <tr><td width=10></td><td>

      <TABLE BORDER=0 STYLE='BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=791>
        <TR width="100%"> 
          <TD width="40%" nowrap valign=top height=100> <b>Supplier Details</b><br>
            <%=AddressString%> <%=ContactString%> </TD>
          <TD width="40%" nowrap valign=top height=100> 
            
          </TD>
          <TD ALIGN=RIGHT VALIGN=TOP width="20%"><IMG SRC="/myImages/Broadland_Purchase_Image.gif" width="134" height="106"></TD>
        </TR>
      </TABLE>
<BR>
<div class="purchase-order-container">
<div class='left'>
    <div>Order Name:&nbsp;&nbsp;&nbsp;<span><%=PONAME%></span></div>
	<% IF ItemName <> "" AND NOT ISNULL(ITEMNAME) THEN %> 
        <div>Attribute:&nbsp;&nbsp;&nbsp;&nbsp;<span><%=ITEMNAME%></span></div>
    <% END IF %>   
    <div>Order No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=PONUMBER%></span></div>
	<% IF POSTATUSNAME = "Invoice Received - In Dispute" THEN %>
    <div style="color: red;">Status:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=POSTATUSNAME%></span>
	<img style='cursor:pointer; width:25' src='../../IMAGES/flag .png' title='Currently Disputed';></div>
	<% Else %>
	<div>Status:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=POSTATUSNAME%></span></div>
	<% END IF %>
    <% IF DEVELOPMENT <> "" AND (NOT ISNULL(DEVELOPMENT)) THEN %>
        <div>Development:&nbsp;<span><%=DEVELOPMENT%></span></div>
    <% END IF %>
    <% IF SchemeName <> "" AND NOT ISNULL(SchemeName) THEN %> 
        <div>Scheme:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=SchemeName%></span></div>
    <% END IF %>  
    <% IF BLOCK <> "" AND NOT ISNULL(BLOCK) THEN %> 
        <div>Block:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><%=BLOCK%></span></div>
    <% END IF %>  
 
	
    <% IF (NOT ISNULL(PROPERTYID) OR NOT ISNULL(BLOCKID)) THEN %>
            <div>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="vertical-align:top; display:inline-block;">
            <%=PAddressString%></span></div>
    <% END IF %>
</div>

<div class='right'>
    <div>
        <table>
            <tr><td>Company:</td><td><%=COMPANY%></td></tr>
            <% if (ApprovedBy <> "") then %>
            <tr><td>Approved By:</td><td><%=ApprovedBy%></td></tr>
            <% else %>
            <tr><td>Approved By:</td> <td>N/A</td></tr>
             <% end if %>
            <tr><td>Order Date:</td> <td><%=PODATE%></td></tr>
            <tr><td>Expected Date:</td> <td><%=EXPPODATE%></td></tr>
            <% if (LASTPAYMENT <> "") then %>
            <tr><td>Last Payment Date:</td> <td><%=FormatDateTime(LASTPAYMENT,2)%></td></tr>
            <% end if %>
        </table>
    </div>
  
</div>

<div class='btn-print'>
    <input type=Button id="PrintButton" name="PrintButton" class="RSLButton" value=" PRINT " onclick="PrintThePP()">
</div>


</div>




<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=791>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=360 NOWRAP><table cellspacing=0 cellpadding=0 width=356>
      <tr style='color:white'>
      <td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs(<%=noOfItems%>)"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
	  <TD width=40><b>Code:</b></TD>
      <TD width=151><b>Approved By:</b></TD>
	  <TD width=80><b>Net (�):</b></TD>
	  <TD width=75><b>VAT (�):</b></TD>
	  <TD width=80><b>Gross (�):</b></TD>
	  <TD width=5></TD>		  
	</TR>
	<%=PIString%> 
  </TABLE>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=791>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD height=20 width=400 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
      <TD width=151 bgcolor=white nowrap></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 bgcolor=white nowrap><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD width=5 bgcolor=white nowrap></TD>		  
	</TR>
  </TABLE>
<%=InvoiceString%>  

<TABLE cellspacing=0 cellpadding=2 width=791>
	<TR><TD><b>PURCHASE NOTES :</b> </TD></TR>
	<TR><TD style='border:1px solid #133e71;HEIGHT:60' height=60px valign=top width=791px><%=PONOTES%>&nbsp;</TD></TR>
</TABLE>

<div align=center style='width:791'><b>Registered Office</b> Broadland Housing Association Limited, NCFC Jarrold Stand, Carrow Road, Norwich, NR1 1HU<br />Tel: 01603 750200, Fax: 01603 750222</div>
</td></tr></table>

</BODY>
</HTML>
