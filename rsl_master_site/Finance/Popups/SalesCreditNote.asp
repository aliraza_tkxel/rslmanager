<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim L_SCNID
L_SCNID = ""
if(Request.QueryString("SCNID") <> "") then L_SCNID = Request.QueryString("SCNID")
OpenDB()

SQL = "SELECT SCN.SCNID, SO.SALEID, SCN.SCNNAME, SO.SUPPLIERID, SO.SALESCUSTOMERID, SS.SOSTATUSNAME, SCNDATE, SCNNOTES, SO.TENANCYID, SO.EMPLOYEEID " &_
		"FROM F_SALESCREDITNOTE SCN " &_
		"INNER JOIN F_SALESINVOICE SO ON SO.SALEID = SCN.INVOICEID " &_
		"INNER JOIN F_SOSTATUS SS ON SS.SOSTATUSID = SO.SOSTATUS " &_
		"WHERE SCN.SCNID = " & L_SCNID

Call OpenRsK(rsPP, SQL) 
if (NOT rsPP.EOF) then
	SCNID = rsPP("SALEID")
	SONUMBER = SaleNumber(rsPP("SALEID"))
	SCNNUMBER = SalesCreditNoteNumber(rsPP("SCNID"))
	SCNNAME = rsPP("SCNNAME")
	SCNDATE = FormatDateTime(rsPP("SCNDATE"),1)

	SOSTATUSNAME = rsPP("SOSTATUSNAME")
	SCNNOTES = rsPP("SCNNOTES")

'	SUPPLIER = rsPP("NAME")
	SUPPLIERID = rsPP("SUPPLIERID")	
	TENANCYID = rsPP("TENANCYID")
	EMPLOYEEID = rsPP("EMPLOYEEID")
	SALESCUSTOMERID = rsPP("SALESCUSTOMERID")
	
	IF NOT isNull(TENANCYID) THEN

		SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
				"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
				"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
				"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
				"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
				"WHERE  CT.ENDDATE IS NULL AND T.TENANCYID = " & TENANCYID
	
		Call OpenRS(rsCust, SQL)
		if (rsCust.EOF) then 'MOST PROBABLY A PREVIOUS TENANT, THEREFORE WE NEED TO AMEND THE SQL STATEMENT TO
			Call CloseRs(rsCust)	
			SQL = "SELECT C.CUSTOMERID, T.TENANCYID, GT.DESCRIPTION AS TITLE, C.FIRSTNAME, C.LASTNAME, P.HOUSENUMBER, P.FLATNUMBER, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.TOWNCITY, P.POSTCODE, P.COUNTY FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID AND CT.ENDDATE = T.ENDDATE " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"LEFT JOIN G_TITLE GT ON C.TITLE = GT.TITLEID " &_
					"WHERE T.TENANCYID = " & TENANCYID
			Call OpenRS(rsCust, SQL)
		End if	

		count = 1
		while NOT rsCust.EOF
			
			StringName = ""
			Title = rsCust("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsCust("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName =rsCust("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			if (count = 1) then
				StringFullName = StringName
				count = 2
			else
				StringFullName = StringFullName & "& " & StringName
			end if
			rsCust.moveNext
		wend
		if (count = 2) then
			rsCust.moveFirst()
			housenumber = rsCust("housenumber")
			if (housenumber = "" or isNull(housenumber)) then
				housenumber = rsCust("flatnumber")
			end if
			if (housenumber = "" or isNull(housenumber)) then
				FirstLineOfAddress = rsCust("Address1")
			else
				FirstLineOfAddress = housenumber & " " & rsCust("Address1")		
			end if
	
			RemainingAddressString = ""
			AddressArray = Array("ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsCust(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			tenancyref = rsCust("TENANCYID")
		end if
		
		FullAddress = "<b>TENANT : " & TenancyReference(TENANCYID) & "</b><BR>" & StringFullName & "<BR>" & "" & FirstLineOfAddress & "<BR>" &	RemainingAddressString			

	ELSEIF NOT isNull(SALESCUSTOMERID) THEN
	
		SQL = "SELECT O.SCID, O.CONTACT, O.ORGANISATION, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM F_SALESCUSTOMER O " &_
				"WHERE SCID = " & SALESCUSTOMERID
	
		Call OpenRS(rsCompany, SQL)

		IF (not rsCompany.EOF) then
			StringFullName = rsCompany("CONTACT")
		
			RemainingAddressString = ""
			AddressArray = Array("ORGANISATION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsCompany(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>SALES CUSTOMER</b> <br> " & StringFullName & "<BR>" & RemainingAddressString			
		else
			FullAddress = "<b>SALES CUSTOMER</b> <br> Details Could not be found!<br>Contact Support Team immediately."
		end if	

	ELSEIF NOT isNull(SUPPLIERID) THEN
	
		SQL = "SELECT O.ORGID, O.NAME, O.BUILDINGNAME, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY FROM S_ORGANISATION O " &_
				"WHERE ORGID = " & SUPPLIERID
	
		Call OpenRS(rsCompany, SQL)

		IF (not rsCompany.EOF) then
			StringFullName = rsCompany("NAME")
		
			RemainingAddressString = ""
			AddressArray = Array("BUILDINGNAME", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TOWNCITY", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsCompany(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>ORGANISATION</b> <br> " & StringFullName & "<BR>" & RemainingAddressString			
		else
			FullAddress = "<b>ORGANISATION</b> <br> Details Could not be found!<br>Contact Support Team immediately."
		end if	
	
	ELSEIF NOT isNull(EMPLOYEEID) THEN

		SQL = "SELECT E.EMPLOYEEID, GT.DESCRIPTION AS TITLE, E.FIRSTNAME, E.LASTNAME, P.ADDRESS1, P.ADDRESS2, P.ADDRESS3, P.POSTALTOWN, P.POSTCODE, P.COUNTY FROM E__EMPLOYEE E " &_
				"INNER JOIN E_CONTACT P ON P.EMPLOYEEID = E.EMPLOYEEID " &_
				"LEFT JOIN G_TITLE GT ON E.TITLE = GT.TITLEID " &_
				"WHERE E.EMPLOYEEID = " & EMPLOYEEID
	
		Call OpenRS(rsEmployee, SQL)
	
		count = 1
		IF NOT rsEmployee.EOF THEN
			
			StringName = ""
			Title = rsEmployee("TITLE")
			if (Title <> "" AND NOT isNull(Title)) then
				StringName = StringName & Title & " "
			end if
			FirstName = rsEmployee("FIRSTNAME")
			if (FirstName <> "" AND NOT isNull(FirstName)) then
				StringName = StringName & FirstName & " "
			end if
			LastName = rsEmployee("LASTNAME")
			if (LastName <> "" AND NOT isNull(LastName)) then
				StringName = StringName & LastName & " "
			end if
			
			RemainingAddressString = ""
			AddressArray = Array("ADDRESS1", "ADDRESS2", "ADDRESS3", "POSTALTOWN", "POSTCODE", "COUNTY")
			for i=0 to Ubound(AddressArray)
				temp = rsEmployee(AddressArray(i))
				if (temp <> "" or NOT isNull(temp)) then
					RemainingAddressString = RemainingAddressString & "" & temp & "<BR>"
				end if
			next
			FullAddress = "<b>EMPLOYEE</b><br>" & StringName & "<BR>" & RemainingAddressString					
		else
			FullAddress = "<b>EMPLOYEE</b><br>Details Could not be found!<br>Contact Support Team immediately."
		end if			
	
	ELSE
		FullAddress = "Unable to idetify account details<br>Please contact your administrator<br>or software vendor."
	END IF
	
end if
Call CloseRs(rsPP)

SQL = "SELECT SC.DESCRIPTION AS SALESCAT, SCI.ITEMNAME, SCI.ITEMDESC, SCN.SCNDATE, SCI.NETCOST, SCI.VAT, V.VATNAME, SCI.GROSSCOST, " &_
		"V.VATCODE FROM F_SALESCREDITNOTEITEM SCI " &_
		"INNER JOIN F_SALESCREDITNOTE SCN ON SCN.SCNID = SCI.SCNID " &_
		"INNER JOIN F_SALESCAT SC ON SC.SALESCATID = SCI.SALESCATID " &_
		"INNER JOIN F_VAT V ON V.VATID = SCI.VATTYPE " &_
		"WHERE SCI.SCNID = " & L_SCNID & " ORDER BY SCNITEMID DESC"

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF) 
	
	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	'turned vat display ON ------------off
	PIString = PIString & "<TR ALIGN=RIGHT valign=top><TD valign=top ALIGN=LEFT width=260>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD valign=top align=center title='" & rsPI("VATNAME") & "' nowrap width=>" & rsPI("VATCODE") & "</TD><TD valign=top nowrap width=80>" & FormatNumber(NetCost,2) & "</TD><TD valign=top nowrap width=75>" & FormatNumber(VAT,2) & "</TD><TD valign=top nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5 valign=top></TD></TR>"
	'PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT width=500>" & DataStorage & rsPI("ITEMNAME") &  " (" & rsPI("SALESCAT") & ")</TD><TD nowrap width=135>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5></TD></TR>"
	
	ITEMDESC = rsPI("ITEMDESC")
	if (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
		PIString = PIString & "<TR  id=""PIDESCS"" STYLE='DISPLAY:none' valign=top><TD valign=top colspan=3><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"
	end if
	rsPI.moveNext
wend
PIString = PIString & "<TR><TD height='100%'></TD></TR>"
CloseRs(rsPI)

CloseDB()
%>
<HTML>
<HEAD>
<title>Sales Credit Note</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

	function PrintThePP(){
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("SHOWDESC").style.visibility = "hidden";
		ReturnBack = 0
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			ToggleDescs()
			ReturnBack = 1
			}
		window.print();
		if (ReturnBack == 1) ToggleDescs()
		document.getElementById("PrintButton").style.visibility = "visible";	
		document.getElementById("SHOWDESC").style.visibility = "visible";
		}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}			
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<style type="text/css">
<!--
.style1 
{
font-size: 10px
}

body{font-family: courier, serif}
-->
</style>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" CLASS='RSLsALES' >
<table cellspacing=0 cellpadding=0 width="644" align="center">
	<tr>
		<td align="center">
			<table cellspacing=0 cellpadding=0 width="640" ALIGN=CENTER>
				  <tr >
						
						<td>
				
							<TABLE BORDER=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=640>
								<TR> 
									  <TD width=100% nowrap valign=top height=100> <b><font style='font-size:14px'>SALES CREDIT NOTE</font></b><BR><%=FullAddress%> </TD>
									  <TD ALIGN=RIGHT VALIGN=TOP width=134><IMG SRC="/myImages/LOGOLETTER.gif" width='145' height='113'></TD>
								</TR>
							</TABLE>
							<BR>
							<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=640PX>
								<TR><TD WIDTH=90PX>Name:</TD><TD><%=SCNNAME%></TD>
								</TR>
								<TR><TD WIDTH=90PX>Credit Note:</TD><TD><%=SCNNUMBER%></TD></TR>
								<TR><TD WIDTH=90PX>Invoice No:</TD><TD><%=SONUMBER%></TD></TR>
								<TR><TD>Tax Date:</TD><TD><%=SCNDATE%></TD><TD rowspan=2 valign=bottom align=right><input type=Button name="PrintButton" class="RSLButton" value=" PRINT " onClick="PrintThePP()"></td></TR>
								<!--<TR><TD>Status:</TD><TD><%=SOSTATUSNAME%></TD></TR>-->
							</TABLE>
							<BR>
						  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=640 height="530" >
							<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white' valign="top"> 
							  <TD height=20 ALIGN=LEFT width=340 NOWRAP valign="top"><table cellspacing=0 cellpadding=0 width=340><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onClick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
						<!--turned  on-->	
							 <TD valign="top" width=><b>Code:</b></TD>
							  <TD valign="top" width=><b>Net (�):</b></TD>
							  <TD valign="top" width=><b>VAT (�):</b></TD>
						 
							  <TD valign="top" width=135><b>Amount (�):</b></TD>
							  <TD valign="top" width=5></TD>		  
							</TR>
							<%=PIString%> 
						  </TABLE>
						  <TABLE width="640" cellpadding=2 cellspacing=0 STYLE='BORDER:1PX SOLID BLACK;border-top:none' >
							<TR bgcolor=steelblue ALIGN=RIGHT> 
							  <TD height=20 width=339 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
						
							  <TD width=114 bgcolor=white nowrap><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
							  <TD width=74 bgcolor=white nowrap><b><%=FormatNumber(TotalVAT,2)%></b></TD>
							  
							  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
							  <TD width=15 bgcolor=white nowrap></TD>		  
							</TR>
						  </TABLE>
						   <%=InvoiceString%>  
				
						 <TABLE cellspacing=0 cellpadding=2 width=640>
							<TR><TD><b>Notes :</b> </TD></TR>
							<TR><TD style='border:1px solid #133e71;HEIGHT:60' height=60px valign=top width=640px><%=SONOTES%>&nbsp;</TD></TR>
						 </TABLE>
					</td>
				</tr>
				<tfoot>
				<tr  valign="bottom">
					
					<td valign="bottom" align="center">
						<div align=center style='width:644'><b>Registered Office</b> Broadland Housing Association Limited, NCFC Jarrold Stand, Carrow Road, Norwich, NR1 1HU<br />Tel: 01603 750200, Fax: 01603 750222<br />VAT No. 927 5127 17</div>
					</td>
				</tr>
				   
				</tfoot>
			</table>
		</td>
	</tr>
</table>
</BODY>
</HTML>

