<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim theamounts, EmployeeLimits, IsShowDeletePI, IsReconciledOrPaid, IsDisableVatDropDown
	Dim optionvalues, optiontext
	Dim optionvalues2, optiontext2
    Dim optionvalues3, optiontext3
	Dim HEADID, PITYPE
    Dim SaveStatus

	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
    SaveStatus = 0
	OpenDB()

	Call GetCurrentYear()
	FY = GetCurrent_YRange
			
    OrderID = REQUEST("OrderID")
	IPAGE = REQUEST("CURRENTPAGE")
	BATCHID=REQUEST("BatchID")
	
	IF(BATCHID="") then
	    BATCHID=-1
	END IF    

    SQL = "SELECT SchemeId, BLOCKID FROM F_PURCHASEORDER " &_
            " WHERE ORDERID =" & OrderID
	Call OpenRs(rsPO, SQL)
    POSchemeId = rsPO("SchemeId")
    POBlockId  = rsPO("BlockId")
    Call CloseRs(rsPO)
   	
	GetDropDownStuff()

	exclude_sql = ""
	
	DISTINCT_SQL = "F_COSTCENTRE CC " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
			"WHERE EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )" &_
			exclude_sql

    Scheme_SQL = "P_SCHEME"
			
Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
Call BuildSelect(lstScheme, "sel_SCHEME", Scheme_SQL, "SCHEMEID, SCHEMENAME", "SCHEMENAME", "Please Select...", POSchemeId, NULL, "textbox200", " onchange='Select_OnchangeScheme()' tabindex=4")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()'  tabindex=5")
	
	CloseDB()

	Function GetDropDownStuff()
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"		
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If
		
		count2 = 0
		optionvalues2 = ""
		optiontext2 = "Please Select..."
		
		if (count2 = 0) Then
			optionvalues2 = ""
			optiontext2 = "No Heads Are Setup..."
		End If

		count3 = 0 
		optionvalues3 = ""
		optiontext3 = "Please Select a Scheme..."
		If(POSchemeId) Then
            optiontext3 = "Please Select..."
            SQL =  "SELECT BLOCKID, BLOCKNAME FROM P_BLOCK " &_
			      "WHERE SchemeId = " & POSchemeId		

		    Call OpenRs(rsBlock, SQL)

		    While (NOT rsBlock.EOF)
			    theText = rsBlock.Fields.Item("BLOCKNAME").Value
			    theText = UCase(Left(theText,1)) & Mid(theText, 3, Len(theText)-1)
			
			    optionvalues3 = optionvalues3 & ";;" & rsBlock.Fields.Item("BLOCKID").Value
			    optiontext3 = optiontext3 & ";;" & theText
			
			      count3 = count3 + 1
			      rsBlock.MoveNext()
		    Wend
		    If (rsBlock.CursorType > 0) Then
		      rsBlock.MoveFirst
		    Else
		      rsBlock.Requery
		    End If
		    if (count3 = 0) Then
			    optionvalues3 = ""
			    optiontext3 = "No Block Are Setup..."
		    End If

		    CloseRs(rsBlock)
        End If
				
	End Function

%>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript" src="/js/financial.js"></script>
<script language="JAVASCRIPT">
    
    var FormFields = new Array()
	
    function SetChecking(val){
        FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
        FormFields[1] = "sel_HEAD|Head|SELECT|Y"
        FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
        FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
        FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
        FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
        FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
        FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
        //FormFields[8] = "sel_SCHEME|Scheme|SELECT|Y"
        //FormFields[9] = "sel_BLOCK|Block|SELECT|Y"
    }

    function LOADNOW(){
        PopulateListBox("<%=optionvalues3%>", "<%=optiontext3%>", 3);			
        THISFORM.sel_BLOCK.value = "<%=POBlockId%>"	
    }

    function PopulateListBox(values, thetext, which){       
        values = values.replace(/\"/g, "");
        thetext = thetext.replace(/\"/g, "");
        values = values.split(";;");
        thetext = thetext.split(";;");
        if(which != undefined){
            if (which == 2)
                var selectlist = document.forms.THISFORM.sel_HEAD;
            else if(which == 1)
                var selectlist = document.forms.THISFORM.sel_EXPENDITURE;	
            else
                var selectlist = document.forms.THISFORM.sel_BLOCK;
            selectlist.length = 0;

            for (i=0; i<thetext.length;i++){
                var oOption = document.createElement("OPTION");

                oOption.text=thetext[i];
                oOption.value=values[i];
                selectlist.options[selectlist.length]= oOption;
            }
        }
    }
	
    function SetPurchaseLimits() {
        eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
        eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
        eAmounts = eAmounts.split(";;");
        eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
        eEMLIMITS = eEMLIMITS.split(";;");

        document.getElementById("txt_EXPBALANCE").value = FormatCurrency(eAmounts[eIndex]);
        document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);
    }

    function Select_OnchangeFund(){
        document.getElementById("txt_EMLIMITS").value = "0.00";						
        document.getElementById("txt_EXPBALANCE").value = "0.00";
        PopulateListBox("", "Waiting for Data...", 2);
        PopulateListBox("", "Please Select a Head...", 1);
        THISFORM.IACTION.value = "gethead";
        THISFORM.action = "../ServerSide/GetHeads.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    }

    function Select_OnchangeScheme(){
        PopulateListBox("", "Waiting for Data...", 3);
        THISFORM.IACTION.value = "getBlock";
        THISFORM.action = "../ServerSide/GetBlocks.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    }
	
    function Select_OnchangeHead() {
        document.getElementById("txt_EXPBALANCE").value = "0.00";
        document.getElementById("txt_EMLIMITS").value = "0.00";
        PopulateListBox("", "Waiting for Data...", 1);
        document.getElementById("IACTION").value = "change";
        document.THISFORM.action = "../ServerSide/GetExpendituresForAmend.asp";
        document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        document.THISFORM.submit();
    }

    function InsertNewPurchaseItem(){
        SetChecking(1)		
        if (!checkForm()) return false

        GrossCost = document.getElementById("txt_GROSSCOST").value
        EmployeeLimit = document.getElementById("txt_EMLIMITS").value
        ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value
        SaveStatus = 1

        //MAKE SURE PEOPLE DONT ENTER A NEGATIVE FIGURE...
        if (parseFloat(GrossCost) < 0) {
            alert("You have entered an item with a negative value. Please correct this to continue.")
            return false;
        }

        //check the expenditure will not go over...
        ExpLeft = document.getElementById("txt_EXPBALANCE").value
        if ((parseFloat(ExpLeft) < parseFloat(GrossCost))) {
            answer = confirm("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget remaining (�" + FormatCurrencyComma(ExpLeft) + ") for the selected item.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");
            if (!answer) return false
        }

        //check the employee limit						
        if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)) {
            SaveStatus = 0
            result = confirm("The item cost is greater than your employee limit (�" + FormatCurrencyComma(EmployeeLimit) + ") for the selected item budget.\nIf you continue then the item will be placed in a queue to be authorised\nby a user who has appropriate limits.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
            if (!result) return false
        }

        document.getElementById("hd_SaveStatus").value = SaveStatus;

        THISFORM.action = "../ServerSide/AddOrderItem.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();	
    }

    function parentReload() {
        opener.location.reload();
        window.close();
    }
	
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="LOADNOW()">
    <table width="100%" cellspacing="5">
        <tr>
            <td rowspan="3">&nbsp;</td>
            <td>
                <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td height="7"></td>
                    </tr>
                    <tr>
                        <td valign="top" height="20">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="2" width="79">
                                        <img src="../Images/tab_amendorderitem.gif" width="139" height="20"></td>
                                    <td height="19"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#133E71">
                                        <img src="images/spacer.gif" height="1"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71; border-bottom: 1px solid #133E71'>
                            <iframe name="PURCHASEFRAME<%=TIMESTAMP%>" style='display: NONE'></iframe>
                            <form id="" name="THISFORM" method="POST">
                                <table cellspacing="0" cellpadding="3">

                                    <tr bgcolor="steelblue" style='color: white'>
                                        <td colspan="9"><b>&nbsp; </b></td>
                                    </tr>
                                    <tr>
                                        <td>Item : </td>
                                        <td>
                                            <input id="txt_ITEMREF" <%=IsReconciledOrPaid%> name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex="3" value="<%=ITEMREF%>"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_ITEMREF" name="img_ITEMREF"></td>

                                    </tr>
                                    <tr>
                                        <td valign="top">Notes : </td>
                                        <td>
                                            <textarea tabindex="3" <%=IsReconciledOrPaid%> id="txt_ITEMDESC" name="txt_ITEMDESC" rows="7" class="TEXTBOX200" style='overflow: hidden; border: 1px solid #133E71'><%=ITEMDESC%></textarea></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_ITEMDESC" name="img_ITEMDESC"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Scheme : </td>
                                        <td>
                                            <%=lstScheme%>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_SCHEME" id="img_SCHEME"></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Block : </td>
                                        <td>
                                            <select id="sel_BLOCK" name="sel_BLOCK" class="TEXTBOX200" tabindex="4">
                                                <option value="">Please Select a Scheme...</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_BLOCK" id="img_BLOCK">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Cost Centre : </td>
                                        <td><%=lstCostCentres%></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRE" id="img_COSTCENTRE"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Head : </td>
                                        <td>
                                            <select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()" tabindex="4">
                                                <option value="">Please Select a Cost Centre...</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_HEAD">
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;Expenditure : </td>
                                        <td>
                                            <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()" tabindex="4">
                                                <option value="">Please select a Head...</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_EXPENDITURE" id="img_EXPENDITURE">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap>&nbsp;Exp Balance : </td>
                                        <td>
                                            <input type="text" id="txt_EXPBALANCE" name="txt_EXPBALANCE" class="textbox200" tabindex="-1" readonly></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Spend Limit : </td>
                                        <td>
                                            <input type="text" name="txt_EMLIMITS" id="txt_EMLIMITS" class="textbox200" readonly tabindex="-1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Net Cost : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> id="txt_NETCOST" name="txt_NETCOST" type="text" class="textbox200" maxlength="20" size="11" onblur="TotalBoth();ResetVAT()" onfocus="alignLeft()" tabindex="5" value="<%=FormatNumber(NETCOST,2,-1,0,0)%>">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_NETCOST" name="img_NETCOST"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Vat Type : </td>
                                        <td>
                                            <div id="divId">
                                                <%=lstVAT%>
                                            </div>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_VATTYPE" id="img_VATTYPE"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;VAT : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> name="txt_VAT" type="text" class="textbox200" maxlength="20" size="11" onblur="TotalBoth()" onfocus="alignLeft()" value="<%=FormatNumber(VAT,2,-1,0,0)%>" tabindex="5" id="txt_VAT"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_VAT" id="img_VAT"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Total : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> id="txt_GROSSCOST" name="txt_GROSSCOST" type="text" class="textbox200" maxlength="20" size="11" readonly tabindex="-1" value="<%=FormatNumber(GROSSCOST,2,-1,0,0)%>"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST"></td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2">
                                            <input type="HIDDEN" name="IACTION" id="IACTION">
                                            <input type="HIDDEN" name="ISQUEUED" id="ISQUEUED" value="0">
                                            <input type="HIDDEN" name="IPAGE" id="IPAGE" value="<%=IPAGE%>">
                                            <input type="HIDDEN" name="ORDERID" id="ORDERID" value="<%=ORDERID%>">
                                            <input type="HIDDEN" name="BATCHID" id="BATCHID" value="<%=BATCHID%>">
                                            <input type="HIDDEN" name="hd_SaveStatus" id="hd_SaveStatus">
                                            <input type="HIDDEN" name="EXPENDITURE_LEFT_LIST" id="EXPENDITURE_LEFT_LIST" value="<%=theamounts%>">
                                            <input type="HIDDEN" name="EMPLOYEE_LIMIT_LIST" id="EMPLOYEE_LIMIT_LIST" value="<%=EmployeeLimits%>">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td nowrap>
                                                        <input type="button" name="ResetButton" value=" CANCEL " class="RSLButton" onclick="window.close()" tabindex="6">&nbsp;</td>
                                                    <td>
                                                        <input type="button" name="AmendButton" value=" Add " class="RSLButton" onclick="InsertNewPurchaseItem()" tabindex="6">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
