<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
if (Request("txt_TENANCY") <> "" AND ISNUMERIC(Request("txt_TENANCY"))) then

	SQL = "SELECT ISNULL(G.DESCRIPTION,'N/A') AS TITLE, C.FIRSTNAME, C.LASTNAME, CT.STARTDATE, ISNULL(CONVERT(VARCHAR,CT.ENDDATE,103), 'Current Tenant') AS ENDDATE FROM C__CUSTOMER C " &_
				"INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C.CUSTOMERID " &_
				"LEFT JOIN G_TITLE G ON C.TITLE = G.TITLEID " &_
				"WHERE CT.TENANCYID = " & Replace(Request("txt_TENANCY"), "'", "''")  & " ORDER BY CT.ENDDATE"
	Call OpenDB()
	Call OpenRS(rsC, SQL)
	TD = ""
	if (NOT rsC.EOF) then
		while NOT rsC.EOF
			TD = TD & "<TR><TD>" & rsC("TITLE") & "</TD><TD>" & rsC("FIRSTNAME") & "</TD><TD>" & rsC("LASTNAME") & "</TD><TD>" & rsC("STARTDATE") & "</TD><TD>" & rsC("ENDDATE") & "</TD></TR>"
			rsC.moveNext
		wend	
	ELSE
		TD = "<TR><TD COLSPAN=5 ALIGN=CENTER>NO TENANTS FOUND FOR TENANT NUMBER: " & Request("txt_TENANCY") & "</TD></TR>"
	END IF
	Call CloseRs(rsC)
	Call CloseDB()
else
	TD = "<TR><TD COLSPAN=5 ALIGN=CENTER>PLEASE ENTER A VALID TENANCY</TD></TR>"
end if

%>
<html>
<head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<title>Tenancy List</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000">
<table>
<form name="RSLFORM" method="POST">
<tr><TD><b>TENANCY HISTORY&nbsp;&nbsp;&nbsp;</b></TD><td>
	<input type==text name="txt_TENANCY" class="textbox" value="<%=Request("txt_TENANCY")%>">
</td><td>
	<input type=submit value=" UPDATE " class="RSLButton">
</td></tr>
</form>
</table>
<TABLE CELLSPACING=0 CELLPADDING=3 STYLE='BORDER-COLlAPSE:COLLAPSE' BORDER=1 width=500>
<tr style='font-weight:bold'><td width=50>Title</td><td>Firstname</td><td>Lastname</td><td width=90>Start date</td><td width=90>End date</td></tr>
<%=TD%>
</TABLE>

</body>
</html>
