<%@  language="VBScript" codepage="65001" %>
<% 
Response.Expires = -1
Server.ScriptTimeout = 600
%>
<% Response.Buffer = true %>
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include file="../../freeaspupload.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
    Call OpenDB()
	Dim cnt, current_status, str_po_detail, status_sql, theURL, poname, OrderID, rsSet,Upload, cnt2, rsSet2, user_id
	Dim mypage, orderBy, str_table_bottom, isDisabled, invoiceNo,taxDate,OrderItemID, isChild, updateParent
    Dim uploadsDirVar
	Dim grossCost
    uploadsDirVar = Server.MapPath("/Invoice_Images/")
	
    'Don't know why we are checking the Form Method type when we are using REQUEST object    
	if Request.ServerVariables("REQUEST_METHOD") <> "POST" then
        
      '//Getting Purchase Item name
        if(Request("OrderName")<>"") then 
            itemName = " : "&Request("OrderName")
        end if
        'Amount check
        if(Request("amount")>0 and Request("amount")<=9) then
            amount = Request("amount")&".00"
        else
           amount = Request("amount")
        End if

        OrderID = Request("OrderID")

        '//Checking if last child item then update parent as well
        Session("OrderID") = Request("OrderID")
            
        '//Checking Uploading Request
        call build_po_detail()
    end if

    Function build_po_detail()
						
        'To check if invoice is already attached then disable invoice upload (update) button.
        strSQL = " SELECT PONAME, ORDERID, POSTATUSNAME, USERID, ISNULL(POL.InvoiceAttached,0) InvoiceAttached FROM F_PURCHASEORDER PO LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID " &_
                 " OUTER APPLY (SELECT TOP 1 1 As InvoiceAttached FROM F_PURCHASEORDER_LOG iPOL WHERE iPOL.ORDERID = PO.ORDERID AND image_url IS NOT NULL ORDER BY POTIMESTAMP DESC) POL " &_
                 " WHERE ORDERID = " & OrderID
        
        ' strSQL = " SELECT PO.PONAME, PS.POSTATUSNAME, PO.USERID FROM F_PURCHASEITEM PI " &_
                 ' " INNER JOIN F_PURCHASEORDER PO ON PI.ORDERID = PO.ORDERID " &_                 
                 ' " LEFT JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID " &_                 
                 ' " WHERE PI.ORDERITEMID = " & OrderItemID 
        
        Call OpenRs (rsSet, strSQL) 
        rsSet.MoveFirst()
        
		str_po_detail = ""
        current_status = rsSet("POSTATUSNAME")
        user_id = rsSet("USERID")

        'To check if invoice is already attached then disable update button.
        'isInvoiceAttached = CBool(rsSet("InvoiceAttached"))        
        'if Not isInvoiceAttached then

        if current_status = "Goods Received" or current_status = "Goods Approved" or current_status = "Invoice Received" or current_status = "Invoice Approved" or current_status = "Work Completed" or current_status = "Part Paid" then 
            isDisabled = ""
        else
            isDisabled = "disabled"
        end if
        poname = rsSet("PONAME")
		
		 strSQL="select sum(PI.GrossCost)as GROSSCOST from F_PURCHASEITEM PI  where PI.OrderId= "&OrderID
		 Call OpenRs (rsSet, strSQL) 
		 grossCost=rsSet("GROSSCOST")
		 
		Call CloseRs(rsSet)
				
	End Function

    Call CloseDB()

	Function currentPageURL()
         dim s ,protocol, port

         if Request.ServerVariables("HTTPS") = "on" then 
           s = "s"
         else 
           s = ""
         end if  
 
         protocol = strleft(LCase(Request.ServerVariables("SERVER_PROTOCOL")), "/") & s 

         if Request.ServerVariables("SERVER_PORT") = "80" OR Request.ServerVariables("SERVER_PORT") = "443" then
           port = ""
         else
           port = ":" & Request.ServerVariables("SERVER_PORT")
         end if  

         currentPageURL = protocol & "://" & Request.ServerVariables("SERVER_NAME") & port
                      '& Request.ServerVariables("SCRIPT_NAME")
    End Function
	
    Function strLeft(str1,str2)
            strLeft = Left(str1,InStr(str1,str2)-1)
    End Function

%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager -- > Suppilers Module</title>
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 0px 0px 0px 0px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../../css/RSL.css" type="text/css" />
    <script type="text/javascript" src="/js/general.js"></script>
	<script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script src="../../js/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function showInvoiceUploadTab() {
            $('.po-history-data').toggleClass('hidden');
            $('.update-postatus').toggleClass('hidden');
            $('.po-table-header').toggleClass('hidden');
        }

        function update_status(self) {
            debugger;
            console.log($(self).val());
            self.disabled = "disabled";

            var btn_txt = $(self).val();
            if (btn_txt == "Save Changes") {
                var po_status = $(".postatus_select option:selected").val();
                var notes = $(".update-postatus-text textarea").val();
                var order_id = $(".order-id").text();
                $('.po-status-form').submit();
            }

            $(self).val(btn_txt == "Update" ? "Save Changes" : "Update");
            $('.po-history-data').toggleClass('hidden');
            $('.update-postatus').toggleClass('hidden');
            $('.po-table-header').toggleClass('hidden');
        }
        //Disabling uploading button

        function changeState(btnName,grossCost) {
            //debugger;
            SetChecking()
            if (!checkForm()) return false
            var invoiceAmount = document.getElementById("txt_InvoiceAmount").value;
            var invoiceNo = document.getElementById("txt_InvoiceNo").value;
            
            var isInvoiceExist = document.getElementById("hid_is_invoice_exist").value;
            
            if(invoiceAmount!=grossCost)
			{
				var r = confirm("Invoice amount is different then actual cost. Do you want to re-queued this order!");
				if (r == true)
				{
				    document.frmSend.action="../iFrames/iOrderStateChecking.asp?invoiceAmount=" + invoiceAmount + "&grossCost=" + grossCost + "&orderId=" + <%=Session("OrderID")%> + "&invoiceNo=" + invoiceNo + "&IsRequeuedOrder=true&IsDeleteOrder=false&IsSaveData=false"
				    document.frmSend.target="serverFrame"
				    document.frmSend.submit();
				}
            }
            else{
                document.frmSend.action="../iFrames/iOrderStateChecking.asp?invoiceAmount=" + invoiceAmount + "&grossCost=" + grossCost + "&orderId=" + <%=Session("OrderID")%> + "&invoiceNo=" + invoiceNo + "&IsRequeuedOrder=false&IsDeleteOrder=false&IsSaveData=false"
                document.frmSend.target="serverFrame"
                document.frmSend.submit();
            }
            //__doPostBack('', '');
            //btnName.disabled = true;
        }
        function checkFileUpload() {
            var totalFiles = document.getElementById("uploadImage").files.length;
            var submitbutton = document.getElementById("btnUploadInvoice");
            if (totalFiles > 0) {
                //var submitbutton = document.getElementById("btnUploadInvoice");
                document.getElementById("btnUploadInvoice").style.display = "block";
                //document.getElementById("btnSubmit").style.display = "none";
            }
            else {
                //document.getElementById("btnUploadInvoice").style.display = "none";
                //document.getElementById("btnSubmit").style.display = "block";
            }
        }
		
        function invoiceExistConfirmation(){
		    var result = confirm("Same invoice already exist/ paid. Do, you want to delete the current order!");
		    if(result == true)
		    {
			    var invoiceAmount = document.getElementById("txt_InvoiceAmount").value;
		        var invoiceNo = document.getElementById("txt_InvoiceNo").value;
		        document.frmSend.action="../iFrames/iOrderStateChecking.asp?invoiceAmount=" + invoiceAmount + "&grossCost=" + <%=grossCost%> + "&orderId=" + <%=Session("OrderID")%> + "&invoiceNo=" + invoiceNo + "&IsRequeuedOrder=false&IsDeleteOrder=true&IsSaveData=false"
		        document.frmSend.target="serverFrame"
		        document.frmSend.submit();
		    }
		}

		function parentReload()
		{
		    opener.location.reload();
		    window.close();
		}

		function saveData()
		{
		    var invoiceAmount = document.getElementById("txt_InvoiceAmount").value;
		    var invoiceNo = document.getElementById("txt_InvoiceNo").value;
		    document.frmSend.action="../iFrames/iOrderStateChecking.asp?invoiceAmount=" + invoiceAmount + "&grossCost=" + <%=grossCost%> + "&orderId=" + <%=Session("OrderID")%> + "&invoiceNo=" + invoiceNo + "&IsRequeuedOrder=false&IsDeleteOrder=false&IsSaveData=true"
		    document.frmSend.target="serverFrame"
		    document.frmSend.submit();
		}

		var FormFields = new Array()
		function SetChecking() {
		    FormFields.length = 0
		    FormFields[0] = "txt_InvoiceNo|invoiceNo|TEXT|Y"
		    FormFields[1] = "txt_TaxDate|taxDate|TEXT|Y"
		    FormFields[2] = "txt_InvoiceAmount|invoiceAmount|NUMBER|Y"
		    FormFields[3] = "txt_notes|notes|TEXT|N"
		}
    </script>
    <link href="../../css/Suppliers/iFrames/iNonRepairJournal.css" rel="stylesheet" type="text/css" />
</head>
<body class="TA" onload="showInvoiceUploadTab()">
    <table width="746" cellpadding="0" cellspacing="0" style="border-collapse: collapse;"
        slcolor='' border="0" hlcolor="STEELBLUE">
        <thead>
            <tr valign="middle" style="background-color: #f5f5dc">
                <td style="border-bottom: 1px solid" width="20" height="20">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="20" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="34" nowrap="nowrap">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.CREATIONDATE ASC')" />
                        Date
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.CREATIONDATE DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="120" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WO.WOID ASC')" />
                        Code
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WO.WOID DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid" width="270" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('FULLADDRESS ASC')" />
                        Address
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('FULLADDRESS DESC')" />
                    </b>
                </td>
                <td style="border-bottom: 1px solid;" width="142" nowrap="nowrap">
                    <b>
                        <img src="/myImages/sort_arrow_up.gif" style='cursor: pointer' border="0" alt="Sort Ascending"
                            onclick="SORTPAGE('WOSTATUSNAME ASC')" />
                        Status
                        <img src="/myImages/sort_arrow_down.gif" style='cursor: pointer' border="0" alt="Sort Descending"
                            onclick="SORTPAGE('WOSTATUSNAME DESC')" />
                    </b>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="8">
                    <div style='overflow: auto;'>
                        <table width="100%" cellpadding="0" cellspacing="0" style="behavior: url(/Includes/Tables/tablehl.htc);
                            border-collapse: collapse" slcolor='' border="0" hlcolor="STEELBLUE">
                            <thead>
                                <tr>
                                    <td colspan="4">
                                        <table cellspacing="0" cellpadding="1" width="100%">
                                            <tr valign="top">
                                                <td >
                                                    <%' Request("RefNo")%>
                                                    <b>Order No:</b>&nbsp;<span class='order-id'><%=OrderID%></span>
                                                </td>
                                                                                          
                                                <td >
                                                    <b>Order Name:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%=poname%>
												</td>
												<td>
                                                    &nbsp;&nbsp; <b>Amount:</b>&nbsp;
                                                    <%="&pound;"&grossCost%>
                                                </td>
												<td nowrap="nowrap">
                                                    Current Status:&nbsp;<font color="red"><%=current_status%></font>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 5px">
                                    <td colspan="4">
                                    </td>
                                </tr>
                                <tr valign="top" class='po-table-header'>
                                    <td style="border-bottom: 1px solid" nowrap="nowrap" width="170px">
                                        Date
                                    </td>
                                    <td style="border-bottom: 1px solid" width="160px">
                                        Action
                                    </td>
                                    <td style="border-bottom: 1px solid" width="300px">
                                        Notes
                                    </td>
                                    <td style="border-bottom: 1px solid" width="200px">
                                        Contractor
                                    </td>
                                    <td style="border-bottom: 1px solid" width="100px">
                                        Image
                                    </td>
                                </tr>
                                <tr style="height: 3px">
                                    <td colspan="5">
                                    </td>
                                </tr>
                            </thead>
                            <tbody class='po-history-data'>
                                <%=str_po_detail%>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class='update-postatus hidden'>
        <form name="frmSend" id="frmSend" method="post" enctype="multipart/form-data" accept-charset="utf-8"
        action="UploadInvoicePopUp.asp" class='po-status-form'>
        <div class='left-container'>
            <div class='update-postatus-select'>
                <span>Status:</span>
                <select class='postatus_select' name="list_values" id="list_values">
                    <option value="7">Invoice Received</option>
                </select>
            </div>
        </div>
        <div class='right-container'>
            <div class='text-boxes'>
                <span>Invoice No:</span>
                <input type="text" name="txt_InvoiceNo" id="txt_InvoiceNo" maxlength="15" value="<%=invoiceNo%>" />
                <img src="/js/FVS.gif" width="15" height="15" name="img_InvoiceNo" id="img_InvoiceNo" alt="" />
            </div>
            <div class='text-boxes'>
                <span>Tax Date:</span>
                <input type="text" name="txt_TaxDate" id="txt_TaxDate" maxlength="10" value="<%=taxDate%>"
                    class='box-label' />
                <img src="/js/FVS.gif" width="15" height="15" name="img_TaxDate" id="img_TaxDate" alt="" />
            </div>
            <div class='text-boxes'>
                <span>Amount:</span>
                <input style="margin-left: 18px;" type="number" name="txt_InvoiceAmount" id="txt_InvoiceAmount" maxlength="10" value="<%=invoiceAmount%>"/>
                <img src="/js/FVS.gif" width="15" height="15" name="img_InvoiceAmount" id="img_InvoiceAmount" alt="" />
            </div>
            <div class='update-postatus-text'>
                <span>Notes:</span>
                <textarea rows="4" cols="20" name="txt_notes" id="txt_notes" maxlength="100" class='text-area'></textarea>
                <img src="/js/FVS.gif" width="15" height="15" name="img_notes" id="img_notes" alt="" />
            </div>
            <div class='update-postatus-upload'>
                <input name="uploadImage" type="file" id="uploadImage" accept="image/*, application/pdf"
                    onchange="checkFileUpload()" size="25" class='upload_field' /><br />
            </div>
            <div class='submit-button'>
                <input type="button" id="btnUploadInvoice" name="btnUploadInvoice" value="Upload and Save"
                    style="display: none;"  onclick="changeState(this,<%=grossCost%>)" />
                <!--<input type="submit" id="btnSubmit" name="btnSubmit" value="Upload and Save" style="display: block;"
                    disabled="disabled" />-->
            </div>
        </div>
        <input type="hidden" name='isChild' id='isChild' value="<%=isChild%>">
        <input type="hidden" name='updateParent' id='updateParent' value="<%=updateParent%>">
        <input type="hidden" name='refNo' id='refNo' value="<%=refNo%>">
        <input type="hidden" name='OrderID' id='OrderID' value="<%=Session("OrderID")%>">
        <input type="hidden" name='amount' id='amount' value="<%=amount%>">
        <input type="hidden" name='itemName' id='itemName' value="<%=itemName%>">
        <input type="hidden" name="hid_is_invoice_exist" id="hid_is_invoice_exist" value="" />
        </form>
		<iframe name="serverFrame" width="200" height="200" style="display: none"></iframe>
    </div>
	<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
