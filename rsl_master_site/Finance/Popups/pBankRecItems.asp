<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split

	Headers = Array("T.Date", "Item","Local Authority","Payment","Claim No","Debit","Credit","")
	SortOrder = Array("",  "", "","","", "", "", "")
	HeaderWidths = Array("90","90","135","115","130", "90", "90","")
			'
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal_debit = Request("detotal_debit")
	if detotal_debit = "" Then detotal_debit = 0 end if

	detotal_credit = Request("detotal_credit")
	if detotal_credit = "" Then detotal_credit = 0 end if

	selected = Request("idlist") // get selected checkboxes into an array
	selected_data = "," & Replace(selected, " ", "") & ","

	todate = Request("txt_TO")
	if todate = "" Then 
		to_sql = "" 
	else
		to_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(todate,1) & "' "
	end if

	fromdate = Request("txt_FROM")
	if fromdate = "" Then 
		from_sql = "" 
	else
		from_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) >= '" & FormatDateTime(fromdate,1) & "' "
	end if

	localauthority = Request("sel_LA")
	if localauthority = "" Then 
		localauthority_sql = "" 
	else
		localauthority_sql = " AND LA.LOCALAUTHORITYID = " & localauthority & " "
	end if

		
	paymenttype = Request("sel_PAYMENTTYPE")
	if paymenttype = "" Then 
		paymenttype_sql = "" 
	else
		paymenttype_sql = " AND RJ.PAYMENTTYPE = " & paymenttype & " "
	end if
	
	ctype = Request("sel_TYPE")
	if ctype = "" then
		ctype_sql = ""
	else
		if ctype = 1 then
			ctype_sql = " AND RJ.AMOUNT >= 0 "
		else
			ctype_sql = " AND RJ.AMOUNT < 0 "
		end if
	end if		

	orderBy = Request("orderBy")
	if (orderBy = "") then
		orderBy = SortOrder(0) & " ASC"
	else
		Matched = false
		CompareOrderByArray = Split(orderBy, " ")
		CompareOrderBy = CompareOrderByArray(i)
		for i=0 to Ubound(SortOrder)
			RW CompareOrderBy & " -- " & SortOrder(i) & "<BR>"
			if (CompareOrderBy = SortOrder(i)) then
				Matched = true
				Exit For
			end if
		next
		if Matched = false then
			orderBy = SortOrder(0) & " ASC"		
		end if
	end if
	
	TheNextPage_NoOrder = "txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_PAYMENTTYPE=" & paymenttype & "&txt_TENANCYID=" & tenancy & "&sel_TYPE=" & ctype
	TheNextPage = "orderBy=" & orderBy & "&" & TheNextPage_NoOrder
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CLng(mypage)
		end if
		
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" 
				
		for i=0 to Ubound(Headers)
			tWidth = "WIDTH=""" & HeaderWidths(i) & """"
			if (tWidth = "WIDTH=""""") then tWidth = ""
			if (SortOrder(i) <> "") then
				str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" &_
						"<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " ASC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_up_black.gif"" border=""0"" alt=""Sort Ascending""></A>&nbsp;" &_
						Headers(i) &_
						"&nbsp;<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " DESC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_down_black.gif"" border=""0"" alt=""Sort Descending""></A>" &_
						"</B></FONT></TD>" 
			else
				str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" & Headers(i) & "</B></FONT></TD>" 
			end if
		next
		
		str_data = str_data & "</TR><TR><TD HEIGHT='100%'></TD></TR>" 

		
		cnt = 0
		strSQL = 	"SELECT 	HBINFO.HBREF, RJ.JOURNALID, RJ.TENANCYID, CONVERT(VARCHAR,RJ.TRANSACTIONDATE, 103) AS TRANSDATE, P.DESCRIPTION AS PAYMENTTYPE, " &_
					"			ISDEBIT, ISNULL(RJ.AMOUNT,0) AS AMOUNT, TS.DESCRIPTION AS STATUS, I.DESCRIPTION AS ITEMTYPE, " &_
					"			CASE WHEN LEN( LA.DESCRIPTION) > 22   " &_
					"				THEN LEFT(LA.DESCRIPTION , 17) +  '...'  "  &_
					"				ELSE  LA.DESCRIPTION " &_
					"			END AS LOCALAUTHORITY , LA.DESCRIPTION AS LOCALAUTHORITYTITLE " &_ 
					"FROM	 	F_RENTJOURNAL RJ " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = RJ.ITEMTYPE " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS TS ON TS.TRANSACTIONSTATUSID = RJ.STATUSID " &_
					"			LEFT JOIN F_CASHPOSTING CP ON CP.JOURNALID = RJ.JOURNALID " &_
					"			LEFT JOIN F_PAYMENTSLIP PS ON PS.PAYMENTSLIPID = CP.PAYMENTSLIPNUMBER " &_
					"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
					"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
					"			INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
					"			LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = DEV.LOCALAUTHORITY " &_
					"			LEFT JOIN F_HBINFORMATION HBINFO ON HBINFO.TENANCYID = T.TENANCYID	" &_
					"WHERE		RJ.STATUSID <> 1  " &_
					" AND 	P.PAYMENTTYPEID IN (1,15,16) AND NOT EXISTS" &_
					"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
					"			AND RJ.TRANSACTIONDATE = '" & REQUEST("dateRange") & "' AND LA.LOCALAUTHORITYID = " & Request("LAuthID") &_
					" ORDER		BY RJ.JOURNALID ASC"
	
		if mypage = 0 then mypage = 1 end if
		'RW STRSQL
		pagesize = 12
	'  		  PLACE THIS NEXT TO WHERE
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		Set regEx = New RegExp 

		
		while NOT rs.EOF 
					
			// determine if check box has been previously selected
			isselected = ""
			trstyle = ""
			JOURNALID = Rs("JOURNALID")
			ListSearch = "," & JOURNALID & ","
			Comparison = InStr(1, selected_data, ListSearch, 0)
			If NOT isNULL(Comparison) AND Comparison > 0  Then
					isselected = " checked "
					trstyle = " style='background-color:steelblue;color:white' "
			End If
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			paymenttype = Rs("PAYMENTTYPE")
			paymenttypetitle = ""
			if (NOT isNull(paymenttype)) then
				if (len(paymenttype) > 20) then
					paymenttypetitle = " title='" & paymenttype& "' "
					paymenttype = LEFT(paymenttype,20) & "..."
				end if
			end if
			 
			str_data = str_data & 	"<TR" & trstyle & "><TD>" & Rs("TRANSDATE") & "</TD>" &_
						"<TD>" & Rs("ITEMTYPE") & "</TD>" &_
						"<TD title="& Rs("LOCALAUTHORITYTITLE") & ">"& Rs("LOCALAUTHORITY") & "</TD>" &_
						"<TD " & paymenttypetitle & ">" & paymenttype & "</TD>" &_
						"<TD>"& Rs("HBREF") &"</TD>" ' TAKEN STATUS OFF -JIMMY *** TAKEN OUT -JIMMY - & TenancyReference(Rs("TENANCYID")) &
			amount = Rs("AMOUNT")
			if (amount < 0) then
				iType = 0
				amount = -amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				iType = 1						
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'></TD>" &_
						"</TR>"
		Rs.movenext()
		Wend

		Rs.close()
		Set Rs = Nothing




	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=8 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
%>
<html>
<head></head>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language=javascript>

function CloseWindow(){

window.close()
}
</script>
<body leftmargin="0" topmargin="0" >
 <object id="WBControl" width="0" height="0" classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object> 
  <script language="VBScript"><!--  
 Sub VBPrint() On Error Resume next  
	 WBControl.ExecWB 6,1  
 End Sub  
 //-->
</script>  

<script language=JavaScript><!--   
 
 function doPrint(){  

	 if(self.print){
	   
	   	 document.PrinterIcon.style.display = "none";
		 self.print();  
		 self.close();  
		 return False;  
 		}  
		
	 else if (navigator.appName.indexOf(' Microsoft') != -1){  

		 VBPrint();  
		 self.close();  
		 }  
 	else{  
	 alert("To print this document, you will need to\nclick the Right Mouse Button and select\n' Print'");  
 	}  
 }  
//-->  
 </script>  

<div id="ReloaderDiv"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td rowspan="5" width=10>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td>Below are the individual transaction items for the selected '<b>Housing 
        Benefit</b>' transaction. The 'Housing Benefit' transaction contains 1 
        or more items, which are displayed below. The chosen transaction based 
        on '<%=request("dateRange")%>'. </td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td><%=str_data%></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width=10>&nbsp;</td>
      <td align="right">
       
      </td>
    </tr>
  </table>
</div>

<div align="right"> 
  <table width="100" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td  align="right">&nbsp;</td>
      <td width="10"></td>
    </tr>
    <tr> 
      <td  align="right"><font color="#0066CC"><b><font size="4"><img name="PrinterIcon" src="../../myImages/PrinterIcon.gif" width="31" height="20" onClick="doPrint()" style="cursor:hand;display:block" alt="Print List"></font></b></font></td>
      <td width="10"></td>
    </tr>
    <tr>
      <td  align="right">&nbsp;</td>
      <td width="10"></td>
    </tr>
    <tr> 
      <td  align="right"> 
        <input type="button" name="Submit" value="Close" class="RSLButton" onClick="CloseWindow()">
      </td>
      <td width="10"></td>
    </tr>
    <tr> 
      <td  align="right">&nbsp; </td>
      <td width="10"></td>
    </tr>
  </table>
</div>
</body>
</html>