<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim theamounts, EmployeeLimits, IsShowDeletePI, IsReconciledOrPaid, IsDisableVatDropDown
	Dim optionvalues, optiontext
	Dim optionvalues2, optiontext2
    Dim optionvalues3, optiontext3
	Dim HEADID, PITYPE

	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")

	OpenDB()

	Call GetCurrentYear()
	FY = GetCurrent_YRange
			
	ORDERITEMID = REQUEST("ORDERITEMID")
	IPAGE = REQUEST("CURRENTPAGE")
	BATCHID=REQUEST("BatchID")
	
	IF(BATCHID="") then
	    BATCHID=-1
	END IF    
		
	SQL = "SELECT PI.ORDERID,PI.ITEMNAME,PI.ITEMDESC,ISNULL(PI.NETCOST,0) AS NETCOST,PI.VAT,PI.VATTYPE,ISNULL(PI.GROSSCOST,0) AS GROSSCOST ,PI.PITYPE,PI.EXPENDITUREID, "&_ 
	             " CC.COSTCENTREID, EXP.DESCRIPTION AS EXPNAME, PO.SchemeId, PO.BLOCKID, PO.POSTATUS FROM F_PURCHASEITEM PI "&_
			"INNER JOIN F_EXPENDITURE EXP ON EXP.EXPENDITUREID = PI.EXPENDITUREID " &_
			"INNER JOIN F_HEAD HE ON HE.HEADID = EXP.HEADID " &_
			"INNER JOIN F_COSTCENTRE CC ON CC.COSTCENTREID = HE.COSTCENTREID " &_
            "INNER JOIN F_PURCHASEORDER PO on PO.ORDERID = PI.ORDERID " &_
			"WHERE ORDERITEMID = " & ORDERITEMID
	Call OpenRs(rsPI, SQL)
	ORDERID 	= rsPI("ORDERID")
	ITEMREF 	= rsPI("ITEMname")
	ITEMDESC 	= rsPI("ITEMDESC")
	NETCOST 	= rsPI("NETCOST")
	VAT 		= rsPI("VAT")
	VATTYPE 	= rsPI("VATTYPE")
	GROSSCOST 	= rsPI("GROSSCOST")
	PITYPE		= rsPI("PITYPE")
    POStatus     = rsPI("POSTATUS")

    IsShowDeletePI = "hidden"
    IsReconciledOrPaid = ""

    IF(POStatus = 0 OR POStatus = 1 OR POStatus = 3 OR POStatus = 18) Then
        IsShowDeletePI = "visible"
    END IF

    IF(POStatus >= 9 AND POStatus <> 19 AND POStatus <>  16 AND POStatus <>  17 AND POStatus <>  18 AND POStatus <>  20) Then
        IsReconciledOrPaid = "readonly"
        IsDisableVatDropDown = "disabled"
    END IF
   	
	IF Request("ReturnTo") = "1"  OR Request("ReturnTo") = "2" THEN 'CAME FROM THE RECONCILE PAGE
	   
		IF GROSSCOST >= 0 AND GROSSCOST <= 500 THEN
			VARIANCE = 15
		ELSEIF GROSSCOST >= 500.01 AND GROSSCOST <= 1000 THEN
			VARIANCE = 10			
		ELSEIF GROSSCOST >= 1000.01 AND GROSSCOST <= 5000 THEN
			VARIANCE = 5			
		ELSEIF GROSSCOST >= 5000.01 AND GROSSCOST <= 10000 THEN
			VARIANCE = 2			
		ELSE 
			VARIANCE = 0
		END IF
		
		MAXVARIANCE = FormatNumber( (GROSSCOST * VARIANCE/100) + GROSSCOST, 2,-1,0,0)
		
	ELSE 'CAME FROM THE AMEND PAGE
		IF GROSSCOST >= 0 AND GROSSCOST <= 500 THEN
			VARIANCE = 1
		ELSEIF GROSSCOST >= 500.01 AND GROSSCOST <= 1000 THEN
			VARIANCE = 0.5			
		ELSEIF GROSSCOST >= 1000.01 AND GROSSCOST <= 5000 THEN
			VARIANCE = 0.2			
		ELSEIF GROSSCOST >= 5000.01 AND GROSSCOST <= 10000 THEN
			VARIANCE = 0.1			
		ELSE 
			VARIANCE = 0
		END IF
		
		MAXVARIANCE = FormatNumber( (GROSSCOST * VARIANCE/100) + GROSSCOST, 2,-1,0,0)
	END IF
	
	COSTCENTRE = rsPI("COSTCENTREID")
	EXPID = rsPI("EXPENDITUREID")
	EXPNAME = rsPI("EXPNAME")

	GetPrevStuff()
	
	Call CloseRs(rsPI)

    ' TODO: Need to move in Uper function GetPrevStuff

    SQL = "SELECT SCInfo.SchemeId, CASE SCInfo.SchemeId WHEN -1 THEN 'All Schme' ELSE (SELECT SchemeName from P_SCHEME where SchemeId = SCInfo.SchemeId) " &_
          " END As SchemeName, IsNull(SCInfo.BlockId,0) as BlockId, CASE WHEN SCInfo.BlockId = -1 THEN 'All Block' WHEN SCInfo.BlockId is NULL THEN '-' ELSE " &_
          " (SELECT BlockName from P_Block where BlockId = SCInfo.BlockId) End As BlockName, IsNull(SCInfo.PropertyId,'0') as PropertyId, " &_
          " CASE WHEN SCInfo.PropertyId = '-1' THEN 'All Property' WHEN SCInfo.PropertyId is NULL THEN '-' ELSE (SELECT HOUSENUMBER + ' ' + ADDRESS1 from P__PROPERTY where PropertyId = SCInfo.PropertyId) " &_
          " END As PropertyName  FROM F_PurchaseItemSCInfo SCInfo WHERE SCInfo.OrderItemId = " & ORDERITEMID

    Call OpenRs(rsSCInfo, SQL)

    SCInfoString = ""
    RowCounter = 0
    while not rsSCInfo.EOF
        SCInfoString = SCInfoString + " <tr id=""TR" & RowCounter & """style=""cursor: pointer;""> "
        SCInfoString = SCInfoString + " <td> " & rsSCInfo("SchemeName") & " : " & rsSCInfo("BlockName") & " : " & rsSCInfo("PropertyName") & " <input " 
        SCInfoString = SCInfoString + " type=""hidden"" name=""SID_ROW"" value=""" & RowCounter & """ > <input " 
        SCInfoString = SCInfoString + " type=""hidden"" name=""iSelDATA" & RowCounter & """ id=""iSelDATA" & RowCounter & """ value=""" & rsSCInfo("SchemeId") & "," & rsSCInfo("BlockId") & "," & rsSCInfo("PropertyId") & """ > "
        SCInfoString = SCInfoString + " </td> <td style=""text-align: center; background-color: rgb(255, 255, 255);""> "
        SCInfoString = SCInfoString + " <img title=""Clicking here will remove this item from the list."" style=""cursor:hand"" "
        SCInfoString = SCInfoString + " src=""/js/img/FVW.gif"" width=""15"" height=""15"" onclick=""DeleteSelectionRow(" & RowCounter & ")"">"
        SCInfoString = SCInfoString + " </td> </tr>"
        rsSCInfo.movenext
        RowCounter = RowCounter + 1
	Wend
    
    CloseRs(rsSCInfo)


	'rw costcentre & "  _   " & PITYPE
	' WE NEED TO DETERMINE WETHER OR NOT TO EXCLUDE ESTATES RELATED COSTCENTRES FROM NON-REPAIR (NOT 2) PURCHASE ITEMS
	exclude_sql = ""
	'If cint(PITYPE) <> 2 Then exclude_sql = " AND CC.COSTCENTREID NOT IN (11,13,22,23) " End If
	
'	DISTINCT_SQL = "F_COSTCENTRE CC " &_
'			"INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
'			"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
'			"WHERE (CC.ACTIVE = 1) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1) " & exclude_sql &_
'			" AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND "

	DISTINCT_SQL = "F_COSTCENTRE CC " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
			"WHERE EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )" &_
			exclude_sql

    Scheme_SQL = "P_SCHEME"

    Block_SQL = "P_BLOCK " &_
			"INNER JOIN P_SCHEME Sch ON Sch.SCHEMEID = P_BLOCK.SCHEMEID"
    Property_SQL = "P__PROPERTY " &_
        "INNER JOIN P_SCHEME Sch ON Sch.SCHEMEID = P__PROPERTY.SCHEMEID"
			
	Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", COSTCENTRE, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")

    Call BuildSelectForSC(lstScheme, "sel_SCHEME", Scheme_SQL, "SCHEMEID, SCHEMENAME", "SCHEMENAME", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeScheme()' tabindex=4","Schemes")
    'Call BuildSelectForSC(lstBlock, "sel_BLOCK", Block_SQL, "BLOCKID, BLOCKNAME", "BLOCKNAME", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeBlock()' tabindex=4", "Blocks")
    'Call BuildSelectForSC(lstProperty, "sel_Property", Property_SQL, "PROPERTYID, HOUSENUMBER + ' ' + ADDRESS1 As PropertyName", "HOUSENUMBER,ADDRESS1", NULL, NULL, NULL, "textbox200", " tabindex=4", "Properties")

	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, VATTYPE, NULL, "textbox200", " onchange='SetVat()' tabindex=5")	
	CloseDB()

	Function GetPrevStuff()
		if (ExpID = -1) then
			Exit Function
		end if

		SQL = "SELECT HEADID FROM F_EXPENDITURE WHERE EXPENDITUREID = " & EXPID
		Call OpenRs(rsHEAD, SQL)
		HeadID = rsHEAD("HEADID")
		CloseRs(rsHEAD)

		SQL = "SELECT DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (isNull(EXA.EXPENDITUREALLOCATION,0) - isNull(GROSSCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) " &_				
				"INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE " &_
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_				
				"left JOIN ( "&_
					"SELECT SUM(GROSSCOST) AS GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM WHERE F_PURCHASEITEM.ACTIVE = 1 AND F_PURCHASEITEM.ORDERITEMID <> " & ORDERITEMID & " " &_
					"AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_					
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (EX.HEADID = " & HeadID & ") "&_
				"ORDER BY DESCRIPTION"
				
		Call OpenRs(rsExpenditure, SQL)		
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"		
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			EmployeeLimits = EmployeeLimits & ";;" & rsExpenditure.Fields.Item("LIMIT").Value			
			
			  count = count + 1
			  rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If
		CloseRs(rsExpenditure)

		SQL = "SELECT COSTCENTREID FROM F_HEAD WHERE HEADID = " & HeadID
		Call OpenRs(rsCC, SQL)
		CCID = rsCC("COSTCENTREID")
		CloseRs(rsCC)		

		SQL = "SELECT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) " &_ 
			"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			"WHERE HE.CostCentreID = " & CCID			
		
		Call OpenRs(rsHead, SQL)
		
		count2 = 0
		optionvalues2 = ""
		optiontext2 = "Please Select..."
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues2 = optionvalues2 & ";;" & rsHead.Fields.Item("HEADID").Value
			optiontext2 = optiontext2 & ";;" & theText
			
			  count2 = count2 + 1
			  rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		if (count2 = 0) Then
			optionvalues2 = ""
			optiontext2 = "No Heads Are Setup..."
		End If

		CloseRs(rsHead)

		
	End Function

%>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript" src="/js/financial.js"></script>
<script src="../../js/jquery-1.6.2.min.js" type="text/javascript"></script>
<script language="JAVASCRIPT">

    var propertyArray = new Array()
    var FormFields = new Array()
    var SelRowCounter = 0
	
    function SetChecking(val){
        FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
        FormFields[1] = "sel_HEAD|Head|SELECT|Y"
        FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
        FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
        FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
        FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
        FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
        FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
        //FormFields[8] = "sel_SCHEME|Scheme|SELECT|Y"
        //FormFields[9] = "sel_BLOCK|Block|SELECT|Y"
    }
		
    function LOADNOW(){
        PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 1);
        THISFORM.sel_EXPENDITURE.value = "<%=EXPID%>"
        PopulateListBox("<%=optionvalues2%>", "<%=optiontext2%>", 2);			
        THISFORM.sel_HEAD.value = "<%=HeadID%>"	
        THISFORM.EXPENDITURE_LEFT_LIST.value = "<%=theamounts%>";
        THISFORM.EMPLOYEE_LIMIT_LIST.value = "<%=EmployeeLimits%>";
        SetPurchaseLimits()			
        if("<%=IsDisableVatDropDown%>" == "disabled"){
            jQuery("#divId").find("input, select, button, textarea").attr("disabled",true);
        }
        var oTable = document.getElementById("SelectionTable");
        
        for (i = 0; i < oTable.rows.length; i++) {
            var data = document.getElementById("iSelDATA" + i).value;
            propertyArray[SelRowCounter] = data;
            SelRowCounter ++;
        }    
    }

    function PopulateListBox(values, thetext, which){
        values = values.replace(/\"/g, "");
        thetext = thetext.replace(/\"/g, "");
        values = values.split(";;");
        thetext = thetext.split(";;");
        if(which != undefined){
            if (which == 2)
                var selectlist = document.forms.THISFORM.sel_HEAD;
            else if (which == 3)
                var selectlist = document.forms.THISFORM.sel_BLOCK;
            else if (which == 4)
                var selectlist = document.forms.THISFORM.sel_Property;
            else
                var selectlist = document.getElementById("sel_EXPENDITURE")

            selectlist.length = 0;

            for (i=0; i<thetext.length;i++){
                var oOption = document.createElement("OPTION");

                oOption.text=thetext[i];
                oOption.value=values[i];
                selectlist.options[selectlist.length]= oOption;
            }
        }
    }
	
    function Select_OnchangeFund(){
        document.getElementById("txt_EMLIMITS").value = "0.00";						
        document.getElementById("txt_EXPBALANCE").value = "0.00";
        PopulateListBox("", "Waiting for Data...", 2);
        PopulateListBox("", "Please Select a Head...", 1);
        THISFORM.IACTION.value = "gethead";
        THISFORM.action = "../ServerSide/GetHeads.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    }

    function loadPropertiesData(values, thetext, which) {
        PopulateListBox(values, thetext, which);
        Select_OnchangeBlock();
    }

    function Select_OnchangeScheme() {
        PopulateListBox("", "Waiting for Data...", 3);
        THISFORM.IACTION.value = "getBlock";
        THISFORM.action = "../ServerSide/GetBlocksForSc.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    }

    function Select_OnchangeBlock() {
        PopulateListBox("", "Waiting for Data...", 4);
        THISFORM.IACTION.value = "getProperty";
        THISFORM.action = "../ServerSide/GetPropertiesForSc.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    } 
	
    function Select_OnchangeHead(){    
        document.getElementById("txt_EXPBALANCE").value = "0.00";
        document.getElementById("txt_EMLIMITS").value = "0.00";								
        PopulateListBox("", "Waiting for Data...", 1);	
        THISFORM.IACTION.value = "change";
        THISFORM.action = "../ServerSide/GetExpendituresForAmend.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        THISFORM.submit();
    }

    function DeletePropertyRecord(selectedRecord) {
        for (j = 0; j < propertyArray.length; j++) {
            if (propertyArray[j] == selectedRecord) {
                propertyArray.splice(j, 1);
            }
        }
    }

    function IsRecordAlreadyExist(selectedRecord) {
        if (propertyArray.length > 0) {
            if (propertyArray.length == 1) {
                if (propertyArray[0] == "-1,-1,-1") {
                    return "All Scheme/Block/Property already added.";
                }
            }

            if (selectedRecord == "-1,-1,-1") {
                return "Different record already added. Need to delete that records first.";
            }

            for (i = 0; i < propertyArray.length; i++) {
                if (propertyArray[i] == selectedRecord) {
                    return "Please select other record. This is already added.";
                }
            }

            for (i = 0; i < propertyArray.length; i++) {
                var selectedRecordSplit = selectedRecord.split(",");
                var propertyArraySplit = propertyArray[i].split(",");
                if (selectedRecordSplit[0] == propertyArraySplit[0]) {
                    if (selectedRecordSplit[1] == propertyArraySplit[1]) {
                        if (selectedRecordSplit[2] == propertyArraySplit[2]) {
                            return "Please select other record. This is already added.";
                        }
                        else {
                            if (selectedRecordSplit[2] == "-1") {
                                return "Different record already added of the same Scheme/Block. Need to delete that records first for Adding all Properties."
                            }
                            else if (propertyArraySplit[2] == "-1") {
                                return "All Properties are already added of the same Scheme/Block. Need to delete that records first for changing Properties."
                            }
                        }
                    }
                    else {
                        if (selectedRecordSplit[1] == "-1") {
                            return "Different record already added for the Block. Need to delete that records first for Adding all Blocks."
                        }
                        else if (propertyArraySplit[1] == "-1") {
                            return "All Blocks are already added of the same Scheme. Need to delete that records first for changing Blocks."
                        }
                    }
                }
                else {
                    if (selectedRecordSplit[0] == "-1") {
                        return "Different record already added for the Scheme. 1Need to delete that records first for Adding all Schemes."
                    }
                    else if (propertyArraySplit[0] == "-1") {
                        return "All Schemes are already added. Need to delete that records first for changing Scheme."
                    }
                }
            }
        }

        return "1";
    }

    function SetPurchaseLimits(){
        eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
        eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
        eAmounts = eAmounts.split(";;");
        eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
        eEMLIMITS = eEMLIMITS.split(";;");
	
        document.getElementById("txt_EXPBALANCE").value = FormatCurrency(eAmounts[eIndex]);
        document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	
    }	

    function DeleteItem(){
        <% IF (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then %>
		    THISFORM.action = "../ServerSide/DeletePurchaseItemSC.asp?ReturnTo=<%=Request("ReturnTo")%>";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        <% else %>
		    THISFORM.action = "../ServerSide/DeletePurchaseItemSC.asp";
        THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        <% end if %>
		
		THISFORM.submit();		
    }

    function parentReload()
    {
        opener.location.reload();
        window.close();
    }

    function UpdateItem(){
        SetChecking(1)		
        var oTable = document.getElementById("SelectionTable");
        var img = document.getElementById("img_SelectionTable");
        if(oTable.rows.length < 1)
        {            
            img.style.visibility = 'visible';
            return false;
        }
        img.style.visibility = 'hidden';
        if (!checkForm()) return false

        if("<%=IsReconciledOrPaid%>" != "readonly"){
            GrossCost = document.getElementById("txt_GROSSCOST").value
            EmployeeLimit = document.getElementById("txt_EMLIMITS").value
            ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value

            // Here we chck if the expenditure budget has been reached and ask if they will or wont allow overspend
            // : refers to request (178) : Budget Overspend (amend PO) - kerri 
            document.getElementById("ISQUEUED").value = 0
            if (parseFloat(ExpenditureLimit) < parseFloat(GrossCost)){
                answer = confirm("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget remaining (�" + FormatCurrencyComma(ExpenditureLimit) + ") for the selected item.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");			
                if (!answer) return false
            }
		
            ByPassEmployeeLimit = false
            BeforeAmendGrossCostMaxVariance = <%=MAXVARIANCE%>
         
            //this if statement allows the item to be amended with the employee limit by-passed if the new cost
            //is within the variance parameters of the old cost and is also for the same expenditure.
            if (BeforeAmendGrossCostMaxVariance >= parseFloat(GrossCost) && <%=EXPID%> == document.getElementById("sel_EXPENDITURE").value)
            {
                ByPassEmployeeLimit = true
            }
            if (!ByPassEmployeeLimit) {
        
                if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)){
                    document.getElementById("ISQUEUED").value = 1
                    //alert("The total cost is greater than your employee limit in the selected Expenditure.\n Please ask your Manager to Amend this item or increase your limits.")
                    //return false
                    result = confirm("The total cost is greater than your employee limit in the selected Expenditure.\nIf you continue then this item will be placed in a queue to be authorised\nby a user who has appropriate limits.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
                    if (!result) return false
                }
            }                     
        }
        document.getElementById("POVatType").value = document.getElementById("sel_VATTYPE").value;
        if("<%=IsReconciledOrPaid%>" == "readonly")
        {
            THISFORM.action = "../ServerSide/AmendReconciledPaidPOSC.asp"
            THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
        }
        else{
            <% IF (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then %>
                //Ref = dialogArguments
                //Ref.disabled = true
      
                    THISFORM.action = "../ServerSide/AmendOrderItemSC.asp?ReturnTo=<%=Request("ReturnTo")%>";
            THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            <% else %>
                THISFORM.action = "../ServerSide/AmendOrderItemSC.asp";
            THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            <% end if %>
            }
        THISFORM.submit();	
        //window.close();	
    }

    function AddSelectionButton() {
        var blockDropDown = document.getElementById("sel_BLOCK");
        var schemeDropDown = document.getElementById("sel_SCHEME");
        var propertyDropDown = document.getElementById("sel_Property");

        AddSelectionRow(blockDropDown, schemeDropDown, propertyDropDown);
    }

    function AddSelectionRow(blockDropDown, schemeDropDown, propertyDropDown) {            
        var schemeValue = schemeDropDown.options[schemeDropDown.selectedIndex].value;
        var schemeName = schemeDropDown.options[schemeDropDown.selectedIndex].text;
            
        var bloxkValue = blockDropDown.options[blockDropDown.selectedIndex].value;
        var blockName = blockDropDown.options[blockDropDown.selectedIndex].text;
        if (bloxkValue == "0")
        {
            bloxkValue = "0"
            blockName = "-"
        }

        var propertyValue = propertyDropDown.options[propertyDropDown.selectedIndex].value;
        var propertyName = propertyDropDown.options[propertyDropDown.selectedIndex].text;
        if (propertyValue == "0") {
            propertyValue = "0"
            propertyName = "-"
        }

        var selectedRecord = schemeValue + "," + bloxkValue + "," + propertyValue;
        var result = IsRecordAlreadyExist(selectedRecord);
        if (result != "1") {
            alert(result);
            return false;
        }

        propertyArray[SelRowCounter] = selectedRecord;

        selTable = document.getElementById("SelectionTable")

        oRow = selTable.insertRow(-1)
        oRow.id = "TR" + SelRowCounter
        oRow.style.cursor = "pointer"

        DATA = "<input type=\"hidden\" name=\"SID_ROW\" value=\"" + SelRowCounter + "\"><input type=\"hidden\" name=\"iSelDATA" + SelRowCounter + "\" id=\"iSelDATA" + SelRowCounter + "\" value=\"" + schemeValue + "," + bloxkValue + "," + propertyValue + "\">"
        ItemName = schemeName + " : " + blockName + " : " + propertyName
        AddCell(oRow, ItemName + DATA, "", "", "", 0)
        DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteSelectionRow(" + SelRowCounter + ")\">"
        AddCell(oRow, DelImage, "", "center", "#FFFFFF", 1)
        SelRowCounter++
        return true;
    }

    function DeleteSelectionRow(RowID) {
        oTable = document.getElementById("SelectionTable")
        for (i = 0; i < oTable.rows.length; i++) {
            if (oTable.rows[i].id == "TR" + RowID) {
                var data = document.getElementById("iSelDATA" + i).value;
                DeletePropertyRecord(data);
                oTable.deleteRow(i);
                break;
            }
        }
        ChangeTableRowId();
    }

    function ChangeTableRowId() {
        var oTable = document.getElementById("SelectionTable")
        for (i = 0; i < oTable.rows.length; i++) {
            oTable.rows[i].setAttribute('id', "TR" + i);
            var inputCollection = oTable.rows[i].getElementsByTagName("input");
            inputCollection[0].setAttribute('value', i);
            inputCollection[1].setAttribute('id', "iSelDATA" + i);
            inputCollection[1].setAttribute('name', "iSelDATA" + i);
            inputCollection[1].setAttribute('name', "iSelDATA" + i);
            var imgCollection = oTable.rows[0].getElementsByTagName("img");
            imgCollection[0].setAttribute('onclick', "DeleteSelectionRow(" + i + ")");
        }
        SelRowCounter--;
    }

    function AddCell(iRow, iData, iTitle, iAlign, iColor, iposition) {
        oCell = iRow.insertCell(iposition)
        oCell.innerHTML = iData
        if (iTitle != "") oCell.title = iTitle
        if (iAlign != "") oCell.style.textAlign = iAlign
        if (iColor != "") oCell.style.backgroundColor = iColor
    }
	
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="LOADNOW()">
    <table width="100%" cellspacing="5">
        <tr>
            <td rowspan="3">&nbsp;</td>
            <td>
                <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td height="7"></td>
                    </tr>
                    <tr>
                        <td valign="top" height="20">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td rowspan="2" width="79">
                                        <img src="../Images/tab_amendorderitem.gif" width="139" height="20"></td>
                                    <td height="19"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#133E71">
                                        <img src="images/spacer.gif" height="1"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style='border-left: 1px solid #133E71; border-right: 1px solid #133E71; border-bottom: 1px solid #133E71'>
                            <iframe name="PURCHASEFRAME<%=TIMESTAMP%>" style='display: NONE'></iframe>
                            <form id="" name="THISFORM" method="POST">
                                <table cellspacing="0" cellpadding="3">

                                    <tr bgcolor="steelblue" style='color: white'>
                                        <td colspan="9"><b>&nbsp; </b></td>
                                    </tr>
                                    <tr>
                                        <td>Item : </td>
                                        <td>
                                            <input id="txt_ITEMREF" <%=IsReconciledOrPaid%> name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex="3" value="<%=ITEMREF%>"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_ITEMREF" name="img_ITEMREF"></td>

                                    </tr>
                                    <tr>
                                        <td valign="top">Notes : </td>
                                        <td>
                                            <textarea tabindex="3" <%=IsReconciledOrPaid%> id="txt_ITEMDESC" name="txt_ITEMDESC" rows="7" class="TEXTBOX200" style='overflow: hidden; border: 1px solid #133E71'><%=ITEMDESC%></textarea></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_ITEMDESC" name="img_ITEMDESC"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Scheme : </td>
                                        <td>
                                            <%=lstScheme%>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_SCHEME" id="img_SCHEME"></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Block : </td>
                                        <td>
                                            <select id="sel_BLOCK" name="sel_BLOCK" class="TEXTBOX200" onchange="Select_OnchangeBlock()"
                                                tabindex="4">
                                                <option value="-1">All Blocks</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_BLOCK" id="img_BLOCK"></td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Property : </td>
                                        <td>
                                            <select id="sel_Property" name="sel_Property" class="TEXTBOX200" tabindex="4">
                                                <option value="-1">All Properties</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_Property" id="img_Property"></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td style="text-align: center;">
                                            <input type="button" name="AddSelection" id="AddSelection" value=" Add Selection " title="AddSelection"
                                                class="RSLButton" onclick="AddSelectionButton()" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td>
                                            <div style="height: 90px; border-style: solid; border-width: 0.5px; width: 200px; overflow-y: auto;">
                                                <table style="behavior: url(/Includes/Tables/tablehl.htc)"
                                                    cellpadding="3" width="185" id="SelectionTable" slcolor='' hlcolor="silver">
                                                    <tbody>
                                                        <%=SCInfoString%>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <td>
                                            <img style="visibility: hidden" src="../Images/FVER.gif"
                                                title="This field is required and must be a valid monetary value."
                                                width="15" height="15" name="img_SelectionTable" id="img_SelectionTable">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;Cost Centre : </td>
                                        <td><%=lstCostCentres%></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRE" id="img_COSTCENTRE"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Head : </td>
                                        <td>
                                            <select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()" tabindex="4">
                                                <option value="">Please Select a Cost Centre...</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_HEAD">
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;Expenditure : </td>
                                        <td>
                                            <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()" tabindex="4">
                                                <option value="">Please select a Head...</option>
                                            </select>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_EXPENDITURE" id="img_EXPENDITURE">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap>&nbsp;Exp Balance : </td>
                                        <td>
                                            <input type="text" id="txt_EXPBALANCE" name="txt_EXPBALANCE" class="textbox200" tabindex="-1" readonly></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Spend Limit : </td>
                                        <td>
                                            <input type="text" name="txt_EMLIMITS" id="txt_EMLIMITS" class="textbox200" readonly tabindex="-1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Net Cost : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> id="txt_NETCOST" name="txt_NETCOST" type="text" class="textbox200" maxlength="20" size="11" onblur="TotalBoth();ResetVAT()" onfocus="alignLeft()" tabindex="5" value="<%=FormatNumber(NETCOST,2,-1,0,0)%>">
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" id="img_NETCOST" name="img_NETCOST"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Vat Type : </td>
                                        <td>

                                            <div id="divId">
                                                <%=lstVAT%>
                                            </div>
                                        </td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_VATTYPE" id="img_VATTYPE"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;VAT : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> name="txt_VAT" type="text" class="textbox200" maxlength="20" size="11" onblur="TotalBoth()" onfocus="alignLeft()" value="<%=FormatNumber(VAT,2,-1,0,0)%>" tabindex="5" id="txt_VAT"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_VAT" id="img_VAT"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;Total : </td>
                                        <td>
                                            <input style='text-align: right' <%=IsReconciledOrPaid%> id="txt_GROSSCOST" name="txt_GROSSCOST" type="text" class="textbox200" maxlength="20" size="11" readonly tabindex="-1" value="<%=FormatNumber(GROSSCOST,2,-1,0,0)%>"></td>
                                        <td>
                                            <img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST"></td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2">
                                            <input type="HIDDEN" name="IACTION" id="IACTION">
                                            <input type="HIDDEN" name="ISQUEUED" id="ISQUEUED" value="0">
                                            <input type="HIDDEN" name="IPAGE" id="IPAGE" value="<%=IPAGE%>">
                                            <input type="HIDDEN" name="ORDERITEMID" id="ORDERITEMID" value="<%=ORDERITEMID%>">
                                            <input type="HIDDEN" name="ORDERID" id="ORDERID" value="<%=ORDERID%>">
                                            <input type="HIDDEN" name="BATCHID" id="BATCHID" value="<%=BATCHID%>">
                                            <input type="HIDDEN" name="POVatType" id="POVatType" value="<%=VATTYPE%>">
                                            <input type="HIDDEN" name="EXPENDITURE_LEFT_LIST" id="EXPENDITURE_LEFT_LIST" value="<%=theamounts%>">
                                            <input type="HIDDEN" name="EMPLOYEE_LIMIT_LIST" id="EMPLOYEE_LIMIT_LIST" value="<%=EmployeeLimits%>">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td nowrap>
                                                        <input type="button" name="DeletePI" value=" DELETE PURCHASE ITEM " class="RSLButton" style="color: red; visibility: <%=IsShowDeletePI%>" onclick="DeleteItem()" tabindex="6">&nbsp;</td>
                                                    <td nowrap>
                                                        <input type="button" name="ResetButton" value=" CANCEL " class="RSLButton" onclick="window.close()" tabindex="6">&nbsp;</td>
                                                    <td>
                                                        <input type="button" name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateItem()" tabindex="6"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
