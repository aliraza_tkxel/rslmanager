<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="Includes/Functions/TableBuilder.asp" -->
<%
L_ORDERID = 0
IF(Request("ORDERID") <> "" AND ISNUMERIC(Request("ORDERID"))) THEN L_ORDERID = Request("OrderID")
%>
<%
OpenDB()

EmployeeLimit = 0

SQL = "SELECT F_PurchaseOrder.POTYPE, F_PurchaseOrder.RECTYPE, MASTERORDERID, POSTATUS, OrderID, VATTYPE, D.DEVELOPMENTNAME, ItemRef, o.name as orgname, ItemDesc, HeadID, ExpenditureID, PODate=CONVERT(VARCHAR,PODate,103), " &_
	" ExpDelDate, DelDetail, CONVERT(VARCHAR,QuotedCost,1) AS QuotedCost, V.VATNAME, CONVERT(VARCHAR,VAT,1) AS VAT, CONVERT(VARCHAR,NETCOST,1) AS NETCOST, Supplier, POTYPENAME, POSTATUSNAME " &_
	"FROM dbo.F_PurchaseOrder " &_
	"LEFT JOIN S_ORGANISATION O ON F_PurchaseOrder.SUPPLIER = O.ORGID " &_	
	"LEFT JOIN PDR_DEVELOPMENT D ON F_PurchaseOrder.DEVELOPMENT = D.DEVELOPMENTID " &_		
	"LEFT JOIN F_VAT V ON F_PurchaseOrder.VATTYPE = V.VATID " &_		
	"LEFT JOIN F_POSTATUS PS ON F_PurchaseOrder.POSTATUS = PS.POSTATUSID " &_		
	"LEFT JOIN F_POTYPE PT ON F_PurchaseOrder.POTYPE = PT.POTYPEID " &_			
	"WHERE Active = 1 AND OrderID = " & L_ORDERID & " AND POSTATUS < 9 " &_
	"ORDER BY OrderNo"
Call OpenRs(rsDR, SQL)
%>
<%
if (NOT rsDR.EOF) then
	'THIS PART GETS ALL THE DATABASE FIELDS...
	L_ORDERID = rsDR.Fields.Item("ORDERID").Value
	L_MASTER_ORDERID = rsDR.Fields.Item("MASTERORDERID").Value	
	L_ITEMREF = rsDR.Fields.Item("ITEMREF").Value
	L_ITEMDESC = rsDR.Fields.Item("ITEMDESC").Value 
	L_PODATE = rsDR.Fields.Item("PODATE").Value
	L_ORGNAME = rsDR.Fields.Item("ORGNAME").Value
	L_POSTATUS_CODE = rsDR.Fields.Item("POSTATUS").Value
	L_POSTATUS_NAME = rsDR.Fields.Item("POSTATUSNAME").Value
	L_POTYPE_CODE = rsDR.Fields.Item("POTYPE").Value
	L_POTYPE_NAME = rsDR.Fields.Item("POTYPENAME").Value
	L_DEVELOPMENTNAME = rsDR.Fields.Item("DEVELOPMENTNAME").Value
	L_NETCOST = rsDR.Fields.Item("NETCOST").Value
	L_VAT = rsDR.Fields.Item("VAT").Value
	L_VATTYPE = rsDR.Fields.Item("VATTYPE").Value
	L_VAT_NAME = rsDR.Fields.Item("VATNAME").Value	
	L_QUOTEDCOST = rsDR.Fields.Item("QUOTEDCOST").Value
	L_EXPENDITURE = rsDR.Fields.Item("EXPENDITUREID").Value
	L_HEAD = rsDR.Fields.Item("HEADID").Value
	
	L_RECTYPE = rsDR.Fields.Item("RECTYPE").Value
	CloseRs(rsDR)	
	
	if ( L_EXPENDITURE = "" OR isNull(L_EXPENDITURE) OR L_EXPENDITURE = 0) then
		'is a head purchase order 
		SQLSPENT = "SELECT ISNULL(SUM(QUOTEDCOST),0) AS SPENT FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND HEADID = " & L_HEAD & " AND ORDERID <> " & L_ORDERID & " AND EXPENDITUREID IS NULL "
		SQLALLOC = "SELECT H.DESCRIPTION, ISNULL(ISNULL(HEADALLOCATION,0) - ISNULL(EXPTOTAL,0),0) AS TOTAL FROM F_HEAD H " &_
				   "LEFT JOIN (SELECT HEADID, SUM(EXPENDITUREALLOCATION) AS EXPTOTAL FROM F_EXPENDITURE GROUP BY HEADID) E ON H.HEADID = E.HEADID " &_
				   "WHERE H.HEADID = " & L_HEAD & " "
		
		Call OpenRS(rsSpent, SQLSPENT)
		Call OpenRS(rsAlloc, SQLALLOC)
		
		If Not rsAlloc.EOF Then TOTAL = rsAlloc("TOTAL") Else TOTAL = 0	End If
		
		If not rsSpent.EOF Then SPENT = rsSpent("SPENT") Else SPENT = 0 End If
		
		Budget_Remaining = TOTAL - SPENT
		
		If not rsAlloc.EOF then	
			TEXT = "The balance remaining in this Head (" & rsAlloc("DESCRIPTION") & "), "
		Else
			TEXT = "The balance remaining in this Head (0), "
		End If
			
		CloseRs(rsSpent)
		CloseRs(rsAlloc)	
	Else
		'is a expenditure purchase order 
		SQLSPENT = "SELECT ISNULL(SUM(QUOTEDCOST),0) AS SPENT FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND EXPENDITUREID = " & L_EXPENDITURE & " AND ORDERID <> " & L_ORDERID & ""
		SQLALLOC = "SELECT EX.DESCRIPTION, ISNULL(EX.EXPENDITUREALLOCATION,0) AS TOTAL, ISNULL(EL.LIMIT,0) AS LIMIT FROM F_EXPENDITURE EX " &_
					"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_				
					"WHERE EX.EXPENDITUREID = " & L_EXPENDITURE & " "
		
		Call OpenRS(rsSpent, SQLSPENT)
		Call OpenRS(rsAlloc, SQLALLOC)
		TOTAL = rsAlloc("TOTAL")
		SPENT = rsSpent("SPENT")
		Budget_Remaining = TOTAL - SPENT
		EmployeeLimit = rsAlloc("LIMIT")	
		TEXT = "The balance remaining in this expenditure item (" & rsAlloc("DESCRIPTION") & "), "
		CloseRs(rsSpent)
		CloseRs(rsAlloc)
	END IF	
	
	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, L_VATTYPE, NULL, "textbox100", " onchange='SetVat()' ")
	
	if (L_POSTATUS_CODE < 4 AND L_POTYPE_CODE = 2) then
		'the above if true means this is a repair and not completed yet,
		'therefore disable the reconcile button
		WarningText = "<font color=red><li><b>This Purchase Order is for a repair which has not been completed yet, and therefore cannot be reconciled.</font>"
		DisabledButton = " disabled "
	elseif (L_POSTATUS_CODE = 0) then
		WarningText = "<font color=red><li><b>This Purchase Order has been put in a queue and therefore cannot be reconciled.</font>"
		DisabledButton = " disabled "
	end if

	'THIS PART FINDS ALL PART RECONCILED ORDERS WHICH HAVE NOT BEEN RECONCILED
	 SQL = "SELECT ORDERID FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND POSTATUS < 9 AND ORDERID <> " & L_ORDERID & " AND MASTERORDERID = " & L_MASTER_ORDERID
	 Call OpenRs(rsLeft, SQL)
	 'THIS MEANS THAT IF THE RECONCILEAMOUNT MATCHES THE TOTAL ABOVE THE MASTER PO CAN BE CLOSED ASWELL
	 StillSomeLeft = 0
	 if (NOT rsLeft.EOF) then
	   'THIS MEANS THAT IF THE RECONCILEAMOUNT MATCHES THE TOTAL ABOVE THE MASTER PO CANNOT BE CLOSED AS THEIR IS OTHER PART PO WAITING TO BE RECONCILED
		StillSomeLeft = 1	
	 end if
	 CloseRs(rsLeft)

else
	WarningText = "<font color=red><li><b>This Purchase Order cannot be found.</font>"
	DisabledButton = " disabled "
end if
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Part Reconciliation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var FormFields = new Array()
FormFields[0] = "txt_INVOICENUMBER|Invoice Number|TEXT|N"
FormFields[1] = "txt_QUOTEDCOST|Quoted Cost|CURRENCY|Y"
FormFields[2] = "txt_VAT|VAT|CURRENCY|Y"
FormFields[3] = "txt_NETCOST|Net Cost|CURRENCY|Y"
FormFields[4] = "txt_TAXDATE|Invoice Date|DATE|Y"

	function ResetVAT(){
		if (document.getElementById("sel_VATTYPE").value == 1) AddVAT(20)
		if (document.getElementById("sel_VATTYPE").value == 2) AddVAT(5)		
		}
		
	function TotalBoth(){
		event.srcElement.style.textAlign = "right"
		if (!checkForm(true)) {
			document.getElementById("txt_QUOTEDCOST").value = ""
			return false
			}
		document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))
		}

	function SetVat(){
		if (document.getElementById("sel_VATTYPE").value == 0 || document.getElementById("sel_VATTYPE").value == 3){
			document.getElementById("txt_VAT").value = "0.00"
			document.getElementById("txt_VAT").readOnly = true
			}
		else if (document.getElementById("sel_VATTYPE").value == 1){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(20)
			}
		else if (document.getElementById("sel_VATTYPE").value == 2){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(5)
			}			
		document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
		}

	function AddVAT(val){
		if (isNumeric("txt_NETCOST", "")){
			//VAT = FormatCurrency(parseFloat(document.getElementById("txt_NETCOST").value) /100 * val)
			VAT = new Number (document.getElementById("txt_NETCOST").value /100 * val)
			// vat needs to be rounded to 3 decimals then to 2 to get correct amount JAVASCRIPT DOESNT WORK !!!!!
			VAT = round(round(VAT,4),2)
			document.getElementById("txt_VAT").value = FormatCurrency(VAT)
			document.getElementById("txt_QUOTEDCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
			}
		}

	function round(number,X) {
	// rounds number to X decimal places, defaults to 2
		X = (!X ? 2 : X);
		return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
	}

	function dosubmit(){
		if (!checkForm()) return false;
		elList = document.all;

		if (parseFloat(elList["txt_QUOTEDCOST"].value) > parseFloat(elList["txt_RECAMOUNT"].value)) {
			alert("The Invoice Amount is more expensive than the Reconcilable amount!\nPlease decrease the invoice amount to continue...");
			return false;		
			}

		if (parseFloat(elList["txt_QUOTEDCOST"].value) > parseFloat(elList["maxValue"].value)) {
			alert("The Invoice Amount is more expensive than the balance remaining in this expenditure/head!\nPlease decrease the invoice amount to continue...");
			return false;		
			}
		 ReconcileMPO = ""
		 if (parseFloat(elList["txt_QUOTEDCOST"].value) == parseFloat(elList["txt_RECAMOUNT"].value) && "<%=StillSomeLeft%>" == "0") {
			RESULT = confirm("This part reconciliation will bring the total of all Part Purchase Orders to match the Master Purchase Order total.\nThis will make the Master Purchase Order fully reconciled as there are no other Part Purchase Orders left\nthat need to be reconciled. Do you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.");
			if (!RESULT) return false;
			ReconcileMPO = "&ReconcileMPO=1"		
			}
		ReconciliationConfirm.target = "HiddenFinanceFrame"
		ReconciliationConfirm.action = "../ServerSide/ReconcileConfirm_svr.asp?MASTERORDERID=<%=L_MASTER_ORDERID%>&PartReconcile=2" + ReconcileMPO;
		ReconciliationConfirm.submit();
		window.close()
		}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TABLE CELLSPACING=5 CELLPADDING=5 WIDTH=100%><TR><TD>
  <TABLE HEIGHT=100% WIDTH=550 CELLPADDING=0 CELLSPACING=0 BORDER=0>
    <tr> 
      <td valign="top" height="20"> 
        <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
          <TR> 
            <TD ROWSPAN=2 width="79"><img src="../Images/tab_tenancy.gif" width="79" height="20"></TD>
            <TD HEIGHT=19></TD>
          </TR>
          <TR> 
            <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=200 HEIGHT=1></TD>
          </TR>
        </TABLE>
      </td>
    </tr>
    <tr> 
      <td valign="top" STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71;BORDER-BOTTOM:1PX SOLID #133E71' ALIGN=CENTER> 
        <table width="95%" border="0" cellspacing="0" cellpadding="0" align=center>
		  <form name="ReconciliationConfirm" method="post">
          <tr> 
            <td colspan=4 class="RSLBlack"><BR><b>Part Purchase Order Information</b></td>
          </tr>
          <tr> 
            <td colspan=2> 
              <table cellspacing=0 cellpadding=3 width=400 style='border:1px solid black'>
                <tr> 
                  <td class="RSLBlack" width=100>Order No :</td>
                  <td><b><%= PartPurchaseNumber(L_ORDERID) %></b></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">Item :</td>
                  <td><%= L_ITEMREF%></td>
                </tr>
                <tr valign=top> 
                  <td class="RSLBlack">Item Desc:</td>
                  <td><%= L_ITEMDESC %></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">Order Date :</td>
                  <td><%= L_PODATE %></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">Supplier :</td>
                  <td><%= L_ORGNAME %></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">PO Status :</td>
                  <td><%= L_POSTATUS_NAME %></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">PO Type :</td>
                  <td><%= L_POTYPE_NAME %></td>
                </tr>
                <tr> 
                  <td class="RSLBlack">Development :</td>
                  <td><%= L_DEVELOPMENTNAME %></td>
                </tr>
              </table>
            </td>
       <%
	  	 SQL = "SELECT ISNULL(SUM(QUOTEDCOST),0) AS MASTER_QUOTEDCOST FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND ORDERID = " & L_MASTER_ORDERID
		 OpenDB()
		 Call OpenRs(rsMax, SQL)
		 L_MASTER_QUOTEDCOST = 0
		 if (NOT rsMax.EOF) then
		 	L_MASTER_QUOTEDCOST = rsMax("MASTER_QUOTEDCOST")	
		 end if
	  	 CloseRs(rsMax)

	  	 SQL = "SELECT ISNULL(SUM(QUOTEDCOST),0) AS TOTALREC FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND ORDERID <> " & L_ORDERID & " AND MASTERORDERID = " & L_MASTER_ORDERID
		 Call OpenRs(rsTotalPartRec, SQL)
		 TotalReconcilableAmount = L_MASTER_QUOTEDCOST
		 if (NOT rsTotalPartRec.EOF) then
		 	TotalReconcilableAmount = L_MASTER_QUOTEDCOST - rsTotalPartRec("TOTALREC")	
		 end if
	  	 CloseRs(rsTotalPartRec)

		 CloseDB()		 
		 
	  %>
            <td colspan=2 valign=top align=center>&nbsp; </td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
          </tr>

          <tr> 
            <td colspan=4  class="RSLBlack"> <b>Reconciliation Process</b> </td>
          </tr>
          <tr> 
            <td colspan=2> 
              <table width=400 cellspacing=2 style='border:1px solid black'>
                <tr> 
                  <td class="RSLBlack">Reconcilable Amount :</td>
                  <td align=right> 
                    <input type=text value="<%=FormatNumber(TotalReconcilableAmount,2,-1,0,0)%>" name=txt_RECAMOUNT class="textbox100" maxlength=29 size=30 READONLY style='border:1px solid black;background-color:beige'>
                    <input type=HIDDEN value="<%=L_QUOTEDCOST%>" name=txt_POMAXCOST>
				    <input type="hidden" name="DoReconciliation" value="Reconcile">					
                  </td>
                </tr>
                <tr> 
                  <td class="RSLBlack">Enter Invoice Number :</td>
                  <td align=right> 
                    <input type=text name=txt_INVOICENUMBER class="textbox100" maxlength=29 size=30>
                  </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_INVOICENUMBER"></td>
                </tr>
                <tr> 
                  <td>Invoice Date :</td>
                  <td align=right> 
                    <input name="txt_TAXDATE" type="text" class="textbox100" maxlength=29 value="<%= FormatDateTime(Date,2) %>">
                  </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_TAXDATE"></td>
                </tr>
                <tr> 
                  <td>Net Cost :</td>
                  <td align=right> 
                    <input style='text-Align:right' name="txt_NETCOST" type="text" class="textbox100" maxlength=29 onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()" value="<%= FormatNumber(L_NETCOST,2,-1,0,0) %>">
                  </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_NETCOST"></td>
                </tr>
                <tr> 
                  <td>VAT Type :</td>
                  <td align=right> <%=lstVAT%> </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_VATYPE"></td>
                </tr>
                <tr> 
                  <td >VAT :</td>
                  <td align=right> 
                    <input style='text-Align:right' name="txt_VAT" type="text" class="textbox100" maxlength=29 onBlur="TotalBoth()" onFocus="alignLeft()" readonly value="<%= FormatNumber(L_VAT,2,-1,0,0) %>">
                  </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_VAT"></td>
                </tr>
                <tr> 
                  <td class="RSLBlack" nowrap>Confirm Invoice Amount :</td>
                  <td align=right> 
                    <input type=text name=txt_QUOTEDCOST  class="textbox100" style='text-align:right' maxlength=29 size=30 readonly value="<%= FormatNumber(L_QUOTEDCOST,2,-1,0,0) %>" >
                  </td>
                  <td><img src="/js/FVS.gif" width=15 height=15 name="img_QUOTEDCOST"></td>
                </tr>
                <tr> 
                  <td> 
                    <input type="hidden" name="OrderID" value="<%= L_ORDERID %>">
                          <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                        </td>
                  <td align=right> 
                    <input type="button" name="Submit" value="Reconcile" class="RSLButton" <%=DisabledButton%> onClick=dosubmit(9)>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <td colspan=4 class="RSLBlack"> <%=TEXT%><br>
              not including this purchase order is : � 
              <input type=text name=maxValue class="RSLBlack" style='text-Align:top;border:none' readonly value="<%=FormatNumber(Budget_Remaining,2,-1,0,0)%>">
              <br>
            </td>
          </tr>
		</FORM>		  
        </table>
      </td>
    </tr>
  </table>
 </TD></TR></TABLE>
</BODY>
</HTML>