<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
Dim L_WORKORDERID
L_WORKORDERID = ""
if(Request.QueryString("WorkOrderID") <> "") then L_WORKORDERID = Request.QueryString("WorkOrderID")

OpenDB()

SQL = "SELECT WOTO.ORDERITEMID, PO.ORDERID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, " &_
		"ISNULL(O.NAME, 'SUPPLIER NOT ASSIGNED') AS SUPPLIER, O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY, O.TELEPHONE1, O.TELEPHONE2, O.FAX, "&_
		"C.FIRSTNAME + ' ' + C.LASTNAME AS CUSTOMERNAME, P.PROPERTYID, P.FLATNUMBER, P.HOUSENUMBER, P.ADDRESS1 AS PADDRESS1, P.ADDRESS2 AS PADDRESS2, P.ADDRESS3 AS PADDRESS3, P.TOWNCITY AS PTOWNCITY, P.POSTCODE AS PPOSTCODE, P.COUNTY AS PCOUNTY, "&_		
		"D.DEVELOPMENTNAME FROM F_PURCHASEORDER PO " &_
		"LEFT JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_
		"LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_		
		"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID " &_
		"LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " &_
		"LEFT JOIN P_WOTOREPAIR WOTO ON WOTO.WOID = WO.WOID	" &_
		"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = WO.CUSTOMERID " &_								
		"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_										
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"WHERE ACTIVE = 1 AND WOTO.WOID = " & L_WORKORDERID
		'RESPONSE.WRITE SQL
Call OpenRs(rsPP, SQL)

if (NOT rsPP.EOF) then
	ORDERID = rsPP("ORDERID")
	PONUMBER = PurchaseNumber(rsPP("ORDERID"))
	PONAME = rsPP("PONAME")
	PODATE = FormatDateTime(rsPP("PODATE"),1)
	SUPPLIER = rsPP("NAME")
	SUPPLIERID = rsPP("SUPPLIERID")	
	POSTATUSNAME = rsPP("POSTATUSNAME")
	DEVELOPMENT = rsPP("DEVELOPMENTNAME")
	PROPERTYID = rsPP("PROPERTYID")
	
	AddressArray = Array("Supplier", "Address1", "Address2", "Address3", "TownCity", "PostCode", "County")
	AddressString = ""
	For i=0 to ubound(AddressArray)
		if (rsPP(AddressArray(i)) <> "" AND NOT ISNULL(rsPP(AddressArray(i)))) THEN
			AddressString = AddressString & rsPP(AddressArray(i)) & "<br>"
		end if
	next
	AddressString = AddressString & "<br>"
	
	ContactArray = Array("Telephone1", "Telephone2", "Fax")
	ContactString = ""
	For i=0 to ubound(ContactArray)
		if (rsPP(ContactArray(i)) <> "" AND NOT ISNULL(rsPP(ContactArray(i)))) THEN
			ContactString = ContactString & "<i>" & ContactArray(i) & ":</i> " & rsPP(ContactArray(i)) & "<br>"
		end if
	next
	if (ContactString <> "") then
		ContactString = ContactString & "<br>"
	end if

	//if a repair get the property address
	if (NOT ISNULL(PROPERTYID)) then
		//THIS SECTION WILL GET THE PROPERTY ADDRESS
		PAddressArray = Array("CUSTOMERNAME", "Housenumber", "PAddress1", "PAddress2", "PAddress3", "PTownCity", "PPostCode", "PCounty")
		PAddressString = ""
		For i=0 to ubound(PAddressArray)
			if (rsPP(PAddressArray(i)) <> "" AND NOT ISNULL(rsPP(PAddressArray(i)))) THEN
				PAddressString = PAddressString & rsPP(PAddressArray(i)) & "<br>"
			end if
		next
		PAddressString = PAddressString & "<br>"
	end if

else
	Response.Redirect ("Purchase Order NOt Found Page.asp")
end if
Call CloseRs(rsPP)

SQL = "SELECT PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, " &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE FROM F_PURCHASEITEM PI " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
		"WHERE PI.ACTIVE = 1 AND ORDERID = " & ORDERID & " ORDER BY ORDERITEMID ASC"

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF) 
	
	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	PIString = PIString & "<TR ALIGN=RIGHT><TD nowrap ALIGN=LEFT width=420>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center title='" & rsPI("VATNAME") & "' nowrap width=40>" & rsPI("VATCODE") & "</TD><TD nowrap width=80>" & FormatNumber(NetCost,2) & "</TD><TD nowrap width=75>" & FormatNumber(VAT,2) & "</TD><TD nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5></TD></TR>"
	ITEMDESC = rsPI("ITEMDESC")
	if (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
		PIString = PIString & "<TR id=""PIDESCS"" STYLE='DISPLAY:NONE'><TD colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"
	end if
	rsPI.moveNext
wend
CloseRs(rsPI)

'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID
Call OpenRs(rsINV, SQL)
if (NOT rsINV.EOF) then
	iTGC = 0
	iTNC = 0
	iTVA = 0
	InvoiceString = "<br><TABLE WIDTH=704 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
			"<TD WIDTH=310 HEIGHT=20 nowrap><b>Invoice No:</b></TD><TD nowrap width=150><b>Tax Date:</b></TD><TD nowrap align=right width=80><b>Net (�):</b></TD><TD nowrap align=right width=75><b>VAT (�):</b></TD><TD nowrap align=right width=80><b>Gross (�):</b></TD><TD WIDTH=5></TD></TR>"
	while NOT rsINV.EOF
		iVAT = rsINV("VAT")
		iNetCost = rsINV("NETCOST")
		iGrossCost = rsINV("GROSSCOST")
		iTGC = iTGC + iGrossCost
		iTNC = iTNC + iNetCost
		iTVA = iTVA + iVAT
		InvoiceString = InvoiceString & "<TR><TD nowrap>" & rsINV("InvoiceNumber") & "</TD><TD nowrap>" & rsINV("TAXDATE") & "</TD><TD ALIGN=RIGHT nowrap>" & FormatNumber(iNetCost,2) & "</TD><TD ALIGN=RIGHT nowrap>" & FormatNumber(iVAT,2) & "</TD><TD ALIGN=RIGHT nowrap>" & FormatNumber(iGrossCost,2) & "</TD></TR>"
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</TABLE>"

  InvoiceString = InvoiceString & "<TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=704>"
	InvoiceString = InvoiceString & "<TR bgcolor=steelblue ALIGN=RIGHT> "
	  InvoiceString = InvoiceString & "<TD height=20 width=460 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=80 bgcolor=white nowrap><b>" & FormatNumber(iTNC,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=75 bgcolor=white nowrap><b>" & FormatNumber(iTVA,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=80 bgcolor=white nowrap><b>" & FormatNumber(iTGC,2) & "</b></TD>"
	  InvoiceString = InvoiceString & "<TD width=5 bgcolor=white nowrap></TD>"
	InvoiceString = InvoiceString & "</TR>"
  InvoiceString = InvoiceString & "</TABLE>"
end if
Call CloseRs(rsINV)

CloseDB()
%>
<HTML>
<HEAD>
<title>&nbsp;</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

	function PrintThePP(){
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("SHOWDESC").style.visibility = "hidden";				
		window.print();
		document.getElementById("PrintButton").style.visibility = "visible";	
		document.getElementById("SHOWDESC").style.visibility = "visible";				
		}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}			
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" CLASS='TA'>
<table cellspacing=0 cellpadding=0><tr><td width=10></td><td>

<TABLE BORDER=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=704>
	<TR><TD width=100% nowrap valign=top height=100>
		<b>Supplier Details</b><br>
		<%=AddressString%>
		<%=ContactString%>								
	</TD><TD width=300px nowrap valign=top height=100>
	<% if (NOT ISNULL(PROPERTYID) ) then %>
		<b>Property Details</b><br>
		<%=PAddressString%>
	<% end if %>
	</TD><TD ALIGN=RIGHT VALIGN=TOP width=134><IMG SRC="/myImages/Broadland_Purchase_Image.gif" width="134" height="106"></TD></TR>
</TABLE>
<BR>
<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=704PX>
	<TR><TD WIDTH=90PX>Name:</TD><TD><%=PONAME%></TD></TR>
	<TR><TD WIDTH=90PX>Order No:</TD><TD><%=PONUMBER%></TD></TR>
	<TR><TD>Description:</TD><TD><%=PONAME%></TD></TR>
	<TR><TD>Order Date:</TD><TD><%=PODATE%></TD></TR>
	<TR><TD>Development:</TD><TD><%=DEVELOPMENT%></TD><TD rowspan=2 valign=bottom align=right><input type=Button name="PrintButton" class="RSLButton" value=" PRINT " onclick="PrintThePP()"></td></TR>
	<TR><TD>Status:</TD><TD><%=POSTATUSNAME%></TD></TR>
</TABLE>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=704>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=420 NOWRAP><table cellspacing=0 cellpadding=0 width=418><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
	  <TD width=40><b>Code:</b></TD>
	  <TD width=80><b>Net (�):</b></TD>
	  <TD width=75><b>VAT (�):</b></TD>
	  <TD width=80><b>Gross (�):</b></TD>
	  <TD width=5></TD>		  
	</TR>
	<%=PIString%> 
  </TABLE>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=704>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD height=20 width=460 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 bgcolor=white nowrap><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD width=5 bgcolor=white nowrap></TD>		  
	</TR>
  </TABLE>
<%=InvoiceString%>  

<TABLE cellspacing=0 cellpadding=2 width=704>
	<TR><TD><b>PURCHASE NOTES :</b> </TD></TR>
	<TR><TD style='border:1px solid #133e71;HEIGHT:60' height=60px valign=top width=704px>&nbsp;</TD></TR>
</TABLE>

</td></tr></table>
<div align=center><b>Registered Office</b> 100 St Benedicts, Norwich, NR2 4AB, Tel: 01603 750200, Fax: 01603 750222</div>
</BODY>
</HTML>
