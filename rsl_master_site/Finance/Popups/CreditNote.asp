<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	Dim C_CNID, SUPPLIER_EXPPAYDATE
	C_CNID = ""
	if(Request.QueryString("CNID") <> "") then C_CNID = Request.QueryString("CNID")

	OpenDB()
	SQL = 	"SELECT '<b>CN</B> ' + RIGHT('0000' + CAST(C.CNID AS VARCHAR),7) AS FORMATCNID, " &_
			"		O.NAME, C.CNID, C.ORDERID, C.CNNAME, C.SUPPLIERID, " &_
			"		C.CNDATE, C.CNNOTES, ISNULL(O.NAME, 'SUPPLIER NOT ASSIGNED') AS SUPPLIER, " &_
			"		O.ADDRESS1, O.ADDRESS2, O.ADDRESS3, O.TOWNCITY, O.POSTCODE, O.COUNTY, " &_
			"		O.TELEPHONE1, O.TELEPHONE2, O.FAX, PS.POSTATUSNAME	" &_
			"FROM 	F_CREDITNOTE C  " &_
			"		LEFT JOIN S_ORGANISATION O ON C.SUPPLIERID = O.ORGID  " &_
			"		INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = C.CNSTATUS	" &_
			"WHERE 	C.CNID = " & C_CNID

	Call OpenRs(rsSet, SQL)
	if (NOT rsSet.EOF) then
		CNID  		= rsSet("FORMATCNID") 	
		If (rsSet("ORDERID") <> "") Or (Not IsNull(rsSet("ORDERID"))) Then
			PONUMBER 	= PurchaseNumber(rsSet("ORDERID"))
		Else
			PONUMBER = ""
		End If
		CNNAME 		= rsSet("CNNAME")
		CNDATE 		= FormatDateTime(rsSet("CNDATE"),1)
		SUPPLIER	= rsSet("NAME")
		SUPPLIERID 	= rsSet("SUPPLIERID")	
		CNSTATUSNAME = rsSet("POSTATUSNAME")
		CNNOTES 	= rsSet("CNNOTES")
		
		AddressArray = Array("Supplier", "Address1", "Address2", "Address3", "TownCity", "PostCode", "County")
		AddressString = ""
		For i=0 to ubound(AddressArray)
			if (rsSet(AddressArray(i)) <> "" AND NOT ISNULL(rsSet(AddressArray(i)))) THEN
				AddressString = AddressString & rsSet(AddressArray(i)) & "<br>"
			end if
		next
		AddressString = AddressString & "<br>"
		
		ContactArray = Array("Telephone1", "Telephone2", "Fax")
		ContactString = ""
		For i=0 to ubound(ContactArray)
			if (rsSet(ContactArray(i)) <> "" AND NOT ISNULL(rsSet(ContactArray(i)))) THEN
				ContactString = ContactString & "<i>" & ContactArray(i) & ":</i> " & rsSet(ContactArray(i)) & "<br>"
			end if
		next
		if (ContactString <> "") then
			ContactString = ContactString & "<br>"
		end if
	
	'else
	'	Response.Redirect ("PurchaseOrderNotFound.asp")
	end if
	Call CloseRs(rsSet)
	
	SQL = "SELECT PI.ORDERITEMID, PI.PAIDON, PI.ITEMNAME, INV.TAXDATE, PI.ITEMDESC, PI.PIDATE, " &_
			"ABS(PI.NETCOST) AS NETCOST, ABS(PI.VAT) AS VAT, V.VATNAME, ABS(PI.GROSSCOST) AS GROSSCOST, " &_
			"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE FROM F_PURCHASEITEM PI " &_
			"INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CR ON CR.ORDERITEMID = PI.ORDERITEMID " &_
			"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
			"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
			"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
			"LEFT JOIN F_ORDERITEM_TO_INVOICE OTOI ON OTOI.ORDERITEMID = PI.ORDERITEMID " &_
			"LEFT JOIN F_INVOICE INV ON INV.INVOICEID = OTOI.INVOICEID " &_
			"WHERE CR.CNID = " & C_CNID  & " ORDER BY PI.ORDERITEMID DESC"
	
	Call OpenRs(rsPI, SQL)		
	while (NOT rsPI.EOF) 
	
		NetCost = rsPI("NETCOST")
		VAT = rsPI("VAT")
		GrossCost = rsPI("GROSSCOST")
		
		TotalNetCost = TotalNetCost + NetCost
		TotalVAT = TotalVAT + VAT
		TotalGrossCost = TotalGrossCost + GrossCost
		
		PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT width=360>" & DataStorage & rsPI("ITEMNAME") & "</TD><TD align=center title='" & rsPI("VATNAME") & "' nowrap width=40>" & rsPI("VATCODE") & "</TD><TD nowrap width=80>" & FormatNumber(NetCost,2) & "</TD><TD nowrap width=75>" & FormatNumber(VAT,2) & "</TD><TD nowrap width=80>" & FormatNumber(GrossCost,2) & "</TD><TD nowrap width=5></TD></TR>"
		ITEMDESC = rsPI("ITEMDESC")
		if (NOT isNull(ITEMDESC) AND ITEMDESC <> "") then
			PIString = PIString & "<TR id=""PIDESCS"" STYLE='DISPLAY:NONE'><TD colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"
		end if
		rsPI.moveNext
	wend
	CloseRs(rsPI)
	
	CloseDB()
%>
<HTML>
<HEAD>
<title>Credit Note</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

	function PrintThePP(){
	
		document.getElementById("PrintButton").style.visibility = "hidden";
		document.getElementById("SHOWDESC").style.visibility = "hidden";
		ReturnBack = 0
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			ToggleDescs()
			ReturnBack = 1
			}
		window.print();
		if (ReturnBack == 1) ToggleDescs()
		document.getElementById("PrintButton").style.visibility = "visible";	
		document.getElementById("SHOWDESC").style.visibility = "visible";
	}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}			
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" CLASS='TA'>
<br><table cellspacing=0 cellpadding=0 width="644" ALIGN=CENTER>
  <tr><td width=10></td><td>
      <TABLE BORDER=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK' CELLSPACING=0 CELLPADDING=2 width=644>
        <TR> 
          <TD width=100% nowrap valign=top height=100> <b>Supplier Details</b><br>
            <%=AddressString%> <%=ContactString%> </TD>
          <TD width=300px nowrap valign=top height=100>&nbsp;</TD>
          <TD ALIGN=RIGHT VALIGN=TOP width=134><IMG SRC="/myImages/Broadland_Purchase_Image.gif" width="134" height="106"></TD>
        </TR>
      </TABLE>
<BR>
<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=644PX>
	<TR>
	      <TD WIDTH=150pX>Credit Note Name:</TD>
	  <TD><%=CNNAME%></TD>
	<TD ALIGN=RIGHT colspan=2>&nbsp;</TD>
	</TR>
	    <TR> 
          <TD WIDTH=90PX>Credit Note No:</TD>
          <TD><%=CNID%></TD>
        </TR>
        <TR> 
          <TD>Credit Note Date:</TD>
          <TD><%=CNDATE%></TD>
        </TR>
        <TR> 
          <TD>PO Number:</TD>
          <TD><%=PONUMBER%></TD>
          <TD rowspan=2 valign=bottom align=right>
            <input type=Button name="PrintButton" class="RSLButton" value=" PRINT " onclick="PrintThePP()">
          </td>
        </TR>	
	<TR><TD>Status:</TD>
          <TD><%=CNSTATUSNAME%></TD>
        </TR>
</TABLE>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=644>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=360 NOWRAP><table cellspacing=0 cellpadding=0 width=356><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
	  <TD width=40><b>Code:</b></TD>
	  <TD width=80><b>Net (�):</b></TD>
	  <TD width=75><b>VAT (�):</b></TD>
	  <TD width=80><b>Gross (�):</b></TD>
	  <TD width=5></TD>		  
	</TR>
	<%=PIString%> 
  </TABLE>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=644>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD height=20 width=400 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 bgcolor=white nowrap><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 bgcolor=white nowrap><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD width=5 bgcolor=white nowrap></TD>		  
	</TR>
  </TABLE>

<TABLE cellspacing=0 cellpadding=2 width=644>
	<TR>
          <TD><b>NOTES :</b> </TD>
        </TR>
	<TR><TD style='border:1px solid #133e71;HEIGHT:60' height=60px valign=top width=644px><%=CNNOTES%>&nbsp;</TD></TR>
</TABLE>

<div align=center style='width:644'><b>Registered Office</b> Broadland Housing Association Limited, NCFC Jarrold Stand, Carrow Road, Norwich, NR1 1HU<br />Tel: 01603 750200, Fax: 01603 750222</div>
</td></tr></table>

</BODY>
</HTML>
