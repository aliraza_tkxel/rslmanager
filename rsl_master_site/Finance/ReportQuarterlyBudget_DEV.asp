<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
fiscalyear = Request("FiscalYear")
if (NOT isNumeric(fiscalyear)) then
	fiscalyear = ""
end if

 Company = "1"
if not Request("sel_Company") = "" then
    Company = Request("sel_Company")
end if


set rsFYear = Server.CreateObject("ADODB.Recordset")
rsFYear.ActiveConnection = RSL_CONNECTION_STRING
if (fiscalyear = "" or isNull(fiscalyear)) then
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
else
	rsFYear.Source = "SELECT YRange, YStart, YEnd FROM dbo.F_FiscalYears WHERE YRange = " & fiscalyear 
end if
rsFYear.CursorType = 0
rsFYear.CursorLocation = 2
rsFYear.LockType = 3
rsFYear.Open()
if (rsFYear.EOF) Then
	rsFYear.close()
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
	rsFYear.Open()
End if
rsFYear_numRows = 0

fiscalyear = rsFYear("YRange")
yearstart = FormatDateTime(rsFYear("YStart"),1)
yearend = FormatDateTime(rsFYear("YEnd"),1)

MonthST = Month(CDate(yearstart))
MonthEN = Month(CDate(yearend))
rsFYear.close()

' This report will only work for date ranges which are at most 1 year apart!!!!

Dim WriteOrder()
Counter = -1
if (MonthST = MonthEN) Then
	Counter = Counter + 1
	Redim Preserve WriteOrder(Counter)
	WriteOrder(Counter) = MonthST
Else
	while (MonthST <> MonthEN)
		Counter = Counter + 1
		Redim Preserve WriteOrder(Counter)
		WriteOrder(Counter) = MonthST
		MonthST = MonthST + 1
		if MonthST = 13 then
			MonthST = 1
		end if
	Wend
	Counter = Counter + 1
	Redim Preserve WriteOrder(Counter)
	WriteOrder(Counter) = MonthST
End if

Dim IDATE_STRING
FUNCTION UPDATEDATESELECT(ItemList)
	OptionList = Array("","<option value=""1"">ORDER DATE</option>","<option value=""2"">RECONCILE DATE</option>", "<option value=""3"">TAXDATE</option>", "<option value=""4"">PAYMENT DATE</option>")
	ItemOptions = ""
	ItemArray = Split(ItemList,",")
	for i=0 to Ubound(ItemArray)
		ItemOptions = ItemOptions & OptionList(ItemArray(i))
	next
	IDATE_STRING = "<select name=""IDATE"" class=""textbox"" style=""width:124px"">" & ItemOptions & "</select>"
End Function

Dim CurrentVal
FUNCTION CheckDate()
	CurrentVal = REQUEST("ISTATUS")
	if (CurrentVal = "") then
		CurrentVal = 1
	else
		CurrentVal = CInt(CurrentVal)
	end if
	Select Case (CurrentVal)
		case 1 UpdateDateSelect("1")
		case 2 UpdateDateSelect("1")
		case 3 UpdateDateSelect("1,2,3")
		case 4 UpdateDateSelect("1,2,3,4")
		case 5 UpdateDateSelect("1,2,3,4")
		case 6 UpdateDateSelect("1,2,3,4")
		case 7 UpdateDateSelect("1,2,3,4")
		case 8 UpdateDateSelect("1")
		case 9 UpdateDateSelect("1,2,3,4")
	End Select
End Function

'THIS WILL BUILD THE APPROPRIATE SELECT BOX DEPENDING ON WHAT STATUS IS SELECTED.
Call CheckDate()

IDATE = REQUEST("IDATE")
Dim DA (5)
DA(1) = "PI.PIDATE"
DA(2) = "CASE WHEN PI.PITYPE = 3 THEN PI.PIDATE ELSE PI.RECONCILEDATE END"
DA(3) = "CASE WHEN PI.PITYPE = 3 THEN PI.PIDATE ELSE INV.TAXDATE END"
DA(4) = "CASE WHEN PI.PITYPE = 3 THEN PI.PIDATE ELSE PB.PROCESSDATE END"

INNER_JOIN = ""
IF (IDATE = "") THEN
	IDATE = 1
	REQUESTED_DATE = DA(1)
ELSE
	REQUESTED_DATE = DA(IDATE)
	IF (CINT(IDATE) > 2) THEN
		INNER_JOIN = " LEFT JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = PI.ORDERITEMID LEFT JOIN F_INVOICE INV ON INV.INVOICEID = FOTI.INVOICEID LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID "		
	END IF
END IF

Dim SC(12)
'SC(1) = " AND PI.active = 1 " ' ALL
SC(1) = " AND PI.active = 1 AND PI.PISTATUS = 0 " 'QUEUED
SC(2) = " AND PI.active = 1 AND PI.PISTATUS > 0 AND PI.PISTATUS < 9 " 'ORDERED		
SC(3) = " AND PI.active = 1 AND PI.PISTATUS = 9 " 'RECONCILED
SC(4) = " AND PI.active = 1 AND PI.PISTATUS > 9 " 'PAID
SC(5) = " AND PI.active = 1 AND PI.PISTATUS = 12 " ' PAID DD		
SC(6) = " AND PI.active = 1 AND PI.PISTATUS = 11 " ' PAID BY BACS		
SC(7) = " AND PI.active = 1 AND PI.PISTATUS = 10 " ' PAID BY CHEQUE
		
SC(8) = " AND PI.active = 0 " ' CANCELLED		

DIM SCT(12)
'SCT(1) = "ALL STATUS'S"
SCT(1) = "QUEUED"
SCT(2) = "ORDERED"
SCT(3) = "RECONCILED"
SCT(4) = "PAID"
SCT(5) = "PAID VIA DD"
SCT(6) = "PAID VIA BACS"
SCT(7) = "PAID BY CHEQUE"
SCT(8) = "CANCELLED"


ISTATUS = Request("ISTATUS")
if (ISTATUS = "" OR NOT ISnUMERIC(ISTATUS)) then
	FilterText = SC(2)
	ISTATUS = 2
	ISTATUSTEXT = SCT(2)
else
	FilterText = SC(CINT(ISTATUS))
	ISTATUSTEXT = SCT(CINT(ISTATUS))	
end if

'this is the number of decimal places to display
IDEC = Request("IDEC")
if (IDEC = "" OR NOT isNUMERIC(IDEC)) then
	IDEC = 2
	CellWidth = "85"
else
	IDEC = CInt(Request("IDEC"))
	if (IDEC = 2) then
		CellWidth = "85"	
	else
		CellWidth = "70"		
	end if
end if

Dim TY(4)
TY(1) = "  " ' ALL
TY(2) = " AND PI.PITYPE = 2 " 'NON - REPAIR
TY(3) = " AND PI.PITYPE = 1 " 'REPAIR
TY(4) = " AND PI.PITYPE = 3 " 'REPAIR

Dim TYT(4)
TYT(1) = "ALL TYPES"
TYT(2) = "REPAIR"
TYT(3) = "NON REPAIR"
TYT(4) = "PETTY CASH"

ITYPE = Request("ITYPE")
if (ITYPE = "" OR NOT ISNUMERIC(ITYPE)) then
	FilterText = FilterText & TY(1)
	ITYPE = 1
	ITYPETEXT = TYT(1)
else
	FilterText = FilterText & TY(CINT(ITYPE))
	ITYPETEXT = TYT(CINT(ITYPE))	
end if
%>
<%
set rsTheYears = Server.CreateObject("ADODB.Recordset")
rsTheYears.ActiveConnection = RSL_CONNECTION_STRING
rsTheYears.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears"
rsTheYears.CursorType = 0
rsTheYears.CursorLocation = 2
rsTheYears.LockType = 3
rsTheYears.Open()
rsTheYears_numRows = 0
%>
<%
set rsFundRows = Server.CreateObject("ADODB.Recordset")
rsFundRows.ActiveConnection = RSL_CONNECTION_STRING
rsFundRows.Source = "SELECT HE.DESCRIPTION, HE.HEADID, SUM(PI.GROSSCOST) as totalpurchases, Month(convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103)) as themonth "&_
      "FROM F_PURCHASEITEM PI " & INNER_JOIN & ", F_EXPENDITURE EX, F_HEAD HE ,F_DEVELOPMENT_HEAD_LINK L	,dbo.PDR_DEVELOPMENT p "&_
      "WHERE  l.HEADID = he.HEADID and p.DEVELOPMENTID = l.DEVELOPMENTID and PI.EXPENDITUREID = EX.EXPENDITUREID AND "&_
           "EX.HEADID = HE.HEADID AND HE.COSTCENTREID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID') "&_
           " " & FilterText & "  and isnull(p.companyid,1) = " & Company &_
		"AND convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103) >= '" & yearstart & "' AND convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103) <= '" & yearend & "' "&_
	"group by HE.DESCRIPTION, HE.HEADID, Month(convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103)) "&_
	"order by HE.DESCRIPTION, themonth"
rsFundRows.CursorType = 0
rsFundRows.CursorLocation = 2
rsFundRows.LockType = 3
rsFundRows.Open()
rsFundRows_numRows = 0

    Call OpenDB()
 
Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", null, Company, NULL, "textbox200", " style='width:200px'")	

Call CloseDB()
%>
<HTML>
<HEAD>
<TITLE>RSL Finance --> Reports --> Reconciled Purchases</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language = "javascript">
<!--
function showRows(toShow){
	if (toShow.style.display == "block")
		toShow.style.display='none';
	else
		toShow.style.display = "block";
	}

var showMode = 'table-cell';
if (document.all) showMode='block';
function toggleVis(myVal, hide){
	cells1 = document.getElementsByName('tcol' + myVal);
	cells2 = document.getElementsByName('tcol' + (myVal+1));
	cells3 = document.getElementsByName('tcol' + (myVal+2));
	if (hide)
		mode = 'none';
	else
		mode = showMode;
	if (cells1.length){
		for(j = 0; j < cells1.length; j++) {
			cells1[j].style.display = mode;
			}
		}
	if (cells2.length){
		for(j = 0; j < cells2.length; j++) {
			cells2[j].style.display = mode;
			}
		}
	if (cells3.length){
		for(j = 0; j < cells3.length; j++) {
			cells3[j].style.display = mode;				
			}
		}
	}

function goGo(){
	thisForm.submit();
}

function SetSelect(){
	thisForm.IDATE.value = "<%=IDATE%>"
	thisForm.IDEC.value = "<%=IDEC%>"
	thisForm.ISTATUS.value = "<%=ISTATUS%>"
	thisForm.ITYPE.value = "<%=ITYPE%>"	
	}		

function UpdateDateSelect(ItemList){
	CurrentIndex = thisForm.IDATE.selectedIndex
	var OptionList = new Array("","<option value=\"1\">ORDER DATE</option>","<option value=\"2\">RECONCILE DATE</option>", "<option value=\"3\">TAXDATE</option>", "<option value=\"4\">PAYMENT DATE</option>")
	ItemOptions = ""
	ItemArray = ItemList.split(",")
	for (i=0; i<ItemArray.length; i++)
		ItemOptions += OptionList[ItemArray[i]]
	thisForm.IDATE.outerHTML = "  <select name=\"IDATE\" class=\"textbox\" style=\"width:124px\">" + ItemOptions + "</select>"
	if (thisForm.IDATE.options.length-1 < CurrentIndex)
		thisForm.IDATE.selectedIndex = 0
	else
		thisForm.IDATE.selectedIndex = CurrentIndex
	}

function CheckDate(){
	CurrentVal = thisForm.ISTATUS.value
	if (CurrentVal != "") 
		CurrentVal = parseInt(CurrentVal, 10)
	switch (CurrentVal){
		case 1 : UpdateDateSelect("1");break;
		case 2 : UpdateDateSelect("1");break;
		case 3 : UpdateDateSelect("1,2,3");break;
		case 4 : UpdateDateSelect("1,2,3,4");break;
		case 5 : UpdateDateSelect("1,2,3,4");break;
		case 6 : UpdateDateSelect("1,2,3,4");break;
		case 7 : UpdateDateSelect("1,2,3,4");break;
		case 8 : UpdateDateSelect("1");break;
		case 9 : UpdateDateSelect("1,2,3,4");break;
		}
	}		
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(5);preloadImages();SetSelect()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->  
	  <form name=thisForm>
  <b>DEVELOPMENTS.</b> This report is based on purchase items, it can be filtered 
  using the select boxes below. Click 'GO' to update results.<br>
  <br><b>		HEAD --> EXPENDITURE</b><br>
<table><tr><td>
<table width=483 bgcolor=beige style='border:1px dotted #133e71; height: 100px'><tr><td>
  <b>Company :</b>&nbsp;<%=lstCompany %><br><br>
  <b>YEAR :</b>&nbsp;
  <select name=fiscalyear class="RSLBlack" style='width:200px'>
                <option value="">Please Select</option>
                <%
While (NOT rsTheYears.EOF)
isSelected = ""
if (CStr(fiscalyear) = CStr(rsTheYears.Fields.Item("YRange").Value)) Then
	isSelected = " selected"
end if
%>
                <option value="<%=(rsTheYears.Fields.Item("YRange").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YStart").Value) & " - " & (rsTheYears.Fields.Item("YEnd").Value)%></option>
                <%
  rsTheYears.MoveNext()
Wend
If (rsTheYears.CursorType > 0) Then
  rsTheYears.MoveFirst
Else
  rsTheYears.Requery
End If
%>
              </select>
  &nbsp;<b>STATUS :</b> 
  <select name="ISTATUS" class="textbox" ONCHANGE="CheckDate()">
    <option value="1">Queued</option>
    <option value="2">Ordered</option>
    <option value="3">Reconciled</option>
    <option value="4">Paid (ALL)</option>
    <option value="5">Paid via DD</option>
    <option value="6">Paid via BACS</option>
    <option value="7">Paid via Cheque</option>
    <option value="9">Paid via Cash</option>
    <option value="">-------</option>
    <option value="8">Cancelled</option>
  </select>
  &nbsp;<br><br><b>DATE :</b>&nbsp;
	<%=IDATE_STRING%>
  &nbsp;<b>TYPE :</b> 
  <select name="ITYPE" class="textbox">
    <option value="1">ALL</option>
    <option value="2">Repair</option>
    <option value="3">Non - Repair</option>
    <option value="4">Petty Cash</option>
  </select>
  &nbsp;<b>DECIMALS :</b> 
  <select name="IDEC" class="textbox">
    <option value="0">0</option>
    <option value="2">2</option>
  </select>  
 &nbsp;</td><td><input type=button class="rslbutton" onclick="goGo()" value=" GO " style='height:30px'>
</td></tr></table>
</td><td>
<TABLE height=56 bgcolor=beige style='border:1px dotted #133e71; height: 100px'><TR>
            <TD><b>NOTE:</b> Previously this report only used the <b>order date</b> 
              to compare against the Year. Now depending on the status, you can 
              select other dates which will shift the figures accordingly.</td>
          </tr></table> 
</td></tr></table>
              <br>
              <table bgcolor=#FFFFFF style='border-collapse:collapse;border-right:1px solid silver' border=1>
		<%
		Dim TheMonthlyTotals (14)
		for i=0 to Ubound(TheMonthlyTotals)
			TheMonthlyTotals(i) = 0
		next
				
		MonthLength = UBound(WriteOrder)
		Response.Write "<TR><td colspan=20 align=center><b><font color=black>[" & ISTATUSTEXT & "]  [" & ITYPETEXT & "]  <font color=gray>Purchase Report for Financial Year: " & yearstart & " - " & yearend &"</b></TD></TR>"
		Response.Write "<TR><TD nowrap width=200px>&nbsp;</TD>"
		For e=0 to MonthLength

			if (e < 3) Then
				Response.Write "<TD align=center nowrap width=" & CellWidth & "px id='tcol"&e&"'>" & MonthName(WriteOrder(e), true) & "</TD>"
			else
				Response.Write "<TD align=center nowrap width=" & CellWidth & "px id='tcol"&e&"' style='display:none'>" & MonthName(WriteOrder(e), true) & "</TD>"
			end if			
			Select Case e
				Case 2		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=" & CellWidth & "px align=center onclick='toggleVis(3,true);toggleVis(6,true);toggleVis(9,true);toggleVis(0)'>Q1</TD>"		
				Case 5		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=" & CellWidth & "px align=center onclick='toggleVis(0,true);toggleVis(6,true);toggleVis(9,true);toggleVis(3)'>Q2</TD>"		
				Case 8		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=" & CellWidth & "px align=center onclick='toggleVis(3,true);toggleVis(0,true);toggleVis(9,true);toggleVis(6)'>Q3</TD>"		
				Case 11		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=" & CellWidth & "px align=center onclick='toggleVis(3,true);toggleVis(6,true);toggleVis(0,true);toggleVis(9)'>Q4</TD>"		
				Case MonthLength  Num = (MonthLength\3)*3
								  HideTog = ""
								  for y=2 to 11 Step 3
								  	if y < MonthLength Then
										HideTog = HideTog & "toggleVis(" & y-2&", true);"
									end if
								  Next
	       						  Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=" & CellWidth & "px align=center onclick='"&HideTog&"toggleVis(" & Num & ")'>Q"&(Num/3+1)&"</TD>"								  
			End Select			
		Next
		Response.Write "<TD width=" & CellWidth & "px nowrap align=center>Total</TD></TR>"
		%>
		<%
		Function WriteAQuarterSum(StartQuarter, EndQuarter)
			QuarterTotal = 0
			For w=StartQuarter to EndQuarter
				tempMonthTotal =  TheMonthlyTotals(WriteOrder(w))
				if (tempMonthTotal <> "") then
					QuarterTotal = QuarterTotal + tempMonthTotal
				End if
			Next
			Response.Write "<TD align=right bgcolor=white><font color=blue><b>" & FormatNumber(QuarterTotal,IDEC) & "</b></TD>"			
		End Function
				
		Function WriteAQuarter(StartQuarter, EndQuarter, isHead)
			QuarterTotal = 0
			For w=StartQuarter to EndQuarter
				tempMonthTotal =  FundLine(WriteOrder(w))
				if (tempMonthTotal <> "") then
					QuarterTotal = QuarterTotal + tempMonthTotal
				End if
			Next
			if (isHead) then
				Response.Write "<TD align=right bgcolor=lightgreen>" & FormatNumber(QuarterTotal,IDEC) & "</TD>"
			else
				Response.Write "<TD align=right bgcolor=green><font color=white><b>" & FormatNumber(QuarterTotal,IDEC) & "</b></TD>"			
			end if
		End Function
		
		Function WriteTotal (isHead)
			lineTotal = 0
			For w=0 to MonthLength
				tempMonthTotal =  FundLine(WriteOrder(w))
				if (tempMonthTotal <> "") then
					lineTotal = lineTotal + tempMonthTotal
					if (NOT isHead) then
						TheMonthlyTotals(WriteOrder(w)) = TheMonthlyTotals(WriteOrder(w)) + tempMonthTotal
					end if
				End if
				FundLine(WriteOrder(w)) = ""
			Next
			finalTotal = finalTotal + lineTotal
			Response.Write "<TD align=right>" & FormatNumber(lineTotal,IDEC) & "</TD>"		
		End Function
		
		Function WriteAFundLine (isHead)
			if (isHead) then
				Response.Write "<TR color=blue><TD nowrap>&nbsp;&nbsp;" & prevHead_Name & "</TD>"
			else
				Response.Write "<TR style='cursor:hand' onclick=""showRows(RR" & prevFund_ID & ")"" bgcolor=beige><TD nowrap><b>" & prevFund_Name & "</b></TD>"
			end if			
			For j=0 to MonthLength
				if (j < 3) then
					Response.Write "<TD align=right id='tcol"&j&"'>" & FundLine(WriteOrder(j)) & "</TD>"
				else
					Response.Write "<TD align=right id='tcol"&j&"' style='display:none'>" & FundLine(WriteOrder(j)) & "</TD>"
				end if				
				Select Case j
					Case 2		WriteAQuarter 0, 2, isHead		
					Case 5		WriteAQuarter 3, 5, isHead		
					Case 8		WriteAQuarter 6, 8, isHead		
					Case 11		WriteAQuarter 9, 11, isHead
					Case MonthLength  WriteAQuarter (MonthLength\3)*3, MonthLength, isHead
				End Select
			Next
			Writetotal isHead
			Response.Write "</TR>"			
		End Function
		
		Dim FundLine(12)
		Dim Totals(5)
		Dim Head_Name, prevHead_Name
		Dim prevFund_ID, finalTotal
		finalTotal = 0
		prevFund_Name = -1
		For i=0 to MonthLength
			FundLine(i) = ""
		Next
		if (rsFundRows.EOF) then
			Response.Write "<TR><TD colspan=20 align=center><b><font color=gray>No purchases made for this financial year</b></td></tr>"
		end if		
		while (NOT rsFundRows.EOF) 
			Fund_Name = rsFundRows("DESCRIPTION")		  	
			If (prevFund_Name <> -1 and Fund_Name <> prevFund_Name) then
				WriteAFundLine false
				GetHeadsForFund prevFund_ID			
			End if
			prevFund_Name = Fund_Name
			prevFund_ID = rsFundRows("HEADID")
			FundLine(CInt(rsFundRows("themonth"))) = FormatNumber(rsFundRows("totalpurchases"),IDEC)
			rsFundRows.moveNext()
		Wend
		If (prevFund_Name <> -1) then
			WriteAFundLine false
			GetHeadsForFund prevFund_ID			
		End if		

		Response.Write "<TR color=blue><TD nowrap><font color=blue><b>Year Total</b></font></TD>"
		For j=0 to MonthLength
			if (j < 3) then
				Response.Write "<TD align=right id='tcol"&j&"'></TD>"
			else
				Response.Write "<TD align=right id='tcol"&j&"' style='display:none'></TD>"
			end if
			Select Case j
				Case 2		WriteAQuarterSum 0, 2		
				Case 5		WriteAQuarterSum 3, 5		
				Case 8		WriteAQuarterSum 6, 8		
				Case 11		WriteAQuarterSum 9, 11
				Case MonthLength  WriteAQuarterSum (MonthLength\3)*3, MonthLength				
			End Select
		Next
		Response.Write "<TD align=right><b><FONT COLOR=#133E71>" & FormatNumber(finalTotal/2,IDEC) & "</FONT></b></TD>"
		Response.Write "</TR>"			

		Function GetHeadsForFund (HEADID)
			set rsHeadRows = Server.CreateObject("ADODB.Recordset")
			rsHeadRows.ActiveConnection = RSL_CONNECTION_STRING
			rsHeadRows.Source = "SELECT EX.DESCRIPTION AS EXPNAME, isnull(SUM(PI.GROSSCOST),0) as totalpurchases, "&_
					"Month(convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103)) as themonth "&_
					"FROM F_PURCHASEITEM PI " & INNER_JOIN & ", F_EXPENDITURE EX, F_HEAD HE "&_
					"WHERE PI.EXPENDITUREID = EX.EXPENDITUREID AND "&_
					"EX.HEADID = HE.HEADID "&_
					" " & FilterText & " "&_
					"AND convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103) >= '" & yearstart & "' AND convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103) <= '" & yearend & "' "&_
					"AND HE.HEADID = " & HEADID & " " &_
				"group by EX.DESCRIPTION, Month(convert(smalldatetime,convert(varchar," & REQUESTED_DATE & ",103),103)) "&_
				"order by EX.DESCRIPTION"
			rsHeadRows.CursorType = 0
			rsHeadRows.CursorLocation = 2
			rsHeadRows.LockType = 3
			rsHeadRows.Open()
			rsHeadRows_numRows = 0
			
			prevHead_Name = -1
			Response.Write "<tbody id='RR" & prevFund_ID & "' style='display:none'>"
			while (NOT rsHeadRows.EOF) 
				Head_Name = rsHeadRows("EXPNAME")		  	
				If (prevHead_Name <> -1 and Head_Name <> prevHead_Name) then
					WriteAFundLine true
				End if
				prevHead_Name = Head_Name
				FundLine(CInt(rsHeadRows("themonth"))) = FormatNumber(rsHeadRows("totalpurchases"),IDEC)
				rsHeadRows.moveNext()
			Wend
			If (prevHead_Name <> -1) then
				WriteAFundLine true
			End if
			Response.Write "</tbody>"
			rsHeadRows.close()						
		End Function
		%>
		</table>
		</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
rsFundRows.Close()
%>
<%
rsTheYears.Close()
%>