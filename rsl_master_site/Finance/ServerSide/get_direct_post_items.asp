<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, customer_id, str_journal_table, str_color, item_type, item_id
	
	OpenDB()
	tenancy_id = Request.FORM("hid_TENANTNUMBER")
	
	item_id = get_item(Request.Form("sel_DIRECTPOSTITEMS"))
	build_journal()

	CloseDB()

	// get the correct item type associated with this posting to ebable use in sql
	Function get_item(item_type)
	
		Dim SQL
		
		SQL = "SELECT F_CORRESPONDINGTYPE FROM F_DIRECTPOSTITEMS WHERE ITEMID = " & item_type
		Call OpenRs(rsSet, SQL)
		if not rsSet.EOF Then
			get_item = rsSEt(0)
			Exit Function
		Else
			get_item = -1
			Exit Function
		End If
		CloseRs(rsSet)
	
	End Function

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT TOP 100 ISNULL(CONVERT(NVARCHAR, J.TRANSACTIONDATE, 103) ,'') AS F_TRANSACTIONDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTSTARTDATE, 103) ,'') AS PAYMENTSTARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTENDDATE, 103) ,'') AS PAYMENTENDDATE, " &_
					"			ISNULL(I.DESCRIPTION, ' ') AS ITEMTYPE, " &_
					"			ISNULL(P.DESCRIPTION, ' ') AS PAYMENTTYPE, " &_
					"			ISNULL(J.AMOUNT, 0) AS AMOUNT, " &_
					"			ISNULL(J.ISDEBIT, 1) AS ISDEBIT, " &_
					"			ISNULL(S.DESCRIPTION, ' ') AS STATUS, " &_
					"			J.JOURNALID " &_
					"FROM	 	F_RENTJOURNAL J " &_
					"			LEFT JOIN F_ITEMTYPE I ON J.ITEMTYPE = I.ITEMTYPEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON J.PAYMENTTYPE = P.PAYMENTTYPEID  " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
					"WHERE		J.TENANCYID = " & tenancy_id & " AND J.PAYMENTTYPE = " & item_id & " " &_ 
					"			AND J.STATUSID NOT IN (10,11) " &_
					"ORDER		BY J.TRANSACTIONDATE DESC "
		response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
		
			str_journal_table = str_journal_table &_
			
			 	"<TR " & str_color & " >" &_
				"<TD>" & rsSet("F_TRANSACTIONDATE") & "</TD>" &_
				"<TD>" & rsSet("ITEMTYPE") & "</TD>" &_
				"<TD>" & rsSet("PAYMENTTYPE") & "</TD>" &_
				"<TD align=right nowrap width=90>" & FormatCurrency(rsSet("AMOUNT")) & "&nbsp;&nbsp;</TD>" &_
				"<TD>" & rsSet("STATUS")  & "</TD>" &_
				"<TD><input type=RADIO name='attach' onclick='parent.ATTACHITEM(" & rsSet("JOURNALID") & ", " & FormatNumber(ABS(rsSet("AMOUNT")),2,-1,0,0) & ")'></TD>" &_
				"<TR>"
				
				
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist for chosen payment type.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
function ReturnData(){
	parent.ACCOUNT_DIV.innerHTML = table_div.innerHTML
	parent.STATUS_DIV.style.visibility = "hidden";
	parent.RSLFORM.txt_AMOUNT.value = "";
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="ReturnData()" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
<DIV ID=table_div>
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD>
		<TR >
			<TD height=20 STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=90><B>Date</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B>Item</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B>Payment Type</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B>Amount</B>&nbsp;&nbsp;</TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B>Status</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120><B>Attach</B></TD>
		</TR>
		</THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</DIV>
</BODY>
</HTML>	
