<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim credit_list, credit_sql, CreditSum, CreditCount
	credit_list = Request("hid_CreditList")

'	IF ANY CREDITS HAVE BEEN SELECTED WE NEED TO INCLUDE THEM IN ALL PROCESSES EXCEPT THE ACTUAL BACS FILE CREATION
	If credit_list = "" Then
		credit_sql = ""
	Else
		credit_sql = "OR (INV.INVOICEID IN (" & credit_list & ")) "
	End If

    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "

	POFilter = ""
	If (Request("PROCESSINGDATE") <> "") Then
		ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
		RequiredDate = CDate(Request("PROCESSINGDATE"))
	Else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	End If

	Call OpenDB()
	'THIS STATEMENT WILL LOCK RECONCILING FOR A MOMENT....
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 1 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
	Conn.Execute (SQL)
	
	'NEXT NEED TO CHECK THAT THE DATA HAS NOT CHANGED SINCE IT WAS SENT OVER...
	SQL = "SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALCOST, COUNT(INV.INVOICEID) AS TOTALCOUNT " &_
			"FROM F_INVOICE INV " &_
			"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
			"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
			"	WHERE (INV.ISCREDIT = 0 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND " &_
			"	PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 9 ) AND " &_
			"	DATEADD(Day, - 9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) <= '" & FormatDateTime(RequiredDate,1) & "' " & CompanyFilter
	Call OpenRs(rsCheck, SQL)
	TotalSum = 0
	TotalCount = 0
	if (NOT rsCheck.EOF) then
		TotalSum = rsCheck("TOTALCOST")
		TotalCount = rsCheck("TOTALCOUNT")
	end if
	Call CloseRs(rsCheck)
	'RW "<br/>" & TOTALSUM & "   " & TOTALCOUNT
	'THIS PART CHECKS THE PRUCHASE ITEM DATA AND REDIRECTS TO THE PREVIOUS PAGE IF IT IS NOT MATCHING
	IF ( NOT (CDbl(TotalSum) = Cdbl(Request("TotalSum")) AND CInt(TotalCount) = CInt(Request("TotalCount"))) ) then
		'THIS STATEMENT WILL UNLOCK RECONCILING
		SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
		Conn.Execute (SQL)
		'rw "<br/>no po match"
		Response.Redirect "../BacsList.asp?PODate=" & ProcessingDate & "&ER89" & Replace(Date, "/", "") & "=1"
	End If
	
	' CHECK ALSO THAT CREDIT NOTE VALUE HAS NOT CHANGED FROM PREVIOUS PAGE IF ANY CREDIT LINES CHOSEN
	If credit_list <> "" Then
		SQL = "SELECT ISNULL(SUM(ABS(CR.GROSSCOST)),0) AS CREDITSUM, COUNT(*) AS CREDITCOUNT   " &_
				"FROM 	F_INVOICE INV   " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID   " &_
				"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID   " &_
				"		INNER JOIN  " &_
				"		    (SELECT SUM(GROSSCOST) AS GROSSCOST, O.INVOICEID, C.CNNAME " &_
				"				FROM F_CREDITNOTE C " &_
				"				INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"				INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CP.ORDERITEMID   " &_
				"				INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = PI.ORDERITEMID " &_
				"				 WHERE PI.ACTIVE = 1 GROUP BY O.INVOICEID, C.CNNAME ) CR ON  " &_
				"				 CR.INVOICEID = INV.INVOICEID " &_
				"WHERE 	INV.ISCREDIT = 1 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND   " &_
				"		S.PAYMENTTYPE = 9 AND INV.INVOICEID IN (" & credit_list & " )  "  & CompanyFilter

		'RW SQL
		Call OpenRs(rsCheck, SQL)
		CreditSum = 0
		CreditCount = 0
		if (NOT rsCheck.EOF) then
			CreditSum = rsCheck("CREDITSUM")
			CreditCount = rsCheck("CREDITCOUNT")
		end if
		Call CloseRs(rsCheck)


		'THIS PART CHECKS THE CREDIT NOTE DATA AND REDIRECTS TO THE PREVIOUS PAGE IF IT IS NOT MATCHING
		'IF ((NOT (CDbl(CreditSum) = Cdbl(Request("CreditSum"))))  AND (NOT (CInt(CreditCount) = CInt(Request("CreditCount"))))  )  then
		
		
		IF ( NOT (CDbl(CreditSum) = Cdbl(Request("CreditSum")) AND CInt(CreditCount) = CInt(Request("CreditCount"))) ) then
		'IF ((NOT (CDbl(CreditSum) = Cdbl(Request("CreditSum"))))  AND (NOT (CInt(CreditCount) = CInt(Request("CreditCount"))))  )  then
			'THIS STATEMENT WILL UNLOCK RECONCILING
			SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
			Conn.Execute (SQL)
			Response.Redirect "../BacsList.asp?PODate=" & ProcessingDate & "&ER89" & Replace(Date, "/", "") & "=2&Company=" & Company
		End If
		
	End If
	
	' IF VALID FIGURES WE ARRIVE HERE	
	ORIGINATORNUMBER = "658670"
	orderBy = "NAME ASC"
	
	' CREATE BACS CSV FILE
	SQLCODE = "SELECT 	ORGID, COUNT(INV.INVOICEID) AS THECOUNT, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, NAME, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER,  " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
				"FROM 	F_INVOICE INV " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
				"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
				"WHERE 	(INV.CONFIRMED = 1 AND INV.ISCREDIT = 0 AND INV.PAYMENTMETHOD IS NULL " &_
				"		AND PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 9 " &_
				"		AND DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) <= '" & FormatDateTime(RequiredDate,1) & "') " & credit_sql & CompanyFilter &_
				"GROUP 	BY ORGID, NAME, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER	 " &_
				"ORDER 	BY " + Replace(orderBy, "'", "''") + ""	
	'RW SQLCODE & "<br/><br/>"
	Set fso = CreateObject("Scripting.FileSystemObject")
	
	'GP_curPath = Request.ServerVariables("PATH_INFO")
	'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
	'	GP_curPath = GP_curPath & "/"
	'end if
    
    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS) 
	
	CurrentDate = FormatDateTime(Date,2)
	CurrentDate = Replace(CurrentDate, "/", "")
	CurrentDate = Replace(CurrentDate, " ", "")
	
	RedirectProcessingDate = ProcessingDate
	ProcessingDate = Replace(ProcessingDate, "/", "")
	ProcessingDate = Replace(ProcessingDate, " ", "")
	
	'FullPath = Trim(Server.mappath(GP_curPath)) & "\BacsFiles\"
    FullPath = doc_DEFAULTPATH & "\finance\BacsFiles\"
	FileName = "INV-" & Company & "C" & CurrentDate & "-" & "P" & ProcessingDate 

	If (fso.FileExists(FullPath & FileName & ".csv")) Then
		GP_FileExist = true
	End If   
	if GP_FileExist then
		Begin_Name_Num = 0
		while GP_FileExist    
			Begin_Name_Num = Begin_Name_Num + 1
			TempFileName = FileName & "_" & Begin_Name_Num
			GP_FileExist = fso.FileExists(FullPath & TempFileName & ".csv")
		wend  
		FileName = TempFileName
	end if
    'response.Write(SQLCODE)
    'response.End()
	Set MyFile= fso.CreateTextFile(FullPath & FileName & ".csv", True)
	
	Call OpenRS(rsLoader, SQLCODE)
	
	BadDataCount = 0
	BadDataTotal = 0
	BadInvoiceCount = 0
	GoodDataCount = 0
	GoodDataTotal = 0
	GoodInvoiceCount = 0
	'MyFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"
	while NOT rsLoader.EOF
		SORTCODE = rsLoader("SORTCODE")
		ACCOUNTNUMBER = rsLoader("ACCOUNTNUMBER")
		ACCOUNTNAME = rsLoader("ACCOUNTNAME")
		if (SORTCODE = "" OR ACCOUNTNUMBER = "" OR ACCOUNTNAME = "" OR isNull(ACCOUNTNAME) OR isNULL(SORTCODE) OR isNULL(ACCOUNTNUMBER)) then
			if (BadDataCount = 0) then
				Set MyBadFile= fso.CreateTextFile(FullPath & FileName & "-BAD.csv", True)
				'MyBadFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"						
			end if
			BadDataCount = BadDataCount + 1
			BadInvoiceCount = BadInvoiceCount + rsLoader("THECOUNT")		
			MyBadFile.WriteLine rsLoader("ORGID") & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
			BadDataTotal = BadDataTotal + rsLoader("PAYMENTAMOUNT")		
		else
			GoodDataCount = GoodDataCount + 1
			GoodInvoiceCount = GoodInvoiceCount + rsLoader("THECOUNT")				
			MyFile.WriteLine rsLoader("ORGID") & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
			GoodDataTotal = GoodDataTotal + rsLoader("PAYMENTAMOUNT")
		end if
		rsLoader.moveNext
	wend
	CloseRs(rsLoader)
	MyFile.Close
	if (BadDataCount > 0) then
		MyBadFile.Close
	end if
	
'	'FINALLY ADD A ROW INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
	SQL = ("SET NOCOUNT ON;INSERT INTO F_BACSDATA (FILEDATE, FILENAME, FILETYPE, FILECOUNT, INVOICECOUNT, TOTALVALUE, PROCESSDATE, ISFILE, COMPANYID) VALUES (" &_
				"'" & FormatDateTime(RequiredDate,1) & "', " &_
				"'" & FileName & ".csv" & "', " &_
				"" & 1 & ", " &_
				"" & GoodDataCount & ", " &_
				"" & GoodInvoiceCount & ", " &_				
				"" & GoodDataTotal & ", " &_
				"'" & FormatDateTime(Date,1) & "', 1," & Company & ");SELECT SCOPE_IDENTITY() AS DATAID ")
	Call OpenRs(rsDATA, SQL)
	DATAID = rsDATA("DATAID")
	Call CloseRs(rsDATA)

'	'The ORDER OF THE FOLLOWING STATEMENTS IS VERY IMPORTANT, AS THE DATAID IS USED TO GET THE REFERENCE FOR THE NEXT TWO STATEMENTS.
'	'THIS STATEMENT WILL INSERT N INVOICE ENTRIES INTO F_POBACS.
	SQL = "INSERT INTO F_POBACS (INVOICEID, DATAID, PROCESSEDBY, PROCESSDATE) " &_
			"SELECT INV.INVOICEID, " & DATAID & ", " & SESSION("USERID") & ", '" & FormatDateTime(Date,1) & "' FROM F_INVOICE INV " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
				"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
				"WHERE (INV.CONFIRMED = 1 AND INV.ISCREDIT = 0 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 9 AND " &_
					"DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) <= '" & FormatDateTime(RequiredDate,1) & "') " & credit_sql &_
					"AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
					"AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_
					"AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''" & CompanyFilter
	'RW SQL & "<br/><br/>"
	Conn.Execute (SQL)
	
''	'THIS STATEMENT WILL UPDATE THE RESPECTIVE PURCHASE ITEMS AND CHANGE THEIR STATUS TO PAID BY BACS
	SQL = "UPDATE F_PURCHASEITEM SET PISTATUS = 11, PAIDON = '" & FormatDateTime(Date,1) & "' " &_
				"FROM F_PURCHASEITEM PI " &_
				"INNER JOIN F_ORDERITEM_TO_INVOICE FITI ON FITI.ORDERITEMID = PI.ORDERITEMID " &_
				"INNER JOIN F_INVOICE INV ON INV.INVOICEID = FITI.INVOICEID " &_
				"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
				"WHERE PB.DATAID = " & DATAID  & CompanyFilter			
	'RW SQL & "<br/><br/>"
	Conn.Execute (SQL)
	
''	'THIS STATEMENT WILL UPDATE THE INVOICE DATA 
	SQL = "UPDATE F_INVOICE SET PAYMENTMETHOD = 9 " &_
				"FROM F_INVOICE INV " &_
				"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
				"WHERE PB.DATAID = " & DATAID	 & CompanyFilter				
'	RW SQL & "<br/><br/>"
	Conn.Execute (SQL)
	
	Conn.Execute "EXEC UPDATE_PURCHASEORDER_STATUS " & DATAID		
	Conn.Execute "EXEC UPDATE_CREDITNOTE_STATUS " & DATAID
	Conn.Execute "EXEC NL_INVOICE_PAYMENTS " & DATAID				
	
	IF (BadDataCount > 0) THEN
'		'also ADD A ROW FOR BAD DATA INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
		Conn.Execute ("INSERT INTO F_BACSDATA (FILEDATE, FILENAME, FILETYPE, FILECOUNT, INVOICECOUNT, TOTALVALUE, PROCESSDATE, ISFILE, Companyid) VALUES (" &_
					"'" & FormatDateTime(RequiredDate,1) & "', " &_
					"'" & FileName & "-BAD.csv" & "', " &_
					"" & 2 & ", " &_
					"" & BadDataCount & ", " &_
					"" & BadInvoiceCount & ", " &_				
					"" & BadDataTotal & ", " &_
					"'" & FormatDateTime(Date,1) & "', 1, " & Company & ") ")																								
	END IF
	
'	'THIS STATEMENT WILL UNLOCK RECONCILING
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
	Conn.Execute (SQL)
	
	CloseDB()
	
	Response.Redirect "../BacsCSV.asp?PROCESSINGDATE=" & RedirectProcessingDate & "&Company=" & Company
%>