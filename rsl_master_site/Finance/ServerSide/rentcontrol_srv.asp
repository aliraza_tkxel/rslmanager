<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="/includes/database/adovbs.inc" -->
<!--#include virtual="/connections/db_connection.asp" -->
<%
	Dim action, tnum, str_data, isValid, success, redir, str_redirection, attach_id, ErrorText
	
	action = Request("action")
	
	tnum = Request.form("txt_TENANTSEARCH")
	if (tnum = "") then tnum = -1
	
	attach_id = Request("hid_ATTACHID")
	Response.Write attach_id
	 
	isValid = 0
	str_data = ""
	success = 0
	redir = 0
	ErrorText = ""
	
	OpenDB()
	if action = "GETWITHHBREF" then	
		getTenancyNumber()
		getTenantInfo() 		
	elseif action = "GET" Then 
		getTenantInfo() 
	else 

		post()
	end If
	
	CloseDB()

		function open_db(ByVal strConnect)
		Set conn = Server.CreateObject("ADODB.Connection")
			conn.Open(strConnect)
			set open_db = conn
		end function

		function create_cmd(ByVal conn,ByVal sp_name)

			Set cmd = Server.CreateObject("ADODB.Command")
			cmd.ActiveConnection = conn
			cmd.CommandText = sp_name
			cmd.CommandType = adCmdStoredProc
			set create_cmd = cmd
			end function

	Function getTenancyNumber()
		'this section gets the tenancyid from hb ref number		
		SQL = "SELECT TENANCYID FROM F_HBINFORMATION WHERE HBREF = '" & REQUEST("txt_CLAIMNUMBER") & "'"
		Call OpenRs(rsHBREF, SQL)
		if (NOT rsHBREF.EOF) then
			tnum = rsHBREF("TENANCYID")
		else
			tnum = -1
		end if
		CloseRs(rsHBREF)
	End Function
				
	Function getTenantInfo()
		'WHY WASTE TIME RUNNING THE SQL....
		if (tnum <> -1) then
		
			'START OF GET CUSTOMER DETAILS....
			CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
					"FROM C_CUSTOMERTENANCY CT " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
					"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & tnum
			Call OpenRs(rsCust, CustSQL)
			strUsers = ""
			CustCount = 0
			while NOT rsCust.EOF 
				if CustCount = 0 then
					strUsers = strUsers & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
					CustCount = 1
				else
					strUsers = strUsers & ", " & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
				end if					
				rsCust.moveNext()
			wend
			CloseRs(rsCust)
			SET rsCust = Nothing
			'END OF GET CUSTOMER DETAILS

			strSQL = "SELECT T.TENANCYID AS TTENANCYID, " &_
					"ISNULL(T.TENANCYID, '') AS TENANCYREF, " &_
					"ISNULL(P.PROPERTYID, '') AS PROPERTYID, " &_
					"ISNULL(P.HOUSENUMBER, '') AS HOUSENUMBER, " &_
					"ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
					"ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
					"ISNULL(P.ADDRESS3, ' ' ) AS ADDRESS3, " &_		
					"ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
					"ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
					"ISNULL(P.COUNTY , ' ' ) AS COUNTY, " &_
					"ISNULL(LA.DESCRIPTION , ' ' ) AS LOCALAUTHORITY " &_					
					"FROM C_TENANCY T " &_
					"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " &_
					"LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
					"WHERE T.TENANCYID = " & tnum
													
			Call OpenRS(rsSet, strSQL)	
			If not rsSet.EOF Then
				isValid = 1
				str_data = "<table><TR><TD width=80px><B>Tenancy No :</B></TD><TD>" & TenancyReference(rsSet("TTENANCYID")) & "</TD></TR>" &_
								"<TR><TD width=80px><B>Name :</B></TD><TD>" & strUsers & "</TD></TR>" &_
								"<TR><TD><B>Address :</B></TD><TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("ADDRESS2") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("ADDRESS3") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("TOWNCITY") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("COUNTY") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("POSTCODE") & "</TD></TR>" &_
								"<TR><TD><B>LA : </B></TD><TD>" & rsSet("LOCALAUTHORITY") & "</TD></TR></TABLE>"								
			End if
			CloseRs(rsSet)
		End if
		
		If str_data = "" then
			str_data = "<table valign=top><TR><TD>No Matching Records!!</TD></TR></TABLE>"
			'Reset TNUM to "" so that it does not return it into the textbox on the main page
			tnum = ""
		End If
		
	End Function

	function zero(strzero)
	if strzero = "" then
		strzero = csng(0)
	else
		strzero = csng(strzero)
	end if
	zero = strzero
	end function
	
	Function post()
	'get form info
	tnum = Request("txt_TENANTSEARCH")
	txt_processdate= request("txt_processdate")
	txt_from= request("txt_from")		
	txt_to= request("txt_to")
	txt_rent= zero(request("txt_rent"))
	txt_ineligible= zero(request("txt_ineligible"))
	txt_support= zero(request("txt_support"))
	txt_water= zero(request("txt_water"))
	txt_amount= zero(request("txt_amount"))
	txt_council= zero(request("txt_council"))
	txt_garage= zero(request("txt_garage"))
	txt_service= zero(request("txt_service"))
	



	strSQL = "SELECT ISNULL(P.PROPERTYID, '') as PROPERTYID " &_
					"FROM C_TENANCY T " &_
					"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"WHERE T.TENANCYID = " & tnum
													
	Call OpenRS(rsSet, strSQL)
	propertyid = rsSet("propertyid")
	CloseRs(rsSet)
	response.write propertyid
'on error resume next
Dim cmderror, param
set connerror = open_db(RSL_CONNECTION_STRING)
Set cmderror=server.CreateObject("ADODB.Command")
With cmderror
  .CommandType=adcmdstoredproc
  .CommandText = "F_POSTERROR"
  set .ActiveConnection=connerror
  set param = .createparameter("@RETURN_VALUE", adInteger, adParamReturnValue, 0)
  .parameters.append param
  set param = .createparameter("@paymenttypeid", adInteger, adParamInput, 0, request("sel_DIRECTPOSTITEMS"))
  .parameters.append param  
  set param = .createparameter("@tenancyid", adInteger, adParamInput, 0, request("txt_TENANTSEARCH"))
  .parameters.append param
  set param = .createparameter("@transactiondate", adDBTimeStamp, adParamInput, 0, cdate(txt_processdate))
  .parameters.append param
  set param = .createparameter("@amount", adCurrency, adParamInput, 0, txt_amount)
  .parameters.append param
  set param = .createparameter("@rent", adCurrency, adParamInput, 0, txt_rent)
  .parameters.append param
  set param = .createparameter("@services", adCurrency, adParamInput, 0, txt_service)
  .parameters.append param
  set param = .createparameter("@council", adCurrency, adParamInput, 0, txt_council)
  .parameters.append param
  set param = .createparameter("@water", adCurrency, adParamInput, 0, txt_water)
  .parameters.append param
  set param = .createparameter("@garage", adCurrency, adParamInput, 0, txt_garage)
  .parameters.append param
  set param = .createparameter("@support", adCurrency, adParamInput, 0,txt_support)
  .parameters.append param
  set param = .createparameter("@ineligible", adCurrency, adParamInput, 0, txt_ineligible)
  .parameters.append param
  set param = .createparameter("@totalrent", adCurrency, adParamInput, 0, txt_amount)
  .parameters.append param
  set param = .createparameter("@propertyid", adVarChar, adParamInput, 50, propertyid)
  .parameters.append param
  set param = .createparameter("@renttype", adInteger, adParamInput, 0, 1)
  .parameters.append param
   set param = .createparameter("@propertytype", adInteger, adParamInput, 0, 1)
  .parameters.append param 
  set param = .createparameter("@startpaymentdate", adDBTimeStamp, adParamInput, 0, cdate(request("txt_from")))
  .parameters.append param
  set param = .createparameter("@endpaymentdate", adDBTimeStamp, adParamInput, 0, cdate(request("txt_to")))
  .parameters.append param
  

  .execute ,,adexecutenorecords
end with
	
	'We are deliberately not trapping the error so the custom error checking page handles it.
	If Err.Number <> 0 Then
		
	ErrorText = "There was an error connecting to the server please try again. Your changes have not been saved." & Err.Description

	end if


	
	End Function
	'RSL_CONNECTION_STRING
	// change the status of the attached journal entry
	Function change_entry_status(int_f_status)
	
		SQL = "UPDATE F_RENTJOURNAL SET STATUSID = " & int_f_status & " WHERE JOURNALID = " & attach_id
		Conn.Execute(SQL)
		
	End Function
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
<% if ErrorText = "" then %>	
	// if actual posting
	<% if str_data = "" then %>	
		// redirect to CRM if required
		if (<%=redir%> != 0){
			parent.location.href = "<%=str_redirection%>"
			}
		parent.details.innerHTML = "&nbsp;";
		parent.ACCOUNT_DIV.innerHTML = "";
		parent.RSLFORM.reset();
		parent.RSLFORM.Detach.style.visibility = "hidden"
		parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
		parent.RSLFORM.btn_POST.disabled = true;
		parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Successful...</B></FONT>";
	<% else %>
		// otherwise tenant data retrieval
		parent.ATTACHID = 0;
		parent.details.innerHTML = "<%=str_data%>";
		parent.RSLFORM.sel_DIRECTPOSTITEMS.value = "";
		parent.RSLFORM.txt_AMOUNT.value = "";
		parent.RSLFORM.hid_TENANTNUMBER.value = "<%=tnum%>";
		parent.STATUS_DIV.style.visibility = "hidden";
		parent.is_a_valid_tenancy = <%=isValid%>;
		if (<%=isValid%> == 1){
			parent.RSLFORM.btn_POST.disabled = false;
			parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;			
			}
		else {
			parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
			parent.RSLFORM.btn_POST.disabled = true;		
			}
	<% end if %>
<% else %>
	alert("<%=ErrorText%>");
	parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Try again in a few seconds...</B></FONT>";	
	parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;
	parent.RSLFORM.btn_POST.disabled = false;		
<% end if %>	
	}
</script>
<body onload="ReturnData()">
</body>
</html>