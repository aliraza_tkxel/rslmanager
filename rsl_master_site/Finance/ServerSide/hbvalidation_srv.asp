<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim startdate, la, ttype
	Dim la_sql, status_sql, cnt, str_data, EndButton
	Dim PageTotal, GrantTotal, CurrentBalance
	startdate = Request("txt_START")
	la = Request("sel_LA")
	ScheduleID = Request("sel_SCHEDULEDATE")
	tenancy = Request("txt_TENANCYID")
	if (isNumeric(tenancy)) then
		tenancy = CLng(tenancy)
	else
		tenancy = ""
	end if
	
	hbref = Replace(Request("txt_HBREF"), "'", "''")
	
	// build local authority sql
	If la = "" Then 
		la_sql = ""
	Else
		la_sql = " AND L.LOCALAUTHORITYID = " & la & " "
	End If

	// build tenancy sql
	If tenancy = "" Then 
		tenancy_sql = ""
	Else
		tenancy_sql = " AND I.TENANCYID = " & tenancy & " "
	End If

	// build hbref sql
	If hbref = "" Then 
		hbref_sql = ""
	Else
		hbref_sql = " AND I.HBREF = '" & hbref & "' "
	End If
	
	// build status sql
	status_sql =  " AND J.STATUSID = 1 "

	orderby = Request("rdo_ORDER")
	if (orderby = 2) then
		OrderString = " FULLNAME, "
	else
		OrderString = " I.HBREF, "
	end if
	
	NextString = "&txt_START=" & startdate & "&sel_LA=" & la & "&sel_SCHEDULEDATE=" & ScheduleID & "&txt_TENANCYID=" & tenancy & "&txt_HBREF=" & hbref & "&rdo_ORDER=" & orderby
	'OpenDB()
	build_hb_val()
	'CloseDB()

	
	Function build_hb_val()
		
		Dim strSQL
		Const adCmdText = &H0001
		
		dim mypage
		mypage = Request("page")
		Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CInt(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" &_
					"<TD WIDTH=50><FONT STYLE='COLOR:WHITE'><B>Status</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Claim</B></FONT></TD>" &_
					"<TD WIDTH=100><FONT STYLE='COLOR:WHITE'><B>Name</B></FONT></TD>" &_
					"<TD WIDTH=200><FONT STYLE='COLOR:WHITE'><B>Address</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Start</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Finish</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Amount</B></FONT></TD>" &_
					"<TD><FONT STYLE='COLOR:WHITE'><B>&nbsp;V</B></FONT></TD>" &_
					"<TD><FONT STYLE='COLOR:WHITE'><B>&nbsp;A</B></FONT></TD><TD WIDTH=15></TD>" &_
				"</TR>" &_
				"<TR><TD HEIGHT='100%'></TD></TR>" 

		
		cnt = 0
		strSQL = 	"SELECT 	TC.TENANCYCLOSEDID, J.JOURNALID, ISNULL(I.HBREF,'') AS HREF, " &_
					"			ISNULL(LTRIM(LASTNAME), '') + ', ' + ISNULL(FIRSTNAME, '') AS FULLNAME, " &_
					"			CASE WHEN HOUSENUMBER IS NULL THEN ISNULL(p.ADDRESS1,'')  ELSE " &_
					"			ISNULL(p.HOUSENUMBER, '') + ' ' + ISNULL(p.ADDRESS1,'') END AS STREET, " &_
					"			ISNULL(p.TOWNCITY, '') AS TOWNCITY, " &_
					"			ISNULL(p.POSTCODE, '') AS POSTCODE, " &_					
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTSTARTDATE, 103), '') AS STARTDATE, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.PAYMENTENDDATE, 103), '') AS ENDDATE, " &_
					"			ISNULL(J.AMOUNT, 0) AS AMOUNT, " &_
					"			ISNULL(ST.DESCRIPTION, '') AS STATUS, " &_
					"			ISNULL(I.STATUS, '0') AS ISINITIALHB " &_					
					"FROM 		F_RENTJOURNAL J " &_
					"			INNER JOIN F_HBACTUALSCHEDULE ACT ON ACT.JOURNALID = J.JOURNALID " &_
					"			INNER JOIN (SELECT MIN(HBROW) AS HBROW  " &_
					"					FROM F_HBACTUALSCHEDULE  " &_
					"						WHERE SENT IS NOT NULL AND VALIDATED IS NULL  " &_
					"							GROUP BY HBID)  " &_
					"								MHB ON MHB.HBROW = ACT.HBROW  " &_
					"			INNER JOIN F_HBINFORMATION I ON I.HBID = ACT.HBID " &_
					"			INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = I.CUSTOMERID AND CT.TENANCYID = I.TENANCYID " &_
					"			INNER JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
					"			INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID " &_
					"			LEFT JOIN C_TENANCYCLOSED TC ON T.TENANCYID = TC.TENANCYID " &_					
					"			INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"			INNER JOIN F_TRANSACTIONSTATUS ST ON J.STATUSID = ST.TRANSACTIONSTATUSID " &_
					"			LEFT JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
					"			LEFT JOIN G_LOCALAUTHORITY L ON D.LOCALAUTHORITY = L.LOCALAUTHORITYID " &_
					"WHERE 		J.ITEMTYPE = 1 AND J.PAYMENTTYPE = 1 AND J.STATUSID = 1 AND " &_
					"			J.PAYMENTSTARTDATE < '" & startdate & "'" & la_sql & status_sql & tenancy_sql & hbref_sql &_
					"ORDER		BY " & OrderString & " J.PAYMENTSTARTDATE ASC, ACT.HBID "

		
		
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 20
	
		
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
		
		//Total for each page
		PageTotal = 0
		
		

			OpenDB()
						
			//GET CURRENT BALALNCE
			dim dbrs,dbcmd
			dim sql
			dim id
			
			set dbrs=server.createobject("ADODB.Recordset")
			set dbcmd= server.createobject("ADODB.Command")
			set xmlDoc=CreateObject("Microsoft.XMLDOM")
			
			dbcmd.ActiveConnection=RSL_CONNECTION_STRING
			dbcmd.CommandType = 4
			dbcmd.CommandText = "F_GET_SCHEDULEDETAILS"
			dbcmd.CommandTimeout=0
			dbcmd.Parameters.Refresh
			dbcmd.Parameters(1) = ScheduleID
			dbcmd.Parameters(2) = 2
			set dbrs=dbcmd.execute
			CurrentBalance = FormatNumber(dbrs("bl"),2,-1,0,0)
			

			//Get Grand Total						
			strGrandSQL  =	"SELECT 	IsNull(Sum(J.AMOUNT),0) as GrandTotal " &_					
							"FROM 		F_RENTJOURNAL J " &_
							"			INNER JOIN F_HBACTUALSCHEDULE ACT ON ACT.JOURNALID = J.JOURNALID " &_
							"			INNER JOIN (SELECT MIN(HBROW) AS HBROW  " &_
							"					FROM F_HBACTUALSCHEDULE  " &_
							"						WHERE SENT IS NOT NULL AND VALIDATED IS NULL  " &_
							"							GROUP BY HBID)  " &_
							"								MHB ON MHB.HBROW = ACT.HBROW  " &_
							"			INNER JOIN F_HBINFORMATION I ON I.HBID = ACT.HBID " &_
							"			INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = I.CUSTOMERID AND CT.TENANCYID = I.TENANCYID " &_
							"			INNER JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID " &_
							"			INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID " &_
							"			LEFT JOIN C_TENANCYCLOSED TC ON T.TENANCYID = TC.TENANCYID " &_					
							"			INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
							"			INNER JOIN F_TRANSACTIONSTATUS ST ON J.STATUSID = ST.TRANSACTIONSTATUSID " &_
							"			LEFT JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
							"			LEFT JOIN G_LOCALAUTHORITY L ON D.LOCALAUTHORITY = L.LOCALAUTHORITYID " &_
							"WHERE 		J.ITEMTYPE = 1 AND J.PAYMENTTYPE = 1 AND J.STATUSID = 1 AND " &_
							"			J.PAYMENTSTARTDATE < '" & startdate & "'" & la_sql & status_sql & tenancy_sql & hbref_sql
			
						
			Call OpenRs(rsGrandTotal, strGrandSQL)
			GrandTotal = rsGrandTotal("GrandTotal")
			CloseRs(rsGrandTotal)
			CloseDB()
			
					
		dim rcnt
		rcnt=1
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt +1
			iJournalID = Rs("JOURNALID")
			iStartDate = Rs("STARTDATE")
			iEndDate = Rs("ENDDATE")
			isInitial = Rs("ISINITIALHB")
			iAmount = FormatNumber(Rs("AMOUNT"),2,-1,0,0)
			
			'THIS IS A CHECK FOR THE INITIAL HB AS WE DO NOT KNOW WHETHER OUR STARTDATE IS CORRECT FOR AN INITIAL HB,
			'SO WE HAVE TO BE ABLE TO AMEND THE STARTDATE ALSO.
			StartDateStyle = ""
			IF (isInitial = 1) THEN
				StartDateStyle = " STYLE='BORDER:NONE'"
			END IF
			TRSTYLE = ""
			IF (Not isNull(Rs("TENANCYCLOSEDID"))) then
				TRSTYLE = " style='background-color:beige;color:red' title='Closed Tenancy' "
			end if
			
			'START APPENDING EACH ROW TO THE TABLE
			str_data = str_data & 	"<TR " & TRSTYLE & "><TD>" & Rs("STATUS") & "</TD>" &_
						"<TD>" & Rs("HREF") & "</TD>" &_
						"<TD>" & Rs("FULLNAME") & "</TD>" &_
						"<TD title='" & Rs("TOWNCITY") & VbCrLf & Rs("POSTCODE") & "'>" & Rs("STREET") & "</TD>" &_
						"<TD><INPUT size=12 name='txt_ST" & iJournalID & "' READONLY TYPE=TEXT class='textbox' " & StartDateStyle & " VALUE='" & iStartDate & "'></TD>" &_
						"<TD><INPUT size=12 name='txt_ED" & iJournalID & "' READONLY TYPE=TEXT class='textbox' VALUE='" & iEndDate & "'></TD>" &_
						"<TD><INPUT size=12 style='text-align:right' name='txt_AM" & iJournalID & "' READONLY TYPE=TEXT class='textbox' VALUE='" & iAmount & "' onBlur='reCalculate()'></TD>" &_
						"<TD><input type='checkbox' name='chkHB_V' id='chkv" & iJournalID & "' value='" & iJournalID & "' onclick='VR(" & iJournalID & ")'></TD>" &_
						"<TD><input type='checkbox' name='chkHB_A' id='chka" & iJournalID & "' value='" & iJournalID & "' onclick='AR(" & iJournalID & ")'>" &_
						"<INPUT TYPE=HIDDEN VALUE='"  & iStartDate & "|" & iEndDate & "|" & iAmount & "' Name='HD" & iJournalID & "'>" &_
						"<INPUT TYPE=HIDDEN VALUE='"  & isInitial & "' NAME='IHB" & iJournalID & "'></TD>" &_
						"<TD><IMG NAME='img_" & iJournalID & "' src='/js/FVS.gif' width=15 height=15></TD> " &_
						"<td><input type='hidden' value='" & rcnt &"' id='hdn" & rcnt &"'></td>" &_
						"</TR>"
			rcnt=rcnt+1
			PageTotal = PageTotal + Rs("AMOUNT")
			Rs.movenext()
			End If
		Next
		
		
		
		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=10 ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			
			EndButton = "<TABLE BORDER=0 width='100%'>" &_
						"<td ALIGN=RIGHT > <b>Page Total : <span id=ptot>" & FormatCurrency(PageTotal) & "</span>  </b>&nbsp; &nbsp;&nbsp; <b>Grand Total : <span id=gtot>" & FormatCurrency(GrandTotal) & "</span> </b>&nbsp; &nbsp;"&_
							"<INPUT TYPE=BUTTON VALUE='Process' CLASS='rslbutton' ONCLICK='process()'>" &_
						"</TD></TR></TABLE>"
			 
			 
		End If
		
			str_data = str_data & "<tr bgcolor=black>" &_
			"	<td colspan=10 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=black>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' target='frm_hbval' href='ServerSide/hbvalidation_srv.asp?page=1&orderBy=" & orderBy & NextString & "'><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' target='frm_hbval' href='ServerSide/hbvalidation_srv.asp?page=" & prevpage& "&orderBy=" & orderBy & NextString & "'><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' target='frm_hbval' href='ServerSide/hbvalidation_srv.asp?page=" & nextpage& "&orderBy=" & orderBy & NextString & "'><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' target='frm_hbval' href='ServerSide/hbvalidation_srv.asp?page=" & numpages& "&orderBy=" & orderBy & NextString & "'><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_hbval.location.href='ServerSide/hbvalidation_srv.asp?page=' + document.getElementById('page').value + '&orderBy=" & orderBy & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"

	End function
	
%>
<html>
<head></head>
<%= strSQL%>
<script language=javascript>
function ReturnData(){
	parent.RSLFORM.txt_BALANCE.value = "<%=CurrentBalance%>"
	parent.RSLFORM.hid_BALANCE.value = "<%=CurrentBalance%>"	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>