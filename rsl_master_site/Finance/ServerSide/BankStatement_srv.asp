<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	statement = Request("sel_STATEMENT")
	if (statement = "") then statement  = -1
	SQL = "SELECT * FROM F_BANK_STATEMENTS WHERE BSID = " & statement
	Call OpenDB()
	Call OpenRs(rsBS, SQL)
	StartDate = rsBS("STARTDATE")
	EndDate = rsBS("ENDDATE")
	StartingBalance = rsBS("STARTINGBALANCE")
	EndingBalance = rsBS("ENDINGBALANCE")
	Debits = rsBS("DEBITS")
	Credits = rsBS("CREDITS")
	Call CloseRs(rsBS)

		SQL = "SELECT " &_
				"(SELECT ISNULL(SUM(AMOUNT),0) AS DEBITS FROM F_RENTJOURNAL RJ " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND RECTYPE = 1 AND BSID = " & statement & " " &_
				"		WHERE AMOUNT >= 0) " &_
				"+ " &_
				"(SELECT ISNULL(SUM(TOTALVALUE),0) AS DEBITS FROM F_BACSDATA BD " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND RECTYPE = 2 AND BSID = " & statement & " " &_
				"		WHERE TOTALVALUE >= 0) " &_
				"AS TOTALDEBITS"
		Call OpenRs(rsDEBITS, SQL)
		RSL_DEBITS = rsDEBITS("TOTALDEBITS")
		Call CloseRs(rsDEBITS)
		
		SQL = "SELECT " &_
				"(SELECT ISNULL(-SUM(AMOUNT),0) AS CREDITS FROM F_RENTJOURNAL RJ " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND RECTYPE = 1 AND BSID = " & statement & " " &_
				"		WHERE AMOUNT < 0) " &_
				"+ " &_
				"(SELECT ISNULL(-SUM(TOTALVALUE),0) AS CREDITS FROM F_BACSDATA BD  " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND RECTYPE = 2 AND BSID = " & statement & " " &_
				"		WHERE TOTALVALUE < 0) " &_
				"AS TOTALCREDITS"
		Call OpenRs(rsCREDITS, SQL)
		RSL_CREDITS = rsCREDITS("TOTALCREDITS")
		Call CloseRs(rsCREDITS)

	RSL_END = StartingBalance + RSL_DEBITS - RSL_CREDITS
	
	Call CloseDB()
%>
<HTML>
<HEAD>
</HEAD>
<SCRIPT LANGUAGE="JAVASCRIPT">
function ReturnData(){
	parent.RSLFORM.txt_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance)%>"
	parent.RSLFORM.hid_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance,2,-1,0,0)%>"	
	parent.RSLFORM.txt_ENDINGBALANCE.value = "<%=FormatNumber(EndingBalance)%>"
	parent.RSLFORM.txt_DEBITS.value = "<%=FormatNumber(Debits)%>"
	parent.RSLFORM.txt_CREDITS.value = "<%=FormatNumber(Credits)%>"			
	parent.RSLFORM.txt_RSLDEBITS.value = "<%=FormatNumber(RSL_DEBITS)%>"
	parent.RSLFORM.txt_RSLCREDITS.value = "<%=FormatNumber(RSL_CREDITS)%>"			
	parent.RSLFORM.hid_RSLDEBITS.value = "<%=FormatNumber(RSL_DEBITS,2,-1,0,0)%>"
	parent.RSLFORM.hid_RSLCREDITS.value = "<%=FormatNumber(RSL_CREDITS,2,-1,0,0)%>"
	parent.RSLFORM.txt_RSLENDINGBALANCE.value = "<%=FormatNumber(RSL_END)%>"				
	parent.RSLFORM.hid_RSLENDINGBALANCE.value = "<%=FormatNumber(RSL_END,2,-1,0,0)%>"					
	parent.RSLFORM.txt_FROM.value = "<%=StartDate%>"					
	parent.RSLFORM.txt_TO.value = "<%=EndDate%>"					
	parent.RSLFORM.txt_PFROM.value = "<%=StartDate%>"					
	parent.RSLFORM.txt_PTO.value = "<%=EndDate%>"
	parent.RSLFORM.txt_HBFROM.value = "<%=StartDate%>"					
	parent.RSLFORM.txt_HBTO.value = "<%=EndDate%>"	
	parent.RSLFORM.sel_VALIDATE.disabled = false				
	}
</SCRIPT>
<BODY onload="ReturnData()">
</BODY>
</HTML>