<%@  language="VBSCRIPT" codepage="1252" %>  
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/RepairPurchaseOrder.asp" -->
<!--#include virtual="Finance/ServerSide/ApplianceEmail.asp" -->
<!--#include virtual="Finance/ServerSide/FaultEmail.asp" -->
<!--#include virtual="Finance/ServerSide/PlannedEmail.asp" -->
<!--#include virtual="Finance/ServerSide/VoidEmail.asp" -->
<%
OpenDB()
if (Request("AUTHORIZE") = 1) then
	For each ORDERITEMID in Request.Form("CHECKITEM")

       
		' This stores values for use in KPIs, I havnt got to look further at this : please record notes here if u deal with this
		'Call LOG_PI_ACTION(ORDERITEMID, "APPROVED FROM REPAIR APPROVAL")

		' there are two possible ways the repair item here
			' 1) If the price info gets changed after its been assign to a supplier and the amount goes over
			'    users limit
			' 2) If on raising the order the limit is overreached
			
		
		' 1) if the limit is reached after its been assigned then we do the first part making sure that when its
		'	accepted it return back to its intial status from queued. Previously by error it was returning to the begining status'.
		SQL = 	"SELECT PI.ORDERID " &_
				"FROM F_PURCHASEITEM PI " &_
				"	INNER JOIN F_PURCHASEITEM_HISTORY PIH ON PIH.ORDERITEMID = PI.ORDERITEMID " &_
				"WHERE PI.ORDERITEMID = " & ORDERITEMID
		Call OpenRs(rsPI, SQL)
			If not rsPI.EOF then
			
				ORDERID = rsPI("ORDERID")
				' here we get the status of the journal ( not the pistatus ). This status is the one used on repair journals 
				' (basically it is used in the customer area, property, contractors)
				SQL = 	"SELECT J.CURRENTITEMSTATUSID , R.ITEMACTIONID  " &_ 
						"FROM 	P_WOTOREPAIR WOTO " &_
						"	INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
						"	INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
						"WHERE 	R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID AND ITEMACTIONID NOT IN (12,13) ) " &_
						"		AND ORDERITEMID = " & ORDERITEMID 
				'rw SQL & "<BR><BR>"
				Call OpenRs(rsPInew, SQL)
					NEWSTAT = rsPInew("CURRENTITEMSTATUSID")
					NEWACT =  rsPInew("ITEMACTIONID")
				'	rw NEWSTAT & "<BR><BR>"
				Call CloseRs(rsPInew)
				
				' here we link to the history table where it stored info about the changes to the PI financial info it
				' holds the pi status also. This is the only place it does once its changed to Queued.
				strSQL = 	"	UPDATE F_PURCHASEITEM SET APPROVED_BY = " & Session("USERID") & " , PISTATUS =  ISNULL(PIH.PISTATUS,0) " &_
							"	FROM  F_PURCHASEITEM PI 	" &_
							"		LEFT JOIN F_PURCHASEITEM_HISTORY PIH ON PIH.ORDERITEMID = PI.ORDERITEMID " &_
							"	WHERE 	PIH.HISTORYID = (SELECT MAX(HISTORYID) FROM F_PURCHASEITEM_HISTORY WHERE ORDERITEMID = PI.ORDERITEMID) " &_
							"			AND PI.ORDERITEMID = " & ORDERITEMID
				'rw strSQL & "<BR><BR>"
				Conn.Execute(strSQL)
				
			' 2) else, if the limit was overreached at the begining when it was raised then we do as was done before and just set the status to
			'	start the repair process
			else
				
				SQL = "SELECT ORDERID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
				Call OpenRs(rsPInew, SQL)
				ORDERID = rsPInew("ORDERID")
				Call CloseRs(rsPInew)
				'rw ORDERID  & "<BR><BR>"
				
				strSQL = 	"UPDATE F_PURCHASEITEM SET PISTATUS = 3, APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID
				'rw strSQL  & "<BR><BR>"
				Conn.Execute(strSQL)	
				
				NEWSTAT = 2 'ASSIGNED
				NEWACT = 2 'ASSIGN TO CONTRACTOR
			
			end if
		Call CloseRs(rsPI)

		'get the work order information
		SQL = "SELECT MAX(REPAIRHISTORYID) AS MAXREPAIRID, WOTO.JOURNALID, WOID FROM P_WOTOREPAIR WOTO " &_
				"INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
				"INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
				"WHERE ORDERITEMID = " & ORDERITEMID & " " &_
				"GROUP BY WOTO.JOURNALID, WOID"
		
		Call OpenRs(rsIN, SQL)
        If not rsIN.EOF then
		    MAXREPAIRID = rsIN("MAXREPAIRID")		
		    JOURNALID = rsIN("JOURNALID")
		    WOID = rsIN("WOID")
        end if
		CloseRs(rsIN)
		

		'ENTER a new line into the c_repair to say that the item has ben authorised
		SQL = "INSERT INTO C_REPAIR (JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,ITEMDETAILID,CONTRACTORID,TITLE,NOTES)" &_
				"SELECT JOURNALID, " & NEWSTAT & ", " & NEWACT & ",	GETDATE(), " & SESSION("USERID") & ", ITEMDETAILID, CONTRACTORID, " &_
				"TITLE, 'AUTHORISED BY " & SESSION("FIRSTNAME") & " " & SESSION("LASTNAME") & "' FROM C_REPAIR WHERE REPAIRHISTORYID = " & MAXREPAIRID & ";"
		Conn.Execute (SQL)

		'ALSO UPDATE THE JOURNAL STATUS
		SQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & NEWSTAT & " WHERE JOURNALID = " & JOURNALID		
		Conn.Execute (SQL)		
		
		'CHECK WHETHER THERE IS ANY OTHER QUEUED PURCHASES UNDER THIS ITEM
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS = 0 AND ORDERID = " & ORDERID
		Call OpenRs(rsCount, SQL)
		iCOUNT = rsCount("THECOUNT")
		CloseRs(rsCount)

        'CHECK WHETHER PURCHASE IS DAY TO DAY REPAIRS THEN EMAIL WILL BE SENT TO CONTRACTORS CONTACT EMAIL ADDRESS
        Dim poName
		SQL = "SELECT PONAME,PONOTES FROM F_PURCHASEORDER WHERE ORDERID = " & ORDERID
		Call OpenRs(rsCount, SQL)
		poName = rsCount("PONAME")
        poNotes = rsCount("PONOTES")
		
		Call CloseRs(rsCount)

		if (CInt(iCOUNT) = 0) then
		
			SET_PO_STATUS(JOURNALID)
            Call NotifyPO(ORDERID, "approved", "Approved")
            if(poName = "PLANNED MAINTENANCE WORK ORDER") then
                Call plannedEmailToContractor(ORDERID)
            end if
            if((poName = "DAY TO DAY REPAIR WORK ORDER") Or (poName = "INVESTIGATIVE WORKS ONLY : PLEASE PROVIDE AN ESTIMATE FOR WORKS PRIOR TO COMMENCEMENT")) then
                Call faultEmailToContractor(ORDERID)
            end if
            if(poName = "SCHEME/BLOCK REACTIVE REPAIR WORK ORDER") then
                Call sbFaultEmailToContractor(ORDERID)
            end if
            if poName = "WORK ORDER" then 
                Call applianceEmailToContractor(ORDERID)
            end if
            if(poName = "CYCLIC MAINTENANCE WORK ORDER") then
                Call voidCyclicEmailToContractor(ORDERID,"Cyclic Maintenance")
            end if
            if(poName = "M&E SERVICING WORK ORDER") then
                 Call voidCyclicEmailToContractor(ORDERID, "M&E Servicing")
            end if
            if(poName = "VOID WORKS WORK ORDER") then
                 Call voidWorksEmailToContractor(ORDERID)
            end if
            if(poName = "PAINT PACK WORK ORDER") then
                 Call voidPaintEmailToContractor(ORDERID)
            end if
            if(poName = "SCHEME BLOCK PURCHASE ORDER") then
                 Call raiseAPOEmailToContractor(ORDERID)
            end if
		end if

	next
elseif (Request("DECLINE") = 1) then
	For each ORDERITEMID in Request.Form("CHECKITEM")
		'Call LOG_PI_ACTION(ORDERITEMID, "DECLINED FROM REPAIR APPROVAL")			
		strSQL = 	"UPDATE F_PURCHASEITEM SET ACTIVE = 0, APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID
		Conn.Execute(strSQL)

		SQL = "SELECT ORDERID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
		Call OpenRs(rsPI, SQL)
		ORDERID = rsPI("ORDERID")
		Call CloseRs(rsPI)

		'get the work order information
		SQL = "SELECT MAX(REPAIRHISTORYID) AS MAXREPAIRID, WOTO.JOURNALID, WOID FROM P_WOTOREPAIR WOTO " &_
				"INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
				"INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
				"WHERE ORDERITEMID = " & ORDERITEMID & " " &_
				"GROUP BY WOTO.JOURNALID, WOID"
		Call OpenRs(rsIN, SQL)
		MAXREPAIRID = rsIN("MAXREPAIRID")		
		JOURNALID = rsIN("JOURNALID")
		WOID = rsIN("WOID")
		CloseRs(rsIN)
		
		NEWSTAT = 5 'CANCELLED
		NEWACT =11 'CANCEL 	//ENTER a new line into the c_repair to say that the item has ben authorised
		SQL = "INSERT INTO C_REPAIR (JOURNALID,ITEMSTATUSID,ITEMACTIONID,LASTACTIONDATE,LASTACTIONUSER,ITEMDETAILID,CONTRACTORID,TITLE,NOTES) " &_
				"SELECT JOURNALID, " & NEWSTAT & ", " & NEWACT & ",	GETDATE(), " & SESSION("USERID") & ", ITEMDETAILID, CONTRACTORID, " &_
				"TITLE, 'AUTHORISATION DECLINED BY " & SESSION("FIRSTNAME") & " " & SESSION("LASTNAME") & "' FROM C_REPAIR WHERE REPAIRHISTORYID = " & MAXREPAIRID & ";"
		Conn.Execute (SQL)

		'ALSO UPDATE THE JOURNAL STATUS
		SQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = " & NEWSTAT & " WHERE JOURNALID = " & JOURNALID		
		Conn.Execute (SQL)		
		
		'CHECK WHETHER THERE IS ANY OTHER QUEUED PURCHASES UNDER THIS ITEM
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS = 0 AND ORDERID = " & ORDERID
		
		Call OpenRs(rsCount, SQL)
		iCOUNT = rsCount("THECOUNT")
		CloseRs(rsCount)
		if (iCOUNT = 0) then
			CancelPO(JOURNALID)

            'THERE ARE NO QUEUED ITEMS FOUND
            'CHECK WHETHER THERE ARE ANY ITEMS WHICH HAVE NOT BEEN DECLINED
            SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = " & ORDERID
            Call OpenRs(rsCount, SQL)
            iCOUNT = rsCount("THECOUNT")
            CloseRs(rsCount)

            'CHECK WHETHER PURCHASE IS DAY TO DAY REPAIRS THEN EMAIL WILL BE SENT TO CONTRACTORS CONTACT EMAIL ADDRESS
         
		    SQL = "SELECT PONAME FROM F_PURCHASEORDER WHERE ORDERID = " & ORDERID
		    Call OpenRs(rsCount, SQL)
		        poName = rsCount("PONAME")
		    Call CloseRs(rsCount)

            if (CInt(iCOUNT) > 0) then
                Call NotifyPO(ORDERID, "approved", "Approved")

                 if((poName = "DAY TO DAY REPAIR WORK ORDER") Or (poName = "INVESTIGATIVE WORKS ONLY : PLEASE PROVIDE AN ESTIMATE FOR WORKS PRIOR TO COMMENCEMENT")) then
                    Call faultEmailToContractor(ORDERID)
                 end if
                 if poName = "WORK ORDER" then 
                        Call applianceEmailToContractor(ORDERID)
                 end if
            else
                ' ALL ITEMS HAVE BEEN DECLINED
                Call NotifyPO(ORDERID, "declined", "Declined")
            end if
		end if

	next
end if
CloseDB()
Response.Redirect "../QueuedRepairs.asp"

Function NotifyPO(orderid, emailVerb, subjectVerb) 

    Dim supplierName
    Dim cost
    Dim requestedByEmail
    Dim employeeName
    Dim itemName
    Dim status
    Dim rs
    Dim emailbody
    Dim emailSubject
    Dim table
    Dim row

    ' Obtain supplier name and 
    SQL = "SELECT  " &_
            "S_ORGANISATION.NAME , " &_
            "ISNULL(E_CONTACT.WORKEMAIL, 'enterprisedev@broadlandgroup.org') AS REQUESTEDBYEMAIL " &_
            "FROM    S_ORGANISATION " &_
            "        INNER JOIN F_PURCHASEORDER ON S_ORGANISATION.ORGID = F_PURCHASEORDER.SupplierId " &_
            "        INNER JOIN E__EMPLOYEE ON EMPLOYEEID = USERID " &_
            "        INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID " &_
            "WHERE   F_PURCHASEORDER.ORDERID = " & orderid

    Call OpenRs(rs, SQL)
    supplierName = rs("NAME")
    requestedByEmail = rs("REQUESTEDBYEMAIL")
    CloseRs(rs)

    ' Obtain purchase order item cost and approved by name
    SQL = "SELECT (CASE WHEN APPROVED_BY IS NOT NULL THEN ISNULL(LTRIM(RTRIM(ab.FIRSTNAME)) + ' ' + LTRIM(RTRIM(ab.LASTNAME)), '') " &_
          " ELSE ISNULL(LTRIM(RTRIM(e.FIRSTNAME)) + ' ' + LTRIM(RTRIM(e.LASTNAME)), '') END) AS EMPLOYEENAME, " &_
            " '�' + CAST(CAST(ISNULL(grosscost, 0) AS NUMERIC(10,2)) AS VARCHAR(100)) AS GROSSCOST, (CASE WHEN ACTIVE = 1 THEN 'Approved' ELSE 'Declined' END) AS STATUS, " &_
            " ITEMNAME, ISNULL(ITEMDESC, '') AS ITEMDESC " &_
            " FROM    F_PURCHASEITEM " &_
            " LEFT OUTER JOIN E__EMPLOYEE AS ab ON F_PURCHASEITEM.APPROVED_BY = ab.EMPLOYEEID " &_
            " LEFT OUTER JOIN E__EMPLOYEE AS e ON F_PURCHASEITEM.USERID = e.EMPLOYEEID " &_
            " WHERE ORDERID = " & orderid

    ' Assemble details table
    ' Table header
    table = "<table style='border-collapse:collapse;font-family: Arial;font-size:12pt' cellpadding='5'><tr><td style='border:1px solid lightgrey;font-weight:bold;background-color:lightgrey;'>Item Name</td><td style='border:1px solid lightgrey;font-weight:bold;background-color:lightgrey;'>Notes</td><td style='border:1px solid lightgrey;font-weight:bold;background-color:lightgrey;'>Gross</td><td style='border:1px solid lightgrey;font-weight:bold;background-color:lightgrey;'>Status</td><td style='border:1px solid lightgrey;font-weight:bold;background-color:lightgrey;'>Approved/Declined by</td></tr>"
    Call OpenRs(rs, SQL)
    
    while not rs.EOF
    
        employeeName = rs("EMPLOYEENAME")
        cost = rs("GROSSCOST")
        status = rs("STATUS")
        itemName = rs("ITEMNAME")
        itemDesc = rs("ITEMDESC")
        row = "<tr><td style='border:1px solid lightgrey;'>" & itemName & "</td><td style='border:1px solid lightgrey;'>" & itemDesc & "</td><td style='border:1px solid lightgrey;'>" & cost & "</td><td style='border:1px solid lightgrey;'>" & status & "</td><td style='border:1px solid lightgrey;'>" & employeeName & "</td></tr>"
        table = table & row
        rs.movenext()
    wend

    Call CloseRs(rs)    

    table = table & "</table>"
    Dim iMsg
    Set iMsg = CreateObject("CDO.Message")
    Dim iConf
    Set iConf = CreateObject("CDO.Configuration")

    Dim Flds
    Set Flds = iConf.Fields
   Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

    emailbody = "<p style='font-family: Arial;font-size:12pt'>Your repair work order has been " & emailVerb & ", details are as follows:<br /><br /><b>PO Number:</b> " & PurchaseNumber(CStr(orderid)) & "<br /><b>Supplier:</b> " & supplierName & "<br />" & table & "</p>"
    emailSubject = "Repair Work Order " & subjectVerb
    
    Set iMsg.Configuration = iConf
    iMsg.To = requestedByEmail
    iMsg.From = "noreply@broadlandgroup.org"
    iMsg.Subject = emailSubject
    iMsg.HTMLBody = emailbody
    iMsg.Send	
   			
End Function




%>