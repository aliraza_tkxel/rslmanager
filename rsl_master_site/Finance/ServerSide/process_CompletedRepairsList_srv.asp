<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim insert_list  
	Dim sel_split
	
	insert_list = Request.Form("idlist")

	response.write "idlist : " & insert_list & "<BR><BR>"
		
	OpenDB()
	
	update_Repair()
	
	CloseDB()
	
	// This function Adds a new History item to c_repair and updates the c_journal
 	Function update_Repair()
		
		sel_split = Split(insert_list ,",") ' split selected string
		
		For each key in sel_split
			
			'Find extra values in relation to the journal id
			SQL = "SELECT  R.CONTRACTORID, J.TITLE" &_
						" FROM	C_JOURNAL J" &_
						" INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID" &_
						" LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID" &_
						" WHERE 	J.JOURNALID = " & clng(key) & " AND" &_
						" R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID)"
			'response.write "select SQL: " & SQL & "<BR><BR>"
			
			Call OpenRs(rsGetRepairData, SQL)
				
				'Extra Items assigned
				ItemContractor = rsGetRepairData("CONTRACTORID")
				theTitle = rsGetRepairData("TITLE")
			
			CloseRs(rsGetRepairData)
						
			'Insert new repair history item into c_repair
			strSQL = "INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER,CONTRACTORID,TITLE) " &_
					 "VALUES (" & clng(key) & ",10,9,'" & date() & "'," & Session("USERID") & ","  & ItemContractor & ",'" & theTitle & "')"
			Conn.Execute(strSQL)
			'response.write "Insert SQL: " & SQL & "<BR><BR>"
					
			'Update the journal status for the item in question
			strSQL = "UPDATE C_JOURNAL SET CURRENTITEMSTATUSID = 10 WHERE JOURNALID = " & clng(key)
			Conn.Execute(strSQL)
			'response.write "update SQL: " & strSQL & "<BR><BR>"
			
		Next
	
	End Function
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.location.href = "../CompletedRepairsList.asp"
	
	}
</script>
<body onload="ReturnData()">
	


</body>
</html>