<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%

OrderId = Request("OrderID")
If (OrderId = "") then
	OrderId = -1
end if

OpenDB()

Call CancelPurchaseOrder(OrderID, Session("USERID"))

CloseDB()

Response.Redirect "../PurchaseCancelList.asp"
%>