<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TenancyID = CStr(Request.Form("txt_TENANCY"))
if (TenancyID = "" OR NOT isNumeric(TenancyID)) then
	TenancyID = "-1"
end if

	SQL = "SELECT C.CUSTOMERID, C.FIRSTNAME + ' ' + LASTNAME AS FULLNAME, CT.ENDDATE FROM C__CUSTOMER C " &_
			"INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = C.CUSTOMERID " &_
			"INNER JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
			"WHERE T.TENANCYID = " & TenancyID & " ORDER BY FIRSTNAME, LASTNAME"
	
	Dim rsCustomers
	OpenDB()
	Call OpenRs(rsCustomers, SQL)
	if (not rsCustomers.EOF) then
		CustomerList = "<option value=''>Please Select</option>"
		while not rsCustomers.EOF
			if (isNull(rsCustomers("ENDDATE"))) then
				style = ""
			else
				style = " style='background-color:red;color:white'"
			end if
			CustomerList = CustomerList & "<option " & style & " value='" & rsCustomers("CUSTOMERID") & "'>" & rsCustomers("FULLNAME")& "</option>"
			rsCustomers.moveNext
		wend
	else
		CustomerList = "<option value=''>No customers found...</option>"
	end if
	CloseRs(rsCustomers)
	CloseDB()

%>			
<html>
<head>
<title>Server Employee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JAVASCRIPT">
function SetCustomers(){
	parent.THISFORM.sel_CUSTOMER.outerHTML = Customers.innerHTML
	}
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000" onload="SetCustomers()">
<div id="Customers">
<select name='sel_CUSTOMER' class='textbox200'>
<%=CustomerList%>
</select>
</div>
</body>
</html>
