<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	if (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then 'from reconcile page
		LOGPAGETEXT = "(RECONCILE PAGE)"
	else
		LOGPAGETEXT = "(AMEND PAGE)"
	end if

	ORDERITEMID = REQUEST("ORDERITEMID")
	ORDERID = REQUEST("ORDERID")

	UserSessionID = SESSION("USERID")
	OpenDB()
    
	SQL = "UPDATE F_PURCHASEITEM SET ACTIVE = 0,PISTATUS = 16  WHERE ORDERITEMID = " & ORDERITEMID
 
	Conn.Execute(SQL)

    'FINALLY WE NEED TO CHECK IF ALL ITEMS HAVE BEEN FULLY CANCELLED, IF SO THEN CANCEL THE MASTER PURCHASE ORDER.
	SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = " & ORDERID
	Call OpenRs(rsTOTAL, SQL)
	TotalCount = 0
	if (NOT rsTOTAL.EOF) then
		TotalCount = rsTOTAL("THECOUNT")
	END IF
	Call CloseRs(rsTOTAL)

    'if total count is 0 then this means there are no active items left
	'therefore make the purchase order in active aswell.
	if (TotalCount = 0) then
		Call LOG_PO_ACTION(ORDERID, "CANCEL", "")
        SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 0 ,POSTATUS = 16 ,"&_ 
				"CancelBy="& UserSessionID & " WHERE ORDERID = " & ORDERID		
        Conn.Execute SQL
    end if
	
	CloseDB()
	
	if (Request("ReturnTo") = "1") then
		REDIRECTPAGE = "/Finance/ReconcileConfirm.asp?CurrentPage=" & Request("IPAGE") & "&ORDERID=" & ORDERID
	elseif (Request("ReturnTo") = "2") then
		REDIRECTPAGE = "/Finance/GasReconcileConfirm.asp?CurrentPage=" & Request("IPAGE") & "&BatchId=" & BATCHID	
	else
	 '   IF REDIRECT_FLAG=1 THEN
	'        REDIRECTPAGE = "/Finance/PurchaseAmendList.asp"
	 ' '  ELSE
		    REDIRECTPAGE = "/Finance/PurchaseOrderAmend.asp?CurrentPage=" & Request("IPAGE") & "&ORDERITEMID=" & ORDERITEMID & "&ORDERID=" & ORDERID'		END IF
	end if
		
	
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_Parent()
        {
            parent.parentReload();
		}
    </script>
</head>
<body onload="Return_Parent();">
</body>
</html>