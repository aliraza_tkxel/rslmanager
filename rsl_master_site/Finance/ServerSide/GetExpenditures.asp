<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, optionvalues2, optiontext2
	Dim intReturn, theamounts, EmployeeLimits, Budget_Remaining, EXPID, HeadID, CCID, CCLeft

	strWhat = Request.form("IACTION")
    isCascade = Request.form("IS_CASCADE")

	'Default to '0' just incase no items are returned...
	Budget_Remaining = 0

	If strWhat = "change" Then
		Call rePop()
	ElseIf strWhat = "LOADPREVIOUS" Then
		Call GetPrevStuff()
	End If

	Function rePop()
		HeadID = -1
		If(Request.Form("sel_HEAD") <> "") Then HeadID = Request.Form("sel_HEAD")

		If (HeadID = -1) Then
			optionvalues = ""
			theamounts = "0"
			EmployeeLimits = "0"
			optiontext = "Please Select a Head..."
			Exit Function
		End If

		Call OpenDB()

		Call GetCurrentYear()
		FY = GetCurrent_YRange

		SQL = "SELECT CC.COSTCENTREID, ISNULL(CCA.COSTCENTREALLOCATION,0) - ISNULL(PITOTAL.TOTALSPENT,0) AS TOTALCCLEFT " &_
				"	FROM F_COSTCENTRE CC " &_
				"	INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
				"	INNER JOIN F_HEAD IHE ON IHE.COSTCENTREID = CC.COSTCENTREID AND IHE.HEADID = " & HeadID & " " &_
				"	LEFT JOIN ( " &_
				"		SELECT SUM(IPI.GROSSCOST) AS TOTALSPENT, IHE.COSTCENTREID FROM F_PURCHASEITEM IPI " &_
				"			INNER JOIN F_EXPENDITURE IEX ON IEX.EXPENDITUREID = IPI.EXPENDITUREID " &_
				"			INNER JOIN F_HEAD IHE ON IHE.HEADID = IEX.HEADID " &_
				"		WHERE IPI.PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND IPI.PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_
				"			AND IPI.ACTIVE = 1 AND IHE.COSTCENTREID = (SELECT COSTCENTREID FROM F_HEAD WHERE HEADID = " & HeadID & ") " &_
				"		GROUP BY IHE.COSTCENTREID " &_
				"		) PITOTAL ON PITOTAL.COSTCENTREID = CC.COSTCENTREID " &_
				"WHERE CCA.ACTIVE = 1"

		Call OpenRs(rsCC, SQL)
		If (NOT rsCC.EOF) Then
			CCLeft = rsCC("TOTALCCLEFT")
			CCID = rsCC("COSTCENTREID")
		Else
			CCLeft = 0
			CCID = -1
		End If
		Call CloseRs(rsCC)

		SQL = "SELECT DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (ISNULL(EXA.EXPENDITUREALLOCATION,0) - ISNULL(GROSSCOST,0)) AS REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) " &_
				"INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE " &_
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_
				"LEFT JOIN ( "&_
					"SELECT SUM(GROSSCOST) AS GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM WHERE ACTIVE = 1 "&_
					"AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID & ") "&_
				"ORDER BY DESCRIPTION"

		Call OpenRs(rsExpenditure, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			EmployeeLimits = EmployeeLimits & ";;" & rsExpenditure.Fields.Item("LIMIT").Value

			count = count + 1
			rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If

		Call CloseRs(rsExpenditure)
		Call CloseDB()

	End Function

	Function GetPrevStuff()

		ExpID = -1
		If(Request("EXPID") <> "") Then ExpID = Request("EXPID")

		If (ExpID = -1) Then
			Exit Function
		End If

		Call OpenDB()

		Call GetCurrentYear()
		FY = GetCurrent_YRange

		SQL = "SELECT HEADID FROM F_EXPENDITURE WHERE EXPENDITUREID = " & EXPID
		Call OpenRs(rsHEAD, SQL)
		    HeadID = rsHEAD("HEADID")
		Call CloseRs(rsHEAD)

		SQL = "SELECT CC.COSTCENTREID, ISNULL(CCA.COSTCENTREALLOCATION,0) - ISNULL(PITOTAL.TOTALSPENT,0) AS TOTALCCLEFT " &_
				"	FROM F_COSTCENTRE CC " &_
				"	INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
				"	INNER JOIN F_HEAD IHE ON IHE.COSTCENTREID = CC.COSTCENTREID AND IHE.HEADID = " & HeadID & " " &_
				"	LEFT JOIN ( " &_
				"		SELECT SUM(IPI.GROSSCOST) AS TOTALSPENT, IHE.COSTCENTREID FROM F_PURCHASEITEM IPI " &_
				"INNER JOIN F_EXPENDITURE IEX ON IEX.EXPENDITUREID = IPI.EXPENDITUREID " &_
				"INNER JOIN F_HEAD IHE ON IHE.HEADID = IEX.HEADID " &_
				"		WHERE IPI.PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND IPI.PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_
				"			AND IPI.ACTIVE = 1 AND IHE.COSTCENTREID = (SELECT COSTCENTREID FROM F_HEAD WHERE HEADID = " & HeadID & ") " &_
				"		GROUP BY IHE.COSTCENTREID " &_
				"		) PITOTAL ON PITOTAL.COSTCENTREID = CC.COSTCENTREID " &_
				"WHERE CCA.ACTIVE = 1"
		Call OpenRs(rsCC, SQL)
		If (NOT rsCC.EOF) Then
			CCLeft = rsCC("TOTALCCLEFT")
			CCID = rsCC("COSTCENTREID")
		Else
			CCLeft = 0
			CCID = -1
		End If
		Call CloseRs(rsCC)

		SQL = "SELECT EX.DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (isNull(EXA.EXPENDITUREALLOCATION,0) - isNull(GROSSCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) " &_
				"INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE " &_
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_
				"left JOIN ( "&_
					"SELECT SUM(GROSSCOST) as GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM where F_PURCHASEITEM.ACTIVE = 1 "&_
					"AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID & ") "&_
				"ORDER BY DESCRIPTION"

		Call OpenRs(rsExpenditure, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			EmployeeLimits = EmployeeLimits & ";;" & rsExpenditure.Fields.Item("LIMIT").Value

			count = count + 1
			rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If
		Call CloseRs(rsExpenditure)

		SQL = "SELECT COSTCENTREID FROM F_HEAD WHERE HEADID = " & HeadID
		Call OpenRs(rsCC, SQL)
		    CCID = rsCC("COSTCENTREID")
		Call CloseRs(rsCC)

		SQL = "SELECT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) " &_ 
			"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			"WHERE HE.CostCentreID = " & CCID

		Call OpenRs(rsHead, SQL)

		count2 = 0
		optionvalues2 = ""
		optiontext2 = "Please Select..."
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues2 = optionvalues2 & ";;" & rsHead.Fields.Item("HEADID").Value
			optiontext2 = optiontext2 & ";;" & theText

			count2 = count2 + 1
			rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		If (count2 = 0) Then
			optionvalues2 = ""
			optiontext2 = "No Heads Are Setup..."
		End If

		Call CloseRs(rsHead)

		Call CloseDB()

	End Function
%>
<html>
<head>
<title>Expenditure Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
<script type="text/javascript" language="JavaScript">

    function loaded() {
        
		if ('<%=strWhat%>' == 'change'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>");
			parent.document.THISFORM.EXPENDITURE_LEFT_LIST.value = "<%=theamounts%>";
			parent.document.THISFORM.EMPLOYEE_LIMIT_LIST.value = "<%=EmployeeLimits%>";
			if (parent.document.THISFORM.hid_TOTALCCLEFT != undefined) {
			    parent.document.THISFORM.hid_TOTALCCLEFT.value = "<%=CCLeft%>"
			}
		}
		else if ('<%=strWhat%>' == 'LOADPREVIOUS'){
			parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>");
			parent.document.THISFORM.sel_EXPENDITURE.value = "<%=EXPID%>"
			parent.PopulateListBox("<%=optionvalues2%>", "<%=optiontext2%>", 2);
			parent.document.THISFORM.sel_HEAD.value = "<%=HeadID%>"
			parent.document.THISFORM.sel_COSTCENTRE.value = "<%=CCID%>"
			parent.document.THISFORM.EXPENDITURE_LEFT_LIST.value = "<%=theamounts%>";
			parent.document.THISFORM.EMPLOYEE_LIMIT_LIST.value = "<%=EmployeeLimits%>";
			if (parent.document.THISFORM.hid_TOTALCCLEFT != undefined) {
			    parent.document.THISFORM.hid_TOTALCCLEFT.value = "<%=CCLeft%>"
			}
			parent.SetPurchaseLimits()			
			}
			
		var cascade = '<%=isCascade%>';
		if (cascade == 'true') {
		    parent.SetExpenditure();
		}
		
		}

</script>

</body>
</html>
