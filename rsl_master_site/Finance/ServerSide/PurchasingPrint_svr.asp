<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim DisplayPurchasing__OrderID
DisplayPurchasing__OrderID = ""
if(Request.QueryString("OrderID") <> "") then DisplayPurchasing__OrderID = Request.QueryString("OrderID")

OpenDB()
SQL = "SELECT E.FIRSTNAME, E.LASTNAME, ITEMDESC, DELDETAIL, INVOICENUMBER, ORDERID, ORDERNO, O.NAME AS SUPPLIER, ISNULL(QUOTEDCOST,0) AS FORMATTEDCOST, ISNULL(VAT,0) AS FORMATTEDVAT, ISNULL(NETCOST,0) AS FORMATTEDNETCOST, " &_
		"EX.DESCRIPTION AS EXPENDITURE, ITEMREF, FORMATTEDRECDATE=CONVERT(VARCHAR,RECDATE,103), FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103), FORMATTEDDELDATE=CONVERT(VARCHAR,EXPDELDATE,103), O.* FROM F_PURCHASEORDER PO " &_
		"LEFT JOIN S_ORGANISATION O ON PO.SUPPLIER = O.ORGID " &_
		"LEFT JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PO.EXPENDITUREID " &_
		"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_		
		"WHERE ORDERID = " & DisplayPurchasing__OrderID
Call OpenRs(rsPP, SQL)


%>
<HTML>
<HEAD>
<title>RSL Manager Finance --> Purchase Order Print</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--

function PrintThePP(){
	document.getElementById("PrintButton").style.visibility = "hidden";
	window.print();
	document.getElementById("PrintButton").style.visibility = "visible";	
	}
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">

                              <table width="96%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                  <td class="RSLBlack" width="42%" align=right><b>Supplier Details</b></td>
                                </tr>
                                <tr> 
                                  <td valign="top" align=right class="Printout" width="42%"> 
                                    <% If Not rsPP.EOF Or Not rsPP.BOF Then %>
                                    <%= rsPP.Fields.Item("SUPPLIER").Value %><br>
                                    <%= rsPP.Fields.Item("Address1").Value %><br>
                                    <%= rsPP.Fields.Item("Address2").Value %><br>
                                    <%= rsPP.Fields.Item("Address3").Value %><br>
                                    <%= rsPP.Fields.Item("TownCity").Value %><br>
                                    <%= rsPP.Fields.Item("PostCode").Value %><br>
                                    <%= rsPP.Fields.Item("cOUNTY").Value %> 
                                    <% End If ' end Not rsPP.EOF Or NOT rsPP.BOF %>
                                    <br><br>
                                  </td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="RSLBlack" align=right>
                                    <% If Not rsPP.EOF Or Not rsPP.BOF Then %>
									
                                    <% if (rsPP.Fields.Item("Telephone1").Value <> "") Then %> 
                                    <%= "<i>Telephone</i>:&nbsp;" & rsPP.Fields.Item("Telephone1").Value %>
									<% end if %> 									
                                    <% End If ' end Not rsPP.EOF Or NOT rsPP.BOF %>
                                  </td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="RSLBlack" align=right>
                                    <% If Not rsPP.EOF Or Not rsPP.BOF Then %>
									
                                    <% if (rsPP.Fields.Item("Telephone2").Value <> "") Then %> 
                                    <%= "<i>Telephone 2</i>:&nbsp;" & rsPP.Fields.Item("Telephone2").Value %>
									<% end if %> 									
                                    <% End If ' end Not rsPP.EOF Or NOT rsPP.BOF %>
                                  </td>
                                </tr>
                                <tr> 
                                  <td valign="top" class="RSLBlack" align=right>
                                    <% If Not rsPP.EOF Or Not rsPP.BOF Then %>
									
                                    <% if (rsPP.Fields.Item("Fax").Value <> "") Then %> 
                                    <%= "<i>Fax</i>:&nbsp;" & rsPP.Fields.Item("Fax").Value %>
									<% end if %> 									
                                    <% End If ' end Not rsPP.EOF Or NOT rsPP.BOF %>
                                  </td>
                                </tr>
                              </table>
                              <input type="hidden" name="UserID2" value="<%= Session("svUserID") %>">
            <table border="0" cellspacing="1" cellpadding="1">
                                <tr> 
                                  <td width=120px class="RSLBlack"><b>Invoice
                                     N&deg;:</b></td>
                                  <td  class="RSLBlack"><%= rsPP.Fields.Item("invoiceNumber").Value %></td>
                                </tr>
                                <tr> 
                                  <td width=120px class="RSLBlack"><b>CX 
                                    Order N&deg;:</b></td>
                                  <td  class="RSLBlack"><%= PurchaseNumber(rsPP.Fields.Item("Orderid").Value) %></td>
                                </tr>
                                <tr> 
                                  <td class="RSLBlack"><b>Order 
                                    Date:</b></td>
                                  <td class="RSLBlack"><%= rsPP.Fields.Item("FORMATTEDPODATE").Value %></td>
                                </tr>
                                <tr> 
                                  
                <td class="RSLBlack"><b>Ordered By:</b></td>
                                  <td class="RSLBlack"><%= rsPP.Fields.Item("FIRSTNAME").Value %>&nbsp;<%= rsPP.Fields.Item("LASTNAME").Value %></td>
                                </tr>
                                <tr> 
                                  <td class="RSLBlack"><b>Item:</b></td>
                                  <td class="RSLBlack"><%= rsPP.Fields.Item("ItemRef").Value %></td>
                                </tr>
                                <tr> 
                                  <td class="Printout" valign=top><b>Item Description:</b></td>
                                  <td class="Printout"><%= rsPP.Fields.Item("ItemDesc").Value %></td>
                                </tr>
                                <tr> 
                                  <td class="RSLBlack"><b>Delivery 
                                    Date:</b></td>
                                  <td class="RSLBlack"><%= rsPP.Fields.Item("FORMATTEDDELDATE").Value %></td>
                                </tr>
                                <tr> 
                                  
                <td class="Printout" valign=top><b>Reconciliation 
                  Date:</b></td>
                                  <td class="Printout" height="14"><%= rsPP.Fields.Item("FORMATTEDRECDATE").Value %></td>
                                </tr>

                                <tr> 
                                  <td class="Printout" height="14" valign=top><b>Delivery 
                                    Details:</b></td>
                                  <td class="Printout" height="14"><%= rsPP.Fields.Item("DelDetail").Value %></td>
                                </tr>
                                <tr> 
                                  <td class="Printout"><b>Net Cost:</b></td>
                                  <td class="Printout" align=right width=100><%= FormatCurrency(rsPP.Fields.Item("FORMATTEDNETCOST").Value) %></td>
                                </tr>
                                <tr> 
                                  <td class="Printout"><b>VAT :</b></td>
                                  <td class="Printout" align=right width=100><%= FormatCurrency(rsPP.Fields.Item("FORMATTEDVAT").Value) %></td>
                                </tr>
                                <tr> 
                                  <td class="Printout"><b>Toal Cost:</b></td>
                                  <td class="Printout" align=right width=100><%= FormatCurrency(rsPP.Fields.Item("FORMATTEDCOST").Value) %></td>
                                </tr>
							
                                <tr class="RSLBlack"> 
                                  <td class="RSLBlack">&nbsp;</td>
                                  <td class="RSLBlack">&nbsp;</td>
                                </tr>
                                <tr> 
                                  <td class="Printout"><b>Signed:</b></td>
                                  <td class="Printout">&nbsp;</td>
                                </tr>
                                <tr> 
                                  <td class="RSLBlack">&nbsp;</td>
                                  <td class="RSLBlack">&nbsp;</td>
                                </tr>
                                <tr> 
                                  <td class="RSLBlack">&nbsp;</td>
                                  
          <td class="RSLBlack" nowrap>.............................................................................&nbsp;&nbsp;&nbsp;&nbsp;
            <input name="PrintButton" onClick="PrintThePP()" type='button' class="RSLButton" value="Print purchase order">
          </td>
</tr>
                        </table>
</BODY>
</HTML>
