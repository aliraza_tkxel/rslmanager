<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
Call OpenDB()
dim array_OrderId,orderitem,ORDERID

	If (Request.Form("CHKS").Count > 0) then
	
	        ' LOOP THROUGH ALL THE SELECTED ORDER ITEMS AND PUT THEM TOGETHER IN A STRING VARAIABLE orderitem
	        For each k in Request.Form("CHKS")
	        
	            IF orderitem <> "" THEN
	                  orderitem = orderitem & ","  & k
	            ELSE
	                  orderitem = k
	            END IF
    	           
	        Next
	        ' END OF ORDERITEM LOOP
	        
	        ' GET THE ORDERS OF THE SELECTED ORDER ITEMS. MORE THAN ONE ORDERS WILL BE SEPERATED BY COMMA
	        SQL = "SELECT  distinct orderid FROM F_PURCHASEITEM WHERE ORDERITEMID in (" & orderitem & ")"
	        Call OpenRs(rsOrders, SQL)

                While (NOT rsOrders.EOF)
    	               
	                      IF ORDERID <> "" THEN
	                           ORDERID = ORDERID & ","  & rsOrders("ORDERID")
	                      ELSE
	                           ORDERID = rsOrders("ORDERID") 
	                      END IF
    	            
	                    rsOrders.movenext
	            Wend 
	         Call CloseRs(rsOrders) 

	End if

' SPLIT THE ORDERS IN AN ARRAY array_OrderId
array_OrderId = SPLIT(ORDERID,",")

'CHECK IF RECONCILING IS LOCKED...
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'RECONCILELOCKED' AND DEFAULTVALUE = 0"
Call OpenRs(rsPostable, SQL)

if (NOT rsPostable.EOF) then
	IF Request.Form("hid_RECONCILE") = "RECONCILE" THEN
		
	    ' LOOP THROUGH THE ORDERS ONE BY ONE
	    For j=0 to UBOUND(array_OrderId)
		
		    If (Request.Form("CHKS").Count > 0) then
	
			    'THIS SQL WILL BUILD AN INVOICE BUT WILL SET IT TO UNCONFIRMED AS WE HAVE TO GET THE TOTALS FIRST, BUT WE NEED THE IDENTITY TO USE 
			    'IN THE LINK TABLE LATER ON
			    SQL = "SET NOCOUNT ON;INSERT INTO F_INVOICE (INVOICENUMBER, ORDERID, RECONCILEDATE, TAXDATE, USERID, SUPPLIERID, CONFIRMED) " &_
					    "VALUES ('" & Replace(Request("txt_INVOICENUMBER"),"'","''") & "', " & array_OrderId(j) & ", '" & FormatDateTime(Date,1) & "', '" & FormatDateTime(Request("txt_TAXDATE"),1) & "', " &_
					    SESSION("USERID") & ", " & Request("hid_SUPPLIERID") & ", 0); SELECT SCOPE_IDENTITY() AS INVOICEID"
	
			    'Response.Write(SQL)
			    Call OpenRs(rsINV, SQL)
			    InvoiceID = rsINV("INVOICEID")
			    Call CloseRs(rsINV)
			
			    NetCost = 0
			    VAT = 0
			    GrossCost = 0
	
			    'NEXT LOOP THROUGH THE ORDER ITEMS FOR RECONCILIATION
			    For each i in Request.Form("CHKS")
						    
				    SQL = "SELECT ISNULL(NETCOST,0) AS NETCOST, VAT, ISNULL(GROSSCOST,0) AS GROSSCOST FROM F_PURCHASEITEM WHERE ORDERITEMID = " & i & " AND ORDERID = " & array_OrderId(j)
				    Call OpenRs(rsPI, SQL)
				    If NOT (rsPI.EOF) then
				        NetCost = NetCost + CDbl(rsPI("NETCOST"))
				        VAT = VAT + CDbl(rsPI("VAT"))
				        GrossCost = GrossCost + CDbl(rsPI("GROSSCOST"))
    				      				    
				        Call LOG_PI_ACTION(i, "RECONCILED")							
				        SQL = "INSERT INTO F_ORDERITEM_TO_INVOICE (ORDERITEMID, INVOICEID) VALUES (" & i & ", " & InvoiceID & "); " 
    			        Conn.Execute(SQL)
    			        
    			        'Need to check the all appliance has been reconciled
    			        SQL="UPDATE F_PURCHASEITEM SET PISTATUS = 9, RECONCILEDATE = '" & FormatDateTime(Date,1) & "' WHERE ORDERITEMID = " & i & ";"
				        Conn.Execute(SQL)	
    				    
    				    'UPDATE THE GS_INVOICE TO CONFIRM THE RECONCILIATION
				   	    SQL = " UPDATE GS_INVOICE SET RECONCILED =1 " &_
                            " WHERE REPAIRHISTORYID = " &_
						    "(" &_
						        "	SELECT DISTINCT INV.REPAIRHISTORYID FROM GS_INVOICE INV " &_
						        "	INNER JOIN C_REPAIR REP ON REP.REPAIRHISTORYID=INV.REPAIRHISTORYID " &_
						        "	INNER JOIN P_WOTOREPAIR WOTO ON WOTO.JOURNALID=REP.JOURNALID " &_
						        "	INNER JOIN F_ORDERITEM_TO_INVOICE OI ON OI.ORDERITEMID=WOTO.ORDERITEMID " &_
						        "	WHERE OI.ORDERITEMID=" & i & "" &_
						        "	AND BATCHID=" & Request.Form("hid_BATCHID") & "" &_
						        ") " &_
                            " AND BATCHID= " & Request.Form("hid_BATCHID")
                        Conn.Execute(SQL)    
    				
				    END IF
				    Call CloseRs(rsPI)
				
			    Next
			    ' END OF ORDER ITEM LOOP
	
			    'NOW USE THE TOTALS FROM ABOVE TO UPDATE THE INVOICE AND SET IT CONFIRMED
			    SQL = "UPDATE F_INVOICE SET NETCOST = " & NetCost & ", VAT = " & VAT & ", GROSSCOST = " & GrossCost & ", CONFIRMED = 1 " &_
					    "WHERE INVOICEID = " & InvoiceID
			    Conn.Execute(SQL)
   			
			    'FINALLY WE NEED TO CHECK IF ALL ITEMS HAVE BEEN FULLY COMPLETED, IF SO THEN RECONCILE THE MASTER PURCHASE ORDER.
			    SQL = "SELECT ORDERITEMID FROM F_PURCHASEITEM WHERE PISTATUS < 9 AND ACTIVE = 1 AND ORDERID = " & array_OrderId(j)
			    Call OpenRs(rsPI, SQL)
			    if (rsPI.EOF) then
				    Call LOG_PO_ACTION(array_OrderId(j), "RECONCILED", "")										
				    SQL = "UPDATE F_PURCHASEORDER SET RECONCILEDATE = '" & FormatDateTime(Date,1) & "', POSTATUS = 9 " &_
						    "WHERE ORDERID = " & array_OrderId(j)
				    Conn.Execute SQL
				    RedirectPageNumber = 1
			    end if
			    Call CloseRs(rsPI)
			
			End if    
		Next
		' END OF ORDER LOOP
		RedirectPageNumber = Request("hid_PAGE")
			
			
		
		
	END IF
Else
	Call CloseRs(rsPostable)
	Call CloseDB()
	Response.Redirect ("../GasReconcileConfirm.asp?BatchId=" & Request("hid_BATCHID") & "&page=" & Request("hid_PAGE") & "&ER12" & Replace(Date, "/", "") & "=1")
End If
Call CloseRs(rsPostable)
Call CloseDB()
Response.Redirect ("../GasReconcileList.asp?page=" & RedirectPageNumber)
%>