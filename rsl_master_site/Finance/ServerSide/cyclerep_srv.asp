<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
	Dim ID, LoaderString
	Dim FieldCounter
	FieldCounter = 6
	reDim DataFields   (FieldCounter)
	reDim DataTypes    (FieldCounter)
	reDim ElementTypes (FieldCounter)
	reDim FormValues   (FieldCounter)
	reDim FormFields   (FieldCounter)
	
	UpdateID	  = "hid_PMID"
	FormFields(0) = "txt_STARTDATE|TEXT"
	FormFields(1) = "txt_CYCLES|TEXT"
	FormFields(2) = "txt_EXPIRYDATE|TEXT"
	FormFields(3) = "hid_DevelopmentID|TEXT"
	FormFields(4) = "sel_BLOCK|SELECT"
	FormFields(5) = "sel_REPAIR|SELECT"
	FormFields(6) = "sel_CONTRACTOR|SELECT"
 

	// ENTER NEW WARRANTY RECORD
	Function NewRecord ()
		ID = Request.Form(UpdateID)		
		Call MakeInsert(strSQL)	
		SQL = "INSERT INTO P_PLANNEDMAINTENANCE " & strSQL & ""
		Response.Write SQL
		Conn.Execute SQL, recaffected	
	End Function
	
	// AMEND WARRANTY RECORD
	Function AmendRecord()
		ID = Request.Form(UpdateID)	
		Call MakeUpdate(strSQL)
		
		SQL = "UPDATE P_PLANNEDMAINTENANCE " & strSQL & " WHERE PMID = '" & ID & "'"
		
		Response.Write SQL
		Conn.Execute SQL, recaffected
	End Function
	
	Function DeleteRecord()
		ID = Request.Form(UpdateID)	
		
		SQL = "DELETE FROM P_PLANNEDMAINTENANCE WHERE PMID = '" & ID & "'"
		Conn.Execute SQL, recaffected
	End Function
	
	// BUILD LOAD STRING --// NOT USED AS YET
	Function LoadRecord()
	
		pm_id = Request.Form("hid_PMID")
	
		SQL = "SELECT 	ISNULL(B.BLOCKNAME,'Scheme Wide') AS CBLOCK, " &_
					"ISNULL(CONVERT(NVARCHAR, P.STARTDATE, 103), '') AS CSTART, " &_
					"ISNULL(P.CYCLES,'') AS CCYCLE, " &_
					"ISNULL(R.DESCRIPTION ,'') AS CREPAIR, " &_
					"ISNULL(CONVERT(NVARCHAR, P.LASTEXECUTED, 103), '') AS CLAST,	 " &_
					"ISNULL(P.NOTES ,'') AS CNOTES, " &_
					"ISNULL(S.NAME,'') AS CONTRACTOR " &_
			  	"FROM 	P_PLANNEDMAINTENANCE P " &_
			  		"LEFT JOIN P_BLOCK B ON P.BLOCK = B.BLOCKID " &_
					"INNER JOIN R_ITEMDETAIL R ON P.REPAIR = R.ITEMDETAILID " &_
					"INNER JOIN S_ORGANISATION S ON P.CONTRACTOR = S.ORGID" &_
				"WHERE 	P.DEVELOPMENTID = " & development_id	
				
		Call OpenRs(rsLoader, SQL)
		LoaderString = ""
		if (NOT rsLoader.EOF) then
			LoaderString = LoaderString & "parent.RSLFORM.sel_WARRANTYTYPE.value = """ & rsLoader("WARRANTYTYPE") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.sel_AREAITEM.value = """ & rsLoader("AREAITEM") & """;"			
			LoaderString = LoaderString & "parent.RSLFORM.sel_CONTRACTOR.value = """ & rsLoader("CONTRACTOR") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_NOTES.value = """ & rsLoader("NOTES") & """;"	
			LoaderString = LoaderString & "parent.RSLFORM.txt_EXPIRYDATE.value = """ & rsLoader("EXPIRYDATE") & """;"	
		end if
		CloseRs(rsLoader)
		
	End Function
	
	TheAction = Request("hid_Action")
	development_id = Request.Form("hid_DevelopmentID")
	response.write "THE ACTION IS : " & theaction & "    DEVID  IS : " & development_id
	OpenDB()
	//Select Case TheAction
	//	Case "NEW"		NewRecord()
	//	Case "AMEND"	AmendRecord()
	//	Case "DELETE"   DeleteRecord()
	//	Case "LOAD"	    LoadRecord()
	//End Select
	CloseDB()
%>
<html>
<body>
<script language=javascript>
function ReturnData(){
	if ("<%=TheAction%>" != "LOAD")
		parent.location.href = "../CyclicalRepairs.asp?DevelopmentID=<%=development_id%>"
	else {
		<%=LoaderString%>
		parent.checkForm();
		//parent.SetButtons('AMEND');
		}
		
}
</script>
</body>
</html>
