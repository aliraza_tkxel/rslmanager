<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim tenenacy_id,ACCOUNT_BALANCE,ERROR

	OpenDB()
	tenancy_id = Request.FORM("hid_TENANTNUMBER")
	tenancy_id1 = Request.FORM("hid_TENANTNUMBER1")
	'get tenancy account closing balance
	get_account_balance()
    Check_Customer_Ids()
	CloseDB()
    
   Function get_account_balance()
       'THIS WILL GET THE BALANCE FOR THE CURRENT CUSTOMER TENANCY
	    strSQL ="SELECT 	ISNULL(SUM(J.AMOUNT), 0) AS AMOUNT " &_
				"FROM	 	F_RENTJOURNAL J " &_
				"			LEFT JOIN F_TRANSACTIONSTATUS S ON J.STATUSID = S.TRANSACTIONSTATUSID " &_
				"WHERE		J.TENANCYID = " & tenancy_id & " AND (J.STATUSID NOT IN (1,4) OR J.PAYMENTTYPE IN (17,18))"
	
	    Call OpenRs (rsSet, strSQL) 
	    ACCOUNT_BALANCE = rsSet("AMOUNT")
	    CloseRs(rsSet)
	    'END OF GET BALANCE
   End Function

   Function Check_Customer_Ids()
        'THIS CHECKS WHETHER THE CUSTOMERID OF BOTH THE TENANCY ARE SAME OR NOT
        strSQL=" SELECT COUNT(SUB.CUSTOMERID) AS  NO_TENANCY,SUB.CUSTOMERID AS CUSTOMERID "&_ 
               " FROM (                                                                   "&_
               "        SELECT CUSTOMERID                                                 "&_ 
               "        FROM C_CUSTOMERTENANCY                                            "&_ 
               "        WHERE TENANCYID IN (" & tenancy_id & " , " & tenancy_id1 & " )    "&_   
               "      ) SUB                                                               "&_   
               " GROUP BY SUB.CUSTOMERID                                                  "&_   
               " HAVING COUNT(SUB.CUSTOMERID)>1                                           "
                  
   	    Call OpenRs (rsSet, strSQL)
   	    
   	    if not rsSet.EOF then
   	        ERROR=""
   	    else   
   	        ERROR="Incorrect tenancy id for balance transfer. Please enter the tenancy id for the same customer." 
   	    end if  
		CloseRs(rsSet)
        
   End Function 
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<script type="text/javascript"  language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript">
function ReturnData(){

	<%if ERROR= "" then %>
	
	    parent.RSLFORM.txt_AMOUNT.value=<%=ACCOUNT_BALANCE%>
	    parent.RSLFORM.hid_AMOUNT.value=<%=ACCOUNT_BALANCE%>
	    parent.RSLFORM.txt_FROM.value="<%=Date()%>";
	    parent.RSLFORM.txt_TO.value="<%=Date()%>";
	    parent.STATUS_DIV.style.visibility = "hidden";
	
	<%else%>
	    parent.STATUS_DIV.style.visibility = "hidden";
	    parent.RSLFORM.sel_DIRECTPOSTITEMS.value=""
	    parent.details1.innerHTML=""
	    parent.RSLFORM.txt_TENANTSEARCH1.value="" 
	    parent.RSLFORM.txt_TENANTSEARCH1.focus();
	    
	    parent.alert("<%=ERROR%>")    
	<%end if %>
	return;
	}
	
</script>
<body onload="ReturnData()">
</body>
</HTML>	
