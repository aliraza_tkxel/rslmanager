<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%

	Dim strWhat, count, optionvalues, optiontext, intReturn, theamounts,poNumber
	
	OpenDB()
	
	Dim POName
	POName = ""
	if(Request.Form("txt_PONAME") <> "") then POName = Replace(Request.Form("txt_PONAME"), "'", "''")

	Dim PODate
	PODate = ""
	if(Request.Form("txt_PODATE") <> "") then PODate = FormatDateTime(Request.Form("txt_PODATE"),1)
	
	Dim DelDate
	DelDate = "NULL"
	if(Request.Form("txt_DELDATE") <> "") then DelDate = "'" & FormatDateTime(Request.Form("txt_DELDATE"),1) & "'"
	
	Dim PONOTES
	PONOTES = ""
	if(Request.Form("txt_PONOTES") <> "") then PONOTES = Replace(Request.Form("txt_PONOTES"), "'", "''")
	
	Dim Supplier
	Supplier = "NULL"
	if(Request.Form("sel_SUPPLIER") <> "") then Supplier = Request.Form("sel_SUPPLIER")
    
	Dim Development
	Development = "NULL"
	if(Request.Form("sel_DEVELOPMENT") <> "") then Development = Request.Form("sel_DEVELOPMENT")

	Dim Block
	Block = "NULL"
	if(Request.Form("hid_BLOCK") <> "") then Block = Request.Form("hid_BLOCK")

 	Dim CompanyId
	CompanyId = "NULL"
	if(Request.Form("sel_COMPANY") <> "") then CompanyId = Request.Form("sel_COMPANY")

	SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEORDER (PONAME, PODATE, EXPPODATE, PONOTES, USERID, SUPPLIERID, DEVELOPMENTID, BLOCKID, ACTIVE, POTYPE, POSTATUS, COMPANYID, IsServiceChargePO) VALUES " &_
			"('" & POName & "', '" & PODate & "', " & DelDate & ", '" & PONOTES & "', " & Session("USERID") & ", " & Supplier & ", " &_
			" " & Development & ", " & Block & ", 1, 1, 1, " & CompanyId & ",1);SELECT SCOPE_IDENTITY() AS ORDERID;SET NOCOUNT OFF"
	
	Call OpenRs(rsPurchase, SQL)		
	OrderID = rsPurchase("ORDERID")
	poNumber = OrderID
	CloseRs(rsPurchase)
	
	isQueued = false
	for each item in Request.Form("ID_ROW")	
		Data = Request.Form("iData" & item)	
			
		DataArray = Split(Data, "||<>||")

		ItemRef = Replace(DataArray(0), "'", "''")
		ItemDesc = Replace(DataArray(1), "'", "''")
		VatTypeID = DataArray(2)
		
		NetCost = DataArray(3)
		
		VAT = DataArray(4)
		GrossCost = DataArray(5)
		ExpenditureID = DataArray(6)
		SaveStatus = DataArray(8)
    
        SBPData = DataArray(9)

        SBPArray = Split(SBPData, ";")			
		
		if (SaveStatus = 0) then
			isQueued = true
		end if
		
		SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS,REFERENCENO) VALUES " &_
				"(" & OrderID & ", " & ExpenditureID & ", '" & ItemRef & "', '" & ItemDesc & "', '" & PODate & "', " & DelDate & ", " &_
				" " & NetCost & ", " & VatTypeID & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 1, "& SaveStatus &","& item & "); SELECT SCOPE_IDENTITY() AS PurchaseItemId;SET NOCOUNT OFF"
	
		Call OpenRs(rsPI, SQL)

	    orderItemId = rsPI("PurchaseItemId")
	    'poNumber = OrderID

        'SQL = "SELECT ORDERITEMID FROM F_PURCHASEITEM " &_
        '       " WHERE ORDERID = " & OrderID & " ORDER BY PIDATE"
        
        'while not rsPI.EOF
         '   orderItemId  = rsPI("ORDERITEMID")
         '   rsPI.movenext
        'Wend
        CloseRs(rsPI)
        
        for each strSBP in SBPArray
            SBP = Split(strSBP,",")
            if(SBP(2) = "null") then
                SQL = "INSERT INTO F_PurchaseItemSCInfo (OrderId,OrderItemId,SchemeId,BlockId,PropertyId,IsActive) VALUES " &_
                "( " & OrderID & ", " & orderItemId & ", " & SBP(0) & ", " & SBP(1) & ", null, 1 )"
            else
                SQL = "INSERT INTO F_PurchaseItemSCInfo (OrderId,OrderItemId,SchemeId,BlockId,PropertyId,IsActive) VALUES " &_
                "( " & OrderID & ", " & orderItemId & ", " & SBP(0) & ", " & SBP(1) & ", '" & SBP(2) & "', 1 )"
            end if
            Response.Write(SQL)
            Conn.Execute SQL
        next
	next
	    
	if (isQueued = true) then
		SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 0 WHERE ORDERID = " & OrderID
		Conn.Execute SQL
        call sendEmail(OrderID )
    else 
        call SendEmailToContractor(Supplier, OrderID)
	end if

    Function SendEmailToContractor(Supplier, OrderID )
        SQL = " SELECT top 1 E.FIRSTNAME + ' ' + E.LASTNAME As ContractorName, C.WORKEMAIL AS Email FROM E__EMPLOYEE E " &_
              "  INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID " &_
              "  LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
              "  WHERE J.ACTIVE = 1 AND E.ORGID = " & Supplier & " "
        Call OpenRs(rsContractor, SQL)

        while not rsContractor.EOF
            contractorName = rsContractor("ContractorName")
            email = rsContractor("Email")
            rsContractor.movenext()
        wend

        SQL = "SELECT DISTINCT E.FIRSTNAME + ' ' + E.LASTNAME AS RasiedBy  " &_
              "    , PI.ITEMNAME AS ItemName  " &_
              "    , PI.ITEMDESC AS ItemNotes  " &_
              "    , PI.GrossCost AS GrossCost  " &_
              "    , cOM.description AS Company  " &_
              " FROM  " &_
              "    F_PURCHASEITEM PI  " &_
              "    INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS  " &_
              "    LEFT JOIN E__EMPLOYEE E ON PI.USERID = E.EMPLOYEEID   " &_
              "    INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID  " &_
              "    INNER JOIN G_COMPANY  cOM on cOM.COMPANYID =  PO.COMPANYID  " &_
              " WHERE 1 = 1  " &_
              "    AND ( PI.ACTIVE = 1 )    " &_
              "    AND ( PI.PISTATUS = 0 OR PI.PISTATUS = 1)   " &_
              "    AND PI.ORDERID = " & OrderID & " "

        Call OpenRs(rsEmail, SQL)
        emailbody = getHTMLEmailBodyForConrtractor()
        
        Set iMsg = CreateObject("CDO.Message")
        
        Set iConf = CreateObject("CDO.Configuration")
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        Set iMsg.Configuration = iConf

        iMsg.HTMLBody = emailbody

        ' Here's the good part, thanks to some little-known members.
        ' This is a BodyPart object, which represents a new part of the multipart MIME-formatted message.
        ' Note you can provide an image of ANY name as the source, and the second parameter essentially
        ' renames it to anything you want.  Great for giving sensible names to dynamically-generated images.        
        Set objBP = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/BHA_TAB.gif"), "broadlandImage",cdoRefTypeId)
        ' Now assign a MIME Content ID to the image body part.
        ' This is the key that was so hard to find, which makes it 
        ' work in mail readers like Yahoo webmail & others that don't
        ' recognise the default way Microsoft adds it's part id's,
        ' leading to "broken" images in those readers.  Note the
        ' < and > surrounding the arbitrary id string.  This is what
        ' lets you have SRC="cid:broadlandImage" in the IMG tag.
        objBP.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadlandImage>"
        objBP.Fields.Update

        'Set invariant fields outside loop.
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = "Confirmation of Purchase Order"
		    
	    while not rsEmail.EOF 
	     	    
		   strRecipient  = email
           	
            emailbody = getHTMLEmailBodyForConrtractor()
 
            emailbody = Replace(emailbody, "{ContractorName}",contractorName)
            emailbody = Replace(emailbody, "{PONumber}","PO "&poNumber)
            emailbody = Replace(emailbody, "{Company}",rsEmail("Company"))
            emailbody = Replace(emailbody, "{ItemName}",rsEmail("ItemName"))
            emailbody = Replace(emailbody, "{ItemNotes}",rsEmail("ItemNotes"))
            emailbody = Replace(emailbody, "{GrossCost}",FormatNumber(rsEmail("GROSSCOST"), 2))            
            emailbody = Replace(emailbody, "{RaisedBy}",rsEmail("RasiedBy"))
            iMsg.To = strRecipient 
            iMsg.HTMLBody = emailbody
            iMsg.Send
	       ' Check next record to send email.
            rsEmail.movenext
		Wend
        CloseRs(rsEmail)
        Set iMsg = Nothing
        SET iConf = Nothing
        SET Flds = Nothing
        SET objBP = Nothing
    End Function

	Function SendEmail(OrderID)
        Dim strRecipient, iMsg, iConf, Flds,emailbody, objBP
        
        SQL = " SELECT DISTINCT c.WorkEmail " & _
    	      "    ,E.FIRSTNAME + ' ' + E.LASTNAME AS RasiedBy " & _
	          "    ,BH.FIRSTNAME + ' ' + BH.LASTNAME AS BudgetHolder " & _
              "    , PI.ITEMNAME AS ItemName " & _
              "    , PI.ITEMDESC AS ItemNotes " & _
              "    , PI.GrossCost AS GrossCost " & _
              "    , cOM.description AS Company " & _
              " FROM " & _
              "    F_PURCHASEITEM PI " & _
              "    INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " & _
              "    LEFT JOIN E__EMPLOYEE E ON PI.USERID = E.EMPLOYEEID  " & _
              "    INNER JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID  AND EL.LIMIT >= PI.GROSSCOST " & _
              "    AND EL.LIMIT = (SELECT MIN(LIMIT) FROM F_EMPLOYEELIMITS WHERE LIMIT > PI.GROSSCOST AND EXPENDITUREID = PI.EXPENDITUREID)  " & _ 
              "    INNER JOIN E__EMPLOYEE BH ON EL.EMPLOYEEID = BH.EMPLOYEEID " & _
              "    INNER JOIN E_CONTACT  c on c.EMPLOYEEID =  BH.EMPLOYEEID " & _
              "    INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = PI.ORDERID " & _
              "    INNER JOIN G_COMPANY  cOM on cOM.COMPANYID =  PO.COMPANYID " & _
              " WHERE 1 = 1 " & _
              "     AND ( PI.ACTIVE = 1 )  " & _ 
              "    AND ( PI.PISTATUS = 0 ) " & _ 
              "    AND PI.ORDERID = " & OrderID & " "
        Call OpenRs(rsEmail, SQL)
        emailbody = getHTMLEmailBody()
        
        Set iMsg = CreateObject("CDO.Message")
        
        Set iConf = CreateObject("CDO.Configuration")
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        Set iMsg.Configuration = iConf

        iMsg.HTMLBody = emailbody

        ' Here's the good part, thanks to some little-known members.
        ' This is a BodyPart object, which represents a new part of the multipart MIME-formatted message.
        ' Note you can provide an image of ANY name as the source, and the second parameter essentially
        ' renames it to anything you want.  Great for giving sensible names to dynamically-generated images.        
        Set objBP = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/BHA_TAB.gif"), "broadlandImage",cdoRefTypeId)
        ' Now assign a MIME Content ID to the image body part.
        ' This is the key that was so hard to find, which makes it 
        ' work in mail readers like Yahoo webmail & others that don't
        ' recognise the default way Microsoft adds it's part id's,
        ' leading to "broken" images in those readers.  Note the
        ' < and > surrounding the arbitrary id string.  This is what
        ' lets you have SRC="cid:broadlandImage" in the IMG tag.
        objBP.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadlandImage>"
        objBP.Fields.Update

        'Set invariant fields outside loop.
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = "Queued Purchase Order"
		    
	    while not rsEmail.EOF 
	     	    
		   strRecipient  = rsEmail("WORKEMAIL")
           	
            emailbody = getHTMLEmailBody()
 
            emailbody = Replace(emailbody, "{BudgetHolder}",rsEmail("BudgetHolder"))
            emailbody = Replace(emailbody, "{PONumber}","PO "&poNumber)
            emailbody = Replace(emailbody, "{SupplierName}",Request.Form("txt_SupplierName"))
            emailbody = Replace(emailbody, "{Company}",rsEmail("Company"))
            emailbody = Replace(emailbody, "{ItemName}",rsEmail("ItemName"))
            emailbody = Replace(emailbody, "{ItemNotes}",rsEmail("ItemNotes"))
            
            emailbody = Replace(emailbody, "{GrossCost}",FormatNumber(rsEmail("GROSSCOST"), 2))            
            emailbody = Replace(emailbody, "{RaisedBy}",rsEmail("RasiedBy"))
            iMsg.To = strRecipient 
            iMsg.HTMLBody = emailbody
            iMsg.Send
	       ' Check next record to send email.
            rsEmail.movenext
		Wend
        CloseRs(rsEmail)
        Set iMsg = Nothing
        SET iConf = Nothing
        SET Flds = Nothing
        SET objBP = Nothing
    End Function
    
    Function currentPageURL()
         dim s ,protocol, port

         if Request.ServerVariables("HTTPS") = "on" then 
           s = "s"
         else 
           s = ""
         end if  
 
         protocol = strleft(LCase(Request.ServerVariables("SERVER_PROTOCOL")), "/") & s 

         if Request.ServerVariables("SERVER_PORT") = "80" OR Request.ServerVariables("SERVER_PORT") = "443" then
           port = ""
         else
           port = ":" & Request.ServerVariables("SERVER_PORT")
         End if  

         currentPageURL = protocol & "://" & Request.ServerVariables("SERVER_NAME") & port
                      '& Request.ServerVariables("SCRIPT_NAME")
    End Function

    Function strLeft(str1,str2)
        strLeft = Left(str1,InStr(str1,str2)-1)
    End Function

	if (REQUEST("REDIRECT") = 1) then
		Response.Redirect "/Portfolio/iFrames/iPurchasesDev.asp?DEVID=" & Development & "&SyncTabs=1"'
	elseif (REQUEST("REDIRECT") = 2) then
		Response.Redirect "/Portfolio/iFrames/iPurchasesBlo.asp?blockid=" & Block & "&SyncTabs=1"
	else	
		Response.Redirect "../PurchaseList.asp"										
	End if

    Function getHTMLEmailBody()

        getHTMLEmailBody = "<!DOCTYPE html><html><head><title>Queued Purchase Order</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>A Purchase Order has been queued and requires your approval, the details are as follows:</p> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                      <tr><td>Company:</td><td>{Company}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(�):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>Queued</td></tr> " & _
                           "                     <tr><td>Raised by:</td><td>{RaisedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "                         <td class=""bottomAllignedCell""> " & _
                           "                             Broadland Housing Group<br /> " & _
                           "                             NCFC, The Jarrold Stand<br /> " & _
                           "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function

     Function getHTMLEmailBodyForConrtractor()

        getHTMLEmailBodyForConrtractor = "<!DOCTYPE html><html><head><title>Confirmation of Purchase Order</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "             </style> " & _
                           "             <span>Dear {ContractorName}</span> " & _
                           "             <p>A Purchase Order has been authorize directly, the details are as follows:</p> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                      <tr><td>Company:</td><td>{Company}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(�):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>Good Ordered.</td></tr> " & _
                           "                     <tr><td>Raised by:</td><td>{RaisedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "                         <td class=""bottomAllignedCell""> " & _
                           "                             Broadland Housing Group<br /> " & _
                           "                             NCFC, The Jarrold Stand<br /> " & _
                           "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function
    
    CloseDB()
%>
