
<% ByPassSecurityAccess = true %>
<%
OpenDB()
Function voidCyclicEmailToContractor(ORDERID,typ)
            
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName   " &_
		                " ,FCW.PurchaseOrderId as OrderId   " &_
                        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId " &_  
		                " ,FCW.JOURNALID as JSN   " &_
		                " ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy   " &_
		                " ,ISNULL( C.WORKDD,'N/A') AS DDial   " &_
		                " ,FCW.PDRContractorId as PDRContractorId    " &_
                        " ,E_CONTACT.WORKEMAIL as WorkEmail    " &_
                        " ,FP.userID As userId  " &_
                        " ,ISNULL(FCW.EstimateRef , 'N/A')as EstimateRef ,ISNULL(FCW.Estimate , 'N/A')as Estimate " &_
		                " FROM	PDR_CONTRACTOR_WORK AS FCW  " &_
		                "     INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID  " &_
		                "     INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID   " &_
                        "     INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseORDERID  " &_
                        "        WHERE	FCW.PurchaseOrderId =  " & ORDERID
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    ContactName = rs("ContactName")
                    OrderId = rs("OrderId")
                    JSG = rs("JSN")
                    DDial = rs("DDial")
                    OrderedBy = rs("OrderedBy")
                    PDRContractorId = rs("PDRContractorId")
                    userId = rs("userId")
                    Recipient = rs("WorkEmail")
                    EstimateRef = rs("EstimateRef")
                    Estimate = rs("Estimate")
                end if
                Call CloseRs(rs)
                
                ' GETTING PROPERTY DETAILS
                SQL = "SELECT " &_
	              "  PROPERTY.PROPERTYID, " &_
	              "  HOUSENUMBER, " &_
	              "  FLATNUMBER, " &_
	              "  PROPERTY.ADDRESS1, " &_
	              "  PROPERTY.ADDRESS2, " &_
	              "  PROPERTY.ADDRESS3, " &_
	              "  ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY, " &_
	              "  ISNULL(PROPERTY.COUNTY,'N/A') as COUNTY, " &_
	              "  ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE, " &_
	              "  ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '') " &_
	              "  + ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress] " &_
	              "  ,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block " &_
	              "  ,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme " &_
	              "  ,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms " &_
                  "  FROM	PDR_JOURNAL " &_
		          "      INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		          "      LEFT JOIN P__PROPERTY PROPERTY ON PDR_MSAT.PropertyId = PROPERTY.PropertyId " &_
		          "      LEFT JOIN P_BLOCK ON PROPERTY.BlockId = P_BLOCK.BLOCKID " &_
		          "      LEFT JOIN P_SCHEME ON P_BLOCK.SchemeId = P_SCHEME.SCHEMEID " &_
		          "      LEFT JOIN " &_
		          "      (SELECT  " &_
			      "          PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID, " &_
			      "          ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS " &_
			      "          FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES  " &_
			      "          LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId " &_
			      "          LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID " &_
			      "          LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID " &_
			      "          WHERE  " &_
			      "              " &_
			      "           PARAMETER.ParameterName = 'Quantity'  " &_
			      "           AND  ITEM.ItemName = 'Bedrooms' " &_
		          "      ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID " &_
                  "   WHERE JOURNALID = " & JSG
            Call OpenRs(rs, SQL)
            if not rs.eof then
                FullAddress = rs("FullStreetAddress")
                TownCity = rs("TOWNCITY")
                Postcode = rs("POSTCODE")
                COUNTY = rs("COUNTY")
                Block = rs("Block")
                Scheme = rs("Scheme")
            else
                FullAddress = "N/A"
                TownCity = "N/A"
                Postcode = "N/A"
                COUNTY = "N/A"
                Block = "N/A"
                Scheme = "N/A"
            end if
             Call CloseRs(rs)

                'GETTING CUSTOMER ID
                SQL =   "SELECT C__CUSTOMER.CustomerId AS CUSTOMERID FROM	PDR_JOURNAL " &_
		                "    INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		                "    INNER JOIN P__PROPERTY ON  PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  " &_
		                "    INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  AND C_TENANCY.ENDDATE IS NULL " &_
		                "    INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID  " &_
						"		AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID) " &_
						"													FROM	C_CUSTOMERTENANCY  " &_
						"													WHERE	TENANCYID=C_TENANCY.TENANCYID  " &_
						"													) " &_
		                "        INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID " &_
                        "    WHERE	PDR_JOURNAL.JOURNALID = " & JSG
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    CUSTOMERID = rs("CUSTOMERID")
                else
                     CUSTOMERID = "N/A"
                end if
                Call CloseRs(rs)

                'GETTING CUSTOMER DETAILS
                if CUSTOMERID = "N/A" then 
                    TenantName = "N/A"
                    Telephone = "N/A"
                    TelephoneWork = "N/A"
                    Mobile = "N/A"
                    Email = "N/A"
                else
                    SQL =   " select ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName  " &_
		                    " ,ISNULL(A.TEL, '') as Telephone  " &_
		                    " ,ISNULL(A.TELWORK, '') as TelephoneWork " &_ 
		                    " ,ISNULL(A.MOBILE, '') as Mobile   " &_
                            " ,ISNULL(A.Email, 'enterprisedev@broadlandgroup.org') as Email    " &_
                            " ,ISNULL(A.COUNTY, 'N/A') as COUNTY   " &_ 
		                    " from C__CUSTOMER AS CC   " &_
		                    " INNER JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1  " &_
		                    " where CC.CustomerId='" & CUSTOMERID & "'" 
                    Call OpenRs(rs, SQL)
                    TenantName = rs("TenantName")
                    Telephone = rs("Telephone")
                    TelephoneWork = rs("TelephoneWork")
                    Mobile = rs("Mobile")
                    Email = rs("Email")
                
                    Call CloseRs(rs)
                end if

                '------------------------------------------------
                ' GET WORKS REQUIRED INFO
                '------------------------------------------------

                SQL = "	SELECT	DISTINCT	ServiceRequired   " &_
	            " FROM	PDR_CONTRACTOR_WORK_DETAIL    "  &_
	            " WHERE	 PDRContractorId  = " & PDRContractorId 

                Call OpenRs(rs, SQL)
    
                WorkRequired = ""
                while not rs.EOF
                    WorkRequired = WorkRequired & rs("ServiceRequired") & " <br />"
                    rs.movenext()
                wend

                Call CloseRs(rs)  

                '------------------------------------------------
                ' GET WORKS COST INFO
                '------------------------------------------------

                SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			            " ,SUM(Vat) as VAT "  &_
			            " ,SUM(Gross) as Gross "  &_
	                    " FROM	PDR_CONTRACTOR_WORK_DETAIL "  &_
	                    " WHERE	 PDRContractorId  = " & PDRContractorId

                Call OpenRs(rs, SQL)
                    NetCost = rs("NetCost")
                    VAT = rs("VAT")
                    Gross = rs("Gross")
                Call CloseRs(rs)  

    
                '------------------------------------------------
                ' GET RISK INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     RiskInfo = "N/A"
                else
                SQL = " SELECT	DISTINCT	CATDESC, SUBCATDESC "   &_
	                    " FROM	RISK_CATS_SUBCATS("& CustomerId & ") "

                Call OpenRs(rs, SQL)
                RiskInfo = ""
                while not rs.EOF
                    RiskInfo = RiskInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br />"
                    rs.movenext()
                wend
                Call CloseRs(rs) 
                end if 

    	        if RiskInfo = "" then
                    RiskInfo = "N/A"
                end if

                '------------------------------------------------
                ' GET VULNARABILITY INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     VulnarabilityInfo = "N/A"
                else
                SQL =    " SELECT DISTINCT CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC "  &_
	                        " FROM	C_CUSTOMER_VULNERABILITY CV "  &_
		                    " INNER JOIN C_JOURNAL J ON CV.JOURNALID = J.JOURNALID "  &_
		                    " INNER JOIN C_VULNERABILITY CCV ON CCV.JOURNALID = J.JOURNALID "  &_
			                " INNER JOIN C_VULNERABILITY_CATEGORY CAT ON CAT.CATEGORYID = CV.CATEGORYID  "  &_ 
			                " INNER JOIN C_VULNERABILITY_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CV.SUBCATEGORYID and cv.categoryid=SUBCAT.categoryid  "  &_
                            " WHERE	 CV.CUSTOMERID ="& CustomerId &" "  &_
			                    "AND ITEMNATUREID = 61 "  &_
			                    "AND CCV.ITEMSTATUSID <> 14 "  &_
			                    "AND CV.VULNERABILITYHISTORYID = (	SELECT	MAX(VULNERABILITYHISTORYID) "  &_
												            "FROM	C_VULNERABILITY IN_CV "  &_
												            "WHERE	IN_CV.JOURNALID = J.JOURNALID) "   


                Call OpenRs(rs, SQL)
                VulnarabilityInfo = ""
                while not rs.EOF
                    VulnarabilityInfo = VulnarabilityInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br /> "
                    rs.movenext()
                wend
                Call CloseRs(rs)  
                end if

                if VulnarabilityInfo = "" then
                    VulnarabilityInfo = "N/A"
                end if

                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Dim emailbody
                emailbody =  getContractorEmailTemplateVoidCyclic()
                emailbody = Replace(emailbody,"{Servicing}",typ)
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{Ref}",JSG)
                emailbody = Replace(emailbody,"{PONumber}","PO " & OrderId)
                emailbody = Replace(emailbody,"{ServiceRequired}",WorkRequired)
                emailbody = Replace(emailbody,"{EstimateRef}",EstimateRef)
                emailbody = Replace(emailbody,"{Estimate}",Estimate)
                emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
                emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
                emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
                emailbody = Replace(emailbody,"{Address}",FullAddress)
                emailbody = Replace(emailbody,"{TenantName}",TenantName)
                emailbody = Replace(emailbody,"{TownCity}",TownCity)
                emailbody = Replace(emailbody,"{County}",COUNTY)
                emailbody = Replace(emailbody,"{PostCode}",Postcode)
                emailbody = Replace(emailbody,"{Block}",Block)
                emailbody = Replace(emailbody,"{Scheme}",Scheme)
                emailbody = Replace(emailbody,"{Telephone}",Telephone)
                emailbody = Replace(emailbody,"{RiskDetail}",RiskInfo)
                emailbody = Replace(emailbody,"{VulnerabilityDetail}",VulnarabilityInfo)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
                sendEmailVoidCyclic emailbody,Recipient,typ

        '------------------------------------------------------------

End Function




    '--------------------------  TEST

    Function sendEmailVoidCyclic(body,recipient,typ)
	
     if IsNull(recipient) then

        session("noRecipient") = "true"
		Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields


        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        emailbody = body
        if typ = "Cyclic Maintenance" then
            emailSubject = "Cyclic Maintenance has been Assigned"
        end if
        if typ = "M&E Servicing" then
            emailSubject = "M&E Servicing has been Assigned"
        end if
        Set iMsg.Configuration = iConf
        iMsg.To = recipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send	

        end if

    End Function


    Function getContractorEmailTemplateVoidCyclic()
        Dim assignToContractorEmailTemplateVoidCyclic
        assignToContractorEmailTemplateVoidCyclic = "<table> <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2"" > Please find below details of {Servicing} works required: </td> </tr> <tr> <td> &nbsp; </td> </tr><tr> <td>PO Number:</td> <td>{PONumber}</td> </tr> <tr> <td style=""padding-right: 20px;""> Ref number: </td> <td > {Ref} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Address: </td> <td> <table> <tr> <td style=""padding-right: 50px;""> {Address} </td> <td style=""padding-right: 20px;""> Tenant: </td> <td> {TenantName} </td> </tr> <tr> <td style=""padding-right: 50px;""> {TownCity} </td> <td style=""padding-right: 20px;""> Telephone: </td> <td> {Telephone} </td> </tr> <tr> <td style=""padding-right: 50px;""> {County} </td> <td style=""padding-right: 20px; vertical-align:top;""> Risk: </td> <td> {RiskDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> {PostCode} </td> <td style=""padding-right: 20px;vertical-align:top;""> Vulnerability: </td> <td> {VulnerabilityDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> {Block} </td> <td style=""padding-right: 20px;vertical-align:top;""> &nbsp </td> <td> </td> </tr> <tr> <td style=""padding-right: 50px;""> {Scheme} </td> <td style=""padding-right: 20px;vertical-align:top;""> &nbsp </td> <td> </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align:top;""> Service required: </td> <td> {ServiceRequired} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Estimate (&pound;): </td> <td> {Estimate} </td> </tr> <tr> <td style=""padding-right: 20px;""> Estimate Ref: </td> <td> {EstimateRef} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style=""padding-right: 20px;""> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style=""padding-right: 20px;""> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
        getContractorEmailTemplateVoidCyclic = assignToContractorEmailTemplateVoidCyclic
    End Function	

    Function PurchaseNumberVoid(strWord)
        PurchaseNumberVoid = "PO " & characterPad(strWord,"0", "l", 7)
    End Function




    '-----------------------------VOID WORKS ------------

    Function voidWorksEmailToContractor(ORDERID)
            
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName   " &_
		                " ,FCW.PurchaseOrderId as OrderId   " &_
                        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId " &_  
		                " ,FCW.JOURNALID as JSN   " &_
		                " ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy   " &_
		                " ,ISNULL( C.WORKDD,'N/A') AS DDial   " &_
		                " ,FCW.PDRContractorId as PDRContractorId    " &_
                        " ,E_CONTACT.WORKEMAIL as WorkEmail    " &_
                        " ,FP.userID As userId  " &_
                        " ,ISNULL(FCW.EstimateRef , 'N/A')as EstimateRef ,ISNULL(FCW.Estimate , 'N/A')as Estimate " &_
		                " FROM	PDR_CONTRACTOR_WORK AS FCW  " &_
		                "     INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID  " &_
		                "     INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID   " &_
                        "     INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseORDERID  " &_
                        "        WHERE	FCW.PurchaseOrderId =  " & ORDERID
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    ContactName = rs("ContactName")
                    OrderId = rs("OrderId")
                    JSG = rs("JSN")
                    DDial = rs("DDial")
                    OrderedBy = rs("OrderedBy")
                    PDRContractorId = rs("PDRContractorId")
                    userId = rs("userId")
                    Recipient = rs("WorkEmail")
                    EstimateRef = rs("EstimateRef")
                    Estimate = rs("Estimate")
                end if
                Call CloseRs(rs)
                SQL = " select InspectionJournalId from V_RequiredWorks " &_
                        "where WorksJournalId = " & JSG
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    InspectionJournal = rs("InspectionJournalId")
                else
                    InspectionJournal = ""
                end if
                ' GETTING PROPERTY DETAILS
                SQL = "SELECT " &_
	              "  PROPERTY.PROPERTYID, " &_
	              "  HOUSENUMBER, " &_
	              "  FLATNUMBER, " &_
	              "  PROPERTY.ADDRESS1, " &_
	              "  PROPERTY.ADDRESS2, " &_
	              "  PROPERTY.ADDRESS3, " &_
	              "  ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY, " &_
	              "  ISNULL(PROPERTY.COUNTY,'N/A') as COUNTY, " &_
	              "  ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE, " &_
	              "  ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '') " &_
	              "  + ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress] " &_
	              "  ,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block " &_
	              "  ,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme " &_
	              "  ,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms " &_
                  "  FROM	PDR_JOURNAL " &_
		          "      INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		          "      LEFT JOIN P__PROPERTY PROPERTY ON PDR_MSAT.PropertyId = PROPERTY.PropertyId " &_
		          "      LEFT JOIN P_BLOCK ON PROPERTY.BlockId = P_BLOCK.BLOCKID " &_
		          "      LEFT JOIN P_SCHEME ON P_BLOCK.SchemeId = P_SCHEME.SCHEMEID " &_
		          "      LEFT JOIN " &_
		          "      (SELECT  " &_
			      "          PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID, " &_
			      "          ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS " &_
			      "          FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES  " &_
			      "          LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId " &_
			      "          LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID " &_
			      "          LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID " &_
			      "          WHERE  " &_
			      "              " &_
			      "           PARAMETER.ParameterName = 'Quantity'  " &_
			      "           AND  ITEM.ItemName = 'Bedrooms' " &_
		          "      ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID " &_
                  "   WHERE JOURNALID = " & JSG
            Call OpenRs(rs, SQL)
            if not rs.eof then
                FullAddress = rs("FullStreetAddress")
                TownCity = rs("TOWNCITY")
                Postcode = rs("POSTCODE")
                COUNTY = rs("COUNTY")
                Block = rs("Block")
                Scheme = rs("Scheme")
                Bedrooms = rs("Bedrooms")
            else
                FullAddress = "N/A"
                TownCity = "N/A"
                Postcode = "N/A"
                COUNTY = "N/A"
                Block = "N/A"
                Scheme = "N/A"
            end if
             Call CloseRs(rs)

                'GETTING CUSTOMER ID
                SQL =   "SELECT C__CUSTOMER.CustomerId AS CUSTOMERID FROM	PDR_JOURNAL " &_
		                "    INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		                "    INNER JOIN P__PROPERTY ON  PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  " &_
		                "    INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  AND C_TENANCY.ENDDATE IS NULL " &_
		                "    INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID  " &_
						"		AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID) " &_
						"													FROM	C_CUSTOMERTENANCY  " &_
						"													WHERE	TENANCYID=C_TENANCY.TENANCYID  " &_
						"													) " &_
		                "        INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID " &_
                        "    WHERE	PDR_JOURNAL.JOURNALID = " & JSG
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    CUSTOMERID = rs("CUSTOMERID")
                else
                     CUSTOMERID = "N/A"
                end if
                Call CloseRs(rs)
                'GETTING CUSTOMER DETAILS
                if CUSTOMERID = "N/A" then 
                    TenantName = "N/A"
                    Telephone = "N/A"
                    TelephoneWork = "N/A"
                    Mobile = "N/A"
                    Email = "N/A"
                else
                    SQL =   " select ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName  " &_
		                    " ,ISNULL(A.TEL, '') as Telephone  " &_
		                    " ,ISNULL(A.TELWORK, '') as TelephoneWork " &_ 
		                    " ,ISNULL(A.MOBILE, '') as Mobile   " &_
                            " ,ISNULL(A.Email, 'enterprisedev@broadlandgroup.org') as Email    " &_
                            " ,ISNULL(A.COUNTY, 'N/A') as COUNTY   " &_ 
		                    " from C__CUSTOMER AS CC   " &_
		                    " INNER JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1  " &_
		                    " where CC.CustomerId='" & CUSTOMERID & "'" 
                    Call OpenRs(rs, SQL)
                    TenantName = rs("TenantName")
                    Telephone = rs("Telephone")
                    TelephoneWork = rs("TelephoneWork")
                    Mobile = rs("Mobile")
                    Email = rs("Email")
                
                    Call CloseRs(rs)
                end if
                '------------------------------------------------
                ' GET WORKS REQUIRED INFO
                '------------------------------------------------

                SQL = "	SELECT	DISTINCT	ServiceRequired   " &_
	            " FROM	PDR_CONTRACTOR_WORK_DETAIL    "  &_
	            " WHERE	 PDRContractorId  = " & PDRContractorId 

                Call OpenRs(rs, SQL)
    
                WorkRequired = ""
                while not rs.EOF
                    WorkRequired = WorkRequired & rs("ServiceRequired") & " <br />"
                    rs.movenext()
                wend

                Call CloseRs(rs)  

                '------------------------------------------------
                ' GET WORKS COST INFO
                '------------------------------------------------

                SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			            " ,SUM(Vat) as VAT "  &_
			            " ,SUM(Gross) as Gross "  &_
	                    " FROM	PDR_CONTRACTOR_WORK_DETAIL "  &_
	                    " WHERE	 PDRContractorId  = " & PDRContractorId

                Call OpenRs(rs, SQL)
                    NetCost = rs("NetCost")
                    VAT = rs("VAT")
                    Gross = rs("Gross")
                Call CloseRs(rs)  

    
                '------------------------------------------------
                ' GET RISK INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     RiskInfo = "N/A"
                else
                SQL = " SELECT	DISTINCT	CATDESC, SUBCATDESC "   &_
	                    " FROM	RISK_CATS_SUBCATS("& CustomerId & ") "

                Call OpenRs(rs, SQL)
                RiskInfo = ""
                while not rs.EOF
                    RiskInfo = RiskInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br />"
                    rs.movenext()
                wend
                Call CloseRs(rs) 
                end if 
    	        if RiskInfo = "" then 
                    RiskInfo = "N/A"
                end if

                '------------------------------------------------
                ' GET VULNARABILITY INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     VulnarabilityInfo = "N/A"
                else
                SQL =    " SELECT DISTINCT CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC "  &_
	                        " FROM	C_CUSTOMER_VULNERABILITY CV "  &_
		                    " INNER JOIN C_JOURNAL J ON CV.JOURNALID = J.JOURNALID "  &_
		                    " INNER JOIN C_VULNERABILITY CCV ON CCV.JOURNALID = J.JOURNALID "  &_
			                " INNER JOIN C_VULNERABILITY_CATEGORY CAT ON CAT.CATEGORYID = CV.CATEGORYID  "  &_ 
			                " INNER JOIN C_VULNERABILITY_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CV.SUBCATEGORYID and cv.categoryid=SUBCAT.categoryid  "  &_
                            " WHERE	 CV.CUSTOMERID ="& CustomerId &" "  &_
			                    "AND ITEMNATUREID = 61 "  &_
			                    "AND CCV.ITEMSTATUSID <> 14 "  &_
			                    "AND CV.VULNERABILITYHISTORYID = (	SELECT	MAX(VULNERABILITYHISTORYID) "  &_
												            "FROM	C_VULNERABILITY IN_CV "  &_
												            "WHERE	IN_CV.JOURNALID = J.JOURNALID) "   


                Call OpenRs(rs, SQL)
                VulnarabilityInfo = ""
                while not rs.EOF
                    VulnarabilityInfo = VulnarabilityInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br /> "
                    rs.movenext()
                wend
                if VulnarabilityInfo = "" then
                    VulnarabilityInfo = "N/A"
                end if
                Call CloseRs(rs)  
                end if


               SQL = " SELECT  " &_
	                 "       CONTRACTOR_WORK.PurchaseOrderId AS ORDERID, " &_
	                 "       PURCHASE_ITEM.NETCOST AS NETCOST, " &_
	                 "       PURCHASE_ITEM.VAT AS VAT, " &_
	                 "       CONVERT(VARCHAR(15), MSAT.ReletDate,103) AS RELETDATE, " &_
	                 "       CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,ISNULL(JOURNAL.JOURNALID,-1)),4)) AS OurRef, " &_
	                 "       PURCHASE_ITEM.ITEMDESC AS ItemNotes " &_
                     "   FROM PDR_CONTRACTOR_WORK CONTRACTOR_WORK  " &_
                     "   INNER JOIN F_PURCHASEITEM PURCHASE_ITEM ON CONTRACTOR_WORK.PurchaseOrderId = PURCHASE_ITEM.ORDERID " &_
                     "   INNER JOIN PDR_JOURNAL JOURNAL ON CONTRACTOR_WORK.JournalId = JOURNAL.JOURNALID " &_
                     "   INNER JOIN PDR_MSAT MSAT ON JOURNAL.MSATID = MSAT.MSATId " &_
                     "   WHERE CONTRACTOR_WORK.JournalId = " & JSG
                Call OpenRs(rs, SQL)

                if not rs.eof then 
                    RELETDATE = rs("RELETDATE")
                    OurRef = rs("OurRef")
                    ItemNotes = rs("ItemNotes")
                    Odi =  rs("ORDERID")
                else
                    RELETDATE = "N/A"
                    OurRef = "N/A"
                    ItemNotes = "N/A"
                    Odi =  "N/A"
                end if

                SQL = " SELECT " &_
	                  " ISNULL(EMPLOYEE.FIRSTNAME,'') + ' ' + ISNULL(EMPLOYEE.LASTNAME,'') AS SUPERVISOR " &_
                      " FROM PDR_APPOINTMENTS APPOINTMENTS " &_
                      " INNER JOIN 	E__EMPLOYEE EMPLOYEE ON APPOINTMENTS.ASSIGNEDTO = EMPLOYEE.EMPLOYEEID " &_
                      " WHERE JOURNALID = " & InspectionJournal
                Call OpenRs(rs, SQL)
                if not rs.eof then 
                    SUPERVISOR = rs("SUPERVISOR")
                else
                    SUPERVISOR = "N/A"
                end if

                SQL = "select (select CAST(CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,ISNULL(RequiredWorksId,-1)),4)) AS VARCHAR(10) )+', '  [text()] " &_
                      "  FROM V_RequiredWorks  " &_
                      "  WHERE InspectionJournalId = " & InspectionJournal & "AND RoomId IS NULL " &_
                      "  FOR XML PATH(''), TYPE) as Required "
                Call OpenRs(rs, SQL)
                if not rs.eof then 
                    Required = rs("Required")
                else
                    Required = "N/A"
                end if
                
                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Dim emailbody
                emailbody =  getContractorEmailTemplateVoidWorks()
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{JSV}",Required)
                emailbody = Replace(emailbody,"{PONumber}","PO " & Odi)
                emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
                emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
                emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
                emailbody = Replace(emailbody,"{Address}",FullAddress)
                emailbody = Replace(emailbody,"{TownCity}",TownCity)
                emailbody = Replace(emailbody,"{PostCode}",Postcode)
                emailbody = Replace(emailbody,"{Block}",Block)
                emailbody = Replace(emailbody,"{Scheme}",Scheme)
                emailbody = Replace(emailbody,"{Bedrooms}",Bedrooms)
                emailbody = Replace(emailbody,"{Telephone}",Telephone)
                emailbody = Replace(emailbody,"{ReLetDate}",RELETDATE)
                emailbody = Replace(emailbody,"{Supervisor}",SUPERVISOR)
                emailbody = Replace(emailbody,"{WorkRequired}",ItemNotes)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
                sendEmailVoidWorks emailbody,Recipient

        '------------------------------------------------------------

End Function

 Function sendEmailVoidWorks(body,recipient)
 
  if IsNull(recipient) then
        session("noRecipient") = "true"
        Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
		Flds.Update

        emailbody = body
        emailSubject = "Void Works has been Assigned"
        Set iMsg.Configuration = iConf
        iMsg.To = recipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send	

        end if
    End Function


    Function getContractorEmailTemplateVoidWorks()
        Dim assignToContractorEmailTemplateVoidWorks
        assignToContractorEmailTemplateVoidWorks = "<table> <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2"" > Please find below details of works required: </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td>PO Number:</td> <td>{PONumber}</td> </tr> <tr> <td> JSV:</td> <td>{JSV}</td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Address: </td> <td> <table> <tr> <td style=""padding-right: 50px;""> Scheme:</td> <td style=""padding-right: 20px;""> {Scheme}</td> <td> &nbsp;</td> </tr> <tr> <td style=""padding-right: 50px;""> Block:</td> <td style=""padding-right: 20px;""> {Block}</td> <td> &nbsp;</td> </tr> <tr> <td style=""padding-right: 50px;""> Address:</td> <td style=""padding-right: 20px; vertical-align:top;""> {Address} </td> <td> &nbsp;</td> </tr> <tr> <td style=""padding-right: 50px;""> Town/City:</td> <td style=""padding-right: 20px;vertical-align:top;""> {TownCity} </td> <td> &nbsp;</td> </tr> <tr> <td style=""padding-right: 50px;""> PostCode:</td> <td style=""padding-right: 20px;vertical-align:top;""> {PostCode} </td> <td> </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align:top;""> Bedrooms:</td> <td> {Bedrooms}</td> </tr> <tr> <td> Re-Let Date:</td> <td>{ReLetDate}</td> </tr> <tr> <td> Supervisor:</td> <td>{Supervisor}</td> </tr> <tr> <td> Telephone:</td> <td>{Telephone}</td> </tr> <tr> <td> &nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td> Work Required:</td> <td>{WorkRequired}</td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td> &nbsp;</td> </tr> <tr> <td> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style=""padding-right: 20px;""> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style=""padding-right: 20px;""> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
        getContractorEmailTemplateVoidWorks = assignToContractorEmailTemplateVoidWorks
    End Function




    '------------------------------------ PAINT PACKS

    Function voidPaintEmailToContractor(ORDERID)
            
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName   " &_
		                " ,FCW.PurchaseOrderId as OrderId   " &_
                        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId " &_  
		                " ,FCW.PaintPackId as PaintPackId   " &_
		                " ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy   " &_
		                " ,ISNULL( C.WORKDD,'N/A') AS DDial   " &_
		                " ,FCW.PDRContractorId as PDRContractorId    " &_
                        " ,E_CONTACT.WORKEMAIL as WorkEmail    " &_
                        " ,FP.userID As userId  " &_
                        " ,ISNULL(FCW.EstimateRef , 'N/A')as EstimateRef ,ISNULL(FCW.Estimate , 'N/A')as Estimate " &_
		                " FROM	PDR_CONTRACTOR_WORK AS FCW  " &_
		                "     INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID  " &_
		                "     INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID   " &_
                        "     INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseORDERID  " &_
                        "        WHERE	FCW.PurchaseOrderId =  " & ORDERID
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    ContactName = rs("ContactName")
                    OrderId = rs("OrderId")
                    PaintPackId = rs("PaintPackId")
                    DDial = rs("DDial")
                    OrderedBy = rs("OrderedBy")
                    PDRContractorId = rs("PDRContractorId")
                    userId = rs("userId")
                    Recipient = rs("WorkEmail")
                    EstimateRef = rs("EstimateRef")
                    Estimate = rs("Estimate")
                end if
                Call CloseRs(rs)

                 SQL =   " Select InspectionJournalId from V_PaintPack where PaintPackId= " & PaintPackId
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    JournalId = rs("InspectionJournalId")
                end if
                
                ' GETTING PROPERTY DETAILS
                SQL = "SELECT " &_
	              "  PROPERTY.PROPERTYID, " &_
	              "  HOUSENUMBER, " &_
	              "  FLATNUMBER, " &_
	              "  PROPERTY.ADDRESS1, " &_
	              "  PROPERTY.ADDRESS2, " &_
	              "  PROPERTY.ADDRESS3, " &_
	              "  ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY, " &_
	              "  ISNULL(PROPERTY.COUNTY,'N/A') as COUNTY, " &_
	              "  ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE, " &_
	              "  ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '') " &_
	              "  + ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress] " &_
	              "  ,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block " &_
	              "  ,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme " &_
	              "  ,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms " &_
                  "  FROM	PDR_JOURNAL " &_
		          "      INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		          "      LEFT JOIN P__PROPERTY PROPERTY ON PDR_MSAT.PropertyId = PROPERTY.PropertyId " &_
		          "      LEFT JOIN P_BLOCK ON PROPERTY.BlockId = P_BLOCK.BLOCKID " &_
		          "      LEFT JOIN P_SCHEME ON P_BLOCK.SchemeId = P_SCHEME.SCHEMEID " &_
		          "      LEFT JOIN " &_
		          "      (SELECT  " &_
			      "          PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID, " &_
			      "          ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS " &_
			      "          FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES  " &_
			      "          LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId " &_
			      "          LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID " &_
			      "          LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID " &_
			      "          WHERE  " &_
			      "              " &_
			      "           PARAMETER.ParameterName = 'Quantity'  " &_
			      "           AND  ITEM.ItemName = 'Bedrooms' " &_
		          "      ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID " &_
                  "   WHERE JOURNALID = " & JournalId
            Call OpenRs(rs, SQL)
            if not rs.eof then
                FullAddress = rs("FullStreetAddress")
                TownCity = rs("TOWNCITY")
                Postcode = rs("POSTCODE")
                COUNTY = rs("COUNTY")
                Block = rs("Block")
                Scheme = rs("Scheme")
            else
                FullAddress = "N/A"
                TownCity = "N/A"
                Postcode = "N/A"
                COUNTY = "N/A"
                Block = "N/A"
                Scheme = "N/A"
            end if
             Call CloseRs(rs)

                'GETTING CUSTOMER ID
                SQL =   "SELECT C__CUSTOMER.CustomerId AS CUSTOMERID FROM	PDR_JOURNAL " &_
		                "    INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
		                "    INNER JOIN P__PROPERTY ON  PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID  " &_
		                "    INNER JOIN C_TENANCY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID  AND C_TENANCY.ENDDATE IS NULL " &_
		                "    INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID  " &_
						"		AND C_CUSTOMERTENANCY.CUSTOMERTENANCYID = (	SELECT	MIN(CUSTOMERTENANCYID) " &_
						"													FROM	C_CUSTOMERTENANCY  " &_
						"													WHERE	TENANCYID=C_TENANCY.TENANCYID  " &_
						"													) " &_
		                "        INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID " &_
                        "    WHERE	PDR_JOURNAL.JOURNALID = " & JournalId
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    CUSTOMERID = rs("CUSTOMERID")
                else
                     CUSTOMERID = "N/A"
                end if
                Call CloseRs(rs)
                'GETTING CUSTOMER DETAILS
                if CUSTOMERID = "N/A" then 
                    TenantName = "N/A"
                    Telephone = "N/A"
                    TelephoneWork = "N/A"
                    Mobile = "N/A"
                    Email = "N/A"
                else
                    SQL =   " select ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName  " &_
		                    " ,ISNULL(A.TEL, '') as Telephone  " &_
		                    " ,ISNULL(A.TELWORK, '') as TelephoneWork " &_ 
		                    " ,ISNULL(A.MOBILE, '') as Mobile   " &_
                            " ,ISNULL(A.Email, 'enterprisedev@broadlandgroup.org') as Email    " &_
                            " ,ISNULL(A.COUNTY, 'N/A') as COUNTY   " &_ 
		                    " from C__CUSTOMER AS CC   " &_
		                    " INNER JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1  " &_
		                    " where CC.CustomerId='" & CUSTOMERID & "'" 
                    Call OpenRs(rs, SQL)
                    TenantName = rs("TenantName")
                    Telephone = rs("Telephone")
                    TelephoneWork = rs("TelephoneWork")
                    Mobile = rs("Mobile")
                    Email = rs("Email")
                
                    Call CloseRs(rs)
                end if
                '------------------------------------------------
                ' GET WORKS REQUIRED INFO
                '------------------------------------------------

                SQL = "	SELECT	DISTINCT	ServiceRequired   " &_
	            " FROM	PDR_CONTRACTOR_WORK_DETAIL    "  &_
	            " WHERE	 PDRContractorId  = " & PDRContractorId 

                Call OpenRs(rs, SQL)
    
                WorkRequired = ""
                while not rs.EOF
                    WorkRequired = WorkRequired & rs("ServiceRequired") & " <br />"
                    rs.movenext()
                wend

                Call CloseRs(rs)  

                '------------------------------------------------
                ' GET WORKS COST INFO
                '------------------------------------------------

                SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			            " ,SUM(Vat) as VAT "  &_
			            " ,SUM(Gross) as Gross "  &_
	                    " FROM	PDR_CONTRACTOR_WORK_DETAIL "  &_
	                    " WHERE	 PDRContractorId  = " & PDRContractorId

                Call OpenRs(rs, SQL)
                    NetCost = rs("NetCost")
                    VAT = rs("VAT")
                    Gross = rs("Gross")
                Call CloseRs(rs)  

    
                '------------------------------------------------
                ' GET RISK INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     RiskInfo = "N/A"
                else
                SQL = " SELECT	DISTINCT	CATDESC, SUBCATDESC "   &_
	                    " FROM	RISK_CATS_SUBCATS("& CustomerId & ") "

                Call OpenRs(rs, SQL)
                RiskInfo = ""
                while not rs.EOF
                    RiskInfo = RiskInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br />"
                    rs.movenext()
                wend
                if RiskInfo = "" then
                    RiskInfo = "N/A"
                end if
                Call CloseRs(rs) 
                end if 
    	

                '------------------------------------------------
                ' GET VULNARABILITY INFO
                '------------------------------------------------
                if CUSTOMERID = "N/A" then 
                     VulnarabilityInfo = "N/A"
                else
                SQL =    " SELECT DISTINCT CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC "  &_
	                        " FROM	C_CUSTOMER_VULNERABILITY CV "  &_
		                    " INNER JOIN C_JOURNAL J ON CV.JOURNALID = J.JOURNALID "  &_
		                    " INNER JOIN C_VULNERABILITY CCV ON CCV.JOURNALID = J.JOURNALID "  &_
			                " INNER JOIN C_VULNERABILITY_CATEGORY CAT ON CAT.CATEGORYID = CV.CATEGORYID  "  &_ 
			                " INNER JOIN C_VULNERABILITY_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CV.SUBCATEGORYID and cv.categoryid=SUBCAT.categoryid  "  &_
                            " WHERE	 CV.CUSTOMERID ="& CustomerId &" "  &_
			                    "AND ITEMNATUREID = 61 "  &_
			                    "AND CCV.ITEMSTATUSID <> 14 "  &_
			                    "AND CV.VULNERABILITYHISTORYID = (	SELECT	MAX(VULNERABILITYHISTORYID) "  &_
												            "FROM	C_VULNERABILITY IN_CV "  &_
												            "WHERE	IN_CV.JOURNALID = J.JOURNALID) "   


                Call OpenRs(rs, SQL)
                VulnarabilityInfo = ""
                while not rs.EOF
                    VulnarabilityInfo = VulnarabilityInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br /> "
                    rs.movenext()
                wend
                if VulnarabilityInfo = "" then
                    VulnarabilityInfo = "N/A"
                end if
                Call CloseRs(rs)  
                end if


                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Dim emailbody
                emailbody =  getContractorEmailTemplatePaintPacks()
                emailbody = Replace(emailbody,"{Servicing}","Paint Packs")
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{Ref}",PaintPackId)
                emailbody = Replace(emailbody,"{PONumber}","PO " & OrderId)
                emailbody = Replace(emailbody,"{ServiceRequired}",WorkRequired)
                emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
                emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
                emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
                emailbody = Replace(emailbody,"{Address}",FullAddress)
                emailbody = Replace(emailbody,"{TenantName}",TenantName)
                emailbody = Replace(emailbody,"{TownCity}",TownCity)
                emailbody = Replace(emailbody,"{County}",COUNTY)
                emailbody = Replace(emailbody,"{PostCode}",Postcode)
                emailbody = Replace(emailbody,"{Block}",Block)
                emailbody = Replace(emailbody,"{Scheme}",Scheme)
                emailbody = Replace(emailbody,"{Telephone}",Telephone)
                emailbody = Replace(emailbody,"{RiskDetail}",RiskInfo)
                emailbody = Replace(emailbody,"{VulnerabilityDetail}",VulnarabilityInfo)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
                sendEmailPaintPacks emailbody,Recipient

        '------------------------------------------------------------

End Function




    '--------------------------  TEST

    Function sendEmailPaintPacks(body,recipient)
	
     if IsNull(recipient) then
	    session("noRecipient") = "true"
        Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
         Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

        emailbody = body
        emailSubject = "Paint Pack Work Required has been Assigned"
        Set iMsg.Configuration = iConf
        iMsg.To = recipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send
        
        end if
        	
    End Function


    Function getContractorEmailTemplatePaintPacks()
        Dim assignToContractorEmailTemplatePaintPacks
        assignToContractorEmailTemplatePaintPacks = "<table> <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2"" > Please find below details of {Servicing} works required: </td> </tr> <tr> <td> &nbsp; </td> </tr><tr> <td>PO Number:</td> <td>{PONumber}</td> </tr> <tr> <td style=""padding-right: 20px;""> Ref number: </td> <td > {Ref} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Address: </td> <td> <table> <tr> <td style=""padding-right: 50px;""> {Address} </td> <td style=""padding-right: 20px;""> Tenant: </td> <td> {TenantName} </td> </tr> <tr> <td style=""padding-right: 50px;""> {TownCity} </td> <td style=""padding-right: 20px;""> Telephone: </td> <td> {Telephone} </td> </tr> <tr> <td style=""padding-right: 50px;""> {County} </td> <td style=""padding-right: 20px; vertical-align:top;""> Risk: </td> <td> {RiskDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> {PostCode} </td> <td style=""padding-right: 20px;vertical-align:top;""> Vulnerability: </td> <td> {VulnerabilityDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> {Block} </td> <td style=""padding-right: 20px;vertical-align:top;""> &nbsp </td> <td> </td> </tr> <tr> <td style=""padding-right: 50px;""> {Scheme} </td> <td style=""padding-right: 20px;vertical-align:top;""> &nbsp </td> <td> </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align:top;""> Works required: </td> <td> {ServiceRequired} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style=""padding-right: 20px;""> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style=""padding-right: 20px;""> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
        getContractorEmailTemplatePaintPacks = assignToContractorEmailTemplatePaintPacks
    End Function	

    
    '----------------------------------------------------------------------------------------------


Function raiseAPOEmailToContractor(ORDERID)
            
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName   " &_
		                " ,FCW.PurchaseOrderId as OrderId   " &_
                        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId " &_  
		                " ,FCW.JOURNALID as JSN   " &_
		                " ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy   " &_
		                " ,ISNULL( C.WORKDD,'N/A') AS DDial   " &_
		                " ,FCW.PDRContractorId as PDRContractorId    " &_
                        " ,E_CONTACT.WORKEMAIL as WorkEmail    " &_
                        " ,FP.userID As userId  " &_
                        " ,ISNULL(FCW.EstimateRef , 'N/A')as EstimateRef ,ISNULL(FCW.Estimate , 'N/A')as Estimate " &_
		                " FROM	PDR_CONTRACTOR_WORK AS FCW  " &_
		                "     INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID  " &_
		                "     INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID   " &_
                        "     INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "     INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseORDERID  " &_
                        "        WHERE	FCW.PurchaseOrderId =  " & ORDERID
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    ContactName = rs("ContactName")
                    OrderId = rs("OrderId")
                    JSG = rs("JSN")
                    DDial = rs("DDial")
                    OrderedBy = rs("OrderedBy")
                    PDRContractorId = rs("PDRContractorId")
                    userId = rs("userId")
                    Recipient = rs("WorkEmail")
                    EstimateRef = rs("EstimateRef")
                    Estimate = rs("Estimate")
                end if
                Call CloseRs(rs)
                
                ' GETTING ADDRESS DETAILS
                SQL = "SELECT  TOP(1)  " &_
	                   " PROPERTY.PROPERTYID, " &_  
	                   " HOUSENUMBER,   " &_
	                   " FLATNUMBER,   " &_
	                   " PROPERTY.ADDRESS1, " &_  
	                   " PROPERTY.ADDRESS2,   " &_
	                   " PROPERTY.ADDRESS3,   " &_
	                   " ISNULL(PROPERTY.TOWNCITY,'N/A') AS TOWNCITY,  " &_  
	                   " ISNULL(PROPERTY.COUNTY,'N/A') as COUNTY,   " &_
	                   " ISNULL(PROPERTY.POSTCODE,'N/A') AS POSTCODE,   " &_
	                   " ISNULL(NULLIF(ISNULL('Flat No:' + PROPERTY.FLATNUMBER + ', ', '') + ISNULL(PROPERTY.HOUSENUMBER, '') + ISNULL(' ' + PROPERTY.ADDRESS1, '')   " &_
	                   " + ISNULL(' ' + PROPERTY.ADDRESS2, '') + ISNULL(' ' + PROPERTY.ADDRESS3, ''), ''), 'N/A') AS [FullStreetAddress]   " &_
	                   " ,ISNULL(P_BLOCK.BLOCKNAME,'N/A') AS Block   " &_
	                   " ,ISNULL(P_SCHEME.SCHEMENAME,'N/A') as Scheme   " &_
	                   " ,ISNULL(BEDROOMS.BEDROOMS,'N/A') AS Bedrooms   " &_
                       " FROM	PDR_JOURNAL   " &_
		               "     INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId " &_
                       " LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID   " &_
                       "   LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId =  P_BLOCK.BLOCKID " &_
                       "   LEFT JOIN P__PROPERTY PROPERTY ON  " &_
                       "   Case WHEN PDR_MSAT.BlockId < 1 THEN PROPERTY.SchemeId  " &_
                       "   ELSE PROPERTY.BlockId  END " &_
                       "   =   " &_
                       "   Case WHEN PDR_MSAT.BlockId < 1 THEN P_SCHEME.SchemeId  " &_
                       "   ELSE   PDR_MSAT.BlockId  END " &_
                       "   LEFT JOIN   " &_
                       "   (SELECT    " &_
                       "    PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID,   " &_
                       "    ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS " &_  
     
                       "     FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES    " &_
                       "    LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId   " &_
                       "    LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID   " &_
                       "    LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID   " &_
                       "    WHERE   " &_
                       "     PARAMETER.ParameterName = 'Quantity' " &_   
                       "     AND  ITEM.ItemName = 'Bedrooms'   " &_
                       "   ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID   " &_
                       "  WHERE JOURNALID = " & JSG
            Call OpenRs(rs, SQL)
            if not rs.eof then
                TownCity = rs("TOWNCITY")
                Postcode = rs("POSTCODE")
                Block = rs("Block")
                Scheme = rs("Scheme")
            else
                TownCity = "N/A"
                Postcode = "N/A"
                COUNTY = "N/A"
                Block = "N/A"
                Scheme = "N/A"
            end if
             Call CloseRs(rs)

                
                '------------------------------------------------
                ' GET WORKS REQUIRED INFO
                '------------------------------------------------

                SQL = "	SELECT	DISTINCT	ServiceRequired   " &_
	            " FROM	PDR_CONTRACTOR_WORK_DETAIL    "  &_
	            " WHERE	 PDRContractorId  = " & PDRContractorId 

                Call OpenRs(rs, SQL)
    
                WorkRequired = ""
                while not rs.EOF
                    WorkRequired = WorkRequired & rs("ServiceRequired") & " <br />"
                    rs.movenext()
                wend

                Call CloseRs(rs)  

                '------------------------------------------------
                ' GET WORKS COST INFO
                '------------------------------------------------

                SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			            " ,SUM(Vat) as VAT "  &_
			            " ,SUM(Gross) as Gross "  &_
	                    " FROM	PDR_CONTRACTOR_WORK_DETAIL "  &_
	                    " WHERE	 PDRContractorId  = " & PDRContractorId

                Call OpenRs(rs, SQL)
                    NetCost = rs("NetCost")
                    VAT = rs("VAT")
                    Gross = rs("Gross")
                Call CloseRs(rs)  

    

                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Dim emailbody
                emailbody =  getContractorEmailTemplateRaiseAPO()
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{PO}","PO " & OrderId)
                emailbody = Replace(emailbody,"{WorkRequired}",WorkRequired)
                emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
                emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
                emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
                emailbody = Replace(emailbody,"{TownCity}",TownCity)
                emailbody = Replace(emailbody,"{PostCode}",Postcode)
                emailbody = Replace(emailbody,"{Block}",Block)
                emailbody = Replace(emailbody,"{Scheme}",Scheme)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                'Response.Write(emailbody)
                'Response.End()
                sendRaiseAPO emailbody,Recipient
        '------------------------------------------------------------

End Function




    '--------------------------  TEST

    Function sendRaiseAPO(body,recipient)
	'Response.Write(recipient)
	'Response.End()
     if IsNull(recipient) then
        session("noRecipient") = "true"
        Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
         Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

        emailbody = body
        emailSubject = "Purchase Order for BHG Works"
        Set iMsg.Configuration = iConf
        iMsg.To = recipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send	

        end if

    End Function


    Function getContractorEmailTemplateRaiseAPO()
        Dim assignToContractorEmailTemplateRaiseAPO
        assignToContractorEmailTemplateRaiseAPO = "<table > <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2"" > Please find below details of works required: </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> PO number: </td> <td > {PO} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Address: </td> <td> <table> <tr> <td style=""padding-right: 50px;""> Scheme: </td> <td style=""padding-right: 50px;""> {Scheme} </td> </tr> <tr> <td style=""padding-right: 50px;""> Block: </td> <td style=""padding-right: 50px;""> {Block} </td> </tr> <tr> <td style=""padding-right: 50px;""> TownCity: </td> <td style=""padding-right: 50px;""> {TownCity} </td> </tr> <tr> <td style=""padding-right: 50px;""> PostCode: </td> <td style=""padding-right: 50px;""> {PostCode} </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align:top;""> Work required: </td> <td> {WorkRequired} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <table> <tr> <td style=""padding-right: 50px;""> Net Cost (&pound;): </td> <td style=""text-align: right;"" > {NetCost} </td> </tr> <tr> <td style=""padding-right: 50px;""> VAT (&pound;): </td> <td style=""text-align: right;""> {VAT} </td> </tr> <tr> <td style=""padding-right: 50px;""> <strong>Total (&pound;):</strong> </td> <td style=""text-align: right;""> <strong>{TOTAL}</strong> </td> </tr> </table> </tr> <tr> <td> <br /> </td> </tr> <tr> <td style=""padding-right: 20px;""> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td> <br /><br /> </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing Group </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: -1px;"" /> </td> <td style=""padding-left: 5mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich NR1 1HU<br /> Customer Services: 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
        getContractorEmailTemplateRaiseAPO = assignToContractorEmailTemplateRaiseAPO
    End Function	

    Function PurchaseNumberVoid(strWord)
        PurchaseNumberVoid = "PO " & characterPad(strWord,"0", "l", 7)
    End Function



%>