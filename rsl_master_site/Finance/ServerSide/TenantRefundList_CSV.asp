<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
POFilter = ""
If (Request("PROCESSINGDATE") <> "") Then
	ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
	RequiredDate = CDate(Request("PROCESSINGDATE"))
Else
	ProcessingDate = FormatDateTime(CDate(Date),2)
	RequiredDate = Date
End If

Call OpenDB()
'THIS STATEMENT WILL LOCK TENANT REFUND POSTING FOR A MOMENT....
SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 1 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_TENANTREFUNDS'"
Conn.Execute (SQL)

'NEXT NEED TO CHECK THAT THE DATA HAS NOT CHANGED SINCE IT WAS SENT OVER...
	SQL = "SELECT ISNULL(SUM(RJ.AMOUNT),0) AS TOTALCOST, COUNT(TR.JOURNALID) AS TOTALCOUNT " &_
			"FROM F_TENANTREFUNDS TR " &_
			"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
			"INNER JOIN C_TENANCY T ON T.TENANCYID = TR.TENANCYID " &_				
			"INNER JOIN C__CUSTOMER C ON TR.CUSTOMERID = C.CUSTOMERID " &_
			"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_									
			"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
			"	WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.PAYMENTTYPE = 19 AND CONVERT(DATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(RequiredDate,1) & "' "

Call OpenRs(rsCheck, SQL)
TotalSum = 0
TotalCount = 0
if (NOT rsCheck.EOF) then
	TotalSum = rsCheck("TOTALCOST")
	TotalCount = rsCheck("TOTALCOUNT")
end if
Call CloseRs(rsCheck)

'THIS pART CHECKS THE DATA AND REDIRECTS TO THE PREVIOUS PAGE IF IT IS NOT MATCHING
IF ( NOT (CDbl(TotalSum) = Cdbl(Request("TotalSum")) AND CInt(TotalCount) = CInt(Request("TotalCount"))) ) then
	'THIS STATEMENT WILL UNLOCK TENANT REFUND POSTING FOR A MOMENT....
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_TENANTREFUNDS'"
	Conn.Execute (SQL)
	Response.Redirect "../TenantRefundList.asp?PODate=" & ProcessingDate & "&ERR97" & Replace(Date, "/", "") & "=1"
End If

ORIGINATORNUMBER = "658670"
orderBy = "NAME ASC"

SQLCODE = "SELECT T.TENANCYID, ISNULL(SUM(AMOUNT),0) AS PAYMENTAMOUNT, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER,  " &_
		"LTRIM(REPLACE(G.DESCRIPTION + ' ' + C.FIRSTNAME + ' ' + C.LASTNAME, '  ',  ' ')) AS NAME, " &_
		"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(TR.JOURNALID) AS THECOUNT " &_
		"FROM F_TENANTREFUNDS TR " &_
		"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
		"INNER JOIN C__CUSTOMER C ON TR.CUSTOMERID = C.CUSTOMERID " &_
		"INNER JOIN C_TENANCY T ON T.TENANCYID = TR.TENANCYID " &_				
		"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_
		"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
		"	WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.PAYMENTTYPE = 19 AND CONVERT(DATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
		" 	GROUP BY T.TENANCYID, LTRIM(REPLACE(G.DESCRIPTION + ' ' + C.FIRSTNAME + ' ' + C.LASTNAME, '  ',  ' ')), " &_
		"		ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER	 " &_
		"		ORDER BY " + Replace(orderBy, "'", "''") + ""
Set fso = CreateObject("Scripting.FileSystemObject")

'GP_curPath = Request.ServerVariables("PATH_INFO")
'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
'	GP_curPath = GP_curPath & "/"
'end if 

    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS) 

CurrentDate = FormatDateTime(Date,2)
CurrentDate = Replace(CurrentDate, "/", "")
CurrentDate = Replace(CurrentDate, " ", "")

RedirectProcessingDate = ProcessingDate
ProcessingDate = Replace(ProcessingDate, "/", "")
ProcessingDate = Replace(ProcessingDate, " ", "")

'FullPath = Trim(Server.mappath(GP_curPath)) & "\TenantRefundFiles\"
FullPath = doc_DEFAULTPATH & "\finance\TenantRefundFiles\"
FileName = "TR-C" & CurrentDate & "-" & "P" & ProcessingDate

If (fso.FileExists(FullPath & FileName & ".csv")) Then
	GP_FileExist = true
End If   
if GP_FileExist then
	Begin_Name_Num = 0
	while GP_FileExist    
		Begin_Name_Num = Begin_Name_Num + 1
		TempFileName = FileName & "_" & Begin_Name_Num
		GP_FileExist = fso.FileExists(FullPath & TempFileName & ".csv")
	wend  
	FileName = TempFileName
end if
Set MyFile= fso.CreateTextFile(FullPath & FileName & ".csv", True)

Call OpenRS(rsLoader, SQLCODE)

BadDataCount = 0
BadDataTotal = 0
BadPOCount = 0
GoodDataCount = 0
GoodDataTotal = 0
GoodPOCount = 0
'MyFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"
while NOT rsLoader.EOF
	SORTCODE = rsLoader("SORTCODE")
	ACCOUNTNUMBER = rsLoader("ACCOUNTNUMBER")
	ACCOUNTNAME = rsLoader("ACCOUNTNAME")
	if (SORTCODE = "" OR ACCOUNTNUMBER = "" OR ACCOUNTNAME = "" OR isNull(ACCOUNTNAME) OR isNULL(SORTCODE) OR isNULL(ACCOUNTNUMBER)) then
		if (BadDataCount = 0) then
			Set MyBadFile= fso.CreateTextFile(FullPath & FileName & "-BAD.csv", True)
			'MyBadFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"						
		end if
		BadDataCount = BadDataCount + 1
		BadPOCount = BadPOCount + rsLoader("THECOUNT")		
		MyBadFile.WriteLine TenancyReference(rsLoader("TENANCYID")) & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
		BadDataTotal = BadDataTotal + rsLoader("PAYMENTAMOUNT")		
	else
		GoodDataCount = GoodDataCount + 1
		GoodPOCount = GoodPOCount + rsLoader("THECOUNT")				
		MyFile.WriteLine TenancyReference(rsLoader("TENANCYID")) & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
		GoodDataTotal = GoodDataTotal + rsLoader("PAYMENTAMOUNT")
		
	end if
	rsLoader.moveNext
wend
CloseRs(rsLoader)
MyFile.Close
if (BadDataCount > 0) then
	MyBadFile.Close
end if

'FINALLY ADD A ROW INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
SQL = ("SET NOCOUNT ON;INSERT INTO F_TENANTBACSFILES (FILEDATE, FILENAME, FILETYPE, FILECOUNT, TRCOUNT, FILEAMOUNT, PROCESSDATE, ISFILE) VALUES (" &_
			"'" & FormatDateTime(RequiredDate,1) & "', " &_
			"'" & FileName & ".csv" & "', " &_
			"" & 1 & ", " &_
			"" & GoodDataCount & ", " &_
			"" & GoodPOCount & ", " &_				
			"" & GoodDataTotal & ", " &_
			"'" & FormatDateTime(Date,1) & "', 1);SELECT SCOPE_IDENTITY() AS FILEID ")
Call OpenRs(rsDATA, SQL)
FILEID = rsDATA("FILEID")
Call CloseRs(rsDATA)

'The ORDER OF THE FOLLOWING STATEMENTS IS VERY IMPORTANT, AS THE DATAID IS USED TO GET THE REFERENCE FOR THE NEXT TWO STATEMENTS.
'THIS STATEMENT WILL INSERT N INVOICE ENTRIES INTO F_TENANTBACS.
SQL = "INSERT INTO F_TENANTBACS (JOURNALID, FILEID, PROCESSEDBY, PROCESSDATE) " &_
		"SELECT TR.JOURNALID, " & FILEID & ", " & SESSION("USERID") & ", '" & FormatDateTime(Date,1) & "' " &_
		"FROM F_TENANTREFUNDS TR " &_
		"INNER JOIN F_RENTJOURNAL RJ ON TR.JOURNALID = RJ.JOURNALID " &_
		"INNER JOIN C_TENANCY T ON T.TENANCYID = TR.TENANCYID " &_				
		"INNER JOIN C__CUSTOMER C ON TR.CUSTOMERID = C.CUSTOMERID " &_
		"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_									
		"LEFT JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID " &_
		"	WHERE TR.STATUS = 0 AND TB.JOURNALID IS NULL AND RJ.PAYMENTTYPE = 19 AND CONVERT(DATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
				"AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
				"AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_
				"AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''"														
		
Conn.Execute SQL

SQL = "UPDATE F_TENANTREFUNDS SET STATUS = 1 " &_
		"FROM F_TENANTREFUNDS TR " &_
		"INNER JOIN F_TENANTBACS TB ON TB.JOURNALID = TR.JOURNALID AND TB.FILEID = " & FILEID & " "
Conn.Execute SQL

IF (BadDataCount > 0) THEN
	'also ADD A ROW FOR BAD DATA INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
	Conn.Execute ("INSERT INTO F_TENANTBACSFILES (FILEDATE, FILENAME, FILETYPE, FILECOUNT, TRCOUNT, FILEAMOUNT, PROCESSDATE, ISFILE) VALUES (" &_
				"'" & FormatDateTime(RequiredDate,1) & "', " &_
				"'" & FileName & "-BAD.csv" & "', " &_
				"" & 2 & ", " &_
				"" & BadDataCount & ", " &_
				"" & BadPOCount & ", " &_				
				"" & BadDataTotal & ", " &_
				"'" & FormatDateTime(Date,1) & "',1) ")																								
END IF

'THIS STATEMENT WILL UNLOCK TENANT REFUND POSTING ....
SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_TENANTREFUNDS'"
Conn.Execute (SQL)

CloseDB()
Response.Redirect "../TenantRefundCSV.asp?PROCESSINGDATE=" & RedirectProcessingDate
%>
