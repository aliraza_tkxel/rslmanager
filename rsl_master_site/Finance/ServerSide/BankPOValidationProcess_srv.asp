<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("sel_STATEMENT")
	
	OpenDB()

	strSQL = 	"SET NOCOUNT ON;" &_	
		"INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
		"VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
		"SELECT SCOPE_IDENTITY() AS BTID;"

	set rsSet = Conn.Execute(strSQL)
	bt_id = rsSet.fields("BTID").value
	rsSet.close()
	set rsSet = Nothing 
	
	sel_split = Split(insert_list ,",") ' split selected string
	rec_type = 2 'This means these items are from the PURCHASE PAYMENT THINGA MA BOB
	TimeStamp = Now()
	method = 1 'Manual Validation
	
	For each key in sel_split
		
		'SQL = "SELECT * FROM F_BANK_RECONCILIATION
		strSQL = "INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD) VALUES " &_
					"(" & bt_id & ", " & bank_statement & ", " & rec_type & ", " & key & ",'" & TimeStamp & "'," & Session("USERID") & "," & method & ")"
		Conn.Execute(strSQL)
		
	Next

	CloseDB()

	Response.Redirect "BankPOValidation_svr.asp?RESET=1&sel_STATEMENT=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_PFROM=" & Request("txt_PFROM") & "&txt_PTO=" & Request("txt_PTO") & "&txt_CHEQUE=" & Request("txt_CHEQUE") & "&sel_PAYMENTMETHOD=" & Request("sel_PAYMENTMETHOD") & "&sel_PTYPE=" & Request("sel_PTYPE")	
%>
