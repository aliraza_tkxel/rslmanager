<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim insert_list, slip_number, slip_id, slip_total
	Dim sel_split
	
	insert_list = Request.Form("idlist")
	slip_number = Request.Form("txt_SLIPNUMBER")
	slip_total = Request("sliptotal")

	response.write insert_list
	response.write slip_total
	
	OpenDB()
	
	insert_slip()
	update_cashpostings()
	
	CloseDB()
	
	// inset new row into slip table
	Function insert_slip()
	
			strSQL = 	"SET NOCOUNT ON;" &_	
				"INSERT INTO F_PAYMENTSLIP (USERID, SLIPNUMBER, SLIPTOTAL) " &_
				"VALUES (" & Session("USERID") & ", '" & slip_number & "', " & slip_total & " );" &_
				"SELECT SCOPE_IDENTITY() AS SLIPID;"
	
			//response.write strSQL		
			set rsSet = Conn.Execute(strSQL)
			slip_id = rsSet.fields("SLIPID").value
			rsSet.close()
			set rsSet = Nothing 
	
	End Function
	
	// update all chosen cash postings with new slip id
	Function update_cashpostings()
		
		sel_split = Split(insert_list ,",") ' split selected string
		For each key in sel_split
			
			strSQL = "UPDATE F_CASHPOSTING SET PAYMENTSLIPNUMBER = '" & slip_id & "' WHERE CASHPOSTINGID = " & Clng(key)
			Conn.Execute(strSQL)
			
		Next

	
	End Function
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.location.href = "../paymentslip.asp"
	
	}
</script>
<body onload="ReturnData()">
	


</body>
</html>