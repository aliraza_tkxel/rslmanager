<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<% Session.LCID = 2057 %>
<%
OpenDB()
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'"
Call OpenRs(rsDev, SQL)
if (NOt rsDev.eOF) then
	DevID = rsDev("DEFAULTVALUE")
else
	DevID = -1
end if
CloseRs(rsDev)
CloseDB()
%>
<html>
<head>
<link rel="StyleSheet" href="/js/Tree/dtree.css" type="text/css" />
<script type="text/javascript" src="/js/Tree/dtree.js"></script>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" bgcolor="beige" text="#000000" border=none STYLE="scrollbar-face-color: #133E71; scrollbar-track-color: #87B4C9; scrollbar-arrow-color: #FFFFFF; scrollbar-3dlight-color: #133E71; scrollbar-shadow-color: #133E71; scrollbar-highlight-color: #133E71; scrollbar-darkshadow-color: #133E71;">
<div class="dtree" style='height:100%'>

	<script type="text/javascript">
		<!--
		d = new dTree('d');
		d.config.useStatusText=true;
		d.add(0,-1,'RSL Finance Builder');
<%
dim rstHeads, rstExpenditures, rstBudgets, codei, budgeti, headi

' Use a datashaping connection string to be used later.
RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

strSQL = "SHAPE {SELECT COSTCENTREID, DESCRIPTION FROM F_COSTCENTRE WHERE ACTIVE = 1 AND COSTCENTREID <> " & DevID & " ORDER BY DESCRIPTION} " &_
		 "	APPEND((SHAPE {SELECT HEADID, DESCRIPTION, COSTCENTREID FROM F_HEAD ORDER BY DESCRIPTION} " &_
		 "		APPEND({SELECT EXPENDITUREID, DESCRIPTION, HEADID FROM F_EXPENDITURE ORDER BY DESCRIPTION} AS EXPENDITURES " &_
		 "		RELATE HEADID TO HEADID)) AS HEADS " &_
	     "	RELATE COSTCENTREID TO COSTCENTREID)"

' Open original recordset
Set rst = Server.CreateObject("ADODB.Recordset")
rst.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING

currenti = 5
headparent = 0
expenditureparent = 0

Do While Not rst.EOF

	Response.Write "d.add(" & currenti & ",0,'" & rst("DESCRIPTION") & "','CC.asp?CCID=" & rst("CostCentreID") & "&CC_A=L','','FB','img1.gif');"
	currenti = currenti + 1
	
    ' Set object to child recordset and iterate through
    Set rstHeads = rst("HEADS").Value
    if not rstHeads.EOF then
		headparent = currenti - 1
        Do While Not rstHeads.EOF
			Response.Write "d.add(" & currenti & "," & headparent & ",'" & rstHeads("DESCRIPTION") & "','HD.asp?HDID=" & rstHeads("HeadID") & "&HD_A=L','','FB','img2.gif');"
			currenti = currenti + 1			
			
			Set rstExpenditures = rstHeads("EXPENDITURES").Value
			if not rstExpenditures.EOF then
				expenditureparent = currenti - 1			
				Do While Not rstExpenditures.EOF
					Response.Write "d.add(" & currenti & "," & expenditureparent & ",'" & rstExpenditures("DESCRIPTION") & "','EX.asp?EXID=" & rstExpenditures("ExpenditureID") & "&EX_A=L','','FB','img4.gif');"
					currenti = currenti + 1
					rstExpenditures.MoveNext
				Loop
				Response.Write "d.add(" & currenti & "," & expenditureparent & ",'<font color=blue>New Expenditure</font>','EX.asp?HDID=" & rstHeads("headId") & "&EX_A=LFD','','FB');"
				currenti = currenti + 1				
			else
				expenditureparent = currenti - 1
				Response.Write "d.add(" & currenti & "," & expenditureparent & ",'<font color=blue>New Expenditure</font>','EX.asp?HDID=" & rstHeads("headId") & "&EX_A=LFD','','FB');"
				currenti = currenti + 1	
			end if
			
            rstHeads.MoveNext
        Loop
		Response.Write "d.add(" & currenti & "," & headparent & ",'<font color=blue>New Head</font>','HD.asp?CCID=" & rst("CostCentreID") & "&HD_A=LFD','','FB');"
		currenti = currenti + 1		
    else
		headparent = currenti - 1
		Response.Write "d.add(" & currenti & "," & headparent & ",'<font color=blue>New Head</font>','HD.asp?CCID=" & rst("CostCentreID") & "&HD_A=LFD','','FB');"
		currenti = currenti + 1	
	end if
    rst.MoveNext
Loop
Response.Write "d.add(" & currenti & ",0,'<font color=blue>New Cost Centre</font>','javascript:parent.NewItem(1)');"

Function AddChild (strSQL)
    dim blnChild, rst2
    Set rst2 = Server.CreateObject("ADODB.Recordset")
    rst2.open strSQL, strConnectionString
        Response.write(rst2("Name") & "<br>")
        blnChild = rst2("Child")
        rst2.close
        set rst2 = nothing

    if blnChild then
        AddChild(strSQL)
    end if
End Function
%>		

		document.write(d);
		//-->
	</script>

</div>
</body>
</html>
