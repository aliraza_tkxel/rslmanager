<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, customer_id, str_journal_table, str_color, item_type

	OpenDB()
	tenancy_id = Request.FORM("hid_TENANTNUMBER")
	
	build_journal()

	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT LTRIM(REPLACE(G.DESCRIPTION + ' ' + C.FIRSTNAME + ' ' + C.LASTNAME, '  ' , ' ')) AS FULLNAME, " &_
					"B.ACCOUNTNAME, B.SORTCODE, B.ACCOUNTNUMBER, CT.ENDDATE, B.BANKDETAILSID " &_
					"FROM F_BANKDETAILS B " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.CUSTOMERID = B.CUSTOMERID " &_
					"INNER JOIN C_TENANCY T ON T.TENANCYID = CT.TENANCYID " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_					
					"WHERE T.TENANCYID = " & tenancy_id & " AND B.ACTIVE = 1 "
		response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			if ( rsSet("ENDDATE") = "" OR isNull(rsSet("ENDDATE")) ) then
				str_color = "black"
				bstatus = "ACTIVE"
			else
				str_color = "red"
				bstatus = "ENDED"
			end if
			
			str_journal_table = str_journal_table &_
			 	"<TR style='color:" & str_color & "'>" &_
				"<TD>" & rsSet("FULLNAME") & "</TD>" &_
				"<TD>" & rsSet("ACCOUNTNAME") & "</TD>" &_
				"<TD>" & rsSet("SORTCODE") & "</TD>" &_
				"<TD>" & rsSet("ACCOUNTNUMBER") & "</TD>" &_
				"<TD>" & bstatus & "</TD>" &_
				"<TD><input type=RADIO name='ATTACHBANK' onclick='parent.ATTACHBANK(" & rsSet("BANKDETAILSID") & ")'></TD>" &_
				"<TR>"
				
				
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>NO BANK DETAILS FOUND FOR ANY CUSTOMERS INVOLVED IN THE SELECTED TENANCY.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
function ReturnData(){
	parent.ACCOUNT_DIV.innerHTML = table_div.innerHTML
	parent.STATUS_DIV.style.visibility = "hidden";
	parent.RSLFORM.txt_AMOUNT.value = "";
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="ReturnData()" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
<DIV ID=table_div>
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD>
		<TR >
			<TD height=20 STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=190><B>Customer</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B>Account Name</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150><B>Sort Code</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=150><B>Account Number</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100 nowrap><B>Tenancy Status</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=50><B>Attach</B></TD>
		</TR>
		</THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</DIV>
</BODY>
</HTML>	
