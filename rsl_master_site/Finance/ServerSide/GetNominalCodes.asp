<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CostCentreID, strWhat, count, optionvalues, optiontext, intReturn, theamounts

	strWhat = Request.form("IACTION")
	If strWhat = "getnominalcodes" or strWhat = "getnominalcodes_AUTO" Then rePop() End If

	Function rePop()
		Company = -1
		If(Request.Form("sel_COMPANY") <> "") Then Company = Request.Form("sel_COMPANY")

		If (Company = -1) Then
			optionvalues = ""
			optiontext = "Please Select..."
			Exit Function
		End If

		Call OpenDB()

		SQL = "SELECT HE.Accountid,HE.ACCOUNTNUMBER + ' ' + HE.NAME AS NAME  FROM NL_Account HE " &_
			    "INNER JOIN NL_Account_to_company CCA ON CCA.accountid = he.accountid " &_ 
			    "WHERE cca.companyid = " & Company & " AND HE.ACCOUNTTYPE IN (3, 6, 7, 10, 11, 9, 12, 8, 13,14) AND (HE.ACCOUNTNUMBER not IN ('1600', '2400') ) ORDER BY HE.AccountNumber"
    		
		Call OpenRs(rsHead, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("name").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsHead.Fields.Item("Accountid").Value
			optiontext = optiontext & ";;" & theText

			count = count + 1
			rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Nominal Codes Are Setup..."
		End If

		Call CloseRs(rsHead)
		Call CloseDB()
	End Function

%>
<html>
<head>
<title>NC Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
<script type="text/javascript" language="JavaScript">
    function loaded() {

        if ('<%=strWhat%>' == 'getnominalcodes') {
            parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 10);
        }



    }


</script>
<% if strWhat = "getnominalcodes_AUTO" then %>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    var availableTags = [

      <%
      OpenDB()
      Dim AccountList, cnt

    If(Request.Form("sel_COMPANY") <> "") Then Company = Request.Form("sel_COMPANY")
        'Getting User name
        strSQL = "SELECT HE.ACCOUNTNUMBER + ' ' + HE.NAME AS Account  FROM NL_Account HE " &_
			    "INNER JOIN NL_Account_to_company CCA ON CCA.accountid = he.accountid " &_ 
			    "WHERE cca.companyid = " & Company & " AND HE.ACCOUNTTYPE IN (3, 6, 7, 10, 11, 9, 12, 8, 13,14) AND (HE.ACCOUNTNUMBER not IN ('1600', '2400') ) ORDER BY HE.AccountNumber"
    		
   ' response.write strSQL
	    'strSQL = "Select top 10  AccountNumber +' '+ Name   as Account from nl_account " 
	    Call OpenRs(rsAccount, strSQL)
        cnt = 1
        while (Not rsAccount.EOF)
	    if cnt > 1 then
        %>
        , 
        <%
        end if
        
      %>
      "<%=rsAccount("Account") %>"
      <% 
        cnt = cnt + 1
      rsAccount.Movenext
    Wend
    Call CloseRs(rsAccount)
      CloseDB()
      %>
    ];
	$(window.parent.document ).find('#txt_ACCOUNT').remove();
	$(window.parent.document ).find('#img_ACCOUNT').remove();
	$(window.parent.document ).find('#accountdiv').append("<input id='txt_ACCOUNT' class='TEXTBOX200'  style='width: 150PX'><img src='/js/FVS.gif' width='15' height='15' id='img_ACCOUNT' name='img_ACCOUNT'>");
	
	$(window.parent.document ).find('#txt_ACCOUNT').autocomplete();
    $(window.parent.document ).find('#txt_ACCOUNT').autocomplete(
      "option", { source: availableTags }  
    );
  } );
  </script>
<% end If %>
</body>
</html>
