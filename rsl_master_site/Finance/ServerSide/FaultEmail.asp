
<% ByPassSecurityAccess = true %>
<%
OpenDB()
Function faultEmailToContractor(orderId)
   
   Dim ContactName,JSN,OrderedBy,DDial,FullAddress,TownCity ,Postcode, userId,TenantName, Telephone, rs ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, PropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo

    '------------------------------------------------
    ' GET FAULTCONTRACTORID
    '------------------------------------------------

    'SQL = "	SELECT	FaultContractorId " &_
	'" FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	'" WHERE	 PURCHASEORDERITEMID =" & orderItemId

    'Call OpenRs(rs, SQL)
    '   FaultContractorId = rs("FaultContractorId")
    'Call CloseRs(rs)  


    '------------------------------------------------
    ' GET PURCHASE ORDER, PROPERTY AND CUSTOMER INFO
    '------------------------------------------------
    SQL = " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName "  &_
		" ,FCW.PurchaseOrderId as OrderId "  &_
        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId "  &_
		" ,FFL.JobSheetNumber as JSN "  &_
		" ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy "  &_
		" ,ISNULL( C.WORKDD,'N/A') AS DDial "  &_
		" ,ISNULL(NULLIF(ISNULL('Flat No:' + PP.FLATNUMBER + ', ', '') + ISNULL(PP.HOUSENUMBER, '') + ISNULL(' ' + PP.ADDRESS1, '') "  &_
		" + ISNULL(' ' + PP.ADDRESS2, '') + ISNULL(' ' + PP.ADDRESS3, ''), ''), 'N/A') AS FullAddress "  &_
		" ,ISNULL(PP.TOWNCITY,'') as TownCity "  &_
		" ,ISNULL(PP.POSTCODE,'') as Postcode "  &_
		" ,ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName "  &_
		" ,ISNULL(A.TEL, '') as Telephone "  &_
		" ,ISNULL(A.TELWORK, '') as TelephoneWork "  &_
		" ,ISNULL(A.MOBILE, '') as Mobile "  &_
        " ,ISNULL(A.Email, 'N/A') as Email "  &_
        " ,ISNULL(A.COUNTY, '') as COUNTY  " &_
		" ,ISNULL(FFL.NOTES, '') as FaultNotes "  &_
		" ,ISNULL(F.description,'') as ReportedFault "  &_
		" ,CONVERT(VARCHAR(10),FFL.DUEDATE,103) AS DueDate "  &_
		" ,FCW.FaultContractorId as FaultContractorId "   &_
		" ,PP.PROPERTYID as PropertyId  " &_
		" ,CC.CUSTOMERID as CustomerId  "  &_
        " ,E_CONTACT.WORKEMAIL as WorkEmail  "  &_
        " ,ISNULL(FCW.EstimateRef , 'N/A' ) as EstimateRef  "  &_
        
        " ,FP.userID As userId " &_
    " FROM	FL_CONTRACTOR_WORK AS FCW "  &_
		    " INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID "  &_
		    " INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID "  &_
            " INNER JOIN FL_FAULT_JOURNAL AS FFJ ON FCW.JOURNALID=FFJ.JOURNALID " &_
		    " INNER JOIN FL_FAULT_LOG AS FFL ON  FFJ.FAULTLOGID = FFL.FAULTLOGID "  &_
            " INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID "  &_
            " INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID "  &_
		    " LEFT JOIN C__CUSTOMER AS CC ON FFL.CUSTOMERID = CC.CUSTOMERID "  &_
            " LEFT JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 "  &_
		    " LEFT JOIN P__PROPERTY AS PP ON FFL.PROPERTYID = PP.PROPERTYID "  &_
		    " INNER JOIN FL_FAULT AS F ON FFL.FAULTID = F.FAULTID "  &_
		    " INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseOrderId " &_
    " WHERE	FCW.PurchaseOrderId = " &  orderId


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        ContactName = rs("ContactName")
        OrderId = rs("OrderId")
        JSN = rs("JSN")
        OrderedBy = rs("OrderedBy")
        DDial = rs("DDial")
        FullAddress = rs("FullAddress")
        TownCity = rs("TownCity")
        County = rs("COUNTY")
        Postcode = rs("Postcode")
        TenantName = rs("TenantName")
        Telephone = rs("Telephone")
	    TelephoneWork = rs("TelephoneWork")
	    Mobile = rs("Mobile")
        Email = rs("Email")
	    FaultNotes = rs("FaultNotes")	
        ReportedFault = rs("ReportedFault")
        DueDate = rs("DueDate")
        FaultContractorId = rs("FaultContractorId")
        PropertyId = rs("PropertyId")
        CustomerId = rs("CustomerId")
        Recipient = rs("WorkEmail")
        EmployeeId = rs("EmployeeId")
        userId = rs("userId")
        EstimateRef = rs("EstimateRef")
    End IF
    Call CloseRs(rs)

    '------------------------------------------------
    ' GET WORKS REQUIRED INFO
    '------------------------------------------------

    SQL = "	SELECT	DISTINCT	WorkRequired " &_
	" FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	" WHERE	 FaultContractorId = " & FaultContractorId

    Call OpenRs(rs, SQL)
    
    WorkRequired = ""
    while not rs.EOF
        WorkRequired = WorkRequired & rs("WorkRequired") & " <br />"
        rs.movenext()
    wend

    Call CloseRs(rs)  


    '------------------------------------------------
    ' GET WORKS COST INFO
    '------------------------------------------------

    SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			" ,SUM(Vat) as VAT "  &_
			" ,SUM(Gross) as Gross "  &_
	        " FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	        " WHERE	 FaultContractorId = " & FaultContractorId

    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
     NetCost = rs("NetCost")
     VAT = rs("VAT")
     Gross = rs("Gross")
    end if
    Call CloseRs(rs)  


    
    '------------------------------------------------
    ' GET RISK INFO
    '------------------------------------------------

    SQL = " SELECT	DISTINCT	CATDESC, SUBCATDESC "   &_
	       " FROM	RISK_CATS_SUBCATS("& CustomerId & ") "

    Call OpenRs(rs, SQL)
    RiskInfo = ""
    while not rs.EOF
        RiskInfo = RiskInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br />"
        rs.movenext()
    wend
    Call CloseRs(rs)  
    if RiskInfo = "" then
        RiskInfo = "N/A"
    end if
    	

    '------------------------------------------------
    ' GET VULNARABILITY INFO
    '------------------------------------------------

    SQL =    " SELECT DISTINCT CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC "  &_
	         " FROM	C_CUSTOMER_VULNERABILITY CV "  &_
		        " INNER JOIN C_JOURNAL J ON CV.JOURNALID = J.JOURNALID "  &_
		        " INNER JOIN C_VULNERABILITY CCV ON CCV.JOURNALID = J.JOURNALID "  &_
			    " INNER JOIN C_VULNERABILITY_CATEGORY CAT ON CAT.CATEGORYID = CV.CATEGORYID  "  &_ 
			    " INNER JOIN C_VULNERABILITY_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CV.SUBCATEGORYID and cv.categoryid=SUBCAT.categoryid  "  &_
             " WHERE	 CV.CUSTOMERID ="& CustomerId &" "  &_
			        "AND ITEMNATUREID = 61 "  &_
			        "AND CCV.ITEMSTATUSID <> 14 "  &_
			        "AND CV.VULNERABILITYHISTORYID = (	SELECT	MAX(VULNERABILITYHISTORYID) "  &_
												"FROM	C_VULNERABILITY IN_CV "  &_
												"WHERE	IN_CV.JOURNALID = J.JOURNALID) "   


    Call OpenRs(rs, SQL)
    VulnarabilityInfo = ""
    while not rs.EOF
        VulnarabilityInfo = VulnarabilityInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br /> "
        rs.movenext()
    wend
    Call CloseRs(rs)  
    if VulnarabilityInfo = "" then
        VulnarabilityInfo = "N/A"
    end if


    '------------------------------------------------
    ' GET ASBESTOS INFO
    '------------------------------------------------

    SQL =   " SELECT DISTINCT RL.ASBRISKLEVELDESCRIPTION,A.RISKDESCRIPTION "   &_
            " FROM P__PROPERTY P "  &_
            "    INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ARL ON P.PROPERTYID = ARL.PROPERTYID "    &_
            "    INNER JOIN P_PROPERTY_ASBESTOS_RISK AR ON ARL.PROPASBLEVELID = AR.PROPASBLEVELID "    &_
            "    INNER JOIN P_ASBESTOS A ON ARL.ASBESTOSID = A.ASBESTOSID "  &_
            "    INNER JOIN P_ASBRISKLEVEL RL ON ARL.ASBRISKLEVELID = RL.ASBRISKLEVELID  "  &_
            " WHERE P.PROPERTYID = '"  &  PropertyId & "'and (ARL.DateRemoved is null or  CONVERT(DATE, ARL.DateRemoved) > '"& date() &"' )"


    Call OpenRs(rs, SQL)
    AsbestosInfo = ""
    while not rs.EOF
        AsbestosInfo = AsbestosInfo & rs("ASBRISKLEVELDESCRIPTION") & " : " & rs("RISKDESCRIPTION") & " <br /> "
        rs.movenext()
    wend
    Call CloseRs(rs)  
     socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if
     '------------------------------------------------
    'SETTING URL FOR ACCEPT PO PAGE
    '------------------------------------------------
   acceptUrl = socket & ":" & "//" & Request.ServerVariables("server_name") & "/PropertyDataRestructure/Views/ReactiveRepairs/AcceptFaultPurchaseOrder.aspx?oid=" & OrderId & "&uid=" & userId
    
    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------
    Dim emailbody
    emailbody =  getContractorEmailTemplateFault()
    if(NOT ISNULL(ContactName)) THEN
    emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
    END IF

    if(NOT ISNULL(OrderId)) THEN
    emailbody = Replace(emailbody,"{OrderId}","PO " & OrderId)
    END IF

    if(NOT ISNULL(JSN)) THEN
    emailbody = Replace(emailbody,"{JobRef}",JSN)
    END IF

    if(NOT ISNULL(EstimateRef)) THEN
    emailbody = Replace(emailbody,"{EstimateRef}",EstimateRef)
    END IF

    if(NOT ISNULL(OrderedBy)) THEN
    emailbody = Replace(emailbody,"{OrderedBy}",OrderedBy)
    END IF

    if(NOT ISNULL(DDial)) THEN
    emailbody = Replace(emailbody,"{DDial}",DDial)
    END IF

    if(NOT ISNULL(ReportedFault)) THEN
    emailbody = Replace(emailbody,"{ReportedFault}",ReportedFault)
    END IF

    if(NOT ISNULL(WorkRequired)) THEN
    emailbody = Replace(emailbody,"{WorksRequired}",WorkRequired)
    END IF

    if(NOT ISNULL(DueDate)) THEN
    emailbody = Replace(emailbody,"{DueDate}",DueDate)
    END IF

    if(NOT ISNULL(NetCost)) THEN
    emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
    END IF

    if(NOT ISNULL(VAT)) THEN
    emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
    END IF

    if(NOT ISNULL(Gross)) THEN
    emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
    END IF

    if(NOT ISNULL(FullAddress)) THEN
    emailbody = Replace(emailbody,"{Address}",FullAddress)
    END IF

    if(NOT ISNULL(TownCity)) THEN
    emailbody = Replace(emailbody,"{TownCity}",TownCity)
    END IF

    if(NOT ISNULL(County)) THEN
    emailbody = Replace(emailbody,"{County}",County)
    END IF

    if(NOT ISNULL(Postcode)) THEN
    emailbody = Replace(emailbody,"{PostCode}",Postcode)
    END IF

    if(NOT ISNULL(TenantName)) THEN
    emailbody = Replace(emailbody,"{TenantName}",TenantName)
    END IF

    if(NOT ISNULL(Telephone)) THEN
    emailbody = Replace(emailbody,"{Telephone}",Telephone)
    END IF

    if(NOT ISNULL(TelephoneWork)) THEN
    emailbody = Replace(emailbody,"{TelephoneWork}",TelephoneWork)
    END IF

    if(NOT ISNULL(Mobile)) THEN
    emailbody = Replace(emailbody,"{Mobile}",Mobile)
    END IF

    if(NOT ISNULL(Email)) THEN
    emailbody = Replace(emailbody,"{Email}",Email)
    END IF

    if(NOT ISNULL(RiskInfo)) THEN
    emailbody = Replace(emailbody,"{RiskDetail}",RiskInfo)
    END IF

    if(NOT ISNULL(VulnarabilityInfo)) THEN
    emailbody = Replace(emailbody,"{VulnerabilityDetail}",VulnarabilityInfo)
    END IF

    if(NOT ISNULL(AsbestosInfo)) THEN
    emailbody = Replace(emailbody,"{Asbestos}",AsbestosInfo)
    END IF

    if(NOT ISNULL(FaultNotes)) THEN
    emailbody = Replace(emailbody,"{FaultNotes}",FaultNotes)
    END IF

    emailbody = Replace(emailbody,"{#}",acceptUrl)
    emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
    emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
    sendEmailFault emailbody,Recipient



End Function


Function sbFaultEmailToContractor(orderId)
   
   Dim ContactName,JSN,OrderedBy,DDial,FullAddress,TownCity ,Postcode, userId,TenantName, Telephone, rs ,Recipient,EmployeeId
   Dim ReportedFault, DueDate, FaultContractorId, lPropertyId, CustomerId,WorkRequired,NetCost,VAT,Gross, RiskInfo,VulnarabilityInfo,AsbestosInfo


   
    '------------------------------------------------
    ' GET FAULTCONTRACTORID
    '------------------------------------------------

    'SQL = "	SELECT	FaultContractorId " &_
	'" FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	'" WHERE	 PURCHASEORDERITEMID =" & orderItemId

    'Call OpenRs(rs, SQL)
    '   FaultContractorId = rs("FaultContractorId")
    'Call CloseRs(rs)  


    '------------------------------------------------
    ' GET PURCHASE ORDER, PROPERTY AND CUSTOMER INFO
    '------------------------------------------------
    SQL = " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName "  &_
		" ,FCW.PurchaseOrderId as OrderId "  &_
        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId "  &_
		" ,FFL.JobSheetNumber as JSN "  &_
		" ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy "  &_
		" ,ISNULL( C.WORKDD,'N/A') AS DDial "  &_
		" ,ISNULL(NULLIF(ISNULL('Flat No:' + PP.FLATNUMBER + ', ', '') + ISNULL(PP.HOUSENUMBER, '') + ISNULL(' ' + PP.ADDRESS1, '') "  &_
		" + ISNULL(' ' + PP.ADDRESS2, '') + ISNULL(' ' + PP.ADDRESS3, ''), ''), 'N/A') AS FullAddress "  &_
		" ,ISNULL(PP.TOWNCITY,'') as TownCity "  &_
		" ,ISNULL(PP.POSTCODE,'') as Postcode "  &_
		" ,ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName "  &_
		" ,ISNULL(A.TEL, '') as Telephone "  &_
		" ,ISNULL(A.TELWORK, '') as TelephoneWork "  &_
		" ,ISNULL(A.MOBILE, '') as Mobile "  &_
        " ,ISNULL(A.Email, 'N/A') as Email "  &_
        " ,ISNULL(A.COUNTY, '') as COUNTY  " &_
		" ,ISNULL(FFL.NOTES, '') as FaultNotes "  &_
		" ,ISNULL(F.description,'') as ReportedFault "  &_
		" ,CONVERT(VARCHAR(10),FFL.DUEDATE,103) AS DueDate "  &_
		" ,FCW.FaultContractorId as FaultContractorId "   &_
		" ,PP.PROPERTYID as PropertyId  " &_
		" ,CC.CUSTOMERID as CustomerId  "  &_
        " ,E_CONTACT.WORKEMAIL as WorkEmail  "  &_
        " ,ISNULL(FCW.EstimateRef , 'N/A' ) as EstimateRef  "  &_
        " ,WO.SCHEMEID as SId" &_
        " ,FP.userID As userId " &_
        " ,FCW.Estimate As Estimate " &_
    " FROM	FL_CONTRACTOR_WORK AS FCW "  &_
		    " INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID "  &_
		    " INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID "  &_
            " INNER JOIN FL_FAULT_JOURNAL AS FFJ ON FCW.JOURNALID=FFJ.JOURNALID " &_
		    " INNER JOIN FL_FAULT_LOG AS FFL ON  FFJ.FAULTLOGID = FFL.FAULTLOGID "  &_
            " INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID "  &_
            " INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID "  &_
		    " LEFT JOIN C__CUSTOMER AS CC ON FFL.CUSTOMERID = CC.CUSTOMERID "  &_
            " LEFT JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1 "  &_
		    " LEFT JOIN P__PROPERTY AS PP ON FFL.PROPERTYID = PP.PROPERTYID "  &_
		    " INNER JOIN FL_FAULT AS F ON FFL.FAULTID = F.FAULTID "  &_
		    " INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseOrderId " &_
            " LEFT JOIN P_WORKORDER WO ON WO.ORDERID = FP.ORDERID " &_
    " WHERE	FCW.PurchaseOrderId = " &  orderId


    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        ContactName = rs("ContactName")
        OrderId = rs("OrderId")
        JSN = rs("JSN")
        OrderedBy = rs("OrderedBy")
        DDial = rs("DDial")
        FullAddress = rs("FullAddress")
        TownCity = rs("TownCity")
        County = rs("COUNTY")
        Postcode = rs("Postcode")
        TenantName = rs("TenantName")
        Telephone = rs("Telephone")
	    TelephoneWork = rs("TelephoneWork")
	    Mobile = rs("Mobile")
        Email = rs("Email")
	    FaultNotes = rs("FaultNotes")	
        ReportedFault = rs("ReportedFault")
        DueDate = rs("DueDate")
        FaultContractorId = rs("FaultContractorId")
        PropertyId = rs("PropertyId")
        CustomerId = rs("CustomerId")
        Recipient = rs("WorkEmail")
        EmployeeId = rs("EmployeeId")
        userId = rs("userId")
        EstimateRef = rs("EstimateRef")
        Estimate = rs("Estimate")
        SId = rs("SId")
    ELSE
        ContactName = ""
        OrderId = ""
        JSN = ""
        OrderedBy = ""
        DDial = ""
        FullAddress = ""
        TownCity = ""
        County = ""
        Postcode = ""
        TenantName = ""
        Telephone = ""
	    TelephoneWork = ""
	    Mobile = ""
        Email = ""
	    FaultNotes = ""
        ReportedFault = ""
        DueDate = ""
        FaultContractorId = ""
        PropertyId = ""
        CustomerId = ""
        Recipient = ""
        EmployeeId = ""
        userId = ""
        EstimateRef = ""
        Estimate = ""
        SId = ""
    End IF
    Call CloseRs(rs)

    SQL = "SELECT BL.BLOCKNAME , BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost FROM F_PURCHASEORDER PO " &_
        "LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " &_
		"LEFT JOIN P_BLOCK BL ON BL.BLOCKID IN(PO.BLOCKID, WO.BLOCKID) " &_	
		"WHERE PO.ACTIVE = 1 AND PO.ORDERID = " & orderId
     Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
        Block = rs("BTC") & "<br/>" & rs("BPost")  & "<br/>" & rs("BCount")& "<br/>"
    End IF
     if(NOT ISNULL(Block)) THEN
         SQL = "select top 1 BL.TOWNCITY as BTC, BL.COUNTY as BCount, BL.POSTCODE as BPost from P_BLOCK as BL where schemeid=" &SId
        Call OpenRs(rs, SQL)
        if (NOT rs.EOF) then
            Block = rs("BTC") & "<br/>" & rs("BPost")  & "<br/>" & rs("BCount")& "<br/>"
        end if
    END IF
    FullAddress = Block
    Call CloseRs(rs)
    '------------------------------------------------
    ' GET WORKS REQUIRED INFO
    '------------------------------------------------

    SQL = "	SELECT	DISTINCT	WorkRequired " &_
	" FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	" WHERE	 FaultContractorId = " & FaultContractorId

    Call OpenRs(rs, SQL)
    
    WorkRequired = ""
    while not rs.EOF
        WorkRequired = WorkRequired & rs("WorkRequired") & " <br />"
        rs.movenext()
    wend

    Call CloseRs(rs)  


    '------------------------------------------------
    ' GET WORKS COST INFO
    '------------------------------------------------

    SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			" ,SUM(Vat) as VAT "  &_
			" ,SUM(Gross) as Gross "  &_
	        " FROM	FL_CONTRACTOR_WORK_DETAIL "  &_
	        " WHERE	 FaultContractorId = " & FaultContractorId

    Call OpenRs(rs, SQL)
    If Not rs.EOF Then 
     NetCost = rs("NetCost")
     VAT = rs("VAT")
     Gross = rs("Gross")
     end if
    Call CloseRs(rs)  

    '------------------------------------------------
    ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
    '------------------------------------------------
    
    Dim emailbody
    emailbody =  getContractorEmailTemplateFaultSB()
    if(NOT ISNULL(ContactName)) THEN
        emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
    END IF

    if(NOT ISNULL(OrderId)) THEN
    emailbody = Replace(emailbody,"{OrderId}","PO " & OrderId)
    END IF

    if(NOT ISNULL(JSN)) THEN
        emailbody = Replace(emailbody,"{PMO}",JSN)
    END IF

    if(NOT ISNULL(EstimateRef)) THEN
    emailbody = Replace(emailbody,"{EstimateRef}",EstimateRef)
    END IF

    if(NOT ISNULL(Estimate)) THEN
        emailbody = Replace(emailbody,"{Estimate}",Estimate)
    END IF

    if(NOT ISNULL(OrderedBy)) THEN
        emailbody = Replace(emailbody,"{OrderedBy}",OrderedBy)
    END IF

    if(NOT ISNULL(WorkRequired)) THEN
        emailbody = Replace(emailbody,"{WorksRequired}",WorkRequired)
    END IF

    if(NOT ISNULL(NetCost)) THEN
        emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
    END IF

    if(NOT ISNULL(VAT)) THEN
        emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
    END IF

    if(NOT ISNULL(Gross)) THEN
        emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
    END IF

    if(NOT ISNULL(FullAddress)) THEN
        emailbody = Replace(emailbody,"{Address}",FullAddress)
    ELSE
         emailbody = Replace(emailbody,"{Address}","N/A")
    END IF

    if(NOT ISNULL(TownCity)) THEN
        emailbody = Replace(emailbody,"{TownCity}",TownCity)
    END IF

    if(NOT ISNULL(County)) THEN
        emailbody = Replace(emailbody,"{County}",County)
    END IF

    if(NOT ISNULL(Postcode)) THEN
        emailbody = Replace(emailbody,"{PostCode}",Postcode)
    END IF

    emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
    emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
    sendEmailFault emailbody,Recipient



End Function


    '--------------------------  TEST

   Function sendEmailFault(body,recipient)
   
    if IsNull(recipient) then
    session("noRecipient") = "true"
        Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
    Dim iMsg
    Set iMsg = CreateObject("CDO.Message")
    Dim iConf
    Set iConf = CreateObject("CDO.Configuration")

    Dim Flds
    Set Flds = iConf.Fields
    Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

    emailbody = body
    emailSubject = "Purchase Order for Reactive Repair Work "
    
    Set iMsg.Configuration = iConf
    iMsg.To = recipient
    iMsg.From = "noreply@broadlandgroup.org"
    iMsg.Subject = emailSubject
    iMsg.HTMLBody = emailbody
    iMsg.Send	

    end if
End Function

Function getContractorEmailTemplateFault()
    Dim assignToContractorEmailTemplate
    assignToContractorEmailTemplate = "<table> <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> Please find below details of Reactive Repair works required: </td> </tr> <tr> <td colspan=""2""> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; width: 25%;""> PO number: </td> <td colspan=""2""> {OrderId} </td> </tr> <tr> <td style=""padding-right: 20px;""> Job Ref: </td> <td colspan=""2""> {JobRef} </td> </tr> <tr> <td style=""padding-right: 20px;""> Ordered by: </td> <td colspan=""2""> {OrderedBy} </td> </tr> <tr> <td style=""padding-right: 20px;""> DD Dial: </td> <td colspan=""2""> {DDial} </td> </tr> <tr> <td style=""padding-right: 20px;""> Email: </td> <td> {Email} </td> </tr> <tr> <td> </td> <td> </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Address: </td> <td colspan=""5""> <table width=""100%""> <tr> <td style=""vertical-align: top;""> {Address} </td> <td style=""vertical-align: top; padding-left: 30px""> Tenant: </td> <td> {TenantName} </td> </tr> <tr> <td style=""padding-right: 50px;""> {TownCity} </td> <td style=""padding-right: 20px; padding-left: 30px""> Telephone: </td> <td> {Telephone} </td> </tr> <tr> <td style=""padding-right: 50px;""> {County} </td> <td style=""padding-left: 30px; vertical-align: top;""> Risk: </td> <td> {RiskDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> {PostCode} </td> <td style=""padding-left: 30px; vertical-align: top;""> Vulnerability: </td> <td> {VulnerabilityDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> &nbsp; </td> <td style=""padding-left: 30px; vertical-align: top;""> Asbestos: </td> <td> {Asbestos} </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align: top;""> Works required: </td> <td colspan=""2""> {WorksRequired} </td> </tr> <tr> <td colspan=""3""> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Estimate Ref: </td> <td> {EstimateRef} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style=""padding-right: 20px;""> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style=""padding-right: 20px;""> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr><tr> <td> <a href=""{#}"">Accept PO page</a> </td> </tr>  <tr> </tr> <tr> </tr> <tr> <td> </td> </tr> <tr> <td> </td> </tr> <tr> <td> Kind regards </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    getContractorEmailTemplateFault = assignToContractorEmailTemplate
End Function

Function getContractorEmailTemplateFaultSB()
    Dim assignToContractorEmailTemplateSB
    assignToContractorEmailTemplateSB = "<table> <tr> <td colspan=""2""> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2"" > Please find below details of Scheme/Block reactive repair&nbsp; works required: </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> JS Number: </td> <td > {PMO} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;width: 100px; vertical-align: top;""> Address: </td> <td> <table> <tr> <td style=""padding-right: 50px;""> {Address} </td> </tr> <tr> <td style=""padding-right: 50px;""> {TownCity} </td> </tr> <tr> <td style=""padding-right: 50px;""> {County} </td> </tr> <tr> <td style=""padding-right: 50px;""> {PostCode} </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px; vertical-align:top;""> Works required: </td> <td> {WorksRequired} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Estimate (&pound;): </td> <td> {Estimate} </td> </tr> <tr> <td style=""padding-right: 20px;""> Estimate Ref: </td> <td> {EstimateRef} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style=""padding-right: 20px;""> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style=""padding-right: 20px;""> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style=""padding-right: 20px;""> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan=""2""> <strong>Broadland Housing </strong> <br /> <br /> <table style=""vertical-align: text-top;""> <tr> <td> <img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
    getContractorEmailTemplateFaultSB = assignToContractorEmailTemplateSB
End Function	

Function PurchaseNumberFault(strWord)
    PurchaseNumberFault = "PO " & characterPad(strWord,"0", "l", 7)
End Function	


'--------------------------------------------------------------


%>
