<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim BlockId, SchemeID, strWhat, count, optionvalues, optiontext, intReturn, theamounts, Company

	strWhat = Request.form("IACTION")
	If strWhat = "getProperty" Then rePop() End If

	Function rePop()
		BlockId = -1
		If(Request.Form("sel_BLOCK") <> "") Then 
            BlockId = Cint(Request.Form("sel_BLOCK"))
        End If
        If(Request.Form("sel_SCHEME") <> "") Then 
            SchemeID = Cint(Request.Form("sel_SCHEME"))
        End If
		Call OpenDB()
        IsAllBlockSelected = 0
        If(SchemeID = -1 And BlockId = -1) then
            IsAllBlockSelected = 1
        ElseIf (SchemeID = -1 And (BlockId <> -1 AND BlockId <> 0)) Then
            SQL = "SELECT PROPERTYID, HOUSENUMBER + ' ' + ADDRESS1 As PropertyName FROM P__PROPERTY " &_
                   " Where BlockId = " & BlockId
        ElseIf ((SchemeID <> -1 And SchemeID <> 0) And (BlockId <> -1 AND BlockId <> 0)) Then
            SQL = "SELECT PROPERTYID, HOUSENUMBER + ' ' + ADDRESS1 As PropertyName FROM P__PROPERTY " &_
                   " Where SchemeID = " & SchemeID & " AND BlockId = " & BlockId
        ElseIf((SchemeID <> -1 And SchemeID <> 0) AND BlockId = -1) Then
            IsAllBlockSelected = 1
        Else
            SQL = "SELECT PROPERTYID, HOUSENUMBER + ' ' + ADDRESS1 As PropertyName FROM P__PROPERTY " &_
                   " Where SchemeID = " & SchemeID
        End If

        If(IsAllBlockSelected = 1) Then
            optionvalues = "-1"
			optiontext = "All Properties"
        Else
        
            Response.Write(SQL)
		    Call OpenRs(rsProperty, SQL)

		    count = 0
		    theamounts = "0"

            If(NOT rsProperty.EOF) Then
                optionvalues = "-1"
		        optiontext = "All Properties"
		        count = count + 1
            End If

		    While (NOT rsProperty.EOF)
			    theText = rsProperty.Fields.Item("PropertyName").Value
			    theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			    optionvalues = optionvalues & ";;" & rsProperty.Fields.Item("PROPERTYID").Value
			    optiontext = optiontext & ";;" & theText

			    count = count + 1
			    rsProperty.MoveNext()
		    Wend
		    If (rsProperty.CursorType > 0) Then
		      rsProperty.MoveFirst
		    Else
		      rsProperty.Requery
		    End If
		    If (count = 0) Then
			    optionvalues = "0"
			    optiontext = "No Properties Are Setup..."
		    End If

		    Call CloseRs(rsProperty)
		    Call CloseDB()
        end if 
	End Function

%>
<html>
<head>
    <title>Property Data Fetcher</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
    <script type="text/javascript" language="JavaScript">
        function loaded() {
            parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 4);
        }
    </script>
</body>
</html>
