<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim TenancyID	
	Dim AMOUNT,RENT_PART,SERVICES_PART,SUPPORTEDSERVICES_PART,INELIGSERV_PART
	Dim	COUNCILTAX_PART,WATERRATES_PART, PAYMENTSTARTDATE

	rw hello
	TenancyID 	= request.form("txt_SEARCH")

	OpenDB()
		load_record()
	CloseDB()
		
	
	Function load_record()
	
		SQL = "F_INITIAL_RENT_DISPLAY " & TenancyID
		
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF then

			AMOUNT 					= rsSet("AMOUNT")
			RENT_PART 				= rsSet("RENT_PART")
			SERVICES_PART			= rsSet("SERVICES_PART")
			SUPPORTEDSERVICES_PART	= rsSet("SUPPORTEDSERVICES_PART")
			INELIGSERV_PART			= rsSet("INELIGSERV_PART")
			COUNCILTAX_PART			= rsSet("COUNCILTAX_PART")
			WATERRATES_PART			= rsSet("WATERRATES_PART")
			PAYMENTSTARTDATE		= rsSet("PAYMENTSTARTDATE")
		Else
			str_content = "No Initial Rent Data found for this tenancy"
		End If
		Call CloseRs(rsSet)
		
	End Function

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript">

	function return_data(){
					
		
		parent.IrDataDiv.innerHTML = document.getElementById("Content1").innerHTML					
	
	}	
	
</SCRIPT>
<BODY onLoad="return_data()">
<div id="Content1" name="Content1">
  <table width="522">
    
    <tr>
      <td><b>Tenancy Ref :</b> </td>
      <td><b><%=TenancyID%></b></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="123">Amount......................</td>
      <td width="127"><%=AMOUNT%></td>
      <td width="3">&nbsp;</td>
      <td width="137">Start Date : </td>
      <td width="86"><%=PAYMENTSTARTDATE%></td>
      <td width="9">&nbsp;</td>
      <td width="5">&nbsp;</td>
    </tr>
    <tr>
      <td height="10" colspan="7"></td>
    </tr>
    <tr>
      <td><strong>Part Breakdown</strong> </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Rent.........................</td>
      <td><%=RENT_PART%></td>
      <td>&nbsp;</td>
      <td>Services....................</td>
      <td><%=SERVICES_PART%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Supported Services..... </td>
      <td><%=SUPPORTEDSERVICES_PART%></td>
      <td>&nbsp;</td>
      <td>Ineligile Services........ </td>
      <td><%=INELIGSERV_PART%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Council Tax .............. </td>
      <td><%=COUNCILTAX_PART%></td>
      <td>&nbsp;</td>
      <td>Water Rates..............</td>
      <td><%=WATERRATES_PART%></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</div>
</BODY>
</HTML>
