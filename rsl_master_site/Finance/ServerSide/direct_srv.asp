<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim action, tnum, str_data, isValid, success, redir, str_redirection, attach_id, ErrorText
	
	action = Request("action")
	
	tnum = Request.form("txt_TENANTSEARCH")
	if (tnum = "") then tnum = -1
	
	attach_id = Request("hid_ATTACHID")
	Response.Write attach_id
	 
	isValid = 0
	str_data = ""
	success = 0
	redir = 0
	ErrorText = ""
	
	OpenDB()
	
	if action = "GETWITHHBREF" then	
		getTenancyNumber()
		getTenantInfo() 		
	elseif action = "GET" Then 
		getTenantInfo() 
	else 
		'IF THE REQUEST (sel_DIRECTPOSTITEMS) OBJECT IS 1 OR 10, THEN NEED TO CHECK THAT NO LA REFUNDS ARE BEING PROCESSED OTHERWISE
		'WE COULD GET LA REFUNDS AUTOMATICALLY BEING PROCESSED BY THE LA REFUND PAYMENT TOOL, WITHOUT THE USER 
		'WHO IS DOING THE PROCESSING KNOWING ABOUT IT.
		IF (Request.Form("sel_DIRECTPOSTITEMS") = 1 OR Request.Form("sel_DIRECTPOSTITEMS") = 10) THEN

			SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_LAREFUNDS' AND DEFAULTVALUE = 0"
			Call OpenRs(rsPostable, SQL)
			if (NOT rsPostable.EOF) then
				post()
			else
				ErrorText = "The LA Refund cannot be processed at this present time\nbecuase previous entries are currently being processed for payment.\nPlease try again in a few seconds."
			end if
			Call CloseRs(rsPostable)
		'SAME AS ABOVE EXCEPT FOR TENANT REFUNDS.
		ELSEIF (Request.Form("sel_DIRECTPOSTITEMS") = 6 OR Request.Form("sel_DIRECTPOSTITEMS") = 11) THEN			
			SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_TENANTREFUNDS' AND DEFAULTVALUE = 0"
			Call OpenRs(rsPostable, SQL)
			if (NOT rsPostable.EOF) then
				post()
			else
				ErrorText = "The Tenant Refund cannot be processed at this present time\nbecuase previous entries are currently being processed for payment.\nPlease try again in a few seconds."
			end if
			Call CloseRs(rsPostable)
		'ANY OTHER DIRECT POST ITEM.....
		ELSE 
			post() 
		END IF
	end If
	
	CloseDB()

	Function getTenancyNumber()
		'this section gets the tenancyid from hb ref number		
		SQL = "SELECT TENANCYID FROM F_HBINFORMATION WHERE HBREF = '" & REQUEST("txt_CLAIMNUMBER") & "'"
		Call OpenRs(rsHBREF, SQL)
		if (NOT rsHBREF.EOF) then
			tnum = rsHBREF("TENANCYID")
		else
			tnum = -1
		end if
		CloseRs(rsHBREF)
	End Function
				
	Function getTenantInfo()
		'WHY WASTE TIME RUNNING THE SQL....
		if (tnum <> -1) then
		
			'START OF GET CUSTOMER DETAILS....
			CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
					"FROM C_CUSTOMERTENANCY CT " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
					"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & tnum
			Call OpenRs(rsCust, CustSQL)
			strUsers = ""
			CustCount = 0
			while NOT rsCust.EOF 
				if CustCount = 0 then
					strUsers = strUsers & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
					CustCount = 1
				else
					strUsers = strUsers & ", " & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
				end if					
				rsCust.moveNext()
			wend
			CloseRs(rsCust)
			SET rsCust = Nothing
			'END OF GET CUSTOMER DETAILS

			strSQL = "SELECT T.TENANCYID AS TTENANCYID, " &_
					"ISNULL(T.TENANCYID, '') AS TENANCYREF, " &_
					"ISNULL(P.PROPERTYID, '') AS PROPERTYID, " &_
					"ISNULL(P.HOUSENUMBER, '') AS HOUSENUMBER, " &_
					"ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
					"ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
					"ISNULL(P.ADDRESS3, ' ' ) AS ADDRESS3, " &_		
					"ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
					"ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
					"ISNULL(P.COUNTY , ' ' ) AS COUNTY, " &_
					"ISNULL(LA.DESCRIPTION , ' ' ) AS LOCALAUTHORITY " &_					
					"FROM C_TENANCY T " &_
					"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID " &_
					"LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
					"WHERE T.TENANCYID = " & tnum
													
			Call OpenRS(rsSet, strSQL)	
			If not rsSet.EOF Then
				isValid = 1
				str_data = "<table><TR><TD width=80px><B>Tenancy No :</B></TD><TD>" & TenancyReference(rsSet("TTENANCYID")) & "</TD></TR>" &_
								"<TR><TD width=80px><B>Name :</B></TD><TD>" & strUsers & "</TD></TR>" &_
								"<TR><TD><B>Address :</B></TD><TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("ADDRESS2") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("ADDRESS3") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("TOWNCITY") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("COUNTY") & "</TD></TR>" &_
								"<TR><TD>&nbsp;</TD><TD>" & rsSet("POSTCODE") & "</TD></TR>" &_
								"<TR><TD><B>LA : </B></TD><TD>" & rsSet("LOCALAUTHORITY") & "</TD></TR></TABLE>"								
			End if
			CloseRs(rsSet)
		End if
		
		If str_data = "" then
			str_data = "<table valign=top><TR><TD>No Matching Records!!</TD></TR></TABLE>"
			'Reset TNUM to "" so that it does not return it into the textbox on the main page
			tnum = ""
		End If
		
	End Function
	
	Function post()
		'GET THE HIDDEN TENANCY NUMBER INSTEAD
		tnum = Request.form("hid_TENANTNUMBER")
		
		if (tnum = "") then tnum = -1		
		Dim item_type, payment_type, f_status, f_direction, isdebit, to_date, from_date, line_status
		
		// format dates if provided
		to_date = "NULL"
		from_date = "NULL"
		if Request.Form("txt_FROM") <> "" Then
			from_date = "'" & FormatDateTime(Request.Form("txt_FROM"),1) & "'"
		end if
		if Request.Form("txt_TO") <> "" Then
			to_date = "'" & FormatDateTime(Request.Form("txt_TO"),1) & "'"
		end if
		
		// determine whether or not to redirect to CRM page
		redir = Request.Form("redir")
		if redir = "" then redir = 0 end if
		
		// get details of chosen payment method
		strSQL = "SELECT * FROM F_DIRECTPOSTITEMS WHERE ITEMID = " & Request.Form("sel_DIRECTPOSTITEMS")
		Call OpenRs(rsSet, strSQL)
		
		// write journal entry for direct posting
		If not rsSet.EOF Then
			
			item_type = rsSet("F_ITEMTYPE")
			payment_type = rsSet("F_PAYMENTTYPE")
			f_status = rsSET("F_STATUS")
			// if DD or cheque then change the status of the attached journal entry
			If attach_id <> "" AND isNumeric(attach_id) Then
				'only if a direct cedit unpaid or cheque unpaid
				if (Request.Form("sel_DIRECTPOSTITEMS") = 4 OR Request.Form("sel_DIRECTPOSTITEMS") = 5 OR Request.Form("sel_DIRECTPOSTITEMS") = 13) then
					Call change_entry_status(f_status)
				end if
			End If
			
			// for DD and chq make status = 13 - 'Adjustment'
			if f_status = 10 or f_status = 11 then
				f_status = 13
			End if

			f_direction = rsSET("DIRECTION")
			if f_direction = "-" then isdebit = 0 else isdebit = 1 end if
		
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL  " &_
			"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID, PAYMENTSTARTDATE, PAYMENTENDDATE, ISDEBIT ) " &_
			" VALUES (" &_
						Request.Form("hid_TENANTNUMBER") & ", '" &_
						FormatDateTime(Request.Form("txt_PROCESSDATE"),1) & "', " &_
						item_type & ", " &_
						payment_type & ", " &_
						f_direction & Request.Form("txt_AMOUNT") & ", " &_
						f_status & ", " &_
						from_date & ", " &_
						to_date & ", " &_
						isdebit & "" &_						
						"); select SCOPE_IDENTITY() AS NEWID;"
			Call OpenRs(rsID, SQL)
			NEWID = rsID("NEWID")
			CloseRS(rsID)

			//insert an entry into the linking table if appropriate
			If attach_id <> "" AND isNumeric(attach_id) Then
				'if dd unpaid or cheque unpaid
				if (Request.Form("sel_DIRECTPOSTITEMS") = 4 OR Request.Form("sel_DIRECTPOSTITEMS") = 5 OR Request.Form("sel_DIRECTPOSTITEMS") = 13 OR Request.Form("sel_DIRECTPOSTITEMS") = 69) then			
					SQL = "INSERT INTO F_DIRECTPOSTLINKING (UPDATED_JOURNALID, NEW_JOURNALID, DIRECTPOSTITEM) VALUES(" & attach_id & ", " & NEWID & ", " & payment_type & " )"
					Conn.Execute (SQL)
                    if (Request.Form("sel_DIRECTPOSTITEMS") = 69) then
                        EnterUniversalCredit(NEWID)
                    end if
				'if tenant refund cheque or bacs
				elseif (Request.Form("sel_DIRECTPOSTITEMS") = 6) then
					'get the account details and store separately, just incase they are changed later on....
					SQL = "SELECT C.CUSTOMERID, " &_
							"ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER FROM F_BANKDETAILS B " &_
							"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = B.CUSTOMERID " &_
							"WHERE BANKDETAILSID = " &  attach_id
					Call OpenRs(rsBank, SQL)
					CustomerID = rsBank("CUSTOMERID")
					AccountName = rsBank("ACCOUNTNAME")
					AccountNumber = rsBank("ACCOUNTNUMBER")
					SortCode = rsBank("SORTCODE")
					CloseRs(rsBank)
					
					'insert the data into the database immediately for processing via bacs, etc...
					SQL = "INSERT INTO F_TENANTREFUNDS (JOURNALID, BANKDETAILSID, CUSTOMERID, TENANCYID, ACCOUNTNAME, ACCOUNTNUMBER, SORTCODE, STATUS) " &_
							"VALUES (" & NEWID & "," & attach_id & "," & CustomerID & "," & Request.Form("hid_TENANTNUMBER") & ",'" & AccountName & "'," &_
							" '" & AccountNumber & "','" & SortCode & "',0)"
					Conn.Execute (SQL)
				elseif Request.Form("sel_DIRECTPOSTITEMS") = 11 then
					'insert the data into the database immediately for processing via bacs, etc...
					SQL = "INSERT INTO F_TENANTREFUNDS (JOURNALID, BANKDETAILSID, CUSTOMERID, TENANCYID, ACCOUNTNAME, ACCOUNTNUMBER, SORTCODE, STATUS) " &_
							"VALUES (" & NEWID & ",NULL, " & attach_id & "," & Request.Form("hid_TENANTNUMBER") & ",NULL," &_
							" NULL,NULL,0)"
					Conn.Execute (SQL)
				end if
			end if
						
		End If
		
		CloseRs(rsSet)
		
		// if redirection is chosen
		if redir = 1 Then
	
			' get customer id to enable redirection to customer page
			strSQL = "SELECT CUSTOMERID FROM C_CUSTOMERTENANCY WHERE TENANCYID = " & Request.Form("hid_TENANTNUMBER")
			Call OpenRs(rsSet, strSQL)
			if not rsSet.EOF then str_redirection = rsSet(0) end if
			CloseRs(rsSet)
			
			// REDIRECT
			str_redirection = "../../customer/CRM.asp?CustomerID=" & str_redirection
			
		End If
		
	End Function
	
	// change the status of the attached journal entry
	Function EnterUniversalCredit(NewJournalid)
	
		'SQL = "dbo.[F_DIRECTPOSTING_UNIVERSALCREDIT] @Journalid = " & NewJournalid 
		'Conn.Execute(SQL)
		
	End Function

    // reverse Universal Credit journal entry
	Function change_entry_status(int_f_status)
	
		SQL = "UPDATE F_RENTJOURNAL SET STATUSID = " & int_f_status & " WHERE JOURNALID = " & attach_id
		Conn.Execute(SQL)
		
	End Function
	
%>
<html>
<head></head>
<script language=javascript>
    function ReturnData() {
<% if ErrorText = "" then %>	
	// if actual posting
	<% if str_data = "" then %>	
		// redirect to CRM if required
		if (<%=redir %> != 0){
            parent.location.href = "<%=str_redirection%>"
        }
        parent.details.innerHTML = "&nbsp;";
        parent.ACCOUNT_DIV.innerHTML = "";
        parent.RSLFORM.reset();
        parent.RSLFORM.Detach.style.visibility = "hidden"
        parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
        parent.RSLFORM.btn_POST.disabled = true;
        parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Successful...</B></FONT>";
	<% else %>
            // otherwise tenant data retrieval
            parent.ATTACHID = 0;
        parent.details.innerHTML = "<%=str_data%>";
        parent.RSLFORM.sel_DIRECTPOSTITEMS.value = "";
        parent.RSLFORM.txt_AMOUNT.value = "";
        parent.RSLFORM.hid_TENANTNUMBER.value = "<%=tnum%>";
        parent.STATUS_DIV.style.visibility = "hidden";
        parent.is_a_valid_tenancy = <%=isValid %>;
        if (<%=isValid %> == 1){
            parent.RSLFORM.btn_POST.disabled = false;
            parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;
        }
		else {
            parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
            parent.RSLFORM.btn_POST.disabled = true;
        }
	<% end if %>
<% else %>
            alert("<%=ErrorText%>");
        parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Try again in a few seconds...</B></FONT>";
        parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;
        parent.RSLFORM.btn_POST.disabled = false;		
<% end if %>	
	}
</script>
<body onload="ReturnData()">
</body>
</html>