<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim SchemeID, strWhat, count, optionvalues, optiontext, intReturn, theamounts, Company

	strWhat = Request.form("IACTION")
	If strWhat = "getBlock" Then rePop() End If

	Function rePop()
		SchemeID = -1
		If(Request.Form("sel_SCHEME") <> "") Then SchemeID = Cint(Request.Form("sel_SCHEME")) End If
        IsAllSchemeSelected = 0
		If (SchemeID = -1) Then
		    IsAllSchemeSelected = 1
        Else
            SQL = "SELECT BLOCKID, BLOCKNAME FROM P_BLOCK " &_
			  "WHERE SchemeId = " & SchemeID    
		End If

        If(IsAllSchemeSelected = 1)Then
            optionvalues = "-1"
			optiontext = "All Blocks"
        Else
        
		    Call OpenDB()
		    Call OpenRs(rsBlock, SQL)
            'Response.Write SQL
            'Response.End()
            'Response.Flush()
		    count = 0
		    theamounts = "0"

            If(NOT rsBlock.EOF) Then
                optionvalues = "-1"
		        optiontext = "All Blocks"
		        count = count + 1
            End If

		    While (NOT rsBlock.EOF)
			    theText = rsBlock.Fields.Item("BLOCKNAME").Value
			    theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			    optionvalues = optionvalues & ";;" & rsBlock.Fields.Item("BLOCKID").Value
			    optiontext = optiontext & ";;" & theText

			    count = count + 1
			    rsBlock.MoveNext()
		    Wend
		    If (rsBlock.CursorType > 0) Then
		      rsBlock.MoveFirst
		    Else
		      rsBlock.Requery
		    End If
		    If (count = 0) Then
			    optionvalues = "0"
			    optiontext = "No Blocks Are Setup..."
		    End If

		    Call CloseRs(rsBlock)
		    Call CloseDB()
        End If
	End Function

%>
<html>
<head>
    <title>Block Data Fetcher</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
    <script type="text/javascript" language="JavaScript">
        function loaded() {
            parent.loadPropertiesData("<%=optionvalues%>", "<%=optiontext%>", 3);
        }
    </script>
</body>
</html>
