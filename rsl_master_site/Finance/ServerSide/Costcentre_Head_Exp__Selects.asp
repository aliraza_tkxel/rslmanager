<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CostCentreID, strWhat, count, optionvalues, optiontext, intReturn 
	Dim theamounts, Expvalues, Exptext, HeadValues, HeadText , CCValues, CCText
	
    Function rePopCCbyCompany()
		CompanyId = -1
		if(Request("sel_COMPANY") <> "") then CompanyId = Request("sel_COMPANY")
		
		if (CompanyId = -1) then
			optionvalues = ""
			optiontext = "Please Select a Company..."			
			Exit Function
		end if
		
		SQL = "SELECT DISTINCT COSTCENTREID, DESCRIPTION FROM F_COSTCENTRE cc " &_ 
			"WHERE cc.companyid = " & CompanyId & " ORDER BY cc.DESCRIPTION ASC "
		rw sql
		Call OpenRs(rsCC, SQL)
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		While (NOT rsCC.EOF)
			theText = rsCC.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsCC.Fields.Item("COSTCENTREID").Value
			optiontext = optiontext & ";;" & theText
			
			  count = count + 1
			  rsCC.MoveNext()
		Wend
		If (rsCC.CursorType > 0) Then
		  rsCC.MoveFirst
		Else
		  rsCC.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No CC Are Setup..."
		End If
		response.write optiontext
		' These are the values  to build the selects
		CCValues = optionvalues
		CCText = optiontext
		
		CloseRs(rsCC)

	End Function

	Function rePopHead()
		CostCentreID = -1
		if(Request("sel_COSTCENTRE") <> "") then CostCentreID = Request("sel_COSTCENTRE")
		rw CostCentreID
		if (CostCentreID = -1) then
			optionvalues = ""
			optiontext = "Please Select a Cost Centre..."			
			Exit Function
		end if
		
		Call GetCurrentYear()
		FY = GetCurrent_YRange

		SQL = "SELECT DISTINCT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 " &_ 
			"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			"WHERE HE.CostCentreID = " & CostCentreID & " ORDER BY HE.DESCRIPTION ASC "
		rw sql
		Call OpenRs(rsHead, SQL)
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsHead.Fields.Item("HEADID").Value
			optiontext = optiontext & ";;" & theText
			
			  count = count + 1
			  rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Heads Are Setup..."
		End If
		' These are the values  to build the selects
		HeadValues = optionvalues
		HeadText = optiontext
		
		CloseRs(rsHead)

	End Function
	
	Function rePopExpenditure()
		HeadID = -1
		if(Request("sel_HEAD") <> "") then HeadID = Request("sel_HEAD")

		if (HeadID = -1) then
			optionvalues = ""
			optiontext = "Select an Expenditure..."			
			Exit Function
		end if
		
		Call GetCurrentYear()
		FY = GetCurrent_YRange
		
			SQL = "SELECT DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (isNull(EXA.EXPENDITUREALLOCATION,0) - isNull(GROSSCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.ACTIVE = 1 AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) " &_
				"INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = EX.QBDEBITCODE " &_	
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_				
				"LEFT JOIN ( "&_
					"SELECT SUM(GROSSCOST) AS GROSSCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEITEM WHERE ACTIVE = 1 "&_
					"AND PIDATE >= '" & FormatDateTime(GetCurrent_StartDate,1) & "' AND PIDATE <= '" & FormatDateTime(GetCurrent_EndDate,1) & "' " &_
					"GROUP BY EXPENDITUREID) PI on PI.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID 

		SQL = "SELECT DISTINCT EX.EXPENDITUREID,EX.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
			"WHERE HE.ACTIVE = 1 AND EX.ACTIVE = 1 AND HE.HEADID = " & HeadID
		
		Call OpenRs(rsExp, SQL)
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsExp.EOF)
			theText = rsExp.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExp.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			
			  count = count + 1
			  rsExp.MoveNext()
		Wend
		If (rsExp.CursorType > 0) Then
		  rsExp.MoveFirst
		Else
		  rsExp.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures Are Setup..."
		End If
		
		' These are the values  to build the selects
		Expvalues = optionvalues
		Exptext = optiontext
		
		CloseRs(rsExp)

	End Function

	
OpenDB()
TheAction = Request("hid_selID")
Select Case TheAction
	Case "1"   rePopHead()	
	Case "2"   rePopExpenditure()
    Case "3"   rePopCCbyCompany()
End Select
CloseDB()

//rw  Expvalues & "<br><br>"
//rw  Exptext & "<br><br>"
%>
<html>
<head>
<title>Head Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>


<script language=javascript>

    function loaded() {
        if ('<%=TheAction%>' == '1') {
            parent.PopulateListBox("<%=Headvalues%>", "<%=Headtext%>", 1);
        }
        if ('<%=TheAction%>' == '2') {
            parent.PopulateListBox("<%=Expvalues%>", "<%=Exptext%>", 2);
        }
        if ('<%=TheAction%>' == '3') {
            parent.PopulateListBox("<%=CCvalues%>", "<%=CCtext%>", 3);
        }
    }

</script>

</body>
</html>

