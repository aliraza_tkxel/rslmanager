<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim credit_list, credit_sql, CreditCount, arrCredit
	credit_list = Request("hid_CreditList")
   
    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "

	' GET NUMBER OF CREDITS
	arrCredit = Split(credit_list, ",")
	CreditCount = UBound(arrCredit)
	
'	IF ANY CREDITS HAVE BEEN SELECTED WE NEED TO INCLUDE THEM IN ALL PROCESSES EXCEPT THE ACTUAL BACS FILE CREATION
	if credit_list = "" Then
		credit_sql = " AND CP.ORDERITEMID IS NULL "
	Else
		credit_sql = "OR (INV.INVOICEID IN (" & credit_list & ")) "
	End If
	
	if (Request("pROCESSINGdATE") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("pROCESSINGdATE")),2)
		RequiredDate = CDate(Request("pROCESSINGdATE"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if
	
	OpenDB()
	'THIS STATEMENT WILL LOCK RECONCILING FOR A MOMENT....
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 1 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
	Conn.Execute (SQL)
	
	DataDirtyFlag = 0
	
	For each ORGID in Request.Form("CHECKITEMS")
		ChequeNumber = Request.Form("txt_TEXT" & ORGID)
	
		'NEED TO CHECK THAT THE DATA HAS NOT CHANGED SINCE IT WAS SENT OVER...
		'the CHECK IS PER ORGANISATION AS THIS IS HOW THE CHEQUE WILL BE SENT.
		SQL = "SELECT ISNULL(SUM(GROSSCOST),0) AS TOTALCOST, COUNT(INV.INVOICEID) AS TOTALCOUNT " &_
				"FROM F_INVOICE INV " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
				"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
				"	WHERE INV.ISCREDIT = 0 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 8 AND " &_
				"	INV.SUPPLIERID = " & ORGID & " AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), INV.TAXDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " & CompanyFilter
		RW SQL	& "<BR><BR>"
		Call OpenRs(rsCheck, SQL)
		TotalSum = 0
		TotalCount = 0
		if (NOT rsCheck.EOF) then
			TotalSum = rsCheck("TOTALCOST")
			TotalCount = rsCheck("TOTALCOUNT")
		end if
		Call CloseRs(rsCheck)
		rw CDbl(TotalSum) & " = " & Cdbl(Request("hid_VA" & ORGID)) & "<BR>"
		rw CInt(TotalCount) & " = " & CInt(Request("hid_CO" & ORGID))& "<BR><BR>"
		'THIS pART CHECKS THE DATA AND REDIRECTS TO THE PREVIOUS PAGE IF IT IS NOT MATCHING

		IF ( CDbl(TotalSum) = Cdbl(Request("hid_VA" & ORGID)) AND CInt(TotalCount) = CInt(Request("hid_CO" & ORGID)) ) then
	
			'ADD A ROW INTO THE DATABASE SO THE INFORMATION IS RETRIEVAL LATER ON
			SQL = ("SET NOCOUNT ON;INSERT INTO F_BACSDATA (INVOICECOUNT, TOTALVALUE, PROCESSDATE, ISFILE, CompanyId) VALUES (" &_
						"" & Request("hid_CO" & ORGID) + (CreditCount+1) & ", " &_				
						"" & Request("hid_VA" & ORGID) + get_credits(ORGID) & ", " &_
						"'" & FormatDateTime(Date,1) & "', 0, " & Company & ");SELECT SCOPE_IDENTITY() AS DATAID ")
			RW SQL	& "<BR><BR>"
			Call OpenRs(rsDATA, SQL)
			DATAID = rsDATA("DATAID")
			Call CloseRs(rsDATA)
	
			'THIS STATEMENT WILL INSERT N INVOICE ENTRIES INTO F_POBACS.
			SQL = "INSERT INTO F_POBACS (INVOICEID, DATAID, PROCESSEDBY, PROCESSDATE, CHEQUENUMBER) " &_
					"SELECT INV.INVOICEID, " & DATAID & ", " & SESSION("USERID") & ", '" & FormatDateTime(Date,1) & "', '" & ChequeNumber & "' FROM F_INVOICE INV " &_
						"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
						"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
						"LEFT JOIN F_ORDERITEM_TO_INVOICE OI ON OI.INVOICEID =INV.INVOICEID "&_
	                    "LEFT JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.ORDERITEMID =OI.ORDERITEMID "&_				
						"WHERE (INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 8 AND " &_
						"	DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), INV.TAXDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
						"	AND ORGID = " & ORGID & " ) " & credit_sql & CompanyFilter
			Response.Write SQL & "<BR><BR>"
			Conn.Execute (SQL)
	
			'THIS STATEMENT WILL UPDATE THE RESPECTIVE PURCHASE ITEMS AND CHANGE THEIR STATUS TO DD
			SQL = "UPDATE F_PURCHASEITEM SET PISTATUS = 10, PAIDON = '" & FormatDateTime(Date,1) & "' " &_
						"FROM F_PURCHASEITEM PI " &_
						"INNER JOIN F_ORDERITEM_TO_INVOICE FITI ON FITI.ORDERITEMID = PI.ORDERITEMID " &_
						"INNER JOIN F_INVOICE INV ON INV.INVOICEID = FITI.INVOICEID " &_
						"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
						"WHERE PB.DATAID = " & DATAID & CompanyFilter	
			Conn.Execute (SQL)
			RW SQL	& "<BR><BR>"		
			'THIS STATEMENT WILL UPDATE THE INVOICE DATA 
			SQL = "UPDATE F_INVOICE SET PAYMENTMETHOD = 8 " &_
						"FROM F_INVOICE INV " &_
						"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
						"WHERE PB.DATAID = " & DATAID & CompanyFilter			
			Conn.Execute (SQL)
			rw SQL & "<BR><BR>"
			Conn.Execute "EXEC UPDATE_PURCHASEORDER_STATUS " & DATAID		
			Conn.Execute "EXEC UPDATE_CREDITNOTE_STATUS " & DATAID
			Conn.Execute "EXEC NL_INVOICE_PAYMENTS " & DATAID				
	
		Else
			DataDirtyFlag = 1
		End If
	
	next
	
	'THIS STATEMENT WILL UNLOCK RECONCILING
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'RECONCILELOCKED'"
	Conn.Execute (SQL)
	rw "DIRTYFLAG = " & DataDirtyFlag
	CloseDB()
	IF (DataDirtyFlag = 1) THEN
		Response.Redirect "../BacsCheque.asp?PODate=" & ProcessingDate & "&ER14" & Replace(Date, "/", "") & "=1" & "&Company=" & Company
	ELSE
		Response.Redirect "../BacsDisplay.asp?PODate=" & ProcessingDate & "&Company=" & Company
	End if
	
	Function get_credits(org)
		if credit_list <> "" Then
			Dim sum
			SQL = "SELECT ISNULL(SUM(GROSSCOST),0) AS CREDITS FROM F_INVOICE INV " &_
					" WHERE	SUPPLIERID = "&org&" AND ISCREDIT = 1 AND INVOICEID IN ("&credit_list&") " & CompanyFilter

			RW SQL	& "<BR><BR>"
			Call OpenRs(rsSet, SQL)
			If Not rsSet.EOF Then sum = rsSet("CREDITS") Else sum = 0 End If
			CloseRs(rsSet)
			get_credits = sum
		Else	
			get_credits = sum
		End If
	End Function

%>