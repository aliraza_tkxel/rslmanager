	<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
TeamID = CStr(Request.Form("sel_TEAMS"))
if (TeamID = "") then
	TeamID = "-1"
end if

	IF (TeamID = "NOT") THEN
		SQL = "SELECT J.ACTIVE, E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
				" WHERE TEAM IS NULL ORDER BY FIRSTNAME, LASTNAME"
	ELSE
		SQL = "SELECT J.ACTIVE, E.EMPLOYEEID, FIRSTNAME + ' ' + LASTNAME AS FULLNAME FROM E__EMPLOYEE E, E_JOBDETAILS J, E_TEAM T " &_
				" WHERE E.EMPLOYEEID = J.EMPLOYEEID AND J.TEAM = T.TEAMID AND T.TEAMID = " & TeamID & " ORDER BY FIRSTNAME, LASTNAME"
	END IF
	
	Dim rsEmployees
	OpenDB()
	Call OpenRs(rsEmployees, SQL)
	if (not rsEmployees.EOF) then
		EmployeeList = "<option value=''>Please Select</option>"
		while not rsEmployees.EOF
			isActive = rsEmployees("ACTIVE")
			if (isActive = 1) then
				EmployeeList = EmployeeList & "<option value='" & rsEmployees("EMPLOYEEID") & "'>" & rsEmployees("FULLNAME")& "</option>"
			else
				EmployeeList = EmployeeList & "<option style='background-color:#FF6666;color:white' value='" & rsEmployees("EMPLOYEEID") & "'>" & rsEmployees("FULLNAME")& "</option>"
			end if			
			rsEmployees.moveNext
		wend
	else
		EmployeeList = "<option value=''>No employees found...</option>"
	end if
	CloseRs(rsEmployees)
	CloseDB()

%>			
<html>
<head>
<title>Server Employee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<SCRIPT LANGUAGE="JAVASCRIPT">
function SetEmployees(){
	parent.THISFORM.sel_EMPLOYEE.outerHTML = Employees.innerHTML
	}
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000" onload="SetEmployees()">
<div id="Employees">
<select name='sel_EMPLOYEE' class='textbox200'>
<%=EmployeeList%>
</select>
</div>
</body>
</html>
