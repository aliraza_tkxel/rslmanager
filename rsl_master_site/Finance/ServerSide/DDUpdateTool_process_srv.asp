<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% 
''''''''''''
'' PAGE NOTE: this updates the dd to customers accounts
''
'' NOTE: Pages to be aware of: DDUpdateTOOL.asp 
''							   DDUpdateTOOL_srv.asp 
''''''''''''

bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	Dim insert_list  
	Dim sel_split
	
	insert_list = Request.Form("idlist")

	response.write "idlist : " & insert_list & "<BR><BR>"
		
	OpenDB()
	
	update_dd()
	
	CloseDB()
	
	// This function Adds a new History item to c_repair and updates the c_journal
 	Function update_dd()
		
		sel_split = Split(insert_list ,",") ' split selected string
		
		For each key in sel_split
			
				
			'UPDATE The Customers DD
			strSQL = "EXEC F_DDACCOUNT_UPDATE " & clng(key)
			Conn.Execute(strSQL)
			'response.write "update SQL: " & strSQL & "<BR><BR>"
			
		Next
	
	End Function
	
	quicktenancyfind = Request("quicktenancyfind")
	AssetFilter = Request("sel_ASSETS")
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	location.href = "DDUpdateTOOL_srv.asp?quicktenancyfind=<%=quicktenancyfind%>&AssetFilter=<%=AssetFilter%>"
	
	}
</script>
<body onLoad="ReturnData()">
	


</body>
</html>