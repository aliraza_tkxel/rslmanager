<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, intReturn, theamounts, EmployeeLimits, Budget_Remaining
	
	strWhat = Request.form("whatAction")

	'Default to '0' just incase no items are returned...
	Budget_Remaining = 0

	If strWhat = "change" Then rePop() Else writeRecord() End If

	Function rePop()
		HeadID = -1
		if(Request.Form("sel_HeadID") <> "") then HeadID = Request.Form("sel_HeadID")		

		if (HeadID = -1) then
			optionvalues = ""
			theamounts = "0"
			EmployeeLimits = "0"			
			optiontext = "Please Select a Head..."			
			Exit Function
		end if
		
		OpenDB()
		SQL = "SELECT DESCRIPTION, ISNULL(EL.LIMIT,0) AS LIMIT, EX.EXPENDITUREID, (isNull(EXPENDITUREALLOCATION,0) - isNull(QUOTEDCOST,0)) as REMAINING "&_
				"FROM F_EXPENDITURE EX " &_
				"LEFT JOIN F_EMPLOYEELIMITS EL ON EX.EXPENDITUREID = EL.EXPENDITUREID AND EL.EMPLOYEEID = " & Session("USERID") & " " &_				
				"left JOIN ( "&_
					"SELECT SUM(QUOTEDCOST) as QUOTEDCOST, EXPENDITUREID "&_
					"FROM F_PURCHASEORDER where F_PURCHASEORDER.ACTIVE = 1 "&_
					"GROUP BY EXPENDITUREID) PO on PO.EXPENDITUREID = EX.EXPENDITUREID "&_
				"WHERE (HEADID = " & HeadID & ") "&_
				"AND EX.ACTIVE = 1 "&_
				"ORDER BY DESCRIPTION"
				
		Call OpenRs(rsExpenditure, SQL)		
		
		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		EmployeeLimits = "0"		
		While (NOT rsExpenditure.EOF)
			theText = rsExpenditure.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)
			
			optionvalues = optionvalues & ";;" & rsExpenditure.Fields.Item("EXPENDITUREID").Value
			optiontext = optiontext & ";;" & theText
			theamounts = theamounts & ";;" & rsExpenditure.Fields.Item("REMAINING").Value
			EmployeeLimits = EmployeeLimits & ";;" & rsExpenditure.Fields.Item("LIMIT").Value			
			
			  count = count + 1
			  rsExpenditure.MoveNext()
		Wend
		If (rsExpenditure.CursorType > 0) Then
		  rsExpenditure.MoveFirst
		Else
		  rsExpenditure.Requery
		End If
		if (count = 0) Then
			optionvalues = ""
			optiontext = "No Expenditures are Setup..."
			theamounts = "0"
			EmployeeLimits = "0"
		End If

		SQLSPENT = "SELECT ISNULL(SUM(QUOTEDCOST),0) AS SPENT FROM F_PURCHASEORDER WHERE ACTIVE = 1 AND HEADID = " & HeadID & " AND EXPENDITUREID IS NULL "
		SQLALLOC = "SELECT ISNULL(ISNULL(HEADALLOCATION,0) - ISNULL(EXPTOTAL,0),0) AS TOTAL FROM F_HEAD H " &_
				   "LEFT JOIN (SELECT HEADID, SUM(EXPENDITUREALLOCATION) AS EXPTOTAL FROM F_EXPENDITURE GROUP BY HEADID) E ON H.HEADID = E.HEADID " &_
				   "WHERE H.HEADID = " & HeadID & " "
		
		Call OpenRS(rsSpent, SQLSPENT)
		Call OpenRS(rsAlloc, SQLALLOC)
		TOTAL = rsAlloc("TOTAL")
		SPENT = rsSpent("SPENT")
		Budget_Remaining = TOTAL - SPENT
		CloseRs(rsSpent)
		CloseRs(rsAlloc)	
		
		CloseRs(rsExpenditure)
		CloseDB()		
	End Function
	
%>

<html>
<head>
<title>Budget Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000" onload=loaded()>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>

	function loaded(){
		if ('<%=strWhat%>' == 'change'){
			parent.populatelistbox("<%=optionvalues%>", "<%=optiontext%>");
			//parent.RSLFORM.theremainingamounts.value = "<%=theamounts%>";
			//parent.RSLFORM.theEmployeeLimits.value = "<%=EmployeeLimits%>";			
			//parent.RSLFORM.maxValueHead.value = FormatCurrency(<%=Budget_Remaining%>);
			}
		}
		
</script>

</body>
</html>

