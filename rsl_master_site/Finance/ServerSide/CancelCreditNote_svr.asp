<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Const TRANSACTIONTYPE = 23 ' CANCEL CREDIT NOTE
	Dim CNID, ISERROR, TXNID, SEQ
	
	SEQ = 1
	CNID = -1
	If Request("hid_CNID") <> "" Then CNID = Request("hid_CNID") Else ISERROR = 1 End If
	
	Call OpenDB()
	If Not ISERROR = 1 Then Call CancelCreditNote() End If
	Call CloseDB()	
	
	' THIS FUNCTION GETS THE ORIGINAL NOMINAL ENTRY AND USES THE DETAILS FROM THIS TO CREATE CANCELLATION
	' ENTRIES IN THE NOMINAL 
	Function CancelCreditNote()
		
		Dim TXNID 
		
		' CREATE TOPLEVEL ENTRY AND CREDIT LINE ENTRY
		SQL = " SELECT 	J.TXNID, D.ACCOUNTID, D.AMOUNT " &_
				" FROM 	NL_JOURNALENTRY J " &_
				"		INNER JOIN NL_JOURNALENTRYDEBITLINE D ON D.TXNID = J.TXNID " &_
				" WHERE	J.TRANSACTIONTYPE = 20 AND J.TRANSACTIONID = " & CNID

		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then 
			TXNID = enter_journal() 
			Call enter_journal_CR(TXNID, rsSet("ACCOUNTID"), SEQ, rsSet("AMOUNT"))
		Else 
			ISERROR = 1 
		End If
		Call CloseRs(rsSet)
		
		' CREATE (N) NUMBER OF DEBIT ENTRIES BASED ON NUMBER OF ROWS RETURNED BY RECORDSET
		SQL = "SELECT 	J.TXNID, C.ACCOUNTID, C.AMOUNT, TRACKERID " &_
				"FROM 	NL_JOURNALENTRY J " &_
				"		INNER JOIN NL_JOURNALENTRYCREDITLINE C ON C.TXNID = J.TXNID " &_
				"WHERE	J.TRANSACTIONTYPE = 20 AND J.TRANSACTIONID = " & CNID
		
		Call OpenRs(rsSet, SQL)
		If Not ISERROR = 1 Then
			While Not rsSet.EOF 
				SEQ = SEQ + 1
				Call enter_journal_DB(TXNID, rsSet("ACCOUNTID"), SEQ, rsSet("AMOUNT"), rsSet("TRACKERID"))	
				rsSet.movenext()
			Wend
		End If
		CloseRs(rsSet)
		Call updateStatii()

	End Function
	
	Function updateStatii()
	
		' UPDATE CREDIT NOTE STATUS
		DS=Request.Form("DSUserName_Reason")
		DS=Split(DS,",")
		data=Split(DS(0),"|")

		SQL = "UPDATE	F_CREDITNOTE SET ACTIVE = 0, CNSTATUS = 16, Reason='"& data(0) & "',CancelBy="& data(1) &",CancelDate=getdate() "&_
		"WHERE	CNID = " & CNID
		Conn.Execute(SQL)

		'CANNOT DELETE INVOICES FIRST BECUASE OF CONSTRAINT,
		'THEREFORE STORE THE IDS, FOR DELETTION LATER
		DeleteList = "-99" ' DUMMY INVOICEID
		'STORE INVOICE ROWS
		SQL = "SELECT INV.INVOICEID " &_
				"FROM	F_CREDITNOTE C " &_
				"		INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"		INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = CP.ORDERITEMID " &_
				"		INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = P.ORDERITEMID " &_
				"		INNER JOIN F_INVOICE INV ON INV.INVOICEID = FOTI.INVOICEID " &_
				"WHERE	C.CNID = " & CNID
		Call OpenRs(rsINV, SQL)
		while NOT rsINV.EOF 
			DeleteList = DeleteList & "," & rsINV("INVOICEID")
			rsINV.moveNext
		wend
		Call CloseRs(rsINV)
		
		' DELETE LINKAGE ROWS
		SQL = "DELETE FROM F_ORDERITEM_TO_INVOICE " &_
				"FROM	F_CREDITNOTE C " &_
				"		INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"		INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = CP.ORDERITEMID " &_
				"		INNER JOIN F_ORDERITEM_TO_INVOICE FOTI ON FOTI.ORDERITEMID = P.ORDERITEMID " &_
				"WHERE	C.CNID = " & CNID
		Conn.Execute(SQL)

		' DELETE INVOICE ROWS USING IDS STORED PREVIOUSLY
		SQL = "DELETE FROM F_INVOICE WHERE INVOICEID IN (" & DeleteList & ")"
		Conn.Execute(SQL)

		' UPDATE CREDIT ITEMS STATUS WITHIN PURCHASEITEMS
		SQL = "UPDATE	F_PURCHASEITEM SET PISTATUS = 16, ACTIVE = 0 " &_
				"FROM	F_CREDITNOTE C " &_
				"		INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"		INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = CP.ORDERITEMID " &_
				"WHERE	C.CNID = " & CNID
		Conn.Execute(SQL)
	
	End Function
		
	' CREATE TOP LEVEL NOMINAL ENTRY
	Function enter_journal()
	
		' CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = TRANSACTIONTYPE
		comm.parameters(2) = CLng(CNID)
		comm.parameters(3) = now 
		comm.parameters(4) = FormatDateTime(date)
		comm.parameters(5) = null
		comm.parameters(6) = null		
		comm.execute
		txnid = comm.parameters(0)
		Set comm = Nothing
		enter_journal = txnid
		
	End Function
	
	' CREATE DEBIT ENTRY
	Function enter_journal_DB(TID, ACCOUNT, SEQ, AMOUNT, TRACKER)

' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYDEBITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext   = "F_INSERT_NL_JOURNALENTRYDEBITLINE"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = CLng(TID)
		comm.parameters(2) = CInt(ACCOUNT)
		comm.parameters(3) = now 
		comm.parameters(4) = CInt(SEQ)
		comm.parameters(5) = FormatDateTime(DATE)
		comm.parameters(6) = AMOUNT
		comm.parameters(7) = "Cancellation for CN " & Right(("000000" & CStr(CNID)),7)
		comm.parameters(8) = null
		comm.parameters(9) = TRACKER
		comm.execute
		Set comm = Nothing
		
	End Function
	 
	' CREATE CREDIT ENTRY
	Function enter_journal_CR(TID, ACCOUNT, SEQ, AMOUNT)
	
' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYDEBITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext   = "F_INSERT_NL_JOURNALENTRYCREDITLINE"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = CLng(TID)
		comm.parameters(2) = CInt(ACCOUNT)
		comm.parameters(3) = now 
		comm.parameters(4) = Cint(editseq)
		comm.parameters(5) = FormatDateTime(date)
		comm.parameters(6) = AMOUNT
		comm.parameters(7) = "Cancellation for CN " & Right(("000000" & CStr(CNID)),7)
		comm.parameters(8) = NULL
		comm.parameters(9) = NULL
		comm.parameters(10) = NULL
		comm.parameters(11) = NULL
		comm.parameters(12) = NULL
		comm.parameters(13) = NULL
		comm.parameters(14) = NULL
		comm.parameters(15) = NULL
		
		comm.execute
		Set comm = Nothing
		
	End Function

	Response.Redirect ("../CreditNoteList.asp")
%>