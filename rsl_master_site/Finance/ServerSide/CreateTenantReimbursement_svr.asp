<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	Expenditure = Request.Form("sel_EXPENDITURE")
	Desc = Replace(Request.Form("txt_ITEMDESC"),"'","''")
	ChequeDate = Request.Form("txt_DATE")
	GrossCost = Request.Form("txt_GROSSCOST")
	Cheque =  Replace(Request.Form("txt_CHEQUE"),"'","''")
	Customer = Request.Form("sel_CUSTOMER")				
	Tenant = Request.Form("txt_TENANCY")

	Call OpenDB()
	
	SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)" &_
	"VALUES (" & Expenditure & ", 'Cheque: " & Cheque & ", " & Left(Desc,150) & "', '" & Desc & "', '" & FormatDateTime(ChequeDate,1) & "', " & GrossCost & ", " & Session("USERID") & ", 1, 5, 10);" &_
	"SELECT SCOPE_IDENTITY() AS ORDERITEMID"

	Call OpenRs(rsIn, SQL)	
	ORDERITEMID = rsIn("ORDERITEMID")

	SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM_CHQ (CHQNUMBER, PAYDATE, TENANCY_ID, CUSTOMER_ID) " &_
 	"VALUES ('" & Cheque & "', '" & FormatDateTime(ChequeDate,1) & "', " & Tenant & ", " & Customer & ");" &_
	"SELECT SCOPE_IDENTITY() AS CHQID"
	
	Call OpenRs(rsIn, SQL)
	CHQID = rsIn("CHQID")

	SQL = "INSERT INTO F_PURCHASEITEM_TO_CHQ (ORDERITEMID, CHQID)" &_
	"VALUES (" & ORDERITEMID & ", " & CHQID & ")"
	Conn.Execute SQL	
	
	SQL = "EXEC NL_TENANTREIMBURSEMENT " & ORDERITEMID
	Conn.Execute SQL		
	
	Response.Redirect "../TenantReimbursements.asp"
%>

