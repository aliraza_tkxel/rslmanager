<%@  language="VBSCRIPT" codepage="1252" %>
<% 'ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	if (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then 'from reconcile page
		LOGPAGETEXT = "(RECONCILE PAGE)"
	else
		LOGPAGETEXT = "(AMEND PAGE)"
	end if
	
	ORDERITEMID = REQUEST("ORDERITEMID")
	ORDERID = REQUEST("ORDERID")
	BATCHID= REQUEST("BatchID")
		
	ItemRef = Replace(Request("txt_ITEMREF"), "'", "''")
	ItemDesc = Replace(Request("txt_ITEMDESC"), "'", "''")
	VatTypeID = Request("POVatType")
	NetCost = Request("txt_NETCOST")
	VAT = Request("txt_VAT")
	GrossCost = Request("txt_GROSSCOST")
	ExpenditureID = Request("sel_EXPENDITURE")

    SchemeId = Request("sel_SCHEME")
    BlockId = Request("sel_BLOCK")

	UserSessionID = SESSION("USERID")
	OpenDB()
    
	'INSERT THE PREVIOUS DATA INTO THE HISTORY TABLE.
	SQL = "INSERT INTO F_PURCHASEITEM_HISTORY (ORDERID,ORDERITEMID,PISTATUS,IsAmend,PIDATE,EXPENDITUREID, VAT, VATTYPE, NETCOST, GROSSCOST,USERID,HISTORY_TIMESTAMP ) " &_
			"SELECT ORDERID,ORDERITEMID,PISTATUS,1,PIDATE, EXPENDITUREID, VAT, VATTYPE, NETCOST, GROSSCOST, " & UserSessionID & ", GETDATE() " &_
			" FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
			
	'rw sql & "<BR>"
	Conn.Execute SQL
	
	' I have assigned a new histroical item when the order has been amended
	' this tells the users who has amended the order
	SQL = " INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
			"					SELECT J.JOURNALID, CURRENTITEMSTATUSID, 13 ," & UserSessionID & ",  R.ITEMDETAILID, R.CONTRACTORID ,R.TITLE,'Applied in Purchase Amend Tool by " & Replace(Session("FirstName"),"'"," ") & " " & Replace(Session("LastName"),"'"," ") & " : " & ItemDesc & "' " &_
			"					FROM P_WOTOREPAIR WOTO " &_
			"						INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
			"						INNER JOIN C_REPAIR R ON R.JOURNALID = WOTO.JOURNALID " &_
			"					WHERE 	REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = R.JOURNALID) " &_
			"						AND WOTO.ORDERITEMID = " & ORDERITEMID 
	'rw sql & "<BR>"
	
	Conn.Execute SQL

	Call LOG_PI_ACTION(ORDERITEMID, "AMENDED FROM FINANCE " & LOGPAGETEXT)			
			
	'UPDATE THE ACTUAL PURCHASE ITEM		
	SQL = "UPDATE F_PURCHASEITEM SET EXPENDITUREID = " & ExpenditureID & ", ITEMNAME = '" & ItemRef & "', " &_
			" ITEMDESC = '" & ItemDesc & "', NETCOST = " & NetCost & ", VATTYPE = " & VatTypeID & ", " &_
			" VAT = " & VAT & ", GROSSCOST = " & GrossCost & " WHERE ORDERITEMID = " & ORDERITEMID
	'rw sql & "<BR>"
	Conn.Execute (SQL)
	
	'UPDATE ACCOUNT INFO IN NL_JOURNALENTRYDEBITLINE AND NL_JOURNALENTRYCREDITLINE IF ITEM HAS BEEN RECONCILED 		
	SQL = " UPDATE  NL_JOURNALENTRYDEBITLINE " &_
		  "	SET		NL_JOURNALENTRYDEBITLINE.ACCOUNTID = AC.ACCOUNTID " &_
		  "	FROM	NL_JOURNALENTRYDEBITLINE " &_
		  "			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = NL_JOURNALENTRYDEBITLINE.TRACKERID " &_
		  "			INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		  "			LEFT JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE  " &_
		  "			LEFT JOIN NL_ACCOUNT BEF_AC ON NL_JOURNALENTRYDEBITLINE.ACCOUNTID = BEF_AC.ACCOUNTID " &_
		  "			WHERE   NL_JOURNALENTRYDEBITLINE.ACCOUNTID <> AC.ACCOUNTID AND TRACKERID =  " & ORDERITEMID
	Conn.Execute (SQL)
	
	SQL = " UPDATE  NL_JOURNALENTRYCREDITLINE " &_
		  "	SET		NL_JOURNALENTRYCREDITLINE.ACCOUNTID = AC.ACCOUNTID " &_
		  "	FROM	NL_JOURNALENTRYCREDITLINE " &_
		  "			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = NL_JOURNALENTRYCREDITLINE.TRACKERID " &_
		  "			INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		  "			LEFT JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE  " &_
		  "			LEFT JOIN NL_ACCOUNT BEF_AC ON NL_JOURNALENTRYCREDITLINE.ACCOUNTID = BEF_AC.ACCOUNTID " &_
		  "			WHERE   NL_JOURNALENTRYCREDITLINE.ACCOUNTID <> AC.ACCOUNTID AND TRACKERID =  " & ORDERITEMID
	Conn.Execute (SQL)
	
    SQL = "UPDATE F_PURCHASEORDER SET IsAmend = 1"
    if(SchemeId <> "") then
    SQL = SQL + " ,SchemeId = " & SchemeId
    else
    SQL = SQL + " ,SchemeId = null"
    end if

    if(BlockId <> "") then
    SQL = SQL + " ,BlockId = " & BlockId
    else
    SQL = SQL + " ,BlockId = null"
    end if
    
	SQL = SQL + " WHERE ORDERID = " & ORDERID

    Conn.Execute(SQL)

	IF (CINT(REQUEST("ISQUEUED")) = 1) THEN
		Call LOG_PI_ACTION(ORDERITEMID, "AMENDED FROM FINANCE " & LOGPAGETEXT & " (QUEUED ALSO)")				
		SQL = "UPDATE F_PURCHASEITEM SET PISTATUS = 0  WHERE ORDERITEMID = " & ORDERITEMID
		'rw sql & "<BR>"
		Conn.Execute(SQL)
       
		'**********************************************************************************
		' TASK 5394
		' CHANGE THE STATUS OF PURCHASE ORDER TO QUEUED IF ANY ITEM IS AMENDED BY STAFF WITH LESS EMPLOYEE LIMITS
		' IN THIS WAY PO WILL COME AGAIN IN QUEUED PURCAHSE LIST AND WILL BE AUTHORIZED AGAIN
		' ADDED ON 5/8/2008
		' ADDED BY ADNAN MIRZA
		Call LOG_PO_ACTION(ORDERID,"AMENDED FROM FINANCE " & LOGPAGETEXT & " (QUEUED ALSO)","")	
		SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 0, IsAmend = 1 WHERE ORDERID = " & ORDERID
	    Conn.Execute(SQL)
		
		REDIRECT_FLAG=1
		'END OF COMMENTS
		'**********************************************************************************
		
		SQL = " INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
				"					SELECT J.JOURNALID, CURRENTITEMSTATUSID, 12 ," & UserSessionID & ",  R.ITEMDETAILID, R.CONTRACTORID ,R.TITLE,'Queued due to variance to order.' " &_
				"					FROM P_WOTOREPAIR WOTO " &_
				"						INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
				"						INNER JOIN C_REPAIR R ON R.JOURNALID = WOTO.JOURNALID " &_
				"					WHERE 	REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = R.JOURNALID) " &_
				"						AND WOTO.ORDERITEMID = " & ORDERITEMID 
		'rw sql & "<BR>"
		Conn.Execute SQL		
	ELSE
	
		SQL = " INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
				"					SELECT R.JOURNALID, ITEMSTATUSID, R.ITEMACTIONID ," & UserSessionID & ",  R.ITEMDETAILID, R.CONTRACTORID ,R.TITLE,' " & ItemDesc & "' " &_
				"					FROM P_WOTOREPAIR WOTO " &_
				"						INNER JOIN C_REPAIR R ON R.JOURNALID = WOTO.JOURNALID " &_
				"					WHERE 	REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = R.JOURNALID AND ITEMACTIONID NOT IN (12,13)) " &_
				"						AND WOTO.ORDERITEMID = " & ORDERITEMID 
		'rw sql & "<BR>"
		Conn.Execute SQL	
	
	END IF
	
	CloseDB()
	
	if (Request("ReturnTo") = "1") then
		REDIRECTPAGE = "/Finance/ReconcileConfirm.asp?CurrentPage=" & Request("IPAGE") & "&ORDERID=" & ORDERID
	elseif (Request("ReturnTo") = "2") then
		REDIRECTPAGE = "/Finance/GasReconcileConfirm.asp?CurrentPage=" & Request("IPAGE") & "&BatchId=" & BATCHID	
	else
	 '   IF REDIRECT_FLAG=1 THEN
	'        REDIRECTPAGE = "/Finance/PurchaseAmendList.asp"
	 ' '  ELSE
		    REDIRECTPAGE = "/Finance/PurchaseOrderAmend.asp?CurrentPage=" & Request("IPAGE") & "&ORDERITEMID=" & ORDERITEMID & "&ORDERID=" & ORDERID'
	END IF	
	
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_Parent()
        {
            parent.parentReload();
		}
    </script>
</head>
<body onload="Return_Parent();">
</body>
</html>
