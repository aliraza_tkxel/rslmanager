<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected,mypage
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split
	Dim CONST_PAGESIZE
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal = Request("detotal")
	
	
	
	if detotal = "" Then detotal = 0 end if
	selected = Request("selected") ' get selected checkboxes into an array
	sel_split = Split(selected,",")
	
	office = Request("sel_OFFICE")
	
	If Request.QueryString("page") <> "" Then intpage = Request.QueryString("page") Else intpage = 1 End If
	If office = "" Then  office = Request.QueryString("sel_OFFICE") End If
	If fromdate = "" Then  fromdate = Request("txt_FROM") End If
	If todate = "" Then todate = Request("sel_OFFICE") End If
		If Request("PAGESIZE") = "" Then CONST_PAGESIZE = 10 Else CONST_PAGESIZE = Request("PAGESIZE") End If
	'response.write(office & "----"& fromdate & "----" & todate & "=====")
' GENERATE OFFICE SQL
	if office = "" Then 
		office_sql = "" 
	else
		office_sql = " AND CP.OFFICE = " & office & " "
	end if
	build_post()
	
	Function build_post()
		

		TotalSQL = 	"SELECT 	ISNULL(SUM(CP.AMOUNT),0) AS AMOUNT " &_
					"FROM	 	F_CASHPOSTING CP " &_
					"			LEFT JOIN C__CUSTOMER C ON CP.CUSTOMERID = C.CUSTOMERID " &_
					"			LEFT JOIN G_OFFICE O ON CP.OFFICE = O.OFFICEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = CP.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = CP.ITEMTYPE " &_
					"WHERE		CP.PAYMENTSLIPNUMBER IS NULL AND CREATIONDATE >= '" & fromdate & "' AND CREATIONDATE <= '" & todate & "'" & office_sql
		Call OpenDB()
		Call OpenRs(rsTOTAL, TotalSQL)
		if (NOT rsTOTAL.EOF) then
			TotalValue = CDbl(rsTOTAL("AMOUNT"))
		else
			TotalValue = 0
		end if
		Call CloseRS(rsTOTAL)
		Call CloseDB()
		Dim strSQL
		
		 
		'mypage = Request("page")
		
	
			
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>"

		str_data = str_data & "<TR style='height:'8px'><TD></TD></TR>" &_
			"<TR><TD COLSPAN=6 align=right><b>&nbsp;Total&nbsp;: <INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_TOTAL CLASS='textbox100' VALUE=" & FormatNumber(TotalValue,2,-1,0,0) & ">&nbsp;" &_
			"&nbsp;&nbsp;&nbsp;Selected&nbsp;: <INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & ">" &_
			"&nbsp;&nbsp;&nbsp;&nbsp;Balance&nbsp;: </b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_BALANCE CLASS='textbox100' VALUE=" & FormatNumber(TotalValue - detotal,2,-1,0,0) & "></TD>" &_			
			"<td><image src='/js/FVS.gif' name='img_BLANK' width='15px' height='15px' border='0'></td></TR>"

		str_data = str_data & "<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" &_
					"<TD WIDTH=50><FONT STYLE='COLOR:WHITE'><B>Date</B></FONT></TD>" &_
					"<TD WIDTH=120><FONT STYLE='COLOR:WHITE'><B>Office</B></FONT></TD>" &_
					"<TD WIDTH=260><FONT STYLE='COLOR:WHITE'><B>Name</B></FONT></TD>" &_
					"<TD WIDTH=120><FONT STYLE='COLOR:WHITE'><B>Item</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Payment</B></FONT></TD>" &_
					"<TD WIDTH=80><FONT STYLE='COLOR:WHITE'><B>Amount</B></FONT></TD>" &_
					"<TD><FONT STYLE='COLOR:WHITE'><B>&nbsp;</B></FONT></TD>" &_
				"</TR>" &_
				"<TR><TD HEIGHT='100%'></TD></TR>" 

		
		cnt = 0

		strSQL = 	" SET CONCAT_NULL_YIELDS_NULL OFF; SELECT 	CP.CASHPOSTINGID, " &_	
					"			THENAME = CASE " &_ 
					"			WHEN CASHPOSTINGORIGIN = 1 THEN CAST(CP.TENANCYID AS VARCHAR) + ': ' + CNGV.LIST " &_
					"			WHEN CASHPOSTINGORIGIN = 2 THEN '<B>Emp</B>: ' + E.FIRSTNAME + ' ' + E.LASTNAME " &_
					"			WHEN CASHPOSTINGORIGIN = 3 THEN '<B>Sup</B>: ' + ORG.NAME " &_
					"			WHEN CASHPOSTINGORIGIN = 4 THEN '<B>S.Cus</B>: ' + SC.CONTACT " &_					
					"			END, " &_
					"			ISNULL(CONVERT(NVARCHAR, CP.CREATIONDATE, 103) ,'') AS CREATIONDATE, " &_
					"			ISNULL(O.DESCRIPTION,'') AS OFFICE, " &_
					"			ISNULL(CP.RECEIVEDFROM,'') AS RECEIVEDFROM, " &_
					"			ISNULL(I.DESCRIPTION,'') AS ITEMTYPE, " &_
					"			ISNULL(P.DESCRIPTION,'') AS PAYMENTTYPE, " &_
					"			ISNULL(CP.AMOUNT, 0) AS AMOUNT, " &_
					"			ISNULL(CP.CHQNUMBER,'') AS CHQNUMBER, " &_
					"			ISNULL(PAYMENTSLIPNUMBER,'') AS SLIPNUM " &_
					"FROM	 	F_CASHPOSTING CP " &_
					"			LEFT JOIN G_OFFICE O ON CP.OFFICE = O.OFFICEID " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = CP.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = CP.ITEMTYPE " &_
					"			LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNGV ON CNGV.I = CP.TENANCYID AND CASHPOSTINGORIGIN = 1 " &_
					"			LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = CP.TENANCYID AND CASHPOSTINGORIGIN = 2 " &_  
					"			LEFT JOIN S_ORGANISATION ORG ON ORG.ORGID = CP.TENANCYID AND CASHPOSTINGORIGIN = 3 " &_  
					"			LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = CP.TENANCYID AND CASHPOSTINGORIGIN = 4 " &_  					
					"WHERE		CP.PAYMENTSLIPNUMBER IS NULL AND cp.CREATIONDATE >= '" & fromdate & "' AND cp.CREATIONDATE <= '" & todate & "'" & office_sql &_
					"ORDER		BY CP.CASHPOSTINGID "
		'response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
	

		pagesize = 12
		Call OpenDB()
		'Call OpenRs(Rs, strsql)
		
		set Rs = Server.CreateObject("ADODB.Recordset")
			Rs.ActiveConnection = RSL_CONNECTION_STRING
			Rs.ActiveConnection.CommandTimeout = 900
			strSQL=strsql
			'rw strSQL
			Rs.Source = strSQL
			Rs.CursorType = 3
			Rs.CursorLocation = 3
			Rs.Open()
			
		
		
			'Response.End()
		 'set rs = server.createobject("adodb.recordset")
		'	rs.activeconnection = rsl_connection_string 			
		' rs.source = strsql
		' rs.cursortype = 2
		' rs.locktype = 1		
		' rs.cursorlocation = 3
		'Response.Write("RS.PageSize = " & RS.PageSize )
		'Response.Write("RS.PageCount = " & RS.PageCount )
		'Response.Write("RS.RecordCount = " & RS.RecordCount )
'Response.End()
'		rs.Open()


		RS.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			RS.CacheSize = RS.PageSize
			'intPageCount = Icount
			intPageCount= RS.PageCount 
			 intRecordCount = RS.RecordCount 
			
			' Sort pages
			intpage = CInt(Request.QueryString("page"))
			'response.write(intpage)
			
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
			
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				RS.AbsolutePage = intPage
				intStart = RS.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (RS.PageSize - 1)
				End if
			End If
			
			'count = 0			
		
		
		For i=1 to RS.PageSize
			If NOT Rs.EOF Then
			
			 'determine if select box has been previously selected
			isselected = ""
				For each key in sel_split
					'response.write "<BR> KEY : " & key & " id : " & Rs("CASHPOSTINGID")
					If Rs("CASHPOSTINGID") = clng(key) Then
						isselected = " checked "
						Exit For
					End If
				Next
			
				title = ""
				TheName = Rs("THENAME") & ""
				'response.write(TheName)
				'response.end
				if (Len(TheName) > 30) then
					title = " title=""" & TheName & """ "
					TheName = Left(TheName,27) & "..."
				end if
				
				'GET ALL STANDARD VARIABLES
				cnt = cnt + 1
				'START APPENDING EACH ROW TO THE TABLE
				str_data = str_data & 	"<TR><TD>" & Rs("CREATIONDATE") & "</TD>" &_
							"<TD>" & Rs("OFFICE") & "</TD>" &_
							"<TD " & title & ">" & TheName & "</TD>" &_
							"<TD>" & Rs("ITEMTYPE") & "</TD>" &_
							"<TD>" & Rs("PAYMENTTYPE") & "</TD>" &_
							"<TD><input class='textbox100' style='text-align:right' type='text' id='amount" & Rs("CASHPOSTINGID") & "' value='" & FormatNumber(Rs("AMOUNT"),2,-1,0,0) & "' READONLY></TD>" &_
							"<TD><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & Rs("CASHPOSTINGID") & "' value='" & Rs("CASHPOSTINGID") & "' onclick='do_sum("& Rs("CASHPOSTINGID") &")'></TD>" &_
							"</TR>"
						
								'pagesize = pagesize - 1
				Rs.movenext()
				If RS.EOF Then Exit for end if
			End If
		Next
		response.write(my_page_size - cnt)
		
		' pad out to bottom of page
		Call fillgaps(my_page_size - cnt)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=7 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & "<TR style='height:'8px'><TD></TD></TR>" &_
				"<TR><TD COLSPAN=6 align=right><b>Total&nbsp;&nbsp;</b></TD>" &_
				"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=7 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE='Attach Payment Slip' CLASS='rslbutton' ONCLICK='process()'>" &_
								"</TD></TR></TABLE>"
		End If
		
		
		Response.Write("</br>RS.PageSize" & RS.PageSize )
		Response.Write("---intpage" & intpage )
		Response.Write("---RS.PageSize" & (intPage-1)*RS.pagesize+cnt )
		Response.Write("---count" & cnt )
		'Response.End()
		
			str_data = str_data & "<tr bgcolor=black>" &_
			"	<td colspan=10 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=black>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite'  onclick=""javascript:frm_slip.location.href='ServerSide/attach_srv.asp?page=" & 1 & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office  & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite'  onclick=""javascript:frm_slip.location.href='ServerSide/attach_srv.asp?page=" & prevpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office  & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			" 						Page " & intpage & " of " & intPageCount & ". Records: " & RS.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*RS.pagesize+cnt &	" of " & RS.RecordCount   &_
			"						<a class='RSLWhite'  onclick=""javascript:frm_slip.location.href='ServerSide/attach_srv.asp?page=" & nextpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office  & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite'  onclick=""javascript:frm_slip.location.href='ServerSide/attach_srv.asp?page=" & intPageCount& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office  & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/attach_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate  & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"
			
			'Response.Write(str_data)
			'Response.End()
			
Call CloseRS(RS)
		Call CloseDB()
	End function
	
	' pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=8 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>