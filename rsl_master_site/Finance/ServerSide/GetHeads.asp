<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CostCentreID, strWhat, count, optionvalues, optiontext, intReturn, theamounts, Company, callbackfunc

	strWhat = Request.form("IACTION")
    isCascade = Request.form("IS_CASCADE")
    selectedHead = Request.form("SELECTED_HEAD")
    
	If strWhat = "gethead" Then rePop() End If

	Function rePop()
		CostCentreID = -1
		If(Request.Form("sel_COSTCENTRE") <> "") Then CostCentreID = Request.Form("sel_COSTCENTRE")

		If (CostCentreID = -1) Then
			optionvalues = ""
			optiontext = "Please Select a Cost Centre..."
			Exit Function
		End If

        Company = 1
		If(Request.Form("sel_COMPANY") <> "") Then Company = Request.Form("sel_COMPANY")

		Call OpenDB()

		Call GetCurrentYear()
		FY = GetCurrent_YRange

    if (CostCentreID = 9) then
     SQL = "SELECT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) " &_ 
			"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			" INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = he.HEADID LEFT JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID " &_ 
            "WHERE HE.CostCentreID = " & CostCentreID & " and p.Companyid = " & Company & " ORDER BY HE.DESCRIPTION"
    else
     SQL = "SELECT HE.HEADID,HE.DESCRIPTION FROM F_HEAD HE " &_
			"INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) " &_ 
			"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			"WHERE HE.CostCentreID = " & CostCentreID & " ORDER BY HE.DESCRIPTION"
    end if
		

		Call OpenRs(rsHead, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsHead.Fields.Item("HEADID").Value
			optiontext = optiontext & ";;" & theText

			count = count + 1
			rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Heads Are Setup..."
		End If

		Call CloseRs(rsHead)
		Call CloseDB()
	End Function

%>
<html>
<head>
<title>Head Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
<script type="text/javascript" language="JavaScript">
	function loaded(){
		if ('<%=strWhat%>' == 'gethead'){
		    parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 2);

		    var cascade = '<%=isCascade%>';
		    var head = '<%=selectedHead%>';

		    if (cascade == 'true' && head && head > 0) {
		        parent.document.getElementById('sel_HEAD').value = head;
		    }

		    parent.Select_OnchangeHead();

			}
	}
</script>
</body>
</html>
