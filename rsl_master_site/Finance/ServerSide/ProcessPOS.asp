<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#include virtual="Finance/ServerSide/ApplianceEmail.asp" -->
<!--#include virtual="Finance/ServerSide/FaultEmail.asp" -->
<!--#include virtual="Finance/ServerSide/PlannedEmail.asp" -->
<!--#include virtual="Finance/ServerSide/VoidEmail.asp" -->
<!--#include virtual="Finance/ServerSide/CyclicalEmail.asp" -->
<%
OpenDB()

if (Request("AUTHORIZE") = 1) then
    
	For each ORDERITEMID in Request.Form("CHECKITEM")
        SQL = "SELECT ORDERID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
		Call OpenRs(rsPI, SQL)
		ORDERID = rsPI("ORDERID")
		Call CloseRs(rsPI)
        'TO CHECK THAT THIS PI WAS PREVIOUSLY IN "GOOD APPROVED"
			
		SQL="select top(1) IsAmend,PISTATUS from F_PURCHASEITEM_HISTORY where ORDERID=" & ORDERID & " order by History_timestamp desc "
		Call OpenRs(rsPP, SQL)
		if (NOT rsPP.EOF) then
			ISAMEND = rsPP("IsAmend")
			PISTATUS = rsPP("PISTATUS")
		end if

        'CHECK IF PI WAS RE-QUEUED FROM UPLOAD INVOICE PAGE BEFORE ENTER IN LOG TABLE
        SQL="select top(1) PISTATUS, ISUPLOADINVOICE from F_PURCHASEITEM_LOG where ORDERITEMID=" & ORDERITEMID & " order by timestamp desc "
		Call OpenRs(rsPL, SQL)
		if (NOT rsPL.EOF) then
			PISTATUSLOG = rsPL("PISTATUS")
            ISUPLOADINVOICE = rsPL("ISUPLOADINVOICE")
		end if
		
        Call LOG_PI_ACTION(ORDERITEMID,"APPROVED FROM PO APPROVAL")
        if(ISAMEND = true and PISTATUS=18)then
		    strSQL = 	"UPDATE F_PURCHASEITEM SET PISTATUS = 18, APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID
        elseif(ISUPLOADINVOICE = True)then ' PISTATUSLOG=18
		    strSQL = 	"UPDATE F_PURCHASEITEM SET PISTATUS = " & PISTATUSLOG & ", APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID
        else    
		    strSQL = 	"UPDATE F_PURCHASEITEM SET PISTATUS = 1, APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID
        end if
    	Conn.Execute(strSQL)

		'CHECK WHETHER THERE IS ANY OTHER QUEUED PURCHASES ITEM UNDER THIS PO
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS = 0 AND ORDERID = " & ORDERID
		Call OpenRs(rsCount, SQL)
		iCOUNT = rsCount("THECOUNT")
		CloseRs(rsCount)
		if (CInt(iCOUNT) = 0) then
        
			SQL="select top(1) IsAmend,PISTATUS from F_PURCHASEITEM_HISTORY where ORDERID=" & ORDERID & " order by HISTORY_TIMESTAMP desc "
			Call OpenRs(rsPP, SQL)
			if (NOT rsPP.EOF) then
				ISAMEND = rsPP("IsAmend")
				PISTATUS = rsPP("PISTATUS")
			end if
			
			if(ISAMEND = true and PISTATUS=7)then
				PurchaseOStatus = 0
				SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 17 WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
			elseif(ISAMEND = true and PISTATUS=18) then
				SQL ="select count(ORDERITEMID) AS COUNT from F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID="&ORDERID&" and PISTATUS=0"
				Call OpenRs(rsPOI, SQL)
				pOICOUNT = rsPOI("COUNT")
				if(pOICOUNT = 0) then
					PurchaseOStatus=18
					SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 18 WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
				else
					SQL="select top(1) POSTATUS from F_PURCHASEORDER_LOG where ORDERID=" & ORDERID & " order by TIMESTAMP desc "
			        Call OpenRs(rsPPO, SQL)
					SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = " & rsPPO("POSTATUS") & " WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
					PurchaseOStatus = rsPPO("POSTATUS")
				end if 
				''''''''''''''''''''''''''previous'''''''''''''''''''''''''''''
                ' SQL="select count(ORDERITEMID) AS COUNT from F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID=" & ORDERID
			    ' Call OpenRs(rsPOI, SQL)
				' pOICOUNT = rsPOI("COUNT")
				' if(pOICOUNT = 1) then
					' PurchaseOStatus = 0
					' SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 18 WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
				
				' else			
					' SQL="select top(1) POSTATUS from F_PURCHASEORDER_LOG where ORDERID=" & ORDERID & " order by TIMESTAMP desc "
			        ' Call OpenRs(rsPPO, SQL)
					' SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = " & rsPPO("POSTATUS") & " WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
					' PurchaseOStatus = rsPPO("POSTATUS")
				' end if
				''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            elseif(ISUPLOADINVOICE = True) then
                SQL="select top(1) POSTATUS from F_PURCHASEORDER_LOG where ORDERID=" & ORDERID & " order by TIMESTAMP desc "
				Call OpenRs(rsPPO, SQL)
				SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = " & rsPPO("POSTATUS") & " WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
				PurchaseOStatus = rsPPO("POSTATUS")
            else
				PurchaseOStatus = 0
				SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 1 WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
			end if
			Conn.Execute SQL
			
			'ONLY IF THE PO ITEM IS PREVIOUSLY QUEUED
			Call LOG_PO_ACTION(ORDERID, "APPROVED FROM PO APPROVAL", "AND POSTATUS = " &PurchaseOStatus)
            Call NotifyPO(ORDERID, "approved", "Approved")
            'Testing for email sending to appliance servicinf , fuel servicing
            SQL = "SELECT ORDERID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
		    Call OpenRs(rsPI, SQL)
		    ORDERID = rsPI("ORDERID")
		    Call CloseRs(rsPI)
			SQL = "Select SendApprovalLink from CM_ContractorWork where PurchaseOrderId =  " & ORDERID
			 Call OpenRs(rsApproval, SQL)
			 'response.write(SQL)
			  while not rsApproval.EOF
				SendApprovalLink = rsApproval("SendApprovalLink")
				rsApproval.movenext()
				wend
		    Call CloseRs(rsApproval)
            SQL = "SELECT PONAME,PONOTES FROM F_PURCHASEORDER WHERE ORDERID = " & ORDERID
		    Call OpenRs(rsCount, SQL)
		    poName = rsCount("PONAME")
            poNotes = rsCount("PONOTES")
            if(poName = "PLANNED MAINTENANCE WORK ORDER") then
                Call plannedEmailToContractor(ORDERID)
            end if
            if((poName = "DAY TO DAY REPAIR WORK ORDER") Or (poName = "INVESTIGATIVE WORKS ONLY : PLEASE PROVIDE AN ESTIMATE FOR WORKS PRIOR TO COMMENCEMENT")) then
                Call faultEmailToContractor(ORDERID)
            end if
            if(poName = "SCHEME/BLOCK REACTIVE REPAIR WORK ORDER") then
                Call sbFaultEmailToContractor(ORDERID)
            end if
            if poName = "WORK ORDER" then 
                Call applianceEmailToContractor(ORDERID)
            end if
			
			
            if(poName = "CYCLICAL WORKS ORDER") then
			
				if(SendApprovalLink) then
				'response.write(poName)
				'response.write(SendApprovalLink)
				'response.end
					Call CyclicEmailToContractor(ORDERID)
				 end if	
            end if
            if(poName = "M&E SERVICING WORK ORDER") then
                 Call voidCyclicEmailToContractor(ORDERID, "M&E Servicing")
            end if
            if(poName = "VOID WORKS WORK ORDER") then
                 Call voidWorksEmailToContractor(ORDERID)
            end if
            if(poName = "PAINT PACK WORK ORDER") then
                 Call voidPaintEmailToContractor(ORDERID)
            end if
            if(poName = "SCHEME BLOCK PURCHASE ORDER") then
                 Call raiseAPOEmailToContractor(ORDERID)
            end if
		end if
        
	next
elseif (Request("DECLINE") = 1) then
'Response.write("DECLINE")

	For each ORDERITEMID in Request.Form("CHECKITEM")

		Call LOG_PI_ACTION(ORDERITEMID,"DECLINED FROM PO APPROVAL")		
		poStatusDeclinedSQL = "SELECT POSTATUSID FROM F_POSTATUS WHERE POSTATUSNAME = 'Declined'"
		Call OpenRs(rsStatusId, poStatusDeclinedSQL)	
    	strSQL = "UPDATE F_PURCHASEITEM SET ACTIVE = 0, PISTATUS=" & rsStatusId("POSTATUSID") & ", APPROVED_BY = " & Session("USERID") & " WHERE ORDERITEMID = " & ORDERITEMID		
		Conn.Execute(strSQL)

		SQL = "SELECT ORDERID FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
		Call OpenRs(rsPI, SQL)
		ORDERID = rsPI("ORDERID")
		Call CloseRs(rsPI)
		
		'CHECK WHETHER THERE IS ANY OTHER QUEUED PURCHASES UNDER THIS ITEM
		SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS = 0 AND ORDERID = " & ORDERID
		Call OpenRs(rsCount, SQL)
		iCOUNT = rsCount("THECOUNT")		
		CloseRs(rsCount)
		if (iCOUNT = 0) then
			'if no queued and main item is queued then check whether there is any active live ones in which case enable the item
			'else cancel the whole purchase order
			SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS >= 1 AND ORDERID = " & ORDERID
			Call OpenRs(rsCount, SQL)
			iCOUNT = rsCount("THECOUNT")
			CloseRs(rsCount)
			if (iCOUNT > 0) then
		
				Call LOG_PO_ACTION(ORDERID, "DECLINED FROM PO APPROVAL", "AND POSTATUS = 0")														
				SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 1 WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
				Conn.Execute SQL
			else
				Call LOG_PO_ACTION(ORDERID, "DECLINED FROM PO APPROVAL", "AND POSTATUS = "& rsStatusId("POSTATUSID"))																								
				SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 0,POSTATUS="& rsStatusId("POSTATUSID") & " WHERE ORDERID = " & ORDERID & " AND POSTATUS = 0"
				Conn.Execute SQL
				
				
			end if			
		end if
	next

    'CHECK WHETHER THERE ARE ANY OTHER QUEUED ITEMS UNDER THIS PURCHASE ORDER
    SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS = 0 AND ORDERID = " & ORDERID
    Call OpenRs(rsCount, SQL)
    iCOUNT = rsCount("THECOUNT")
    CloseRs(rsCount)

    if (CInt(iCOUNT) = 0) then

        'THERE ARE NO QUEUED ITEMS FOUND
        'CHECK WHETHER THERE ARE ANY ITEMS WHICH HAVE NOT BEEN DECLINED
        SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = " & ORDERID
        Call OpenRs(rsCount, SQL)
        iCOUNT = rsCount("THECOUNT")
        CloseRs(rsCount)

        if (CInt(iCOUNT) > 0) then

            Call NotifyPO(ORDERID, "approved", "Approved")

        else
            ' ALL ITEMS HAVE BEEN DECLINED
            Call NotifyPO(ORDERID, "declined", "Declined")

        end if

    end if

end if

CloseDB()

Function NotifyPO(orderid, emailVerb, subjectVerb) 

    Dim supplierName
    Dim cost
    Dim requestedByEmail
    Dim employeeName
    Dim itemName
    Dim status
    Dim rs
    Dim emailbody
    Dim emailSubject
    Dim table
    Dim row

    ' Obtain supplier name and 
    SQL = "SELECT  " &_
            "S_ORGANISATION.NAME , " &_
            "ISNULL(E_CONTACT.WORKEMAIL, 'enterprisedev@broadlandgroup.org') AS REQUESTEDBYEMAIL " &_
            "FROM    S_ORGANISATION " &_
            "        INNER JOIN F_PURCHASEORDER ON S_ORGANISATION.ORGID = F_PURCHASEORDER.SupplierId " &_
            "        INNER JOIN E__EMPLOYEE ON EMPLOYEEID = USERID " &_
            "        INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID " &_
            "WHERE   F_PURCHASEORDER.ORDERID = " & orderid

    Call OpenRs(rs, SQL)
    supplierName = rs("NAME")
    requestedByEmail = rs("REQUESTEDBYEMAIL")
    CloseRs(rs)
    ' Obtain purchase order item cost and approved by name
	SQL= "SELECT " &_ 
	"(CASE WHEN APPROVED_BY IS NOT NULL THEN ISNULL(LTRIM(RTRIM(ab.FIRSTNAME)) + ' ' + LTRIM(RTRIM(ab.LASTNAME)), '') "  &_
	"ELSE ISNULL(LTRIM(RTRIM(e.FIRSTNAME)) + ' ' + LTRIM(RTRIM(e.LASTNAME)), '') END) AS EMPLOYEENAME, " &_  
	"'�' + CAST(CAST(ISNULL(pi.grosscost, 0) AS NUMERIC(10,2)) AS VARCHAR(100)) AS GROSSCOST, " &_ 
	"status.POSTATUSNAME STATUS, " &_
	"ITEMNAME, ISNULL(ITEMDESC, '') AS ITEMDESC, " &_ 
	"e.FIRSTNAME + ' ' + e.LASTNAME AS RaisedBy, po.PONAME " &_
	"FROM F_PURCHASEORDER po " &_
	"LEFT JOIN F_PURCHASEITEM PI ON po.ORDERID = PI.ORDERID " &_
	"LEFT JOIN F_POSTATUS status ON po.POSTATUS  = status.POSTATUSID " &_
	"LEFT OUTER JOIN E__EMPLOYEE ab ON PI.APPROVED_BY = ab.EMPLOYEEID  " &_
	"LEFT OUTER JOIN E__EMPLOYEE AS e ON PI.USERID = e.EMPLOYEEID " &_
	"WHERE po.ORDERID ="  & orderid		
    ' Assemble details table
    ' Table header
     Call OpenRs(rs, SQL)
     'response.Write(SQL)
    counter = 1
    totalCost = 0
    itemName =""
    itemDecription =""
	poName=""
    while not rs.EOF
    
	if (emailVerb = "declined") then
		emailbody = getHTMLEmailBodyForDeclinedPO(emailVerb)
		emailbody = Replace(emailbody, "{DeclinedBy}",rs("EMPLOYEENAME"))
	else
		emailbody = getHTMLEmailBody(emailVerb)
		emailbody = Replace(emailbody, "{PONumber}","PO "&orderid)
		emailbody = Replace(emailbody, "{ApprovedBy}",rs("EMPLOYEENAME"))
	end if
        
        emailbody = Replace(emailbody, "{BudgetHolder}",rs("RaisedBy"))
		totalCost = totalCost + rs("GROSSCOST")
		poName = rs("PONAME")
            if (counter=1) then
              itemName = rs("ITEMNAME")
              itemDecription =  rs("ITEMDESC")
            else
				if (poName="CYCLICAL WORKS ORDER") then
					itemName = rs("ITEMNAME")
					itemDecription =  "CYCLICAL SERVICING"
				else
					itemName =  itemName & "/"& rs("ITEMNAME")  
					itemDecription = itemDecription &"<br/>"& rs("ITEMDESC")
				end if
            end if    
             
             emailbody = Replace(emailbody, "{POStatus}",rs("STATUS"))
             counter = counter +1
        rs.movenext()
    wend

    Call CloseRs(rs)  
    if( isnull(itemDecription) ) then
         itemDecription=""
     end if
     emailbody = Replace(emailbody, "{ItemNotes}",itemDecription)
     emailbody = Replace(emailbody, "{SupplierName}",supplierName)  
     if( isnull(itemName) ) then
         itemName=""
     end if
     emailbody = Replace(emailbody, "{ItemName}",itemName)
     emailbody = Replace(emailbody, "{GrossCost}",totalCost)  
    ' response.Write(emailbody)
    Dim iMsg
    Set iMsg = CreateObject("CDO.Message")
    Dim iConf
    Set iConf = CreateObject("CDO.Configuration")

    Dim Flds
    Set Flds = iConf.Fields
		Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
    Flds.Update

    emailSubject = "Purchase Order " & subjectVerb
    
    Set iMsg.Configuration = iConf
    iMsg.To = requestedByEmail
    iMsg.From = "noreply@broadlandgroup.org"
    iMsg.Subject = emailSubject
    iMsg.HTMLBody = emailbody
    iMsg.Send	
   			
End Function	

Function PurchaseNumber(strWord)
    PurchaseNumber = "PO " & characterPad(strWord,"0", "l", 7)
End Function	
Function getHTMLEmailBody(emailVerb)

        getHTMLEmailBody = "<!DOCTYPE html><html><head><title>"&emailSubject&"</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>Your Purchase Order has been <span style=""text-transform: uppercase;"">"  &emailVerb& "</span>, details are as follows:</p> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(�):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>{POStatus}</td></tr> " & _
                           "                     <tr><td>Approved by:</td><td>{ApprovedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "                         <td class=""bottomAllignedCell""> " & _
                           "                             Broadland Housing Group<br /> " & _
                           "                             NCFC, The Jarrold Stand<br /> " & _
                           "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function
	


Function getHTMLEmailBodyForDeclinedPO(emailVerb)

        getHTMLEmailBodyForDeclinedPO = "<!DOCTYPE html><html><head><title>"&emailSubject&"</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>Your Purchase Order has been <span style=""text-transform: uppercase;"">"  &emailVerb& "</span>, details are as follows:</p> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td></td><td></td></tr> " & _
                           "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(�):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>{POStatus}</td></tr> " & _
                           "                     <tr><td>Declined by:</td><td>{DeclinedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "                         <td class=""bottomAllignedCell""> " & _
                           "                             Broadland Housing Group<br /> " & _
                           "                             NCFC, The Jarrold Stand<br /> " & _
                           "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">
        function Return_ToParent() {
            window.location.href = "../PurchaseListQueued.asp"
        }
    </script>
</head>
<body onload="Return_ToParent();">
</body>
</html>
