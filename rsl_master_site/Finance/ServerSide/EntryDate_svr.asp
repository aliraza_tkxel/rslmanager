<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
PERIOD = REQUEST("sel_PERIOD")
if (PERIOD = "") then PERIOD = -1
IF (PERIOD <> -1) THEN
	OpenDB()
	
	SQL = "SELECT * FROM F_FISCALYEARS WHERE YRANGE = " & PERIOD
	Call OpenRs(rsPeriod, SQL)
	PS = CDate(FormatDateTime(rsPeriod("YSTART"),1))
	PE = CDate(FormatDateTime(rsPeriod("YEND"),1))
	Call CloseRs(rsPeriod)
	
	YearStart = Year(PS)
	YearEnd = Year(PE)
	MonthStart = Month(PS)
	MonthEnd = Month(PE)
	
	'A MONTH COUNT OF THE TOTAL INCLUSIVE MONTHS BETWEEN THE PERIOD THAT IS CURRENTLY SELECTED.
	MonthCount = DateDiff("m", CDate("1 " & MonthName(MonthStart) & " " & YearStart),  CDate("1 " & MonthName(MonthEnd) & " " & YearEnd)) + 1
	
	OptionString = ""
	for i=1 to MonthCount
		OptionString = OptionString & "<OPTION VALUE=""" & MonthStart & "_" & YearStart & """>" & MonthName(MonthStart) & " " & YearStart & "</OPTION>"
		MonthStart = MonthStart + 1
		if (MonthStart = 13) then
			MonthStart = 1
			YearStart = YearStart + 1
		end if
	next
	
	CloseDB()
END IF
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript>
function ReturnData(){
	parent.RSLFORM.sel_ENTRYDATE.outerHTML = RGOSELECT.innerHTML
	}
</script>
<body bgcolor="#FFFFFF" text="#000000" onload="ReturnData()">

<div id="RGOSELECT">
<% IF PERIOD = -1 THEN %>
	<SELECT NAME="sel_ENTRYDATE" class="textbox200" disabled>
		<OPTION VALUE="">Please Select</OPTION>
	</SELECT>
<% ELSE %>	
	<SELECT NAME="sel_ENTRYDATE" CLASS="TEXTBOX200">
		<OPTION VALUE="">FULL YEAR</OPTION>
		<%=OptionString%>
	</SELECT>
<% END IF %>
</div>

</body>
</html>
