<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim valid_chk_values, update_chk_values

	'get these values for the redirect at the end
	startdate = Request("txt_START")
	Actual_HB_Validation = Request("hid_SCHEDULEDATE")
	la = Request("sel_LA")
	orderby = Request("rdo_ORDER")
	ScheduleID = Request("sel_SCHEDULEDATE")
	tenancy = Request("txt_TENANCYID")
	if (isNumeric(tenancy)) then
		tenancy = CLng(tenancy)
	else
		tenancy = ""
	end if
	hbref = Replace(Request("txt_HBREF"), "'", "''")	
		
	ResponsePath = "hbvalidation_srv.asp?txt_START=" & startdate & "&sel_LA=" & la & "&sel_SCHEDULEDATE=" & ScheduleID & "&txt_TENANCYID=" & tenancy & "&txt_HBREF=" & hbref & "&rdo_ORDER=" & orderby
	if (NOT isDate(Actual_HB_Validation)) then Response.Redirect ResponsePath

	
	valid_chk_values = Request.form("chkHB_V")
	amend_chk_values = Request.form("chkHB_A")
	
	RW "validate" & valid_chk_values
	RW "amend" & amend_chk_values
	
	OpenDB()
	validate_hb()
	amend_hb()

	'Run the auto hbschedule entry to set any estimated rent entries into the rent journal
	Conn.Execute("F_AUTO_HBSCHEDULE_ENTRY")
	
	CloseDB()
	
	Response.Redirect ResponsePath
		
	// parse valid checkbox string then loop through, changing status to paid
	Function validate_hb()
	
		arrChk = Split(Replace(valid_chk_values, " ", ""), ",")
		
		For Each chk In arrChk
			'Response.Write "IHB" & chk & "<BR>"
			isinitial = Request.Form("IHB" & chk)
			'Response.Write "initial Value = " & isinitial & "<BR>"		
			Call Validate(chk, isinitial)			
		Next
		
	End Function

	// parse amend checkbox string then loop through, changing status to paid
	Function amend_hb()
	
		arrChk = Split(Replace(amend_chk_values, " ", ""), ",")
		
		For Each chk In arrChk
			amount =  Request.Form("txt_AM" & chk)
			startdate = Request.Form("txt_ST" & chk)
			enddate = Request.Form("txt_ED" & chk)
			isinitial = CStr(Request.Form("IHB" & chk))
			Call ReValidate(chk, amount, startdate, enddate, isinitial)
		Next
		
	End Function

	'this function just validates both rows in the rent journal and hhb schedule
	Function Validate(journalid, InitialHB)
		'NEED TO FIND THE HBID VALUE FOR THE CURRENT ROW
		SQL = "SELECT HB.CUSTOMERID, HB.HBID FROM F_HBACTUALSCHEDULE HBA, F_HBINFORMATION HB WHERE HBA.HBID = HB.HBID AND HBA.JOURNALID = " & journalid
		'rESPONSE.wRITE sql
		Call OpenRs(rsHB, SQL)
		if (NOT rsHB.EOF) then
			HBID = rsHB("HBID")
		end if
		CloseRs(rsHB)

		Response.Write ""
		'updates THE INITIAL HB ESTIMATE IF IT WAS AN ESTIMATE and sets it to actual .
		if (InitialHB = 0) then

			SQL = "UPDATE F_HBINFORMATION SET STATUS = 1 WHERE HBID = " & HBID 
			Conn.Execute(SQL)
		end if
	
		'UPDATES THE APPROPRIATE RENT JOURNAL ITEM AND SETS THE STATUS TO PAID
		SQL = "Update F_RENTJOURNAL SET TRANSACTIONDATE = '" & FormatDateTime(Actual_HB_Validation,1) & "', STATUSID = 2 WHERE JOURNALID = " & journalid
		'rESPONSE.wRITE sql
		Conn.Execute(SQL)
		
		'Updates Actual HB Schedule and sets it to Validated
		SQL = "UPDATE F_HBACTUALSCHEDULE SET VALIDATED = 1 WHERE JOURNALID = " & journalid		
		Conn.Execute(SQL)
		
		'Finally need to insert a new scheduled value for the HB, so as to keep 13 in the system at one time
		SQL = "SELECT HB, FSTARTDATE = CONVERT(VARCHAR, STARTDATE, 103), ENDDATE = CONVERT(VARCHAR, ENDDATE, 103) FROM F_HBACTUALSCHEDULE WHERE HBID = " & HBID & " AND VALIDATED IS NULL ORDER BY STARTDATE DESC"
		Call OpenRs(rsFinal, SQL)
		if (NOT rsFinal.EOF) then
			F_STARTDATE = dateadd("d", 28, CDate(FormatDateTime(rsFinal("FSTARTDATE"),1)))
			F_ENDDATE = dateadd("d", 28, CDate(FormatDateTime(rsFinal("ENDDATE"),1)))
			F_HB = rsFinal("HB")
			
			SQL = "INSERT INTO F_HBACTUALSCHEDULE (HBID, STARTDATE, ENDDATE, HB) VALUES " &_
					"(" & HBID & ", '" & FormatDateTime(F_STARTDATE,1) & "', '" & FormatDateTime(F_ENDDATE,1) & "', " & F_HB & ") "
			Conn.Execute(SQL)
		end if
		CloseRs(rsFinal)
	End Function
	
	'this function amends the figures for the schedule and then validates them both, and then rebuilds 13 rows of the schedule
	Function ReValidate(journalid, amount, startdate, enddate, InitialHB)

		SQL = "SELECT HB.CUSTOMERID, HB.TENANCYID, HB.HBID FROM F_HBACTUALSCHEDULE HBA, F_HBINFORMATION HB WHERE HBA.HBID = HB.HBID AND HBA.JOURNALID = " & journalid
		Call OpenRs(rsHB, SQL)
		if (NOT rsHB.EOF) then
			HBID = rsHB("HBID")
			CUSTOMERID = rsHB("CUSTOMERID")
			TENANCYID = rsHB("TENANCYID")			
		end if
		CloseRs(rsHB)		

		'UPDATES THE INITIAL HB ESTIMATE IF IT WAS AN ESTIMATE.
		if (InitialHB = 0) then
			SQL = "UPDATE F_HBINFORMATION SET INITIALPAYMENT = " & amount & ", INITIALSTARTDATE = '" & FormatDateTime(startdate,1) & "', INITIALENDDATE = '" & FormatDateTime(enddate,1) & "', STATUS = 1 WHERE HBID = " & HBID 
			Conn.Execute(SQL)
		end if
		
		'UPDATES THE APPROPRIATE RENT JOURNAL ITEM AND SETS THE STATUS TO PAID
		SQL = "Update F_RENTJOURNAL SET TRANSACTIONDATE = '" & FormatDateTime(Actual_HB_Validation,1) & "', PAYMENTSTARTDATE = '" & FormatDateTime(startdate,1) & "', PAYMENTENDDATE = '" & FormatDateTime(enddate,1) & "', AMOUNT = " & amount & ", STATUSID = 2 WHERE JOURNALID = " & journalid
		'rESPONSE.wRITE sql
		Conn.Execute(SQL)
		
		'Updates Actual HB Schedule and sets it to Validated
		SQL = "UPDATE F_HBACTUALSCHEDULE SET STARTDATE = '" & FormatDateTime(startdate,1) & "', ENDDATE = '" & FormatDateTime(enddate,1) & "', HB = " & amount & ", VALIDATED = 1 WHERE JOURNALID = " & journalid		
		Conn.Execute(SQL)
		
		'NEXT DELETE ALL PREVIOUS ESTIMATED ITEMS IN THE RENT JOURNAL AND ALL PREVIOUSLY HBACTUAL SCHEDULE ITEMS
		SQL = "DELETE F_RENTJOURNAL " &_
				"FROM F_RENTJOURNAL RJ, F_HBACTUALSCHEDULE HBA, F_HBINFORMATION HBI " &_
				"WHERE RJ.JOURNALID = HBA.JOURNALID AND HBA.HBID = HBI.HBID AND HBI.CUSTOMERID = " & CUSTOMERID & " AND HBI.TENANCYID = " & TENANCYID & " " &_
				"AND RJ.STATUSID = 1 AND RJ.PAYMENTTYPE = 1 AND RJ.ITEMTYPE = 1"		
		'rESPONSE.wRITE sql
		Conn.Execute(SQL)
		
		SQL = "DELETE FROM F_HBACTUALSCHEDULE WHERE VALIDATED IS NULL AND HBID = " & HBID
		'rESPONSE.wRITE sql
		Conn.Execute(SQL)
		
		InStartDate = CDate(startdate)
		InEndDate = CDate(enddate)
		if (InStartDate > InEndDate) then
			DailyRate = 0
		else
			DailyRate = amount/(DateDiff("d", InStartDate, InEndDate) + 1)
		end if
		
		CycleStart = DateAdd("d", 1, InEndDate)
		CycleEnd = DateAdd("d", 27, CycleStart)
		HB = FormatNumber(28 * DailyRate,2,-1,0,0)
		for i=0 to 14
			SQL = "INSERT INTO F_HBACTUALSCHEDULE (HBID, STARTDATE, ENDDATE, HB) VALUES (" & HBID & ", '" & FormatDateTime(CycleStart) & "', '" & FormatDateTime(CycleEnd) & "', " & hb & ")"
			'Response.Write SQL
			Conn.Execute(SQL)
			
			CycleStart = DateAdd("d", 28, CycleStart)
			CycleEnd = DateAdd("d", 28, CycleEnd)
		next
		
	End Function
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	alert("<%=valid_chk_values%>")
	alert("<%=amend_chk_values%>")
	
	}
</script>
<body onload="ReturnData()">
</body>
</html>