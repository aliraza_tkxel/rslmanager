<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim SchemeID, strWhat, count, optionvalues, optiontext, intReturn, theamounts, Company

	strWhat = Request.form("IACTION")
	If strWhat = "getBlock" Then rePop() End If

	Function rePop()
		SchemeID = -1
		If(Request.Form("sel_SCHEME") <> "") Then SchemeID = Request.Form("sel_SCHEME")

		If (SchemeID = -1) Then
			optionvalues = ""
			optiontext = "Please Select a Scheme..."
			Exit Function
		End If

		Call OpenDB()
        
        SQL = "SELECT BLOCKID, BLOCKNAME FROM P_BLOCK " &_
			  "WHERE SchemeId = " & SchemeID

		Call OpenRs(rsBlock, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsBlock.EOF)
			theText = rsBlock.Fields.Item("BLOCKNAME").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsBlock.Fields.Item("BLOCKID").Value
			optiontext = optiontext & ";;" & theText

			count = count + 1
			rsBlock.MoveNext()
		Wend
		If (rsBlock.CursorType > 0) Then
		  rsBlock.MoveFirst
		Else
		  rsBlock.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Blocks Are Setup..."
		End If

		Call CloseRs(rsBlock)
		Call CloseDB()
	End Function

%>
<html>
<head>
    <title>Block Data Fetcher</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
    <script type="text/javascript" language="JavaScript">
        function loaded() {
            parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", 3);
        }
    </script>
</body>
</html>
