<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	Dim strWhat, count, optionvalues, optiontext, intReturn, theamounts
	
	OpenDB()	
	
	Dim OrderID
	OrderID = -1
	if(Request.Form("hid_ORDERID") <> "") then OrderID = Request.Form("hid_ORDERID")

	Dim ExpenditureID
	ExpenditureID = ""
	if(Request.Form("sel_Expenditure") <> "") then ExpenditureID = Request.Form("sel_Expenditure")
	
	Dim HEADID
	HEADID = ""
	if(Request.Form("sel_HEADID") <> "") then HEADID = UCase(Request.Form("sel_HEADID"))
	
	Dim ItemRef
	ItemRef = ""
	if(Request.Form("txt_ITEMREF") <> "") then ItemRef = Replace(Request.Form("txt_ITEMREF"), "'", "''")
	
	Dim ItemDesc
	ItemDesc = ""
	if(Request.Form("txt_ITEMDESC") <> "") then ItemDesc = Replace(Request.Form("txt_ITEMDESC"), "'", "''")
	
	Dim PODate
	PODate = ""
	if(Request.Form("txt_PODATE") <> "") then PODate = FormatDateTime(Request.Form("txt_PODATE"),1)
	
	Dim VatType
	VatType = ""
	if(Request.Form("sel_VATTYPE") <> "") then VatType = Request.Form("sel_VATTYPE")

	Dim NetCost
	NetCost = ""
	if(Request.Form("txt_NETCOST") <> "") then NetCost = Request.Form("txt_NetCost")
	
	Dim VAT
	VAT = ""
	if(Request.Form("txt_VAT") <> "") then VAT = Request.Form("txt_VAT")
	
	Dim DelDate
	DelDate = "NULL"
	if(Request.Form("txt_DELDATE") <> "") then DelDate = FormatDateTime(Request.Form("txt_DELDATE"),1)
	
	Dim DelDetail
	DelDetail = ""
	if(Request.Form("txt_DelDetail") <> "") then DelDetail = Replace(Request.Form("txt_DelDetail"), "'", "''")
	
	Dim QuotedCost
	QuotedCost = ""
	if(Request.Form("txt_QUOTEDCOST") <> "") then QuotedCost = Request.Form("txt_QUOTEDCOST")
	
	Dim UserID
	UserID = Session("UserID")
	
	Dim Supplier
	Supplier = ""
	if(Request.Form("sel_SUPPLIER") <> "") then Supplier = Request.Form("sel_SUPPLIER")

	Call AmendPurchaseOrder_Limited (OrderID, ItemRef, ItemDesc, DelDetail, VatType, NetCost, VAT, QuotedCost, UserID)
	
	CloseDB()
	
	Response.Redirect "../PurchaseAmendList.asp"										
%>

