<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	if (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then 'from reconcile page
		LOGPAGETEXT = "(RECONCILE PAGE)"
	else
		LOGPAGETEXT = "(AMEND PAGE)"
	end if
	
	ORDERITEMID = REQUEST("ORDERITEMID")
	ORDERID = REQUEST("ORDERID")
	BATCHID= REQUEST("BatchID")
		
	ItemDesc = Replace(Request("txt_ITEMDESC"), "'", "''")

	ExpenditureID = Request("sel_EXPENDITURE")

	UserSessionID = SESSION("USERID")
	OpenDB()
    
	'INSERT THE PREVIOUS DATA INTO THE HISTORY TABLE.
	SQL = "INSERT INTO F_PURCHASEITEM_HISTORY (ORDERID,ORDERITEMID,PISTATUS,IsAmend,PIDATE,EXPENDITUREID, VAT, VATTYPE, NETCOST, GROSSCOST,USERID,HISTORY_TIMESTAMP ) " &_
			"SELECT ORDERID,ORDERITEMID,PISTATUS,1,PIDATE, EXPENDITUREID, VAT, VATTYPE, NETCOST, GROSSCOST, " & UserSessionID & ", GETDATE() " &_
			" FROM F_PURCHASEITEM WHERE ORDERITEMID = " & ORDERITEMID
			
	'rw sql & "<BR>"
	Conn.Execute SQL
	
	' I have assigned a new histroical item when the order has been amended
	' this tells the users who has amended the order
	SQL = " INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONUSER, ITEMDETAILID, CONTRACTORID, TITLE, NOTES) " &_
			"					SELECT J.JOURNALID, CURRENTITEMSTATUSID, 13 ," & UserSessionID & ",  R.ITEMDETAILID, R.CONTRACTORID ,R.TITLE,'Applied in Purchase Amend Tool by " & Replace(Session("FirstName"),"'"," ") & " " & Replace(Session("LastName"),"'"," ") & " : " & ItemDesc & "' " &_
			"					FROM P_WOTOREPAIR WOTO " &_
			"						INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID " &_
			"						INNER JOIN C_REPAIR R ON R.JOURNALID = WOTO.JOURNALID " &_
			"					WHERE 	REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = R.JOURNALID) " &_
			"						AND WOTO.ORDERITEMID = " & ORDERITEMID 
	'rw sql & "<BR>"
	
	Conn.Execute SQL

	Call LOG_PI_ACTION(ORDERITEMID, "AMENDED FROM FINANCE " & LOGPAGETEXT)			
			
	'UPDATE THE ACTUAL PURCHASE ITEM		
	SQL = "UPDATE F_PURCHASEITEM SET EXPENDITUREID = " & ExpenditureID & " WHERE ORDERITEMID = " & ORDERITEMID
	'rw sql & "<BR>"
	Conn.Execute (SQL)


	'UPDATE ACCOUNT INFO IN NL_JOURNALENTRYDEBITLINE AND NL_JOURNALENTRYCREDITLINE IF ITEM HAS BEEN RECONCILED 		
	SQL = " UPDATE  NL_JOURNALENTRYDEBITLINE " &_
		  "	SET		NL_JOURNALENTRYDEBITLINE.ACCOUNTID = AC.ACCOUNTID " &_
		  "	FROM	NL_JOURNALENTRYDEBITLINE " &_
		  "			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = NL_JOURNALENTRYDEBITLINE.TRACKERID " &_
		  "			INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		  "			LEFT JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE  " &_
		  "			LEFT JOIN NL_ACCOUNT BEF_AC ON NL_JOURNALENTRYDEBITLINE.ACCOUNTID = BEF_AC.ACCOUNTID " &_
		  "			WHERE   NL_JOURNALENTRYDEBITLINE.ACCOUNTID <> AC.ACCOUNTID AND TRACKERID =  " & ORDERITEMID
	Conn.Execute (SQL)
	
	SQL = " UPDATE  NL_JOURNALENTRYCREDITLINE " &_
		  "	SET		NL_JOURNALENTRYCREDITLINE.ACCOUNTID = AC.ACCOUNTID " &_
		  "	FROM	NL_JOURNALENTRYCREDITLINE " &_
		  "			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = NL_JOURNALENTRYCREDITLINE.TRACKERID " &_
		  "			INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		  "			LEFT JOIN NL_ACCOUNT AC ON AC.ACCOUNTNUMBER = EX.QBDEBITCODE  " &_
		  "			LEFT JOIN NL_ACCOUNT BEF_AC ON NL_JOURNALENTRYCREDITLINE.ACCOUNTID = BEF_AC.ACCOUNTID " &_
		  "			WHERE   NL_JOURNALENTRYCREDITLINE.ACCOUNTID <> AC.ACCOUNTID AND TRACKERID =  " & ORDERITEMID
	Conn.Execute (SQL)
	
	
    SQL = "UPDATE F_PURCHASEORDER SET IsAmend = 1"
    Conn.Execute(SQL)
    
    ' Logic for getting Property record from F_ServiceChargeExProperties
    ExPropertyCount = 0
    SQL = "SELECT Count(*) as ItemCount from F_ServiceChargeExProperties " &_
            "WHERE PurchaseItemId = " & ORDERITEMID
    Call OpenRs(rsCountExPP, SQL)
    while (NOT rsCountExPP.EOF)
        ExPropertyCount = rsCountExPP("ItemCount")
        rsCountExPP.moveNext
    wend
    Call CloseRs(rsCountExPP)
    'Const arraySize = ExPropertyCount
    if(ExPropertyCount > 0) then
        REDIM samePropertyRecordArray (5)
        REDIM Preserve samePropertyRecordArray (ExPropertyCount)
        'Note: For Deleting data from F_ServiceChargeExProperties
        SQL = "SELECT * from F_ServiceChargeExProperties " &_
                "WHERE PurchaseItemId = " & ORDERITEMID
        Call OpenRs(rsExPP, SQL)
        arrayIndex = 0
        while(NOT rsExPP.EOF)
            samePropertyRecordArray(arrayIndex) = rsExPP("PropertyId") + ",0"
            arrayIndex = arrayIndex + 1
        rsExPP.moveNext
        wend
        Call CloseRs(rsExPP)
        'SBP(2) = Property
        'SBP(1) = Block
        'SBP(0) = Schme
        for each item in Request.Form("SID_ROW")	
		    Data = Request.Form("iSelDATA" & item)	
            SBP = Split(Data,",")
            
            'TODO: Need to remove this later
            SQL = "SELECT PROPERTYID from P__PROPERTY WHERE SCHEMEID = 0"
            Call OpenRs(rsPP, SQL)
            if(SBP(2) <> "0") then
                
                if(SBP(2) = "-1") then
                    if(SBP(1) = "-1" Or SBP(1) = "0" )then
                        if(SBP(0) = "-1") then
                            SQL = "SELECT PROPERTYID from P__PROPERTY " &_
                                "INNER JOIN P_SCHEME Sch ON Sch.SCHEMEID = P__PROPERTY.SCHEMEID"
                            Call OpenRs(rsPP, SQL)
                        else
                            SQL = "SELECT PROPERTYID from P__PROPERTY WHERE SCHEMEID = " & SBP(0)
                            Call OpenRs(rsPP, SQL)
                        end if
                    else
                        SQL = "SELECT PROPERTYID from P__PROPERTY WHERE BLOCKID = " & SBP(1)
                        Call OpenRs(rsPP, SQL)
                    end if
                else
                    'Need to check that property with table and check is that property exist
                    rowcount = 0
                    'for each prop in samePropertyRecordArray
                     for i = 0 to Ubound(samePropertyRecordArray) - 1
                        propSplit = Split(samePropertyRecordArray(i),",")                        
                        if(propSplit(1) = "0") then
                           if(SBP(2) = propSplit(0)) then
                                propSplit(1) = "1"
                                samePropertyRecordArray(i) = propSplit(0) + "," + propSplit(1)
                            end if
                        end if
                    next                    
                end if
            end if

            while(NOT rsPP.EOF)
                for i = 0 to Ubound(samePropertyRecordArray) - 1
                    propSplit = Split(samePropertyRecordArray(i),",")
                    if(propSplit(1) <> "1") then
                        if(rsPP("PROPERTYID") = propSplit(0)) then
                            propSplit(1) = "1"
                            samePropertyRecordArray(i) = propSplit(0) + "," + propSplit(1)
                        end if
                    end if
                next                
                rsPP.moveNext
            wend
            Call CloseRs(rsPP)
        next

        'Note: Delete date from F_ServiceChargeExProperties
        ' Logic here
        for i = 0 to Ubound(samePropertyRecordArray) - 1
            propSplit = Split(samePropertyRecordArray(i),",")
            if(propSplit(1) = "0") then
                SQL = "Delete from F_ServiceChargeExProperties " &_
                       "Where PropertyId = '" & propSplit(0) & "' And PurchaseItemId = " & ORDERITEMID
                Conn.Execute(SQL)
            end if
        next  
    end if

    'Insert new data in F_PurchaseItemSCInfo
    SQL = "Delete From F_PurchaseItemSCInfo where OrderItemId = " & ORDERITEMID
    Conn.Execute(SQL)

    for each item in Request.Form("SID_ROW")	
		Data = Request.Form("iSelDATA" & item)	
        SBP = Split(Data,",")
        if(SBP(1) = "0") then
            SBP(1) = "null"
        end if
        if(SBP(2) = "0") then
            SQL = "INSERT INTO F_PurchaseItemSCInfo (OrderId,OrderItemId,SchemeId,BlockId,PropertyId,IsActive) VALUES " &_
            "( " & OrderID & ", " & orderItemId & ", " & SBP(0) & ", " & SBP(1) & ", null, 1 )"
        else
            SQL = "INSERT INTO F_PurchaseItemSCInfo (OrderId,OrderItemId,SchemeId,BlockId,PropertyId,IsActive) VALUES " &_
            "( " & OrderID & ", " & orderItemId & ", " & SBP(0) & ", " & SBP(1) & ", '" & SBP(2) & "', 1 )"
        end if
        Response.Write(SQL)
        Conn.Execute SQL
    next
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_Parent()
        {
            parent.parentReload();
		}
    </script>
</head>
<body onload="Return_Parent();">
</body>
</html>
