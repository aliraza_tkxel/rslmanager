<% ByPassSecurityAccess = true %>
<%
OpenDB()
Function CyclicEmailToContractor(ORDERID)
            
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " Select cw.PurchaseOrderId, ISNULL(SCH.SCHEMENAME,'-') as SchemeName,ISNULL(B.BLOCKNAME,'-') AS BlockName,I.ItemName AS ServiceRequired, " &_
                        "'Every '+cast(s.Cycle AS NVARCHAR)+' '+ CT.CycleType AS CYCLE,CONVERT(NVARCHAR(20),s.ContractCommencement,103)AS Commencement,'�'+Cast(CONVERT(DECIMAL(10,2),s.CycleValue) AS NVARCHAR) AS CycleValue," &_
                        "'�'+Cast(CONVERT(DECIMAL(10,2),cw.ContractNetValue) AS NVARCHAR) AS NetCost,'�'+Cast(CONVERT(DECIMAL(10,2),cw.Vat) AS NVARCHAR) AS vat,'�'+Cast(CONVERT(DECIMAL(10,2),cw.ContractGrossValue) AS NVARCHAR) AS GrossCost," &_
                        "ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '')  AS OrderedBy,ISNULL(C.WORKDD, '') AS DD,ISNULL(C.WORKEMAIL, '') AS [Email],Con.ContractorContactName,Con.Email as ContractorEmail,cw.CMContractorId,cw.ContactId" &_
                        " FROM CM_ServiceItems s" &_
                        " INNER JOIN CM_ContractorWork cw ON s.ServiceItemId = cw.ServiceItemId and s.PORef = cw.PurchaseOrderId" &_
                        " INNER JOIN PA_ITEM I ON s.ItemId = I.ItemID" &_
                        " LEFT JOIN P_BLOCK B ON s.BlockId=B.BLOCKID" &_
                        " LEFT JOIN P_SCHEME SCH ON s.SchemeId=SCH.SCHEMEID OR B.SchemeId=SCH.SCHEMEID " &_
                        " INNER JOIN PDR_CycleType CT ON s.CycleType=CT.CycleTypeId" &_
                        " INNER JOIN E__EMPLOYEE E ON cw.AssignedBy = E.EMPLOYEEID" &_
                        " INNER JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID" &_
                        " CROSS APPLY(SELECT 	ISNULL(E.FIRSTNAME, '') + ISNULL(' ' + E.LASTNAME, '') AS ContractorContactName," &_
                        "	ISNULL(C.WORKEMAIL, '') AS [Email], ISNULL(C.WORKDD, '') AS DD " &_
                        " FROM E__EMPLOYEE E" &_
                        " INNER JOIN E_CONTACT C" &_
                        "	ON E.EMPLOYEEID = C.EMPLOYEEID" &_
                        " WHERE E.EMPLOYEEID =cw.ContactId) Con" &_
                        " Where s.PORef =" & ORDERID
                Call OpenRs(rs, SQL)
                if not rs.eof then
                    ContactName = rs("ContractorContactName")
                    ContractorEmail = rs("ContractorEmail")
                    Orderby = rs("OrderedBy")
                    PurchaseOrderId = rs("PurchaseOrderId")
                    DDDial = rs("DD")
                    Email = rs("Email")
                    SchemeName = rs("SchemeName")
                    BlockName = rs("BlockName")
                    ServiceRequired = rs("ServiceRequired")
                    Cycle = rs("Cycle")
                    Commencement = rs("Commencement")
                    CycleValue = rs("CycleValue")
                    NetCost = rs("NetCost")
                    vat = rs("vat")
                    GrossCost = rs("GrossCost")
                    CMContractorId = rs("CMContractorId")
                    ContactId = rs("ContactId")
                end if
                Call CloseRs(rs)
                'Dim CycleDates 
                SQLAccess = "EXECUTE CM_GetContractWorkDetail @CMContractorId = " & CMContractorId
                Dim rsAccess
                Set rsAccess = Conn.Execute (SQLAccess)
                'OpenDB()
                if (NOT rsAccess.EOF) then
		            CycleDates = rsAccess("CycleDates")	            
                end if
                CloseRs(rsAccess)
	            'CloseDB()
                
				socket   = "http"
    If Request.ServerVariables("HTTPS") = "on" then 
        socket = "https"
    End if
                '------------------------------------------------
    'SETTING URL FOR ACCEPT PO PAGE
    '------------------------------------------------
   acceptUrl = socket & ":" & "//" & Request.ServerVariables("server_name") & "/PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx?oid=" & PurchaseOrderId & "&uid=" & ContactId

                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Recipient = ContractorEmail
                Dim emailbody
                emailbody =  getContractorEmailTemplateVoidCyclic()
                'emailbody = Replace(emailbody,"{Servicing}",typ)
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{Orderby}",Orderby)
                emailbody = Replace(emailbody,"{PONumber}",PurchaseOrderId)
                emailbody = Replace(emailbody,"{DDDial}",DDDial)
                emailbody = Replace(emailbody,"{Email}",Email)
                emailbody = Replace(emailbody,"{Scheme}",SchemeName)
                emailbody = Replace(emailbody,"{Block}",BlockName)
                emailbody = Replace(emailbody,"{ServiceRequired}",ServiceRequired)
                emailbody = Replace(emailbody,"{Cycle}",Cycle)
                emailbody = Replace(emailbody,"{Commencement}",Commencement)
                emailbody = Replace(emailbody,"{CycleValue}",CycleValue)
                emailbody = Replace(emailbody,"{NetCost}",NetCost)
                emailbody = Replace(emailbody,"{VAT}",vat)
                emailbody = Replace(emailbody,"{TOTAL}",GrossCost)
                
                emailbody = Replace(emailbody,"{CycleDates}",CycleDates)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")
                emailbody = Replace(emailbody,"{#}",acceptUrl)
                emailbody = Replace(emailbody,"{accept}","Please follow this link to accept the PO:")
				'response.write(ORDERID)
				'response.write(acceptUrl)               
			   sendEmailVoidCyclic emailbody,Recipient

        '------------------------------------------------------------

End Function




    '--------------------------  TEST

    Function sendEmailVoidCyclic(body,recipient)
	'response.write(recipient)
	'response.write(body)
     if IsNull(recipient) then

        session("noRecipient") = "true"
		Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

    else
        Dim iMsg
        Set iMsg = CreateObject("CDO.Message")
        Dim iConf
        Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields


        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        emailbody = body
         
        emailSubject = "Confirmation of Cyclical Services Order"
        
        
        Set iMsg.Configuration = iConf
        iMsg.To = recipient
        iMsg.From = "noreply@broadlandgroup.org"
        iMsg.Subject = emailSubject
        iMsg.HTMLBody = emailbody
        iMsg.Send	

        end if

    End Function


    Function getContractorEmailTemplateVoidCyclic()
        Dim assignToContractorEmailTemplateVoidCyclic
        assignToContractorEmailTemplateVoidCyclic = "<table> <tr> <td colspan='2'> Dear {ContractorContactName} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan='2'> Please find below details of the requested Cyclical works: </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style='padding-right: 20px;'> PO Number: </td> <td> {PONumber} </td> </tr> <tr> <td style='padding-right: 20px;'> Order by: </td> <td> {Orderby} </td> </tr> <tr> <td style='padding-right: 20px;'> DD Dial: </td> <td> {DDDial} </td> </tr> <tr> <td style='padding-right: 20px;'> Email: </td> <td> {Email} </td> </tr> <tr> <td style='padding-right: 20px;'> Scheme: </td> <td> {Scheme} </td> </tr> <tr> <td style='padding-right: 20px;'> Block: </td> <td> {Block} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style='padding-right: 20px;'> Service required: </td> <td> {ServiceRequired} </td> </tr> <tr> <td style='padding-right: 20px;'> Cycle: </td> <td> {Cycle} </td> </tr> <tr> <td style='padding-right: 20px;'> Commencement: </td> <td> {Commencement} </td> </tr> <tr> <td style='padding-right: 20px;'> Cycle Value(&pound;): </td> <td> {CycleValue} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style='padding-right: 20px;'> <b>Cycle Dates:</b> </td> <td> </td> </tr> <tr> <td colspan='2'> {CycleDates} </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style='padding-right: 20px;'> Net Cost (&pound;): </td> <td> {NetCost} </td> </tr> <tr> <td style='padding-right: 20px;'> VAT (&pound;): </td> <td> {VAT} </td> </tr> <tr> <td style='padding-right: 20px;'> <strong>Total (&pound;):</strong> </td> <td> <strong>{TOTAL}</strong> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td colspan='2'> <a href='{#}'>{accept}</a> </td> </tr> <tr> <td colspan='2'> <b>Please contact Broadland for details of any asbestos that might be present at the property.</b> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr> <td style='padding-right: 20px;'> Kind regards </td> <td> &nbsp; </td> </tr> <tr> <td colspan='2'> <strong>Broadland Housing </strong> <br /> <br /> <table style='vertical-align: text-top;'> <tr> <td> <img src='{Logo_50_years}' alt='Brs 50 Years' />&nbsp; <img src='{Logo_Broadland-Housing-Association}' alt='Broadland Repairs' style='margin-left: 2mm;' /> </td> <td style='padding-left: 2mm;'> Broadland Housing Group<br /> NCFC, The Jarrold Stand<br /> Carrow Road, Norwich, NR1 1HU<br /> Customer Services 0303 303 0003<br /> enq@broadlandgroup.org<br /> </td> </tr> </table> </td> </tr> </table>"
        getContractorEmailTemplateVoidCyclic = assignToContractorEmailTemplateVoidCyclic
    End Function	
 


%>