<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	SQL = "EXEC GET_VALIDATION_PERIOD 5"
    Call OpenDB()
	Call OpenRs(rsTAXDATE, SQL)
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)
	'Call CloseDB()

Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'Get the boundary
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    Response.Write "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br/>"
    Response.Write "Please correct the form attributes and try again."
    Response.End
  end if
  'Check ADO Version
	set checkADOConn = Server.CreateObject("ADODB.Connection")
	adoVersion = CSng(checkADOConn.Version)
	set checkADOConn = Nothing
	if adoVersion < 2.5 then
    Response.Write "<b>You don't have ADO 2.5 installed on the server.</b><br/>"
    Response.Write "The File Upload extension needs ADO 2.5 or greater to run properly.<br/>"
    Response.Write "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br/>"
    Response.End
	end if		
  'Check content length if needed
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      Response.Write "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'Get all data inside the boundaries
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'Members variable of objects are put in a dictionary object
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'Get an object name
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'Test if object is of file type
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'Get Filename, content-type and content of file
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'Add filename to dictionary object
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'Add content-type to dictionary object
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'Get content of object
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'Get content of object
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'Add content to dictionary object
    UploadControl.Add "Value" , Value	
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen	
    'Add dictionary object to main dictionary
    UploadRequest.Add name, UploadControl	
    'Loop to next object
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'Save all uploaded files
    if UploadRequest.Item(GP_curKey).Item("FileName") <> "" then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      if GP_valueLen = 0 then
        Response.Write "<B>An error has occured saving uploaded file!</B><br/><br/>"
        Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
        Response.Write "File does not exists or is empty.<br/>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
	  	  response.End
	    end if
      
      'Create a Stream instance
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")
      
      'Open the stream
      GP_strm1.Open
      GP_strm1.Type = 1 'Binary
      GP_strm2.Open
      GP_strm2.Type = 1 'Binary
        
      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen
    
        SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	    Call OpenRs(rsDEFAULTS, SQL)
	    doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	    Call CloseRs(rsDEFAULTS)

         GP_curPath = doc_DEFAULTPATH & "\finance\StandingOrderFiles\"

      'Create and Write to a File
      'GP_curPath = Request.ServerVariables("PATH_INFO")
      'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
      'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
      '  GP_curPath = GP_curPath & "/"
      'end if 
      GP_CurFileName = UploadRequest.Item(GP_curKey).Item("FileName")
      'GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & GP_CurFileName
      GP_FullFileName = GP_curPath & GP_CurFileName
      'Check if the file alreadu exist
      GP_FileExist = false
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = true
      End If      
      if nameConflict = "error" and GP_FileExist then
	  	Response.Redirect "FileAlreadyExists.asp?FileName=" & GP_CurFileName
        Response.Write "<B>File already exists!</B><br/><br/>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
				GP_strm1.Close
				GP_strm2.Close
	  	  response.End
      end if
      if ((nameConflict = "over" or nameConflict = "uniq") and GP_FileExist) or (NOT GP_FileExist) then
        if nameConflict = "uniq" and GP_FileExist then
          Begin_Name_Num = 0
          while GP_FileExist    
            Begin_Name_Num = Begin_Name_Num + 1
            'GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FullFileName = GP_curPath & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FileExist = fso.FileExists(GP_FullFileName)
          wend  
          UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
					UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
        end if
        on error resume next
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
          Response.Write "<B>An error has occured saving uploaded file!</B><br/><br/>"
          Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
          Response.Write "Maybe the destination directory does not exist, or you don't have write permission.<br/>"
          Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    end if
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			end if
        on error goto 0
      end if
    end if
  next

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString =""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1))) 
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  end if  
End Function

'START THE UPLOAD REQUEST IF THE APPROPRIATE VARIABLE IS NOT EMPTY
If (CStr(Request.QueryString("GP_upload")) <> "") Then
  GP_redirectPage = ""
  If (GP_redirectPage = "") Then
    GP_redirectPage = CStr(Request.ServerVariables("URL"))
  end if
    
  RequestBin = Request.BinaryRead(Request.TotalBytes)
  Dim UploadRequest
  Set UploadRequest = CreateObject("Scripting.Dictionary")  
  BuildUploadRequest RequestBin, "StandingOrderFiles", "file", "", "uniq"
  
  '*** GP NO REDIRECT
end if  
'END THE UPLOAD REQUEST

Call OpenDB()
'INSERT THE FILE INFORMATION INTO THE DATABASE WITHOUT THE PROCESSING INFORMATION
UploadedFileName = UploadFormRequest("FILENAME")
SQL = "SET NOCOUNT ON;INSERT INTO F_STANDINGORDERFILES (FILENAME, FILETYPE, USERID) VALUES ('" & UploadedFileName & "', 1, " & SESSION("USERID") & ");SELECT SCOPE_IDENTITY() AS NEWID;"
Call OpenRs(rsInsert, SQL)
    NewId = rsInsert("NEWID")
Call CloseRs(rsInsert)

'NEXT DO SOME FILE PROCESSING IF THE FILE HAS BEEN UPLOADED SUCCESSFULLY
If (UploadFormRequest("FILENAME") <> "") Then
	Const ForReading = 1, ForWriting = 2, ForAppending = 8
	Dim CurrentDate 
	CurrentDate = FormatDateTime(Date,1)
	
	TotalGoodCount = 0
	TotalGoodPayment = 0
	
	TotalBadPayment = 0
	TotalBadCount = 0
	
	set fso = server.CreateObject("Scripting.FileSystemObject") 

	Call OpenDB()
    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	    doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS)

    FullPath = doc_DEFAULTPATH & "\finance\StandingOrderFiles\"
	'end of get path

	'open the text file for processing
    Dim ConnExcel
    Set ConnExcel = CreateObject("ADODB.Connection")
    Set RSExcel = CreateObject("ADODB.Recordset")
		
    SQL = "Select * From [Sheet1$]"
    SQL = "Select * From [" & UploadedFileName & "]"	
	ConnExcel.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & FullPath & ";Extended Properties=""text;HDR=Yes;FMT=Delimited"""
    RSExcel.cursortype = 3 ' adStatic.

	on error resume next
    RSExcel.open SQL, ConnExcel

	If Err.Number <> 0 Then
	    RSExcel.close()
		ConnExcel.close()			
		Set RSExcel = Nothing
		Set ConnExcel = Nothing
		'IF THE FILE DOES NOT EXIST, OR THE FILE SHEET NAME IS INVALID
		Set ConnExcel = Nothing
		Dim oFSO
		Dim sMyFile
		
		SQL = "DELETE FROM F_STANDINGORDERFILES WHERE FILEID = " & NewID
		Conn.Execute SQL
		
		'instantiate object
		Set oFSO = Server.CreateObject("Scripting.FileSystemObject")
		
		'physical path to the file name
		sMyFile = FullPath & UploadedFileName 
		
		'check if file really exists
		If oFSO.FileExists (sMyFile) Then
		oFSO.DeleteFile (sMyFile) 'delete file
		End If
		
		'release resources
		Set oFSO = Nothing
		
		Response.Redirect ("../StandingOrderError.asp?Error=ERR1&PROCESSINGDATE=" & FormatDateTime(Date,2))		
	end if

	Dim ValidFields
	Dim ValidExists
	ValidFields = Array("TenancyRef", "Amount", "TransDate", "Details")
	ValidExists = Array(0,0,0,0)
	ValidExists(0) = 0
	ValidExists(1) = 0
	ValidExists(2) = 0
	ValidExists(3) = 0
	ValidExists(4) = 0				
	
	 On Error GoTo 0
	 
	 'this part checks whether there is a vlid set of columns and that they only appear once.
	 if (NOT RSExcel.EOF) then
		For X = 0 To RSExcel.Fields.Count - 1
			 CurrentItem = RSExcel.Fields.Item(X).Name
        	 For i=0 to UBound(ValidFields)
				 if (StrComp(ValidFields(i), CurrentItem, 1) = 0) then
				 	ValidExists(i) = ValidExists(i) + 1
					Exit For
				 end if
			 Next
	    Next
	 end if	 
	 
	 For j=0 to UBound(ValidFields)
	 	if (ValidExists(j) <> 1) then
			RSExcel.close()
			ConnExcel.close()			
			Set RSExcel = Nothing
			Set ConnExcel = Nothing		
			Response.Write "" &  ValidExists(j)		
			'IF THE FILE DOES NOT EXIST, OR THE FILE SHEET NAME IS INVALID
			Set ConnExcel = Nothing
			Dim oFSO1
			Dim sMyFile1
			
			SQL = "DELETE FROM F_STANDINGORDERFILES WHERE FILEID = " & NewID
			Conn.Execute SQL
			
			'instantiate object
			Set oFSO1 = Server.CreateObject("Scripting.FileSystemObject")
			
			'physical path to the file name
			sMyFile1 = FullPath & UploadedFileName 
			
			'check if file really exists
			If oFSO1.FileExists (sMyFile1) Then
			oFSO1.DeleteFile (sMyFile1) 'delete file
			End If
			
			'release resources
			Set oFSO1 = Nothing
			if (ValidExists(j) > 1) then
				Response.Redirect ("../StandingOrderError.asp?Error=ERR3&PROCESSINGDATE=" & FormatDateTime(Date,2))				
			else
				Response.Redirect ("../StandingOrderError.asp?Error=ERR2&PROCESSINGDATE=" & FormatDateTime(Date,2))				
			end if			
		end if		
	 Next
	 
	 RSExcel.moveFirst
     While Not RSExcel.EOF
		  Amount = RSExcel("AMOUNT")
          TenancyID = RSExcel("TenancyRef")
		  Amount = RSExcel("AMOUNT")
		  TransactionDate = RSExcel("TRANSDATE")
		  Details = RSExcel("DETAILS")
		  BADLINE	= ""

			
			'if datediff("d",formatdatetime(TransactionDate,2),formatdatetime(YearStartDate,2)) > 0 then
			'	BADLINE = "  Transaction Date is not within the valid range (" & YearStartDate & "/" & YearEndDate & "). "
			'end if
			
			'if datediff("d",formatdatetime(TransactionDate,2),formatdatetime(YearEndDate,2)) < 0 then
			'	BADLINE = "  Transaction Date is not within the valid range (" & YearStartDate & "/" & YearEndDate & "). "
	  		'end if

			if (NOT isNumeric(Amount)) then
				BADLINE = "  AMOUNT IS EMPTY OR NOT A NUMBER. "
			end if
			if (NOT isDate(TransactionDate)) then
				BADLINE = BADLINE & "  TRANSACTION DATE IS EMPTY OR NOT VALID. "
			end if
			if (isNumeric(TenancyID)) then
				ItemTypeID = 1
				SQL = "SELECT TENANCYID FROM C_TENANCY WHERE TENANCYID = " & TenancyID
				Call OpenRs(rsExists, SQL)
				if (rsExists.EOF) then
					BADLINE = BADLINE & "  CANNOT FIND TENANCY REFERENCE (" & TenancyID & ") IN THE SYSTEM. "
				end if
				CloseRs(rsExists)
			else
				BADLINE = BADLINE & "  INVALID TENANCY REFERENCE (" & TenancyID & "). "				
			end if

			if (BADLINE = "") then
				'THIS IS A VALID LINE...
				if (GoodTotalCount = 0) then
					FilePaymentCode = PaymentCode
					PeriodDate = PaymentDate
				end if

				'INSERT A LINE INTO THE RENT JOURNAL AND GET THE JOURNALID BACK
				SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, PAYMENTSTARTDATE, PAYMENTENDDATE, AMOUNT, ISDEBIT, STATUSID) VALUES ("&_
						"" & TenancyID & ", " &_
						"'" & TransactionDate & "', " &_
						"1,3, " &_
						"'" & TransactionDate & "', " &_
						"'" & TransactionDate & "', " &_
						"" & -Amount & ", " &_
						"0,2); SELECT SCOPE_IDENTITY() AS NEWID; "

				Call OpenRs(rsQuickInsert, SQL)
				NewJournalID = rsQuickInsert("NEWID")
				CloseRs(rsQuickInsert)
				
				'NEXT INSERT A LINE INTO F_PAYMENTCARDDATA SO THAT THE FILE NAME CAN BE MATCHED UPTO THE DATA				
				SQL = "INSERT INTO F_STANDINGORDERDATA (FILEID, JOURNALID, DETAILS) VALUES (" & NewID & ", " & NewJournalID & ", '" & Details & "')"
				Conn.Execute (SQL)
				
				TotalGoodCount = TotalGoodCount + 1
				TotalGoodPayment = TotalGoodPayment + CDbl(Abs(Amount))
			else
				'THIS IS A INCORRECT FILE
				if (TotalBadCount = 0) then
					'IF THIS IS THE FIRST INCORRECT LINE CREATE A TEXT FILE TO STORE THE INFORMATION
					
					UploadedDotPosition = InStrRev(UploadedFileName, ".")
					if (UploadedDotPosition <> 0) then
						UploadedFileName = Left(UploadedFileName, UploadedDotPosition-1) & "_BAD" 
					else
						UploadedFileName = UploadedFileName & "_BAD"
					end if
					
					If (fso.FileExists(FullPath & UploadedFileName & ".txt")) Then
						GP_FileExist = true
					End If   
					if GP_FileExist then
						Begin_Name_Num = 0
						while GP_FileExist    
							Begin_Name_Num = Begin_Name_Num + 1
							TempFileName = UploadedFileName & "_" & Begin_Name_Num
							GP_FileExist = fso.FileExists(FullPath & TempFileName & ".txt")
						wend  
						UploadedFileName = TempFileName
					end if
					Set MyBadFile= fso.CreateTextFile(FullPath & UploadedFileName & ".txt", True)
				
				end if
				MyBadFile.WriteLine TransactionDate & "," & Amount & "," & TenancyID & "," & Details & "  " & BADLINE
				TotalBadCount = TotalBadCount + 1
				if (isNumeric(Amount)) then
					TotalBadPayment = TotalBadPayment + CDbl(Abs(Amount))
				end if
			end if
				
     	RSExcel.MoveNext
	Wend	

	'FINALISE EVERYTHING AND UPDATE THE DATABSE WITH THE PROCESSING DETAILS
	SQL = "UPDATE F_STANDINGORDERFILES SET PROCESSCOUNT = " & TotalGoodCount & ", PROCESSVALUE = " & TotalGoodPayment & " WHERE FILEID = " & NewID
	Conn.Execute (SQL)
	
	if (TotalBadCount > 0) then
		SQL = "INSERT INTO F_STANDINGORDERFILES (FILENAME, PROCESSCOUNT, PROCESSVALUE, FILETYPE, USERID) VALUES ('" & UploadedFileName & ".txt" & "', " & TotalBadCount & ", " & TotalBadPayment & ", 2, " & SESSION("USERID") & ")"
		Conn.Execute(SQL)
	end if
end if

Response.Redirect "../StandingOrderFiles.asp?UploadDate=" & FormatDateTime(Date,2)
%>
