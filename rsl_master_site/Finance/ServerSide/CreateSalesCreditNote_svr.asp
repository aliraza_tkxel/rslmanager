<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" --> 
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
    dim rsSCN, SalesInvoiceID, SCNName, SCNDate, TenancyID, SupplierID, EmployeeID, SalesCustomerID
    SalesInvoiceID = Request("hid_SALESINVOICEID")
    SCNDate = Now()
    
	OpenDB()
	SQL = "BEGIN TRANSACTION SALESCREDITNOTE"
    Conn.Execute SQL
    
 	SQL = "SET NOCOUNT ON;INSERT INTO F_SALESCREDITNOTE ([SCNNAME],[SCNDATE],[INVOICEID],[SCNNOTES],[USERID],[SCNTIMESTAMP]) VALUES " &_
			"('Sales Credit Note against " & SaleNumber(SalesInvoiceID) & "', CONVERT(CHAR(11),GETDATE(), 106), " & SalesInvoiceID & ", NULL, " & Session("USERID") & ", GETDATE())" &_
			";SELECT SCOPE_IDENTITY() AS SCNID, SONAME AS SCNNAME, TENANCYID,SUPPLIERID,EMPLOYEEID,SALESCUSTOMERID FROM F_SALESINVOICE WHERE SALEID = " & SalesInvoiceID & ";SET NOCOUNT OFF"

	Call OpenRs(rsSCN, SQL)	
			
	ScnID = rsSCN("SCNID")
	SCNName = rsSCN("SCNNAME")
	TenancyID = rsSCN("TENANCYID")
	SupplierID = rsSCN("SUPPLIERID")
	EmployeeID = rsSCN("EMPLOYEEID")
	SalesCustomerID = rsSCN("SALESCUSTOMERID")
	CloseRs(rsSCN)

	for each item in Request.Form("ID_ROW")	
		Data = Request.Form("iData" & item)
		DataArray = Split(Data, "||<>||")
		
		ItemRef = Replace(DataArray(0), "'", "''")
		ItemDesc = Replace(DataArray(1), "'", "''")
		VatTypeID = DataArray(2)
		NetCost = DataArray(3)
		VAT = DataArray(4)
		GrossCost = DataArray(5)
		SaleCat = DataArray(6)
		SaveStatus = DataArray(7)										
		RentJournal_id = "NULL"

		'IF A TENANT PUT A TENANT RECHARGE LINE ON TO THE RENT JOURNAL FOR EVERY ITEM.
		if (Not(IsNull(TenancyID))) then   'Tenant Recharge
			SQL = "SET NOCOUNT ON;" &_
			      "INSERT INTO F_RENTJOURNAL (TRANSACTIONDATE, PAYMENTTYPE, ITEMTYPE, TENANCYID, AMOUNT, STATUSID) VALUES (" &_
				    "'" & formatdatetime(SCNDate,2) & "', 91, 5, " & TenancyID & ", " & -GrossCost & ",2);" &_
				"SELECT SCOPE_IDENTITY() AS JOURNALID;"
			set rsRentJourn = Conn.Execute(SQL)
			RentJournal_id = rsRentJourn("JOURNALID")
		end if
		
		SQL = "INSERT INTO F_SALESCREDITNOTEITEM ([SCNID],[SALESCATID],[ITEMNAME],[ITEMDESC],[NETCOST],[VATTYPE],[VAT],[GROSSCOST],[USERID],[SICREATED]) VALUES " &_
				"(" & SCNID & ", " & SaleCat & ", '" & ItemRef & "', '" & ItemDesc & "', " &_
				" " & NetCost & ", " & VatTypeID & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", CONVERT(CHAR(11),GETDATE(), 106) )"
		Conn.Execute SQL
	next
	
	Conn.Execute "EXEC NL_SALESCREDITNOTE " & SCNID
	
	'SQL="EXEC F_SALES_LEDGER_BALANCE '1' , " & TenancyID & " , "  & GrossCost & " "

	'**************************************************************************************************
	' CODE HAS BEEN MODIFIED AS A RESULT OF TASK 4482
	' MODIFIED BY ADNAN MIRZA
	' MODIFIED ON 10/03/2008
	
	if (Not(IsNull(TenancyID))) then
		SQL="EXEC F_SALES_LEDGER_BALANCE '1' , " & TenancyID & " , "  & GrossCost
	elseif  (Not(IsNull(SupplierID))) then
		SQL="EXEC F_SALES_LEDGER_BALANCE '3' , " & SupplierID & " , "  & GrossCost
	elseif  (Not(IsNull(EmployeeID))) then
		SQL="EXEC F_SALES_LEDGER_BALANCE '2' , " & EmployeeID & " , "  & GrossCost
	elseif  (Not(IsNull(SalesCustomerID))) then
		SQL="EXEC F_SALES_LEDGER_BALANCE '4' , " & SalesCustomerID & " , "  & GrossCost
	end if

	
	'**************************************************************************************************
	
	
	Conn.Execute(SQL)

    SQL="IF @@ERROR <> 0  " &_
        "BEGIN  " &_
        "	INSERT INTO NL_ERRORS(TIMESTAMP, ERRORDESC) " &_
        "	VALUES (GETDATE(), 'Transaction Rollback. Insert failed for " & SCNID & "') " &_
        "	ROLLBACK TRANSACTION SALESCREDITNOTE " &_
        "	RETURN " &_
        "END " &_
        "COMMIT TRANSACTION SALESCREDITNOTE" 
    Conn.Execute SQL	
	CloseDB()

	Response.Redirect "../SalesCreditNote.asp"										
%>
   

