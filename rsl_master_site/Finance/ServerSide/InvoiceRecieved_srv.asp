<%@  language="VBSCRIPT" codepage="1252" %>
<%
''''''''''''
'' PAGE NOTE: This is a list of all the repairs that an invoice has been recieved for
''
'' NOTE: Pages to be aware of: InvoiceRecievedList.asp 
''							   InvoiceRecieved_srv.asp 
''						       InvoiceRecieved.asp
''''''''''''

 bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected,disputedInvoice
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split, str_parameters, currentPO,counter,nodeImage, showChild,notEligiblePOs
    'Default Values
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")
    currentPO = REQUEST("CPO")
    counter = 0
    notEligiblePOs = 0
	detotal = Request("detotal")
	if detotal = "" Then detotal = 0 end if
	detotal = detotal
	selected = Request("selected") ''// get selected checkboxes into an array
	response.write selected
	sel_split = Split(selected,",")
	
	office = Request("sel_OFFICE")
	'' GENERATE OFFICE SQL
	
	if office = "" Then 
		office_sql = "" 
	else
		office_sql = " AND CP.OFFICE = " & office & " "
	end if
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
		count = 0	
	
		str_data = str_data & "<table border='0' cellpadding='1' cellspacing='0' style='border-collapse: collapse;border: 1px solid black;'>" &_
								" <tr style='color: Black; background-color:#133e71;'>" &_
								" <td style='width:110px;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>Order #</b></FONT></td>" &_
								" <td style='width:180px;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>Ordered</b></FONT></td>" &_
								" <td style='width:180px;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>By</b></FONT></td>" &_
								" <td style='width:300;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>Supplier</b></FONT></td>" &_
								" <td style='width:260;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>Item</b></FONT></td>" &_
								" <td style='width:150;border: 1px solid black;'align ='center'><FONT style='COLOR:WHITE'><b>Status</b></FONT></td>" &_
								" <td style='width:100;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'><b>Value</b></FONT></td>"&_
								" <td style='width:90;border: 1px solid black;' align ='center'><FONT style='COLOR:WHITE'></FONT></td>" &_
								" </tr>" 
	
	cnt = 0
	POFilter = ""
	If (Request("PONumber") <> "") Then
		If (isNumeric(Request("PONumber"))) Then
			POFilter = " AND PO.ORDERID = '" & Clng(Request("PONumber")) & "' "
		End If
	End If

	SupFilter = ""
	rqSupplier = Request("sel_SUPPLIER")
	If ( rqSupplier <> "") Then
		SupFilter = " AND PO.SUPPLIERID = '" & rqSupplier & "' "
	End If

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if

	rqFiscalYear = ""
	rqFiscalYear = Request("sel_FISCALYEAR")
	If (rqFiscalYear <> "") Then
	    SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & rqFiscalYear
	    Call OpenDB()
	    Call OpenRs(rsFiscalYear, SQL)
	    If (NOT rsFiscalYear.EOF) Then
		    FiscalFilter = " AND (PO.PODATE >= '" & FormatDateTime(rsFiscalYear("YSTART"),1) & "' and PO.PODATE <= '" & FormatDateTime(rsFiscalYear("YEND"),1) & "')"
		End If
	    Call CloseRs(rsFiscalYear)
	    Call CloseDB()
	End If

    dim UserId
    UserId = SESSION("USERID")
    If (isnull(UserId)) Then
        UserId = 0
    End If

            
        ' Main SQL that retrieves all the repairs that are awaiting approval
		strSQL =" DECLARE @POSTATUSID INT " &_
                " DECLARE @FINALPOSTATUSNAME NVARCHAR(100) " &_
                " SELECT @POSTATUSID = PS.POSTATUSID, @FINALPOSTATUSNAME =PS.POSTATUSNAME From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'invoice approved' " &_
                " " &_
                " SELECT DISTINCT " &_
	            " 0 AS JOURNALID, " &_
                " 0 AS WOID, " &_
                " PO.ORDERID AS ORDERID, " &_
                " PO.PODATE AS PODATE, " &_
                " PO.PONAME AS RepairName, " &_
                " O.NAME AS ContractorName, " &_
                " PO.POSTATUS AS POSTATUSID, " &_
                " CONVERT(VARCHAR,ISNULL(PI.TOTALCOST,0),1) AS FORMATTEDCOST,  " & _ 
                " @FINALPOSTATUSNAME AS FINALPOSTATUSNAME, " &_
                " POSTATUS.POSTATUSNAME AS POSTATUSNAME, " &_
                " PO.USERID AS USERID, " &_
                " E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, " & _
                " ISNULL(Invoice.image_url,'') AS image_url " &_
                " FROM F_PURCHASEORDER PO  " &_
                " CROSS APPLY (SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) PI " &_
 	            "   Left JOIN F_PURCHASEITEM P ON P.ORDERID = PO.ORDERID AND PISTATUS IN (7,21)" &_
                "   INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PO.POSTATUS  " &_
                "   LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
                "   LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
                "  CROSS APPLY (SELECT TOP 1 image_url AS image_url FROM F_PURCHASEITEM_LOG POL WHERE POL.ORDERID = PO.ORDERID AND image_url IS NOT NULL " &_
                "  ORDER BY TIMESTAMP DESC) Invoice " &_
                "   Cross Apply (SELECT 1 AS ONE from F_PURCHASEITEM FPI  Where FPI.PISTATUS in(7,21) AND FPI.ORDERID = PO.ORDERID "&_  
				"	AND image_url IS NOT NULL  ) InvoiceUploaded  "&_
                " WHERE PO.ACTIVE = 1"&_ 
                "   AND (( P.APPROVED_BY IS NULL AND P.USERID = " & UserID & ") OR (P.APPROVED_BY= " & UserID & "))"   & SupFilter & CompanyFilter  & POFilter & FiscalFilter & _
                " ORDER BY ORDERID DESC"
        
					' response.write(strSQL)
	 ' response.end
	 ' response.flush
      
        ' str_data = str_data & strSQL
      	if mypage = 0 then mypage = 1 end if
		
		pagesize = 14
	
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
   '//If Record Exist		
   If(numrecs>0) then	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	   
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			'' determine if select box has been previously selected
			isselected = ""
			For each key in sel_split
				response.write "<BR> KEY : " & key & " id : " & Rs("JOURNALID")
				
				If Rs("JOURNALID") = clng(key) Then
					isselected = " checked "
				Exit For
				End If
			Next

			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE                
            if(Not ISNULL(Rs("FINALPOSTATUSNAME"))) then
                str_parameters = Replace(Rs("FINALPOSTATUSNAME"), " ", "_")
								''''''''''''''''''
				SQL = "select POSTATUSNAME from F_POSTATUS where POSTATUSID=21"
				Call OpenDB()
				Call OpenRs(disputeStatus, SQL)
				If (NOT disputeStatus.EOF) Then
					disputedInvoice = disputeStatus("POSTATUSNAME")
					disputedInvoice=Replace(disputedInvoice," ","&")
				End If
				Call CloseRs(disputeStatus)
				Call CloseDB()
					 ' response.write(disputedInvoice)
	 ' response.end
	 ' response.flush
				''''''''''''''''''
				'disputedInvoice="Invoice&Received&-&In&Dispute"
				'disputedInvoice = Replace(disputedInvoice, " ", "&")
            else
                str_parameters = Rs("FINALPOSTATUSNAME")
				''''''''''''''''''
				SQL = "select POSTATUSNAME from F_POSTATUS where POSTATUSID=21"
				Call OpenDB()
				Call OpenRs(disputeStatus, SQL)
				If (NOT disputeStatus.EOF) Then
					disputedInvoice = disputeStatus("POSTATUSNAME")
					disputedInvoice=Replace(disputedInvoice," ","&")
				End If
				Call CloseRs(disputeStatus)
				Call CloseDB()
					' response.write(disputedInvoice)
	 ' response.end
	 ' response.flush
				''''''''''''''''''
				'disputedInvoice="Invoice&Received&-&In&Dispute"
				'disputedInvoice = Replace(disputedInvoice, " ", "&")

            end if
            orderId =  Rs("ORDERID")  
            'imgURL = REPLACE(Rs("PurchaseItemImage")," ","%20")
            userId = Rs("USERID")
            poStatusId = Rs("POSTATUSID")
            'Creating Parent Node
            If ((currentPO = 0 and counter=0) OR (CStr(currentPO) = Cstr(orderId))) Then
             showChild = 1
             nodeImage = "../../Finance/img/minus.gif"
            Else
             nodeImage = "../../Finance/img/plus.gif"
             showChild = 0 
            End If 
            IsInvoiceApproved = 0
			'//Avoiding Reconcile PO
			If(Rs("POSTATUSID")=18) then
			 	  'Checking if all Purchase Items Approved
					SQL =  " SELECT POL.ORDERID FROM F_PURCHASEORDER POL WHERE POL.ORDERID = " & orderId & " AND POL.ACTIVE = 1 AND POL.POSTATUS IN (17)  " 
					OpenDB()
					Call OpenRs_RecCount(rsCheckPOL, SQL)
					countPOLs = rsCheckPOL.RecordCount
					 if(countPOLs>0) then
					 IsInvoiceApproved = 1
					 Rs.movenext()
					 END If
			         CloseDB() 
			         
			End If	
			'str_data = str_data & numpages	
	        If(IsInvoiceApproved = 1) then
	        notEligiblePOs = notEligiblePOs + 1
	        
	        Elseif(IsInvoiceApproved = 0) then
           
            str_data = str_data & "<tr style='border: 1px solid black;cursor:pointer;'>" & _
            "<td style='border: 1px solid black;' align='center'><img src='"&nodeImage&"'' onclick =load_me("&orderId&","&mypage&")><span onclick=show_detail("&orderId&")><b>PO&nbsp;</b>" & orderId & "</span></td>" & _
            "<td style='border: 1px solid black; ' align='center' onclick =show_detail("&orderId&")>" & Rs("PODATE") & "</td>" & _
             "<td style='border: 1px solid black; ' align='center' onclick =show_detail("&orderId&")>" & Rs("FULLNAME") & "</td>" & _
            "<td style='border: 1px solid black; ' align='center' onclick =show_detail("&orderId&")>" & Rs("ContractorName") & "</td>" & _
            "<td style='border: 1px solid black; 'align='center'onclick =show_detail("&orderId&")>"& Rs("RepairName") &"</td>" & _
            "<td style='border: 1px solid black; ' align='center' onclick =show_detail("&orderId&")>" & Rs("POSTATUSNAME") & "</td>" & _
            "<td style='border: 1px solid black; ' align='center' onclick =show_detail("&orderId&")>�" & Rs("FORMATTEDCOST") & "</td>" & _
            "<td></td>" & _
		    "</tr>"
            Rs.movenext()
	           'Adding Child Node
                 If (showChild = 1) Then 
			        refNo = 1
				  
		            str_data = str_data &"<tr style='background:#4681b3;font-size:10px;color:White;font-weight:bold;'>" &_
		                "<td align='center'>Ref:</td>" & _
		                "<td align='center'>Item Name:</td>" & _
		                "<td align='center'>Expenditure:</td>" & _
		                "<td align='center'>Status:</td>" & _
		                "<td align='center'>Net(�):</td>" & _
		                "<td align='center'>VAT(�):</td>" & _
		                "<td align='center'>Gross(�):</td>" & _
		                "<td align='center'></td>" &_
		                "</tr>"
				    'Fetching Child Node
	                SQL =  " SELECT DISTINCT PI.ORDERITEMID, PI.GROSSCOST, PS.POSTATUSNAME, PI.ITEMDESC, PI.ITEMNAME,EX.DESCRIPTION AS EXPENDITURE,PI.NETCOST,PI.VAT, PI.PISTATUS," &_
	                        " ISNULL(Invoice.PurchaseItemImage,'') AS PurchaseItemImage " &_
							    " FROM F_PURCHASEITEM PI " &_							    
                                "INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
							    "INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID " &_
							    "OUTER APPLY (SELECT TOP 1 image_url AS PurchaseItemImage FROM F_PURCHASEITEM_LOG PIL WHERE PIL.ORDERITEMID = PI.ORDERITEMID AND PIL.image_url IS NOT NULL ORDER BY PIL.TIMESTAMP DESC) Invoice " &_
    						    " WHERE PI.ORDERID = " & orderId & " AND PI.ACTIVE = 1  " &_
                         	    "ORDER BY ORDERITEMID ASC"
    			    OpenDB()
				   'str_data = str_data & SQL
				   Call OpenRs_RecCount(rsPI, SQL)
				   totalPIs = rsPI.RecordCount
				  'Checking if all Purchase Items Approved
				  	SQLDISPUTE =  " SELECT PI.ORDERITEMID FROM F_PURCHASEITEM PI WHERE PI.ORDERID = " & orderId & " AND PI.ACTIVE = 1 AND PISTATUS IN ( 8,9,10,11,12,13,14,19,21) " '1,3,18 
					SQL =  " SELECT PI.ORDERITEMID FROM F_PURCHASEITEM PI WHERE PI.ORDERID = " & orderId & " AND PI.ACTIVE = 1 AND PISTATUS IN ( 8,9,10,11,12,13,14,17,19) " '1,3,18 
					'str_data = str_data &SQL
					Call OpenRs_RecCount(rsCheckPI, SQL)
					approvedPIs = rsCheckPI.RecordCount
					Call CloseRs(rsCheckPI)
					Call OpenRs_RecCount(rsCheckPIDISPUTED, SQLDISPUTE)
					disputedPIs = rsCheckPIDISPUTED.RecordCount
					Call CloseRs(rsCheckPIDISPUTED)

				   'End Checking Approved Items	
				   'str_data = str_data &SQL & "<br/>approvedPIs "&approvedPIs &"<br/>totalPIs "&totalPIs
	              'Getting Purchasing Items (with Invoice Received status) of Purchase Order Fetching  
	               counter =1 
	               
				    While NOT rsPI.EOF
					 if(refNo<=9) then
					    refNo ="00"& refNo
					 else
			            refNo ="0"&refNo
					  end if
					 imgURL = REPLACE(rsPI("PurchaseItemImage")," ","%20")
					'str_data =str_data +  strSQL
					    str_data = str_data &"<tr>" &_
						"<td align='center'>" & refNo& "</td>" &_
						"<td align='center'>" & rsPI("ITEMNAME")& "</td>" &_
						"<td align='center'>" & rsPI("EXPENDITURE")& "</td>" &_
						"<td align='center'>" & rsPI("POSTATUSNAME")& "</td>" &_
						"<td align='center'>" & FormatCurrency(rsPI("NETCOST"))& "</td>" &_
						"<td align='center'>" & FormatCurrency(rsPI("VAT"))& "</td>" &_
						"<td align='center'>" & FormatCurrency(rsPI("GROSSCOST"))& "</td>" 
						if((totalPIs - approvedPIs) = 1) then
						  updateParent = 1
					  	else
						  updateParent = 0  
						end if
						
						if((totalPIs-disputedPIs)=1) then	
						DisputedParent =1
						else 
						DisputedParent=0
						end if
						
						if(rsPI("PISTATUS") = 17) then
					    	str_data = str_data &"<td><img style='cursor:pointer; cursor:hand;' src='../../IMAGES/IconAttachment.gif' onclick=window.open('" & imgURL & "');> </td>"
						elseif(rsPI("PISTATUS") = 21) then
						   
					       str_data = str_data & "<td><img style='cursor:pointer; cursor:hand;' src='../../IMAGES/IconAttachment.gif' onclick=window.open('" & imgURL & "');><img style='cursor:pointer; cursor:hand; width:25' src='../../IMAGES/flag .png' title='Currently Disputed';><input id='btn_approve"&counter&"' type='button' value='Approve' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",17,'" & str_parameters & "',1,"&updateParent&","&totalPIs&");>" 
						   'str_data = str_data & "<input id='btn_dispute"&counter&"' type='button' value='Dispute' style=' color: red; width:100%' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",21,'" & disputedInvoice & "',1,"&updateParent&","&totalPIs&") ;></td>" 

							elseif(rsPI("PISTATUS") = 7) then
					       str_data = str_data & "<td><img style='cursor:pointer; cursor:hand;' src='../../IMAGES/IconAttachment.gif' onclick=window.open('" & imgURL & "');><input id='btn_approve"&counter&"' type='button' value='Approve' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",17,'" & str_parameters & "',1,"&updateParent&","&totalPIs&");>" 
						   str_data = str_data & "<input id='btn_dispute"&counter&"' type='button' value='Dispute' style=' color: red' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",21,'" & disputedInvoice & "',1,"&DisputedParent&","&totalPIs&") ;></td>" 
					   	elseif (rsPI("PISTATUS") = 18) then
					   	  SQL =  " SELECT OrderItemID " &_
							    " FROM F_PURCHASEITEM_LOG PIL " &_
                         	    " WHERE PIL.ORDERITEMID = " & rsPI("ORDERITEMID") & " AND PIL.ACTIVE = 1 AND PIL.PISTATUS IN (7) " 
    				       
    				     '  str_data = str_data &SQL
    				       Call OpenRs_RecCount(rsPIStatus, SQL)
				           totalPIStatus = rsPIStatus.RecordCount
    				       if(totalPIStatus>0) then
    				          str_data = str_data & "<td><img style='cursor:pointer; cursor:hand;' src='../../IMAGES/IconAttachment.gif' onclick=window.open('" & imgURL & "');> <input id='btn_approve"&counter&"' type='button' value='Approve' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",17,'" & str_parameters & "',1,"&updateParent&","&totalPIs&");></td>" 
    				          'str_data = str_data & "<input id='btn_approve"&counter&"' type='button' value='Dispute' style=' color: red; width:100%' onclick=updatePOStatus('" & orderId &"-"&rsPI("ORDERITEMID")& "'," & userId & ",21,'" & disputedInvoice & "',1,"&updateParent&","&totalPIs&");></td>" 

						   end if 
					   	    Call CloseRs(rsPIStatus)
                        end if						
                       str_data = str_data & "</tr>" 
					     rsPI.moveNext
					        refNo = refNo+1
					    counter = counter + 1   
					  Wend
					      Call CloseRs(rsPI)
					        'Getting Total of Purchase Items
				             SQL =  " SELECT Sum(PI.GROSSCOST) As TotalGrossCost,Sum(PI.NETCOST) as TotalNetCost,Sum(PI.VAT) as TotalVAT" &_
							    " FROM F_PURCHASEITEM PI " &_
                         	    " WHERE PI.ORDERID = " & orderId & " AND PI.ACTIVE = 1  " 
    				        Call OpenRs(rsTotalPI, SQL)
    				        
					        If(Not rsTotalPI.EOF) Then
				                str_data = str_data & "<TR style='font-size:10px;border-style:none;text-align:center;border: 1px solid black;'>"&_
							    "<TD align=right style='background:#4681b3;' colspan=4>"  &_
							        "<span style='text-align=right;color:white;font-size:11px;font-weight:bold;'>Total:</span>"&_
							    "</TD>"&_
							    "<TD align='center' style='font-weight:bold;'>"&rsTotalPI("TotalNetCost")&"</TD>" &_
							    "<TD align='center' style='font-weight:bold;'>"& rsTotalPI("TotalVAT")&"</TD>"&_
							    "<TD align='center' style='font-weight:bold;'>"& rsTotalPI("TotalGrossCost") &"</TD>"&_
							   	"</TR>"
							End If	
				         
				    CloseDB()     
				    End If ' Child main If     
			        'End Adding Child Node
			    
			     End If '
			         counter = counter + 1
	         
	        End If
        Next
       End If 'If Main record exist 
      	'Call fillgaps(pagesize - cnt)
         
		If cnt = 0 Then 
			str_data = str_data & "<tr><td COLSPAN=6 WIDTH=100% ALIGN=CENTER><b>No matching records exist !!</b></td></tr>" &_
						"<tr style='HEIGHT:3PX'><td></td></tr>"
		Else
			         
            str_data = str_data & "<tr style='display:none'><td COLSPAN=6 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></td>" &_
				"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></tr>"

			'EndButton = "<table WIDTH='100%'><tr><td colspan=4 align=right>" &_
			'					"<INPUT TYPE=BUTTON VALUE='Approve Repair' CLASS='rslbutton' onclick='javascipt:updatePOStatus(this);' style='dispay:none'>" &_
			'					"</td></tr></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=9 align=center>" &_
			"		<table style='width:100%;' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/InvoiceRecieved_srv.asp?page=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/InvoiceRecieved_srv.asp?page=" & prevpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/InvoiceRecieved_srv.asp?page=" & nextpage& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='ServerSide/InvoiceRecieved_srv.asp?page=" & numpages& "&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_OFFICE=" & office & NextString & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='ServerSide/InvoiceRecieved_srv.asp?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & "&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"

	End function
	
	'' pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<tr><td COLSPAN=7 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></td></tr>"
			cnt = cnt + 1
		wend		
	
	End Function
%>
<html>
<head>
    <title></title>

</head>
<script type="text/javascript" language="javascript">
    function ReturnData() {
       
        parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
      
    }
</script>
<body onload="ReturnData()">
    <div id="ReloaderDiv">
        <%=str_data%>
        <!--<%=EndButton%>-->
    </div>
</body>
</html>
