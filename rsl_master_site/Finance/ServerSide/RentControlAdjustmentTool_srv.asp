<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim action, tnum, str_data, isValid, success, redir, str_redirection, attach_id, ErrorText,which,enabletext
	
	Const NL_DEBIT_ID=14
	Const NL_CREDIT_ID=14
	
	
	action = Request("action")
	which=Request("which")
	if(which=1) then 
	    tnum = Request.form("txt_TENANTSEARCH")
	else
	    tnum = Request.form("txt_TENANTSEARCH1")
	end if
	if (tnum = "") then tnum = -1
	
	attach_id = Request("hid_ATTACHID")
	Response.Write attach_id
	 
	isValid = 0
	enabletext=0
	str_data = ""
	success = 0
	redir = 0
	ErrorText = ""
	
	OpenDB()
	
	if action = "GET" Then 
		getTenantInfo() 
		ELSE 
			post() 
	end If
	
	CloseDB()

				
	Function getTenantInfo()
		'WHY WASTE TIME RUNNING THE SQL....
		if (tnum <> -1) then
		
			'START OF GET CUSTOMER DETAILS....
			CustSQL = "SELECT CT.CUSTOMERID, REPLACE(ISNULL(TI.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
					"FROM C_CUSTOMERTENANCY CT " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE TI ON TI.TITLEID = C.TITLE " &_						
					"WHERE CT.ENDDATE IS NULL AND CT.TENANCYID = " & tnum
			Call OpenRs(rsCust, CustSQL)
			strUsers = ""
			CustCount = 0
			while NOT rsCust.EOF 
				if CustCount = 0 then
					strUsers = strUsers & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
					CustCount = 1
				else
					strUsers = strUsers & ", " & "<A HREF='../../Customer/CRM.asp?CustomerID=" & rsCust("CUSTOMERID") & "'>" & rsCust("FULLNAME") & "</a>"
				end if					
				rsCust.moveNext()
			wend
			CloseRs(rsCust)
			SET rsCust = Nothing
			'END OF GET CUSTOMER DETAILS

			strSQL = "SELECT T.TENANCYID AS TENANCYID, " &_
					"ISNULL(T.TENANCYID, '') AS TENANCYREF, " &_
					"ISNULL(P.PROPERTYID, '') AS PROPERTYID, " &_
					"ISNULL(P.HOUSENUMBER, '') AS HOUSENUMBER, " &_
					"ISNULL(P.ADDRESS1 , ' ' ) AS ADDRESS1, " &_
					"ISNULL(P.ADDRESS2, ' ' ) AS ADDRESS2, " &_
					"ISNULL(P.ADDRESS3, ' ' ) AS ADDRESS3, " &_		
					"ISNULL(P.TOWNCITY , ' ' ) AS TOWNCITY, " &_
					"ISNULL(P.POSTCODE , ' ' ) AS POSTCODE, " &_
					"ISNULL(P.COUNTY , ' ' ) AS COUNTY " &_
					"FROM C_TENANCY T " &_
					"INNER JOIN P__PROPERTY P ON T.PROPERTYID = P.PROPERTYID " &_
					"WHERE T.TENANCYID = " & tnum
													
			Call OpenRS(rsSet, strSQL)	
			
			If not rsSet.EOF Then
			
            If(which=1) then
            enabletext=1
			elseif(which=2) then
			    isValid = 1
			end if 
			
				str_data = "<table><TR><TD width=80px heigt=80px><B>Tenancy No :</B></TD><TD>" & rsSet("TENANCYID") & "</TD></TR>" &_
								"<TR><TD width=80px><B>Name :</B></TD><TD>" & strUsers & "</TD></TR>" &_
								"<TR><TD valign=top><B>Address:</B></TD><TD>" & rsSet("HOUSENUMBER") & " " & rsSet("ADDRESS1") & " "& rsSet("ADDRESS2") & " "& rsSet("ADDRESS3") & " "& rsSet("TOWNCITY") & " "& rsSet("COUNTY") &" "& rsSet("POSTCODE") &  "</TD></TR>" &_
								"</TABLE>"								
			End if
			CloseRs(rsSet)
		End if
		
		If str_data = "" then
			str_data = "<table valign=top><TR><TD>No Matching Records!!</TD></TR></TABLE>"
			tnum = ""
		End If
		
	End Function
	function post()
	    
	    Dim hid_Misposted_TenantNumber,hid_To_Be_Posted_TenantNumber,TransactionType
	    
	    hid_Misposted_TenantNumber = Request.form("hid_TENANTNUMBER") 
	    hid_To_Be_Posted_TenantNumber = Request.form("hid_TENANTNUMBER1")
	    TransactionType=8 'THIS STANDS FOR RENT RECIEPT
	    
	    // format dates if provided
		to_date = "NULL"
		from_date = "NULL"
		if Request.Form("txt_FROM") <> "" Then
			from_date = "'" & FormatDateTime(Request.Form("txt_FROM"),1) & "'"
		end if
		if Request.Form("txt_TO") <> "" Then
			to_date = "'" & FormatDateTime(Request.Form("txt_TO"),1) & "'"
		end if
		
		// determine whether or not to redirect to CRM page
		redir = Request.Form("redir")
		if redir = "" then redir = 0 end if

		 // get details of chosen payment method
		    strSQL = "SELECT * FROM F_DIRECTPOSTITEMS WHERE ITEMID = " & Request.Form("sel_DIRECTPOSTITEMS")
		    Call OpenRs(rsSet, strSQL)
		
		// write journal entry for direct posting
		If not rsSet.EOF Then
			
			item_type = rsSet("F_ITEMTYPE")
			payment_type = rsSet("F_PAYMENTTYPE")
			f_status = 13'rsSET("F_STATUS")
			f_direction = request.Form("hid_ISDEBIT") 
			f_amount=Request.Form("txt_AMOUNT")
			payment_description= rsSet("ITEMNAME")
			'DebitId=Request.Form("hid_DEBITID")
			'CreditId=Request.Form("hid_CREDITID") 
			
			'if(Request.Form("sel_DIRECTPOSTITEMS")=66) then
			    if(f_amount<0) then
			        f_direction=1
			    else
			        f_direction=0
			    end if
			'end if
			
			isdebit=f_direction
			
			'inserting value from into the tenancy to be posted
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL  " &_
			"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID, PAYMENTSTARTDATE, PAYMENTENDDATE, ISDEBIT ) " &_
			" VALUES (" &_
						Request.Form("hid_TENANTNUMBER1") & ", '" &_
						FormatDateTime(Request.Form("txt_PROCESSDATE"),1) & "', " &_
						item_type & ", " &_
						payment_type & ", " &_
						f_amount & ", " &_
						f_status & ", " &_
						from_date & ", " &_
						to_date & ", " &_
						isdebit & "" &_						
						"); select SCOPE_IDENTITY() AS NEWID;"
			Call OpenRs(rsID, SQL)
			To_be_Posted_NEWID = rsID("NEWID")
			CloseRS(rsID)
			
	    	    if(f_amount<0) then
			        f_direction=0
			    else
			        f_direction=1
			    end if
			
			if f_direction = 0 then 
			    isdebit = 1 
			    f_amount=abs(f_amount)
			    CreditTenenacyId=hid_To_Be_Posted_TenantNumber
			    DebitTenancyId=hid_Misposted_TenantNumber
			else 
			    isdebit = 0 
			    f_amount=-f_amount
			    CreditTenenacyId=hid_Misposted_TenantNumber
			    DebitTenancyId=hid_To_Be_Posted_TenantNumber
			end if
			
			'inserting value from into the misposted tenancy 
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL  " &_
			"(TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID, PAYMENTSTARTDATE, PAYMENTENDDATE, ISDEBIT ) " &_
			" VALUES (" &_
						Request.Form("hid_TENANTNUMBER") & ", '" &_
						FormatDateTime(Request.Form("txt_PROCESSDATE"),1) & "', " &_
						item_type & ", " &_
						payment_type & ", " &_
						f_amount & ", " &_
						f_status & ", " &_
						from_date & ", " &_
						to_date & ", " &_
						isdebit & "" &_						
						"); select SCOPE_IDENTITY() AS NEWID;"
			Call OpenRs(rsID, SQL)
			Misposted_NEWID = rsID("NEWID")
			CloseRS(rsID)
			
			'inserting value into F_RENTJOURNAL_MISPOSTING to get one journal id for both F_RENTJOURNAL for Journal entry 
			SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL_MISPOSTED  " &_
			"(MISPOSTED_JOURNALID,TO_BE_POSTED_JOURNALID) " &_
			" VALUES (" &_
						Misposted_NEWID & "," &_
						To_be_Posted_NEWID & ");" &_
						" select SCOPE_IDENTITY() AS NEWID;"
			Call OpenRs(rsID, SQL)
			JOURNALID = rsID("NEWID")
			CloseRS(rsID)
							
			'Create Journal entry
	        SQL = "SET NOCOUNT ON;INSERT INTO NL_JOURNALENTRY   " &_
			"(TRANSACTIONTYPE, TRANSACTIONID, TXNDATE) " &_
			" VALUES (" &_
						TransactionType & "," &_
						JOURNALID & ", " &_
						"CONVERT(VARCHAR, GETDATE(), 103)" & "" &_						
						"); select SCOPE_IDENTITY() AS NEWID;"
			Call OpenRs(rsJR, SQL)
			TXNID = rsJR("NEWID")
			CloseRS(rsJR)
			
			'Insert into Credit table
	        SQL = "INSERT INTO NL_JOURNALENTRYCREDITLINE (TXNID, ACCOUNTID, EDITSEQUENCE, TXNDATE, AMOUNT, DESCRIPTION)  " &_
			      " VALUES ( " & TXNID & " , " & NL_CREDIT_ID & ",1,CONVERT(VARCHAR, GETDATE(), 103) , " & abs(f_amount) & ", CAST( '" & CreditTenenacyId & ":" & payment_description & "' AS VARCHAR))"
			Conn.Execute (SQL)

			'Insert into Debit table
	        SQL = "INSERT INTO NL_JOURNALENTRYDEBITLINE  (TXNID, ACCOUNTID, EDITSEQUENCE, TXNDATE, AMOUNT, DESCRIPTION)  " &_
			      " VALUES ( " & TXNID & " , " & NL_DEBIT_ID & ",2,CONVERT(VARCHAR, GETDATE(), 103) , " & abs(f_amount) & ", CAST( '" & DebitTenancyId & ":" & payment_description & "' AS VARCHAR))"
			Conn.Execute (SQL)
					
	    end if
	    CloseRs(rsSet)
	    
	   // if redirection is chosen
		
		if redir = 1 Then
	
			'get customer id to enable redirection to customer page
			strSQL = "SELECT CUSTOMERID FROM C_CUSTOMERTENANCY WHERE TENANCYID = " & Request.Form("hid_TENANTNUMBER1")
			Call OpenRs(rsSet, strSQL)
			if not rsSet.EOF then str_redirection = rsSet(0) end if
			CloseRs(rsSet)
			
			// REDIRECT
			str_redirection = "../../customer/CRM.asp?CustomerID=" & str_redirection
			
		End If
		
	end function
	
	
	// change the status of the attached journal entry
	Function change_entry_status(int_f_status)
	
		SQL = "UPDATE F_RENTJOURNAL SET STATUSID = " & int_f_status & " WHERE JOURNALID = " & attach_id
		Conn.Execute(SQL)
		
	End Function
	
%>
<html>
<head></head>
<script type="text/javascript" language="javascript">
function ReturnData(){
<% if ErrorText = "" then %>	
	// if actual posting
	<% if str_data = "" then %>	
		// redirect to CRM if required
		if (<%=redir%> != 0){
			parent.location.href = "<%=str_redirection%>"
			}
		
		parent.details.innerHTML = "&nbsp;";
		parent.details1.innerHTML = "&nbsp;";
		
		parent.ACCOUNT_DIV.innerHTML = "";
		parent.RSLFORM.reset();
		parent.RSLFORM.Detach.style.visibility = "hidden"
		parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
		parent.RSLFORM.btn_POST.disabled = true;
		parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Successful...</B></FONT>";
	<% else %>
		// otherwise tenant data retrieval
		parent.ATTACHID = 0;
		<%if which=1  then %>
		    parent.details.innerHTML = "<%=str_data%>";
		<%else%>
		   parent.details1.innerHTML = "<%=str_data%>"; 
		<%end if %>  
		parent.RSLFORM.sel_DIRECTPOSTITEMS.value = "";
		parent.RSLFORM.txt_AMOUNT.value = "";
		parent.RSLFORM.hid_TENANTNUMBER.value = "<%=tnum%>";
		parent.STATUS_DIV.style.visibility = "hidden";
		parent.is_a_valid_tenancy = <%=isValid%>;
		if(<%=isValid%>==1){
			parent.RSLFORM.btn_POST.disabled = false;
			parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;
			parent.RSLFORM.txt_AMOUNT.disabled=false;
			parent.RSLFORM.txt_PROCESSDATE.disabled=false;
			parent.RSLFORM.txt_FROM.disabled=false;
			parent.RSLFORM.txt_TO.disabled=false;				
			}
		else if(<%=enabletext%>==1) {
		    parent.RSLFORM.btn_FINDT1.disabled = false;
			parent.RSLFORM.txt_TENANTSEARCH1.disabled = false;
		
		}
		else {
			parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = true;
			parent.RSLFORM.btn_POST.disabled = true;		
			}
	<% end if %>
<% else %>
	alert("<%=ErrorText%>");
	parent.STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Try again in a few seconds...</B></FONT>";	
	parent.RSLFORM.sel_DIRECTPOSTITEMS.disabled = false;
	parent.RSLFORM.btn_POST.disabled = false;		
<% end if %>	
	}
</script>
<body onload="ReturnData();">
</body>
</html>