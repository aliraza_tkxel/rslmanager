<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	insert_list = Request.Form("idlist")
	detotal_credit = Request("detotal_credit")
	detotal_debit = Request("detotal_debit")
	bank_statement = Request("sel_STATEMENT")
	
	OpenDB()

	strSQL = 	"SET NOCOUNT ON;" &_	
		"INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
		"VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
		"SELECT SCOPE_IDENTITY() AS BTID;"

	set rsSet = Conn.Execute(strSQL)
	bt_id = rsSet.fields("BTID").value
	rsSet.close()
	set rsSet = Nothing 
	
	sel_split = Split(insert_list ,",") ' split selected string
	rec_type = 1 'This means these items are from the rent journal
	TimeStamp = Now()
	method = 1 'Manual Validation
	
	For each key in sel_split
		
		'SQL = "SELECT * FROM F_BANK_RECONCILIATION
		strSQL = "INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD) VALUES " &_
					"(" & bt_id & ", " & bank_statement & ", " & rec_type & ", " & key & ",'" & TimeStamp & "'," & Session("USERID") & "," & method & ")"
		Conn.Execute(strSQL)
		
	Next

	CloseDB()

	Response.Redirect "BankValidation_svr.asp?RESET=1&sel_STATEMENT=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_FROM") & "&txt_TO=" & Request("txt_TO") & "&sel_PAYMENTTYPE=" & Request("sel_PAYMENTTYPE") & "&txt_TENANCYID=" & Request("txt_TENANCYID") & "&sel_TYPE=" & Request("sel_TYPE")	
%>
