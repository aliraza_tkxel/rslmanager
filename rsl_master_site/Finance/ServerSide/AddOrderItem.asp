<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	Dim ExpDate
	if (Request("ReturnTo") = "1" OR Request("ReturnTo") = "2") then 'from reconcile page
		LOGPAGETEXT = "(RECONCILE PAGE)"
	else
		LOGPAGETEXT = "(AMEND PAGE)"
	end if

	ORDERID = REQUEST("ORDERID")
		
	ItemRef = Replace(Request("txt_ITEMREF"), "'", "''")
	ItemDesc = Replace(Request("txt_ITEMDESC"), "'", "''")
	VatTypeID = Request("sel_VATTYPE")
	NetCost = Request("txt_NETCOST")
	VAT = Request("txt_VAT")
	GrossCost = Request("txt_GROSSCOST")
	ExpenditureID = Request("sel_EXPENDITURE")
    SaveStatus = Request("hd_SaveStatus")
    SchemeId = Request("sel_SCHEME")
    BlockId = Request("sel_BLOCK")

	UserSessionID = SESSION("USERID")
	OpenDB()
    
	ExpDate = ""
	SQL = "SELECT EXPPODATE FROM F_PURCHASEORDER WHERE ORDERID = "	& ORDERID		
    Call OpenRs(rsPO, SQL)
    while (NOT rsPO.EOF)       
		If (rsPO("EXPPODATE") <> "")then
			ExpDate = "'" & FormatDateTime(rsPO("EXPPODATE"),1) & "'"
		end if
        rsPO.moveNext
    wend

    if(ExpDate <> "") then
		SQL = "INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS,REFERENCENO) VALUES " &_
				"(" & OrderID & ", " & ExpenditureID & ", '" & ItemRef & "', '" & ItemDesc & "', getdate(), " & ExpDate & ", " &_
				" " & NetCost & ", " & VatTypeID & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 1, "& SaveStatus &", 1)"
	else
		SQL = "INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS,REFERENCENO) VALUES " &_
		"(" & OrderID & ", " & ExpenditureID & ", '" & ItemRef & "', '" & ItemDesc & "', getdate(), null, " &_
		" " & NetCost & ", " & VatTypeID & ", " & VAT & ", " & GrossCost & ", " & Session("USERID") & ", 1, 1, "& SaveStatus &", 1)"
	end if
    
	Conn.Execute (SQL)

    SQL = "SELECT PIStatus FROM F_PURCHASEITEM WHERE ORDERID = " & ORDERID
    Call OpenRs(rsPI, SQL)
    while (NOT rsPI.EOF)       
		PIStatus = rsPI("PIStatus")
        if( PIStatus < SaveStatus) then     
            SaveStatus = PIStatus
        end if
        rsPI.moveNext
    wend

	SQL = "UPDATE F_PURCHASEORDER SET IsAmend = 1, POStatus = " & SaveStatus
    if(SchemeId <> "") then
    SQL = SQL + " ,SchemeId = " & SchemeId
    else
    SQL = SQL + " ,SchemeId = null"
    end if

    if(BlockId <> "") then
    SQL = SQL + " ,BlockId = " & BlockId
    else
    SQL = SQL + " ,BlockId = null"
    end if
    
	SQL = SQL + " WHERE ORDERID = " & ORDERID
	
	
	Conn.Execute(SQL)
'	 response.write(SQL)
'	 response.end
'	 response.flush
	
	CloseDB()
	
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_Parent() {
            parent.parentReload();
        }
    </script>
</head>
<body onload="Return_Parent();">
</body>
</html>
