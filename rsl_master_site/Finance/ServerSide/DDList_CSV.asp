<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
POFilter = ""
If (Request("PROCESSINGDATE") <> "") Then
	ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
	RequiredDate = CDate(Request("PROCESSINGDATE"))
Else
	ProcessingDate = FormatDateTime(CDate(Date),2)
	RequiredDate = Date
End If

ORIGINATORNUMBER = "658670"
orderBy = "ACCOUNTNAME ASC"

	if (RequiredDate > Date) then
		ReturnType = "Forecast"
		SQLCODE = "SELECT CASE WHEN LEN(CAST(T.TENANCYID AS NVARCHAR)) < 6 THEN '0' + " &_
					"CAST(T.TENANCYID AS NVARCHAR) ELSE CAST(T.TENANCYID AS NVARCHAR) END AS TENANCYID, " &_
					"DD.ACCOUNTNAME, " &_
					"CAST(DD.SORTCODE AS NVARCHAR) AS SORTCODE, CAST(DD.ACCOUNTNUMBER AS NVARCHAR) AS ACCOUNTNUMBER, " &_
					"PAYMENTAMOUNT = CASE " &_
					"WHEN DD.INITIALDATE = '" & FormatDateTime(RequiredDate,1) & "' THEN DD.INITIALAMOUNT " &_
					"ELSE DD.REGULARPAYMENT " &_
					"END, " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
					"FROM F_DDSCHEDULE DD " &_
					"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
					"	WHERE DAY(PAYMENTDATE) = DAY('" & FormatDateTime(RequiredDate,1) & "') AND PERIODBALANCE <> 0 AND ISNULL(SUSPEND,0) <> 1  AND ISNULL(TEMPSUSPEND,0) <> 1 AND PAYMENTDATE <= '" & FormatDateTime(RequiredDate,1) & "' " &_
					"		AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''  " &_
					"		AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
					"		AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_					
				    "		ORDER BY " + Replace(orderBy, "'", "''") + ""
	else
	'the sql below shows the direct debit transactions which have occured.
		ReturnType = "Actual"
		SQLCODE = "SELECT CASE WHEN LEN(CAST(T.TENANCYID AS NVARCHAR)) < 6 THEN '0' + " &_
					"CAST(T.TENANCYID AS NVARCHAR) ELSE CAST(T.TENANCYID AS NVARCHAR) END AS TENANCYID,  " &_
					"DD.ACCOUNTNAME, " &_
					"CAST(DD.SORTCODE AS NVARCHAR) AS SORTCODE, CAST(DD.ACCOUNTNUMBER AS NVARCHAR) AS ACCOUNTNUMBER, " &_
					"ISNULL(ABS(RJ.AMOUNT),0) AS PAYMENTAMOUNT, " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
					"FROM F_RENTJOURNAL RJ " &_
					"INNER JOIN F_RENTJOURNALDDSCHED RJD ON RJ.JOURNALID = RJD.JOURNALID " &_
					"INNER JOIN F_DDSCHEDULE DD ON RJD.DDSCHEDULEID = DD.DDSCHEDULEID " &_
					"INNER JOIN C_TENANCY T ON DD.TENANCYID = T.TENANCYID " &_
					"	WHERE RJ.STATUSID = 2 AND RJ.TRANSACTIONDATE = '" & FormatDateTime(RequiredDate,1) & "' " &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""
	end if	

Set fso = CreateObject("Scripting.FileSystemObject")

'GP_curPath = Request.ServerVariables("PATH_INFO")
'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
'	GP_curPath = GP_curPath & "/"
'end if 

    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenDB()
    Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS)
    Call CloseDB() 

CurrentDate = FormatDateTime(Date,2)
CurrentDate = Replace(CurrentDate, "/", "")
CurrentDate = Replace(CurrentDate, " ", "")

RedirectProcessingDate = ProcessingDate
ProcessingDate = Replace(ProcessingDate, "/", "")
ProcessingDate = Replace(ProcessingDate, " ", "")

'FullPath = Trim(Server.mappath(GP_curPath)) & "\CSVFiles\"
FullPath = doc_DEFAULTPATH & "\finance\DDFiles\"
FileName = "DD-C" & CurrentDate & "-" & "P" & ProcessingDate

If (fso.FileExists(FullPath & FileName & ".csv")) Then
	GP_FileExist = true
End If   
if GP_FileExist then
	Begin_Name_Num = 0
	while GP_FileExist    
		Begin_Name_Num = Begin_Name_Num + 1
		TempFileName = FileName & "_" & Begin_Name_Num
		GP_FileExist = fso.FileExists(FullPath & TempFileName & ".csv")
	wend  
	FileName = TempFileName
end if
Set MyFile= fso.CreateTextFile(FullPath & FileName & ".csv", True)


OpenDB()
Call OpenRS(rsLoader, SQLCODE)

BadDataCount = 0
BadDataTotal = 0
GoodDataCount = 0
GoodDataTotal = 0
'MyFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"
while NOT rsLoader.EOF
	SORTCODE = rsLoader("SORTCODE")
	ACCOUNTNUMBER = rsLoader("ACCOUNTNUMBER")
	ACCOUNTNAME = rsLoader("ACCOUNTNAME")
	if (SORTCODE = "" OR ACCOUNTNUMBER = "" OR ACCOUNTNAME = "" OR isNull(ACCOUNTNAME) OR isNULL(SORTCODE) OR isNULL(ACCOUNTNUMBER)) then
		if (BadDataCount = 0) then
			Set MyBadFile= fso.CreateTextFile(FullPath & FileName & "-BAD.csv", True)
			'MyBadFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"						
		end if
		BadDataCount = BadDataCount + 1
		MyBadFile.WriteLine rsLoader("TENANCYID") & "," & SORTCODE & ","  & ACCOUNTNUMBER & "," & ACCOUNTNAME & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ORIGINATORNUMBER		
		BadDataTotal = BadDataTotal + rsLoader("PAYMENTAMOUNT")		
	else
		GoodDataCount = GoodDataCount + 1
		MyFile.WriteLine rsLoader("TENANCYID") & "," & SORTCODE & ","  & ACCOUNTNUMBER & "," & ACCOUNTNAME & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ORIGINATORNUMBER
		GoodDataTotal = GoodDataTotal + rsLoader("PAYMENTAMOUNT")				
	end if
	rsLoader.moveNext
wend
CloseRs(rsLoader)
MyFile.Close
if (BadDataCount > 0) then
	MyBadFile.Close
end if


'fINALLY ADD A ROW INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
Conn.Execute ("INSERT INTO F_DDFILES (FILEDATE, FILENAME, FILETYPE, FILECOUNT, FILEAMOUNT, PROCESSDATE) VALUES (" &_
				"'" & FormatDateTime(RequiredDate,1) & "', " &_
				"'" & FileName & ".csv" & "', " &_
				"" & 1 & ", " &_
				"" & GoodDataCount & ", " &_
				"" & GoodDataTotal & ", " &_
				"'" & FormatDateTime(Date,1) & "') ")

IF (BadDataCount > 0) THEN
	'also ADD A ROW FOR BAD DATA INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
	Conn.Execute ("INSERT INTO F_DDFILES (FILEDATE, FILENAME, FILETYPE, FILECOUNT, FILEAMOUNT, PROCESSDATE) VALUES (" &_
				"'" & FormatDateTime(RequiredDate,1) & "', " &_
				"'" & FileName & "-BAD.csv" & "', " &_
				"" & 2 & ", " &_
				"" & BadDataCount & ", " &_
				"" & BadDataTotal & ", " &_
				"'" & FormatDateTime(Date,1) & "') ")																								
END IF

CloseDB()
Response.Redirect "../DDCSV.asp?PROCESSINGDATE=" & RedirectProcessingDate
%>
