<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess= true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
  'Updated By: Umair Naeem
  'Update Date: 04/06/2008
  'Update Reason: One of the line in the code was using formatedatetime to format date and comparing it directly.
  '               Now it is using CDate to convert into date and using DateDiff to compare it.

	SQL = "EXEC GET_VALIDATION_PERIOD 5"
	Call OpenDB()
	Call OpenRs(rsTAXDATE, SQL)
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)
	'Call CloseDB()

Sub BuildUploadRequest(RequestBin,UploadDirectory,storeType,sizeLimit,nameConflict)
  'Get the boundary
  PosBeg = 1
  PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
  if PosEnd = 0 then
    Response.Write "<b>Form was submitted with no ENCTYPE=""multipart/form-data""</b><br/>"
    Response.Write "Please correct the form attributes and try again."
    Response.End
  end if
  'Check ADO Version
	set checkADOConn = Server.CreateObject("ADODB.Connection")
	adoVersion = CSng(checkADOConn.Version)
	set checkADOConn = Nothing
	if adoVersion < 2.5 then
    Response.Write "<b>You don't have ADO 2.5 installed on the server.</b><br/>"
    Response.Write "The File Upload extension needs ADO 2.5 or greater to run properly.<br/>"
    Response.Write "You can download the latest MDAC (ADO is included) from <a href=""www.microsoft.com/data"">www.microsoft.com/data</a><br/>"
    Response.End
	end if		
  'Check content length if needed
	Length = CLng(Request.ServerVariables("HTTP_Content_Length")) 'Get Content-Length header
	If "" & sizeLimit <> "" Then
    sizeLimit = CLng(sizeLimit)
    If Length > sizeLimit Then
      Request.BinaryRead (Length)
      Response.Write "Upload size " & FormatNumber(Length, 0) & "B exceeds limit of " & FormatNumber(sizeLimit, 0) & "B"
      Response.End
    End If
  End If
  boundary = MidB(RequestBin,PosBeg,PosEnd-PosBeg)
  boundaryPos = InstrB(1,RequestBin,boundary)
  'Get all data inside the boundaries
  Do until (boundaryPos=InstrB(RequestBin,boundary & getByteString("--")))
    'Members variable of objects are put in a dictionary object
    Dim UploadControl
    Set UploadControl = CreateObject("Scripting.Dictionary")
    'Get an object name
    Pos = InstrB(BoundaryPos,RequestBin,getByteString("Content-Disposition"))
    Pos = InstrB(Pos,RequestBin,getByteString("name="))
    PosBeg = Pos+6
    PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(34)))
    Name = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
    PosFile = InstrB(BoundaryPos,RequestBin,getByteString("filename="))
    PosBound = InstrB(PosEnd,RequestBin,boundary)
    'Test if object is of file type
    If  PosFile<>0 AND (PosFile<PosBound) Then
      'Get Filename, content-type and content of file
      PosBeg = PosFile + 10
      PosEnd =  InstrB(PosBeg,RequestBin,getByteString(chr(34)))
      FileName = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      FileName = Mid(FileName,InStrRev(FileName,"\")+1)
      'Add filename to dictionary object
      UploadControl.Add "FileName", FileName
      Pos = InstrB(PosEnd,RequestBin,getByteString("Content-Type:"))
      PosBeg = Pos+14
      PosEnd = InstrB(PosBeg,RequestBin,getByteString(chr(13)))
      'Add content-type to dictionary object
      ContentType = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      UploadControl.Add "ContentType",ContentType
      'Get content of object
      PosBeg = PosEnd+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = FileName
      ValueBeg = PosBeg-1
      ValueLen = PosEnd-Posbeg
    Else
      'Get content of object
      Pos = InstrB(Pos,RequestBin,getByteString(chr(13)))
      PosBeg = Pos+4
      PosEnd = InstrB(PosBeg,RequestBin,boundary)-2
      Value = getString(MidB(RequestBin,PosBeg,PosEnd-PosBeg))
      ValueBeg = 0
      ValueEnd = 0
    End If
    'Add content to dictionary object
    UploadControl.Add "Value" , Value	
    UploadControl.Add "ValueBeg" , ValueBeg
    UploadControl.Add "ValueLen" , ValueLen	
    'Add dictionary object to main dictionary
    UploadRequest.Add name, UploadControl	
    'Loop to next object
    BoundaryPos=InstrB(BoundaryPos+LenB(boundary),RequestBin,boundary)
  Loop

  GP_keys = UploadRequest.Keys
  for GP_i = 0 to UploadRequest.Count - 1
    GP_curKey = GP_keys(GP_i)
    'Save all uploaded files
    if UploadRequest.Item(GP_curKey).Item("FileName") <> "" then
      GP_value = UploadRequest.Item(GP_curKey).Item("Value")
      GP_valueBeg = UploadRequest.Item(GP_curKey).Item("ValueBeg")
      GP_valueLen = UploadRequest.Item(GP_curKey).Item("ValueLen")

      if GP_valueLen = 0 then
        Response.Write "<B>An error has occured saving uploaded file!</B><br/><br/>"
        Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
        Response.Write "File does not exists or is empty.<br/>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
	  	  response.End
	    end if
      
      'Create a Stream instance
      Dim GP_strm1, GP_strm2
      Set GP_strm1 = Server.CreateObject("ADODB.Stream")
      Set GP_strm2 = Server.CreateObject("ADODB.Stream")
      
      'Open the stream
      GP_strm1.Open
      GP_strm1.Type = 1 'Binary
      GP_strm2.Open
      GP_strm2.Type = 1 'Binary
        
      GP_strm1.Write RequestBin
      GP_strm1.Position = GP_ValueBeg
      GP_strm1.CopyTo GP_strm2,GP_ValueLen
    
      'Create and Write to a File
      'GP_curPath = Request.ServerVariables("PATH_INFO")
      'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
      'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
      '  GP_curPath = GP_curPath & "/"
      'end if 

        SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	    Call OpenRs(rsDEFAULTS, SQL)
	    doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	    Call CloseRs(rsDEFAULTS)

        GP_curPath = doc_DEFAULTPATH & "\finance\PaymentCardFiles\"

      GP_CurFileName = UploadRequest.Item(GP_curKey).Item("FileName")
      'GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & GP_CurFileName
      GP_FullFileName = GP_curPath & GP_CurFileName
      'Check if the file alreadu exist
      GP_FileExist = false
      Set fso = CreateObject("Scripting.FileSystemObject")
      If (fso.FileExists(GP_FullFileName)) Then
        GP_FileExist = true
      End If      
      if nameConflict = "error" and GP_FileExist then
	  	Response.Redirect "FileAlreadyExists.asp?FileName=" & GP_CurFileName
        Response.Write "<B>File already exists!</B><br/><br/>"
        Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
				GP_strm1.Close
				GP_strm2.Close
	  	  response.End
      end if
      if ((nameConflict = "over" or nameConflict = "uniq") and GP_FileExist) or (NOT GP_FileExist) then
        if nameConflict = "uniq" and GP_FileExist then
          Begin_Name_Num = 0
          while GP_FileExist    
            Begin_Name_Num = Begin_Name_Num + 1
            'GP_FullFileName = Trim(Server.mappath(GP_curPath))& "\" & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FullFileName = GP_curPath & fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
            GP_FileExist = fso.FileExists(GP_FullFileName)
          wend  
          UploadRequest.Item(GP_curKey).Item("FileName") = fso.GetBaseName(GP_CurFileName) & "_" & Begin_Name_Num & "." & fso.GetExtensionName(GP_CurFileName)
					UploadRequest.Item(GP_curKey).Item("Value") = UploadRequest.Item(GP_curKey).Item("FileName")
        end if
        on error resume next
        GP_strm2.SaveToFile GP_FullFileName,2
        if err then
          Response.Write "<B>An error has occured saving uploaded file!</B><br/><br/>"
          Response.Write "Filename: " & Trim(GP_curPath) & UploadRequest.Item(GP_curKey).Item("FileName") & "<br/>"
          Response.Write "Maybe the destination directory does not exist, or you don't have write permission.<br/>"
          Response.Write "Please correct and <A HREF=""javascript:history.back(1)"">try again</a>"
    		  err.clear
  				GP_strm1.Close
  				GP_strm2.Close
  	  	  response.End
  	    end if
  			GP_strm1.Close
  			GP_strm2.Close
  			if storeType = "path" then
  				UploadRequest.Item(GP_curKey).Item("Value") = GP_curPath & UploadRequest.Item(GP_curKey).Item("Value")
  			end if
        on error goto 0
      end if
    end if
	temp = IsValidFileFormat()

	if( temp= false) then
	fso.DeleteFile(GP_FullFileName)
	Response.Redirect "InvalidFile.asp?FileName=" & GP_CurFileName	
	response.end()
	end if
  next

End Sub

'String to byte string conversion
Function getByteString(StringStr)
  For i = 1 to Len(StringStr)
 	  char = Mid(StringStr,i,1)
	  getByteString = getByteString & chrB(AscB(char))
  Next
End Function

'Byte string to string conversion
Function getString(StringBin)
  getString =""
  For intCount = 1 to LenB(StringBin)
	  getString = getString & chr(AscB(MidB(StringBin,intCount,1))) 
  Next
End Function

Function UploadFormRequest(name)
  on error resume next
  if UploadRequest.Item(name) then
    UploadFormRequest = UploadRequest.Item(name).Item("Value")
  end if  
End Function

'START THE UPLOAD REQUEST IF THE APPROPRIATE VARIABLE IS NOT EMPTY
If (CStr(Request.QueryString("GP_upload")) <> "") Then
  GP_redirectPage = ""
  If (GP_redirectPage = "") Then
    GP_redirectPage = CStr(Request.ServerVariables("URL"))
  end if
    
  RequestBin = Request.BinaryRead(Request.TotalBytes)
  Dim UploadRequest
  Set UploadRequest = CreateObject("Scripting.Dictionary")  
  BuildUploadRequest RequestBin, "PaymentCardFiles", "file", "", "error"
  
  '*** GP NO REDIRECT
end if  
'END THE UPLOAD REQUEST

OpenDB()

'INSERT THE FILE INFORMATION INTO THE DATABASE WITHOUT THE PROCESSING INFORMATION
SQL = "SET NOCOUNT ON;INSERT INTO F_PAYMENTCARDFILES (FILENAME, UPLOADDATE, FILETYPE) VALUES ('" & UploadFormRequest("FILENAME") & "', '" & FormatDateTime(Date,1) & "', 1);SELECT SCOPE_IDENTITY() AS NEWID;"
UploadedFileName = UploadFormRequest("FILENAME")
Call OpenRs(rsInsert, SQL)
NewId = rsInsert("NEWID")
CloseRs(rsInsert)
'END OF DATABASE FILE INFORMATION RECORDING

'HELFUL NOTES BY ZANFAR ON HOW THE FILE IS SET UP
'<-----------------43---------------------->  TOTAL LENGTH
'<-----13----><-------19-------->1<---10--->
'  REFERENCE        PAYMENT      P  DATE
'<--7--><-6-->
'  REF  TENANCYREF 
'END OF HELPFUL NOTES
  
'NEXT DO SOME FILE PROCESSING IF THE FILE HAS BEEN UPLOADED SUCCESSFULLY
If (UploadFormRequest("FILENAME") <> "") Then
    PaymentType = "13" ' for ".PO, .TDC, .PP" entries
    DirectDebitExtensions = ".DD"
    uploadFileExtention = Right(UploadFormRequest("FILENAME"),3)
    If (uploadFileExtention = DirectDebitExtensions) Then
        PaymentType = "2" ' for ".DD" entries
    end if  
	Const ForReading = 1, ForWriting = 2, ForAppending = 8
	Dim CurrentDate 
	CurrentDate = FormatDateTime(Date,1)
	
	TotalGoodCount = 0
	TotalGoodPayment = 0
	
	TotalBadPayment = 0
	TotalBadCount = 0
	
	set fso = server.CreateObject("Scripting.FileSystemObject") 

	'Get the Path of the file to where it was uploaded...
	'GP_curPath = Request.ServerVariables("PATH_INFO")
	'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
	'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
	'	GP_curPath = GP_curPath & "/"
	'end if

    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS)

	'FullPath = Trim(Server.mappath(GP_curPath)) & "\PaymentCardFiles\"
    FullPath = doc_DEFAULTPATH & "\finance\PaymentCardFiles\"
	'end of get path

	'open the text file for processing
	set f = fso.GetFile(FullPath & UploadedFileName) 
	set ts = f.OpenAsTextStream(ForReading, -2)	

	PeriodDate = ""
	FilePaymentCode = ""
	
	Do While not ts.AtEndOfStream
		TextStream = ts.ReadLine
		if (Len(TextStream) >= 43 AND InStr(TextStream,"Total Amount Updated onto System:") =false AND InStr(TextStream,"Total Transactions updated onto System:") =false) then 
			'THIS IS A POTENTIALLY VALID LINE
			BADLINE = ""
			TenancyRef = Mid(TextStream, 8, 6)
			Payment = Mid(TextStream, 14, 19)
			PaymentDate = Mid(TextStream, 34, 10)
			PaymentCode = Mid(TextStream, 33, 1)	        		 
            'Updated by Umair (04/06/2008)
            'Update the if condition to properly match the date.
            'Before it was using formatdatetime to format the date and compare it directly.
            'Update Start
			if (NOT isDate(PaymentDate)) then
				BADLINE = BADLINE & "  PAYMENTDATE IS EMPTY OR NOT VALID. "				
			end if			
			'if (isDate(PaymentDate)) then
			'	if( DateDiff("d",CDate(YearStartDate),CDate(PaymentDate)) < 0  or DateDiff( "d",CDate(YearEndDate),CDate(PaymentDate) ) > 0 ) then
			'		BADLINE = "  Transaction Date is not within the valid range (" & YearStartDate & "/" & YearEndDate & "). "					
			'	END IF
            'end if
			if (NOT isNumeric(Payment)) then 
				BADLINE = "  PAYMENT IS EMPTY OR NOT A NUMBER. "
				
			end if
			
			'this next check will also fail on SDP - sundry debtor purchases
			if (isNumeric(TenancyRef)) then  
				ItemTypeID = 1
				SQL = "SELECT TENANCYID FROM C_TENANCY WHERE TENANCYID = " & TenancyRef
				Call OpenRs(rsExists, SQL)
				if (rsExists.EOF) then
					BADLINE = BADLINE & "  CANNOT FIND TENANCY REFERENCE (" & TenancyRef & ") IN THE SYSTEM. "
				end if
				CloseRs(rsExists)
			else
				'check if the reference exists in the SDP TABLE
				SQL = "SELECT SDP.TENANCYID FROM F_SDPTOTENANCY SDP INNER JOIN C_TENANCY T ON T.TENANCYID = SDP.TENANCYID WHERE SDP.SDP = '" & TenancyRef & "'"
				Response.Write SQL
				Call OpenRs(rsSDP, SQL)
				ItemTypeID = 5				
				if (NOT rsSDP.EOF) then
					TenancyRef = rsSDP("TENANCYID")
				else
					BADLINE = BADLINE & "  BAD TENANCY REFERENCE (" & TenancyRef & "), CANNOT BE LOCATED IN THE SYSTEM. "
				end if
				CloseRs(rsSDP)
			end if			
			if (BADLINE = "") then
				'THIS IS A VALID LINE...
				if (GoodTotalCount = 0) then
					FilePaymentCode = PaymentCode
					PeriodDate = PaymentDate
				end if
				Payment=-(Payment)
				isDebit=0
				if (Payment > 0) then 
				    isDebit = 1
				end if
				
				'INSERT A LINE INTO THE RENT JOURNAL AND GET THE JOURNALID BACK
				SQL = "SET NOCOUNT ON;INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, PAYMENTSTARTDATE, PAYMENTENDDATE, AMOUNT, ISDEBIT, STATUSID) VALUES ("&_
						"" & TenancyRef & ", " &_
						"'" & PaymentDate & "', " &_
						"" & ItemTypeID  & ", " &_
                        "" & PaymentType & ", " &_
						"'" & PaymentDate & "', " &_
						"'" & PaymentDate & "', " &_
						"" & Payment & ", " &_
						"" & isDebit &",2); SELECT SCOPE_IDENTITY() AS NEWID; "
				Call OpenRs(rsQuickInsert, SQL)
				NewJournalID = rsQuickInsert("NEWID")
				CloseRs(rsQuickInsert)
				
				'NEXT INSERT A LINE INTO F_PAYMENTCARDDATA SO THAT THE FILE NAME CAN BE MATCHED UPTO THE DATA				
				SQL = "INSERT INTO F_PAYMENTCARDDATA (FILEID, JOURNALID) VALUES (" & NewID & ", " & NewJournalID & ")"
				Conn.Execute (SQL)
				
				TotalGoodCount = TotalGoodCount + 1
				TotalGoodPayment = TotalGoodPayment + CDbl(Abs(Payment))
			else
				'THIS IS A INCORRECT FILE
				if (TotalBadCount = 0) then
					'IF THIS IS THE FIRST INCORRECT LINE CREATE A TEXT FILE TO STORE THE INFORMATION
					
					UploadedDotPosition = InStrRev(UploadedFileName, ".")
					if (UploadedDotPosition <> 0) then
						Extension = "." & Right(UploadedFileName, Len(UploadedFileName) - UploadedDotPosition)
						UploadedFileName = Left(UploadedFileName, UploadedDotPosition-1) & "_BAD" 
					else
						UploadedFileName = UploadedFileName & "_BAD"
						Extension = ""
					end if
					
					If (fso.FileExists(FullPath & UploadedFileName & Extension)) Then
						GP_FileExist = true
					End If   
					if GP_FileExist then
						Begin_Name_Num = 0
						while GP_FileExist    
							Begin_Name_Num = Begin_Name_Num + 1
							TempFileName = UploadedFileName & "_" & Begin_Name_Num
							GP_FileExist = fso.FileExists(FullPath & TempFileName & Extension)
						wend  
						UploadedFileName = TempFileName
					end if
					Set MyBadFile= fso.CreateTextFile(FullPath & UploadedFileName & Extension, True)
				
				end if
				MyBadFile.WriteLine TextStream & BADLINE
				TotalBadCount = TotalBadCount + 1
				if (isNumeric(Payment)) then
					TotalBadPayment = TotalBadPayment + CDbl(Abs(Payment))
				end if
			end if
			'END OF POTENTIALLY CORRECT LINE IF ELSE....			
		elseif (Len(TextStream) = 0) then
			'Most PROB AN EMPTY LINE
			DoNothing = true
		elseif (Len(TextStream) = 50 OR Len(TextStream) = 31 OR Len(TextStream) = 50) then
			'MOST PROB ONE OF THE END LINES WHICH CONTAIN TOTALS, ETC...
			DoNothing = true
		end if
	Loop 

	if (PeriodDate <> "") then
		PeriodDate = "'" & FormatDateTime(CDate(PeriodDate),1) & "'"
	else
		PeriodDate = "NULL"
	end if
	
	'FINALISE EVERYTHING AND UPDATE THE DATABSE WITH THE PROCESSING DETAILS
	SQL = "UPDATE F_PAYMENTCARDFILES SET PROCESSCOUNT = " & TotalGoodCount & ", PROCESSVALUE = " & TotalGoodPayment & ", PERIODDATE = " & PeriodDate & ", PAYMENTCODE = '" & FilePaymentCode & "' WHERE FILEID = " & NewID
	Conn.Execute (SQL)
	
	if (TotalBadCount > 0) then
		SQL = "INSERT INTO F_PAYMENTCARDFILES (FILENAME, UPLOADDATE, PROCESSCOUNT, PROCESSVALUE, FILETYPE, PERIODDATE) VALUES ('" & UploadedFileName & Extension & "', '" & FormatDateTime(Date,1) & "', " & TotalBadCount & ", " & TotalBadPayment & ", 2, " & PeriodDate & ")"
		Conn.Execute(SQL)
	end if
end if

Function IsValidFileFormat()
set fso = server.CreateObject("Scripting.FileSystemObject") 
IsValidFileFormat= true		
    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS)
    FullPath = doc_DEFAULTPATH & "\finance\PaymentCardFiles\"
	
	set f = fso.GetFile(FullPath & UploadFormRequest("FILENAME")) 
	set ts = f.OpenAsTextStream(ForReading, -2)	
	fileText = ts.ReadAll
	set ts = f.OpenAsTextStream(ForReading, -2)	
	if(InStr(fileText,"Total Amount Updated onto System:") > 0 AND InStr(fileText,"Total Transactions updated onto System:") > 0) then
		Do While not ts.AtEndOfStream
			TextStream = ts.ReadLine	
			if (Len(TextStream) >= 43 AND InStr(TextStream,"Total Amount Updated onto System:") =false AND InStr(TextStream,"Total Transactions updated onto System:") =false) then 
				BADLINE = ""
				TenancyRef = Mid(TextStream, 8, 6)
				Payment = Mid(TextStream, 14, 19)
				PaymentDate = Mid(TextStream, 34, 10)
				PaymentCode = Mid(TextStream, 33, 1)			
	'check PaymentDate	
			
				if (isDate(PaymentDate)) then	
					'if( DateDiff("d",CDate(YearStartDate),CDate(PaymentDate)) < 0  or DateDiff( "d",CDate(YearEndDate),CDate(PaymentDate) ) > 0 ) then
					'	IsValidFileFormat =  false	
					'end if
				else
					IsValidFileFormat =  false
				end if	
	'check Payment			
				if (isNumeric(Payment) = false) then
				IsValidFileFormat=  false
				end if
	'check TenancyRef
				if (isNumeric(TenancyRef) = false) then										
					IsValidFileFormat=  false	
				
				elseif (isNumeric(TenancyRef) = true) then	
					ItemTypeID = 1
					SQL = "SELECT TENANCYID FROM C_TENANCY WHERE TENANCYID = " & TenancyRef		
					Call OpenRs(rsExists, SQL)
					if(Not rsExists.EOF) then
						TenancyRef = rsExists("TENANCYID")
					else
						SQL = "SELECT SDP.TENANCYID FROM F_SDPTOTENANCY SDP INNER JOIN C_TENANCY T ON T.TENANCYID = SDP.TENANCYID WHERE SDP.SDP = '" & TenancyRef & "'"
						Call OpenRs(rsSDP, SQL)
						ItemTypeID = 5				
						if (NOT rsSDP.EOF) then
							TenancyRef = rsSDP("TENANCYID")
						else
							IsValidFileFormat= false
						end if
						CloseRs(rsSDP)
					end if								
					
				end if
			
			end if	
		Loop
	else
	IsValidFileFormat =  false	
	end if	
End Function
Response.Redirect "../PaymentCardFiles.asp?UploadDate=" & FormatDateTime(Date,2)
%>
