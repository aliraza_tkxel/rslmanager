<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, office, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split, FndAssets
	Dim DevelopemntID, DevString, LetterIs_SQL,sorter
	Dim PrintAction,FndTenancy,allTenanciesCheckBoxes
	Dim DDANAMString, DDAnomaly,DDCompare, DD_JOIN
				
	quicktenancyfind = Request("quicktenancyfind")
	sorter = Request.form("hid_SORT")
	AssetFilter = Request("sel_ASSETS")
	if not AssetFilter <> "" then AssetFilter = Request("rq_ASSET") end if
	selected = Request("selected") // get selected checkboxes into an array
	'response.write selected
	sel_split = Split(selected,",")
	
	'rw quicktenancyfind & "<BR>"
	if quicktenancyfind <> "" then
	FndTenancy = " AND T.TENANCYID = " & quicktenancyfind
	end if
	
	'RW AssetFilter & "<BR>"
	if AssetFilter <> "" then
		if AssetFilter = 1 then
			FndAssets = " AND ISNULL(TR.OLD_ADLASSETCNT,0) > 0 "
		else
			FndAssets = " AND ISNULL(TR.OLD_ADLASSETCNT,0) = 0 "
		end if
	end if		
	
	rw "sorter: " & Sorter
	if sorter <> "" then
		sort_sql = " ORDER BY " & sorter & " "
	ELSE
		sort_sql = " ORDER BY T.TENANCYID ASC "
	end if
	

	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		'Response.Write mypage
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = clng(mypage)
		end if
			
		str_data = str_data & "<TABLE BORDER=0 width=800 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
								" <TR BGCOLOR=#133e71 STYLE='COLOR:WHITE'>" &_
								" <TD align=cemtre ><FONT STYLE='COLOR:WHITE'><B><a onclick='RSLFORM.hid_SORT.value = """ & "T.TENANCYID ASC" & """ ;quick_find()' style='text-decoration:none;CURSOR:HAND'><img src='../myImages/sort_arrow_up_old.gif' border='0' alt='Sort Ascending' width='17' height='16'></a><BR> Tenancy Ref <BR><a onclick='RSLFORM.hid_SORT.value = """ & "T.TENANCYID DESC" & """;quick_find()'  style='text-decoration:none;CURSOR:HAND'><img src='../myImages/sort_arrow_DOWN_old.gif' border='0' alt='Sort Descending' width='17' height='16'></a></B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> Property Ref</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> Name</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> Old Rent</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> Old Adnl Assets</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> Old DD</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> New Rent</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> New Adnl Assets</B></FONT></TD>" &_
								" <TD ><FONT STYLE='COLOR:WHITE'><B> New DD</B></FONT></TD>" &_	
								" <TD align=cemtre ><FONT STYLE='COLOR:WHITE'><B><a onclick='RSLFORM.hid_SORT.value = """ &" DATEADD(M,1,LASTPOSTDATE) ASC" & """;quick_find()' style='text-decoration:none;CURSOR:HAND'><img src='../myImages/sort_arrow_up_old.gif' border='0' alt='Sort Ascending' width='17' height='16'></a><BR> Effective <BR><a  onclick='RSLFORM.hid_SORT.value = """ & " DATEADD(M,1,LASTPOSTDATE) DESC " & """;quick_find()' style='text-decoration:none;CURSOR:HAND'><img src='../myImages/sort_arrow_DOWN_old.gif' border='0' alt='Sort Descending' width='17' height='16'></a></B></FONT></TD>" &_	
								" <TD><FONT STYLE='COLOR:WHITE'></FONT></TD>" &_
								" </TR>" &_
								" <TR><TD HEIGHT='100%' colspan='5'></TD></TR>" 

		cnt = 0
			
		' Main SQL that retrieves all the repairs that are awaiting approval
		
		'' NEED TO CHANGE THE GARAGE CHANGE VALUES IN THE 2ND LEFT JOIN - NEVER HAD TIME TO DEVELOP THIS DYNAMICALLY
		 
		strSQL = 	"SELECT 	ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'') AS FULLNAME, " &_
					"	T.TENANCYID, " &_
					"	C.CUSTOMERID , " &_
					"	P.PROPERTYID, " &_
					"	F.TOTALRENT, " &_
					"	DD.REGULARPAYMENT, " &_
					"	ISNULL(ADN.GARAGECNT,0) as GARAGECNT, " &_
					"	ISNULL(ADN.GARAGERENT,0) as GARAGERENT, " &_
					"	(ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)) AS NEWRENT, " &_
					"	ISNULL(NEW_ADN.GARAGERENT,0) as NEWGARAGECHARGE, " &_
					"	NEWDD = ( " &_
					"		CASE 	 " &_
					"			WHEN DD.REGULARPAYMENT = F.TOTALRENT THEN (ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)) " &_
					"			WHEN DD.REGULARPAYMENT = ISNULL(ADN.GARAGERENT,0) THEN ISNULL(NEW_ADN.GARAGERENT,0) " &_
					"			WHEN DD.REGULARPAYMENT = ISNULL(F.TOTALRENT,0) + ISNULL(ADN.GARAGERENT,0) THEN (ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)) + ISNULL(NEW_ADN.GARAGERENT,0)  " &_
					"			ELSE ROUND(CONVERT(MONEY,((DD.REGULARPAYMENT/F.TOTALRENT*100)/100)*(ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)),2),2) " &_
					"		END " &_
					"		), " &_
					"	 DATEADD(M,1,LASTPOSTDATE) AS  RENTEFFECTIVE " &_
					"FROM	C_TENANCY T  " &_
					"	INNER JOIN F_DDSCHEDULE DD ON 	DD.TENANCYID = T.TENANCYID  " &_
					"					AND DD.SUSPEND NOT IN (1)  " &_
					"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  " &_
					"	INNER JOIN P_FINANCIAL F ON F.PROPERTYID = T.PROPERTYID " &_
					"	INNER JOIN P_TARGETRENT TR ON TR.PROPERTYID = P.PROPERTYID AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE()) " &_
					"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = DD.CUSTOMERID " &_
					"	LEFT JOIN ( " &_
					"			SELECT SUM(TOTALRENT) AS GARAGERENT, COUNT(C.PROPERTYID) AS GARAGECNT , C.TENANCYID  " &_
					"			FROM	C_ADDITIONALASSET C " &_
					"				INNER JOIN P_FINANCIAL F ON F.PROPERTYID = C.PROPERTYID " &_
					"			WHERE C.ENDDATE IS NULL " &_
					"			GROUP BY C.TENANCYID " &_
					"		 " &_
					"		  ) ADN ON ADN.TENANCYID = T.TENANCYID " &_
					"	LEFT JOIN ( " &_
					"			SELECT SUM( " &_
					"						CASE TOTALRENT " &_
					"							WHEN 12.50 THEN 15 " &_
					"							WHEN 25 THEN 30 " &_
					"						END " &_
					"					) AS GARAGERENT, COUNT(C.PROPERTYID) AS GARAGECNT , C.TENANCYID  " &_
					"			FROM	C_ADDITIONALASSET C " &_
					"				INNER JOIN P_FINANCIAL F ON F.PROPERTYID = C.PROPERTYID " &_
					"			WHERE C.ENDDATE IS NULL " &_
					"			GROUP BY C.TENANCYID " &_
					"		 " &_
					"		  ) NEW_ADN ON NEW_ADN.TENANCYID = T.TENANCYID " &_
					"WHERE  ISNULL(TR.YEARLY_DD_INCREASE,0) NOT IN (1) AND DD.REGULARPAYMENT  = (F.TOTALRENT + ISNULL(ADN.GARAGERENT,0)) AND T.TENANCYTYPE in (1,8) AND T.ENDDATE IS NULL " & FndTenancy & FndAssets & sort_sql

	
			strSQL = 	"SELECT 	ISNULL(C.FIRSTNAME,'') + ' ' + ISNULL(C.LASTNAME,'') AS FULLNAME,   " &_
					"	T.TENANCYID,   " &_
					"	C.CUSTOMERID ,   " &_
					"	P.PROPERTYID,   " &_
					"	TR.OLD_RENTTOTAL AS TOTALRENT,   " &_
					"	DD.REGULARPAYMENT,   " &_
					"	TR.OLD_ADLASSETCNT AS GARAGECNT,   " &_
					"	TR.OLD_ADLASSETTOTAL as GARAGERENT,   " &_
					"	(ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)) AS NEWRENT,  " &_
					"	ISNULL(TR.NEW_ADLASSETTOTAL,0) as NEWGARAGECHARGE,   " &_
					"	NEWDD = (   " &_
					"		CASE 	   " &_
					"			WHEN DD.REGULARPAYMENT = TR.OLD_RENTTOTAL THEN (ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0))   " &_
					"			WHEN DD.REGULARPAYMENT = ISNULL(TR.OLD_ADLASSETTOTAL,0) THEN ISNULL(TR.NEW_ADLASSETTOTAL,0)   " &_
					"			WHEN DD.REGULARPAYMENT = ISNULL(TR.OLD_RENTTOTAL,0) + ISNULL(TR.OLD_ADLASSETTOTAL,0) THEN (ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)) + ISNULL(TR.NEW_ADLASSETTOTAL,0)    " &_
					"			ELSE ROUND(CONVERT(MONEY,((DD.REGULARPAYMENT/TR.OLD_RENTTOTAL*100)/100)*(ISNULL(TR.RENT,0) + ISNULL(TR.SERVICES,0) + ISNULL(TR.COUNCILTAX,0) + ISNULL(TR.WATERRATES,0) + ISNULL(TR.INELIGSERV,0) + ISNULL(TR.SUPPORTEDSERVICES,0)),2),2)   " &_
					"		END   " &_
					"		),   " &_
					"	 DATEADD(M,1,LASTPOSTDATE) AS  RENTEFFECTIVE " &_
					"FROM	C_TENANCY T   " &_
					"	INNER JOIN F_DDSCHEDULE DD ON 	DD.TENANCYID = T.TENANCYID   " &_
					"					AND DD.SUSPEND NOT IN (1)  	 " &_
					"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID   " &_
					"	INNER JOIN P_TARGETRENT TR ON TR.PROPERTYID = P.PROPERTYID AND YEAR(TR.REASSESMENTDATE) = YEAR(GETDATE())  " &_
					"	INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = DD.CUSTOMERID   " &_
					"WHERE  	DD.REGULARPAYMENT  = (TR.OLD_RENTTOTAL + ISNULL(TR.OLD_ADLASSETTOTAL,0))  " &_
					"	AND T.TENANCYTYPE in (1,8)  " &_
					"	AND DATEADD(M,1,LASTPOSTDATE) > GETDATE() " &_
					"	AND T.ENDDATE IS NULL  	" & FndTenancy & FndAssets & sort_sql
		
		if mypage = 0 then mypage = 1 end if
		pagesize = 12
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	 
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
		
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		For i=1 to pagesize
			If NOT Rs.EOF Then
						
			// determine if select box has been previously selected
			isselected = ""
			
			theTenancy = Rs("TENANCYID")
			
			For each key in sel_split
				'response.write "<BR> KEY : " & key & " id : " & theTenancy
				
				If theTenancy = clng(key) Then
					isselected = " checked "
				End If
			Next
	
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			Propertyid =  Rs("Propertyid")
										
			'START APPENDING EACH ROW TO THE TABLE
			if Rs("TENANCYID") <> "" then
										
				str_data = str_data & 	"<TR style='cursor:hand' >" &_
										"<TD  ><a href='/CUSTOMER/CRM.asp?CustomerID=" &  Rs("CUSTOMERID")  & "'>" & Rs("TENANCYID") & "</a></TD>" &_
										"<TD  >" & Rs("PROPERTYID") & "</TD>" &_
										"<TD  >" & Rs("FULLNAME") & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("TOTALRENT"),2) & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("GARAGERENT"),2) & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("REGULARPAYMENT"),2) & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("NEWRENT"),2) & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("NEWGARAGECHARGE"),2) & "</TD>" &_
										"<TD  >" & FormatCurrency(Rs("NEWDD"),2) & "</TD>" &_	
										"<TD  >" & Rs("RENTEFFECTIVE") & "</TD>" &_								
										"<TD><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & Rs("CUSTOMERID") & "' value='" & Rs("CUSTOMERID") & "' onclick='do_sum("& Rs("CUSTOMERID") &")'></TD>" &_
										"</TR>"	
				if allTenanciesCheckBoxes 	<> "" then
					allTenanciesCheckBoxes = allTenanciesCheckBoxes & "," & Rs("CUSTOMERID")
				else
					allTenanciesCheckBoxes = Rs("CUSTOMERID")
				End if
			
			End If
			Rs.movenext()
			End If
			
		Next
		
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data &	"<TR><TD COLSPAN=11 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
									"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & 	"<TR style='height:'8px'><TD></TD></TR>" &_
									"<TR style='display:none'><TD COLSPAN=11 align=right><b>Total&nbsp;&nbsp;</b><INPUT READONLY style='text-align:right;font-weight:bold' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTAL CLASS='textbox100' VALUE=" & FormatNumber(detotal,2,-1,0,0) & "></TD>" &_
									"<td><image src='/js/FVS.gif' name='img_POSTTOTAL' width='15px' height='15px' border='0'></td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=11 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE='Amend DD Account' CLASS='rslbutton' ONCLICK='process()' Style='display:block'>" &_
								"</TD></TR></TABLE>"
		End If
		
		
		RedirectPage = "ServerSide/DDUpdateTool_srv.asp"
		'rw RedirectPage
			str_data = str_data & "<tr bgcolor=#133e71>" &_
			"	<td colspan=11 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=#133e71>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & RedirectPage & "?page=" & orderBy & "&rq_ASSET=" & AssetFilter & "&orderBy=" & orderBy & "&detotal='+detotal+'&selected='+str_idlist""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & RedirectPage & "?page=" & prevpage&  "&rq_ASSET=" & AssetFilter & "&orderBy=" & orderBy & "&detotal='+detotal+'&selected='+str_idlist""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & RedirectPage & "?page=" & nextpage &  "&rq_ASSET=" & AssetFilter &"&orderBy=" & orderBy & "&detotal='+detotal+'&selected='+str_idlist""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=# onclick=""javascript:frm_slip.location.href='" & RedirectPage & "?page=" & numpages &  "&rq_ASSET=" & AssetFilter &"&orderBy=" & orderBy & "&detotal='+detotal+'&selected='+str_idlist""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) frm_slip.location.href='" & RedirectPage & "?detotal='+detotal+'&selected='+str_idlist+'&page=' + document.getElementById('page').value + '&orderBy=" & orderBy & DevString &"&txt_FROM=" & fromdate & "&txt_TO=" & todate & NextString & "'; else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr></table>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=4 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	'rw allTenanciesCheckBoxes
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	
	parent.RSLFORM.TickAll.checked = false;
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	parent.RSLFORM.hid_tenancylist.value = "<%=allTenanciesCheckBoxes%>"
	}
</script>
<body onLoad="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>