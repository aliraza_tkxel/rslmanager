<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
Call OpenDB()

'CHECK IF CANCELLING/RECONCLIING IS LOCKED...
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'RECONCILELOCKED' AND DEFAULTVALUE = 0"
Call OpenRs(rsPostable, SQL)

if (NOT rsPostable.EOF) then
	IF Request.Form("hid_CANCEL") = "CANCEL" THEN
		
		if (Request.Form("CHKS").Count > 0) then
	
			'NEXT LOOP THROUGH THE REQUEST COLLECTION UPDATING THE ACTIVE
			For each i in Request.Form("CHKS")
				
				chk=Split(i,"|")
				Call LOG_PI_ACTION(i,"CANCEL")
			DS=Request.Form("DSUserName_Reason")
			DS=Split(DS,",")
			data=Split(DS(0),"|")
				SQL = "UPDATE F_PURCHASEITEM SET ACTIVE = 0,PISTATUS = 16,"&_ 
				"Reason='"& data(0) & "',CancelBy="& data(1) & "  WHERE ORDERITEMID = " & chk(0)
				Conn.Execute(SQL)
									
			Next
	
			RedirectPageNumber = Request("hid_PAGE")
			
			'FINALLY WE NEED TO CHECK IF ALL ITEMS HAVE BEEN FULLY CANCELLED, IF SO THEN CANCEL THE MASTER PURCHASE ORDER.
			SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND ORDERID = " & Request("hid_ORDERID")
			Call OpenRs(rsTOTAL, SQL)
			TotalCount = 0
			if (NOT rsTOTAL.EOF) then
				TotalCount = rsTOTAL("THECOUNT")
			END IF
			Call CloseRs(rsTOTAL)
			
			'if total count is 0 then this means there are no active items left
			'therefore make the purchase order in active aswell.
			if (TotalCount = 0) then
				Call LOG_PO_ACTION(Request("hid_ORDERID"), "CANCEL", "")
			DS=Request.Form("DSUserName_Reason")
			DS=Split(DS,",")
			data=Split(DS(0),"|")
				SQL = "UPDATE F_PURCHASEORDER SET ACTIVE = 0 ,POSTATUS = 16 ,"&_ 
				"Reason='"& data(0) & "',CancelBy="& data(1) & " WHERE ORDERID = " & Request("hid_ORDERID")

				Conn.Execute SQL
			else
				'Get the paid count 
				 SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE PISTATUS > 9 AND  ACTIVE = 1 AND ORDERID = " & Request("hid_ORDERID")
				 TotalPaid = 0
				 Call OpenRs(rsPAID, SQL)
				 TotalPaid = rsPAID("THECOUNT")
				 Call CloseRs(rsPAID)
				 'if total count = total paid then set the purchase order status to paid
				 if (TotalPaid = TotalCount) then
					 Call LOG_PO_ACTION(Request("hid_ORDERID"), "CANCEL", "")			
					 SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 13 WHERE ORDERID = " & Request("hid_ORDERID")
					 Conn.Execute SQL
				 else
					'Get the reconciled count 
					 SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE PISTATUS >= 9 AND ACTIVE = 1 AND ORDERID = " & Request("hid_ORDERID")
					 TotalReconciled = 0
					 Call OpenRs(rsREC, SQL)
					 TotalReconciled = rsREC("THECOUNT")
					 Call CloseRs(rsREC)
					 'if total count = total reconciled and paid then set the purchase order status to paid
					 if (TotalReconciled = TotalCount) then
						 Call LOG_PO_ACTION(Request("hid_ORDERID"), "CANCEL", "")			
						 SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 9 WHERE ORDERID = " & Request("hid_ORDERID")
						 Conn.Execute SQL
					 else
					 	'get the queued count
						 SQL = "SELECT COUNT(ORDERITEMID) AS THECOUNT FROM F_PURCHASEITEM WHERE PISTATUS = 0 AND ACTIVE = 1 AND ORDERID = " & Request("hid_ORDERID")
						 TotalQueued = 0
						 Call OpenRs(rsQUE, SQL)
						 TotalQueued = rsQUE("THECOUNT")
						 Call CloseRs(rsQUE)
						 'if total queued equal total count then set the po to queued
						 if (TotalQueued = TotalCount) then
							 Call LOG_PO_ACTION(Request("hid_ORDERID"), "CANCEL", "")			
							 SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 0 WHERE ORDERID = " & Request("hid_ORDERID")
							 Conn.Execute SQL
						 else
							 'Set it all to Goods Ordered if all other stages are passed
						     Call LOG_PO_ACTION(Request("hid_ORDERID"), "CANCEL", "")			
							 SQL = "UPDATE F_PURCHASEORDER SET POSTATUS = 1 WHERE ORDERID = " & Request("hid_ORDERID")
							 Conn.Execute SQL
						 end if
					end if
				end if
			end if	
						 

			
		end if
		
	END IF
Else
	Call CloseRs(rsPostable)
	Call CloseDB()
	Response.Redirect ("../CancelConfirm.asp?OrderID=" & Request("hid_ORDERID") & "&page=" & Request("hid_PAGE") & "&ER12" & Replace(Date, "/", "") & "=1")
End If
Call CloseRs(rsPostable)
Call CloseDB()
Response.Redirect ("../PurchaseOrderCancel.asp?page=" & RedirectPageNumber)
%>