<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim CostCentreID, strWhat, count, optionvalues, optiontext, intReturn, theamounts

	strWhat = Request.form("IACTION")
	If strWhat = "getcostcentre" Then rePop() End If

	Function rePop()
		Company = -1
		If(Request.Form("sel_COMPANY") <> "") Then Company = Request.Form("sel_COMPANY")

		If (Company = -1) Then
			optionvalues = ""
			optiontext = "Please Select..."
			Exit Function
		End If

		Call OpenDB()

		Call GetCurrentYear()
		FY = GetCurrent_YRange

DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (22,23) " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"


		SQL = "SELECT HE.COSTCENTREID,HE.DESCRIPTION FROM F_COSTCENTRE HE " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION HEA ON HEA.COSTCENTREID = HE.COSTCENTREID AND HEA.ACTIVE = 1 AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) " &_ 
			"WHERE (HE.companyid = " & Company & " or he.COSTCENTREID in (9)) ORDER BY HE.DESCRIPTION"
    '"AND EXISTS (SELECT HEADID FROM F_EXPENDITURE EX INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND (EXA.FISCALYEAR = " & FY & " OR EXA.FISCALYEAR IS NULL) AND EXA.ACTIVE = 1) " &_
			
		Call OpenRs(rsHead, SQL)

		count = 0
		optionvalues = ""
		optiontext = "Please Select..."
		theamounts = "0"
		While (NOT rsHead.EOF)
			theText = rsHead.Fields.Item("DESCRIPTION").Value
			theText = UCase(Left(theText,1)) & Mid(theText, 2, Len(theText)-1)

			optionvalues = optionvalues & ";;" & rsHead.Fields.Item("COSTCENTREID").Value
			optiontext = optiontext & ";;" & theText

			count = count + 1
			rsHead.MoveNext()
		Wend
		If (rsHead.CursorType > 0) Then
		  rsHead.MoveFirst
		Else
		  rsHead.Requery
		End If
		If (count = 0) Then
			optionvalues = ""
			optiontext = "No Cost Centres Are Setup..."
		End If

		Call CloseRs(rsHead)
		Call CloseDB()
	End Function

%>
<html>
<head>
<title>CC Data Fetcher</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body onload="loaded()">
<script type="text/javascript" language="JavaScript">
    function loaded() {

        if ('<%=strWhat%>' == 'getcostcentre') {
            parent.PopulateListBox("<%=optionvalues%>", "<%=optiontext%>", -1);
        }
    }
</script>
</body>
</html>
