<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim fromdate, action, todate, detotal, isselected
	Dim cnt, str_data, office_sql, endbutton, selected, sel_split

	Headers = Array("T.Date", "Item","Local Authority","Payment","Slip No","Debit","Credit","&nbsp;")
	SortOrder = Array("RJ.TRANSACTIONDATE",  "I.DESCRIPTION", "LA.DESCRIPTION","P.DESCRIPTION","PS.SLIPNUMBER", "RJ.AMOUNT", "-RJ.AMOUNT", "")
	HeaderWidths = Array("90","90","135","115","130", "90", "90", "")
			'
	fromdate = Request("txt_FROM")
	todate = Request("txt_TO")

	detotal_debit = Request("detotal_debit")
	if detotal_debit = "" Then detotal_debit = 0 end if

	detotal_credit = Request("detotal_credit")
	if detotal_credit = "" Then detotal_credit = 0 end if

	selected = Request("idlist") // get selected checkboxes into an array
	selected_data = "," & Replace(selected, " ", "") & ","

	todate = Request("txt_TO")
	if todate = "" Then 
		to_sql = "" 
	else
		to_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) <= '" & FormatDateTime(todate,1) & "' "
	end if

	fromdate = Request("txt_FROM")
	if fromdate = "" Then 
		from_sql = "" 
	else
		from_sql = " AND CONVERT(SMALLDATETIME,CONVERT(VARCHAR,RJ.TRANSACTIONDATE,103),103) >= '" & FormatDateTime(fromdate,1) & "' "
	end if


	tenancy = Request("txt_TENANCYID")
	if tenancy = "" Then 
		tenancy_sql = "" 
	else
		tenancy_sql = " AND RJ.TENANCYID = " & tenancy & " "
	end if
	
	slipnumber = Request("txt_SLIPNO")
	if slipnumber = "" Then 
		slipnumber_sql = "" 
	else
		slipnumber_sql = " AND PS.SLIPNUMBER = " & slipnumber & " "
	end if

	localauthority = Request("sel_LA")
	if localauthority = "" Then 
		localauthority_sql = "" 
	else
		localauthority_sql = " AND LA.LOCALAUTHORITYID = " & localauthority & " "
	end if

		
	paymenttype = Request("sel_PAYMENTTYPE")
	if paymenttype = "" Then 
		paymenttype_sql = "" 
	else
		paymenttype_sql = " AND RJ.PAYMENTTYPE = " & paymenttype & " "
	end if
	
	ctype = Request("sel_TYPE")
	if ctype = "" then
		ctype_sql = ""
	else
		if ctype = 1 then
			ctype_sql = " AND RJ.AMOUNT >= 0 "
		else
			ctype_sql = " AND RJ.AMOUNT < 0 "
		end if
	end if		

	orderBy = Request("orderBy")
	if (orderBy = "") then
		orderBy = SortOrder(0) & " ASC"
	else
		Matched = false
		CompareOrderByArray = Split(orderBy, " ")
		CompareOrderBy = CompareOrderByArray(i)
		for i=0 to Ubound(SortOrder)
			RW CompareOrderBy & " -- " & SortOrder(i) & "<BR>"
			if (CompareOrderBy = SortOrder(i)) then
				Matched = true
				Exit For
			end if
		next
		if Matched = false then
			orderBy = SortOrder(0) & " ASC"		
		end if
	end if
	
	TheNextPage_NoOrder = "txt_FROM=" & fromdate & "&txt_TO=" & todate & "&sel_PAYMENTTYPE=" & paymenttype & "&txt_TENANCYID=" & tenancy & "&sel_TYPE=" & ctype
	TheNextPage = "orderBy=" & orderBy & "&" & TheNextPage_NoOrder
	
	build_post()
	
	Function build_post()
		
		Dim strSQL
		
		dim mypage
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CLng(mypage)
		end if
		
		str_data = str_data & "<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 STYLE='BORDER-COLLAPSE:COLLAPSE;BORDER:1PX SOLID BLACK'>" &_
				"<TR BGCOLOR=BLACK STYLE='COLOR:WHITE'>" 
				
		for i=0 to Ubound(Headers)
			tWidth = "WIDTH=""" & HeaderWidths(i) & """"
			if (tWidth = "WIDTH=""""") then tWidth = ""
			if (SortOrder(i) <> "") then
				str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" &_
						"<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " ASC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_up_black.gif"" border=""0"" alt=""Sort Ascending""></A>&nbsp;" &_
						Headers(i) &_
						"&nbsp;<a href=""javascript:TravelTo('orderBy=" & SortOrder(i) & " DESC&" & TheNextPage_NoOrder & "')"" style=""text-decoration:none""><img src=""/myImages/sort_arrow_down_black.gif"" border=""0"" alt=""Sort Descending""></A>" &_
						"</B></FONT></TD>" 
			else
				str_data = str_data & "<TD " & tWidth & " height=20><FONT STYLE='COLOR:WHITE'><B>" & Headers(i) & "</B></FONT></TD>" 
			end if
		next
		
		str_data = str_data & "</TR><TR><TD HEIGHT='100%'></TD></TR>" 

		
		cnt = 0
		strSQL = 	"SELECT 	RJ.JOURNALID, RJ.TENANCYID, CONVERT(VARCHAR,RJ.TRANSACTIONDATE, 103) AS TRANSDATE, P.DESCRIPTION AS PAYMENTTYPE, " &_
					"			ISDEBIT, ISNULL(RJ.AMOUNT,0) AS AMOUNT, TS.DESCRIPTION AS STATUS, I.DESCRIPTION AS ITEMTYPE,ISNULL(PS.SLIPNUMBER,'-') AS SLIPNUMBER, " &_
					"			CASE WHEN LEN( LA.DESCRIPTION) > 22   " &_
					"				THEN LEFT(LA.DESCRIPTION , 17) +  '...'  "  &_
					"				ELSE  LA.DESCRIPTION " &_
					"			END AS LOCALAUTHORITY , LA.DESCRIPTION AS LOCALAUTHORITYTITLE " &_ 
					"FROM	 	F_RENTJOURNAL RJ " &_
					"			LEFT JOIN F_PAYMENTTYPE P ON P.PAYMENTTYPEID = RJ.PAYMENTTYPE " &_
					"			LEFT JOIN F_ITEMTYPE I ON I.ITEMTYPEID = RJ.ITEMTYPE " &_
					"			LEFT JOIN F_TRANSACTIONSTATUS TS ON TS.TRANSACTIONSTATUSID = RJ.STATUSID " &_
					"			LEFT JOIN F_CASHPOSTING CP ON CP.JOURNALID = RJ.JOURNALID " &_
					"			LEFT JOIN F_PAYMENTSLIP PS ON PS.PAYMENTSLIPID = CP.PAYMENTSLIPNUMBER " &_
					"			INNER JOIN C_TENANCY T ON T.TENANCYID	= RJ.TENANCYID " &_
					"			INNER JOIN P__PROPERTY PROP ON PROP.PROPERTYID = T.PROPERTYID " &_
					"			INNER JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = PROP.DEVELOPMENTID " &_
					"			LEFT JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = DEV.LOCALAUTHORITY " &_
					"WHERE		RJ.STATUSID <> 1  " &_
					" AND 	P.PAYMENTTYPEID NOT IN (1,10,11,12,27,28,26,13,15,16) AND NOT EXISTS" &_
					"			(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 1 AND RECCODE = RJ.JOURNALID) " &_
					"			" & from_sql & to_sql & paymenttype_sql & tenancy_sql & ctype_sql & localauthority_sql & slipnumber_sql & " " &_
					"ORDER		BY " & orderBy & ", RJ.JOURNALID ASC"
		response.write strSQL			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 12
	'  		  PLACE THIS NEXT TO WHERE
		set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.ActiveConnection = RSL_CONNECTION_STRING 			
		Rs.Source = strSQL
		Rs.CursorType = 2
		Rs.LockType = 1		
		Rs.CursorLocation = 3
		Rs.Open()
			
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
	
		numpages = Rs.PageCount
		numrecs = Rs.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not Rs.EOF AND NOT Rs.BOF then
			Rs.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if	
	
		Set regEx = New RegExp 

		
		For i=1 to pagesize
			If NOT Rs.EOF Then
			
			// determine if check box has been previously selected
			isselected = ""
			trstyle = ""
			JOURNALID = Rs("JOURNALID")
			ListSearch = "," & JOURNALID & ","
			Comparison = InStr(1, selected_data, ListSearch, 0)
			If NOT isNULL(Comparison) AND Comparison > 0  Then
					isselected = " checked "
					trstyle = " style='background-color:steelblue;color:white' "
			End If
			
			'GET ALL STANDARD VARIABLES
			cnt = cnt + 1
			'START APPENDING EACH ROW TO THE TABLE
			paymenttype = Rs("PAYMENTTYPE")
			paymenttypetitle = ""
			if (NOT isNull(paymenttype)) then
				if (len(paymenttype) > 20) then
					paymenttypetitle = " title='" & paymenttype& "' "
					paymenttype = LEFT(paymenttype,20) & "..."
				end if
			end if
			 
			str_data = str_data & 	"<TR" & trstyle & "><TD>" & Rs("TRANSDATE") & "</TD>" &_
						"<TD>" & Rs("ITEMTYPE") & "</TD>" &_
						"<TD title="& Rs("LOCALAUTHORITYTITLE") & ">"& Rs("LOCALAUTHORITY") & "</TD>" &_
						"<TD " & paymenttypetitle & ">" & paymenttype & "</TD>" &_
						"<TD>" & Rs("SLIPNUMBER") & "</TD>" ' TAKEN STATUS OFF -JIMMY *** TAKEN OUT -JIMMY - & TenancyReference(Rs("TENANCYID")) &
			amount = Rs("AMOUNT")
			if (amount < 0) then
				iType = 0
				amount = -amount
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" 
			else
				iType = 1						
				str_data = str_data &_			
						"<TD STYLE='border-left:1px dotted #133e71' align=right>" & FormatCurrency(amount) & "&nbsp;</TD>" &_
						"<TD STYLE='border-left:1px dotted #133e71'>&nbsp;</TD>"
			end if
				str_data = str_data &_ 			
						"<TD STYLE='border-left:1px dotted #133e71'><input type='checkbox' " & isselected & " name='chkpost' id='chkpost" & JOURNALID & "' value='" & JOURNALID & "' onclick='do_sum("& JOURNALID &", " & FormatNumber(amount,2,-1,0,0)& "," & iType & ")'></TD>" &_
						"</TR>"
			
			Rs.movenext()
			End If
		Next

		Rs.close()
		Set Rs = Nothing
		
		// pad out to bottom of page
		Call fillgaps(pagesize - cnt)

		If cnt = 0 Then 
			str_data = str_data & "<TR><TD COLSPAN=8 WIDTH=100% ALIGN=CENTER><B>No matching records exist !!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		Else
			str_data = str_data & "<TR style='height:'8px'><TD ></TD></TR>" &_
				"<TR><TD COLSPAN=5 align=right style='border-top:1px solid #133e71'><b>Total&nbsp;</b></TD>" &_
				"<TD style='border-top:1px solid #133e71'><INPUT READONLY style='text-align:right;font-weight:bold;border:none' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTALDEBIT CLASS='textbox100' VALUE='" & FormatNumber(detotal_debit,2) & "'></TD>" &_
				"<TD style='border-top:1px solid #133e71'><INPUT READONLY style='text-align:right;font-weight:bold;border:none' BGCOLOR=WHITE TYPE=TEXT id=txt_POSTTOTALCREDIT CLASS='textbox100' VALUE='" & FormatNumber(detotal_credit,2) & "'></TD>" &_
				"<td style='border-top:1px solid #133e71'>&nbsp;</td></TR>"

			EndButton = "<TABLE WIDTH='100%'><TR><TD colspan=8 align=right>" &_
								"<INPUT TYPE=BUTTON VALUE=' Validate Selected ' CLASS='rslbutton' ONCLICK='process()'>" &_
								"</TD></TR></TABLE>"
		End If
		
			str_data = str_data & "<tr bgcolor=black>" &_
			"	<td colspan=10 align=center>" &_
			"		<table width='100%' cellspacing=0 cellpadding=0>" &_
			"			<tfoot>" &_
			"				<tr style='color:white' bgcolor=black>" &_
			"					<td width=550px align=right>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=1&" & Server.HTMLEncode(TheNextPage) & "')""><b>FIRST</b></a>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & prevpage & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>PREV</b></a>" &_
			"						Page " & mypage& " of " & numpages& ". Records: " & (mypage-1)*pagesize+1& " to " & (mypage-1)*pagesize+cnt& " of " & numrecs& "" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & nextpage & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>NEXT</b></a>" &_
			"						<a class='RSLWhite' href=""javascript:TravelTo('page=" & numpages & "&" & Server.HTMLEncode(TheNextPage) & "')""><b>LAST</b></a>" &_
			"					</td>" &_
			"				    <td align=right>" &_
			"						Page:" &_
			"						<input type=text class='textbox' name='page' size=4 maxlength=4 style='font-size:8px'>" &_
			"						<input type=button class='RSLButtonSmall' value=GO onclick=""javascript:if (!isNaN(document.getElementById('page').value)) TravelTo('page=' + document.getElementById('page').value + '&" & Server.HTMLEncode(TheNextPage) & "'); else alert('You have entered (' + document.getElementById('page').value + ').\nThis is invalid please re-enter a valid page number.');"" style='font-size:8px'>" &_
			"					</td>" &_
			"				</tr>" &_
			"			</tfoot>" &_
			"		</table>" &_
			"	</td>" &_
			"</tr>"

	End function
	
	// pads table out to keep the height consistent
	Function fillgaps(int_size)
	
		Dim tr_num, cnt
		cnt = 0
		while (cnt < int_size)
			str_data = str_data & "<TR><TD COLSPAN=8 ALIGN=CENTER><input style='visibility:hidden' type='checkbox' name='dummyforheight'></TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	if (Request("RESET") = 1) then
	
		Call OpenDB()
		
		statement = Request("sel_STATEMENT")
		REsponse.Write "<BR>statement" & statement & "<BR>"
		if (statement = "") then statement  = -1
		SQL = "SELECT * FROM F_BANK_STATEMENTS WHERE BSID = " & statement
		Call OpenDB()
		Call OpenRs(rsBS, SQL)
		StartingBalance = rsBS("STARTINGBALANCE")
		EndingBalance = rsBS("ENDINGBALANCE")
		Debits = rsBS("DEBITS")
		Credits = rsBS("CREDITS")
		Call CloseRs(rsBS)
	
		SQL = "SELECT " &_
				"(SELECT ISNULL(SUM(AMOUNT),0) AS DEBITS FROM F_RENTJOURNAL RJ " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND RECTYPE = 1 AND BSID = " & statement & " " &_
				"		WHERE AMOUNT >= 0) " &_
				"+ " &_
				"(SELECT ISNULL(SUM(TOTALVALUE),0) AS DEBITS FROM F_BACSDATA BD " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND RECTYPE = 2 AND BSID = " & statement & " " &_
				"		WHERE TOTALVALUE >= 0) " &_
				"AS TOTALDEBITS"
		Call OpenRs(rsDEBITS, SQL)
		RSL_DEBITS = rsDEBITS("TOTALDEBITS")
		Call CloseRs(rsDEBITS)
		
		SQL = "SELECT " &_
				"(SELECT ISNULL(-SUM(AMOUNT),0) AS CREDITS FROM F_RENTJOURNAL RJ " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = RJ.JOURNALID AND RECTYPE = 1 AND BSID = " & statement & " " &_
				"		WHERE AMOUNT < 0) " &_
				"+ " &_
				"(SELECT ISNULL(-SUM(TOTALVALUE),0) AS CREDITS FROM F_BACSDATA BD  " &_
				"	INNER JOIN F_BANK_RECONCILIATION BR ON BR.RECCODE = BD.DATAID AND RECTYPE = 2 AND BSID = " & statement & " " &_
				"		WHERE TOTALVALUE < 0) " &_
				"AS TOTALCREDITS"
		Call OpenRs(rsCREDITS, SQL)
		RSL_CREDITS = rsCREDITS("TOTALCREDITS")
		Call CloseRs(rsCREDITS)
	
		RSL_END = StartingBalance + RSL_DEBITS - RSL_CREDITS
	
		Call CloseDB()
		
	end if	
%>
<html>
<head></head>
<script language=javascript>
function ReturnData(){
	<% if (Request("RESET") = 1) then %>
		parent.reset_client_totals()
		parent.RSLFORM.txt_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance)%>"
		parent.RSLFORM.hid_STARTINGBALANCE.value = "<%=FormatNumber(StartingBalance,2,-1,0,0)%>"	
		parent.RSLFORM.txt_ENDINGBALANCE.value = "<%=FormatNumber(EndingBalance)%>"
		parent.RSLFORM.txt_DEBITS.value = "<%=FormatNumber(Debits)%>"
		parent.RSLFORM.txt_CREDITS.value = "<%=FormatNumber(Credits)%>"			
		parent.RSLFORM.txt_RSLDEBITS.value = "<%=FormatNumber(RSL_DEBITS)%>"
		parent.RSLFORM.txt_RSLCREDITS.value = "<%=FormatNumber(RSL_CREDITS)%>"			
		parent.RSLFORM.hid_RSLDEBITS.value = "<%=FormatNumber(RSL_DEBITS,2,-1,0,0)%>"
		parent.RSLFORM.hid_RSLCREDITS.value = "<%=FormatNumber(RSL_CREDITS,2,-1,0,0)%>"
		parent.RSLFORM.txt_RSLENDINGBALANCE.value = "<%=FormatNumber(RSL_END)%>"				
		parent.RSLFORM.hid_RSLENDINGBALANCE.value = "<%=FormatNumber(RSL_END,2,-1,0,0)%>"
	<% end if %>
	parent.hb_div.innerHTML = ReloaderDiv.innerHTML;
	}
</script>
<body onload="ReturnData()">
<div id="ReloaderDiv">
<%=str_data%>
<%=EndButton%>
</div>
</body>
</html>