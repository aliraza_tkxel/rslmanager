<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
	CONST C_CNTYPE = 6		'	SUPPLIER CREDIT NOTE
	CONST C_CNSTATUS = 6	'	CREDITED
	
	Dim CNName, CNDate, CNNOTES, ItemList, cnt, exists, PONumber, POIN, txnid, editseq, CNID
	Dim total_net, total_vat, total_gross, InvoiceID
	Dim DB_DESC, DB_MEMO, DB_ACCOUNT, DB_AMOUNT
	Dim CR_DESC, CR_MEMO, CR_ACCOUNT, CR_AMOUNT, CR_TRACKER
	
	if Request("txt_PONUMBER") = "" Then 
		PONumber = null 
		POIN = "NULL"
	Else
		PONumber = Request("txt_PONUMBER")
		POIN = Request("txt_PONUMBER")
	End If
	
    Dim CompanyId
	CompanyId = "NULL"
	if(Request.Form("sel_COMPANY") <> "") then CompanyId = Request.Form("sel_COMPANY")

	OpenDB()
	
	' START PROCESSING --------------------------------------------------------------------
	If IsNull(PONumber) Then															'--					
		Call create_credit_note()														'--
		Call create_invoice()															'--			
		Call insert_nominal()															'--
	ElseIf ponumber_exists()Then														'--
		Call create_credit_note()														'--
		Call create_invoice()															'--		
		Call insert_nominal()															'--
	End If																				'--
	' END PROCESSING ----------------------------------------------------------------------
	
	CloseDB()
	
'	IF USER HAS ATTACHED THE CREDIT NOTE TO A PURCHASE ORDER, CHECK TO SEE IF IT EXISTS	
	Function ponumber_exists()
	
		SQL = "SELECT * FROM F_PURCHASEORDER WHERE ORDERID = " & PONumber
		Call OpenRs(rsSet, SQL)
		If Not rsSet.EOF Then 
			exists = 1			
		Else
			exists = 0
		End If
		CloseRs(rsSet)
		if Cint(exists) = 1 Then
			ponumber_exists = True
		Else
			ponumber_exists = False
		End If
		
	End Function
	
'	CREATE CREDIT NOTE ENTRIES. 1 IN F_CREDITNOTE AND N IN F_PURCHASEITEM	
	Function create_credit_note()
	
		cnt = 0
		ItemList = ""
		CNName = ""
		if(Request.Form("txt_CNNAME") <> "") then CNName = Replace(Request.Form("txt_CNNAME"), "'", "''")
		CNDate = ""
		if(Request.Form("txt_CNDATE") <> "") then CNDate = FormatDateTime(Request.Form("txt_CNDATE"),1)
		CNNOTES = ""
		if(Request.Form("txt_CNNOTES") <> "") then CNNOTES = Replace(Request.Form("txt_CNNOTES"), "'", "''")
		

	
	'	INSERT TOP LEVEL RECORD -- CREDIT NOTE	
		SQL = "SET NOCOUNT ON;INSERT INTO F_CREDITNOTE (CNNAME, ORDERID, CNDATE, CNNOTES, USERID, SUPPLIERID, ACTIVE, CNTYPE, CNSTATUS, COMPANYID) VALUES " &_
				"('" & CNName & "', " & POIN  & ", '" & CNDate & "', '" & CNNOTES & "', " & Session("USERID") & ", " & Request("sel_SUPPLIER") & ", " &_
				" 1, " & C_CNTYPE & " , " & C_CNSTATUS & ", " & CompanyId & ");SELECT SCOPE_IDENTITY() AS CNID;SET NOCOUNT OFF"
		
		OpenDB()
		Call OpenRs(rsSet, SQL)		
		CNID = rsSet("CNID")
		CloseRs(rsSet)
	
	'	INSERT INDIVIDUAL ITEMS
		for each item in Request.Form("ID_ROW")
			cnt = cnt +1	
			Data = Request.Form("iData" & item)
			DataArray = Split(Data, "||<>||")
			
			ItemRef = Replace(DataArray(0), "'", "''")
			ItemDesc = Replace(DataArray(1), "'", "''")
			VatTypeID = Cint(DataArray(2))
			NetCost = cdbl(DataArray(3))
			VAT = Cdbl(DataArray(4))
			GrossCost = cdbl(DataArray(5))
			ExpenditureID = cint(DataArray(6))
			total_net = Cdbl(total_net) + Cdbl(DataArray(3))
			total_vat = Cdbl(total_vat) + Cdbl(DataArray(4))
			total_gross = Cdbl(total_gross) + Cdbl(DataArray(5))
			
			SQL = "SET NOCOUNT ON;INSERT INTO F_PURCHASEITEM (EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE, EXPPIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS) VALUES " &_
					"(" & ExpenditureID & ", '" & ItemRef & "', '" & ItemDesc & "', '" & CNDate & "', NULL, " &_
					" " & -NetCost & ", " & VatTypeID & ", " & -VAT & ", " & -GrossCost & ", " & Session("USERID") & ", 1 , " & C_CNTYPE & ", " & C_CNSTATUS & ");SELECT SCOPE_IDENTITY() AS ITEMID;SET NOCOUNT OFF"
			Call OpenRs(rsSet, SQL)		
			if cnt = 1 Then
				ItemList = ItemList & rsSet("ITEMID")
			Else
				ItemList = ItemList & "," & rsSet("ITEMID")
			End If
			rw total_net & "<BR>"
		next
		CloseRs(rsSet)
		' INSERT INTO JOIN TABLE
		ItemList = Split(ItemList, ",")
		For Each item in ItemList
		
			SQL = "INSERT INTO F_CREDITNOTE_TO_PURCHASEITEM (CNID, ORDERITEMID) VALUES (" & CNID & "," & item & ")"
			Conn.Execute(SQL)
			
		Next
		
	End Function
	
'	CREATE INVOICE ENTRY TO ENABLE CREDIT NOTE TO BE RECOGNISED BY PAYMENT TOOL	
	Function create_invoice()
		
		' THIS SQL WILL BUILD AN INVOICE BUT WILL SET IT TO UNCONFIRMED AS WE HAVE TO GET THE TOTALS FIRST, 
		' BUT WE NEED THE IDENTITY TO USE IN THE LINK TABLE LATER ON
		SQL = "SET NOCOUNT ON;INSERT INTO F_INVOICE (INVOICENUMBER, ORDERID, RECONCILEDATE, TAXDATE, USERID, SUPPLIERID, CONFIRMED, ISCREDIT, COMPANYID) " &_
				"VALUES ('Credit Note: " & CNID & "', NULL, '" & FormatDateTime(Date,1) & "', '" & FormatDateTime(CNDATE,1) & "', " &_
				SESSION("USERID") & ", " & Request("sel_SUPPLIER") & ", 0, 1, " & CompanyId & "); SELECT SCOPE_IDENTITY() AS INVOICEID"

		Call OpenRs(rsINV, SQL)
		InvoiceID = rsINV("INVOICEID")
		Call CloseRs(rsINV)
		
		NetCost = 0
		VAT = 0
		GrossCost = 0

		'NEXT LOOP THROUGH THE REQUEST COLLECTION UPDATING THE STATUS' AND GETTING THE TOTAL VALUES
		For Each i in ItemList
			
			SQL = "INSERT INTO F_ORDERITEM_TO_INVOICE (ORDERITEMID, INVOICEID) VALUES (" & i & ", " & InvoiceID & "); "
			Conn.Execute(SQL)
								
		Next

		'NOW USE THE TOTALS FROM ABOVE TO UPDATE THE INVOICE AND SET IT CONFIRMED
		SQL = "UPDATE F_INVOICE SET NETCOST = " & -total_net & ", VAT = " & -total_vat & ", GROSSCOST = " & -total_gross & ", CONFIRMED = 1 " &_
				"WHERE INVOICEID = " & InvoiceID
		Conn.Execute(SQL)
	
	End Function
	
'	INSERT NOMINAL LEDGER ENTRIES	
	Function insert_nominal()
	
		Dim str_date, str_ref, str_details, int_account,  str_memo
		Dim general_journal_id, cnt
		
		cnt = 0	
		editseq = 0
		
'		FIRST GET CREDIT NOTE DETAILS AND RELEVANT ACCOUNT IDS
		SQL = "SELECT 	'<b>CN</b> ' + RIGHT('000000' + CAST(C.CNID AS VARCHAR),7) + ' ' + C.CNNAME AS DB_DESC, " &_
				"		'<b>CN</b> ' + RIGHT('000000' + CAST(C.CNID AS VARCHAR),7) + ' ' + P.ITEMNAME AS CR_DESC,	 " &_
				"		C.CNNOTES AS DB_MEMO, P.ITEMDESC AS CR_MEMO,  " &_
				"		A2.ACCOUNTID AS DB_ACCOUNT, A.ACCOUNTID AS CR_ACCOUNT, " &_
				"		PI.TOTALCOST AS DB_AMOUNT, P.GROSSCOST AS CR_AMOUNT, " &_
				"		CNDATE, C.CNID, P.ORDERITEMID, P.ORDERITEMID AS CR_TRACKER " &_
				"FROM 	F_CREDITNOTE C " &_
				"		INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CR ON CR.CNID = C.CNID " &_
				"		INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = CR.ORDERITEMID " &_
				"		INNER JOIN F_EXPENDITURE E ON E.EXPENDITUREID = P.EXPENDITUREID " &_
				"		INNER JOIN NL_ACCOUNT A ON A.ACCOUNTNUMBER = E.QBDEBITCODE " &_
				"		INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, CNID FROM F_PURCHASEITEM F  " &_
				"	 	  	INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM P ON F.ORDERITEMID = P.ORDERITEMID GROUP BY CNID) PI  " &_
				"		     ON C.CNID = PI.CNID  " &_
				"		INNER JOIN (SELECT DEFAULTVALUE, DEFAULTNAME FROM RSL_DEFAULTS) DF ON DEFAULTNAME = 'PURCHASECONTROLACCOUNTNUMBER' " &_
				"INNER JOIN NL_ACCOUNT A2 ON A2.ACCOUNTNUMBER = DF.DEFAULTVALUE " &_
				"WHERE	C.CNID = " & CNID
		Call OpenRs(rsSet, SQL)
		rsSet.movefirst()
		Call enter_journal()
		While Not rsSet.EOF
		
			editseq = editseq + 1
			If cnt = 0 Then			' ENTER DEBIT LINE
				DB_DESC		= rsSet("DB_DESC")
				DB_MEMO		= rsSet("DB_MEMO")
				DB_ACCOUNT	= rsSet("DB_ACCOUNT")
				DB_AMOUNT	= ABS(rsSet("DB_AMOUNT"))			
				Call enter_journal_DB()
			End If
			CR_DESC		= rsSet("CR_DESC")
			CR_MEMO		= rsSet("CR_MEMO")
			CR_ACCOUNT	= rsSet("CR_ACCOUNT")
			CR_AMOUNT	= ABS(rsSet("CR_AMOUNT"))
			CR_TRACKER	= rsSet("CR_TRACKER")			
			Call enter_journal_CR()
			cnt = cnt + 1
			rsSet.movenext()
			
		Wend
		
	End Function
	
	Function enter_journal()
	
		' CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRY ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext = "F_INSERT_NL_JOURNALENTRY"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = 20
		comm.parameters(2) = Cint(CNID)
		comm.parameters(3) = now 
		comm.parameters(4) = FormatDateTime(CNDate)
		comm.parameters(5) = CNName
		comm.parameters(6) = null	
        comm.parameters(7) = null
        comm.parameters(8) = null
        comm.parameters(9) = null
        comm.parameters(10) = CompanyId
		comm.execute
		txnid = comm.parameters(0)
		Set comm = Nothing
		
	End Function
	
	Function enter_journal_DB()

' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYDEBITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext   = "F_INSERT_NL_JOURNALENTRYDEBITLINE"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = Clng(txnid)
		comm.parameters(2) = Cint(DB_ACCOUNT)
		comm.parameters(3) = now 
		comm.parameters(4) = Cint(editseq)
		comm.parameters(5) = FormatDateTime(CNDate)
		comm.parameters(6) = DB_AMOUNT
		comm.parameters(7) = DB_DESC
		comm.parameters(8) = DB_MEMO
		comm.parameters(9) = null
        comm.parameters(10) = null
        comm.parameters(11) = null
        comm.parameters(12) = null
        comm.parameters(13) = null
        comm.parameters(14) = CompanyId
		comm.execute
		Set comm = Nothing
		
	End Function
	
	Function enter_journal_CR()
	
' 		CALL STORED PROCEDURE TO CREATE NL_JOURNALENTRYDEBITLINE ENTRY
		Set comm = Server.CreateObject("ADODB.Command")
		comm.commandtext   = "F_INSERT_NL_JOURNALENTRYCREDITLINE"
		comm.commandtype = 4
		Set comm.activeconnection = Conn
		comm.parameters(1) = Clng(txnid)
		comm.parameters(2) = Cint(CR_ACCOUNT)
		comm.parameters(3) = now 
		comm.parameters(4) = Cint(editseq)
		comm.parameters(5) = FormatDateTime(CNDate)
		comm.parameters(6) = CR_AMOUNT
		comm.parameters(7) = CR_DESC
		comm.parameters(8) = CR_MEMO
		comm.parameters(9) = CR_TRACKER
        comm.parameters(10) = null
        comm.parameters(11) = null
        comm.parameters(12) = null
        comm.parameters(13) = CompanyId 
		comm.execute
		Set comm = Nothing
		
	End Function

	

%>
<HTML>
<HEAD>
<title></title>
</HEAD>
<SCRIPT LANGUAGE=JAVASCRIPT>
			
	function body_onload(){
	
		if ("<%=exists%>" == "0")
			parent.ManualError("img_PONUMBER", "PONumber Does Not Exist", 0)
		else
			parent.location.href = "../CreditNoteList.asp";
	
	}
	
</SCRIPT>
<BODY onload="body_onload()">
</BODY>
</HTML>



