
<% ByPassSecurityAccess = true %>
<%
OpenDB()
Function plannedEmailToContractor(ORDERID)
            plannedServicingContractorId = ""
            CUSTOMERID = ""
            PropertyId = ""
            SQL =   " SELECT ISNULL(PP.HOUSENUMBER, '') + ISNULL(' ' + PP.ADDRESS1, '') +  " &_
                    " ISNULL(' ' + PP.ADDRESS2, '') + ISNULL(' ' + PP.ADDRESS3, '') AS FullAddress " &_
                    " ,ISNULL(PP.TOWNCITY,'') as TownCity  " &_
				    " ,ISNULL(PP.POSTCODE,'') as Postcode  " &_
                    " ,PP.PROPERTYID as PropertyId " &_
		            " FROM F_PURCHASEORDER PO " &_
                    " LEFT JOIN P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " &_
		            " LEFT JOIN P__PROPERTY PP ON PP.PROPERTYID = WO.PROPERTYID  " &_
		            " WHERE PO.ACTIVE = 1 AND PO.ORDERID = " & ORDERID 
            'Response.Write(SQL)
			'Response.Write("</br>")
			Call OpenRs(rs, SQL)
            If Not rs.EOF Then 
            FullAddress = rs("FullAddress")
            TownCity = rs("TownCity")
            Postcode = rs("Postcode")
            PropertyId = rs("PropertyId")
            end if
             Call CloseRs(rs)
            'GETTING WORK AND CONTACT DETAILS
                SQL =   " SELECT	ISNULL(E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME, 'N/A') as ContactName   " &_
		                " ,FCW.PurchaseOrderId as OrderId   " &_
                        " ,E__EMPLOYEE.EMPLOYEEID as EmployeeId " &_  
		                " ,FCW.JOURNALID as JSN   " &_
		                " ,ISNULL(E.FIRSTNAME + ' ' + E.LASTNAME, 'N/A') as OrderedBy   " &_
		                " ,ISNULL( C.WORKDD,'N/A') AS DDial   " &_
		                " ,FCW.PlannedContractorId as PlannedContractorId    " &_
                        " ,E_CONTACT.WORKEMAIL as WorkEmail    " &_
                        " ,FP.userID As userId  " &_
                        " ,FCW.CustomerID as CustomerId" &_
                        " ,ISNULL(FCW.EstimateRef , 'N/A') as EstimateRef" &_
		                " FROM	PLANNED_CONTRACTOR_WORK AS FCW  " &_
		                "     INNER JOIN E__EMPLOYEE ON FCW.CONTACTID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "    INNER JOIN E__EMPLOYEE AS E on FCW.AssignedBy = E.EMPLOYEEID   	" &_                       
                        "    INNER JOIN E_CONTACT AS C ON C.EMPLOYEEID = E.EMPLOYEEID   " &_
                        "    INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID   " &_
		                "    INNER JOIN F_PURCHASEORDER  FP ON FP.orderId = FCW.PurchaseORDERID  " &_
                        "   WHERE	FCW.PurchaseOrderId = " & ORDERID
                 'Response.Write(SQL)
			'Response.Write("</br>")
				Call OpenRs(rs, SQL)
                If Not rs.EOF Then 
                    ContactName = rs("ContactName")
                    OrderId = rs("OrderId")
                    PMO = rs("JSN")
                    DDial = rs("DDial")
                    OrderedBy = rs("OrderedBy")
                    plannedServicingContractorId = rs("PlannedContractorId")
                    userId = rs("userId")
                    Recipient = rs("WorkEmail")
                    EstimateRef = rs("EstimateRef")
                    CUSTOMERID = rs("CustomerId")
                end if
                Call CloseRs(rs)
                'GETTING CUSTOMER DETAILS
                SQL =   "select ISNULL(CC.FIRSTNAME,'') +' '+ ISNULL(CC.LASTNAME,'') as TenantName " &_
		                " ,ISNULL(A.TEL, '') as Telephone" &_
		                " ,ISNULL(A.TELWORK, '') as TelephoneWork" &_
		                " ,ISNULL(A.MOBILE, '') as Mobile " &_
                        " ,ISNULL(A.Email, 'N/A') as Email  " &_
                        " ,ISNULL(A.COUNTY, 'N/A') as COUNTY  " &_
		                " from C__CUSTOMER AS CC " &_
		                " INNER JOIN C_ADDRESS AS A ON CC.CUSTOMERID = A.CUSTOMERID AND A.ISDEFAULT = 1" &_
		                " where CC.CustomerId='" & CUSTOMERID & "'" 
                 'Response.Write(SQL)
			'Response.Write("</br>")
				Call OpenRs(rs, SQL)
                If Not rs.EOF Then 
                    TenantName = rs("TenantName")
                    Telephone = rs("Telephone")
                    TelephoneWork = rs("TelephoneWork")
                    Mobile = rs("Mobile")
                    Email = rs("Email")
                    COUNTY = rs("COUNTY")
                    
                end if
                Call CloseRs(rs)

                'Getting Bedrooms
                SQL = "SELECT " &_
	                  "      ISNULL(BEDROOMS.BEDROOMS,0) AS Bedrooms " &_
                      "  FROM P__PROPERTY PROPERTY LEFT JOIN ( " &_
		              "          SELECT " &_
			          "              PROPERTY_ATTRIBUTES.PROPERTYID AS PROPERTYID, " &_
			          "              ISNULL(PROPERTY_ATTRIBUTES.PARAMETERVALUE,0) AS BEDROOMS " &_
			          "              FROM PA_PROPERTY_ATTRIBUTES PROPERTY_ATTRIBUTES  " &_
			          "              LEFT OUTER JOIN PA_ITEM_PARAMETER ITEM_PARAMETER ON PROPERTY_ATTRIBUTES.ItemParamId = ITEM_PARAMETER.ItemParamId " &_
			          "              LEFT OUTER JOIN PA_PARAMETER PARAMETER ON PARAMETER.ParameterID = ITEM_PARAMETER.ParameterID " &_
			          "              LEFT OUTER JOIN PA_ITEM ITEM ON ITEM_PARAMETER.ItemId = ITEM.ItemID " &_
			          "              WHERE PROPERTY_ATTRIBUTES.PROPERTYID = 'K020080009' " &_
			          "               AND  PARAMETER.ParameterName = 'Quantity' " &_
			          "               AND  ITEM.ItemName = 'Bedrooms' " &_
		              "          ) BEDROOMS ON PROPERTY.PROPERTYID = BEDROOMS.PROPERTYID " &_
                      "  WHERE PROPERTY.PROPERTYID = '" & PropertyId & "' "

			'Response.Write(SQL)
			'Response.Write("</br>")               
			   Call OpenRs(rs, SQL)
                    If Not rs.EOF Then 
                    Bedrooms = rs("Bedrooms")
                end if
                Call CloseRs(rs)
                if plannedServicingContractorId <> "" then 
                    '------------------------------------------------
                    ' GET WORKS REQUIRED INFO
                    '------------------------------------------------

                    SQL = "	SELECT	DISTINCT	WorkRequired " &_
	                " FROM	PLANNED_CONTRACTOR_WORK_DETAIL "  &_
	                " WHERE	 PlannedContractorId = " & plannedServicingContractorId

                    Call OpenRs(rs, SQL)
						 'Response.Write(SQL)
						'Response.Write("</br>")
                    WorkRequired = ""
                    while not rs.EOF
                        WorkRequired = WorkRequired & rs("WorkRequired") & " <br />"
                        rs.movenext()
                    wend

                    Call CloseRs(rs)  


                    '------------------------------------------------
                    ' GET WORKS COST INFO
                    '------------------------------------------------

                    SQL = "	SELECT	SUM(NetCost) as NetCost "  &_
			                " ,SUM(Vat) as VAT "  &_
			                " ,SUM(Gross) as Gross "  &_
	                        " FROM	PLANNED_CONTRACTOR_WORK_DETAIL "  &_
	                        " WHERE	 PlannedContractorId = " & plannedServicingContractorId
					'Response.Write(SQL)
					'Response.Write("</br>")
                    Call OpenRs(rs, SQL)
                    If Not rs.EOF Then 
                        NetCost = rs("NetCost")
                        VAT = rs("VAT")
                        Gross = rs("Gross")
                    end if
                    Call CloseRs(rs)  
                
                end if
    
                if CustomerId <> "" then 
                    '------------------------------------------------
                    ' GET RISK INFO
                    '------------------------------------------------

                    SQL = " SELECT	DISTINCT	CATDESC, SUBCATDESC "   &_
	                        " FROM	RISK_CATS_SUBCATS("& CUSTOMERID & ") "

                    Call OpenRs(rs, SQL)
                    RiskInfo = ""
                    while not rs.EOF
                        RiskInfo = RiskInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br />"
                        rs.movenext()
                    wend
                    Call CloseRs(rs)  
    	        
                    if RiskInfo = "" then
                        RiskInfo = "N/A"
                    end if
                    '------------------------------------------------
                    ' GET VULNARABILITY INFO
                    '------------------------------------------------

                    SQL =    " SELECT DISTINCT CAT.DESCRIPTION CATDESC,SUBCAT.DESCRIPTION SUBCATDESC "  &_
	                            " FROM	C_CUSTOMER_VULNERABILITY CV "  &_
		                        " INNER JOIN C_JOURNAL J ON CV.JOURNALID = J.JOURNALID "  &_
		                        " INNER JOIN C_VULNERABILITY CCV ON CCV.JOURNALID = J.JOURNALID "  &_
			                    " INNER JOIN C_VULNERABILITY_CATEGORY CAT ON CAT.CATEGORYID = CV.CATEGORYID  "  &_ 
			                    " INNER JOIN C_VULNERABILITY_SUBCATEGORY SUBCAT ON SUBCAT.SUBCATEGORYID = CV.SUBCATEGORYID and cv.categoryid=SUBCAT.categoryid  "  &_
                                " WHERE	 CV.CUSTOMERID ="& CustomerId &" "  &_
			                        "AND ITEMNATUREID = 61 "  &_
			                        "AND CCV.ITEMSTATUSID <> 14 "  &_
			                        "AND CV.VULNERABILITYHISTORYID = (	SELECT	MAX(VULNERABILITYHISTORYID) "  &_
												                "FROM	C_VULNERABILITY IN_CV "  &_
												                "WHERE	IN_CV.JOURNALID = J.JOURNALID) "   


                    					'Response.Write(SQL)
					'Response.Write("</br>")
  
					Call OpenRs(rs, SQL)
                    VulnarabilityInfo = ""
                    while not rs.EOF
                        VulnarabilityInfo = VulnarabilityInfo & rs("CATDESC") & " : " & rs("SUBCATDESC") &" <br /> "
                        rs.movenext()
                    wend
                    Call CloseRs(rs)  
                end if
                if VulnarabilityInfo = "" then
                    VulnarabilityInfo = "N/A"
                end if

                '------------------------------------------------
                ' GET ASBESTOS INFO
                '------------------------------------------------
                if PropertyId <> "" then
                    SQL =   " SELECT DISTINCT RL.ASBRISKLEVELDESCRIPTION,A.RISKDESCRIPTION "   &_
                            " FROM P__PROPERTY P "  &_
                            "    INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ARL ON P.PROPERTYID = ARL.PROPERTYID "    &_
                            "    INNER JOIN P_PROPERTY_ASBESTOS_RISK AR ON ARL.PROPASBLEVELID = AR.PROPASBLEVELID "    &_
                            "    INNER JOIN P_ASBESTOS A ON ARL.ASBESTOSID = A.ASBESTOSID "  &_
                            "    INNER JOIN P_ASBRISKLEVEL RL ON ARL.ASBRISKLEVELID = RL.ASBRISKLEVELID  "  &_
                            " WHERE P.PROPERTYID = '"  &  PropertyId & "' and (ARL.DateRemoved is null or  CONVERT(DATE, ARL.DateRemoved) > '"& date() &"' )"

					'Response.Write(SQL)
					'Response.Write("</br>")
  
                    Call OpenRs(rs, SQL)
                    AsbestosInfo = ""
                    while not rs.EOF
						AsbestosInfo = AsbestosInfo & "<tr>"
                        AsbestosInfo = AsbestosInfo & "<td>" & rs("ASBRISKLEVELDESCRIPTION") & " : " & "</td>" & "<td>" & rs("RISKDESCRIPTION") & "</td>" &" <br /> "
                        AsbestosInfo = AsbestosInfo & "<tr/>"
						rs.movenext()
                    wend
                    Call CloseRs(rs)  
                end if
                if AsbestosInfo = "" then
                    AsbestosInfo = "N/A"
                end if

                '------------------------------------------------
                ' POPULATE EMAIL TEMPLATE WITH DATA GATHERED
                '------------------------------------------------
                Dim emailbody
                emailbody =  getContractorEmailTemplateplanned()
                emailbody = Replace(emailbody,"{ContractorContactName}",ContactName)
                emailbody = Replace(emailbody,"{OrderId}","PO " &OrderId)
                emailbody = Replace(emailbody,"{PMO}",PMO)
                emailbody = Replace(emailbody,"{OrderedBy}",OrderedBy)
                emailbody = Replace(emailbody,"{DDial}",DDial)
                emailbody = Replace(emailbody,"{Email}",Email)
                emailbody = Replace(emailbody,"{WorksRequired}",WorkRequired)
                emailbody = Replace(emailbody,"{EstimateRef}",EstimateRef)
                emailbody = Replace(emailbody,"{NetCost}",FormatNumber( NetCost,2))
                emailbody = Replace(emailbody,"{VAT}",FormatNumber(VAT,2))
                emailbody = Replace(emailbody,"{TOTAL}",FormatNumber(Gross,2))
                emailbody = Replace(emailbody,"{Address}",FullAddress)
                emailbody = Replace(emailbody,"{TownCity}",TownCity)
                emailbody = Replace(emailbody,"{County}",County)
                emailbody = Replace(emailbody,"{PostCode}",Postcode)
                emailbody = Replace(emailbody,"{TenantName}",TenantName)
                emailbody = Replace(emailbody,"{Telephone}",Telephone)
                emailbody = Replace(emailbody,"{Mobile}",Mobile)
                emailbody = Replace(emailbody,"{RiskDetail}",RiskInfo)
                emailbody = Replace(emailbody,"{VulnerabilityDetail}",VulnarabilityInfo)
                emailbody = Replace(emailbody,"{Asbestos}",AsbestosInfo)
                emailbody = Replace(emailbody,"{Bedrooms}",Bedrooms)
                emailbody = Replace(emailbody,"{Logo_Broadland-Housing-Association}", "https://devcrm.broadlandhousinggroup.org/Finance/Images/LOGOLETTER.gif")
                emailbody = Replace(emailbody,"{Logo_50_years}","https://devcrm.broadlandhousinggroup.org/Finance/Images/50years.gif")

                sendEmailplanned emailbody,Recipient
                   
          



        '------------------------------------------------------------




        '------------------------------------------------------------

End Function




    '--------------------------  TEST

    Function sendEmailplanned(body,recipient)
	
        'Response.Write(recipient)
	'Response.End()
        if IsNull(recipient) then
			
			session("noRecipient") = "true"
			
			Response.Redirect ("../PurchaseListQueued.asp?noRecipient=true")

        else
		
        
				Dim iMsg
				Set iMsg = CreateObject("CDO.Message")
				Dim iConf
				Set iConf = CreateObject("CDO.Configuration")

        Dim Flds
        Set Flds = iConf.Fields
         Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60

				Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
				Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
				Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
				Flds.Update

				emailbody = body
				emailSubject = "Planned Work has been Assigned"
			
				Set iMsg.Configuration = iConf
				iMsg.To = recipient
				iMsg.From = "noreply@broadlandgroup.org"
				iMsg.Subject = emailSubject
				iMsg.HTMLBody = emailbody
				iMsg.Send
        	
        end if

    End Function


    Function getContractorEmailTemplateplanned()
        Dim assignToContractorEmailTemplate
        assignToContractorEmailTemplate = "<table> <tr><td colspan=""3""> Dear {ContractorContactName} </td></tr> <tr> <td> &nbsp; </td> </tr> <tr><td colspan=""3""> Please find below details of Planned Maintenance works required: </td></tr> <tr><td colspan=""3""> &nbsp; </td></tr> <tr><td style=""width:150px; padding-right: 10px;""> PO number: </td><td colspan=""2""> {OrderId} </td></tr> <tr><td style=""padding-right: 10px;""> Job Ref: </td><td colspan=""2""> {PMO} </td></tr> <tr><td style=""padding-right: 10px;""> Ordered by: </td><td colspan=""2""> {OrderedBy} </td></tr> <tr><td style=""padding-right: 10px;""> DD Dial: </td><td colspan=""2""> {DDial} </td></tr> <tr> <td style=""padding-right: 10px;""> Email: </td colspan=""2""> <td> {Email} </td> </tr> <tr> <td style=""padding-right: 10px; vertical-align: top;""> Address: </td><td colspan=""2""> <table> <tr> <td style="" width:210px; padding-right: 50px;""> {Address} </td><td style=""padding-right: 10px;""> Tenant: </td> <td> {TenantName} </td> </tr> <tr> <td style=""padding-right: 50px;""> {TownCity} </td><td style=""padding-right: 10px;""> Bedrooms:</td> <td> {Bedrooms}</td> </tr> <tr> <td style=""padding-right: 50px;""> {County} </td><td style=""padding-right: 10px;""> Telephone: </td> <td> {Telephone} </td> <td style=""padding-right: 10px;""> Mobile: </td> <td> {Mobile} </td> </tr> <tr> <td style=""padding-right: 50px;""> {PostCode} </td><td style=""padding-right: 10px; vertical-align: top;""> Risk: </td> <td> {RiskDetail} </td> </tr> <tr> <td style=""padding-right: 50px;""> &nbsp;</td><td style=""padding-right: 10px; vertical-align: top;""> Vulnerability: </td> <td> {VulnerabilityDetail} </td> </tr> </table> </td> </tr> <tr> <td> &nbsp; </td> </tr> <tr><td style=""padding-right: 10px; vertical-align: top;""> Works required: </td><td colspan=""2""> {WorksRequired} </td></tr> <tr><td colspan=""3""> &nbsp; </td></tr> <tr> <td style=""padding-right: 10px;"">Estimate Ref:</td> <td>{EstimateRef}</td> <td rowspan=""6"" valign=""top""><table style=""margin-left:-76px;""> <tr> <td style=""vertical-align:top"">Asbestos: </td> <td><table style=""vertical-align:top;margin-top:-3px;"">{Asbestos}</table></td></tr></table></td></tr><tr> <td> &nbsp; </td></tr><tr> <td style=""padding-right: 10px;""> Net Cost (&pound;): </td> <td> {NetCost} </td></tr><tr> <td style=""padding-right: 10px;""> VAT (&pound;): </td> <td> {VAT} </td></tr><tr> <td style=""padding-right: 10px;""><strong>Total (&pound;):</strong></td> <td><strong>{TOTAL}</strong></td></tr><tr> <td> &nbsp; </td></tr><tr> <td style=""padding-right: 10px;""> Kind regards </td> <td> &nbsp; </td></tr><tr> <td> &nbsp; </td></tr><tr><td colspan=""2""><strong>Broadland Housing </strong><br /><br /><table style=""vertical-align: text-top;""><tr> <td><img src=""{Logo_50_years}"" alt=""Brs 50 Years"" />&nbsp; <img src=""{Logo_Broadland-Housing-Association}"" alt=""Broadland Repairs"" style=""margin-left: 2mm;"" /> </td> <td style=""padding-left: 2mm;""> Broadland Housing Group <br /> NCFC, The Jarrold Stand <br /> Carrow Road, Norwich, NR1 1HU <br /> Customer Services 0303 303 0003 <br /> enq@broadlandgroup.org <br /></td></tr></table></td></tr></table>"
        getContractorEmailTemplateplanned = assignToContractorEmailTemplate
    End Function	

    Function PurchaseNumberplanned(strWord)
        PurchaseNumberplanned = "PO " & characterPad(strWord,"0", "l", 7)
    End Function


%>