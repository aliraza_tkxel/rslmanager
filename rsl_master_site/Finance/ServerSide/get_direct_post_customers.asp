<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim cnt, customer_id, str_journal_table, str_color, item_type

	OpenDB()
	tenancy_id = Request.FORM("hid_TENANTNUMBER")
	
	build_journal()

	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT CT.ENDDATE, T.ENDDATE AS GLOBALEND, C.CUSTOMERID, REPLACE(ISNULL(G.DESCRIPTION, ' ') + ' ' + ISNULL(C.FIRSTNAME, ' ') + ' ' + ISNULL(C.LASTNAME, ' '), '  ', '') AS FULLNAME " &_
					"FROM C_TENANCY T " &_
					"INNER JOIN C_CUSTOMERTENANCY CT ON CT.TENANCYID = T.TENANCYID " &_
					"INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = CT.CUSTOMERID " &_
					"LEFT JOIN G_TITLE G ON G.TITLEID = C.TITLE " &_					
					"WHERE T.TENANCYID = " & tenancy_id & " "

		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			if ( (rsSet("ENDDATE") = "" OR isNull(rsSet("ENDDATE"))) AND (rsSet("GLOBALEND") = "" OR isNull(rsSet("GLOBALEND"))) ) then
				str_color = "black"
				bstatus = "Current Tenant"
				istatus = "CURRENT"
			else
				str_color = "red"
				bstatus = "Previous Tenant or Ended Tenancy"
				istatus = "PREVIOUS"
			end if
			
			str_journal_table = str_journal_table &_
			 	"<TR style='color:" & str_color & "'>" &_
				"<TD>" & rsSet("FULLNAME") & "</TD>" &_
				"<TD title='" & bstatus & "'>" & istatus & "</TD>" &_				
				"<TD><input type=RADIO name='ATTACHCUSTOMER' onclick='parent.ATTACHCUSTOMER(" & rsSet("CUSTOMERID") & ")'></TD>" &_
				"<TR>"
				
				
			rsSet.movenext()
			
		Wend
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=3 ALIGN=CENTER>NO CUSTOMERS FOUND.</TD></TR></TFOOT>"
		End If
		
	End Function
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
function ReturnData(){
	parent.ACCOUNT_DIV.innerHTML = table_div.innerHTML
	parent.STATUS_DIV.style.visibility = "hidden";
	parent.RSLFORM.txt_AMOUNT.value = "";
	}
	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF onload="ReturnData()" TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA'>
<DIV ID=table_div>
	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD>
		<TR >
			<TD height=20 STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=390><B>Customer</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=150 nowrap><B>Tenant Status</B></TD>
			<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=50><B>Attach</B></TD>
		</TR>
		</THEAD>
			<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
</DIV>
</BODY>
</HTML>	
