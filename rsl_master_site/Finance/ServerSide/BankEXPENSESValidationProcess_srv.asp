<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%	
	'''''' page notes     ''''''
	' For the processing for this page we need to get the local authority id 
	' to match the records that have been seleccted
	'''''''''''''''''''''''''''''
	Dim bt_id ,SQLFilter, insert_list, detotal_credit, detotal_debit, bank_statement, LocalAuthority
	Dim IsDebbit, rec_type, method, TimeStamp, key_TransactionDate , key_LAuthID
	
	SQLFilter = " "
	
	' Get all the variables that are passed to this page
	
		insert_list = Request.Form("idlist")
		detotal_credit = Request("detotal_credit")
		detotal_debit = Request("detotal_debit")
		bank_statement = Request("sel_STATEMENT")
		LocalAuthority = Request("LocalAuthorityID")
		IsDebbit = Request("IsDebbit")
	
	'Set Statis variables
	
		rec_type = 5 ''This means these items are from the rent journal
		method = 1 'Manual Validation
		TimeStamp = Now()
	
	if insert_list <> "" then
		Split_Array()
		OpenDB()
		InsertDebitCredit_Amount()
		CloseDB()
	End If
	
		
	Function Split_Array()
		
		sel_split = Split(insert_list ,",") ' split selected string
		
		' Go through all the selected items and split the date and LA id
		COUNT = 0
		For each key in sel_split
		
		
			' Build an SQL Filter of OR statements
			if count = 0 then
				SQLFilter			 = SQLFilter & " ( PIQ.CHQNUMBER = " &  key & ")"
				else
				SQLFilter			 = SQLFilter & "OR ( PIQ.CHQNUMBER = " &  key & ")"
			End If
			
		COUNT = COUNT + 1
		Next
		
		' Clean up the filter statement
		SQLFilter = " AND( " & SQLFilter & "  ) "
	
	End Function
	
	
	Function InsertDebitCredit_Amount ()
	
		'insert the debits and credits into table to balance the amounts
		
		
		strSQL = 	"SET NOCOUNT ON;" &_	
					"	INSERT INTO F_BANK_TOTALS (BTDEBIT, BTCREDIT) " &_
					"	VALUES (" & detotal_debit & ", " & detotal_credit & " );" &_
					"	SELECT SCOPE_IDENTITY() AS BTID;"
	
		set rsSet = Conn.Execute(strSQL)
		bt_id = rsSet.fields("BTID").value
		rsSet.close()
		set rsSet = Nothing 
		
		Insert_HB_rec_Values()
	
	End Function

	
	Function Insert_HB_rec_Values()
	
		SQL = " INSERT INTO F_BANK_RECONCILIATION (BTID, BSID, RECTYPE, RECCODE, RECDATE, RECUSER, METHOD) " &_
			  " SELECT 	" & bt_id & ", " & bank_statement & ", " & rec_type & ", PIQ.CHQNUMBER ,'" & TimeStamp & "'," & Session("USERID") & "," & method &_ 
						" FROM F_PURCHASEITEM pi" &_
						"		INNER JOIN F_PURCHASEITEM_CHQ PIQ ON PIQ.ORDERITEMID = PI.ORDERITEMID " &_
						" WHERE PI.PITYPE IN (4,3)" &_
						"		AND NOT EXISTS	(SELECT * FROM F_BANK_RECONCILIATION WHERE RECTYPE = 5 AND RECCODE = PIQ.CHQNUMBER) " & SQLFilter

		Conn.Execute(SQL)
		
	End Function

	Response.Redirect "BankEXPENSESValidation_svr.asp?RESET=1&sel_STATEMENT=" & bank_statement & "&orderBy=" & Request("orderBy") & "&txt_FROM=" & Request("txt_HBFROM") & "&txt_TO=" & Request("txt_HBTO") & "&txt_TENANCYID=" & Request("txt_TENANCYID") & "&sel_TYPE=" & Request("sel_TYPE")	
%>
