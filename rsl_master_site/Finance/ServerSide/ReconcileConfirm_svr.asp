<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
Call OpenDB()

'CHECK IF RECONCILING IS LOCKED...
SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'RECONCILELOCKED' AND DEFAULTVALUE = 0"
Call OpenRs(rsPostable, SQL)
if (NOT rsPostable.EOF) then
	
	IF Request.Form("hid_RECONCILE") = "RECONCILE" THEN
		if (Request.Form("CHKS").Count > 0) then
	
'response.Write(SQL)
			'THIS SQL WILL BUILD AN INVOICE BUT WILL SET IT TO UNCONFIRMED AS WE HAVE TO GET THE TOTALS FIRST, BUT WE NEED THE IDENTITY TO USE 
			'IN THE LINK TABLE LATER ON
                    
        SQL = " SELECT POSTATUSNAME FROM F_POSTATUS POSTATUS, F_PURCHASEORDER PURCHASEORDER WHERE PURCHASEORDER.POSTATUS = POSTATUS.POSTATUSID AND PURCHASEORDER.ORDERID = " & Request("hid_ORDERID") & " "
        CALL OpenRs(rsPoName , SQL)
        PoName = rsPoName("POSTATUSNAME")
        if (PoName = "Reconciled") then
            
            Response.Redirect ("../ReconcileList.asp?page=" & RedirectPageNumber & "&sel_Supplier=" & suppId & "&reconcile=true")
            Session("Reconciled") = "true"
              
        end if
        
       
        Call CloseRs(rsPoName)

            
                                                              

                    SQL = "SET NOCOUNT ON; DECLARE @COMPANYID INT; SET @COMPANYID = (SELECT COMPANYID FROM F_PURCHASEORDER WHERE ORDERID = " & Request("hid_ORDERID") &"); INSERT INTO F_INVOICE (INVOICENUMBER, ORDERID, RECONCILEDATE, TAXDATE, USERID, SUPPLIERID, CONFIRMED, COMPANYID) " &_
					"VALUES ('" & Replace(Request("txt_INVOICENUMBER"),"'","''") & "', " & Request("hid_ORDERID") & ", '" & FormatDateTime(Request("txt_RECONCILEDATE"),1) & "', '" & FormatDateTime(Request("txt_TAXDATE"),1) & "', " &_
					SESSION("USERID") & ", " & Request("hid_SUPPLIERID") & ", 0, @COMPANYID); SELECT SCOPE_IDENTITY() AS INVOICEID"
	
			Call OpenRs(rsINV, SQL)
			InvoiceID = rsINV("INVOICEID")
			Call CloseRs(rsINV)
			
			NetCost = 0
			VAT = 0
			GrossCost = 0
	
			'NEXT LOOP THROUGH THE REQUEST COLLECTION UPDATING THE STATUS' AND GETTING THE TOTAL VALUES
			For each i in Request.Form("CHKS")
				
				SQL = "SELECT NETCOST, VAT, GROSSCOST FROM F_PURCHASEITEM WHERE ORDERITEMID = " & i
				Call OpenRs(rsPI, SQL)
				NetCost = NetCost + CDbl(rsPI("NETCOST"))
				VAT = VAT + CDbl(rsPI("VAT"))
				GrossCost = GrossCost + CDbl(rsPI("GROSSCOST"))
				Call CloseRs(rsPI)
                'Moved Below
				'Call LOG_PI_ACTION(i, "RECONCILED")							
				SQL = "INSERT INTO F_ORDERITEM_TO_INVOICE (ORDERITEMID, INVOICEID) VALUES (" & i & ", " & InvoiceID & "); " &_
						"UPDATE F_PURCHASEITEM SET PISTATUS = 9, RECONCILEDATE = '" & FormatDateTime(Date,1) & "' WHERE ORDERITEMID = " & i & ";"
				Conn.Execute(SQL)
				Call LOG_PI_ACTION(i, "RECONCILED")												
			Next
	
			'NOW USE THE TOTALS FROM ABOVE TO UPDATE THE INVOICE AND SET IT CONFIRMED
			SQL = "UPDATE F_INVOICE SET NETCOST = " & NetCost & ", VAT = " & VAT & ", GROSSCOST = " & GrossCost & ", CONFIRMED = 1 " &_
					"WHERE INVOICEID = " & InvoiceID
			Conn.Execute(SQL)
	
			RedirectPageNumber = Request("hid_PAGE")
			'FINALLY WE NEED TO CHECK IF ALL ITEMS HAVE BEEN FULLY COMPLETED, IF SO THEN RECONCILE THE MASTER PURCHASE ORDER.
			'SQL = "SELECT ORDERITEMID FROM F_PURCHASEITEM WHERE PISTATUS < 9 AND ACTIVE = 1 AND ORDERID = " & Request("hid_ORDERID")
			'SQL = "Select OrderItemId from F_PURCHASEITEM_LOG where OrderItemId ="& OrderItemID &" and PIStatus In (17,18,5)"
       
            'response.Write (SQL)
			'Call OpenRs(rsPI, SQL)
			if (REQUEST("hid_RemainPIs")<=0) then
				Call LOG_PO_ACTION(Request("hid_ORDERID"), "RECONCILED", "")										
				SQL = "UPDATE F_PURCHASEORDER SET RECONCILEDATE = '" & FormatDateTime(Date,1) & "', POSTATUS = 9 " &_
						"WHERE ORDERID = " & Request("hid_ORDERID")
				Conn.Execute SQL
				RedirectPageNumber = 1
			end if
			'Call CloseRs(rsPI)
			
		end if
		
	END IF
Else
	Call CloseRs(rsPostable)
	Call CloseDB()
	Response.Redirect ("../ReconcileConfirm.asp?OrderID=" & Request("hid_ORDERID") & "&page=" & Request("hid_PAGE") & "&ER12" & Replace(Date, "/", "") & "=1")
End If
Call CloseRs(rsPostable)
Call CloseDB()
suppId=Session("SupplierReturenId")
Response.Redirect ("../ReconcileList.asp?page=" & RedirectPageNumber & "&sel_Supplier=" & suppId)

%>