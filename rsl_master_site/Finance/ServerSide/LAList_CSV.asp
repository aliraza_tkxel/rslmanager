<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
POFilter = ""
If (Request("PROCESSINGDATE") <> "") Then
	ProcessingDate = FormatDateTime(CDate(Request("PROCESSINGDATE")),2)
	RequiredDate = CDate(Request("PROCESSINGDATE"))
Else
	ProcessingDate = FormatDateTime(CDate(Date),2)
	RequiredDate = Date
End If

Call OpenDB()
'THIS STATEMENT WILL LOCK LA REFUND POSTING FOR A MOMENT....
SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 1 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_LAREFUNDS'"
Conn.Execute (SQL)

'NEXT NEED TO CHECK THAT THE DATA HAS NOT CHANGED SINCE IT WAS SENT OVER...
SQL = "SELECT ISNULL(SUM(RJ.AMOUNT),0) AS TOTALCOST, COUNT(RJ.JOURNALID) AS TOTALCOUNT " &_
		"FROM F_RENTJOURNAL RJ " &_
		"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
		"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
		"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
		"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
		"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
		"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
		"WHERE RJ.PAYMENTTYPE = 23 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " 
		
Call OpenRs(rsCheck, SQL)
TotalSum = 0
TotalCount = 0
if (NOT rsCheck.EOF) then
	TotalSum = rsCheck("TOTALCOST")
	TotalCount = rsCheck("TOTALCOUNT")
end if
Call CloseRs(rsCheck)

'THIS pART CHECKS THE DATA AND REDIRECTS TO THE PREVIOUS PAGE IF IT IS NOT MATCHING
IF ( NOT (CDbl(TotalSum) = Cdbl(Request("TotalSum")) AND CInt(TotalCount) = CInt(Request("TotalCount"))) ) then
	'THIS STATEMENT WILL UNLOCK LA REFUND POSTING FOR A MOMENT....
	SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_LAREFUNDS'"
	Conn.Execute (SQL)
	Response.Redirect "../LAList.asp?PODate=" & ProcessingDate & "&ERR97" & Replace(Date, "/", "") & "=1"
End If

ORIGINATORNUMBER = "658670"
orderBy = "NAME ASC"

SQLCODE = "SELECT ORGID, COUNT(RJ.JOURNALID) AS THECOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, NAME, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER,  " &_
			"'" & ORIGINATORNUMBER & "' AS ORIGINATOR " &_
			"FROM F_RENTJOURNAL RJ " &_
			"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
			"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
			"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
			"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
			"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
			"WHERE RJ.PAYMENTTYPE = 23 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
			" 	GROUP BY ORGID, NAME, ACCOUNTNAME, SORTCODE, ACCOUNTNUMBER	 " &_
			"		ORDER BY " + Replace(orderBy, "'", "''") + ""	

Set fso = CreateObject("Scripting.FileSystemObject")

'GP_curPath = Request.ServerVariables("PATH_INFO")
'GP_curPath = Trim(Mid(GP_curPath,1,InStrRev(GP_curPath,"/")) & UploadDirectory)
'if Mid(GP_curPath,Len(GP_curPath),1)  <> "/" then
'	GP_curPath = GP_curPath & "/"
'end if

    SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTID = 55"
	Call OpenRs(rsDEFAULTS, SQL)
	doc_DEFAULTPATH = rsDEFAULTS("DEFAULTVALUE")
	Call CloseRs(rsDEFAULTS) 

CurrentDate = FormatDateTime(Date,2)
CurrentDate = Replace(CurrentDate, "/", "")
CurrentDate = Replace(CurrentDate, " ", "")

RedirectProcessingDate = ProcessingDate
ProcessingDate = Replace(ProcessingDate, "/", "")
ProcessingDate = Replace(ProcessingDate, " ", "")

'FullPath = Trim(Server.mappath(GP_curPath)) & "\LARefundFiles\"
FullPath = doc_DEFAULTPATH & "\finance\LARefundFiles\"
FileName = "LAR-C" & CurrentDate & "-" & "P" & ProcessingDate

If (fso.FileExists(FullPath & FileName & ".csv")) Then
	GP_FileExist = true
End If   
if GP_FileExist then
	Begin_Name_Num = 0
	while GP_FileExist    
		Begin_Name_Num = Begin_Name_Num + 1
		TempFileName = FileName & "_" & Begin_Name_Num
		GP_FileExist = fso.FileExists(FullPath & TempFileName & ".csv")
	wend  
	FileName = TempFileName
end if
Set MyFile= fso.CreateTextFile(FullPath & FileName & ".csv", True)


Call OpenRS(rsLoader, SQLCODE)

BadDataCount = 0
BadDataTotal = 0
BadInvoiceCount = 0
GoodDataCount = 0
GoodDataTotal = 0
GoodInvoiceCount = 0
'MyFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"
while NOT rsLoader.EOF
	SORTCODE = rsLoader("SORTCODE")
	ACCOUNTNUMBER = rsLoader("ACCOUNTNUMBER")
	ACCOUNTNAME = rsLoader("ACCOUNTNAME")
	if (SORTCODE = "" OR ACCOUNTNUMBER = "" OR ACCOUNTNAME = "" OR isNull(ACCOUNTNAME) OR isNULL(SORTCODE) OR isNULL(ACCOUNTNUMBER)) then
		if (BadDataCount = 0) then
			Set MyBadFile= fso.CreateTextFile(FullPath & FileName & "-BAD.csv", True)
			'MyBadFile.WriteLine "Tenant No,Sort Code,Account Number,Account Name,Amount,Reference"						
		end if
		BadDataCount = BadDataCount + 1
		BadInvoiceCount = BadInvoiceCount + rsLoader("THECOUNT")		
		MyBadFile.WriteLine rsLoader("ORGID") & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
		BadDataTotal = BadDataTotal + rsLoader("PAYMENTAMOUNT")		
	else
		GoodDataCount = GoodDataCount + 1
		GoodInvoiceCount = GoodInvoiceCount + rsLoader("THECOUNT")				
		MyFile.WriteLine rsLoader("ORGID") & "," & rsLoader("NAME") & "," & FormatNumber(rsLoader("PAYMENTAMOUNT"),2,-1,0,0) & "," & ACCOUNTNAME  & "," & SORTCODE & ","  & ACCOUNTNUMBER 		
		GoodDataTotal = GoodDataTotal + rsLoader("PAYMENTAMOUNT")
	end if
	rsLoader.moveNext
wend
CloseRs(rsLoader)
MyFile.Close
if (BadDataCount > 0) then
	MyBadFile.Close
end if

'FINALLY ADD A ROW INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
SQL = ("SET NOCOUNT ON;INSERT INTO F_LADATA (FILEDATE, FILENAME, FILETYPE, FILECOUNT, REFUNDCOUNT, TOTALVALUE, PROCESSDATE, ISFILE) VALUES (" &_
			"'" & FormatDateTime(RequiredDate,1) & "', " &_
			"'" & FileName & ".csv" & "', " &_
			"" & 1 & ", " &_
			"" & GoodDataCount & ", " &_
			"" & GoodInvoiceCount & ", " &_				
			"" & GoodDataTotal & ", " &_
			"'" & FormatDateTime(Date,1) & "', 1);SELECT SCOPE_IDENTITY() AS DATAID ")
Call OpenRs(rsDATA, SQL)
DATAID = rsDATA("DATAID")
Call CloseRs(rsDATA)

'The ORDER OF THE FOLLOWING STATEMENTS IS VERY IMPORTANT, AS THE DATAID IS USED TO GET THE REFERENCE FOR THE NEXT TWO STATEMENTS.
'THIS STATEMENT WILL INSERT N INVOICE ENTRIES INTO F_POBACS.
SQL = "INSERT INTO F_LABACS (JOURNALID, DATAID, PROCESSEDBY, PROCESSDATE) " &_
		"SELECT RJ.JOURNALID, " & DATAID & ", " & SESSION("USERID") & ", '" & FormatDateTime(Date,1) & "' FROM F_RENTJOURNAL RJ " &_
			"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
			"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
			"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
			"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
			"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
			"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
			"WHERE RJ.PAYMENTTYPE = 23 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
				"AND SORTCODE IS NOT NULL AND SORTCODE <> '' " &_
				"AND ACCOUNTNUMBER IS NOT NULL AND ACCOUNTNUMBER <> '' " &_
				"AND ACCOUNTNAME IS NOT NULL AND ACCOUNTNAME <> ''"														
Response.Write SQL
Conn.Execute (SQL)

IF (BadDataCount > 0) THEN
	'also ADD A ROW FOR BAD DATA INTO THE DATABASE SO THAT THE FILES CAN BE RETRIEVED LATER ON
	Conn.Execute ("INSERT INTO F_BACSDATA (FILEDATE, FILENAME, FILETYPE, FILECOUNT, TOTALVALUE, PROCESSDATE, ISFILE) VALUES (" &_
				"'" & FormatDateTime(RequiredDate,1) & "', " &_
				"'" & FileName & "-BAD.csv" & "', " &_
				"" & 2 & ", " &_
				"" & BadDataCount & ", " &_
							
				"" & BadDataTotal & ", " &_
				"'" & FormatDateTime(Date,1) & "', 1) ")																								
END IF

'THIS STATEMENT WILL UNLOCK LA REFUND POSTING ....
SQL = "UPDATE RSL_DEFAULTS SET DEFAULTVALUE = 0 WHERE DEFAULTNAME = 'DIRECTPOSTINGLOCKED_LAREFUNDS'"
Conn.Execute (SQL)


CloseDB()
Response.Redirect "../LACSV.asp?pROCESSINGdATE=" & RedirectProcessingDate
%>
