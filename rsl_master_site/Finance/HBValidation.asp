<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	OpenDB()
	Call BuildSelect(lst_LA, "sel_LA", "F_HBINFORMATION HB INNER JOIN C_TENANCY CT ON HB.TENANCYID = CT.TENANCYID INNER JOIN P__PROPERTY P ON CT.PROPERTYID = P.PROPERTYID	INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID	INNER JOIN G_LOCALAUTHORITY G ON G.LOCALAUTHORITYID = D.LOCALAUTHORITY", " DISTINCT G.LOCALAUTHORITYID, G.DESCRIPTION", "G.DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " onChange=""ChangeLoc()"" ")
	CloseDB()
		
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Business --> HR Tools --> Establishment</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	FormFields[0] = "txt_START|Start Date|DATE|Y"
	FormFields[1] = "sel_LA|Local Authority|SELECT|Y"
	FormFields[2] = "sel_SCHEDULEDATE|Schedule Date|SELECT|Y"

	function save_form() {
		if (!checkForm()) return false;
		RSLFORM.target = "frm_hbval";
		RSLFORM.action = "serverside/hbvalidation_srv.asp";
		RSLFORM.submit();
		}

	function AR(row) {
		if (document.getElementById("chka" + row).checked == true) {
			document.getElementById("chkv" + row).checked = false
			if (document.getElementById("IHB" + row).value == 0){
				document.getElementById("txt_ST" + row).readOnly = false;
				document.getElementById("txt_ED" + row).readOnly = false;
				document.getElementById("txt_AM" + row).readOnly = false;
				document.getElementById("txt_ST" + row).select();
				}
			else {
				document.getElementById("txt_ED" + row).readOnly = false;
				document.getElementById("txt_AM" + row).readOnly = false;
				document.getElementById("txt_ED" + row).select();
				}
			}
		else {
			ResetRow(row)
			}
		reCalculate()			
		}

	function VR(row) {
		if (document.getElementById("chkv" + row).checked == true) {
			document.getElementById("chka" + row).checked = false
			document.getElementById("txt_ST" + row).readOnly = true			
			document.getElementById("txt_ED" + row).readOnly = true;
			document.getElementById("txt_AM" + row).readOnly = true;
			InitialValues = document.getElementById("HD" + row).value
			IV_Array = InitialValues.split("|")
			document.getElementById("txt_ST" + row).value = IV_Array[0]			
			document.getElementById("txt_ED" + row).value = IV_Array[1];
			document.getElementById("txt_AM" + row).value = IV_Array[2];
			}
		else {
			ResetRow(row)
			}
		reCalculate()			
		}

	function ResetRow(row){
		document.getElementById("txt_ST" + row).readOnly = true			
		document.getElementById("txt_ED" + row).readOnly = true;
		document.getElementById("txt_AM" + row).readOnly = true;
		InitialValues = document.getElementById("HD" + row).value
		IV_Array = InitialValues.split("|")
		document.getElementById("txt_ST" + row).value = IV_Array[0]			
		document.getElementById("txt_ED" + row).value = IV_Array[1];
		document.getElementById("txt_AM" + row).value = IV_Array[2];
		}

	function isDate_BESPOKED(itemName, errName){
		dtStr = document.getElementById(itemName).value;
		var daysInMonth = DaysArray(12)
		var pos1=dtStr.indexOf(dtCh)
		var pos2=dtStr.indexOf(dtCh,pos1+1)
		var strDay=dtStr.substring(0,pos1)
		var strMonth=dtStr.substring(pos1+1,pos2)
		var strYear=dtStr.substring(pos2+1)
		strYr=strYear
		if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
		if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
		}
		month=parseInt(strMonth)
		day=parseInt(strDay)
		year=parseInt(strYr)
		if (pos1==-1 || pos2==-1) return false
		if (strMonth.length<1 || month<1 || month>12) return false
		if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]) return false
		if (strYear.length != 4 || year==0 || year<minYear || year>maxYear)	return false
		if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isIntegerScan(stripCharsInBag(dtStr, dtCh))==false) return false
		return true
	}					

	MONTHS = new Array(
	  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul',
	  'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
  
	function process(){
		rowRef = document.getElementsByName("chkHB_V")
		AtLeastOneError	= false
		if (rowRef){
			for (i=0; i<rowRef.length; i++){
				ErrorString = ""
				ID = rowRef[i].id
				rowNum = ID.substring(4, ID.length)
				isCorrect = isDate_BESPOKED("txt_ST" + rowNum)
				if (isCorrect == false) ErrorString += "The Start Date is not a valid date."
				isCorrect = isDate_BESPOKED("txt_ED" + rowNum)
				if (isCorrect == false) ErrorString += "\nThe End Date is not a valid date."

				if (ErrorString == "") {
					//MAKE SURE THE END DATE IS GREATER THAN OR EQUAL TO THE START DATE
					HB_InitialStart = document.getElementById("txt_ST" + rowNum).value
					SDA = HB_InitialStart.split("/")
					HB_InitialStart = SDA[0] + " " + MONTHS[SDA[1]-1] + " " + SDA[2]
					
					//GET THE END DATE
					HB_InitialEnd = document.getElementById("txt_ED" + rowNum).value
					EDA = HB_InitialEnd.split("/")
					HB_InitialEnd = EDA[0] + " " + MONTHS[EDA[1]-1] + " " + EDA[2]
					
					hb_st_tst = new Date(HB_InitialStart)
					hb_ed_tst = new Date(HB_InitialEnd)
					if (hb_st_tst > hb_ed_tst) 
						ErrorString += "The Start Date must be less than or equal to the End Date."
					//end of date checking
					}

				HBAmount = parseFloat(document.getElementById("txt_AM" + rowNum).value)				
				if (isNaN(HBAmount))
					ErrorString += "\nThe value for amount is not a valid number."
				else {
					if (parseFloat(HBAmount) > 0) HBAmount = 0 - parseFloat(HBAmount)
					document.getElementById("txt_AM" + rowNum).value = FormatCurrency(HBAmount)
					}
				if (ErrorString != "") {
					AtLeastOneError = true
					ManualError ("img_" + rowNum, ErrorString, 0)
					}
				else 
					ManualError ("img_" + rowNum, "", 3)				
				}
			}
			
		if (!AtLeastOneError) {
				if (!checkForm()) return false;		
				
				RSLFORM.target = "frm_hbval";
				RSLFORM.action = "serverside/hb_validation_process_srv.asp";
				RSLFORM.submit();	
				}
			}
		
		function ShowNewSchedule() {
			location.href="NewSchedule.asp"
			}
	
		function ChangeLoc() {
			
			RSLFORM.txt_SCHEDULEDTOT.value=""
			RSLFORM.txt_BALANCE.value=""			
			if (RSLFORM.sel_LA.value=="")  {
				RSLFORM.sel_SCHEDULEDATE.options.length=0;
				var oOption = document.createElement("OPTION");
				RSLFORM.sel_SCHEDULEDATE.options.add(oOption);
				oOption.innerText = "Please Select"
				oOption.value = ""
				return false;
				}
				
			var xh=new ActiveXObject("Microsoft.XMLHTTP");
			var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
			var url="/Finance/ServerSide/xmlsrv.asp?id=" + RSLFORM.sel_LA.value + "&task=1";
			
			var nodeMap
			xh.open("POST",url,false);
			xh.send();
			xmlobj=xh.responseXML;
			xmlobj.async = false;
			var t=xmlobj.selectSingleNode("//rs:data")
			
			
			var f=t.childNodes;
			
			RSLFORM.sel_SCHEDULEDATE.options.length=0;
			RSLFORM.sel_SCHEDULEDATE.options[RSLFORM.sel_SCHEDULEDATE.options.length] = new Option("Please Select", "")
			for (var i=0; i<f.length; i++) { 
				var oOption = document.createElement("OPTION");
				RSLFORM.sel_SCHEDULEDATE.options[RSLFORM.sel_SCHEDULEDATE.options.length] = new Option(f[i].getAttribute("sd"), f[i].getAttribute("id"))
				} 
				hb_div.innerHTML="";
			}
	
		function Selectschedule() {
			RSLFORM.txt_SCHEDULEDTOT.value=""
			RSLFORM.txt_BALANCE.value=""			
			RSLFORM.hid_SCHEDULEDATE.value=""						
			if(RSLFORM.sel_SCHEDULEDATE.value=="") return false;
			
			RSLFORM.button.value="Proceed"
			RSLFORM.sel_LA.style.visibility='visible';
			
			var xh=new ActiveXObject("Microsoft.XMLHTTP");
			var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
			
			var url="/Finance/ServerSide/xmlsrv.asp?id=" + RSLFORM.sel_SCHEDULEDATE.value + "&task=2";
			
			var nodeMap
			xh.open("POST",url,false);
			xh.send();
			xmlobj=xh.responseXML;
			
			xmlobj.async = false;
			var t=xmlobj.selectSingleNode("//rs:data/z:row");
			
			
			var Scheduledtot=t.getAttribute("st")
			var balance=t.getAttribute("bl")
			var ScheduleDate = t.getAttribute("sd")

			RSLFORM.txt_SCHEDULEDTOT.value=FormatCurrency(Scheduledtot)
			RSLFORM.txt_BALANCE.value=FormatCurrency(balance)
			RSLFORM.hid_BALANCE.value=FormatCurrency(balance)			
			RSLFORM.hid_SCHEDULEDATE.value = ScheduleDate

			RSLFORM.txt_SCHEDULEDTOT.readOnly=true
			hb_div.innerHTML="";
			}
		
		function reCalculate() {
			rowRef = document.getElementsByName("chkHB_V")			
			TotalValue = 0
			if (rowRef){
				for (i=0; i<rowRef.length; i++){
					JID = rowRef[i].value
					if (document.getElementById("chkv" + JID).checked || document.getElementById("chka" + JID).checked)
						TotalValue += Math.abs(document.getElementById("txt_AM" + JID).value)
					}
				}
				RSLFORM.txt_BALANCE.value = FormatCurrency(parseFloat(RSLFORM.hid_BALANCE.value)-parseFloat(TotalValue))
			}
// -->
</SCRIPT>
<!-- End Preload Script -->


<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0" >
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = "RSLFORM" method=post>

	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR>
			<TD>Select search criteria to bring back appropriate HB schedule entries from the rent account journal.</TD>
		</TR>
		<tr>
			<td> 
			</td>
		</tr>
	</TABLE>
	
	
	
	
        <TABLE id="md" BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2 HEIGHT=40PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
          <TR> 
			
			<td>
				<b>Local Authority:</b>
			</td>
			<td>
				<b>Schedule Date:</b> 
			</td>
            <TD NOWRAP>
				<b>Period End Date:</b>
			</TD>
            <TD>
				<b>Schedules Total:</b>
			</TD>
			<TD>
				<b>Balance:</b>
			</TD>
			
          </TR>
          <TR> 
           
           
			<td><%=lst_LA%> <image src="/js/FVS.gif" name="img_LA" width="15px" height="15px" border="0"></td>
            
            <td>
				<select name="sel_SCHEDULEDATE" class="textbox" onchange="Selectschedule()">
					<option value="0" > Please Select </option>
					
              </select><image src="/js/FVS.gif" name="img_SCHEDULEDATE" width="15px" height="15px" border="0">
			  <input type=hidden name="hid_SCHEDULEDATE">
			</td>
             <TD NOWRAP>
              <INPUT TYPE=TEXT NAME=txt_START CLASS='TEXTBOX100' value="<%=FormatDateTime(Date,2)%>">
              <image src="/js/FVS.gif" name="img_START" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP>
              <INPUT TYPE=TEXT NAME=txt_SCHEDULEDTOT CLASS='TEXTBOX100' style='text-align:right'>
              <image src="/js/FVS.gif" name="img_SCHEDULEDTOT" width="15px" height="15px" border="0"> 
            </TD>
             <TD NOWRAP>
              <INPUT TYPE=TEXT NAME=txt_BALANCE CLASS='TEXTBOX100' style='text-align:right' readonly>
              <INPUT TYPE=hidden NAME=hid_BALANCE CLASS='TEXTBOX100' style='text-align:right' readonly>			  
              
            </TD>
          </TR>
          <tr>
 		    <TD colspan=4 nowrap>
			<input type="button" value=" SCHEDULES " class="RSLButton"  onclick="ShowNewSchedule()" name="button"> 
              &nbsp;&nbsp;
			  <b>Filter :</b> Tenancy <input type="text" name="txt_TENANCYID" value="" class="textbox" size=10 maxlength=7>
              &nbsp;
			  HB Ref <input type="text" name="txt_HBREF" value="" class="textbox" size=10>
              &nbsp;
			  <b>Order By :</b> HB Ref <input type="radio" name="rdo_ORDER" value="1" checked> Surname <input type="radio" name="rdo_ORDER" value="2">
			</td>
			
			<td  NOWRAP align=right>  
			  <input type="button" value="Proceed" class="RSLButton"  onclick="save_form()" name="button">
			 </td>
			
			
            
          </tr>
        </TABLE>
	<!--</div>-->
<BR>
<DIV ID="hb_div"></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_hbval width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

