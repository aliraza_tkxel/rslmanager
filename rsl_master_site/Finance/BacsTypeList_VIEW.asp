<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "

	SQLCODE = "SELECT " &_
			"CASE  INV.PAYMENTMETHOD " &_
			"WHEN 2 THEN 'BACSDD.ASP' " &_
			"WHEN 7 THEN 'BACSLIST.ASP' " &_
			"WHEN 8 THEN 'BACSCHEQUE.ASP' " &_
			"WHEN 9 THEN 'BACSLIST.ASP' " &_									
			"END AS REDIR, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, " &_
			"COUNT(INV.INVOICEID) AS TOTALINVOICES, INV.PAYMENTMETHOD,  PT.DESCRIPTION AS PAYMENTNAME " &_
			"FROM F_INVOICE INV " &_
			"INNER JOIN S_ORGANISATION O ON INV.SUPPLIERID = O.ORGID " &_
			"INNER JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID " &_
			"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
			"WHERE INV.PAYMENTMETHOD IS NOT NULL AND CONVERT(DATETIME,CONVERT(VARCHAR,PB.PROCESSDATE,103),103) = '" & REQUEST("PODATE")& "' " & CompanyFilter &_
			"GROUP BY INV.PAYMENTMETHOD, PT.DESCRIPTION "

	OpenDB()
	
	TableString = ""

	Call OpenRs(rsList, SQLCODE)
	if (NOT rsList.EOF) then
		while NOT rsList.EOF
			TableString = TableString & "<TR STYLE='CURSOR:HAND' ONCLICK=""GO('" & rsList("REDIR") & "')""><TD>" & rsList("PAYMENTNAME") & "</TD><TD>" & rsList("TOTALINVOICES") & "</TD><TD align=right>" & FormatCurrency(rsList("PAYMENTAMOUNT")) & "</TD></TR>"
			rsList.moveNext
		wend
	else
		TableString = "<TR><TD COLSPAN=3 ALIGN=CENTER>No Reconciled purchase orders found.</TD></TR>"
	end if
	CloseRs(rsList)
	CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bacs List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function RemoveBad() { 
	strTemp = event.srcElement.value;
	strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
	event.srcElement.value = strTemp;
	}

function GO(ThePage){
	location.href = ThePage + "?PODATE=<%=Request("PODATE")%>&VIEW=1&Company=<%=Company%>" 
	}

function SaveResults(){
	if (isDate("pROCESSINGdATE","")){
        location.href = "ServerSide/BacsList_CSV.asp?CC_Sort=<%=orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value + "&Company=<%=Company%>"
		}
	}	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack"><tr>
      <td> This page shows the purchase orders which have <b>already been paid</b> 
        grouped by the payment type. To view any individual payment type click 
        on it. <br>
        <br>
</td></tr></table>

  <TABLE CELLPADDING=3 CELLSPACING=0 border=1 width=750 STYLE='BORDER-COLLAPSE:COLLAPSE;BEHAVIOR:URL(/INCLUDES/TABLES/TABLEHL.HTC)' HLCOLOR='STEELBLUE' SLCOLOR=''>
	<THEAD>
    <TR> 
      <TD style='border-bottom:1px solid #133e71'><b>Payment Type</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>Invoice Count</b></TD>
      <TD style='border-bottom:1px solid #133e71'><b>Total Value</b></TD>
    </TR>
	</THEAD>
    <%=TableString%> 
  </TABLE>
</form>
<br>&nbsp<a href='BacsDisplay.asp?date=<%=REQUEST("PODATE")%>&Company=<%=Company%>'><font color=blue><b>BACK to Bacs Calendar</b></font></a>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>