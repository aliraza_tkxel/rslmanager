<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilderCreditNote.asp" -->
<%
	CONST CONST_PAGESIZE = 20

	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (7)	 'USED BY CODE
	Dim DatabaseFields (7)	 'USED BY CODE
	Dim ColumnWidths   (7)	 'USED BY CODE
	Dim TDSTUFF        (7)	 'USED BY CODE
	Dim TDPrepared	   (7)	 'USED BY CODE
	Dim ColData        (7)	 'Syntax	[column title] | [database field] | [display length(px)]
	Dim SortASC        (7)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (7)	 'All Array sizes must match
	Dim TDFunc		   (7)
	Dim clean_desc

	ColData(0)  = "Order No|ORDERID|90"
	SortASC(0) 	= "CN.ORDERID ASC"
	SortDESC(0) = "CN.ORDERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|80"
	SortASC(1) 	= "CNDATE ASC"
	SortDESC(1) = "CNDATE DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""

	ColData(2)  = "By|FULLNAME|120"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"
	TDSTUFF(2)  = ""
	TDFunc(2) = ""

	ColData(3)  = "Supplier|SUPPLIER|120"
	SortASC(3) 	= "SUPPLIER ASC"
	SortDESC(3) = "SUPPLIER DESC"
	TDSTUFF(3)  = ""
	TDFunc(3) = ""

	ColData(4)  = "Name|CNNAME|175"
	SortASC(4) 	= "CNNAME ASC"
	SortDESC(4) = "CNNAME DESC"
	TDSTUFF(4)  = ""
	TDFunc(4) = ""

	ColData(5)  = "REASON|REASON|100"
	SortASC(5) 	= "REASON ASC"
	SortDESC(5) = "REASON DESC"
	TDSTUFF(5)  = ""
	TDFunc(5) = ""
		

    ColData(6)  = "CancelledBy|CANCELBY|100"
	SortASC(6) 	= "CANCELBY ASC"
	SortDESC(6) = "CANCELBY DESC"
	TDSTUFF(6)  = ""
	TDFunc(6) = ""
	

	PageName = "CancelledCreditNote.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(1)
	RowClickColumn = " "" title="""""" & rsSet(""REASON"") & """""" onclick=""""load_me("" & rsSet(""ORDERID"") & "")"""" """

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	If (Request("PONumber") <> "") Then
		If (isNumeric(Request("PONumber"))) Then
			POFilter = " AND CN.ORDERID = '" & Clng(Request("PONumber")) & "' "
		End If
	End If

	SupFilter = ""
	rqSupplier = Request("sel_SUPPLIER")
	If ( rqSupplier <> "") Then
		SupFilter = " AND CN.SUPPLIERID = '" & rqSupplier & "' "
	End If


	
   CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND isnull(CN.COMPANYID,1) = '" & rqCompany & "' "
	end if

	
	rqFiscalYear = ""
	rqFiscalYear = Request("sel_FISCALYEAR")
	If (rqFiscalYear <> "") Then
	    SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YRANGE = " & rqFiscalYear
	    Call OpenDB()
	    Call OpenRs(rsFiscalYear, SQL)
	    If (NOT rsFiscalYear.EOF) Then
		    FiscalFilter = " AND (CN.CNDATE >= '" & FormatDateTime(rsFiscalYear("YSTART"),1) & "' and CN.CNDATE <= '" & FormatDateTime(rsFiscalYear("YEND"),1) & "')"
		End If
	    Call CloseRs(rsFiscalYear)
	    Call CloseDB()
	End If

    TeamIdFilter = ""
	rqTeamId = Request("sel_TEAMS")
	If ( rqTeamId <> "") Then
		TeamIdFilter = " AND T.TEAMID = '" & rqTeamId & "' "
	End If
	
	
	SQLCODE ="select CN.ORDERID ,CONVERT(VARCHAR(10), CN.CNDATE, 103) AS FORMATTEDPODATE "&_
",(C.FIRSTNAME + ' ' + C.LASTNAME) AS FULLNAME "&_
",(S.NAME) AS SUPPLIER "&_
",CN.CNNAME "&_
",1 AS POSTATUSNAME "&_
",1 AS NextActionFor "&_ 
",CN.REASON AS REASON "&_
",((E.FIRSTNAME + E.LASTNAME )  "&_
"+' ('  +CONVERT(VARCHAR(10), CN.CancelDate, 103)+')') as CancelBy  "&_
"from F_CREDITNOTE as CN  "&_
"INNER JOIN C__CUSTOMER AS C ON C.CUSTOMERID = CN.USERID "&_
"INNER JOIN S_ORGANISATION S ON S.ORGID = CN.SUPPLIERID "&_
"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = CN.CANCELBY "&_
"LEFT JOIN E_TEAM T ON T.TEAMID = E.JOBROLETEAMID "&_
"where CN.CNSTATUS =16 and CN.Active =0 " & SupFilter & POFilter & FiscalFilter  & CompanyFilter & TeamIdFilter & "ORDER BY "+ Replace(orderBy, "'", "''")

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	'Response.Write(SQLCODE)
	'Response.End()
  ' Response.write(SQLCODE)
  ' Response.end
  ' Response.flush
	If Request.QueryString("page") = "" Then
		intpage = 1
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If
	Call Create_Table()

	Call OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", rqSupplier, NULL, "textbox200", " style=""width:150px"" ")
	Call BuildSelect(lstFiscalYear, "sel_FISCALYEAR", "F_FISCALYEARS", "YRANGE, CONVERT(VARCHAR, YSTART, 103) + ' - ' + CONVERT(VARCHAR, Yend, 103)", "CONVERT(VARCHAR, YSTART, 103)+ ' - ' + CONVERT(VARCHAR, Yend, 103)", "Please Select...", rqFiscalYear, NULL, "textbox200", " style=""width:150px"" ")
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select", rqCompany, NULL, "textbox200", " style='width:150px'")	
    Call BuildSelect(lstTeams, "sel_TEAMS", "E_TEAM T INNER JOIN G_TEAMCODES TC ON TC.TEAMID = T.TEAMID WHERE T.TEAMID <> 1 AND T.ACTIVE=1", "T.TEAMID, T.TEAMNAME", "T.TEAMNAME", "Please Select", rqTeamId, NULL, "textbox200", " style=""width:150px"" ")
	Call CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Purchase Order List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css" media="all">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript">

    function load_me(Order_id) {
	    if (window.showModelessDialog) {        // Internet Explorer
		    //window.showModalDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
		  } 
        else {
            //window.open ("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "","width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
        }
    }

    function RemoveBad() { 
        strTemp = event.srcElement.value;
        strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
        event.srcElement.value = strTemp;
    }

    function setSupplier() {
	    document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = '<%=Request("sel_SUPPLIER")%>';
        }

        function setCompany() {
	    document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value = '<%=Request("sel_COMPANY")%>';
	}

    function SubmitPage() {
        var PONumber = document.getElementById("PONumber").value;
        var SupplierNumber = document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value;
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
        var FiscalYear = document.getElementById("sel_FISCALYEAR").options[document.getElementById("sel_FISCALYEAR").selectedIndex].value;
        var TeamId = document.getElementById("sel_TEAMS").options[document.getElementById("sel_TEAMS").selectedIndex].value;
	    if (isNaN(PONumber)) {
		    alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.");
		    return false;
	    }
	    
	    location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + PONumber + "&sel_FISCALYEAR=" + FiscalYear + "&sel_TEAMS=" + TeamId;;

	}

    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages();setSupplier()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get" action="">
    <table class="RSLBlack">
         <tr>
            <td>
                <b>Order No: </b>
            </td>
            <td>
                <b>Fiscal Years: </b>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="PONumber" id="PONumber" class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>"
                    onblur="RemoveBad()" style="width: 135px; background-image: url('img/CXfilter.gif');
                    background-repeat: no-repeat; background-attachment: fixed" />
            </td>
            <td>
                <%=lstfiscalyear%>
            </td>
            <td>
                <%=lstPOStatus%>
            </td>
        </tr>
	    <tr>
            <td>
                <b>Company: </b>
            </td>
            <td>
                <b>Suppliers: </b>
            </td>
            <td>
                <b>Directorate: </b>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <%=lstCompany%>
            </td>
            <td>
                <%=lstSuppliers%>
            </td>
            <td>
                <%=lstTeams%>
            </td>
            <td>
                <input type="button" class="RSLButton" value=" Update Search " title="Update Search" onclick="SubmitPage()" />
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="750" height="1" alt="" /></td>
        </tr>
    </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="4px" height="4px" style="display: none">
    </iframe>
</body>
</html>
