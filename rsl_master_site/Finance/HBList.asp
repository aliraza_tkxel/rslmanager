<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	if (Request("LA") <> "" AND isNumeric(Request("LA"))) then
		lasql =	"	INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
			"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
			"	INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID AND D.LOCALAUTHORITY = " & Clng(Request("LA")) & " "
		thela = Clng(Request("LA")) 
		LAname = "SELECT DESCRIPTION FROM G_LOCALAUTHORITY WHERE LOCALAUTHORITYID = " & thela
		OpenDB()
		Call OpenRs(rsLA, LAname)
		if (NOT rsLA.EOF) then
			LA_NAME = rsLA("DESCRIPTION")
		else
			LA_NAME = "UNKNOWN / UNABLE TO ESTABLISH"
		end if
		Call CloseRs(rsLA)
	else
		lasql = ""
		thela = ""
		LA_NAME = "ALL Local Authorities"		
	end if


	
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)

	ColData(0)  = "Tenancy|TENANCYID|90"
	SortASC(0) 	= "rj.TENANCYID ASC"
	SortDESC(0) = "rj.TENANCYID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "TenancyReference(|)"

	ColData(1)  = "Customer Name|FULLNAME|300"
	SortASC(1) 	= "FULLNAME ASC"
	SortDESC(1) = "FULLNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "HB REF|HBREF|120"
	SortASC(2) 	= "HBREF ASC"
	SortDESC(2) = "HBREF DESC"
	TDSTUFF(2)  = ""
	TDFunc(2) = ""

	ColData(3)  = "Payment Type|PAYMENTTYPE|200"
	SortASC(3) 	= "PT.DESCRIPTION ASC"
	SortDESC(3) = "PT.DESCRIPTION DESC"
	TDSTUFF(3)  = ""
	TDFunc(3) = ""

	ColData(4)  = "Start|PAYMENTSTARTDATE|85"
	SortASC(4) 	= "RJ.PAYMENTSTARTDATE ASC"
	SortDESC(4) = "RJ.PAYMENTSTARTDATE DESC"
	TDSTUFF(4)  = ""
	TDFunc(4) = ""

	ColData(5)  = "End|PAYMENTENDDATE|85"
	SortASC(5) 	= "RJ.PAYMENTENDDATE ASC"
	SortDESC(5) = "RJ.PAYMENTENDDATE DESC"
	TDSTUFF(5)  = ""
	TDFunc(5) = ""


	ColData(6)  = "Amount|PAYMENTAMOUNT|90"
	SortASC(6) 	= "PAYMENTAMOUNT ASC"
	SortDESC(6) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(6)  = " ""align='right'"" "
	TDFunc(6) = "FormatCurrency(|)"		
	
	PageName = "HBList.asp"
	EmptyText = "No Housing Benefit items found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("pROCESSINGdATE") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("pROCESSINGdATE")),2)
		RequiredDate = CDate(Request("pROCESSINGdATE"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ButtonDisabled = ""
	
'	Response.Write ProcessingDate & "<BR>"
'	Response.Write RequiredDate & "<BR>"
'	Response.Write Date & "<BR>"	
	
	'two sql scripts are required, the one which appears next shows
	'the direct debits which will potentially come out on the specified date
		if (Request("FILTER") = "" OR NOT isNumeric(Request("FILTER"))) then
			iFilter = -1
		else
			iFilter = Request("FILTER")
		end if
		
		ExtraSQL = ""
		ReturnType = "Actual"		
		if (iFilter = 1) then
			ExtraSQL = " RJ.PAYMENTTYPE = 1 "
			ReturnType = "Housing Benefit(s)"			
		elseif (iFilter = 15) then
			ExtraSQL = " RJ.PAYMENTTYPE = 15 "
			ReturnType = "HB Over Payment(s)"			
		elseif (iFilter = 16) then
			ExtraSQL = " RJ.PAYMENTTYPE = 16 "
			ReturnType = "HB Back Payment(s) "			
		else
			ExtraSQL = " RJ.PAYMENTTYPE IN (1,15,16) "
			ReturnType = "ALL HB"			
		END IF
		'the sql below shows the direct debit transactions which have occured.

		SQLCODE = "SELECT FULLNAME = CASE RJ.PAYMENTTYPE WHEN 1 THEN ISNULL(C.FIRSTNAME + ' ','') + C.LASTNAME ELSE CNG.LIST END, " &_
					"RJ.TRANSACTIONDATE, ISNULL(HB.HBREF,'N/A') AS HBREF, RJ.TENANCYID, RJ.PAYMENTSTARTDATE, RJ.PAYMENTENDDATE, " &_
					"ISNULL(RJ.AMOUNT,0) AS PAYMENTAMOUNT, PT.DESCRIPTION AS PAYMENTTYPE " &_
					"FROM F_RENTJOURNAL RJ INNER JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = RJ.PAYMENTTYPE " & lasql &_
					"	LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.JOURNALID = RJ.JOURNALID " &_
					"	LEFT JOIN F_HBINFORMATION HB ON HB.HBID = HBA.HBID " &_
					"	LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = HB.CUSTOMERID " &_
					"	LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNG ON CNG.I = RJ.TENANCYID " &_
					"	WHERE STATUSID <> 1 AND RJ.TRANSACTIONDATE = '" & FormatDateTime(RequiredDate,1) & "' AND " & ExtraSQL &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	Call OpenDB()
	Call BuildSelect(lstPT, "sel_PAYMENTTYPE", "F_PAYMENTTYPE WHERE PAYMENTTYPEID IN (1,15,16)", "PAYMENTTYPEID, DESCRIPTION", "DESCRIPTION", "ALL", Request("FILTER"), NULL, "textbox", NULL)		


	SQLTOTAL = "SELECT ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT " &_
				"FROM F_RENTJOURNAL RJ INNER JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = RJ.PAYMENTTYPE " & lasql &_
				"	LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.JOURNALID = RJ.JOURNALID " &_
				"	LEFT JOIN F_HBINFORMATION HB ON HB.HBID = HBA.HBID " &_
				"	WHERE STATUSID <> 1 AND RJ.TRANSACTIONDATE = '" & FormatDateTime(RequiredDate,1) & "' AND " & ExtraSQL 
	Call OpenRs(rsTOTAL, SQLTOTAL)
	if (NOT rsTOTAL.EOF) then
		THETOTAL = rsTOTAL("PAYMENTAMOUNT")
	else
		THETOTAL = 0
	end if
	Call CloseRs(rsTOTAL)

	Call CloseDB()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Housing Benefit List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function cHECKpAGE(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "<%=PageName & "?CC_Sort=" & orderBy%>&LA=<%=thela%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value + "&Filter=" + thisForm.sel_PAYMENTTYPE.value
		}
	}

function CancelBubble(){
	event.cancelBubble = true
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack" width=750><tr><td><b>&nbsp;DATE</b></td>
<td><input type=text name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>" style='width:100px;'></td>
<TD><%=lstPT%></TD>
<td><input type=button class="RSLButton" value=" Update Search " onclick="cHECKpAGE()" <%=ButtonDisabled%>>
</td><td nowrap> &nbsp;&nbsp;Showing '<b><%=ReturnType%></b>'.
</td>
<td align=right width=100%>&nbsp<a href='HBDisplay.asp?sel_LA=<%=thela%>&date=<%=Request("pROCESSINGdATE")%>'><font color=blue><b>HB Calendar</b></font></a></td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;<b>LOCAL AUTHORITY: <i><font color=blue><%=LA_NAME%></font></i></b></TD>
      <TD align=right height=20><a href="#" onclick="javascript:window.open('Popups/TenantCheck.asp','','width=530,height=200')" style='text-decoration:none;color:red'>TENANCY CHECK</a>&nbsp;&nbsp;&nbsp;<b>TOTAL: <font color=red><%=FormatCurrency(THETOTAL)%></font></b>&nbsp;</TD>	  
	</TR>
	<TR>
		<TD colspan=2 BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>