<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
fiscalyear = Request("FiscalYear")
if (NOT isNumeric(fiscalyear)) then
	fiscalyear = ""
end if

set rsFYear = Server.CreateObject("ADODB.Recordset")
rsFYear.ActiveConnection = RSL_CONNECTION_STRING
if (fiscalyear = "" or isNull(fiscalyear)) then
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
else
	rsFYear.Source = "SELECT YRange, YStart, YEnd FROM dbo.F_FiscalYears WHERE YRange = " & fiscalyear 
end if
rsFYear.CursorType = 0
rsFYear.CursorLocation = 2
rsFYear.LockType = 3
rsFYear.Open()
if (rsFYear.EOF) Then
	rsFYear.close()
	today = FormatDateTime(Date, 1)
	rsFYear.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears  WHERE YStart <= '" & today & "' and YEnd >= '" & today & "'"
	rsFYear.Open()
End if
rsFYear_numRows = 0

fiscalyear = rsFYear("YRange")
yearstart = FormatDateTime(rsFYear("YStart"),1)
yearend = FormatDateTime(rsFYear("YEnd"),1)

MonthST = Month(CDate(yearstart))
MonthEN = Month(CDate(yearend))
rsFYear.close()

' This report will only work for date ranges which are at most 1 year apart!!!!

Dim WriteOrder()
Counter = -1
if (MonthST = MonthEN) Then
	Counter = Counter + 1
	Redim Preserve WriteOrder(Counter)
	WriteOrder(Counter) = MonthST
Else
	while (MonthST <> MonthEN)
		Counter = Counter + 1
		Redim Preserve WriteOrder(Counter)
		WriteOrder(Counter) = MonthST
		MonthST = MonthST + 1
		if MonthST = 13 then
			MonthST = 1
		end if
	Wend
	Counter = Counter + 1
	Redim Preserve WriteOrder(Counter)
	WriteOrder(Counter) = MonthST
End if

IsReconciled = " > 0 AND PO.POSTATUS < 9 "
%>
<%
set rsTheYears = Server.CreateObject("ADODB.Recordset")
rsTheYears.ActiveConnection = RSL_CONNECTION_STRING
rsTheYears.Source = "SELECT YRange, YStart, YEnd  FROM dbo.F_FiscalYears"
rsTheYears.CursorType = 0
rsTheYears.CursorLocation = 2
rsTheYears.LockType = 3
rsTheYears.Open()
rsTheYears_numRows = 0
%>
<%
set rsFundRows = Server.CreateObject("ADODB.Recordset")
rsFundRows.ActiveConnection = RSL_CONNECTION_STRING
rsFundRows.Source = "SELECT CC.DESCRIPTION, CC.COSTCENTREID, SUM(QuotedCost) as totalpurchases, Month(PODate) as themonth "&_
      "FROM F_purchaseorder PO, F_HEAD HE, F_COSTCENTRE CC "&_
      "WHERE PO.ACTIVE = 1 AND "&_
           "PO.HEADID = HE.HEADID AND "&_
           "HE.COSTCENTREID = CC.COSTCENTREID AND "&_		   
           "PO.POSTATUS " & IsReconciled & " "&_
		"AND PODate >= '" & yearstart & "' AND PODate <= '" & yearend & "' "&_
	"group by CC.DESCRIPTION, CC.COSTCENTREID, Month(PODate) "&_
	"order by CC.DESCRIPTION, Month(PODate)"
rsFundRows.CursorType = 0
rsFundRows.CursorLocation = 2
rsFundRows.LockType = 3
rsFundRows.Open()
rsFundRows_numRows = 0
%>
<HTML>
<HEAD>
<TITLE>RSL Finance --> Reports --> NON - Reconciled Purchases</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<script language = "javascript">
<!--
function showRows(toShow){
	if (toShow.style.display == "block")
		toShow.style.display='none';
	else
		toShow.style.display = "block";
	}

var showMode = 'table-cell';
if (document.all) showMode='block';
function toggleVis(myVal, hide){
	cells1 = document.getElementsByName('tcol' + myVal);
	cells2 = document.getElementsByName('tcol' + (myVal+1));
	cells3 = document.getElementsByName('tcol' + (myVal+2));
	if (hide)
		mode = 'none';
	else
		mode = showMode;
	if (cells1.length){
		for(j = 0; j < cells1.length; j++) {
			cells1[j].style.display = mode;
			}
		}
	if (cells2.length){
		for(j = 0; j < cells2.length; j++) {
			cells2[j].style.display = mode;
			}
		}
	if (cells3.length){
		for(j = 0; j < cells3.length; j++) {
			cells3[j].style.display = mode;				
			}
		}
	}

function goGo(){
	thisForm.submit();
}		
//-->
</script>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
	  <form name=thisForm>
<br>Please select the year for which you would like to generate the report: &nbsp;
<select name=fiscalyear class="RSLBlack" style='width:200px' onchange=goGo()>
                <option value="">Please Select</option>
                <%
While (NOT rsTheYears.EOF)
isSelected = ""
if (CStr(fiscalyear) = CStr(rsTheYears.Fields.Item("YRange").Value)) Then
	isSelected = " selected"
end if
%>
                <option value="<%=(rsTheYears.Fields.Item("YRange").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YStart").Value) & " - " & (rsTheYears.Fields.Item("YEnd").Value)%></option>
                <%
  rsTheYears.MoveNext()
Wend
If (rsTheYears.CursorType > 0) Then
  rsTheYears.MoveFirst
Else
  rsTheYears.Requery
End If
%>
              </select><br>
              <br>
              <table bgcolor=#FFFFFF style='border-collapse:collapse;border-right:1px solid silver' class="RSLBlack" border=1>
		<%
		MonthLength = UBound(WriteOrder)
		Response.Write "<TR><td colspan=20 align=center><b><font color=black>NON Reconciled <font color=gray>Purchase Report for Financial Year: " & yearstart & " - " & yearend &"</b></TD></TR>"
		Response.Write "<TR><TD nowrap width=200px>&nbsp;</TD>"
		For e=0 to MonthLength
			if (e < 3) Then
				Response.Write "<TD align=center nowrap width=70px id='tcol"&e&"'>" & MonthName(WriteOrder(e), true) & "</TD>"
			else
				Response.Write "<TD align=center nowrap width=70px id='tcol"&e&"' style='display:none'>" & MonthName(WriteOrder(e), true) & "</TD>"
			end if			
			Select Case e
				Case 2		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=70px align=center onclick='toggleVis(3,true);toggleVis(6,true);toggleVis(9,true);toggleVis(0)'>Q1</TD>"		
				Case 5		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=70px align=center onclick='toggleVis(0,true);toggleVis(6,true);toggleVis(9,true);toggleVis(3)'>Q2</TD>"		
				Case 8		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=70px align=center onclick='toggleVis(3,true);toggleVis(0,true);toggleVis(9,true);toggleVis(6)'>Q3</TD>"		
				Case 11		Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=70px align=center onclick='toggleVis(3,true);toggleVis(6,true);toggleVis(0,true);toggleVis(9)'>Q4</TD>"		
				Case MonthLength  Num = (MonthLength\3)*3
								  HideTog = ""
								  for y=2 to 11 Step 3
								  	if y < MonthLength Then
										HideTog = HideTog & "toggleVis(" & y-2&", true);"
									end if
								  Next
	       						  Response.Write "<TD bgcolor=lightgreen style='cursor:hand' nowrap width=70px align=center onclick='"&HideTog&"toggleVis(" & Num & ")'>Q"&(Num/3+1)&"</TD>"								  
			End Select			
		Next
		Response.Write "<TD width=70px nowrap align=center>Total</TD></TR>"
		%>
		<%
		Function WriteAQuarter(StartQuarter, EndQuarter, isHead)
			QuarterTotal = 0
			For w=StartQuarter to EndQuarter
				tempMonthTotal =  FundLine(WriteOrder(w))
				if (tempMonthTotal <> "") then
					QuarterTotal = QuarterTotal + tempMonthTotal
				End if
			Next
			if (isHead) then
				Response.Write "<TD align=right bgcolor=lightgreen>" & FormatNumber(QuarterTotal,0) & "</TD>"
			else
				Response.Write "<TD align=right bgcolor=green><font color=white><b>" & FormatNumber(QuarterTotal,0) & "</b></TD>"			
			end if
		End Function
		
		Function WriteTotal 
			lineTotal = 0
			For w=0 to MonthLength
				tempMonthTotal =  FundLine(WriteOrder(w))
				if (tempMonthTotal <> "") then
					lineTotal = lineTotal + tempMonthTotal
				End if
				FundLine(WriteOrder(w)) = ""
			Next
			finalTotal = finalTotal + lineTotal
			Response.Write "<TD align=right>" & FormatNumber(lineTotal,0) & "</TD>"		
		End Function
		
		Function WriteAFundLine (isHead)
			if (isHead) then
				Response.Write "<TR color=blue><TD nowrap>&nbsp;&nbsp;" & prevHead_Name & "</TD>"
			else
				Response.Write "<TR style='cursor:hand' onclick=""showRows(RR" & prevFund_ID & ")"" bgcolor=beige><TD nowrap><b>" & prevFund_Name & "</b></TD>"
			end if			
			For j=0 to MonthLength
				if (j < 3) then
					Response.Write "<TD align=right id='tcol"&j&"'>" & FundLine(WriteOrder(j)) & "</TD>"
				else
					Response.Write "<TD align=right id='tcol"&j&"' style='display:none'>" & FundLine(WriteOrder(j)) & "</TD>"
				end if				
				Select Case j
					Case 2		WriteAQuarter 0, 2, isHead		
					Case 5		WriteAQuarter 3, 5, isHead		
					Case 8		WriteAQuarter 6, 8, isHead		
					Case 11		WriteAQuarter 9, 11, isHead
					Case MonthLength  WriteAQuarter (MonthLength\3)*3, MonthLength, isHead
				End Select
			Next
			Writetotal
			Response.Write "</TR>"			
		End Function
		
		Dim FundLine(12)
		Dim Totals(5)
		Dim Head_Name, prevHead_Name
		Dim prevFund_ID, finalTotal
		finalTotal = 0
		prevFund_Name = -1
		For i=0 to MonthLength
			FundLine(i) = ""
		Next
		if (rsFundRows.EOF) then
			Response.Write "<TR><TD colspan=20 align=center><b><font color=gray>No purchases made for this financial year</b></td></tr>"
		end if		
		while (NOT rsFundRows.EOF) 
			Fund_Name = rsFundRows("DESCRIPTION")		  	
			If (prevFund_Name <> -1 and Fund_Name <> prevFund_Name) then
				WriteAFundLine false
				GetHeadsForFund prevFund_ID			
			End if
			prevFund_Name = Fund_Name
			prevFund_ID = rsFundRows("COSTCENTREID")
			FundLine(CInt(rsFundRows("themonth"))) = FormatNumber(rsFundRows("totalpurchases"),0)
			rsFundRows.moveNext()
		Wend
		If (prevFund_Name <> -1) then
			WriteAFundLine false
			GetHeadsForFund prevFund_ID			
		End if		

		Response.Write "<TR color=blue><TD nowrap><font color=blue><b>Year Total</b></font></TD>"
		For j=0 to MonthLength
			if (j < 3) then
				Response.Write "<TD align=right id='tcol"&j&"'></TD>"
			else
				Response.Write "<TD align=right id='tcol"&j&"' style='display:none'></TD>"
			end if
			Select Case j
				Case 2		Response.Write "<TD></TD>"
				Case 5		Response.Write "<TD></TD>"		
				Case 8		Response.Write "<TD></TD>"		
				Case 11		Response.Write "<TD></TD>"
				Case MonthLength  Response.Write "<TD></TD>"
			End Select
		Next
		Response.Write "<TD align=right><b>�" & FormatNumber(finalTotal/2,0) & "</b></TD>"
		Response.Write "</TR>"			

		Function GetHeadsForFund (COSTCENTREID)
			set rsHeadRows = Server.CreateObject("ADODB.Recordset")
			rsHeadRows.ActiveConnection = RSL_CONNECTION_STRING
			rsHeadRows.Source = "SELECT HE.DESCRIPTION AS HEADNAME, isnull(SUM(QuotedCost),0) as totalpurchases, "&_
					"Month(PODATE) as themonth "&_
					"FROM F_PURCHASEORDER PO, F_HEAD HE, F_COSTCENTRE CC "&_
					"WHERE PO.active = 1 AND "&_
					"PO.HEADID = HE.HEADID AND "&_
					"HE.COSTCENTREID = CC.COSTCENTREID AND "&_
					"PO.POSTATUS " & IsReconciled & " "&_
					"AND PODate >= '" & yearstart & "' AND PODate <= '" & yearend & "' "&_
					"AND CC.COSTCENTREID = " & COSTCENTREID & " " &_
				"group by HE.DESCRIPTION, Month(PODATE) "&_
				"order by HE.DESCRIPTION"
			rsHeadRows.CursorType = 0
			rsHeadRows.CursorLocation = 2
			rsHeadRows.LockType = 3
			rsHeadRows.Open()
			rsHeadRows_numRows = 0
			
			prevHead_Name = -1
			Response.Write "<tbody id='RR" & prevFund_ID & "' style='display:none'>"
			while (NOT rsHeadRows.EOF) 
				Head_Name = rsHeadRows("HEADNAME")		  	
				If (prevHead_Name <> -1 and Head_Name <> prevHead_Name) then
					WriteAFundLine true
				End if
				prevHead_Name = Head_Name
				FundLine(CInt(rsHeadRows("themonth"))) = FormatNumber(rsHeadRows("totalpurchases"),0)
				rsHeadRows.moveNext()
			Wend
			If (prevHead_Name <> -1) then
				WriteAFundLine true
			End if
			Response.Write "</tbody>"
			rsHeadRows.close()						
		End Function
		%>
		</table>
		</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
rsFundRows.Close()
%>
<%
rsTheYears.Close()
%>