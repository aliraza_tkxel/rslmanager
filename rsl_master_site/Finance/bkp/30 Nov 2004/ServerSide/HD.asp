<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Function PCase(strInput)
    Dim iPosition ' Our current position in the string (First character = 1)
    Dim iSpace ' The position of the next space after our iPosition
    Dim strOutput ' Our temporary string used to build the function's output

    iPosition = 1

    Do While InStr(iPosition, strInput, " ", 1) <> 0
            iSpace = InStr(iPosition, strInput, " ", 1)
            strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
            strOutput = strOutput & LCase(Mid(strInput, iPosition + 1, iSpace - iPosition))
            iPosition = iSpace + 1
    Loop

    strOutput = strOutput & UCase(Mid(strInput, iPosition, 1))
    strOutput = strOutput & LCase(Mid(strInput, iPosition + 1))

    PCase = strOutput
End Function
	
	Dim treeText
	Dim CCID, headname, headcode, headallocation, active, datecreated, HDID, totalallocated, totalfund, totalremaining, allocated, minimumTotal
	
Function GetFormFields ()
	headallocation = Request.Form("txt_headallocation")
	if headallocation = "" then headallocation = null end if
	headname = Request.Form("txt_HeadDescription")
	if headname = "" then headname = null end if	
	active = Request.Form("rdo_headactive")
	if active = "" then active = 0 end if	
	CCID = Request.Form("CCID")
	if CCID = "" then CCID = 0 end if
End Function

Function NewRecord ()
	GetFormFields()

	SQL = "INSERT INTO F_HEAD (DESCRIPTION, HEADDATE, USERID, ACTIVE, LASTMODIFIED, HEADALLOCATION, COSTCENTREID) " &_
			"VALUES ('" & headname & "', " &_
					"'" & Date & "', "&_
					"" & Session("UserID") & ", "&_
					"" & active & ", "&_
					"'" & Date & "', "&_
					"" & headallocation & ", "&_
					"'" & CCID & "') "

	Response.Write SQL
		
	Conn.Execute (SQL)
		
End Function

Function UpdateRecord (theID)
	GetFormFields()
		
	SQL = "UPDATE F_HEAD " &_
			"SET DESCRIPTION = '" & headname & "', " &_
					"USERID = " & Session("UserID") & ", "&_
					"ACTIVE = " & active & ", "&_
					"LASTMODIFIED = '" & Date & "', "&_
					"HEADALLOCATION = " & headallocation & " "&_
					"WHERE HEADID = " & theID

	Response.Write SQL
		
	Conn.Execute (SQL)
End Function

Function GetData(theID)
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT * FROM F_HEAD WHERE HEADID = " & theID & ""
	Set Rs = Conn.Execute(strEventQuery)

	headname = Rs("DESCRIPTION")
	headallocation = Rs("HeadAllocation")
	if (headallocation <> "") then
		headallocation = FormatNumber(headallocation,2,-1,0,0)
		DBL_headallocation = CDbl(headallocation)
	else 
		headallocation = "0.00"
		DBL_headallocation = CDbl(0)
	end if
	CCID = Rs("COSTCENTREID")
	active = Rs("active")
	
	Rs.Close
	Set Rs = Nothing
	
	//find the total fund value
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT COSTCENTREID, COSTCENTREALLOCATION from F_COSTCENTRE where COSTCENTREID = " & CCID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("COSTCENTREALLOCATION"))
	totalfund = FormatNumber(Rs2("COSTCENTREALLOCATION"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select sum(headallocation) as totalallocated from F_HEAD where COSTCENTREID = " & CCID & " and HEADID <> " & HDID & " group by COSTCENTREID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("totalallocated")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_headallocation - CDbl(Rs3("totalallocated"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = FormatNumber(DBL_totalfund - DBL_headallocation,2,-1,0,0)
	end if
	Rs3.Close
	Set Rs3 = Nothing
	
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "select isNull(sum(EXPENDITUREALLOCATION),0) AS TOTALEXPENDITURE from F_EXPENDITURE EX left join F_HEAD HD on HD.HEADID = EX.HEADID where HD.HEADID = " & theID & ""
	Set Rs4 = Conn.Execute(strEventQuery)
	minimumTotal = Cdbl(Rs4("TOTALEXPENDITURE"))
	Rs4.Close
	Set Rs4 = Nothing	
End Function	

Function GetCostCentreData ()
	headallocation = "0.00"
	DBL_headallocation = CDbl(0)	
	//find the total fund value
	Set Rs2 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT COSTCENTREID, COSTCENTREALLOCATION from F_COSTCENTRE where COSTCENTREID = " & CCID & ""
	Set Rs2 = Conn.Execute(strEventQuery)
	DBL_totalfund = CDbl(Rs2("COSTCENTREALLOCATION"))
	totalfund = FormatNumber(Rs2("COSTCENTREALLOCATION"),2,-1,0,0)
	Rs2.Close
	Set Rs2 = Nothing

	Set Rs3 = Server.CreateObject("ADODB.Recordset")
	strEventQuery = "SELECT SUM(HEADALLOCATION) AS TOTALALLOCATED FROM F_HEAD where COSTCENTREID = " & CCID & " group by COSTCENTREID"
	Set Rs3 = Conn.Execute(strEventQuery)
	if (NOT Rs3.EOF) then
		allocated = FormatNumber((Rs3("TOTALALLOCATED")),2,-1,0,0)
		totalremaining = DBL_totalfund - DBL_headallocation - CDbl(Rs3("TOTALALLOCATED"))
		totalremaining = FormatNumber(totalremaining,2,-1,0,0)
	else
		allocated = "0.00"
		totalremaining = totalfund
	end if
	Rs3.Close
	Set Rs3 = Nothing
End Function

Function DisplayMiniTree(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT HD.DESCRIPTION AS HEADNAME, CC.DESCRIPTION AS COSTCENTRENAME FROM F_HEAD HD Left join F_COSTCENTRE CC on HD.COSTCENTREID = CC.COSTCENTREID WHERE (HD.HEADID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='iagManagerSmallBlk' width=370px><tr><td><b><u>Update Head</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("COSTCENTRENAME") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;" & Rs4("headname") & "</td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DisplayMiniTree2(theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT DESCRIPTION FROM F_COSTCENTRE WHERE (COSTCENTREID = " & theID & ")"
	Set Rs4 = Conn.Execute(strEventQuery)
	treeText = "<table cellpadding='0' cellspacing='0' class='RSLBlack' width=370px><tr><td><b><u>Add New Head</u></b></td></tr><tr><td>&nbsp;</td></tr>"
	treeText = treeText & "<tr><td valign=center>&nbsp;" & Rs4("DESCRIPTION") & "</td></tr>"
	treeText = treeText & "<tr><td valign=center><img src='img/empty.gif' width='18' height='18' align='absmiddle'><img src='img/joinbottom.gif' width='18' height='18' align='absmiddle'>&nbsp;<font color=red>...</font></td></tr>"
	treeText = treeText & "<tr><td>&nbsp;</td></tr></table>"
	Rs4.close()
	Set Rs4 = Nothing
End Function

Function DelRecord (theID)
	Set Rs4 = Server.CreateObject("ADODB.Recordset")
	Dim StrEventQuery
	strEventQuery = "SELECT COUNT(EXPENDITUREID) AS THECOUNT from F_EXPENDITURE where HEADID = " & theID
	Set Rs4 = Conn.Execute(strEventQuery)
	ActualCount = Rs4("THECOUNT")
	Rs4.close()
	Set Rs4 = Nothing
			
	if (ActualCount = 0) then
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Set Rs = Conn.Execute ("DELETE FROM F_HEAD WHERE HEADID = " & theID & ";")
		treeText = "Head deleted successfully."		
	else
		treeText = "Sorry, cannot delete the selected head as it is has (" & ActualCount & ") Expenditure Item(s) setup underneath it. You can set the head in-active instead."
	end if

End Function

ACTION_TO_TAKE = Request("HD_A")
HDID = Request("HDID")

OpenDB()

If (ACTION_TO_TAKE = "ADD") Then
	NewRecord()
ElseIf (ACTION_TO_TAKE = "LFD") Then
	CCID = Request("CCID")
	GetCostCentreData()
	DisplayMiniTree2(CCID)	
ElseIf (ACTION_TO_TAKE = "L") Then
	GetData(HDID)
	DisplayMiniTree(HDID)
ElseIf (ACTION_TO_TAKE = "DELETE") Then
	DelRecord(HDID)
ElseIf (ACTION_TO_TAKE = "UPDATE") Then
	UpdateRecord(HDID)
End If

CloseDB()
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bgcolor="#FFFFFF" text="#000000" onload="returnData()">
<script language=javascript defer>
function returnData(){
<% if ACTION_TO_TAKE = "ADD" Then %>
		parent.refreshSideBar();
		parent.setText("New head added successfully.",1);			
<% Elseif ACTION_TO_TAKE = "UPDATE" Then %>
		parent.refreshSideBar();
		parent.setText("Head updated successfully.",1);	
<% Elseif ACTION_TO_TAKE = "LFD" Then %>
		parent.ResetDiv('HEAD');
		parent.setCheckingArray('HEAD');
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("HEAD",1);				
		parent.thisForm.CCID.value = "<%=CCID%>";
		parent.thisForm.HD_A.value = "ADD";
		parent.thisForm.mintotal.value = "0.00";		
		parent.thisForm.txt_headallocation.value = "<%=headallocation%>";
		parent.thisForm.totalallocated.value = "<%=allocated%>";
		parent.thisForm.totalremaining.value = "<%=totalremaining%>";
		parent.thisForm.totalfund.value = "<%=totalfund%>";		
		parent.Head.style.display = "block";
<% Elseif ACTION_TO_TAKE = "DELETE" Then %>
		parent.refreshSideBar();
		parent.setText("<%=treeText%>",1);	
<% Elseif ACTION_TO_TAKE = "L" Then %>
		parent.NewItem(2);
		parent.setText("<%=treeText%>");
		parent.showDeleteButton("HEAD",2);									
		parent.thisForm.txt_headallocation.value = "<%=headallocation%>";
		parent.thisForm.txt_HeadDescription.value = "<%=headname%>";
		parent.thisForm.totalallocated.value = "<%=allocated%>";
		parent.thisForm.mintotal.value = "<%=minimumTotal%>";		
		parent.thisForm.totalremaining.value = "<%=totalremaining%>";
		parent.thisForm.totalfund.value = "<%=totalfund%>";		
		parent.thisForm.CCID.value = "<%=CCID%>";
		parent.thisForm.HDID.value = "<%=HDID%>";
		if ("<%=active%>" == "True")
			parent.thisForm.rdo_headactive[0].checked = true;
		else
			parent.thisForm.rdo_headactive[1].checked = true;				
		parent.thisForm.HD_A.value = "UPDATE";
		parent.Head.style.display = "block";
		parent.setCheckingArray('HEAD');
<% End if %>
	}
</script>
	

</body>
</html>
