<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	'''''' PAGE NOTE: '''''
	' 7/june/2004 - This page was originally a mirror of CreateScheme.asp with slight 
	' adjustments to allow amendment of expenditures
	' <<<< new note here please >>>>>
	'''''''''''''''''''''''
	
	'7/june/2004 - Get the Head ID from AmendScheme pop up
	Dim HDID
	HDID = Request.Form("HDID")
	
	OpenDB()

	CheckList = Replace(Request.Form("iChecks"), " ", "")

	'GET THE INFORMATION REQUIRED TO CREATE A NEW HEAD SCHEME	
	SchemeName = Request.Form("txt_SCHEME")
	SchemeTotal = Request.Form("txt_SCHEMEMANUALTOTAL")
	CurrentDate = FormatDateTime(Date,1)
	
	' 7/june/2004 - Construct SQL statement to amend the details of the development
	' Agreed by Jimmy & Zanfa baby that USERID is last person to modify
	
	SQL = "UPDATE F_HEAD " & _
		  "SET F_HEAD.DESCRIPTION = '" & SchemeName & "', " &_
		  " F_HEAD.USERID = " & Session("USERID") & ", " &_  
		  " F_HEAD.ACTIVE = 1, "&_
		  " F_HEAD.LASTMODIFIED = '" & CurrentDate & "', "&_
		  " F_HEAD.HEADALLOCATION = " & SchemeTotal & " "&_
		  " WHERE ((F_HEAD.HEADID= " & HDID & "))"
	Conn.Execute(SQL)
		
	'SPLIT THE CHECKLIST INTO AN ARRAY SO WE CAN LOOP THORUGH EACH OF THE ITEMS....	
	CheckArray = Split(CheckList, ",")

	'Declare vars for strings and counters
	Dim ExID, NoStatus, ICounter
	
	
	ExIDList = ""
	ICounter = 0
	
	'Find the size of the array
	theArraySize =  Ubound(CheckArray)
	 
	'Loop through array of Expend records in array
	for each ExpendIDNumber in CheckArray
	
		ExID =  Request.Form("txt_EXP" & ExpendIDNumber)
		ExDescription = Request.Form("txt_Code" & ExpendIDNumber)

		'If the record in the array does already exist in the db then AMEND the data
		If ExID <> "" then
		
				'Amend the expenditure info that match with the expend IDs			
				SQL = "UPDATE F_EXPENDITURE " & _
				  "SET F_EXPENDITURE.DESCRIPTION = '" & ExDescription & "', " &_
				  "F_EXPENDITURE.EXPENDITUREALLOCATION = " & Request.Form("txt_Val" & ExpendIDNumber) & ", "  &_
				  "F_EXPENDITURE.USERID = " & Session("USERID") & ", " &_
				  "F_EXPENDITURE.ACTIVE = 1, " &_
				  "F_EXPENDITURE.LASTMODIFIED = '" & CurrentDate & "' " &_
				  " WHERE ((F_EXPENDITURE.HEADID= " & HDID & " AND F_EXPENDITURE.EXPENDITUREID = " & ExID & " ))"
				  Conn.Execute(SQL)  
				  response.write SQL & "<BR><BR>"
		else
		
			SQL = "Select * from F_EXPENDITURE where headid = " & HDID & " AND DESCRIPTION = '" & ExDescription & "'"
			response.write SQL & "<BR>"
			Call OpenRs(rsInspectEx, SQL)
			IF not rsInspectEx.eof THEN tempExID = rsInspectEx("EXPENDITUREID") End If
			CloseRs(rsInspectEx)
			
			
			If tempExID <> "" then
				ExID = tempExID
				'Amend the expenditure info that match with the expend IDs			
				SQL = "UPDATE F_EXPENDITURE " & _
				  "SET F_EXPENDITURE.DESCRIPTION = '" & ExDescription & "', " &_
				  "F_EXPENDITURE.EXPENDITUREALLOCATION = " & Request.Form("txt_Val" & ExpendIDNumber) & ", "  &_
				  "F_EXPENDITURE.USERID = " & Session("USERID") & ", " &_
				  "F_EXPENDITURE.ACTIVE = 1, " &_
				  "F_EXPENDITURE.LASTMODIFIED = '" & CurrentDate & "' " &_
				  " WHERE ((F_EXPENDITURE.HEADID= " & HDID & " AND F_EXPENDITURE.EXPENDITUREID = " & ExID & " ))"
				  Conn.Execute(SQL)  
				  response.write SQL & "<BR><BR>"
			Else
		
				' INSERT new expenditure and retrieve the ID number		
				SQL = "SET NOCOUNT ON;INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREALLOCATION, EXPENDITUREDATE, USERID, ACTIVE, LASTMODIFIED, HEADID, QBDEBITCODE, QBCONTROLCODE) VALUES " &_
					 "('" & ExDescription & "', " & Request.Form("txt_Val" & ExpendIDNumber) & ", '" & CurrentDate & "', " & Session("USERID") & ", 1, '" & CurrentDate & "', " & HDID  & ", '" & Request.Form("txt_QBDEBITCODE" & CheckArray(i))  & "', '" & Request.Form("txt_QBCONTROLCODE" & CheckArray(i)) & "') " &_
					 "SELECT SCOPE_IDENTITY() AS EXPID;SET NOCOUNT OFF"
					 Call OpenRs(rsInsert, SQL)
					 ExID = rsInsert("EXPID")
					 CloseRs(rsInsert)
					 response.write SQL & "<BR><BR>"
			End If
	
		End IF	
		
		'If the item is the last in the array then don't put a comma on the end
		ICounter = ICounter + 1
		ExIDList = ExIDList & ExID
		If ExID <> "" then 
		If ICounter <= theArraySize then ExIDList = ExIDList & "," End If
		End IF
			
	next
	SQL = "UPDATE F_EXPENDITURE SET ACTIVE = 0 WHERE HEADID =  " & HDID & " AND EXPENDITUREID NOT IN (" & ExIDList & ")"
    response.write SQL & "<BR>"
	Conn.Execute(SQL)

	
	' 7/june/2004 - Construct SQL statement to AMEND the Development rates
	GrantFrom = "NULL"
	if (Request.Form("sel_FROM") <> "") then GrantFrom = Request.Form("sel_FROM")
	
	SQL = "UPDATE F_DEVELOPMENT_RATES " & _
		  "SET F_DEVELOPMENT_RATES.[GRANT] = '" & Request.Form("txt_GRANT") & "', " &_
		  "F_DEVELOPMENT_RATES.LOAN = " & Request.Form("txt_LOAN") & ", " &_  
		  "F_DEVELOPMENT_RATES.GRANTFROM = " & GrantFrom & ", " &_
		  "F_DEVELOPMENT_RATES.INTERESTRATE = " &  Request.Form("txt_INTERESTRATE") & " " &_
		  " WHERE ((F_DEVELOPMENT_RATES.HEADID= " & HDID & "))"
	
	Conn.Execute(SQL)	

	CloseDB()
		
	Response.Redirect ("FinanceBuilderDev_svr.asp")
%>