<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%

	' Recordset to retrieve data that is relevant to HEAD ID from AmendScheme.asp
	OpenDB()
	SQL = "SELECT * FROM F_HEAD where HEADID = " & Request.Querystring("HDID")
	Call OpenRs(rsHeadDetails, SQL)
	DevelopmentName =  rsHeadDetails("DESCRIPTION")
	DevelopmentAllocation = rsHeadDetails("HEADALLOCATION")
	CloseRs(rsHeadDetails)
	'
		
	OpenDB()
	SQL = "SELECT EX.QBDEBITCODE, EX.QBCONTROLCODE, CODEID, EXPENDITUREID, ISNULL(DESCRIPTION, CODENAME) AS THEITEM, " &_
		  "ISNULL(EXPENDITUREALLOCATION, 0) AS EXPENDITUREALLOCATION " &_
		  "from F_DEVELOPMENTCODES DC " &_
		  "LEFT JOIN F_EXPENDITURE EX ON DC.CODENAME = EX.DESCRIPTION AND EX.HEADID = "  & Request.Querystring("HDID") &_
		  " AND EX.ACTIVE = 1"

	Call OpenRs(rsCodes, SQL)
    TableString = ""
	ItemArray = ""
	while NOT rsCodes.EOF
		' If the expenditure is not assigned to a head dont check the checkbox
		iF NOT rsCodes("EXPENDITUREID") = "NULL" Then CheckStatus = "checked" Else CheckStatus = "" End if	
		ItemArray = ItemArray & """" & rsCodes("THEITEM") & ""","
		TableString = TableString & "<TR valign=middle><TD>" & rsCodes("THEITEM") & "<input type=hidden name=""txt_QBCONTROLCODE" 	& rsCodes("CodeID") & """ value=""" & rsCodes("QBCONTROLCODE") & """>&nbsp;<input type=hidden name=""txt_QBDEBITCODE" & rsCodes("CodeID") & """ value=""" & rsCodes("QBDEBITCODE") & """>&nbsp;<input type=hidden name=""txt_EXP" & rsCodes("CodeID")& """ value=""" & rsCodes("EXPENDITUREID") & """></TD><TD><input style=""text-align:right"" type=text class=""textbox"" name=""txt_Val" & rsCodes("CodeID") & """ value=""" & rsCodes("EXPENDITUREALLOCATION")& """ onblur=""BlurTotal()""><input type=hidden name=""txt_Code" & rsCodes("CodeID") & """ value=""" & rsCodes("THEITEM") & """>&nbsp;<img name=""img_Val" & rsCodes("CodeID") & """ src=""/js/FVS.gif"" width=14 heiight=14></TD><TD><input type=""checkbox"" name=""iChecks"" value=""" & rsCodes("CodeID") & """ " & CheckStatus & " onclick=""BlurTotal()""></TD></TR>"
		rsCodes.moveNext
	wend
	ItemArray = ItemArray & """"""
	CloseRs(rsCodes)

	//this part finds the amount left in the development cost centre
	SQL = "SELECT isnull(SUM(HEADALLOCATION),0) AS TOTALALLOCATED FROM F_HEAD where COSTCENTREID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID') group by COSTCENTREID"
	Call OpenRs(rsAllocated, SQL)
	if NOT rsAllocated.EOF then
		Allocated = rsAllocated("TOTALALLOCATED")
	ELSE
		Allocated = 0
	end if
	CloseRs(rsAllocated)

	SQL = "SELECT isnull(COSTCENTREALLOCATION,0) as ta from F_COSTCENTRE where COSTCENTREID = (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID')"
	Call OpenRs(rsTotal, SQL)
	if NOT rsTotal.EOF then
		Total = rsTotal("ta")
	ELSE
		Total = 0
	end if
	CloseRs(rsTotal)

	TotalLeft = CDbl(Total) - Cdbl(Allocated)


	Call BuildSelect(lstGRANTS, "sel_FROM", "G_GRANT", "GRANTID, GRANTNAME", "GRANTNAME", "Please Select...", NULL, NULL, "textbox200", NULL)	
		
	CloseDB()
%>
<html>
<head>
<title>Amend Scheme</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language=javascript src="/js/formValidation.js"></script>
<script language=javascript>
var ItemArray = new Array(<%=ItemArray%>)

var FormFields = new Array()
FormFields[0] = "txt_SCHEME|Development Name|TEXT|Y"
FormFields[1] = "txt_SCHEMEMANUALTOTAL|Total Development Cost|CURRENCY|Y"
FormFields[2] = "txt_INTERESTRATE|Interest Rate|FLOAT|Y|5"
FormFields[3] = "txt_LOAN|Loan|CURRENCY|Y"
FormFields[4] = "txt_GRANT|Grant|CURRENCY|Y"
FormFields[5] = "sel_FROM|From|SELECT|N"

function AmendScheme(){
	FormFields.length = 6

	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		for (i=0; i<CheckList.length; i++){
			FormFields[i+6] = "txt_Val" + CheckList[i].value + "||TEXT|N"
			}
		}
	checkForm()

	FormFields.length = 6			
	if (CheckList.length){
		JCounter = 6
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked){
				FormFields[JCounter] = "txt_Val" + CheckList[i].value + "|" + ItemArray[i] + "|CURRENCY|Y"
				JCounter ++
				}
			}
		}

	if (!checkForm()) return false	

	if (CheckList.length){
		RunningTotal = 0
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked)
				RunningTotal += parseFloat(document.getElementById("txt_Val" + CheckList[i].value).value)
			}
		}
	document.getElementById("txt_RunningTotal").value = FormatCurrency(RunningTotal)
	
	RunningTotal = parseFloat(FormatCurrency(RunningTotal))
	if (RunningTotal > parseFloat(document.getElementById("txt_FULL").value)){
		alert("The Expenditure totals add up to more than the budget left in the 'Development' Cost Centre.")
		return false;
		}

	if (RunningTotal > parseFloat(document.getElementById("txt_SCHEMEMANUALTOTAL").value)){
		alert("The Expenditure totals add up to more than the 'Total Development Cost'.\nPlease lower the expenditures or increase the total development cost")
		return false;
		}		
	document.getElementById("txt_SchemeTotal").value = FormatCurrency(RunningTotal)
	
	RSLFORM.action = "../ServerSide/AmendScheme.asp"
	RSLFORM.target = "theSideBar"
	RSLFORM.submit()
	window.close()
	}

function Toggle() {
	if (document.getElementById("chkToggle").checked)
		checkStatus = true
	else
		checkStatus = false
			
	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		for (i=0; i<CheckList.length; i++){	
			CheckList[i].checked = checkStatus
			}
		}
	}

function BlurTotal(){
	CheckList = document.getElementsByName("iChecks")
	if (CheckList.length){
		RunningTotal = 0
		for (i=0; i<CheckList.length; i++){
			if (CheckList[i].checked){
				if (!isNaN(document.getElementById("txt_Val" + CheckList[i].value).value)){
					RunningTotal += parseFloat(document.getElementById("txt_Val" + CheckList[i].value).value,10)
					document.getElementById("txt_Val" + CheckList[i].value).value = FormatCurrency(document.getElementById("txt_Val" + CheckList[i].value).value)
					}
				}
			}
		}
	document.getElementById("txt_RunningTotal").value =FormatCurrency(RunningTotal)
	}
</script>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<body bgcolor="#FFFFFF" text="#000000">
<table HEIGHT=100% WIDTH=100% cellspacing=5>
  <tr>
    <td> 
      <TABLE HEIGHT=100% WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>
        <tr> 
          <td valign="top" height="20"> 
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
              <TR> 
                <TD ROWSPAN=2 width="79"><img src="../Images/tab_new_scheme.gif" width="110" height="20"></TD>
                <TD HEIGHT=19></TD>
              </TR>
              <TR> 
                <TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <tr> 
          <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
            <table>
              <FORM name="RSLFORM" method="POST">
                <tr> 
                  <td colspan=2>Enter the development name and un-check any expenditures 
                    you do not wish to be created for the new scheme. Finally 
                    enter a budget value for each expedniture item. 
                <tr>
                  <td colspan=2></td>
                </tr>
                <tr> 
                  <td colspan=2><b>New Development Name:</b> 
                    <input type="text" value="<%=DevelopmentName%>" name="txt_SCHEME" class="textbox200" style="width:180">
                    <img name="img_SCHEME" src="/js/FVS.gif" width=14 height=14></td>
                </tr>

                <tr> 
                  <td colspan=2><b>Total Development Cost:</b>&nbsp; 
                    <input type="text" class="textbox" value="<%=DevelopmentAllocation%>" name="txt_SCHEMEMANUALTOTAL" style="width:180">
                    <img name="img_SCHEMEMANUALTOTAL" src="/js/FVS.gif" width=14 height=14> 
                  </td>
                </tr>
                <tr> 
                  <td><b>Standard Expenditures for Scheme...</b></td>
                  <td nowrap> 
                    <input type="checkbox" name="chkToggle" onclick="Toggle()" checked title="Toggle De/Select All">
                    &nbsp;&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=2 style='border-top:1px solid #133e71;border-bottom:1px solid #133e71'> 
                    <div style='overflow:auto;height:260;width:384' class="TA"> 
                      <table width=100% cellspacing=1 cellpadding=1>
                        <%=TableString%> 
                      </table>
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td><b>Current Expenditure Totals:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="text" class="textbox" name="txt_RunningTotal" disabled value="0.00" style="text-align:right">
                  </td>
                </tr>
                <tr> 
                  <td style='border-bottom:1px solid #133e71' colspan=2><b>Development 
                    Budget:</b> 
                    <input type=hidden name="txt_FULL" value="<%=FormatNumber(TotalLeft,2,-1,0,0)%>">
                    <input type=hidden name="txt_SchemeTotal" value="">
					<input type=hidden name="HDID" value="<%=Request("HDID")%>">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="text" class="textbox" name="txt_Full" disabled value="<%=FormatCurrency(TotalLeft)%>" style="text-align:right">
                    <!--#include virtual="Includes/Bottoms/BlankBottom.html" -->
                  </td>
                </tr>
                <tr> 
                  <td colspan=2><b>Interest Rate %:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="text" class="textbox" name="txt_INTERESTRATE" value="0.00" style="text-align:right">
                    <img name="img_INTERESTRATE" src="/js/FVS.gif" width="14" height="14"> 
                  </td>
                </tr>
                <tr> 
                  <td><b>Loan:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="text" class="textbox" name="txt_LOAN" value="0.00" style="text-align:right">
                    <img name="img_LOAN" src="/js/FVS.gif" width="14" height="14">	
                  </td>
                </tr>
                <tr> 
                  <td><b>Grant �:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <input type="text" class="textbox" name="txt_GRANT" value="0.00" style="text-align:right">
                    <img name="img_GRANT" src="/js/FVS.gif" width="14" height="14"> 
                    <img name="img_FROM" src="/js/FVS.gif" width="14" height="14">	
                  </td>
                </tr>
                <tr> 
                  <td colspan=2><b>From:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <%=lstGRANTS%> 
                    <input type="button" onclick="AmendScheme()" value="AMEND" class="RSLButton">
                  </td>
                </tr>
              </FORM>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
