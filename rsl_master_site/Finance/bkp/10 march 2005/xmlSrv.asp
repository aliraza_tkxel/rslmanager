<!-- #include virtual="Connections/db_connection.asp" -->
<%
Response.ContentType = "text/xml"
dim dbrs,dbcmd
dim sql
dim id
dim gff
set dbrs=server.createobject("ADODB.Recordset")
set dbcmd= server.createobject("ADODB.Command")
Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")


if request("task")="lst" then
	
	
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_SCHEDULEDETAILS"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	dbcmd.Parameters(2) = 1
	set dbrs=dbcmd.execute
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
	
elseif request("task")="sd" then

	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_SCHEDULEDETAILS"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	dbcmd.Parameters(2) = 2
	set dbrs=dbcmd.execute
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="DVB" then
	dim bal
	bal=request("nop")
	if bal="" then
		bal=0
	end if
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_ACC_OPENBALANCE"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	dbcmd.Parameters(2) = bal
	dbcmd.Parameters(3) = request("HDID")
	set dbrs=dbcmd.execute
	
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="BLT" then
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "NL_GET_BALANCE"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="PTY" then
	gff="pty"
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "NL_GET_HEADNEXPENDITURE"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	if request("subtask")="HEAD" then
		dbcmd.Parameters(2) = 1
	else 
		dbcmd.Parameters(2) = 2
	end if
	dbcmd.Parameters(3) = Session("USERID")
	set dbrs=dbcmd.execute
	
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="PTYSAVE" then
	dim rs,cmd,kl
	set rs=server.CreateObject("ADODB.Recordset")
	set rs1=server.CreateObject("ADODB.Recordset")
	set cmd=server.CreateObject("ADODB.Command")
	set cmd1=server.CreateObject("ADODB.Command")
	set cmd2=server.CreateObject("ADODB.Command")
	set con=server.CreateObject("ADODB.Connection")
	Set xmlDocs = Server.CreateObject("Microsoft.XMLDOM")
	rs.Fields.append "ID",3
	rs.Fields.append "bank",3
	rs.Fields.append "Vouchers",12
	rs.Fields.append "vdate",7
	rs.Fields.append "item",12
	rs.Fields.append "cc",3
	rs.Fields.append "netcost",6
	rs.Fields.append "notes",12
	rs.Fields.append "heads",3
	rs.Fields.append "Expenditure",3
	rs.Fields.append "vattype",3
	rs.Fields.append "vat",6
	rs.Fields.append "total",6
	rs.Fields.append "vatInc",3
	rs.Fields.append "LId",12
	rs.Fields.append "Deleted",3
	rs.Open ,,2
	
	xmldocs.async=false
	xmldocs.load(Request)
	
	
	dim d,k, gtotal, netcoset,vat,GrosCost,vDate
	dim i,j,t,v
	dim TXID,TRANSCATIONID,ORDERITEMID,nodlst
	d =xmldocs.documentElement.childNodes.length

	gtotal=0
	dim dfd
	for i=0 to d-1
			
			'set t=nodlst.item(i)
			
			rs.AddNew
			
			'v=xmldocs.documentElement.childNodes.item(i).selectSingleNode("id").text
			rs("ID").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("id").text
			rs("bank").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("bank").text
			rs("Vouchers").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("voucher").text
			rs("vdate").Value = trim(xmldocs.documentElement.childNodes.item(i).selectSingleNode("vdate").text)
			rs("item").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("item").text
			rs("cc").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("cc").text
			rs("netcost").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("netcost").text
			netcoset=formatnumber (xmldocs.documentElement.childNodes.item(i).selectSingleNode("netcost").text)
			rs("notes").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("notes").text
			rs("heads").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("heads").text
			rs("Expenditure").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("expenditure").text
			'response.write(t.selectSingleNode("expenditure").text & "<BR>")
			rs("vattype").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("vattype").text
			rs("vat").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("vat").text
			vat=formatnumber (xmldocs.documentElement.childNodes.item(i).selectSingleNode("vat").text)
			rs("total").Value = xmldocs.documentElement.childNodes.item(i).selectSingleNode("total").text
			rs("vatInc").Value = cint(xmldocs.documentElement.childNodes.item(i).selectSingleNode("vatinc").text)
			rs("LId").Value = 0
			rs("Deleted").Value = cint(xmldocs.documentElement.childNodes.item(i).selectSingleNode("Deleted").text)		
			rs.Update
			if cint(xmldocs.documentElement.childNodes.item(i).selectSingleNode("Deleted").text) =0 then
				gtotal = gtotal + formatnumber(netcoset,2) + formatnumber(vat,2)
			end if
			'set t=xmldocs.documentElement.selectSingleNode("//records").nextSibling
	next
	'rs.save "c:\xmldump.xml",1
	rs.MoveFirst()
	
	con.ConnectionString=RSL_CONNECTION_STRING
	con.Open
	'con.BeginTrans
	cmd.CommandType = 4
	set cmd.ActiveConnection=con
	
	'*************************************************
	' F_CREATE_PETTYCASH_HDR
	'*************************************************
	cmd.CommandText = "F_CREATE_PETTYCASH_HDR" 
	
	cmd.Parameters.Refresh
	cmd.Parameters(1)=  formatdatetime(rs("vdate"),2)
	vDate=rs("vdate")
	cmd.Parameters(2)=   rs("Vouchers")
	cmd.Parameters(3)= null
	cmd.Parameters(4)= null
	cmd.Parameters(5)= gtotal
	cmd.Parameters(6)= rs("Vouchers")
	cmd.Parameters(7)= null
	set rs1=cmd.execute
	TXID=rs1("TXID")
	TRANSCATIONID=rs1("TRANSCATIONID")
	rs1.Close()
	set cmd=nothing
	
	'*************************************************
	' F_INSERT_PURCHASEITEM
	'*************************************************
	
	rs.MoveFirst()
	cmd1.CommandText = "F_INSERT_PURCHASEITEM" 
	cmd1.CommandType = 4
	set cmd1.ActiveConnection=con

	
	cmd2.CommandText = "F_CREATE_PETTYCASH_DTL" 
	cmd2.CommandType = 4
	set cmd2.ActiveConnection=con
	while not rs.EOF
		if rs("Deleted")=0 then
			cmd1.Parameters.Refresh
			cmd2.Parameters.Refresh
			
			cmd1.Parameters(1)=   null
			cmd1.Parameters(2)=   formatnumber(rs("Expenditure"))
			cmd1.Parameters(3)= rs("item")
			cmd1.Parameters(4)= rs("notes")
			cmd1.Parameters(5)= formatdatetime(rs("vdate"),2)
			cmd1.Parameters(6)= null
			cmd1.Parameters(7)= null
			cmd1.Parameters(8)= formatnumber(rs("netcost"),2)
			cmd1.Parameters(9)= formatnumber(rs("vattype").Value )
			cmd1.Parameters(10)= formatnumber(rs("vat").Value,2)
			GrosCost=cmd1.Parameters(8)+ cmd1.Parameters(10)
			cmd1.Parameters(11)= GrosCost 
			cmd1.Parameters(12)= formatnumber(Session("USERID"))
			cmd1.Parameters(13)=1
			cmd1.Parameters(14)=3
			cmd1.Parameters(15)= 10
			cmd1.Parameters(16)= null
			cmd1.Parameters(17)=0
			cmd1.Parameters(18)= 0
			cmd1.Parameters(19)= null
			cmd1.Parameters(20)= formatnumber(rs("vatInc"))
			set rs1=cmd1.Execute
			ORDERITEMID=rs1("OREDERITEMID")
			kl=kl & "," & ORDERITEMID
		
			
			cmd2.Parameters(1)= cmd1.Parameters(5)
			cmd2.Parameters(2)= formatnumber(TXID)
			cmd2.Parameters(3)= formatnumber(rs("Expenditure"))
			cmd2.Parameters(4)= null
			cmd2.Parameters(5)= GrosCost
			cmd2.Parameters(6)= rs("Vouchers") & ": " & rs("item")
			cmd2.Parameters(7)= rs("notes")
			cmd2.Parameters(8)= TRANSCATIONID
			cmd2.Parameters(9)= ORDERITEMID
			cmd2.Execute
		end if
		rs.MoveNext()
	
	wend	 
	
	'con.RollbackTrans()
	'response.write "<T>" & v & "</T>"
'	'response.write  xmldocs.xml 
'	

	
	
	set xmlDocs=nothing
	set rs=nothing
	set cmd1=nothing
	set cmd2=nothing
	con.Close
	set con=nothing
elseif request("task")="PTYGETDATA" then
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_PETTYCASH_DTL"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="PTYUPDATE" then
	dim rrs,ccmd,ccmd1,txnids
	set rrs=server.CreateObject("ADODB.Recordset")
	txnids=request("id")
	set ccmd=server.CreateObject("ADODB.Command")
	set ccmd1=server.CreateObject("ADODB.Command")


	set con=server.CreateObject("ADODB.Connection")
	Set xmlDocs = Server.CreateObject("Microsoft.XMLDOM")
	rrs.Fields.append "ID",3
	rrs.Fields.append "bank",3
	rrs.Fields.append "Vouchers",12
	rrs.Fields.append "vdate",7
	rrs.Fields.append "item",12
	rrs.Fields.append "cc",3
	rrs.Fields.append "netcost",6
	rrs.Fields.append "notes",12
	rrs.Fields.append "heads",3
	rrs.Fields.append "Expenditure",3
	rrs.Fields.append "vattype",3
	rrs.Fields.append "vat",6
	rrs.Fields.append "total",6
	rrs.Fields.append "vatInc",3
	rrs.Fields.append "OrderItemId",3
	rrs.Fields.append "LId",12
	rrs.Fields.append "Deleted",3
	
	rrs.Open ,,2
	xmldocs.async=false
	xmldocs.load(Request)
	dim d1,k1, gtotal1, netcoset1,vat1,GrosCost1,vDate1
	dim i1,j1,t1,vv
	dim strSQL
	dim TXID1,TRANSCATIONID1,ORDERITEMID1
	d1 =xmldocs.documentElement.childNodes.length
	'set t1=xmldocs.documentElement.selectSingleNode("//records")
	gtotal1=0
	dim dfd1
	for i1=0 to d1-1
	
			rrs.AddNew
			rrs("ID").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("id").text
			rrs("bank").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("bank").text
			rrs("Vouchers").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("voucher").text
			rrs("vdate").Value = trim(xmldocs.documentElement.childNodes.item(i1).selectSingleNode("vdate").text)
			rrs("item").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("item").text
			rrs("cc").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("cc").text
			rrs("netcost").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("netcost").text
			netcoset1=formatnumber (xmldocs.documentElement.childNodes.item(i1).selectSingleNode("netcost").text)
			rrs("notes").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("notes").text
			rrs("heads").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("heads").text
			rrs("Expenditure").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("expenditure").text
			rrs("vattype").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("vattype").text
			rrs("vat").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("vat").text
			vat1=formatnumber (xmldocs.documentElement.childNodes.item(i1).selectSingleNode("vat").text)
			rrs("total").Value = xmldocs.documentElement.childNodes.item(i1).selectSingleNode("total").text
			rrs("vatInc").Value = cint(xmldocs.documentElement.childNodes.item(i1).selectSingleNode("vatinc").text)
			rrs("OrderItemId").Value = cint(xmldocs.documentElement.childNodes.item(i1).selectSingleNode("OrderItemid").text)

			vv=xmldocs.documentElement.childNodes.item(i1).selectSingleNode("lineid").text
			rrs("LId").Value = vv
			rrs("Deleted").Value = cint(xmldocs.documentElement.childNodes.item(i1).selectSingleNode("Deleted").text)		
			'rrs("LineId").Value =1
			
			rrs.Update
			if cint(xmldocs.documentElement.childNodes.item(i1).selectSingleNode("Deleted").text) =0 then
				gtotal1 = gtotal1 + formatnumber(netcoset1,2) + formatnumber(vat1,2)
			end if		
		
			'set t1=xmldocs.documentElement.selectSingleNode("//records").nextSibling
	next
'	rrs.save "c:\xmldump.xml",1
	con.ConnectionString=RSL_CONNECTION_STRING
	con.Open
	'con.BeginTrans
	
	set ccmd.ActiveConnection=con
	rrs.MoveFirst()
	
	
	ccmd.CommandText = "F_UPDATE_PURCHASEITEM" 
	ccmd.CommandType = 4
	
	
	ccmd1.CommandText = "F_UPDATE_NL_JOURNALENTRYDEBITLINE" 
	ccmd1.CommandType = 4
	set ccmd1.ActiveConnection=con

	while not rrs.EOF
		ccmd.Parameters.Refresh
		ccmd1.Parameters.Refresh
		
		ccmd.Parameters(1)=   formatnumber(rrs("Expenditure"))
		ccmd.Parameters(2)=   rrs("item")
		ccmd.Parameters(3)=   rrs("notes")
		ccmd.Parameters(4)=   formatnumber(rrs("netcost"),2)
		ccmd.Parameters(5)=   formatnumber(rrs("netcost"),2)
		ccmd.Parameters(6)=   formatnumber(Session("USERID"))
		ccmd.Parameters(7)=   formatnumber(rrs("vatInc"))
		ccmd.Parameters(8)=   formatnumber(rrs("OrderItemId"))
		ccmd.execute
	
		ccmd1.Parameters(1)=   formatnumber(txnids)
		ccmd1.Parameters(2)=   formatnumber(rrs("Expenditure"))
		ccmd1.Parameters(3)=   formatnumber(rrs("netcost"),2)
		ccmd1.Parameters(4)=   rrs("item")
		ccmd1.Parameters(5)=   rrs("notes")
		ccmd1.Parameters(6)=   rrs("LId")
		
		ccmd1.execute
		if rrs("Deleted")=1 then
			strSQL="delete from NL_JOURNALENTRYDEBITLINE where lineid=" & rrs("LId") & " and txnid=" & txnids
			con.execute strSQL

			strSQL="delete from NL_JOURNAL2PURCHASEITEM where lineid=" & rrs("LId") & " and TRANSACTIONID=" & txnids
			con.execute strSQL
			
			
			strSQL="update F_PURCHASEITEM set ACTIVE=0 where ORDERITEMID=" &  rrs("OrderItemId")
			con.execute strSQL
		end if
       rrs.movenext()
	
	
	
	wend
	
	strSQL="UPDATE NL_JOURNALENTRYCREDITLINE SET TIMEMODIFIED=GETDATE(), AMOUNT=" & gtotal1 & " WHERE TXNID=" &  txnids
	con.execute strSQL
	strSQL="UPDATE NL_JOURNALENTRY SET TIMEMODIFIED=GETDATE() WHERE TXNID=" &  txnids
	con.execute strSQL

	'con.rollbacktrans()
	con.close
	set con=nothing
	set ccmd=nothing
	set ccmd1=nothing
	
	response.write "<t>" & gtotal1 & "</t>"
elseif request("task")="BUDGETTRANSINSERT" then
	
	dim xmlDocs,x
	Set xmlDocs = Server.CreateObject("Microsoft.XMLDOM")
	xmldocs.async=false
	xmldocs.load(Request)
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	
	dbcmd.CommandText = "F_INSERT_BUDGETTRANSFER"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = formatdatetime(xmldocs.documentElement.selectSingleNode("//records/BDDate").text,2)
	dbcmd.Parameters(2) = formatnumber(xmldocs.documentElement.selectSingleNode("//records/fromcc").text)
	dbcmd.Parameters(3) = formatnumber(xmldocs.documentElement.selectSingleNode("//records/amount").text)
	dbcmd.Parameters(4) = formatnumber(xmldocs.documentElement.selectSingleNode("//records/tocc").text)
	dbcmd.Parameters(5) = xmldocs.documentElement.selectSingleNode("//records/notes").text
	dbcmd.Parameters(6) = formatnumber(Session("USERID"))
	dbcmd.execute
	set xmlDocs=nothing
	
elseif request("task")="BUDGETUPDATE" then 
	dim xmlDocs1
	dim cmds
	set cmds=server.createobject("ADODB.Command")
	Set xmlDocs1 = Server.CreateObject("Microsoft.XMLDOM")
	xmlDocs1.async=false
	xmlDocs1.load(Request)
	cmds.ActiveConnection=RSL_CONNECTION_STRING
	cmds.CommandType = 4
	cmds.CommandText = "F_UPDATE_BUDGETRTRANSFER"
	cmds.Parameters.Refresh
	cmds.Parameters(1) = formatnumber(xmlDocs1.documentElement.selectSingleNode("//records/cctranid").text)
	cmds.Parameters(2) = formatnumber(xmlDocs1.documentElement.selectSingleNode("//records/fromcc").text)
	cmds.Parameters(3) = formatnumber(xmlDocs1.documentElement.selectSingleNode("//records/amount").text)
	cmds.Parameters(4) = formatnumber(xmlDocs1.documentElement.selectSingleNode("//records/tocc").text)
	cmds.Parameters(5) = xmlDocs1.documentElement.selectSingleNode("//records/notes").text
	cmds.Parameters(6) = formatdatetime(xmlDocs1.documentElement.selectSingleNode("//records/BDDate").text,2)
	cmds.Parameters(7) = formatnumber(Session("USERID"))
	cmds.execute
	
	set xmlDocs1=nothing
	set cmds=Nothing
	response.write "<t>1</t>"
	
elseif request("task")="MISSAVE" then  
	

	dim rset1,c_cmd1,c_cmd2,c_con1,xml_Docs,d_2,Gross_Amt,rset_rtn,TXN_ID1,HDR_ID1,BSlip,Net_cost
	set rset1=server.CreateObject("ADODB.Recordset")
	set rset_rtn=server.CreateObject("ADODB.Recordset")
	set c_cmd1=server.CreateObject("ADODB.Command")
	set c_cmd2=server.CreateObject("ADODB.Command")
	set c_con1=server.CreateObject("ADODB.Connection")
	Set xml_Docs = Server.CreateObject("Microsoft.XMLDOM")
	xml_Docs.async=false
	xml_Docs.load(Request)
	
	rset1.Fields.append "Bank",3
	rset1.Fields.append "Date",7
	rset1.Fields.append "PMethod",3
	rset1.Fields.append "BSlip",12
	rset1.Fields.append "From",12
	rset1.Fields.append "NLAcc",3
	rset1.Fields.append "Net",6
	rset1.Fields.append "Detail",12
	rset1.Fields.append "Deleted",3
	rset1.Fields.append "development",3
	rset1.Open ,,2
	d_2 =xml_Docs.documentElement.childNodes.length
	for i2=0 to d_2-1
			
			rset1.AddNew
			rset1("Bank").Value = cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("Bank").text)
			rset1("Date").Value =formatdatetime( xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("Date").text,2)
			rset1("PMethod").Value = cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("PMethod").text)
			BSlip=trim(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("BSlip").text)
			if len(BSlip) =0 then
				rset1("BSlip").Value = ""
			else
				rset1("BSlip").Value = trim(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("BSlip").text)
			end if
			rset1("From").Value = trim(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("From").text)
			rset1("NLAcc").Value = cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("NLAcc").text)
			rset1("Net").Value =formatnumber(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("Net").text)
			
			if cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("deleted").text) <> 1 then
				Net_cost=CDbl(replace(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("Net").text,",",""))
				Gross_Amt=Gross_Amt + Net_cost
			end if
			
			rset1("Detail").Value = trim(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("Detail").text)
			rset1("Deleted").Value=cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("deleted").text)
			rset1("development").Value=cint(xml_Docs.documentElement.childNodes.item(i2).selectSingleNode("development").text)
			rset1.update
	next
	rset1.MoveFirst()
'	rset1.save "c:\xmldump.xml",1
	c_con1.ConnectionString=RSL_CONNECTION_STRING
	c_con1.Open


	c_cmd1.CommandType = 4
	set c_cmd1.ActiveConnection=c_con1
	
	
	c_cmd1.CommandText = "NL_MISCELLANEOUS_INCOME_HDR" 
'	c_con1.BeginTrans
	c_cmd1.Parameters.Refresh
	c_cmd1.Parameters(1)=  rset1("PMethod")
	c_cmd1.Parameters(2)=  rset1("From")
	c_cmd1.Parameters(3)=  rset1("BSlip")
	c_cmd1.Parameters(4)= rset1("Bank")
	c_cmd1.Parameters(5)= formatnumber(Gross_Amt)
	c_cmd1.Parameters(6)= formatdatetime(rset1("Date"))
	c_cmd1.Parameters(7)=  cint(rset1("development"))
	set rset_rtn=c_cmd1.execute
	HDR_ID1=rset_rtn("HDRID")
	TXN_ID1=rset_rtn("TXNID")
'	c_con1.rollbacktrans()
	
	set c_cmd2.ActiveConnection=c_con1
	c_cmd2.CommandType = 4
	c_cmd2.CommandText = "NL_MISCELLANEOUS_INCOME_DTL" 
	rset1.MoveFirst()
	while (not rset1.eof)
		if 	rset1("Deleted")=0 then
			c_cmd2.Parameters.Refresh
			c_cmd2.Parameters(1)=   HDR_ID1
			c_cmd2.Parameters(2)=   cint(rset1("NLAcc"))
			c_cmd2.Parameters(3)=   formatnumber(rset1("Net"))
			c_cmd2.Parameters(4)=   rset1("Detail").VALUE
			c_cmd2.Parameters(5)=   TXN_ID1
			c_cmd2.Parameters(6)=   formatdatetime(rset1("Date"))
			c_cmd2.execute
		end if
		rset1.movenext
	wend

	str="00000000" & HDR_ID1
	RsStr="MIS" & Right(str,8)




	'c_con1.rollbacktrans()

	set rset1=nothing
	set c_cmd1=nothing
	set r_set2=nothing
	set c_cmd2=nothing
	Set xml_Docs =nothing


'
	response.write "<output>" & RsStr & "</output>"
	

elseif request("task")="PREPAYGETDATA" then 
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_PREPAYMENTDATA"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="PREPAYGETDATAPO" then 
	dim RecCount
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GETPODETAILSFORPREPAY"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	RecCount=dbrs.RecordCount 
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="ACCRLGETDATA" then 
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GET_ACCRUALDATA"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
elseif request("task")="ACCRLGETDATAPO" then 
	dim Rec_Count
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	dbcmd.CommandType = 4
	dbcmd.CommandText = "F_GETPODETAILSFORACCRUALS"
	dbcmd.Parameters.Refresh
	dbcmd.Parameters(1) = request("id")
	set dbrs=dbcmd.execute
	Rec_Count=dbrs.RecordCount 
	dbrs.save xmlDoc, 1
	response.write xmlDoc.xml
end if
	
set dbrs=nothing
set dbcmd= nothing
Set xmlDoc = nothing


%>



