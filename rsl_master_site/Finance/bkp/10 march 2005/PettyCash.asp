<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="../accounts/NominalLedger/INCLUDES/functions.asp" -->
<%
	OpenDB()
	
	' GET PATTY CASH BALANCE
	Dim balance, ptID, PS, PE, VoucherNum
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME LIKE '%PETTYCASH%'"
	Call openRs(rsSet, SQL)	
	if not rsSet.EOF Then ptID = rsSet(0) else ptID = -1 End If
	CloseRs(rsSet)
	get_year(6)	
	SQL = "SELECT 	(ISNULL(DBNOCHILD.DEBIT,0) + ISNULL(DBCHILD.DEBIT,0)) - (ISNULL(CRNOCHILD.CREDIT,0) + ISNULL(CRCHILD.CREDIT,0)) AS BALANCE " &_
		 "			FROM 	NL_ACCOUNT TOPLEVEL " &_
		 "			LEFT JOIN NL_ACCOUNTTYPE AT ON AT.ACCOUNTTYPEID = TOPLEVEL.ACCOUNTTYPE " &_
		 "			LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, ACCOUNTID FROM NL_JOURNALENTRYDEBITLINE WHERE TXNDATE >= '" & PS & " ' AND TXNDATE <= '" & PE & "' GROUP BY ACCOUNTID) DBNOCHILD " &_
		 "			  ON DBNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "			LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, ACCOUNTID FROM NL_JOURNALENTRYCREDITLINE WHERE TXNDATE >= '" & PS & " ' AND TXNDATE <= '" & PE & "' GROUP BY ACCOUNTID) CRNOCHILD " &_
		 "	 		  ON CRNOCHILD.ACCOUNTID = TOPLEVEL.ACCOUNTID " &_
		 "			LEFT JOIN (SELECT SUM(AMOUNT) AS DEBIT, PARENTREFLISTID FROM NL_JOURNALENTRYDEBITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE X.TXNDATE >= '" & PS & " ' AND X.TXNDATE <= '" & PE & "' GROUP BY PARENTREFLISTID) DBCHILD " &_
	  	 "			  ON DBCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "			LEFT JOIN (SELECT SUM(AMOUNT) AS CREDIT, PARENTREFLISTID FROM NL_JOURNALENTRYCREDITLINE X INNER JOIN NL_ACCOUNT A ON A.ACCOUNTID = X.ACCOUNTID WHERE X.TXNDATE >= '" & PS & " ' AND X.TXNDATE <= '" & PE & "' GROUP BY PARENTREFLISTID) CRCHILD " &_
		 "			  ON CRCHILD.PARENTREFLISTID = TOPLEVEL.ACCOUNTID " &_
		 "WHERE		TOPLEVEL.ACCOUNTID = " & ptID
	Call openRs(rsSet, SQL)	
	if not rsSet.EOF Then balance = rsSet(0) else balance = 0 End If
	CloseRs(rsSet)

Dim lstApprovedBy
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
		"WHERE CC.COSTCENTREID <> 11 AND (CC.ACTIVE = 1) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1)" &_
		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND "

Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_Costcentre()' tabindex=4 datasrc=""#ptyAdd"" datafld=""cc"" ")
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, 0, NULL, "textbox200", "  STYLE='WIDTH:70;visibility:hidden' tabindex=5 datasrc=""#ptyAdd"" datafld=""vattype"" ")

SQL="SET NOCOUNT ON;SELECT 'VO ' + right('000000000' + cast(DEFAULTVALUE as varchar),7) as DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME='PETTYCASHVOUCHER';UPDATE RSL_DEFAULTS SET DEFAULTVALUE=DEFAULTVALUE+1 WHERE  DEFAULTNAME='PETTYCASHVOUCHER'"
Call openRs(rsSet, SQL)
IF  NOT rsSet.EOF THEN
	VoucherNum=rsSet("DEFAULTVALUE")
END IF
CloseDB()

TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
%>
<html>
<head>
<title>RSL Manager Finance - Create Purchase Order</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<meta http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />


</head>
<script LANGUAGE="JavaScript" SRC="/js/preloader.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/general.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/financial.js"></script>
<script LANGUAGE="JAVASCRIPT">
	var FormFields = new Array()
	var ind=1
	var RowNumber
	var running_balance = <%=balance%>
	var txid
	var AmmedClick=0
	function SetChecking(Type)
	{
		FormFields.length = 0
		if (Type == 1) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
			FormFields[1] = "sel_HEAD|Head|SELECT|Y"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_NETCOST|Net Cost|CURRENCY|Y"
			FormFields[6] = "txt_vNum|Voucher Number|TEXT|Y"
			}
		else if (Type == 2){
			FormFields[0] = "txt_PONAME|Name|TEXT|Y"
			FormFields[1] = "sel_SUPPLIER|Supplier|SELECT|Y"
			FormFields[2] = "txt_PODATE|Order Date|DATE|Y"
			FormFields[3] = "txt_DELDATE|Expected Date|DATE|N"
			FormFields[4] = "txt_PONOTES|Purchase Notes|TEXT|N"
			}
		else if (Type == 3) {
			FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|N"
			FormFields[1] = "sel_HEAD|Head|SELECT|N"
			FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|N"
			FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|N"
			FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
			FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|N"
			FormFields[6] = "txt_VAT|VAT|CURRENCY|N"
			FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|N"
			}
		}
			
	function PopulateListBox(values, thetext, which){
		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");
		if (which == 2)
			var selectlist = document.forms.THISFORM.sel_HEAD;
		else 
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE;	
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");

			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
	
	
	
	
	function SetPurchaseLimits(){
		eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
		eEMLIMITS = eEMLIMITS.split(";;");
	
		//document.getElementById("txt_EXPBALANCE").value = FormatCurrency(eAmounts[eIndex]);
		//document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	
		}	
	
	var RowCounter = 0
	
	function AddRow(insertPos)
		{
			
			//alert(THISFORM.AmendButton.style.display)
			SetChecking(1)
			if (!checkForm()) return false
			if( document.getElementById("txt_ITEMREF").value=="") 
			{
				alert("Please enter new item details")
				return false
			}
			var ctot
			
			ctot=parseFloat(document.getElementById("hiGC").value) + parseFloat(document.getElementById("txt_NETCOST").value)
		
		
		
		if ( parseFloat(ctot) > parseFloat(document.getElementById("bal").innerHTML.replace(',','') ))
			{
				alert ("Total cost can not be more than Remining")
				return false;
			}
			running_balance -= parseFloat(document.getElementById("txt_GROSSCOST").value)
			bal.innerHTML = FormatCurrencyComma(running_balance)

			SaveStatus = 1
			
			GrossCost = document.getElementById("txt_GROSSCOST").value
			
			ItemRef = document.getElementById("txt_ITEMREF").value			
			
			ItemDesc = document.getElementById("txt_vNum").value			
			ExpenditureID = document.getElementById("sel_EXPENDITURE").value
			
			ExpenditureName = document.getElementById("sel_EXPENDITURE").options(document.getElementById("sel_EXPENDITURE").selectedIndex).text					
			NetCost = document.getElementById("txt_NETCOST").value	
					
			VAT = document.getElementById("txt_VAT").value											
			VatTypeID = document.getElementById("sel_VATTYPE").value			
			
			VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text	
						
			VatTypeCode = VatTypeName.substring(0,1).toUpperCase()
			
			
			
			RowCounter++
			oTable = document.getElementById("ItemTable")
			for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "EMPTYLINE") {
				oTable.deleteRow(i)
				break;
				}
			}

			
			oRow = oTable.insertRow(insertPos)
			oRow.id = "TR" + RowCounter
			oRow.onclick = AmendRow
			oRow.style.cursor = "hand"
			DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + ExpenditureID + "||<>||" + SaveStatus + "\">"
			AddCell (oRow, ItemRef + DATA , ItemDesc, "", "")
			AddCell (oRow, ExpenditureName, "", "", "")		
			//AddCell (oRow, ItemDesc, "", "", "")	
			
			/*AddCell (oRow, FormatCurrencyComma(NetCost), "", "right", "")
			AddCell (oRow, FormatCurrencyComma(VAT), "", "right", "")*/
			AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "")
			DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
			AddCell (oRow, DelImage, "", "center", "#FFFFFF")
			
			ind = ind+1
			
			var insRecord = ptyAdd.XMLDocument.documentElement.setAttribute("ids",document.getElementById("hdnId").value)
			insRecord = ptyAdd.XMLDocument.documentElement.cloneNode(true);
			pttyCash.XMLDocument.documentElement.appendChild(insRecord);
			last();
		
			
			document.getElementById("hdnId").value=ind
			document.getElementById("txt_Date").readOnly =true
			document.getElementById("txt_vNum").readOnly =true
			
			
			var d =pttyCash.XMLDocument.documentElement.childNodes.length
			//var t=pttyCash.XMLDocument.documentElement.selectSingleNode("//records")
			var nCost=0;
			for (i=0; i< d;i++)
			{
				if (parseInt(pttyCash.XMLDocument.documentElement.childNodes.item(i).selectSingleNode("Deleted").text)==0)
				{
					nCost=nCost+  parseFloat(pttyCash.XMLDocument.documentElement.childNodes.item(i).selectSingleNode("netcost").text)
				//t=pttyCash.XMLDocument.documentElement.selectSingleNode("//records").nextSibling
				}
			}
			
			SetTotals (nCost, 0, nCost)
			ResetData()
		}
	
	
	function last()
	{
         ptyAdd.recordset.moveLast();
         currentRecord = ptyAdd.recordset.recordcount - 1;
	}
	function ResetData(){
		SetChecking(3)
		checkForm()
		//document.getElementById("txt_EMLIMITS").value = "0.00";						
		//document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Please Select a Cost Centre...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		ResetArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_COSTCENTRE")
		for (i=0; i<ResetArray.length; i++)
			document.getElementById(ResetArray[i]).value = ""
		document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		document.getElementById("txt_VAT").value = "0.00";	
		document.getElementById("AmendButton").style.display = "none"		
		document.getElementById("AddButton").style.display = "block"
		//document.getElementById("txt_vNum").value="";
		THISFORM.invvat.checked=false
		}
		
	function AmendRow(){
		event.cancelBubble = true
		Ref = this.id
		RowNumber = Ref.substring(2,Ref.length)
		StoredData = document.getElementById("iDATA" + RowNumber).value
		StoredArray = StoredData.split("||<>||")
		var itName="//records[@ids=" + RowNumber + "]/item"
		var VNumber="//records[@ids=" + RowNumber + "]/voucher"
		var pdate="//records[@ids=" + RowNumber + "]/vdate"
		var cc="//records[@ids=" + RowNumber + "]/cc"
		var nc="//records[@ids=" + RowNumber + "]/netcost"
		var notes="//records[@ids=" + RowNumber + "]/notes"
		var head="//records[@ids=" + RowNumber + "]/heads"
		var vt="//records[@ids=" + RowNumber + "]/vattype"
		var exp="//records[@ids=" + RowNumber + "]/expenditure"
		var vat="//records[@ids=" + RowNumber + "]/vat"
		var tot="//records[@ids=" + RowNumber + "]/total"
		var vatInc="//records[@ids=" + RowNumber + "]/vatinc"
		var OrderItemId="//records[@ids=" + RowNumber + "]/OrderItemid"
		var lineid="//records[@ids=" + RowNumber + "]/lineid"
		
		document.getElementById("txt_ITEMREF").value =pttyCash.XMLDocument.documentElement.selectSingleNode(itName).text 
		document.getElementById("txt_vNum").value =pttyCash.XMLDocument.documentElement.selectSingleNode(VNumber).text
		document.getElementById("txt_Date").value =pttyCash.XMLDocument.documentElement.selectSingleNode(pdate).text  
		document.getElementById("sel_COSTCENTRE").value =pttyCash.XMLDocument.documentElement.selectSingleNode(cc).text
		document.getElementById("txt_NETCOST").value =pttyCash.XMLDocument.documentElement.selectSingleNode(nc).text
		Select_Costcentre()
		document.getElementById("sel_HEAD").value =pttyCash.XMLDocument.documentElement.selectSingleNode(head).text
		Select_OnchangeHead()
		document.getElementById("sel_EXPENDITURE").value =pttyCash.XMLDocument.documentElement.selectSingleNode(exp).text                                
		document.getElementById("txt_ITEMDESC").value =pttyCash.XMLDocument.documentElement.selectSingleNode(notes).text
		document.getElementById("sel_VATTYPE").value =pttyCash.XMLDocument.documentElement.selectSingleNode(vt).text                                
		
		document.getElementById("txt_VAT").value =pttyCash.XMLDocument.documentElement.selectSingleNode(vat).text
		document.getElementById("txt_GROSSCOST").value =pttyCash.XMLDocument.documentElement.selectSingleNode(tot).text
		

		document.getElementById("ORDERITEMID").value =pttyCash.XMLDocument.documentElement.selectSingleNode(OrderItemId).text
		document.getElementById("hdnLineId").value =pttyCash.XMLDocument.documentElement.selectSingleNode(lineid).text
		if (parseInt(pttyCash.XMLDocument.documentElement.selectSingleNode(vatInc).text)==0)
		{
			THISFORM.invvat.checked=false
		}
		else
		{
			THISFORM.invvat.checked=true
		}
		document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
		document.getElementById("AddButton").style.display = "none"
		document.getElementById("AmendButton").style.display = "block"
		//alert(ptyAdd.XMLDocument.xml)
		
		}
	
	function UpdateRow()
	{
		var srchStr
		srchStr="//records[@ids=" + RowNumber + "]"
	    var d =pttyCash.XMLDocument.documentElement.selectSingleNode(srchStr)
		pttyCash.XMLDocument.documentElement.removeChild(d)
		
		AmmedClick=1
		sTable = document.getElementById("ItemTable")
		//alert(pttyCash.XMLDocument.xml)
		
		sTable = document.getElementById("ItemTable")
		RowData = document.getElementById("UPDATEID").value
		RowArray = RowData.split("||<>||")
		MatchRow = RowArray[0]
		for (i=0; i<sTable.rows.length; i++){
			if (sTable.rows(i).id == "TR" + MatchRow) {
				sTable.deleteRow(i)
				SetTotals (-RowArray[1], -RowArray[2], -RowArray[3])				
				break;
				}
			}
		AddRow(i)		
		AmmedClick=0
		//AddRow(RowNumber);
		
	}
		
	function SetTotals(iNE, iVA, iGC) {
		totalNetCost = parseFloat(iNE)
		totalVAT =  parseFloat(iVA)
		totalGrossCost =parseFloat(iGC)
		
		document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		document.getElementById("hiVA").value = FormatCurrency(totalVAT)
		document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
		
		/*document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)*/
		document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalNetCost)
//		alert(document.getElementById("hdnRunBal").value)
		document.getElementById("bal").innerHTML =FormatCurrencyComma(parseFloat(document.getElementById("hdnRunBal").value)-totalGrossCost)
		}
		
	function AddCell(iRow, iData, iTitle, iAlign, iColor) {
		oCell = iRow.insertCell()
		oCell.innerHTML = iData
		if (iTitle != "") oCell.title = iTitle
		if (iAlign != "") oCell.style.textAlign = iAlign
		if (iColor != "") oCell.style.backgroundColor = iColor
		}
	
	function DeleteRow(RowID,NE,VA,GR)
		{
		var srchStr
		//document.getElementById("hdnDeleted").value=1
		
		var tot="//records[@ids=" + RowID + "]/total"
		//alert(pttyCash.XMLDocument.xml)
		running_balance += parseFloat(pttyCash.XMLDocument.documentElement.selectSingleNode(tot).text)
		bal.innerHTML = FormatCurrencyComma(running_balance)

//		srchStr="//records[@ids=" + RowID + "]"
		srchStr1="//records[@ids=" + RowID + "]/Deleted"
//		var d =pttyCash.XMLDocument.documentElement.selectSingleNode(srchStr)
//		var d1=pttyCash.XMLDocument.documentElement.selectSingleNode(srchStr1)
		//pttyCash.XMLDocument.documentElement.removeChild(d)
		pttyCash.XMLDocument.documentElement.selectSingleNode(srchStr1).text="1"
		
		
		//alert(d.selectSingleNode("/Deleted").text)
		
		oTable = document.getElementById("ItemTable")
		/*sTable.deleteRow("TR" + RowID)*/

		var d =pttyCash.XMLDocument.documentElement.childNodes.length
		//var t=pttyCash.XMLDocument.documentElement.selectSingleNode("//records")
		var nCost=0;
		for (i=0; i< d;i++)
			{
				if (parseInt(pttyCash.XMLDocument.documentElement.childNodes.item(i).selectSingleNode("Deleted").text)==0)
				{
					nCost=nCost+  parseFloat(pttyCash.XMLDocument.documentElement.childNodes.item(i).selectSingleNode("netcost").text)
				}
				//t=pttyCash.XMLDocument.documentElement.selectSingleNode("//records").nextSibling
			}
		
		
		SetTotals (nCost, 0, nCost)
		
		
		
		
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "TR" + RowID) {
				oTable.deleteRow(i)
				//SetTotals (-NE, -VA, -GR)				
				break;
				}
			}
			
		if (oTable.rows.length == 1) {
			oRow = oTable.insertRow()
			oRow.id = "EMPTYLINE"
			oCell = oRow.insertCell()
			oCell.colSpan = 7
			oCell.innerHTML = "Please enter an item from above"
			oCell.style.textAlign = "center"
			}
		ResetData()
		}

	function SavePurchaseOrder()
	{
		ResetData()
		SetChecking(2)	
		var url		
		var xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		
		

		if ( document.getElementById("btOrder").value.indexOf("CREATE")> 0)
		{
				url="/Includes/xmlServer/xmlsrv.asp?id=0" + "&task=PTYSAVE";
		}
		else
		{
				url="/Includes/xmlServer/xmlsrv.asp?id=" +txid + "&task=PTYUPDATE"
		}
		
		if (parseFloat(document.getElementById("hiGC").value) <= 0) {
			alert("Please enter some items before creating a purchase order.")
			return false;
			}
			
		
		//THISFORM.t.value=pttyCash.XMLDocument.xml
		
		xmlHttp.open("POST", url, false);
		xmlHttp.setRequestHeader( "Content-Type","text/xml")
		xmlHttp.send(pttyCash.XMLDocument);
				
		xmlobj=xmlHttp.responseXML
		var msg=xmlobj.xml

//		alert(xmlobj.xml)
		location.href="PettyCash_dtl.asp"
		
		}
	
	
	function Select_Costcentre()
	{
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var url="/Includes/xmlServer/xmlsrv.asp?id=" + THISFORM.sel_COSTCENTRE.value + "&task=PTY&subtask=HEAD";
		var nodeMap
		
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		//alert(xh.responseText);		
		xmlobj.async = false;
		var t=xmlobj.selectSingleNode("//rs:data")
		var f=t.childNodes;
		THISFORM.sel_HEAD.options.length=0;
		var oOption = document.createElement("OPTION");
		THISFORM.sel_HEAD.options.add(oOption);
		oOption.innerText = "Please Select"
		//oOption.value = 0
		for (var i=0; i<f.length; i++)
		{ 
			var oOption = document.createElement("OPTION");
			THISFORM.sel_HEAD.options.add(oOption);
			oOption.innerText = f[i].getAttribute("DESCRIPTION")
			oOption.value = f[i].getAttribute("HEADID")
		} 
	}
	function Select_OnchangeHead()
	{
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var url="/Includes/xmlServer/xmlsrv.asp?id=" + THISFORM.sel_HEAD.value + "&task=PTY&subtask=EXP";
		
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		xmlobj.async = false;
		var t=xmlobj.selectSingleNode("//rs:data")
		var f=t.childNodes;
		THISFORM.sel_EXPENDITURE.options.length=0;
		var oOption = document.createElement("OPTION");
		THISFORM.sel_EXPENDITURE.options.add(oOption);
		oOption.innerText = "Please Select"
		//oOption.value = 0
		for (var i=0; i<f.length; i++)
		{ 
			var oOption = document.createElement("OPTION");
			THISFORM.sel_EXPENDITURE.options.add(oOption);
			oOption.innerText = f[i].getAttribute("DESCRIPTION")
			oOption.value = f[i].getAttribute("EXPENDITUREID")
		} 
	}
	
	function Get_Data(txnid,cnt)
	{
		
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var url="/Includes/xmlServer/xmlsrv.asp?id=" + txnid + "&task=PTYGETDATA";
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		xmlobj.async = false;
		txid=txnid;
		//alert(xmlobj.xml)
		var t=xmlobj.selectSingleNode("//rs:data")
		var f=t.childNodes;
		for (var i=0; i<f.length; i++)
			{ 
				
				
				document.getElementById("txt_ITEMREF").value=f[i].getAttribute("ITEMNAME")
				document.getElementById("txt_ITEMDESC").value=f[i].getAttribute("ITEMDESC")
				document.getElementById("txt_vNum").value=f[i].getAttribute("VNUMBER")
				document.getElementById("sel_COSTCENTRE").value=f[i].getAttribute("COSTCENTREID")
 				Select_Costcentre()
				document.getElementById("sel_HEAD").value=f[i].getAttribute("HEADID")
				Select_OnchangeHead()
				document.getElementById("sel_EXPENDITURE").value=f[i].getAttribute("EXPENDITUREID")
				document.getElementById("txt_NETCOST").value=f[i].getAttribute("NETCOST")
				document.getElementById("txt_GROSSCOST").value=f[i].getAttribute("NETCOST")
				document.getElementById("ORDERITEMID").value=f[i].getAttribute("ORDERITEMID")
				document.getElementById("txt_Date").value=f[i].getAttribute("VDATE")
				document.getElementById("hdnLineId").value=f[i].getAttribute("LINEID")
				if (f[i].getAttribute("VATINC")==0)
				{
					document.getElementById("invvat").checked=true;
				}
				
				AddRow(-1)
			} 
		document.getElementById("btOrder").value=" UPDATE ORDER "
		document.getElementById("AddButton").disabled=true
	}
	
		
</script>

<xml id="pttyCash"> <root> </root> </xml> 
<%
	dim xmlStracture
	dim rsdata
	dim txnid
	txnid=request("txnid")
	'if request("txnid") = "" then
'					xmlStracture="<xml id=""ptyAdd"">" 
'		xmlStracture=xmlStracture &	"<records ids=""0"">" 
'		xmlStracture=xmlStracture &		"<id>1</id>" 
'		xmlStracture=xmlStracture &		"<bank>15</bank>" 
'		xmlStracture=xmlStracture &		"<voucher></voucher>"
'		xmlStracture=xmlStracture &		"<vdate>" & formatDateTime(Date,2) &	"</vdate>" 
'		xmlStracture=xmlStracture &		"<item></item>" 
'		xmlStracture=xmlStracture &		"<cc></cc>" 
'		xmlStracture=xmlStracture &		"<netcost></netcost>" 
'		xmlStracture=xmlStracture &		"<notes></notes>" 
'		xmlStracture=xmlStracture &		"<heads></heads>" 
'		xmlStracture=xmlStracture &		"<expenditure></expenditure>" 
'		xmlStracture=xmlStracture &		"<vattype>0</vattype>" 
'		xmlStracture=xmlStracture &		"<vat>0.00</vat>" 
'		xmlStracture=xmlStracture &		"<total></total>" 
'		xmlStracture=xmlStracture &		"<vatinc>0</vatinc>" 
'		xmlStracture=xmlStracture &	"</records>" 
'		xmlStracture=xmlStracture &"</xml>" 
'		response.write(xmlStracture)
	'else
		//OpenDB()
		//SQL=select 
	'end if
	
%>
<!--
**************************************************************
				IMPORTENT NOTE:
Do not change the format or stracture of the xml 
**************************************************************

-->
<xml id="ptyAdd"> <records ids="0"> <id>1</id> <bank>15</bank> <voucher><%=VoucherNum%></voucher> 
<vdate><%=formatDateTime(Date,2)%> </vdate> <item></item> <cc></cc> <netcost></netcost> 
<notes></notes> <heads></heads> <expenditure></expenditure> <vattype>0</vattype> 
<vat>0.00</vat> <total></total> <vatinc>0</vatinc><OrderItemid>0</OrderItemid><lineid></lineid><Deleted>0</Deleted></records></xml> 

<body BGCOLOR="#FFFFFF" ONLOAD="initSwipeMenu(3);preloadImages(); <% if txnid > 0 Then %> Get_Data(<%=txnid%>,<%= request("cnt")%>) <% End If %>" onUnload="macGo()" MARGINHEIGHT="0" LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0"> 
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<iframe src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style="display:NONE"></iframe> 
<form NAME="THISFORM" method="post">
  <br>
  <table STYLE="BORDER:1PX SOLID BLACK" cellspacing="0" cellpadding="3" width="755">
    <tr bgcolor="steelblue" style="color:white"> 
      <td colspan="9"><b>NEW ITEM</b></td>
    </tr>
    <tr> 
      <td> Bank </td>
      <td> 1100 - Petty cash </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_ITEMREF"></td>
      <td>&nbsp;Voucher No:</td>
      <td> 
        <input type="text" name="txt_vNum" class="textbox200" maxlength="50" size="11" datasrc="#ptyAdd" datafld="Voucher">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_vNum"></td>
      <td>&nbsp;Date :</td>
      <td>
        <input type="text" name="txt_Date" value="<%=formatDateTime(Date,2)%>" class="textbox200" maxlength="12" style="width:75" datasrc="#ptyAdd" datafld="vdate">
      </td>
    </tr>
    <tr> 
      <td>Item : </td>
      <td>
        <input name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex="3" datasrc="#ptyAdd" datafld="item">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_ITEMREF"></td>
      <td>&nbsp;Cost Centre : </td>
      <td><%=lstCostCentres%></td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRE"></td>
      <td>&nbsp;Cost : </td>
      <td>
        <input style="text-Align:right;width:75" name="txt_NETCOST" type="text" class="textbox" maxlength="20" size="11" onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()" tabindex="5" datasrc="#ptyAdd" datafld="netcost">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_NETCOST"></td>
    </tr>
    <tr> 
      <td rowspan="3" valign="top">Notes : </td>
      <td rowspan="4">
        <textarea datasrc="#ptyAdd" datafld="notes" tabindex="3" name="txt_ITEMDESC" rows="7" class="TEXTBOX200" style="overflow:hidden;border:1px solid #133E71"></textarea>
      </td>
      <td rowspan="3"><img src="/js/FVS.gif" width="15" height="15" name="img_ITEMDESC"></td>
      <td>&nbsp;Head : </td>
      <td> 
        <select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()" tabindex="4" datasrc="#ptyAdd" datafld="heads">
          <option value>Please Select a Cost Centre...</option>
        </select>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_HEAD"></td>
      <td>&nbsp;Vat Inc: </td>
      <td>
        <input type="checkbox" name="invvat" datasrc="#ptyAdd" datafld="vatInc">
      </td>
    </tr>
    <tr> 
      <td>&nbsp;Expenditure : </td>
      <td> 
        <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" tabindex="4" datasrc="#ptyAdd" datafld="Expenditure">
          <option value>Please select a Head...</option>
        </select>
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_EXPENDITURE"></td>
      <td width="0">
        <!--&nbsp;VAT :-->
      </td>
      <td>
        <input style="text-Align:right;visibility:hidden" name="txt_VAT" type="text" datasrc="#ptyAdd" datafld="vat" class="textbox" maxlength="20" size="11" onBlur="TotalBoth()" onFocus="alignLeft()" VALUE="0.00" tabindex="5">
        <img src="/js/FVS.gif" width="15" height="15" name="img_VAT"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td></td>
      <td>
        <!--&nbsp;Total :-->
      </td>
      <td>
        <input datasrc="#ptyAdd" datafld="total" style="text-Align:right;visibility:hidden" name="txt_GROSSCOST" type="text" class="textbox" maxlength="20" size="11" readonly tabindex="-1">
      </td>
      <td><img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST"></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right" colspan="3"> 
        <input TYPE="HIDDEN" NAME="IACTION">
        <input TYPE="HIDDEN" NAME="UPDATEID">
        <input TYPE="HIDDEN" NAME="EXPENDITURE_LEFT_LIST">
        <input TYPE="HIDDEN" NAME="EMPLOYEE_LIMIT_LIST">
		<input TYPE="HIDDEN" NAME="ORDERITEMID" VALUE="0" datasrc="#ptyAdd" datafld="OrderItemid">
        <table cellspacing="0" cellpadding="0">
          <tr> 
            <td nowrap>
              <input type="button" Name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()" tabindex="6">
              &nbsp;</td>
            <td>
              <input type="button" Name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(-1)" tabindex="6">
            </td>
            <td>
              <input type="button" Name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateRow()" style="display:none" tabindex="6">
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr> 
      <td>
        <!--&nbsp;Vat Type :-->
      </td>
      <td><%=lstVAT%></td>
      <td><img src="/js/FVS.gif" name="img_VATTYPE"></td>
    </tr>
  </table>
  <br>
  <table STYLE="BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)" cellspacing="0" cellpadding="3" width="755" id="ItemTable" slcolor hlcolor="silver">
    <thead> 
    <tr bgcolor="steelblue" ALIGN="RIGHT" style="color:white"> 
      <td height="20" ALIGN="left" width="245" NOWRAP><b>Item Name:</b></td>
      <td width="200" nowrap align="left"><b>Expenditure:</b></td>
      <td width="80" nowrap><b>Gross (�):</b></td>
      <td width="29" nowrap>&nbsp;</td>
    </tr>
    </thead> <TBODY> 
    <TR ID="EMPTYLINE">
      <TD colspan=7 align="center">Please enter an item from above</TD>
    </TR>
    </TBODY> 
  </table>
  <!--<div STYLE="overflow: auto; width: 756px; height: 182; 
            border-left: 1px gray solid; border-bottom: 1px gray solid; 
            padding:0px; margin: 0px">
  <table STYLE="BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)" cellspacing="0" cellpadding="3" width="755" id="ItemTable" slcolor hlcolor="silver">
	
		
  </table>
 </div>-->
  <table STYLE="BORDER:1PX SOLID BLACK;border-top:none" cellspacing="0" cellpadding="2" width="755">
    <tr bgcolor="steelblue" ALIGN="RIGHT"> 
      <td height="20" width="490" NOWRAP style="border:none;color:white"><b>TOTAL 
        : &nbsp;</b></td>
      <td width="80" nowrap bgcolor="white"><b>
        <input type="hidden" id="hiNC" value="0">
        <div id="iNC"></div>
        </b></td>
      <td width="75" nowrap bgcolor="white"><b>
        <input type="hidden" id="hiVA" value="0">
        <div id="iVA"></div>
        </b></td>
      <td width="80" nowrap bgcolor="white"><b>
        <input type="hidden" id="hiGC" value="0">
        <div id="iGC"></div>
        </b></td>
      <td width="29" nowrap bgcolor="white">&nbsp;</td>
    </tr>
    <tr bgcolor="steelblue" ALIGN="RIGHT"> 
      <td height="20" width="490" NOWRAP style="border:none;color:white"><b>REMAINING 
        : &nbsp;</b></td>
      <td width="80" nowrap bgcolor="white"></td>
      <td width="75" nowrap bgcolor="white"></td>
      <td width="80" nowrap bgcolor="white"><b>
        <div id="bal"><%=FormatNumber(balance,2)%></div>
        </b></td>
      <td width="29" nowrap bgcolor="white">&nbsp;</td>
    </tr>
  </table>
  <br>
  <table cellspacing="0" cellpadding="2" width="755">
    <tr ALIGN="RIGHT"> 
      <td width="754" align="right" nowrap>&nbsp;
        <input type="button" value=" CREATE ORDER " class="RSLButton" onclick="SavePurchaseOrder()" name="btOrder">
      </td>
    </tr>
  </table>
  <input type="hidden" name="hdnId" datasrc="#ptyAdd" datafld="id" value="1">
  <input type="hidden" name="hdnLineId" datasrc="#ptyAdd" datafld="lineid">
  <!--<input type="hidden" name="hdnDeleted" datasrc="#ptyAdd" datafld="Deleted" >-->
  <input type="hidden" name="hdnRunBal" value=<%=balance%> >

  <!--<textarea  name="t"></textarea>-->
  
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>


