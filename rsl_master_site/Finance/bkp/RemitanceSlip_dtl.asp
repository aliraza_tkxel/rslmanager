<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<HTML>
<HEAD>
<!-- #include virtual="Connections/db_connection.asp" -->

<%

dim suppRs,InvDtl,dbcmd,rsDtl
dim InvId,gSum
dim recCount
InvId=-1
gSum=0

set suppRs= server.createobject("ADODB.Recordset")
set InvDtl= server.createobject("ADODB.Recordset")
set rsDtl= server.createobject("ADODB.Recordset")
set dbcmd= server.createobject("ADODB.Command")
'get  Supplier details.....
strsql= "select NAME,ADDRESS1,ADDRESS2, ADDRESS3,TOWNCITY,POSTCODE,COUNTY from " & _
		"S_ORGANISATION  " & _
		" where ORGID = " & Request.QueryString("Supplierid") 

suppRs.Open strsql,RSL_CONNECTION_STRING
'suppRs.ActiveConnection=RSL_CONNECTION_STRING



strsql="SELECT    Inv.ORDERID, Inv.INVOICENUMBER, Inv.INVOICEID, Inv.TAXDATE,Inv.GROSSCOST, PTYPE.DESCRIPTION, Inv.RECONCILEDATE   " &_
		"FROM  F_INVOICE INV INNER JOIN  " &_
		"F_POBACS PBACS ON INV.INVOICEID = PBACS.INVOICEID INNER JOIN  " &_
		"F_BACSDATA BACDATA ON PBACS.DATAID = BACDATA.DATAID INNER JOIN   " &_
		"S_ORGANISATION SUP ON INV.SUPPLIERID = SUP.ORGID INNER JOIN  " &_
		"F_PAYMENTTYPE PTYPE ON INV.PAYMENTMETHOD = PTYPE.PAYMENTTYPEID  " &_
		"WHERE      CONVERT(VARCHAR(12), PBACS.PROCESSDATE, 103) ='" & Request.QueryString("PROCESSDATE") & "' and SUP.ORGID=" & Request.QueryString("Supplierid") & " and PTYPE.PAYMENTTYPEID=" & Request.QueryString("PAYMENTTYPEID")





InvDtl.Open strsql,RSL_CONNECTION_STRING,3

recCount= InvDtl.RecordCount
TotPages = 1
RecordsLeft = recCount - 35
if (RecordsLeft > 0) then
	TotPages = TotPages + RecordsLeft / 55 
	if (RecordsLeft mod 55 > 0) then
		TotPages = TotPages + 1
	end if
end if

dim ar
ar=split(cstr(TotPages),".")
'Response.Write (ar(0))

%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Business</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<!--
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
-->
<script lanaguage=javascript>
function PrintMe(){
	document.getElementById("PrintButton").style.visibility = "hidden"
	print()
	document.getElementById("PrintButton").style.visibility = "visible"	
	}
</script>
<BODY BGCOLOR=#ffffff  MARGINHEIGHT="0" TOPMARGIN="10" MARGINWIDTH="0" >
<table width="100%" >
<tr>
<td>&nbsp;&nbsp;
</td><td  valign=top>
	<table width="95%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap><b><font color="#133e71" style="FONT-SIZE: 14px">Remitance Slip</font></b></td>
		<td></td>
	</tr>
	</table>
	<br>
	<br>
	<table width="95%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td nowrap><%=suppRs("NAME")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("ADDRESS1")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("ADDRESS2")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("ADDRESS3")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("TOWNCITY")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("POSTCODE")%></td>
		<td></td>
	</tr>
	<tr>
		<td nowrap><%=suppRs("COUNTY")%></td>
		<td></td>
	</tr>
	<tr>
		<td height=20>Date : <%=FormatDateTime(date,1)%>  </td>
		<td height=20  align=right  > 
	            <input type="button" value=" Print" onClick="PrintMe()" class="RSLButton" name="PrintButton" style="LEFT: 311px; TOP: 0px">
	     </td>
	</tr>
	</table>

	<table>
	<tr>
		<td>Payment method :</td>
		<td nowrap><%=InvDtl("DESCRIPTION")%></td>
		
	</tr>
	<br>
	<br>
	<tr>
		<td>Payment date :</td>
		<!--<td nowrap><%=InvDtl("RECONCILEDATE")%></td>-->
		<td nowrap><%=FormatDateTime(Request.QueryString("PROCESSDATE"),1)%>
		
		
	</tr>
	</table>
	<%
		dim PCount
		PCount=1
	%>

	<table  WIDTH="95%" CELLPADDING=2 CELLSPACING=0 STYLE="BEHAVIOR: url(../../Includes/Tables/tablehl.htc); BORDER-BOTTOM: 2px; BORDER-COLLAPSE: collapse;" border=0 hlcolor="STEELBLUE" slcolor="">
	<%
		dim output
		dim THeader
		dim mdcond
		dim firstpage
		dim pc
		pc=1
		firstpage=true
		mdcond=35
		THeader="<thead><tr valign=top>" & _
					"<td STYLE='BORDER-LEFT: 2px solid;BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid'><b><font color=#133e71>Tax Date</font></b></td> " &_
					"<td STYLE='BORDER-TOP: 2px solid; BORDER-BOTTOM: 2px solid'><b><font color='#133e71'>Invoice No:	</font></b></td> " &_
					"<td STYLE='BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid'><b><font color='#133e71'>Purchase No:</font></b></td>" &_
					"<td STYLE='BORDER-RIGHT: 2px solid;BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid' align=right><b><font color='#133e71'>Amount</font></b></td>" &_
				"</tr></thead>"
				
				Response.Write THeader
				
				Response.Write "<tbody>"
				while not InvDtl.EOF
				
				
				gSum=gSum + InvDtl("GROSSCOST")
				
				
				output="<tr>" & _
					"<td STYLE='BORDER-LEFT:SOLID 1PX #133E71;BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & InvDtl("TAXDATE")	& "</td>" &_
					"<td STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & InvDtl("INVOICENUMBER") & "</td>" &_
					"<td STYLE='BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0'>" & PurchaseNumber(InvDtl("ORDERID")) & "</td>" &_
					"<td STYLE='BORDER-RIGHT:SOLID 1PX #133E71;BORDER-BOTTOM:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE;border-color:#C0C0C0' align=right>" & FormatCurrency(InvDtl("GROSSCOST"))  & "</td>" &_
				"</tr>"
			
				Response.Write output
				Response.Write "</tbody>"
				output=""
				
				
				
				
				if PCount mod mdcond=0 then
					if PCount=mdcond and firstpage=true then
						Response.Write "<tr><td align=right  STYLE='BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid; background-color:white;color:black' colspan=4 >" &  pc &" of " & ar(0) &"<p STYLE='page-break-after: always '>"
						Response.Write THeader
						Response.Write "</p></td></tr>"
						mdcond=50
						firstpage=false
						pc=pc+1
					end if
					if (PCount-mdcond)=mdcond and firstpage=false then
						Response.Write "<tr><td align=right  STYLE='BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid; background-color:white;color:black' colspan=4 >" &  pc &" of " & ar(0) &"<p STYLE='page-break-after: always '>"
						Response.Write THeader
						Response.Write "</p></td></tr>"
						mdcond=50
						firstpage=false
						pc=pc+1
					end if
						
					
				end if
				PCount=PCount+1
				InvDtl.MoveNext
				
				'Response.Flush
			wend
			suppRs.Close
			InvDtl.Close
			
			set dbcmd=nothing
			set suppRs=nothing
			set InvDtl=nothing
			
		%>
		<tfoot>
		<tr >
		<TD NOWRAP STYLE="BORDER-RIGHT: 2px solid;BORDER-LEFT: 2px solid;BORDER-TOP: 2px solid;  BORDER-BOTTOM: 2px solid" colspan=4 align=right>
			<b>Total 
	      Amount &nbsp;: &nbsp; <%=FormatCurrency( gSum)%></b>
		</TD>
		
		</tr>
		<TD align=right  STYLE='BORDER-TOP: 2px solid;   background-color:white;color:black' colspan=4 > <%= pc %>  of  <%= ar(0) %></td>
		
		</tr>
		</tfoot>
	</table>
</td>
</tr>
</table>
</BODY>
</HTML>

