<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc		   (3)

	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Payment Date|PROCESSDATE_TXT|120"
	SortASC(0) 	= "PBACS.PROCESSDATE ASC"
	SortDESC(0) = "PBACS.PROCESSDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Supplier|Name|300"
	SortASC(1) 	= "Sup.Name ASC"
	SortDESC(1) = "Sup.Name DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Payment Method|PAYMENTMETHOD|150"
	SortASC(2) 	= "PTYPE.PAYMENTMETHOD ASC"
	SortDESC(2) = "PTYPE.PAYMENTMETHOD DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Amount|Total|120"
	SortASC(3) 	= "Total ASC"
	SortDESC(3) = "Total DESC"	
	TDSTUFF(3)  = " "" ALIGN=RIGHT"" "
	TDFunc(3) = "FormatCurrency(|)"		

			

	PageName = "RemitanceSlip.asp"
	EmptyText = "No Relevant Remittance Slips found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	
	RowClickColumn = " "" ONCLICK=""""load_me('"" & rsSet(""PROCESSDATE"") & ""',"" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTMETHODid"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " WHERE FINV.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	end if


	SQLCODE="SELECT  PBACS.PROCESSDATE, CONVERT(VARCHAR, PBACS.PROCESSDATE, 103) AS PROCESSDATE_txt,  "  &_
			"SUP.NAME AS Name, 	PTYPE.DESCRIPTION AS PAYMENTMETHOD, "  &_
			"sum(FINV.GROSSCOST)  AS Total, "  &_
			"SUP.ORGID, FINV.PAYMENTMETHOD as PAYMENTMETHODid " &_
			"FROM F_INVOICE FINV  " &_
			"INNER JOIN F_POBACS PBACS ON FINV.INVOICEID = PBACS.INVOICEID 	 " &_
			"INNER JOIN F_BACSDATA BACDATA ON PBACS.DATAID = BACDATA.DATAID  " &_
			"INNER JOIN S_ORGANISATION SUP ON FINV.SUPPLIERID = SUP.ORGID  " &_
			"INNER JOIN F_PAYMENTTYPE PTYPE ON FINV.PAYMENTMETHOD = PTYPE.PAYMENTTYPEID " &_
			"" & SupFilter &_
			"GROUP BY  SUP.ORGID,PBACS.PROCESSDATE, SUP.NAME, PTYPE.DESCRIPTION,FINV.PAYMENTMETHOD " &_
			"ORDER BY " & orderBy


	


	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Remitence Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(ProcessDate, SupplierID, PaymentType){
	window.showModelessDialog("RemitanceSlip_dtl.asp?SupplierID=" + SupplierID  + "&PROCESSDATE=" + ProcessDate + "&PAYMENTTYPEID=" + PaymentType + "&Random=" + new Date(),1,"dialogHeight: 600px; dialogWidth:790px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;");
	//,,"dialogHeight: 200px; dialogWidth: 250px; dialogTop: 23px; dialogLeft: 1076px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;");
	
	}

function setSupplier(){
	thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
	}

function SubmitPage(){
	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
Click on the purchase order that you would like to reconcile. 
<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><%=lstSuppliers%>
</td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()" id=button1 name=button1>
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>