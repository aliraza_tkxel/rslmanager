<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	Const adUseClient = 3
	
	Dim oConnection

	Set oConnection = CreateObject("ADODB.Connection")
	oConnection.Open "DSN=RSLQB;"
	 				
	Call credit_account(4000, 120, True)
	'Call credit_account(4030, 120, 1)
	'Call credit_account(4040, 10, 1)
	'Call credit_account(4050, 10, 1)
	'Call credit_account(4060, 20, 1)
	'Call credit_account(4070, 20, 1)
	'Call credit_account(4080, 20, 1)	
	
	Call debit_account(1600, 120, False)

	Function debit_account(nominal_code, debit_amount, FQ_status)
		
		Dim nominal_code_table_listid , sel_Recordset, drRecordset	
		
		// get correct reference id from accounts table
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		Set sel_Recordset = Nothing
		response.write "<BR> list id = : " & nominal_code_table_listid
		
		// open recordset for updating
		Set drRecordset = CreateObject("ADODB.Recordset")
		drRecordset.CursorLocation = adUseClient
		drRecordset.Open "SELECT * FROM JOURNALENTRYDEBITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		// update
		drRecordset.AddNew()
		drRecordset.Fields("REFNUMBER").Value = "1"
		drRecordset.Fields("JOURNALDEBITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		drRecordset.Fields("JOURNALDEBITLINEAMOUNT").Value = debit_amount
		drRecordset.Fields("JOURNALDEBITLINEMEMO").Value = "Auto RSL Debit"
		drRecordset.Fields("JournalDEBitLineEntityRefListID").Value = "A0000-1084200309"
		drRecordset.Fields("JournalDEBitLineEntityRefFullName").Value = "BHA"
		drRecordset.Fields("FQSaveToCache").Value = FQ_status	
		drRecordset.Update()
		drRecordset.close()
		
	End Function	

	Function credit_account(nominal_code, credit_amount, FQ_status)
		
		Dim nominal_code_table_listid , sel_Recordset, crRecordset	
		
		// get correct reference id from accounts table
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		Set sel_Recordset = Nothing
		response.write "<BR> list id = : " & nominal_code_table_listid
		
		// open recordset for updating
		Set crRecordset = CreateObject("ADODB.Recordset")
		crRecordset.CursorLocation = adUseClient
		crRecordset.Open "SELECT * FROM JOURNALENTRYCREDITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		// update
		crRecordset.AddNew()
		crRecordset.Fields("REFNUMBER").Value = "1"
		crRecordset.Fields("JOURNALCREDITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		crRecordset.Fields("JOURNALCREDITLINEAMOUNT").Value = debit_amount
		crRecordset.Fields("JOURNALCREDITLINEMEMO").Value = "Auto RSL Debit"
		crRecordset.Fields("JournalCREDitLineEntityRefListID").Value = "A0000-1084200309"
		crRecordset.Fields("JournalCREDitLineEntityRefFullName").Value = "BHA"
		crRecordset.Fields("FQSaveToCache").Value = FQ_status	
		crRecordset.Update()
		crRecordset.close()
		
	End Function	

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bacs List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF>

</BODY>
</HTML>