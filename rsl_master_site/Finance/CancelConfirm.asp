<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 25%;
    top: 25%;
    width: 25%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0),0 6px 20px 0 rgba(0,0,0,0);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:0}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:0}
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header {
    padding: 2px 16px;
    background-color: #000000;
    color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
    padding: 2px 16px;
    background-color: #5cb85c;
    color: white;
}
</style>
</HEAD>

</HTML>
<%
L_ORDERID = 0
IF(Request("ORDERID") <> "" AND ISNUMERIC(Request("ORDERID"))) THEN L_ORDERID = Request("OrderID")
%>
<%
OpenDB()

UserId=Session("UserID")
EmployeeLimit = 0

SQL = "SELECT PO.ORDERID, PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE, " &_
		"D.DEVELOPMENTNAME FROM F_PURCHASEORDER PO " &_
		"INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID " &_
		"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 AND PISTATUS Not IN (9,10,11,12,13,14,16) GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_		
		"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = PO.DEVELOPMENTID " &_		
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " &_
		"WHERE ACTIVE = 1 AND PO.ORDERID = " & L_ORDERID
		
Call OpenRs(rsDR, SQL)
if (NOT rsDR.EOF) then
	ORDERID = rsDR("ORDERID")
	PONUMBER = PurchaseNumber(rsDR("ORDERID"))
	PONAME = rsDR("PONAME")
	PODATE = FormatDateTime(rsDR("PODATE"),1)
	SUPPLIER = rsDR("NAME")
	SUPPLIERID = rsDR("SUPPLIERID")	
	POSTATUSNAME = rsDR("POSTATUSNAME")
	DEVELOPMENT = rsDR("DEVELOPMENTNAME")
else
	Response.Redirect ("PurchaseOrderNotFound.asp")
end if
Call CloseRs(rsDR)


SQL = "SELECT PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, PI.NETCOST, PI.VAT, V.VATNAME, PI.GROSSCOST, " &_
		"PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE FROM F_PURCHASEITEM PI " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
		"INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE " &_
		"INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS " &_
		"WHERE PI.ACTIVE = 1 AND ORDERID = " & ORDERID & " ORDER BY ORDERITEMID ASC"

PIString = ""
TotalNetCost = 0
TotalVAT = 0
TotalGrossCost = 0
TotalReconcilableNetCost = 0
TotalReconcilableVAT = 0
TotalReconcilableGrossCost = 0
TotalRows = 0
RowsThatCanBeReconciled = 0
RowsThatWillBeReconciled = 0

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF) 
	
	OrderItemID = rsPI("ORDERITEMID")

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	DataStorage = "<INPUT TYPE=""HIDDEN"" id=""DS" & OrderItemID & """ NAME=""DS" & OrderItemID & """ VALUE=""" & NetCost & "|" & VAT & "|" & GrossCost & """>"


	UserName_Reason = "<INPUT id=""UserName_Reason"" TYPE=""HIDDEN"" NAME=""DSUserName_Reason"" ""  >"
	
	
	PITYPE = rsPI("PITYPE")
	PISTATUS = rsPI("PISTATUS")
	'Response.write(PISTATUS)
	itemIdAndStatus=OrderItemID&"|"&PISTATUS

	RowsThatCanBeReconciled = RowsThatCanBeReconciled + 1
	

	if (PISTATUS >= 9 AND PISTATUS <= 16) then
		CheckData = "<IMG SRC=""/myImages/tick.gif"" title=""This item has been reconciled/paid and therefore cannot be cancelled."">"
		'RowsThatCanBeReconciled = RowsThatCanBeReconciled - 1		
	elseif (PISTATUS = 0) then
		CheckData = "<IMG SRC=""/myImages/info.gif"" title=""This item is queued and must be declined via the Queued Purchases functionality."">"
		'RowsThatCanBeReconciled = RowsThatCanBeReconciled - 1		
	else
		CheckData = "<INPUT TYPE=""CHECKBOX"" NAME=""CHKS"" ID=""CHKS"" VALUE=""" & itemIdAndStatus & """ CHECKED onclick=""UpdateReconcileTotal()"">"
		RowsThatWillBeReconciled = RowsThatWillBeReconciled + 1
		TotalReconcilableNetCost = TotalReconcilableNetCost + NetCost
		TotalReconcilableVAT = TotalReconcilableVAT + VAT
		TotalReconcilableGrossCost = TotalReconcilableGrossCost + GrossCost
	end if

	PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT>" & DataStorage &UserName_Reason  & rsPI("ITEMNAME") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD><TD>" & CheckData & "</TD></TR>"
	ItemDesc = rsPI("ITEMDESC")
	if ( NOT(ItemDesc = "" OR isNULL(ItemDesc)) ) then
		PIString = PIString & "<TR ID=""PIDESCS"" STYLE=""DISPLAY:NONE""><TD width=410 colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"	
	end if
	rsPI.moveNext
wend

if (RowsThatWillBeReconciled = RowsThatCanBeReconciled) then
	DisplayText = "This Purchase Order will become fully cancelled"
else 
	if (RowsThatWillBeReconciled = 0) then
		DisplayText = "No rows have or can be selected therefore no cancellation will occur."
		DisabledButton = " disabled"
	elseif (RowsThatCanBeReconciled - RowsThatWillBeReconciled = 1) then
		DisplayText = "This Purchase Order will not be fully Cancelled as there is " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " item that has<br> not or can not be selected."
	else
		DisplayText = "This Purchase Order will not be fully Cancelled as there are " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " items that have<br> not or can not be selected."
	end if						
end if


'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID
Call OpenRs(rsINV, SQL)
if (NOT rsINV.EOF) then
	InvoiceString = "<br><TABLE WIDTH=755 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
			"<TD WIDTH=170 HEIGHT=20><b>Invoice No:</b></TD><TD width=100><b>Tax Date:</b></TD><TD width=180><b>Status</b></TD><TD align=right width=80><b>Net (�):</b></TD><TD align=right width=75><b>VAT (�):</b></TD><TD align=right width=80><b>Gross (�):</b></TD><TD WIDTH=30></TD></TR>"
	while NOT rsINV.EOF
		InvoiceString = InvoiceString & "<TR><TD>" & rsINV("InvoiceNumber") & "</TD><TD>" & rsINV("TAXDATE") & "</TD><TD>" & rsINV("THESTATUS") & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("NETCOST"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("VAT"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("GROSSCOST"),2) & "</TD></TR>"
		rsINV.movenext
	wend
	InvoiceString = InvoiceString & "</TABLE>"
end if
Call CloseRs(rsINV)

CloseDB()
%>
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Purchase Order > Cancellation</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var RowsThatWillBeReconciled = <%=RowsThatWillBeReconciled%>
	var RowsThatCanBeReconciled = <%=RowsThatCanBeReconciled%>
	
	function FormatCurrrenyComma(Figure){
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}

	function UpdateReconcileTotal(){
		iChecks = document.getElementsByName("CHKS")
		NetValue = 0
		TheVAT = 0
		GrossValue = 0
		if (iChecks.length){
			RowsThatWillBeReconciled = 0
			for (i=0; i<iChecks.length; i++){
				if (iChecks[i].checked == true){
					ORDERITEMID = iChecks[i].value
					ORDERITEMID=ORDERITEMID.split("|")
					FieldData = document.getElementById("DS" + ORDERITEMID[0]).value
					FieldArray = FieldData.split("|")
					NetValue += parseFloat(FieldArray[0])
					TheVAT += parseFloat(FieldArray[1])
					GrossValue += parseFloat(FieldArray[2])
					RowsThatWillBeReconciled++
					}
				}
			}
		document.getElementById("NET").innerHTML = FormatCurrrenyComma(NetValue)
		document.getElementById("VAT").innerHTML = FormatCurrrenyComma(TheVAT)
		document.getElementById("GROSS").innerHTML = FormatCurrrenyComma(GrossValue)				
		document.getElementById("ReconcileButton").disabled = false
		if (RowsThatWillBeReconciled == RowsThatCanBeReconciled)
			document.getElementById("INFO").innerHTML = "This Purchase Order will become fully cancelled"
		else {
			if (RowsThatWillBeReconciled == 0) {
				document.getElementById("ReconcileButton").disabled = true
				document.getElementById("INFO").innerHTML = "No rows have or can be selected therefore no Cancellation will occur."
				}
			else if (RowsThatCanBeReconciled - RowsThatWillBeReconciled == 1)
				document.getElementById("INFO").innerHTML = "This Purchase Order will not be fully Cancelled as there is " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " item that has<br> not or can not be selected."
			else
				document.getElementById("INFO").innerHTML = "This Purchase Order will not be fully Cancelled as there are " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " items that have<br> not or can not be selected."
			}						
		}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}		
		
	function Cancel(){
		document.getElementById("ReconcileButton").disabled=true;
		var invoiceRecivesStatusCode=["7","8","10","11","12","13","14","17","19"]
		var isInvoiceRecived=0
		//var IsPurchaseOrderCancel=0
		var inputs = document.querySelectorAll("input[type='checkbox']");
		for(var i = 1; i < inputs.length; i++)
		{
		var x=inputs[i].value ; 
		var arr=x.split('|')
		//inputs[i].value = arr[0]
		if(invoiceRecivesStatusCode.indexOf(arr[1])!=-1 && inputs[i].checked ==true)
		{
		isInvoiceRecived=1
		}
		//else if(inputs[i].checked ==false)
		//{
		//IsPurchaseOrderCancel=1
		//}
		}
		//if(IsPurchaseOrderCancel==1)
		//{
		//	document.getElementById("ReconcileButton").disabled = true
		//	document.getElementById("ReconcileButton").value = " Submitted "
		//	RSLFORM.action = "ServerSide/CancelConfirm_svr.asp"
		//	RSLFORM.submit();
		//}
		if(isInvoiceRecived==1) //&& IsPurchaseOrderCancel!=1 )
		{
			var modal = document.getElementById('ConfirmationPopUp');
			modal.style.display = "block";
			var inputs = document.querySelectorAll("input[type='checkbox']");
			for(var i = 0; i < inputs.length; i++) 
			{
				inputs[i].disabled = true;   
			}
		}
		else if(isInvoiceRecived!=1) //&& IsPurchaseOrderCancel!=1)
		{
			var inputs = document.querySelectorAll("input[type='checkbox']");
			for(var i = 0; i < inputs.length; i++) 
			{
			inputs[i].disabled = false;   
			}
			ReasonPopUp.style.display="block"
			//CancelPO();

		}		
		}

	function UpdateChecks(){
		if (document.getElementById("MegaClick").checked == true)
			NewStatus = true
		else
			NewStatus = false
		ChkRef = document.getElementsByName("CHKS")
		if (ChkRef.length){
			for (i=0; i<ChkRef.length; i++){
				ChkRef[i].checked = NewStatus
				}
			}
		UpdateReconcileTotal()
		} 


<% if Request("ER12" & Replace(Date, "/", "")) = 1 then %>
	alert("A user is processing some invoices to be paid.\nPlease wait a few seconds before re-processing your actions.")
<% end if %>	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border="0" cellspacing="5" cellpadding="0">
<FORM name=RSLFORM method=POST>
<TR><TD>
<table cellspacing=0 cellpadding=0><tr><td>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=490PX>
		<TR>
			<TD WIDTH=90PX>Order No:</TD><TD><%=PONUMBER%></TD>
		</TR><TR>
			<TD>Description:</TD><TD><%=PONAME%></TD>
		</TR><TR>		
			<TD>Order Date:</TD><TD><%=PODATE%></TD>
		</TR><TR>		
			<TD>Supplier:</TD><TD><%=SUPPLIER%></TD>						
		</TR><TR>		
			<TD>Development:</TD><TD><%=DEVELOPMENT%></TD>								
		</TR><TR>		
			<TD>Status:</TD><TD><%=POSTATUSNAME%></TD>								
		</TR>
	</TABLE>
</td><td width=10>&nbsp;</TD><td valign=top>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=255PX height=100>
		<TR>
			<TD height=100% colspan=3><font color=red><div id="INFO"><%=DisplayText%></div></font></TD>
		</TR>
	</TABLE>
</td></tr></table>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=LEFT width=410 NOWRAP><table cellspacing=0 cellpadding=0 width=410><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=80 nowrap><b>Net (�):</b></TD>
	  <TD width=75 nowrap><b>VAT (�):</b></TD>
	  <TD width=80 nowrap><b>Gross (�):</b></TD>
	  <TD width=30><input id="MegaClick" type=checkbox name="MegaClick" checked onclick="UpdateChecks()"></TD>		  
	</TR>
	<%=PIString%> 
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD width=30 bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL CANCEL AMOUNT : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="NET"><%=FormatNumber(TotalReconcilableNetCost,2)%></div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><div id="VAT"><%=FormatNumber(TotalReconcilableVAT,2)%></div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="GROSS"><%=FormatNumber(TotalReconcilableGrossCost,2)%></div></b></TD>
	  <TD width=30 bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>  
  <TABLE cellspacing=0 cellpadding=2 width=755>
	<TR ALIGN=RIGHT> 
	  <TD>
	  <input type="HIDDEN" name="hid_PAGE" value="<%=Request("CurrentPage")%>">
	  <input type="HIDDEN" name="hid_ORDERID" value="<%=ORDERID%>">
	  <input type="HIDDEN" name="hid_SUPPLIERID" value="<%=SUPPLIERID%>">
	  <input type="HIDDEN" name="hid_CANCEL" value="CANCEL">
	  <input type="BUTTON" id="ReconcileButton" name="ReconcileButton" value=" CANCEL " onclick="Cancel()" class="RSLButton" <%=DisabledButton%>>
	  </TD>		  
	</TR>
  </TABLE>
<%=InvoiceString%>  
</TD></TR>
</FORM>
</table>
<IFRAME  src="/secureframe.asp" NAME="PURCHASERECONCILEFRAME123" STYLE='DISPLAY:NONE'></IFRAME>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->

<div id="ConfirmationPopUp" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span id= "close" class="close">&times;</span>
      <h2>Invoice attached!</h2>
    </div>
    <div class="modal-body">
      <p> An invoice has been received for the PO you are canceling.</p>
	  <p>Would you like to continue?</p>
<button id="myBtn" onclick="Yes()">Yes</button>
<button id="myBtn" onclick="No()">No</button>
    </div>
  </div>
</div>

<div id="ReasonPopUp" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
     <!--<span id= "close" class="close">&times;</span> -->
      <h2>Reasons for Cancellation</h2>
    </div>
    <div class="modal-body">
	<br>
	<select id="Reasons" onchange="OnReasonChange()">
		<option value="" disabled selected>Please Select</option>
		<option value="Incorrect Supplier">Incorrect Supplier</option>
		<option value="Purchase Order duplicated">Purchase Order duplicated</option>
		<option value="Purchase Order no longer required">Purchase Order no longer required</option>
		<option value="Purchase Order raised in error">Purchase Order raised in error</option>
</select>
<br><br>
<button id="saveReason" disabled onclick="CancelPO()">Save Reason</button>
<br>
    </div>
  </div>
</div>


<script>
// Get the modal
var modal = document.getElementById('ConfirmationPopUp');
var ReasonPopUp = document.getElementById('ReasonPopUp');
//alert("script")
// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementById("close");

 //When the user clicks the button, open the modal 


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
	document.getElementById("ReconcileButton").disabled=false;
    modal.style.display = "none";
	ReasonPopUp.style.display="none";
	var inputs = document.querySelectorAll("input[type='checkbox']");
for(var i = 0; i < inputs.length; i++) {
    inputs[i].disabled = false;   
}
}

// When the user clicks anywhere outside of the modal, close it
//window.onclick = function(event) {
    //if (event.target == modal) {
    //    modal.style.display = "none";
   // }
//}
function Yes()
{
	modal.style.display = "none";
	ReasonPopUp.style.display="block"
	//document.getElementById("ReconcileButton").disabled = true
	//document.getElementById("ReconcileButton").value = " Submitted "
	//RSLFORM.action = "ServerSide/CancelConfirm_svr.asp"
	//RSLFORM.submit();

}
function No()
{
	document.getElementById("ReconcileButton").disabled=false;
modal.style.display = "none";
	var inputs = document.querySelectorAll("input[type='checkbox']");
for(var i = 0; i < inputs.length; i++) {
    inputs[i].disabled = false;   
}}
function CancelPO()
{
	var inputs = document.querySelectorAll("input[type='checkbox']");
	for(var i = 0; i < inputs.length; i++) 
	{
    inputs[i].disabled = false;   
	}
	var reason=document.getElementById("Reasons").value;
	document.getElementById("UserName_Reason").value=reason+"|"+<%=UserId%>//userID
	document.getElementById("ReconcileButton").disabled = true
	document.getElementById("ReconcileButton").value = " Submitted "
	RSLFORM.action = "ServerSide/CancelConfirm_svr.asp"
	RSLFORM.submit();
}
function OnReasonChange()
{
var value =document.getElementById("Reasons").value;
if(value ==null)
document.getElementById("saveReason").disabled=true;
else 
document.getElementById("saveReason").disabled=false;
}
</script>
</BODY>
</HTML>
