<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML> 
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager Finance - Finance Builder</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/formValidation.js"></SCRIPT>
<script language=javascript>
	var FormFields 
	var Diff
	function refreshSideBar(){
		thisForm.action = "ServerSide/FinanceBuilderDev_svr.asp";
		thisForm.target = "theSideBar";
		thisForm.submit();
		}
	
	function setText(txt,hideallboolean){
	TheText.innerHTML = txt;
	if (hideallboolean == 1) {
		Budget.style.display = "none";
		Head.style.display = "none";
		CostCentre.style.display = "none";
		}
	TheText.style.display = "block";
	}
	
	function DoCancel(){
		setText("Please select an item you would like to edit...",1);
	}
		
	function ResetDiv(what){
	Head.style.display = "none";
	CostCentre.style.display = "none";
	Budget.style.display = "none";
	if (what == "HEAD")
		temp = new Array("txt_HeadDescription", "txt_headallocation", "headcreated");
	else if (what == "EXPENDITURE")
		temp = new Array("txt_expenditurename", "txt_expenditureallocation");
	else if (what == "COSTCENTRE")
		temp = new Array("txt_CostCentreDescription", "CCID", "txt_AccountableBody", "txt_Department", "txt_DateStart", "txt_DateEnd", "txt_CostCentreAllocation");
		doc = document.all;
		for (i=0; i<temp.length; i++)
			doc[temp[i]].value = "";
		}
	
	function setCheckingArray(what){
	if (what == "HEAD") {
		FormFields = new Array("txt_HeadDescription|Head Description|TEXT|Y", "txt_headallocation|Head Allocation|CURRENCY|Y","txt_dtStart|Start Date|DATE|Y","endDate|End Date|DATE|Y","txt_totValue|Total Value|CURRENCY|Y");		
		
		}
	else if (what == "EXPENDITURE"){
		FormFields = new Array("txt_expenditurename|Expenditure Name|TEXT|Y", "txt_expenditureallocation|Expenditure Allocation|CURRENCY|Y","txt_OPBalance|Opening Balance|CURRENCY|Y");	
		}
	else if (what == "COSTCENTRE"){
		FormFields = new Array("txt_CostCentreDescription|Cost Centre|TEXT|Y", "txt_AccountableBody|Budget Holder|TEXT|Y", "txt_Department|Department|TEXT|Y", "txt_DateStart|Date Commence|DATE|Y", "txt_DateEnd|Date Ceases|DATE|Y", "txt_CostCentreAllocation|Amount|CURRENCY|Y");	
		}
	}
	
	function NewItem(what, item1){
	if (what == 2){
		ResetDiv('HEAD');
		setCheckingArray('HEAD');
		Head.style.display = "block";
		thisForm.CCID.value = item1;
		thisForm.HD_A.value = "ADD";	
		}
	else if (what == 4){
		ResetDiv('EXPENDITURE');
		setCheckingArray('EXPENDITURE');	
		Budget.style.display = "block";
		thisForm.HDID.value = item1;
		thisForm.EX_A.value = "ADD";	
		}
	else if (what == 1){
		showDeleteButton("COSTCENTRE",1);
		ResetDiv('COSTCENTRE');
		setCheckingArray('COSTCENTRE');
		setText("<table class='RSLBlack' width=380px><tr><td><b><u>Add New Cost Centre</u></b></td></tr></table>")	
		CostCentre.style.display = "block";
		thisForm.CC_A.value = "ADD";
		thisForm.costcentremintotal.value = "0.00";		
		}
	}
	
	function showDeleteButton(what, theStatus){
	doc = document.all;
	if (what == "HEAD") {
		if (theStatus == 1) 
			HEAD_DELETE.style.display = "none";
		else
			HEAD_DELETE.style.display = "block";
		}
	else if (what == "EXPENDITURE"){
		if (theStatus == 1) 
			EXPENDITURE_DELETE.style.display = "none";
		else
			EXPENDITURE_DELETE.style.display = "block";
		}
	else if (what == "COSTCENTRE"){
		if (theStatus == 1) 
			COSTCENTRE_DELETE.style.display = "none";
		else
			COSTCENTRE_DELETE.style.display = "block";
		}
	}
	
	function DoDelete(what){
	if (what == "HEAD") {
		thisForm.action = "ServerSide/HD.asp";
		thisForm.HD_A.value = "DELETE";		
		}
	else if (what == "EXPENDITURE"){
		thisForm.action = "ServerSide/EX.asp";
		thisForm.EX_A.value = "DELETE";		
		}
	else if (what == "COSTCENTRE"){
		thisForm.action = "ServerSide/CC.asp";
		thisForm.CC_A.value = "DELETE";	
		}
	thisForm.target = "FB";	
	thisForm.submit();
	}
	
	function DoSave(what){
	if (what == "HEAD") {
		thisForm.action = "ServerSide/HD.asp";	
		}
	else if (what == "EXPENDITURE"){
		thisForm.action = "ServerSide/EX.asp";
		}
	else if (what == "COSTCENTRE"){
		thisForm.action = "ServerSide/CC.asp";
		}
		if (checkForm()) 
			{
				
			thisForm.target = "FB";	
			thisForm.submit();
			}
	}
	
	function DoReset(what){
	if (what == "HEAD")
		temp = new Array("txt_HeadDescription", "txt_headallocation");
	else if (what == "EXPENDITURE")
		temp = new Array("txt_expenditurename", "txt_expenditureallocation");
	else if (what == "COSTCENTRE")
		temp = new Array("txt_CostCentreDescription", "txt_AccountableBody", "txt_Department", "txt_DateStart", "txt_DateEnd", "txt_CostCentreAllocation");
		doc = document.all;
		for (i=0; i<temp.length; i++)
			doc[temp[i]].value = "";
	}
	
	function checkAllocation(){
	if (checkForm(true)){
		doc = document.all;
		total = parseFloat(doc["totalfund"].value,10);
		totalallocated = parseFloat(doc["totalallocated"].value,10);
		totalremaining = parseFloat(doc["totalremaining"].value,10);
		headallocation = parseFloat(doc["txt_headallocation"].value,10);			
		mintotal = parseFloat(doc["mintotal"].value,10);
		totalleft = total - totalallocated;
		if (headallocation > totalleft){
			event.cancelBubble = true;
			alert("The value you have entered for the Head Allocation is greater than the balance remaining in its parent Cost Centre.\nTherefore the value will be set to the maximum value it can be.");
			doc["txt_headallocation"].value = FormatCurrency(totalleft);
			doc["totalremaining"].value	= "0.00";
			return false;
			}
		else if (headallocation < mintotal) {
			event.cancelBubble = true;
			alert("The value you have entered for the Head Allocation is less than the amount for which Expenditures have been allocated.\nTherefore the value will be set to the lowest value it can be.");
			doc["txt_headallocation"].value = FormatCurrency (mintotal);
			doc["totalremaining"].value	= FormatCurrency (total - totalallocated - mintotal);
			return false;
			}
		else 
			doc["totalremaining"].value	= FormatCurrency (total - totalallocated - headallocation);
		}
	}
	
	function checkAllocation2(){
	if (checkForm(true)){
		doc = document.all;
		total = parseFloat(doc["Btotalfund"].value,10);
		totalallocated = parseFloat(doc["Btotalallocated"].value,10);
		totalremaining = parseFloat(doc["Btotalremaining"].value,10);
		budgetallocation = parseFloat(doc["txt_expenditureallocation"].value,10);			
		mintotal = parseFloat(doc["Bmintotal"].value,10);
//		alert(parseFloat(doc["Bmintotal"].value,10))				
		totalleft = total - totalallocated;
		if (budgetallocation > totalleft){
			event.cancelBubble = true;
			alert("The value you have entered for 'Expenditure Allocation' is greater than the balance remaining in its parent HEAD.\nTherefore the value will be set to the maximum value it can be.");
			doc["txt_expenditureallocation"].value = FormatCurrency(totalleft);
			doc["Btotalremaining"].value = "0.00";
			return false;
			}
		else if (budgetallocation < mintotal) {
			event.cancelBubble = true;
			alert("The value you have entered for the 'Expenditure Allocation' is less than the amount for which\npurchases have been made under this expenditure.\nTherefore the value will be set to the lowest value it can be.");
			doc["txt_expenditureallocation"].value = FormatCurrency (mintotal);
			doc["Btotalremaining"].value	= FormatCurrency (total - totalallocated - mintotal);
			return false;
			}
		else 
			doc["Btotalremaining"].value	= FormatCurrency (total - totalallocated - budgetallocation);
		}
	}
	
	function checkAllocation3(){
	if (checkForm(true)){
		doc = document.all;
		mintotal = parseFloat(doc["costcentremintotal"].value,10);
		amount = parseFloat(doc["txt_CostCentreAllocation"].value,10);			
		if (amount < mintotal){
			event.cancelBubble = true;
			alert("The value you have entered for Amount is less than the the total funds already allocated to Heads.\nTherefore the value will be set to the lowest value it can be.");
			doc["txt_CostCentreAllocation"].value = FormatCurrency (mintotal);
			return false;
			}
		}
	}
	
	function SetEmployeeLimits(){
		window.showModalDialog("Popups/SetEmployeeLimits.asp?EXID=" + thisForm.EXID.value + "&rand=" + new Date(), "Expenditures", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
		}

	function EditExpend(){
		var theURL = "Popups/AmendScheme.asp?HDID=" + document.thisForm.HDID.value + "&stDate="+ document.thisForm.txt_dtStart.value + "&enDate="+ document.thisForm.txt_endDate.value+ "&tv="+ document.thisForm.txt_totValue.value  + "&rand=" + new Date() ;
		
		window.showModalDialog(theURL, "Expenditures", "dialogHeight: 575px; dialogWidth: 420px; status: No; resizable: No;");
	}
	function getOB()
	{
		
		var xh=new ActiveXObject("Microsoft.XMLHTTP");
		var xmlobj =new ActiveXObject("Microsoft.XMLDOM")
		var t=thisForm.txt_OPBalance.value
		if (t=="") t=0
		var url="xmlsrv.asp?id=" + thisForm.txt_nominalcode.value + "&task=DVB"+ "&nop=" + t + "&HDID=" + thisForm.HDID.value;
		
		var nodeMap
		xh.open("POST",url,false);
		xh.send();
		xmlobj=xh.responseXML;
		
		xmlobj.async = false;
		
		var t=xmlobj.selectSingleNode("//rs:data/z:row");
		
		var balance=t.getAttribute("BALANCE")
		
		if (balance < 0) 
				{
					alert("Balance will exceed Opening Balance for associated Nominal Account")
					thisForm.txt_OPBalance.value=thisForm.hdn_OPBalance.value
				}
	}
</script>

<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(2);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopSpecial.asp" -->
  
  <TR> 
    <TD HEIGHT=100% STYLE='BORDER-LEFT:1PX SOLID #7E729E' width=300> 
      <table cellspacing=0 cellpadding=0 height=100% width=100%>
        <tr> 
          <td style='border-top:1px solid #FFFFFF;border-right:1px solid #7E729E;' height=100% width=100% valign=top nowrap> 
            <iframe name=theSideBar src="ServerSide/FinanceBuilderDev_svr.asp" id=theSideBar height=100% frameborder=none bordercolor=#FFFFFF></iframe> 
          </td>
        </tr>
        <tr> 
          <TD height=1 BGCOLOR=#FFFFFF align=right valign=middle></TD>
        </tr>
        <tr> 
          <TD height=28 BGCOLOR=#133e71 align=right valign=middle><b>&nbsp;</b></TD>
        </tr>
      </table>
    </TD>
    <td HEIGHT=100% width=476> 
      <form name=thisForm method=post>
        <table cellspacing=0 cellpadding=0 height=100% width=100%>
          <tr> 
            <td width=100% colspan=2 STYLE='BORDER-RIGHT:1PX SOLID #7E729E' align=center valign=top class="RSLBlack"> 
              <br>
              <div id=everything style='height:370px;'> <font color=blue> 
                <div id=TheText></div>
                </font> 
                <div id=CostCentre style='display:none'> 
				
                  <table class="RSLBlack">
                    <tr> 
                      <td>Cost Centre</td>
                      <td> 
                        <input type=text name=txt_CostCentreDescription style='width:260px' maxlength=30 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_CostCentreDescription"></td>
                    </tr>
                    <tr> 
                      <td>Department</td>
                      <td> 
                        <input type=text name=txt_Department style='width:260px' maxlength=45 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_Department"></td>					  
                    </tr>
                    <td>Budget Holder</td>
                      <td> 
                        <input type=text name=txt_AccountableBody style='width:260px' maxlength=45 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_AccountableBody"></td>					  
                    </tr>
                    <tr> 
                      <td>Date Commence</td>
                      <td> 
                        <input type=text name=txt_DateStart style='width:260px' maxlength=10 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_DateStart"></td>					  
                    </tr>
                    <tr> 
                      <td>Date Ceases</td>
                      <td> 
                        <input type=text name=txt_DateEnd style='width:260px' maxlength=10 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_DateEnd"></td>					  
                    </tr>
                    <tr> 
                      <td>Amount (�)</td>
                      <td> 
                        <input type=text name=txt_CostCentreAllocation style='width:260px;text-Align:right' maxlength=10 class="RSLBlack" onblur=checkAllocation3() onfocus=alignLeft()>
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_CostCentreAllocation"></td>					  					  
                    </tr>
                    <tr> 
                      <td>Cost Centre Active</td>
                      <td class="RSLBlack">Yes 
                        <input type=radio name=rdo_CostCentreActive value=1 class="RSLBlack" checked>
                        &nbsp;&nbsp;No 
                        <input type=radio name=rdo_CostCentreActive value=0 class="RSLBlack">
                      </td>
					  <td><img src="/js/FVS.gif" height=15 width=15 name="img_CostCentreActive	"></td>					  					  					  
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <input type=hidden name=costcentremintotal>
                        <input type=hidden name=CC_A>
                        <input type=hidden name=CCID>
                        <input type=button name=saveButton value=' Save ' class="RSLButton" onclick="DoSave('COSTCENTRE')">
                        &nbsp; 
                        <input type=button name=resetButton value=' Reset ' class="RSLButton" onclick="DoReset('COSTCENTRE')">
                        &nbsp; 
                        <input type=button name=cancelButton value=' Cancel ' class="RSLButton" onclick="DoCancel()">
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <div id=COSTCENTRE_DELETE> 
                          <input type=button name="FDG" value=' Delete ' class="RSLButton" onClick="DoDelete('COSTCENTRE')">
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div id=Head style='display:none'>
                <table class="RSLBlack" style='border-collapse:collapse' cellpadding=2>
                  <tr bgcolor=beige> 
                    <td style='border-top:1px solid black;border-left:1px solid black;'>Head 
                      Name</td>
                    <td style='border-top:1px solid black;border-right:1px solid black;'> 
					  <div name="theHeadName" id="theHeadName"></div>
                      <input type=text name=txt_HeadDescription style='width:210px;Border:1px solid black; ' maxlength=30 class="RSLBlack" readonly>
                    </td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_HeadDescription"></td>
                  </tr>
                  <tr bgcolor=beige> 
                    <td style='border-left:1px solid black;'>Head Allocation</td>
                    <td style='border-right:1px solid black;'> 
					  <div name="theTotalAllocated" id="theTotalAllocated"></div>
                      <input type=text name=txt_headallocation style='width:210px;text-Align:right;Border:1px solid black;' maxlength=10 class="RSLBlack" onblur="checkAllocation()" readonly> 
                    </td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_headallocation"></td>
                  </tr>
                  <tr bgcolor=beige> 
                    <td style='border-bottom:1px solid black;border-left:1px solid black;'>Head 
                      Active</td>
                    <td class="RSLBlack" style='border-bottom:1px solid black;border-right:1px solid black;'>Yes 
                      <input type=radio name=rdo_headactive value=1 class="RSLBlack" checked>
                      &nbsp;&nbsp;No 
                      <input type=radio name=rdo_headactive value=0 class="RSLBlack">
                    </td>
				 </tr>
				 <tr bgcolor=beige>
					<td style='border-left:1px solid black;'>Start Date</td>
					<td style='border-right:1px solid black;'>
					  <div name="dtStart" id="dtStart"></div>
                      <input type=text name=txt_dtStart style='width:100px;Border:1px solid black;' maxlength=12 class="RSLBlack" readonly> 
					</td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_dtStart"></td>
                 </tr>
				 <tr bgcolor=beige>
					<td style='border-bottom:1px solid black;border-left:1px solid black;'>End Date</td>
					<td style='border-right:1px solid black;border-bottom:1px solid black;'>
					  <div name="endDate" id="endDate"></div>
                      <input type=text name=txt_endDate style='width:100px;Border:1px solid black;' maxlength=12 class="RSLBlack" readonly > 
					</td>
                    <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_endDate"></td>
                 </tr bgcolor=beige>
				 
				 <!--<tr  bgcolor=beige>
					<td style='border-bottom:1px solid black;border-left:1px solid black;'>Total Value</td>
					<td style='border-right:1px solid black;border-bottom:1px solid black;'>
					  <div name="totValue" id="totValue"></div>
                      <input type=hidden name=txt_totValue style='width:100px;text-Align:right;Border:1px solid black;' maxlength=10 class="RSLBlack" readonly > 
					</td>
                    <td  bgcolor=white><img src="/js/FVS.gif" height=15 width=15 name="img_totValue"></td>
                 </tr>-->

                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td><u><b>Budget Summary</b></u></td>
                  </tr>
                  <tr style='display:none'> 
                    <td>Date Created</td>
                    <td colspan=2> 
                      <input type=text readonly name=headcreated maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px'>
                    </td>
                  </tr>
                  <tr> 
                    <td>Cost Centre Budget</td>
                    <td colspan=1 align=right> 
                      <input type=text name=totalfund maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right' readonly>
                    </td>
                  </tr>
                  <tr> 
                    <td>Other Budgets Allocated</td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=totalallocated maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
                  <tr> 
                    <td>Budget Remaining</td>
                    <td colspan=1 align=right> 
                      <input type=text readonly name=totalremaining maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                    </td>
                  </tr>
                  <tr> 
                    <td>&nbsp;</td>
                    <td align=right colspan=1> 
                      <input type=button name=AmendExpen value=' Amend  Expenditures' class="RSLButton" onClick="EditExpend()">
                      <input type=hidden name=mintotal>
                      <input type=hidden name=HD_A>
                      <input type=hidden name=HDID>
                      <div style="display:none;"> 
                        <input type=button name=saveButton value=' Save ' class="RSLButton" onclick="DoSave('HEAD')" style="display:none;">
                        &nbsp; 
                        <input type=button name=resetButton value=' Reset ' class="RSLButton" onclick="DoReset('HEAD')" style="visibility:hide">
                        &nbsp; 
                        <input type=button name=cancelButton value=' Cancel ' class="RSLButton" onclick="DoCancel()" >
                      </div>
                    </td>
                  </tr>
                  <tr> 
                    <td>&nbsp;</td>
                    <td align=right colspan=1> 
                      <div id=HEAD_DELETE> 
                        <input type=button name="HDG" value=' Delete ' class="RSLButton" onClick="DoDelete('HEAD')" style="display:none;">
                      </div>
                    </td>
                  </tr>
                </table>
                </div>
                <div id=Budget style='display:none'> 
                  <table class="RSLBlack" style='border-collapse:collapse' cellpadding=2>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;border-top:1px solid black'>Expenditure Name</td>
                      <td style='border-right:1px solid black;border-top:1px solid black'> 
                        <input type=text name=txt_expenditurename style='width:210px' maxlength=30 class="RSLBlack" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expenditurename"></td>					  					  
                    </tr>
                    <tr bgcolor=beige> 
                      <td style='border-left:1px solid black;'>Expenditure Allocation</td>
                      <td style='border-right:1px solid black;'> 
                        <input type=text name=txt_expenditureallocation style='width:210px;text-Align:right' maxlength=10 class="RSLBlack" onblur="checkAllocation2()" readonly>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_expenditureallocation"></td>					  
                    </tr>
					<tr bgcolor=beige> 
                      <td style='border-left:1px solid black;'>Opening Balance</td>
                      <td style='border-right:1px solid black;'> 
                        <input type=text name=txt_OPBalance style='width:210px;text-Align:right' maxlength=10 class="RSLBlack" onBlur=getOB()>
						<input type=hidden name=hdn_OPBalance>
                      </td>
					  <td bgcolor=#FFFFFF><img src="/js/FVS.gif" height=15 width=15 name="img_OPBalance"></td>					  
                    </tr>
                    <tr bgcolor=beige> 
                      
                    <td style='border-left:1px solid black;border-bottom:1px solid black'>Expenditure Active</td>
                      <td class="RSLBlack" style='border-right:1px solid black;border-bottom:1px solid black'>Yes 
                        <input type=radio name=budgetactive value=1 class="RSLBlack" checked>
                        &nbsp;&nbsp;No 
                        <input type=radio name=budgetactive value=0 class="RSLBlack">
                      </td>
                    </tr>
                    <tr style='display:none'> 
                      <td>Date Created</td>
                      <td> 
                        <input type=text readonly name=budgetcreated maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px'>
                      </td>
                    </tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><u><b>Budget Summary</b></u></td></tr>					
                    <tr> 
                      <td>Total Development Cost</td>
                      <td> 
                        <input type=text readonly name=Btotalfund maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                      </td>
                    </tr>
                    <tr> 
                      <td>Expenditures Allocated</td>
                      <td> 
                        <input type=text readonly name=Btotalallocated maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                      </td>
                    </tr>
                    <tr> 
                      <td>Budget Remaining</td>
                      <td> 
                        <input type=text readonly name=Btotalremaining maxlength=30 class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
                      </td>
                    </tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><u><b>Accounts Interfacing</b></u></td></tr>					
                    <tr> 
                      <td>Nominal Code</td>
                      <td> 
                        <input type=text name=txt_nominalcode READONLY class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
						<img src="/js/FVS.gif" height=15 width=15 name="img_nominalcode">
                      </td>
                    </tr>
                    <tr> 
                      <td>Control Account</td>
                      <td> 
                        <input type=text name=txt_controlcode READONLY class="RSLBlack" style='border:1px solid black;background-color:beige;width:210px;text-Align:right'>
						<img src="/js/FVS.gif" height=15 width=15 name="img_controlcode">
                      </td>
                    </tr>					
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <input type=hidden name=Bmintotal>					  					  
                        <input type=hidden name=EX_A>
                        <input type=hidden name=EXID>
						<input type=hidden name=hid_nominalcode>
						<input type=hidden name=hid_controlcode>
                        <input type=button name=saveButton value=' Save ' class="RSLButton" onclick="DoSave('EXPENDITURE')" style="display:none;">
						<input type=hidden name=txt_totValue style='width:100px;text-Align:right;Border:1px solid black;' maxlength=10 class="RSLBlack" readonly > 
                        &nbsp; 
                        <input type=button name=resetButton value=' Reset ' class="RSLButton" onclick="DoReset('EXPENDITURE')" style="display:none;">
                        &nbsp; 
                        <input type=button name=cancelButton value=' Cancel ' class="RSLButton" onclick="DoCancel()" style="display:none;">
                          <input type=button name="BDGEE" value=' Set Limits ' class="RSLButton" onClick="SetEmployeeLimits()">						
						<img src="/js/FVS.gif" height=15 width=15 name="img_dummy">						
                      </td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td align=right> 
                        <div id=EXPENDITURE_DELETE> 
                          <input type=button name="BDG" value=' Delete ' class="RSLButton" onClick="DoDelete('EXPENDITURE')" style="display:none;">
						  <img src="/js/FVS.gif" height=15 width=15 name="img_dummy">												  
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </td>
          </tr>
          <tr> 
            <TD HEIGHT=44 width=100% align=right>&nbsp;</TD>
            <td rowspan=2><IMG SRC="/myImages/My113.gif" WIDTH=72 HEIGHT=72></td>
          </tr>
          <TR> 
            <TD height=28 BGCOLOR=#133e71 align=right class="RSLWhite" valign=middle><b>rslManager 
              is a Reidmark eBusiness System</b></TD>
          </TR>
        </table>
      </form>
    </TD>
  </TR>
</TABLE>
<iframe  src="/secureframe.asp" name=FB style='display:none;width:800px;height:500px'></iframe> 
<!-- #include virtual="Includes/Bottoms/BlankBottom.html" -->
</body>
</html>

