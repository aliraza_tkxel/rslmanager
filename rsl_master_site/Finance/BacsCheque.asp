<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	
	CONST CONST_PAGESIZE = 20
	OpenDB()
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim TotalSum, TotalCount, clist, csql, ccnt, ctotal, view

	TotalSum = 0
	view = 2
	If Request("VIEW") <> "" Then view = Request("VIEW") End If
	
    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "


	clist = ""
	If Request("CreditList") <> "" Then
		clist = Request("CreditList")
		csql = " OR (INV.INVOICEID IN ("&clist&") ) " 
	End If	
	
	ccnt = 0
	If Request("CreditCount") <> "" Then ccnt = clng(Request("CreditCount")) End If
	
	ctotal = 0
	If Request("CreditTotal") <> "" Then ctotal = cdbl(Request("CreditTotal")) End If
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
'	SET ORDER BY
	Dim orderBy
	OrderByMatched = 0
	orderBy = " NAME ASC "
	Call SetSort()

'	SET PO FILTER	
	POFilter = ""
	if (Request("PODate") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("PODate")),2)
		RequiredDate = CDate(Request("PODate"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if
	ORIGINATORNUMBER = "658670"
	getData()
	Call CloseDB()
	
	' CHECK TO SEE IF CURRENT CHECKBOX HAS BEEN PREVIOUSLY CHECKED
	Function isChecked(invoiceID)
	
		If clist <> "" Then
			Dim arrChk
			arrChk = Split(clist, ",")
			For Each item In arrChk 
				If CLng(item) = CLng(invoiceID) Then 
					isChecked = True 
					Exit Function
				End If
			Next
			isChecked = False	
		End If
	
	End Function
	
	Function getData()
		
		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
		if (Request("VIEW") = 1) then
		SQLCODE = "SELECT INV.SUPPLIERID, '' AS EMPTY, ISNULL(CRS,0) AS NOTECOUNT, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, NAME, ORGID,  " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(INV.INVOICEID) AS TOTALINVOICES, " &_
					"PB.PROCESSDATE AS INPUTBOX, " &_
					"'' AS CHECKBOX " &_				
					"FROM F_INVOICE INV " &_
					"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
					"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
					"LEFT JOIN (SELECT COUNT(*)AS CRS, SUPPLIERID FROM F_INVOICE IV " &_
					"			INNER JOIN F_POBACS P ON P.INVOICEID = IV.INVOICEID " &_
					"			WHERE IV.ISCREDIT = 1 " &_
					"			AND CONVERT(DATETIME,CONVERT(VARCHAR,P.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " &_
					"			GROUP BY SUPPLIERID) CR ON CR.SUPPLIERID = INV.SUPPLIERID " &_
					"	WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD = 8 AND CONVERT(DATETIME,CONVERT(VARCHAR,PB.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " & CompanyFilter &_
					" 	GROUP BY INV.SUPPLIERID, NAME, ORGID, PB.PROCESSDATE, CRS " &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""
		else
			SQLCODE = "SELECT INV.SUPPLIERID, '' AS EMPTY, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, NAME, ORGID,  " &_
					"		'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(INV.INVOICEID) AS TOTALINVOICES, " &_
					"		'<input type=""text"" name=""txt_TEXT' + CAST(ORGID AS VARCHAR) + '"" value="""" CLASS=""TEXTBOX200"" MAXLENGTH=20><img src=""/js/FVS.gif"" width=15 height=15 name=""img_TEXT' + CAST(ORGID AS VARCHAR) + '"">' AS INPUTBOX, " &_

					"		'<input type=""HIDDEN"" name=""hid_CO' + CAST(ORGID AS VARCHAR) + '"" value=""' + CAST(COUNT(INV.INVOICEID) AS VARCHAR) + '""><input type=""HIDDEN"" name=""hid_VA' + CAST(ORGID AS VARCHAR) + '"" value=""' + CAST(ISNULL(SUM(GROSSCOST),0) AS VARCHAR) + '""><input type=""CHECKBOX"" name=""CHECKITEMS"" value=""' + CAST(ORGID AS VARCHAR) + '"">' AS CHECKBOX, " &_				
					"		ISNULL(CREDITNOTECOUNT.CR, 0) AS NOTECOUNT " &_
					"FROM 	F_INVOICE INV " &_
					"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
					"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
					"		LEFT JOIN " &_
					"		    (SELECT ISNULL(COUNT(*),0) AS CR, SUPPLIERID FROM F_CREDITNOTE C " &_
					"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID	" &_
					"			INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = CP.ORDERITEMID GROUP BY SUPPLIERID) CREDITNOTECOUNT " &_
					"		    ON CREDITNOTECOUNT.SUPPLIERID = INV.SUPPLIERID " &_
					"WHERE 	(INV.ISCREDIT = 0 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND " &_
					"		PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 8 AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), " &_
					"		INV.TAXDATE) <= '" & FormatDateTime(RequiredDate,1) & "') " & csql & CompanyFilter &_
					"GROUP 	BY INV.SUPPLIERID, NAME, ORGID, CREDITNOTECOUNT.CR " &_
					"ORDER 	BY " + Replace(orderBy, "'", "''") + "" 
		end if	
		
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = SQLCODE
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()
		
		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 
		
		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If
	
		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If
		
		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<TBODY CLASS='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				
				If Cint(rsSet("NOTECOUNT")) > 0 Then
					str_data = str_data & "<TR bgcolor='whitesmoke' TITLE='Click to attach credit note' STYLE='CURSOR:HAND' ONCLICK=""toggle('SUPP"&rsSet("SUPPLIERID")&"')"">"
				Else
					str_data = str_data & "<TR>"
				End If
				str_data = str_data & "<TD WIDTH=250PX>" & rsSet("NAME") & "</TD>" &_
						"<TD WIDTH=100PX align=right><DIV ID='AMOUNT"&rsSet("SUPPLIERID")&"'>" & FormatNumber(rsSet("PAYMENTAMOUNT")) &	"</DIV></TD>" &_
						"<TD ALIGN=CENTER WIDTH=40PX>" & rsSet("TOTALINVOICES") & "</TD>"
						if (Request("VIEW") = 1) then
							str_data = str_data & "<TD WIDTH=250PX onclick=""CancelBubble()"" align=RIGHT>" & rsSet("INPUTBOX") & "</TD>" 
						Else
							str_data = str_data & "<TD WIDTH=250PX onclick=""CancelBubble()"" align=left>" & rsSet("INPUTBOX") & "</TD>" 
						End If
						str_data = str_data & "<TD onclick=""CancelBubble()"" align=center>"  & rsSet("CHECKBOX") & "</TD></TR>" 
						
				count = count + 1
				If Cint(Request("VIEW")) = 1 Then
					If Cint(rsSet("NOTECOUNT")) > 0 Then 
						str_data = str_data & check_for_readonly_creditnotes(rsSet("SUPPLIERID")) 
					End If
				Else
					If Cint(rsSet("NOTECOUNT")) > 0 Then 
						str_data = str_data & check_for_creditnotes(rsSet("SUPPLIERID")) 
					End If
				End If
				TotalSum = TotalSum + rsSet("PAYMENTAMOUNT")
				rsSet.movenext()
				If rsSet.EOF Then Exit for
					
			Next
			
			str_data = str_data & "</TBODY>"
			
			'ensure table height is consistent with any amount of records
			fill_gaps()
			
			' links
			str_data = str_data &_
			"<TFOOT><TR><TD COLSPAN=7 STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
			"<A style='cursor:hand' onclick=""gotopage(1)""><b><font color=BLUE>First</font></b></a> " &_
			"<A style='cursor:hand' onclick=""gotopage(" & prevpage & ")""><b><font color=BLUE>Prev</font></b></a>" &_
			" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & (intRecordCount + 1) &_
			" <A style='cursor:hand' onclick=""gotopage(" & nextpage & ")""><b><font color=BLUE>Next</font></b></a>" &_ 
			" <A style='cursor:hand' onclick=""gotopage(" & intPageCount & ")""><b><font color=BLUE>Last</font></b></a>" &_
			"</TD></TR></TFOOT>"

		End If
	
		If intRecord = 0 Then 
			str_data = "<TR><TD COLSPAN=7 ALIGN=CENTER><B>No invoices found in the system for the specified criteria!!!</B></TD></TR>" &_
						"<TR STYLE='HEIGHT:3PX'><TD></TD></TR>"
		End If
		
		rsSet.close()
		Set rsSet = Nothing
		
	End function

'	THIS FUNCTION CHECKS IF THERE ARE ANY CREDIT NOTES AVAILABLE FOR THE SPECIFIED SUPPLIER
'	ONLY CHECKS FOR CREDIT NOTES WITH A STATUS OF 6 -- RECEIVED
	Function check_for_creditnotes(SID)
	
		Dim str_CData, counter
		str_CData = ""
		counter = 0
		SQL = "SELECT 	INV.INVOICEID, INV.SUPPLIERID, ISNULL(SUM(CR.GROSSCOST),0) AS GROSSCOST, " &_
				"		INV.INVOICENUMBER AS CRNO, CR.CNNAME   " &_
				"FROM 	F_INVOICE INV   " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID   " &_
				"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID   " &_
				"		INNER JOIN  " &_
				"		    (SELECT SUM(GROSSCOST) AS GROSSCOST, O.INVOICEID, C.CNNAME " &_
				"			FROM F_CREDITNOTE C " &_
				"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CP.ORDERITEMID   " &_
				"			INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = PI.ORDERITEMID " &_
				"			 WHERE PI.ACTIVE = 1 GROUP BY O.INVOICEID, C.CNNAME ) CR ON  " &_
				"			 CR.INVOICEID = INV.INVOICEID " &_
				"	WHERE 	INV.ISCREDIT = 1 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND   " &_
				"		S.PAYMENTTYPE = 8 AND INV.SUPPLIERID = " & SID & CompanyFilter &_
				"	GROUP 	BY INV.SUPPLIERID, INV.INVOICENUMBER, INV.INVOICEID, CR.CNNAME " &_
				"	ORDER BY INV.INVOICEID " 
				
		Call OpenRs(rsCSet, SQL)
		While Not rsCSet.EOF 
			
			if counter = 0 Then
				str_CData = str_CData & "<THEAD>"
			End If

			counter = counter + 1
			str_CData = str_CData & "<TR bgcolor=beige STYLE='DISPLAY:block' ID='SUPP"&rsCSet("SUPPLIERID")&"'>" &_
					"<TD>" & rsCSet("CRNO") & "</TD>" &_
					"<TD align=RIGHT>" & FormatNumber(abs(rsCSet("GROSSCOST"))) &"</TD><td></td><td></td>"
					if isChecked(rsCSet("INVOICEID")) Then
						str_CData = str_CData & "<TD ALIGN=CENTER><input type='checkbox' name='credits' id='"&rsCSet("INVOICEID")&"' value='"&rsCSet("INVOICEID")&"' onclick=""calculate_me('AMOUNT"&rsCSet("SUPPLIERID")&"', "&abs(rsCSet("GROSSCOST"))&", '"&rsCSet("INVOICEID")&"')"" checked></TD></TR>" 
					Else
						str_CData = str_CData & "<TD ALIGN=CENTER><input type='checkbox' name='credits' id='"&rsCSet("INVOICEID")&"' value='"&rsCSet("INVOICEID")&"' onclick=""calculate_me('AMOUNT"&rsCSet("SUPPLIERID")&"', "&abs(rsCSet("GROSSCOST"))&", '"&rsCSet("INVOICEID")&"')""></TD></TR>" 
					End If		
			rsCSet.movenext()
		Wend
		str_CData = str_CData & "</THEAD>"
		if counter = 0 Then str_CData = "" End If
		CloseRs(rsCSet)
		check_for_creditnotes = str_CData

	End Function

'	THIS FUNCTION CHECKS IF THERE ARE ANY CREDIT NOTES AVAILABLE FOR THE SPECIFIED SUPPLIER
'	ONLY CHECKS FOR CREDIT NOTES WITH A STATUS OF 6 -- RECEIVED
	Function check_for_readonly_creditnotes(SID)
	
		Dim str_CData, counter
		str_CData = ""
		counter = 0
		SQL = "SELECT 	INV.INVOICEID, INV.SUPPLIERID, ISNULL(SUM(CR.GROSSCOST),0) AS GROSSCOST, " &_
				"		INV.INVOICENUMBER AS CRNO, CR.CNNAME   " &_
				"FROM 	F_INVOICE INV   " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID   " &_
				"		INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID   " &_
				"		INNER JOIN  " &_
				"		    (SELECT SUM(GROSSCOST) AS GROSSCOST, O.INVOICEID, C.CNNAME " &_
				"			FROM F_CREDITNOTE C " &_
				"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CP.ORDERITEMID   " &_
				"			INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = PI.ORDERITEMID " &_
				"			 WHERE PI.ACTIVE = 1 GROUP BY O.INVOICEID, C.CNNAME ) CR ON  " &_
				"			 CR.INVOICEID = INV.INVOICEID " &_
				"	WHERE 	INV.ISCREDIT = 1 AND INV.CONFIRMED = 1 AND CONVERT(DATETIME,CONVERT(VARCHAR,PB.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " &_
				"		AND S.PAYMENTTYPE = 8 AND INV.SUPPLIERID = " & SID & CompanyFilter &_
				"	GROUP 	BY INV.SUPPLIERID, INV.INVOICENUMBER, INV.INVOICEID, CR.CNNAME " &_
				"	ORDER BY INV.INVOICEID " 
				
		Call OpenRs(rsCSet, SQL)
		While Not rsCSet.EOF 
			
			if counter = 0 Then
				str_CData = str_CData & "<THEAD>"
			End If

			counter = counter + 1
			str_CData = str_CData & "<TR bgcolor=beige STYLE='DISPLAY:BLOCK' ID='SUPP"&rsCSet("SUPPLIERID")&"'>" &_
					"<TD>&nbsp;&nbsp;&nbsp;&nbsp;" & rsCSet("CRNO") & "</TD>" &_
					"<TD align=right>" & FormatNumber(abs(rsCSet("GROSSCOST"))) &"&nbsp;</TD><TD colspan=3></TD>"
			rsCSet.movenext()
		Wend
		str_CData = str_CData & "</THEAD>"
		if counter = 0 Then str_CData = "" End If
		CloseRs(rsCSet)
		check_for_readonly_creditnotes = str_CData

	End Function

	// pads table out to keep the height consistent
	Function fill_gaps()
	
		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<TR><TD COLSPAN=7 ALIGN=CENTER>&nbsp;</TD></TR>"
			cnt = cnt + 1
		wend		
	
	End Function
	
	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bacs List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var credit_total = <%=ctotal%>
	var credit_count = <%=ccnt%>
	var checked_list = "<%=clist%>"
		
	function gotopage(pageno){
	
		location.href = "bacsDD.asp?VIEW=<%=VIEW%>&PODate=<%=Request("PODate")%>&CreditTotal="+credit_total+"&CreditCount="+credit_count+"&page="+pageno+"&CreditList="+checked_list + "&Company=<%=Company%>";
	}	

//	OPEN OR CLOSE CREDIT NOTE ROWS
	function toggle(what){
	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}
	
	// REMOVE UNCHECKED ITEM FROM ITEM LIST
	// THIS LIST IS USED TO PASS CHECKED ITEMS TO NEXT PAGE
	function remove_item_from_list(chk){
		
		var strChk = new String(chk);
		var arrChk = checked_list.split(",")
		var lenChk = arrChk.length
		checked_list = ""
		var cnt = 0

		for (i=0 ; i < lenChk ; i++)
			if (arrChk[i] != strChk){
				cnt +=1;
				if (parseInt(cnt) == 1)
					checked_list = checked_list + arrChk[i];
				else
					checked_list = checked_list + "," + arrChk[i];
			}
	}
	
	// THIS FUNCTION IS CALLED IF A CREDIT NOTE CHECKBOX IS CHECKED. IT CALCULATES AND UPDATES THE PAYMENY AMOUNT
	// IT ALSO CONTROLS THE CONTENT OF THE CHECKED LIST WHICH IS USED TO PASS THE IDS OF CHECKED CREDIT NOTE
	// BOXES FROM PAGE TO PAGE
	function calculate_me(supplierid, rowcost, chkbox){
	
		var payment_amount, credit_amount, result
		
		// first convert currency number to real number
		payment_amount = new String(document.getElementById(""+supplierid+"").innerHTML)
		payment_amount = parseFloat(payment_amount.replace(",",""));
		credit_amount	= parseFloat(rowcost);

		// WE EITHER ADD THE AMOUNT OR DEDUCT DEPENDING ON WHETHER THE CHECKBOX IS
		// CHECKED OR UNCHECKED
		if (document.getElementById(""+chkbox+"").checked == true){
			result	= FormatCurrency(payment_amount - credit_amount);
			credit_total = parseFloat(Math.abs(credit_total)) + parseFloat(Math.abs(credit_amount));
			credit_count += 1
			if (credit_count == 1)
				checked_list = checked_list + chkbox;
			else
				checked_list = checked_list + "," + chkbox;
			}
		else {
			result	= FormatCurrency(payment_amount + credit_amount);
			credit_total = parseFloat(Math.abs(credit_total)) - parseFloat(Math.abs(credit_amount));
			credit_count -= 1
			remove_item_from_list(chkbox);
			} 

		// WE CANNOT ALLOW USER TO CONTINUE IF PAYMENT AMOUNT BECOMES NEGATIVE AND MUST RESET THE VALUE
		if (result < 0){
			alert("You cannot apply this credit note otherwise the payment will become negative.");
			document.getElementById(""+chkbox+"").checked = false;
			credit_total = parseFloat(Math.abs(credit_total)) - parseFloat(Math.abs(credit_amount));
			credit_count -= 1
			remove_item_from_list(chkbox);
			return false;
			}
		else {
			document.getElementById(""+supplierid+"").innerHTML = result;
			}
	}
	
	function RemoveBad() { 
	
		strTemp = event.srcElement.value;
		strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
		event.srcElement.value = strTemp;
	}
	
	function CancelBubble(){
	event.cancelBubble = true
	}

	var FormFields = new Array()

	function SendCheque() {
	
		chkitems = document.getElementsByName("CHECKITEMS")
		FormFields.length = 0
		iCounter = 0
		TotalValue = 0
		TotalCount = 0
		if (chkitems.length){
			for (j=0; j<chkitems.length; j++) {
				ManualError ("img_TEXT" + chkitems[j].value, "", 3)
				}
		for (i=0; i<chkitems.length; i++) {
			if (chkitems[i].checked) {
				FormFields[iCounter] = "txt_TEXT" + chkitems[i].value + "|Cheque Number|TEXT|Y"
				TotalCount += parseInt(document.getElementById("hid_CO" + chkitems[i].value).value)
				TotalValue += parseFloat(document.getElementById("hid_VA" + chkitems[i].value).value)
				iCounter ++
					}
				}
			}
		if (TotalCount == 0) {
			alert("No lines have been selected. To select a line click on the\nrespective checkbox and enter a date for when the DD was taken,\notherwise the row will not be processed.")
			return false
			} 
		TotalCount += credit_count;
		TotalValue = TotalValue - credit_total;
		if (!checkForm()) return false
		result = confirm("You have selected '" + TotalCount + "' invoice(s) to be processed at a total value of '�" + FormatCurrency(TotalValue) +"'.\nDo you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")
		if (!result) return false
		thisForm.hid_CreditList.value = checked_list;
		thisForm.action = "ServerSide/BacsList_Cheque.asp?pROCESSINGdATE=<%=Request("PODate")%>&Company=<%=Company%>"
		thisForm.submit()
	}
		
	<% if (Request("ER14" & Replace(Date, "/", "")) = 1) then %>
		alert("The number of invoices which are ready to be paid has changed.\nTherefore some rows will not have been processed, \nre-check these rows and then reprocess.")
	<%
	 end if 
	%>	
	
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table width=750 class="RSLBlack"><tr>
<td> <b>&nbsp;Invoice Cheque List </b>
<input type=HIDDEN name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>">
</td>
<td align=right>
&nbsp<a href='BacsDisplay.asp?date=<%=ProcessingDate%>&Company=<%=Company %>'><font color=blue><b>BACK to Bacs Calendar</b></font></a>
</td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=0 CLASS='TAB_TABLE' STYLE='BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor=STEELBLUE BORDER=1>
	<THEAD>
	<TR><TD CLASS='TABLE_HEAD' WIDTH="250"><a href="BacsDD.asp?PODATE=<%=Request("PODate")%>&CC_Sort=NAME+ASC&Company=<%=Company %>" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Name&nbsp;<a href="BacsList.asp?PODATE=<%=Request("PODate")%>&CC_Sort=NAME+DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="100"><a href="BacsDD.asp?PODATE=<%=Request("PODate")%>5&CC_Sort=ACCOUNTNAME+ASC&Company=<%=Company %>" style="text-decoration:none"><img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending"></a>&nbsp;Amount&nbsp;<a href="BacsList.asp?PODATE=<%=Request("PODate")%>&CC_Sort=ACCOUNTNAME+DESC" style="text-decoration:none"><img src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending"></a></TD>
	<TD CLASS='TABLE_HEAD' WIDTH="40">&nbsp;Invoices&nbsp;</TD>
	<TD CLASS='TABLE_HEAD' WIDTH="250">&nbsp;Cheque Number&nbsp;</TD>
	<TD CLASS='TABLE_HEAD' WIDTH="50">&nbsp;</TD></TR>
	<TR STYLE='HEIGHT:3PX'><TD COLSPAN=6 ALIGN='CENTER' STYLE='BORDER-bottom:2PX SOLID #133E71' CLASS='TABLE_HEAD'></TD></TR>
	</THEAD>
	<%=str_data%>
</TABLE>
<% if (Request("VIEW") <> 1) then %>
<table width=750><tr><td align=right>&nbsp;<input class="RSLButton" type=button onclick="SendCheque()" value=" AUTHORISE "></td></tr></table>
<% END IF %>
	<input type="hidden" name="hid_CreditSum">
	<input type="hidden" name="hid_CreditCount">
	<input type="hidden" name="hid_CreditList">
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>