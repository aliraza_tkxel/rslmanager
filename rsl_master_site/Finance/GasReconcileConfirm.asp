<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
L_BATCHID = 0
IF(Request("BATCHID") <> "" AND ISNUMERIC(Request("BATCHID"))) THEN L_BATCHID = Request("BATCHID")
%>
<%
OpenDB()

EmployeeLimit = 0

' 
' THIS QUERY CAN RETURN MORE THAN ONE ROWS IF ORDERS ARE DIFFERENT
' EXAMPLE: WE CAN HAVE 3 ROWS OF BATCH ID 2

SQL=" SELECT BATCHID,PW.ORDERID,PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE "&_
	" FROM GS_INVOICE INV "&_
	"    LEFT JOIN C_REPAIR CR ON CR.REPAIRHISTORYID=INV.REPAIRHISTORYID "&_
	"    LEFT JOIN P_WOTOREPAIR PWR ON PWR.JOURNALID=CR.JOURNALID "&_
	"    LEFT JOIN P_WORKORDER PW ON PW.WOID=PWR.WOID "&_
	"    LEFT JOIN F_PURCHASEORDER PO ON PO.ORDERID=PW.ORDERID "&_
 	"    INNER JOIN S_ORGANISATION S ON S.ORGID = PO.SUPPLIERID "&_
   	"    LEFT JOIN P_WORKORDERTOPATCH PWP ON PWP.WOID=PW.WOID "&_	
	"    LEFT JOIN PDR_DEVELOPMENT D ON D.PATCHID=PWP.PATCHID "&_
	"         AND (D.DEVELOPMENTID = PW.DEVELOPMENTID 	OR PW.DEVELOPMENTID IS NULL)	"&_
	"   INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS "&_
    " WHERE BATCHID=" & L_BATCHID &_
    " GROUP BY BATCHID,PW.ORDERID,PO.PONAME, PO.SUPPLIERID, S.NAME, PS.POSTATUSNAME, PODATE "
		  
		
Call OpenRs(rsDR, SQL)
if (NOT rsDR.EOF) then
    while (NOT rsDR.EOF) 

            BATCH_ORDERID = rsDR("ORDERID") 
   	        BATCHID=rsDR("BATCHID")    
	        PONUMBER = BatchNumber(rsDR("BATCHID"))
	        PONAME = rsDR("PONAME")
	        'PODATE = FormatDateTime(rsDR("PODATE"),1)
			
	        
	        IF PODATE <> "" THEN
	              PODATE = PODATE & ", "  & FormatDateTime(rsDR("PODATE"),1)
	        ELSE
	              PODATE = FormatDateTime(rsDR("PODATE"),1)
	        END IF
	        
	        SUPPLIER = rsDR("NAME")
	        SUPPLIERID = rsDR("SUPPLIERID")	
	        POSTATUSNAME = rsDR("POSTATUSNAME")
	        'DEVELOPMENT = rsDR("DEVELOPMENTNAME")
	        
	      rsDR.moveNext  
	WEND        
else
	Response.Redirect ("PurchaseOrderNotFound.asp")
end if
Call CloseRs(rsDR)


SQL=" SELECT PI.ORDERID,INV.BATCHID,PI.ORDERITEMID, PI.ITEMNAME, PI.ITEMDESC, PI.PIDATE, ISNULL(PI.NETCOST,0) AS NETCOST, PI.VAT, V.VATNAME, ISNULL(PI.GROSSCOST,0) AS GROSSCOST, "&_
	"        PI.PISTATUS, PI.PITYPE, PS.POSTATUSNAME, V.VATCODE, "&_
	"        ISNULL(FLATNUMBER+', ','')+ISNULL(HOUSENUMBER+',','')  "&_
    "       +ISNULL(ADDRESS1+', ','')+ISNULL(ADDRESS2+', ','') "&_
    "       +ISNULL(ADDRESS3+', ','')+ISNULL(TOWNCITY,'')  AS PROPERTYADDRESS  "&_
    " FROM F_PURCHASEITEM PI "&_
	"    INNER JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID "&_
	"    INNER JOIN F_VAT V ON V.VATID = PI.VATTYPE "&_
	"    INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PI.PISTATUS "&_
	"    INNER JOIN P_WOTOREPAIR PWR ON PWR.ORDERITEMID=PI.ORDERITEMID "&_
	"    INNER JOIN C_REPAIR CR ON CR.JOURNALID=PWR.JOURNALID "&_
	"    INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
	"    INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID "&_
	"    INNER JOIN (SELECT DISTINCT REPAIRHISTORYID,BATCHID FROM GS_INVOICE) INV ON INV.REPAIRHISTORYID=CR.REPAIRHISTORYID "&_
    " WHERE PI.ACTIVE = 1 AND BATCHID = " & BATCHID &_
     " ORDER BY PI.ORDERITEMID ASC "	
     


PIString = ""
TotalNetCost = 0
TotalVAT = 0
TotalGrossCost = 0
TotalReconcilableNetCost = 0
TotalReconcilableVAT = 0
TotalReconcilableGrossCost = 0
TotalRows = 0
RowsThatCanBeReconciled = 0
RowsThatWillBeReconciled = 0

Call OpenRs(rsPI, SQL)		
while (NOT rsPI.EOF) 
	
	OrderItemID = rsPI("ORDERITEMID")
	OrderId = rsPI("ORDERID")

	NetCost = rsPI("NETCOST")
	VAT = rsPI("VAT")
	GrossCost = rsPI("GROSSCOST")
	
	TotalNetCost = TotalNetCost + NetCost
	TotalVAT = TotalVAT + VAT
	TotalGrossCost = TotalGrossCost + GrossCost
	
	DataStorage = "<INPUT TYPE=""HIDDEN"" NAME=""DS" & OrderItemID & """ VALUE=""" & NetCost & "|" & VAT & "|" & GrossCost & """>"
	
	PITYPE = rsPI("PITYPE")
	PISTATUS = rsPI("PISTATUS")

	RowsThatCanBeReconciled = RowsThatCanBeReconciled + 1

	if (PISTATUS >= 9) then
		CheckData = "<IMG SRC=""/myImages/tick.gif"" title=""This item has already been reconciled/paid."">"
		RowsThatCanBeReconciled = RowsThatCanBeReconciled - 1		
	elseif (PISTATUS = 0) then
		CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as it still waiting authorisation."">"	
	elseif (PITYPE = 2 AND PISTATUS <> 7 AND PISTATUS <> 5 AND PISTATUS <> 3) then
		CheckData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be reconciled as it has not been completed yet."">"
	else
		CheckData = "<INPUT TYPE=""CHECKBOX"" NAME=""CHKS"" ID=""CHKS"" VALUE=""" & OrderItemID & """ CHECKED onclick=""UpdateReconcileTotal()"">"
		RowsThatWillBeReconciled = RowsThatWillBeReconciled + 1
		TotalReconcilableNetCost = TotalReconcilableNetCost + NetCost
		TotalReconcilableVAT = TotalReconcilableVAT + VAT
		TotalReconcilableGrossCost = TotalReconcilableGrossCost + GrossCost
	end if

	if (PISTATUS >= 9) then
		AmendData = "<IMG SRC=""/myImages/tick.gif"" title=""This item has already been reconciled/paid and therefore cannot be amended."">"
	elseif (PISTATUS = 0) then
		AmendData = "<IMG SRC=""/myImages/Info.gif"" title=""This item cannot be amended as it still waiting authorisation."">"	
	else
		AmendData = "<INPUT TYPE=""BUTTON"" VALUE=""AM"" TITLE=""To amend this Purchase Item click here."" onclick=""APO(" & OrderItemID & "," & OrderId  & ")"" class=""RSLBUTTON"">"
	end if
	
	PIString = PIString & "<TR ALIGN=RIGHT><TD ALIGN=LEFT >" & DataStorage &  rsPI("PROPERTYADDRESS") & "</TD><TD align=center>" & rsPI("VATCODE") & "</TD><TD>" & FormatNumber(NetCost,2) & "</TD><TD>" & FormatNumber(VAT,2) & "</TD><TD>" & FormatNumber(GrossCost,2) & "</TD><TD>" & AmendData & "</TD><TD>" & CheckData & "</TD></TR>"
	ItemDesc = rsPI("ITEMDESC")
	if ( NOT(ItemDesc = "" OR isNULL(ItemDesc)) ) then
		PIString = PIString & "<TR ID=""PIDESCS"" STYLE=""DISPLAY:NONE""><TD width=410 colspan=5><I>" & rsPI("ITEMDESC") & "</I></TD></TR>"	
	end if
	rsPI.moveNext
wend

if (RowsThatWillBeReconciled = RowsThatCanBeReconciled) then
	DisplayText = "This Batch of Purchase Orders will become fully reconciled"
else 
	if (RowsThatWillBeReconciled = 0) then
		DisplayText = "No rows have or can be selected therefore no Reconciliation will occur."
		DisabledButton = " disabled"
	elseif (RowsThatCanBeReconciled - RowsThatWillBeReconciled = 1) then
		DisplayText = "This Batch of Purchase Orders  will not be fully Reconciled as there is " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " item that has<br> not or can not be selected."
	else
		DisplayText = "This Batch of Purchase Orders  will not be fully Reconciled as there are " & (RowsThatCanBeReconciled - RowsThatWillBeReconciled) & " items that have<br> not or can not be selected."
	end if						
end if

'*********************************************************************************************
'THIS PART WILLL FIND ALL PREVIOUS INVOICES THAT THE PURCHASE ORDER IS ASSOCIATED WITH
' IT IS DECIDED TO HIDE THIS FEATURE ON THIS PAGE. THIS IS NOT A AN AREA TO VIEW RECONCILED ITEMS
'*********************************************************************************************

'SQL = "SELECT *, THESTATUS = CASE CONFIRMED WHEN 0 THEN '<font color=red>Waiting Confirmation</font>' ELSE 'Confirmed' END FROM F_INVOICE WHERE ORDERID = " & ORDERID
'Call OpenRs(rsINV, SQL)
'if (NOT rsINV.EOF) then
'	InvoiceString = "<br><TABLE WIDTH=755 CELLPADDING=2 CELLSPACING=0 STYLE='BORDER:1PX SOLID BLACK'><TR style='color:white;background-color:#133e71'>" &_
'			"<TD WIDTH=170 HEIGHT=20><b>Invoice No:</b></TD><TD width=100><b>Tax Date:</b></TD><TD width=180><b>Status</b></TD><TD align=right width=80><b>Net (�):</b></TD><TD align=right width=75><b>VAT (�):</b></TD><TD align=right width=80><b>Gross (�):</b></TD><TD WIDTH=30></TD></TR>"
'	while NOT rsINV.EOF
'		InvoiceString = InvoiceString & "<TR><TD>" & rsINV("InvoiceNumber") & "</TD><TD>" & rsINV("TAXDATE") & "</TD><TD>" & rsINV("THESTATUS") & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("NETCOST"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("VAT"),2) & "</TD><TD ALIGN=RIGHT>" & FormatNumber(rsINV("GROSSCOST"),2) & "</TD></TR>"
'		rsINV.movenext
'	wend
'	InvoiceString = InvoiceString & "</TABLE>"
'end if
'Call CloseRs(rsINV)

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)
	
	
	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	' THERE ARE 2 SP's FOR THIS a)GET_VALIDATION_PERIOD_NODEFAULT and b)GET_VALIDATION_PERIOD THE DEFAULT ONE 
	' HAS A WIDER DATE RANGE THAN THE CURRENT YEAR FOR TIDYING UP PURPOSES
	SQL = "EXEC GET_VALIDATION_PERIOD_NODEFAULT"
	Call OpenRs(rsTAXDATE, SQL)	
	if(NOT rsTAXDATE.EOF) then
	    YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	    YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	end if    
	Call CloseRs(rsTAXDATE)

CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Reconciliation > Confirm</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	var FormFields = new Array()
	FormFields[0] = "txt_INVOICENUMBER|Invoice Number|TEXT|Y"
	FormFields[1] = "txt_TAXDATE|Tax Date|DATE|Y"
	
	var RowsThatWillBeReconciled = <%=RowsThatWillBeReconciled%>
	var RowsThatCanBeReconciled = <%=RowsThatCanBeReconciled%>

	
	function FormatCurrrenyComma(Figure){
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}

	function UpdateReconcileTotal(){
		iChecks = document.getElementsByName("CHKS")
		NetValue = 0
		TheVAT = 0
		GrossValue = 0
		if (iChecks.length){
			RowsThatWillBeReconciled = 0
			for (i=0; i<iChecks.length; i++){
				if (iChecks[i].checked == true){
					ORDERITEMID = iChecks[i].value
					FieldData = document.getElementById("DS" + ORDERITEMID).value
					FieldArray = FieldData.split("|")
					NetValue += parseFloat(FieldArray[0])
					TheVAT += parseFloat(FieldArray[1])
					GrossValue += parseFloat(FieldArray[2])
					RowsThatWillBeReconciled++
					}
				}
			}
		document.getElementById("NET").innerHTML = FormatCurrrenyComma(NetValue)
		document.getElementById("VAT").innerHTML = FormatCurrrenyComma(TheVAT)
		document.getElementById("GROSS").innerHTML = FormatCurrrenyComma(GrossValue)				
		document.getElementById("ReconcileButton").disabled = false
		if (RowsThatWillBeReconciled == RowsThatCanBeReconciled)
			document.getElementById("INFO").innerHTML = "This Batch of Purchase Orders  will become fully reconciled"
		else {
			if (RowsThatWillBeReconciled == 0) {
				document.getElementById("ReconcileButton").disabled = true
				document.getElementById("INFO").innerHTML = "No rows have or can be selected therefore no Reconciliation will occur."
				}
			else if (RowsThatCanBeReconciled - RowsThatWillBeReconciled == 1)
				document.getElementById("INFO").innerHTML = "The Batch of Purchase Orders will not be fully Reconciled as there is " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " item that has<br> not or can not be selected."
			else
				document.getElementById("INFO").innerHTML = "This Batch of Purchase Orders will not be fully Reconciled as there are " + (RowsThatCanBeReconciled - RowsThatWillBeReconciled) + " items that have<br> not or can not be selected."
			}						
		}

	function ToggleDescs(){
		if (document.getElementById("SHOWDESC").innerHTML == "SHOW DESCRIPTIONS"){
			NewText = "HIDE DESCRIPTIONS"
			NewStatus = "block"
			}
		else {
			NewText = "SHOW DESCRIPTIONS"
			NewStatus = "none"
			}
		iDesc = document.getElementsByName("PIDESCS")
		if (iDesc.length){
			for (i=0; i<iDesc.length; i++)
				iDesc[i].style.display = NewStatus
			}
		document.getElementById("SHOWDESC").innerHTML = NewText
		}		
		
	function Reconcile(){
	
	<% if LockdownStatus = "ON" then %>
		    var YStart = new Date("<%=YearStartDate%>")
		    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
		    var YEnd = new Date("<%=YearendDate%>")
		    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
		    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 2, YearStartDate),1) %>") 

		    if (!checkForm()) return false;
		    if  ( real_date(RSLFORM.txt_TAXDATE.value) < YStart || real_date(RSLFORM.txt_TAXDATE.value) > YEnd ) {
		        // outside current financial year
		        if ( TodayDate <= YEndPlusTime ) {
		            // inside grace period
		            if ( real_date(RSLFORM.txt_TAXDATE.value) < YStartPlusTime || real_date(RSLFORM.txt_TAXDATE.value) > YEnd) {
		                // outside of last and this financial year
                        alert("Please enter a tax date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
                        return false;
		            } else {
		                // date must be ok?
		            }
		        } else {
		            // we're outside the grace period and also outside this financial year
                    alert("Please enter a tax date for this financial year\n(i.e. between '<%=YearStartDate%>' and '<%=YearendDate%>')");
                    return false;                
			    }
		    }	
		<% end if %>
		
		document.getElementById("ReconcileButton").disabled = true
		document.getElementById("ReconcileButton").value = " Submitted "
		RSLFORM.action = "ServerSide/GasReconcileConfirm_svr.asp"
		RSLFORM.submit();
		}
		
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}

	function load_me(Order_id){
		window.showModelessDialog("Popups/PartReconcile.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 500px; dialogWidth: 580px; status: No; resizable: No;")
		}

	function UpdateChecks(){
		if (document.getElementById("MegaClick").checked == true)
			NewStatus = true
		else
			NewStatus = false
		ChkRef = document.getElementsByName("CHKS")
		if (ChkRef.length){
			for (i=0; i<ChkRef.length; i++){
				ChkRef[i].checked = NewStatus
				}
			}
		UpdateReconcileTotal()
		}

	function APO(OrderItemid,OrderID){
		result = window.showModelessDialog("Popups/AmendPurchaseItem.asp?BatchID=<%=BATCHID%>&OrderID=" + OrderID + "&CurrentPage=<%=REQUEST("CURRENTPAGE")%>&ReturnTo=2&OrderItemID=" + OrderItemid + "&Random=" + new Date(), document.getElementById("ReconcileButton"), "dialogHeight: 553px; dialogWidth: 354px; status: No; resizable: No;")
		}
				
<% if Request("ER12" & Replace(Date, "/", "")) = 1 then %>
	alert("A user is processing some invoices to be paid.\nPlease wait a few seconds before re-processing your actions.")
<% end if %>	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table border="0" cellspacing="5" cellpadding="0">
<FORM name=RSLFORM method=POST>
<TR><TD>
<table cellspacing=0 cellpadding=0><tr><td>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=490PX>
		<TR>
			<TD WIDTH=90PX>Batch ID:</TD><TD><%=PONUMBER%></TD>
		</TR><TR>
			<TD>Description:</TD><TD><%=PONAME%></TD>
		</TR><TR>		
			<TD>Order Date:</TD><TD><%=PODATE%></TD>
		</TR><TR>		
			<TD>Supplier:</TD><TD><%=SUPPLIER%></TD>						
		</TR>
		<TR>		
			<TD>&nbsp;</TD><TD>&nbsp;</TD>								
		</TR>
		<!--<TR>		
			<TD>Status:</TD><TD><%=POSTATUSNAME%></TD>								
		</TR>-->
		<tr><td>&nbsp;</td></tr>
	</TABLE>
</td><td width=10>&nbsp;</TD><td valign=top>
	<TABLE STYLE='BORDER:1PX SOLID BLACK' WIDTH=255PX height=100>
		<TR>
			<TD WIDTH=90PX nowrap>Invoice No:</TD><TD width=120 nowrap><input type="text" name="txt_INVOICENUMBER" maxlength=20 class="textbox"></TD>
			<TD width=40 nowrap><img src="/js/FVS.gif" name="img_INVOICENUMBER" width=15 height=15></TD>
		</TR><TR>
			<TD WIDTH=90PX>Tax Date:</TD><TD width=120 nowrap><input type="text" name="txt_TAXDATE" maxlength=10 class="textbox" VALUE=""></TD>
			<TD><img src="/js/FVS.gif" name="img_TAXDATE" width=15 height=15></TD>			
		</TR><TR>
			<TD height=100% colspan=3><font color=red><div id="INFO"><%=DisplayText%></div></font></TD>
		</TR>
	</TABLE>
</td></tr></table>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT style='color:white'> 
	  <!--<TD height=20 ALIGN=LEFT width=410 NOWRAP><table cellspacing=0 cellpadding=0 width=410><tr style='color:white'><td><b>Item Name:</b></td><td align=right style='cursor:hand' onclick="ToggleDescs()"><b><div id="SHOWDESC">SHOW DESCRIPTIONS</div></b></td></tr></table></TD>-->
	  <td width=70 ALIGN=LEFT width=410 NOWRAP><b>Item Name:</b></td>
	  <!--<td width=90 ALIGN=LEFT width=410 NOWRAP><b>Appliance Type</b></td>-->
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=80 nowrap><b>Net (�):</b></TD>
	  <TD width=75 nowrap><b>VAT (�):</b></TD>
	  <TD width=80 nowrap><b>Gross (�):</b></TD>
	  <TD width=30>&nbsp;</TD>		  
	  <TD width=30><input type=checkbox name="MegaClick" checked onclick="UpdateChecks()"></TD>		  
	</TR>
	<%=PIString%> 
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK;' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalNetCost,2)%></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><%=FormatNumber(TotalVAT,2)%></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><%=FormatNumber(TotalGrossCost,2)%></b></TD>
	  <TD width=30 bgcolor=white>&nbsp;</TD>
	  <TD width=30 bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=#133e71 ALIGN=RIGHT> 
	  <TD height=20 width=450 NOWRAP style='border:none;color:white'><b>TOTAL RECONCILABLE : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="NET"><%=FormatNumber(TotalReconcilableNetCost,2)%></div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><div id="VAT"><%=FormatNumber(TotalReconcilableVAT,2)%></div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><div id="GROSS"><%=FormatNumber(TotalReconcilableGrossCost,2)%></div></b></TD>
	  <TD width=30 bgcolor=white>&nbsp;</TD>
	  <TD width=30 bgcolor=white >&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>  
  <TABLE cellspacing=0 cellpadding=2 width=755>
	<TR ALIGN=RIGHT> 
	  <TD>
	  <input type="HIDDEN" name="hid_PAGE" value="<%=Request("CurrentPage")%>">
	  <input type="HIDDEN" name="hid_ORDERID" value="<%=ORDERID%>">
	  <input type="HIDDEN" name="hid_BATCH_ORDERID" value="<%=BATCH_ORDERID%>">
	  <input type="HIDDEN" name="hid_SUPPLIERID" value="<%=SUPPLIERID%>">
	  <input type="HIDDEN" name="hid_BATCHID" value="<%=BATCHID%>" />
	  <input type="HIDDEN" name="hid_RECONCILE" value="RECONCILE">
	  <input type="BUTTON" name="ReconcileButton" value=" RECONCILE " onclick="Reconcile()" class="RSLButton" <%=DisabledButton%>>
	  </TD>		  
	</TR>
  </TABLE>
<%=InvoiceString%>  
</TD></TR>
</FORM>
</table>
<iframe name="PURCHASERECONCILEFRAME123" style='display:none'></iframe>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
