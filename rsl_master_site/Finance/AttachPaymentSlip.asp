<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_office
	
	OpenDB()
	
	Call BuildSelect(lst_office, "sel_OFFICE", "G_OFFICE", "OFFICEID, DESCRIPTION", "DESCRIPTION", "All", 0, "width:150px", "textbox200", NULL)
	
	CloseDB()	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
	FormFields[1] = "sel_OFFICE|Local Office|SELECT|N"
	FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|N"
	FormFields[3] = "txt_TO|To Date|DATE|Y"
		
	function process(){

		FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|Y"
		FormFields[4] = "txt_POSTTOTAL|total|CURRENCY|Y"
		if (!checkForm()) return false;
			if (document.getElementById("txt_POSTTOTAL").value <= 0){
			ManualError("img_POSTTOTAL","Please pick at least one posting.", 0);
			return false;
			}		
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/process_attach_srv.asp?sliptotal="+detotal;
		RSLFORM.submit();

	}

	
	function reset_client_totals(){
	
		int_initial = 0;
		str_idlist = "";
		detotal = 0;
	
	}
	
	function save_form(){
		
		FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|N"
		FormFields[4] = "txt_TO|To Date|DATE|Y"
		
		reset_client_totals();		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/attach_srv.asp";
		RSLFORM.submit();
	}
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist + int_num.toString();
			else
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string
			}
		else {
			detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			remove_item(int_num);
			}

		document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("txt_BALANCE").value = FormatCurrency(document.getElementById("txt_TOTAL").value - detotal);				
		document.getElementById("idlist").value = str_idlist;
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = RSLFORM method=post>

	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR>
			<TD>Select search criteria to bring back appropriate <b>cash posting</b> entries from the rent account journal.</TD>
		</TR>
	</TABLE>
			
        <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2 HEIGHT=40PX STYLE='BORDER:1PX SOLID #133E71' bgcolor=beige>
          <TR> 
            
      <TD NOWRAP><b>Local Office</b></TD>
            <TD><b>From</b></TD>
			<TD><b>To</b></TD>
            <TD><b>Slip Number</b></TD>
          </TR>
          <TR> 
            <TD NOWRAP>
				<%=lst_office%>
                <image src="/js/FVS.gif" name="img_OFFICE" width="15px" height="15px" border="0"> 
            </TD>
            <TD NOWRAP><INPUT TYPE=TEXT NAME=txt_FROM CLASS='TEXTBOX100' id = "txt_FROM"> 
						<image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0"> 
		    <TD NOWRAP><INPUT TYPE=TEXT NAME=txt_TO CLASS='TEXTBOX100' id = "txt_TO"> 
						<image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">
            </TD>
            <TD NOWRAP> 
              <INPUT TYPE=TEXT NAME=txt_SLIPNUMBER CLASS='TEXTBOX200' MAXLENGTH=48 id = "txt_SLIPNUMBER"> 
              <image src="/js/FVS.gif" name="img_SLIPNUMBER" width="15px" height="15px" border="0"> 
            </TD>
            <TD WIDTH=100% NOWRAP> 
              <input type="button" value="Proceed" class="RSLButton"  onclick="save_form()" "name="button" id = "button">
            </TD>
          </TR>
		  <tr style='display:none'><td colspan=4><input type=text id=idlist name=idlist CLASS='TEXTBOX200' >
		  		 </td></tr>
        </TABLE>
<BR>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_slip width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

