<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (2)	 'USED BY CODE
	Dim DatabaseFields (2)	 'USED BY CODE
	Dim ColumnWidths   (2)	 'USED BY CODE
	Dim TDSTUFF        (2)	 'USED BY CODE
	Dim TDPrepared	   (2)	 'USED BY CODE
	Dim ColData        (2)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (2)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (2)	 'All Array sizes must match	
	Dim TDFunc		   (2)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Voucher Date|vDate|90"
	SortASC(0) 	= "nlj.TXNDATE ASC"
	SortDESC(0) = "nlj.TXNDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "formatdatetime(|)"

	ColData(1)  = "Voucher No:|vnumber|300"
	SortASC(1) 	= "nlj.REFNUMBER  ASC"
	SortDESC(1) = "nlj.REFNUMBER  DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Gross Amount|GTOT|90"
	SortASC(2) 	= "GTOT ASC"
	SortDESC(2) = "GTOT DESC"	
	TDSTUFF(2)  = " "" ALIGN=RIGHT"" "
	TDFunc(2) = "FormatCurrency(|)"		

	
			
	
	
	PageName = "PettyCash_DTL.asp"
	EmptyText = "No Relevant Petty Cash found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	'RowClickColumn = " "" ONCLICK=""""load_me("" & rsSet(""PROCESSDATE"") & "","" & rsSet(""ORGID"") & "","" & rsSet(""PAYMENTTYPEID"") & "")"""" """ 

	
	RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""TXNID"") & "","" & rsSet(""cnt"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	
	
	SQLCODE="select count(fp.orderitemid) cnt,sum(fp.GROSSCOST) as GTOT,convert(varchar,nlj.TXNDATE,103) as vdate, replace(nlj.REFNUMBER,'VO','<b>VO</b>') as vnumber,nlj.TXNID " &_
			"from F_PURCHASEITEM fp inner join NL_JOURNAL2PURCHASEITEM j2p on fp.ORDERITEMID=j2p.ORDERITEMID " &_
			"inner join NL_JOURNALENTRY nlj on j2p.TRANSACTIONID=nlj.TXNID  " &_
			"where fp.PITYPE=3 and nlj.TRANSACTIONTYPE=5  " &_
			"group by  nlj.REFNUMBER,nlj.TXNDATE,nlj.TXNID" &_
			" ORDER BY " & orderBy
	

	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	'OpenDB()
	
	'Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "NL_ACCOUNT", "ACCOUNTID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	'CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Balance Transfer</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
var FormFields = new Array()




function SubmitPage()
	{
		if (	document.getElementById("txt_DT").value != "")
		{
			FormFields[0] = "txt_DT|Date|DATE|Y"
			if (!checkForm()) 
			{
				return false
			}
		}

	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  ;
	}

function load_me(txnid,cnt)
	{
		
	location.href="pettycash.asp?txnid=" + txnid + "&cnt="+ cnt
	//window.showModelessDialog("pettycash.asp?txnid=" + txnid  );
	//,,"dialogHeight: 200px; dialogWidth: 250px; dialogTop: 23px; dialogLeft: 1076px; edge: Raised; center: Yes; help: Yes; resizable: Yes; status: Yes;");
	
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>
<!--
<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><%=lstSuppliers%>
</td>

<td><img src="/js/FVS.gif" width="15" height="15" name="img_DT"></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()" id=button1 name=button1>
</td></tr></table>-->
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>