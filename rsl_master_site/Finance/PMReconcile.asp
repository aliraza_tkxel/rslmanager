<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>
        <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
        <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>RSL Manager Finance --> Planned Maintenance  </title>
        <link rel="stylesheet" href="/css/RSL.css" type="text/css">
        <script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
        <script type="text/javascript" language="javascript" src="/js/general.js"></script>
        <script type="text/javascript" language="javascript" src="/js/menu.js"></script>
        <script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
        <script type="text/javascript" language="javascript">
	        function pmopen() {
 	            window.open("/RSLPlannedMaintenance","pmmainapp","status=1,scrollbars=1,resizable=1,left=10,top=20");
    	        }   
	        function openURL(url) {
	            window.parent.navigate(url);
	        }
        </script>
    </head>
    <body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()" style=" background-color:White; margin:10px 0px 0px 10px;">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->  
            <iframe id="ifmPlannedWork" src="/RSLPlannedMaintenance/PMReconciliation.aspx"  frameborder="0"  height="100%" width="100%;"   >
            </iframe>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    </body>
</html>