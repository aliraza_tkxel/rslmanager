<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	Const adUseClient = 3
					
	Dim oConnection
	Dim oRecordset
	Dim pRecordset

	Set oConnection = CreateObject("ADODB.Connection")
	oConnection.Open "DSN=RSLQB;"
	
	Session.lcid = 2057
	
	Call debit_account(1605, 100, True)
	
	//Call credit_account(4020, 20, True)
	//Call credit_account(4030, 20, True)
	//Call credit_account(4040, 10, True)
	//Call credit_account(4050, 10, True)
	//Call credit_account(4060, 20, True)
	//Call credit_account(4070, 20, False)
	
	oConnection.Close
	Set oConection = Nothing
	
	Function debit_account(nominal_code, credit_amount, FQ_status)
		
		Dim nominal_code_table_listid 	
	
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient
	
		// get correct reference id from accounts table for 5630
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		Set oRecordset = CreateObject("ADODB.Recordset")
		oRecordset.Open "SELECT * FROM JOURNALENTRYDEBITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		oRecordset.AddNew()
		oRecordset.Fields("REFNUMBER").Value = "1"
		oRecordset.Fields("JOURNALDEBITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		oRecordset.Fields("JOURNALDEBITLINEAMOUNT").Value = credit_amount
		oRecordset.Fields("JOURNALDEBITLINEMEMO").Value = "Auto RSL Debit"
		oRecordset.Fields("JournalDEBitLineEntityRefListID").Value = "A0000-1084200309"
		oRecordset.Fields("JournalDEBitLineEntityRefFullName").Value = "BHA"
		oRecordset.Fields("FQSaveToCache").Value = FQ_status	
		oRecordset.Update()
		
		oRecordset.close()
		
	End Function	

	Function credit_account(nominal_code, credit_amount, FQ_status)
		
		Dim nominal_code_table_listid 	
		
		Set sel_Recordset = CreateObject("ADODB.Recordset")
		sel_Recordset.CursorLocation = adUseClient

		// get correct reference id from accounts table for 5630
		sel_Recordset.Open "SELECT LISTID FROM ACCOUNT WHERE ACCOUNTNUMBER = '" & nominal_code & "'" , oConnection, adOpenStatic, adLockOptimistic
		nominal_code_table_listid = sel_Recordset("LISTID")	
		sel_Recordset.Close
		response.write "<BR> list id = : " & nominal_code_table_listid
	
		Set oRecordset = CreateObject("ADODB.Recordset")
		oRecordset.Open "SELECT * FROM JOURNALENTRYCREDITLINE WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
		
		oRecordset.AddNew()
		oRecordset.Fields("REFNUMBER").Value = "1"
		oRecordset.Fields("JOURNALCREDITLINEACCOUNTREFLISTID").Value = nominal_code_table_listid
		oRecordset.Fields("JOURNALCREDITLINEAMOUNT").Value = credit_amount
		oRecordset.Fields("JOURNALCREDITLINEMEMO").Value = "Auto RSL Credit"
		oRecordset.Fields("JournalCREDitLineEntityRefListID").Value = "A0000-1084200309"
		oRecordset.Fields("JournalCREDitLineEntityRefFullName").Value = "BHA"
		oRecordset.Fields("FQSaveToCache").Value = FQ_status	
		oRecordset.Update()
		
		oRecordset.close()
		
	End Function	

	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Bacs List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF>
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>