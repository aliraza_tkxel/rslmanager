<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<%
'This creates one JournalEntry with 2 credit lines and 2 debit lines. Note the FQSaveToCache field, set to True except on the last line.
	ON ERROR RESUME NEXT

	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	Const adUseClient = 3
	
	Dim oConnection
	Dim oRecordsetCredit
	Dim oRecordsetDebit
	Dim sLastVendor
	Dim dTotalApplied
	Dim dAmountDue
	
	Set oConnection = CreateObject("ADODB.Connection")
	Set oRecordsetCredit = CreateObject("ADODB.Recordset")
	Set oRecordsetDebit = CreateObject("ADODB.Recordset")
	
	oConnection.Open "DSN=RSLQB;;OLE DB Services=-2"
	oRecordsetCredit.CursorLocation = adUseClient
	oRecordsetCredit.Open "SELECT * FROM JournalEntryCreditLine WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
	oRecordsetDebit.CursorLocation = adUseClient
	oRecordsetDebit.Open "SELECT * FROM JournalEntryDebitLine WHERE TxnId = 'X'" , oConnection, adOpenStatic, adLockOptimistic
	
	//CREDIT RENT ACCOUNT
	oRecordsetCredit.AddNew()
	oRecordsetCredit.Fields("RefNumber").Value = "1"
	oRecordsetCredit.Fields("JournalCreditLineAccountRefListID").Value = "90000-1078306095"
	oRecordsetCredit.Fields("JournalCreditLineAmount").Value = 100.22
	oRecordsetCredit.Fields("JournalCreditLineMemo").Value = "RSL Automated Insert"
	oRecordsetCredit.Fields("JournalcreditLineEntityRefListID").Value = "A0000-1084200309"
	oRecordsetCredit.Fields("JournalcreditLineEntityRefFullName").Value = "BHA"
	oRecordsetCredit.Fields("FQSaveToCache").Value = True
	oRecordsetCredit.Update()

	// DEBIT  TOTAL OF RENTS
	oRecordsetDebit.AddNew()
	oRecordsetDebit.Fields("RefNumber").Value = "1"
	oRecordsetDebit.Fields("JournalDebitLineAccountRefListID").Value = "1530000-1078369866"
	oRecordsetDebit.Fields("JournalDebitLineAmount").Value = 100.22
	oRecordsetDebit.Fields("JournalDebitLineMemo").Value = "RSL Automated Insert"
	//oRecordsetDebit.Fields("JournalDebitLineEntityRefListID").Value = "A0000-1084200309"
	//oRecordsetDebit.Fields("JournalDebitLineEntityRefFullName").Value = "BHA"
	oRecordsetDebit.Fields("FQSaveToCache").Value = False
	oRecordsetDebit.Update()
	
	oRecordsetCredit.Close
	oRecordsetDebit.Close
	oConnection.Close

	RESPONSE.WRITE ("Error # " & CStr(Err.Number) & " " & Err.Description)

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Rent Insert</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<BODY BGCOLOR=#FFFFFF>

</BODY>
</HTML>