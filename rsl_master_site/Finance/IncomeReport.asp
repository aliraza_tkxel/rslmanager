<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include VIRTUAL="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<%
TimeStamp = Replace(Replace(Replace(Now, "/", ""), ":", ""), " ", "")
'Call BuildSelect(lstAssets, "sel_ASSETTYPE", "P_ASSETTYPE", "ASSETTYPEID, DESCRIPTION", "DESCRIPTION", "Please Select", REQUEST("sel_ASSETTYPE"), NULL, "textbox200", NULL)	

%>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Finance</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANAGUAGE=JAVASCRIPT>
	var FormFields = new Array()
	FormFields[0] = "sel_PERIOD|FISCAL YEAR|SELECT|Y"
	
	function GO(){
		if (!checkForm()) return false
		RESULT.innerHTML = "<BR><B>Please wait. Loading . . . . . . </B>"
		RSLFORM.action = "ServerSide/IncomeReport_svr.asp"
		RSLFORM.target = "iDataSub<%=TimeStamp%>"
		RSLFORM.submit()
		}

	function GetEntryList(){
		if (RSLFORM.sel_PERIOD.value == ""){
			RSLFORM.sel_ENTRYDATE.outerHTML = "<SELECT NAME='sel_ENTRYDATE' class='textbox200' disabled><OPTION VALUE=''>PLEASE SELECT</OPTION></SELECT>"
			return false;
			}
		RSLFORM.action = "ServerSide/ENTRYDATE_svr.asp"
		RSLFORM.target = "iDataSub<%=TimeStamp%>"
		RSLFORM.submit()
		}			
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include VIRTUAL="/Includes/Tops/BodyTop.asp" --> 
<FORM NAME=RSLFORM METHOD=POST> 
<TABLE STYLE='BORDER-COLLAPSE:COLLAPSE;border:1px solid silver' CELLPADDING=2 CELLSPACING=0 BORDER=0>
	<TR ALIGN=LEFT><TD COLSPAN=4 STYLE='BORDER-bottom:1PX SOLID SILVER'><b>FILTERS: </b></TD></TR>
	<TR BGCOLOR=BEIGE><TD WIDTH=130>TENANCY STATUS: </TD><TD ALIGN=RIGHT><SELECT NAME="sel_TYPE" CLASS="TEXTBOX200">
									<OPTION VALUE="" <%IF (ITYPE=-1) THEN RESPONSE.WRITE "SELECTED"%>>BOTH</OPTION>
									<OPTION VALUE=1 <%IF (ITYPE=1) THEN RESPONSE.WRITE "SELECTED"%>>CURRENT</OPTION>
									<OPTION VALUE=2 <%IF (ITYPE=2) THEN RESPONSE.WRITE "SELECTED"%>>PREVIOUS</OPTION>																		
									</SELECT>
								</TD><TD><IMG NAME="img_TYPE" src="/js/FVS.gif" WIDTH=15 HEIGHT=15></TD><TD WIDTH=129>ARREARS STATUS: </TD>
								<TD ALIGN=RIGHT><SELECT NAME="sel_ARREARSTYPE" CLASS="TEXTBOX200">
									<OPTION VALUE="" <%IF (ATYPE=-1) THEN RESPONSE.WRITE "SELECTED"%>>BOTH</OPTION>
									<OPTION VALUE=1 <%IF (ATYPE=1) THEN RESPONSE.WRITE "SELECTED"%>>IN ARREARS</OPTION>
									<OPTION VALUE=2 <%IF (ATYPE=2) THEN RESPONSE.WRITE "SELECTED"%>>IN CREDIT</OPTION>																		
									</SELECT>
								</TD><TD><IMG NAME="img_ARREARSTYPE" src="/js/FVS.gif" WIDTH=15 HEIGHT=15></TD></TR>
	<TR BGCOLOR=BEIGE><TD>FISCAL YEAR: </TD><TD ALIGN=RIGHT>
	<SELECT NAME="sel_PERIOD" CLASS="textbox200" onchange="GetEntryList()">
  	<OPTION value="">PLEASE SELECT</OPTION>
    <%
		OpenDB()
		SQL = "SELECT * FROM F_FISCALYEARS ORDER BY YRANGE"
		Call OpenRs(rsTheYears, SQL)
		While (NOT rsTheYears.EOF)
			isSelected = ""
			if (CStr(PERIOD) = CStr(rsTheYears.Fields.Item("YRANGE").Value)) Then
				isSelected = " selected"
			end if
	%>
     <OPTION VALUE="<%=(rsTheYears.Fields.Item("YRANGE").Value)%>" <%=isSelected%>><%=(rsTheYears.Fields.Item("YSTART").Value) & " - " & (rsTheYears.Fields.Item("YEND").Value)%></OPTION>
    <%
	  		rsTheYears.MoveNext()
		Wend
	%>
   </SELECT>	
	</TD>
									<TD><IMG NAME="img_PERIOD" src="/js/FVS.gif" WIDTH=15 HEIGHT=15></TD>	
									<TD>MONTH: </TD><TD ALIGN=RIGHT>	
									<SELECT NAME="sel_ENTRYDATE" class="textbox200" disabled>
										<OPTION VALUE="">PLEASE SELECT</OPTION>
									</SELECT>
									</TD>
									<TD><IMG NAME="img_ENDDATE" src="/js/FVS.gif" WIDTH=15 HEIGHT=15></TD>
									</TR>
	<TR BGCOLOR=BEIGE>
	  <TD>ASSET TYPE </TD>
	  <TD ALIGN=RIGHT><SELECT NAME="sel_ASSETTYPE" CLASS="textbox200">
        <OPTION value="">ALL</OPTION>
        <%
		
		SQL = "SELECT * FROM P_ASSETTYPE ORDER BY DESCRIPTION"
		Call OpenRs(rsAssetType, SQL)
		While (NOT rsAssetType.EOF)
			isSelected = ""
			if (CStr(ASSETTYPE) = CStr(rsAssetType.Fields.Item("ASSETTYPEID").Value)) Then
				isSelected = " selected"
			end if
	%>
        <OPTION VALUE="<%=(rsAssetType.Fields.Item("ASSETTYPEID").Value)%>" <%=isSelected%>><%=(rsAssetType.Fields.Item("DESCRIPTION").Value) %></OPTION>
        <%
	  		rsAssetType.MoveNext()
		Wend
	%>
      </SELECT></TD>
	  <TD>&nbsp;</TD>
	  <TD>&nbsp;</TD>
	  <TD ALIGN=RIGHT>&nbsp;</TD>
	  <TD>&nbsp;</TD>
	  </TR>

	<TR><TD COLSPAN=5 ALIGN=RIGHT STYLE='border-none;BORDER-TOP:1PX SOLID SILVER'><INPUT TYPE=BUTTON VALUE=" SUBMIT " CLASS="RSLBUTTON" ONCLICK="GO()"></TD><TD STYLE='BORDER-TOP:1PX SOLID SILVER'></TD></TR>
</TABLE>
</FORM>
<DIV ID="RESULT"></DIV>
<iframe  src="/secureframe.asp" name="iDataSub<%=TimeStamp%>" STYLE='WIDTH:500;display:none'></iframe>
<!--#include virtual="/Includes/Bottoms/BodyBottom.asp" -->
<%

%>
</BODY>
</HTML>
