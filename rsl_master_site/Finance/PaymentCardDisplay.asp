<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/Calendar.asp" -->
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance - Payment Card Display</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript">

        function getFileExtension(filePath) { //v1.0
            fileName = ((filePath.indexOf('/') > -1) ? filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length) : filePath.substring(filePath.lastIndexOf('\\') + 1, filePath.length));
            return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
        }

        function checkFileUpload(form) { //v1.0
            document.MM_returnValue = true;
			var extensions=[".PO", ".PP", ".DD", ".CSH", ".CQE", ".TC", ".TCC", ".TDC"]; 
            if (extensions && extensions != '') {
                for (var i = 0; i < form.elements.length; i++) {
                    field = form.elements[i];
                    if (field.type.toUpperCase() != 'FILE') continue;
					 if (field.value.length > 0) {
                var blnValid = false;
                for (var j = 0; j < extensions.length; j++) {
                    var sCurExtension = extensions[j];
                    if (field.value.substr(field.value.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    ManualError('img_FILEIMAGE', 'This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + extensions.join(", ") + '.\nPlease select another file and try again.', 0);
                        document.MM_returnValue = false; field.focus();
                        return false;
                        break;
                }
            }
                    
                }
            }
            ManualError('img_FILEIMAGE', '', 3);
            return true;
        }
		 


        function BtnSubmit() {
            result = checkFileUpload(RSLFORM);
            if (RSLFORM.FILENAME.value == "") {
                ManualError('img_FILEIMAGE', 'You must select a file to upload.', 2);
                return false;
            }
            if (!result) return false;
            RSLFORM.submit();
        }

    </script>
</head>
<body onload="initSwipeMenu(0);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.
	
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='PaymentCardFiles.asp?UploadDate=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	FutureCast = ""
	FutureSQL = ""
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		RequestDate = CDate("1" & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear)
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear		
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)	
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear				
	End If
	
	'THIS IS THE PAYMENTCARD ACTUAL SQL
	SQL = "SELECT DAY(UPLOADdATE) AS THEDAY, SUM(PROCESSCOUNT) AS TOTALCOUNT, ISNULL(SUM(PROCESSVALUE),0) AS TOTALVALUE " &_
			"FROM F_PAYMENTCARDFILES "&_
			"WHERE UPLOADDATE >= '" & StartOfMonthDate & "' AND UPLOADDATE <= '" & EndOfMonthDate & "' AND FILETYPE = 1 " &_
			"GROUP BY DAY(UPLOADDATE)"
					
	' Add items to the calendar
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			OpenDB()
			
			'THIS SECTION DISPLAYS THE ACTUAL PAYMENTCARD DATA UPLOADED IN THE SYSTEM
			Call OpenRs(rsDays, SQL)
			while (NOT rsDays.EOF) 
				MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a href='PaymentCardFiles.asp?UploadDate=" & rsDays("THEDAY") & CurrentDateSmall & "' title='Total Files Uploaded --> " & rsDays("TOTALCOUNT") & ".  Total Value of Files --> " & FormatCurrency(rsDays("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDays("TOTALCOUNT") & " </b></small></font></a>", "lightgreen"
				rsDays.moveNext
			wend
			CloseRs(rsDays)									
		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
                %>
                <div style='position: absolute; left: 500; top: 140; width: 200px'>
                    <b><u>Payment Cards</u></b><br />
                    <br />
                    This calendar shows the number of Payment Card transactions that have been successfully
                    uploaded into RSL Manager on any particular day. To get the financial value of these
                    transactions hover over the respective number.
                    <br />
                    <br />
                    To change the selected month click on '<b><<</b>' or '<b>>></b>' to go backwards
                    and forwards respectively.
                    <br />
                    <br />
                    To view the Payment Card files for any particular day click on the respective month
                    day number.
                    <form method="POST" name="RSLFORM" action="ServerSide/PaymentCardUpload.asp?GP_upload=true"
                    enctype="multipart/form-data">
                    <table border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td>
                                <b>Upload Payment File</b>
                            </td>
                            <tr>
                                <tr>
                                    <td>
                                        <input type="file" size="30" name="FILENAME" id="FILENAME" class="RSLButton" style="background-color: white;
                                            color: black" />&nbsp;
                                    </td>
                                    <td>
                                        <img src="/js/FVS.gif" name="img_FILEIMAGE" id="img_FILEIMAGE" width="15px" height="15px"
                                            border="0" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <input type="button" name="Submit" value="Upload File" onclick="BtnSubmit()" class="RSLButton" />&nbsp;
                                    </td>
                                </tr>
                    </table>
                    </form>
                </div>
            </td>
        </tr>
    </table>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="400px" height="400px" style='display: none'>
    </iframe>
</body>
</html>
