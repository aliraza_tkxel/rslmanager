<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include file="../../freeaspupload.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%

    Dim InvoiceAmount, GrossCost, OrderId, InvoiceNo, IsInvoiceExist, IsRequeuedOrder, IsDeleteOrder, IsSaveData, uploadsDirVar
    IsInvoiceExist = 0
	uploadsDirVar = Server.MapPath("/Invoice_Images/")
	InvoiceAmount = Request("invoiceAmount")
    GrossCost = Request("grossCost")
	OrderId = Request("orderId")
	InvoiceNo = Request("invoiceNo")
    IsRequeuedOrder = Request("isRequeuedOrder")
    IsDeleteOrder = Request("isDeleteOrder")
    IsSaveData = Request("isSaveData")
    if(IsSaveData = "true") then
        Call SaveInvoiceData()
    end if
    if (IsRequeuedOrder = "false" and IsDeleteOrder = "false" and IsSaveData = "false") then
        Call IsInvoiceAlreadyExist(InvoiceNo, InvoiceAmount, OrderId)
    end if
    if(IsRequeuedOrder = "true") then
        Call RequeuedOrder(OrderId)
    end if
    if(IsDeleteOrder = "true") then
        call CancelOrder(OrderId)
    end if

    Function SaveInvoiceData ()
        SaveFiles()
        'End Parametters
       'Call build_po_detail()
        OrderID = Session("OrderID")
	    Call SendEmail(OrderID)

    End Function


    Function RequeuedOrder(OrderId)
    
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
        POStatus = 0
        Call LOG_PO_ACTIONLOG_INVOICEUPLOADPAGE(OrderID, "REQUEUED FROM UPLOAD INVOICE")

         SQL = "SELECT ORDERITEMID FROM F_PURCHASEITEM WHERE ORDERID = " & ORDERID 
		    Call OpenRs(rsPI, SQL)
        While (NOT rsPI.EOF)
            ORDERITEMID = rsPI("ORDERITEMID")
            Call LOG_PI_ACTION_InvoiceUploadPage(ORDERITEMID,"REQUEUED FROM UPLOAD INVOICE")
            rsPI.movenext
        Wend
        Call CloseRs(rsPI)
		
		SQLCODE = "UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" &_
         "; UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" 
        Conn.Execute(SQLCODE)
    End Function

    Function IsInvoiceAlreadyExist(invoiceNumber, invoiceCost, orderId)
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
        SQL = "select * from F_PURCHASEORDER where ORDERID =" & orderId
        Call OpenRs(rsPO, SQL)
        While (NOT rsPO.EOF)
            SupplierId = rsPO("SUPPLIERID")
            rsPO.movenext
        Wend
        Call CloseRs(rsPO)
        SQL ="Select * from F_INVOICE Where SUPPLIERID = " & SupplierId & " and INVOICENUMBER = '" & CStr(invoiceNumber) & "' and GROSSCOST = " & invoiceCost
		Call OpenRs(rsPOInvoice, SQL)
		
        While (NOT rsPOInvoice.EOF)
            IsInvoiceExist = 1       
            rsPOInvoice.movenext
         Wend
        Call CloseRs(rsPOInvoice)
        Call CloseDB()
		
    End Function

    Function CancelOrder(OrderId)
		' Response.Write( "In Cencel" )
		 ' Response.End()
		 ' Response.flush
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
        POStatus = 16
        SQLCODE = "UPDATE F_PURCHASEORDER SET ACTIVE = 0, POSTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" &_
         "; UPDATE F_PURCHASEITEM SET ACTIVE = 0, PISTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "')" 
        Conn.Execute(SQLCODE)
		
		SQLCODE = "DELETE FROM F_INVOICE WHERE ORDERID = " & OrderID
		Conn.Execute(SQLCODE)
    End Function

    Function SaveFiles

        Dim FSO
        Set FSO = CreateObject("Scripting.FileSystemObject")
        If NOT (FSO.FolderExists(uploadsDirVar)) Then
            FSO.CreateFolder(uploadsDirVar)
        End If

        Dim fileName, fileSize, ks, i, fileKey, outFileName
        
        Set Upload = New FreeASPUpload
        Upload.SaveOne uploadsDirVar, 0, fileName, outFileName

	    ' If something fails inside the script, but the exception is handled
	    If Err.Number<>0 then Exit function
       'OrderID = Upload.Form("OrderID")
        '//Start Parameters
        'Getting the OderId & OrderItemId(if Child Item selected)
         OrderID =Upload.Form("OrderID")
        Session("OrderID")=Upload.Form("OrderID")
        '//End Parameters
        SaveFiles = ""
        ks = Upload.UploadedFiles.keys
        if (UBound(ks) <> -1) then
            SaveFiles = "<B>Files uploaded:</B> "
            for each fileKey in Upload.UploadedFiles.keys
                SaveFiles = SaveFiles & Upload.UploadedFiles(fileKey).FileName & " (" & Upload.UploadedFiles(fileKey).Length & "B) "
            next
        else
            SaveFiles = "No file selected for upload or the file name specified in the upload form does not correspond to a valid file in the system."
        end if
		
        SavePOHistory OrderID, Upload.Form("list_values"), Upload.Form("t_area"), "/Invoice_Images/" & outFileName ,InvoiceNo,Upload.Form("txt_TaxDate")
             
    End Function

    function SavePOHistory(OrderID, POStatus, Notes, ImageURL, InvoiceNo, TaxDate)

        'ALTER TABLE [RSLBHALive1].[dbo].[F_PURCHASEORDER_LOG] ADD image_url VARCHAR(1000) null ;
        Dim SQLCODE
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
     '//Updating Main Tables (Purchase Order & Item)
        
        ' TODO: Need to discuss the status of ORder here
         SQL ="Select Distinct ORDERITEMID from F_PURCHASEITEM Where OrderID= " & OrderID & " and PIStatus<>7"
         Call OpenRs(rsPurchaseItems, SQL)
         '//Updating Purchase Item Logs
         Dim PIArray()
         aryCounter = 0
         While (NOT rsPurchaseItems.EOF)
           ReDim Preserve PIArray(aryCounter) 
           PIArray(aryCounter) = rsPurchaseItems("ORDERITEMID")
           aryCounter = aryCounter + 1
           rsPurchaseItems.movenext
         Wend
          Call CloseRs(rsPurchaseItems) 
         SQLCODE = "UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "') " &_
         "; UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatus & " WHERE ORDERID=CONVERT(INT,'" & OrderID & "') AND PISTATUS = 18" 
         SQL = "Select CMContractorId,ServiceItemId from CM_ContractorWork where PurchaseOrderId = " & OrderID
            Call OpenRs(rs, SQL)
			if not rs.EOF then
              CMContractorId = rs("CMContractorId")  
              ServiceItemId = rs("ServiceItemId")        
            end if
			CloseRs(rs)
            if(ServiceItemId > 0) then
             SQLUpdate ="Update CM_ServiceItems set StatusId =  (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) WHERE ServiceItemId = " & ServiceItemId
                 Conn.Execute(SQLUpdate)
            SQLUpdate ="Update CM_ContractorWorkDetail SET StatusId = (Select s.StatusId from CM_Status s where s.Title='Invoice Uploaded' ) where CMContractorId=" & CMContractorId
             Conn.Execute(SQLUpdate)
			  end if
     
        Conn.Execute(SQLCODE)
           
        '//Checking Image Duplication in Purchase Items
        SQL ="Select Distinct ORDERID from F_PURCHASEORDER_LOG Where OrderID= " & orderID & " and POStatus=7"
       'response.Write (SQL)
        Call OpenRs(rsPurchaseOrder, SQL)
        
        if(NOT rsPurchaseOrder.EOF) then
             updateSQL = "Update F_PURCHASEOrder_LOG set image_url='"&ImageURL&"' Where OrderID= " & orderID & " and POStatus=7"
             Conn.Execute(updateSQL)
            ' response.Write(updateSQL)
        else 
             Call LOG_PO_ACTION_WITH_IMAGE(OrderID, ImageURL, Notes, " AND POSTATUS=" & POStatus)
             '//Getting Purchase Items
             SQL ="Select Distinct ORDERITEMID from F_PURCHASEITEM Where OrderID= " & OrderID & " and PIStatus<>7"
           '  response.Write (SQL)
             Call OpenRs(rsPurchaseItems, SQL)
             '//Updating Purchase Item Logs
             for each item in PIArray
              '  response.Write("i am here" & item)
              if(len(item)>0) then
               Call LOG_PI_ACTION_WITH_IMAGE(item, ImageURL, Notes," AND PISTATUS=" & POStatus)
              end if 
             Next

            Call CloseRs(rsPurchaseOrder) 
       
        End If'// 
         
      'Storing Invoice & TAX
       Call SaveInvoiceInfo (OrderID,InvoiceNo,TaxDate)
    End Function

    '// Storing Invoice No & Tax Date
    Function SaveInvoiceInfo(OrderID, InvoiceNo,TaxDate)
        Dim SQLCODE
		
		
        Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)

        SQL ="Select * from F_PURCHASEORDER Where OrderID= " & OrderID
        Call OpenRs(rsPO, SQL)
         While (NOT rsPO.EOF)
            SUPPLIERID = rsPO("SUPPLIERID")
            rsPO.movenext
         Wend

         SQL ="Select * from F_PURCHASEITEM Where OrderID= " & OrderID & " and PIStatus=7"
         Call OpenRs(rsPurchaseItems, SQL)
         While (NOT rsPurchaseItems.EOF)
           'TODO: Need to ADD gross cost in this insert statement
           SQLCODE = "SET NOCOUNT ON;INSERT INTO F_INVOICE (INVOICENUMBER, ORDERID, SUPPLIERID, TAXDATE, USERID,VAT,GROSSCOST,NETCOST,OrderItemId) " &_
					"VALUES ('" & Replace(InvoiceNo,"'","''") & "', " & OrderID & ", "& SUPPLIERID &", '" & TaxDate & "', " &_
					SESSION("USERID") & ",0,"&rsPurchaseItems("GROSSCOST")&",0,"&rsPurchaseItems("ORDERITEMID")&"); SELECT SCOPE_IDENTITY() AS INVOICEID"
			
            'response.write(SQLCODE)
            Conn.Execute(SQLCODE)
			rsPurchaseItems.movenext
         Wend
    End Function

    Function SendEmail(OrderId)
        Dim strRecipient, iMsg, iConf, Flds,emailbody, objBP, piFilter, objImg
		Set Conn = Server.CreateObject("ADODB.Connection")
        Conn.Open(RSL_CONNECTION_STRING)
        SQL = " SELECT DISTINCT C.WorkEmail AS WORKEMAIL " & _
    	      " ,E.FIRSTNAME + ' ' + E.LASTNAME AS RasiedBy " & _
	          " ,BH.FIRSTNAME + ' ' + BH.LASTNAME AS BudgetHolder " & _
              " , PI.ITEMNAME AS ItemName " & _
              " , PI.ITEMDESC AS ItemNotes " & _
              " , O.NAME AS SupplierName " &_
              " , T.GrossTotal AS GrossTotal " &_
              " , PI.GrossCost AS GrossCost " &_
              " , PO.PONotes AS PONotes " &_
			  " , PO.PONAME AS POName " &_
              " FROM F_PURCHASEORDER PO " & _
              "  CROSS APPLY (SELECT SUM(iPI.GROSSCOST) GrossTotal FROM F_PURCHASEITEM iPI WHERE iPI.ORDERID = PO.ORDERID AND iPI.ACTIVE = 1) T " & _
              "   /*  Check from purchase order log if Goods are already approved. */ " &_
              "  CROSS APPLY (SELECT TOP 1 1 AS 'GoodsApproved' FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND (POL.POSTATUS = 18 OR POL.POSTATUS = 5 OR POL.POSTATUS = 7)) GoodsApproved " &_
              "  INNER JOIN F_PURCHASEITEM PI ON PO.ORDERID = PI.ORDERID " & _
              "  INNER JOIN E__EMPLOYEE BH ON ISNULL(PI.APPROVED_BY, PI.USERID) = BH.EMPLOYEEID " & _
              "  INNER JOIN E_CONTACT C ON BH.EMPLOYEEID = C.EMPLOYEEID " & _
              "  INNER JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " & _
              "  INNER JOIN E__EMPLOYEE E ON PI.USERID = E.EMPLOYEEID " & _
              " WHERE PO.ORDERID = " & OrderId
			  
	    Call OpenRs(rsEmail, SQL)
		

         emailbody = getHTMLEmailBody()
         'response.Write ("Before:"&emailbody)
        Set iMsg = CreateObject("CDO.Message")
        
        Set iConf = CreateObject("CDO.Configuration")
        Set Flds = iConf.Fields
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.office365.com"
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true 'Use SSL for the connection (True or False)
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
        Flds ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="outgoingmail@broadlandgroup.org" 
        Flds ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="S@xupad2"
        Flds.Update

        Set iMsg.Configuration = iConf

        iMsg.HTMLBody = emailbody
        
        ' Here's the good part, thanks to some little-known members.
        ' This is a BodyPart object, which represents a new part of the multipart MIME-formatted message.
        ' Note you can provide an image of ANY name as the source, and the second parameter essentially
        ' renames it to anything you want.  Great for giving sensible names to dynamically-generated images.        
        Set objBP = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/BHA_TAB.gif"), "broadlandImage",cdoRefTypeId)
        Set objImg = iMsg.AddRelatedBodyPart(Server.MapPath("/myImages/50years.gif"), "broadland50YearsImage",cdoRefTypeId)
        ' Now assign a MIME Content ID to the image body part.
        ' This is the key that was so hard to find, which makes it 
        ' work in mail readers like Yahoo webmail & others that don't
        ' recognise the default way Microsoft adds it's part id's,
        ' leading to "broken" images in those readers.  Note the
        ' < and > surrounding the arbitrary id string.  This is what
        ' lets you have SRC="cid:broadlandImage" in the IMG tag.
        objBP.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadlandImage>"
        objBP.Fields.Update
        objImg.Fields.Item("urn:schemas:mailheader:Content-ID") = "<broadland50YearsImage>"
        objImg.Fields.Update
        iMsg.From = "noreply@broadlandgroup.org"
        'iMsg.Subject = "PO Invoice has been uploaded"
        iMsg.Subject = "Invoice Received"
	   
        While NOT rsEmail.EOF And NOT rsEmail.BOF
		    ' send email
		    
		    strRecipient  = rsEmail("WORKEMAIL")
		    emailbody = getHTMLEmailBody()
            
            emailbody = Replace(emailbody, "{BudgetHolder}",rsEmail("BudgetHolder"))
             emailbody = Replace(emailbody, "{PONumber}","PO "&OrderId)
             emailbody = Replace(emailbody, "{GrossCost}",FormatNumber(rsEmail("GrossTotal"), 2))
            emailbody = Replace(emailbody, "{POName}",rsEmail("POName"))
             emailbody = Replace(emailbody, "{PONotes}",rsEmail("PONotes"))

            emailbody = Replace(emailbody, "{SupplierName}",rsEmail("SupplierName"))
            emailbody = Replace(emailbody, "{RaisedBy}",rsEmail("RasiedBy"))
            
          ' Check next record to send email.
            rsEmail.movenext
           ' response.Write (emailbody)
	    
        Wend
	    'Sending Email
        'iMsg.To = "shehriyar.zafar@tkxel.com"
         iMsg.To = strRecipient 
         iMsg.HTMLBody = emailbody
	   
	    On Error Resume Next
        iMsg.Send
	    Call CloseRs(rsEmail)                
        Set iMsg = Nothing
        SET iConf = Nothing
        SET Flds = Nothing
        SET objBP = Nothing
         SET objImg = Nothing
		 'Response.Write( "Email send" )
			
    End Function

    Function getHTMLEmailBody()

        getHTMLEmailBody = "<html><head><title>Queued Purchase Order</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "* {font-size: 12px ; font-family: Arial;}"  & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>The invoice for the following PO has been received.</p><p>As the budget holder please approve the invoice for payment using the Invoice Received alert.</p><br/><br/> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                     <tr><td>PO Name:</td><td>{POName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">PO Notes:</td><td>{PONotes}</td></tr> " & _
						   "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(&pound;):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>Invoice received</td></tr> " & _
                           "                     <tr><td>Raised by:</td><td>{RaisedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr>Kind regards <br/><b>The Finance Team</b></tr><tr><td><img src=""cid:broadland50YearsImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "               <td class=""bottomAllignedCell""> " & _
                           "                             <img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function
%>

<html>
<head>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script type="text/javascript" language="javascript">

        function Return_IsInvoiceExist()
        {
            if(<%=IsInvoiceExist%> == 1)
            {
				parent.invoiceExistConfirmation();
            }
            else if(<%=IsRequeuedOrder%> == true || <%=IsDeleteOrder%> == true || <%=IsSaveData%> == true)
			{
                parent.parentReload();
            }
            else
            {
                parent.saveData();
            }
        }

    </script>
</head>
<body onload="Return_IsInvoiceExist();">
</body>
</html>