<%@  language="VBSCRIPT" codepage="1252" %>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST CONST_PAGESIZE = 20

    Call OpenDB()

	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim TotalSum, TotalCount, clist, csql, ccnt, ctotal, view


	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
    '// ****** this was disabled by the request of mark carney - see the javascript validation has been disabled as well
	SQL = "EXEC GET_VALIDATION_PERIOD 3"
	Call OpenRs(rsTAXDATE, SQL)
		YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
		YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "

	TotalSum = 0
	view = 2
	If Request("VIEW") <> "" Then view = Request("VIEW") End If

	clist = ""
	If Request("CreditList") <> "" Then
		clist = Request("CreditList")
		csql = " OR (INV.INVOICEID IN ("&clist&") ) "
	End If

	ccnt = 0
	If Request("CreditCount") <> "" Then ccnt = clng(Request("CreditCount")) End If

	ctotal = 0
	If Request("CreditTotal") <> "" Then ctotal = cdbl(Request("CreditTotal")) End If

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		If (IsNumeric(Request.QueryString("page"))) Then
			intpage = CInt(Request.QueryString("page"))
		Else
			intpage = 1
		End If
	End If

'	SET ORDER BY
	Dim orderBy
	OrderByMatched = 0
	orderBy = " NAME ASC "
	Call SetSort()

'	SET PO FILTER
	POFilter = ""
	If (Request("PODate") <> "") Then
		ProcessingDate = FormatDateTime(CDate(Request("PODate")),2)
		RequiredDate = CDate(Request("PODate"))
	Else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	End If
	ORIGINATORNUMBER = "658670"
	Call getData()
	Call CloseDB()

	' CHECK TO SEE IF CURRENT CHECKBOX HAS BEEN PREVIOUSLY CHECKED
	Function isChecked(invoiceID)

		If clist <> "" Then
			Dim arrChk
			    arrChk = Split(clist, ",")
			For Each item In arrChk
				If CLng(item) = CLng(invoiceID) Then
					isChecked = True
					Exit Function
				End If
			Next
			isChecked = False
		End If

	End Function


	
	Function getData()
		
		Dim strSQL, rsSet, intRecord 

		intRecord = 0
		str_data = ""
		If (Request("VIEW") = 1) Then
		SQLCODE = "SELECT INV.SUPPLIERID, '' AS EMPTY, ISNULL(CRS,0) AS NOTECOUNT, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, NAME, ORGID,  " &_
					"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(INV.INVOICEID) AS TOTALINVOICES, " &_
					"PB.PROCESSDATE AS INPUTBOX, " &_
					"'' AS CHECKBOX " &_				
					"FROM F_INVOICE INV " &_
					"INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
					"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_				
					"LEFT JOIN (SELECT COUNT(*)AS CRS, SUPPLIERID FROM F_INVOICE IV " &_
					"			INNER JOIN F_POBACS P ON P.INVOICEID = IV.INVOICEID " &_
					"			WHERE IV.ISCREDIT = 1 " &_
					"			AND CONVERT(DATETIME,CONVERT(VARCHAR,P.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " &_
					"			GROUP BY SUPPLIERID) CR ON CR.SUPPLIERID = INV.SUPPLIERID " &_
					"	WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD = 2 AND CONVERT(DATETIME,CONVERT(VARCHAR,PB.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " & CompanyFilter &_
					" 	GROUP BY INV.SUPPLIERID, NAME, ORGID, PB.PROCESSDATE, CRS " &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""
		Else
			SQLCODE = "SELECT INV.SUPPLIERID, '' AS EMPTY, ISNULL(SUM(GROSSCOST),0) AS PAYMENTAMOUNT, NAME, ORGID,  " &_
					"		'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(INV.INVOICEID) AS TOTALINVOICES, " &_
					"		'<input type=""text"" name=""txt_DATE' + CAST(ORGID AS VARCHAR) + '"" id=""txt_DATE' + CAST(ORGID AS VARCHAR) + '"" value="""" class=""TEXTBOX200"" maxlength=""20"" /><img src=""/js/FVS.gif"" width=""15"" height=""15"" name=""img_DATE' + CAST(ORGID AS VARCHAR) + '"" id=""img_DATE' + CAST(ORGID AS VARCHAR) + '"" />' AS INPUTBOX, " &_
					"		'<input type=""hidden"" name=""hid_CO' + CAST(ORGID AS VARCHAR) + '"" id=""hid_CO' + CAST(ORGID AS VARCHAR) + '"" value=""' + CAST(COUNT(INV.INVOICEID) AS VARCHAR) + '"" /><input type=""hidden"" name=""hid_VA' + CAST(ORGID AS VARCHAR) + '"" id=""hid_VA' + CAST(ORGID AS VARCHAR) + '"" value=""' + CAST(ISNULL(SUM(GROSSCOST),0) AS VARCHAR) + '"" /><input type=""CHECKBOX"" name=""CHECKITEMS"" id=""CHECKITEMS"" value=""' + CAST(ORGID AS VARCHAR) + '"" />' AS CHECKBOX, " &_				
					"		ISNULL(CREDITNOTECOUNT.CR, 0) AS NOTECOUNT " &_
					"FROM 	F_INVOICE INV " &_
					"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
					"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
					"		LEFT JOIN " &_
					"		    (SELECT ISNULL(COUNT(*),0) AS CR, SUPPLIERID FROM F_CREDITNOTE C " &_
					"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID	" &_
					"			INNER JOIN F_PURCHASEITEM I ON I.ORDERITEMID = CP.ORDERITEMID GROUP BY SUPPLIERID) CREDITNOTECOUNT " &_
					"		    ON CREDITNOTECOUNT.SUPPLIERID = INV.SUPPLIERID " &_
					"WHERE 	(INV.ISCREDIT = 0 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND " &_
					"		PB.INVOICEID IS NULL AND S.PAYMENTTYPE = 2 AND DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), " &_
					"		INV.TAXDATE)) <= '" & FormatDateTime(RequiredDate,1) & "') " & csql & CompanyFilter &_
					"GROUP 	BY INV.SUPPLIERID, NAME, ORGID, CREDITNOTECOUNT.CR " &_
					"ORDER 	BY " + Replace(orderBy, "'", "''") + "" 
		End If

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING
		rsSet.Source = SQLCODE
		rsSet.CursorType = 3
		rsSet.CursorLocation = 3
		rsSet.Open()

		rsSet.PageSize = CONST_PAGESIZE
		my_page_size = CONST_PAGESIZE
		' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
		rsSet.CacheSize = rsSet.PageSize
		intPageCount = rsSet.PageCount 
		intRecordCount = rsSet.RecordCount 

		' Sort pages
		intpage = CInt(Request("page"))
		If intpage = 0 Then intpage = 1 End If
		' Just in case we have a bad request
		If intpage > intPageCount Then intpage = intPageCount End If
		If intpage < 1 Then intpage = 1 End If

		nextPage = intpage + 1
		If nextPage > intPageCount Then nextPage = intPageCount	End If
		prevPage = intpage - 1
		If prevPage <= 0 Then prevPage = 1 End If

		'  double check to make sure that you are not before the start
		' or beyond end of the recordset.  If you are beyond the end, set 
		' the current page equal to the last page of the recordset.  If you are
		' before the start, set the current page equal to the start of the recordset.
		If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount End If
		If CInt(intPage) <= 0 Then intPage = 1 End If

		' Make sure that the recordset is not empty.  If it is not, then set the 
		' AbsolutePage property and populate the intStart and the intFinish variables.
		If intRecordCount > 0 Then
			rsSet.AbsolutePage = intPage
			intStart = rsSet.AbsolutePosition
			If CInt(intPage) = CInt(intPageCount) Then
				intFinish = intRecordCount
			Else
				intFinish = intStart + (rsSet.PageSize - 1)
			End if
		End If

		count = 0
		If intRecordCount > 0 Then
		' Display the record that you are starting on and the record
		' that you are finishing on for this page by writing out the
		' values in the intStart and the intFinish variables.
		str_data = str_data & "<tbody class='CAPS'>"
			' Iterate through the recordset until we reach the end of the page
			' or the last record in the recordset.
			For intRecord = 1 to rsSet.PageSize
				If Cint(rsSet("NOTECOUNT")) > 0 Then
					str_data = str_data & "<tr bgcolor=""whitesmoke"" title=""Click to attach credit note"" style=""cursor:pointer"" onclick=""toggle('SUPP"&rsSet("SUPPLIERID")&"')"">"
				Else
					str_data = str_data & "<tr>"
				End If
				str_data = str_data & "<td width=""250px"">" & rsSet("NAME") & "</td>" &_
						"<td width=""100px"" align=""right""><div id='AMOUNT"&rsSet("SUPPLIERID")&"'>" & FormatNumber(rsSet("PAYMENTAMOUNT")) &	"</div></td>" &_
						"<td align=""center"" width=""40px"">" & rsSet("TOTALINVOICES") & "</td>"
						If (Request("VIEW") = 1) Then
							str_data = str_data & "<td width=""250px"" onclick=""CancelBubble()"" align=""right"">" & rsSet("INPUTBOX") & "</td>"
						Else
							str_data = str_data & "<td width=""250px"" onclick=""CancelBubble()"" align=""left"">" & rsSet("INPUTBOX") & "</td>"
						End If
						str_data = str_data & "<td onclick=""CancelBubble()"" align=""center"">" & rsSet("CHECKBOX") & "</td></tr>"

				count = count + 1
				If Cint(Request("VIEW")) = 1 Then
					If Cint(rsSet("NOTECOUNT")) > 0 Then 
						str_data = str_data & check_for_readonly_creditnotes(rsSet("SUPPLIERID"))
					End If
				Else
					If Cint(rsSet("NOTECOUNT")) > 0 Then 
						str_data = str_data & check_for_creditnotes(rsSet("SUPPLIERID"))
					End If
				End If
				TotalSum = TotalSum + rsSet("PAYMENTAMOUNT")
				rsSet.movenext()
				If rsSet.EOF Then Exit for

			Next

			str_data = str_data & "</tbody>"

			'ensure table height is consistent with any amount of records
			Call fill_gaps()

			' links
			str_data = str_data &_
			"<tfoot><tr><td colspan=""7"" style=""border-top:2px solid #133e71"" align=""center"">" &_
			"<a style=""cursor:pointer"" onclick=""gotopage(1)""><b><font color=""blue"">First</font></b></a> " &_
			"<a style=""cursor:pointer"" onclick=""gotopage(" & prevpage & ")""><b><font color=""blue"">Prev</font></b></a>" &_
            " Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount &_
			" <a style=""cursor:pointer"" onclick=""gotopage(" & nextpage & ")""><b><font color=""blue"">Next</font></b></a>" &_ 
			" <a style=""cursor:pointer"" onclick=""gotopage(" & intPageCount & ")""><b><font color=""blue"">Last</font></b></a>" &_
			"</td></tr></tfoot>"

		End If

		If intRecord = 0 Then
			str_data = "<tr><td colspan=""7"" align=""center""><b>No invoices found in the system for the specified criteria!!!</b></td></tr>" &_
						"<tr style=""height:3px""><td></td></tr>"
		End If

		rsSet.close()
		Set rsSet = Nothing

	End function

'	THIS FUNCTION CHECKS IF THERE ARE ANY CREDIT NOTES AVAILABLE FOR THE SPECIFIED SUPPLIER
'	ONLY CHECKS FOR CREDIT NOTES WITH A STATUS OF 6 -- RECEIVED
	Function check_for_creditnotes(SID)

		Dim str_CData, counter
		str_CData = ""
		counter = 0
		SQL = "SELECT 	INV.INVOICEID, INV.SUPPLIERID, ISNULL(SUM(CR.GROSSCOST),0) AS GROSSCOST, " &_
				"		INV.INVOICENUMBER AS CRNO, CR.CNNAME " &_
				"FROM 	F_INVOICE INV " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
				"		LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
				"		INNER JOIN " &_
				"		    (SELECT SUM(GROSSCOST) AS GROSSCOST, O.INVOICEID, C.CNNAME " &_
				"			FROM F_CREDITNOTE C " &_
				"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CP.ORDERITEMID " &_
				"			INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = PI.ORDERITEMID " &_
				"			 WHERE PI.ACTIVE = 1 GROUP BY O.INVOICEID, C.CNNAME ) CR ON " &_
				"			 CR.INVOICEID = INV.INVOICEID " &_
				"	WHERE 	INV.ISCREDIT = 1 AND INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND   " &_
				"		S.PAYMENTTYPE = 2 AND INV.SUPPLIERID = " & SID & CompanyFilter &_
				"	GROUP 	BY INV.SUPPLIERID, INV.INVOICENUMBER, INV.INVOICEID, CR.CNNAME " &_
				"	ORDER BY INV.INVOICEID "

		Call OpenRs(rsCSet, SQL)
		While Not rsCSet.EOF

			If counter = 0 Then
				str_CData = str_CData & "<thead>"
			End If

			counter = counter + 1
			str_CData = str_CData & "<tr bgcolor=""beige"" style=""display:block"" id='SUPP"&rsCSet("SUPPLIERID")&"'>" &_
					"<td>" & rsCSet("CRNO") & "</td>" &_
					"<td align=""right"">" & FormatNumber(abs(rsCSet("GROSSCOST"))) &"</td><td></td><td></td>"
					If isChecked(rsCSet("INVOICEID")) Then
						str_CData = str_CData & "<td align=""center""><input type=""checkbox"" name=""credits"" id='"&rsCSet("INVOICEID")&"' value='"&rsCSet("INVOICEID")&"' onclick=""calculate_me('AMOUNT"&rsCSet("SUPPLIERID")&"', "&abs(rsCSet("GROSSCOST"))&", '"&rsCSet("INVOICEID")&"')"" checked=""checked""></td></tr>" 
					Else
						str_CData = str_CData & "<td align=""center""><input type=""checkbox"" name=""credits"" id='"&rsCSet("INVOICEID")&"' value='"&rsCSet("INVOICEID")&"' onclick=""calculate_me('AMOUNT"&rsCSet("SUPPLIERID")&"', "&abs(rsCSet("GROSSCOST"))&", '"&rsCSet("INVOICEID")&"')""></td></tr>" 
					End If
			rsCSet.movenext()
		Wend
		str_CData = str_CData & "</thead>"
		If counter = 0 Then str_CData = "" End If
		Call CloseRs(rsCSet)
		check_for_creditnotes = str_CData

	End Function

'	THIS FUNCTION CHECKS IF THERE ARE ANY CREDIT NOTES AVAILABLE FOR THE SPECIFIED SUPPLIER
'	ONLY CHECKS FOR CREDIT NOTES WITH A STATUS OF 6 -- RECEIVED
	Function check_for_readonly_creditnotes(SID)

		Dim str_CData, counter
		str_CData = ""
		counter = 0
		SQL = "SELECT 	INV.INVOICEID, INV.SUPPLIERID, ISNULL(SUM(CR.GROSSCOST),0) AS GROSSCOST, " &_
				"		INV.INVOICENUMBER AS CRNO, CR.CNNAME " &_
				"FROM 	F_INVOICE INV " &_
				"		INNER JOIN S_ORGANISATION S ON S.ORGID = INV.SUPPLIERID " &_
				"		INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
				"		INNER JOIN  " &_
				"		    (SELECT SUM(GROSSCOST) AS GROSSCOST, O.INVOICEID, C.CNNAME " &_
				"			FROM F_CREDITNOTE C " &_
				"			INNER JOIN F_CREDITNOTE_TO_PURCHASEITEM CP ON CP.CNID = C.CNID " &_
				"			INNER JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = CP.ORDERITEMID " &_
				"			INNER JOIN F_ORDERITEM_TO_INVOICE O ON O.ORDERITEMID = PI.ORDERITEMID " &_
				"			 WHERE PI.ACTIVE = 1 GROUP BY O.INVOICEID, C.CNNAME ) CR ON  " &_
				"			 CR.INVOICEID = INV.INVOICEID " &_
				"	WHERE 	INV.ISCREDIT = 1 AND INV.CONFIRMED = 1 AND CONVERT(DATETIME,CONVERT(VARCHAR,PB.PROCESSDATE,103),103) = '" & FormatDateTime(RequiredDate,1) & "' " &_
				"		AND S.PAYMENTTYPE = 2 AND INV.SUPPLIERID = " & SID & CompanyFilter &_
				"	GROUP 	BY INV.SUPPLIERID, INV.INVOICENUMBER, INV.INVOICEID, CR.CNNAME " &_
				"	ORDER BY INV.INVOICEID "

		Call OpenRs(rsCSet, SQL)
		While Not rsCSet.EOF

			if counter = 0 Then
				str_CData = str_CData & "<thead>"
			End If

			counter = counter + 1
			str_CData = str_CData & "<tr bgcolor=""beige"" style='display:block' id='SUPP"&rsCSet("SUPPLIERID")&"'>" &_
					"<td>&nbsp;&nbsp;&nbsp;&nbsp;" & rsCSet("CRNO") & "</TD>" &_
					"<td align=""right"">" & FormatNumber(abs(rsCSet("GROSSCOST"))) &"&nbsp;</td><td colspan=""3""></td>"
			rsCSet.movenext()
		Wend
		str_CData = str_CData & "</thead>"
		If counter = 0 Then str_CData = "" End If
		Call CloseRs(rsCSet)
		check_for_readonly_creditnotes = str_CData

	End Function

	' pads table out to keep the height consistent
	Function fill_gaps()

		Dim tr_num, cnt
		cnt = 0
		tr_num = my_page_size - count
		while (cnt < tr_num)
			str_data = str_data & "<tr><td colspan=""7"" align=""center"">&nbsp;</td></tr>"
			cnt = cnt + 1
		wend

	End Function

	Function SetSort()
		if (Request("CC_Sort") <> "") then 
			PotentialOrderBy = Request("CC_Sort")
			for i=0 to UBound(SortASC)
				if SortASC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
				if SortDESC(i) = PotentialOrderBy then
					orderBy = PotentialOrderBy
					Exit For
				end if
			next
		end if
	End Function
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Bacs List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body
        {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/FormValidation.js"></script>
    <script language="JavaScript" type="text/javascript">

	var credit_total = <%=ctotal%>
	var credit_count = <%=ccnt%>
	var checked_list = "<%=clist%>"

	function real_date(str_date){		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");
		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);		
	}
		
	function gotopage(pageno){	
		location.href = "bacsDD.asp?VIEW=<%=VIEW%>&PODate=<%=Request("PODate")%>&CreditTotal="+credit_total+"&CreditCount="+credit_count+"&page="+pageno+"&CreditList="+checked_list + "&Company=<%=Company%>";
	}	

//	OPEN OR CLOSE CREDIT NOTE ROWS
	function toggle(what){	
		what = new String(what)
		var coll = document.getElementsByName(what);
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.display == "none")
				  		coll[i].style.display = "block";
					else
						coll[i].style.display = "none";
	}
	
	// REMOVE UNCHECKED ITEM FROM ITEM LIST
	// THIS LIST IS USED TO PASS CHECKED ITEMS TO NEXT PAGE
	function remove_item_from_list(chk){

		var strChk = new String(chk);
		var arrChk = checked_list.split(",")
		var lenChk = arrChk.length
		checked_list = ""
		var cnt = 0

		for (i=0 ; i < lenChk ; i++)
			if (arrChk[i] != strChk){
				cnt +=1;
				if (parseInt(cnt) == 1)
					checked_list = checked_list + arrChk[i];
				else
					checked_list = checked_list + "," + arrChk[i];
			}
	}

	// THIS FUNCTION IS CALLED IF A CREDIT NOTE CHECKBOX IS CHECKED. IT CALCULATES AND UPDATES THE PAYMENY AMOUNT
	// IT ALSO CONTROLS THE CONTENT OF THE CHECKED LIST WHICH IS USED TO PASS THE IDS OF CHECKED CREDIT NOTE
	// BOXES FROM PAGE TO PAGE
	function calculate_me(supplierid, rowcost, chkbox){

		var payment_amount, credit_amount, result

		// first convert currency number to real number
		payment_amount = new String(document.getElementById(""+supplierid+"").innerHTML)
		payment_amount = parseFloat(payment_amount.replace(",",""));
		credit_amount	= parseFloat(rowcost);

		// WE EITHER ADD THE AMOUNT OR DEDUCT DEPENDING ON WHETHER THE CHECKBOX IS
		// CHECKED OR UNCHECKED
		if (document.getElementById(""+chkbox+"").checked == true){
			result	= FormatCurrency(payment_amount - credit_amount);
			credit_total = parseFloat(Math.abs(credit_total)) + parseFloat(Math.abs(credit_amount));
			credit_count += 1
			if (credit_count == 1)
				checked_list = checked_list + chkbox;
			else
				checked_list = checked_list + "," + chkbox;
			}
		else {
			result	= FormatCurrency(payment_amount + credit_amount);
			credit_total = parseFloat(Math.abs(credit_total)) - parseFloat(Math.abs(credit_amount));
			credit_count -= 1
			remove_item_from_list(chkbox);
			}

		// WE CANNOT ALLOW USER TO CONTINUE IF PAYMENT AMOUNT BECOMES NEGATIVE AND MUST RESET THE VALUE
		if (result < 0){
			alert("You cannot apply this credit note otherwise the payment will become negative.");
			document.getElementById(""+chkbox+"").checked = false;
			credit_total = parseFloat(Math.abs(credit_total)) - parseFloat(Math.abs(credit_amount));
			credit_count -= 1
			remove_item_from_list(chkbox);
			return false;
			}
		else {
			document.getElementById(""+supplierid+"").innerHTML = result;
			}
	}
	
	function RemoveBad() {	
		strTemp = event.srcElement.value;
		strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
		event.srcElement.value = strTemp;
	}
	
	function CancelBubble(){
	event.cancelBubble = true
	}

	var FormFields = new Array()

	function SendDD() {

 		var YStart = new Date("<%=YearStartDate%>")
	    var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
	    var YEnd = new Date("<%=YearendDate%>")
	    var YStartPlusTime = new Date("<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>")
	    var YEndPlusTime = new Date("<%=FormatDateTime(DateAdd("m", 1, YearStartDate),1) %>")

		chkitems = document.getElementsByName("CHECKITEMS")
		FormFields.length = 0
		iCounter = 0
		TotalValue = 0
		TotalCount = 0
//console.log('number of checkboxes : '+ chkitems.length);
		if (chkitems.length){
			for (j=0; j<chkitems.length; j++) {
				ManualError ("img_DATE" + chkitems[j].value, "", 3)
				}
			for (i=0; i<chkitems.length; i++) {
				if (chkitems[i].checked) {
//console.log('checkbox val : '+ chkitems[i].value);

// fiscal year script
	  //  if  ( real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) < YStart || real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) > YEnd ) {
	  //      // outside current financial year
	  //      if ( TodayDate <= YEndPlusTime ) {
	  //          // inside grace period
	  //          if ( real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) < YStartPlusTime || real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) > YEnd) {
	  //              // outside of last and this financial year
          //          	alert("Please enter a date for either the previous\nor current financial year\n(i.e. between '<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate),1) %>' and '<%=YearendDate%>')");
          //          	return false;
	  //          } else {
	  //              // date must be ok?
	  //          }
	  //      } else {
	  //          // we're outside the grace period and also outside this financial year
          //      alert("Please enter a date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
          //      return false;
	  //	    }
	  //  }	

		
		   //if  ( real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) < YStart || real_date(document.getElementById("txt_DATE" + chkitems[i].value).value) > YEnd ) {
		   //   // we're outside the grace period and also outside this financial year
           //     alert("Please enter a date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
           //     return false;
		   // }
					FormFields[iCounter] = "txt_DATE" + chkitems[i].value + "|DD Date|DATE|Y"
					TotalCount += parseInt(document.getElementById("hid_CO" + chkitems[i].value).value)
					TotalValue += parseFloat(document.getElementById("hid_VA" + chkitems[i].value).value)
//console.log('TotalValue : ' + TotalValue);
//console.log('TotalSum : '+ <%=TotalSum%>);
					iCounter ++
					}
				}
			}
		if (TotalCount == 0) {
			alert("No lines have been selected. To select a line click on the\nrespective checkbox and enter a date for when the DD was taken,\notherwise the row will not be processed.")
			return false
			} 
			//return false;
		if (!checkForm()) return false
		//var recvalue = parseFloat(<%=TotalSum%>) - parseFloat(credit_total);
        var recvalue = parseFloat(TotalValue) - parseFloat(credit_total);
		result = confirm("You have selected '" + TotalCount + "' invoice(s) to be processed at a total value of '�" + FormatCurrency(recvalue) +"'.\nDo you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")
		if (!result) return false
		thisForm.hid_CreditList.value = checked_list;
		thisForm.action = "ServerSide/BacsList_DD.asp?pROCESSINGdATE=<%=Request("PODate")%>&Company=<%=Company%>"
		thisForm.submit()
		}

	// DOESNT WORK AT THE MO
	<% if Cint(Request("ER89" & Replace(Date, "/", ""))) = 1 then %>
		alert("The invoices which are ready to be paid have changed on the system.\nPlease check the data and re-process.")
	<% elseif Cint(Request("ER89" & Replace(Date, "/", ""))) = 2 then %>
		alert("The credit notes which are ready to be paid have changed on the system.\nPlease check the data and re-process.")
	<% end if %>
	
    </script>
</head>
<body onload="initSwipeMenu(1);preloadImages()" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post" action="">
    <table width="750" class="RSLBlack">
        <tr>
            <td>
                <b>&nbsp;Purchase Order Direct Debit List</b>
                <input type="hidden" name="pROCESSINGdATE" id="pROCESSINGdATE" class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>" />
            </td>
            <td align="right">
                &nbsp<a href="BacsDisplay.asp?date=<%=ProcessingDate%>&Company=<%=Company %>"><font color="blue"><b>BACK to
                    Bacs Calendar</b></font></a>
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133e71">
                <img src="images/spacer.gif" width="750" height="1" alt="" />
            </td>
        </tr>
    </table>
    <table width="750" cellpadding="1" cellspacing="0" class='TAB_TABLE' style='border-collapse: collapse;
        behavior: url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor="steelblue" border="1">
        <thead>
            <tr>
                <td class='TABLE_HEAD' width="250">
                    <a href="BacsDD.asp?PODATE=<%=Request("PODate")%>&CC_Sort=NAME+ASC" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Name&nbsp;<a
                            href="BacsList.asp?PODATE=<%=Request("PODate")%>&CC_Sort=NAME+DESC&Company=<%=Company %>" style="text-decoration: none"><img
                                src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="100">
                    <a href="BacsDD.asp?PODATE=<%=Request("PODate")%>5&CC_Sort=ACCOUNTNAME+ASC" style="text-decoration: none">
                        <img src="/myImages/sort_arrow_up.gif" border="0" alt="Sort Ascending" /></a>&nbsp;Amount&nbsp;<a
                            href="BacsList.asp?PODATE=<%=Request("PODate")%>&CC_Sort=ACCOUNTNAME+DESC&Company=<%=Company %>" style="text-decoration: none"><img
                                src="/myImages/sort_arrow_down.gif" border="0" alt="Sort Descending" /></a>
                </td>
                <td class='TABLE_HEAD' width="40">
                    &nbsp;Invoices&nbsp;
                </td>
                <td class='TABLE_HEAD' width="250">
                    &nbsp;Direct Debit Taken&nbsp;
                </td>
                <td class='TABLE_HEAD' width="50">
                    &nbsp;
                </td>
            </tr>
            <tr style="height: 3px">
                <td colspan="6" align="center" style='border-bottom: 2px solid #133e71' class='TABLE_HEAD'>
                </td>
            </tr>
        </thead>
        <%=str_data%>
    </table>
    <% If (Request("VIEW") <> 1) Then %>
    <table width="750">
        <tr>
            <td align="right">
                &nbsp;<input class="RSLButton" type="button" onclick="SendDD()" value=" SAVE DD " />
            </td>
        </tr>
    </table>
    <% End If %>
    <input type="hidden" name="hid_CreditSum" id="hid_CreditSum" />
    <input type="hidden" name="hid_CreditCount" id="hid_CreditCount" />
    <input type="hidden" name="hid_CreditList" id="hid_CreditList" />
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
