<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	Call OpenDB()
	Call GetCurrentYear()
	FY = GetCurrent_YRange

	DISTINCT_SQL = "F_COSTCENTRE CC " &_
			"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
			"WHERE EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"

	Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
	Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' tabindex=5")
	Call BuildSelect(lstEmployee, "sel_EMPLOYEE", "E__EMPLOYEE E INNER JOIN E_JOBDETAILS JD ON JD.EMPLOYEEID = E.EMPLOYEEID WHERE JD.ACTIVE = 1", "E.EMPLOYEEID, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME", "FULLNAME", NULL, NULL, NULL, "textbox200", " tabindex=1")	

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	SQL = "SELECT * FROM F_FISCALYEARS WHERE YSTART <= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	Call CloseDB()

	TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Purchase Order > Create Tenant Reimbursement</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	var FormFields = new Array()
	
	function SetChecking(){
		FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
		FormFields[1] = "sel_HEAD|Head|SELECT|Y"
		FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
		FormFields[3] = "txt_CHEQUE|Cheque|TEXT|Y"
		FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
		FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
		FormFields[6] = "sel_CUSTOMER|Customer|SELECT|Y"
		FormFields[7] = "txt_DATE|Cheque Date|DATE|Y"
		FormFields[8] = "txt_TENANCY|Tenant Number|TEXT|Y"		
		}
		
	function LOADNOW(){
		PopulateListBox("<%=optionvalues%>", "<%=optiontext%>");
		THISFORM.sel_EXPENDITURE.value = "<%=EXPID%>"
		PopulateListBox("<%=optionvalues2%>", "<%=optiontext2%>", 2);			
		THISFORM.sel_HEAD.value = "<%=HeadID%>"			
		THISFORM.EXPENDITURE_LEFT_LIST.value = "<%=theamounts%>";
		THISFORM.EMPLOYEE_LIMIT_LIST.value = "<%=EmployeeLimits%>";
		SetPurchaseLimits()			
		}

	function PopulateListBox(values, thetext, which){
		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");
		if (which == 2)
			var selectlist = document.forms.THISFORM.sel_HEAD;
		else 
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE;	
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");
			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
	
	function Select_OnchangeFund(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		THISFORM.IACTION.value = "gethead";
		THISFORM.action = "ServerSide/GetHeads.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}
	
	function Select_OnchangeHead(){
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		document.getElementById("txt_EMLIMITS").value = "0.00";								
		PopulateListBox("", "Waiting for Data...", 1);	
		THISFORM.IACTION.value = "change";
		THISFORM.action = "ServerSide/GetExpenditures.asp";
		THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
		THISFORM.submit();
		}

	function SetPurchaseLimits(){
		MatchRow = -1
				
		eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
		eEMLIMITS = eEMLIMITS.split(";;");

		document.getElementById("txt_EXPBALANCE").value = FormatCurrency(parseFloat(eAmounts[eIndex]));
		document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	
		document.getElementById("hid_CCBALANCE").value = FormatCurrency(parseFloat(document.getElementById("hid_TOTALCCLEFT").value));				
		}	

	var RowCounter = 0
	var ExpenditureArray = new Array()
	var ExpValueArray = new Array()
	var CCArray = new Array()	
		
	function SaveItem(){
		SetChecking()		
		if (!checkForm()) return false

		GrossCost = document.getElementById("txt_GROSSCOST").value
		EmployeeLimit = document.getElementById("txt_EMLIMITS").value
		ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value

		//check the costcentre will not go over...
		//note: have to remove the total of any other entries aswell.
		CCLeft = document.getElementById("hid_CCBALANCE").value
		if ((parseFloat(CCLeft) < parseFloat(GrossCost) )){
			alert("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the Cost Centre budget remaining (�" + FormatCurrencyComma(CCLeft) + ") for the selected item.\nTherefore this item cannot be entered onto the system.");			
			return false;
			}		
			
		//check the expenditure will not go over...
		ExpLeft = document.getElementById("txt_EXPBALANCE").value
		if ((parseFloat(ExpLeft) < parseFloat(GrossCost) )){
			answer = confirm("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget remaining (�" + FormatCurrencyComma(ExpLeft) + ") for the selected item.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");			
			if (!answer) return false
			}
		
		if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)){
			document.getElementById("ISQUEUED").value = 1
			result = confirm("The total cost is greater than your employee limit in the selected Expenditure.\nDo you wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
			if (!result) return false
			}
		THISFORM.action = "ServerSide/CreateTenantReimbursement_svr.asp";
		THISFORM.target = "";
		THISFORM.submit();	
		}

	function GetCustomers(){
		THISFORM.sel_CUSTOMER.outerHTML = "<select name='sel_CUSTOMER' class='textbox200'><option value=''>Please enter a tenant number</option></select>"		
		if (THISFORM.txt_TENANCY.value != "") {
			THISFORM.action = "Serverside/GetCustomers.asp"
			THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>"
			THISFORM.submit()
			}
		}		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table><tr><td>

<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0>
    <tr><td height=7></td></tr>
	<tr> 
	  <td valign="top" height="20"> 
		<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0>
		  <TR> 
			<TD ROWSPAN=2 nowrap style='border-top:1px solid black;border-left:1px solid black;border-right:1px solid black'>&nbsp;&nbsp;Tenant Reimbursement&nbsp;&nbsp;</TD>
			<TD HEIGHT=19></TD>
		  </TR>
		  <TR> 
			<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" HEIGHT=1></TD>
		  </TR>
		</TABLE>
	  </TD>
	</TR>
	<tr> 
	  <td valign="top" STYLE='BORDER-LEFT:1px solid #133E71; BORDER-RIGHT:1px solid #133E71; BORDER-BOTTOM:1px solid #133E71'> 
	 <iframe  src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style='display:NONE'></iframe> 

 <TABLE cellspacing=0 cellpadding=3>
    <FORM name="THISFORM" method="POST">			
	<TR bgcolor=steelblue style='color:white'> 
        <TD colspan=9><b>&nbsp; </b></TD>
	</TR>
	<tr>
		<td><b>Tenancy</b></td>
		<td><input name="txt_TENANCY" type="text" class="textbox200" maxlength="50" tabindex=3 VALUE="" onblur="GetCustomers()"></td>
		<TD><image src="/js/FVS.gif" name="img_TENANCY" width="15px" height="15px" border="0"></TD>							
	</tr>
	<TR>
		<TD>Customer : </TD><TD><select name="sel_CUSTOMER" class="textbox200">
								  <option value="">Please enter a tenant number</option>
                            </select></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_CUSTOMER"></td>
	</TR>
	<TR>
		<TD>Cheque Date : </TD><TD><input name="txt_DATE" type="text" class="textbox200" maxlength="50" tabindex=3 VALUE=""></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_DATE"></td>
	</TR>
	<TR>
		<TD>Cheque Num: </TD><TD><input name="txt_CHEQUE" type="text" class="textbox200" maxlength="50" tabindex=3 VALUE=""></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_CHEQUE"></td>
	</TR>
	<TR>
		<TD valign=top>Notes : </TD><TD><textarea tabindex=3 name="txt_ITEMDESC" rows=7 class="TEXTBOX200"  style='overflow:hidden;border:1px solid #133E71'></textarea></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMDESC"></td>
	</TR>
     <TR><TD></TD><TD></TD></tr>
	<tr>
	<TD>&nbsp;Cost Centre : </TD><TD><%=lstCostCentres%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_COSTCENTRE"></td>
	</tr>	
	<tr>
	<TD>&nbsp;Head : </TD><TD>
		<select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()" tabindex=4>
		<option value="">Please Select a Cost Centre...</option>
	  </select>
	  </TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_HEAD"></td>	
	</tr>
	<tr>
		<TD>&nbsp;Expenditure : </TD><TD>                        
		<select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()" tabindex=4>
		  <option value="">Please select a Head...</option>
		</select>
		</TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_EXPENDITURE"></td>	
	</tr>
	<tr>
		<TD NOWRAP>&nbsp;Exp Balance : </TD><TD><input type="text" name="txt_EXPBALANCE" class="textbox200" tabindex=-1 readonly></TD><td></td>	
	</tr>
	<tr>
		 <td>&nbsp;Spend Limit : </td>
		  <td><input type="text" name="txt_EMLIMITS" class="textbox200" readonly tabindex=-1>
		 </td>
	</tr>
	<tr>
		<TD>&nbsp;Total : </TD><TD>
			<input style='text-Align:right' name="txt_GROSSCOST" type="text"  class="textbox200"  maxlength=20 size=11  onBlur="event.srcElement.style.textAlign = 'right'" onFocus="alignLeft()" tabindex=5 value="">
		  </TD>
		  <td><img src="/js/FVS.gif" width=15 height=15 name="img_GROSSCOST"></td>			
	</tr>
	<tr>
		<TD align=right colspan=2>
			<INPUT TYPE=HIDDEN NAME="IACTION">
			<INPUT TYPE=HIDDEN NAME="ISQUEUED">			
			<INPUT TYPE=HIDDEN NAME="ORDERID">						
		  	<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST">
		  	<INPUT TYPE=HIDDEN NAME="EMPLOYEE_LIMIT_LIST">
			<INPUT TYPE=HIDDEN NAME="hid_TOTALCCLEFT">								
			<INPUT TYPE=HIDDEN NAME="hid_CCBALANCE">											
			<input type=RESET Name="ResetButton" value=" RESET " class="RSLButton" tabindex=6>&nbsp;
			<input type=button Name="AmendButton" value=" SAVE " class="RSLButton" onclick="SaveItem()" tabindex=6>
		</TD>
	</tr>
    </FORM>			  
</TABLE>		

          </td>
        </tr>
      </table>

</td><td valign=top>

<table align=right valign=top>
<tr><td>
<br><br>
&nbsp;&nbsp;&nbsp;<b>Steps to create a Tenant Reimbursement</b>
<ul>
<li>Enter the tenancy number of the tenant</li>
<li>Select the main customer of the tenancy</li>
<li>Enter the cheque number and date</li>
<li>Select the appropriate expenditure by selecting the cost centre and head first.</li>
<li>Enter the total cost of the reimbursement</li>
<li>Click 'SAVE'</li>
</ul>
</td></tr></table>

</td></tr></table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
