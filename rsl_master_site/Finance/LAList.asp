<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (5)	 'USED BY CODE
	Dim DatabaseFields (5)	 'USED BY CODE
	Dim ColumnWidths   (5)	 'USED BY CODE
	Dim TDSTUFF        (5)	 'USED BY CODE
	Dim TDPrepared	   (5)	 'USED BY CODE
	Dim ColData        (5)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (5)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (5)	 'All Array sizes must match	
	Dim TDFunc		   (5)

	ColData(0)  = "Name|NAME|150"
	SortASC(0) 	= "NAME ASC"
	SortDESC(0) = "NAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Account Holder|ACCOUNTNAME|200"
	SortASC(1) 	= "ACCOUNTNAME ASC"
	SortDESC(1) = "ACCOUNTNAME DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Sort Code|SORTCODE|85"
	SortASC(2) 	= "SORTCODE ASC"
	SortDESC(2) = "SORTCODE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Acc No|ACCOUNTNUMBER|100"
	SortASC(3) 	= "ACCOUNTNUMBER ASC"
	SortDESC(3) = "ACCOUNTNUMBER DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Amount|PAYMENTAMOUNT|100"
	SortASC(4) 	= "PAYMENTAMOUNT ASC"
	SortDESC(4) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	ColData(5)  = "Refunds|TOTALREFUNDS|40"
	SortASC(5) 	= ""
	SortDESC(5) = ""	
	TDSTUFF(5)  = " ""align='right'"" "
	TDFunc(5) = ""
	
	PageName = "LAList.asp"
	EmptyText = "No LA Refunds found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PODate") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("PODate")),2)
		RequiredDate = CDate(Request("PODate"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ORIGINATORNUMBER = "658670"
	
	if (Request("VIEW") = 1) then
		SQLCODE = "SELECT ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, S.ACCOUNTNAME, S.NAME, S.SORTCODE, S.ACCOUNTNUMBER, " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(RJ.JOURNALID) AS TOTALREFUNDS " &_
				"FROM F_RENTJOURNAL RJ " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
				"INNER JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
				"WHERE RJ.PAYMENTTYPE = 23 AND LB.PROCESSDATE = '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY S.NAME, S.ACCOUNTNAME, S.SORTCODE, S.ACCOUNTNUMBER	 " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""
	else
		SQL = "SELECT COUNT(RJ.JOURNALID) AS TOTALCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS TOTALCOST " &_
				"FROM F_RENTJOURNAL RJ " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
				"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
				"WHERE RJ.PAYMENTTYPE = 23 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_

		OpenDB()
		Call OpenRs(rsCheck, SQL)
		TotalSum = 0
		TotalCount = 0
		if (NOT rsCheck.EOF) then
			TotalSum = rsCheck("TOTALCOST")
			TotalCount = rsCheck("TOTALCOUNT")
		end if
		Call CloseRs(rsCheck)

		SQLCODE = "SELECT ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, S.ACCOUNTNAME, S.NAME, S.SORTCODE, S.ACCOUNTNUMBER, " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(RJ.JOURNALID) AS TOTALREFUNDS " &_
				"FROM F_RENTJOURNAL RJ " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
				"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
				"WHERE RJ.PAYMENTTYPE = 23 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY S.NAME, S.ACCOUNTNAME, S.SORTCODE, S.ACCOUNTNUMBER	 " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""
		
	end if
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> LA Refund List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

function SaveResults(){
	result = confirm("The total number of LA Refunds that will be reconciled will be: <%=TotalCount%>.\nThe value of these LA Refunds will be: <%=FormatCurrency(TotalSum)%>.\nDo you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")
	if (!result) return false
	location.href = "ServerSide/LAList_CSV.asp<%="?CC_Sort=" & orderBy%>&pROCESSINGdATE=<%=Request("PODate")%>&TotalCount=<%=TotalCount%>&TotalSum=<%=TotalSum%>"
	}

<% if (Request("ERR97" & Replace(Date, "/", "")) = 1) then %>
alert ("The process was aborted as more LA Refunds have been entered on to the system.\nPlease check the data and re-process.")
<% end if %>
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack" width=750><tr>
<% if (Request("VIEW") <> 1) then %>
<td><input type=HIDDEN name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>" style='width:135px;'></td>
<td><input type=button class="RSLButton" value=" Export to CSV " onclick="SaveResults()"></td>
<% end if %>
<td nowrap> <b>&nbsp;LA Refund Bacs List</b>
</td>
<td align=right width=100%>
&nbsp<a href='LADisplay.asp?date=<%=ProcessingDate%>'><font color=blue><b>BACK to LA Refunds Calendar</b></font></a>
</td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>