<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim FromDate,ToDate      'Max and Min date of the displayed record

	Dim TableTitles    (6)	 'USED BY CODE
	Dim DatabaseFields (6)	 'USED BY CODE
	Dim ColumnWidths   (6)	 'USED BY CODE
	Dim TDSTUFF        (6)	 'USED BY CODE
	Dim TDPrepared	   (6)	 'USED BY CODE
	Dim ColData        (6)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (6)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (6)	 'All Array sizes must match	
	Dim TDFunc		   (6)

	THE_TABLE_HIGH_LIGHT_COLOUR = "blue"
		
	
	ColData(0)  = "TENANCY|TENANCYID|80"
	SortASC(0) 	= "TENANCYID ASC"
	SortDESC(0) = "TENANCYID DESC"	
	TDSTUFF(0)  = ""
	TDFunc(0) = ""		

	ColData(1)  = "CUSTOMER|CUSTOMER|170"
	SortASC(1) 	= "CUSTOMER ASC"
	SortDESC(1) = "CUSTOMER DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "PAYMENT TYPE|PAYMENTTYPE|212"
	SortASC(2) 	= "PAYMENTTYPE ASC"
	SortDESC(2) = "PAYMENTTYPE DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		
	
	ColData(3)  = "PROCESS DATE|PROCESSDATE|170"
	SortASC(3) 	= "PROCESSDATE ASC"
	SortDESC(3) = "PROCESSDATE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""	
	
    ColData(4)  = "CORRECTION PERIOD|CORRECTIONPERIOD|255"
	SortASC(4) 	= "CORRECTIONPERIOD ASC"
	SortDESC(4) = "CORRECTIONPERIOD DESC"	
	TDSTUFF(4)  = ""
	TDFunc(4) = ""	

	ColData(5)  = "CREDIT|CREDIT|80"
	SortASC(5) 	= "CREDIT ASC"
	SortDESC(5) = "CREDIT DESC"	
	TDSTUFF(5)  = " ""align='right'"" "
	TDFunc(5) = "FormatCurrency(|)"	
	
	ColData(6)  = "DEBIT|DEBIT|80"
	SortASC(6) 	= "DEBIT ASC"
	SortDESC(6) = "DEBIT DESC"	
	TDSTUFF(6)  = " ""align='right'"" "
	TDFunc(6) = "FormatCurrency(|)"		

	PageName = "RentControlAdjustmentReport.asp"
	EmptyText = "No Relevant Rent Control Adjustment record found in the system!!!"
	
	DefaultOrderBy = SortDesc(0)
	
	'RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
        Call GetFromToDate()
        
    FromDateFilter = ""
    if(Request("txt_FromDate")<>"") then
        FromDate=Request("txt_FromDate")
        FromDateFilter = " AND FR.TRANSACTIONDATE>=Convert(smalldatetime,'" & FromDate & "',103)"
    else
        FromDateFilter = " AND FR.TRANSACTIONDATE>=Convert(smalldatetime,'" & FromDate & "',103)"
    end if
    
    ToDateFilter= ""
    if(Request("txt_ToDate")<>"") then
        ToDate=Request("txt_ToDate")
        ToDateFilter = " AND FR.TRANSACTIONDATE<=Convert(smalldatetime,'" & ToDate & "',103)"
    else
        ToDateFilter = " AND FR.TRANSACTIONDATE<=Convert(smalldatetime,'" & ToDate & "',103)"
    end if

	SQLCODE=" SELECT FR.TENANCYID TENANCYID,CNG.LIST CUSTOMER,FP.DESCRIPTION AS PAYMENTTYPE,FR.TRANSACTIONDATE PROCESSDATE,  "&_
        " CONVERT(VARCHAR(10),FR.PAYMENTSTARTDATE,103) + ' - ' + CONVERT(VARCHAR(10),FR.PAYMENTENDDATE,103)  AS CORRECTIONPERIOD, "&_
        " ISNULL(CASE WHEN FR.AMOUNT < 0 THEN FR.AMOUNT END,0) AS CREDIT,ISNULL(CASE WHEN FR.AMOUNT > 0 THEN FR.AMOUNT END,0) AS DEBIT "&_
	    "  FROM F_RENTJOURNAL FR     "&_
        " INNER JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNG ON CNG.I=FR.TENANCYID "&_
        " INNER JOIN F_PAYMENTTYPE FP ON FP.PAYMENTTYPEID = FR.PAYMENTTYPE   "&_
        " WHERE FP.PAYMENTTYPEID IN (74,75,75,76,77,78,79,80,81,82,83,84)    " & FromDateFilter & ToDateFilter  &_
        " Order By " + Replace(orderBy, "'", "''") + ""		

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()
	
	Function GetFromToDate()
	    Dim SQL
	    
	    OpenDB()
	    
	    SQL=" SELECT MAX(FR.TRANSACTIONDATE) TODATE,MIN(FR.TRANSACTIONDATE) FROMDATE "&_
            " FROM F_RENTJOURNAL FR                                                  "&_
            " INNER JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CNG ON CNG.I=FR.TENANCYID     "&_
            " INNER JOIN F_PAYMENTTYPE FP ON FP.PAYMENTTYPEID = FR.PAYMENTTYPE       "&_ 
            " WHERE FP.PAYMENTTYPEID IN (74,75,75,76,77,78,79,80,81,82,83,84)        "  
       Call OpenRs(rsID, SQL)
	   
	   FROMDATE = rsID("FROMDATE")
	   TODATE= rsID("TODATE")
	   
	   CloseRS(rsID)
	   CloseDB()
	End Function


%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Rent Control Adjustment Report</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

function Search(){
	DateError = false

	if(convertDate(document.getElementById("txt_ToDate").value)<convertDate(document.getElementById("txt_FromDate").value)){
	    alert("The TO date must be greater then or equal to FROM date");
	    DateError=true
	}
	if (DateError == true) return false;
       location.href = "<%=PageName & "?CC_Sort=" & orderBy %>&txt_FromDate=" + document.getElementById("txt_FromDate").value +  "&txt_ToDate=" + document.getElementById("txt_ToDate").value   
  
}
	
function convertDate(strDate) {
    strDate = new String(strDate);
    arrDates = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    arrSplit = strDate.split("/");
    return new Date(arrSplit[0] + " " + arrDates[(arrSplit[1]-1)] + " " + arrSplit[2]);
}
-->	
</SCRIPT>
<!-- End Preload Script -->
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages()" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->  
    <form action=""  name = "thisForm" method="post">
        Please select from and to date to search between specific dates. 
        <table class="RSLBlack">
        <tr>
            <td>Transaction processed between:&nbsp;&nbsp;</td>
            <td width="20px">&nbsp;</td>
            <td>From Date</td><td><input type="text" name="txt_FromDate" class="textbox100"  maxlength="10"  size="11" value="<%=FromDate%>" /></td>
            <td width="20px">&nbsp;</td>
            <td>To Date</td><td><input type="text" name="txt_ToDate" class="textbox100"  maxlength="10"  size="11" value="<%=ToDate%>" /></td>
            <td width="10px">&nbsp;</td>
            <td><input type="button" class="RSLButton" value="Go" onclick="Search()" /></td> 
        </tr>
        </table>
        <table width="750" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	           <td>&nbsp;</td>
	        </tr>
	        <tr>
		        <td bgcolor="#133E71"><img  alt="" src="images/spacer.gif" width="750" height="1"/></td>
	        </tr>
        </table>
    <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="400px" height="400px" style='display:none'></iframe>
</body>
</HTML>