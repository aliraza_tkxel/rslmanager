<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/CalendarForHB.asp" -->
<%
OpenDB()
SQL = "SELECT YSTART, YEND FROM F_FISCALYEARS WHERE YSTART <= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(DATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
Call OpenRs(rsYear, SQL)
	YSTART = rsYear("YSTART")
	YEND = rsYear("YEND")
CloseRs(rsYear)

Call BuildSelect(lstLA, "sel_LA", "G_LOCALAUTHORITY", "LOCALAUTHORITYID, DESCRIPTION", "DESCRIPTION", "ALL Local Authorities", Request("sel_LA"), NULL, "textbox", " onchange=""ChangeLA()"" ")		

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance - Housing Benefit Display</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
function ChangeLA(){
	RSLFORM.submit()
	}
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
<%

	'Configure LA Filter
	if (Request("sel_LA") <> "" AND isNumeric(Request("sel_LA"))) then
		lasql =	"	INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
			"	INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
			"	INNER JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID AND D.LOCALAUTHORITY = " & Clng(Request("sel_LA")) & " "
		thela = Clng(Request("sel_LA")) 
	else
		lasql = ""
		thela = ""
	end if

	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.

	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='HBList.asp?LA=" & thela & "&pROCESSINGdATE=$date'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))

		RequestDate = CDate("1" & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear)

		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)

		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear							
	End If

	StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

	'THIS PART WILL STORE THE WHOLE MONTHS' TOTALS INTO AN ARRAY FOR LATER DISPLAY WHEN THE CURRENT DAY IN RED IS HOVERED ON
	
	Dim  HBMonthArray(32)
	for i=1 to 32
		HBMonthArray(i) = "-1"
	next
	
	SQL = "SELECT DAY(RJ.TRANSACTIONDATE) AS THEDAY, COUNT(RJ.JOURNALID) AS PCOUNT, ISNULL(SUM(RJ.AMOUNT),0) AS PVALUE FROM F_RENTJOURNAL RJ " & lasql &_
			"	WHERE RJ.PAYMENTTYPE IN (1,15,16) AND STATUSID <> 1 AND RJ.TRANSACTIONDATE >= '" & StartOfMonthDate & "' and RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
			"		GROUP BY DAY(RJ.TRANSACTIONDATE)"
	OpenDB()
	Call OpenRs(rsHB, SQL)
	while NOT rsHB.EOF
		HBMonthArray(rsHB("THEDAY")) = " title=""Total Housing Benefit : " & rsHB("PCOUNT") & " Total Value : " & FormatCurrency(-rsHB("PVALUE")) & " "" "
		rsHB.moveNext
	wend
	Call CloseRs(rsHB)

	'THIS CALCULATES THE HOUSING BENEFIT WHICH HAVE GONE THROUGH
	ACTUALSQL = "SELECT COUNT(RJ.JOURNALID) AS THECOUNT, DAY(RJ.TRANSACTIONDATE) AS THEDAY, RJ.PAYMENTTYPE, ISNULL(SUM(RJ.AMOUNT),0) AS THEAMOUNT " &_
		"FROM F_RENTJOURNAL RJ " & lasql &_
		"	WHERE RJ.PAYMENTTYPE IN (1,15,16) AND STATUSID <> 1 AND RJ.TRANSACTIONDATE >= '" & StartOfMonthDate & "' and RJ.TRANSACTIONDATE <= '" & EndOfMonthDate & "' " &_
		"		GROUP BY DAY(RJ.TRANSACTIONDATE),  RJ.PAYMENTTYPE " &_
		"			ORDER BY RJ.PAYMENTTYPE ASC"

	' Add items to the calendar
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			'THIS SECTION DISPLAYS THE HOUSING BENEFIT IN THE SYSTEM
			Call OpenRs(rsDays, ACTUALSQL)
			while (NOT rsDays.EOF) 
				if (rsDays("PAYMENTTYPE") = 1) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' href='HBList.asp?LA=" & thela & "&Filter=1&pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='Housing Benefit Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=white><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "blue"
				elseif (rsDays("PAYMENTTYPE") = 15) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' href='HBList.asp?LA=" & thela & "&Filter=15&pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='HB Over Payment Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=white><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "red"
				elseif (rsDays("PAYMENTTYPE") = 16) then
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' href='HBList.asp?LA=" & thela & "&Filter=16&pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='HB Back Payment Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=white><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "green"
				else
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a style='text-decoration:none' href='HBList.asp?LA=" & thela & "&pROCESSINGdATE=" & rsDays("THEDAY") & CurrentDateSmall & "' title='Unknown Count : " & rsDays("THECOUNT") & ". Value : " & FormatCurrency(-rsDays("THEAMOUNT")) & "'><font color=black><small><b>" & rsDays("THECOUNT") & " </b></small></font></a>", "silver"
				end if				
				rsDays.moveNext
			wend
			CloseRs(rsDays)									
		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
	CloseDB()
%>
<div style='position:absolute;left:500;top:140;width:200px'>
<b><u>Housing Benefit</u></b><br><br>
<form METHOD="POST" name="RSLFORM" action="HBDisplay.asp?date=<%=Request("date")%>">
<%=lstLA%><br><br>
</form>
This calendar allows you to view any housing benefit which have been processed. <br><br><font style='background-color:blue'>&nbsp;&nbsp;</font> Validated Housing Benefit <br>
 <br><font style='background-color:red'>&nbsp;&nbsp;</font> HB Over Payments<br> 
 <br><font style='background-color:green'>&nbsp;&nbsp;</font> HB Back Payments<br>  
 <br><br>
        To change the selected month click on '<b><<</b>' or '<b>>></b>' to go 
        backwards and forwards respectively. <br>
        <br>The display shows a figure of how many Housing Benefit payments/credits have been recieved on each day. <BR><BR>To view detailed information click on the respective item.

<table border="0" cellspacing="0" cellpadding="3">
</table>
</div>
	
	</td>
  </tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

