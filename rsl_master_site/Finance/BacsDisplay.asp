<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include file="ServerSide/CalendarForBacs.asp" -->
<%
    Company = Request("Company")
    if (Company="") then Company = "1"
    CompanyFilter = " AND  isnull(INV.COMPANYID,1) = '" & Company & "' "
    Call OpenDB()
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", null, Company, NULL, "textbox200", " onchange='GO()' style='width:170px'")		
    Call CloseDB()    
    %>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance - BACS Display</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
 

function GO(){
	var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
	location.href = "BacsDisplay.asp?Company="  + Company;
	}
 
// -->
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><%=lstCompany %></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
<%
	Function LastDayOfMonth(DateIn)
    	Dim TempDate
    	TempDate = "-" & MonthName(Month(DateIn)) & "-" & Year(dateIn)
    	if IsDate("28" & TempDate) Then LastDayOfMonth = 28
    	if IsDate("29" & TempDate) Then LastDayOfMonth = 29
    	if IsDate("30" & TempDate) Then LastDayOfMonth = 30
    	if IsDate("31" & TempDate) Then LastDayOfMonth = 31
    End function


	Dim MyCalendar

	' Create the calendar
	Set MyCalendar = New Calendar
	
	' Set the visual properties
	MyCalendar.Top = 140 'Sets the top position
	MyCalendar.Left = 50 'Sets the left position
	MyCalendar.Position = "absolute" 'Relative or Absolute positioning
	MyCalendar.Height = "310" 'Sets the height
	MyCalendar.Width = "400" 'Sets the width
	MyCalendar.TitlebarColor = "#133E71" 'Sets the color of the titlebar
	MyCalendar.TitlebarFont = "Verdana" 'Sets the font face of the titlebar
	MyCalendar.TitlebarFontColor = "white" 'Sets the font color of the titlebar
	MyCalendar.TodayBGColor = "skyblue" 'Sets the highlight color of the current day
	MyCalendar.ShowDateSelect = True 'Toggles the Date Selection form.
	MyCalendar.Company = Company
	' Add event code for when a day is clicked on. Notice
	' that when run inside your browser, "$date" is replaced
	' by the date you click on. 
	MyCalendar.OnDayClick = "javascript:location.href='BacsCSV.asp?pROCESSINGdATE=$date&Company=" & Company & "'"

	'THIS PART GETS THE RESPECTIVE DATE PART AND SETS THE SQL FOR IT
	FutureCast = ""
	FutureSQL = ""

	If Request("date") <> "" Then 
		'THIS SECTION IS RUN IF THE REQUEST DATE IS NOT EMPTY
		TheCurrentMonth = Month(CDate(Request("date")))
		TheCurrentYear = Year(CDate(Request("date")))
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear		
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		RequestDate = CDate(StartOfMonthDate)		
		EndOfMonthDate = LastDayOfMonth(RequestDate) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear

		RealMonth = Month(Date)
		RealYear = Year(Date)	
		CurrentDate = CDate(Day(Date) & " " & MonthName(RealMonth) & " " & RealYear)								
	Else 
		'ELSE RUN THIS SECTION
		TheCurrentMonth = Month(Date)
		TheCurrentYear = Year(Date)	
		StartOfMonthDate = "1 " & MonthName(TheCurrentMonth) & " " & TheCurrentYear		
		EndOfMonthDate = LastDayOfMonth(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear
		CurrentDateSmall = "/" & TheCurrentMonth & "/" & TheCurrentYear
		CurrentDate = CDate(Day(Date) & " " & MonthName(TheCurrentMonth) & " " & TheCurrentYear)				
	End If
	
	MONTH_START_OF_REAL_DATE = CDate("1 " & MonthName(Month(Date)) & " " & Year(Date))
	MONTH_END_OF_REAL_DATE = CDate(LastDayOfMonth(Date) & " " & MonthName(Month(Date)) & " " & Year(Date))	

	if (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) then			
	'THIS IS THE BACS TODATE ACTUAL SQL
	SQL = "SELECT COUNT(INV.INVOICEID) AS TOTALCOUNT, ISNULL(SUM(GROSSCOST),0) AS TOTALVALUE " &_
			"FROM F_INVOICE INV " &_
			"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
			"LEFT JOIN S_ORGANISATION S ON INV.SUPPLIERID = S.ORGID " &_			
			"WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND " &_
            " DATEADD(DAY, -9, (DATEADD(DAY, (ISNULL(S.PAYMENTTERMS,0)), TAXDATE))) <= '" & CurrentDate & "' "  & CompanyFilter

	'THIS IS THE BACS FUTURE ACTUAL SQL FOR THE CURRENT MONTH
	SQLFUTURE = "SELECT " &_
            "( " &_
		" CASE " &_
			" WHEN DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE) < INV.RECONCILEDATE " &_
			" 	THEN DAY(GETDATE()) " &_
			" WHEN CAST(DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 AS INT) <= 0 " &_
			" 	THEN DAY(GETDATE()) " &_
			" ELSE DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 " &_
			" END " &_
		" ) AS THEDAY, " &_
            
            " COUNT(INV.INVOICEID) AS TOTALCOUNT, ISNULL(SUM(GROSSCOST),0) AS TOTALVALUE " &_
			"FROM F_INVOICE INV " &_
			"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
			"LEFT JOIN S_ORGANISATION S ON INV.SUPPLIERID = S.ORGID " &_			
			"WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND " &_
            " DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) > '" & CurrentDate & "' AND "&_
            " DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) <= '" & EndOfMonthDate & "' "   & CompanyFilter &_
			"GROUP BY CASE " &_
			" WHEN DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE) < INV.RECONCILEDATE " &_
			" 	THEN DAY(GETDATE()) " &_
			" WHEN CAST(DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 AS INT) <= 0 " &_
			" 	THEN DAY(GETDATE()) " &_
			" ELSE DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 " &_
			" END "

	elseif (CDate(StartOfMonthDate) > MONTH_END_OF_REAL_DATE) then

	'THIS IS THE BACS FUTURE ACTUAL SQL FOR THE NEXT MONTH
	SQLFUTURE = "SELECT " &_
            "( " &_
		    " CASE " &_
			" WHEN DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE) < INV.RECONCILEDATE " &_
			" 	THEN DAY(GETDATE()) " &_
			" WHEN CAST(DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 AS INT) <= 0 " &_
			" 	THEN DAY(GETDATE()) " &_
			" ELSE DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 " &_
			" END " &_
		    " ) AS THEDAY, " &_ 
            "COUNT(INV.INVOICEID) AS TOTALCOUNT, ISNULL(SUM(GROSSCOST),0) AS TOTALVALUE " &_
			"FROM F_INVOICE INV " &_
			"LEFT JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
			"LEFT JOIN S_ORGANISATION S ON INV.SUPPLIERID = S.ORGID " &_			
			"WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NULL AND PB.INVOICEID IS NULL AND " &_
            " DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) > '" & StartOfMonthDate & "' AND " &_
            " DATEADD(Day,-9, DATEADD(DAY, ISNULL(S.PAYMENTTERMS, 0), TAXDATE)) <= '" & EndOfMonthDate & "' "   & CompanyFilter &_
			" GROUP BY CASE " &_
			" WHEN DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE) < INV.RECONCILEDATE " &_
			" 	THEN DAY(GETDATE()) " &_
			" WHEN CAST(DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 AS INT) <= 0 " &_
			" 	THEN DAY(GETDATE()) " &_
			" ELSE DAY(DATEADD(DAY, (ISNULL(S.PAYMENTTERMS, 0)), TAXDATE)) - 9 " &_
			" END "

	end if

	' Add items to the calendar
	Select Case Month(MyCalendar.GetDate())
		' January
		Case TheCurrentMonth

			OpenDB()

			SQL_ALREADY_DONE = "SELECT DAY(PB.PROCESSDATE) AS THEDAY, " &_
                    " COUNT(INV.INVOICEID) AS TOTALCOUNT, ISNULL(SUM(GROSSCOST),0) AS TOTALVALUE " &_
					"FROM F_INVOICE INV " &_
					"INNER JOIN F_POBACS PB ON PB.INVOICEID = INV.INVOICEID " &_
					"LEFT JOIN S_ORGANISATION S ON INV.SUPPLIERID = S.ORGID " &_			
					"WHERE INV.CONFIRMED = 1 AND INV.PAYMENTMETHOD IS NOT NULL AND PB.PROCESSDATE >= '" & StartOfMonthDate & "' AND PB.PROCESSDATE <= '" & EndOfMonthDate & "' "   & CompanyFilter &_
					"GROUP BY DAY(PB.PROCESSDATE) "

			Call OpenRs(rsDone, SQL_ALREADY_DONE)
			while NOT rsDone.EOF
				'Response.Write "A"
				MyCalendar.Days(Cint(rsDone("THEDAY"))).AddActivity "<a href='BacsTypeList_VIEW.asp?PODate=" & rsDone("THEDAY") & CurrentDateSmall & "&COMPANY=" & Company & "' title='Total Invoices Paid --> " & rsDone("TOTALCOUNT") & ".  Total Value of Invoices --> " & FormatCurrency(rsDone("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDone("TOTALCOUNT") & " </b></small></font></a>", "steelblue"				
				rsDone.moveNext
			wend
			CloseRs(rsDone)
			
			if (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) then			
			
				'THIS SECTION DISPLAYS THE ACTUAL POS DUE
				Call OpenRs(rsDays, SQL)
				if (NOT rsDays.EOF) then
					InTheSystem = rsDays("TOTALCOUNT")
					MyCalendar.Days(Cint(Day(Date))).AddActivity "<a href='BacsTypeList.asp?PODate=" & Day(Date) & CurrentDateSmall & "&COMPANY=" & Company & "' title='Total Invoices --> " & rsDays("TOTALCOUNT") & ".  Total Value of Invoices --> " & FormatCurrency(rsDays("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDays("TOTALCOUNT") & " </b></small></font></a>", "RED"
				end if
				CloseRs(rsDays)									

			end if

			if ( (TheCurrentMonth = Month(Date) AND TheCurrentYear = Year(Date)) OR (CDate(StartOfMonthDate) > MONTH_END_OF_REAL_DATE) ) then

				'Response.Write "hello"
				'THIS SECTION DISPLAYS THE FUTURE POS DUE
				Call OpenRs(rsDays, SQLFUTURE)
				while (NOT rsDays.EOF)
					InTheSystem = rsDays("TOTALCOUNT")
					MyCalendar.Days(Cint(rsDays("THEDAY"))).AddActivity "<a title='Total Invoices --> " & rsDays("TOTALCOUNT") & ".  Total Value of Invoices --> " & FormatCurrency(rsDays("TOTALVALUE")) & "'><font color=blue><small><b>" & rsDays("TOTALCOUNT") & " </b></small></font></a>", "LIGHTGREEN"
					rsDays.moveNext
				wend
				CloseRs(rsDays)									

			end if		
	End Select
	
	' Draw the calendar to the browser
	MyCalendar.Draw()
%>
<div style='position:absolute;left:500;top:140;width:200px'>
<b><u>Invoices</u></b><br><br>
This calendar shows the number of Invoices that are ready to be paid. To get the financial value of these transactions hover over the respective number.
<br><br>To change the selected month click on '<b><<</b>' or '<b>>></b>' to go backwards and forwards respectively.
<br><br>
<A BGCOLOR=RED style='background-color:red'>&nbsp;&nbsp;</a> Invoices Due Payment.<br>
        <br>
        <A BGCOLOR=steelblue style='background-color:steelblue'>&nbsp;&nbsp;</a> 
        Invoices Previously Paid.<br>
        <br>
        <A BGCOLOR=lightgreen style='background-color:lightgreen'>&nbsp;&nbsp;</a> 
        Invoices Due In Future.<br>
        <br>
To view processing options for the Invoices click on the red number.
      </div>
	
	</td>
  </tr>
</table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe> 
</BODY>
</HTML>
