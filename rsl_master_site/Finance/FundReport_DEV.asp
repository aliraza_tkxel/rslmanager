<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 



<!--#include virtual="ACCESSCHECK.asp" -->

<%
Dim Company
Call OpenDB()
Company = "1"
if not Request("Company") = "" then
    Company = Request("Company")
end if
Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", null, Company, NULL, "textbox200", " style='width:200px'")	

Call CloseDB()
 
%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Finance - Fund Report</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<script language=javascript defer>

    function refreshCompany() {
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
        thisForm.action = "FundReport_DEV.asp?Company=" + Company;
        thisForm.submit();
    }

    function openCloseLines(start, end, start2, end2) {
        doc = document.all;
        if (doc["line" + start].style.display == "none")
            newStatus = "block";
        else
            newStatus = "none";
        for (i = start; i < end; i++)
            doc["line" + i].style.display = newStatus
        for (j = start2; j < end2; j++)
            doc["lineE" + j].style.display = "none"
    }

    function openCloseLines2(start, end) {
        doc = document.all;
        if (doc["lineE" + start].style.display == "none")
            newStatus = "block";
        else
            newStatus = "none";
        for (i = start; i < end; i++)
            doc["lineE" + i].style.display = newStatus
    }
</script>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<%
OpenDB()
SQL = "SELECT CONVERT(VARCHAR,ISNULL(Sum(HEA.HEADALLOCATION),0) ,1)  AS TotalCostCentre " &_
		"FROM dbo.F_COSTCENTRE CC " &_ 
        "           INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.FISCALYEAR IS NULL " &_
		"           INNER JOIN F_HEAD HE ON HE.COSTCENTREID = CC.COSTCENTREID " &_
        "           INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.FISCALYEAR IS NULL " &_
        "			INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = he.HEADID  " &_
        "           INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  " &_
		"WHERE CC.COSTCENTREID IN ((SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID')) and isnull(p.companyid,1)  =  " & Company 

Call OpenRs(sprocTotalFundVal, SQL)
if (NOT sprocTotalFundVal.EOF) then
	TotalBudgetValue = sprocTotalFundVal("TOTALCOSTCENTRE")
else
	TotalBudgetValue = 0
end if
CloseRs(sprocTotalFundVal)
CloseDB()

RSL_DATASHAPE_CONNECTION_STRING = "Provider=MSDataShape;Data " & RSL_CONNECTION_STRING

strSQL = "	SHAPE {  " &_
	" " &_
	"	SELECT CC.DESCRIPTION, CC.COSTCENTREID,  " &_
	"		ISNULL(CCA.COSTCENTREALLOCATION,0) as COSTCENTREALLOCATION, " &_ 
	"		ISNULL(SUM(HDA.HEADALLOCATION),0) as COSTCENTREALLOCATED,  " &_
	"		(ISNULL(CCA.COSTCENTREALLOCATION,0) - ISNULL(SUM(HDA.HEADALLOCATION),0)) as NOTALLOCATED  " &_
	"		FROM F_COSTCENTRE CC  " &_
	"			INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.FISCALYEAR IS NULL " &_
	"			INNER JOIN F_HEAD HD on CC.COSTCENTREID = HD.COSTCENTREID  " &_
    "			INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = hD.HEADID  " &_
    "           INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  " &_
	"			LEFT JOIN F_HEAD_ALLOCATION HDA ON HDA.HEADID = HD.HEADID AND HDA.FISCALYEAR IS NULL " &_
	"				WHERE CC.COSTCENTREID IN ((SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'))   and isnull(p.companyid,1)  =  " & Company &_
	"		GROUP BY CCA.COSTCENTREALLOCATION, CC.COSTCENTREID, CC.DESCRIPTION  " &_
	"	ORDER BY CC.DESCRIPTION  " &_
	" " &_
	"} APPEND ( ( SHAPE {  " &_
	" " &_
	"	SELECT F_HEAD.COSTCENTREID,  " &_
	"		isNull(EXPENDITURETOTAL,0) as HEADALLOCATED,  " &_
	"		isNull(ORDERED_PURCHASES,0) as ORDERED_PURCHASES, " &_ 
	"		isNull(RECONCILED_PURCHASES,0) as RECONCILED_PURCHASES, " &_ 
	"		ISNULL(HDA.HEADALLOCATION - isNull(RECONCILED_PURCHASES,0) - isNull(ORDERED_PURCHASES,0),0) AS POTENTIALPURCHASEPOWERLEFT, " &_ 
	"   	F_HEAD.DESCRIPTION AS HEADNAME, HDA.HEADALLOCATION, F_HEAD.HEADID  " &_
	"		FROM F_HEAD  " &_
	"		INNER JOIN F_HEAD_ALLOCATION HDA ON HDA.HEADID = F_HEAD.HEADID AND HDA.FISCALYEAR IS NULL " &_
	"			        INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = F_HEAD.HEADID  " &_
    "                   INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  and isnull(p.companyid,1)  =  " & Company  &_
	"		LEFT JOIN (  " &_
	"			SELECT SUM(EXA.EXPENDITUREALLOCATION) AS EXPENDITURETOTAL, EX.HEADID  " &_
	"				FROM F_EXPENDITURE EX " &_
	"					INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EX.EXPENDITUREID = EXA.EXPENDITUREID AND EXA.FISCALYEAR IS NULL " &_
	"					INNER JOIN F_HEAD ON EX.HEADID = F_HEAD.HEADID  " &_
    "			        INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = F_HEAD.HEADID  " &_
    "                   INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  and isnull(p.companyid,1)  =  " & Company  &_
	"				GROUP BY EX.HEADID )  " &_
	"			EXPENDITURE_ALLOC_TOTALS ON F_HEAD.HEADID = EXPENDITURE_ALLOC_TOTALS.HEADID  " &_
	"		LEFT JOIN (  " &_
	"			SELECT SUM(GROSSCOST) as ORDERED_PURCHASES, F_EXPENDITURE.HEADID  " &_
	"				FROM F_PURCHASEITEM  " &_
	"					INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID  " &_
	"					INNER JOIN F_HEAD ON F_EXPENDITURE.HEADID = F_HEAD.HEADID  " &_
    "			        INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = F_HEAD.HEADID  " &_
    "                   INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  and isnull(p.companyid,1)  =  " & Company  &_
	"				WHERE F_PURCHASEITEM.PISTATUS < 9 AND F_PURCHASEITEM.ACTIVE = 1 " &_
	"			GROUP BY F_EXPENDITURE.HEADID ) " &_
	"			ORDERED_PURCHASES ON F_HEAD.HEADID = ORDERED_PURCHASES.HEADID  " &_
	"		LEFT JOIN (  " &_
	"			SELECT SUM(GROSSCOST) as RECONCILED_PURCHASES, F_EXPENDITURE.HEADID  " &_
	"				FROM F_PURCHASEITEM  " &_
	"					INNER JOIN F_EXPENDITURE ON F_EXPENDITURE.EXPENDITUREID = F_PURCHASEITEM.EXPENDITUREID  " &_
	"					INNER JOIN F_HEAD ON F_EXPENDITURE.HEADID = F_HEAD.HEADID  " &_
    "			        INNER JOIN F_DEVELOPMENT_HEAD_LINK l ON l.HEADID = F_HEAD.HEADID  " &_
    "                   INNER JOIN dbo.PDR_DEVELOPMENT p ON p.DEVELOPMENTID = l.DEVELOPMENTID  and isnull(p.companyid,1)  =  " & Company  &_
	"				WHERE F_PURCHASEITEM.PISTATUS >= 9 AND F_PURCHASEITEM.ACTIVE = 1 " &_
	"			GROUP BY F_EXPENDITURE.HEADID) " &_
	"			RECONCILED_PURCHASES ON F_HEAD.HEADID = RECONCILED_PURCHASES.HEADID  " &_
	"		ORDER BY F_HEAD.DESCRIPTION " &_
	" " &_
	"} APPEND ( { " &_
	" " &_
	"	SELECT EX.HEADID, EXA.EXPENDITUREALLOCATION,  " &_
	"		ISNULL(E_ORDERED_PURCHASES.ORDERED_PURCHASES,0) AS EX_ORDERED, " &_
	"		ISNULL(E_RECONCILED_PURCHASES.RECONCILED_PURCHASES,0) AS EX_RECONCILED,  " &_
	"		ISNULL(EXA.EXPENDITUREALLOCATION - ISNULL(E_ORDERED_PURCHASES.ORDERED_PURCHASES,0) - ISNULL(E_RECONCILED_PURCHASES.RECONCILED_PURCHASES,0) ,0) AS POTENTIALPURCHASEPOWERLEFT,  " &_
	"		DESCRIPTION  " &_
	"		FROM F_EXPENDITURE EX  " &_
	"		INNER JOIN F_EXPENDITURE_ALLOCATION EXA ON EXA.EXPENDITUREID = EX.EXPENDITUREID AND EXA.FISCALYEAR IS NULL " &_
	"			LEFT JOIN ( " &_
	"				SELECT EXPENDITUREID, SUM(GROSSCOST) AS ORDERED_PURCHASES " &_
	"					FROM F_PURCHASEITEM  " &_
	"						WHERE F_PURCHASEITEM.ACTIVE = 1 AND F_PURCHASEITEM.PISTATUS < 9 " &_
	"					GROUP BY EXPENDITUREID " &_
	"				)  " &_
	"				E_ORDERED_PURCHASES ON E_ORDERED_PURCHASES.EXPENDITUREID = EX.EXPENDITUREID " &_ 
	"			LEFT JOIN ( " &_
	"				SELECT EXPENDITUREID, SUM(GROSSCOST) AS RECONCILED_PURCHASES " &_
	"					FROM F_PURCHASEITEM  " &_
	"						WHERE F_PURCHASEITEM.ACTIVE = 1 AND F_PURCHASEITEM.PISTATUS >= 9 " &_
	"					GROUP BY EXPENDITUREID " &_
	"				)  " &_
	"				E_RECONCILED_PURCHASES ON E_RECONCILED_PURCHASES.EXPENDITUREID = EX.EXPENDITUREID  " &_
	" 		ORDER BY EX.DESCRIPTION " &_
	" " &_
	"} AS EXPDATA RELATE HEADID TO HEADID )) AS HEADDATA RELATE COSTCENTREID TO COSTCENTREID) "

' Open original recordset
Set rst = Server.CreateObject("ADODB.Recordset")
rst.Open strSQL, RSL_DATASHAPE_CONNECTION_STRING
%>
    <form name=thisForm method=post>
      <table width="95%" align=center border="0" cellspacing="0" cellpadding="0">
        
<tr class="RSLBlack"> 
          <td width="98%" valign="top">
           <%=lstCompany%>
                 <input type="button" class="RSLButton" value=" Update Search " title="Update Search"
                        onclick="refreshCompany()" />
           </td>
    <td>
        </tr>
<tr class="RSLBlack"> 
                    </tr>
          <td width="98%" valign="top">
            <p><br>
        For further analysis please click on a development name to view the budgets 
        and expenditure allocations. <bR>
              <br>
              Total Development Budget &pound;<span class="RSLBlack"><strong><%= TotalBudgetValue %></strong></span></p>
          </td>

      </table>
      </form>
        <br>
      <table class="RSLBlack" border=1 style='border-collapse:collapse' cellspacing=0 align=center>
        <!--
		<tr bgcolor=blue style='color:white'>
          <td width=170px nowrap><b>Cost Centre Name</b></td>
          <td align=right width=110px title="Cost Centre Budget"><b>CC Budget</b></td>
          <td align=right width=100px><b>Allocated</b></td>
          <td align=right width=110px><b>Not Allocated</b></td>
          <td align=right width=100px><b>----</b></td>
          <td align=right width=100px><b>----</b></td>
        </tr>
		-->
        <%
counter = 1
startcounter = 0
grand_counter = 0
mydata = ""
if NOT rst.EOF then

Do While Not rst.EOF
    Set rstChild = rst("HEADDATA").Value
	startcounter = counter
	super_grand_startcounter = grand_counter
	mydata = ""
    if not rstChild.EOF then
		mydata = mydata & "<tr bgcolor=green style='HEIGHT:20;color:white' id=line" & counter & "><td>&nbsp;&nbsp;Development Name</td><td align=right width=110px>Budget</td><td align=right width=100px>Allocated</td><td align=right width=110px>Queued & Ordered</td><td align=right width=100px>Reconciled & Paid</td><td align=right width=100px>Balance</td></tr>"
		counter = counter + 1			
        Do While Not rstChild.EOF
			grand_mydata = ""		
			Set rstGrandChild = rstChild("EXPDATA").Value
			grand_startcounter = grand_counter
			if (not rstGrandChild.EOF) then
				'grand_mydata = grand_mydata & "<tr bgcolor=green style='display:block;color:white' id=lineE" & grand_counter & "><td>&nbsp;&nbsp;&nbsp;&nbsp;Expenditure Name</td><td align=right width=110px>Expenditure Allocation</td><td align=right width=110px>Allocated</td><td align=right width=110px>Not Allocated</td><td align=right width=110px>Purchases (PM)</td><td align=right width=110px>Purchase (PA)</td></tr>"
				'grand_counter = grand_counter + 1			
				Do While Not rstGrandChild.EOF
					grand_mydata = grand_mydata & "<TR bgcolor=#F5F5F5 style='display:none;' id=lineE" & grand_counter & "><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>" & rstGrandChild("Description") & "</i></td><td align=right>�" & FormatNumber(rstGrandChild("EXPENDITUREALLOCATION"),2) & "</td><td align=right>--</td><td align=right>�" & FormatNumber(rstGrandChild("EX_ORDERED"),2) & "</td><td align=right>�" & FormatNumber(rstGrandChild("EX_RECONCILED"),2) & "</td><td align=right>�" & FormatNumber(rstGrandChild("potentialpurchasepowerleft"),2) & "</td></tr>"
					grand_counter = grand_counter + 1
					rstGrandChild.MoveNext
				Loop
			else
				'grand_mydata = grand_mydata & "<tr bgcolor=green style='display:block;color:white' id=lineE" & grand_counter & "><td>&nbsp;&nbsp;&nbsp;&nbsp;Expenditure Name</td><td align=right width=110px>Expenditure Allocation</td><td align=right width=110px>Allocated</td><td align=right width=110px>Not Allocated</td><td align=right width=110px>Purchases (PM)</td><td align=right width=110px>Purchase (PA)</td></tr>"
				'grand_counter = grand_counter + 1	
				grand_mydata = grand_mydata & "<tr bgcolor=#F5F5F5 style='display:none' id=lineE" & grand_counter & "><td colspan=6 align=center>No Purchases or Expenditure Items setup for Development</td></tr>"
				grand_counter = grand_counter + 1		
			end if
			mydata = mydata & "<TR bgcolor=beige onclick=openCloseLines2(" & grand_startcounter & "," & grand_counter & ") style='cursor:hand' id=line" & counter & "><td>&nbsp;&nbsp;" & rstChild("headname") & "</td><td align=right>�" & FormatNumber(rstChild("headallocation"),2) & "</td><td align=right>�" & FormatNumber(rstChild("HEADALLOCATED"),2) & "</td><td align=right>�" & FormatNumber(rstChild("ORDERED_PURCHASES"),2) & "</td><td align=right>�" & FormatNumber(rstChild("RECONCILED_PURCHASES"),2) & "</td><td align=right>�" & FormatNumber(rstChild("potentialpurchasepowerleft"),2) & "</td></tr>"
            counter = counter + 1
			rstChild.MoveNext
			mydata = mydata & grand_mydata
        Loop
    else
		mydata = mydata & "<tr bgcolor=green style='display:none;color:white' id=line" & counter & "><td>&nbsp;&nbsp;Development Name</td><td align=right>Development Budget</td><td align=right>Allocated</td><td align=right>Queued & Ordered</td><td align=right>Reconciled & Paid</td><td align=right>Balance</td></tr>"
		counter = counter + 1	
		mydata = mydata & "<tr bgcolor=beige style='display:none' id=line" & counter & "><td colspan=6 align=center>No Purchases or Development setup for Cost Centre</td></tr>"
        counter = counter + 1		
	end if
	'Response.Write "<TR bgcolor=lightblue onclick='openCloseLines(" & startcounter & "," & counter & "," & super_grand_startcounter & "," & grand_counter & ")' style='cursor:hand'><td><font color=blue><b>" & rst("DESCRIPTION") & "</b></font></td><td align=right><b>�" & FormatNumber(rst("COSTCENTREALLOCATION"),2) & "</b></td><td align=right><b>�" & FormatNumber(rst("costcentreallocated"),2) & "</b></td><td align=right><b>�" & FormatNumber(rst("notallocated"),2) & "</b></td><td>&nbsp;</td><td>&nbsp;</td></tr>"	
	Response.Write mydata
    rst.MoveNext
Loop
else
	Response.Write "<tr><td colspan=6 align=center>No Cost Centres found for selected period</td></tr>"
end if
%>
      </table>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>
<%
rst.close()
%>