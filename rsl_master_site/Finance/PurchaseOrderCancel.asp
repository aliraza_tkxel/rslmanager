<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=5" />
</HEAD>
</HTML>
<%
	CONST CONST_PAGESIZE = 20 
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)
	Dim clean_desc

	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Order No|ORDERID|90"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|90"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "By|FULLNAME|120"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"	
	TDSTUFF(2)  = ""
	TDFunc(2) = ""		

	ColData(3)  = "Item|PONAME|175"
	SortASC(3) 	= "PONAME ASC"
	SortDESC(3) = "PONAME DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Cost|FORMATTEDCOST|60"
	SortASC(4) 	= "TOTALCOST ASC"
	SortDESC(4) = "TOTALCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	PageName = "PurchaseOrderCancel.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PONumber") <> "") then
		if (isNumeric(Request("PONumber"))) then
			POFilter = " AND PO.ORDERID = '" & Clng(Request("PONumber")) & "' "
		end if
	end if
	
	SupFilter = ""
    rqSupplier = Request("sel_SUPPLIER")
	if (rqSupplier<> "") then
		SupFilter = " AND PO.SUPPLIERID = '" & rqSupplier & "' "
	end if

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if
	SQLCODE ="SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, PO.ORDERID, O.NAME AS SUPPLIER, CONVERT(VARCHAR,PI.TOTALCOST,1) AS FORMATTEDCOST, PS.POSTATUSNAME, PONAME, " &_
			"FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103) FROM F_PURCHASEORDER PO " &_
			"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_
			"LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID " &_
			"LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
			"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_			
			"WHERE PO.POSTATUS not in (9, 10, 11, 12, 13, 16) AND PO.ACTIVE = 1 " & SupFilter & CompanyFilter  & POFilter &_			
			" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select Supplier", rqSupplier, NULL, "textbox200", " style='width:240px'")	
	Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Purchase Order List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(Order_id){
	location.href = "CancelConfirm.asp?OrderID=" + Order_id
	}

function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

    function setSupplier() {
	    document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value = '<%=Request("sel_SUPPLIER")%>';
        }

        function setCompany() {
	    document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value = '<%=Request("sel_COMPANY")%>';
	}

function SubmitPage(){
        var SupplierNumber = document.getElementById("sel_SUPPLIER").options[document.getElementById("sel_SUPPLIER").selectedIndex].value;
        var Company = document.getElementById("sel_COMPANY").options[document.getElementById("sel_COMPANY").selectedIndex].value;
	if (isNaN(thisForm.PONumber.value)){
		alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
		}
        location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + SupplierNumber + "&sel_COMPANY=" + Company + "&PONumber=" + thisForm.PONumber.value ;
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();setSupplier();setCompany()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=PONumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>" onblur="RemoveBad()" style='width:135px;background-image:url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
<td><%=lstSuppliers%>
</td>
    <td><%=lstCompany%>
</td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>