<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	PrintAction 	= request("PrintAction")
	DevelopmentID 	= request("DevelopmentID")
	quicktenancyfind =  request("quicktenancyfind")
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();

	function process(){
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/DDUpdateTool_Process_srv.asp";
		RSLFORM.submit();
	}

	function GetList(){
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/DDUpdateTool_srv.asp?DevelopmentID=<%=DevelopmentID%>&PrintAction=<%=PrintAction%>&quicktenancyfind=" + RSLFORM.quicktenancyfind.value;
		RSLFORM.submit();
	}
	
	function quick_find(){
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/DDUpdateTool_srv.asp?DevelopmentID=<%=DevelopmentID%>&LetterAction=<%=PrintAction%>&quicktenancyfind=" + RSLFORM.quicktenancyfind.value;
		RSLFORM.submit();
	}	
  
  	function TickAllChecks(){
	
		var theNum,TheAnswer,numberIDarray	
		 
		if (RSLFORM.TickAll.checked)
			TheAnswer = true
		else 
			TheAnswer = false
					
		numberIDarray = RSLFORM.hid_tenancylist.value
		numberarray = numberIDarray.split(",")
		
		for(i=0;i<numberarray.length; i++){
			theNum = numberarray[i];
			if (document.getElementById("chkpost" + theNum +"").checked != TheAnswer) {
				document.getElementById("chkpost" + theNum +"").checked = TheAnswer
				do_sum(theNum)
				}
		}
	
	}
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		document.getElementById("idlist").value = str_idlist;
		
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	function open_me(WorkOrder_id){
		event.cancelBubble = true;
		}
		

// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTopFlexible.asp" -->
<form name = RSLFORM method=post>

	
  <TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
    <TR>
      <TD colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="18%">Additional Assets :   			</td>
          <td width="44%"><select name="sel_ASSETS" class="textbox200" style="width:147">
            <option value="" id="ASSETS">No Filter</option>
            <option value="1" id="ASSETS">With Assets</option>
            <option value="2" id="ASSETS">Without Assets</option>
          </select></td>
          <td width="38%">
		  <input name="idlist" id="idlist" value="" type="HIDDEN">
		  <input name="hid_SORT" id="hid_SORT" value="" type="HIDDEN">
		  </td>
        </tr>
        <tr>
          <td colspan="3" height="3"></td>
          </tr>
        <tr>
          <td>Tenancy Ref : </td>
          <td><input name="quickfind" id="quicktenancyfind" value="<%=request("quicktenancyfind")%>" type="text"></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" height="3"></td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type='BUTTON' name='btn_FIND' title='Apply Filters' class='RSLBUTTON'  width=100 value='Apply Filters' onClick='quick_find()'></td>
          <td>&nbsp;</td>
        </tr>
      </table></TD>
    </TR>
    <TR>
      <TD colspan="3"  height="3"></TD>
    </TR>
    <TR> 
      <TD width="51%" align="left"><strong>To Select All the records you can see below on this page click here 
        
      </strong></TD>
      <TD width="3%" align="center"><div align="left"><strong>
      </strong>          
          <table width="25" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="1"></td>
            </tr>
            <tr>
              <td width="25" height="25" align="center" bgcolor="#133e71">
                <input type="checkbox" name="TickAll" value="1" onClick="TickAllChecks()">
					<input name="hid_tenancylist" type="hidden" value="">			  </td>
            </tr>
         <tr>
              <td height="1"></td>
            </tr>
          </table>
      <strong>          </strong></div></TD>
      <TD width="45%" align="right">&nbsp;</TD>
    </TR>
  </TABLE>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name=frm_slip width=1 height=1 style='display:block'></iframe>
</BODY>
</HTML>

