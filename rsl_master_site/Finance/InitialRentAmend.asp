<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	OpenDB()
	

	'SQL = ""
	'Call openRs(rsSet, SQL)	
	'if not rsSet.EOF Then balance = rsSet(0) else balance = 0 End If
	'CloseRs(rsSet)


CloseDB()

TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	
%>
<html>
<head>
<title>RSL Manager Finance - Initial Rent Amend</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<meta http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
</style>
</head>
<script LANGUAGE="JavaScript" SRC="/js/preloader.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/general.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/menu.js"></script>
<script LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></script>
<script LANGUAGE="JAVASCRIPT">

	var FormFields = new Array();
	
	
	function GetInfo(){
		tempID = THISFORM.txt_SEARCH.value
		FormFields[0] = "txt_SEARCH|Tenancy Search|TEXT|Y"
		if (!checkForm()) return false;
		THISFORM.target = "ServerIFrame";
		THISFORM.action = "serverside/InitialRentGetTenancy_srv.asp";
		THISFORM.submit();
		THISFORM.reset()
		THISFORM.txt_SEARCH.value = tempID 
	}
	
	function AmendRent(){
		FormFields[0] = "txt_AMOUNT|AMOUNT|CURRENCY|Y"
		FormFields[1] = "txt_STARTDATE|STARTDATE|DATE|Y"
		FormFields[2] = "txt_RENT_PART|RENT PART|CURRENCY|Y"
		FormFields[3] = "txt_SERVICES_PART|SERVICES PART|CURRENCY|Y"
		FormFields[4] = "txt_SUPPORTEDSERVICES_PART|SUPPORTEDSERVICES PART|CURRENCY|Y"
		FormFields[5] = "txt_INELIGSERV_PART|INELIGSERV PART|CURRENCY|Y"
		FormFields[6] = "txt_COUNCILTAX_PART|COUNCILTAX PART|CURRENCY|Y"
		FormFields[7] = "txt_WATERRATES_PART|WATERRATES PART|CURRENCY|Y"

		if (!checkForm()) return false;
		
		if (!calculateTotal()) return false;
		
		THISFORM.target = "ServerIFrame";
		THISFORM.action = "serverside/InitialRentGetAmend_srv.asp";
		THISFORM.submit();
	}

	function calculateTotal(){
	
		var part_values
		full_value  = THISFORM.txt_AMOUNT.value
		part_values = parseFloat(THISFORM.txt_RENT_PART.value) 
		part_values = part_values + parseFloat(THISFORM.txt_SERVICES_PART.value) 
		part_values = part_values + parseFloat(THISFORM.txt_SUPPORTEDSERVICES_PART.value) 	
		part_values = part_values + parseFloat(THISFORM.txt_INELIGSERV_PART.value) 
		part_values = part_values + parseFloat(THISFORM.txt_COUNCILTAX_PART.value)  
		part_values = part_values + parseFloat(THISFORM.txt_WATERRATES_PART.value) 	
		if (full_value == part_values)
			return true
		else {
			alert("Your part values do not add up to the total amount")
			return false
			}
	}

</script>
<body BGCOLOR="#FFFFFF" ONLOAD="initSwipeMenu(3);preloadImages();" onUnload="macGo()" MARGINHEIGHT="0" LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0"> 
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form NAME="THISFORM" method="post">
  <br>
  <table STYLE="BORDER:1PX SOLID BLACK" cellspacing="0" cellpadding="3" width="700" >
    <tr  >
      <td colspan="9">1) Search for a Tenancy<br><br>

2) Review what Initial rent they have at present against data you wish to amend with.<br><br>

3) Enter the data to change the inital rent <br><br>

4) Check that the data you have entered is correct <br><br></td>
    </tr>
    <tr bgcolor="steelblue" style="color:white"> 
      <td colspan="9" bgcolor="steelblue"><b>NEW ITEM</b></td>
    </tr>
    <tr> 
      <td width="142">Search Tenancy </td>
      <td width="145"><label>
        <input name="txt_SEARCH" type="text" id="txt_SEARCH" tabindex="1">
      </label></td>
      <td width="56"><img src="/js/FVS.gif" name="img_SEARCH" width="15" height="15" id="img_SEARCH"></td>
      <td width="151">&nbsp;
      <input type="button" value="Get Tenancy " class="RSLButton" onClick="GetInfo()" name="btOrder2"></td>
      <td width="134">&nbsp;</td>
      <td width="34">&nbsp;</td>
    </tr>
    <tr>
      <td height="29" colspan="7" bgcolor="steelblue"><span class="style1">Existing Initial Rent Break Down </span></td>
    </tr>
    <tr>
      <td height="29" colspan="7" bgcolor="#F5FEFA"><div id="IrDataDiv" name="IrDataDiv">Awaiting search</div></td>
    </tr>
    <tr>
      <td height="29" colspan="7" bgcolor="steelblue"><span class="style1"><strong>- New</strong> - Initial Rent Break Down </span></td>
    </tr>
    <tr>
      <td height="29" colspan="7">Awaiting search</td>
    </tr>
    <tr>
      <td height="29" colspan="7">
	 	<table>	
			<tr>
			  <td height="29">Amount : </td>
			  <td height="29"><input name="txt_AMOUNT" type="text" id="txt_AMOUNT" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_AMOUNT" width="15" height="15" ></td>
			  <td height="29">Start Date : </td>
			  <td height="29"><input name="txt_STARTDATE" type="text" id="txt_STARTDATE" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_STARTDATE" width="15" height="15" ></td>
			  <td width="1" height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29"><strong>Part Breakdown</strong> </td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29">Rent</td>
			  <td height="29"><input name="txt_RENT_PART" type="text" id="txt_RENT_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_RENT_PART" width="15" height="15" ></td>
			  <td height="29">Services</td>
			  <td height="29"><input name="txt_SERVICES_PART" type="text" id="txt_SERVICES_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_SERVICES_PART" width="15" height="15" ></td>
			  <td height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29">Supported Services </td>
			  <td height="29"><input name="txt_SUPPORTEDSERVICES_PART" type="text" id="txt_SUPPORTEDSERVICES_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_SUPPORTEDSERVICES_PART" width="15" height="15" ></td>
			  <td height="29">Ineligile Services </td>
			  <td height="29"><input name="txt_INELIGSERV_PART" type="text" id="txt_INELIGSERV_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_INELIGSERV_PART" id="img_INELIGSERV_PART" width="15" height="15" ></td>
			  <td height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29">Council Tax </td>
			  <td height="29"><input name="txt_COUNCILTAX_PART" type="text" id="txt_COUNCILTAX_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_COUNCILTAX_PART"  id="img_COUNCILTAX_PART" width="15" height="15" ></td>
			  <td height="29">Water Rates </td>
			  <td height="29"><input name="txt_WATERRATES_PART" type="text" id="txt_WATERRATES_PART" tabindex="1"></td>
			  <td height="29"><img src="/js/FVS.gif" name="img_WATERRATES_PART" id="img_WATERRATES_PART" width="15" height="15" ></td>
			  <td height="29">&nbsp;</td>
			</tr>
			<tr>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
		  </tr>

			<tr>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
			  <td height="29"><input type="button" value="Get Tenancy " class="RSLButton" onClick="AmendRent()" name="btOrder22"></td>
			  <td height="29">&nbsp;</td>
			  <td height="29">&nbsp;</td>
		  </tr>
	    </table>	 	</tr>
	 </tr>
  </table>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<IFRAME  src="/secureframe.asp" NAME=ServerIFrame WIDTH=400 HEIGHT=100 STYLE='DISPLAY:block'></IFRAME>
</body>
</html>


