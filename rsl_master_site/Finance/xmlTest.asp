<%@ LANGUAGE = VBScript    %>
<!-- #include virtual="Connections/db_connection.asp" -->
<!-- #Include virtual="Includes/Database/ADOVBS.INC" -->

<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Developer Studio"/>
<META HTTP-EQUIV="Content-Type" content="text/html" charset="iso-8859-1"/>
<TITLE>ADO 2.6 E</TITLE>


<%
   Response.Write "<H3>Server-side processing</H3>"
Set xmlDoc = Server.CreateObject("Microsoft.XMLDOM")
       Dim adoConn
Set adoConn = Server.CreateObject("ADODB.Connection")

Dim sConn
sConn = RSL_CONNECTION_STRING
adoConn.ConnectionString = sConn
adoConn.CursorLocation = adUseClient
adoConn.Open

   Dim adoCmd
Set adoCmd = Server.CreateObject("ADODB.Command")
Set adoCmd.ActiveConnection = adoConn

   Dim sQuery
sQuery = "<ROOT xmlns:sql='urn:schemas-microsoft-com:xml-sql'><sql:query>SELECT * FROM PRODUCTS ORDER BY PRODUCTNAME FOR XML AUTO</sql:query></ROOT>"

   Dim adoStreamQuery
Set adoStreamQuery = Server.CreateObject("ADODB.Stream")
adoStreamQuery.Open
   adoStreamQuery.WriteText sQuery, adWriteChar
   adoStreamQuery.Position = 0

   Set adoCmd.CommandStream = adoStreamQuery
   adoCmd.Dialect = "{5D531CB2-E6Ed-11D2-B252-00C04F681B71}"

   Response.write "Pushing XML to client for processing "  & "<BR/>"

   adoCmd.Properties("Output Stream") = adoStreamQuery 
Response.write "<XML ID='MyDataIsle'>"
   adoCmd.Execute , , adExecuteStream
	xmlDoc.Load adoStreamQuery 
   Response.write "</XML>"
response.write xmlDoc.xml
%>