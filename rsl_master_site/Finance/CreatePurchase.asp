<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
OpenDB()

Dim lstApprovedBy
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_HEAD HE ON CC.COSTCENTREID = HE.COSTCENTREID " &_
		"INNER JOIN F_EXPENDITURE EX ON EX.HEADID = HE.HEADID " &_
		"WHERE (CC.ACTIVE = 1) AND (EX.ACTIVE = 1) AND (HE.ACTIVE = 1)" &_
		"AND '" & FormatDateTime(Date,1) & "' >= DATESTART AND '" & FormatDateTime(Date,1) & "' <= DATEEND "

Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()'")
Call BuildSelect(lstDevelopments, "sel_DEVELOPMENT", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "Please Select", l_developmentid, NULL, "textbox200", "" )
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE  ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", "")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox200", " onchange='SetVat()' STYLE='WIDTH:70'")

CloseDB()
	
%>
<HTML>
<HEAD>
<title>RSL Manager Finance - Create Purchase Order</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE=JAVASCRIPT>
	var FormFields = new Array()
	
	FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
	FormFields[1] = "sel_HEAD|Head|SELECT|Y"
	FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
	FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
	FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
	FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
	FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
	FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
	
	function FormatCurrencyComma(Figure){
		NewFigure = FormatCurrency(Figure)
		if ((Figure >= 1000 || Figure <= -1000)) {
			var iStart = NewFigure.indexOf(".");
			if (iStart < 0)
				iStart = NewFigure.length;
	
			iStart -= 3;
			while (iStart >= 1) {
				NewFigure = NewFigure.substring(0,iStart) + "," + NewFigure.substring(iStart,NewFigure.length)
				iStart -= 3;
			}		
		}
		return NewFigure
	}

	function TotalBoth(){
		event.srcElement.style.textAlign = "right"
		if (!checkForm(true)) {
			document.getElementById("txt_GROSSCOST").value = ""
			return false
			}
		document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))
		}

	function SetVat(){
		if (document.getElementById("sel_VATTYPE").value == 0 || document.getElementById("sel_VATTYPE").value == 3){
			document.getElementById("txt_VAT").value = "0.00"
			document.getElementById("txt_VAT").readOnly = true
			}
		else if (document.getElementById("sel_VATTYPE").value == 1){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(20)
			}
		else if (document.getElementById("sel_VATTYPE").value == 2){
			document.getElementById("txt_VAT").readOnly = false
			AddVAT(5)
			}			
		document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
		}

	function AddVAT(val){
		if (isNumeric("txt_NETCOST", "")){
			//VAT = FormatCurrency(parseFloat(document.getElementById("txt_NETCOST").value) /100 * val)
			VAT = new Number (document.getElementById("txt_NETCOST").value /100 * val)
			VAT = round(round(VAT,4),2)
			document.getElementById("txt_VAT").value = VAT
			document.getElementById("txt_GROSSCOST").value = FormatCurrency(parseFloat(document.getElementById("txt_VAT").value) + parseFloat(document.getElementById("txt_NETCOST").value))			
			}
		}

	function round(number,X) {
	// rounds number to X decimal places, defaults to 2
		X = (!X ? 2 : X);
		return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
	}

	function ResetVAT(){
		if (document.getElementById("sel_VATTYPE").value == 1) AddVAT(20)
		if (document.getElementById("sel_VATTYPE").value == 2) AddVAT(5)		
		}		

	function PopulateListBox(values, thetext, which){
		values = values.replace(/\"/g, "");
		thetext = thetext.replace(/\"/g, "");
		values = values.split(";;");
		thetext = thetext.split(";;");
		if (which == 2)
			var selectlist = document.forms.THISFORM.sel_HEAD;
		else 
			var selectlist = document.forms.THISFORM.sel_EXPENDITURE;	
		selectlist.length = 0;

		for (i=0; i<thetext.length;i++){
			var oOption = document.createElement("OPTION");

			oOption.text=thetext[i];
			oOption.value=values[i];
			selectlist.options[selectlist.length]= oOption;
			}
		}
	
	function Select_OnchangeFund(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Waiting for Data...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		THISFORM.IACTION.value = "gethead";
		THISFORM.action = "ServerSide/GetHeads.asp";
		THISFORM.target = "PURCHASEFRAME<%=Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")%>";
		THISFORM.submit();
		}
	
	function Select_OnchangeHead(){
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		document.getElementById("txt_EMLIMITS").value = "0.00";								
		PopulateListBox("", "Waiting for Data...", 1);	
		THISFORM.IACTION.value = "change";
		THISFORM.action = "ServerSide/GetExpenditures.asp";
		THISFORM.target = "PURCHASEFRAME<%=Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")%>";
		THISFORM.submit();
		}

	function SetPurchaseLimits(){
		eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
		eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
		eAmounts = eAmounts.split(";;");
		eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
		eEMLIMITS = eEMLIMITS.split(";;");
	
		document.getElementById("txt_EXPBALANCE").value = FormatCurrency(eAmounts[eIndex]);
		document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);	
		}	
	
	var RowCounter = 0
	
	function AddRow(insertPos){
		if (!checkForm()) return false
		GrossCost = document.getElementById("txt_GROSSCOST").value
		EmployeeLimit = document.getElementById("txt_EMLIMITS").value
		ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value
		SaveStatus = 1
		if (parseFloat(ExpenditureLimit) < parseFloat(GrossCost)){
			alert("The total cost of this item more than the funds remaining in the selected Expenditure.\nTo continue please decrease the amount.") 
			return false;
			}
		if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)){
			SaveStatus = 0
			result = confirm("The total cost is greater than your employee limit in the selected Expenditure.\nIf you continue then the item will be placed in a queue to be authorised\nby a user who has appropriate limits.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
			if (!result) return false
			}
		
		RowCounter++
		ItemRef = document.getElementById("txt_ITEMREF").value			
		ItemDesc = document.getElementById("txt_ITEMDESC").value			
		ExpenditureID = document.getElementById("sel_EXPENDITURE").value
		ExpenditureName = document.getElementById("sel_EXPENDITURE").options(document.getElementById("sel_EXPENDITURE").selectedIndex).text					
		NetCost = document.getElementById("txt_NETCOST").value			
		VAT = document.getElementById("txt_VAT").value											
		VatTypeID = document.getElementById("sel_VATTYPE").value			
		VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text					
		VatTypeCode = VatTypeName.substring(0,1).toUpperCase()					

		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "EMPTYLINE") {
				oTable.deleteRow(i)
				break;
				}
			}

		oRow = oTable.insertRow(insertPos)
		oRow.id = "TR" + RowCounter
		oRow.onclick = AmendRow
		oRow.style.cursor = "hand"
		
		DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + ExpenditureID + "||<>||" + SaveStatus + "\">"
	
		AddCell (oRow, ItemRef + DATA, ItemDesc, "", "")
		AddCell (oRow, ExpenditureName, "", "", "")		
		AddCell (oRow, VatTypeCode, VatTypeName + " Rate", "center", "")	
		AddCell (oRow, FormatCurrencyComma(NetCost), "", "right", "")
		AddCell (oRow, FormatCurrencyComma(VAT), "", "right", "")
		AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "")
		DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
		AddCell (oRow, DelImage, "", "center", "#FFFFFF")

		SetTotals (NetCost, VAT, GrossCost)
		ResetData()
		}
	
	function ResetData(){
		document.getElementById("txt_EMLIMITS").value = "0.00";						
		document.getElementById("txt_EXPBALANCE").value = "0.00";
		PopulateListBox("", "Please Select a Cost Centre...", 2);
		PopulateListBox("", "Please Select a Head...", 1);
		ResetArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_COSTCENTRE")
		for (i=0; i<ResetArray.length; i++)
			document.getElementById(ResetArray[i]).value = ""
		document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		document.getElementById("txt_VAT").value = "0.00";	
		document.getElementById("AmendButton").style.display = "none"		
		document.getElementById("AddButton").style.display = "block"
		}
		
	function AmendRow(){
		event.cancelBubble = true
		Ref = this.id
		RowNumber = Ref.substring(2,Ref.length)
		StoredData = document.getElementById("iDATA" + RowNumber).value
		StoredArray = StoredData.split("||<>||")
		
		THISFORM.IACTION.value = "LOADPREVIOUS";
		THISFORM.action = "ServerSide/GetExpenditures.asp?EXPID=" + StoredArray[6];
		THISFORM.target = "PURCHASEFRAME<%=Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")%>";
		THISFORM.submit();
		
		ReturnArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST")
		for (i=0; i<ReturnArray.length; i++)
			document.getElementById(ReturnArray[i]).value = StoredArray[i]
		document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
		document.getElementById("AddButton").style.display = "none"
		document.getElementById("AmendButton").style.display = "block"		
		}
	
	function UpdateRow(){
		sTable = document.getElementById("ItemTable")
		RowData = document.getElementById("UPDATEID").value
		RowArray = RowData.split("||<>||")
		MatchRow = RowArray[0]
		for (i=0; i<sTable.rows.length; i++){
			if (sTable.rows(i).id == "TR" + MatchRow) {
				sTable.deleteRow(i)
				SetTotals (-RowArray[1], -RowArray[2], -RowArray[3])				
				break;
				}
			}
		AddRow(i)		
		}
		
	function SetTotals(iNE, iVA, iGC) {
		totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
		totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
		totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

		document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		document.getElementById("hiVA").value = FormatCurrency(totalVAT)
		document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
		
		document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
		document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)						
		}
		
	function AddCell(iRow, iData, iTitle, iAlign, iColor) {
		oCell = iRow.insertCell()
		oCell.innerHTML = iData
		if (iTitle != "") oCell.title = iTitle
		if (iAlign != "") oCell.style.textAlign = iAlign
		if (iColor != "") oCell.style.backgroundColor = iColor
		}
	
	function DeleteRow(RowID,NE,VA,GR){
		oTable = document.getElementById("ItemTable")
		for (i=0; i<oTable.rows.length; i++){
			if (oTable.rows(i).id == "TR" + RowID) {
				oTable.deleteRow(i)
				SetTotals (-NE, -VA, -GR)				
				break;
				}
			}
		if (oTable.rows.length == 1) {
			oRow = oTable.insertRow()
			oRow.id = "EMPTYLINE"
			oCell = oRow.insertCell()
			oCell.colSpan = 7
			oCell.innerHTML = "Please enter an item from above"
			oCell.style.textAlign = "center"
			}
		ResetData()
		}

	function SavePurchaseOrder(){
		if (parseFloat(document.getElementById("hiGC").value) <= 0) {
			alert("Please enter some items before creating a purchase order.")
			return false;
			}
		THISFORM.method = "POST"
		THISFORM.action = "ServerSide/CreatePurchaseOrder_svr.asp"
		THISFORM.submit()
		}		
</SCRIPT>
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=755>
	<TR bgcolor=steelblue style='color:white'> 
		<TD colspan=6><b>PURCHASE ORDER INFORMATION</b></TD>
	</TR>
	<TR><TD>Name : </TD><TD><input name="txt_PONAME" type="text" class="TEXTBOX200" size=50 maxlength=50></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_PONAME"></td><TD rowspan=3 valign=top NOWRAP>&nbsp;Delivery&nbsp;Details : </TD>
		<TD rowspan=3><textarea name="txt_DELIVERYDETAILS" cols=55 rows=7 class="TEXTBOX200"  style='WIDTH:305;overflow:hidden;border:1px solid #133E71'></textarea></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_DELIVERYDETAILS"></td></TR>
	<TR><TD>Supplier : </TD><TD><%=lstSuppliers%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_SUPPLIER"></td></TR>
	<TR><TD>Development : </TD><TD><%=lstDevelopments%></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_DEVELOPMENT"></td></tr>
	<TR><TD>Order Date : </TD><TD><input name="txt_PODATE" type="text" class="TEXTBOX200" size=50 value="<%=FormatDateTime(Date,2)%>"></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_PODATE"></td>
	<TD>&nbsp;Expected Date : </TD><TD><input name="txt_DELDATE" type="text" class="TEXTBOX200" size=50></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_DELDATE"></td></tr>	
</TABLE>
<br>		
<TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=755>
<FORM NAME=THISFORM method=post>
	<TR bgcolor=steelblue style='color:white'> 
		<TD colspan=9><b>NEW ITEM</b></TD>
	</TR>
	<TR>
		<TD>Item : </TD><TD><input name="txt_ITEMREF" type="text" class="textbox200" maxlength="50"></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMREF"></td>
		<TD>&nbsp;Cost Centre : </TD><TD><%=lstCostCentres%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_COSTCENTRE"></td>	
		<TD>&nbsp;Net Cost : </TD><TD><input style='text-Align:right' name="txt_NETCOST" type="text"  class="textbox"  maxlength=20 size=11  onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()"></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_NETCOST"></td>			
	</TR>
	<TR>
		<TD rowspan=3 valign=top>Notes : </TD><TD rowspan=4><textarea name="txt_ITEMDESC" rows=7 class="TEXTBOX200"  style='overflow:hidden;border:1px solid #133E71'></textarea></TD><td rowspan=3><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMDESC"></td>
		<TD>&nbsp;Head : </TD><TD>
							<select id="sel_HEAD" name="sel_HEAD" class="TEXTBOX200" onchange="Select_OnchangeHead()">
                            <option value="">Please Select a Cost Centre...</option>
                          </select>
						  </TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_HEAD"></td>	
		<TD>&nbsp;Vat Type : </TD><TD><%=lstVAT%></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_VATTYPE"></td>			
	</TR>
	<TR>
		<TD>&nbsp;Expenditure : </TD><TD>                        
						<select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="TEXTBOX200" onchange="SetPurchaseLimits()">
                          <option value="">Please select a Head...</option>
                        </select>
						</TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_EXPENDITURE"></td>	
		<TD>&nbsp;VAT : </TD><TD><input style='text-Align:right' name="txt_VAT" type="text"  class="textbox"  maxlength=20 size=11  onBlur="TotalBoth()" onFocus="alignLeft()" VALUE="0.00"></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_VAT"></td>			
	</TR>
	<TR>
		<TD>&nbsp;Exp Balance : </TD><TD><input type="text" name="txt_EXPBALANCE" class="textbox200"></TD><td></td>	
	    <TD>&nbsp;Total : </TD><TD><input style='text-Align:right' name="txt_GROSSCOST" type="text"  class="textbox"  maxlength=20 size=11 readonly></TD><td><img src="/js/FVS.gif" width=15 height=15 name="img_GROSSCOST"></td>			
	</TR>
	<TR><TD></TD><TD></TD><td>&nbsp;Spend Limit : </td><td><input type="text" name="txt_EMLIMITS" class="textbox200" readonly></td>
		<TD align=right colspan=3>
			<INPUT TYPE=HIDDEN NAME="IACTION">
			<INPUT TYPE=HIDDEN NAME="UPDATEID">			
		  	<INPUT TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST">
		  	<INPUT TYPE=HIDDEN NAME="EMPLOYEE_LIMIT_LIST">						  
			<table cellspacing=0 cellpadding=0><tr>
			<td nowrap><input type=button Name="ResetButton" value=" RESET " class="RSLButton" onclick="ResetData()">&nbsp;</td>
			<td><input type=button Name="AddButton" value=" ADD " class="RSLButton" onclick="AddRow(-1)"></td>
			<td><input type=button Name="AmendButton" value=" AMEND " class="RSLButton" onclick="UpdateRow()" style="display:none"></td>
			</tr></table>			
		</TD>
	</tr>
</form>	
</TABLE>		
<br>
  <TABLE STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=755 id="ItemTable" slcolor='' hlcolor="silver">
	<THEAD>
	<TR bgcolor=steelblue ALIGN=RIGHT style='color:white'> 
	  <TD height=20 ALIGN=left width=250 NOWRAP><b>Item Name:</b></TD>
	  <TD width=200 nowrap align="left"><b>Expenditure:</b></TD>
	  <TD width=40 nowrap><b>Code:</b></TD>
	  <TD width=80 nowrap><b>Net (�):</b></TD>
	  <TD width=75 nowrap><b>VAT (�):</b></TD>
	  <TD width=80 nowrap><b>Gross (�):</b></TD>
	  <TD width=29 nowrap>&nbsp;</TD>	  
	</TR>
	</THEAD>
	<TBODY>
	<TR ID="EMPTYLINE"><TD colspan=7 align="center">Please enter an item from above</TD></TR>
	</TBODY>
  </TABLE>
<BR>  
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD height=20 width=490 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></TD>
	  <TD width=80 nowrap bgcolor=white><b><input type="hidden" id="hiNC" value="0"><div id="iNC">0.00</div></b></TD>
	  <TD width=75 nowrap bgcolor=white><b><input type="hidden" id="hiVA" value="0"><div id="iVA">0.00</div></b></TD>
	  <TD width=80 nowrap bgcolor=white><b><input type="hidden" id="hiGC" value="0"><div id="iGC">0.00</div></b></TD>
	  <TD width=29 nowrap bgcolor=white>&nbsp;</TD>		  
	</TR>
  </TABLE>
<BR>
  <TABLE STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=2 width=755>
	<TR bgcolor=steelblue ALIGN=RIGHT> 
	  <TD width=754 align=right nowrap>&nbsp;<input type="button" onclick="SavePurchaseOrder()"></TD>		  
	</TR>
  </TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe  src="/secureframe.asp" name="PURCHASEFRAME<%=Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")%>" style='display:BLOCK'></iframe> 
</BODY>
</HTML>

