<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
dim SCNTab
SCNTab = Request("SCNTab")
if SCNTab = "" then SCNTab = "0"
%>
<HTML>
<HEAD>
    <title>RSL Manager Finance - Sales Credit Notes</title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />

    <SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js" ></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js" ></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js" ></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js" ></SCRIPT>
    <SCRIPT LANGUAGE="JavaScript" SRC="/js/financial.js" ></SCRIPT>
    <script language="javascript">
        function ChangeTab(TabID){
            THISFORM.SCNTab.value = TabID; 
            THISFORM.submit();
        }
    </script>
</HEAD>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(3);preloadImages();" onUnload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<form NAME="THISFORM" method="post" action="SalesCreditNote.asp">

<table width="755" cellpadding="0" cellspacing="0" border="0">
    <tr height="20">
        <td width="100"><img src="/Finance/Images/tab_salescreditnotes<%if SCNTab=0 then response.write "-o" %>.gif" border="0" alt="Sales Credit Notes" onclick="ChangeTab(0)" /></td>
        <td width="100"><img src="/Finance/Images/tab_newsalescreditnote<%if SCNTab=1 then response.write "-o" %>.gif" border="0" alt="New Sales Credit Note" onclick="ChangeTab(1)" /></td>
        <td width="555">&nbsp;</td>
    </tr>
</table>
<table STYLE='BORDER:1PX SOLID BLACK' cellspacing="0" cellpadding="3" width="755">
	<tr bgcolor=steelblue style='color:white'> 
		<td colspan=6><b>SALES CREDIT NOTE INFORMATION</b></td>
	</tr>
</table>
<br />

<% if SCNTab = 0 then %>
    <!--#include file="ListSalesCreditNotes.asp" -->
<% elseif SCNTab = 1 then %>
    <!--#include file="CreateSalesCreditNote.asp" -->
<% end if %>
   
<input type="hidden" name="SCNTab" value="<%=SCNTab %>" />
</form>

<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name="SCNFRAME<%=TIMESTAMP%>" style='display:kNONE'  height="400" width="400" src="/SecureFrame.asp"></iframe> 
</BODY>
</HTML>



