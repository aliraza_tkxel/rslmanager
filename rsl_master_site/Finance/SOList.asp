<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (3)	 'USED BY CODE
	Dim DatabaseFields (3)	 'USED BY CODE
	Dim ColumnWidths   (3)	 'USED BY CODE
	Dim TDSTUFF        (3)	 'USED BY CODE
	Dim TDPrepared	   (3)	 'USED BY CODE
	Dim ColData        (3)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (3)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (3)	 'All Array sizes must match	
	Dim TDFunc		   (3)

	ColData(0)  = "Tenant Ref|TENANCYID|100"
	SortASC(0) 	= "rj.TENANCYID ASC"
	SortDESC(0) = "rj.TENANCYID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "TenancyReference(|)"

	ColData(1)  = "Payment Date|TRANSACTIONDATE|150"
	SortASC(1) 	= "TRANSACTIONDATE ASC"
	SortDESC(1) = "TRANSACTIONDATE DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "Details|DETAILS|350"
	SortASC(2) 	= ""
	SortDESC(2) = ""
	TDSTUFF(2)  = ""
	TDFunc(2) = ""

	ColData(3)  = "Payment Amount|PAYMENTAMOUNT|150"
	SortASC(3) 	= "PAYMENTAMOUNT ASC"
	SortDESC(3) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(3)  = " ""align='right'"" "
	TDFunc(3) = "FormatCurrency(|)"		
	
	PageName = "SOList.asp"
	EmptyText = "No Standing Orders found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("pROCESSINGdATE") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("pROCESSINGdATE")),2)
		RequiredDate = CDate(Request("pROCESSINGdATE"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ButtonDisabled = ""
	ORIGINATORNUMBER = "658670"
	
'	Response.Write ProcessingDate & "<BR>"
'	Response.Write RequiredDate & "<BR>"
'	Response.Write Date & "<BR>"	
	
	'two sql scripts are required, the one which appears next shows
	'the direct debits which will potentially come out on the specified date

		ExtraSQL = ""
		ReturnType = "Actual"		
		if (Request("DECLINED") = 1) then
			ExtraSQL = " AND RJ.STATUSID = 11 "
			ButtonDisabled = " disabled"
			ReturnType = "Subsequently Declined"			
		else
			ExtraSQL = " AND RJ.STATUSID <> 11 "		
		END IF
		'the sql below shows the direct debit transactions which have occured.

		SQLCODE = "SELECT SOD.DETAILS, RJ.TRANSACTIONDATE, '' as empty, RJ.TENANCYID, -ISNULL(RJ.AMOUNT,0) AS PAYMENTAMOUNT, " &_
					"CONVERT(VARCHAR, RJ.TRANSACTIONDATE, 103) AS INPUTBOX, " &_
					"CAST(RJ.JOURNALID AS VARCHAR) AS CHECKBOX " &_				
					"FROM F_RENTJOURNAL RJ LEFT JOIN F_STANDINGORDERDATA SOD ON SOD.JOURNALID = RJ.JOURNALID " &_
					"	WHERE PAYMENTTYPE = 3 AND RJ.TRANSACTIONDATE = '" & FormatDateTime(RequiredDate,1) & "' " & ExtraSQL &_
					"		ORDER BY " + Replace(orderBy, "'", "''") + ""
			
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Standing Order List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function cHECKpAGE(){
	if (isDate("pROCESSINGdATE","")){
		location.href = "<%=PageName & "?CC_Sort=" & orderBy%>&pROCESSINGdATE=" + thisForm.pROCESSINGdATE.value
		}
	}

function CancelBubble(){
	event.cancelBubble = true
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table class="RSLBlack" width=750><tr><td><b>&nbsp;DATE</b></td>
<td><input type=text name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>" style='width:135px;'></td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="cHECKpAGE()" <%=ButtonDisabled%>>
</td><td nowrap> &nbsp;&nbsp;Showing '<b><%=ReturnType%></b>' Standing Orders.
</td>
<td align=right width=100%>&nbsp<a href='SODisplay.asp?date=<%=Request("pROCESSINGdATE")%>'><font color=blue><b>BACK to SO Calendar</b></font></a></td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>