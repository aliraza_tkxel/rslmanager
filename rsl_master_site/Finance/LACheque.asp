<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	
	CONST CONST_PAGESIZE = 19
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)

	ColData(0)  = "Name|NAME|250"
	SortASC(0) 	= "NAME ASC"
	SortDESC(0) = "NAME DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""

	ColData(1)  = "Amount|PAYMENTAMOUNT|100"
	SortASC(1) 	= "PAYMENTAMOUNT ASC"
	SortDESC(1) = "PAYMENTAMOUNT DESC"	
	TDSTUFF(1)  = " ""align='right'"" "
	TDFunc(1) = "FormatCurrency(|)"		

	ColData(2)  = "Refunds|TOTALREFUNDS|40"
	SortASC(2) 	= ""
	SortDESC(2) = ""	
	TDSTUFF(2)  = " ""align='right'"" "
	TDFunc(2) = ""

	if (Request("VIEW") = 1) then
		ColData(3)  = "Cheque Number|EMPTY|150"
		SortASC(3) 	= ""
		SortDESC(3) = ""	
		TDSTUFF(3)  = " "" onclick=""""CancelBubble()"""" align=RIGHT>"" & rsSet(""INPUTBOX"") & ""<b"" "
		TDFunc(3) = ""
	ELSE
		ColData(3)  = "Cheque Number|EMPTY|100"
		SortASC(3) 	= ""
		SortDESC(3) = ""	
		TDSTUFF(3)  = " "" onclick=""""CancelBubble()"""" style='background-color:white' align=left>"" & rsSet(""INPUTBOX"") & ""<b"" "
		TDFunc(3) = ""
	END IF

	ColData(4)  = "|EMPTY|50"
	SortASC(4) 	= ""
	SortDESC(4) = ""	
	TDSTUFF(4)  = " "" onclick=""""CancelBubble()"""" style='background-color:white' align=center>"" & rsSet(""CHECKBOX"") & ""<b"" "
	TDFunc(4) = ""
	
	PageName = "LACheque.asp"
	EmptyText = "No LA Refunds found in the system for the specified criteria!!!"
	DefaultOrderBy = SortASC(1)
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PODate") <> "") then
		ProcessingDate = FormatDateTime(CDate(Request("PODate")),2)
		RequiredDate = CDate(Request("PODate"))
	else
		ProcessingDate = FormatDateTime(CDate(Date),2)
		RequiredDate = Date
	end if

	ORIGINATORNUMBER = "658670"
	
	if (Request("VIEW") = 1) then
		SQLCODE = "SELECT '' AS EMPTY, ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, S.NAME, S.ORGID,  " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(RJ.JOURNALID) AS TOTALREFUNDS, " &_
				"LB.CHEQUENUMBER AS INPUTBOX, " &_
				"'' AS CHECKBOX " &_				
				"FROM F_RENTJOURNAL RJ " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
				"INNER JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
				"WHERE RJ.PAYMENTTYPE = 14 AND LB.PROCESSDATE = '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY S.NAME, S.ORGID, LB.CHEQUENUMBER " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""
	else
		SQLCODE = "SELECT '' AS EMPTY, ISNULL(SUM(RJ.AMOUNT),0) AS PAYMENTAMOUNT, S.NAME, S.ORGID,  " &_
				"'" & ORIGINATORNUMBER & "' AS ORIGINATOR, COUNT(RJ.JOURNALID) AS TOTALREFUNDS, " &_
				"'<input type=""text"" name=""txt_TEXT' + CAST(S.ORGID AS VARCHAR) + '"" value="""" CLASS=""TEXTBOX200"" MAXLENGTH=20><img src=""/js/FVS.gif"" width=15 height=15 name=""img_TEXT' + CAST(S.ORGID AS VARCHAR) + '"">' AS INPUTBOX, " &_
				"'<input type=""HIDDEN"" name=""hid_CO' + CAST(S.ORGID AS VARCHAR) + '"" value=""' + CAST(COUNT(RJ.JOURNALID) AS VARCHAR) + '""><input type=""HIDDEN"" name=""hid_VA' + CAST(S.ORGID AS VARCHAR) + '"" value=""' + CAST(ISNULL(SUM(RJ.AMOUNT),0) AS VARCHAR) + '""><input type=""CHECKBOX"" name=""CHECKITEMS"" value=""' + CAST(S.ORGID AS VARCHAR) + '"">' AS CHECKBOX " &_				
				"FROM F_RENTJOURNAL RJ " &_
				"INNER JOIN C_TENANCY T ON T.TENANCYID = RJ.TENANCYID " &_
				"INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " &_
				"INNER JOIN PDR_DEVELOPMENT D ON P.DEVELOPMENTID = D.DEVELOPMENTID " &_
				"INNER JOIN G_LOCALAUTHORITY LA ON LA.LOCALAUTHORITYID = D.LOCALAUTHORITY " &_
				"INNER JOIN S_ORGANISATION S ON S.ORGID = LA.LINKTOSUPPLIER " &_
				"LEFT JOIN F_LABACS LB ON LB.JOURNALID = RJ.JOURNALID " &_
				"WHERE RJ.PAYMENTTYPE = 14 AND LB.JOURNALID IS NULL AND DATEADD(DAY, ISNULL(S.PAYMENTTERMS,0), RJ.TRANSACTIONDATE) <= '" & FormatDateTime(RequiredDate,1) & "' " &_
				" 	GROUP BY S.NAME, S.ORGID " &_
				"		ORDER BY " + Replace(orderBy, "'", "''") + ""
	end if	
	'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> LA Refund List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function CancelBubble(){
	event.cancelBubble = true
	}

var FormFields = new Array()

function SendCheques() {
	chkitems = document.getElementsByName("CHECKITEMS")
	FormFields.length = 0
	iCounter = 0
	TotalValue = 0
	TotalCount = 0
	if (chkitems.length){
		for (j=0; j<chkitems.length; j++) {
			ManualError ("img_TEXT" + chkitems[j].value, "", 3)
			}
			
		for (i=0; i<chkitems.length; i++) {
			if (chkitems[i].checked) {
				FormFields[iCounter] = "txt_TEXT" + chkitems[i].value + "|Cheque Number|TEXT|Y"
				TotalCount += parseInt(document.getElementById("hid_CO" + chkitems[i].value).value)
				TotalValue += parseFloat(document.getElementById("hid_VA" + chkitems[i].value).value)
				iCounter ++
				}
			}
		}
	if (TotalCount == 0) {
		alert("No lines have been selected. To select a line click on the\nrespective checkbox and enter a cheque number next to\nit. Otherwise the row will not be processed.")
		return false
		} 
		
	if (!checkForm()) return false
	result = confirm("You have selected '" + TotalCount + "' LA Refund(s) to be processed at a total value of '�" + FormatCurrency(TotalValue) +"'.\nDo you wish to continue?\n\nClick on 'OK' to continue.\nClick on 'CANCEL' to abort.")
	if (!result) return false
	thisForm.action = "ServerSide/LAList_Cheque.asp?pROCESSINGdATE=<%=Request("PODate")%>"
	thisForm.submit()
	}

<% if (Request("ERR97" & Replace(Date, "/", "")) = 1) then %>
alert ("Some entries were not processed as more LA Refunds have been entered on to the system.\nPlease check the data and re-process as required.")
<% end if %>

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=thisForm method=post>
<table width=750 class="RSLBlack"><tr>
<td> <b>&nbsp;LA Refund Cheque List</b>
<input type=HIDDEN name=pROCESSINGdATE class="RSLBlack" value="<%=Server.HTMLEncode(ProcessingDate)%>">
</td>
<td align=right>
&nbsp<a href='LADisplay.asp?date=<%=ProcessingDate%>'><font color=blue><b>BACK to LA Refund Calendar</b></font></a>
</td>
</tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
<% if (Request("VIEW") <> 1) then %>
<table width=750><tr><td align=right>&nbsp;<input class="RSLButton" name="AButton" type=button onclick="SendCheques()" value=" AUTHORIZE "></td></tr></table>
<% END IF %>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>