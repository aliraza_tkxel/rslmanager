<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%

    Company = "1"
    if not Request("Company") = "" then
        Company = Request("Company")
    end if

	CONST CONST_PAGESIZE = 20
	Dim Total_NetCost, Total_VAT, Total_GROSSCOST
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (7)	 'USED BY CODE
	Dim DatabaseFields (7)	 'USED BY CODE
	Dim ColumnWidths   (7)	 'USED BY CODE
	Dim TDSTUFF        (7)	 'USED BY CODE
	Dim TDPrepared	   (7)	 'USED BY CODE
	Dim ColData        (7)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (7)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (7)	 'All Array sizes must match	
	Dim TDFunc		   (7)
	Dim clean_desc
	
	ColData(0)  = "Name|NAME|180"
	SortASC(0) 	= "O.NAME ASC"
	SortDESC(0) = "O.NAME  DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""
	
	ColData(1)  = "Invoice No.|INVOICENUMBER|100"
	SortASC(1) 	= "inv.INVOICENUMBER ASC"
	SortDESC(1) = "inv.INVOICENUMBER DESC"
	TDSTUFF(1)  = ""
	TDFunc(1) = ""			

	ColData(2)  = "Cert No.|CISCERTIFICATENUMBER|90"
	SortASC(2) 	= "O.CISCERTIFICATENUMBER ASC"
	SortDESC(2) = "O.CISCERTIFICATENUMBER DESC"
	TDSTUFF(2)  = ""
	TDFunc(2) = ""			
    
	ColData(3)  = "Paid|PROCESSDATE|70"
	SortASC(3) 	= "PROCESSDATE ASC"
	SortDESC(3) = "PROCESSDATE DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = "|"		

	ColData(4)  = "Net Cost|NETCOST|80"
	SortASC(4) 	= "NETCOST ASC"
	SortDESC(4) = "NETCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	ColData(5)  = "Vat|VAT|70"
	SortASC(5) 	= "VAT ASC"
	SortDESC(5) = "VAT DESC"	
	TDSTUFF(5)  = " ""align='right'"" "
	TDFunc(5) = "FormatCurrency(|)"		

	ColData(6)  = "Cost|GROSSCOST|80"
	SortASC(6) 	= "GROSSCOST ASC"
	SortDESC(6) = "GROSSCOST DESC"
	TDSTUFF(6)  = " ""align='right'"" "
	TDFunc(6) = "FormatCurrency(|)"

    ColData(7)  = "Category|CISCATEGORY|80"
	SortASC(7) 	= "O.CISCATEGORY ASC"
	SortDESC(7) = "O.CISCATEGORY DESC"	
	TDSTUFF(7)  = ""
	TDFunc(7) = ""

	PageName = "CisReport.asp"
	EmptyText = "No Relevant Invoices found in the system!!!"
	DefaultOrderBy = SortASC(0)
	RowClickColumn = " ""title='Payment Method : "" & rsSet(""PAYMENTTYPE"") & ""' "" " 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	CompareDate = DateAdd("m",-1,Date)
	CompareDate2 = Date		
	if (Request("COMPAREDATE") <> "") then
		CompareDate = Request("COMPAREDATE")
		CompareDate2 = Request("COMPAREDATE2")			
	end if
	
	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " AND INV.SUPPLIERID = " & Request("sel_SUPPLIER") & " "
	end if

    CompanyFilter = " AND INV.companyid = " & Company & " "
	
	CISFilter = ""
	if (Request("CIS") <> "") then
		CISFilter = " AND O.CISCATEGORY = '" & Request("CIS") & "'"
	end if

	SQLCODE ="SELECT O.CISCERTIFICATENUMBER, O.NAME, INV.INVOICEID, INV.INVOICENUMBER,  BAC.PROCESSDATE, INV.NETCOST, INV.VAT, INV.GROSSCOST, PT.DESCRIPTION AS PAYMENTTYPE, O.CISCATEGORY AS CISCATEGORY " &_
			"FROM F_INVOICE INV " &_
			"INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID " &_
			"INNER JOIN F_POBACS BAC ON BAC.INVOICEID = INV.INVOICEID " &_
			"INNER JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID " &_
			"WHERE PAYMENTMETHOD IS NOT NULL " &_
				"AND BAC.PROCESSDATE >= '" & FormatDateTime(CompareDate,1) & "' AND BAC.PROCESSDATE <= '" & FormatDateTime(CompareDate2,1) & "' " &_
					"" & SupFilter & CISFilter & CompanyFilter &_	
					" Order By " + Replace(orderBy, "'", "''") + ""

'rESPONSE.wRITE SQLCODE
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If

	Function GetPageTotals()
	
		SQL =		"SELECT 	ISNULL(SUM(INV.NETCOST),0) AS NETCOST, " &_
					"			ISNULL(SUM(INV.VAT ),0) AS VAT, " &_
					"			ISNULL(SUM(INV.GROSSCOST),0) AS GROSSCOST " &_
					"FROM F_INVOICE INV " &_
					"INNER JOIN S_ORGANISATION O ON O.ORGID = INV.SUPPLIERID " &_
					"INNER JOIN F_POBACS BAC ON BAC.INVOICEID = INV.INVOICEID " &_
					"INNER JOIN F_PAYMENTTYPE PT ON INV.PAYMENTMETHOD = PT.PAYMENTTYPEID " &_
					"WHERE O.CISCERTIFICATENUMBER IS NOT NULL " &_
						"AND PAYMENTMETHOD IS NOT NULL " &_
						"AND BAC.PROCESSDATE >= '" & FormatDateTime(CompareDate,1) & "' AND BAC.PROCESSDATE <= '" & FormatDateTime(CompareDate2,1) & "' " &_
							"" & SupFilter & CISFilter & CompanyFilter
	
		Call OpenRs(rsTotals, SQL)
		
			IF NOT rsTotals.EOF THEN
			
				Total_NetCost	 = rsTotals("NETCOST")
				Total_VAT		 = rsTotals("VAT")
				Total_GROSSCOST	 = rsTotals("GROSSCOST")
			
			END IF
					
		CloseRs(rsTotals)
	
	End Function
	
	Call Create_Table()
	

    Dim Company


	OpenDB()
	Call GetPageTotals()
    Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
response.write Company
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE ORGACTIVE=1 ", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	Call BuildSelect(lstCIS, "sel_CIS", "G_CISCATEGORY", "distinct CISCATEGORY,  CISCATEGORY", "1", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> CIS REPORT</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<script language="JavaScript" src="/js/preloader.js"></script>
<script language="JavaScript" src="/js/general.js"></script>
<script language="JavaScript" src="/js/menu.js"></script>
<script language="JavaScript" src="/js/FormValidation.js"></script>
<script language="JavaScript">
<!--
    function load_me(Order_id) {
        window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
    }

    var FormFields = new Array()
    FormFields[0] = "txt_DATE|From Date|DATE|Y"
    FormFields[1] = "txt_DATE2|To Date|DATE|Y"

    function setSupplier() {
        thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
        thisForm.sel_CIS.value = "<%=Request("CIS")%>";
        thisForm.sel_COMPANY.value = "<%=Request("sel_COMPANY")%>";
    }

    function SubmitPage() {
        if (!checkForm()) return false;
        location.href = "<%=PageName%>?CC_Sort=<%=orderBy%>&sel_Supplier=" + thisForm.sel_SUPPLIER.value + "&COMPAREDATE=" + thisForm.txt_DATE.value + "&sel_COMPANY=" + thisForm.sel_COMPANY.value + "&COMPAREDATE2=" + thisForm.txt_DATE2.value + "&CIS=" + thisForm.sel_CIS.value;
    }
// -->
</script>
<!-- End Preload Script -->
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages();setSupplier()" onunload="macGo()"
    marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="get">
    <table width="678" class="RSLBlack">
        <tr>
            <td width="74">
                &nbsp;SUPPLIER
            </td>
            <td width="266">
                <%=lstSuppliers%>
            </td>
            <td width="71">
                FROM: &nbsp;
            </td>
            <td width="155">
                <input type="text" name="txt_DATE" id="txt_DATE" value="<%=CompareDate%>" class="textbox100"
                    maxlength="10" />
                <img name="img_DATE" src="/js/FVS.gif" width="15" height="15" />&nbsp;
            </td>
            <td width="88">
                &nbsp;
            </td>
        </tr>

        <tr>
            <td>
                CIS Category
            </td>
            <td>
                <%=lstCIS%>
            </td>
            <td>
                TO:
            </td>
            <td>
                <input type="text" name="txt_DATE2" id="txt_DATE2" value="<%=CompareDate2%>" class="textbox100"
                    maxlength="10" />
                &nbsp;<img name="img_DATE2" src="/js/FVS.gif" width="15" height="15" />&nbsp;
            </td>
            <td>
               
            </td>
        </tr>
        <tr>
            <td>
                Company
            </td>
            <td>
                <%=lstCompany%>
            </td>
            <td>
              
            </td>
            <td>
            
            </td>
            <td>
            <input type="button" class="RSLButton" value="RUN REPORT" onclick="SubmitPage()" />
            </td>
        </tr>
    </table>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td bgcolor="#133E71">
                <img src="images/spacer.gif" width="750" height="1" />
            </td>
        </tr>
    </table>
    <%=TheFullTable%>
    <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="1" bgcolor="#133E71">
                <img src="images/spacer.gif" width="1" height="14" />
            </td>
            <td width="42">
                &nbsp;
            </td>
            <td width="69">
                &nbsp;
            </td>
            <td width="69">
                &nbsp;
            </td>
            <td width="69">
                &nbsp;
            </td>
            <td width="239" align="right">
                Totals for all
                <%=intRecordCount%>
                results:
            </td>
            <td width="88" align="right">
                <strong>
                    <%=FormatCurrency(Total_NetCost,2)%></strong>
            </td>
            <td width="84" align="right">
                <strong>
                    <%=FormatCurrency(Total_VAT,2)%></strong>
            </td>
            <td width="88" align="right">
                <strong>
                    <%=FormatCurrency(Total_GROSSCOST,2)%></strong>&nbsp;
            </td>
            <td width="1" bgcolor="#133E71">
                <img src="images/spacer.gif" width="1" height="14">
            </td>
        </tr>
        <tr>
            <td colspan="10" bgcolor="#133E71">
                <img src="images/spacer.gif" width="750" height="1">
            </td>
        </tr>
    </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
