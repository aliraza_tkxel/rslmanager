<%LANGUAGE="VBSCRIPT" %> 

<%
OpenDB()

dim SalesInvoiceID, SalesInvoiceTotal
SalesInvoiceID = Request("txt_SINV")
if SalesInvoiceID = "" then SalesInvoiceID = -1

Call BuildSelect(lstCATS, "sel_SALESCAT", "F_SALESCAT C INNER JOIN F_SALESINVOICEITEM I ON C.SALESCATID = I.SALESCATID WHERE I.SALEID = " & SalesInvoiceID, "C.SALESCATID, C.DESCRIPTION", "C.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " tabindex=4")
Call BuildSelect(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID",NULL, NULL, NULL, "textbox200", " onchange='SetVat()' tabindex=5")
TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")	

dim rsSalesInvoice, siSQL
siSQL = "SELECT SI.SALEID, SI.SODATE, ISNULL(SI.SONOTES, 'None') AS SONOTES, " &_
            "ITM.INVTOTAL AS INVTOTAL, " &_
            "CASE " &_
                "WHEN SI.TENANCYID IS NOT NULL THEN CUS.LIST " &_
                "WHEN SI.EMPLOYEEID IS NOT NULL THEN E.FIRSTNAME + ' ' + E.LASTNAME " &_
                "WHEN SI.SALESCUSTOMERID IS NOT NULL THEN SC.CONTACT " &_
                "WHEN SI.SUPPLIERID IS NOT NULL THEN SUP.NAME " &_
            " END AS NAME, " &_
            "CASE " &_
                "WHEN SI.TENANCYID IS NOT NULL THEN         REPLACE(REPLACE(ISNULL(P.ADDRESS1,'') + '<BR>' + ISNULL(P.ADDRESS2,'') + '<BR>' + ISNULL(P.ADDRESS3,'') + '<BR>' + ISNULL(P.TOWNCITY,'')                                            ,'<BR><BR><BR>','<BR>'),'<BR><BR>','<BR>') " &_
                "WHEN SI.EMPLOYEEID IS NOT NULL THEN        REPLACE(REPLACE(ISNULL(EC.ADDRESS1,'') + '<BR>' + ISNULL(EC.ADDRESS2,'') + '<BR>' + ISNULL(EC.ADDRESS3,'') + '<BR>' + ISNULL(EC.POSTALTOWN,'')                                      ,'<BR><BR><BR>','<BR>'),'<BR><BR>','<BR>') " &_
                "WHEN SI.SALESCUSTOMERID IS NOT NULL THEN   REPLACE(REPLACE(ISNULL(SC.ORGANISATION,'') + '<BR>' + ISNULL(SC.ADDRESS1,'') + '<BR>' + ISNULL(SC.ADDRESS2,'') + '<BR>' + ISNULL(SC.ADDRESS3,'') + '<BR>' + ISNULL(SC.TOWNCITY,'')  ,'<BR><BR><BR>','<BR>'),'<BR><BR>','<BR>') " &_
                "WHEN SI.SUPPLIERID IS NOT NULL THEN        REPLACE(REPLACE(ISNULL(SUP.BUILDINGNAME,'') + '<BR>' + ISNULL(SUP.ADDRESS1,'') + '<BR>' + ISNULL(SUP.TOWNCITY,'') + '<BR>' + ISNULL(SUP.POSTCODE,'')                                ,'<BR><BR><BR>','<BR>'),'<BR><BR>','<BR>') " &_
            " END AS ADDRESS " &_
        "FROM F_SALESINVOICE SI " &_
            "INNER JOIN ( " &_
                "SELECT ISNULL(SUM(GROSSCOST),0) AS INVTOTAL, SALEID FROM F_SALESINVOICEITEM WHERE ACTIVE = 1 GROUP BY SALEID " &_
            ") ITM ON SI.SALEID = ITM.SALEID " &_
            "LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = SI.EMPLOYEEID " &_
            "LEFT JOIN E_CONTACT EC ON EC.EMPLOYEEID = E.EMPLOYEEID " &_
            "LEFT JOIN F_SALESCUSTOMER SC ON SC.SCID = SI.SALESCUSTOMERID " &_
            "LEFT JOIN S_ORGANISATION SUP ON SUP.ORGID = SI.SUPPLIERID " &_
            "LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CUS ON CUS.I = SI.TENANCYID " &_
            "LEFT JOIN C_TENANCY CT ON CT.TENANCYID = SI.TENANCYID " &_
            "LEFT JOIN P__PROPERTY P ON CT.PROPERTYID = P.PROPERTYID " &_
        "WHERE SI.SALEID = " & SalesInvoiceID

    Call OpenRsK(rsSalesInvoice, siSQL)
    if rsSalesInvoice.RecordCount <= 0 then
        SalesInvoiceID = -1
        SalesInvoiceTotal = 0
    else 
        SalesInvoiceTotal = rsSalesInvoice("INVTOTAL")
    end if

%>
    <SCRIPT LANGUAGE="JavaScript">
	       		
	    var FormFields = new Array()
    	
	    function SetChecking(){
		    FormFields[0] = "sel_SALESCAT|Category|SELECT|Y"
		    FormFields[1] = "txt_ITEMREF|Item Ref|TEXT|Y"
		    FormFields[2] = "txt_ITEMDESC|Item Description|TEXT|N"
		    FormFields[3] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
		    FormFields[4] = "txt_VAT|VAT|CURRENCY|Y"
		    FormFields[5] = "txt_NETCOST|Net Cost|CURRENCY|Y"
	    }
    			
	    var RowCounter = 0
    	
	    function AddRow(insertPos){
		    SetChecking()			
		    if (!checkForm()) return false
		    GrossCost = document.getElementById("txt_GROSSCOST").value

		    SaveStatus = 1
    		
		    RowCounter++
		    ItemRef = document.getElementById("txt_ITEMREF").value			
		    ItemDesc = document.getElementById("txt_ITEMDESC").value			
		    SalesCatID = document.getElementById("sel_SALESCAT").value
		    SalesCatName = document.getElementById("sel_SALESCAT").options(document.getElementById("sel_SALESCAT").selectedIndex).text					
		    NetCost = document.getElementById("txt_NETCOST").value			
		    VAT = document.getElementById("txt_VAT").value											
		    VatTypeID = document.getElementById("sel_VATTYPE").value			
		    VatTypeName = document.getElementById("sel_VATTYPE").options(document.getElementById("sel_VATTYPE").selectedIndex).text					
		    VatTypeCode = VatTypeName.substring(0,1).toUpperCase()					

		    oTable = document.getElementById("ItemTable")
		    for (i=0; i<oTable.rows.length; i++){
			    if (oTable.rows(i).id == "EMPTYLINE") {
				    oTable.deleteRow(i)
				    break;
				    }
			    }

		    oRow = oTable.insertRow(insertPos)
		    oRow.id = "tr" + RowCounter
		    oRow.onclick = AmendRow
		    oRow.style.cursor = "hand"
    		
		    DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + SalesCatID + "||<>||" + SaveStatus + "\">"
    	
		    AddCell (oRow, ItemRef + DATA, ItemDesc, "", "")
		    AddCell (oRow, SalesCatName, "", "", "")		
		    AddCell (oRow, VatTypeCode, VatTypeName + " Rate", "center", "")	
		    AddCell (oRow, FormatCurrencyComma(NetCost), "", "right", "")
		    AddCell (oRow, FormatCurrencyComma(VAT), "", "right", "")
		    AddCell (oRow, FormatCurrencyComma(GrossCost), "", "right", "")
		    DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
		    AddCell (oRow, DelImage, "", "center", "#FFFFFF")

		    SetTotals (NetCost, VAT, GrossCost)
		    ResetData()
		}
    	
	    function ResetData(){
		    SetChecking()
		    checkForm()

		    ResetArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_SALESCAT")
		    for (i=0; i<ResetArray.length; i++)
			    document.getElementById(ResetArray[i]).value = ""
		    document.getElementById("sel_VATTYPE").selectedIndex = 0;					
		    document.getElementById("txt_VAT").value = "0.00";	
		    document.getElementById("AmendButton").style.display = "none"		
		    document.getElementById("AddButton").style.display = "block"
		}
    		
	    function AmendRow(){
		    event.cancelBubble = true
		    Ref = this.id
		    RowNumber = Ref.substring(2,Ref.length)
		    StoredData = document.getElementById("iDATA" + RowNumber).value
		    StoredArray = StoredData.split("||<>||")
    		
		    ReturnArray = new Array ("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST", "sel_SALESCAT")
		    for (i=0; i<ReturnArray.length; i++)
			    document.getElementById(ReturnArray[i]).value = StoredArray[i]
		    document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
		    document.getElementById("AddButton").style.display = "none"
		    document.getElementById("AmendButton").style.display = "block"		
		}
    	
	    function UpdateRow(){
		    sTable = document.getElementById("ItemTable")
		    RowData = document.getElementById("UPDATEID").value
		    RowArray = RowData.split("||<>||")
		    MatchRow = RowArray[0]
		    for (i=0; i<sTable.rows.length; i++){
			    if (sTable.rows(i).id == "tr" + MatchRow) {
				    sTable.deleteRow(i)
				    SetTotals (-RowArray[1], -RowArray[2], -RowArray[3])				
				    break;
				    }
			    }
		    AddRow(i)		
		}
    		
	    function SetTotals(iNE, iVA, iGC) {
		    totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
		    totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
		    totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

		    document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
		    document.getElementById("hiVA").value = FormatCurrency(totalVAT)
		    document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)						
    		
		    document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
		    document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
		    document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)						
	    }
    		
	    function AddCell(iRow, iData, iTitle, iAlign, iColor) {
		    oCell = iRow.insertCell()
		    oCell.innerHTML = iData
		    if (iTitle != "") oCell.title = iTitle
		    if (iAlign != "") oCell.style.textAlign = iAlign
		    if (iColor != "") oCell.style.backgroundColor = iColor
	    }
    	
	    function DeleteRow(RowID,NE,VA,GR){
		    oTable = document.getElementById("ItemTable")
		    for (i=0; i<oTable.rows.length; i++){
			    if (oTable.rows(i).id == "tr" + RowID) {
				    oTable.deleteRow(i)
				    SetTotals (-NE, -VA, -GR)				
				    break;
				    }
			    }
		    if (oTable.rows.length == 1) {
			    oRow = oTable.insertRow()
			    oRow.id = "EMPTYLINE"
			    oCell = oRow.insertCell()
			    oCell.colSpan = 7
			    oCell.innerHTML = "Please enter an item from above"
			    oCell.style.textAlign = "center"
			    }
		    ResetData()
		}

        function FindSalesInvoice(){
            FormFields = new Array()
		    FormFields[0] = "txt_SINV|Sales Credit Note|TEXT|Y"
		    if (!checkForm()) return false
        	THISFORM.method = "POST"
		    THISFORM.action = "SalesCreditNote.asp"
		    THISFORM.submit()
        }

	    function SaveSalesCreditNote(){
		    //ResetData()
		    //SetChecking()		
		    //if (!checkForm()) return false
		    if (parseFloat(document.getElementById("hiGC").value) <= 0) {
			    alert("Please enter some items before creating the credit note.")
			    return false;
			    }
			if (parseFloat(document.getElementById("hiGC").value) > <%=SalesInvoiceTotal %>) {
			    alert("The value of the credit note cannot exceed the sales invoice total")
			    return false;
			    }
		    THISFORM.target = ""
		    THISFORM.method = "POST"
		    THISFORM.action = "ServerSide/CreateSalesCreditNote_svr.asp"
		    THISFORM.submit()
		}		

		function SalesInvPopup(Sale_id){
	        window.showModelessDialog("SalesInvoice/Popups/SalesInvoice.asp?SaleID=" + Sale_id + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
	    }
    </SCRIPT>


<table STYLE='BORDER:1PX dotted BLACK' cellspacing=0 cellpadding=3 width="755">
	<tr><td><table>
	<tr> 
      <td>Sales Invoice Number: <input name="txt_SINV" type="text" class="TEXTBOX200" tabindex=1 value="<%=Request("txt_SINV") %>">&nbsp;<input type="button" onclick="FindSalesInvoice()" class="RSLButton" value=" Find "/></td>
      <td><img src="/js/FVS.gif" width=15 height=15 name="img_SINV"></td>
      <td><img src="/js/FVS.gif" width=15 height=15 name="img_TYPE"></td>
      <td>&nbsp;</td>
	</tr>
	</table></td></tr>
</table>
<br />
<% if SalesInvoiceID = -1 and len(Request("txt_SINV")) > 0 then %>
    <div><p>This sales invoice could not be found.  Please try again.</p></div>
<% elseif SalesInvoiceID > -1 then %>
    <div id="divNewItemDetails" style="display:">
    <table cellpadding=3 cellspacing=3 width=755 border=0>
        <input type="hidden" name="hid_SALESINVOICEID" value="<%=SalesInvoiceID %>" />
        <tr style="vertical-align: top;">
            <td width="100">Invoice Number: </td>
            <td width="80"><%=SaleNumber(rsSalesInvoice("SALEID")) %></td>
            <td width="300" rowspan=3 style="background-color:#f0f0f0;">Sales Notes:<br /><i><%=rsSalesInvoice("SONOTES") %></i></td>
            <td width="80">Name: </td>
            <td width="200"><%=rsSalesInvoice("NAME") %></td>
        </tr>
        <tr style="vertical-align: top;">
            <td>Tax Date:</td>
            <td><%=FormatDateTime(rsSalesInvoice("SODATE"), ShortDate) %></td>
            
            <td>Address:</td>
            <td><%=rsSalesInvoice("ADDRESS") %><br /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="button" value="View Sales Invoice" class="RSLButton" onclick="SalesInvPopup(<%=SalesInvoiceID %>)"/></td>
            <td>&nbsp;</td>
            <td style="padding: 3px; border: dotted 1px #000000; font-weight: bold;">Sales Invoice Total: <%=FormatCurrency(rsSalesInvoice("INVTOTAL")) %></td>
        </tr>
    </table>
    <br />
    <table STYLE='BORDER:1PX SOLID BLACK' cellspacing=0 cellpadding=3 width=755 border=0>
	    <tr bgcolor=steelblue style='color:white'> 
		    <td colspan=9><b>NEW CREDIT NOTE</b></td>
	    </tr>
	    <tr>
		    <td>Item : </td><td><input name="txt_ITEMREF" type="text" class="textbox200" maxlength="50" tabindex=3></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMREF"></td>
		    <td>&nbsp;Category : </td><td><%=lstCATS%></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_SALESCAT"></td>
		    <td width=100% rowspan=5 valign=bottom>
			    <input TYPE=HIDDEN NAME="IACTION">
			    <input TYPE=HIDDEN NAME="UPDATEID">			
		  	    <input TYPE=HIDDEN NAME="EXPENDITURE_LEFT_LIST">
		  	    <input TYPE=HIDDEN NAME="EMPLOYEE_LIMIT_LIST">						  
			    <table cellspacing=0 cellpadding=0>
			        <tr>
			            <td nowrap><input type=button Name="ResetButton" value=" RESET " class="RSLButton" onClick="ResetData()" tabindex=6>&nbsp;</td>
			            <td><input type=button Name="AddButton" value=" ADD " class="RSLButton" onClick="AddRow(-1)"  tabindex=6></td>
			            <td><input type=button Name="AmendButton" value=" AMEND " class="RSLButton" onClick="UpdateRow()" style="display:none" tabindex=6></td>
			        </tr>
			    </table>			
		    </td>
	    </tr>
	    <tr>
		    <td rowspan=4 valign=top>Notes : </td><td rowspan=4><textarea tabindex=3 name="txt_ITEMDESC" rows=7 class="TEXTBOX200"  style='overflow:hidden;border:1px solid #133E71'></textarea></td><td rowspan=4><img src="/js/FVS.gif" width=15 height=15 name="img_ITEMDESC"></td>
		    <td nowrap>&nbsp;Net Price : </td><td><input style='text-Align:right' name="txt_NETCOST" type="text"  class="textbox200"  maxlength=20 size=20  onBlur="TotalBoth();ResetVAT()" onFocus="alignLeft()" tabindex=5></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_NETCOST"></td>
        </tr>
        <tr>
            <td nowrap>&nbsp;Vat Type : </td><td><%=lstVAT%></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_VATTYPE"></td>			
	    </tr>
	    <tr>
		    <td>&nbsp;VAT : </td><td><input style='text-Align:right' name="txt_VAT" type="text"  class="textbox200"  maxlength=20 size=20  onBlur="TotalBoth()" onFocus="alignLeft()" VALUE="0.00" tabindex=5></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_VAT"></td>			
	    </tr>
	    <tr>
	        <td>&nbsp;Total : </td><td><input style='text-Align:right' name="txt_GROSSCOST" type="text"  class="textbox200"  maxlength=20 size=20 readonly tabindex=-1></td><td><img src="/js/FVS.gif" width=15 height=15 name="img_GROSSCOST"></td>			
	    </tr>

    </table>
    <br>
      <table STYLE='BORDER:1PX SOLID BLACK;behavior:url(/Includes/Tables/tablehl.htc)' cellspacing=0 cellpadding=3 width=755 id="ItemTable" slcolor='' hlcolor="silver">
	    <THEAD>
	    <tr bgcolor=steelblue align=RIGHT style='color:white'> 
	      <td height=20 align=left width=250 NOWRAP><b>Item Name:</b></td>
	      <td width=200 nowrap align="left"><b>Category:</b></td>
	      <td width=40 nowrap><b>Code:</b></td>
	      <td width=80 nowrap><b>Net (�):</b></td>
	      <td width=75 nowrap><b>VAT (�):</b></td>
	      <td width=80 nowrap><b>Gross (�):</b></td>
	      <td width=29 nowrap>&nbsp;</td>	  
	    </tr>
	    </THEAD>
	    <TBODY>
	    <tr ID="EMPTYLINE"><td colspan=7 align="center">Please enter an item from above</td></tr>
	    </TBODY>
      </table>
      <table STYLE='BORDER:1PX SOLID BLACK;border-top:none' cellspacing=0 cellpadding=2 width=755>
	    <tr bgcolor=steelblue align=RIGHT> 
	      <td height="20" width=490 NOWRAP style='border:none;color:white'><b>TOTAL : &nbsp;</b></td>
	      <td width="80" nowrap bgcolor="white"><b><input type="hidden" id="hiNC" value="0"><div id="iNC">0.00</div></b></td>
	      <td width="75" nowrap bgcolor="white"><b><input type="hidden" id="hiVA" value="0"><div id="iVA">0.00</div></b></td>
	      <td width="80" nowrap bgcolor="white"><b><input type="hidden" id="hiGC" value="0"><div id="iGC">0.00</div></b></td>
	      <td width="29" nowrap bgcolor="white">&nbsp;</td>		  
	    </tr>
      </table>
    <BR>
      <table cellspacing="0" cellpadding="2" width="755">
	    <tr align="right"> 
	      <td width=754 align=right nowrap>&nbsp;<input type="button" value=" CREATE CREDIT NOTE " class="RSLButton" onClick="SaveSalesCreditNote()"></td>		  
	    </tr>
      </table>	
    </div>	
<% end if %>

<% 
Call CloseRs(rsSalesInvoice)
'CloseDB()
%>

