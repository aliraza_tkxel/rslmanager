<% bypasssecurityaccess = True %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FinanceFunctions.asp" -->
<%
    Dim SQLCODE, rsSet, POID, POStatusId, POStatusName,isChild,updateParent
    POID = request.querystring("orderid")
    UserID = CInt(request.querystring("userId"))
    POStatusId = request.querystring("postatusid")
    POStatusName = CStr(request.querystring("postatusname"))
    isChild = request.querystring("ischild")
    updateParent = request.querystring("updateParent")

    Set Conn = Server.CreateObject("ADODB.Connection")
    Conn.Open(RSL_CONNECTION_STRING)
    if(InStr(POID, "-") > 0) then
      orderArray = Split(POID, "-") 
      POID = orderArray(0)
      OrderItemId = orderArray(1)      
    end if
    
    
    
    if(isChild = 1) then
         Dim WorkDetailId
        SQLCODE = "UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatusId & "  WHERE ORDERITEMID = " & OrderItemId
        SQL = "SELECT WorkDetailId from CM_ContractorWorkDetail where PurchaseOrderItemId = " & OrderItemId
         Call OpenRs(rs, SQL)
		 if not rs.EOF then
			WorkDetailId = rs("WorkDetailId") 
		end if
		 
        Call CloseRs(rs)
        if(WorkDetailId > 0) then
        SQLUpdate ="Update CM_ContractorWorkDetail SET StatusId = (Select s.StatusId from CM_Status s where s.Title='Works Completed' ) where WorkDetailId=" & WorkDetailId
         
		 Conn.Execute(SQLUpdate)
        end if

        if(updateParent = 1) then
          SQLCODE = SQLCODE & "; UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatusId & "  WHERE ORDERID = " & POID
          SQL = "Select CMContractorId,ServiceItemId from CM_ContractorWork where PurchaseOrderId =" & POID
            response.write(SQL)
			Call OpenRs(rs, SQL)
			if not rs.EOF then
              CMContractorId = rs("CMContractorId")  
              ServiceItemId = rs("ServiceItemId") 
			end if		
            CloseRs(rs)
            if(ServiceItemId > 0) then
             SQLUpdate ="Update CM_ServiceItems set StatusId =  (Select s.StatusId from CM_Status s where s.Title='Works Completed' ) WHERE ServiceItemId = " & ServiceItemId
                 Conn.Execute(SQLUpdate)
            end if 
        end if
    else
        SQLCODE = "UPDATE F_PURCHASEORDER SET POSTATUS = " & POStatusId & "  WHERE ORDERID = " & POID &_
         "; UPDATE F_PURCHASEITEM SET PISTATUS = " & POStatusId & "  WHERE ORDERID = " & POID   
        
        SQL = "Select CMContractorId,ServiceItemId from CM_ContractorWork where PurchaseOrderId =" & POID
            Call OpenRs(rs, SQL)
			if not rs.EOF then
              CMContractorId = rs("CMContractorId")  
              ServiceItemId = rs("ServiceItemId")
			end if	
            CloseRs(rs)
            if(ServiceItemId > 0) then
             SQLUpdate ="Update CM_ServiceItems set StatusId =  (Select s.StatusId from CM_Status s where s.Title='Works Completed' ) WHERE ServiceItemId = " & ServiceItemId
                 Conn.Execute(SQLUpdate)
            SQLUpdate ="Update CM_ContractorWorkDetail SET StatusId = (Select s.StatusId from CM_Status s where s.Title='Works Completed' ) where CMContractorId=" & CMContractorId
             Conn.Execute(SQLUpdate)
       
            end if 

       
    end if 
    
     Conn.Execute(SQLCODE)
       Call LOG_PO_ACTION(POID,POStatusName,"")
 
    if(isChild = 1)  then
        Call LOG_PI_ACTION (OrderItemId, POStatusName)
        if(updateParent = 1) then
         Call LOG_PO_ACTION(POID,POStatusName,"")
        end if 
    else
       Call LOG_PO_ACTION(POID,POStatusName,"")
       if(updateParent = 1) then
         Call LOG_PI_ACTION (OrderItemId, POStatusName)  
       end if
    end if
    
       
    Conn.Close()
    Set Conn = Nothing
    
    eSubject ="Goods "
    eMessage ="The Goods for the following PO have been Approved "
    If (POStatusId = 17) then
      eSubject ="Invoice "
      eMessage ="The Invoice for the following PO have been Approved "
  
    end if
    Call NotifyPO(POID, "approved", "Approved")

    Function NotifyPO(orderid, emailVerb, subjectVerb) 

    Dim supplierName
    Dim cost
    Dim requestedByEmail
    Dim employeeName
    Dim itemName
    Dim status
    Dim rs
    Dim emailbody
    Dim emailSubject
    Dim table
    Dim row
    
    Call OpenDB()
     
    ' Obtain supplier name and 
    SQL = "SELECT  " &_
            "S_ORGANISATION.NAME , " &_
            "ISNULL(E_CONTACT.WORKEMAIL, 'enterprisedev@broadlandgroup.org') AS REQUESTEDBYEMAIL " &_
            "FROM    S_ORGANISATION " &_
            "        INNER JOIN F_PURCHASEORDER ON S_ORGANISATION.ORGID = F_PURCHASEORDER.SupplierId " &_
            "        INNER JOIN E__EMPLOYEE ON EMPLOYEEID = USERID " &_
            "        INNER JOIN E_CONTACT ON E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID " &_
            "WHERE   F_PURCHASEORDER.ORDERID = " & orderid

    Call OpenRs(rs, SQL)
	if not rs.EOF then
    supplierName = rs("NAME")
    requestedByEmail = rs("REQUESTEDBYEMAIL")
	end if
    CloseRs(rs)
if (requestedByEmail<> "" ) then 
    ' Obtain purchase order item cost and approved by name
	   SQL= "SELECT " &_ 
		"(CASE WHEN APPROVED_BY IS NOT NULL THEN ISNULL(LTRIM(RTRIM(ab.FIRSTNAME)) + ' ' + LTRIM(RTRIM(ab.LASTNAME)), '') "  &_
		"ELSE ISNULL(LTRIM(RTRIM(e.FIRSTNAME)) + ' ' + LTRIM(RTRIM(e.LASTNAME)), '') END) AS EMPLOYEENAME, " &_  
		"'�' + CAST(CAST(ISNULL(pi.grosscost, 0) AS NUMERIC(10,2)) AS VARCHAR(100)) AS GROSSCOST, " &_ 
		"status.POSTATUSNAME STATUS, " &_
		"ITEMNAME, ISNULL(ITEMDESC, '') AS ITEMDESC, " &_ 
		"e.FIRSTNAME + ' ' + e.LASTNAME AS RaisedBy " &_
		"FROM F_PURCHASEORDER po " &_
		"LEFT JOIN F_PURCHASEITEM PI ON po.ORDERID = PI.ORDERID " &_
		"LEFT JOIN F_POSTATUS status ON po.POSTATUS  = status.POSTATUSID " &_
		"LEFT OUTER JOIN E__EMPLOYEE ab ON PI.APPROVED_BY = ab.EMPLOYEEID  " &_
		"LEFT OUTER JOIN E__EMPLOYEE AS e ON PI.USERID = e.EMPLOYEEID " &_
		"WHERE po.ORDERID = "  & orderid

		' Assemble details table
		' Table header
		 Call OpenRs(rs, SQL)
		 'response.Write(SQL)
		counter = 1
		totalCost = 0
		itemName =""
		itemDecription =""
		while not rs.EOF
		
			emailbody = getHTMLEmailBody()
			emailbody = Replace(emailbody, "{BudgetHolder}",rs("RaisedBy"))
		  
			emailbody = Replace(emailbody, "{PONumber}","PO "&orderid)
			totalCost = totalCost + rs("GROSSCOST")
				if (counter=1) then
				  itemName = rs("ITEMNAME")
				  itemDecription =  rs("ITEMDESC")
				else
				  itemName =  itemName & "/"& rs("ITEMNAME")  
				  itemDecription = itemDecription &"<br/>"& rs("ITEMDESC")
				end if    
				 
				 emailbody = Replace(emailbody, "{POStatus}",rs("STATUS"))
				 emailbody = Replace(emailbody, "{ApprovedBy}",rs("EMPLOYEENAME"))
				 counter = counter +1
			rs.movenext()
		wend

		Call CloseRs(rs)  
		 emailbody = Replace(emailbody, "{Message}",eMessage)
		 emailbody = Replace(emailbody, "{ItemNotes}",itemDecription)
		 emailbody = Replace(emailbody, "{SupplierName}",supplierName)  
		 emailbody = Replace(emailbody, "{ItemName}",itemName)
		 emailbody = Replace(emailbody, "{GrossCost}",totalCost)  
		' response.Write(emailbody)
		Dim iMsg
		Set iMsg = CreateObject("CDO.Message")
		Dim iConf
		Set iConf = CreateObject("CDO.Configuration")

		Dim Flds
		Set Flds = iConf.Fields
		Flds("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "10.0.1.55"
		Flds("http://schemas.microsoft.com/cdo/configuration/smtpserverport")  = 26
		Flds("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 ' cdoSendUsingPort
		Flds("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
		Flds.Update

		Set iMsg.Configuration = iConf
		iMsg.To = requestedByEmail
		iMsg.From = "noreply@broadlandgroup.org"
		iMsg.Subject = eSubject & subjectVerb
		iMsg.HTMLBody = emailbody
		iMsg.Send	
   end if 			
End Function	

Function PurchaseNumber(strWord)
    PurchaseNumber = "PO " & characterPad(strWord,"0", "l", 7)
End Function	
Function getHTMLEmailBody()

        getHTMLEmailBody = "<!DOCTYPE html><html><head><title>"&emailSubject&"</title></head> " & _
                           "         <body> " & _
                           "             <style type=""text/css""> " & _
                           "                 .topAllignedCell{vertical-align: top;} " & _
                           "                 .bottomAllignedCell{vertical-align: bottom;} " & _
                           "             </style> " & _
                           "             <span>Dear {BudgetHolder}</span> " & _
                           "             <p>{Message}</p> " & _
                           "             <table><tbody> " & _
                           "                     <tr><td>PO Number:</td><td>{PONumber}</td></tr> " & _
                           "                      <tr><td>Supplier Name:</td><td>{SupplierName}</td></tr> " & _
                           "                     <tr><td>Item Name:</td><td>{ItemName}</td></tr> " & _
                           "                     <tr><td class=""topAllignedCell"">Notes:</td><td>{ItemNotes}</td></tr> " & _
                           "                     <tr><td>&nbsp;</td><td>&nbsp;</td></tr> " & _
                           "                     <tr><td>Gross(�):</td><td>{GrossCost}</td></tr> " & _
                           "                     <tr><td>Status:</td><td>{POStatus}</td></tr> " & _
                           "                     <tr><td>Approved by:</td><td>{ApprovedBy}</td></tr> " & _
                           "             </tbody></table><br /><br /> " & _
                           "             <table><tbody><tr><td><img src=""cid:broadlandImage"" alt=""Broadland Housing Group"" /></td> " & _
                           "                         <td class=""bottomAllignedCell""> " & _
                           "                             Broadland Housing Group<br /> " & _
                           "                             NCFC, The Jarrold Stand<br /> " & _
                           "                             Carrow Road, Norwich, NR1 1HU<br /> " & _
                           "                         </td> " & _
                           "                     </tr></tbody></table></body></html> "
    End Function



    Function sendEmail(userId)
        Call OpenDB()
        SQL = "SELECT WORKEMAIL FROM dbo.E_CONTACT C WHERE EMPLOYEEID=" & userId
	    Call OpenRs(rsEmail, SQL)

	    If NOT rsEmail.EOF And NOT rsEmail.BOF Then 
		    ' send email
		    Dim strRecipient
		    strRecipient=(rsEmail.Fields.Item("WORKEMAIL").Value)

            Dim iMsg
            Set iMsg = CreateObject("CDO.Message")
            Dim iConf
            Set iConf = CreateObject("CDO.Configuration")

            Dim Flds
            Set Flds = iConf.Fields
            Flds("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "10.0.1.55"
            Flds("http://schemas.microsoft.com/cdo/configuration/smtpserverport")  = 25
            Flds("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 ' cdoSendUsingPort
            Flds("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
            Flds.Update

            emailbody = "Approve"

            Set iMsg.Configuration = iConf
            iMsg.To = strRecipient
            iMsg.From = strRecipient
            iMsg.Subject = "New ASB Item Logged"
            iMsg.HTMLBody = emailbody
            iMsg.Send

	    End If
	    Call CloseRs(rsEmail)
        Call CloseDB()
    End Function
        

%>