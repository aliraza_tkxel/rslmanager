<%@  language="VBSCRIPT" codepage="1252" %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (7)	 'USED BY CODE
	Dim DatabaseFields (7)	 'USED BY CODE
	Dim ColumnWidths   (7)	 'USED BY CODE
	Dim TDSTUFF        (7)	 'USED BY CODE
	Dim TDPrepared	   (7)	 'USED BY CODE
	Dim ColData        (7)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (7)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (7)	 'All Array sizes must match	
	Dim TDFunc		   (7)

	THE_TABLE_HIGH_LIGHT_COLOUR = "red"
		
	ColData(0)  = "Order No|ORDERID|90"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Ordered|FORMATTEDPODATE|75"
	SortASC(1) 	= "PODATE ASC"
	SortDESC(1) = "PODATE DESC"	
	TDSTUFF(1)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(1) = ""		

	ColData(2)  = "By|FULLNAME|100"
	SortASC(2) 	= "FULLNAME ASC"
	SortDESC(2) = "FULLNAME DESC"	
	TDSTUFF(2)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(2) = ""		

	ColData(3)  = "Name|PONAME|165"
	SortASC(3) 	= "PONAME ASC"
	SortDESC(3) = "PONAME DESC"	
	TDSTUFF(3)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(3) = ""		

	ColData(4)  = "CIS Category|CISCATEGORY|100"
	SortASC(4) 	= "O.CISCATEGORY ASC"
	SortDESC(4) = "O.CISCATEGORY DESC"
	TDSTUFF(4)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(4) = ""

    ColData(5)  = "STATUS|POSTATUSNAME|95"
	SortASC(5) 	= "PS.POSTATUSNAME ASC"
	SortDESC(5) = "PS.POSTATUSNAME DESC"
	TDSTUFF(5)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(5) = ""
    
    ColData(6)  = "PaymentType|PaymentType|110"
	SortASC(6) 	= "O.PaymentType ASC"
	SortDESC(6) = "O.PaymentType DESC"
	TDSTUFF(6)  = " "" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "","" & rsSet(""POSTATUSID"") & "")"""" "" "
	TDFunc(6) = ""

	ColData(7)  = "Cost|FORMATTEDCOST|80"
	SortASC(7) 	= "TOTALCOST ASC"
	SortDESC(7) = "TOTALCOST DESC"	
	TDSTUFF(7)  = " ""align='right'"" "
	'TDFunc(6) = "FormatCurrency(|) & "" <img style='cursor:pointer; cursor:hand;' src='../../IMAGES/IconAttachment.gif' onclick='return window.open("""""" & REPLACE(rsSet(""image_url""),"" "",""%20"") & """""",""""_blank"""",""""toolbar=no"""");'>"" "		

	PageName = "ReconcileList.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """"""  """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PONumber") <> "") then
		if (isNumeric(Request("PONumber"))) then
			POFilter = " AND PO.ORDERID = '" & CLng(Request("PONumber")) & "' "
		end if
	end if
	
	SupFilter = ""
    rqSupplier = Request("sel_SUPPLIER")
	if (rqSupplier<> "") then
		SupFilter = " AND PO.SUPPLIERID = '" & rqSupplier & "' "
	end if

    CompanyFilter = ""
    rqCompany = Request("sel_COMPANY")
	if (rqCompany <> "") then
		CompanyFilter = " AND PO.COMPANYID = '" & rqCompany & "' "
	end if

		SQLCODE= " SELECT DISTINCT " &_
            "	E.FIRSTNAME + ' ' + E.LASTNAME		AS FULLNAME " &_
            "	,PO.ORDERID " &_
            "	,O.NAME								AS SUPPLIER " &_
            "	,CONVERT(VARCHAR, ISNULL(PI.TOTALCOST,0), 1)	AS FORMATTEDCOST " &_
            "	,ISNULL(PI.TOTALCOST,0)	AS TOTALCOST " &_
            "	,PS.POSTATUSNAME					AS POSTATUSNAME " &_
            "	,PONAME " &_
            "	,FORMATTEDPODATE =					CONVERT(VARCHAR, PODATE, 103) " &_
            "	, PODATE " &_
            "	,PS.POSTATUSID						AS POSTATUSID " &_
            "   ,ISNULL(POL.image_url,'')               AS image_url " &_
            "   ,O.CISCATEGORY                            AS CISCATEGORY " &_
            "   ,F_PAYMENTTYPE.DESCRIPTION                as PaymentType " &_ 
            " FROM " &_
            "	F_PURCHASEORDER PO " &_
            "	CROSS APPLY( SELECT PIL.ORDERITEMID FROM F_PURCHASEITEM_LOG PIL WHERE ORDERID = PO.ORDERID AND PIL.PISTATUS = 17 ) InvoiceReceived " &_
            "	CROSS APPLY( SELECT SUM(GROSSCOST) AS TOTALCOST FROM F_PURCHASEITEM PI WHERE ACTIVE = 1 AND PI.ORDERID = PO.ORDERID) PI " &_
            "   Cross Apply (SELECT 1 AS ONE from F_PURCHASEITEM FPI "&_
            "   Where FPI.PISTATUS IN(17) and FPI.ORDERID = PO.ORDERID AND ( InvoiceReceived.ORDERITEMID = FPI.ORDERITEMID) ) reconcile" &_
            "	INNER JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID" &_
            "   OUTER APPLY (SELECT TOP 1 image_url FROM F_PURCHASEITEM_LOG PILInner WHERE PILInner.ORDERID = PO.ORDERID AND PILInner.image_url IS NOT NULL ORDER BY TIMESTAMP DESC) POL " &_
            "	LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID " &_
            "	LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID " &_
            "	LEFT JOIN (SELECT F.ORDERID FROM F_PURCHASEORDER F " &_
            "						INNER JOIN F_PURCHASEITEM I ON I.ORDERID = F.ORDERID " &_
            "						INNER JOIN P_WOTOREPAIR W ON W.ORDERITEMID = I.ORDERITEMID " &_
            "						INNER JOIN DBO.C_JOURNAL J ON J.JOURNALID = W.JOURNALID	AND J.ITEMNATUREID IN (47, 60, 62) " &_
            "			) WORDER ON PO.ORDERID = WORDER.ORDERID " &_
            " INNER JOIN F_PAYMENTTYPE ON F_PAYMENTTYPE.PAYMENTTYPEID = O.PAYMENTTYPE " &_
            " WHERE PO.ACTIVE = 1 AND PO.POSTATUS IN (0,1,2,3,4,5,7,17,18,19)" & SupFilter & CompanyFilter  & POFilter &_
			" Order By " + Replace(orderBy, "'", "''") + ""	
        'response.Write(SQLCODE)
        'response.End()
'AND WORDER.ORDERID IS NULL
'            " WHERE PO.ACTIVE = 1 AND PO.POSTATUS IN (1,2,7,17,18) AND WORDER.ORDERID IS NULL " & SupFilter & POFilter &_
                   		
'		"WHERE PO.ACTIVE = 1 and PO.POSTATUS IN (5,7,9,17,18) " & SupFilter & POFilter &_
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	
	'response.Write(SQLCODE)
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select Supplier", rqSupplier, NULL, "textbox200", " style='width:240px'")	
    Call BuildSelect(lstCompany, "sel_COMPANY", "G_COMPANY", "COMPANYID, DESCRIPTION", "COMPANYID", "Please Select Company", rqCompany, NULL, "textbox200", " style='width:200px'")	
	
	CloseDB()
%>
<html>
<head>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <title>RSL Manager Finance --> Purchase Order List</title>
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <script language="JavaScript" type="text/javascript" src="/js/preloader.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/general.js"></script>
    <script language="JavaScript" type="text/javascript" src="/js/menu.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="JavaScript" type="text/javascript">
<!--

        function showAlert(subject, message) {
            $("#message").text(message);
            $("#AlertMessage").dialog({
                title: subject,
                modal: true,
                draggable: false,
                resizable: false,
                width: 400,
                position: {
                    my: "center",
                    at: "center",
                    of: $("body"),
                    within: $("body")
                },
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        function load_me(Order_id, PO_StatusId) {
            //if (PO_StatusId == 17 ) { // Changed PO_StatusId from 17 to 18

            location.href = "ReconcileConfirm.asp?OrderID=" + Order_id + "&CurrentPage=<%=intpage%>&suppId=" + thisForm.sel_SUPPLIER.value; ;
            //}
            /* else if (PO_StatusId == 17) {
            showAlert('Information!', 'Goods ordered have not been approved for the the purchase order you are attempting to reconcile.');
            }
            else if (PO_StatusId == 7) {
            showAlert('Information!', 'Invoice has not been approved for the the purchase order you are attempting to reconcile.');
            }
            else if (PO_StatusId == 9) {
            showAlert('Information!', 'Invoice has been reconciled.');
            }
            else {
            showAlert('Information!', 'Invoice has not been received for the the purchase order you are attempting to reconcile.');
            }*/

        }

        function RemoveBad() {
            strTemp = event.srcElement.value;
            strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g, "");
            event.srcElement.value = strTemp;
        }

        function setSupplier() {
            thisForm.sel_SUPPLIER.value = '<%=Request("sel_SUPPLIER")%>';

        }
        function checkReconcile() {

            var reconcile = "true";
            if (reconcile == '<%=Request("reconcile")%>') {
                alert("The PO had already been reconciled by some other user.");
            }


        }

        function SubmitPage() {
            if (isNaN(thisForm.PONumber.value)) {
                alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
                return false;
            }
			var suppId=thisForm.sel_SUPPLIER.value;
            var COMPANYID =thisForm.sel_COMPANY.value;
            location.href = '<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>' + suppId + "&PONumber=" + thisForm.PONumber.value + "&sel_COMPANY=" + COMPANYID;
        }
    // -->
    </script>
    <!-- End Preload Script -->
</head>
<body bgcolor="#FFFFFF" onload="initSwipeMenu(1);preloadImages();setSupplier(); checkReconcile()" onunload="macGo()"
    marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <form name="thisForm" method="post">
        Click on the purchase order that you would like to reconcile.
		
    <table class="RSLBlack">
        <tr>
            <td>
                <b>&nbsp;QUICK FIND Facility</b>
            </td>
            <td>
                <input type="text" name="PONumber" class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>"
                    onblur="RemoveBad()" style='width: 135px; background-image: url(img/CXfilter.gif); background-repeat: no-repeat; background-attachment: fixed' />
            </td>
            <td>
                <%=lstSuppliers%>
            </td>
             <td>
                <%=lstCompany%>
            </td>
            <td>
                <input type="button" class="RSLButton" value=" Update Search " onclick="SubmitPage()" />
            </td>
        </tr>
    </table>
        <table width="750" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td bgcolor="#133E71">
                    <img alt="" src="images/spacer.gif" width="750" height="1" />
                </td>
            </tr>
        </table>
        <%=TheFullTable%>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
    <iframe src="/secureframe.asp" name="frm_team" width="400px" height="400px" style='display: none'></iframe>
    <div id="dialog" title="Alert message" style="display: none">
        <div class="ui-dialog-content ui-widget-content">
            <p>
                <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0"></span>
                <label id="lblMessage">
                </label>
            </p>
        </div>
    </div>
    <div id="AlertMessage" style="display: none;">
        <p id="message">
        </p>
    </div>
</body>
</html>

