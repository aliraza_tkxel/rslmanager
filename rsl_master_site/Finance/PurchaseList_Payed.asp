<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE
	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)
	Dim clean_desc
	
	ColData(0)  = "Order No|ORDERID|90"
	SortASC(0) 	= "PO.ORDERID ASC"
	SortDESC(0) = "PO.ORDERID DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = "PurchaseNumber(|)"

	ColData(1)  = "Supplier|SUPPLIER|100"
	SortASC(1) 	= "SUPPLIER ASC"
	SortDESC(1) = "SUPPLIER DESC"	
	TDSTUFF(1)  = ""
	TDFunc(1) = ""		

	ColData(2)  = "By|FIRSTNAME|100"
	SortASC(2) 	= "FIRSTNAME ASC"
	SortDESC(2) = "FIRSTNAME DESC"	
	TDSTUFF(2)  = " "" TITLE="""""" & rsSet(""FIRSTNAME"") & "" "" & rsSet(""LASTNAME"") & """""" "" "
	TDFunc(2) = ""		

	ColData(3)  = "Item|PONAME|175"
	SortASC(3) 	= "PONAME ASC"
	SortDESC(3) = "PONAME DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""		

	ColData(4)  = "Cost|FORMATTEDCOST|60"
	SortASC(4) 	= "QUOTEDCOST ASC"
	SortDESC(4) = "QUOTEDCOST DESC"	
	TDSTUFF(4)  = " ""align='right'"" "
	TDFunc(4) = "FormatCurrency(|)"		

	PageName = "PurchaseList_Payed.asp"
	EmptyText = "No Relevant Purchase Orders found in the system!!!"
	DefaultOrderBy = SortDESC(0)
	RowClickColumn = " "" TITLE="""""" & rsSet(""POSTATUSNAME"") & """""" ONCLICK=""""load_me("" & rsSet(""ORDERID"") & "")"""" """ 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()

	POFilter = ""
	if (Request("PONumber") <> "") then
		POFilter = " AND ORDERID = '" & Request("PONumber") & "' "
	end if
	
	SupFilter = ""
	if (Request("sel_SUPPLIER") <> "") then
		SupFilter = " AND PO.SUPPLIERID = '" & Request("sel_SUPPLIER") & "' "
	end if

	SQLCODE =" SELECT E.FIRSTNAME, E.LASTNAME, PO.ORDERID, O.NAME AS SUPPLIER, "&_	
		 " CONVERT(VARCHAR,PI.TOTALCOST,1) AS FORMATTEDCOST, "&_	
		 " PS.POSTATUSNAME, PONAME, "&_	
		 " FORMATTEDPODATE=CONVERT(VARCHAR,PODATE,103), BAC.PROCESSDATE "&_	
	" FROM F_PURCHASEORDER PO "&_	
		" INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID "&_	
				" FROM F_PURCHASEITEM "&_	
				" WHERE ACTIVE = 1 "&_	
				" GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID "&_	
		" LEFT JOIN F_POSTATUS PS ON PO.POSTATUS = PS.POSTATUSID "&_	
		" LEFT JOIN S_ORGANISATION O ON PO.SUPPLIERID = O.ORGID "&_	
		" LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = PO.USERID "&_	
		" LEFT JOIN F_INVOICE I ON I.ORDERID = PO.ORDERID "&_	
		" LEFT JOIN F_POBACS BAC ON BAC.INVOICEID = I.INVOICEID "&_	
		" WHERE PO.POSTATUS IN (10,11,12) AND PO.ACTIVE = 1 " & SupFilter & POFilter &_	
		" Order By " + Replace(orderBy, "'", "''") + ""

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

	OpenDB()
	Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " style='width:240px'")	
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Purchase Order List</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function load_me(Order_id){
	window.showModelessDialog("Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")
	}

function RemoveBad() { 
strTemp = event.srcElement.value;
strTemp = strTemp.replace(/\<|\>|\"|\'|\%|\;/g,"");
event.srcElement.value = strTemp;
}

function setSupplier(){
	thisForm.sel_SUPPLIER.value = "<%=Request("sel_SUPPLIER")%>";
	}

function SubmitPage(){
	if (isNaN(thisForm.PONumber.value)){
		alert("When searching for a Purchase Order you only need to enter the last identifiable digits.\nFor example to search for the Purchase Order 'PO00000345' enter '345'.")
		return false;
		}
	location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value + "&PONumber=" + thisForm.PONumber.value
	}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages();setSupplier()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=get>
<table class="RSLBlack"><tr><td><b>&nbsp;QUICK FIND Facility</b></td>
<td><input type=text name=PONumber class="RSLBlack" value="<%=Server.HTMLEncode(Request("PONumber"))%>" onblur="RemoveBad()" style='width:135px;background-image: url(img/CXfilter.gif);background-repeat: no-repeat; background-attachment: fixed'></td>
<td><%=lstSuppliers%>
<%
Dim myArray

MyArray=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", "Oct", "Nov","Dec")
DrpDown1 = "<select name='theDate'><option>Select Month</option>"
For I=0 to 11
DrpDown1 = DrpDown1 & "<option id=1/" & Myarray(I) & "/2004'>" &  Myarray(I) & "/2004</option>"
Next
DrpDown1 = DrpDown1 & "</select>"
response.write DrpDown1
%></td>
	<td><select name="year" class="textbox">
<option value="2004" selected>2004</option>
	</select>
        </td>
<td><input type=button class="RSLButton" value=" Update Search " onclick="SubmitPage()">
</td></tr></table>
<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_team width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>