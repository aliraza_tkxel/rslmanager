<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK_POPUP.asp" -->
<%
	OpenDB()

	'TO BEGIN GET THE DEVELOPMENT ID COST CENTRE
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID'"
	Call OpenRs(rsDev, SQL)
	if (NOt rsDev.eOF) then
		DevID = rsDev("DEFAULTVALUE")
	else
		'NOT FOUND THEREFORE REDIRECT TO DISPLAY
		Response.Redirect ("FinanceBuilderDev_svr.asp")		
	end if
	CloseRs(rsDev)

	CheckList = Replace(Request.Form("iChecks"), " ", "")

	'GET THE INFORMATION REQUIRED TO CREATE A NEW HEAD SCHEME	
	SchemeName = Request.Form("txt_SCHEME")
	SchemeTotal = Request.Form("txt_SCHEMEMANUALTOTAL")
	CurrentDate = FormatDateTime(Date,1)

	'BUILD THE SQL STRING AND INSERT THE DATA...but return the identity so we can use it later on
	SQL = "SET NOCOUNT ON;INSERT INTO F_HEAD (DESCRIPTION, HEADDATE, USERID, ACTIVE, LASTMODIFIED, HEADALLOCATION, COSTCENTREID) VALUES " &_
		"('" & SchemeName & "', '" & CurrentDate & "', " & Session("USERID") & ", 1, '" & CurrentDate & "', " & SchemeTotal & ", " & DevID & ");" &_
		"SELECT SCOPE_IDENTITY() AS HEADID"
	
		Response.Write SQL
	Set rsSet = Conn.Execute(SQL)
	HeadID = rsSet.fields("HEADID").value
	rsSet.close()
	Set rsSet = Nothing

	'SPLIT THE CHECKLIST INTO AN ARRAY SO WE CAN LOOP THORUGH EACH OF THE ITEMS....	
	CheckArray = Split(CheckList, ",")

	'FINANLY INSERT EACH ITEM INTO THE DATABASE
	for i=0 to Ubound(CheckArray)
		SQL = "INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREALLOCATION, EXPENDITUREDATE, USERID, ACTIVE, LASTMODIFIED, HEADID, QBDEBITCODE, QBCONTROLCODE) VALUES " &_
				"('" & Request.Form("txt_Code" & CheckArray(i)) & "', " & Request.Form("txt_Val" & CheckArray(i)) & ", '" & CurrentDate & "', " & Session("USERID") & ", 1, '" & CurrentDate & "', " & HeadID  & ", '" & Request.Form("txt_QBDEBITCODE" & CheckArray(i))  & "', '" & Request.Form("txt_QBCONTROLCODE" & CheckArray(i)) & "')"
		RESPONSE.WRITE sql
		Conn.Execute(SQL)				
	next
	
	//FINALLY INSERT THE LAST VALUES INTO THE DEVELOPMENT_RATES TABLE
	GrantFrom = "NULL"
	if (Request.Form("sel_FROM") <> "") then GrantFrom = Request.Form("sel_FROM")
	SQL = "INSERT INTO F_DEVELOPMENT_RATES (HEADID, [GRANT], LOAN, GRANTFROM, INTERESTRATE) VALUES " &_
		"(" & HeadID & ", " & Request.Form("txt_GRANT") & ", " & Request.Form("txt_LOAN") & ", " & GrantFrom & ", " & Request.Form("txt_INTERESTRATE") & ")"
	Conn.Execute(SQL)	

	CloseDB()
		
	Response.Redirect ("FinanceBuilderDev_svr.asp")
%>