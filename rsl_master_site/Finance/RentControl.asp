<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst_post, lst_paymenttype
	
	OpenDB()
	
	Call BuildSelect(lst_post, "sel_DIRECTPOSTITEMS", " F_MISPOSTITEMS ", "ITEMID, ITEMNAME", "ITEMNAME", "Please Select", 0, null, "textbox200", "ONCHANGE='get_items()' disabled")	
	Call BuildSelect(lst_paymenttype, "sel_", " F_DIRECTPOSTITEMS ", "ITEMID, ITEMNAME", "ITEMNAME", "Please Select", 0, null, "textbox200", "ONCHANGE='get_items()' disabled")

	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	SQL = "SELECT * FROM F_FISCALYEARS WHERE YSTART <= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103) AND YEND >= CONVERT(SMALLDATETIME,CONVERT(VARCHAR,GETDATE(),103),103)"
	Call OpenRs(rsTAXDATE, SQL)	
	YearStartDate = FormatDateTime(rsTAXDATE("YSTART"),1)
	YearendDate = FormatDateTime(rsTAXDATE("YEND"),1)
	Call CloseRs(rsTAXDATE)

	CloseDB()	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Error Posting</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var is_a_valid_tenancy = 0
	var strtotal = ""
	function SetForSave(){
		// SET APPROPRIATE FORM FIELDS
		FormFields.length = 0		
		FormFields[0] = "hid_TENANTNUMBER|TENANTNUMBER|INTEGER|Y"
		FormFields[1] = "sel_DIRECTPOSTITEMS|DIRECTPOSTITEMS|SELECT|Y"
	 	FormFields[2] = "txt_PROCESSDATE|Processing Date|DATE|Y"
		FormFields[3] = "txt_FROM|FROM|DATE|Y"
		FormFields[4] = "txt_TO|TO|DATE|Y"
		FormFields[5] = "txt_RENT|RENT|CURRENCY|N"
		FormFields[6] = "txt_SUPPORT|SUPPORT|CURRENCY|N"
		FormFields[7] = "txt_INELIGIBLE|INELIGIBLE|CURRENCY|N"
		FormFields[8] = "txt_WATER|WATER|CURRENCY|N"
		FormFields[9] = "txt_COUNCIL|COUNCIL|CURRENCY|N"
		FormFields[10] = "txt_GARAGE|GARAGE|CURRENCY|N"
		FormFields[11] = "txt_SERVICE|SERVICE|CURRENCY|N"
		}

	function SetForCalc(){
		// SET APPROPRIATE FORM FIELDS
		FormFields.length = 0		
		FormFields[0] = "txt_RENT|RENT|CURRENCY|N"
		FormFields[1] = "txt_SUPPORT|SUPPORT|CURRENCY|N"
		FormFields[2] = "txt_INELIGIBLE|INELIGIBLE|CURRENCY|N"
		FormFields[3] = "txt_WATER|WATER|CURRENCY|N"
		FormFields[4] = "txt_COUNCIL|COUNCIL|CURRENCY|N"
		FormFields[5] = "txt_GARAGE|GARAGE|CURRENCY|N"
		FormFields[6] = "txt_SERVICE|SERVICE|CURRENCY|N"
		}


	function ATTACHITEM (JournalID, JournalValue){
		RSLFORM.txt_AMOUNT.value = FormatCurrency(JournalValue);	
		RSLFORM.hid_ATTACHID.value = JournalID
		RSLFORM.txt_AMOUNT.readOnly = true
		RSLFORM.Detach.style.visibility = "visible";					
		}
	
	function ATTACHBANK(BankID){
		RSLFORM.hid_ATTACHID.value = BankID	
		}

	function ATTACHCUSTOMER(CustomerID){
		RSLFORM.hid_ATTACHID.value = CustomerID	
		}
		
	function DETACHITEM(){
		RSLFORM.txt_AMOUNT.readOnly = false
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";
		RSLFORM.hid_ATTACHID.value = "";															
		ref = document.getElementsByName("attach")
		if (ref.length){
			for (i=0; i<ref.length; i++){
				ref[i].checked = false
				}
			}
		}
		
	function return_zero(strzero)
	{
		if(strzero=="")
		{
			return parseFloat("0");
		}
		else
		{
			return parseFloat(strzero);		
		}
	}	

	function strtotal_format(title,str)
	{

		if (str=="")
		{
			return ""
		}
		else
		{
			return "\n " + title + ": " + FormatCurrency(return_zero(str),2)
		}
	}
	
	function calc_date()
	{
	SetForSave()
	checkForm()
	}	
	
	function calc_form()
	{
	//debugger
	var total = 0
	SetForCalc()
	if (checkForm())
	{
	strtotal = "\n "

	total = total + return_zero(RSLFORM.txt_RENT.value)
	strtotal = strtotal + strtotal_format("Rent",RSLFORM.txt_RENT.value)
	
	total = total + return_zero(RSLFORM.txt_SERVICE.value)
	strtotal = strtotal + strtotal_format("Services",RSLFORM.txt_SERVICE.value)
	
	total = total + return_zero(RSLFORM.txt_INELIGIBLE.value)
	strtotal = strtotal + strtotal_format("Ineligible Services",RSLFORM.txt_INELIGIBLE.value)
		
	total = total + return_zero(RSLFORM.txt_SUPPORT.value)
	strtotal = strtotal + strtotal_format("Support Services",RSLFORM.txt_SUPPORT.value)
		
	total = total + return_zero(RSLFORM.txt_WATER.value)
	strtotal = strtotal + strtotal_format("Water Rates",RSLFORM.txt_WATER.value)
		
	total = total + return_zero(RSLFORM.txt_COUNCIL.value)
	strtotal = strtotal + strtotal_format("Council Tax",RSLFORM.txt_COUNCIL.value)
					
	total = total + return_zero(RSLFORM.txt_GARAGE.value)
	strtotal = strtotal + strtotal_format("Garage",RSLFORM.txt_GARAGE.value)		

	
	RSLFORM.txt_AMOUNT.value = FormatCurrency(total)
	checkForm()
	}

	}			
	function save_form(){
		calc_form()
		SetForSave()
		if (!checkForm()) return false;

		// MAKE SURE ORDER DATE IS IN CURRENT FISCAL YEAR
		var YStart = new Date()
		YStart.setHours(0,0,0,0)

		
		if (real_date(RSLFORM.txt_PROCESSDATE.value) < YStart){
			alert("Please enter either current or future date as process date");
			return false;
		}

		if (real_date(RSLFORM.txt_TO.value) <= real_date(RSLFORM.txt_FROM.value)){
			alert("The to date must be after the from date for the correction period");
			return false;
		}
		
		if (parseFloat(RSLFORM.txt_AMOUNT.value) == 0){
			alert("To post a correction the total must be greater than 0");
			return false;
		}
		// END ------------------------------------------


		tenantNumber = RSLFORM.txt_TENANTSEARCH.value

		PostingType  = RSLFORM.sel_DIRECTPOSTITEMS[ RSLFORM.sel_DIRECTPOSTITEMS.selectedIndex].text;
		ProcessDate	 = RSLFORM.txt_PROCESSDATE.value
		
		if (RSLFORM.txt_FROM.value != ""){
			FromToString = "\n\n Date From : " + RSLFORM.txt_FROM.value + "\n\n Date To : " +  RSLFORM.txt_TO.value
			}
		else {
			FromToString = ""
			}
		
		Amount		 = RSLFORM.txt_AMOUNT.value
		alert("Tenant Number : " + tenantNumber + " \n\nPosting Type : " + PostingType  + " \n\nProcess Date : " + ProcessDate + FromToString + strtotal + " \n\nAmount : " + Amount + " \n\n\n\n Please confirm that you have checked all the submitted information is correct.")

		if (confirm("Please confirm that you have checked all the submitted information is correct.")){
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait posting...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		RSLFORM.action = "serverside/rentcontrol_srv.asp";
		RSLFORM.btn_POST.disabled = true;		
		RSLFORM.submit();
		}
	}
	
	// 1 = called by tenant number 2 = callled by claim number
	function get_tenant(int_which){
	
		// SET APPROPRIATE FORMM FIELDS
		FormFields.length = 0
		if (int_which == 1)
			FormFields[0] = "txt_TENANTSEARCH|Tenant Number|INTEGER|Y"
		else 
			FormFields[0] = "txt_CLAIMNUMBER|Claim Number|TEXT|Y"
			
		if (!checkForm()) return false;
		
		ACCOUNT_DIV.innerHTML = "";
		STATUS_DIV.innerHTML = "<FONT STYLE='FONT-SIZE:13PX'><B>Please wait finding tenant...</B></FONT>";
		STATUS_DIV.style.visibility = "visible";
		RSLFORM.target = "frm_direct";
		if (int_which == 1)
			RSLFORM.action = "serverside/rentcontrol_srv.asp?action=GET";
		else
			RSLFORM.action = "serverside/rentcontrol_srv.asp?action=GETWITHHBREF";
		
		RSLFORM.Detach.style.visibility = "hidden";
		RSLFORM.txt_AMOUNT.value = "";											
		RSLFORM.hid_ATTACHID.value = "";													
		RSLFORM.submit();
		
	
	}
	var dates_required = 0 //used to determine whether or not to force from and to dates
	
	// retrieve items from this rent account if cheque or DD is selected 
	function get_items(){

	
		SetForSave()

		checkForm()


			ACCOUNT_DIV.innerHTML = "";
	}
			
	function real_date(str_date){
		
		var datearray;
		var months = new Array("nowt","jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		str_date = new String(str_date);
		datearray = str_date.split("/");

		return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
		
	}
		
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR>
			
      
    <TD>To enable the management of errors that occur on the different rent charge 
      processes that affect a customer account.</TD>
		</TR>
		<TR style=height:15px><TD></td></TR>
	</TABLE>
			
    
<TABLE BORDER=0 width=90% CELLPADDING=2 CELLSPACING=0 align=center>
  <form name = RSLFORM method=post>
    <TR> 
      <TD nowrap><B>Enter Tenant Number :</B></TD>
      <TD colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_TENANTSEARCH CLASS='TEXTBOX100' MAXLENGTH=10>
        <image src="/js/FVS.gif" name="img_TENANTSEARCH" width="15px" height="15px" border="0"> 
        <INPUT TYPE=BUTTON CLASS='RSLBUTTON' NAME='btn_FINDT' VALUE=' Find ' onclick='get_tenant(1)'>
      </TD>
      <TD valign=top STYLE='BORDER:1PX SOLID #133E71'  width=90% bgcolor=beige rowspan=7 height=100px> 
        <div id=details>&nbsp;</div>
      </TD>
    </TR>
 
    <TR style=height:15px> 
      <TD colspan=3></td>
    </TR>
    <TR> 
      <TD nowrap><B>Posting Type :</B></TD>
      <TD nowrap><%=lst_post%> <image src="/js/FVS.gif" name="img_DIRECTPOSTITEMS" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
    <TR> 
      <TD nowrap><span id=processspan style='visibility:visible'><b>Process Date :</b></span></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_PROCESSDATE CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible' onChange="calc_date()">
        <image src="/js/FVS.gif" name="img_PROCESSDATE" width="15px" height="15px" border="0"> 
      </TD>
    </TR>
    <TR> 
      <TD nowrap><span id=fromspan style='visibility:visible'><b>Correction Period:</b></span></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_FROM CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_date()">
        <image src="/js/FVS.gif" name="img_FROM" width="15px" height="15px" border="0"> 
        &nbsp;<B><span id=tospan style='visibility:visible'>To:</span></B> 
        <INPUT TYPE=TEXT NAME=txt_TO CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_date()">
        <image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">	
      </TD>
    </TR>
	    <TR> 
      <TD nowrap><b><span id=rentspan style='visibility:visible'>Rent :</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_RENT CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible' onChange="calc_form()">
		<image src="/js/FVS.gif" name="img_RENT" width="15px" height="15px" border="0">	
      </TD>
    </TR>
	    <TR> 
      <TD nowrap><b><span id=ineligiblespan style='visibility:visible'>Services:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_SERVICE CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
        <image src="/js/FVS.gif" name="img_SERVICE" width="15px" height="15px" border="0">
      </TD>
    </TR>
	    <TR> 
      <TD nowrap><b><span id=ineligiblespan style='visibility:visible'>Ineligible Services:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_INELIGIBLE CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
        <image src="/js/FVS.gif" name="img_INELIGIBLE" width="15px" height="15px" border="0">
      </TD>
    </TR>
	  
	    <TR> 
      <TD nowrap><b><span id=supportspan style='visibility:visible'>Supported Services:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_SUPPORT CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
	<image src="/js/FVS.gif" name="img_SUPPORT" width="15px" height="15px" border="0">
      </TD>
    </TR>
	    <TR> 
      <TD nowrap><b><span id=waterspan style='visibility:visible'>Water Rates:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_WATER CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
         <image src="/js/FVS.gif" name="img_WATER" width="15px" height="15px" border="0">
      </TD>
    </TR>
		    <TR> 
      <TD nowrap><b><span id=councilspan style='visibility:visible'>Council Tax:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_COUNCIL CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
		<image src="/js/FVS.gif" name="img_COUNCIL" width="15px" height="15px" border="0">
      </TD>
    </TR>
	    <TR> 
      <TD nowrap><b><span id=garagespan style='visibility:visible'>Garage:</span></b></TD>
      <TD nowrap colspan=2> 
        <INPUT TYPE=TEXT NAME=txt_GARAGE CLASS='TEXTBOX100' MAXLENGTH=10 style='visibility:visible'  onChange="calc_form()">
		<image src="/js/FVS.gif" name="img_GARAGE" width="15px" height="15px" border="0">
      </TD>
    </TR>
    <TR> 
      <TD nowrap valign="top"><span id=totalspan style='visibility:visible'><B>Total :</B></span></TD>
      <TD nowrap colspan=2>
        <INPUT TYPE="TEXT" NAME="txt_AMOUNT" CLASS="TEXTBOX100" MAXLENGTH="10" style="visibility:visible" READONLY >
        <image src="/js/FVS.gif" name="img_AMOUNT" width="15px" height="15px" border="0"> 
        <INPUT TYPE=hidden NAME=hid_TENANTNUMBER>
        <INPUT TYPE=hidden NAME=hid_ATTACHID>
        <image src="/js/FVS.gif" name="img_TENANTNUMBER" width="15px" height="15px" border="0"> 
        <input type=button name=Detach value="DETACH" class="RSLButton" style='color:red;visibility:hidden' onclick="DETACHITEM()">
      </TD>
    </TR>
		    <TR> 
      <TD nowrap><b></b></TD>
      <TD nowrap colspan=2>
        
        <input type=BUTTON class='RSLBUTTON' name='btn_POST' value=' Post ' DISABLED onClick="save_form()">
      </TD>
    </TR>

	    <TR> 
      <TD colspan=3 > <DIV ID=ACCOUNT_DIV STYLE='VISIBILITY:HIDDEN'><FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT></DIV>&nbsp; 
      </TD>
      <td> 
                <DIV ID=STATUS_DIV STYLE='VISIBILITY:HIDDEN'><FONT STYLE='FONT-SIZE:13PX'><B>&nbsp;</B></FONT></DIV>
      </td>
    </TR>

  </form>
</TABLE>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_direct width=1px height=1px style='display:BLOCK'></iframe>
</BODY>
</HTML>
