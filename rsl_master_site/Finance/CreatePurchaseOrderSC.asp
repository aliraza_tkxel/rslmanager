<%@  language="VBSCRIPT" codepage="1252" %>

<!--#include virtual="ACCESSCHECK.asp" -->

<%

Call OpenDB()

Call GetCurrentYear()
FY = GetCurrent_YRange

Dim lstApprovedBy
'WHERE CC.COSTCENTREID NOT IN (13,22,23)  'EMAIL FROM M CARNEY 12/5/2016 - Show the Services Cost Centre (13) on the Create PO page
DISTINCT_SQL = "F_COSTCENTRE CC " &_
		"INNER JOIN F_COSTCENTRE_ALLOCATION CCA ON CCA.COSTCENTREID = CC.COSTCENTREID AND CCA.ACTIVE = 1 AND (CCA.FISCALYEAR = " & FY & " OR CCA.FISCALYEAR IS NULL) " &_
		"WHERE CC.COSTCENTREID NOT IN (22,23) " &_ 
		"AND EXISTS (SELECT COSTCENTREID FROM F_HEAD HE INNER JOIN F_HEAD_ALLOCATION HEA ON HEA.HEADID = HE.HEADID AND HEA.ACTIVE = 1 AND HE.COSTCENTREID = CC.COSTCENTREID AND (HEA.FISCALYEAR = " & FY & " OR HEA.FISCALYEAR IS NULL) )"

Scheme_SQL = "P_SCHEME"
Block_SQL = "P_BLOCK " &_
			"INNER JOIN P_SCHEME Sch ON Sch.SCHEMEID = P_BLOCK.SCHEMEID"
Property_SQL = "P__PROPERTY " &_
        "INNER JOIN P_SCHEME Sch ON Sch.SCHEMEID = P__PROPERTY.SCHEMEID"
Call BuildSelectForSC(lstScheme, "sel_SCHEME", Scheme_SQL, "SCHEMEID, SCHEMENAME", "SCHEMENAME", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeScheme()' tabindex=4","Schemes")
Call BuildSelect(lstCostCentres, "sel_COSTCENTRE", DISTINCT_SQL, "DISTINCT CC.COSTCENTREID, CC.DESCRIPTION", "CC.DESCRIPTION", "Please Select...", NULL, NULL, "textbox200", " onchange='Select_OnchangeFund()' tabindex=4")
Call BuildSelect(lstSuppliers, "sel_SUPPLIER", "S_ORGANISATION WHERE ORGACTIVE = 1 ", "ORGID, NAME", "NAME", "Please Select...", NULL, NULL, "textbox200", " tabindex=1")
Call BuildSelectVAT(lstVAT, "sel_VATTYPE", "F_VAT", "VATID, VATNAME", "VATID", NULL, NULL, NULL, "textbox170", " onchange='SetVat()' STYLE='WIDTH:70' tabindex=5")
Call BuildSelect(lstCompany, "sel_COMPANY", "G_Company", "CompanyId, Description", "CompanyId", NULL, NULL, NULL, "textbox200", " onchange='Select_OnchangeCostCentre()'  tabindex=5")

	' IF LOCKDOWN IS ON THEN WE CANT POST IN PREV YEARS OR FUTURE YEARS
	SQL = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'ACCOUNTS_LOCKDOWN' "
	Call OpenRs(rsLockdown, SQL)	
	    LockdownStatus = rsLockdown("DEFAULTVALUE")
	Call CloseRs(rsLockdown)
	
	' GET FISCAL TEAR BOUNDARIES - LATER TO BE MODULARISED
	' THIS CAN SOMETIMES RETURN A RANGE GREATER THAN ONE YEAR. SO BE CAREFUL IN ITS USE OTHER THAN FOR VALIDATION.
	' THERE ARE 2 SP's FOR THIS a)GET_VALIDATION_PERIOD_NODEFAULT and b)GET_VALIDATION_PERIOD THE DEFAULT ONE 
	' HAS A WIDER DATE RANGE THAN THE CURRENT YEAR FOR TIDYING UP PURPOSES
	SQL = "EXEC GET_VALIDATION_PERIOD 8, 1"
	Call OpenRs(rsTAXDATE1, SQL)	
	    YearStartDate1 = FormatDateTime(rsTAXDATE1("YSTART"),1)
	    YearendDate1 = FormatDateTime(rsTAXDATE1("YEND"),1)
	Call CloseRs(rsTAXDATE1)

    SQL = "EXEC GET_VALIDATION_PERIOD 8, 2"
	Call OpenRs(rsTAXDATE2, SQL)	
	    YearStartDate2 = FormatDateTime(rsTAXDATE2("YSTART"),1)
	    YearendDate2 = FormatDateTime(rsTAXDATE2("YEND"),1)
	Call CloseRs(rsTAXDATE2)

    	SQL = "EXEC GET_VALIDATION_PERIOD 8, 3"
	Call OpenRs(rsTAXDATE3, SQL)	
	    YearStartDate3 = FormatDateTime(rsTAXDATE3("YSTART"),1)
	    YearendDate3 = FormatDateTime(rsTAXDATE3("YEND"),1)
	Call CloseRs(rsTAXDATE3)

Call CloseDB()

TIMESTAMP = Replace(Replace(Replace(Now(), "/", ""), ":", ""), " ", "")
 
%>
<html>
<head>
    <title>RSL Manager Finance - Create Purchase Order</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="/css/RSL.css" type="text/css" />
    <style type="text/css">
        body {
            background-color: White;
            margin: 10px 0px 0px 10px;
        }
    </style>
    <meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
    <meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
    <script type="text/javascript" language="JavaScript" src="/js/preloader.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/general.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/menu.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/FormValidation.js"></script>
    <script type="text/javascript" language="JavaScript" src="/js/financial.js"></script>
    <script type="text/javascript" language="JavaScript">
        var FormFields = new Array()
        var propertyArray = new Array()

        function SetChecking(Type) {
            FormFields.length = 0
            if (Type == 1) {
                FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|Y"
                FormFields[1] = "sel_HEAD|Head|SELECT|Y"
                FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|Y"
                FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|Y"
                FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
                FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|Y"
                FormFields[6] = "txt_VAT|VAT|CURRENCY|Y"
                FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|Y"
                FormFields[8] = "sel_VATTYPE|VAT Type|SELECT|Y"
            }
            else if (Type == 2) {
                FormFields[0] = "txt_PONAME|Name|TEXT|Y"
                FormFields[1] = "sel_SUPPLIER|Supplier|SELECT|Y"
                FormFields[2] = "txt_PODATE|Order Date|DATE|Y"
                FormFields[3] = "txt_DELDATE|Expected Date|DATE|N"
                FormFields[4] = "txt_PONOTES|Purchase Notes|TEXT|N"
            }
            else if (Type == 3) {
                FormFields[0] = "sel_COSTCENTRE|Cost Centre|SELECT|N"
                FormFields[1] = "sel_HEAD|Head|SELECT|N"
                FormFields[2] = "sel_EXPENDITURE|Expenditure|SELECT|N"
                FormFields[3] = "txt_ITEMREF|Item Ref|TEXT|N"
                FormFields[4] = "txt_ITEMDESC|Item Description|TEXT|N"
                FormFields[5] = "txt_GROSSCOST|Gross Cost|CURRENCY|N"
                FormFields[6] = "txt_VAT|VAT|CURRENCY|N"
                FormFields[7] = "txt_NETCOST|Net Cost|CURRENCY|N"
                FormFields[8] = "sel_VATTYPE|VAT Type|SELECT|N"
            }
        }

        function PopulateListBox(values, thetext, which) {
            values = values.replace(/\"/g, "");
            thetext = thetext.replace(/\"/g, "");
            values = values.split(";;");
            thetext = thetext.split(";;");

            if (which == -1)
                var selectlist = document.getElementById("sel_COSTCENTRE");
            else {
                if (which == 2)
                    var selectlist = document.getElementById("sel_HEAD");
                else if (which == 3)
                    var selectlist = document.forms.THISFORM.sel_BLOCK;
                else if (which == 4)
                    var selectlist = document.forms.THISFORM.sel_Property;
                else
                    var selectlist = document.getElementById("sel_EXPENDITURE")
            }
            selectlist.length = 0;

            for (i = 0; i < thetext.length; i++) {
                var oOption = document.createElement("OPTION");
                oOption.text = thetext[i];
                oOption.value = values[i];
                selectlist.options[selectlist.length] = oOption;
            }

        }

        function Select_OnchangeCostCentre() {
            document.getElementById("txt_EMLIMITS").value = "0.00";
            document.getElementById("txt_EXPBALANCE").value = "0.00";
            //PopulateListBox("", "Waiting for Data...", -1);
            document.getElementById("IACTION").value = "getcostcentre";
            document.THISFORM.action = "ServerSide/GetCostCentres.asp";
            document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            document.THISFORM.submit();
        }

        function Select_OnchangeFund() {
            document.getElementById("txt_EMLIMITS").value = "0.00";
            document.getElementById("txt_EXPBALANCE").value = "0.00";
            PopulateListBox("", "Waiting for Data...", 2);
            PopulateListBox("", "Please Select a Head...", 1);
            document.getElementById("IACTION").value = "gethead";
            document.THISFORM.action = "ServerSide/GetHeads.asp";
            document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            document.THISFORM.submit();
        }

        function Select_OnchangeScheme() {
            PopulateListBox("", "Waiting for Data...", 3);
            THISFORM.IACTION.value = "getBlock";
            THISFORM.action = "./ServerSide/GetBlocksForSc.asp";
            THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            THISFORM.submit();
        }

        function loadPropertiesData(values, thetext, which) {
            PopulateListBox(values, thetext, which);
            Select_OnchangeBlock();
        }

        function Select_OnchangeBlock() {
            PopulateListBox("", "Waiting for Data...", 4);
            THISFORM.IACTION.value = "getProperty";
            THISFORM.action = "./ServerSide/GetPropertiesForSc.asp";
            THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            THISFORM.submit();
        }

        function Select_OnchangeHead() {
            document.getElementById("txt_EXPBALANCE").value = "0.00";
            document.getElementById("txt_EMLIMITS").value = "0.00";
            PopulateListBox("", "Waiting for Data...", 1);
            document.getElementById("IACTION").value = "change";
            document.THISFORM.action = "ServerSide/GetExpenditures.asp";
            document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            document.THISFORM.submit();
        }

        function SetPurchaseLimits() {
            MatchRow = -1
            if (document.getElementById("AmendButton").style.display == "block") {
                RowData = document.getElementById("UPDATEID").value
                RowArray = RowData.split("||<>||")
                MatchRow = RowArray[0]
            }

            eIndex = document.getElementById("sel_EXPENDITURE").selectedIndex;
            eAmounts = document.getElementById("EXPENDITURE_LEFT_LIST").value;
            eAmounts = eAmounts.split(";;");
            eEMLIMITS = document.getElementById("EMPLOYEE_LIMIT_LIST").value;
            eEMLIMITS = eEMLIMITS.split(";;");

            Ref = document.getElementsByName("ID_ROW")
            //this part removes the expenditure bits
            ExpID = document.getElementById("sel_EXPENDITURE").value
            EXPDeduct = 0
            if (Ref.length) {
                for (i = 0; i < Ref.length; i++) {
                    if (ExpenditureArray[Ref[i].value] == ExpID && Ref[i].value != MatchRow)
                        EXPDeduct += parseFloat(ExpValueArray[Ref[i].value])
                }
            }
            document.getElementById("txt_EXPBALANCE").value = FormatCurrency(parseFloat(eAmounts[eIndex]) - parseFloat(EXPDeduct));
            document.getElementById("txt_EMLIMITS").value = FormatCurrency(eEMLIMITS[eIndex]);

            //this part removes the costcentre part.
            CCID = document.getElementById("sel_COSTCENTRE").value
            CCDeduct = 0
            if (Ref.length) {
                for (i = 0; i < Ref.length; i++) {
                    if (CCArray[Ref[i].value] == CCID && Ref[i].value != MatchRow)
                        CCDeduct += parseFloat(ExpValueArray[Ref[i].value])
                }
            }
            document.getElementById("hid_CCBALANCE").value = FormatCurrency(parseFloat(document.getElementById("hid_TOTALCCLEFT").value) - parseFloat(CCDeduct));
        }

        var RowCounter = 0
        var SelRowCounter = 0
        var ExpenditureArray = new Array()
        var ExpValueArray = new Array()
        var CCArray = new Array()

        function AddSelectionButton() {
            var blockDropDown = document.getElementById("sel_BLOCK");
            var schemeDropDown = document.getElementById("sel_SCHEME");
            var propertyDropDown = document.getElementById("sel_Property");

            AddSelectionRow(blockDropDown, schemeDropDown, propertyDropDown);
        }

        function AddSelectionRow(blockDropDown, schemeDropDown, propertyDropDown) {
            var schemeValue = schemeDropDown.options[schemeDropDown.selectedIndex].value;
            var schemeName = schemeDropDown.options[schemeDropDown.selectedIndex].text;

            var bloxkValue = blockDropDown.options[blockDropDown.selectedIndex].value;
            var blockName = blockDropDown.options[blockDropDown.selectedIndex].text;
            if (bloxkValue == "0") {
                bloxkValue = "null"
                blockName = "-"
            }

            var propertyValue = propertyDropDown.options[propertyDropDown.selectedIndex].value;
            var propertyName = propertyDropDown.options[propertyDropDown.selectedIndex].text;
            if (propertyValue == "0") {
                propertyValue = "null"
                propertyName = "-"
            }

            var selectedRecord = schemeValue + "," + bloxkValue + "," + propertyValue;
            var result = IsRecordAlreadyExist(selectedRecord);
            if (result != "1") {
                alert(result);
                return false;
            }

            propertyArray[SelRowCounter] = selectedRecord;

            selTable = document.getElementById("SelectionTable")

            oRow = selTable.insertRow(-1)
            oRow.id = "TR" + SelRowCounter
            oRow.style.cursor = "pointer"

            DATA = "<input type=\"hidden\" name=\"SID_ROW\" value=\"" + SelRowCounter + "\"><input type=\"hidden\" name=\"iSelDATA" + SelRowCounter + "\" id=\"iSelDATA" + SelRowCounter + "\" value=\"" + schemeValue + "," + bloxkValue + "," + propertyValue + "\">"
            ItemName = schemeName + " : " + blockName + " : " + propertyName;
            AddCell(oRow, ItemName + DATA, "", "", "", 0)
            DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteSelectionRow(" + SelRowCounter + ")\">"
            AddCell(oRow, DelImage, "", "center", "#FFFFFF", 1)
            SelRowCounter++
            return true;
        }

        function DeleteSelectionRow(RowID) {
            oTable = document.getElementById("SelectionTable")
            for (i = 0; i < oTable.rows.length; i++) {
                if (oTable.rows[i].id == "TR" + RowID) {
                    var data = document.getElementById("iSelDATA" + i).value;
                    DeletePropertyRecord(data);
                    oTable.deleteRow(i);
                    break;
                }
            }
            ChangeTableRowId();
        }

        function DeletePropertyRecord(selectedRecord) {
            for (j = 0; j < propertyArray.length; j++) {
                if (propertyArray[j] == selectedRecord) {
                    propertyArray.splice(j, 1);
                }
            }
        }

        function IsRecordAlreadyExist(selectedRecord) {
            if (propertyArray.length > 0) {
                if (propertyArray.length == 1) {
                    if (propertyArray[0] == "-1,-1,-1") {
                        return "All Scheme/Block/Property already added.";
                    }
                }

                if (selectedRecord == "-1,-1,-1") {
                    return "Different record already added. Need to delete that records first.";
                }

                for (i = 0; i < propertyArray.length; i++) {
                    if (propertyArray[i] == selectedRecord) {
                        return "Please select other record. This is already added.";
                    }
                }

                for (i = 0; i < propertyArray.length; i++) {
                    var selectedRecordSplit = selectedRecord.split(",");
                    var propertyArraySplit = propertyArray[i].split(",");
                    if (selectedRecordSplit[0] == propertyArraySplit[0]) {
                        if (selectedRecordSplit[1] == propertyArraySplit[1]) {
                            if (selectedRecordSplit[2] == propertyArraySplit[2]) {
                                return "Please select other record. This is already added.";
                            }
                            else {
                                if (selectedRecordSplit[2] == "-1") {
                                    return "Different record already added of the same Scheme/Block. Need to delete that records first for Adding all Properties."
                                }
                                else if (propertyArraySplit[2] == "-1") {
                                    return "All Properties are already added of the same Scheme/Block. Need to delete that records first for changing Properties."
                                }
                            }
                        }
                        else {
                            if (selectedRecordSplit[1] == "-1") {
                                return "Different record already added for the Block. Need to delete that records first for Adding all Blocks."
                            }
                            else if (propertyArraySplit[1] == "-1") {
                                return "All Blocks are already added of the same Scheme. Need to delete that records first for changing Blocks."
                            }
                        }
                    }
                    else {
                        if (selectedRecordSplit[0] == "-1") {
                            return "Different record already added for the Scheme. 1Need to delete that records first for Adding all Schemes."
                        }
                        else if (propertyArraySplit[0] == "-1") {
                            return "All Schemes are already added. Need to delete that records first for changing Scheme."
                        }
                    }
                }
            }

            return "1";
        }

        function ChangeTableRowId() {
            var oTable = document.getElementById("SelectionTable")
            for (i = 0; i < oTable.rows.length; i++) {
                oTable.rows[i].setAttribute('id', "TR" + i);
                var inputCollection = oTable.rows[i].getElementsByTagName("input");
                inputCollection[0].setAttribute('value', i);
                inputCollection[1].setAttribute('id', "iSelDATA" + i);
                inputCollection[1].setAttribute('name', "iSelDATA" + i);
                inputCollection[1].setAttribute('name', "iSelDATA" + i);
                var imgCollection = oTable.rows[0].getElementsByTagName("img");
                imgCollection[0].setAttribute('onclick', "DeleteSelectionRow(" + i + ")");
            }
            SelRowCounter--;
        }

        function AddRow(insertPos) {
            var ctable = document.getElementById("SelectionTable");
            var img = document.getElementById("img_SelectionTable");
            if (ctable.rows.length < 1) {
                img.style.visibility = 'visible';
                return false;
            }
            img.style.visibility = 'hidden';

            SetChecking(1)
            if (!checkForm()) return false

            GrossCost = document.getElementById("txt_GROSSCOST").value
            EmployeeLimit = document.getElementById("txt_EMLIMITS").value
            ExpenditureLimit = document.getElementById("txt_EXPBALANCE").value
            SaveStatus = 1

            //MAKE SURE PEOPLE DONT ENTER A NEGATIVE FIGURE...
            if (parseFloat(GrossCost) < 0) {
                alert("You have entered an item with a negative value. Please correct this to continue.")
                return false;
            }

            //check the costcentre will not go over...
            //note: have to remove the total of any other entries aswell.
            CCLeft = document.getElementById("hid_CCBALANCE").value
            if ((parseFloat(CCLeft) < parseFloat(GrossCost))) {
                alert("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the Cost Centre budget remaining (�" + FormatCurrencyComma(CCLeft) + ") for the selected item.\nTherefore this item cannot be entered onto the system.");
                return false;
            }

            //check the expenditure will not go over...
            ExpLeft = document.getElementById("txt_EXPBALANCE").value
            if ((parseFloat(ExpLeft) < parseFloat(GrossCost))) {
                answer = confirm("The item cost (�" + FormatCurrencyComma(GrossCost) + ") is more than the expenditure budget remaining (�" + FormatCurrencyComma(ExpLeft) + ") for the selected item.\nDo you still wish to continue?.\n\nClick 'OK' to continue.\nClick 'CANCEL' to cancel.");
                if (!answer) return false
            }

            //check the employee limit						
            if (parseFloat(EmployeeLimit) < parseFloat(GrossCost)) {
                SaveStatus = 0
                result = confirm("The item cost is greater than your employee limit (�" + FormatCurrencyComma(EmployeeLimit) + ") for the selected item budget.\nIf you continue then the item will be placed in a queue to be authorised\nby a user who has appropriate limits.\n\nClick 'OK' to continue.\nClick 'CANCEL' to abort.")
                if (!result) return false
            }

            //this part deletes the previous row for amends
            //amends come in with a variable of zero (0) for insertPos
            if (insertPos != -1) {
                sTable = document.getElementById("ItemTable")
                RowData = document.getElementById("UPDATEID").value
                RowArray = RowData.split("||<>||")
                MatchRow = RowArray[0]
                for (i = 0; i < sTable.rows.length; i++) {
                    if (sTable.rows[i].id == "TR" + MatchRow) {
                        sTable.deleteRow(i)
                        insertPos = i
                        SetTotals(-RowArray[1], -RowArray[2], -RowArray[3])
                        break;
                    }
                }
            }

            RowCounter++
            ItemRef = document.getElementById("txt_ITEMREF").value
            ItemDesc = document.getElementById("txt_ITEMDESC").value
            ExpenditureID = document.getElementById("sel_EXPENDITURE").value
            ExpenditureName = document.getElementById("sel_EXPENDITURE").options[document.getElementById("sel_EXPENDITURE").selectedIndex].text
            NetCost = document.getElementById("txt_NETCOST").value
            VAT = document.getElementById("txt_VAT").value
            VatTypeID = document.getElementById("sel_VATTYPE").value
            VatTypeName = document.getElementById("sel_VATTYPE").options[document.getElementById("sel_VATTYPE").selectedIndex].text
            VatTypeCode = VatTypeName.substring(0, 1).toUpperCase()
            CompanyId = document.getElementById("sel_COMPANY").value

            oTable = document.getElementById("ItemTable")
            for (i = 0; i < oTable.rows.length; i++) {
                if (oTable.rows[i].id == "EMPTYLINE") {
                    oTable.deleteRow(i)
                    break;
                }
            }

            oRow = oTable.insertRow(insertPos)
            oRow.id = "TR" + RowCounter
            oRow.onclick = AmendRow
            oRow.style.cursor = "pointer"

            var selTab = document.getElementById("SelectionTable");

            var selectedSBP = "";

            for (i = 0; i < selTab.rows.length; i++) {
                var itemIndex = 'iSelDATA' + i;
                SData = document.getElementById(itemIndex).value;
                selectedSBP = selectedSBP + SData
                if (i + 1 != selTab.rows.length) {
                    selectedSBP = selectedSBP + ";"
                }
            }

            DATA = "<input type=\"hidden\" name=\"ID_ROW\" value=\"" + RowCounter + "\"><input type=\"hidden\" name=\"iDATA" + RowCounter + "\" id=\"iDATA" + RowCounter + "\" value=\"" + ItemRef + "||<>||" + ItemDesc + "||<>||" + VatTypeID + "||<>||" + NetCost + "||<>||" + VAT + "||<>||" + GrossCost + "||<>||" + ExpenditureID + "||<>||" + CompanyId + "||<>||" + SaveStatus + "||<>||" + selectedSBP + "\">"
            //THIS PART ADDS THE EXPENDITURE ID AND VALUE TO ARRAYS, INCASE ANOTHER REPAIR OF THE SAME TYPE IS SELECTED. 
            //SO A TRUE COMPARISON TO THE EXPENDITURE LEFT CAN BE MADE.
            ExpenditureArray[RowCounter] = document.getElementById("sel_EXPENDITURE").value
            ExpValueArray[RowCounter] = GrossCost
            CCArray[RowCounter] = document.getElementById("sel_COSTCENTRE").value
            RefNo = "0" + RowCounter
            if (RowCounter <= 9) {
                RefNo = "0" + RefNo
            }

            //selTab.setAttribute("id", "iDATASel" + RowCounter);
            //var destination = document.getElementById("tempTable");
            var selTabCopy = selTab.cloneNode(true);
            selTabCopy.setAttribute('id', "iDATASel" + RowCounter);
            //destination.parentNode.replaceChild(copy, destination);

            AddCell(oRow, RefNo, "", "", "", 0)
            AddCell(oRow, ItemRef + DATA, ItemDesc, "", "", 1)
            AddCell(oRow, ExpenditureName, "", "", "", 2)
            AddCell(oRow, selTabCopy.outerHTML, "", "", "", 3)
            AddCell(oRow, VatTypeCode, VatTypeName + " Rate", "center", "", 4)
            AddCell(oRow, FormatCurrencyComma(NetCost), "", "right", "", 5)
            AddCell(oRow, FormatCurrencyComma(VAT), "", "right", "", 6)
            AddCell(oRow, FormatCurrencyComma(GrossCost), "", "right", "", 7)
            DelImage = "<img title='Clicking here will remove this item from the list.' style='cursor:hand' src='/js/img/FVW.gif' width=15 height=15 onclick=\"DeleteRow(" + RowCounter + "," + NetCost + "," + VAT + "," + GrossCost + ")\">"
            AddCell(oRow, DelImage, "", "center", "#FFFFFF", 8)

            SetTotals(NetCost, VAT, GrossCost)
            ResetData()
            return true;
        }

        function ResetData() {
            SetChecking(3)
            checkForm()
            SelRowCounter = 0;
            document.getElementById("SelectionTable").innerHTML = "";
            document.getElementById("txt_EMLIMITS").value = "0.00";
            document.getElementById("txt_EXPBALANCE").value = "0.00";
            PopulateListBox("", "Please Select a Cost Centre...", 2);
            PopulateListBox("", "Please Select a Head...", 1);
            ResetArray = new Array("txt_ITEMREF", "txt_ITEMDESC", "txt_NETCOST", "txt_GROSSCOST", "sel_COSTCENTRE")
            for (i = 0; i < ResetArray.length; i++)
                document.getElementById(ResetArray[i]).value = ""
            document.getElementById("sel_VATTYPE").selectedIndex = 0;
            document.getElementById("txt_VAT").value = "0.00";
            document.getElementById("AmendButton").style.display = "none"
            document.getElementById("AddButton").style.display = "block"
            propertyArray = [];
        }

        function AmendRow(evt) {
            //event.cancelBubble = true

            if (!evt)
                window.event.cancelBubble = true;
            else
                evt.stopPropagation();

            Ref = this.id
            RowNumber = Ref.substring(2, Ref.length)

            StoredSelectedSBP = document.getElementById("iDATASel" + RowNumber)
            var storedSelectedSBPCopy = document.getElementById("SelectionTable")

            var storedSelectedSBPCopy = StoredSelectedSBP.cloneNode(true);
            storedSelectedSBPCopy.setAttribute('id', "SelectionTable");

            document.getElementById("SelectionTable").outerHTML = storedSelectedSBPCopy.outerHTML
            SelRowCounter = document.getElementById("SelectionTable").rows.length

            StoredData = document.getElementById("iDATA" + RowNumber).value
            StoredArray = StoredData.split("||<>||")

            ReturnArray = new Array("txt_ITEMREF", "txt_ITEMDESC", "sel_VATTYPE", "txt_NETCOST", "txt_VAT", "txt_GROSSCOST")
            for (i = 0; i < ReturnArray.length; i++)
                document.getElementById(ReturnArray[i]).value = StoredArray[i]
            document.getElementById("UPDATEID").value = RowNumber + "||<>||" + StoredArray[3] + "||<>||" + StoredArray[4] + "||<>||" + StoredArray[5]
            //alert (document.getElementById("UPDATEID").value)
            document.getElementById("AddButton").style.display = "none"
            document.getElementById("AmendButton").style.display = "block"

            document.THISFORM.IACTION.value = "LOADPREVIOUS";
            document.THISFORM.action = "ServerSide/GetExpenditures.asp?EXPID=" + StoredArray[6];
            document.THISFORM.target = "PURCHASEFRAME<%=TIMESTAMP%>";
            document.THISFORM.submit();
        }

        function SetTotals(iNE, iVA, iGC) {
            totalNetCost = parseFloat(document.getElementById("hiNC").value) + parseFloat(iNE)
            totalVAT = parseFloat(document.getElementById("hiVA").value) + parseFloat(iVA)
            totalGrossCost = parseFloat(document.getElementById("hiGC").value) + parseFloat(iGC)

            document.getElementById("hiNC").value = FormatCurrency(totalNetCost)
            document.getElementById("hiVA").value = FormatCurrency(totalVAT)
            document.getElementById("hiGC").value = FormatCurrency(totalGrossCost)

            document.getElementById("iNC").innerHTML = FormatCurrencyComma(totalNetCost)
            document.getElementById("iVA").innerHTML = FormatCurrencyComma(totalVAT)
            document.getElementById("iGC").innerHTML = FormatCurrencyComma(totalGrossCost)
        }

        function AddCell(iRow, iData, iTitle, iAlign, iColor, iposition) {
            oCell = iRow.insertCell(iposition)
            oCell.innerHTML = iData
            if (iTitle != "") oCell.title = iTitle
            if (iAlign != "") oCell.style.textAlign = iAlign
            if (iColor != "") oCell.style.backgroundColor = iColor
        }

        function DeleteRow(RowID, NE, VA, GR) {
            oTable = document.getElementById("ItemTable")
            for (i = 0; i < oTable.rows.length; i++) {
                if (oTable.rows[i].id == "TR" + RowID) {
                    oTable.deleteRow(i)
                    SetTotals(-NE, -VA, -GR)
                    break;
                }
            }
            if (oTable.rows.length == 1) {
                oRow = oTable.insertRow()
                oRow.id = "EMPTYLINE"
                oCell = oRow.insertCell()
                oCell.colSpan = 7
                oCell.innerHTML = "Please enter an item from above"
                oCell.style.textAlign = "center"
            }
            ResetData()
        }

        function real_date(str_date) {
            var datearray;
            var months = new Array("nowt", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
            str_date = new String(str_date);
            datearray = str_date.split("/");
            return new Date(datearray[0] + " " + months[parseInt(datearray[1])] + " " + datearray[2]);
        }

        function SavePurchaseOrder() {

            this.disabled = true;
            if (document.getElementById("sel_COMPANY").value == "1") {
                var YStart = new Date("<%=YearStartDate1%>")
                var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
                var YEnd = new Date("<%=YearendDate1%>")
                var YStartPlusTime = new Date('<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate1),1) %>')
                var YEndPlusTime = new Date('<%=FormatDateTime(DateAdd("m", 1, YearStartDate1),1) %>')

            }

            if (document.getElementById("sel_COMPANY").value == "2") {
                var YStart = new Date("<%=YearStartDate2%>")
                var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
                var YEnd = new Date("<%=YearendDate2%>")
                var YStartPlusTime = new Date('<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate2),1) %>')
                var YEndPlusTime = new Date('<%=FormatDateTime(DateAdd("m", 1, YearStartDate2),1) %>')

            }

            if (document.getElementById("sel_COMPANY").value == "3") {
                var YStart = new Date("<%=YearStartDate3%>")
                var TodayDate = new Date("<%=FormatDateTime(Now(),1)%>")
                var YEnd = new Date("<%=YearendDate3%>")
                var YStartPlusTime = new Date('<%=FormatDateTime(DateAdd("yyyy", -1, YearStartDate3),1) %>')
                var YEndPlusTime = new Date('<%=FormatDateTime(DateAdd("m", 1, YearStartDate3),1) %>')
            }


            if (real_date(THISFORM.txt_PODATE.value) < YStart || real_date(THISFORM.txt_PODATE.value) > YEnd) {
                //alert("Please enter an Order Date between '<%=YearStartDate%>' and '<%=YearendDate%>'");
                alert("The month is now locked down and entries can only be made for the current month");

                this.disabled = false;
                return false;
            }

            ResetData()
            SetChecking(2)
            if (!checkForm()) {
                this.disabled = false;
                return false;
            }

            if (parseFloat(document.getElementById("hiGC").value) <= 0) {
                alert("Please enter some items before creating a purchase order.")
                this.disabled = false;
                return false;
            }
            var elementName = document.getElementById("sel_SUPPLIER");

            document.getElementById("txt_SupplierName").value = elementName.options[elementName.selectedIndex].text;

            document.THISFORM.target = "";
            document.THISFORM.method = "POST";
            document.THISFORM.action = "ServerSide/CreatePurchaseOrderSC_svr.asp";
            document.THISFORM.submit();

            document.getElementById("MainSave").disabled = true;
        }
    </script>
    <style type="TEXT/CSS">
        #ItemTable tr td:nth-child(4) img {
            display: none;
        }

        #ItemTable tr td:last-child + img {
            display: block !important;
        }
    </style>
</head>
<body onload="initSwipeMenu(3);preloadImages();" onunload="macGo()">
    <!--#include virtual="Includes/Tops/BodyTop.asp" -->
    <iframe src="/secureframe.asp" name="PURCHASEFRAME<%=TIMESTAMP%>" style="display: none"></iframe>
    <form name="THISFORM" method="post" action="">
        <table style="border: 1px solid black" cellspacing="0" cellpadding="3" width="768">
            <tr bgcolor="#4682b4" style="color: white">
                <td colspan="6">
                    <b>PURCHASE ORDER INFORMATION</b>
                </td>
            </tr>
            <tr>
                <td>Name :
                </td>
                <td>
                    <input name="txt_PONAME" id="txt_PONAME" type="text" class="textbox200" size="50" maxlength="50"
                        tabindex="1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_PONAME" id="img_PONAME" alt="" />
                </td>
                <td rowspan="3" valign="top" nowrap="nowrap">&nbsp;Purchase Notes :
                </td>
                <td rowspan="5">
                    <textarea name="txt_PONOTES" id="txt_PONOTES" cols="55" rows="9" class="textbox200" tabindex="2" style="width: 305; overflow: hidden; border: 1px solid #133E71"></textarea>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_PONOTES" id="img_PONOTES" alt="" />
                </td>
            </tr>
            <tr>
                <td>Supplier :
                </td>
                <td>
                    <%=lstSuppliers%>
                    <input name="txt_SupplierName" id="txt_SupplierName" type="hidden" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_SUPPLIER" id="img_SUPPLIER" alt="" />
                </td>
            </tr>
            <tr>
                <td>Order Date :
                </td>
                <td>
                    <input name="txt_PODATE" id="txt_PODATE" type="text" class="textbox200" tabindex="1" size="50" maxlength="10"
                        value="<%=FormatDateTime(Date,2)%>" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_PODATE" id="img_PODATE" alt="" />
                </td>
            </tr>
            <tr>
                <td>Exp Date :
                </td>
                <td>
                    <input name="txt_DELDATE" id="txt_DELDATE" type="text" class="textbox200" tabindex="2" maxlength="10"
                        size="50" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_DELDATE" id="img_DELDATE" alt="" />
                </td>


            </tr>
            <tr>
                <td>&nbsp;Company:
                </td>
                <td>
                    <%=lstCompany%>
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <br />
        <table style="border: 1px solid black" cellspacing="0" cellpadding="3" width="765">

            <thead>
                <tr>
                    <td align="left" width="50" nowrap="nowrap"></td>
                    <td align="left" width="110" nowrap="nowrap"></td>
                    <td align="left" width="25" nowrap="nowrap"></td>
                    <td align="left" width="80" nowrap="nowrap"></td>
                    <td align="left" width="130" nowrap="nowrap"></td>
                    <td align="left" width="25" nowrap="nowrap"></td>
                    <td align="left" width="70" nowrap="nowrap"></td>
                    <td align="left" width="80" nowrap="nowrap"></td>
                    <td align="left" width="25" nowrap="nowrap"></td>
                </tr>
            </thead>

            <tr bgcolor="#4682b4" style='color: white'>
                <td colspan="9">
                    <b>NEW ITEM</b>
                </td>
            </tr>
            <tr>
                <td>Item :
                </td>
                <td>
                    <input name="txt_ITEMREF" id="txt_ITEMREF" type="text" class="textbox170" maxlength="50" tabindex="3" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMREF" id="img_ITEMREF" alt="" />
                </td>
                <td>Scheme :
                </td>
                <td>
                    <%=lstScheme%>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_SCHEME" id="img_SCHEME">
                </td>
            </tr>

            <tr>
                <td rowspan="8" valign="top">Notes :
                </td>
                <td rowspan="8">
                    <textarea tabindex="3" name="txt_ITEMDESC" id="txt_ITEMDESC" rows="12" cols="10" class="textbox170" style="height: 255px; overflow: hidden; border: 1px solid #133E71"></textarea>
                </td>
                <td rowspan="8">
                    <img src="/js/FVS.gif" width="15" height="15" name="img_ITEMDESC" id="img_ITEMDESC" alt="" />
                </td>
                <td></td>
                <td>
                    <select id="sel_BLOCK" name="sel_BLOCK" class="TEXTBOX200" onchange="Select_OnchangeBlock()"
                        tabindex="4">
                        <option value="-1">All Blocks</option>
                    </select>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_BLOCK" id="img_BLOCK">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <select id="sel_Property" name="sel_Property" class="TEXTBOX200" tabindex="4">
                        <option value="-1">All Properties</option>
                    </select>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_Property" id="img_Property">
                </td>
                <td>
                    <input type="button" name="AddSelection" id="AddSelection" value=" Add Selection " title="AddSelection" class="RSLButton" onclick="AddSelectionButton()"
                        tabindex="6" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div style="height: 100px; border-style: solid; border-width: 0.5px; width: 200px; overflow-y: auto;">
                        <table style="behavior: url(/Includes/Tables/tablehl.htc)"
                            cellpadding="3" width="185" id="SelectionTable" slcolor='' hlcolor="silver">
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td>
                    <img style="visibility: hidden" src="Images/FVER.gif"
                        title="This field is required and must be a valid monetary value."
                        width="15" height="15" name="img_SelectionTable" id="img_SelectionTable">
                </td>
            </tr>
            <tr>
                <td>&nbsp;Cost Centre :
                </td>
                <td>
                    <%=lstCostCentres%>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_COSTCENTRE" id="img_COSTCENTRE" alt="" />
                </td>
                <td>&nbsp;Net Cost :
                </td>
                <td>
                    <input style="text-align: right" name="txt_NETCOST" id="txt_NETCOST" type="text" class="textbox" maxlength="20"
                        size="11" onblur="TotalBoth();ResetVAT()" onfocus="alignLeft()" tabindex="5" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_NETCOST" id="img_NETCOST" alt="" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;Head :
                </td>
                <td>
                    <select id="sel_HEAD" name="sel_HEAD" class="textbox200" onchange="Select_OnchangeHead()"
                        tabindex="4">
                        <option value="">Please Select a Cost Centre...</option>
                    </select>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_HEAD" id="img_HEAD" alt="" />
                </td>

                <td>&nbsp;Vat Type :
                </td>
                <td>
                    <%=lstVAT%>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_VATTYPE" id="img_VATTYPE" alt="" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;Expenditure :
                </td>
                <td>
                    <select id="sel_EXPENDITURE" name="sel_EXPENDITURE" class="textbox200" onchange="SetPurchaseLimits()"
                        tabindex="4">
                        <option value="">Please select a Head...</option>
                    </select>
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_EXPENDITURE" id="img_EXPENDITURE" alt="" />
                </td>
                <td>&nbsp;VAT :
                </td>
                <td>
                    <input style="text-align: right" name="txt_VAT" id="txt_VAT" type="text" class="textbox" maxlength="20"
                        size="11" onblur="TotalBoth()" onfocus="alignLeft()" value="0.00" tabindex="5" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_VAT" id="img_VAT" alt="" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;Exp Balance :
                </td>
                <td>
                    <input type="text" name="txt_EXPBALANCE" id="txt_EXPBALANCE" class="textbox200" tabindex="-1" readonly="readonly" />
                </td>
                <td></td>
                <td>&nbsp;Total :
                </td>
                <td>
                    <input style="text-align: right" name="txt_GROSSCOST" id="txt_GROSSCOST" type="text" class="textbox"
                        maxlength="20" size="11" readonly="readonly" tabindex="-1" />
                </td>
                <td>
                    <img src="/js/FVS.gif" width="15" height="15" name="img_GROSSCOST" id="img_GROSSCOST" alt="" />
                </td>
            </tr>
            <tr>
                <!--<td></td>
                <td></td>-->
                <td>&nbsp;Spend Limit :
                </td>
                <td>
                    <input type="text" name="txt_EMLIMITS" id="txt_EMLIMITS" class="textbox200" readonly="readonly" tabindex="-1" />
                </td>
                <td align="right" colspan="3">
                    <input type="hidden" name="IACTION" id="IACTION" />
                    <input type="hidden" name="UPDATEID" id="UPDATEID" />
                    <input type="hidden" name="EXPENDITURE_LEFT_LIST" id="EXPENDITURE_LEFT_LIST" />
                    <input type="hidden" name="EMPLOYEE_LIMIT_LIST" id="EMPLOYEE_LIMIT_LIST" />
                    <input type="hidden" name="hid_TOTALCCLEFT" id="hid_TOTALCCLEFT" />
                    <input type="hidden" name="hid_CCBALANCE" id="hid_CCBALANCE" />
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td nowrap="nowrap">
                                <input type="button" name="ResetButton" id="ResetButton" value=" RESET " title="RESET" class="RSLButton" onclick="ResetData()"
                                    tabindex="6" />&nbsp;
                            </td>
                            <td>
                                <input type="button" name="AddButton" id="AddButton" value=" ADD " title="ADD" class="RSLButton" onclick="AddRow(-1)"
                                    tabindex="6" />
                            </td>
                            <td>
                                <input type="button" name="AmendButton" id="AmendButton" value=" AMEND " title="AMEND" class="RSLButton" onclick="AddRow(0)"
                                    style="display: none" tabindex="6" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table style="border: 1px solid black; behavior: url(/Includes/Tables/tablehl.htc)"
            cellspacing="0" cellpadding="3" width="768" id="ItemTable" slcolor='' hlcolor="silver">
            <thead>
                <tr bgcolor="#4682b4" align="right" style='color: white'>
                    <td height="20" align="left" width="20" nowrap="nowrap">
                        <b>Ref#:</b>
                    </td>
                    <td height="20" align="left" width="115" nowrap="nowrap">
                        <b>Item Name:</b>
                    </td>
                    <td width="124" nowrap="nowrap" align="left">
                        <b>Expenditure:</b>
                    </td>
                    <td width="145" nowrap="nowrap" align="left">
                        <b>Scheme/Block/Property:</b>
                    </td>
                    <td width="40" nowrap="nowrap">
                        <b>Code:</b>
                    </td>
                    <td width="80" nowrap="nowrap">
                        <b>Net (�):</b>
                    </td>
                    <td width="75" nowrap="nowrap">
                        <b>VAT (�):</b>
                    </td>
                    <td width="80" nowrap="nowrap">
                        <b>Gross (�):</b>
                    </td>
                    <td width="29" nowrap="nowrap">&nbsp;
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr id="EMPTYLINE">
                    <td colspan="8" align="center">Please enter an item from above
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="border: 1px solid black; border-top: none" cellspacing="0" cellpadding="2"
            width="768">
            <tr bgcolor="#4682b4" align="right">
                <td height="20" width="490" nowrap="nowrap" style="border: none; color: white">
                    <b>TOTAL : &nbsp;</b>
                </td>
                <td width="80" nowrap="nowrap" bgcolor="white">

                    <input type="hidden" id="hiNC" value="0" /><div id="iNC" style="font-weight: bold">
                        0.00
                    </div>

                </td>
                <td width="75" nowrap="nowrap" bgcolor="white">

                    <input type="hidden" id="hiVA" value="0" /><div id="iVA" style="font-weight: bold">
                        0.00
                    </div>

                </td>
                <td width="80" nowrap="nowrap" bgcolor="white">

                    <input type="hidden" id="hiGC" name="hiGC" value="0" /><div id="iGC" style="font-weight: bold">
                        0.00
                    </div>

                </td>
                <td width="29" nowrap="nowrap" bgcolor="white">&nbsp;
                </td>
            </tr>
        </table>
        <br />
        <table cellspacing="0" cellpadding="2" width="768">
            <tr align="right">
                <td width="754" align="right" nowrap="nowrap">&nbsp;<input type="button" value=" CREATE ORDER " name="MainSave" id="MainSave" class="RSLButton"
                    onclick="SavePurchaseOrder()" />
                </td>
            </tr>
        </table>
        <table style="behavior: url(/Includes/Tables/tablehl.htc)"
            cellpadding="3" width="200" id="tempTable" slcolor='' hlcolor="silver">
            <tbody>
            </tbody>
        </table>
    </form>
    <!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>
