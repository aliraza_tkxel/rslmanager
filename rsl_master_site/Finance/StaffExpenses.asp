<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<% Response.Expires = 0 %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE virtual="/Includes/Functions/TableBuilder.asp" -->
<%
	CONST CONST_PAGESIZE = 20
		
	Dim PageName			 'WILL STORE THE NAME OF THE PAGE TO TRAVERSE
	Dim DefaultOrderBy		 'the default order clause to be used, must be a number.
	Dim EmptyText			 'what to display if no records are returned
	Dim SQLCODE			     'the SQL CODE

	Dim TableTitles    (4)	 'USED BY CODE
	Dim DatabaseFields (4)	 'USED BY CODE
	Dim ColumnWidths   (4)	 'USED BY CODE
	Dim TDSTUFF        (4)	 'USED BY CODE
	Dim TDPrepared	   (4)	 'USED BY CODE
	Dim ColData        (4)	 'Syntax	[column title] | [database field] | [display length(px)] 
	Dim SortASC        (4)	 'All Items must be included, if a sort is not reuired for the field then put ""
	Dim SortDESC       (4)	 'All Array sizes must match	
	Dim TDFunc		   (4)

	dim Accid 
	
	THE_TABLE_HIGH_LIGHT_COLOUR = "steelblue"
	ColData(0)  = "Date|PAYDATE|90"
	SortASC(0) 	= "PAYDATE ASC"
	SortDESC(0) = "PAYDATE DESC"
	TDSTUFF(0)  = ""
	TDFunc(0) = ""
	
	ColData(1)  = "Item|ITEMNAME|200"
	SortASC(1) 	= "ITEMNAME ASC"
	SortDESC(1) = "ITEMNAME DESC"
	TDSTUFF(1)  = " "" title=""""Exp: "" & rsSet(""DESCRIPTION"") & """""" "" "
	TDFunc(1) = ""

	ColData(2)  = "Cheque|CHQNUMBER|90"
	SortASC(2) 	= "CHQNUMBER ASC"
	SortDESC(2) = "CHQNUMBER DESC"	
	TDSTUFF(2)  = " "" ALIGN=RIGHT"" "
	TDFunc(2) = ""		

	ColData(3)  = "Employee|FULLNAME|180"
	SortASC(3) 	= "FULLNAME ASC"
	SortDESC(3) = "FULLNAME DESC"	
	TDSTUFF(3)  = ""
	TDFunc(3) = ""
	
	ColData(4)  = "Amount|GROSSCOST|80"
	SortASC(4) 	= "PI.GROSSCOST ASC"
	SortDESC(4) = "PI.GROSSCOST DESC"	
	TDSTUFF(4)  = " "" align='right' "" "
	TDFunc(4) = "FormatCurrency(|)"
	
	
	PageName = "StaffExpenses.asp"
	EmptyText = "No Relevant Staff Expenses found in the system!!!"
	DefaultOrderBy = SortDesc(0)
	
	'RowClickColumn = " ""ONCLICK=""""load_me("" & rsSet(""id"") &  "")"""" """
	RowClickColumn = "" 

	Dim orderBy
		OrderByMatched = 0
		orderBy = DefaultOrderBy
		Call SetSort()
	
	SQLCODE = "SELECT ITEMNAME, PAYDATE, EX.DESCRIPTION, CHQ.CHQNUMBER, E.FIRSTNAME + ' ' + E.LASTNAME AS FULLNAME, PI.GROSSCOST " &_
				"FROM F_PURCHASEITEM PI " &_ 
				"LEFT JOIN F_EXPENDITURE EX ON EX.EXPENDITUREID = PI.EXPENDITUREID " &_
				"INNER JOIN F_PURCHASEITEM_TO_CHQ PCHQ ON PCHQ.ORDERITEMID = PI.ORDERITEMID " &_
				"INNER JOIN F_PURCHASEITEM_CHQ CHQ ON CHQ.CHQID = PCHQ.CHQID " &_
				"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = CHQ.EMP_ID " &_
			"WHERE PI.PITYPE = 4 AND PI.ACTIVE = 1 " &_
			"ORDER BY "& orderBy

	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then
		intpage = 1	
	Else
		if (IsNumeric(Request.QueryString("page"))) then
			intpage = CInt(Request.QueryString("page"))
		else
			intpage = 1			
		end if
	End If
	
	Call Create_Table()

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Staff Expenses</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array()
	
	function SubmitPage(){
		location.href = "<%=PageName & "?CC_Sort=" & orderBy & "&sel_Supplier="%>" + thisForm.sel_SUPPLIER.value  ;
		}

	function load_me(txnid){
		window.showModelessDialog("Popups/MIS.asp?txnid=" + txnid  + "&Random=" + new Date(), "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
		}
// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(0);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = thisForm method=post>

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		
      <TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD BGCOLOR=#133E71><IMG SRC="../Finance/images/spacer.gif" WIDTH=750 HEIGHT=1></TD>
	</TR>
</TABLE>
<%=TheFullTable%>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>