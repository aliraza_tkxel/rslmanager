	Function bhDays()
		Dim bhSplit, cnt, testBHDate, high, testDate, bhCount, holStart, holEnd,tmpStartDate,totdate,i
		
		holStart = DateValue(RSLFORM.txt_STARTDATE.value)
		if RSLFORM.radMulti.checked = true then
			holEnd = DateValue(RSLFORM.txt_RETURNDATE.value)
		else
			holEnd = DateValue(RSLFORM.txt_STARTDATE.value)
		end if
		
		bhSplit=Split(RSLFORM.dates.value,",")
			
		cnt=1
		bhCount=0
		i=0
		if RSLFORM.radMulti.checked = true then
			tmpStartDate=holStart
			totdate=DateDiff("d",holStart,holEnd)+2
			do while cnt < totdate
			
				do while i <= Ubound(bhSplit)
				    if (DateDiff("d",FormatDateTime(bhSplit(i)),FormatDateTime(tmpStartDate)))=0 then
					    bhCount= bhCount +1
				    end if
				    i=i+1
			     loop 
			
			     i=0
			     tmpStartDate=DateAdd("d",1,tmpStartDate)
			     cnt=cnt+1
			loop
			
		else
			
			if inStr(RSLFORM.dates.value,holStart) then
				bhCount= -1
			end if
		end if
		bhDays= bhCount
	End Function
	
    Function bhSickDays()
		Dim bhSplit, cnt, testBHDate, high, testDate, bhCount, holStart, holEnd,tmpStartDate,totdate,i
		
		holStart = DateValue(RSLFORM.txt_STARTDATE.value)
        If RSLFORM.txt_RETURNDATE.value = "" then
            holEnd = Date
        else
           holEnd = DateValue(RSLFORM.txt_RETURNDATE.value)
        end if
		bhSplit=Split(RSLFORM.dates.value,",")
			
		cnt=1
		bhCount=0
		i=0
		
		tmpStartDate=holStart
		totdate=DateDiff("d",holStart,holEnd)+1
			do while cnt <= totdate
			
				do while i <= Ubound(bhSplit)
				    if (DateDiff("d",FormatDateTime(bhSplit(i)),FormatDateTime(tmpStartDate)))=0 then
					    bhCount= bhCount +1
				    end if
				    i=i+1
			     loop 
			
			     i=0
			     tmpStartDate=DateAdd("d",1,tmpStartDate)
			     cnt=cnt+1
			loop
		bhSickDays= bhCount
	End Function
	
	
	// calculates number of work days between two dates (PP)
	Function workDays()     
   ' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
   
   	
		getType()
		BegDate = DateValue(RSLFORM.txt_STARTDATE.value)
		EndDate = DateValue(RSLFORM.txt_RETURNDATE.value)
		bhCountdays = bhDays()
		non_work_days = RSLFORM.hid_non_work_days.value
		
		// if multi day is picked
		If RSLFORM.radMulti.checked = true Then
			WholeWeeks = DateDiff("w", BegDate, EndDate)
			DateCnt = DateAdd("ww", WholeWeeks, BegDate)
			EndDays = 0
			Do While DateCnt <= EndDate
				If weekday(DateCnt) <> "1" And _
					weekday(DateCnt) <> "7" Then
					EndDays = EndDays + 1
				End If
				DateCnt = DateAdd("d", 1, DateCnt)
			Loop
			
			WorkDays = WholeWeeks * 5 + EndDays
			// remove half days from total
			If RSLFORM.radStartAM.checked = true OR RSLFORM.radStartPM.checked = true then
				WorkDays = Workdays - 0.5
			End If
			If RSLFORM.radEndAM.checked = true OR RSLFORM.radEndPM.checked = true then
				WorkDays = Workdays - 0.5
			End If
			If WorkDays < 0.5 Then
				RSLFORM.txt_DURATION.style.color = "red"
				RSLFORM.txt_DURATION.value = "Invalid Date Range"
				RSLFORM.btn_submit.disabled = "true"
			Else
				RSLFORM.txt_DURATION.style.color = "black"
				//WorkDays = WorkDays - bhCountdays
				WorkDays = WorkDays - bhCountdays - non_work_days
				RSLFORM.txt_DURATION.value = WorkDays
				RSLFORM.btn_submit.disabled = "false"
			End If
		Else
			// if single day is chosen
			If RSLFORM.radStartAM.checked = true OR RSLFORM.radStartPM.checked = true then
				WorkDays = 0.5
			Else 
				Workdays = 1
			End If
			EndDate = BegDate
			
			
			if (weekday(BegDate)=1)  or (weekday(BegDate)=7) then
				
				RSLFORM.txt_DURATION.style.color = "red"
				RSLFORM.txt_DURATION.value = "Weekend"
				RSLFORM.btn_submit.disabled = "true"
				//RSLFORM.txt_DURATION.value=0
				exit function
			end if
			
			if bhCountdays >= 0 then
				RSLFORM.txt_DURATION.style.color = "black"
				//WorkDays = WorkDays - bhCountdays
				WorkDays = WorkDays - bhCountdays - non_work_days
				RSLFORM.txt_DURATION.value = WorkDays
				RSLFORM.btn_submit.disabled = "false"
			else
		    	RSLFORM.txt_DURATION.style.color = "red"
				RSLFORM.txt_DURATION.value = "Bank Holiday"
				RSLFORM.btn_submit.disabled = "true"
			end if
		End If
		
	End Function

   Function sicknessDays()     
   ' GET THE TOTAL NO. OF DAYS AN EMPLOYEE WORKS FROM FUNCTION
   
   RSLFORM.btn_submit.disabled = "false"
      
   	    Dim holType,totdays
		if RSLFORM.radStartFull.checked = true Then
				holType = "F"
			elseif RSLFORM.radStartAM.checked = true Then
				holType = "M"
			else holType = "A"
		End if
        RSLFORM.holtype.value = holType
		BegDate = DateValue(RSLFORM.txt_STARTDATE.value)
        totdays=DateDiff("d",BegDate,Date)
		bhCountdays = bhSickDays()
		non_work_days = RSLFORM.hid_non_work_days.value

        //--------------------------
        if RSLFORM.hid_pagename.value = "pSickLeave" then
            if RSLFORM.txt_RETURNDATE.value = "" then
                EndDate = Date
            else
                EndDate = DateValue(RSLFORM.txt_RETURNDATE.value)
            end if
       else
           EndDate = Date  
       end if     
            WholeWeeks = DateDiff("w", BegDate, EndDate)
			DateCnt = DateAdd("ww", WholeWeeks, BegDate)
			EndDays = 0
			Do While DateCnt <= EndDate
				If weekday(DateCnt) <> "1" And _
					weekday(DateCnt) <> "7" Then
					EndDays = EndDays + 1
				End If
				DateCnt = DateAdd("d", 1, DateCnt)
			Loop
			
			sicknessDays = WholeWeeks * 5 + EndDays
        //--------------------------
		
    if totdays < 0 then
      RSLFORM.txt_DURATION.style.color = "red"
	   RSLFORM.txt_DURATION.value = "Invalid Date"
	   RSLFORM.btn_submit.disabled = "true"
       exit function
    end if
	
	// if single day is chosen
    if totdays = 0 then
   	   If RSLFORM.radStartAM.checked = true OR RSLFORM.radStartPM.checked = true then
		    sicknessDays = 0.5
       Else 
		    sicknessDays = 1
       End If
    EndDate = BegDate
    else
       If RSLFORM.radStartAM.checked = true OR RSLFORM.radStartPM.checked = true then
		    sicknessDays = sicknessDays - 0.5
	   End If
       if RSLFORM.hid_pagename.value = "pSickLeave" then
            If RSLFORM.radEndAM.checked = true OR RSLFORM.radEndPM.checked = true then
		        sicknessDays = sicknessDays - 0.5
	        End If
       end if
    end if
	
	
	if (weekday(BegDate)=1)  or (weekday(BegDate)=7) then
				
		RSLFORM.txt_DURATION.style.color = "red"
		RSLFORM.txt_DURATION.value = "Weekend"
		RSLFORM.btn_submit.disabled = "true"
		exit function
	end if
    RSLFORM.hid_pagename.value=RSLFORM.hid_pagename.value	

	if bhCountdays >= 0 then
		RSLFORM.txt_DURATION.style.color = "black"
		sicknessDays = sicknessDays - bhCountdays - non_work_days
		RSLFORM.txt_DURATION.value = sicknessDays
		if sicknessDays > 0 then
           RSLFORM.btn_submit.disabled = "false"
        elseif sicknessDays=0 then
            RSLFORM.txt_DURATION.style.color = "red"
		    //RSLFORM.txt_DURATION.value = "Bank Holiday"
            RSLFORM.txt_DURATION.value = non_work_days
		    RSLFORM.btn_submit.disabled = "true"
        else
            RSLFORM.txt_DURATION.style.color = "red"
		    RSLFORM.txt_DURATION.value = "Invalid Date"
		    RSLFORM.btn_submit.disabled = "true"
        end if
	end if
     //RSLFORM.txt_DURATION.value = totdays
	
	End Function

    	
	Function getType()
	
		Dim holType
		holType = ""
		
		// if multi day
		if RSLFORM.radMulti.checked = true Then
			if RSLFORM.radStartFull.checked = true Then
				holType = "F"
			ElseIf RSLFORM.radStartAM.checked = true Then
				holType = "M"
			else holType = "A"
			End if 
			if RSLFORM.radEndFull.checked = true Then
				holType = holType & "-F"
			elseif RSLFORM.radEndAM.checked = true Then
				holType = holType & "-M"
			else holType = holType & "-A"
			End if
		else
			if RSLFORM.radStartFull.checked = true Then
				holType = "F"
			elseif RSLFORM.radStartAM.checked = true Then
				holType = "M"
			else holType = "A"
			End if
		End if
		RSLFORM.holtype.value = holType
	
	End Function

    Function getSicknessType()
	
		Dim holType
		holType = ""
		
		
		if RSLFORM.radStartFull.checked = true Then
			holType = "F"
		elseif RSLFORM.radStartAM.checked = true Then
			holType = "M"
		else holType = "A"
		End if

        if RSLFORM.hid_pagename.value = "pSickLeave" then
            if RSLFORM.radEndFull.checked = true Then
				holType = holType & "-F"
			elseif RSLFORM.radEndAM.checked = true Then
				holType = holType & "-M"
			else holType = holType & "-A"
			End if
        end if

		
		RSLFORM.holtype.value = holType
	
	End Function
      


