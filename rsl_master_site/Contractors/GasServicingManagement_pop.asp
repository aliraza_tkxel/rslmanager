<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true%>
<!--#include virtual="AccessCheck_Popup.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<%
    Call OpenDB()
    Call BuildSelect(lstOrganisation, "sel_ORGANISATION", "S_ORGANISATION WHERE ORGID=" & SESSION("ORGID"), "ORGID, NAME", "NAME", "Please Select", SESSION("ORGID"),null , "textbox200", " STYLE='margin-left:2px;'")	    
    Call BuildSelect(lstPatch, "sel_PATCH", "E_PATCH", "PATCHID, LOCATION", "LOCATION", "All", null, NULL, "textbox200", " STYLE='margin-left:29px;' TABINDEX=11 OnChange=""GetSchemes();"" ")	    
    Call BuildSelect(lstDevelopemnt, "sel_SCHEME", "PDR_DEVELOPMENT", "DEVELOPMENTID, DEVELOPMENTNAME", "DEVELOPMENTNAME", "All", null,null, "textbox200", "style='margin-left:17px;'" )	
    Call BuildSelect(lstAppointmentStatus, "sel_APPOINTMENTSTAGE", "C_GASSERVICINGAPPOINTMENT WHERE APPOINTMENTID IN(1,2)", "APPOINTMENTID, DESCRIPTION", "APPOINTMENTID", "Please Select", null,null, "textbox200", "style='margin-left:29px;' disabled=disabled" )	
    Call CloseDB()	
%>
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager --> Gas Servicing </title>
<link rel="stylesheet" type="text/css" href="../css/RSLManager_greenBrand.css" />

<style  type="text/css" media="all">
#main{width:100%;}
    /*CSS for filter*/
    #filter {background-color: #FFFFFF;width:25%;margin-left:5px;padding-top:10px;float:left;margin-right:3px;}
    .heading {border:1px #133e71 solid;width:160px;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    #filter_head{width:291px;}
    #filter_headline{width:131px;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .filter_headlinediv{width:131px;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    #filter_body{width:291px;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-bottom:18px;margin-bottom:10px;}
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;font-size:13px;}
    .linkstylesmall{text-decoration: underline;color:blue;cursor:hand;font-size:10px;padding:0 5 0 5;}
    .boxstyle{border:1px solid #133e71;width:12px;height:1px;margin:5 5 0 10;}
    .dottedline{border-bottom:1px dotted #133e71; padding:0 0 0 0; margin:0 9 0 9;}
    .line{border-bottom:1px solid #133e71; padding:0 0 0 0; margin:0 9 0 2;}
    .dottedbox{border:1px dotted #133e71;}
    .dottedbox img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .elementlabel{font-size:10px;margin-left:10px;font-weight:bold;color:#133e71}
    .elementlabeldisabled{font-size:10px;margin-left:10px;font-weight:bold;color:gray}
    .margintop{margin:10 0 0 0;}
    /*CSS for content*/
    .content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;float:left;}
    .content_head{width:99%;}
    .heading_content {border:1px #133e71 solid;width:24%;height:28px;margin:0 0 0 0;padding:5 0 0 6;display:inline;font-size:12px; font-weight:bold; color:#133e71;} 
    .content_headline{width:76%;height:20px;margin:0 0 0 0;padding:0 0 0 0;display:inline;}
    .content_headlinediv{width:100%;height:1px;margin:0 0 0 0;padding:0 0 0 0;}
    .content_body{width:99%;border:solid #133e71; border-width: 0px 1px 1px 1px;padding-left:10px;float:left;height:100%;}
    .heading2{font-weight:bold;color:#133e71;}
    .heading3{font-weight:bold;color:#133e71;font-size:12px;}
    .fonttwelevewithcolor{font-size:12;color:#133e71;} 


</style>


</head>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
  
 //var FormFields = new Array();
//	   FormFields[0] = "txt_appointment_date|Appointment Date|DATE|Y"
	 
  function chkObject(theVal)
  {
    if (document.getElementById(theVal) != null) 
            return true;
        else 
            return false;

  }
  
  function ManageAppointment(propertyid,appointmentdate,repairhistoryid,appointmentid,Letterid)
  {
   window.open("popups/pManageAppointment.asp?propertyid="+propertyid+"&appointmentdate="+appointmentdate+"&repairhistoryid="+repairhistoryid+"&appointmentid="+appointmentid+"&Letterid="+Letterid, "_blank","width=680,height=300,left=100,top=200") ;
  }  
  function IssueCP12(propertyid,appointmentdate,repairhistoryid)
  {
   window.open("popups/pIssueCP12.asp?propertyid="+propertyid+"&appointmentdate="+appointmentdate+"&repairhistoryid="+repairhistoryid, "_blank","width=480,height=320,left=100,top=200") ;
  }  
  function GetSchemes()
  {
   
	RSLFORM.sel_SCHEME.disabled = true;
	RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetSchemes.asp"
	RSLFORM.submit()
  }
  
  function ResetFilter()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;
    var parent,child

    switch (selectedoption) 
    {
        case "0":
                if( chkObject("mon_menu_data")==true)
                {
                    parent=document.getElementById("content"); 
                    child=document.getElementById("mon_menu_data"); 
                    parent.removeChild(child);
                } 
                document.getElementById("content_head").style.display="block";
	            document.getElementById("content_body").style.display="block";
	            RSLFORM.sel_APPOINTMENTSTAGE.value="";
	            RSLFORM.sel_APPOINTMENTSTAGE.disabled=true;
                RSLFORM.sel_RENEWAL.disabled=true;
                RSLFORM.txt_street.value="";
                RSLFORM.txt_street.disabled=true;
                RSLFORM.txt_postcode.value="";
                RSLFORM.txt_postcode.disabled=true;
                RSLFORM.cust_name.disabled=true;
                RSLFORM.cust_name.value="";
                RSLFORM.cust_no.disabled=true;
                RSLFORM.cust_no.value="";
                RSLFORM.txt_appointmentdate.disabled=true;
                RSLFORM.txt_appointmentdate.value="";
                document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                document.getElementById("lbl_street").className ="elementlabeldisabled";
                document.getElementById("lbl_postcode").className ="elementlabeldisabled"; 
                document.getElementById("lbl_appointmentstage").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_name").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_no").className ="elementlabeldisabled";
                document.getElementById("lbl_appointmentdate").className ="elementlabeldisabled";
                break;
        case "1":
                if( chkObject("mon_menu_data")==true)
                    {
                        parent=document.getElementById("content"); 
                        child=document.getElementById("mon_menu_data"); 
                        parent.removeChild(child);
                    }
                 document.getElementById("content_head").style.display="block";
	             document.getElementById("content_body").style.display="block"; 
	             RSLFORM.sel_APPOINTMENTSTAGE.disabled=true;
	             RSLFORM.sel_RENEWAL.disabled=false;
                 RSLFORM.txt_street.disabled=false;
                 RSLFORM.txt_postcode.disabled=false;
                 RSLFORM.cust_name.disabled=true;
                 RSLFORM.cust_name.value="";
                 RSLFORM.cust_no.disabled=true;
                 RSLFORM.cust_no.value="";
                 RSLFORM.txt_appointmentdate.disabled=true;
                 RSLFORM.txt_appointmentdate.value="";
                 document.getElementById("lbl_renwal").className ="elementlabel";
                 document.getElementById("lbl_street").className ="elementlabel";
                 document.getElementById("lbl_postcode").className ="elementlabel";
                 document.getElementById("lbl_appointmentstage").className ="elementlabeldisabled";
                 document.getElementById("lbl_cust_name").className ="elementlabeldisabled";
                 document.getElementById("lbl_cust_no").className ="elementlabeldisabled";
                 document.getElementById("lbl_appointmentdate").className ="elementlabeldisabled";
                 break;
        case "2": 
               if( chkObject("mon_menu_data")==true)
                    {
                        parent=document.getElementById("content"); 
                        child=document.getElementById("mon_menu_data"); 
                        parent.removeChild(child);
                    }
                 document.getElementById("content_head").style.display="block";
	             document.getElementById("content_body").style.display="block";    
	             RSLFORM.sel_RENEWAL.value=""; 
	             RSLFORM.sel_RENEWAL.disabled=true;
	             RSLFORM.txt_street.disabled=false;
                 RSLFORM.txt_postcode.disabled=false;
                 RSLFORM.cust_name.disabled=false;
                 RSLFORM.cust_no.disabled=false;
                 RSLFORM.sel_APPOINTMENTSTAGE.disabled=false;
                 RSLFORM.txt_appointmentdate.disabled=false;
                 document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                 document.getElementById("lbl_street").className ="elementlabel";
                 document.getElementById("lbl_postcode").className ="elementlabel";
                 document.getElementById("lbl_appointmentstage").className ="elementlabel";
                 document.getElementById("lbl_cust_name").className ="elementlabel";
                 document.getElementById("lbl_cust_no").className ="elementlabel";
                 document.getElementById("lbl_appointmentdate").className ="elementlabel";
                break;
         case "3": 
                if( chkObject("mon_menu_data")==true)
                    {
                        parent=document.getElementById("content"); 
                        child=document.getElementById("mon_menu_data"); 
                        parent.removeChild(child);
                    }
                 document.getElementById("content_head").style.display="block";
	             document.getElementById("content_body").style.display="block";        
	             RSLFORM.sel_RENEWAL.value=""; 
	             RSLFORM.sel_RENEWAL.disabled=true;
	             RSLFORM.txt_street.disabled=false;
                 RSLFORM.txt_postcode.disabled=false;
                 RSLFORM.cust_name.disabled=false;
                 RSLFORM.cust_no.disabled=false;
                 RSLFORM.sel_APPOINTMENTSTAGE.disabled=false;
                 RSLFORM.txt_appointmentdate.disabled=false;
                 document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                 document.getElementById("lbl_street").className ="elementlabel";
                 document.getElementById("lbl_postcode").className ="elementlabel";
                 document.getElementById("lbl_appointmentstage").className ="elementlabel";
                 document.getElementById("lbl_cust_name").className ="elementlabel";
                 document.getElementById("lbl_cust_no").className ="elementlabel";
                 document.getElementById("lbl_appointmentdate").className ="elementlabel";
                break;        
        case "4":
                if( chkObject("mon_menu_data")==true)
                {
                    parent=document.getElementById("content"); 
                    child=document.getElementById("mon_menu_data"); 
                    parent.removeChild(child);
                } 
                document.getElementById("content_head").style.display="none";
	            document.getElementById("content_body").style.display="none";
	            RSLFORM.sel_APPOINTMENTSTAGE.value="";
	            RSLFORM.sel_APPOINTMENTSTAGE.disabled=true;
                RSLFORM.sel_RENEWAL.disabled=true;
                RSLFORM.txt_street.value="";
                RSLFORM.txt_street.disabled=true;
                RSLFORM.txt_postcode.value="";
                RSLFORM.txt_postcode.disabled=true;
                RSLFORM.cust_name.disabled=true;
                RSLFORM.cust_name.value="";
                RSLFORM.cust_no.disabled=true;
                RSLFORM.cust_no.value="";
                RSLFORM.txt_appointmentdate.disabled=true;
                RSLFORM.txt_appointmentdate.value="";
                document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                document.getElementById("lbl_street").className ="elementlabeldisabled";
                document.getElementById("lbl_postcode").className ="elementlabeldisabled"; 
                document.getElementById("lbl_appointmentstage").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_name").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_no").className ="elementlabeldisabled";
                document.getElementById("lbl_appointmentdate").className ="elementlabeldisabled";
                break;
         case "5":
                if( chkObject("mon_menu_data")==true)
                {
                    parent=document.getElementById("content"); 
                    child=document.getElementById("mon_menu_data"); 
                    parent.removeChild(child);
                } 
                document.getElementById("content_head").style.display="block";
	            document.getElementById("content_body").style.display="block";
	            RSLFORM.sel_APPOINTMENTSTAGE.value="";
	            RSLFORM.sel_APPOINTMENTSTAGE.disabled=true;
                RSLFORM.sel_RENEWAL.disabled=true;
                RSLFORM.txt_street.value="";
                RSLFORM.txt_street.disabled=true;
                RSLFORM.txt_postcode.value="";
                RSLFORM.txt_postcode.disabled=true;
                RSLFORM.cust_name.disabled=true;
                RSLFORM.cust_name.value="";
                RSLFORM.cust_no.disabled=true;
                RSLFORM.cust_no.value="";
                RSLFORM.txt_appointmentdate.disabled=true;
                RSLFORM.txt_appointmentdate.value="";
                document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                document.getElementById("lbl_street").className ="elementlabeldisabled";
                document.getElementById("lbl_postcode").className ="elementlabeldisabled"; 
                document.getElementById("lbl_appointmentstage").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_name").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_no").className ="elementlabeldisabled";
                document.getElementById("lbl_appointmentdate").className ="elementlabeldisabled";        
                break;
        default:
                if( chkObject("mon_menu_data")==true)
                    {
                        parent=document.getElementById("content"); 
                        child=document.getElementById("mon_menu_data"); 
                        parent.removeChild(child);
                    }
                document.getElementById("content_head").style.display="block";
	            document.getElementById("content_body").style.display="block"; 
	            RSLFORM.sel_APPOINTMENTSTAGE.value="";
	            RSLFORM.sel_APPOINTMENTSTAGE.disabled=true;
	            RSLFORM.sel_RENEWAL.disabled=true;
                RSLFORM.sel_reportlist.value="";
                RSLFORM.txt_street.value="";
                RSLFORM.txt_street.disabled=true;
                RSLFORM.txt_postcode.value="";
                RSLFORM.txt_postcode.disabled=true;
                RSLFORM.cust_name.disabled=true;
                RSLFORM.cust_name.value="";
                RSLFORM.cust_no.disabled=true;
                RSLFORM.cust_no.value="";
                RSLFORM.txt_appointmentdate.disabled=true;
                RSLFORM.txt_appointmentdate.value="";
                document.getElementById("lbl_renwal").className ="elementlabeldisabled";
                document.getElementById("lbl_street").className ="elementlabeldisabled";
                document.getElementById("lbl_postcode").className ="elementlabeldisabled";
                document.getElementById("lbl_appointmentstage").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_name").className ="elementlabeldisabled";
                document.getElementById("lbl_cust_no").className ="elementlabeldisabled";
                document.getElementById("lbl_appointmentdate").className ="elementlabeldisabled";
    }
    return;
  }
  
  function GetSummary()
  {
    
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    
    RSLFORM.sel_reportlist.value="";
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetSummary.asp"
	RSLFORM.submit()
	return;
  }

  function GetContracts()
  {

    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    
    //ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetContracts.asp"
	RSLFORM.submit()
	return;
  }
  
  function GetAppointmentBooking()
  {
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetAppointmentBooking.asp"
	RSLFORM.submit()
	return;
  }
  
  function GetInvoicing()
  {
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetInvoicing.asp"
	RSLFORM.submit()
	return;
  }
  
  function GetManageAppointment()
  {
    
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetManageAppointment.asp"
	RSLFORM.submit()
	return;
  }
  
  function GetMonitoring()
  {
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetMonitoring.asp"
	RSLFORM.submit()
	return;
  }
  function GetIssueCP12()
  {
    if(RSLFORM.sel_ORGANISATION.value=="") 
    {
        alert("Please select the organisation");
        return;
    }
    ResetFilter();
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetIssueCP12.asp"
	RSLFORM.submit()
	return;
  }
   function GetMonitoringDetail()
  {
    RSLFORM.target = "ServerFrame"
	RSLFORM.action = "ServerSide/GetMonitoringDetail.asp"
	RSLFORM.submit()
	return;
  }
  function FilterRecords()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;

    switch (selectedoption) 
    {
        case "0":
             GetContracts();
             break 
        case "1":
             GetAppointmentBooking();  
             break;
        case "2": 
            GetManageAppointment();
            break;
        case "3":    
             GetIssueCP12();
             break;
        case "4":    
             GetMonitoring();
             break;
        case "5":    
             GetInvoicing();
             break;          
        default: GetSummary();
    }
  }
  
  function swap()
  {
    var selectedoption=RSLFORM.sel_reportlist.value;
    switch (selectedoption) 
    {
        
        case "0":
              GetContracts();
              break 
        case "1": 
             GetAppointmentBooking();  
             break;
        case "2": 
             GetManageAppointment();
             break;
        case "3":    
             GetIssueCP12();
             break;
        case "4":    
             RSLFORM.hid_mon_menu.value="1";
             GetMonitoring();
             break;
        case "5":    
             GetInvoicing();
             break;     
        default: GetSummary();
    }
    return true;
  }
  
  function selectall(checkboxname)
  {
    var cbcount,btnvalue,chkstatus
    
    cbcount=document.getElementsByName(checkboxname).length;
    
    
    if(RSLFORM.btn_selectall.value=="Select All")
    {
        btnvalue="De-select All"
        chkstatus=true
    }
    else
    {
        btnvalue="Select All"
        chkstatus=false
    }
     
    for (var x = 0; x < cbcount; x++)
    {
        //if(document.getElementsByName(checkboxname)[x].checked==true)
         //{   
         
            //document.getElementsByName(checkboxname)[x].checked = false;
            //RSLFORM.btn_selectall.value="Select All";
            
         //}
         //else 
         //{
            //document.getElementsByName(checkboxname)[x].checked = true;
            //RSLFORM.btn_selectall.value="De-select All"
         //}   
         
         document.getElementsByName(checkboxname)[x].checked = chkstatus;
         RSLFORM.btn_selectall.value=btnvalue;
            
    }
    if(checkboxname=="cb_invoice") 
    {
        cal_batchvalue();
    }
    
  }
  
  
	
 

	
function chkBSelected()
{
		var chkB_count = 0
		iRef = document.getElementsByName("cb_appointment");
		for (j=0; j<iRef.length; j++)
			if (iRef[j].checked == true) 
			{ 
				chkB_count = chkB_count + 1
			}
			else 
			{
				chkB_count = chkB_count + 0
			}

		if (chkB_count == 0)
		{
		return false;
		}
	return true;
}
  
function save_appointment()
{

var cbselected
var AppointmentTime 

	cbselected = ""
	AppointmentTime="";	
	AppointmentTime=RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;
	RSLFORM.hid_appointmenttime.value=AppointmentTime;
	
	//if (!checkForm()) return false;
	
	if(document.getElementById("txt_appointment_date").value=='') 
	{
		alert("Please select the appointment date");
		return false;
	}
	
	
	if (!chkBSelected())
	{
		alert("Please select customer for appointment")
		return false;
	}
	else
	{
		var chkB_count = 0
			iRef = document.getElementsByName("cb_appointment");
		
		for (j=0; j<iRef.length; j++)
			if (iRef[j].checked == true) 
			{ 				
				chkB_count = chkB_count + 1
				cbselected=cbselected + iRef[j].value + "|"
			}  
			  
    	RSLFORM.hid_select_repairhistoryid.value=cbselected;
		
		//if ()
	
		if(confirm(chkB_count+" Appointments will be saved!"))
		{		
		   RSLFORM.target = "ServerFrame"
		   RSLFORM.action = "ServerSide/SaveAppointmentBooking.asp";
		   RSLFORM.submit();
		}    	
	
	}

}
  
  
  function ChangeMenu()
  {
    RSLFORM.hid_mon_menu.value=RSLFORM.sel_Monitoring.value;
    GetMonitoring();
    return;
  }
  function GetDetailMonitoring(Status)
  {

    RSLFORM.mon_status.value=Status;
    GetMonitoringDetail();
  }  
  
  function accept_contract(scopeid)
  {
     var cbcount,cbselected,count_check 
     
     cbselected="";
     
     count_check=0;
     
    cbcount=RSLFORM.cb_contract.length;
 
    for (var x = 0; x < cbcount; x++)
    {
        if(RSLFORM.cb_contract[x].checked==true)
         {
            if(cbselected!="")
                cbselected=cbselected + "|" + RSLFORM.cb_contract[x].value;
            else    
                cbselected=RSLFORM.cb_contract[x].value;
            count_check=count_check+1    
         }
    }
    
    RSLFORM.hid_select_scopeid.value=cbselected;
    
    
    if(cbselected!="")
    {
        if(confirm(count_check+" Contract will be saved!"))
        {
            RSLFORM.target = "ServerFrame"
            RSLFORM.action = "ServerSide/SaveAcceptedContract.asp";
            RSLFORM.submit();
        }    
        return;
	 }   
	 else
	 {
	    alert("Please select contract for acceptance.");
	    return;
	 }   
  }
 function cal_batchvalue()
 {
    var cbcount,cbselected,count_check 
     
     cbselected="";
     
     count_check=0;
     
     cbcount=RSLFORM.cb_invoice.length;
 
    for (var x = 0; x < cbcount; x++)
    {
        if(RSLFORM.cb_invoice[x].checked==true)
         {
            if(cbselected!="")
                cbselected=cbselected + "|" + RSLFORM.cb_invoice[x].value;
            else    
                cbselected=RSLFORM.cb_invoice[x].value;
            count_check=count_check+1    
         }
    }
    
    RSLFORM.hid_select_repairhistoryid.value=cbselected;
    RSLFORM.target = "ServerFrame"
    RSLFORM.action = "ServerSide/GetBatchValue.asp";
    RSLFORM.submit();

 }
 function save_batch_invoice()
  {
     var cbcount,cbselected,count_check 
     
     cbselected="";
     
     count_check=0;
     
    cbcount=RSLFORM.cb_invoice.length;
 
    for (var x = 0; x < cbcount; x++)
    {
        if(RSLFORM.cb_invoice[x].checked==true)
         {
            if(RSLFORM.cb_invoice[x].value!="0,0")
            {
                if(cbselected!="")
                    cbselected=cbselected + "|" + RSLFORM.cb_invoice[x].value;
                else    
                    cbselected=RSLFORM.cb_invoice[x].value;
                count_check=count_check+1    
            }   
         }
    }
    
    RSLFORM.hid_select_repairhistoryid.value=cbselected;
    
    if(cbselected!="")
    {
        if(confirm(count_check+" Property appliance will be Invoiced!"))
        {
            RSLFORM.target = "ServerFrame"
            RSLFORM.action = "ServerSide/SaveInvoicing.asp";
            RSLFORM.submit();
        }    
        return;
	 }   
	 else
	 {
	    alert("Please select property appliance for invoicing.");
	    return;
	 }   
  }
  </script>

<body onLoad="GetSummary();">

<form name = "RSLFORM" method="post" action="">

<div id="maincontent" >
	<div id="topbar">
		<div id="topmenu">
			<div id="system_logo"></div>
			<div style="float:right;margin-right:4px;margin-top:10px;"><a class="lnklogout" href="#">Help</a> <a class="lnklogout"  href="#">Whiteboard</a> <a class="lnklogout" href="#" onClick="window.close()">Log Out</a></div>
			
			<div id="PageHeading" style="clear:right;float:right;margin-right:4px;margin-top:40px;">Gas Servicing</div>
			
		</div>
		
	</div>
	<div id="stripe"></div> 
	<br />



   <div id="main">
    <div  id="filter">
        <div id="filter_head">
            <div id="filter_heading" class="heading">Gas Servicing</div>
            <div id="filter_headline">
                <div class="filter_headlinediv" style="border-bottom:1px solid #133e71;"></div>
                <div class="filter_headlinediv" style="border-right:1px solid #133e71;"></div>
            </div>
         </div>
         <div id="filter_body">
            <p style="margin-top:5px;">
                <span class="boxstyle"></span>
                <span class="linkstyle" onClick="GetSummary();">Summary</span>
            </p>

            <p style="margin-bottom:10px;">
                <span class="elementlabel">Action:</span>
                <select id="sel_reportlist" name="sel_reportlist" class="textbox200" style="width:150" onChange="swap();" >
                    <option value="">Please Select</option> 
                     <option value="0">Contracts</option>
                     <option value="1">1st Appointment</option>  
                     <option value="2">Manage Appointment</option> 
                     <option value="3">Issue LGSR</option>                        
                     <option value="4">Monitoring</option>   
                     <option value="5">Invoicing</option> 
                </select>
            </p>
            <p class="dottedline" style="margin-bottom:10px;">&nbsp;</p>
            <p style="font-size:14px;font-weight:bold;margin-left:10px;margin-top:0px;margin-bottom:0px;">Search</p>
            <p style="margin-bottom:0px;" class="margintop">
                 <span class="elementlabel">Contractor:</span>
                 <%=lstOrganisation%>
            </p>
            <p  class="margintop">
                <label class="elementlabeldisabled" for="sel_RENEWAL" id="lbl_renwal"> 
                    <span style="display:block;">LGSR</span> <span style="margin-left:10px;">Renewal:</span>
                </label>
                <select id="sel_RENEWAL" name="sel_RENEWAL" class="textbox200" style="width:200;margin-left:15px;" disabled="disabled">
                     <option value="">Select</option>  
                     <option value="0-6">0-6 days</option>  
                     <option value="7-12">7-12 days</option> 
                     <option value="1">1 month</option>                        
                     <option value="1+">1 month+</option>   
                 </select>
            </p>
            <p class="margintop">
                <label class="elementlabeldisabled" for="sel_APPOINTMENTSTAGE" id="lbl_appointmentstage">Stage:</label>
                <select id="sel_APPOINTMENTSTAGE" name="sel_APPOINTMENTSTAGE" class="textbox200" style="width:200;margin-left:29px;" disabled="disabled">
                     <option value="">Select</option>  
                     <option value="1">1st &nbsp;Appointment</option>  
                     <option value="2">2nd Appointment</option> 
                     <option value="3">3rd &nbsp;Appointment</option>                        
                     <option value="4">4th &nbsp;Appointment</option>
                     <option value="5">5th &nbsp;Appointment</option>
                     <option value="6">6th &nbsp;Appointment</option>
                     <option value="0">No-Entry</option>   
                 </select>
            </p>
            <p class="margintop">
                <span class="elementlabel">Patch:</span>
                <%=lstPatch %>
            </p>
            <p class="margintop">
                <span class="elementlabel">Scheme:</span>
                <%=lstDevelopemnt %>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="txt_street" id="lbl_street">Address:</label>  
              <input type="text" value="" id="txt_street" name="txt_street" class="textbox200" style="margin-left:15px;" disabled="disabled"/>
			  <img alt="" src="/js/FVS.gif" name="img_street" width="15px" height="15px" border="0"/>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="txt_postcode" id="lbl_postcode">PostCode:</label>  
              <input type="text" value="" id="txt_postcode" name="txt_postcode" class="textbox200" style="margin-left:8px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="cust_name" id="lbl_cust_name">Customer:</label>  
              <input type="text" value="" id="cust_name" name="cust_name" class="textbox200" style="margin-left:7px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="cust_no" id="lbl_cust_no">Cust No:</label>  
              <input type="text" value="" id="cust_no" name="cust_no" class="textbox200" style="margin-left:19px;" disabled="disabled"/>
            </p>
            <p class="margintop">
              <label class="elementlabeldisabled" for="txt_appointmentdate" id="lbl_appointmentdate">Appointment date:</label>  
              <input id="txt_appointmentdate" name="txt_appointmentdate" type="text" value="DD/MM/YYYY" class="textbox200" style="margin-left:38px;width:120;" disabled="disabled" />
		    </p>
            <p class="margintop">
               <input type="button" value="Search" class="RSLButton" onClick="FilterRecords();" style="margin-left:227px;"/>
            </p>
            <p class="dottedline">&nbsp;</p>
         </div>
     </div>
     <div class="content" id="content">
        <div id="content_head" class="content_head">
            <div id="content_heading" class="heading_content"></div>
            <div id="content_headline" class="content_headline">
                <div class="content_headlinediv" style="border-bottom:1px solid #133e71;"></div>
                <div class="content_headlinediv" style="border-right:1px solid #133e71;"></div>
            </div>
         </div>
         <div id="content_body" class="content_body">
         </div>
     </div>
 </div>
 <input type="hidden" id="hid_select_repairhistoryid" name="hid_select_repairhistoryid" value="" />
 <input type="hidden" id="hid_select_scopeid" name="hid_select_scopeid" value="" />
 <input type="hidden" id="hid_mon_menu" name="hid_mon_menu" value="1"/>
 <input type="hidden" id="mon_status" name="mon_status" value="0"/>
 <input type="hidden" id="hid_appointmenttime" name="hid_appointmenttime" value="" />
 </div>
<br /> 
<img alt="appointment date" src="/js/FVS.gif" name="img_appointment_date" id="img_appointment_date" width="15px" height="15px" border="0"/>
<div id="strapline">RSLmanager is a <a href="http://www.reidmark.com" target="_blank">Reidmark</a> ebusiness system � All rights reserved 2007</div>

</form>
<iframe src="/secureframe.asp" name="ServerFrame" style="display:none" height="200" width="200"></iframe>
</body>
</html>