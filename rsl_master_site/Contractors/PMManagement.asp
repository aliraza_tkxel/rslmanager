<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include virtual="Connections/db_connection.asp" -->
<html>
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>RSL Manager --> Planned Maintenance </title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<style  type="text/css" media="all">
    .linkstyle{text-decoration: underline;color:blue;cursor:hand;font-size:9px;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
  
  function GetPM()
  {
   window.open("/RSLPlannedMaintenance/PMContractorsPortal.aspx", "display","scrollbars=1,resizable=1,left=100,top=200") ;
  }  

</script>
<body bgcolor="#FFFFFF" onload="preloadImages();GetPM();" onunload="macGo()" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<p>Please use the pop up for Planned Maintenance</p>
<p>If a new window doesn't appear after 10 seconds please <span class="linkstyle" onclick="GetPM();">click here</span> </p>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
</body>
</html>