<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

    Function GetFolderPath(FolderID)

		SQL = "SELECT FOLDERNAME, PARENTFOLDER FROM DOC_FOLDER WHERE FOLDERID = " & FolderID

		Call OpenRs(rsFolder, SQL)
		FolderName = "/" & rsFolder("FOLDERNAME") 
		FolderID = rsFolder("PARENTFOLDER")
		Call CloseRs(rsFolder)
		
		While not PathStart = "END"
		
			SQL = "SELECT PARENTFOLDER,FOLDERNAME FROM DOC_FOLDER WHERE FOLDERID = " & FolderID
			
			Call OpenRs(rsFolderPath, SQL)
			
			IF NOT rsFolderPath.EOF THEN
				FolderName = "/" & rsFolderPath("FOLDERNAME") & FolderName
				IF rsFolderPath("PARENTFOLDER") <> -1 THEN
					FolderID = rsFolderPath("PARENTFOLDER")
				ELSE
					PathStart =  "END"
				END if
			ELSE
				PathStart = "END"
			END IF
			
			Call CloseRs(rsFolderPath)
			
		Wend
		GetFolderPath =  FolderName
	End Function

	Response.ContentType = "text/xml"
	
	Set objDom = Server.CreateObject("Microsoft.XMLDOM")
	objDom.preserveWhiteSpace = True

	dim FLDID
	FLDID = Request("FLDID")
	
	SQL = "SELECT DISTINCT F.* FROM DOC_FOLDER F INNER JOIN DOC_DOCUMENT D ON (D.FOLDERID = F.FOLDERID AND DOCFOR LIKE '%," & Session("TeamCode") & ",%') WHERE PARENTFOLDER = " & FLDID & " ORDER BY FOLDERNAME"
		
	Call OpenDB()
	Call OpenRs(rsHE, SQL)

	'Create your root element and append it to the XML document.
	 Set objRoot = objDom.createElement("tree")
	 objDom.appendChild objRoot
		
		While NOT rsHE.EOF
		   Set objRow = objDom.createElement("tree")
	
		   '*** Append the text attribute to the field node ***
		   Set objcolName = objDom.createAttribute("text")
		   objcolName.Text = rsHE("FOLDERNAME") 
		   objRow.SetAttributeNode(objColName)
	
		   '*** Append the src attribute to the field node ***
		   Set objcolName = objDom.createAttribute("src")
		   objcolName.Text = "TREELOAD.asp?FLDID=" & rsHE("FOLDERID")
		   objRow.SetAttributeNode(objColName)
	
	     '*** Append the action attribute to the field node ***
		   'Set objcolName = objDom.createAttribute("action")
		   'objcolName.Text = "Javascript:DoNothing();"
		   'objRow.SetAttributeNode(objColName)


		 '*** Append the icon attribute to the field node ***
		 	' Set objcolName = objDom.createAttribute("icon")
		  	' objcolName.Text = "img2.gif"
		   	' objRow.SetAttributeNode(objColName)
		
		 '*** Append the openIcon attribute to the field node ***
		  	' Set objcolName = objDom.createAttribute("openIcon")
		   	' objcolName.Text = "img2.gif"
			' objRow.SetAttributeNode(objColName)

		   objRoot.appendChild objRow
	
			rsHE.moveNext()	
		Wend
	Call CloseRs(rsHE)
	
	SQL = 	"SELECT DOCUMENTID, DOCNAME, DOCFILE, FOLDERID " &_
			"FROM DOC_DOCUMENT D " &_
			"	LEFT JOIN G_TEAMCODES TC ON D.DOCFOR LIKE '%' + TC.TEAMCODE + '%' " &_
			"	LEFT JOIN E_TEAM T ON T.TEAMID = TC.TEAMID " &_
			" WHERE FOLDERID = " & FLDID & " AND ((TC.TEAMCODE LIKE '%" & Session("TeamCode") & "%') OR (D.CREATEDBY = " & SESSION("USERID") & " AND D.DOCUMENTID = (SELECT MAX(DOCUMENTID) FROM DOC_DOCUMENT))) " &_
			" GROUP BY DOCUMENTID, DOCNAME, DOCFILE, FOLDERID " &_
			" ORDER BY DOCNAME "
			
	Call OpenRs(rsDoc, SQL)

	While NOT rsDoc.EOF
	
		Set objRow = objDom.createElement("tree")
	
		'*** Append the text attribute to the field node ***
		Set objcolName = objDom.createAttribute("text")
		objcolName.Text =rsDoc("DOCNAME")
		objRow.SetAttributeNode(objColName)
	
		Set objcolName = objDom.createAttribute("id")
		objcolName.text = rsDoc("DOCUMENTID")
		objRow.SetAttributeNode(objColName)

		'*** Append the action attribute to the field node ***
		Set objcolName = objDom.createAttribute("action")
		'objcolName.Text = "Javascript:DoNothing();"
		objcolName.Text = "Javascript:GO('/NEWS/DOCUMENTMANAGER/DOCUMENTS" & GetFolderPath(rsDoc("FOLDERID")) & "/" & rsDoc("DOCFILE") & "')"
		objRow.SetAttributeNode(objColName)
	
		objRoot.appendChild objRow

			
	rsDoc.moveNext()	
	Wend
		
	Call CloseRs(rsDoc)
		

	Call CloseDB()

	Set objPI = objDom.createProcessingInstruction("xml", "version='1.0'")

	Response.write objDom.xml 
	

%>
