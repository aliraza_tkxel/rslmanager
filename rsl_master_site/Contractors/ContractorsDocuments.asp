<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
  	
    Function stripHTML(strHTML)
    'Strips the HTML tags from strHTML

      Dim objRegExp, strOutput
      Set objRegExp = New Regexp

      objRegExp.IgnoreCase = True
      objRegExp.Global = True
      objRegExp.Pattern = "<(.|\n)+?>"

      'Replace all HTML tag matches with the empty string
      strOutput = objRegExp.Replace(strHTML, "")
      
      'Replace all < and > with &lt; and &gt;
      strOutput = Replace(strOutput, "<", "&lt;")
      strOutput = Replace(strOutput, ">", "&gt;")
      
      stripHTML = strOutput    'Return the value of strOutput

      Set objRegExp = Nothing
    End Function

	l_firstname	= session("FIRSTNAME")
	l_lastname	= session("LASTNAME")
	
	' Complete the login user area at the top right of the whiteboard
	id_card_HTML =  " <table width='117' border='0' cellspacing='0' cellpadding='0'>" 
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td><b>" & l_firstname & " " & l_lastname & "</b></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td height='3'></td><td></td></tr>"
	id_card_HTML = id_card_HTML & "<tr><td width='5'>&nbsp;</td><td>" & DATE() & "</td></tr>"
	id_card_HTML = id_card_HTML & " </table>"
%>

<HTML>
<HEAD>
	<title>Contractor Documents</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<link rel="stylesheet" href="/css/RSL.css" type="text/css">
	<link type="text/css" rel="stylesheet" href="/js/XTree/xtree2.css" />
	<style type="text/css">
	<!--
		.style1 {
			color: #133E71;
			font-weight: bold;
		}

		a {text-decoration: none !important;}
		a:active {text-decoration: none !important;}
		a:hover {text-decoration: none !important;}
	-->
	</style>

	<script type="text/JavaScript" src="/js/XTree/xtree2.js"></script>
	<script type="text/JavaScript" src="/js/XTree/xmlextras.js"></script>
	<script type="text/JavaScript" src="/js/XTree/xloadtree2.js"></script>
	<script language="JavaScript">
		var rti;
		var CurrentSelected = ""
		function GO(iPage){
		   CurrentlySelected = tree.getSelected().getParent()
		   location.href = iPage
		}
				
		function DoNothing() { var a; }

		function ReloadMe(){
			try {
				if (CurrentlySelected != "")
					CurrentlySelected.reload();
			}
			catch(e){
				location.href = "BoardPapers.asp"	
			}
		}
	</script>
	<script language="JavaScript" src="/js/preloader.js"></script>
	<script language="JavaScript" src="/js/general.js"></script>
	<script language="JavaScript" src="/js/menu.js"></script>
	<script language="JavaScript">
		function cC(itemRef, theCol){
			if (itemRef.style.color == "") 
				itemRef.style.color = theCol;
			else
				itemRef.style.color = "";	
		}

		function load_MPIData(){
		<% if Session("TeamCode") <> "SUP" then %>
			MPI_loadbutton.innerHTML = "Loading MPI Data ....."
			RSLFORM2.action = "include/MPIRight.asp"
			RSLFORM2.target = "MPIData"
			RSLFORM2.submit()
		<%end if%>
		}

	</script>
</head>
<body bgcolor=#ffffff onload="preloadImages();initSwipeMenu(0)" marginheight=0 leftmargin="10" topmargin="10" marginwidth="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->
<table height=100% cellspacing=0 cellpadding=0>
	<TR>
		<td width=12px height=100% valign=top nowrap>&nbsp;</td>
	  	<td width=173px height=100% valign=top nowrap> 
        <!-- START OF LEFT SIDE BAR -->

		<table width="234" border="0" cellpadding="0" cellspacing="0" class="bgColour">
			<tr>
				<td>  
					<div style="display:none;">
						<img name="web_folder" src="images/folder.png" />
						<img name="web_openfolder" src="images/openfolder.png" />
						<img name="web_file" src="images/file.png" />
						<img name="web_lminus" src="images/Lminus.png" />
						<img name="web_lplus" src="images/Lplus.png" />
						<img name="web_tminus" src="images/Tminus.png" />
						<img name="web_tplus" src="images/Tplus.png" />
						<img name="web_i" src="images/I.png" />
						<img name="web_l" src="images/L.png" />
						<img name="web_t" src="images/T.png" />
						<img name="web_base" src="images/base.gif" />
					</div>
					<div class="dtree">
					<script type="text/javascript"> 

						webFXTreeConfig.rootIcon		= web_folder.src;
						webFXTreeConfig.openRootIcon	= web_openfolder.src;
						webFXTreeConfig.folderIcon		= web_folder.src;
						webFXTreeConfig.openFolderIcon	= web_openfolder.src;
						webFXTreeConfig.fileIcon		= web_file.src;
						webFXTreeConfig.lMinusIcon		= web_lminus.src;
						webFXTreeConfig.lPlusIcon		= web_lplus.src;
						webFXTreeConfig.tMinusIcon		= web_tminus.src;
						webFXTreeConfig.tPlusIcon		= web_tplus.src;
						webFXTreeConfig.iIcon			= web_i.src;
						webFXTreeConfig.lIcon			= web_l.src;
						webFXTreeConfig.tIcon			= web_t.src;
			
						var rti;
						var tree = new WebFXTree("RSL Document Manager", "", "", web_base.src, web_base.src);
						<%
							SQL = "SELECT DISTINCT F.* FROM DOC_FOLDER F INNER JOIN DOC_DOCUMENT D ON (D.FOLDERID = F.FOLDERID AND DOCFOR LIKE '%," & Session("TeamCode") & ",%') WHERE PARENTFOLDER = -1 ORDER BY FOLDERNAME"
							Call OpenDB()
							Call OpenRs(rsCC, SQL)
							while NOT rsCC.EOF
									Response.Write "tree.add(new WebFXLoadTreeItem(""" & rsCC("FOLDERNAME") & """, ""Treeload.asp?FLDID=" & rsCC("FOLDERID") & """, ""JavaScript:DoNothing();"", """"));"
								rsCC.moveNext
							wend

							Call CloseRs(rsCC)
							Call CloseDB()
						%>

						tree.write()
						</script> 
					</div>
				</td>
			</tr>
		</table>

        <!-- END OF LEFT SIDE BAR -->
      	</td>
		<td valign=top width=349px nowrap>&nbsp;</td>
		<td><img src="/myImages/spacer.gif" width=1 height=17></td>
		<TD valign=top width=100% style='border-left:1px solid #133e71' bgcolor="#f8f6f6"> </td>
	</tr>
</table>
</div>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name="frmSaveTarget" style="display:none" src="about:blank"></iframe>
</body>
</html>
