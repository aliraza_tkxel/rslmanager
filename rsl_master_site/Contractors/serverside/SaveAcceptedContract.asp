<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
Dim orgid,patchid,developmentid,scopeid,dbcmd,dbrs

set dbrs=server.createobject("ADODB.Recordset")
set dbcmd= server.createobject("ADODB.Command")
	dbcmd.ActiveConnection=RSL_CONNECTION_STRING

Function GetPostedData()
	'This function will get all the posted data
	orgid=Request.Form.Item("sel_ORGANISATION")
	patchid=Request.Item("sel_PATCH")
	developmentid=Request.Item("sel_SCHEME")
	scopeid=Request.Item("hid_select_scopeid")
End Function

Function AcceptContract()

	Dim array_scopeids,i
		array_scopeids=split(scopeid,"|")
	
	 For i=0 to UBOUND(array_scopeids)
			
				SQL=" INSERT INTO C_REPAIR (JOURNALID, LASTACTIONDATE, ITEMSTATUSID, ITEMACTIONID, ITEMDETAILID, "&_
					"    CONTRACTORID, TITLE, NOTES,SCOPEID) "&_	
					" SELECT CR.JOURNALID,GETDATE(),CR.ITEMSTATUSID,16,NULL," & orgid & ",'Annual Gas Servicing',NULL,CR.SCOPEID "&_
					" FROM C_REPAIR CR "&_
					" WHERE CR.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID=CR.JOURNALID) "&_
					"   AND CR.SCOPEID IN(" & array_scopeids(i) & ")" 
				'rw SQL
				'Response.End()
			 
				Conn.Execute(SQL)
	 Next 	
End Function

OpenDB()
	GetPostedData()
	AcceptContract()
CloseDB()		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Accept Contract</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
    parent.alert("Contract Accepted");
	parent.GetContracts();
	return;
}
</script>
<body onLoad="ReturnData()">
<p>Contract Accepted</p>
</body>
</html>