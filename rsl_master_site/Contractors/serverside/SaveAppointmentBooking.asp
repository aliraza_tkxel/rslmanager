<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim orgid,patchid,developmentid,repairhistoryid,appointmentdate,dbcmd,dbrs,appointmenttime
	
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")    
    	dbcmd.ActiveConnection=RSL_CONNECTION_STRING
	
	Function GetPostedData()
	    'This function will get all the posted data
	    orgid=Request.Item("sel_ORGANISATION")
	    patchid=Request.Item("sel_PATCH")
	    developmentid=Request.Item("sel_SCHEME")
		
		repairhistoryid=Request.Item("hid_select_repairhistoryid")			
		If Mid(repairhistoryid , Len(repairhistoryid ), 1) = "|" Then 
			repairhistoryid = Mid(repairhistoryid , 1, Len(repairhistoryid ) - 1) 
		End If 
			
	    appointmentdate=Request.Item("txt_appointment_date")
	    appointmenttime=Request.Item("hid_appointmenttime")
		
	End Function
	
	
	Function SaveAppointments()
	
	    dim array_repairhistoryid,i
        	array_repairhistoryid=split(repairhistoryid,"|")
        
         For i=0 to UBOUND(array_repairhistoryid)
                
                      
           SQLREPAIR =" SELECT CR.JOURNALID,CR.ITEMSTATUSID,CR.ITEMACTIONID,CR.TITLE,CR.SCOPEID,ISNULL(CRP.APPOINTMENTID,0) AS APPOINTMENTID  "&_
                      " FROM C_REPAIR CR "&_
	                  "   LEFT JOIN C_REPAIRTOGASSERVICING CRP ON CRP.REPAIRHISTORYID=CR.REPAIRHISTORYID "&_
                      "  WHERE CR.REPAIRHISTORYID= "& array_repairhistoryid(i)  
                      

           Call OpenRs(rsSet, SQLREPAIR)        
           
          if(not rsSet.eof) then
                
                 SQL=	" SET NOCOUNT ON;INSERT INTO C_REPAIR (JOURNALID, LASTACTIONDATE, ITEMSTATUSID, ITEMACTIONID, ITEMDETAILID, "&_
					    "	  CONTRACTORID,TITLE,SCOPEID)	"&_ 
		                "    VALUES(" & rsSet("JOURNALID") & ",GETDATE()," & rsSet("ITEMSTATUSID") & "," & rsSet("ITEMACTIONID")& ",NULL," & orgid & ",'" & rsSet("TITLE") & "'," & rsSet("SCOPEID") & ");SELECT @@IDENTITY AS REPAIRHISTORYID; "    
                
                dbcmd.CommandText=SQL
                set dbrs=dbcmd.execute
                
                 if(not dbrs.eof) then	
                    
                    SQLGS = "  INSERT INTO C_REPAIRTOGASSERVICING (REPAIRHISTORYID, APPOINTMENTID,APPOINTMENTDATE) " &_
                          "   VALUES (" & dbrs("REPAIRHISTORYID") & ",1,convert(smalldatetime,'" & appointmentdate &" "& appointmenttime & "',103))"
                 
                    Conn.Execute(SQLGS)
                    
                 end if
                 
                      
                 
           end if
          CloseRs(rsSet) 
	    Next 
			
    End Function
    
	Call OpenDB()
		Call GetPostedData()
        Call SaveAppointments()
	Call CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Save Appointments</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
    parent.alert("Appointment Saved");
	parent.GetAppointmentBooking();
	return;
}
</script>
<body onload="ReturnData()">
<p>Save Appointment</p>
</body>
</html>