<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,appointmentid,Invoicing
	
	Heading="Invoicing"
	
	Function GetPostedData()
	    //This function will get all the posted data
	    orgid=nulltest(Request.Form.Item("sel_ORGANISATION"))
	    patchid=nulltest(Request.Form.Item("sel_PATCH"))
	    developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
	End Function
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull
		
	End Function
	
	Function GetInvoiceDetail()
	
	'------------------------------------------------------------------------'
    ' STORED PROCEDURE [GS_INVOICE_PROPERTY_APPLIANCE] IS USED TO GET THE VALUE 
    ' FOR INVOICE STAGE ON THE CONTRACTOR SIDE
    '------------------------------------------------------------------------'
	      
     set spUser = Server.CreateObject("ADODB.Command")
	    spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "[GS_INVOICE_PROPERTY_APPLIANCE]"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,ORGID)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,PATCHID)
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,DEVELOPMENTID)
    set rsSet = spUser.Execute
	    
       
   if(not rsSet.eof) then
        Invoicing ="<div style=""height:350;overflow:scroll;margin:0 10 0 10;padding-right:5px;width:100%;""><table style='border-collapse:collapse;width:100%;'  cellpadding=2px; >"
        Invoicing = Invoicing & "<tr>"   
        Invoicing = Invoicing & "<td class=""heading2"">Address</td>"
        Invoicing = Invoicing & "<td class=""heading2"">Appliance</td>"
        Invoicing = Invoicing & "<td class=""heading2"">LGSR No</td>"
        Invoicing = Invoicing & "<td class=""heading2"">Issue Date</td>"
        Invoicing = Invoicing & "<td class=""heading2"">Value</td>"
        Invoicing = Invoicing & "<td class=""heading2"">Contract Name</td>"
        Invoicing = Invoicing & "</tr>"
        Invoicing = Invoicing & "<tr><td class=""dottedline"" colspan=7 style=""margin-left:10px;""></td></tr>"
        
        While(not rsSet.eof)
            Invoicing = Invoicing & "<tr style='color:#133e71;'>" 
            Invoicing = Invoicing & "<td>" &  rsSet("PROPERTYADDRESS") & "</td>"
            Invoicing = Invoicing & "<td>" &  rsSet("APPLIANCETYPE") & "</td>"
            Invoicing = Invoicing & "<td>" &  rsSet("CP12NUMBER") & "</td>"
            Invoicing = Invoicing & "<td>" &  rsSet("ISSUEDATE") & "</td>"
            Invoicing = Invoicing & "<td>" &  FormatCurrency(rsSet("COSTPERAPPLIANCE")) & "</td>"
            Invoicing = Invoicing & "<td>" &  rsSet("CONTRACTNAME") & "</td>"
            Invoicing = Invoicing & "<td style='text-align:center;'><input type=""checkbox"" name=""cb_invoice"" id=""cb_invoice"" value=""" & rsSet("REPAIRHISTORYID")& "," &  rsSet("APPLIANCETYPEID") & """ onclick=""cal_batchvalue()""/><input type=""checkbox"" name=""cb_invoice"" id=""cb_invoice""  value=""0,0"" style='display:none;'/></td>"
            Invoicing = Invoicing & "</tr>"
            rsSet.movenext()
       wend  	   
       Invoicing = Invoicing & "</table></div>"
       Invoicing = Invoicing & "<p class=""dottedline""></p>"
       
       
       Invoicing=Invoicing & "<p style=""margin:19 10 15 0;text-align:right;padding-bottom:0px;"">" &_
                                                    "&nbsp;" &_
                                                    "Invoice Value: <input type=""text"" value="""" class=""textbox100"" name=""txt_Invoicevalue"" readonly />" &_    
                                                "</p>"

       Invoicing=Invoicing & "<p style=""margin:0 10 0 0;text-align:right;padding-bottom:15px;"">" &_
                                                    "&nbsp;" &_
                                                    "<input type=""button"" value=""Select All"" name=""btn_selectall"" class=""RSLbutton"" onclick=""selectall('cb_invoice');""/>&nbsp;&nbsp;" &_
                                                    "<input type=""button"" value=""Process Invoice"" class=""RSLbutton"" onclick=""save_batch_invoice()"" />" &_    
                                                "</p>"
           
	 else
	   Invoicing="No data exist for this contractor"  
     end if
       
    End Function

	OpenDB()
		GetPostedData()
        GetInvoiceDetail()
	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Invoicing</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.document.getElementById("content_heading").innerHTML = Heading.innerHTML;
	parent.document.getElementById("content_body").innerHTML=Summary.innerHTML;
	return;
}
</script>
<body onLoad="ReturnData()">
<div id="Heading"><%=Heading%></div>
<div  style=" text-align:center;" id="Summary"><%=Invoicing%></div>
</body>
</html>