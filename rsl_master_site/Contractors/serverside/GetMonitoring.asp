<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,AppointmentBooking
	Dim HeadPlusData,MonitoringData,AllData,Menu,Head,monitoringid,AppointmentData,SubHead
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function
	
	Function GetPostedData()
		'This function will get all the posted data
		orgid=nulltest(Request.Item("sel_ORGANISATION"))
		patchid=nulltest(Request.Form.Item("sel_PATCH"))
		developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
        monitoringid=Request.Item("hid_mon_menu") 
	End Function
	
		
	Function GetHead(StyleLine,Title,Id,styleTitle)
	    Dim AnyHead
	    AnyHead="      <div >"
        AnyHead=AnyHead & "   <div class='heading' id='" & Id & "' " & styleTitle & ">" & Title & "</div>"
        AnyHead=AnyHead & "   <div  class='content_headline' "& StyleLine &">"
        AnyHead=AnyHead & "            <div class='content_headlinediv' style='border-bottom:1px solid #133e71;'></div> "
        AnyHead=AnyHead & "            <div class='content_headlinediv' style='border-right:1px solid #133e71;'></div> "
        AnyHead=AnyHead & "    </div>"
        AnyHead=AnyHead & "</div>"
        GetHead=AnyHead
	End Function
	
	Function MenuOption()
	    Menu="<p style='margin:20 0 5 0'>"
        Menu=Menu & "<select id=""sel_Monitoring"" name=""sel_Monitoring"" class='textbox200' style='width:190px;font-weight:bold;color:#133e71;font-size:12px;' onchange=""ChangeMenu();"">"   
        if(monitoringid<>1 ) then
            Menu=Menu & "<option value='1'>LGSR Status</option>"
            Menu=Menu & "<option value='2' selected>Appointments Status</option>"
        else
            Menu=Menu & "<option value='1' selected>LGSR Status</option>"
            Menu=Menu & "<option value='2'>Appointments Status</option>"
        end if
        Menu=Menu & "</select>"
        Menu=Menu & "</p>"
        Menu=Menu & "<p class='line'> </p>"
	End Function
	
	
	Function GetPropertyCP12Status()
	    
   MonitoringData="<table style='border-collapse:collapse;width:100%;' cellspacing=4 cellpadding=2>"
		
	set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GA_CP12_COUNTS"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
	set rsSet = spUser.Execute
		If NOT rsSet.EOF Then
					MonitoringData=MonitoringData & "<tr><td class='fonttwelevewithcolor'>Expiry Date Missing <span class='linkstylesmall' onclick=""GetDetailMonitoring(7)"">" & rsSet("ISSUEDATE_MISSING") & "</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Expired <span class='linkstylesmall' onclick=""GetDetailMonitoring(0)"">" & rsSet("EXPIRED") & "</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in < 1 week <span class='linkstylesmall' onclick=""GetDetailMonitoring('1')"">" & rsSet("LESSTHANWEEK_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 1 - 2 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('1-2')"">" & rsSet("BETWEEN1AND2WEEKS_CP12") &"</span></td></tr>"
					MonitoringData=MonitoringData & "<tr><td class='fonttwelevewithcolor'>Due in 2 - 3 weeks <span class='linkstylesmall' onclick=""GetDetailMonitoring('2-3')"">" & rsSet("BETWEEN2AND4WEEKS_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 3 - 6 months <span class='linkstylesmall' onclick=""GetDetailMonitoring('3-6')"">" & rsSet("BETWEEN3AND6MONTHS_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in 6 - 12 months <span class='linkstylesmall' onclick=""GetDetailMonitoring('6-12')"">" & rsSet("BETWEEN6AND12MONTHS_CP12") &"</span></td>"
					MonitoringData=MonitoringData & "<td class='fonttwelevewithcolor'>Due in > 12 months <span class='linkstylesmall' onclick=""GetDetailMonitoring('12')"">" & rsSet("GREATERTHAN12MONTHS_CP12") &"</span></td></tr>"
		End If
	set sprocDisplayIagManagerUsers = nothing
	
     MonitoringData= MonitoringData & "</table>"
     AllData="<div class=""content_body"" style='width:97%;'>" & Menu & MonitoringData & "</div>"

    End Function
	
    
    Function GetAppointmentStatus()
	    
   AppointmentData="<table style='border-collapse:collapse;width:100%;' cellspacing=4 cellpadding=2>"
	
	set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GA_APPOINTMENT_COUNTS"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
	set rsSet = spUser.Execute
		If(NOT rsSet.eof) Then
        AppointmentData=AppointmentData & "<tr><td class='fonttwelevewithcolor'>Overdue <span class='linkstylesmall' onclick=""GetDetailMonitoring('0')"">" & rsSet("OVERDUE_APPOINTMENT") &"</span></td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in < 1 week <span class='linkstylesmall' onclick=""GetDetailMonitoring('1')"">" & rsSet("LESSTHAN1WEEK_APPOINTMENT") &"</span></td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in 1 month <span class='linkstylesmall' onclick=""GetDetailMonitoring('1-1')"">" & rsSet("BETWEEN1WEEKAND1MONTH_APPOINTMENT") &"</span></td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in > 1 month <span class='linkstylesmall' onclick=""GetDetailMonitoring('2')"">" & rsSet("GREATERTHAN1MONTH_APPOINTMENT") &"</span></td></tr>"
		Else
	    AppointmentData=AppointmentData & "<tr><td class='fonttwelevewithcolor'>Overdue 0</td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in < 1 week 0</td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in 1 month 0</td>"
        AppointmentData=AppointmentData & "<td class='fonttwelevewithcolor'>Due in > 1 month 0</td></tr>"
		End If
	set sprocDisplayIagManagerUsers = nothing
	
    AppointmentData= AppointmentData & "</table>"
    AllData="<div class=""content_body"" style='width:97%;'>" & Menu & AppointmentData & "</div>"
	
    End Function
    

	Call OpenDB()
		Call GetPostedData()
        Head=GetHead("style='width:75%;'","Monitoring","MonitoringHead","style='width:22%;'")
        SubHead=GetHead("style='width:73%'","","MonitoringSubHead","style='width:25%;'")
		Call MenuOption()
		If(monitoringid=1) Then
			Call GetPropertyCP12Status()
		Else
			Call GetAppointmentStatus()
		End If
	Call CloseDB()
	HeadPlusData="<div id=""mon_menu_data"" style='width:100%;height:100%;'><div id=""mon_menu"" style='width:100%;margin-bottom:10px;'>" & Head & AllData & "</div><div id=""mon_data"" style='border:1px solid white;float:left;width:100%;'>" & SubHead &  "<div class=""content_body"" id=""subdatabody"" style='width:98%;height:100%;padding-bottom:29px;'></div> </div></div>"
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Manage Appointment</title></head>
<script language="javascript" type="text/javascript">
function ReturnData()
{
	parent.GetDetailMonitoring(0);
	parent.document.getElementById("content").innerHTML= parent.document.getElementById("content").innerHTML + Summary.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<form name = "thisForm" method="post" action="">
<div id="Heading"><%=Heading%></div>
<div  style=" text-align:center;" id="Summary"><%=HeadPlusData%></div>
</form>
</body>
</html>