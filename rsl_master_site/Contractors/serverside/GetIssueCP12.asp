<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,customername,customerno,appointmentdate,AppointmentBooking,appointmentid
	
	Heading="Issue LGSR"
	
	Function GetPostedData()
	    'This function will get all the posted data
	    orgid=nulltest(Request.Form.Item("sel_ORGANISATION"))
	    patchid=nulltest(Request.Form.Item("sel_PATCH"))
	    developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
	    address=nulltest(Request.Form.Item("txt_STREET"))
	    postcode=nulltest(Request.Form.Item("txt_POSTCODE"))
        customername=nulltest(Request.Form.Item("cust_name"))
	    customerno=nulltest(Request.Form.Item("cust_no"))
	    appointmentdate=nulltest(Request.Form.Item("txt_appointmentdate"))
	    appointmentid=nulltest(Request.Form.Item("sel_APPOINTMENTSTAGE"))
	End Function
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull
		
	End Function
	
	Function GetContractDetail()
	    
     '------------------------------------------------------------------------'
    ' STORED PROCEDURE GS_ISSUE_CP12 IS USED TO GET THE VALUE 
    ' FOR ISSUE CP12 STAGE ON THE CONTRACTOR SIDE
    '------------------------------------------------------------------------'
	      
     set spUser = Server.CreateObject("ADODB.Command")
	    spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "[GS_ISSUE_CP12]"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,ORGID)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,PATCHID)
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,DEVELOPMENTID)
		spUser.Parameters.Append spUser.CreateParameter("@POSTCODE", 200, 1,10,POSTCODE)
		spUser.Parameters.Append spUser.CreateParameter("@ADDRESS", 200, 1,150,ADDRESS)
		spUser.Parameters.Append spUser.CreateParameter("@APPOINTMENTDATE", 200, 1,10,APPOINTMENTDATE)
		spUser.Parameters.Append spUser.CreateParameter("@CUSTOMERNAME", 200, 1,150,CUSTOMERNAME)
		spUser.Parameters.Append spUser.CreateParameter("@CUSTOMERNO", 200, 1,50,CUSTOMERNO)
		spUser.Parameters.Append spUser.CreateParameter("@APPOINTMENTID", 3, 1,4,APPOINTMENTID)
    set rsSet = spUser.Execute
          
   if(not rsSet.eof) then
                            
        AppointmentBooking ="<div style=""height:358;overflow:scroll;width:100%;margin:0 10 0 10;padding-right:5px;""><table style='border-collapse:collapse;width:100%;'  cellpadding=10px; >"
        AppointmentBooking = AppointmentBooking & "<tr>"   
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Customer Name</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Address</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Appointment Date</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Stage</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Contract Name</td>"
        AppointmentBooking = AppointmentBooking & "</tr>"
        AppointmentBooking = AppointmentBooking & "<tr><td class=""dottedline"" colspan=5 style=""margin-left:10px;""></td></tr>"
        
        While(not rsSet.eof)
            AppointmentBooking = AppointmentBooking & "<tr style='color:#133e71;'>" 
            AppointmentBooking = AppointmentBooking & "<td class=""linkstylesmall"" onclick=""IssueCP12('" & rsSet("PROPERTYID") & "','" & rsSet("APPOINTMENTDATE")& "'," & rsSet("REPAIRHISTORYID")& ");"">" &  rsSet("CUSTOMERNAME") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" & rsSet("PROPERTYADDRESS") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" & rsSet("APPOINTMENTDATE") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" & rsSet("DESCRIPTION") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" & rsSet("CONTRACTNAME") & "</td>"
            AppointmentBooking = AppointmentBooking & "</tr>"
            rsSet.movenext()
       wend  	   
       AppointmentBooking = AppointmentBooking & "</table></div>"
       AppointmentBooking = AppointmentBooking & "<p class=""dottedline"" style='margin-bottom:76px;'></p>"
	   
	 else
	   AppointmentBooking="No data exist for this contractor"  
     end if
    End Function

	OpenDB()
		GetPostedData()
        GetContractDetail()
	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Manage Appointment</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.document.getElementById("content_heading").innerHTML = Heading.innerHTML;
	parent.document.getElementById("content_body").innerHTML=Summary.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<div id="Heading"><%=Heading%></div>
<div  style=" text-align:center;" id="Summary"><%=AppointmentBooking%></div>
</body>
</html>