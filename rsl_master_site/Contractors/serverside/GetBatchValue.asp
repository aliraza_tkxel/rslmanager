<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim repairhistoryandappointmentid,batchvalue,orgid
	
	Function GetPostedData()
	    repairhistoryandappointmentid=Request.Form("hid_select_repairhistoryid")	
	    orgid=Request.Form.Item("sel_ORGANISATION")
	End Function
	
	Function GetBatchValue()
	   dim array_repairhistoryandappointmetid,i
       array_repairhistoryandappointmetid=split(repairhistoryandappointmentid,"|")
       
       batchvalue=0
       
       For i=0 to UBOUND(array_repairhistoryandappointmetid)
            
           repairhistoryid=split(array_repairhistoryandappointmetid(i),",")
           
           SQL=" SELECT ISNULL(SA.COSTPERAPPLIANCE,0)  AS COSTPERAPPLIANCE"&_
               " FROM S_SCOPE S "&_
               "  INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID "&_
               "  INNER JOIN PDR_DEVELOPMENT PD ON PD.PATCHID=SS.PATCHID "&_
               "      AND (PD.DEVELOPMENTID=SS.DEVELOPMENTID OR SS.DEVELOPMENTID IS NULL) "&_
               "  INNER JOIN P__PROPERTY PP ON PP.DEVELOPMENTID=PD.DEVELOPMENTID "&_
               "  INNER JOIN  C_JOURNAL CJ ON CJ.PROPERTYID=PP.PROPERTYID "&_
               "  INNER JOIN C_REPAIR CR ON CR.JOURNALID=CJ.JOURNALID  AND CR.SCOPEID IS NOT NULL "&_
               "  LEFT JOIN GS_PROPERTY_APPLIANCE GPA ON GPA.PROPERTYID=PP.PROPERTYID "&_
               "  LEFT JOIN S_SCOPETOAPPLIANCE SA ON SA.SCOPEID=S.SCOPEID AND SA.APPLIANCETYPEID=GPA.APPLIANCETYPEID "&_	
               " WHERE S.ORGID=" & ORGID & " and CR.REPAIRHISTORYID=" & repairhistoryid(0) & " and GPA.APPLIANCETYPEID= " & repairhistoryid(1)

           SQL=" SELECT CASE "&_
			   "            WHEN S.COSTTYPEID=1 THEN S.PROPERTY "&_
			   "            ELSE SA.COSTPERAPPLIANCE "&_
	           "        END AS COSTPERAPPLIANCE "&_
               " FROM  dbo.C_REPAIR CR "&_
	           "    INNER JOIN dbo.C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
	           "    INNER JOIN S_SCOPE S ON S.SCOPEID=CR.SCOPEID "&_
	           "    LEFT JOIN S_SCOPETOAPPLIANCE SA ON SA.SCOPEID=S.SCOPEID AND SA.APPLIANCETYPEID=" & repairhistoryid(1) &_
               " WHERE S.ORGID=" & ORGID & " AND CR.REPAIRHISTORYID=" & repairhistoryid(0)
           
           Call OpenRs(rsSet, SQL)        
           
           if(not rsSet.eof) then
                batchvalue=batchvalue + rsSet("COSTPERAPPLIANCE")
           end if
           CloseRs(rsSet) 
           batchvalue=formatCurrency(batchvalue)
       Next 
	    
	End Function
	
	OpenDB()
		GetPostedData()
        GetBatchValue()
	CloseDB()
		
%>
<html>
<head></head>
<body onload="ReturnData()">
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.RSLFORM.txt_Invoicevalue.value = "<%=batchvalue %>";
	return;
}
</script>
</body>
</html>