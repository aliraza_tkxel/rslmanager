<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,appointmentid,AppointmentBooking
	
	Heading="1st Appointment"
	
	Function GetPostedData()
	    'This function will get all the posted data
	    orgid=nulltest(Request.Form.Item("sel_ORGANISATION"))
	    patchid=nulltest(Request.Form.Item("sel_PATCH"))
	    developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
	    renewal=nulltest(Request.Form.Item("sel_RENEWAL"))
	    address=nulltest(Request.Form.Item("txt_STREET"))
	    postcode=nulltest(Request.Form.Item("txt_POSTCODE"))
        'appointmentid=nulltest(Request.Form.Item("sel_APPOINTMENTSTAGE")) 
	End Function
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull
		
	End Function
	
	Function GetContractDetail()
	    
     Dim issuedatefrom,issuedateto  
     
     issuedatefrom=""
     issuedateto="" 
	 
	 if(renewal<>"") then
	    if(renewal="0-6") then
	        issuedatefrom=0
	        issuedateto=6
	     elseif(renewal="7-12") then
	        issuedatefrom=7
	        issuedateto=12
	    elseif(renewal="1") then
	        issuedatefrom=30
	        issuedateto=365
	    elseif(renewal="1+") then
	        issuedatefrom=365
        end if    
	 end if
	 
    '------------------------------------------------------------------------'
    ' STORED PROCEDURE GS_FIRST_APPOINTMENT_BOOKING IS USED TO GET THE VALUE 
    ' FOR FIRST APPOINTMENT STAGE ON THE CONTRACTOR SIDE
    '------------------------------------------------------------------------'
	 
    set spUser = Server.CreateObject("ADODB.Command")
	    spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GS_FIRST_APPOINTMENT_BOOKING"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,ORGID)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,PATCHID)
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,DEVELOPMENTID)
		spUser.Parameters.Append spUser.CreateParameter("@POSTCODE", 200, 1,10,POSTCODE)
		spUser.Parameters.Append spUser.CreateParameter("@ADDRESS", 200, 1,150,ADDRESS)
		spUser.Parameters.Append spUser.CreateParameter("@ISSUDATEFROM", 3, 1,4,nulltest(ISSUEDATEFROM))
		spUser.Parameters.Append spUser.CreateParameter("@ISSUEDATETO", 3, 1,4,nulltest(ISSUEDATETO))
	set rsSet = spUser.Execute

   
   if(not rsSet.eof) then
        AppointmentBooking ="<div style=""height:307;overflow:scroll;margin:0 10 0 10;padding-right:5px;width:100%;""><table style='border-collapse:collapse;width:100%;'  cellpadding=2px; >"
        AppointmentBooking = AppointmentBooking & "<tr>"   
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Customer Name</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Address</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Renewal Date</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Contract Name</td>"
        AppointmentBooking = AppointmentBooking & "<td class=""heading2"">Select</td>"
        AppointmentBooking = AppointmentBooking & "</tr>"
        AppointmentBooking = AppointmentBooking & "<tr><td class=""dottedline"" colspan=5 style=""margin-left:10px;""></td></tr>"
        
        While(not rsSet.eof)
            AppointmentBooking = AppointmentBooking & "<tr style='color:#133e71;'>" 
            AppointmentBooking = AppointmentBooking & "<td>" &  rsSet("CUSTOMERNAME") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" &  rsSet("PROPERTYADDRESS") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" &  rsSet("ISSUEDATE") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td>" &  rsSet("CONTRACTNAME") & "</td>"
            AppointmentBooking = AppointmentBooking & "<td style='text-align:center;'><input type=""checkbox"" name=""cb_appointment"" id=""cb_appointment"" value=""" & rsSet("REPAIRHISTORYID") & """ /></td>"
            AppointmentBooking = AppointmentBooking & "</tr>"
            rsSet.movenext()
       wend  	   
       AppointmentBooking = AppointmentBooking & "</table></div>"
       AppointmentBooking = AppointmentBooking & "<p class=""dottedline""></p>"
       AppointmentBooking=AppointmentBooking & "<p style=""margin:10 10 0 0;width:100%;text-align:right;"" id=""p_selectall"">" &_
                                                "<input type=""button"" value=""Select All"" name=""btn_selectall"" class=""RSLbutton"" onclick=""selectall('cb_appointment');""/>" &_
                                                "</p>"
         
        AppointmentBooking=AppointmentBooking & "<div style=""text-align:right;width:100%;margin:0 0 0 0;padding:0 0 0 0; ""><div class='dottedbox' style=""margin:10 10 10 0;padding:5 5 5 5;text-align:right;width:70%;"">"&_
                                                "<label for=""txt_appointment_date"" class=""elementlabel"" style=""padding-left:5px;"">Enter appointment date and time for selected properties:</label>" &_
                                                "<input name=""txt_appointment_date"" id=""txt_appointment_date"" type=""text"" class=""textbox200"" style=""margin:0 5 0 5;width:150px;""  readonly />"&_
												"<span stytle=""margin:5 0 0 0;""><a href=""javascript:;"" class=""iagManagerSmallBlue"" onclick=""YY_Calendar('txt_appointment_date',360,150,'de','#FFFFFF','#133E71','YY_calendar1')"" tabindex=""1"">" &_
                                                "<img alt=""calender_image"" width=""18px"" height=""19px""  src=""images/cal.gif""/></a></span>"
        AppointmentBooking=AppointmentBooking & " <p style='margin:5 0 5 0'> "&_
                                                  " <select name='sel_HOUR' class='textbox100' style='width:50px;'> "&_
			                                                " <option value='1'>01</option> "&_
			                                                " <option value='2'>02</option> "&_
			                                                " <option value='3'>03</option> "&_
			                                                " <option value='4'>04</option> "&_
			                                                " <option value='5'>05</option> "&_
			                                                " <option value='6'>06</option> "&_
			                                                " <option value='7'>07</option> "&_
			                                                " <option value='8' selected>08</option> "&_
			                                                " <option value='9'>09</option> "&_
			                                                " <option value='10'>10</option> "&_
			                                                " <option value='11'>11</option> "&_
			                                                " <option value='12'>12</option> "&_
			                                      "  </select> "
		AppointmentBooking=AppointmentBooking &   "  <select name='sel_MINUTES' class='textbox100' style='width:50px;margin-right:10px;'> "&_
			                       		       " <option value='00'>00</option> "&_                                
				  		       " <option value='15'>15</option> "&_
			                                                 " <option value='30'>30</option> "&_
			                                                 " <option value='45'>45</option> "&_
			                                       " </select> "
		AppointmentBooking=AppointmentBooking &    " <select name='sel_MARADIAM' class='textbox100' style='width:50px;'> "&_
			                                       "     <option value='1'>AM</option> "&_
			                                       "     <option value='2'>PM</option> "&_ 
			                                       " </select> "
		AppointmentBooking=AppointmentBooking & "</p>" 	                                       
        
        AppointmentBooking=AppointmentBooking & "</div></div> "
        AppointmentBooking=AppointmentBooking & " <div id='Calendar1' style='background-color:white;position:absolute; left:20px; top:20px; width:200px; height:109px; z-index:20; visibility: hidden'></div>"
        AppointmentBooking=AppointmentBooking & "<p style=""margin:0 10 0 0;text-align:right;padding-bottom:0px;"">" &_
                                                    "&nbsp;" &_
                                                    "<input type=""button"" value=""Save Appointments"" class=""RSLbutton"" onclick=""save_appointment()"" />" &_    
                                                "</p>"
           
	 else
	   AppointmentBooking="No data exist for this contractor"  
     end if
       
   End Function

	OpenDB()
		GetPostedData()
        GetContractDetail()
	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Appointment Booking</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.document.getElementById("content_heading").innerHTML = Heading.innerHTML;
	parent.document.getElementById("content_body").innerHTML=Summary.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<div id="Heading"><%=Heading%></div>
<div style=" text-align:center;" id="Summary"><%=AppointmentBooking%></div>
</body>
</html>