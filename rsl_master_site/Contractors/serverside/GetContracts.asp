<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal,ContractAccept
	
	Heading="Contracts"
	
	Function GetPostedData()
	    //This function will get all the posted data
	    orgid=nulltest(Request.Form.Item("sel_ORGANISATION"))
	    patchid=nulltest(Request.Form.Item("sel_PATCH"))
	    developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
	End Function
	
  	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
   Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull
		
	End Function
	 
  Function GetContractDetail()
  
  	'------------------------------------------------------------------------'
    ' STORED PROCEDURE [GS_CONTRACT] IS USED TO GET THE VALUE 
    ' FOR CONTRACT STAGE ON THE CONTRACTOR SIDE
    '------------------------------------------------------------------------'
	      
     set spUser = Server.CreateObject("ADODB.Command")
	    spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "[GS_CONTRACT]"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,ORGID)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,PATCHID)
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,DEVELOPMENTID)
    set rsSet = spUser.Execute
	    
   
   if(not rsSet.eof) then
                        
        ContractAccept ="<div style=""height:398;overflow:scroll;width:100%;margin-right:10px;""><table style='border-collapse:collapse;width:100%;'  cellpadding=8px; >"
        ContractAccept = ContractAccept & "<tr>"   
        ContractAccept = ContractAccept & "<td class=""heading2"">Contract Name</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Type</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Patch</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Scheme</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Cost Type</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Estimated Value</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Start Date</td>"
        ContractAccept = ContractAccept & "<td class=""heading2"">Accepted</td>"
        ContractAccept = ContractAccept & "</tr>"
        ContractAccept = ContractAccept & "<tr><td class=""dottedline"" colspan=8 style=""margin-left:10px;""></td></tr>"
        
        While(not rsSet.eof)
            ContractAccept = ContractAccept & "<tr style='color:#133e71;'>" 
            ContractAccept = ContractAccept & "<td>" &  rsSet("CONTRACTNAME") & "</td>"
            ContractAccept = ContractAccept & "<td>" &  rsSet("AREAOFWORK") & "</td>"
            ContractAccept = ContractAccept & "<td>" &  rsSet("PATCH") & "</td>"
            ContractAccept = ContractAccept & "<td>" &  rsSet("SCHEME") & "</td>"
            ContractAccept = ContractAccept & "<td>" &  rsSet("COSTTYPE") & "</td>"
            ContractAccept = ContractAccept & "<td>" &  formatCurrency(rsSet("ESTIMATEDVALUE")) & "</td>"
            ContractAccept = ContractAccept & "<td>" &  rsSet("STARTDATE") & "</td>"
            ContractAccept = ContractAccept & "<td style='text-align:center;'><input type=""checkbox"" name=""cb_contract"" value=""" & rsSet("SCOPEID") & """ /><input type=""checkbox"" name=""cb_contract"" style='display:none;' value=""" & rsSet("SCOPEID") & """ /></td>"
            ContractAccept = ContractAccept & "</tr>"
            rsSet.movenext()
       wend 
       ContractAccept = ContractAccept & "<tr><td class=""dottedline"" colspan=8 style=""margin-left:10px;""></td></tr>"
         	   
       ContractAccept = ContractAccept & "</table></div>"
       ContractAccept=ContractAccept & "<p style=""margin:10 10 10 0;width:100%;text-align:right;"">" &_
                                            "<input type=""button"" value=""Accept Contract"" class=""RSLbutton"" onclick=""accept_contract(54)"" />" &_
                                        "</p>"
	 else
	   ContractAccept="No data exist for this contractor"  
     end if
    End Function

	OpenDB()
		GetPostedData()
        GetContractDetail()
	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Manage Appointment</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
    //parent.document.getElementById("content_body").style.height="410px";
	parent.document.getElementById("content_heading").innerHTML = Heading.innerHTML;
	parent.document.getElementById("content_body").innerHTML=Summary.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<div id="Heading"><%=Heading%></div>
<div  style=" text-align:center;" id="Summary"><%=ContractAccept%></div>
</body>
</html>