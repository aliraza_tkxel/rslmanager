<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim repairhistoryandappointmentid,batchvalue
	
	Function GetPostedData()
	    repairhistoryandappointmentid=Request.Form("hid_select_repairhistoryid")	
		
	End Function
	
	Function SaveBatchValue()
	   dim array_repairhistoryandappointmetid,i,maxbatchid
       array_repairhistoryandappointmetid=split(repairhistoryandappointmentid,"|")
       
      batchvalue=0
      
      maxbatchid=0
       
      SQL=" SELECT ISNULL(MAX(BATCHID),0) AS MAX_BATCHID FROM GS_INVOICE "
       
      Call OpenRs(rsSet, SQL)    
      
      if(not rsSet.eof) then
      
        maxbatchid= rsSet("MAX_BATCHID")
      
      end if    
      CloseRs(rsSet)
      
      maxbatchid=maxbatchid+1
        
      For i=0 to UBOUND(array_repairhistoryandappointmetid)
            
          repairhistoryid=split(array_repairhistoryandappointmetid(i),",")
           
          
               SQL=" SET NOCOUNT ON;INSERT INTO GS_INVOICE (BATCHID, APPLIANCETYPEID, REPAIRHISTORYID, COSTPERAPPLIANCE, CREATIONDATE) "&_  
                " SELECT DISTINCT " & maxbatchid & ",GPA.APPLIANCETYPEID,CR.REPAIRHISTORYID,SA.COSTPERAPPLIANCE, "&_
               " CONVERT(SMALLDATETIME,GETDATE(),102)  "&_
               " FROM S_SCOPE S "&_
               " INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID "&_
	           "     INNER JOIN C_REPAIR CR ON CR.SCOPEID=S.SCOPEID "&_
	           "     INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=CR.JOURNALID "&_
	           "     INNER JOIN P__PROPERTY PP ON PP.PROPERTYID=CJ.PROPERTYID "&_
	           "     LEFT JOIN PDR_DEVELOPMENT PD ON PD.DEVELOPMENTID=PP.DEVELOPMENTID "&_
               " left JOIN GS_PROPERTY_APPLIANCE GPA ON GPA.PROPERTYID=PP.PROPERTYID  "&_
               " left JOIN S_SCOPETOAPPLIANCE SA ON SA.SCOPEID=S.SCOPEID AND SA.APPLIANCETYPEID=GPA.APPLIANCETYPEID "&_
               " WHERE CR.REPAIRHISTORYID=" & repairhistoryid(0) & " AND GPA.APPLIANCETYPEID=" & repairhistoryid(1)
          
           'response.Write(SQL & "<br>")
           Conn.Execute(SQL)    

		'maxbatchid=maxbatchid+1 
           
         Next 
	    
	End Function
	
	OpenDB()
		GetPostedData()
        SaveBatchValue()
	CloseDB()
		
%>
<html>
<head></head>
<body onload="ReturnData()">
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.alert("Batch has been saved...");
	parent.GetInvoicing();
	return;
}
</script>
</body>
</html>