<%
	Function GetFolderPath(FolderID)

		SQL = "SELECT FOLDERNAME, PARENTFOLDER FROM DOC_FOLDER WHERE FOLDERID = " & FolderID

		Call OpenRs(rsFolder, SQL)
		FolderName = "/" & rsFolder("FOLDERNAME") 
		FolderID = rsFolder("PARENTFOLDER")
		Call CloseRs(rsFolder)
		
		While not PathStart = "END"
		
			SQL = "SELECT PARENTFOLDER,FOLDERNAME FROM DOC_FOLDER WHERE FOLDERID = " & FolderID
			
			Call OpenRs(rsFolderPath, SQL)
			
			IF NOT rsFolderPath.EOF THEN
				FolderName = "/" & rsFolderPath("FOLDERNAME") & FolderName
				IF rsFolderPath("PARENTFOLDER") <> -1 THEN
					FolderID = rsFolderPath("PARENTFOLDER")
				else
					PathStart =  "END"
				end if
			else
				PathStart = "END"
			END IF
			
			Call CloseRs(rsFolderPath)
			
		Wend
				GetFolderPath =  FolderName
	End Function


dim numberDocs
numberDocs = "TOP 6"
if (Request("numberDocs") <> "") then numberDocs = "TOP " & Request("numberDocs")
if (Request("numberDocs") = "-1") then numberDocs = ""

set rsDocs = Server.CreateObject("ADODB.Recordset")
rsDocs.ActiveConnection = RSL_CONNECTION_STRING
	rsDocs.Source = "SELECT " & numberDocs & " DOCUMENTID, FOLDERID, DOCNAME AS TITLE, DOCFILE AS DOCUMENTFILE FROM DOC_DOCUMENT WHERE DOCFOR LIKE '%," & RTRIM(LTRIM(Session("TeamCode"))) & ",%' ORDER BY ORDERID ASC, DATECREATED DESC, DOCUMENTID DESC"

rsDocs.CursorType = 0
rsDocs.CursorLocation = 2
rsDocs.LockType = 2
rsDocs.Open()

%>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
	<style type="text/css">
        <!--
        .style1 {
	        color: #0000CC;
	        font-style: italic;
        }
        -->
    </style>
    <table width="146" border="0" cellspacing="0" cellpadding="0">
		   
		   <%
		    if not rsDocs.EOF then
		    Do While (NOT rsDocs.EOF)
			openDB()
			thePath = GetFolderPath(rsDocs("FOLDERID"))
			CloseDB()
			filename = rsDocs("DOCUMENTFILE")
			docType = Right(rsDocs("DOCUMENTFILE"),3)
				select case lcase(DocType)
					case "txt" 
						ImageType = "<IMG SRC='images/im_txt.gif' WIDTH=12 HEIGHT=16 ALT='' border='0'>"
					case "doc" 
						ImageType = "<IMG SRC='images/im_word.gif' WIDTH=15 HEIGHT=16 ALT='' border='0'>"
					case "pdf"
						ImageType = "<IMG SRC='images/im_pdf.gif' WIDTH=16 HEIGHT=16 ALT='' border='0'>"
					case "xls"
						ImageType = "<IMG SRC='images/im_xls.gif' WIDTH=15 HEIGHT=16 ALT='' border='0'>"
				end select 
				
			 %>
		   
                  <tr>
				  <td width="10" height="1"></td>
              	  <td width="3" height="1"></td>
				  <td >
				    <table width="146" border="0" cellspacing="0" cellpadding="0">
					 <tr>
					 
                    <td><%=rsDocs("TITLE")%></td>
                    <td width="3"></td>
                    <td width="16"></td>
                    <td width="3"></td>
                    <td width="16"></td>
                    <td width="16"><a href="/NEWS/DOCUMENTMANAGER/DOCUMENTS<%=thePath%>/<%=filename%>" target="_blank"><%=ImageType%></a></td>
                    <td width="6"></td>
					<td width="1" bgcolor="#C9CBC5"></td>
                  </tr>
				  </table></td><td  width="1" bgcolor="#C9CBC5"></td></tr>  <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
		   <%	
		   		rsDocs.Movenext()
		   		Loop
			else
		    %>
			  <tr>
				  <td width="10" height="1"></td>
              	  <td width="3" height="1"></td>
				  <td >
				    <table width="146" border="0" cellspacing="0" cellpadding="0">
					 <tr>
					 
                    <td width="5">&nbsp;</td>
                    <td>No Documents available to view </td>
                    <td width="6"></td>
                  </tr>
				  </table></td><td  width="1" bgcolor="#C9CBC5"></td></tr>  <tr>
              <td width="10" height="1"></td>
              <td width="3" height="1"></td>
              <td width="220" height="1" bgcolor="#C9CBC5"></td>
              <td width="1" height="1"></td>
            </tr>
			<% end if %>
              </table>
