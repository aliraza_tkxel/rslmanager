<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,ContractSummary
	
	Heading="Summary"
	
	Function GetPostedData()
	    //This function will get all the posted data
	    orgid=Request.Form.Item("sel_ORGANISATION")
	    patchid=Request.Form.Item("sel_PATCH")
	    developmentid=Request.Form.Item("sel_SCHEME")
	End Function
	
	Function GetContractDetail()
	    
     Dim filter   
	 if (patchid = "") then
	    filter=""
	 else
	    filter=",@patchid=" &  patchid 
	 end if
	
	 if(developmentid<>"") then
	    filter=filter & ",@developmentid=" & developmentid	   
	 end if
	
	set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
             
	dbrs.ActiveConnection = RSL_CONNECTION_STRING 			
	sql="GS_CONTRACT_SUMMARY @ORGID=" & orgid & filter
	
	response.Write(sql)
	
	dbrs.Source = sql
	dbrs.CursorType = 3
	dbrs.CursorLocation = 3
	dbrs.Open()
    
    NoofRecords=dbrs.RecordCount
    
   
    if(not dbrs.eof) then
        ContractSummary="<div style=""height:436;overflow:scroll;width:100%;""><table style='border-collapse:collapse;width:100%;'  cellpadding=2px; >"
        ContractSummary= ContractSummary & "<tr>"   
        ContractSummary= ContractSummary & "<td>&nbsp;</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"">Properties</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Fire</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Water Heater/<br/>Multipoint</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Cooker <br/> Hob</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Traditional <br/> Boiler</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Combi <br/> Boiler</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Oil <br/> Boiler</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Wall <br/> Heater</td>"
        ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:center;'>Services Overdue</td>"    
        ContractSummary= ContractSummary & "</tr>"
        ContractSummary= ContractSummary & "<tr><td class=""dottedline"" colspan=10></td></tr>"
         For intRecord = 1 to dbrs.RecordCount
            
           'IF WE HAVE CHANGED PATCHES WE NEED TO ADD IN THE PATCH SUMMARY LINE
           RecordCount = dbrs.RecordCount
           if(Scheme_PatchID <>"") then
                pre_Scheme_PatchID=Scheme_PatchID
                pre_Scheme_Location=Scheme_Location
                pre_Scheme_Contract_Name=Scheme_Contract_Name
           else
                pre_Scheme_PatchID  		= dbrs("PATCHID")
                pre_Scheme_Location		= dbrs("LOCATION")      
                pre_Scheme_Contract_Name=dbrs("CONTRACTNAME")    
           end if
           Scheme_PatchID  		= dbrs("PATCHID")
           Scheme_Location		= dbrs("LOCATION")
           Scheme_Contract_Name		= dbrs("CONTRACTNAME")
           Scheme_DevelopmentName	= dbrs("DEVELOPMENTNAME")
           Scheme_PropertyCount 	= dbrs("PROPERTIES")
           Scheme_TraditionalBoilerCount 	= dbrs("NOOFTRADITIONALBOLIER")
           Scheme_TraditionalBoilerCost = dbrs("COSTPERTRADITIONALBOILER")
           Scheme_FireCount	= dbrs("NOOFFIRE")
           Scheme_FireCost 	= dbrs("COSTPERFIRE") 
           Scheme_WaterHeaterMultipointCount 	= dbrs("NOOFWATERHEATER_MULTIPOINT")
           Scheme_WaterHeaterMultipointCost 	= dbrs("COSTPERWATERHEATER_MULTIPOINT")    
           Scheme_CookHobCount 		= dbrs("NOOFCOOKER_HOB")
           Scheme_CookHobCost 		= dbrs("COSTPERCOOKER_HOB")
           Scheme_CombiBoilerCount 	= dbrs("NOOFCOMBIBOILER")
           Scheme_CombiBoilerCost 	= dbrs("COSTPERCOMBIBOILER")
           Scheme_OilBoilerCount 	= dbrs("NOOFOILBOILERS")
           Scheme_OilBoilerCost 	= dbrs("COSTPEROILBOILERS")
           Scheme_WallHeaterCount 	= dbrs("NOOFWALLHEATER") 
           Scheme_WallHeaterCost = dbrs("COSTPERWALLHEATER") 
           Scheme_Overdue 	= dbrs("OVERDUEPROPERTIES")
           
           ContractSummaryDev = ContractSummaryDev & "<tr style='color:#133e71;'>" 
           ContractSummaryDev = ContractSummaryDev & "<td style='padding-left:20px;'>" &  Scheme_DevelopmentName & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_PropertyCount & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_FireCount & " (" & formatcurrency(Scheme_FireCost) & ")" & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_WaterHeaterMultipointCount & " (" & formatcurrency(Scheme_WaterHeaterMultipointCost) & ")"  & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_CookHobCount & " (" & formatcurrency(Scheme_CookHobCost) & ")" & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_TraditionalBoilerCount & " (" & formatcurrency(Scheme_TraditionalBoilerCost) &  ")" & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_CombiBoilerCount & " (" & formatcurrency(Scheme_CombiBoilerCost) & ")"  & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_OilBoilerCount & " (" & formatcurrency(Scheme_OilBoilerCost) & ")" & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_WallHeaterCount & " (" & formatCurrency(Scheme_WallHeaterCost) & ")"  & "</td>"
           ContractSummaryDev = ContractSummaryDev & "<td style='text-align:right;'>"  &  Scheme_Overdue & "</td>"
           ContractSummaryDev = ContractSummaryDev & "</tr>"   
     


           Patch_PropertyCount 	= cInt(Patch_PropertyCount) + Cint(Scheme_PropertyCount)
           Patch_TraditionalBoilerCount  = Patch_TraditionalBoilerCount   + Scheme_TraditionalBoilerCount 
           Patch_TraditionalBoilerCost = Patch_TraditionalBoilerCost + Scheme_TraditionalBoilerCost
           Patch_FireCount	= Patch_FireCount  + Scheme_FireCount
           Patch_FireCost = Patch_FireCost  +	Scheme_FireCost
           Patch_WaterHeaterMultipointCount 		= Patch_WaterHeaterMultipointCount + Scheme_WaterHeaterMultipointCount
           Patch_WaterHeaterMultipointCost = Patch_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost  
           Patch_CookHobCount 	= Patch_CookHobCount + Scheme_CookHobCount
           Patch_CookHobCost = Patch_CookHobCost +   Scheme_CookHobCost
           Patch_CombiBoilerCount	= Patch_CombiBoilerCount + Scheme_CombiBoilerCount
           Patch_CombiBoilerCost = Patch_CombiBoilerCost + Scheme_CombiBoilerCost
           Patch_OilBoilerCount = Patch_OilBoilerCount + SchemeOilBoilerCount
           Patch_OilBoilerCost = Patch_OilBoilerCost + Scheme_OilBoilerCost 			 			
           Patch_WallHeaterCount	= Patch_WallHeaterCount + Scheme_WallHeaterCount
           Patch_WallHeaterCost = Patch_WallHeaterCost + Scheme_WallHeaterCost
           Patch_Overdue 	= Patch_Overdue + Scheme_Overdue   

           'if ((cInt(Scheme_PatchID) = cInt(dbrs("PATCHID")) and Scheme_PatchID <> "") or (intRecord   + 1 > dbrs.RecordCount)) then		  
            
'            Response.Write(pre_Scheme_PatchID & "<>" & dbrs("PATCHID") & "<br/>")
'            Response.Write(intRecord   + 1 & ">" & dbrs.RecordCount & "<br/>")
           if ((cInt(pre_Scheme_PatchID) <> cInt(dbrs("PATCHID")) and pre_Scheme_PatchID <> "") or (intRecord   + 1 > dbrs.RecordCount) ) then		
            
                  
                    ContractSummary= ContractSummary & "<tr class=""heading2"" style='padding-left:4px;' >" 
                    ContractSummary= ContractSummary & "<td >" &  pre_Scheme_Location & " - " &  pre_Scheme_Contract_Name & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>"&   Patch_PropertyCount & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_FireCount  & " (" & formatcurrency(Patch_FireCost ) & ")" & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_WaterHeaterMultipointCount   & " (" & formatcurrency(Patch_WaterHeaterMultipointCost  ) & ")" & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_CookHobCount  & " (" & formatcurrency(Patch_CookHobCost ) & ")" & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_TraditionalBoilerCount   & " (" & formatcurrency(Patch_TraditionalBoilerCost )  & ")"  & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_CombiBoilerCount  & " (" & formatcurrency(Patch_CombiBoilerCost ) & ")"  & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_OilBoilerCount  & " (" & formatCurrency(Patch_OilBoilerCost ) & ")" & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_WallHeaterCount & " (" & formatCurrency(Patch_WallHeaterCost ) &  ")" & "</td>"
                    ContractSummary= ContractSummary & "<td class=""heading2"" style='text-align:right;'>" &  Patch_Overdue   & "</td>"
                    ContractSummary= ContractSummary & "</tr>"
                    ContractSummary= ContractSummary & ContractSummaryDev
                    
                    ' now set the dev rows empty for next patch
                    ContractSummaryDev = ""
                    ContractSummaryDevPrev=ContractSummaryDev
                    Patch_PropertyCount 	= 0
                    Patch_TraditionalBoilerCount = 0
                    Patch_TraditionalBoilerCost = 0
                    Patch_FireCount 		= 0
                    Patch_FireCost 		= 0
                    Patch_WaterHeaterMultipointCount = 0
                    Patch_WaterHeaterMultipointCost = 0
                    Patch_CookHobCount		= 0
                    Patch_CookHobCost =0
                    Patch_CombiBoilerCount  	= 0
                    Patch_CombiBoilerCost = 0
                    Patch_OilBoilerCount  	= 0
                    Patch_OilBoilerCost =0	
                    Patch_WallHeaterCount = 0
                    Patch_WallHeaterCost =0
                    Patch_Overdue 	= 0
           
                        
           end if   
           
  
            
           Total_PropertyCount	= Total_PropertyCount + Cint(Scheme_PropertyCount)
           Total_TraditionalBoilerCount  = Total_TraditionalBoilerCount  + Scheme_TraditionalBoilerCount 
           Total_TraditionalBoilerCost = Total_TraditionalBoilerCost + Scheme_TraditionalBoilerCost
           Total_FireCount	= Total_FireCount  + Scheme_FireCount
           Total_FireCost = Total_FireCost +	Scheme_FireCost
           Total_WaterHeaterMultipointCount = Total_WaterHeaterMultipointCount + Scheme_WaterHeaterMultipointCount
           Total_WaterHeaterMultipointCost = Total_WaterHeaterMultipointCost + Scheme_WaterHeaterMultipointCost 
           Total_CookHobCount 	= Total_CookHobCount + Scheme_CookHobCount
           Total_CookHobCost = Total_CookHobCost + Scheme_CookHobCost
           Total_CombiBoilerCount	= Total_CombiBoilerCount +  Scheme_CombiBoilerCount
           Total_CombiBoilerCost = Total_CombiBoilerCost + Scheme_CombiBoilerCost
           Total_OilBoilerCount = Total_OilBoilerCount + SchemeOilBoilerCount
           Total_OilBoilerCost = Total_OilBoilerCost + Scheme_OilBoilerCost 			
           Total_WallHeaterCount	= Total_WallHeaterCount + Scheme_WallHeaterCount
           Total_WallHeaterCost = Total_WallHeaterCost + Scheme_WallHeaterCost
           Total_Overdue 	= Total_Overdue + Scheme_Overdue     
             
           counter=counter+1    
           dbrs.movenext()
	       If dbrs.EOF Then Exit for
    	Next
    	
    	ContractSummaryTotal=ContractSummaryTotal & "<tr >"
    	ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:center;'>Total</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>"&  Total_PropertyCount & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_FireCount & " (" & formatcurrency(Total_FireCost) & ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_WaterHeaterMultipointCount  & " (" & formatcurrency(Total_WaterHeaterMultipointCost) & ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_CookHobCount  & " (" & formatcurrency(Total_CookHobCost + Patch_CookHobCost) & ")"  & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_TraditionalBoilerCount   & " (" & formatcurrency(Total_TraditionalBoilerCost)  & ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_CombiBoilerCount   & " (" & formatcurrency(Total_CombiBoilerCost) & ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_OilBoilerCount  & " (" & formatCurrency(Total_OilBoilerCost) & ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_WallHeaterCount  & " (" & formatCurrency(Total_WallHeaterCost) &  ")" & "</td>"
        ContractSummaryTotal=ContractSummaryTotal &  "<td class=""heading2"" style='text-align:right;'>" &  Total_Overdue & "</td>"
        ContractSummaryTotal=ContractSummaryTotal & "</tr>"
                
   	    ContractSummary= ContractSummary & "<tr><td class=""dottedline"" colspan=10></td></tr>"
   	    ContractSummary=ContractSummary & ContractSummaryTotal
        ContractSummary=ContractSummary & "</table></div>"
   else
        ContractSummary="No data exist for this contractor"  
   end if
 End Function
	OpenDB()
		GetPostedData()
		GetContractDetail()
	CloseDB()
		
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Summary</title></head>
<script language="javascript" type="text/javascript">
function ReturnData(){
	parent.document.getElementById("content_heading").innerHTML = Heading.innerHTML;
	parent.document.getElementById("content_body").innerHTML=Summary.innerHTML;
	return;
}
</script>
<body onLoad="ReturnData()">
<div id="Heading"><%=Heading%></div>
<div id="Summary"><%=ContractSummary%></div>
</body>
</html>