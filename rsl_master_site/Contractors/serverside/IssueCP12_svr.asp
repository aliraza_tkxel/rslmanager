<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Includes/Functions/FormCollection.asp" -->
<%
Dim dirtyFlag ,dirtymessage,AppliaceString,repairhistoryid,notes,appliancetypeid,LoaderString,appliancetype
Dim TheAction,ID,orgid 

Function GetPostedData()
    TheAction = Request("hid_Action")
    ID = Request.Form.Item("hid_Propertyid")
    repairhistoryid=Request.Form.Item("hid_repairhistoryid")	
    notes=Request.Form.Item("txt_NOTES")
    orgid=Session("ORGID")
    appliancetypeid=Request.Form.Item("hid_appliancetypeid")
    appliancetype=Request.Form.Item("hid_appliancetype")
End Function

Function Rebuild()
	
	SQL = " SELECT AT.APPLIANCETYPEID,AT.APPLIANCETYPE AS APPLIANCETYPE,CP12NUMBER,ISSUEDATE,CP12DOCUMENT, ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'') AS ISSUEDBY "&_
	      " FROM GS_PROPERTY_APPLIANCE GS " &_
	      "     INNER JOIN GS_APPLIANCE_TYPE AT ON AT.APPLIANCETYPEID=GS.APPLIANCETYPEID " &_ 
	      "     LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = GS.CP12ISSUEDBY " &_
	      " WHERE GS.PROPERTYID= '" & ID & "' "&_
	      "   AND GS.ISSUEDATE>DATEADD(MM,-1,GETDATE()) "
	
	Call OpenRs(rsSet,SQL)
	if(not rsSet.eof) then
        while NOT rsSet.EOF 
            AppliaceString= AppliaceString & "<tr onmouseover=""style.backgroundColor='#004174';"" onmouseout=""style.backgroundColor='transparent';"" >"  
            AppliaceString= AppliaceString & "<td style='width:144px;'>" & rsSet("APPLIANCETYPE") & "</td>"
            AppliaceString= AppliaceString & "<td style='width:124px;'>" & rsSet("CP12NUMBER") & "</td>"
            AppliaceString= AppliaceString & "<td style='width:71px;'>" & rsSet("ISSUEDATE") & "</td>"
            AppliaceString= AppliaceString & "<td style='width:100px;'>" & rsSet("ISSUEDBY") & "</td>"
            if(isnull(rsSet("CP12DOCUMENT"))) then
                AppliaceString= AppliaceString & "<td>&nbsp;</td>"
            else
                 AppliaceString= AppliaceString & "<td class='linkstyle' onclick=""DisplayDoc( '" & Propertyid & "'," & rsSet("APPLIANCETYPEID") & ")""><img alt='' src='/myImages/paper_clip.gif'/></td>"
            end if 
            AppliaceString= AppliaceString & "</tr>"
           	rsSet.moveNext
		wend    
    else
       AppliaceString = "<tr><td colspan=5 align=center>no items found</td></tr>" 
    end if    
    CloseRs(rsSet)
End Function

Function CheckUpdateRecord()
	' check that the cp12 issue date is not earlier than the last appointment date
SQL = 	"SELECT GSJ.APPOINTMENTDATE " &_
	"FROM S_SCOPE S   " &_
	" 	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON SS.SCOPEID=S.SCOPEID  " &_
	" 	INNER JOIN PDR_DEVELOPMENT PD ON PD.PATCHID=SS.PATCHID AND (PD.DEVELOPMENTID=SS.DEVELOPMENTID OR SS.DEVELOPMENTID IS NULL)  " &_
	" 	INNER JOIN P__PROPERTY PP ON PP.DEVELOPMENTID=PD.DEVELOPMENTID  " &_
	" 	INNER JOIN  C_JOURNAL CJ ON CJ.PROPERTYID=PP.PROPERTYID 	 " &_
	" 	INNER JOIN C_REPAIR CR ON CR.JOURNALID=CJ.JOURNALID  AND CR.SCOPEID IS NOT NULL 	 " &_
	" 	INNER JOIN C_REPAIRTOGASSERVICING GSJ ON GSJ.REPAIRHISTORYID=CR.REPAIRHISTORYID 		 " &_
	" WHERE s.orgid = "  & Session("ORGID")  & " and  pp.propertyid = '" & ID  & "' and CR.REPAIRHISTORYID=(SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID=CJ.JOURNALID) " 
	Call OpenRs(rsCP12dateCheck, SQL)
		if not rsCP12dateCheck.eof then
		    if(Isnull(rsCP12dateCheck("APPOINTMENTDATE"))=false) then
                    appointmentdate=FormatDateTime(rsCP12dateCheck("APPOINTMENTDATE"),2)
                    IssueDate=FormatDateTime(Request.Form("txt_ISSUEDATE"),2)
			        if CDate(appointmentdate) > CDate(IssueDate) then
				        dirtyflag = 1
				        dirtymessage = "The CP12 issue date  has to be greater or equal to the Appointment Date."
			  end if
			end if
		end if 
	CloseRs(rsCP12dateCheck)


	' CHECK THAT THE CP 12 NUMBER DOES NOT EXIST
	SQL = "SELECT COUNT(1)  as checkcnt FROM GS_PROPERTY_APPLIANCE WHERE PROPERTYID <> '" & ID  & "' AND CP12NUMBER = '" & Request.Form("txt_CP12NUMBER") &"'"
	Call OpenRs(rsCP12Check, SQL)
		if cInt(rsCP12Check("checkcnt")) > 0 then
			dirtyFlag = 1
			dirtymessage = "The CP12 Number exists against another property please enter the correct number."
		end if
	CloseRs(rsCP12Check)
	if(TheAction="CHECK") then
	response.Write("fsd")
	    InsertRepairJournal()
	end if    
End Function
Function InsertRepairJournal()
    dim dbrs,dbcmd
    set dbrs=server.createobject("ADODB.Recordset")
    set dbcmd= server.createobject("ADODB.Command")
    
    dbcmd.ActiveConnection=RSL_CONNECTION_STRING
    'RESPONSE.Write (dirtyflag)
    if(dirtyflag <> 1) then
            SQLREPAIR =" SELECT CR.JOURNALID,CR.ITEMSTATUSID,CR.ITEMACTIONID,CR.TITLE,CR.SCOPEID,ISNULL(CRP.APPOINTMENTID,0) AS APPOINTMENTID  "&_
                       "  FROM C_REPAIR CR "&_
                       "   LEFT JOIN C_REPAIRTOGASSERVICING CRP ON CRP.REPAIRHISTORYID=CR.REPAIRHISTORYID "&_
                       "  WHERE CR.REPAIRHISTORYID= "& repairhistoryid  
                      
           Call OpenRs(rsSet, SQLREPAIR)        
           
           if(not rsSet.eof) then
                
                      SQL=	" SET NOCOUNT ON;INSERT INTO C_REPAIR (JOURNALID, LASTACTIONDATE, ITEMSTATUSID, ITEMACTIONID, ITEMDETAILID, "&_
				            "	  CONTRACTORID,TITLE,SCOPEID,NOTES)	"&_ 
		                    "    VALUES(" & rsSet("JOURNALID") & ",GETDATE()," & rsSet("ITEMSTATUSID") & "," & rsSet("ITEMACTIONID")& ",NULL," & orgid & ",'" & appliancetype & "'," & rsSet("SCOPEID") & ",'"& notes &"');SELECT @@IDENTITY AS REPAIRHISTORYID; "    
                                   
                       dbcmd.CommandText=SQL
                       set dbrs=dbcmd.execute
                
                 if(not dbrs.eof) then	
                    
                    SQLGS = "  INSERT INTO C_REPAIRTOGASSERVICING (REPAIRHISTORYID, APPOINTMENTID,APPOINTMENTDATE) " &_
                          "   VALUES (" & dbrs("REPAIRHISTORYID") & ",9,NULL)"
                    Conn.Execute(SQLGS)
                    
                 end if
                 
                      
                 
           end if
          CloseRs(rsSet) 
    End if
End Function

Function Load()
    SQL=" SELECT ISSUEDATE,CP12NUMBER,APPLIANCETYPEID,NOTES "&_
        " FROM GS_PROPERTY_APPLIANCE "&_ 
        " WHERE PROPERTYID='" &ID &"'" &_
        " AND APPLIANCETYPEID=" & appliancetypeid
    	Call OpenRs(rsLoader, SQL)
	 LoaderString = ""
	 if (NOT rsLoader.EOF) then    
	    SQLSELECT="GS_APPLIANCE_TYPE GA INNER JOIN GS_PROPERTY_APPLIANCE GP ON GP.APPLIANCETYPEID=GA.APPLIANCETYPEID WHERE PROPERTYID='" & ID & "' AND GP.APPLIANCETYPEID=" & appliancetypeid
	    Call BuildSelect(lstAppliance, "sel_Appliance", SQLSELECT ,"GA.APPLIANCETYPEID, GA.APPLIANCETYPE", "GA.APPLIANCETYPEID", "Please Select", appliancetypeid, NULL, "textbox200", " style='width:300px;margin-left:49px;' ")
	    LoaderString = LoaderString & "parent.document.getElementById(""sel_Appliance"").outerHTML=""" & lstAppliance  & """;"
	    LoaderString=LoaderString & "parent.RSLFORM.txt_ISSUEDATE.value = """ & rsLoader("ISSUEDATE") & """;"
	    LoaderString=LoaderString & "parent.RSLFORM.txt_CP12NUMBER.value = """ & rsLoader("CP12NUMBER") & """;"
	    LoaderString=LoaderString & "parent.RSLFORM.txt_NOTES.value = """ & rsLoader("NOTES") & """;"
	    LoaderString=LoaderString & "parent.RSLFORM.btn_save.value = ""Amend"";"
	    LoaderString=LoaderString & "parent.RSLFORM.btn_save.onclick = function() {parent.Amend();}"
	 end if
	 CloseRs(rsLoader)
End Function
GetPostedData()
OpenDB()
Select Case TheAction 
	Case "CHECK"   CheckUpdateRecord()
	Case "REBUILD" Rebuild()
	Case "LOAD" Load()
	Case "AMEND" CheckUpdateRecord()
End Select
CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head> <title></title></head>
<script language="javascript"  type="text/javascript">
function sendData(){
    <%if TheAction="CHECK" or TheAction="AMEND"  then%>
	    //alert("<%=TheAction %>");
	    <% if dirtyFlag = 1 then%>
		    parent.alert("<%=dirtymessage %>");
		<%else %>
		    parent.RSLFORM.sel_Appliance.disabled=true;
		    parent.RSLFORM.btn_save.disabled=true;
		    
		    parent.Add_CP12();
		<% end if %>    
	<%elseif TheAction="REBUILD" then %>	
	    parent.detail.innerHTML = Reloader.innerHTML; 
	<%elseif TheAction="LOAD" then %>  
	     <%=LoaderString%>     
	<% end if %>
	return;
}
</script>
<body onLoad="sendData()">
    <div id="Reloader">
        <table cellspacing="0" cellpadding="3" border="1" style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc);width:100%;' slcolor='' hlcolor='STEELBLUE'>
            <%=AppliaceString%>
        <tr><td style="height:100%;" colspan="4"></td></tr>
        </table>
    </div>
</body>
</html>