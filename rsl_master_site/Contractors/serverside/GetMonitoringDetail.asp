<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim Heading,orgid,patchid,developmentid,address,postcode,renewal
	Dim monitoringid,mon_status,MonitoringDetailData
	
	'-------------------------------------------------------------------
	' CHECK FORM ELEMENT AND SEE IF IT IS NULL
	'-------------------------------------------------------------------	
	Function nulltest(str_isnull)

		If isnull(str_isnull) OR str_isnull = "" Then
			str_isnull = Null
		End If
		nulltest = str_isnull

	End Function

    Function GetPostedData()
		'This function will get all the posted data
		orgid=nulltest(Request.Item("sel_ORGANISATION"))
		patchid=nulltest(Request.Form.Item("sel_PATCH"))
		developmentid=nulltest(Request.Form.Item("sel_SCHEME"))
        monitoringid=Request.Item("hid_mon_menu") 
        mon_status=Request.Form("mon_status")
	End Function
		

	Function DebugRquests()
		If Request.Form <> "" Then
			For Each Item In Request.Form
				rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
			Next
		 End If	
		 If Request.QueryString <> "" Then
			For Each Item In Request.QueryString
				rw "" & Item & " : " & Request(Item) & "<BR>" & vbcrlf
			Next
		 End If  
	End Function

	'Call DebugRquests()

	Function GetPropertyCP12StatusDetail()
	    
	Dim CP12DetailData   

	Select case mon_status
	
		case "0"
			Heading =  "Expired "  
			filterid = 0   
	   case "1"
			        Heading="Due in < 1 week " 
				filterid = 6
	   case "1-2"
		  Heading=  "Due in 1 - 2 weeks "         
				filterid = 5        
	   case "2-3"
		  Heading=  "Due in 2 - 3  weeks " 
				filterid = 4
	   case "3-6"
		  Heading=  "Due in 3 - 6  months "         
				filterid = 3        
	   case "6-12"		        
	             Heading =  "Due 6 - 12  months "  	
				filterid = 2  	
	   	case "12"		        
	     Heading =  "Due in > 12  months "	        	
				filterid = 1
	   case else
				Heading="Issue Date Missing " 
				filterid = 7  
				  
	End Select	 
	 
	 
	 set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GA_CP12_RECORDSET"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
		spUser.Parameters.Append spUser.CreateParameter("@PERIODFILTER", 3, 1,4,filterid)
	set rsSet = spUser.Execute
	
   CP12DetailData="<div style=""height:300px;overflow:scroll;width:100%;""><table style='border-collapse:collapse;'  cellspacing=4 cellpadding=2>"
   CP12DetailData=CP12DetailData & "<tr class=""heading2"">"
			CP12DetailData=CP12DetailData & "<td></td>"
   CP12DetailData=CP12DetailData & "<td >Address</td>"
    CP12DetailData=CP12DetailData & "<td>Appliance Type</td>"
			CP12DetailData=CP12DetailData & "<td>Expiry Date</td>"
   CP12DetailData=CP12DetailData & "<td>Stage</td>"
   CP12DetailData=CP12DetailData & "</tr>" 
			CP12DetailData=CP12DetailData & "<tr><td class=""dottedline"" colspan=4></td></tr>"
			
			If(NOT rsSet.eof) Then
				i = 1
    While(not rsSet.eof) 
        CP12DetailData=CP12DetailData & "<tr style='color:#133e71;'>"
		CP12DetailData=CP12DetailData & "<td>" & i & "</td>"
        CP12DetailData=CP12DetailData & "<td title=""Contract Name: " & rsSet("CONTRACTNAME") & """>" & rsSet("PROPERTYADDRESS") &"</td>"
        CP12DetailData=CP12DetailData & "<td>" & rsSet("APPLIANCETYPE") &"</td>"
        CP12DetailData=CP12DetailData & "<td>" & rsSet("CURR_CERT_DATE") &"</td>"
        CP12DetailData=CP12DetailData & "<td>" & rsSet("STAGE") &"</td>"
        CP12DetailData=CP12DetailData & "</tr>"
        rsSet.movenext()
				i = i + 1
    wend     
			Else
	    CP12DetailData=CP12DetailData & "<tr>"
				CP12DetailData=CP12DetailData & "<td colspan=4>No data exit for selected CP12 status</td>"
	    CP12DetailData=CP12DetailData & "</tr>"
			End If
			
			CP12DetailData=CP12DetailData & "<tr><td class=""dottedline"" colspan=4></td></tr>"   
     CP12DetailData= CP12DetailData & "</table></div>"
			
	set sprocDisplayIagManagerUsers = nothing
 
     GetPropertyCP12StatusDetail=CP12DetailData

   End Function
    
    Function GetAppointmentStatusDetail()
	    
	Dim AppointmentDetailData   
 
	 select case mon_status
	 case "1"
	   Heading = " Due in < 1 week "         
	   filterid = 3
	 case "1-1"	 
		Heading = " Due in 1 month "
		filterid = 2   
     case "2"
	   Heading = " Due in > 1 month "         
	    filterid = 1
	 case else
		    Heading =  "Overdue"      
		filterid = 0   
   end select
   
    set spUser = Server.CreateObject("ADODB.Command")
		spUser.ActiveConnection = RSL_CONNECTION_STRING
		spUser.CommandText = "GA_APPOINTMENT_RECORDSET"
		spUser.CommandType = 4
		spUser.CommandTimeout = 0
		spUser.Prepared = true
		spUser.Parameters.Append spUser.CreateParameter("@DEVELOPMENTID", 3, 1,4,developmentid)
		spUser.Parameters.Append spUser.CreateParameter("@PATCHID", 3, 1,4,patchid)
		spUser.Parameters.Append spUser.CreateParameter("@ORGID", 3, 1,4,orgid)
		spUser.Parameters.Append spUser.CreateParameter("@PERIODFILTER", 3, 1,4,filterid)
	set rsSet = spUser.Execute
	
   AppointmentDetailData="<div style=""height:322px;overflow:scroll;width:100%;""><table style='border-collapse:collapse;'  cellspacing=4 cellpadding=2>"
   AppointmentDetailData=AppointmentDetailData & "<tr class=""heading2"">"
		AppointmentDetailData=AppointmentDetailData & "<td> </td>"
   AppointmentDetailData=AppointmentDetailData & "<td> Address </td>"
   AppointmentDetailData=AppointmentDetailData & "<td> Appointment Date </td>"
   AppointmentDetailData=AppointmentDetailData & "<td> Stage </td>"
   AppointmentDetailData=AppointmentDetailData & "</tr >" 
		AppointmentDetailData=AppointmentDetailData & "<tr><td class=""dottedline"" colspan=4></td></tr>" 
                                                
		If(NOT rsSet.eof) Then
			i = 1
    While(not rsSet.eof)    
        AppointmentDetailData=AppointmentDetailData & "<tr style='color:#133e71;'>"
	AppointmentDetailData=AppointmentDetailData & "<td>" & i & "</td>"
        AppointmentDetailData=AppointmentDetailData & "<td title=""Contract Name: " & rsSet("CONTRACTNAME") & """>" & rsSet("PROPERTYADDRESS") &"</td>"
	AppointmentDetailData=AppointmentDetailData & "<td>" & rsSet("APPOINTMENT_DATE") &"</td>"
        AppointmentDetailData=AppointmentDetailData & "<td>" & rsSet("STAGE") &"</td>"
        AppointmentDetailData=AppointmentDetailData & "</tr >"
        rsSet.movenext()
			i = i + 1
    wend      
		Else
	    AppointmentDetailData=AppointmentDetailData & "<tr  >"
			AppointmentDetailData=AppointmentDetailData & "<td colspan=4>No data exit for selected appointment status</td>"
	    AppointmentDetailData=AppointmentDetailData & "</tr >"
		End If
			
		AppointmentDetailData=AppointmentDetailData & "<tr><td class=""dottedline"" colspan=4></td></tr>"   
     AppointmentDetailData= AppointmentDetailData & "</table></div>"
			
	set sprocDisplayIagManagerUsers = nothing

     GetAppointmentStatusDetail=AppointmentDetailData
	
    End Function
    
	Call OpenDB()
		Call GetPostedData()
	   If(monitoringid=1) Then
            MonitoringDetailData=GetPropertyCP12StatusDetail()
        Else     
            MonitoringDetailData=GetAppointmentStatusDetail()
      End If
	Call CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Manage Appointment</title></head>
<script language="javascript" type="text/javascript">
function ReturnData()
{
    parent.document.getElementById("MonitoringSubHead").innerHTML = Heading.innerHTML;
    parent.document.getElementById("subdatabody").innerHTML= MonitoringDetail.innerHTML;
	return;
}
</script>
<body onload="ReturnData()">
<form name = "thisForm" method="post" action="">
<div id="Heading"><%=Heading%></div>
<div  style=" text-align:center;" id="MonitoringDetail"><%=MonitoringDetailData%></div>
</form>
</body>
</html>