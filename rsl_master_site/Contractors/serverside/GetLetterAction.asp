<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	Dim lst, int_source, page_source, sel_width
	
	int_source = Request("sel_ITEMACTIONID")
	if int_source = "" then int_source = -1 end If
	page_source = Request("pagesource")
	LetterCode=Request("hid_LETTERCODE")

	' IF CALLED FROM POP UP SELECT WIDTH MUST BE SMALLER
	
	OpenDB()
	build_select()
	
	Function build_select()
		
		sql = "SELECT LETTERID, LETTERDESC FROM C_STANDARDLETTERS WHERE LETTERCODE IN ('AP1','AP2','APR1','APR2') AND SUBITEMID = " & int_source
		
		response.Write(sql)
		
		Call OpenRs(rsSet, sql)
		
		lst = "<select name=sel_LETTER class='textbox200' style='margin-left:15px;'>"
		lst = lst & "<option value=''>Please Select...</option>"
		int_lst_record = 0

		While (NOT rsSet.EOF)
			
			lst = lst & "<option value='" & rsSet(0) & "'>" & rsSet(1) & "</option>"
			rsSet.MoveNext()
			int_lst_record = int_lst_record + 1
		
		Wend
		
		CloseRs(rsSet)				
		lst = lst & "</select>" 
				
		If int_lst_record = 0 then
			lst = "<select name=sel_LETTER class='textbox200' style='margin-left:15px;' disabled><option value=''> No associated letters found</option></select>"
		End If			

	End Function
	

%>
<html>
<head></head>
<script language="javascript">
function ReturnData(){
	
		parent.RSLFORM.sel_LETTER.outerHTML = "<%=lst%>";

	}
</script>
<body onload="ReturnData()">
</body>
</html>