<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="Connections/db_connection.asp" -->
<%
    Dim Propertyid,PropertyAppliance,tenancyid,appointmentdate,RepairHistoryId,username,AppliaceString,orgid,employeeid
    
	Function GetPostedData()
	    'This function will get all the posted data
        Propertyid=Request.Item("propertyid")
        RepairHistoryId=Request.Item("repairhistoryid")
        orgid=Session("ORGID")
        employeeid=Session("USERID")
    End Function
	
	
	Function ApplianceDetail()
	
	SQL=" SELECT AT.APPLIANCETYPEID,AT.APPLIANCETYPE AS APPLIANCETYPE,CP12NUMBER,GS.ISSUEDATE,CP12DOCUMENT , ISNULL(E.FIRSTNAME,'') + ' ' + ISNULL(E.LASTNAME,'') AS ISSUEDBY,GETDATE() AS CURR_DATE "&_
		" FROM GS_PROPERTY_APPLIANCE GS "&_
		"   INNER JOIN GS_APPLIANCE_TYPE AT ON AT.APPLIANCETYPEID=GS.APPLIANCETYPEID "&_ 
		"   LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = GS.CP12ISSUEDBY " &_
		" WHERE GS.PROPERTYID= '" & Propertyid & "' "&_
		"   AND DATEADD(MM,11,GS.ISSUEDATE)>=GETDATE() "
		
		Call OpenRs(rsSet, SQL)    
		
			If(NOT rsSet.eof) Then	
					
				While NOT rsSet.EOF 
								
					If(DateDiff("m",FormatDateTime(rsSet("ISSUEDATE")),FormatDateTime(rsSet("CURR_DATE")))<2) Then
						AppliaceString= AppliaceString & "<tr onmouseover=""style.backgroundColor='#004174';style.cursor='hand'"" onmouseout=""style.backgroundColor='transparent';"" onclick='LoadData(" & rsSet("APPLIANCETYPEID") & ");'>"  
					Else
						AppliaceString= AppliaceString & "<tr onmouseover=""style.backgroundColor='#004174'"" onmouseout=""style.backgroundColor='transparent';"">"  
					End If                
				
					AppliaceString= AppliaceString & "<td style='width:144px;' >" & rsSet("APPLIANCETYPE") & "</td>"
					AppliaceString= AppliaceString & "<td style='width:124px;'>" & rsSet("CP12NUMBER") & "</td>"
					AppliaceString= AppliaceString & "<td style='width:71px;'>" & rsSet("ISSUEDATE") & "</td>"
					AppliaceString= AppliaceString & "<td style='width:100px;'>" & rsSet("ISSUEDBY") & "</td>"
				
					If(isnull(rsSet("CP12DOCUMENT"))) Then
						AppliaceString= AppliaceString & "<td>&nbsp;</td>"                    
					Else
						AppliaceString= AppliaceString & "<td class='linkstyle' onclick=""DisplayDoc( '" & Propertyid & "'," & rsSet("APPLIANCETYPEID") & ")""><img alt='' src='/myImages/paper_clip.gif'/></td>"
					End If
				
					AppliaceString= AppliaceString & "</tr>"
				
				rsSet.moveNext
				Wend    
				
			End If   
		
		Call CloseRs(rsSet)  
	
	End Function
	
	Call OpenDB()
		Call GetPostedData()
		Call ApplianceDetail()	  
		SQL="GS_APPLIANCE_TYPE GA INNER JOIN GS_PROPERTY_APPLIANCE GP ON GP.APPLIANCETYPEID=GA.APPLIANCETYPEID WHERE PROPERTYID='" & Propertyid & "' AND (DATEADD(MM,1,GP.ISSUEDATE)<GETDATE() OR ISSUEDATE IS NULL)"
		Call BuildSelect(lstAppliance, "sel_Appliance", SQL ,"GA.APPLIANCETYPEID, GA.APPLIANCETYPE", "GA.APPLIANCETYPEID", "Please Select", NULL, NULL, "textbox200", " style='width:300px;margin-left:49px;' ")
	Call CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<title>Issue CP12</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<style type="text/css">
    /*CSS for filter*/
    #appliancedetail {border:1px solid 133e71;height:20px;}
    /*CSS for content*/
    #content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;margin-right:0px;padding-right:0px;}
    .customerinfotable{border:1px solid #133e71;border-collapse:collapse;} 
    .customerinfotable td{border:1px solid #133e71;vertical-align:top;}
    .date_filter img { border: none; margin:0 0 0 1; padding:0 0 0 0;}
    .fileupload {width:200px;FONT-SIZE: 10px;FONT-FAMILY: Verdana, Arial;border: 1px solid #133e71;background-color: #FFFFFF;COLOR: #133E71;}
    .linkstyle{cursor:hand;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript">
<!--
var FormFields = new Array();
	FormFields[0] = "txt_CP12NUMBER|CP12 Number|TEXT|Y"
	FormFields[1] = "sel_Appliance|Appliance|NUMBER|Y"
	FormFields[2] = "txt_ISSUEDATE|Issue Date|DATE|Y"
	FormFields[3] = "txt_NOTES|Notes|TEXT|N"

function Add_CP12()
{
	var doctype
		document.forms[0].encoding = "multipart/form-data";      
		RSLFORM.target = "_self";
		RSLFORM.action = "/DBFileUpload/uploader.aspx";
		RSLFORM.keycriteria.value="PROPERTYID='<%=Propertyid%>' AND APPLIANCETYPEID="+RSLFORM.sel_Appliance.value;
		RSLFORM.updatefields.value=" orgid=<%=orgid%>,ISSUEDATE='"+RSLFORM.txt_ISSUEDATE.value+"',CP12NUMBER='"+RSLFORM.txt_CP12NUMBER.value+"',NOTES='"+RSLFORM.txt_NOTES.value+"',CP12ISSUEDBY=<%=employeeid%>"
		
		doctype=RSLFORM.rslfile.value;
		if(doctype.length>0) 
		{
		  RSLFORM.updatefields.value=RSLFORM.updatefields.value+",DOCUMENTTYPE='"+ doctype.substring(doctype.length-3)+"'"; 
		}
	RSLFORM.submit();	
	return;
}	


function DataCheck()
{
	if (!checkForm()) return false;
	//else
	//alert('validation met. information will not save this minute')
	//return false;
	
	RSLFORM.hid_appliancetype.value=RSLFORM.sel_Appliance.options[RSLFORM.sel_Appliance.selectedIndex].text;
	RSLFORM.hid_Action.value="CHECK";
	RSLFORM.target = "ServerFrame";
	RSLFORM.action = "../Serverside/IssueCP12_svr.asp";
	document.forms[0].encoding = "application/x-www-form-urlencoded";
	RSLFORM.submit();	
	return;
}


function Amend()
{
	RSLFORM.hid_appliancetype.value=RSLFORM.sel_Appliance.options[RSLFORM.sel_Appliance.selectedIndex].text;
	RSLFORM.hid_Action.value="AMEND";
	RSLFORM.target = "ServerFrame";
	RSLFORM.action = "../Serverside/IssueCP12_svr.asp";
	document.forms[0].encoding = "application/x-www-form-urlencoded";
	RSLFORM.submit();	
	return;
}


function Rebuild()
{
	RSLFORM.hid_Action.value="REBUILD";
	RSLFORM.target = "ServerFrame";
	RSLFORM.action = "../Serverside/IssueCP12_svr.asp";
	document.forms[0].encoding = "application/x-www-form-urlencoded";
	RSLFORM.submit();
	return;
}


function DisplayDoc	(propertyid,appliancetypeid)
{
	window.open("pCP12Document.asp?propertyid="+propertyid+"&appliancetypeid="+appliancetypeid, "_blank",null);
}


function CloseWindow()
{
	window.opener.parent.GetIssueCP12();
	window.close();
}


function LoadData(Appliancetypeid)
{
	RSLFORM.hid_appliancetypeid.value=Appliancetypeid;
	RSLFORM.hid_Action.value="LOAD";
	RSLFORM.target = "ServerFrame";
	RSLFORM.action = "../Serverside/IssueCP12_svr.asp";
	document.forms[0].encoding = "application/x-www-form-urlencoded";
	RSLFORM.submit();
	return;
}
//-->
</script>
<BODY BGCOLOR=#FFFFFF onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<form name="RSLFORM" method="post">
    <div id="main">
     <div id="content">
        <p style="margin-top:5px;margin-bottom:0px;">
            <label for="sel_Appliance" style="vertical-align:top;">Appliance:</label>
            <%=lstAppliance %><img alt="" src="/js/FVS.gif" name="img_Appliance" width="15px" height="15px" border="0" />
	    </p>
        <p style="margin-top:5px;margin-bottom:0px;" id="p_CP12NUMBER">
			<label for="txt_CP12NUMBER" style="vertical-align:top;">LGSR Number:</label> 
			<input type="text" class="textbox200" name="txt_CP12NUMBER" id="txt_CP12NUMBER" value="" style="width:170px;margin-left:155px;" /> 
			<img alt="" src="/js/FVS.gif" name="img_CP12NUMBER" width="15px" height="15px" border="0" />
        </p>
        <p class="date_filter" style="margin-top:5px;margin-bottom:0px;">
			<label for="txt_ISSUEDATE" style="vertical-align:top;">Issue Date:</label> 
			<input type="text" class="textbox200" name="txt_ISSUEDATE" id="txt_ISSUEDATE" value="" style="width:140px;margin-left:170px;" />
			<span style="margin:5 0 0 0;"><a href="javascript:;" class="iagManagerSmallBlue" onClick="YY_Calendar('txt_ISSUEDATE',190,90,'de','#FFFFFF','#133E71','YY_calendar1')" tabindex="1">
			<img alt="calender_image" width="18px" height="19px"  src="../images/cal.gif"/ ></a></span><img alt="" src="/js/FVS.gif" name="img_ISSUEDATE" width="15px" height="15px" border="0" />
		</p>
        <p style="margin-top:5px;margin-bottom:0px;">
           <label for="rslfile" style="vertical-align:top;">Attach Certificate:</label> 
           <input type="file"  class="textbox200" style="width:300px;margin-left:10px;" name="rslfile" />
        </p>
        <p style="margin-bottom:0px;margin-top:5px;">
           <label for="txt_NOTES" style="vertical-align:top;">Notes:</label>
           <textarea id="txt_NOTES" name="txt_NOTES" cols="35" rows="5" style="margin-left:72px;"></textarea>
		   <img alt="" src="/js/FVS.gif" name="img_NOTES" width="15px" height="15px" border="0" />
        </p>
        <p style="margin-top:5px;margin-left:300px;">
            <input type="button" class="RSLButton" name="btn_close" value = " Close " onClick="CloseWindow();" />
            <input type="button" class="RSLButton" name="btn_save" value=" Add " onClick="DataCheck();" />
        </p>
     </div>
     <div style="margin-top:10px;width:100%;">
        <table cellspacing='0' cellpadding='0' border='1' style='border-collapse:collapse;border-left:1px solid #133e71;border-bottom:1px solid #133e71;border-right:1px solid #133e71;width:90%;'>
          <tr bgcolor='#133e71' style='color:#ffffff'> 
                <td  style="white-space:nowrap;width:165px;"><b>Appliance</b></td>
                <td  style="white-space:nowrap;width:140px;"><b>LGSR No:</b></td>  
                <td  style="white-space:nowrap;width:90px;"><b>Issue:</b></td>
                <td  style="white-space:nowrap;width:102px;"><b>Issued By:</b></td>
                <td style="width:18px">&nbsp;</td>
           </tr>
           <tr>
                <td style=" vertical-align:top;height:100%;" colspan="5"> 
	                <div style='height:50px;overflow:auto' class='TA' id="detail"> 
	                    <table cellspacing="0" cellpadding="3" border="1" style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc);width:100%;' slcolor='' hlcolor='STEELBLUE'>
	                    <%=AppliaceString%>
	                    </table>
	                </div>
                </td>
            </tr>
        </table>
     </div>
 </div>
<input type="hidden" name="hid_Action" />
<input type="hidden" name="error_message" />
<input type="hidden" name="hid_Propertyid" value="<%=Propertyid %>" />
<input type="hidden" name="keycriteria" value="" />
<input type="hidden" name="updatefields" value="" />
<input type="hidden" name="filefield" value="CP12DOCUMENT" />
<input type="hidden" name="tablename" value="GS_PROPERTY_APPLIANCE" />
<input type="hidden" name="connectionstring" value="connRSL" />
<input type="hidden" name="hid_repairhistoryid" value="<%=RepairHistoryId%>" />
<input type="hidden" name="hid_appliancetypeid" value="" />
<input type="hidden" name="hid_appliancetype" value="" />
</form>
<div id='Calendar1' style='background-color:white;position:absolute; margin-right:100px; width:200px; height:109px; z-index:20; visibility: hidden'></div>
<iframe src="/secureframe.asp" name="ServerFrame" style="display:knone"></iframe>
<img src="/js/img/FVER.gif" width="1px" height="1px" name="FVER_Image"> 
<img src="/js/img/FVEB.gif" width="1px" height="1px" name="FVEB_Image"> 
<img src="/js/img/FVS.gif" width="1px" height="1px" name="FVS_Image"> 
<img src="/js/img/FVW.gif" width="1px" height="1px" name="FVW_Image"> 
<img src="/js/img/FVTG.gif" width="1px" height="1px" name="FVTG_Image"> 
</body>
</html>