<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!-- #include virtual="Connections/db_connection.asp" -->
<%
    Dim Propertyid,ManageAppointment,tenancyid,appointmentdate,RepairHistoryId,username,appointmentid,appointmentTime
    Dim appointmentHour,appoitmentMinutes,letterid,appointmentdatetime
    
	Function GetPostedData()
	    //This function will get all the posted data
        Propertyid=Request.Item("propertyid")
        appointmentdatetime=Request.Item("appointmentdate")
        RepairHistoryId=Request.Item("repairhistoryid")
        username=Session("FirstName") & "," & Session("LastName")
        appointmentid=Request.Item("appointmentid")
        letterid=Request.Item("Letterid")
        if(appointmentdatetime="") then
            appointmentdatetime=GetAppointmentDateTime(RepairHistoryId)
        end if
        if(appointmentdatetime<>"") then
            appointmentdate=FormatDateTime(appointmentdatetime,2)
            appointmentTime=FormatDateTime(appointmentdatetime,3)
            arraymaradian=split(appointmentTime," ")
            appointmentHour=Hour(appointmentTime)
            if(appointmentHour>12) then 
		appointmentHour=appointmentHour-12
	    end if	
            appoitmentMinutes=Minute(appointmentTime)
            if(appoitmentMinutes="0") then
                appoitmentMinutes="00"
            end if
        end if
 	End Function
	
	Function GetAppointmentDateTime(ID)
	    
	    SQL= " SELECT CRG.APPOINTMENTDATE "&_
            "  FROM C_REPAIRTOGASSERVICING CRG "&_
	        "   INNER JOIN 	C_REPAIR CR ON CR.REPAIRHISTORYID=CRG.REPAIRHISTORYID "&_
	        "   INNER JOIN ( "&_
	        "                SELECT J.JOURNALID "&_
			"            	 FROM C_REPAIR R "&_
			"           		INNER JOIN C_JOURNAL J ON J.JOURNALID=R.JOURNALID "&_
			"           	 WHERE R.REPAIRHISTORYID="& ID &_
			"           	)S ON S.JOURNALID=CR.JOURNALID 	 "&_
            " WHERE cr.REPAIRHISTORYID=( "&_
            "                           SELECT MAX(KR.REPAIRHISTORYID) "&_
			"                           FROM C_REPAIRTOGASSERVICING KRG "&_
			"                   			INNER JOIN C_REPAIR KR ON KRG.REPAIRHISTORYID=KR.REPAIRHISTORYID "&_
			"               			WHERE KR.JOURNALID=CR.JOURNALID AND KRG.APPOINTMENTDATE IS NOT NULL "&_
			"                           )	"
			 
			 
			 Call OpenRs(rsSet, SQL)
			 
			 if(not rsSet.eof) then
		         GetAppointmentDateTime=rsSet("APPOINTMENTDATE")
		     else
		        GetAppointmentDateTime=""       	       
			 end if
			 
			 CloseRs(rsSet)
	End Function
	
	Function CustomerNumber(strWord)
		dim customer_no,formatedcustomerno
	    if(Instr(strWord,",")<>0) then
	        customer_no=split(strWord,",")
	        for i=0 to ubound(customer_no)
	            if(i<>0) then
	                formatedcustomerno=formatedcustomerno &  ",CN" & characterPad(customer_no(i),"0", "l", 8)
	            else
	                formatedcustomerno= "CN" & characterPad(customer_no(i),"0", "l", 8)
	            end if    
	        next
	        CustomerNumber=formatedcustomerno
	    else
	        CustomerNumber = "CN" & characterPad(strWord,"0", "l", 8)    
	    end if    
	End Function
	
	
	Function GetCustomerInfo()
	   
	   SQL=" SELECT CT.TENANCYID AS TENANCYID, CC.CLIST AS CUSTOMERID, "&_
	       "	    CASE "&_
           "		   WHEN CT.TENANCYID IS NULL THEN 'VOID' 	"&_
           "		   ELSE CC.LIST "&_
           "	    END AS CUSTOMERNAME, ISNULL(FLATNUMBER+',','')+ISNULL(HOUSENUMBER+',','') "&_
           "        +ISNULL(ADDRESS1+',','')+ISNULL(ADDRESS2+',','') "&_
           "        +ISNULL(ADDRESS3+',','')+ISNULL(TOWNCITY+',','')+ISNULL(POSTCODE,'')  AS PROPERTYADDRESS "&_
           " FROM P__PROPERTY P "&_
           "   LEFT JOIN C_TENANCY CT ON CT.PROPERTYID=P.PROPERTYID AND CT.ENDDATE IS NULL "&_
           "   LEFT JOIN C_CUSTOMER_NAMES_GROUPED_VIEW CC ON CC.I=CT.TENANCYID "&_
           " WHERE P.PROPERTYID= '" & Propertyid & "'"

	   Call OpenRs(rsSet, SQL)
	   ManageAppointment=ManageAppointment & "<table class='customerinfotable' cellpadding=8 cellspacing=2>"
	   if(not rsSet.eof) then
	        ManageAppointment=ManageAppointment & "<tr><td>Customer:</td><td>" & rsSet("CUSTOMERNAME") & "</td></tr>"
	        ManageAppointment=ManageAppointment & "<tr><td>Address:</td><td>" & rsSet("PROPERTYADDRESS") & "</td></tr>"
	        ManageAppointment=ManageAppointment & "<tr><td>Cust No:</td><td>" & CustomerNumber(rsSet("CUSTOMERID")) & "</td></tr>"
	        ManageAppointment=ManageAppointment & "<tr><td nowrap>Tenancy Ref:</td><td>" & rsSet("TENANCYID") & "</td></tr>"
	        tenancyid=rsSet("TENANCYID")
	   end if
	   ManageAppointment=ManageAppointment & "</table>"
       CloseRs(rsSet)
	End Function
	
	Function GetActionid(ID)
	   Dim NoofNoEntry,FirstAppointment,SecondAppointment,ActionId
       NoofNoEntry=0
       FirstAppointment=0 
       SecondAppointment=0
	   SQL=" SELECT ISNULL(CRG.LETTERACTIONID,-1) AS LETTERACTIONID,ISNULL(CRG.APPOINTMENTID,-1) AS APPOINTMENTID"&_
           " FROM C_REPAIRTOGASSERVICING CRG "&_
	       "  INNER JOIN 	C_REPAIR CR ON CR.REPAIRHISTORYID=CRG.REPAIRHISTORYID "&_
	       "  INNER JOIN ( "&_
	       "              SELECT J.JOURNALID "&_
		   "		      FROM C_REPAIR R "&_
		   "         		INNER JOIN C_JOURNAL J ON J.JOURNALID=R.JOURNALID "&_
		   " 	          WHERE R.REPAIRHISTORYID=" & ID &_
		   "             )S ON S.JOURNALID=CR.JOURNALID "&_	 
	       "  LEFT JOIN C_LETTERACTION CL ON CL.ACTIONID=CRG.LETTERACTIONID ORDER BY CR.REPAIRHISTORYID "
	     
	   Call OpenRs(rsSet, SQL)
	   if(not rsSet.eof) then
	         While(not rsSet.eof)
	            if(rsSet("APPOINTMENTID")=1 AND rsSet("LETTERACTIONID")<>""  ) then
	                FirstAppointmentConfirmed=1
	               ' LetterCode="AP1"
	             elseif(NoofNoEntry=1 AND rsSet("LETTERACTIONID")<>""  AND rsSet("APPOINTMENTID")=-1 ) then
	                SecondAppointment=1
	             elseif(rsSet("APPOINTMENTID")=4 ) then   
	                NoofNoEntry=NoofNoEntry+1
	            end if                 
	           rsSet.movenext()
             wend
	   end if   
	   'Response.Write("FirstAppointmentConfirmed:"& FirstAppointmentConfirmed & "SecondAppointment:" & SecondAppointment & "NoofNoEntry:" & NoofNoEntry )
	   if(NoofNoEntry>1) then
	       ActionId="108" 
	   elseif(FirstAppointmentConfirmed=1 and SecondAppointment=1) then 
	       ActionId="107,108"  
	   else
	     ActionId="104,107,108"      
	   end if
	   GetActionid=ActionId
	   CloseRs(rsSet)    
	End Function
	
	OpenDB()
	  GetPostedData()
	  GetCustomerInfo()
	  Actionid=GetActionid(RepairHistoryid)
	  Call BuildSelect(lstAction, "sel_ITEMACTIONID", "C_LETTERACTION WHERE NATURE =47 AND ACTIONID IN(" & Actionid &")" ,"ACTIONID, DESCRIPTION", "DESCRIPTION", "Please Select", NULL, NULL, "textbox200", " style='width:198px;margin-left:40px;' onchange='action_change()' ")
   
    CloseDB()
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Page-Enter" content="revealTrans(Duration=0.000,transition=5)" />
<meta http-equiv="Page-Exit" content="revealTrans(Duration=0.000,transition=5)" />
<title>Manage Appointment</title>
<link rel="stylesheet" href="/css/RSL.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style  type="text/css" media="all">
    /*CSS for filter*/
    #customerdetail {background-color: #FFFFFF;width:250px;padding-top:10px;float:left;margin-right:3px;margin-left:0px;padding-left:0px;}
    /*CSS for content*/
    #content{border: #FFFFFF solid;border-width:1px;height:1px;padding-top:10px;margin-right:0px;padding-right:0px;}
    .customerinfotable{border:1px solid #133e71;border-collapse:collapse;} 
    .customerinfotable td{border:1px solid #133e71;vertical-align:top;}
</style>

</head>
<script type="text/javascript" language="javascript" src="/js/calendarFunctions.js"></script>
<script type="text/javascript" language="javascript" src="/js/preloader.js"></script>
<script type="text/javascript" language="javascript" src="/js/general.js"></script>
<script type="text/javascript" language="javascript" src="/js/menu.js"></script>
<script type="text/javascript" language="javascript" src="/js/FormValidation.js"></script>
<script type="text/javascript" language="javascript">
<!--

var FormFields = new Array();

FormFields[0] = "txt_APPOINTMENTDATE|Appointment Date|DATE|N"
FormFields[1] = "sel_LETTER|Document|SELECT|N"
FormFields[2] = "sel_ITEMACTIONID|Stage|SELECT|Y"
 function SetTime()
 {
    if("<%=appointmentHour%>"!="")
    {
        RSLFORM.sel_HOUR.value="<%=appointmentHour%>"
        RSLFORM.sel_MINUTES.value="<%=appoitmentMinutes %>"
    }
    else
    {
       RSLFORM.sel_HOUR.value="8"     
       RSLFORM.sel_MINUTES.value="15" 
    }   
 }
 
 function action_change()
 {

	    RSLFORM.btn_view.disabled=false;  
	    RSLFORM.txtarea_notes.disabled=false;  
	    RSLFORM.hid_LETTERTEXT.value="";
	
		
	    if(RSLFORM.sel_ITEMACTIONID.value=="107")
	    {  
	         RSLFORM.txt_APPOINTMENTDATE.disabled=false;
	         RSLFORM.sel_HOUR.disabled=false;
	         RSLFORM.sel_MINUTES.disabled=false;
	         RSLFORM.sel_MARADIAM.disabled=false;
		 
		 
	    } 
	    else if(RSLFORM.sel_ITEMACTIONID.value=="104" && (("<%=appointmentid %>"!="1") || ("<%=letterid%>"!="-1" && "<%=appointmentid %>"=="1")))
	    {  
	         RSLFORM.txt_APPOINTMENTDATE.disabled=false;
	         RSLFORM.sel_HOUR.disabled=false;
	         RSLFORM.sel_MINUTES.disabled=false;
	         RSLFORM.sel_MARADIAM.disabled=false;
	    }
	    else if(RSLFORM.sel_ITEMACTIONID.value=="108") 
	    {
	        
		RSLFORM.txt_APPOINTMENTDATE.disabled=true; 
	        RSLFORM.sel_HOUR.disabled=true;    
	        RSLFORM.sel_MINUTES.disabled=true;    
	        RSLFORM.sel_MARADIAM.disabled=true;
		RSLFORM.btn_view.disabled=true;  
		RSLFORM.txtarea_notes.disabled=true;    
	    }   	       
	    else 
	    {
	        
		RSLFORM.txt_APPOINTMENTDATE.disabled=true; 
	        RSLFORM.sel_HOUR.disabled=true;    
	        RSLFORM.sel_MINUTES.disabled=true;    
	        RSLFORM.sel_MARADIAM.disabled=true;

	    }    

		RSLFORM.action = "../Serverside/GetLetterAction.asp"
		RSLFORM.target = "ServerFrame"
		RSLFORM.submit()
	    return;
}


function open_letter(){
     var AppointmentTime
        
     AppointmentTime=RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;
	
 	if(RSLFORM.sel_ITEMACTIONID.value!="108")        
	{
		FormFields[1] = "sel_LETTER|Document|SELECT|Y"
	}
	else
	{
	 	FormFields[1] = "sel_LETTER|Document|SELECT|N"
				
	}
		if (!checkForm()) return false;			
	
		var tenancy_id = "<%=tenancyid%>"
		window.open("pView_letter.asp?tenancyid="+tenancy_id+"&letterid="+RSLFORM.sel_LETTER.value + "&DATE1=" +RSLFORM.txt_APPOINTMENTDATE.value+" "+AppointmentTime+ "&DATE2=<%=appointmentdatetime%>" , "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	
	}
function save_form(){
        var AppointmentTime
        
        AppointmentTime=RSLFORM.sel_HOUR.value+":"+RSLFORM.sel_MINUTES.value+" "+RSLFORM.sel_MARADIAM.options[RSLFORM.sel_MARADIAM.selectedIndex].text;
        
        RSLFORM.hid_APPOINTMENTTIME.value=AppointmentTime;
       
	if(RSLFORM.sel_ITEMACTIONID.value!="108")        
	{
	
		FormFields[0] = "txt_APPOINTMENTDATE|Appointment Date|DATE|Y"	
		FormFields[1] = "sel_LETTER|Document|SELECT|Y"
		
        }
	else
	{
		FormFields[0] = "txt_APPOINTMENTDATE|Appointment Date|DATE|N"	
		FormFields[1] = "sel_LETTER|Document|SELECT|N"
	}

        if (!checkForm()) return false;
	
        if(RSLFORM.hid_LETTERTEXT.value=="" && RSLFORM.sel_ITEMACTIONID.value!="108")
        {
          alert("Please attach the selected letter.")   
          return;
        }
        
        RSLFORM.btn_view.disabled=true;
        RSLFORM.btn_save.disabled=true;
        
		RSLFORM.target = "ServerFrame";
		RSLFORM.action = "../Serverside/SaveAppointmentLetters.ASP";
		RSLFORM.submit();
		//refreshParent();

}	


function refreshParent() {
        window.opener.parent.GetManageAppointment();
        window.close();
  }
//-->	
 </script>
<body bgcolor="#FFFFFF" onLoad="SetTime();" marginheight="0" leftmargin="10" topmargin="10" marginwidth="0">
<form name = "RSLFORM" method="post" action="">
   <div id="main">
    <div  id="customerdetail">
       <%=ManageAppointment %>
     </div>
     <div id="content">
        <p style="margin-top:5px;margin-bottom:0px;">
            <label for="sel_ITEMACTIONID" style="vertical-align:top;">Stage:</label>
            <%=lstAction %>
	    <img alt="" src="/js/FVS.gif" name="img_ITEMACTIONID" id="img_ITEMACTIONID" width="15px" height="15px" border="0" />
		
        </p>
        <p style="margin-top:5px;margin-bottom:0px;">
           <label for="sel_LETTER" style="vertical-align:top;">Document:</label>
           <div id="dvLetter">
           <select name="sel_LETTER" id="sel_LETTER" class="textbox200" style="margin-left:15px;">
                <option>Please select aciton</option>
           </select>
           </div>
	   <img alt="" src="/js/FVS.gif" name="img_LETTER" id="img_LETTER" width="15px" height="15px" border="0" />	
        </p>
        <p style="margin-top:5px;margin-bottom:0px;">
           <label for="txt_APPOINTMENTDATE" style="vertical-align:top;">Appointment:</label> 
           <input type="text" class="textbox200" name="txt_APPOINTMENTDATE" value="<%=appointmentdate%>" disabled="disabled" />
	   <img alt="" src="/js/FVS.gif" name="img_APPOINTMENTDATE" id="img_APPOINTMENTDATE" width="15px" height="15px" border="0" />	
        </p>
        <p style="margin-top:5px;margin-bottom:0px;">
               <label for="sel_HOUR" style="vertical-align:top;margin-right:43px;">Time:</label>  
            	<select name="sel_HOUR" class="textbox100" style="width:50px;" disabled="disabled" >
			        <option value="1">01</option>
			        <option value="2">02</option>
			        <option value="3">03</option>
			        <option value="4">04</option>
			        <option value="5">05</option>
			        <option value="6">06</option>
			        <option value="7">07</option>
			        <option value="8">08</option>
			        <option value="9">09</option>
			        <option value="10">10</option>
			        <option value="11">11</option>
			        <option value="12">12</option>
			    </select>
			    <select name="sel_MINUTES" class="textbox100" style="width:50px;margin-right:20px;" disabled="disabled" >
			        <option value="00">00</option>
			        <option value="15">15</option>
			        <option value="30">30</option>
			        <option value="45">45</option>
			    </select>
			    <select name="sel_MARADIAM" class="textbox100" style="width:50px;" disabled="disabled">
			        <option value="1">AM</option>
			        <option value="2">PM</option>
			    </select>
        </p>
        <p style="margin-bottom:0px;margin-top:5px;">
            <label for="txt_ACTIONBY">Last Action By</label>
            <input type="text" class="textbox200" name="txt_ACTIONBY" value="<%=username %>" style='width:198px;' readonly="readonly" />
        </p>
        <p style="margin-bottom:0px;margin-top:5px;">
           <label for="txtarea_notes" style="vertical-align:top;">Notes:</label>
           <textarea id="txtarea_notes" name="txtarea_notes" cols="22" rows="5" style="margin-left:40px;"></textarea>
        </p>
        <p style="margin-top:15px;margin-left:80px;">
            <input type="button" class="RSLButton" name="btn_view" value="View Letter"  onclick="open_letter()"/>
            <input type="button" class="RSLButton" name="btn_close" value = " Close " onClick="refreshParent()" />
            <input type="button" class="RSLButton" name="btn_save" value=" Save " onClick="save_form()" />
            
        </p>
     </div>
 </div>
 		<input type="hidden" name="hid_LETTERCODE" />
 		<input type="hidden" name="hid_LETTERTEXT" value=""/>
		<input type="hidden" name="hid_SIGNATURE" value=""/>
		<input type="hidden" name="hid_APPOINTMENTTIME" value="" />
		<input type="hidden" name="hid_APPOINTMENTID" value="<%=appointmentid%>" />
		<input type="hidden" name="hid_REPAIRHISTORYID" value="<%=RepairHistoryId%>"
 </form>
		<img style='visibility:hidden' src="/js/img/FVER.gif" width="1px" height="1px" name="FVER_Image">
		<img style='visibility:hidden' src="/js/img/FVEB.gif" width="1px" height="1px" name="FVEB_Image"> 
		<img style='visibility:hidden' src="/js/img/FVS.gif"width="1px" height="1px" name="FVS_Image">
		<img style='visibility:hidden' src="/js/img/FVW.gif" width="1px" height="1px" name="FVW_Image"> 
		<img style='visibility:hidden' src="/js/img/FVTG.gif" width="1px" height="1px" name="FVTG_Image"> 
 <iframe src="/secureframe.asp" name="ServerFrame" style='display:none' height="400" width="400"></iframe>
</body>
</html>