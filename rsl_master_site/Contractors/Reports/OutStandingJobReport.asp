<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start of variables declaration
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Dim CONST_PAGESIZE
		
	' Declare all of the variables that will be used in the page.
	Dim objConn			' ADO Connection Object
	Dim intPageCount	' The number of pages in the recordset.
	Dim intRecordCount	' The number of records in the recordset.
	Dim intPage			' The current page that we are on.
	Dim intRecord		' Counter used to iterate through the recordset.
	Dim intStart		' The record that we are starting on.
	Dim intFinish		' The record that we are finishing on.
	Dim nextPage
	Dim prevPage
	Dim str_data, count, my_page_size
	Dim theURL	
	Dim PageName, MaxRowSpan
	Dim contractor
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End of variables declaration
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start setting variables values
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	' Check to see if there is value in the NAV querystring.  If there
	' is, we know that the client is using the Next and/or Prev hyperlinks
	' to navigate the recordset.
	If Request.QueryString("page") = "" Then intPage = 1 Else intPage = Request.QueryString("page") End If
	CONST_PAGESIZE = 20 
	dim filterOption,filterstring
	
	filterOption=Request.QueryString("PO")
	
	if filterOption<>"" then 
		filterstring=" AND WO.ORDERID=" & filterOption
	Else
	   filterstring=""
	end if
			
	PageName = "OutStandingJobReport.asp"
	MaxRowSpan = 7
	contractor = Session("ORGID")
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End setting variables values
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start the function call to make report
	'''''''''''''''''''''''''''''''''''''''''''''''''''''
	
	Call OpenDB()
	    Call get_Outstanding_Job()
	Call CloseDB()
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''
	' End the function call to make report
	''''''''''''''''''''''''''''''''''''''''''''''''''''

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Start of function definition that gets the data and make tables
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function get_Outstanding_Job()
	
			Dim orderBy, strSQL, rsSet, intRecord ,dbcmd, Icount
						
			intRecord = 0
			str_data = ""
			
			set rsSet = Server.CreateObject("ADODB.Recordset")
			
			rsSet.ActiveConnection = RSL_CONNECTION_STRING 	
					
						
	        strSQL =    " SELECT  WO.ORDERID, WO.WOID, WO.TITLE, WS.DESCRIPTION AS WOSTATUSNAME,  " &_
	                    "		ISNULL(CONVERT(NVARCHAR, WO.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), WO.CREATIONDATE, 108) ,'') AS CREATIONDATE,  " &_
	                    "		FULLADDRESS = REPLACE(ISNULL(P.HOUSENUMBER+' ','') +  ISNULL(P.ADDRESS1,'') + ISNULL(', '+P.ADDRESS2,'') + ISNULL(', '+P.TOWNCITY,'') + ISNULL(', '+P.POSTCODE,''),',,',',')  " &_
	                    "					,CJ.JOURNALID,PI.EXPPIDATE " &_
                        " FROM P_WORKORDER WO  "&_ 
	                    "   INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID "&_
	                    "   INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS "&_
	                    "   LEFT JOIN P_WORKORDER_OVERTIME OVT ON OVT.ORDERID = WO.ORDERID AND WO.WOSTATUS <= 6 "&_ 	
	                    "   INNER JOIN P_WOTOREPAIR PWO ON PWO.WOID=WO.WOID "&_
	                    "   LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = PWO.ORDERITEMID "&_
	                    "   INNER JOIN C_JOURNAL CJ ON CJ.JOURNALID=PWO.JOURNALID "&_		    
	                    "   LEFT JOIN P__PROPERTY P ON P.PROPERTYID = CJ.PROPERTYID "&_  								
	                    "   LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = P.DEVELOPMENTID "&_   
	                    " WHERE PO.SUPPLIERID =  " & contractor & "  AND PO.ACTIVE = 1 AND PO.POSTATUS > 0 AND WO.BIRTH_NATURE<>47  " &_
	                    "	  AND OVT.OVERTIME>0 " & filterstring &_
	                    " ORDER BY convert(smalldatetime,convert(varchar,PI.EXPPIDATE,103))    asc,convert(smalldatetime,convert(varchar,PI.EXPPIDATE,108))  asc"
	                    
	         
				
			'rw strSQL
			
			rsSet.Source = strSQL
			rsSet.CursorType = 3
			rsSet.CursorLocation = 3
			rsSet.Open()
			
		 	
			
			rsSet.PageSize = CONST_PAGESIZE
			my_page_size = CONST_PAGESIZE
			' Set the PageSize, CacheSize, and populate the intPageCount and intRecordCount variables.
			rsSet.CacheSize = rsSet.PageSize
			'intPageCount = Icount
			intPageCount= rsSet.PageCount 
			intRecordCount = rsSet.RecordCount 
			
			' Sort pages
			intpage = CInt(Request("page"))
			
			
			If intpage = 0 Then intpage = 1 End If
			' Just in case we have a bad request
			If intpage > intPageCount Then intpage = intPageCount End If
			If intpage < 1 Then intpage = 1 End If
		
			nextPage = intpage + 1
			If nextPage > intPageCount Then nextPage = intPageCount	End If
			prevPage = intpage - 1
			If prevPage <= 0 Then prevPage = 1 End If
	
			' double check to make sure that you are not before the start
			' or beyond end of the recordset.  If you are beyond the end, set 
			' the current page equal to the last page of the recordset.  If you are
			' before the start, set the current page equal to the start of the recordset.
			
			If CInt(intPage) => CInt(intPageCount) Then intPage = intPageCount End If
			If CInt(intPage) <= 0 Then intPage = 1 End If
			
			' Make sure that the recordset is not empty.  If it is not, then set the 
			' AbsolutePage property and populate the intStart and the intFinish variables.
			If intRecordCount > 0 Then

				rsSet.AbsolutePage = intPage
				intStart = rsSet.AbsolutePosition
				If CInt(intPage) = CInt(intPageCount) Then
					intFinish = intRecordCount
				Else
					intFinish = intStart + (rsSet.PageSize - 1)
				End if
			End If
			
			count = 0
			
			If intRecordCount > 0 Then
				' Display the record that you are starting on and the record
				' that you are finishing on for this page by writing out the
				' values in the intStart and the intFinish variables.
				str_data = str_data & "<TBODY CLASS='CAPS'>"
					' Iterate through the recordset until we reach the end of the page
					' or the last record in the recordset.
					For intRecord = 1 to rsSet.PageSize
					
					
                        strStart =  "<TR style='cursor:hand' onClick='LoadJournal(""" & rsSet("JOURNALID") & """)'><TD>" & rsSet("ORDERID") & "</TD>" 
						strEnd =	"<TD>" &  rsSet("FULLADDRESS") &  "</TD>" &_
									"<TD > " & rsSet("TITLE") & "</TD>" &_
									"<TD>" & rsSet("WOSTATUSNAME") & "</TD>"&_ 	
									"<TD>" & rsSet("EXPPIDATE") & "</TD></TR>" 	
						str_data = str_data & strStart & "<TD>" & rsSet("WOID") & "</TD>" & strEnd					
					 
										
						count = count + 1
						CustomerNameStr = ""
						rsSet.movenext()
						If rsSet.EOF Then Exit for 

					Next
					str_data = str_data & "</TBODY>"
	
					'ensure table height is consistent with any amount of records
					fill_gaps()
					
					' links
					str_data = str_data &_
					"<TFOOT><TR><TD COLSPAN=" & MaxRowSpan & " STYLE='BORDER-TOP:2PX SOLID #133E71' ALIGN=CENTER>" &_
					"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%'><THEAD><TR><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
					"<A HREF = '" & PageName & "?page=1'><b><font color=BLUE>First</font></b></a> "  &_
					"<A HREF = '" & PageName & "?page=" & prevpage &  "'><b><font color=BLUE>Prev</font></b></a>"  &_
					" Page " & intpage & " of " & intPageCount & ". Records: " & rsSet.PageSize * (intpage - 1) + 1 & "  to " & (intPage-1)*rsSet.pagesize+count &	" of " & intRecordCount   &_
					" <A HREF='" & PageName & "?page=" & nextpage & "'><b><font color=BLUE>Next</font></b></a>"  &_ 
					" <A HREF='" & PageName & "?page=" & intPageCount & "'><b><font color=BLUE>Last</font></b></a>"  &_
					"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
					"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
					"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
			End If

			' if no teams exist inform the user
			If intRecord = 0 Then 
				str_data = "<TR><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>No records found</TD></TR>" 
				count = 1
				fill_gaps()
			End If
				
			rsSet.close()
			Set rsSet = Nothing
			
		End function
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    ' End of function definition that gets the data and make tables
	    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    
	    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    ' Start of function definition that fills the gap in table
	    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		// pads table out to keep the height consistent
		Function fill_gaps()
		
			Dim tr_num, cnt
			cnt = 0
			tr_num = my_page_size - count
			while (cnt < tr_num)
				str_data = str_data & "<TR ><TD COLSPAN=" & MaxRowSpan & " ALIGN=CENTER>&nbsp;</TD></TR>"
				cnt = cnt + 1
			wend		
		
		End Function
		
	    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	    ' End of function definition that fills the gap in table
	    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Contractor --&gt; Outstanding Jobs</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--

    function LoadJournal(JournalId)
    {
	location.href = "../CORM.asp?JournalId=" + JournalId 
    }                       
	
	function JumpPage(){
		
		iPage = document.getElementById("QuickJumpPage").value
		if (iPage != "" && !isNaN(iPage))
			location.href = "OutStandingJobReport.asp?page="+iPage+"&PO="+document.getElementById("txt_PO").value
		else
			document.getElementById("QuickJumpPage").value = "" 
		}	

	function click_go()
	{
        if(document.getElementById("txt_PO").value!="") 
        {
        	location.href = "OutStandingJobReport.asp?page=1&PO="+document.getElementById("txt_PO").value
        }		
        else
        {
           location.href = "OutStandingJobReport.asp?page=1"
        }
	}

// -->
</SCRIPT>
<!-- End Preload Script -->
<BODY BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(1);preloadImages()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0"><!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name=RSLFORM method=post ID="Form1">
<table>
	<tr>
	
		<td>Purchase Order: </td>
		<td><input type="text" class="textbox100" id="txt_PO" name="txt_PO" maxlength="10"/> </td>
        <td><input id="btn_Search" class="RSLButton" value="Search" type="button" onclick="click_go();"  /></td> 
	
	</tr>
</table>
 <TABLE WIDTH=750 CELLPADDING=1 CELLSPACING=1 STYLE="BORDER-COLLAPSE:COLLAPSE;behavior:url(/Includes/Tables/tablehl.htc)" slcolor='' hlcolor=STEELBLUE  BORDER=1 ID="Table1">
    <THEAD>
		<TR> 
			<td width="100PX">&nbsp;<b>Purchase Order</b></td> 
			<TD WIDTH=75PX>&nbsp;<B>Work Order</B></TD>
			<TD WIDTH=350PX>&nbsp;<B>Address</B></TD>
			<TD WIDTH=120PX>&nbsp;<B>Title</B>&nbsp;</TD>
			<TD WIDTH=70PX>&nbsp;<B>Status</B>&nbsp;</TD>
			<TD WIDTH=120PX>&nbsp;<B>Exp Del Date</B>&nbsp;</TD>
		</TR>
		
    </THEAD> 
    <TR STYLE='HEIGHT:3PX'> 
      <TD COLSPAN=6 ALIGN="CENTER" STYLE='BORDER-bottom:2PX SOLID #133E71'></TD>
    </TR>
    <%=str_data%> 
 
  </TABLE>
  
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe name=frm_team width=400px height=400px style='display:none' src="/SecureFrame.asp" ></iframe>
</BODY>
</HTML>
						
