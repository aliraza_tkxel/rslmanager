<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	//THIS PAGE BUILDS THE WORK ORDER JOURNAL FOR TYHE SUPPLIER CRM
	
	//LAST MODIFIED BY ZANFAR ALI 15 JUNE 2004 AT 17:15 HOURS
	//WORK DONE: TOTAL REWRITE TO TAKE INTO ACCOUNT NEW WORK ORDER FUNCTIONALITY.
	
	Dim cnt, customer_id, str_journal_table, status_sql, theURL
	Dim mypage, orderBy, str_table_bottom	
	
	//HOLDS THE ID OF THE SELECTED SUPPLIER
	CompanyID = Session("ORGID")
	
	//STORES THE CURRENT OPEN WORK ORDER VALUE, WHICHIS CONTINAULLY ROTATED WITH THE MASTER HOLDING CRM PAGE
	CurrentWorkOrder = Request("CWO")
	// BIUILD FILTER SQL STRING
	if request("filterid") = "" Then
		status_sql = ""
	Else
		status_sql = " AND WS.ITEMSTATUSID = " & request("filterid")
	END IF

	FilterWO = Replace(Request("WO"), "'", "''")
	if FilterWO <> "" AND isNumeric(FilterWO) Then
		status_sql = status_sql & " AND WO.WOID = " & Clng(FilterWO) & " " 
	END IF
		
	OpenDB()
	build_work_order()
	CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	Function build_work_order()
		//THIS PART BUILDS THE MAIN WORK ORDER ROWS
		//IT JOINS THE P_WORKORDERENTITY AND P_WORKORDERMODULE TABLES TO GET RESPECTIVE IMAGES
		//THE FOLLOWING ARE RULES WHICH DEFINE HOW THIS SHOULD WORK.
		//IF THE DEVELOPMENTID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS SCHEME WIDE 
		//IF THE BLOCKID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS BLOCK WIDE
		//IF THE PROPERTYID IS NOT NULL AND THE OTHER TWO ARE NULL THEN THE WORKORDER IS PROPERTY SPECIFIC
		cnt = 0
		PageName = "iRepairJournal.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "PO.PODATE  DESC"
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001
		
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CInt(mypage)
		end if
			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 8

		strSQL = "SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WOM.BIRTH_MODULE_IMAGE, WOM.BIRTH_MODULE_DESCRIPTION, WOE.BIRTH_ENTITY_IMAGE, WOE.BIRTH_ENTITY_DESCRIPTION, " &_
				"N.DISPLAYIMAGE, WO.ORDERID, WO.WOID, WO.TITLE, WS.DESCRIPTION AS WOSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, WO.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), WO.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
				"FULLADDRESS = CASE " &_
				"WHEN B.BLOCKID IS NOT NULL THEN 'Block: ' + B.BLOCKNAME + ' Development: ' + D.DEVELOPMENTNAME " &_
				"WHEN D.DEVELOPMENTID IS NOT NULL THEN ' Development: ' + D.DEVELOPMENTNAME " &_
				"ELSE P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE " &_
				"END, isnull(OVT.OVERTIME,0) as OVERTIME " &_
				"FROM P_WORKORDER WO " &_
				"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = WO.PROPERTYID " &_									
				"LEFT JOIN PDR_DEVELOPMENT D ON D.DEVELOPMENTID = WO.DEVELOPMENTID " &_									
				"LEFT JOIN P_BLOCK B ON B.BLOCKID = WO.BLOCKID " &_													
				"INNER JOIN F_PURCHASEORDER PO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN C_STATUS WS ON WS.ITEMSTATUSID = WO.WOSTATUS " &_
				"INNER JOIN P_WORKORDERENTITY WOE ON WOE.BIRTH_ENTITY_ID = WO.BIRTH_ENTITY " &_				
				"INNER JOIN P_WORKORDERMODULE WOM ON WOM.BIRTH_MODULE_ID = WO.BIRTH_MODULE " &_								
				"INNER JOIN C_NATURE N ON N.ITEMNATUREID = WO.BIRTH_NATURE " &_												
				"LEFT JOIN ( " &_	
				"			SELECT 	COUNT(PI.ORDERID) AS OVERTIME, PI.ORDERID " &_	
				"			FROM	F_PURCHASEITEM PI " &_	
				"				INNER JOIN P_WOTOREPAIR WOTO ON WOTO.ORDERITEMID = PI.ORDERITEMID " &_	
				"				INNER JOIN C_JOURNAL J ON J.JOURNALID = WOTO.JOURNALID  " &_	
				"				INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_	
				"				INNER JOIN R_ITEMDETAIL IDT ON IDT.ITEMDETAILID = R.ITEMDETAILID " &_	
				"				INNER JOIN R_PRIORITY PRI ON PRI.PRIORITYID = IDT.PRIORITY " &_	
				"				INNER JOIN p_WORKORDER WO ON WO.ORDERID = PI.ORDERID " &_	
				"			WHERE	DATEADD(S,PRI.ESTTIME_SEC,J.CREATIONDATE) < GETDATE() " &_	 
				"				AND R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID)	 " &_	
				"				AND J.CURRENTITEMSTATUSID IN (2,6)	 " &_	
				"			GROUP BY PI.ORDERID " &_	
				"		  ) OVT ON OVT.ORDERID = WO.ORDERID	 " &_	
				"WHERE PO.SUPPLIERID = " & CompanyID & " AND PO.ACTIVE = 1 AND PO.POSTATUS > 0 " & status_sql & " " &_ 
				"ORDER BY " & Replace(orderBy, "'", "''") 
		'RW strSQL
		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
			
		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize
	
		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF then
			rsSet.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if
				
		str_journal_table = ""
		For i=1 to pagesize
			If NOT rsSet.EOF Then
			
			cnt = cnt + 1
			WOID = rsSet("WOID")
			
			'THE IMAGE TAG IS BUILT HERE, WILL ALWAYS CONTAIN THREEE IMAGES
			ImageTD = "<TD style='background-color:white' width=20 NOWRAP><img src='/myImages/Repairs/" & rsSet("BIRTH_MODULE_IMAGE") & ".gif' title='" & rsSet("BIRTH_MODULE_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
						"<TD style='background-color:white' width=20 NOWRAP><img src='/myImages/Repairs/" & rsSet("BIRTH_ENTITY_IMAGE") & ".gif' title='" & rsSet("BIRTH_ENTITY_DESCRIPTION") & "' width='20' height='18' border=0></td>" &_
						"<TD style='background-color:white' width=34 NOWRAP><img src='/myImages/Repairs/" & rsSet("DISPLAYIMAGE") & ".gif' title=""" & rsSet("TITLE") & """ width='29' height='18' border=0></TD>"						
			
			AddressTitle = ""
			THEADDRESS = rsSet("FULLADDRESS")
			if (Len(THEADDRESS) > 35) THEN
				AddressTitle = " title=""" & THEADDRESS & """"
				THEADDRESS = Left(THEADDRESS, 35) & "..."
			end if

			' Lets set the row red if any items in the workorder are overdure
			OVERTIME = rsSet("OVERTIME") 
			if OVERTIME > 0 THEN
				RowColour = "RED"
			ELSE
				RowColour = ""
			END IF
			
			
			'HERE WE HAVE CHECK IF THE ITEM HAS BEEN SELECTED IN WHICH CASE WE SHOW THE RESPECTIVE REPAIRS FOR THE ITEM
			if ( (CStr(CurrentWorkOrder) = CStr(WOID)) ) then
				'ON THE STRING THAT APPEARS NEXT YOU WILL SEE THAT AN <A> TAG HAS BEEN ENTERED THIS IS SO THAT THE FRAME
				'CAN BE SCROLLED TO THIS WORK ORDER ON LOAD....
				str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND;color:" & RowColour & "'><A NAME=""WOID"">" &_
																"<TD width=20 NOWRAP style='background-color:white'><img src='/js/tree/img/minus.gif' width=18 height=18 border=0></TD>" &_
																ImageTD &_
																"<TD width=120 NOWRAP><b>" & rsSet("CREATIONDATE") & "</b></TD>" &_
																"<TD width=120 NOWRAP><b>" & WorkNumber(rsSet("WOID")) & "</b></TD>" &_
																"<TD width=270 NOWRAP " & AddressTitle & "><b>" & THEADDRESS & "</b></TD>" &_			
																"<TD><b>" & rsSet("WOSTATUSNAME")  & "</b></TD>" &_
															"<TR>"
				'CHECK WHEHTER THE ITEM HAS BEEN CLICKED ON AGAIN IN WHICH CASE WE CLOSE THE ITEM
				if (Request("CloseWO") <> "1") then
					BuildRepairList(WOID)	//SHOW THE APPROPRIATE REPAIR ROWS IF APPLICABLE
				else
				'RESET THE CURRENTWORKORDER VARIABLE AS IT HAS BEEN CLOSED
					CurrentWorkOrder = ""
				end if
			else
				'THIS PART BUILDS THE STANDARD WORK ORDER LINE WHICH IS CLOSED...
				str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND;color:" & RowColour & "'>" &_
																"<TD width=20 NOWRAP style='background-color:white'><img src='/js/tree/img/plus.gif' width=18 height=18 border=0></TD>" &_
																ImageTD &_
																"<TD width=120 NOWRAP><b>" & rsSet("CREATIONDATE") & "</b></TD>" &_
																"<TD width=120 NOWRAP><b>" & WorkNumber(rsSet("WOID")) & "</b></TD>" &_
																"<TD width=270 NOWRAP " & AddressTitle & "><b>" & THEADDRESS & "</b></TD>" &_																			
																"<TD><b>" & rsSet("WOSTATUSNAME")  & "</b></TD>" &_
															"<TR>"
			end if
			
			rsSet.movenext()
			
			End If
		Next
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TR><TD HEIGHT=18 COLSPAN=8 ALIGN=CENTER>No repair work order entries exist for selected criteria.</TD></TR>"
		End If

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<TFOOT><tr><TD HEIGHT='100%'></TD></TR><TR><TD COLSPAN=8 ALIGN=CENTER HEIGHT=20>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%' STYLE='BORDER-TOP:1PX SOLID;'><THEAD><TR STYLE='BACKGROUND-COLOR:BEIGE;'><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
		" Page <font color=red><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b>Next</font></b></a>"  &_ 
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b>Last</font></b></a>"  &_
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
		"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
				
		
	End Function

	//THIS FUNCTION BUILDS THE ACTUAL REPAIR ITEMS FOR THE SELECTED WORK ORDER
	Function BuildRepairList(WOID)
		cnt2 = 0
		//HUMONGOUS SQL WHICH DOES SOMETHING.....
		//INCLUDING GETTING THE REDIRECT PAGE FOR EACH SELECTED ITEM....
		
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF;" &_
					"SELECT 	J.JOURNALID, " &_
					"			CASE WHEN J.ITEMNATUREID = 1 THEN 'iDefectsDetail.asp' " &_
					"			WHEN J.ITEMNATUREID IN (2,20,21,22,34,35,36,37,38,39,40) THEN 'iRepairDetail.asp'  " &_		
					"			END AS REDIR, " &_
					"			ISNULL(CONVERT(NVARCHAR, J.CREATIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), J.CREATIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			P.HOUSENUMBER + ' ' + P.ADDRESS1 + ', ' + P.ADDRESS2 + ', ' + P.TOWNCITY + ', ' + P.POSTCODE AS FULLADDRESS, " &_
					"			ISNULL(N.DESCRIPTION, 'N/A') AS NATURE, " &_
					"			TITLE = CASE " &_
					"			WHEN J.PROPERTYID IS NULL AND LEN(J.TITLE) > 80 THEN LEFT(J.TITLE,80) + '...' " &_
					"			WHEN J.PROPERTYID IS NOT NULL AND LEN(J.TITLE) > 40 THEN LEFT(J.TITLE,40) + '...' " &_
					"			ELSE ISNULL(J.TITLE,'No Title') " &_
					"			END, J.TITLE AS FULLTITLE, J.PROPERTYID, " &_
					"			ISNULL(S.DESCRIPTION, 'N/A') AS STATUS, " &_
					"			J.ITEMNATUREID " &_
					"FROM	 	C_JOURNAL J " &_
					"			INNER JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_
					"			INNER JOIN P_WORKORDER WO ON WTR.WOID = WO.WOID " &_
					"			LEFT JOIN P__PROPERTY P ON J.PROPERTYID = P.PROPERTYID " &_																			
					"			LEFT JOIN C_STATUS S 	ON J.CURRENTITEMSTATUSID = S.ITEMSTATUSID " &_
					"			LEFT JOIN C_NATURE N	ON J.ITEMNATUREID = N.ITEMNATUREID " &_
					"WHERE	 	J.ITEMNATUREID IN (1,2,20,21,22,34,35,36,37,38,39,40) AND J.CURRENTITEMSTATUSID <> 5 " &_
					"			AND	WO.WOID = " & WOID & " " &_					
					"ORDER BY WTR.ORDERITEMID DESC"
		//response.write strSQL
		Call OpenRs (rsSet2, strSQL) 
		
		While Not rsSet2.EOF
			
			cnt2 = cnt2 + 1
			//WE CHECK IF THE EACH REPAIR JOURNAL HAS AN ADDRESS IN WHICH CASE
			//WE DISPLAY THE ADDRESS FOR EACH INDIVIDUAL REPAIR
			if (rsSet2("PROPERTYID") <> "") THEN
				
				//THIS PART GETS THE ADDRESS AND CHECKS WHETHER IT IS TOO LONG IN WHICH CASE IT IS REDUCED IN LENGTH AND A TITLE ADDED
				//TO THE RESPECTIVE TD
				AddressTitle = ""
				THEADDRESS = rsSet2("FULLADDRESS")
				if (Len(THEADDRESS) > 35) THEN
					AddressTitle = " title=""" & THEADDRESS & """"
					THEADDRESS = Left(THEADDRESS, 35) & "..."
				end if			

				str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet2("ITEMNATUREID") & "," & rsSet2("JOURNALID") & ",""" & rsSet2("REDIR") & """)' STYLE='CURSOR:HAND'>" &_
															"<TD colspan=4 style='background-color:white'><img src='/js/tree/img/joinlong.gif' width=84 height=18 border=0></TD>" &_
															"<TD colspan=2 " & AddressTitle & ">&nbsp;&nbsp;" & THEADDRESS & "</TD>" &_
															"<TD TITLE=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE")  & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</TD>" &_
														"<TR>"
			ELSE
				str_journal_table = str_journal_table & 	"<TR ONCLICK='open_me(" & rsSet2("ITEMNATUREID") & "," & rsSet2("JOURNALID") & ",""" & rsSet2("REDIR") & """)' STYLE='CURSOR:HAND'>" &_
															"<TD colspan=4 style='background-color:white'><img src='/js/tree/img/joinlong.gif' width=84 height=18 border=0></TD>" &_
															"<TD colspan=3 TITLE=""" & rsSet2("FULLTITLE")& """>&nbsp;&nbsp;" & rsSet2("TITLE")  & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("STATUS")  & "</TD>" &_
														"<TR>"
														
			END IF
			rsSet2.movenext()
			
		Wend
		CloseRs(rsSet2)
		
		IF cnt2 = 0 then
			str_journal_table = str_journal_table & "<TR><TD COLSPAN=9 ALIGN=CENTER>No repair item entries exist.</TD></TR>"
		End If	

	End Function	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=<%=orderBy%>&CWO=" + WOID + "&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value
	}

	function SORTPAGE(SORT){
		location.href = "iRepairJournal.asp?CompanyID=<%=CompanyID%>&page=<%=mypage%>&CC_Sort=" + SORT + "&CWO=<%=CurrentWorkOrder%>&filterid=" + parent.document.getElementById("sel_STATUS").value + "&WO=" + parent.document.getElementById("txt_WO").value		
	}
	
	//THIS FUNCTION OPENS UP THE RESPECTIVE REPAIRS JOURNAL
	function open_me(int_nature, int_journal_id, str_redir){
		parent.JOURNALID = int_journal_id
		parent.swap_div(10, "BOTTOM")
		parent.swap_div(4, "top")		
	}

	//THIS FUNCTION UPDATES THE CURRENTPURCHASEORDER VARIABLE IN THE PARENT HOLDING CRM
	//IT THEN MAKES THE FRAME SCROLL TO THE TOP OF THE CURRENT SELECTED WORK ORDER...		
	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		parent.MASTER_OPEN_WORKORDER_PAGE = "<%=mypage%>"
		parent.MASTER_OPEN_WORKORDER_SORT = "<%=orderBy%>"		
		location.href = "#WOID"		
		}
		
	<%= WriteJavaJumpFunction() %>	
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onLoad="DoSync();parent.STOPLOADER('BOTTOM');">

	<TABLE HEIGHT=100% WIDTH=746 CELLPADDING=0 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=CENTER STYLE='BACKGROUND-COLOR:BEIGE'>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=20 HEIGHT=20>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=20 NOWRAP>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=34 NOWRAP>&nbsp;</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap>
				<B>
				<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onClick="SORTPAGE('PO.PODATE ASC ASC')">
				Date
				<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onClick="SORTPAGE('PO.PODATE ASC DESC')">				
				</B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap>
				<B>
				<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onClick="SORTPAGE('WO.WOID ASC')">
				Code
				<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onClick="SORTPAGE('WO.WOID DESC')">				
				</B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=270 nowrap>
				<B>
				<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onClick="SORTPAGE('FULLADDRESS ASC')">
				Address
				<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onClick="SORTPAGE('FULLADDRESS DESC')">				
				</B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID;" WIDTH=142 nowrap>
				<B>
				<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onClick="SORTPAGE('WOSTATUSNAME ASC')">
				Status
				<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onClick="SORTPAGE('WOSTATUSNAME DESC')">				
				</B>
				</TD>
		</TR>
		</THEAD>
			<TBODY>
			<TR><TD colspan=8>
			<div style='overflow:auto;height:165'>
			<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0 STYLE="behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
			<%=str_journal_table%>
			</TABLE>
			</div>
			</TD></TR>
			<%=str_table_bottom%>
			</TBODY>
	</TABLE>
</BODY>
</HTML>	
	
	
	
	
	