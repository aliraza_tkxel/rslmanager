<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#INCLUDE VIRTUAL="Includes/Functions/URLQueryReturn.asp" -->
<%
	//THIS PAGE BUILDS THE REPAIR PURCHASE ORDERS FOR TYHE SUPPLIER CRM
	
	//LAST MODIFIED BY ZANFAR ALI 15 JUNE 2004 AT 17:15 HOURS
	//WORK DONE: TOTAL REWRITE TO TAKE INTO ACCOUNT NEW WORK ORDER FUNCTIONALITY.
	
	Dim cnt, customer_id, str_journal_table, orderby, status_sql, theURL
	Dim mypage, str_table_bottom	
	
	CompanyID = Session("ORGID")
	CurrentWorkOrder = Request("CWO")
	// BIUILD FILTER SQL STRING
	if request("filterid") = "" Then
		status_sql = ""
	Else
		status_sql = " AND POS.POSTATUSID = " & request("filterid")
	END IF

	FilterPO = Replace(Request("PO"), "'", "''")
	if FilterPO <> "" AND isNumeric(FilterPO) Then
		status_sql = status_sql & " AND PO.ORDERID = " & Clng(FilterPO) & " " 
	END IF
		
	orderby = Request("sort")
	if orderby = "" then orderby = "PO.ORDERID" end if

	OpenDB()	
	build_purchase_orders()
	CloseDB()

	Function WriteJavaJumpFunction()
		JavaJump = "function JumpPage(){" & VbCrLf
		JavaJump = JavaJump & "iPage = document.getElementById(""QuickJumpPage"").value" & VbCrLf
		JavaJump = JavaJump & "if (iPage != """" && !isNaN(iPage))" & VbCrLf
		JavaJump = JavaJump & "location.href = """ & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page="" + iPage" & VbCrLf
		JavaJump = JavaJump & "else" & VbCrLf
		JavaJump = JavaJump & "document.getElementById(""QuickJumpPage"").value = """" " & VbCrLf
		JavaJump = JavaJump & "}" & VbCrLf
		WriteJavaJumpFunction = JavaJump
	End Function
	
	Function build_purchase_orders()
		
		cnt = 0
		PageName = "iAccount.asp"
		theURL = theURLSET("cc_sort|page")

		orderBy = "WO.WOID DESC"
		if (Request("CC_Sort") <> "") then orderBy = Request("CC_Sort")

		Const adCmdText = &H0001
		
		mypage = Request("page")
		if (NOT isNumeric(mypage)) then 
			mypage = 1
		else
			mypage = CInt(mypage)
		end if
			
		if mypage = 0 then mypage = 1 end if
		
		pagesize = 8
				
		strSQL = 	"SET CONCAT_NULL_YIELDS_NULL OFF; " &_
				"SELECT WO.ORDERID, WO.WOID,  " &_
				"ISNULL(CONVERT(NVARCHAR, Po.PoDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), Po.PoDATE, 108) ,'')AS formatteddate, " &_				
				"			PODATE, PONAME, ISNULL(TOTALCOST,0) AS GROSSCOST, POSTATUSNAME,	" &_
				"			ITEMDESCPART = CASE " &_
				"			WHEN LEN(PO.PONAME) > 30 THEN LEFT(PO.PONAME,30) + '...' " &_
				"			ELSE ISNULL(PO.PONAME,'No Title') " &_
				"			END, PO.PONOTES " &_
				"FROM F_PURCHASEORDER PO " &_
				"INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID " &_
				"INNER JOIN F_POSTATUS POS ON POS.POSTATUSID = PO.POSTATUS " &_															
				"INNER JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) PI ON PI.ORDERID = PO.ORDERID " &_				
				"WHERE PO.SUPPLIERID = " & CompanyID & " AND PO.ACTIVE = 1 AND WO.BIRTH_NATURE<>47 AND PO.POSTATUS > 0 " & status_sql & " " &_ 
				"ORDER BY " & Replace(orderBy, "'", "''") 

		set rsSet = Server.CreateObject("ADODB.Recordset")
		rsSet.ActiveConnection = RSL_CONNECTION_STRING 			
		rsSet.Source = strSQL
		rsSet.CursorType = 2
		rsSet.LockType = 1		
		rsSet.CursorLocation = 3
		rsSet.Open()
			
		rsSet.PageSize = pagesize
		rsSet.CacheSize = pagesize
	
		numpages = rsSet.PageCount
		numrecs = rsSet.RecordCount
	
	' Just in case we have a bad request
		If mypage > numpages Then mypage = numpages 
		If mypage < 1 Then mypage = 1
		
		Dim nextpage, prevpage
		nextpage = mypage + 1
		if nextpage > numpages then 
			nextpage = numpages
		end if
		prevpage = mypage - 1
		if prevpage <= 0 then
			prevpage = 1
		end if
	' This line sets the current page
		If Not rsSet.EOF AND NOT rsSet.BOF then
			rsSet.AbsolutePage = mypage
		end if
		
		if (nextpage = 0) then nextpage = 1 end if
		if (numpages = 0) then numpages = 1 end if
				
		PrevAddressString = ""				
		str_journal_table = ""
		For i=1 to pagesize
			If NOT rsSet.EOF Then

		
			cnt = cnt + 1
			ORDERID = rsSet("ORDERID")
			WOID = rsSet("WOID")
			
			if (CStr(CurrentWorkOrder) = CStr(WOID)) then
			str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND'><A NAME=""WOID"">" &_
																"<TD width=20 nowrap style='background-color:white'><img src='/js/tree/img/minus.gif' width=18 height=18 border=0></TD>" &_
															"<TD width=120 nowrap><b>" & rsSet("formatteddate") & "</b></TD>" &_
															"<TD width=100 nowrap><a href=# onclick='open_me(" & ORDERID & ")'><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></TD>" &_															
															"<TD width=270 TITLE=""" & rsSet("PONOTES")& """><b>&nbsp;&nbsp;" & rsSet("ITEMDESCPART")  & "</b></TD>" &_
															"<TD width=120><b>" & rsSet("POSTATUSNAME")  & "</b></TD>" &_
															"<TD align=right><b>" & FormatCurrency(rsSet("GROSSCOST"))	  & "</b>&nbsp;</TD>" &_															
														"<TR>"
				Build_Purchase_Item_List(ORDERID)
				CurrentWorkOrder = WOID
			else
			str_journal_table = str_journal_table & 	"<TR ONCLICK='ReloadWO(" & WOID & ")' STYLE='CURSOR:HAND'>" &_
																"<TD width=20 nowrap style='background-color:white'><img src='/js/tree/img/plus.gif' width=18 height=18 border=0></TD>" &_
															"<TD width=120 nowrap><b>" & rsSet("formatteddate") & "</b></TD>" &_
															"<TD width=100 nowrap><a href=# onclick='open_me(" & ORDERID & ")'><b>" & PurchaseNumber(rsSet("ORDERID")) & "</b></a></TD>" &_															
															"<TD width=270 nowrap TITLE=""" & rsSet("PONOTES")& """><b>&nbsp;&nbsp;" & rsSet("ITEMDESCPART")  & "</b></TD>" &_
															"<TD width=120 nowrap><b>" & rsSet("POSTATUSNAME")  & "</b></TD>" &_
															"<TD align=right><b>" & FormatCurrency(rsSet("GROSSCOST"))	  & "</b>&nbsp;</TD>" &_															
														"<TR>"
			
			end if			

			rsSet.movenext()
			
			End If
		Next
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TR><TD height=20 COLSPAN=6 ALIGN=CENTER>No purchase order entries exist for selected criteria.</TD></TR>"
		End If

		'aDD rECORD pAGER AT BOTTOM
		orderBy = Server.URLEncode(orderBy)

		' links
		str_table_bottom = "" &_
		"<TFOOT><tr><TD HEIGHT='100%'></TD></TR><TR><TD COLSPAN=8 ALIGN=CENTER HEIGHT=20>" &_
		"<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH='100%' STYLE='BORDER-TOP:1PX SOLID;'><THEAD><TR STYLE='BACKGROUND-COLOR:BEIGE;'><TD WIDTH=100></TD><TD ALIGN=CENTER>"  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=1'><b><font color=BLUE>First</font></b></a> "  &_
		"<A HREF = '" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & prevpage & "'><b><font color=BLUE>Prev</font></b></a>"  &_
		" Page <font color=red><b>" & mypage & "</b></font> of " & numpages & ". Records: " & (mypage-1)*pagesize+1 & "  to " & (mypage-1)*pagesize+cnt & " of " & numrecs   &_
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & nextpage & "'><b>Next</font></b></a>"  &_ 
		" <A HREF='" & PageName & "?" & theURL & "CC_Sort=" & orderBy & "&page=" & numpages & "'><b>Last</font></b></a>"  &_
		"</TD><TD ALIGN=RIGHT WIDTH=100>Page:&nbsp;<input type='text' name='QuickJumpPage' value='' size=2 maxlength=3 class='textbox' style='border:1px solid #133E71;font-size:11px'>&nbsp;"  &_
		"<input type='button' class='RSLButtonSmall' value='GO' onclick='JumpPage()' style='font-size:10px'>"  &_
		"</TD></TR></THEAD></TABLE></TD></TR></TFOOT>" 
				
	End Function

	Function Build_Purchase_Item_List(ORDERID)
		cnt2 = 0
		strSQL = "SELECT PI.ORDERID, pi.active, PI.ITEMDESC, CASE WHEN LEN(PI.ITEMNAME) > 50 THEN LEFT(PI.ITEMNAME,50) + '...' ELSE PI.ITEMNAME END AS THEITEMNAME, PI.ITEMNAME, PI.PIDATE, PI.GROSSCOST, PS.POSTATUSNAME, " &_
				"ISNULL(CONVERT(NVARCHAR, PI.PIDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), PI.PIDATE, 108) ,'')AS formatteddate " &_
				"FROM F_PURCHASEITEM PI " &_
				"INNER JOIN F_POSTATUS PS ON PI.PISTATUS = PS.POSTATUSID AND ACTIVE = 1 " &_
				"INNER JOIN P_WOTOREPAIR WTR ON WTR.ORDERITEMID = PI.ORDERITEMID " &_				
				"WHERE PI.ORDERID = " & ORDERID & " " &_
				"ORDER BY pi.ORDERITEMID DESC"
			
		
		//response.write strSQL
		Call OpenRs (rsSet2, strSQL) 
		
		While Not rsSet2.EOF
			
			cnt2 = cnt2 + 1
			str_journal_table = str_journal_table & 	"<TR>" &_
															"<TD style='background-color:white'><img src='/js/tree/img/join.gif' width=18 height=18 border=0></TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("formatteddate") & "</TD>" &_
															"<TD COLSPAN=2 TITLE=""" & rsSet2("ITEMNAME") & """>&nbsp;&nbsp;" & rsSet2("THEITEMNAME") & "</TD>" &_
															"<TD>&nbsp;&nbsp;" & rsSet2("POSTATUSNAME")  & "</TD>" &_
															"<TD ALIGN=RIGHT><FONT COLOR=BLUE>" & fORMATcURRENCY(rsSet2("GROSSCOST")) & "</FONT>&nbsp;</TD>" &_
														"<TR>"
			rsSet2.movenext()
			
		Wend
		CloseRs(rsSet2)
		
		IF cnt2 = 0 then
			str_journal_table = str_journal_table & "<TR><TD COLSPAN=6 ALIGN=CENTER>No purchase item entries exist.</TD></TR>"
		End If	

	End Function	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Customer -- > Customer Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
	function open_me(Order_id){
		event.cancelBubble = true
		window.showModelessDialog("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")	
		}

	//THIS TABD LOADS UP THE SELECTED WORK ORDER
	function ReloadWO(WOID){
		parent.MASTER_OPEN_WORKORDER = WOID
		//CHECK IF THE WORK ORDER IS ALREADY OPEN IN WHICH CASE WE WILL CLOSE IT
		if (parseInt("<%=CurrentWorkOrder%>",10) == parseInt(WOID,10))
			WOID = "&CLOSEWO=1"
		location.href = "iAccount.asp?CompanyID=<%=CompanyID%>&CWO=" + WOID + "&page=<%=mypage%>&CC_Sort=<%=orderBy%>&filterid=" + parent.document.getElementById("sel_RPO").value + "&PO=" + parent.document.getElementById("txt_WOPO").value
	}

	function SORTPAGE(SORT){
		location.href = "iAccount.asp?CompanyID=<%=CompanyID%>&page=1&CC_Sort=" + SORT + "&CWO=<%=CurrentWorkOrder%>&filterid=" + parent.document.getElementById("sel_RPO").value + "&PO=" + parent.document.getElementById("txt_WOPO").value		
	}
	
	function DoSync(){
		parent.MASTER_OPEN_WORKORDER = "<%=CurrentWorkOrder%>"
		parent.MASTER_OPEN_WORKORDER_PAGE = "<%=mypage%>"
		parent.MASTER_OPEN_WORKORDER_SORT2 = "<%=orderBy%>"
		location.href = "#WOID"		
		}

	<%= WriteJavaJumpFunction() %>				
</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="DoSync();parent.STOPLOADER('BOTTOM');">

	<TABLE width=746 CELLPADDING=0 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
		<THEAD><TR VALIGN=center STYLE='BACKGROUND-COLOR:BEIGE'>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=20 height=20></TD>		
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="SORTPAGE('PO.PODATE ASC')">
						 Date : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="SORTPAGE('PO.PODATE DESC')">
					</FONT></B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=100 nowrap>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="SORTPAGE('PO.ORDERID ASC')">
						Order No : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="SORTPAGE('PO.ORDERID DESC')">
					</FONT></B>
				</TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=270 nowrap><B><FONT COLOR='BLUE'>Item:</FONT></B></TD>
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=120 nowrap>
					<B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="SORTPAGE('POSTATUSNAME ASC')">
						Status : 
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="SORTPAGE('POSTATUSNAME DESC')">
					</FONT></B>
				</TD>				
				<TD STYLE="BORDER-BOTTOM:1PX SOLID" align=right><B><FONT COLOR='BLUE'>
						<img src="/myImages/sort_arrow_up.gif" style='cursor:hand' border="0" alt="Sort Ascending" onclick="SORTPAGE('GROSSCOST ASC')">
						Total Cost :
						<img src="/myImages/sort_arrow_down.gif" style='cursor:hand' border="0" alt="Sort Descending" onclick="SORTPAGE('GROSSCOST DESC')">
						</FONT></B>
				</TD>				
		</TR>
			<TBODY>
			<TR><TD colspan=6>
			<div style='overflow:auto;height:165'>
			<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0 STYLE="behavior:url(/Includes/Tables/tablehl.htc);border-collapse:collapse" slcolor='' border=0 hlcolor=STEELBLUE>
			<%=str_journal_table%>
			</TABLE>
			</div>
			</TD></TR>
			<%=str_table_bottom%>
			</TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	