<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	OpenDB()
	CompanyID = Request("CompanyID")
	
	SQL = "SELECT S.SCOPEID, S.CONTRACTNAME, APPROVALSTATUS AS APPROVED, A.DESCRIPTION AS AREAOFWORK, S.DATEAPPROVED, ISNULL(ESTIMATEDVALUE,0) AS ESTIMATEDVALUE FROM S_SCOPE S LEFT JOIN S_AREAOFWORK A ON S.AREAOFWORK = A.AREAOFWORKID WHERE S.ORGID = " & CompanyID & " ORDER BY CONTRACTNAME"
	Call OpenRs(rsLoader, SQL)
	ContactString = ""
	if (NOT rsLoader.EOF) then
		WHILE NOT rsLoader.EOF
			APPROVED = rsLoader("APPROVED")
			IF (CSTR(APPROVED) = "1") THEN
				APPROVED = "YES"
			ELSE 
				APPROVED = "NO"
			END IF
			ContactString = ContactString & "<TR style='cursor:hand'><TD width=240 nowrap>" & rsLoader("CONTRACTNAME") & "</TD>" &_
						"<TD width=240 nowrap>" & rsLoader("AREAOFWORK") & "</TD>" &_
						"<TD width=80 nowrap align=right>" & FormatCurrency(rsLoader("ESTIMATEDVALUE")) & "</TD>" &_
						"<TD width=60 nowrap>" & APPROVED & "</TD>" &_					
						"<TD width='100%'>" & rsLoader("DATEAPPROVED") & "</TD></TR>"
			rsLoader.MoveNext
		wend
	else
		ContactString = ContactString & "<TR style='cursor:hand'><TD colspan=5 align=center>No Contracts exist for this company</TD></TR>" 
	end if
	
	CloseRs(rsLoader)
	CloseDB()
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE cellspacing=0 cellpadding=0 border=1 style='border-collapse:collapse;border-left:1px solid #133E71;border-bottom:1px solid #133E71;border-right:1px solid #133E71' WIDTH=750 height="192">
<TR bgcolor='#133E71' style='color:#FFFFFF'> 
  <TD height=20 width=240 nowrap><b>&nbsp;Contract Name:</b></TD>
  <TD width=240 nowrap><b>&nbsp;Area of Work:</b></TD>
  <TD width=80 nowrap><b>&nbsp;Value:</b></TD>
  <TD width=60 nowrap title='Approval Status'><b>&nbsp;App:</b></TD>  
  <TD width=128 nowrap><b>&nbsp;Date Approved:</b></TD>
</TR>
<TR>
  <td valign=top height=100% colspan=5> 
	<div style='height:170;width:748;overflow:auto' class='TA'> 
	<table cellspacing=0 cellpadding=3 border=1 style='border-collapse:collapse;behavior:url(/Includes/Tables/tablehl.htc)' slcolor='' hlcolor='STEELBLUE' width=100%>
	<%=ContactString%>
	</table>
	</div>
  </td>
</tr>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	