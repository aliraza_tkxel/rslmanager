<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	OpenDB()
	CompanyID = Request("CompanyID")
	
	strSQL = 	"SELECT E.FIRSTNAME + ' '  + E.LASTNAME AS FULLNAME, O.* FROM S_ORGANISATION O " &_
				"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = O.BRoADLANDCONTACT " &_
				"WHERE o.ORGID = " & CompanyID
				
	//response.write strSQL
	Call OpenRs (rsSet,strSQL) 
	
	If not rsSet.EOF Then
		// get property details
		l_buildingname = rsSet("BUILDINGNAME")
		l_address1 = rsSet("ADDRESS1")
		l_address2 = rsSet("ADDRESS2")
		l_address3 = rsSet("ADDRESS3")
		l_towncity = rsSet("TOWNCITY")
		l_county = rsSet("COUNTY")
		l_postcode = rsSet("POSTCODE")
		l_telephone1 = rsSet("TELEPHONE1")
		l_telephone2 = rsSet("TELEPHONE2")
		l_fax = rsSet("FAX")																								

		l_website = rsSet("WEBSITE")																								
		l_bc = rsSet("FULLNAME")																								
	Else
		l_buildingname = "<B>Company Not found !!!!</b>"
	End If
	
	CloseRs(rsSet)
		
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
	<TR>
		<TD ROWSPAN=2 HEIGHT=100%>
			
			
        
      <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
        <TR> 
          <TD width=120 CLASS='LONGTITLE'>Building Name:</TD>
          <TD width=200><%=l_buildingname%></TD>
          <TD width=120 CLASS='LONGTITLE'>Telephone 1:</TD>
          <TD  LONGTITLE='<%=pd_ethnicity%>'><%=l_telephone1%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 1:</TD>
          <TD><%=l_address1%></TD>
          <TD CLASS='LONGTITLE'>Telephone 2:</TD>
          <TD><%=l_telephone2%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 2:</TD>
          <TD><%=l_address2%></TD>
          <TD CLASS='LONGTITLE'>Fax:</TD>
          <TD><%=l_fax%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 3:</TD>
          <TD ><%=l_address3%></TD>
          <TD CLASS='LONGTITLE'>Web Site:</TD>
          <TD><%=l_website%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Town / City:</TD>
          <TD><%=l_towncity%></TD>
          <TD CLASS='LONGTITLE'>&nbsp;</TD>
          <TD>&nbsp;</TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>County:</TD>
          <TD><%=l_county%></TD>
          <TD CLASS='LONGTITLE'>Broadland Contact:</TD>
          <TD><%=l_bc%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Postcode:</TD>
          <TD><%=l_postcode%></TD>
          <TD CLASS='LONGTITLE'>&nbsp;</TD>
          <TD>&nbsp;</TD>
        </TR>
        <TR> 
          <TD colspan=4 height=100%></TD>
      </TABLE>
			
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	