<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	OpenDB()
	JournalID = Request("JournalID")
	
	strSQL = 	"SELECT " &_
				"C.FIRSTNAME, C.LASTNAME, " &_
				"ISNULL(P.HOUSENUMBER, 'N/A') AS HOUSENUMBER, ISNULL(P.ADDRESS1, 'N/A') AS ADDRESS1, " &_
				"ISNULL(P.ADDRESS2, 'N/A') AS ADDRESS2, ISNULL(P.ADDRESS3,'N/A') AS ADDRESS3, " &_
				"ISNULL(P.TOWNCITY,'N/A') AS TOWNCITY, ISNULL(P.POSTCODE,'N/A') AS POSTCODE, ISNULL(P.COUNTY,'N/A') AS COUNTY, " &_
				"J.TITLE, PI.PIDATE, PI.EXPPIDATE, " &_
				"isnull(PI.ORDERID,0) as orderid, isnull(PI.GROSSCOST,0) as quotedcost, " &_
				"PR.ESTTIME, PR.DESCRIPTION " &_
				"FROM C_JOURNAL J " &_
				"LEFT JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID " &_
				"INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID " &_
				"INNER JOIN R_ITEMDETAIL IT ON R.ITEMDETAILID = IT.ITEMDETAILID " &_
				"LEFT JOIN R_PRIORITY PR ON PR.PRIORITYID = IT.PRIORITY " &_
				"LEFT JOIN P__PROPERTY P ON P.PROPERTYID = J.PROPERTYID " &_
				"LEFT JOIN P_WOTOREPAIR WTR ON WTR.JOURNALID = J.JOURNALID " &_				
				"LEFT JOIN F_PURCHASEITEM PI ON PI.ORDERITEMID = WTR.ORDERITEMID " &_	
				"WHERE R.REPAIRHISTORYID = (SELECT MAX(REPAIRHISTORYID) AS REPAIRHISTORYID FROM C_REPAIR RR WHERE RR.JOURNALID = J.JOURNALID) " &_
				"AND J.JOURNALID = " & JournalID
	//response.write strSQL
	Call OpenRs (rsSet,strSQL) 
	
	If not rsSet.EOF Then
		// get property details
		LL = rsSet("FIRSTNAME")
		LLL = rsSet("LASTNAME")
		fullname = LL & " " & LLL
		if (fullname = " ") then fullname = "N/A"
		l_housenumber = rsSet("housenumber")		
		
		l_address1 = rsSet("ADDRESS1")
		l_address2 = rsSet("ADDRESS2")
		l_address3 = rsSet("ADDRESS3")
		l_towncity = rsSet("TOWNCITY")
		l_county = rsSet("COUNTY")
		l_postcode = rsSet("POSTCODE")
		orderid = rsSet("ORDERID")
		l_orderid = PurchaseNumber(rsSet("ORDERID"))
		l_orderdate = rsSet("pIdate")
		l_expdeldate = rsSet("expPIdate")
		l_priority = rsSet("description")
		l_esttime = rsSet("esttime")
		cost = FormatCurrency(rsSet("quotedcost"))
		l_title = rsSet("title")
	Else
		l_buildingname = "<B>Repair Information Not Found !!!!</b>"
	End If
	
	CloseRs(rsSet)
		
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<script language=javascript>
function open_me(Order_id){
	window.showModalDialog("../Popups/PurchaseOrder.asp?OrderID=" + Order_id + "&Random=" + new Date(), "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;")	
	}

function ShowMap(){
	window.showModalDialog('http://www.streetmap.co.uk/streetmap.dll?postcode2map?<%=l_postcode%>','','DialogHeight=650px; DialogWidth=700px')
	}
</script>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
	<TR>
		<TD ROWSPAN=2 HEIGHT=100%>
			
			
        
      <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
        <TR> 
          <TD width=120 CLASS='LONGTITLE'>Repair Info:</TD>
          <TD colspan=3><%=l_title%></TD>
        </TR>
        <TR> 
          <TD width=120 CLASS='LONGTITLE'>Customer Name:</TD>
          <TD width=200><%=fullname%></TD>
          <TD width=120 CLASS='LONGTITLE'>Order No:</TD>
          <TD  LONGTITLE='<%=pd_ethnicity%>'><%=l_orderid%></TD>
        </TR>
        <TR> 
          <TD width=120 CLASS='LONGTITLE'>Flat/House No:</TD>
          <TD width=200><%=l_housenumber%></TD>
          <TD width=120 CLASS='LONGTITLE'>Order Date:</TD>
          <TD  LONGTITLE='<%=pd_ethnicity%>'><%=l_orderdate%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 1:</TD>
          <TD><%=l_address1%></TD>
          <TD CLASS='LONGTITLE'>Exp Del Date:</TD>
          <TD><%=l_expdeldate%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 2:</TD>
          <TD><%=l_address2%></TD>
          <TD CLASS='LONGTITLE'>Est Time:</TD>
          <TD><%=l_esttime%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Address 3:</TD>
          <TD ><%=l_address3%></TD>
          <TD CLASS='LONGTITLE'>Priority:</TD>
          <TD><%=l_priority%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Town / City:</TD>
          <TD><%=l_towncity%></TD>
          <TD CLASS='LONGTITLE'>Cost:</TD>
          <TD><%=cost%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>County:</TD>
          <TD><%=l_county%></TD>
          <TD CLASS='LONGTITLE'>Purchase Order:</TD>
          <TD><a href="javascript:open_me(<%=orderid%>)"><font color=blue><b>Show FULL Purchase Order</b></font></a></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Postcode:</TD>
          <TD><%=l_postcode%></TD>
          <TD CLASS='LONGTITLE'>Map Link:</TD>
          <TD><a href=# onclick="javascript:ShowMap()"><b><font color=blue>Show Map</font></b></a></TD>
        </TR>
        <TR> 
          <TD colspan=4 height=100%></TD>
      </TABLE>
			
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	