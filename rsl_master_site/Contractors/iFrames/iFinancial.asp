<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	OpenDB()
	CompanyID = Request("CompanyID")
	
	strSQL = 	"SELECT E.FIRSTNAME + ' ' + E.LASTNAME AS REVIEWDBY, ISNULL(CURRENTTURNOVER, 0) AS CURRENTTURNOVER, " &_
				"ISNULL(TURNOVERLIMIT,0) AS TURNOVERLIMIT, " &_
				"ISNULL(CONTRACTLIMIT,0) AS CONTRACTLIMIT, DATEREVIEWED = CONVERT(VARCHAR, DATEREVIEWED,103), " &_
				"DATEAPPROVED = CONVERT(VARCHAR,DATEAPPROVED,103), " &_
				"LATESTACCOUNTDATE = CONVERT(VARCHAR,LATESTACCOUNTDATE,103), " &_				
				"AE.FIRSTNAME + ' ' + AE.LASTNAME AS APPROVEDBY FROM S_FINANCIAL F " &_
				"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = F.REVIEWEDBY " &_
				"LEFT JOIN E__EMPLOYEE AE ON AE.EMPLOYEEID = F.APPROVEDBY " &_
				"WHERE F.ORGID = " & CompanyID
				
	//response.write strSQL
	Call OpenRs (rsSet,strSQL) 
	
	If not rsSet.EOF Then
		// get property details
		l_currentturnover = rsSet("CURRENTTURNOVER")
		l_turnoverlimit = rsSet("TURNOVERLIMIT")
		l_contractlimit = rsSet("CONTRACTLIMIT")
		l_datereviewed = rsSet("DATEREVIEWED")
		l_dateapproved = rsSet("DATEAPPROVED")
		l_approvedby = rsSet("APPROVEDBY")
		l_reviewedby = rsSet("REVIEWDBY")
		l_latestaccountdate = rsSet("LATESTACCOUNTDATE")		
																								
	Else
		l_currentturnover = "<B>No Financial Figures set.</b>"
	End If
	
	CloseRs(rsSet)
		
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
	<TR>
		<TD ROWSPAN=2 HEIGHT=100%>
			
			
        
      <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
        <TR> 
          <TD width=150 nowrap CLASS='LONGTITLE'>Current Turnover:</TD>
          <TD width=100%><%=FormatCurrency(l_currentturnover)%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Latest Account Date:</TD>
          <TD><%=l_latestaccountdate%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Reviewed By:</TD>
          <TD><%=l_reviewedby%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Date of Review:</TD>
          <TD ><%=l_datereviewed%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Turnover Limit:</TD>
          <TD><%=FormatCurrency(l_turnoverlimit)%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Contract Limit:</TD>
          <TD><%=FormatCurrency(l_contractlimit)%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Approved By:</TD>
          <TD><%=l_approvedby%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Date of Approval:</TD>
          <TD><%=l_dateapproved%></TD>
        </TR>
        <TR> 
          <TD colspan=2 height=100%></TD>
        </TR>				
      </TABLE>
			
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	