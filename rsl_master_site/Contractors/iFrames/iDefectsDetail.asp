<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<%
	
	Dim cnt, employee_id, str_journal_table, absence_id, nature_id, repair_history_id, Defects_Title, last_status
	
	OpenDB()
	journal_id = Request("journalid")
	nature_id = Request("natureid")	
	build_journal()
	
	CloseDB()

	Function build_journal()
		
		cnt = 0
		strSQL = 	"SELECT		" &_
					"			ISNULL(CONVERT(NVARCHAR, P.LASTACTIONDATE, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), P.LASTACTIONDATE, 108) ,'')AS CREATIONDATE, " &_
					"			ISNULL(P.NOTES, 'N/A') AS NOTES, " &_
					"			J.TITLE, P.DEFECTSHISTORYID, O.NAME, " &_
					"			S.DESCRIPTION AS STATUS, " &_
					"			A.DESCRIPTION AS ACTION " &_					
					"FROM		P_DEFECTS P " &_
					"			LEFT JOIN C_STATUS S ON P.ITEMSTATUSID = S.ITEMSTATUSID  " &_
					"			LEFT JOIN C_ACTION A ON P.ITEMACTIONID = A.ITEMACTIONID  " &_					
					"			LEFT JOIN S_ORGANISATION O ON P.CONTRACTOR = O.ORGID " &_
					"			LEFT JOIN C_JOURNAL J ON P.JOURNALID = J.JOURNALID " &_
					"WHERE		P.JOURNALID = " & journal_id &_
					"ORDER 		BY DEFECTSHISTORYID DESC "
		
		//response.write strSQL
		Call OpenRs (rsSet, strSQL) 
		
		str_journal_table = ""
		While Not rsSet.EOF
			
			cnt = cnt + 1
			IF cnt > 1 Then
				str_journal_table = str_journal_table & 	"<TR STYLE='COLOR:GRAY'>"
			else
				Defects_Title = rsSet("TITLE")
				str_journal_table = str_journal_table & 	"<TR VALIGN=TOP>"
				repair_history_id = rsSet("DEFECTSHISTORYID")
				last_status = rsSet("STATUS")
			End If
				str_journal_table = str_journal_table & 	"<TD>" & rsSet("CREATIONDATE") & "</TD>" &_
															"<TD>" & rsSet("ACTION") & "</TD>" &_
															"<TD>" & rsSet("NOTES") & "</TD>" &_
															"<TD>" & rsSet("NAME")  & "</TD>" &_
														"<TR>"
			rsSet.movenext()
			
		Wend
		
		CloseRs(rsSet)
		
		IF cnt = 0 then
			str_journal_table = "<TFOOT><TR><TD COLSPAN=6 ALIGN=CENTER>No journal entries exist for this defect.</TD></TR></TFOOT>"
		End If
		
	End Function
	
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager My Job -- > Employment Relationship Manager</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/RSL.css" type="text/css">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">

	function update_repair(int_repair_history_id){
		var str_win
		str_win = "../PopUps/pRepair.asp?repairhistoryid="+int_repair_history_id+"&natureid=<%=nature_id%>" ;
		window.open(str_win,"display","width=407,height=280, left=200,top=200") ;
	}

</SCRIPT>
<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" class='TA' onload="parent.STOPLOADER('BOTTOM')">

	<TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 STYLE="border-collapse:collapse" slcolor='' border=0>
	<THEAD>
	<TR>
	<TD COLSPAN=4><table cellspacing=0 cellpadding=1 width=100%><tr valign=top><td><b>Defects Information:</b>&nbsp;<%=Defects_Title%></TD>
	<TD nowrap width=150>Current Status:&nbsp;<font color=red><%=last_status%></font></td><td align=right width=100>
	<INPUT TYPE=BUTTON NAME=BTN_UPDATE CLASS=RSLBUTTON VALUE =" Update " style="background-color:beige" onclick='update_repair(<%=repair_history_id%>)' DISABLED>
	</td></tr></table>
	</TD></TR>
	<TR valign=top>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" nowrap WIDTH=120PX>Date</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=130PX>Action</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" WIDTH=300PX>Notes</TD>
		<TD STYLE="BORDER-BOTTOM:1PX SOLID" width=200>Contractor</TD>
	</TR>
	<TR STYLE='HEIGHT:7PX'><TD COLSPAN=5></TD></TR></THEAD>
	<TBODY><%=str_journal_table%></TBODY>
	</TABLE>
	
</BODY>
</HTML>	
	
	
	
	
	