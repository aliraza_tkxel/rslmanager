<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!--#include virtual="includes/database/bankholidays.asp" -->
<!--#include virtual="includes/tables/crm_right_box.asp" -->
<%

	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=180" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7
	
	OpenDB()
	CompanyID = Request("CompanyID")
	
	strSQL = 	"SELECT PT.DESCRIPTION AS PAYMENTDESC, CTR.DESCRIPTION AS CTRADE, CT.DESCRIPTION AS CTYPE, ISNULL(O.PICOVERAMOUNT,0) AS FPICOVERAMOUNT, " &_
				"ISNULL(O.INSURANCECO,0) AS FINSURANCECO, O.* FROM S_ORGANISATION O " &_
				"LEFT JOIN F_PAYMENTTYPE PT ON PT.PAYMENTTYPEID = O.PAYMENTTYPE " &_
				"LEFT JOIN S_COMPANYTYPE CT ON O.COMPANYTYPE = CT.TYPEID " &_
				"LEFT JOIN S_COMPANYTRADE CTR ON O.TRADE = CTR.TRADEID " &_
				"WHERE ORGID = " & CompanyID
				
	//response.write strSQL
	Call OpenRs (rsSet,strSQL) 
	
	If not rsSet.EOF Then
		// get property details
		l_companyname = rsSet("NAME")
		l_companytype = rsSet("CTYPE")
		l_companytrade = rsSet("CTRADE")
		l_picoveramount = rsSet("FPICOVERAMOUNT")
		l_insuranceco = rsSet("FINSURANCECO")
		l_certificatenumber = rsSet("CERTIFICATENUMBER")
		l_professionalbody = rsSet("PROFESSIONALBODY")
		l_dateestablished = rsSet("DATEESTABLISHED")
		l_companynumber = rsSet("COMPANYNUMBER")
		l_dunsnumber = rsSet("DUNSNUMBER")																								

		l_vatregnumber = rsSet("VATREGNUMBER")																								
		l_ciscategory = rsSet("CISCATEGORY")																								
		l_ciscertificateno = rsSet("CISCERTIFICATENUMBER")																								
		l_cisexpirydate = rsSet("CISEXPIRYDATE")																								
		l_bankname = rsSet("BANKNAME")																								
		l_sortcode = rsSet("SORTCODE")																																		
		l_accountnumber = rsSet("ACCOUNTNUMBER")																								
		l_paymentterms = rsSet("PAYMENTTERMS")
		l_paymenttype = rsSet("PAYMENTDESC")																														
	Else
		l_companyname = "<B>Company Not found !!!!</b>"
	End If
	
	CloseRs(rsSet)
		
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>

<BODY BGCOLOR=#FFFFFF TOPMARGIN="0" LEFTMARGIN="0" RIGHTMARGIN="0" MARGINheight="0" MARGINWIDTH="0" onload="parent.STOPLOADER('TOP')">
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
	<TR>
		<TD ROWSPAN=2 HEIGHT=100%>
			
			
        
      <TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=1 CLASS="TAB_TABLE">
        <TR> 
          <TD width=120 CLASS='LONGTITLE'>Company Name : </TD>
          <TD width=200><%=l_companyname%></TD>
          <TD width=120 CLASS='LONGTITLE'>Trade : </TD>
          <TD  LONGTITLE='<%=pd_ethnicity%>'><%=l_companytrade%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Company Type :</TD>
          <TD><%=l_companytype%></TD>
          <TD CLASS='LONGTITLE'>PI Cover Amount :</TD>
          <TD><%=fORMATcURRENCY(l_picoveramount)%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Date Established:</TD>
          <TD><%=l_dateestablished%></TD>
          <TD CLASS='LONGTITLE'>Insurance Co:</TD>
          <TD><%=fORMATcURRENCY(l_insuranceco)%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>Company Number:</TD>
          <TD ><%=l_companynumber%></TD>
          <TD CLASS='LONGTITLE'>Certificate Number:</TD>
          <TD><%=l_certificatenumber%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>DUNS Number:</TD>
          <TD><%=l_dunsnumber%></TD>
          <TD CLASS='LONGTITLE'>Professional Body:</TD>
          <TD><%=l_professionalbody%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>VAT Reg Number:</TD>
          <TD><%=l_vatregnumber%></TD>
          <TD CLASS='LONGTITLE'>Payment Terms:</TD>
          <TD><%=l_paymentterms%> days via <%=l_paymenttype%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>CIS Category:</TD>
          <TD><%=l_ciscategory%></TD>
          <TD CLASS='LONGTITLE'>CIS Certificate No:</TD>
          <TD><%=l_ciscertificateno%></TD>
        </TR>
        <TR> 
          <TD CLASS='LONGTITLE'>CIS Expiry Date:</TD>
          <TD ><%=l_cisexpirydate%></TD>
          <TD CLASS='LONGTITLE'>&nbsp;</TD>
          <TD>&nbsp;</TD>
        </TR>
        <TR> 
      </TABLE>
			
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>	
	
	
	
	
	