<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include virtual="ACCESSCHECK.asp" -->
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Finance --> Attach Payment Slip</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var FormFields = new Array();
	var str_idlist, int_initial;
	int_initial = 0;
	str_idlist = "";
	detotal = new Number();

	FormFields[0] = "txt_FROM|From Date|DATE|Y"
	FormFields[1] = "sel_OFFICE|Local Office|SELECT|N"
	FormFields[2] = "txt_SLIPNUMBER|Slip Number|TEXT|N"
	FormFields[3] = "txt_TO|To Date|DATE|Y"
		
	function process(){

		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/process_WorksApprovedList_srv.asp?";
		RSLFORM.submit();

	}
	
	function Filter(){

		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/WorksApproved_srv.asp?PONumber=" + RSLFORM.txt_PONUMBER.value
		RSLFORM.submit();

	}

	function reset_client_totals(){
	
		int_initial = 0;
		str_idlist = "";
		detotal = 0;
	
	}
	//location.href = "serverside/WorksApproved_srv.asp?PONumber=RSLFORM.txt_PONUMBER.value"
	function GetList(){
		
		RSLFORM.target = "frm_slip";
		RSLFORM.action = "serverside/WorksApproved_srv.asp?";
		RSLFORM.submit();
	}
  
	// CALCULATE RUNNING TOTAL OF SLIP SELECTIONS
	function do_sum(int_num){
		
		if (document.getElementById("chkpost" + int_num+"").checked == true){	
			//detotal = detotal + parseFloat(document.getElementById("amount" + int_num+"").value);
			if (int_initial == 0) // first entry
				str_idlist = str_idlist  + int_num.toString();
			else 
				str_idlist = str_idlist + "," + int_num.toString();
			int_initial = int_initial + 1; // increase count of elements in string

			}
		else {
			//detotal = detotal - parseFloat(document.getElementById("amount" + int_num+"").value);
			int_initial = int_initial - 1;
			
			remove_item(int_num);
			}

		//document.getElementById("txt_POSTTOTAL").value = FormatCurrency(detotal);
		
		document.getElementById("idlist").value = str_idlist;
	}
	
	// REMOVE ID FROM IDLIST
	function remove_item(to_remove){
		
		var stringsplit, newstring, index, cnt, nowt;
		stringsplit = str_idlist.split(","); // split id string
		cnt = 0;
		newstring = "";
		nowt = 0;
		
		for (index in stringsplit) 
			if (to_remove == stringsplit[index])
				nowt = nowt;
			else {
				//alert("keeping  "+stringsplit[index]);
				if (cnt == 0)
					newstring = newstring + stringsplit[index].toString();
						else
					newstring = newstring + "," + stringsplit[index].toString();
				cnt = cnt + 1;
				}
		str_idlist = newstring;
	}
	
	
// -->
</SCRIPT>
<!-- End Preload Script -->

<BODY CLASS='TA' BGCOLOR=#FFFFFF ONLOAD="initSwipeMenu(3);preloadImages();GetList()" onUnload="macGo()" MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include virtual="Includes/Tops/BodyTop.asp" -->  
<form name = RSLFORM method=post>

	<TABLE BORDER=0 width=100% CELLPADDING=2 CELLSPACING=2>
		<TR>
			
      <TD><p>Below are the repairs that have been approved. When an invoice has been 
        sent please select the repair and click the 'Invoice Sent' button.<br>(If you know the purchase order number your looking for, enter it in the box below and click 'Filter') </p>
        </TD>
		</TR>
		<TR >
		  <TD>
		  
		   <!-- Begin of filter table -->
		  <table border=0 width=750 cellpadding=2 cellspacing=2 height=40PX style='padding-right:0px;' >
            <tr>

              <td width="100" NOWRAP ><b>PO Number : </b></td>
              <td width="592" >&nbsp;</td>
            </tr>
            <tr>
			  <td NOWRAP  valign="top"><input type=text id=txt_PONUMBER name=idlist2 CLASS='TEXTBOX100' ></td>
			  <td NOWRAP  valign="top"><image src="/js/FVS.gif" name="img_TO" width="15px" height="15px" border="0">              <span style="width:100%;">
				<input type="button" value=" Filter " class="RSLButton" onClick="Filter()" name="button">
				</span>
				</td>
     		</tr>

          </table>
		  
		  <!-- end of filter table -->
		  
		  </TD>
	  </TR>
		<TR style="display:none">
			<TD><input type=text id=idlist name=idlist CLASS='TEXTBOX200' ></TD>
		</TR>
	</TABLE>
 <BR>
<DIV ID=hb_div></DIV>
</form>
<!--#include virtual="Includes/Bottoms/BodyBottom.asp" -->
<iframe src="/secureframe.asp" name=frm_slip width=400px height=400px style='display:none'></iframe>
</BODY>
</HTML>

