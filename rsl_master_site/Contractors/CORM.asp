<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 

<!--#include virtual="ACCESSCHECK.asp" -->
<%
	CONST TABLE_DIMS = "WIDTH=750 HEIGHT=200" 'declare table sizes as a constant for all tables
	CONST TABLE_ROW_SIZE = 7

	Dim CompanyID,JournalId
	Dim str_diff_dis, str_holiday, str_policy, str_skills, cnt, list_item, rsPrev, disa_bled
	Dim CON_BOTTOM_FRAME_SRC
	CompanyID = Session("ORGID")
	JournalId= Request.QueryString.Item("JournalId") 
	
	if(JournalId<>"") Then
	    CON_BOTTOM_FRAME_SRC="iFrames/iRepairDetail.asp?JournalID="& JournalId   
	    CON_TOP_FRAME_SRC="/contractors/iframes/icustomerdetails.asp?JournalID="& JournalId  
	    DYNAMICSTYLE1="DISPLAY:NONE" 
	    DYNAMICSTYLE2="DISPLAY:BLOCK" 
	    IMG1_SRC="images/1-closed.gif"
	    IMG2_SRC="images/2-closed.gif"
	    IMG3_SRC="images/3-previous.gif"
	    IMG4_SRC="images/4-open.gif"
        IMG7_SRC="images/7-closed.gif"
	    IMG8_SRC="images/8-closed.gif"
	    IMG9_SRC="images/9-previous.gif"
	    IMG10_SRC="images/10-open.gif"
	else
	    CON_BOTTOM_FRAME_SRC="iFrames/iRepairJournal.asp?CompanyID="& CompanyID    
	    CON_TOP_FRAME_SRC="iframes/iMainDetails.asp?CompanyID=" & CompanyID
	    DYNAMICSTYLE1="DISPLAY:BLOCK" 
	    DYNAMICSTYLE2="DISPLAY:NONE" 
	    IMG1_SRC="images/1-open.gif"
	    IMG2_SRC="images/2-previous.gif"
	    IMG3_SRC="images/3-closed.gif"
	    IMG4_SRC="images/4-closed.gif"
	    IMG7_SRC="images/7-open.gif"
	    IMG8_SRC="images/8-previous.gif"
	    IMG9_SRC="images/9-closed.gif"
	    IMG10_SRC="images/10-closed.gif"
	end if
	
	OpenDB()
	
	Call BuildSelect(lst_work, "sel_STATUS", " C_STATUS WHERE ITEMSTATUSID IN (1,2,6,10,12,11) ", "ITEMSTATUSID, DESCRIPTION", "ITEMSTATUSID", "All", NULL, "WIDTH:73", "textbox100", " ONCHANGE=""javascript:WOGO(7)"" ")
	Call BuildSelect(lst_rpo, "sel_RPO", " F_POSTATUS WHERE POSTATUSID NOT IN (1,2,8,10,11,12) ", "POSTATUSID, POSTATUSNAME", "POSTATUSID", "All", NULL, "WIDTH:125", "textbox100", " ONCHANGE=""javascript:WOGO(8)"" ")
	Call BuildSelect(lst_po, "sel_PO", " F_POSTATUS WHERE POSTATUSID NOT IN (3,4,5,8,10,11,12) ", "POSTATUSID, POSTATUSNAME", "POSTATUSID", "All", NULL, "WIDTH:125", "textbox100", " ONCHANGE=""javascript:WOGO(9)"" ")
	Call BuildSelect(lst_nature, "sel_NATURE", " C_NATURE WHERE ITEMID = 1 AND ITEMNATUREID IN (2,20,21,22,34,35,36,37,38,39,40)", "ITEMNATUREID, DESCRIPTION", "DESCRIPTION", "All", NULL, "WIDTH:142", "textbox100",  " ONCHANGE=""javascript:WOGO(7)"" ")
	CloseDB()
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>RSL Manager Contractors -- > Relationship Manager</TITLE>
<link rel="stylesheet" href="/css/RSL.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style media=all type="text/css">
    .filters{ font-weight:bold; font-size:9px; }
</style>
</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/preloader.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/general.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/menu.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/FormValidation.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="/js/Loading.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
	var JOURNALID
	var iFrameArray = new Array ("empty",
								"imaindetails",
								"iaddress",
								"icontact",
								"icustomerdetails",
								"",
								"",
								"iRepairjournal",
								"iaccount",
								"iPurchases",								
								"irepairdetail")

	var FilterArray = new Array ()
	FilterArray[7] = "sel_STATUS"
	FilterArray[8] = "sel_RPO"
	FilterArray[9] = "sel_PO"
	FilterArray[10] = "sel_STATUS"			
	
	var MAIN_OPEN_BOTTOM_FRAME = 7
    var LOADER_FRAME_APPEND
	var MASTER_OPEN_WORKORDER = ""
	var MASTER_OPEN_WORKORDER_PAGE = ""	
	var MASTER_OPEN_WORKORDER_SORT = ""		
	var MASTER_OPEN_WORKORDER_SORT2 = ""			
	var MASTER_OPEN_PURCHASEORDER = ""
	var MASTER_OPEN_PURCHASEORDER_PAGE = ""	
	var MASTER_OPEN_PURCHASEORDER_SORT = ""		
	//var work_filtertext = "<%=lst_work%>"
	//var po_filtertext = <%=lst_po%>
	//var rpo_filtertext = <%=lst_rpo%>
	// swaps the images on the divs when the user clicks em -- NEEDS OPTIMISING -- PP
	// where is either 'top' or 'bottom' relating to the tab area of the page
	// FULL RELOAD FUNCTIONALITY NEEDS TO BE IMPLEMETED TO RELOAD ALL DYNAMIC DIVS -- PDP
	function swap_div(int_which_one, str_where){
		var divid, imgid, upper
		if (str_where == 'top'){
			upper = 4; lower = 1;
			STARTLOADER('TOP')
			if (int_which_one == 4)
				CON_TOP_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?JournalID=" + JOURNALID;			
			else
				CON_TOP_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>";			
			}
		else {
			upper = 10;	lower = 7;
			STARTLOADER('BOTTOM')
			MAIN_OPEN_BOTTOM_FRAME = int_which_one			
			if (int_which_one == 10)
				CON_BOTTOM_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?JournalID=" + JOURNALID;			
			else if (int_which_one == 9)
				CON_BOTTOM_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CPO=" + MASTER_OPEN_PURCHASEORDER + "&page=" + MASTER_OPEN_PURCHASEORDER_PAGE + "&cc_sort=" + MASTER_OPEN_PURCHASEORDER_SORT + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&PO=" + document.getElementById("txt_PO").value+ "&natureid=" +document.getElementById("sel_NATURE").value;
			else if (int_which_one == 8)
				CON_BOTTOM_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT2 + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&PO=" + document.getElementById("txt_WOPO").value+ "&natureid=" +document.getElementById("sel_NATURE").value;
			else if (int_which_one == 7) 
				CON_BOTTOM_FRAME.location.href = "/contractors/iframes/" + iFrameArray[int_which_one] + ".asp?CompanyID=<%=CompanyID%>&CWO=" + MASTER_OPEN_WORKORDER + "&page=" + MASTER_OPEN_WORKORDER_PAGE + "&cc_sort=" + MASTER_OPEN_WORKORDER_SORT + "&filterid=" + document.getElementById(FilterArray[int_which_one]).value + "&WO=" + document.getElementById("txt_WO").value + "&WOPO=" + document.getElementById("sel_WOPO").value + "&natureid=" +document.getElementById("sel_NATURE").value;

			}
		imgid = "img" + int_which_one.toString();
		
		// manipulate images and filter
		for (j = lower ; j <= upper ; j++){
			//alert("filterList" + j + "")
			document.getElementById("img" + j + "").src = "Images/" + j + "-closed.gif"
			//alert(document.getElementById("img" + j + "").src);
			if (j >= 7)
			     document.getElementById("filterList" + j + "").style.display = "none"
			}
		
		// unless last image in row
		if (int_which_one != upper)
			{
			    document.getElementById("img" + (int_which_one + 1) + "").src = "Images/" + (int_which_one + 1) + "-previous.gif"
		    }
		document.getElementById("img" + int_which_one + "").src = "Images/" + int_which_one + "-open.gif"
		if (int_which_one >= 7)
			document.getElementById("filterList" + int_which_one + "").style.display = "block"
			
	}
	
	// receives the url of the page to open plus the required width and the height of the popup
	function update_record(str_redir, wid, hig){
	
		window.open(str_redir, "display","width="+wid+",height="+hig+",left=100,top=200") ;
	
	}

	function WOGO(WH){
		MASTER_OPEN_PURCHASEORDER_PAGE = 1
		MASTER_OPEN_WORKORDER_PAGE = 1
		swap_div(WH, 'BOTTOM')
		}
// -->
</SCRIPT>


<BODY BGCOLOR=#FFFFFF onload="initSwipeMenu(0);preloadImages();" onUnload="macGo()" class='ta' MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<TR>
  <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772>
    <TR>
      
  <TD> 
    <TR>
      <TD HEIGHT=100% VALIGN=TOP STYLE='BORDER-LEFT:1PX SOLID #133E71;BORDER-RIGHT:1PX SOLID #133E71' width=772>
        <TR>
          <TD> 
            <!--#include virtual="Includes/Tops/BodyTop.asp" -->
            <form name = RSLFORM method=post> 
<!-- ImageReady Slices (My_Job_jan_perdetails_tabs.psd) -->

<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		  <TD ROWSPAN=2> <IMG NAME="img1" TITLE='Organisation Information' SRC="<%=IMG1_SRC %>" WIDTH=127 HEIGHT=20 BORDER=0 onclick="swap_div(1, 'top')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2> <IMG NAME="img2" TITLE='Address Details' SRC="<%=IMG2_SRC %>" WIDTH=72 HEIGHT=20 BORDER=0 onclick="swap_div(2, 'top')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2> <IMG NAME="img3" TITLE='Contact Information' SRC="<%=IMG3_SRC %>" WIDTH=71 HEIGHT=20 BORDER=0 onclick="swap_div(3, 'top')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2> <IMG NAME="img4" TITLE='' SRC="<%=IMG4_SRC %>" WIDTH=95 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		<TD>
			<IMG SRC="/myImages/spacer.gif" WIDTH=383 HEIGHT=19></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004376>
			<IMG SRC="images/spacer.gif" WIDTH=203 HEIGHT=1></TD>
	</TR>
</TABLE>

<!-- End ImageReady Slices -->
<DIV ID=TOP_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<IFRAME NAME=CON_TOP_FRAME <%=TABLE_DIMS%> src="<%=CON_TOP_FRAME_SRC%>" STYLE="OVERFLOW:hidden" frameborder=0></IFRAME>
</DIV>

<DIV ID=TOP_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:200' <%=TABLE_DIMS%>>
<TABLE WIDTH=750 HEIGHT=180 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=2 border=0 CLASS="TAB_TABLE">
    <TR>
      <TD ROWSPAN=2 WIDTH=70% HEIGHT=100%> 		
			<TABLE HEIGHT=100% WIDTH=100% STYLE="BORDER:SOLID 1PX #133E71;BORDER-COLLAPSE:COLLAPSE" CELLPADDING=3 CELLSPACING=0 border=0 CLASS="TAB_TABLE">
				<TR><TD width=17px></TD><TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_TOP"></DIV></b></TD></TR>
			</TABLE>
      </TD>
	</TR>		
</TABLE>
</DIV>
	
	<TABLE WIDTH=750 BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
		  <TD ROWSPAN=2 valign=bottom width=54>
		    <IMG NAME="img7" SRC="<%=IMG7_SRC%>" WIDTH=54 HEIGHT=20 BORDER=0  onclick="swap_div(7, 'bottom')" STYLE='CURSOR:HAND'>
		  </TD>
		  <TD ROWSPAN=2 valign=bottom width=63><IMG NAME="img8" SRC="<%=IMG8_SRC%>" WIDTH=62 HEIGHT=20 BORDER=0 onclick="swap_div(8, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom width=99> <IMG NAME="img9" SRC="<%=IMG9_SRC%>"  WIDTH=98 HEIGHT=20 BORDER=0 onclick="swap_div(9, 'bottom')" STYLE='CURSOR:HAND'></TD>
		  <TD ROWSPAN=2 valign=bottom width=21><IMG NAME="img10" SRC="<%=IMG10_SRC%>"  WIDTH=20 HEIGHT=20 BORDER=0 STYLE='CURSOR:HAND'></TD>
		  <TD align=right height=19><TABLE WIDTH=100% CELLPADDING=2 height=100% CELLSPACING=0><TR>
				<TD ALIGN=RIGHT height=24>
						<span id='filterList7' STYLE='<%=DYNAMICSTYLE1%>'>
						        <table width=512 cellspacing=0 cellpadding=1 style='border:1px solid #133e71'>
						            <tr style='background-color:beige' valign=middle>
						                <td><span class="filters" style="padding-left:4px;padding-right:4px;">REPAIR TYPE:</span><%=lst_nature %></td>
						                <td>
						                    <span class="filters">
						                        <select class="textbox100" style="width:50px; font-weight:bold;" id="sel_WOPO" name="sel_WOPO">
						                            <option selected="selected"  value="1" >WO</option> 
						                            <option value="2" >PO</option> 
						                         </select>   
						                    </span> 
						                    <input class="textbox" type='text' size=10 name="txt_WO" maxlength="6"/>
						                </td>
						                <td>&nbsp;<span class="filters">STATUS</span>: <%=lst_work%></td>
						                <td><input type=button onclick="WOGO(7)" value=" GO " class="RSLButtonSmall"></td>
						             </tr>
						         </table>
						</span>
						<span id='filterList8' STYLE='DISPLAY:NONE'><table width=330 cellspacing=0 cellpadding=1 style='border:1px solid #133e71'><tr style='background-color:beige' valign=middle><td><b>&nbsp;PO :</b> <input class="textbox" type='text' size=10 name="txt_WOPO" maxlength=6></td><td>&nbsp;<b>STATUS</b> : <%=lst_rpo%></td><td><input type=button onclick="WOGO(8)" value=" GO " class="RSLButtonSmall"></td></tr></table></span>
						<span id='filterList9' STYLE='DISPLAY:NONE'><table width=330 cellspacing=0 cellpadding=1 style='border:1px solid #133e71'><tr style='background-color:beige' valign=middle><td><b>&nbsp;PO :</b> <input class="textbox" type='text' size=10 name="txt_PO" maxlength=6></td><td>&nbsp;<b>STATUS</b> : <%=lst_po%></td><td><input type=button onclick="WOGO(9)" value=" GO " class="RSLButtonSmall"></td></tr></table></span>						
						<span id='filterList10' STYLE='<%=DYNAMICSTYLE2%>' ><table width=330 cellspacing=0 cellpadding=1 style='border:1px solid #133e71'><tr style='background-color:beige' valign=middle><td><b>Editing Journal Entry</b><select class='textbox' style='visibility:hidden'><option value=''>1</option></select></td></tr></table></span>
				</TD>
				</TR>
			</TABLE></TD>
	</TR>
	<TR>
		<TD BGCOLOR=#004174><IMG SRC="images/spacer.gif" WIDTH=355 HEIGHT=1></TD>
	</TR>
</TABLE>

<DIV ID=BOTTOM_DIV STYLE='DISPLAY:BLOCK;OVERFLOW:hidden"'>
	<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=1 CELLSPACING=0 border=0 CLASS="TAB_TABLE" >
	  <TR> 
		<TD><iframe name=CON_BOTTOM_FRAME src="<%=CON_BOTTOM_FRAME_SRC%>" width=100% height="100%" frameborder=0 style="border:none"></iframe></TD>
	  </TR>
	</TABLE>
</DIV>
<DIV ID=BOTTOM_DIV_LOADER STYLE='DISPLAY:NONE;OVERFLOW:hidden;width:750;height:210' WIDTH=750 HEIGHT=210>
<TABLE WIDTH=750 HEIGHT=210 STYLE="BORDER-RIGHT: SOLID 1PX #133E71" CELLPADDING=10 CELLSPACING=10 border=0 CLASS="TAB_TABLE">
    <TR>
		<TD STYLE='COLOR:SILVER;FONT-SIZE:20PX' ALIGN=LEFT VALIGN=CENTER><b><DIV ID="LOADINGTEXT_BOTTOM"></DIV></b></TD>
	</TR>		
</TABLE>
</DIV>

</form>
<!--#include VIRTUAL="INCLUDES/Bottoms/BodyBottom.asp" -->
</BODY>
</HTML>

