<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<HTML>
<HEAD>
<TITLE>Timed Out</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/RSL.css" type="text/css">

<SCRIPT LANGUAGE="JAVASCRIPT">
<!--
	try {
		window.resizeTo(400, 330)
		}
	catch (e){
		temp =1
		}
	try {
		window.dialogHeight = "320px"
		}
	catch (e){
		temp =1
		}
	try {
		window.dialogWidth = "400px"
		}
	catch (e){
		temp =1
		}

function DoFunction(){
	window.focus();
	}
//-->
</SCRIPT>
</HEAD>
<body bgcolor="#FFFFFF" MARGINTOP=0 MARGINHEIGHT=0 TOPMARGIN=6 onload="DoFunction()">
<table width=379 border="0" cellspacing="0" cellpadding="0" style='border-collapse:collapse'>
<form name="RSLFORM" method="POST">
  <tr> 
    <td width=10 height=10><img src="/myImages/corner_pink_white_littl.gif" width="10" height="10" alt="" border=0 /></td>
	<td width=302 style='border-top:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
	<td width=67 style='border-top:1px solid #133e71;border-right:1px solid #133e71'><img src="/myImages/spacer.gif" height=9 /></td>
  </tr>
  <tr>
  	<td height=190 colspan=3 valign=top style='border-left:1px solid #133e71;border-right:1px solid #133e71'>
	<table cellspacing=5 width=100%><tr><td>
		
		<table cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td colspan=2 align=left><img src="/myImages/My60_pink.gif" width="48" height="19">
			</tr>
			<tr bgcolor="#133e71"> 
			      <td height=19px class="RSLWhite">&nbsp;<b>Timed Out</b></td>
			  <td class="RSLWhite" align=right>&nbsp;</td>
			</tr>  
			<tr>
				  <td colspan=2 valign=middle bgcolor="#f8f6f6" style='border:1px solid #133e71' align=center height=150px> 
                    &nbsp; <font color="red"><b>The session has timed out, therefore 
                    you will have to log back into RSL Manager.</b></font> <br>
                    
				</td>
			</tr>
		</table>
		
	</td></tr></table>
	</td>
  </tr>
  <tr> 
	  <td colspan=2 align="right" width=100% style='border-bottom:1px solid #133e71;border-left:1px solid #133e71'>
	  <a href="#" onclick="window.close()" class="RSLBlack">Close Window</a></td>
	  <td width=67 align="right"><img src="/myImages/corner_pink_white_big.gif" width="67" height="69" /></td>
  </tr>
</FORM>
</table>
<iframe src="/secureframe.asp" name="ServerFrame" style='display:none'></iframe>
</BODY>
</HTML>

