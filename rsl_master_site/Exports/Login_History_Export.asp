<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<% ByPassSecurityAccess = true %>
<!--#include virtual="ACCESSCHECK.asp" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>BHA Login History Export</title>
<script language="JavaScript" type="text/javascript" src="../js/formValidation.js"></script>
<script language="JavaScript" type="text/javascript" src="../js/general.js"></script>
<SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../js/calendarFunctions.js"></script>
<script language="JavaScript" type="text/javascript">

function Export()
	{
		if(document.thisForm.txtstartdate.value == '')
			{
				alert("You must enter a start date")
				return false
			}
		if(document.thisForm.txtenddate.value == '')
			{
				alert("You must enter an end date")
				return false
			}
	
	GO('Export.asp','');
	}

function GO(thePage, myAction)
	{
		thisForm.action = thePage; 
		thisForm.target="ServerFrame"; 
		thisForm.theAction.value = myAction;
		thisForm.submit();
	}

</script>
</head>
	
<body>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:109px; z-index:20; visibility: hidden'></div>
<form name="thisForm" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" width="700" align="center">
    <tr> 
      <td width="10"></td>
      <td valign="top"> 
	    <table width=781 border=0 cellpadding=0 cellspacing=0> 
          <tr> 
            <td width="658" height=18 valign="top">
			  <table width="658" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="10">&nbsp;</td>
                  <td valign="top">
				  	<table border="0" cellspacing="0" cellpadding="0">
						<tbody id="XMLSEARCH" name="XMLSEARCH" style="display:block;width:404px">
						<tr> 
							<td width="80" height="1" bgcolor="#0099CC"></td>
							<td width="324" height="1" bgcolor="#0099CC"></td>
						</tr>
						<tr>
							<td colspan=2>&nbsp;</td>
						</tr>
						<tr> 
							<td width="80"></td>
							<td width="324"> 
						</td>
						</tr>
						<tr> 
							<td width="324" height="5" colspan=2>Please enter a start date and an end date for the report data and then click the export button.</td>
						</tr>
						<tr> 
							<td width="324" height="5" colspan=2></td>
						</tr>
						<tr> 
							<td width="80">Start Date:</td>
							<td width="324"> 
								<input type=text name=txtstartdate maxlength=10 style="width=100" tabindex=1 READONLY value="<% = str_completiondate %>">
                      			<input type="button" name="btnDate" value="Date" onClick="YY_Calendar('txtstartdate',400,80,'de','#FFFFFF','#0099CC','YY_calendar1')" class="EntryButton">
							</td>
						</tr>
						<tr> 
							<td width="80" height="5">End Date:</td>
							<td width="324" height="5"><input type=text name=txtenddate maxlength=10 style="width=100" tabindex=1 READONLY value="<% = str_completiondate %>">
                      	<input type="button" name="btnDate" value="Date" onClick="YY_Calendar('txtenddate',400,104,'de','#FFFFFF','#0099CC','YY_calendar1')" class="EntryButton"></td>
						</tr>
						<tr> 
							<td width="80" height="5"></td>
							<td width="324" height="5"></td>
						</tr>
						<tr> 
							<td width="80">&nbsp;</td>
							<td width="324">&nbsp;</td>
						</tr>
						<tr> 
							<td width="80" height="1"></td>
							<td width="324" height="1"></td>
						</tr>
						<tr align="right"> 
							<td colspan="2" height="10"></td>
						</tr>
						<tr align="right"> 
							<td colspan="2" height="1" bgcolor="#0099CC"></td>
						</tr>
						<tr align="right">
							<td colspan="2" height="10"></td>
						</tr>
						<tr align="right"> 
							<td colspan="2"> 
							<input type="button" name="btn_CourseSearch" value=" Export " onClick="Export();" class="SEND" style="cursor:pointer">
							</td>
						</tr>
						</tbody>
						<tbody id="XMLSEARCHRESULTS" colspan="2" style="display:none;width:404px"><tr><td colspan="2"><input type="button" name="btn_ToggleSearch" value=" SEARCH AGAIN" onClick="ToggleSearch();" class="SEND" style="cursor:pointer"></td></tr>
						<tr> 
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr> 
							<td colspan="2"> 
							<div id="Div_Loading" style="position:absolute; z-index:5; visibility: hidden"><font color="01A8DC" face="Arial, Helvetica, sans-serif">Please 
							be patient while your results are loading...</font></div>
							<div id="Div_Results" style="background-color:#FFFFFF; overflow:auto; width=350 height:290px">&nbsp;</div>
							<div id="data"> </div><input type="hidden" name="HotCourseID"><input type="hidden" name="HotCourseTitle">
							</td>
						</tr>
						</tbody>
					</table>	  
				  </td>
				</td>
                </tr>
              </table>
			 </td>
          </tr>
        </table>
      </td>
      <td width="10">&nbsp;</td>
    </tr>
  </table>
<input type="hidden" name="theAction">
<input type="hidden" name="hid_http" value="0">
<iframe id="ServerFrame" name="ServerFrame" width="600" height="600" style="display:none" src="../secureframe.asp">LIVE OPEN</iframe> 
</form>
</body>
</html>
