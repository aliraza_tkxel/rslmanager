<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% Session.LCID = 2057 %>
<!--#include file="../Connections/db_connection.asp" -->
<!-- #Include file="ADOVBS.INC" -->
<%
set rsNeighbourhoods = Server.CreateObject("ADODB.Recordset")
rsNeighbourhoods.ActiveConnection = RSL_CONNECTION_STRING
rsNeighbourhoods.Source = "SELECT *  FROM dbo.rslNeighbourHood  ORDER BY NeighbourHood"
rsNeighbourhoods.CursorType = 0
rsNeighbourhoods.CursorLocation = 2
rsNeighbourhoods.LockType = 3
rsNeighbourhoods.Open()
rsNeighbourhoods_numRows = 0
%>
<%
	dim count
	dim crnstring, namestring, enqstring, categorystring

	dim mypage
	mypage = CInt(Request("page"))
	
	nh = Request("neighbourhood")
	if (nh = "") then
		nhc = -1
	else
		nhc = nh
	end if	
	postcode = Request("postcode")
	tolet = Request("tolet")
	forsale = Request("forsale")
	bedrooms = Request("bedrooms")
	theAsset = Request("asset")
	
	sqlFilter = " and rslAsset.AssetID = " & theAsset
	
	StrEventQuery =	"SELECT BedroomCount, rslAvailability.Availability, rslAsset.AssetID, rslAsset.PropStatID, rslPropertyStatus.PropStat, rslOwner.Owner, "&_
		"rslAsset.PicUrl, rslNeighbourHood.NeighbourHood, rslDwelling.Dwelling, rslLocation.Address1, rslLocation.Address2, "&_
		"rslLocation.Address3, rslLocation.Address4, rslLocation.Postcode, rslLocation.County, "&_
		"rslLocation.Telephone, rslLocation.HouseNumber, rslLevel.[Level], rslAssetType.AssetType, "&_ 
		"rslTaxBand.TaxBand, rslServiceChargeCategory.ServiceCat, rslRentCat.RentCat, isNull(rslRentCat.RentRate,0) as RentRate, rslAsset.CapVal, isNull(rslAsset.OpenMarkVal,0) as OpenMarkVal, "&_
		"rslAgeCat.AgeCat FROM rslAsset "&_
		"LEFT OUTER JOIN rslLevel ON rslAsset.LevelID = rslLevel.LevelID LEFT OUTER JOIN "&_
		"rslAgeCat ON rslAsset.AgeID = rslAgeCat.AgeID LEFT OUTER JOIN "&_
		"rslAssetType ON rslAsset.TypeID = rslAssetType.AssetTypeID LEFT OUTER JOIN "&_
		"rslRentCat ON rslAsset.RentID = rslRentCat.RentID LEFT OUTER JOIN "&_
		"rslServiceChargeCategory ON rslAsset.ServiceCatID = rslServiceChargeCategory.ServiceCatID LEFT OUTER JOIN "&_
		"rslTaxBand ON rslAsset.TaxBandID = rslTaxBand.TaxBandID LEFT OUTER JOIN "&_
		"rslLocation ON rslAsset.AssetID = rslLocation.AssetID LEFT OUTER JOIN "&_
		"rslOwner ON rslAsset.OwnerID = rslOwner.OwnerID LEFT OUTER JOIN "&_
		"rslDwelling ON rslAsset.DwellingID = rslDwelling.DwellingID LEFT OUTER JOIN "&_
		"rslNeighbourHood ON rslAsset.NeighbourHoodID = rslNeighbourHood.NeighbourHoodID LEFT OUTER JOIN "&_
		"rslPropertyStatus ON rslPropertyStatus.PropStatID = rslAsset.PropStatID LEFT OUTER JOIN "&_
		"rslAvailability ON rslPropertyStatus.AvailabilityID = rslAvailability.AvailabilityID "&_
		"left join bedroomCount on rslAsset.Assetid = BedroomCount.assetid where rslAsset.AssetID = rslAsset.AssetID " &_
		sqlFilter
	'Response.Write strEventQuery 
	
	if mypage = 0 then mypage = 1 end if
	
	pagesize = 5
	
	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open Application("MSSQLConnectionString")

	Set Rs = Server.CreateObject("ADODB.Recordset")
	Rs.PageSize = pagesize
	Rs.CacheSize = pagesize
	Rs.CursorLocation = adUseClient

	Rs.Open strEventQuery, Conn, adOpenForwardOnly, adLockReadOnly, adCmdText

%>

<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Find-a-Home</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript>
function checkData(){
doc= document.all;
errTxt.innerHTML = "&nbsp;<br>&nbsp;";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt.innerHTML = "Please check a box for<br>Property Status.";
	return false;
	}
if (doc["neighbourhood"].value == "" && doc["postcode"].value == ""){
	errTxt.innerHTML = "Please select a neighbourhood<br>or enter a postcode.";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt.innerHTML = "Please select the number<br>of bedrooms you would like.";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include file="BodyTop.html" -->
      <table width=213 border=0 cellpadding=0 cellspacing=0 align=right>
        <tr> 
          <td bgcolor=#FFFFFF width=3 height=1><img src="images/spacer.gif" width=1 height=5></td>		  
        </tr>
        <tr> 
          <td bgcolor=#395D88 width=1 height=1></td>
          <td bgcolor=#133E71 width=13 height=1></td>
          <td bgcolor=#395D88 width=186 height=1></td>
          <td bgcolor=#133E71 width=12 height=1></td>
          <td bgcolor=#395D88 width=1 height=1></td>
          <td bgcolor=#FFFFFF width=3 height=1><img src="images/spacer.gif" width=3 height=1></td>		  
        </tr>
        <tr> 
          <td bgcolor=#264E7D width=1 height=23></td>
          <td bgcolor=#133E71 width=13 height=23></td>
          <td><img src="images/title_your_search.gif" width=186 height=23></td>
          <td bgcolor=#133E71 width=12 height=23></td>
          <td bgcolor=#133E71 width=1 height=23></td>
        </tr>
        <tr> 
          <td bgcolor="#315683" width=1 height=205></td>
          <td><img src="images/ys_bg_01.gif" width=13 height=205></td>          
		  <td width=186 height=205 background="images/ys_bg_02.gif" valign="top">
<table cellspacing="0" cellpadding="0" border="0" class="iagManagerSmallBlk">
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td>Property Status</td>
</tr>
<form name=thisForm method=post>
<tr> 
<td>To Let 
<% 
isChecked = ""
if (tolet <> "") then
	isChecked = " checked"
End if
%>
<input type=checkbox value=1 name=tolet <%=isChecked%>>&nbsp;
<% 
isChecked = ""
if (forsale <> "") then
	isChecked = " checked"
End if
%>
For Sale <input type=checkbox value=2 name=forsale <%=isChecked%>>
</td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td>Select Neighbourhood&nbsp;</td>
</tr>
<tr> 
<td> 
<select name=neighbourhood style="width:180px" class="iagManagerSmallBlk">
<option value="">Please Select</option>
<%
While (NOT rsNeighbourhoods.EOF)
isSelected = ""

if (CInt(rsNeighbourhoods.Fields.Item("NeighbourHoodID").Value) = CInt(nhc)) then
	isSelected = " selected"
End if
%>
<option value="<%=(rsNeighbourhoods.Fields.Item("NeighbourHoodID").Value)%>" <%=isSelected%>><%=(rsNeighbourhoods.Fields.Item("NeighbourHood").Value)%></option>
<%
  rsNeighbourhoods.MoveNext()
Wend
If (rsNeighbourhoods.CursorType > 0) Then
  rsNeighbourhoods.MoveFirst
Else
  rsNeighbourhoods.Requery
End If
%>
</select>
</td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td>Area/Postcode</td>
</tr>
<tr> 
<td> 
<input type=text style="width:180px" name=postcode class="iagManagerSmallBlk" value="<%=postcode%>">
</td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td>Number of Bedrooms</td>
</tr>
<tr> 
<td> 
<select name=bedrooms style="width:180px" class="iagManagerSmallBlk">
<option value="ALL">Any Number</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "1") then 
 isSelected = " selected"
End if 
%>
<option value='1'<%=isSelected%>>1 Bedroom</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "2") then 
 isSelected = " selected"
End if 
%>

<option value='2'<%=isSelected%>>2 Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "3") then 
 isSelected = " selected"
End if 
%>

<option value='3'<%=isSelected%>>3 Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "4") then 
 isSelected = " selected"
End if 
%>

<option value='4'<%=isSelected%>>4 or more</option>
</select>
</td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td><font color=red> 
<div id=errTxt>&nbsp;<br>&nbsp;</div>
</font></td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td align=right> 
<input type=button value=' Search Again ' onClick="checkData()" name="button" class="iagButton">&nbsp;<input type=reset value=' Clear ' name="reset" class="iagButton"></td>
</tr>
</form>
</table>
</td>
          <td><img src="images/ys_bg_03.gif" width=12 height=205></td>
          <td bgcolor=#315683 width=1 height=205></td>
        </tr>
        <tr> 
          <td><img src="images/ys_btm_01.gif" width=1 height=12></td>
          <td><img src="images/ys_btm_02.gif" width=13 height=12></td>
          <td><img src="images/ys_btm_03.gif" width=186 height=12></td>
          <td><img src="images/ys_btm_04.gif" width=12 height=12></td>
          <td><img src="images/ys_btm_05.gif" width=1 height=12></td>
        </tr>
      </table>
      <blockquote>
<table cellspacing=0 class="rslManager">
        <tr> 
          <td bgcolor=#FFFFFF width=3 height=1><img src="images/spacer.gif" width=1 height=5></td>		  
        </tr>
<tr><td width=120px></td><td width=200px></td><td width=100px></td><td></td></tr>
<%
			Response.Write "<tr class='iagManagerTable'>"

			myArray3 = Array("housenumber","address1","address2","address3","address4","postcode","county")
			add_details = "<TABLE Cellspacing=0 cellpadding=0 class='rslManager'>"	
			For j=0 To 6 
				theItem = Rs(myArray3(j))
				If theItem <> "" Then
					if (myArray3(j) = "housenumber") then
						add_details = add_details & "<TR><TD>" & theItem & "</TD></TR>"
					elseif (myArray3(j) = "telephone") then
						add_details = add_details & "<TR><TD>TEL: " & theItem & "</TD></TR>"
					else
						add_details = add_details & "<TR><TD>" & theItem & "</TD></TR>"										
					end if
				End If
			Next
			add_details = add_details & "</TABLE>"
			
			if (Rs("PropStatID") = 1) then
				temp1 = "To Let"
				temp2 = Rs("rentCat")
				temp3 = "�" & FormatNumber(Rs("rentRate"),2)
				temp4 = "Rent"
			Else
				temp1 = "For Sale"
				temp2 = ""
				temp3 = "�" & FormatNumber(Rs("openmarkval"),2)
				temp4 = "Price"
			End if

			mypic = Rs("PicUrl")
			if (mypic = "" OR isNull(mypic)) then
				mypic = "<image src='/rslAssets/propimages/default.gif' border=0 alt='No Property Image' width=86 height=78>"
			else
				mypic = "<image src='/rslAssets/propimages/" & mypic &"' border=0 alt='Property Image' width=86 height=78>"			
			end if
			Response.Write "<td rowspan=4 valign=top>" & mypic & "</td><td rowspan=4 valign=top>" & add_details & "</td><td valign=top>No Bedrooms: </td><td valign=top>" & Rs("bedroomCount") & "</td><td align=right>&nbsp;</td></tr>"
			Response.Write "<tr><td valign=top height=""100%""><table height='100%'><tr><td height='100%'>&nbsp;</td></tr></table></td></tr>"						
			Response.Write "<tr><td valign=top>Property Status:</td><td align=right>" & temp1 & " </td></tr>"
			Response.Write "<tr><td valign=top>" & temp4 & ":</td><td align=right>" & temp3 & " </td></tr>"
%>
<tr><td></td><td colspan=3><li>Dwelling Type:&nbsp;<%=Rs("Dwelling")%>, <%=Rs("Level")%></td></tr>
<tr><td></td><td colspan=3><li>No Bedrooms:&nbsp;<%=Rs("BedroomCount")%> Bedroom(s)</td></tr>
<tr><td></td><td colspan=3><li>Age:&nbsp;<%=Rs("AgeCat")%></td></tr>
<tr><td></td><td colspan=3><li>Council Tax Band:&nbsp;<%=Rs("TaxBand")%></td></tr>
<%
			Response.Write "<tr><td colspan=4 align=right class='iagManagerSmallBlue'>&nbsp;[<a href='view.asp?asset=" & Rs("AssetID") &"' class='iagManagerSmallBlue'>Arrange to View</a>]</td></tr>"			

%>
<tr><td colspan=4 align=right>&nbsp;<br>&nbsp;<br>&nbsp;<br></td></tr>
<tr><td colspan=4 align=right><input type=button class="iagButton" value=" Back to Results " onclick="javascript:history.back()"></td></tr>
</table>

</blockquote>
<!--#include file="BodyBottom.html" -->
</BODY>
</HTML>
<% 
rsNeighbourhoods.close()
%>