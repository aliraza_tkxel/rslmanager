<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/db_connection.asp" -->
<%
set rsNeighbourhoods = Server.CreateObject("ADODB.Recordset")
rsNeighbourhoods.ActiveConnection = RSL_CONNECTION_STRING
rsNeighbourhoods.Source = "SELECT *  FROM dbo.rslNeighbourHood  ORDER BY NeighbourHood"
rsNeighbourhoods.CursorType = 0
rsNeighbourhoods.CursorLocation = 2
rsNeighbourhoods.LockType = 3
rsNeighbourhoods.Open()
rsNeighbourhoods_numRows = 0
%>
<html>
<head>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Find-a-Home</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript>
function checkData(){
doc= document.all;
errTxt.innerHTML = "";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt.innerHTML = "Please check a box for Property Status.";
	return false;
	}
if (doc["neighbourhood"].value == "" && doc["postcode"].value == ""){
	errTxt.innerHTML = "Please select a neighbourhood or enter a postcode.";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt.innerHTML = "Please select the number of bedrooms you would like.";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="10" TOPMARGIN="10" MARGINWIDTH="0">
<!--#include file="BodyTop.html" -->
<TABLE WIDTH=743 BORDER=0 CELLPADDING=0 CELLSPACING=0>
<TR> 
<TD colspan="2" HEIGHT=87 valign="bottom"><img src="images/find_a_home.gif" width=291 height=25></TD>
</TR>
<TR> 
<TD HEIGHT=17 align="right"> 
<table width=330 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=28></td>
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=28></td>
<td bgcolor=#133E71> <img src="images/title_search.gif" width=326 height=28></td>
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=28></td>
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=28></td>
</tr>
<tr> 
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=157></td>
<td bgcolor=#C0CBDA> <img src="images/spacer.gif" width=1 height=157></td>
<td width=326 height=157 valign="top" background="images/search_box_bg.gif">
<table cellspacing="0" cellpadding="0" border="0" class="iagManagerSmallBlk">
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<form name=thisForm method=post>
<tr> 
<td>Property Status</td>
<td>To Let 
<input type=checkbox value=1 name=tolet checked>
&nbsp; For Sale 
<input type=checkbox value=2 name=forsale checked>
</td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td>Select Neighbourhood&nbsp;&nbsp;</td>
<td> 
<select name=neighbourhood style='width:200px' class="iagManagerSmallBlk">
<option value="">Please Select</option>
<%
While (NOT rsNeighbourhoods.EOF)
%>
<option value="<%=(rsNeighbourhoods.Fields.Item("NeighbourHoodID").Value)%>" ><%=(rsNeighbourhoods.Fields.Item("NeighbourHood").Value)%></option>
<%
  rsNeighbourhoods.MoveNext()
Wend
If (rsNeighbourhoods.CursorType > 0) Then
  rsNeighbourhoods.MoveFirst
Else
  rsNeighbourhoods.Requery
End If
%>
</select>
</td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td>Area/Postcode</td>
<td> 
<input type=text style='width:200px' name=postcode class="iagManagerSmallBlk">
</td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td>Number of Bedrooms</td>
<td> 
<select name=bedrooms style='width:200px' class="iagManagerSmallBlk">
<option value="ALL">Any Number</option>
<option value='1'>1 Bedroom</option>
<option value='2'>2 Bedrooms</option>
<option value='3'>3 Bedrooms</option>
<option value='4'>4 or more</option>
</select>
</td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td align=right colspan="2"><font color=red> 
<div id=errTxt></div>
</font></td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td align=right>&nbsp; </td>
<td align=right> 
<input type=button value=' Search ' onClick="checkData()" name="button" class="iagButton">
&nbsp; 
<input type=reset value=' Clear ' name="reset" class="iagButton">
&nbsp;&nbsp; </td>
</tr>
</form>
</table>
</td>
<td bgcolor=#BAC6D5> <img src="images/spacer.gif" width=1 height=157></td>
<td bgcolor=#133E71> <img src="images/spacer.gif" width=1 height=157></td>
</tr>
<tr> 
<td> <img src="images/find-a-job__39.gif" width=1 height=15></td>
<td> <img src="images/find-a-job__40.gif" width=1 height=15></td>
<td>
<TABLE WIDTH=326 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD>
			<IMG SRC="images/find-a-job__2_44.gif" WIDTH=12 HEIGHT=15></TD>
		<TD>
			<IMG SRC="images/find-a-job__2_45.gif" WIDTH=302 HEIGHT=15></TD>
		<TD>
			<IMG SRC="images/find-a-job__2_46.gif" WIDTH=12 HEIGHT=15></TD>
	</TR>
</TABLE>
</td>
<td> <img src="images/find-a-job__42.gif" width=1 height=15></td>
<td> <img src="images/find-a-job__43.gif" width=1 height=15></td>
</tr>
</table>
</TD>
<TD HEIGHT=17 align="right" width="10">&nbsp;</TD>
</TR>
<TR> 
<TD colspan="2" HEIGHT=17>&nbsp; </TD>
</TR>
</TABLE>
<!--#include file="BodyBottom.html" -->
</BODY>
</HTML>