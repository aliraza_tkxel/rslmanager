<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="../Connections/db_connection.asp" --><% Session.LCID = 2057 %>
<%
	' Implemented for sake of demo at property show
	' Must eventually come from valid tenancy record upon provision of valid Property Reference No
	Session("TenancyID") = Request("theID")
	Session("RSLID") = 4
%>
<HTML>
<HEAD>
<TITLE>Broadland Housing Association Limited</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript">
<!--
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		reporting_repairs_over = newImage("images/reporting%2brepairs-over.gif");
		rr_broadland_47_reporting_repairs_over = newImage("images/rr_broadland_47-reporting%2br.gif");
		preloadFlag = true;
	}
}
// -->
</SCRIPT>
<script language="javascript">

	function loadPage(intID){
		//alert(thisForm.action);
		reColour(intID);
		thisForm.action = "main.asp?theID=" + intID;
		thisForm.submit();	
	}
	
	
	function reColour(intID){
	
		var coll = document.all.tags("TD");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.backgroundColor == "beige")
				  		coll[i].style.backgroundColor = "#c8c7c7"
						
		document.all.item("TD" + intID).style.backgroundColor = "beige";
		
	
	}
	
	function drillDown(intID){
	
		thisForm.action = "drilldown.asp?drillID=" + intID;
		//alert(thisForm.action);
		thisForm.submit();
	
	}
	
	function drillSubmit(intAction){
	//alert(thisForm.action);
		reColour(10);
		thisForm.action = "basket.asp?action=" + intAction;
		thisForm.submit();
	
	}
	
	function loadApp(){
	//alert(thisForm.action);
		thisForm.action = "app.asp";
		thisForm.submit();
			
	}
	
	function submitApp(){
	//alert(thisForm.action);
		thisForm.action = "saveApp.asp";
		thisForm.submit();
		
	}
	
</script>
<link rel="stylesheet" href="css/reporter.css" type="text/css">
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 ONLOAD="preloadImages();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
<td align="center" valign="middle">
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td>
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr>
<td> 
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;"> 
<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="images/rr_broadland_15.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="91">&nbsp;</td>
<td>
<table width=705 border=0 cellpadding=0 cellspacing=0>
<form name='thisForm' method='post' target='frmRR'>
<tr> 
<td rowspan=8> <img src="images/_gif_22.gif" width=6 height=395 alt=""></td>
<td width=9 height=24 bgcolor="#FFFFFF"></td>
<td colspan=4> <img src="images/_gif_24.gif" width=604 height=24 alt=""></td>
<td width=23 height=24 bgcolor="#FFFFFF"></td>
<td colspan=2> <img src="images/_gif_26.gif" width=63 height=24 alt=""></td>
<td> <img src="images/spacer.gif" width=1 height=24 alt=""></td>
</tr>
<tr> 
<td rowspan=7 width=9 height=371 bgcolor="#FFFFFF"></td>
<td rowspan="7" height=101 bgcolor="#FFFFFF" valign="top"> 
<!--#include file="menu.asp" -->
</td>
<td colspan="6" height=320 bgcolor="#FFFFFF" valign="top" rowspan="7">
<div id='divMain' named='divMain' width=100% style='overflow:auto;height:371px' class='TA'>
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td height="23"><img src="images/_gif_31.gif" width=347 height=30 alt=""></td>
<td height="63" rowspan="4"><img src="images/_gif_32.gif" width=180 height=144 alt=""></td>
</tr>
<tr> 
<td>&nbsp;</td>
</tr>
<tr> 
<td><img src="images/_gif_35.gif" width=351 height=51 alt=""></td>
</tr>
<tr> 
<td>&nbsp;</td>
</tr>
<tr> 
<td colspan="2"><img src="Images/_gif_37.gif" width="531" height="227"></td>
</tr>
</table>
</div>
</td>
<td> <img src="images/spacer.gif" width=1 height=30 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=23 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=51 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=40 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=156 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=30 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=1 height=41 alt=""></td>
</tr>
<tr> 
<td> <img src="images/_gif_41.gif" width=6 height=8 alt=""></td>
<td colspan=7> <img src="images/_gif_42.gif" width=686 height=8 alt=""></td>
<td> <img src="images/_gif_43.gif" width=13 height=8 alt=""></td>
<td> <img src="images/spacer.gif" width=1 height=8 alt=""></td>
</tr>
<tr> 
<td> <img src="images/spacer.gif" width=6 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=9 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=159 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=4 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=347 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=94 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=23 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=50 height=1 alt=""></td>
<td> <img src="images/spacer.gif" width=13 height=1 alt=""></td>
<td></td>
</tr>
</form>
</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
<IFRAME width=500px height=300px name='frmRR' style='display:block'></IFRAME>
</BODY>
</HTML>