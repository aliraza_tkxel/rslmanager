<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/db_connection.asp" --><%
Dim rsDevelopments
Dim rsDevelopments_numRows
set rsDevelopments = Server.CreateObject("ADODB.Recordset")
	rsDevelopments.ActiveConnection = RSL_CONNECTION_STRING
	rsDevelopments.Source = "SELECT *  FROM dbo.rslDevelopments  ORDER BY DevelopmentName"
	rsDevelopments.CursorType = 0
	rsDevelopments.CursorLocation = 2
	rsDevelopments.LockType = 3
	rsDevelopments.Open()
	rsDevelopments_numRows = 0
%><HTML>
<HEAD>
<TITLE>Find A Home</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<SCRIPT TYPE="text/javascript">
<!--
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		reporting_repairs_over = newImage("images/reporting%2brepairs-over.gif");
		//our+properties+menu_over = newImage("images/our+properties+menu-over.gif");
		preloadFlag = true;
	}
}
// -->
</SCRIPT>
<script language=javascript>
function checkData(){
doc= document.all;
errTxt2.innerHTML = "&nbsp;<br>&nbsp;";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt2.innerHTML = "<BR>Please check a box for Property Status.<BR>";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt2.innerHTML = "<BR>Please select the number of bedrooms you would like.<BR>";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 ONLOAD="preloadImages();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr> 
<td align="center" valign="middle"> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr> 
<td> 
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;"> 
<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="Images/our+properties+title.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="91">&nbsp;</td>
<td> 
<table width=705 border=0 cellpadding=0 cellspacing=0>
<form name="thisForm" method="post">
<tr> 
<td rowspan=2> <img src="images/_gif_22.gif" width=6 height=395 alt=""></td>
<td colspan="2" height=24 bgcolor="#FFFFFF" align="right"><img src="images/curve.gif" width=690 height=24 alt=""></td>
</tr>
<tr> 
<td width=9 height=370 bgcolor="#FFFFFF" valign="top"> 
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="9">&nbsp;</td>
<td width="35">&nbsp;&nbsp;&nbsp;</td>
<td width="343"> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td colspan=2><img src="images/txt_welcome.gif" width=343 height=30></td>
</tr>
<tr> 
<td colspan="2" height=27></td>
</tr>
<tr> 
<td colspan=2><img src="images/txt_search_info.gif" width=343 height=40><font color=red> 
<div id="errTxt2">&nbsp;<br>
&nbsp;</div>
</font></td>
</tr>
<tr> 
<td width=152 height=17>Property Status</td>
<td width=191 height=17> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td width=13 height=17> 
<% 
isChecked = ""
if (tolet <> "") then
	isChecked = " checked"
End if
%>
<input type=checkbox value=1 name=tolet <%=isChecked%>>
</td>
<td width=42 height=17>To Let</td>
<td width=20 height=17></td>
<td width=13 height=17> 
<% 
isChecked = ""
If (forsale <> "") Then
	isChecked = " checked"
End if
%>
<input type=checkbox value=2 name=forsale <%=isChecked%>>
</td>
<td height=17>For Sale </td>
</tr>
</table>
</td>
</tr>
<tr> 
<td width=152 height=17>&nbsp;</td>
<td width=191 height=17>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=17>Select Neighbourhood&nbsp;&nbsp;</td>
<td width=191 height=17> 
<select name=Development style="width:150px" class="iagManagerSmallBlk">
<option value="">ANY</option>
<%
While (NOT rsDevelopments.EOF)
isSelected = ""
If (CInt(rsDevelopments.Fields.Item("DevelopmentID").Value) = CInt(nhc)) Then
	isSelected = " selected"
End if
%>
<option value="<%=(rsDevelopments.Fields.Item("DevelopmentID").Value)%>" <%=isSelected%>><%=(rsDevelopments.Fields.Item("DevelopmentName").Value)%></option>
<%
  rsDevelopments.MoveNext()
Wend
If (rsDevelopments.CursorType > 0) Then
  rsDevelopments.MoveFirst
Else
  rsDevelopments.Requery
End If
  rsDevelopments.close()
%>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=15>Number of Bedrooms </td>
<td width=191 height=15> 
<select name=bedrooms style="width:150px" class="iagManagerSmallBlk">
<option value="ALL">ALL</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "1") Then 
 isSelected = " selected"
End If 
%>
<option value='1'<%=isSelected%>>1 
Bedroom</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "2") Then 
 isSelected = " selected"
End If 
%>
<option value='2'<%=isSelected%>>2 
Bedrooms</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "3") Then 
 isSelected = " selected"
End If 
%>
<option value='3'<%=isSelected%>>3 
Bedrooms</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "4") Then 
 isSelected = " selected"
End If 
%>
<option value='4'<%=isSelected%>>4 
or more</option>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp; </td>
</tr>
<tr> 
<td colspan=2 width=343 height=15> 
<table border=0 cellpadding=0 cellspacing=0 width=343>
<tr> 
<td colspan=6 width=276 height=19>&nbsp; </td>
<td colspan=2> 
<input type=button value=' Search' onClick="checkData()" name="button2" class="iagButton">
</td>
<td width=17 height=19></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td width="15"><img src="Images/spacer.gif" width="15" height="1">&nbsp;</td>
<td valign="top"> 
<table border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height=30><img src="images/txt_fah.gif" width=170 height=30></td>
</tr>
<tr> 
<td height=27></td>
</tr>
<tr> 
<td width=124 height=12></td>
</tr>
<tr> 
<td idth=306 height=13></td>
</tr>
<tr> 
<td width=306>&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td height=370 bgcolor="#FFFFFF" valign="top">&nbsp; </td>
</tr>
<tr> 
<td> <img src="images/_gif_41.gif" width=6 height=8 alt=""></td>
<td> <img src="images/_gif_42.gif" width=686 height=8 alt=""></td>
<td width="13"> <img src="images/_gif_43.gif" width=13 height=8 alt=""></td>
</tr>
</form>
</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
</BODY>
</HTML>