<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/db_connection.asp" --><!-- #Include file="ADOVBS.INC" -->
<%
    nh = Request("Development")
	if (nh = "") then
		nhc = -1
	else
		nhc = nh
	end if
	
	postcode = Request("postcode")
	tolet = Request("tolet")
	forsale = Request("forsale")
	bedrooms = Request("bedrooms")
	
	bedroom_sql = ""
	if (bedrooms = "ALL") then
		bedroom_sql = ""	
	elseif (bedrooms >= 4) Then
		bedroom_sql = " and bedroomcount >= 4"
	else
		bedroom_sql = " and bedroomcount = " & bedrooms
	end if
	
	stat_sql = ""
	if (tolet <> "" AND forsale <> "") then
		stat_sql = " and (rslAsset.PropStatID = 1 OR rslAsset.PropStatID = 2)"
	elseif (tolet <> "" and  forsale = "") then
		stat_sql = " and (rslAsset.PropStatID = 1)"
	elseif (tolet = "" and  forsale <> "") then
		stat_sql = " and (rslAsset.PropStatID = 2)"
	end if		
	
	nh_sql = ""
	if (nh <> "" and nh <> -1) then
		nh_sql = " and rslAsset.DevelopmentID = " & nh
	end if

	postcode_sql = ""
	if (postcode <> "") then
		postcode_sql = " and (rslLocation.Postcode like '%" & postcode & "%')"
	end if
		
	
	StrEventQuery =	"SELECT rslDevelopments.DevelopmentID, rslDevelopments.DevelopmentName , BedroomCount, rslAvailability.Availability, rslAsset.AssetID, rslAsset.PropStatID, rslPropertyStatus.PropStat, rslOwner.Owner, "&_
		"rslAsset.PicUrl, rslDevelopments.DevelopmentName, rslDwelling.Dwelling, rslLocation.Address1, rslLocation.Address2, "&_
		"rslLocation.Address3, rslLocation.Address4, rslLocation.Postcode, rslLocation.County, "&_
		"rslLocation.Telephone, rslLocation.HouseNumber, rslLevel.[Level], rslAssetType.AssetType, "&_ 
		"rslTaxBand.TaxBand, rslServiceChargeCategory.ServiceCat, rslRentCat.RentCat, isNull(rslRentCat.RentRate,0) as RentRate, rslAsset.CapVal, isNull(rslAsset.OpenMarkVal,0) as OpenMarkVal, "&_
		"rslAgeCat.AgeCat FROM rslAsset "&_
		"LEFT OUTER JOIN rslLevel ON rslAsset.LevelID = rslLevel.LevelID LEFT OUTER JOIN "&_
		"rslAgeCat ON rslAsset.AgeID = rslAgeCat.AgeID LEFT OUTER JOIN "&_
		"rslAssetType ON rslAsset.TypeID = rslAssetType.AssetTypeID LEFT OUTER JOIN "&_
		"rslRentCat ON rslAsset.RentID = rslRentCat.RentID LEFT OUTER JOIN "&_
		"rslServiceChargeCategory ON rslAsset.ServiceCatID = rslServiceChargeCategory.ServiceCatID LEFT OUTER JOIN "&_
		"rslTaxBand ON rslAsset.TaxBandID = rslTaxBand.TaxBandID LEFT OUTER JOIN "&_
		"rslLocation ON rslAsset.AssetID = rslLocation.AssetID LEFT OUTER JOIN "&_
		"rslOwner ON rslAsset.OwnerID = rslOwner.OwnerID LEFT OUTER JOIN "&_
		"rslDwelling ON rslAsset.DwellingID = rslDwelling.DwellingID LEFT OUTER JOIN "&_	
		"rslDevelopments ON rslAsset.DevelopmentID = rslDevelopments.DevelopmentID LEFT OUTER JOIN "&_
		"rslPropertyStatus ON rslPropertyStatus.PropStatID = rslAsset.PropStatID LEFT OUTER JOIN "&_
		"rslAvailability ON rslPropertyStatus.AvailabilityID = rslAvailability.AvailabilityID "&_
		"left join bedroomCount on rslAsset.Assetid = BedroomCount.assetid where rslAsset.OrgID = 258 AND rslAsset.AssetID = rslAsset.AssetID " &_
		postcode_sql & stat_sql & bedroom_sql & nh_sql

Dim rsDevelopments
Dim rsDevelopments_numRows
set rsDevelopments = Server.CreateObject("ADODB.Recordset")
	rsDevelopments.ActiveConnection = RSL_CONNECTION_STRING
	rsDevelopments.Source = StrEventQuery	
	rsDevelopments.CursorType = 0
	rsDevelopments.CursorLocation = 2
	rsDevelopments.LockType = 3
	rsDevelopments.Open()
	rsDevelopments_numRows = 0
	
	'"SELECT *  FROM dbo.rslDevelopments  ORDER BY DevelopmentName ASC"
	
Dim rsDD
Dim rsDD_numRows
set rsDD = Server.CreateObject("ADODB.Recordset")
	rsDD.ActiveConnection = RSL_CONNECTION_STRING
	rsDD.Source = "SELECT *  FROM dbo.rslDevelopments  ORDER BY DevelopmentName ASC"	
	rsDD.CursorType = 0
	rsDD.CursorLocation = 2
	rsDD.LockType = 3
	rsDD.Open()
	rsDD_numRows = 0
%>
<html>
<head>
<TITLE>Find-a-Home</TITLE>
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language=javascript>
function checkData(){
doc= document.all;
errTxt2.innerHTML = "&nbsp;<br>&nbsp;";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt2.innerHTML = "Please check a box for Property Status.";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt2.innerHTML = "Please select the number of bedrooms you would like.";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>

<SCRIPT TYPE="text/javascript" src="js/FormValidation.js"></script>
<SCRIPT TYPE="text/javascript" Language="JavaScript">
<!--
FormFields = new Array("FName|First Name|TEXT|Y", "Tel|Telephone|TEL|Y");
function SendEmail(){
	if (checkForm()) {
		thisViewForm.target = "ServerFrame";
		thisViewForm.method="POST";
		thisViewForm.action="ServerSide/SendView.asp";	
		thisViewForm.submit();
		}
}
//-->
</SCRIPT>
<SCRIPT TYPE="text/javascript" Language="JavaScript">
<!--

function FormCancel(){
document.thisForm.reset()
}
	
//-->
</SCRIPT>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr> 
<td align="center" valign="middle"> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr> 
<td> 
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;"> 
<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="Images/our+properties+title.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="91">&nbsp;</td>
<td> 
<table width=705 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td rowspan=2> <img src="images/_gif_22.gif" width=6 height=395 alt=""></td>
<td colspan="2" height=24 bgcolor="#FFFFFF" align="right"><img src="images/curve.gif" width=690 height=24 alt=""></td>
</tr>
<tr> 
<td width=9 height=370 bgcolor="#FFFFFF" valign="top"> 
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="15"><img src="Images/spacer.gif" width="15" height="1"></td>
<td width="343" valign="top"> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<form name="thisForm" method="post">
<tr> 
<td colspan=2><img src="images/txt_welcome.gif" width=343 height=30></td>
</tr>
<tr> 
<td colspan="2" height=20></td>
</tr>
<tr> 
<td colspan=2><img src="images/txt_search_info.gif" width=343 height=40><font color=red> 
<div id="errTxt2">&nbsp;<br>
&nbsp;</div>
</font></td>
</tr>
<tr> 
<td width=152 height=17>Property Status</td>
<td width=191 height=17> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td width=13 height=17> 
<% 
isChecked = ""
if (tolet <> "") then
	isChecked = " checked"
End if
%>
<input type=checkbox value=1 name=tolet <%=isChecked%>>
</td>
<td width=42 height=17>To Let</td>
<td width=20 height=17></td>
<td width=13 height=17> 
<% 
isChecked = ""
If (forsale <> "") Then
	isChecked = " checked"
End if
%>
<input type=checkbox value=2 name=forsale <%=isChecked%>>
</td>
<td height=17>For Sale </td>
</tr>
</table>
</td>
</tr>
<tr> 
<td width=152 height=17>&nbsp;</td>
<td width=191 height=17>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=17>Select Neighbourhood&nbsp;&nbsp;</td>
<td width=191 height=17> 
<select name=Development style="width:150px" class="iagManagerSmallBlk">
<option value="">ANY</option>
<%
While (NOT rsDD.EOF)
isSelected = ""
If (CInt(rsDD.Fields.Item("DevelopmentID").Value) = CInt(nhc)) Then
	isSelected = " selected"
End if
%>
<option value="<%=(rsDD.Fields.Item("DevelopmentID").Value)%>" <%=isSelected%>><%=(rsDD.Fields.Item("DevelopmentName").Value)%></option>
<%
  rsDD.MoveNext()
Wend
If (rsDD.CursorType > 0) Then
  rsDD.MoveFirst
Else
  rsDD.Requery
End If
  rsDD.close()
%>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=15>Number of Bedrooms </td>
<td width=191 height=15> 
<select name=bedrooms style="width:150px" class="iagManagerSmallBlk">
<option value="ALL">ALL</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "1") then 
 isSelected = " selected"
End if 
%>
<option value='1'<%=isSelected%>>1 
Bedroom</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "2") then 
 isSelected = " selected"
End if 
%>
<option value='2'<%=isSelected%>>2 
Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "3") then 
 isSelected = " selected"
End if 
%>
<option value='3'<%=isSelected%>>3 
Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "4") then 
 isSelected = " selected"
End if 
%>
<option value='4'<%=isSelected%>>4 
or more</option>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp; </td>
</tr>
<tr> 
<td colspan=2 width=343 height=15> 
<table border=0 cellpadding=0 cellspacing=0 width=343>
<tr> 
<td colspan=6 width=276 height=19>&nbsp; </td>
<td colspan=2> 
<input type=button value=' Search' onClick="checkData()" name="button22" class="iagButton">
</td>
<td width=17 height=19></td>
</tr>
</table>
</td>
</tr>
</form>
</table>
</td>
<td width="15"><img src="Images/spacer.gif" width="15" height="1">&nbsp;</td>
<td valign="top"> 
<table border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height=30><img src="images/txt_fah.gif" width=170 height=30></td>
</tr>
<tr> 
<td height=20></td>
</tr>
<tr> 
<td width=306> 
<div id="thisResponse" name="thisResponse"> 
<table cellspacing="0" cellpadding="0" border="0" width="400" class="iagManagerTable">
<tr> 
<td colspan="3"><img src="Images/txt_further_information.gif" width="182" height="12"></td>
</tr>
<tr> 
<td colspan="3" height="13">&nbsp;</td>
</tr>
<form name="thisViewForm">
<tr> 
<td width="160">Title</td>
<td width="10">&nbsp;</td>
<td> 
<select name="Title" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:120px; border: 1px solid #4D6675">
<option value="Mr">Mr</option>
<option value="Mrs">Mrs</option>
<option value="Ms">Ms</option>
<option value="Miss">Miss</option>
</select>
</td>
</tr>
<tr> 
<td width="160">First Name(s)&nbsp;</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="FName" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
<font color="#FF0000">*</font> </td>
</tr>
<tr> 
<td width="160">Surname</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="SName" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Gender</td>
<td width="10">&nbsp;</td>
<td> 
<table width="200" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td> 
<select name="Gender" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:120px; border: 1px solid #4D6675">
<option>Male</option>
<option>Female</option>
</select>
</td>
<td  width="50" align="right" class="iagManagerSmallBlk">Age</td>
<td  width="10">&nbsp;</td>
<td width="50"> 
<input type="text" name="Age" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:50px; border: 1px solid #4D6675">
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td align=right colspan="3"></td>
</tr>
<tr> 
<td width="160">Address Line 1</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Address1" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Address Line 2</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Address2" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Address Line 3</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Address3" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Town/City</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="TownCity" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">County</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="County" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">PostCode</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="PostCode" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Mobile</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Mobile" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Email</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Email" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td width="160">Tel</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="Tel" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
<font color="#FF0000">*</font> </td>
</tr>
<tr> 
<td width="160">Current Housing Provider</td>
<td width="10">&nbsp;</td>
<td> 
<input type="text" name="CHP" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; width:200px; border: 1px solid #4D6675">
</td>
</tr>
<tr> 
<td align=right width="160"><iframe name=ServerFrame style='display:none'>&nbsp;</iframe><font color="#FF0000">* 
Required Fields&nbsp;</font></td>
<td align=right width="10">&nbsp;</td>
<td> 
<input type="hidden" name="AssetID" value="<%= theAsset %>">
<input type=button value='View' name="button" class="iagButton" onClick="SendEmail();">
&nbsp; 
<input type=reset value=' Clear ' name="reset" class="iagButton" onClick="FormCancel()">
&nbsp; </td>
</tr>
</form>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td height=370 bgcolor="#FFFFFF" valign="top">&nbsp; </td>
</tr>
<tr> 
<td> <img src="images/_gif_41.gif" width=6 height=8 alt=""></td>
<td bgcolor="#2E3192"> <img src="images/_gif_42.gif" width=686 height=8 alt=""></td>
<td width="13"> <img src="images/_gif_43.gif" width=13 height=8 alt=""></td>
</tr>

</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
</BODY>
</HTML>