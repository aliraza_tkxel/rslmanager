<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!--#include file="../Connections/db_connection.asp" --><!-- #Include file="ADOVBS.INC" -->
<%
	Application("MSSQLConnectionString") = "provider=SQLOLEDB;server=wilma;uid=rslManagerDemoUser;pwd=WebUser;database=rsl manager demo"

	Dim arrChk, arrCnt, strArea, theMax, tblDetail, intAction
	
	response.write Session.sessionid
	
	strArea = Request("area")
	arrCHK = Split(Request("CHK"), ",")
	intAction = Request("action")
	response.write "action = " & intAction
	set rsCommon = Server.CreateObject("ADODB.Recordset")
	rsCommon.ActiveConnection = RSL_CONNECTION_STRING
	rsCommon.CursorType = 0
	rsCommon.CursorLocation = 2
	rsCommon.LockType = 3
	
	// create recordset for basket details
	Set rsRepair = Server.CreateObject("ADODB.Recordset")
	rsRepair.ActiveConnection = RSL_CONNECTION_STRING
	rsRepair.CursorType = 0
	rsRepair.CursorLocation = 2
	rsRepair.LockType = 3
	
	//<tr><td colspan=4 width=800px bgcolor='#912d45' style='color:white' class='rrTop'>&nbsp;Repairs Reporter</td></tr>" &_
	
	tblDetail = "<table border=0>" &_	
				"<tr><td colspan=4><img src='Images/arrange+your+appointment.jpg'></td></tr>" &_
				"<tr><td colspan=4>&nbsp;</td></tr>" &_
				"<td colspan=4><img src='Images/arrange+an+appointment.gif' style='cursor:url(images/hand-rl.cur)' onclick='loadApp()'></td></tr>" &_
				"<tr><td colspan=4>&nbsp;</td></tr><tr><td colspan=4 class='rrTop'><u><b>Your Repairs Basket</b></u></td></tr>" &_
				"<tr><td colspan=4>&nbsp;</td></tr>"
	
	If intAction = 1 Then doInsert() End If
	buildTable()
	
	Function doInsert()
	
		rsCommon.source = "insert into rrid values (" & Session("RSLID") & "," & Session("TenancyID") & ",'" & strArea & "', " & Session.SessionID & ", 1, '" & Session("repairType") & "', null, null)" 
		rsCommon.open()
		rsCommon.source = "select max(rr_id) as theid from rrid"
		rsCommon.open()
		theMax = rsCommon("theid")
		rsCommon.close()
		Call NewRecord(theMax, strArea)
		
		For each arrCnt in arrChk
		
			response.write "<BR>" & arrCnt
			rsCommon.source = "insert into rrDetail values (" & theMax & "," & arrCnt & ")"
			rsCommon.Open()
			
		Next
	
	End Function
	
	
	Function buildTable()
		
		Dim rrID, detCnt, idCnt, sessID
		
		rsCommon.source = "select * from rrid  where rr_session = " & Session.SessionID
		response.write "<BR><BR>" & rsCommon.source
		rsCommon.open()
		idCnt = 0
		
		While Not rsCommon.EOF
			
			sessID = Cstr(rsCommon("rr_Session")) & "/" & Cstr(rsCommon("rr_id"))
			detCnt = 0
			rrID = rsCommon("rr_ID")
			tblDetail = tblDetail & "<tr><td><table border=0><tr><td style='width:200px' class='rrTop'>ID - " & sessID &_
					 "&nbsp;&nbsp;&nbsp;<img src='Images/add.gif' width=15px height=15px style='cursor:url(images/hand-rl.cur)' title='Amend'>" &_
					 "&nbsp;<img src='Images/smalldelete.gif' width=15px height=15px style='cursor:url(images/hand-rl.cur)' title='Delete'></td>" 
			rsRepair.source = "select * from rrdetail d, rrMenuItemOption o where d.rrDetail_problemID = o.menuItemOption_id " &_
								"and d.rr_ID = " & rrID
			response.write "<BR><BR>" & rsRepair.source
			rsRepair.open()
			
			While Not rsRepair.EOF
				
				If detCnt = 0 Then
					tblDetail = tblDetail & "<td class='rrTop'>" & (detCnt + 1) & "." & rsRepair("menuitemoption_desc") & "</td></tr>"
				Else
					tblDetail = tblDetail & "<tr><td>&nbsp;</td><td class='rrTop'>" & (detCnt + 1) & "." & rsRepair("menuitemoption_desc") & "</td></tr>"
				End If
				detCnt = detCnt + 1
				rsRepair.movenext()
			
			Wend
			rsRepair.close()
			rsCommon.movenext()
			tblDetail = tblDetail & "</table><HR></td></tr>"
		
		Wend
		
		if detCnt = 0 Then 
			tblDetail = tblDetail & "<tr><td class='rrTop'>Your basket is currently empty.</td></tr>"
		End If
		
	End Function
	

	Function NewRecord (intID, strAr)
		
		Dim cuID
		
		set rsDets = Server.CreateObject("ADODB.Recordset")
		rsDets.ActiveConnection = RSL_CONNECTION_STRING
		rsDets.Source = strIn
		rsDets.CursorType = 0
		rsDets.CursorLocation = 2
		rsDets.LockType = 3
		rsDets.Source = "select * from customerid c, rsltenancy t where t.tenancyid = " & Session("TenancyID") & " and t.customerid = c.customerid"
		rsDets.Open()
		
		cuId = rsDets("customerid").value
		
		rsDets.close()
		Set rsDets = Nothing
		
		//---------------------------------------------------
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open Application("MSSQLConnectionString")
		
		Set Rs = Server.CreateObject("ADODB.Command")
	
		Set Rs.activeconnection = Conn
		Rs.commandtype = 4 
		Rs.commandtext = "TheIdentity"
		Rs.parameters(1) = cuID
		Rs.execute
		
		enquiryID = Rs.parameters(0)
		Set Rs = Nothing
	
		Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.Open "SELECT * FROM CUSTOMERENQUIRY", Conn, adopenstatic, adLockOptimistic
		Rs.AddNew	
		
		Rs("TakenBy") = null
		Rs("ModifiedBy") = null	
		Rs("IsCurrent") = 1
		Rs("IsCurrentOriginal") = 1	
		Rs("CustomerID") = cuID
		Rs("enquiryID") = enquiryID
		Rs("Notes") = strAr
		Rs("Category") = 2
		Rs("EnquiryStatus") = 1
		Rs("EnquiryTitle") = "Repair Required"
		Rs("FollowUp") = DateAdd("d", 2, Date)
		Rs("Referrer") = null
		Rs("EnquirySource") = intID
		Rs("MediaSource") = null
		Rs("LearningStatus") = null
		Rs("EmploymentStatus") = null
		Rs("Nature") = 7
		Rs("Type") = null	
		Rs("PriorityGroup") = null
		Rs("Qualifications") = null
			
		Rs.Update
		
		Set Rs2 = Server.CreateObject("ADODB.Recordset")
		Rs2.Open "SELECT * FROM CUSTOMERACTIVITY", Conn, adopenstatic, adLockOptimistic
		Rs2.AddNew	
		Rs2("TakenBy") = null
		Rs2("ModifiedBy") = null	
		Rs2("IsCurrent") = 1
		Rs2("CustomerID") = cuID
		Rs2("enquiryID") = enquiryID
		notes = "Repair Activity"
		Rs2("Notes") = null
		Rs2("AssignTo") = null
		Rs2("ActivityStatus") = 2	
		Rs2("ActivityTitle") = "Repair Enquiry to be progressed"
		Rs2("Action") = 5
		followUp = null
		Rs2("FollowUp") = DateAdd("d", 2, Date)
		Rs2("ReferredTo") = null	

	Rs2.Update

	Rs.Close
	Set Rs = Nothing
	Rs2.Close
	Set Rs2 = Nothing
	
	Conn.Close
	Set Conn = Nothing
		
End Function

	
	tblDetail = tblDetail & "</table>"
	
	Set rsRepair = Nothing
	//rsCommon.close()
	Set rsCommon = Nothing
	
%>
<HTML>
<HEAD>
<META http-equiv="Page-Enter" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<META http-equiv="Page-Exit" CONTENT="revealTrans(Duration=0.000,transition=5)" />
<TITLE>Repair Reporter Server</TITLE>
<link rel="stylesheet" href="/css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
</HEAD>
<BODY onload="build()">
<script language="javascript">
	function build(){		
		parent.divMain.innerHTML = "<%=tblDetail%>";		
	}
</script>
</BODY>
</HTML>