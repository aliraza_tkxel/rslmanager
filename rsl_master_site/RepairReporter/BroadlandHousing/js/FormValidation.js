errorTColour = "#FFFFFF";TColour = "#000000";

function checkItem(){
	doc = document.all;
	elName = event.srcElement.name;
	for (i=0;i<FormFields.length;i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		if (tpStr[0] == elName){
			itemValue = doc[tpStr[0]].value;
			blankStatus = isBlank(itemValue);
			doc[tpStr[0]].style.color = TColour;						
			if (!blankStatus) {
				if (tpStr[2] == "CURRENCY") {
					if (isNumeric(tpStr[0], tpStr[1])){
						doc[tpStr[0]].value = FormatCurrency(itemValue);
						doc[tpStr[0]].style.textAlign = "right";
						}
					else {
						doc[tpStr[0]].style.color = "red";				
						return false;
						}
					}
				else if (tpStr[2] == "INTEGER"){
					if (!isInteger(tpStr[0], tpStr[1])){
						doc[tpStr[0]].style.color = "red";				
						return false;
						}
					}
				else if (tpStr[2] == "POSTCODE"){
					if (!isUKPostCode(tpStr[0], tpStr[1])){
						doc[tpStr[0]].style.color = "red";
						return false;
						}
					}
				else if (tpStr[2] == "EMAIL"){
					if (!checkEmail(tpStr[0], tpStr[1])){
						doc[tpStr[0]].style.color = "red";
						return false;
						}
					}
				else if (tpStr[2] == "TELEPHONE"){
					if (!isTelephone(tpStr[0], tpStr[1])){
						doc[tpStr[0]].style.color = "red";				
						return false;
						}
					}
				else if (tpStr[2] == "TEXT"){
					if (!filterText(tpStr[0], tpStr[1])){
						doc[tpStr[0]].style.color = "blue";				
						return false;
						}
					}					
				}
		return true;
			}
	}
}


function checkForm(){
	doc = document.all;
	errTxt = "";
	for (i=0; i<FormFields.length; i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		itemValue = doc[tpStr[0]].value;
		blankStatus = isBlank(itemValue);
		if (tpStr[2] != "RADIO")
			doc[tpStr[0]].style.color = TColour;		
		if (tpStr[3] == "Y" && blankStatus) {
			if (tpStr[2] == "SELECT" || tpStr[2] == "TEXT")
				errTxt += "  " + tpStr[1] + "\n";			
			else if(tpStr[2] == "EMAIL" || tpStr[2] == "INTEGER")
				errTxt += "  " + tpStr[1] + " - must be an " + tpStr[2] + "\n";
			else if(tpStr[2] == "TELEPHONE")
				errTxt += "  " + tpStr[1] + " - must be a NUMBER\n";
			else							
				errTxt += "  " + tpStr[1] + " - must be a " + tpStr[2] + "\n";			
			if (tpStr[2] != "RADIO")
				doc[tpStr[0]].style.color = "red";
			}
		}
		if (errTxt != "") {
			alert("You must complete the following fields to continue:\n" + errTxt);
			return false;
			}
	for (i=0; i<FormFields.length; i++){
		tpStr = FormFields[i];
		tpStr = tpStr.split("|");
		itemValue = doc[tpStr[0]].value;
		blankStatus = isBlank(itemValue);			
		if (!blankStatus) {
			if (tpStr[2] == "CURRENCY") {
				if (isNumeric(tpStr[0], tpStr[1]))
					doc[tpStr[0]].value = FormatCurrency(itemValue);
				else {
					doc[tpStr[0]].style.color = "red";				
					return false;
					}
				}
			else if (tpStr[2] == "INTEGER"){
				if (!isInteger(tpStr[0], tpStr[1])){
					doc[tpStr[0]].style.color = "red";				
					return false;
					}
				}
			else if (tpStr[2] == "POSTCODE"){
				if (!isUKPostCode(tpStr[0], tpStr[1])){
					doc[tpStr[0]].style.color = "red";
					return false;
					}
				}
			else if (tpStr[2] == "EMAIL"){
				if (!checkEmail(tpStr[0], tpStr[1])){
					doc[tpStr[0]].style.color = "red";
					return false;
					}
				}
			else if (tpStr[2] == "TELEPHONE"){
				if (!isTelephone(tpStr[0], tpStr[1])){
					doc[tpStr[0]].style.color = "red";				
					return false;
					}
				}
			else if (tpStr[2] == "TEXT"){
				if (!filterText(tpStr[0], tpStr[1])){
					doc[tpStr[0]].style.color = "blue";				
					return false;
					}
				}								
			}
		}
	return true;
	}

function checkEmail(itemName, errName) {
	doc = document.all;
	elVal = doc[itemName].value;
	if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,7}|[0-9]{1,3})(\]?)$/.test(elVal))
		return (true);
	else {
		alert("You have entered an invalid email for '" + errName + "', please re-enter.\nCorrect formats: [zanfarali@hotmail.com][elephant@inet.org.uk]");
		doc[itemName].focus();
		return (false);
		}
	}

function isBlank(val){
	if(val==null){return true;}
	for(var i=0;i<val.length;i++) {
		if ((val.charAt(i)!=' ')&&(val.charAt(i)!="\t")&&(val.charAt(i)!="\n")&&(val.charAt(i)!="\r")){return false;}
		}
	return true;
	}

function isDigit(num) {
	if (num.length>1){return false;}
	var string="1234567890";
	if (string.indexOf(num)!=-1){return true;}
	return false;
	}

function isUKPostCode (itemName, errName){
	doc = document.all;
	elVal = doc[itemName].value;
	elVal = TrimAll(elVal.toUpperCase());
	result = elVal.match(/^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$/g);
	if (!result) {
		alert("You must input a valid postcode for '" + errName + "',\nCorrect formats: [SW12 2EG][L7 2PR][L17 2EA].");
		doc[itemName].focus();
		return false;	
		}
	else {
		elVal = String(elVal.replace(/\s/g,""));
		elValLen = elVal.length;
		elVal = elVal.substring(0, elValLen-3) + " " + elVal.substring(elValLen-3, elValLen);
		doc[itemName].value = elVal;
		return true;
		}
	}
	
function isTelephone(itemName, errName){
	doc = document.all;
	elVal = doc[itemName].value;
	elVal = elVal.replace(/\s/g, "");	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			alert("You must input a valid Telephone Number for '" + errName + "'.");
			doc[itemName].focus();
			return false;
			}
		}
	doc[itemName].value = TrimAll(doc[itemName].value);		
	return true;
	}

function filterText(itemName, errName) { 
	doc = document.all;
	elVal = doc[itemName].value;
	if (elVal.match(/\<|\>|\"|\'|\%|\;|\&/)){
		elVal = elVal.replace(/\<|\>|\"|\'|\%|\;|\&/g,""); 
		doc[itemName].value = elVal;
		return false;
		}
	else
		return true;
} 
	
function isInteger(itemName, errName){
	doc = document.all;
	elVal = doc[itemName].value;
	elVal = TrimAll(elVal);	
	for(var i=0;i<elVal.length;i++){
		if(!isDigit(elVal.charAt(i))){
			alert("You must input a positive valid number for '" + errName + "'.");
			doc[itemName].focus();
			return false;
			}
		}
	doc[itemName].value = elVal;		
	return true;
	}

function LTrimAll(str) {
	if (str==null){return str;}
	for (var i=0; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i++);
	return str.substring(i,str.length);
	}
	
function RTrimAll(str) {
	if (str==null){return str;}
	for (var i=str.length-1; str.charAt(i)==" " || str.charAt(i)=="\n" || str.charAt(i)=="\t"; i--);
	return str.substring(0,i+1);
	}
	
function TrimAll(str) {
	return LTrimAll(RTrimAll(str));
	}

function isNumeric(itemName, errName){
	doc = document.all;
	elVal = doc[itemName].value;
	elVal = TrimAll(elVal);
	if ( parseFloat(elVal,10) == ( elVal*1) ){
		doc[itemName].value = elVal;
		return true;
		}
	else {
		alert("You must input a positive valid number for '" + errName + "'.");
		doc[itemName].focus();
		return false;
		}
	}

function FormatCurrency ( fPrice ) { 
   var sCurrency = "" + ( parseFloat(fPrice,10) + 0.00500000001 ); 
   var nPos = sCurrency.indexOf ( '.' ); 
   if ( nPos < 0 ){ 
      sCurrency += '.00'; 
   } 
   else { 
      sCurrency = sCurrency.slice ( 0, nPos + 3 ); 
      var nZero = 3 - ( sCurrency.length - nPos ); 
      for ( var i=0; i<nZero; i++ ) 
         sCurrency += '0'; 
   } 
   return sCurrency; 
}

function alignLeft(){
    event.srcElement.style.textAlign = "left";
    }