<HTML>
<HEAD>
<TITLE>Broadland Housing Association Limited</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<SCRIPT TYPE="text/javascript">
<!--
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		reporting_repairs_over = newImage("images/reporting%2brepairs-over.gif");
		//our+properties+menu_over = newImage("images/our+properties+menu-over.gif");
		preloadFlag = true;
	}
}
// -->
</SCRIPT>
<SCRIPT TYPE="text/javascript">
<!--
	function goIn(){	
		divFront.style.display = 'none';
		divIn.style.display = 'block';	
	}
	
	function go_in(){
	
		location.href = 'rrin.asp?theId=' + thisForm.pr1.value;
	
	}
	
// -->
</SCRIPT>
<link rel="stylesheet" href="css/reporter.css" type="text/css">
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 ONLOAD="preloadImages();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr>
<td align="center" valign="middle">
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td>
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr>
<td>
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;">
				<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="images/rr_broadland_15.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL" align="left"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="89">&nbsp;</td>
<td>
<table border=0 cellpadding=0 cellspacing=0>
<form name='thisForm' method='post' target='frmRR'>
<tr> 
<td width=6 height=24><img src="Images/6x24.gif" width="6" height="24"></td>
<td width=7 height=24 bgcolor="#FFFFFF"></td>
<td height=24 width="391" bgcolor="#FFFFFF"></td>
<td width=7 height=24 bgcolor="#FFFFFF"></td>
</tr>
<tr> 
<td bgcolor="#2E3192"></td>
<td bgcolor="#FFFFFF"></td>
<td valign="top" bgcolor="#FFFFFF">
<div id='divFront' named='divFront' width=100% style='overflow:auto;height:371px' class='TA'> 
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="389" valign="top" bgcolor="#FFFFFF"><img src="images/rr_broadland_30.gif" width=367 height=272>&nbsp; 
</td>
</tr>
<tr> 
<td width="389" bgcolor="#FFFFFF">
<input type="button" onClick='goIn()' name="btnSubmit" value="Report A Repair Now" class='rrButtonBig' style='cursor:url(images/hand-rl.cur)'>
</td>
</tr>
</table>
</div>
<div id='divIn' named='divIn' width=100% style='display:none;overflow:auto;height:371px' class='TA'> 
<table width=198 border=0 cellpadding="0" cellspacing="0" valign="top">
<tr height=80px valign=bottom> 
<td width=40px>&nbsp;</td>
<td class='rrTop'>Enter your property reference number (from rent card)</td>
</tr>
<tr> 
<td width=40px>&nbsp;</td>
<td class='rrTop' nowrap> 
<input type="text" name="pr1" class='rrLargeBox' size=6 maxlength=6><div style='visibility:hidden'>
<input type="text" name="pr12" class='rrLargeBox' size=6 maxlength=6>
<input type="text" name="pr2" class='rrLargeBox' size=6 maxlength=6>
<input type="text" name="pr2" class='rrLargeBox' size=6 maxlength=6>
<input type="text" name="pr2" class='rrLargeBox' size=6 maxlength=6></div>
</td>
</tr>
<tr> 
<td width=40px colspan=2>&nbsp;</td>
</tr>
<tr> 
<td width=40px>&nbsp;</td>
<td class='rrTop'>As a security precaution please enter your mothers<br>maiden name as specified on your customer record. </td>
</tr>
<tr> 
<td width=40px>&nbsp;</td>
<td class='rrTop'><input type="text" name="pr12" class='rrLargeBox' size=40 maxlength=40></td>
</tr>
<tr> 
<td width=40px colspan=2>&nbsp;</td>
</tr>
<tr> 
<td width=40px>&nbsp;</td>
<td class='rrTop'> 
<input type="button" name="btnSubmit" class='rrButton' value='Submit' onClick="go_in()" style='cursor:url(images/hand-rl.cur)'>
</td>
</tr>
</table>
</div>
</td>
<td bgcolor="#FFFFFF"></td>
</tr>
<tr> 
<td><img src="images/rr_broadland_39.gif" width=6 height=8 alt=""></td>
<td bgcolor="#2E3192" width=7 height=8></td>
<td bgcolor="#2E3192" width=198 height=8></td>
<td width="7"><img src="images/rr_broadland_41.gif" width=7 height=8 alt=""></td>
</tr>
</form>
</table>
</td>
<td valign="top"> 
<table border=0 cellpadding=0 cellspacing=0>
<tr> 
<td><img src="images/rr_broadland_25.gif" width=13 height=403 alt=""></td>
<td><img src="images/rr_broadland_26.gif" width=279 height=403 alt=""></td>
<td width=4 height=403 bgcolor="#00A651"></td>
</tr>
</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
<IFRAME width=500px height=300px name='frmRR' style='display:none'></IFRAME>
</BODY>
</HTML>