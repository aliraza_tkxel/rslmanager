<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% Session.LCID = 2057 %>
<!--#include file="../Connections/db_connection.asp" --><!-- #Include file="ADOVBS.INC" -->
<%
	dim count
	dim crnstring, namestring, enqstring, categorystring

	dim mypage
	mypage = CInt(Request("page"))
	
	nh = Request("Development")
	if (nh = "") then
		nhc = -1
	else
		nhc = nh
	end if
	
	postcode = Request("postcode")
	tolet = Request("tolet")
	forsale = Request("forsale")
	bedrooms = Request("bedrooms")
	
	bedroom_sql = ""
	if (bedrooms = "ALL") then
		bedroom_sql = ""	
	elseif (bedrooms >= 4) Then
		bedroom_sql = " and bedroomcount >= 4"
	else
		bedroom_sql = " and bedroomcount = " & bedrooms
	end if
	
	stat_sql = ""
	if (tolet <> "" AND forsale <> "") then
		stat_sql = " and (rslAsset.PropStatID = 1 OR rslAsset.PropStatID = 2)"
	elseif (tolet <> "" and  forsale = "") then
		stat_sql = " and (rslAsset.PropStatID = 1)"
	elseif (tolet = "" and  forsale <> "") then
		stat_sql = " and (rslAsset.PropStatID = 2)"
	end if		
	
	nh_sql = ""
	if (nh <> "" and nh <> -1) then
		nh_sql = " and rslAsset.NeighbourHoodID = " & nh
	end if

	postcode_sql = ""
	if (postcode <> "") then
		postcode_sql = " and (rslLocation.Postcode like '%" & postcode & "%')"
	end if
		
	
		'StrEventQuery =	"SELECT BedroomCount, rslAvailability.Availability, rslAsset.AssetID, rslAsset.PropStatID, rslPropertyStatus.PropStat, rslOwner.Owner, "&_
		'"rslAsset.PicUrl, rslDevelopments.DevelopmentName, rslDwelling.Dwelling, rslLocation.Address1, rslLocation.Address2, "&_
		'"rslLocation.Address3, rslLocation.Address4, rslLocation.Postcode, rslLocation.County, "&_
		'"rslLocation.Telephone, rslLocation.HouseNumber, rslLevel.[Level], rslAssetType.AssetType, "&_ 
		'"rslTaxBand.TaxBand, rslServiceChargeCategory.ServiceCat, rslRentCat.RentCat, isNull(rslRentCat.RentRate,0) as RentRate, rslAsset.CapVal, isNull(rslAsset.OpenMarkVal,0) as OpenMarkVal, "&_
		'"rslAgeCat.AgeCat FROM rslAsset "&_
		'"LEFT OUTER JOIN rslLevel ON rslAsset.LevelID = rslLevel.LevelID LEFT OUTER JOIN "&_
		'"rslAgeCat ON rslAsset.AgeID = rslAgeCat.AgeID LEFT OUTER JOIN "&_
		'"rslAssetType ON rslAsset.TypeID = rslAssetType.AssetTypeID LEFT OUTER JOIN "&_
		'"rslRentCat ON rslAsset.RentID = rslRentCat.RentID LEFT OUTER JOIN "&_
		'"rslServiceChargeCategory ON rslAsset.ServiceCatID = rslServiceChargeCategory.ServiceCatID LEFT OUTER JOIN "&_
		'"rslTaxBand ON rslAsset.TaxBandID = rslTaxBand.TaxBandID LEFT OUTER JOIN "&_
		'"rslLocation ON rslAsset.AssetID = rslLocation.AssetID LEFT OUTER JOIN "&_
		'"rslOwner ON rslAsset.OwnerID = rslOwner.OwnerID LEFT OUTER JOIN "&_
		'"rslDwelling ON rslAsset.DwellingID = rslDwelling.DwellingID LEFT OUTER JOIN "&_	
		'"rslDevelopments ON rslAsset.DevelopmentID = rslDevelopments.DevelopmentID LEFT OUTER JOIN "&_
		'"rslPropertyStatus ON rslPropertyStatus.PropStatID = rslAsset.PropStatID LEFT OUTER JOIN "&_
		'"rslAvailability ON rslPropertyStatus.AvailabilityID = rslAvailability.AvailabilityID "&_
		'"left join bedroomCount on rslAsset.Assetid = BedroomCount.assetid where rslAsset.AssetID = rslAsset.AssetID " &_
		'postcode_sql & stat_sql & bedroom_sql & nh_sql
		
		
		StrEventQuery =	"SELECT rslAsset.NeighbourHoodID As [Dev], BedroomCount, rslAvailability.Availability, rslAsset.AssetID, rslAsset.PropStatID, rslPropertyStatus.PropStat, rslOwner.Owner, "&_
		"rslAsset.PicUrl, rslNeighbourHood.NeighbourHood, rslDwelling.Dwelling, rslLocation.Address1, rslLocation.Address2, "&_
		"rslLocation.Address3, rslLocation.Address4, rslLocation.Postcode, rslLocation.County, "&_
		"rslLocation.Telephone, rslLocation.HouseNumber, rslLevel.[Level], rslAssetType.AssetType, "&_ 
		"rslTaxBand.TaxBand, rslServiceChargeCategory.ServiceCat, rslRentCat.RentCat, isNull(rslRentCat.RentRate,0) as RentRate, rslAsset.CapVal, isNull(rslAsset.RentValue,0) as RentValue, "&_
		"rslAgeCat.AgeCat FROM rslAsset "&_
		"LEFT OUTER JOIN rslLevel ON rslAsset.LevelID = rslLevel.LevelID LEFT OUTER JOIN "&_
		"rslAgeCat ON rslAsset.AgeID = rslAgeCat.AgeID LEFT OUTER JOIN "&_
		"rslAssetType ON rslAsset.TypeID = rslAssetType.AssetTypeID LEFT OUTER JOIN "&_
		"rslRentCat ON rslAsset.RentID = rslRentCat.RentID LEFT OUTER JOIN "&_
		"rslServiceChargeCategory ON rslAsset.ServiceCatID = rslServiceChargeCategory.ServiceCatID LEFT OUTER JOIN "&_
		"rslTaxBand ON rslAsset.TaxBandID = rslTaxBand.TaxBandID LEFT OUTER JOIN "&_
		"rslLocation ON rslAsset.AssetID = rslLocation.AssetID LEFT OUTER JOIN "&_
		"rslOwner ON rslAsset.OwnerID = rslOwner.OwnerID LEFT OUTER JOIN "&_
		"rslDwelling ON rslAsset.DwellingID = rslDwelling.DwellingID LEFT OUTER JOIN "&_
		"rslNeighbourHood ON rslAsset.NeighbourHoodID = rslNeighbourHood.NeighbourHoodID LEFT OUTER JOIN "&_
		"rslPropertyStatus ON rslPropertyStatus.PropStatID = rslAsset.PropStatID LEFT OUTER JOIN "&_
		"rslAvailability ON rslPropertyStatus.AvailabilityID = rslAvailability.AvailabilityID "&_
		"left join bedroomCount on rslAsset.Assetid = BedroomCount.assetid where rslAsset.OrgID = 258 AND rslAsset.AssetID = rslAsset.AssetID " &_
		postcode_sql & stat_sql & bedroom_sql & nh_sql

	'Response.Write strEventQuery 
	
	if mypage = 0 then mypage = 1 end if
	
	pagesize = 2
	
	Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open "provider=SQLOLEDB;server=wilma;uid=rslManager;pwd=WebUser;database=rsl Manager"
		'Application("Cal_MSSQLConnectionString")

	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
		Rs.CursorLocation = adUseClient

		Rs.Open strEventQuery, Conn, adOpenForwardOnly, adLockReadOnly, adCmdText

	numpages = Rs.PageCount
	numrecs = Rs.RecordCount

' Just in case we have a bad request
	If mypage > numpages Then mypage = numpages 
	If mypage < 1 Then mypage = 1
	
	Dim nextpage, prevpage
	nextpage = mypage + 1
	if nextpage > numpages then 
		nextpage = numpages
	end if
	prevpage = mypage - 1
	if prevpage <= 0 then
		prevpage = 1
	end if
' This line sets the current page
	If Not Rs.EOF then
		Rs.AbsolutePage = mypage
	end if
	
	if (nextpage = 0) then nextpage = 1 end if
	if (numpages = 0) then numpages = 1 end if
%>
<%
Dim Developments
Dim Developments_numRows
set Developments = Server.CreateObject("ADODB.Recordset")
	Developments.ActiveConnection = RSL_CONNECTION_STRING
	Developments.Source = "SELECT *  FROM dbo.rslDevelopments  ORDER BY DevelopmentName ASC"
	Developments.CursorType = 0
	Developments.CursorLocation = 2
	Developments.LockType = 3
	Developments.Open()
	Developments_numRows = 0
%><HTML>
<HEAD>
<TITLE>Find A Home</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<script language=javascript>
function checkData(){
doc= document.all;
errTxt2.innerHTML = "&nbsp;<br>&nbsp;";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt2.innerHTML = "<BR>Please check a box for Property Status.<BR>";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt2.innerHTML = "<BR>Please select the number of bedrooms you would like.<BR>";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr> 
<td align="center" valign="middle"> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr> 
<td> 
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;"> 
<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="Images/our+properties+title.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="91">&nbsp;</td>
<td> 
<table width=705 border=0 cellpadding=0 cellspacing=0>
<form name='thisForm' method='post' target='frmRR'>
<tr> 
<td rowspan=2> <img src="images/_gif_22.gif" width=6 height=395 alt=""></td>
<td colspan="2" height=24 bgcolor="#FFFFFF" align="right"><img src="images/curve.gif" width=690 height=24 alt=""></td>
</tr>
<tr> 
<td width=9 height=370 bgcolor="#FFFFFF" valign="top"> 
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="15"><img src="Images/spacer.gif" width="15" height="1"></td>
<td width="343" valign="top"> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td colspan=2><img src="images/txt_welcome.gif" width=343 height=30></td>
</tr>
<tr> 
<td colspan="2" height=20></td>
</tr>
<tr> 
<td colspan=2><img src="images/txt_search_info.gif" width=343 height=40><font color=red> 
<div id="errTxt2">&nbsp;<br>
&nbsp;</div>
</font></td>
</tr>
<tr> 
<td width=152 height=17>Property Status</td>
<td width=191 height=17> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td width=13 height=17> 
<% 
isChecked = ""
if (tolet <> "") then
	isChecked = " checked"
End if
%>
<input type=checkbox value=1 name=tolet <%=isChecked%>>
</td>
<td width=42 height=17>To Let</td>
<td width=20 height=17></td>
<td width=13 height=17> 
<% 
isChecked = ""
If (forsale <> "") Then
	isChecked = " checked"
End if
%>
<input type=checkbox value=2 name=forsale <%=isChecked%>>
</td>
<td height=17>For Sale </td>
</tr>
</table>
</td>
</tr>
<tr> 
<td width=152 height=17>&nbsp;</td>
<td width=191 height=17>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=17>Select Neighbourhood&nbsp;&nbsp;</td>
<td width=191 height=17> 
<select name=Development style="width:150px" class="iagManagerSmallBlk">
<option value="">ANY</option>
<% While (NOT Developments.EOF)
	isSelected = ""										
	if (CInt(Developments.Fields.Item("DevelopmentID").Value) = CInt(nhc)) then
	isSelected = " selected"
	End if
%>
<option value="<%=(Developments.Fields.Item("DevelopmentID").Value)%>" <%=isSelected%>><%=(Developments.Fields.Item("DevelopmentName").Value)%></option>
<%
	Developments.MoveNext()
	Wend
	If (Developments.CursorType > 0) Then
		Developments.MoveFirst
	Else
		Developments.Requery
	End If
%>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=15>Number of Bedrooms </td>
<td width=191 height=15> 
<select name=bedrooms style="width:150px" class="iagManagerSmallBlk">
<option value="ALL">ALL</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "1") Then 
 isSelected = " selected"
End If 
%>
<option value='1'<%=isSelected%>>1 
Bedroom</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "2") Then 
 isSelected = " selected"
End If 
%>
<option value='2'<%=isSelected%>>2 
Bedrooms</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "3") Then 
 isSelected = " selected"
End If 
%>
<option value='3'<%=isSelected%>>3 
Bedrooms</option>
<% 
isSelected = ""
If (CStr(bedrooms) = "4") Then 
 isSelected = " selected"
End If 
%>
<option value='4'<%=isSelected%>>4 
or more</option>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp; </td>
</tr>
<tr> 
<td colspan=2 width=343 height=15> 
<table border=0 cellpadding=0 cellspacing=0 width=343>
<tr> 
<td colspan=6 width=276 height=19>&nbsp; </td>
<td colspan=2> 
<input type=button value=' Search' onClick="checkData()" name="button2" class="iagButton">
</td>
<td width=17 height=19></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td width="15"><img src="Images/spacer.gif" width="15" height="1">&nbsp;</td>
<td valign="top"> 
<table border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height=30><img src="images/txt_fah.gif" width=170 height=30></td>
</tr>
<tr> 
<td height=20></td>
</tr>
<tr> 
<td width=124 height=12><img src="Images/txt_your_results.gif" width="124" height="12"></td>
</tr>
<tr> 
<td idth=306 height=13></td>
</tr>
<tr> 
<td width=306> 
<table cellspadding='0' cellspacing='0' border='0' class='rslManager'>
<%
	count = 0
	For i=1 to pagesize
		If NOT Rs.EOF Then
			if (count <> 0 ) then
				'Response.Write "<TR>"
					'Response.Write "<TD colspan=3 style=""border-top:1px solid silver;line-height:2px"">&nbsp;</td>"
				'Response.Write "</tr>"
			End if
			'Response.Write "<tr class='iagManagerTable'>"

			myArray3 = Array("housenumber","address1","address2","address3","address4","postcode","county")
			myArray4 = Array()
			add_details = "<TABLE Cellspacing=0 cellpadding=0 class='iagManagerTable' width='100'>"
			add_details = add_details & "<TR><TD bgcolor=""#00A651"" style=""padding-left:5px; color:#FFFFFF"">Address</TD></TR><TR><TD height=""5"" style=""padding-left:5px; background-color:#98D4B5""></TD></TR>"	
			
			For j=0 To 6 
				theItem = Rs(myArray3(j))
				If theItem <> "" Then
					if (myArray3(j) = "housenumber") then
						add_details = add_details & "<TR><TD style=""padding-left:5px; color:#000000; background-color:#98D4B5"">" & theItem & "</TD></TR>"
					elseif (myArray3(j) = "telephone") then
						add_details = add_details & "<TR><TD style=""padding-left:5px; color:#000000; background-color:#98D4B5"">TEL: " & theItem & "</TD></TR>"
					else
						add_details = add_details & "<TR><TD style=""padding-left:5px; color:#000000; background-color:#98D4B5"">" & theItem & "</TD></TR>"										
					end if
				End If
			Next
			
			add_details = add_details & "<TR><TD height=""5"" style=""padding-left:5px; background-color:#98D4B5""></TD></TR>"
			add_details = add_details & "</TABLE>"
			
			For j=0 To 6 
				theItem = Rs(myArray3(j))
				If Rs(myArray3(1)) <> "" Then
				Else
				
				add_details = ""
						'add_details = add_details & "<TR><TD style=""padding-left:5px; color:#000000; background-color:#CCD3D8"">&nbsp;</TD></TR>"									
				End If
			Next
			
			If (Rs("PropStatID") = 1) Then
				temp1 = "To Let"
				temp2 = Rs("rentCat")
				temp3 = "�" & FormatNumber(Rs("RentValue"),2) ' was rentRate
				temp4 = "Rent"
			Else
				temp1 = "For Sale"
				temp2 = ""
				temp3 = "�" & FormatNumber(Rs("RentValue"),2)
				temp4 = "Price"
			End if
			
			mypic = Rs("PicUrl")
			
			If isNull(mypic) then 
			mypic = ""
			else 
			mypic = "<img src=""http://www.rslmanager.co.uk/fah/propimages/" & Rs("PicUrl")& """ border=""0"" alt=""No Property Image"" width=""62"" height=""58"">"
			'"" & Rs("PicUrl")& ""
			end If

			%>
<tr class='iagManagerTable'> 
<td valign='top' width="279" height="100"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td align="left" width="120" valign="top"> <%= add_details %> </td>
<td align="left" width="30" valign="top">&nbsp;</td>
<td valign="top"> 
<table cellspadding='0' cellspacing='0' border='0' class='iagManagerTable' cellpadding='0' width="180">
<tr> 
<td valign='top' colspan="2" style="padding-left:5px; color:#FFFFFF"><%=mypic%></td>
</tr>
<tr> 
<td valign='top' height="5" width="100"></td>
<td align='right' width="100" height="5"></td>
</tr>
<% If Rs.Fields.Item("bedroomCount").value <> "" Then %>
<tr> 
<td valign='top' bgcolor='#00A651' width="100" style="padding-left:5px; color:#FFFFFF">No 
Bedrooms:</td>
<td valign='top' align='right' width="100" bgcolor="#98D4B5"><%=Rs.Fields.Item("bedroomCount").value %></td>
</tr>
<% End If %>
<tr> 
<td valign='top' height="5" width="100"></td>
<td align='right' width="100" height="5"></td>
</tr>
<tr> 
<td valign='top' bgcolor='#00A651' width="100" style="padding-left:5px; color:#FFFFFF">Property 
Status:</td>
<td align='right' width="100" bgcolor="#98D4B5"><%=temp1%></td>
</tr>
<tr> 
<td valign='top' height="5" width="100"></td>
<td align='right' width="100" height="5"></td>
</tr>
<tr> 
<td valign='top' bgcolor='#00A651' width="100" style="padding-left:5px; color:#FFFFFF"><%=temp4%>:</td>
<td align='right' width="100" bgcolor="#98D4B5"><%=temp3%></td>
</tr>
<tr align="left"> 
<% Response.Write "<td valign='top' colspan=2 class='iagManagerSmallBlue'>[<a href='further.asp?asset=" &  Rs("AssetID") & "&bedrooms=" & bedrooms & "&forsale=" & forsale & "&tolet=" &tolet & "&postcode=" & postcode & "&Development="  & nh & "' class='iagManagerSmallBlue'>Further info</a>]&nbsp;[<a href='view.asp?asset=" &  Rs("AssetID") & "&bedrooms=" & bedrooms & "&forsale=" & forsale & "&tolet=" &tolet & "&postcode=" & postcode & "&Development="  & nh & "' class='iagManagerSmallBlue'>Arrange to View</a>]</td>" %>
</tr>
<tr align="center"> 
<td valign='top' colspan="2">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr class='iagManagerTable'> 
<td valign='top' width="299"></td>
</tr>
<%
			count = count + 1
			Rs.moveNext
		End If
	Next
	%>
<%
If (count = 0) Then
		If nh = 1 Then   
Response.write "<tr>"
	Response.write "<td colspan=3 align=center><br><br>Please contact our selling agents Dears Brack &amp; Associates on <b>0151 242 1500</b> or email <a href=""mailto:info@dearsbrack.co.uk"">info@dearsbrack.co.uk</a> for current availability and appointment to view.</td>"
Response.write "</tr>"
		ElseIf nh = 4 Or nh = 5 Then  
Response.write "<tr>"
	Response.write "<td colspan=3 align=center><br><br>Please contact Atrium City Living on <b>0151 708 2404 </b> or email <a href=""mailto:atrium@lht.co.uk"">atrium@lht.co.uk</a> for current availability and appointment to view.</td>"
Response.write "</tr>"
		Else		
Response.write "<tr>"
	Response.write "<td colspan=3 align=center><br><br>No properties found for the specified criteria.<br>Please change your search criteria and try again.</td>"
Response.write "</tr>"
		End If
End If
%>
<% If (count <> 0 ) Then %>
<tr> 
<td style='border-top:1px solid silver;line-height:2px'>&nbsp;</td>
</tr>
<tr class='iagManagerTable'> 
<td align='center' nowrap><a href="results.asp?page=1&bedrooms=<%=bedrooms%>&forsale=<%=forsale%>&tolet=<%=tolet%>&postcode=<%=postcode%>&Development=<%=nh%>" class="iagManagerSmallBlue">FIRST</a> 
<a href="results.asp?page=<%=prevpage%>&bedrooms=<%=bedrooms%>&forsale=<%=forsale%>&tolet=<%=tolet%>&postcode=<%=postcode%>&Development=<%=nh%>" class="iagManagerSmallBlue">PREV</a> 
Page <%=mypage%> of <%=numpages%>.&nbsp;<%=numrecs%>&nbsp;Records Total&nbsp;<a class="iagManagerSmallBlue" href="results.asp?page=<%=nextpage%>&bedrooms=<%=bedrooms%>&forsale=<%=forsale%>&tolet=<%=tolet%>&postcode=<%=postcode%>&Development=<%=nh%>">NEXT</a> 
<a class="iagManagerSmallBlue" href="results.asp?page=<%=numpages%>&bedrooms=<%=bedrooms%>&forsale=<%=forsale%>&tolet=<%=tolet%>&postcode=<%=postcode%>&Development=<%=nh%>">LAST</a></td>
</tr>
<% End if %>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td height=370 bgcolor="#FFFFFF" valign="top">&nbsp; </td>
</tr>
<tr> 
<td> <img src="images/_gif_41.gif" width=6 height=8 alt=""></td>
<td> <img src="images/_gif_42.gif" width=686 height=8 alt=""></td>
<td width="13"> <img src="images/_gif_43.gif" width=13 height=8 alt=""></td>
</tr>
</form>
</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
</BODY>
</HTML>