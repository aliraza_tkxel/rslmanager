<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<% Session.LCID = 2057 %>
<!--#include file="../Connections/db_connection.asp" --><!-- #Include file="ADOVBS.INC" -->
<%
Dim rsDevelopments
Dim rsDevelopments_numRows
set rsDevelopments = Server.CreateObject("ADODB.Recordset")
	rsDevelopments.ActiveConnection = RSL_CONNECTION_STRING
	rsDevelopments.Source = "SELECT *  FROM dbo.rslDevelopments  ORDER BY DevelopmentName ASC"
	rsDevelopments.CursorType = 0
	rsDevelopments.CursorLocation = 2
	rsDevelopments.LockType = 3
	rsDevelopments.Open()
	rsDevelopments_numRows = 0
%>

<%
	dim count
	dim crnstring, namestring, enqstring, categorystring

	dim mypage
	mypage = CInt(Request("page"))
	
	nh = Request("Development")
	if (nh = "") then
		nhc = -1
	else
		nhc = nh
	end if	
	postcode = Request("postcode")
	tolet = Request("tolet")
	forsale = Request("forsale")
	bedrooms = Request("bedrooms")
	theAsset = Request("asset")
	
	sqlFilter = " and rslAsset.AssetID = " & theAsset
	
	' -------------
	' Modified By 	Robert Egan
	' Modified Date 03/09/2003
	' -------------
	
	StrEventQuery =	"SELECT rslTaxBand.TaxBand, rslAsset.NeighbourHoodID As [Dev], ISNULL(BedroomCount,0) AS BedroomCount, rslAvailability.Availability, rslAsset.AssetID, rslAsset.PropStatID, rslPropertyStatus.PropStat, rslOwner.Owner, "&_
		"rslAsset.PicUrl, rslNeighbourHood.NeighbourHood, rslDwelling.Dwelling, rslLocation.Address1, rslLocation.Address2, "&_
		"rslLocation.Address3, rslLocation.Address4, rslLocation.Postcode, rslLocation.County, "&_
		"rslLocation.Telephone, rslLocation.HouseNumber, rslLevel.[Level], rslAssetType.AssetType, "&_ 
		"rslTaxBand.TaxBand, rslServiceChargeCategory.ServiceCat, rslRentCat.RentCat, isNull(rslRentCat.RentRate,0) as RentRate, rslAsset.CapVal, isNull(rslAsset.RentValue,0) as RentValue, "&_
		"rslAgeCat.AgeCat FROM rslAsset "&_
		"LEFT OUTER JOIN rslLevel ON rslAsset.LevelID = rslLevel.LevelID LEFT OUTER JOIN "&_
		"rslAgeCat ON rslAsset.AgeID = rslAgeCat.AgeID LEFT OUTER JOIN "&_
		"rslAssetType ON rslAsset.TypeID = rslAssetType.AssetTypeID LEFT OUTER JOIN "&_
		"rslRentCat ON rslAsset.RentID = rslRentCat.RentID LEFT OUTER JOIN "&_
		"rslServiceChargeCategory ON rslAsset.ServiceCatID = rslServiceChargeCategory.ServiceCatID LEFT OUTER JOIN "&_
		"rslTaxBand ON rslAsset.TaxBandID = rslTaxBand.TaxBandID LEFT OUTER JOIN "&_
		"rslLocation ON rslAsset.AssetID = rslLocation.AssetID LEFT OUTER JOIN "&_
		"rslOwner ON rslAsset.OwnerID = rslOwner.OwnerID LEFT OUTER JOIN "&_
		"rslDwelling ON rslAsset.DwellingID = rslDwelling.DwellingID LEFT OUTER JOIN "&_
		"rslNeighbourHood ON rslAsset.NeighbourHoodID = rslNeighbourHood.NeighbourHoodID LEFT OUTER JOIN "&_
		"rslPropertyStatus ON rslPropertyStatus.PropStatID = rslAsset.PropStatID LEFT OUTER JOIN "&_
		"rslAvailability ON rslPropertyStatus.AvailabilityID = rslAvailability.AvailabilityID "&_
		"left join bedroomCount on rslAsset.Assetid = BedroomCount.assetid where rslAsset.OrgID = 258 AND rslAsset.AssetID = rslAsset.AssetID " &_
		sqlFilter
	
	'Response.Write strEventQuery 
	
	if mypage = 0 then 
	   mypage = 1 
	end if
	
	pagesize = 5
	
	Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open "provider=SQLOLEDB;server=wilma;uid=rslManager;pwd=WebUser;database=rsl Manager"
		'Application("Cal_MSSQLConnectionString")

	Set Rs = Server.CreateObject("ADODB.Recordset")
		Rs.PageSize = pagesize
		Rs.CacheSize = pagesize
		Rs.CursorLocation = adUseClient
		Rs.Open strEventQuery, Conn, adOpenForwardOnly, adLockReadOnly, adCmdText

%>
<%
Dim GenAtt__r_varAsset
	GenAtt__r_varAsset = "282"
if (Request("Asset") <> "") then GenAtt__r_varAsset = Request("asset")
%>
<%
Dim GenAtt
Dim GenAtt_numRows
set GenAtt = Server.CreateObject("ADODB.Recordset")
	GenAtt.ActiveConnection = RSL_CONNECTION_STRING
	GenAtt.Source = "SELECT rslAttributeCategory.CategoryName, TheAttribute = CASE when     rslAttributes.AttributeName is null then r2.AttributeName else     rslAttributes.AttributeName end, rslGeneralAttributes.*  from   (select  yyy = case when (isNumeric(theValue)=1) then theValue else -1 end,*  FROM rslGeneralAttributes)  as rslGeneralAttributes   inner join rslAsset on rslGeneralAttributes.AssetId = rslAsset.AssetId  left join rslAttributeCategory on rslAttributeCategory.AttributeCategoryId =     rslGeneralAttributes.CategoryId  left join rslAttributes on rslAttributes.AttributeID = rslGeneralAttributes.AttributeID  left join rslAttributes r2 on r2.AttributeID = rslGeneralAttributes.yyy  WHERE rslAsset.AssetID = " + Replace(GenAtt__r_varAsset, "'", "''") + ""
	GenAtt.CursorType = 0
	GenAtt.CursorLocation = 2
	GenAtt.LockType = 3
	GenAtt.Open()
	GenAtt_numRows = 0
%>
<%
Dim Repeat1__numRows
	Repeat1__numRows = -1
Dim Repeat1__index
	Repeat1__index = 0
	GenAtt_numRows = GenAtt_numRows + Repeat1__numRows
%>
<HTML>
<HEAD>
<TITLE>Atrium | Find-a-Home</TITLE>
<link rel="stylesheet" href="css/iagManager.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<script language=javascript>
function checkData(){
doc= document.all;
errTxt.innerHTML = "&nbsp;<br>&nbsp;";
if (!doc["tolet"].checked && !doc["forsale"].checked){
	errTxt.innerHTML = "Please check a box for<br>Property Status.";
	return false;
	}
if (doc["bedrooms"].value == ""){
	errTxt.innerHTML = "Please select the number<br>of bedrooms you would like.";
	return false;
	}
	thisForm.action = "results.asp";
	thisForm.submit();	
}
</script>
</HEAD>
<BODY BGCOLOR=#FFFFFF MARGINHEIGHT=0 LEFTMARGIN="0" TOPMARGIN="0" MARGINWIDTH="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
<tr> 
<td align="center" valign="middle"> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td> 
<table width="884" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td valign="top" width="736" height="61"> 
<table border="0" cellspacing="0" cellpadding="0" width="593">
<tr> 
<td> 
<table width=593 border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height="23"><img src="images/rr_broadland_01.gif" width=116 height=23></td>
<td height="23"><a href="FindAHome.asp"
				onMouseOver="changeImages('our+properties+menu', 'images/our+properties+menu-over.gif'); return true;"
				onMouseOut="changeImages('our+properties+menu', 'images/our+properties+menu.gif'); return true;"> 
<img name="our+properties+menu" src="images/our+properties+menu.gif" width=97 height=23 alt="Our Properties" border="0"></a></td>
<td height="23"><img src="images/rr_broadland_03.gif" width=268 height=23></td>
<td height="23"><a href="#"
				onMouseOver="changeImages('reporting_repairs', 'images/reporting%2brepairs-over.gif'); return true;"
				onMouseOut="changeImages('reporting_repairs', 'images/reporting%2brepairs.gif'); return true;"> 
<img name="reporting_repairs" src="images/reporting%2brepairs.gif" width=112 height=23 border=0 alt="Reporting Repairs"></a></td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="38" class="ThinLineL">&nbsp;</td>
</tr>
</table>
</td>
<td width="148" align="right"><img src="images/rr_broadland_06.gif" width=148 height=61 alt=""></td>
</tr>
<tr> 
<td valign="top" align="right" colspan="2"> 
<table border="0" cellspacing="0" cellpadding="0" width="884" class="ThinLineL">
<tr> 
<td width="109" height="59">&nbsp;</td>
<td valign="bottom" width="122" height="59"><img src="Images/our+properties+title.gif" width=122 height=15 alt=""></td>
<td align="right" width="282" height="59">&nbsp;</td>
<td align="right" width="283" height="59"><img src="images/rr_broadland_12.gif" width=283 height=59 alt=""></td>
<td align="right" width="88" height="59">&nbsp;</td>
</tr>
<tr> 
<td width="109" bgcolor="#00A651" height="26">&nbsp;</td>
<td valign="bottom" width="122" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="282" bgcolor="#00A651" height="26">&nbsp;</td>
<td align="right" width="283" bgcolor="#00A651" height="26"><img src="images/rr_broadland_19.gif" width=283 height=26 alt=""></td>
<td align="right" width="88" bgcolor="#00A651" height="26">&nbsp;</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="403" bgcolor="#00A651" valign="top" class="ThinLineL"> 
<table width="882" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="91">&nbsp;</td>
<td> 
<table width=705 border=0 cellpadding=0 cellspacing=0>
<form name='thisForm' method='post' target='frmRR'>
<tr> 
<td rowspan=2> <img src="images/_gif_22.gif" width=6 height=395 alt=""></td>
<td colspan="2" height=24 bgcolor="#FFFFFF" align="right"><img src="images/curve.gif" width=690 height=24 alt=""></td>
</tr>
<tr> 
<td width=9 height=370 bgcolor="#FFFFFF" valign="top"> 
<table border="0" cellspacing="0" cellpadding="0">
<tr> 
<td width="15"><img src="Images/spacer.gif" width="15" height="1"></td>
<td width="343" valign="top"> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td colspan=2><img src="images/txt_welcome.gif" width=343 height=30></td>
</tr>
<tr> 
<td colspan="2" height=20></td>
</tr>
<tr> 
<td colspan=2><img src="images/txt_search_info.gif" width=343 height=40><font color=red> 
<div id="errTxt2">&nbsp;<br>
&nbsp;</div>
</font></td>
</tr>
<tr> 
<td width=152 height=17>Property Status</td>
<td width=191 height=17> 
<table border=0 cellpadding=0 cellspacing=0 class="calTextLge">
<tr> 
<td width=13 height=17> 
<% 
isChecked = ""
if (tolet <> "") then
	isChecked = " checked"
End if
%>
<input type=checkbox value=1 name=tolet <%=isChecked%>>
</td>
<td width=42 height=17>To Let</td>
<td width=20 height=17></td>
<td width=13 height=17> 
<% 
isChecked = ""
If (forsale <> "") Then
	isChecked = " checked"
End if
%>
<input type=checkbox value=2 name=forsale <%=isChecked%>>
</td>
<td height=17>For Sale </td>
</tr>
</table>
</td>
</tr>
<tr> 
<td width=152 height=17>&nbsp;</td>
<td width=191 height=17>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=17>Select Neighbourhood&nbsp;&nbsp;</td>
<td width=191 height=17> 
<select name=Development style="width:150px" class="iagManagerSmallBlk">
<option value="">ANY</option>
<%
While (NOT rsDevelopments.EOF)
isSelected = ""

if (CInt(rsDevelopments.Fields.Item("DevelopmentID").Value) = CInt(nhc)) then
	isSelected = " selected"
End if
%>
<option value="<%=(rsDevelopments.Fields.Item("DevelopmentID").Value)%>" <%=isSelected%>><%=(rsDevelopments.Fields.Item("DevelopmentName").Value)%></option>
<%
  rsDevelopments.MoveNext()
Wend
If (rsDevelopments.CursorType > 0) Then
  rsDevelopments.MoveFirst
Else
  rsDevelopments.Requery
End If
  rsDevelopments.close()
%>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp;</td>
</tr>
<tr> 
<td width=152 height=15>Number of Bedrooms </td>
<td width=191 height=15> 
<select name=bedrooms style="width:150px" class="iagManagerSmallBlk">
<option value="ALL">ALL</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "1") then 
 isSelected = " selected"
End if 
%>
<option value='1'<%=isSelected%>>1 
Bedroom</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "2") then 
 isSelected = " selected"
End if 
%>
<option value='2'<%=isSelected%>>2 
Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "3") then 
 isSelected = " selected"
End if 
%>
<option value='3'<%=isSelected%>>3 
Bedrooms</option>
<% 
isSelected = ""
if (CStr(bedrooms) = "4") then 
 isSelected = " selected"
End if 
%>
<option value='4'<%=isSelected%>>4 
or more</option>
</select>
</td>
</tr>
<tr> 
<td colspan=2 width=343 height=15>&nbsp; </td>
</tr>
<tr> 
<td colspan=2 width=343 height=15> 
<table border=0 cellpadding=0 cellspacing=0 width=343>
<tr> 
<td colspan=6 width=276 height=19>&nbsp; </td>
<td colspan=2> 
<input type=button value=' Search' onClick="checkData()" name="button22" class="iagButton">
</td>
<td width=17 height=19></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td width="15"><img src="Images/spacer.gif" width="15" height="1">&nbsp;</td>
<td valign="top"> 
<table border=0 cellpadding=0 cellspacing=0>
<tr> 
<td height=30><img src="images/txt_fah.gif" width=170 height=30></td>
</tr>
<tr> 
<td height=20></td>
</tr>
<tr> 
<td width=124 height=12><img src="Images/txt_further_information.gif" width="182" height="12"></td>
</tr>
<tr> 
<td idth=306 height=13> 
<%
			myArray3 = Array("housenumber","address1","address2","address3","address4","postcode","county")
	
			For j=0 To 6 
				theItem = Rs(myArray3(j))
				If theItem <> "" Then
					if (myArray3(j) = "housenumber") then
						add_details = add_details & "<TR><TD>" & theItem & "</TD></TR>"
					elseif (myArray3(j) = "telephone") then
						add_details = add_details & "<TR><TD>TEL: " & theItem & "</TD></TR>"
					else
						add_details = add_details & "<TR><TD>" & theItem & "</TD></TR>"										
					end if
				End If
			Next
			
			add_details = add_details
			
			if (Rs("PropStatID") = 1) then
				temp1 = "To Let"
				temp2 = Rs("rentCat")
				temp3 = "�" & FormatNumber(Rs("RentValue"),2)'rentRate
				temp4 = "Rent"
			Else
				temp1 = "For Sale"
				temp2 = ""
				temp3 = "�" & FormatNumber(Rs("RentValue"),2)
				temp4 = "Price"
			End if
		
				mypic = Rs("PicUrl")
			
			if (mypic = "" OR isNull(mypic)) AND Rs("Dev") = 1 then
				mypic = "<img src=""images/propimages/fah_kd.gif"" border=""0"" alt="""& Rs("Neighbourhood")& """ width=""86"" height=""78"">"
			elseif (mypic = "" OR isNull(mypic)) AND Rs("Dev") = 2 then
				mypic = "<img src=""images/propimages/fah_ic.gif"" border=""0"" alt="""& Rs("Neighbourhood")& """ width=""86"" height=""78"">"
			elseif (mypic = "" OR isNull(mypic)) AND Rs("Dev") = 3 then
				mypic = "<img src=""images/propimages/fah_cc.gif"" border=""0"" alt="""& Rs("Neighbourhood")& """ width=""86"" height=""78"">"
			elseif (mypic = "" OR isNull(mypic)) AND Rs("Dev") = 4 then
				mypic = "<img src=""images/propimages/fah_fs.gif"" border=""0"" alt="""& Rs("Neighbourhood")& """ width=""86"" height=""78"">"
			elseif (mypic = "" OR isNull(mypic)) AND Rs("Dev") = "" OR isNull(Rs("Dev")) then
				mypic = "<img src=""images/propimages/default.gif"" border=""0"" alt=""No Property Image"" width=""86"" height=""78"">"
			else mypic = "<img src=""http://www.rslmanager.co.uk/fah/propimages/" & Rs("PicUrl")& """ border=""0"" alt=""No Property Image"" width=""86"" height=""78"">"			
			end if
%>
</td>
</tr>
<tr> 
<td width=306> 
<table cellspacing=0 class="rslManager" border="0" cellpadding="0" width="374">
<tr> 
<td width="100" valign="top"> 
<table width="100" border="0" cellspacing="0" cellpadding="0" class="iagManagerTable">
<tr> 
<td><%= mypic%></td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<% If (add_details) = "" Then %>
<% Else
 
   Dim tbl_add_details
 	 	tbl_add_details = "<TABLE Cellspacing=0 cellpadding=0 class='iagManagerTable'>" 
 	 	tbl_add_details = tbl_add_details & add_details & "</TABLE>"

%>
<tr> 
<td bgcolor="#00A651" style="padding-left:5px; color:#FFFFFF">Address:</td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000"> <%= tbl_add_details %> </td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<% End If %>
<% If Rs("bedroomCount") = "0" Then %>
<% Else %>
<td bgcolor="#00A651" style="padding-left:5px; color:#FFFFFF">No Bedrooms:</td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000"><%= Rs("bedroomCount")%></td>
</tr>
<% End If %>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td bgcolor="#00A651" style="padding-left:5px; color:#FFFFFF">Property Status:</td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000"><%=temp1%></td>
</tr>
<tr> 
<td height="5"></td>
</tr>
<tr> 
<td bgcolor="#00A651" style="padding-left:5px; color:#FFFFFF"><%=temp4%>(pcm):</td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000"><%=temp3%></td>
</tr>
</table>
</td>
<td valign="top" width="10">&nbsp;</td>
<td valign="top" width="264"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="iagManagerTable">
<tr bgcolor="#00A651"> 
<td colspan="2" style="padding-left:5px; color:#FFFFFF">Further Information</td>
</tr>
<tr> 
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000">Dwelling Type</td>
<td style="padding-left:5px; color:#000000"><%=Rs("Dwelling")%>, <%=Rs("Level")%></td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000">No Bedrooms</td>
<td style="padding-left:5px; color:#000000"><%=Rs("BedroomCount")%> Bedroom(s) </td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000">Age</td>
<td style="padding-left:5px; color:#000000"><%=Rs("AgeCat")%></td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<tr> 
<td bgcolor="#98D4B5" style="padding-left:5px; color:#000000">Council Tax Band</td>
<td style="padding-left:5px; color:#000000"><%=Rs("TaxBand")%></td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<% 
While ((Repeat1__numRows <> 0) AND (NOT GenAtt.EOF))
%>
<% 
If GenAtt.Fields.Item("TheAttribute").Value = "None" Then
'Do Nothing
Else
%>
<tr> 
<td bgcolor="#CCD3D8" style="padding-left:5px; color:#000000"><%=(GenAtt.Fields.Item("CategoryName").Value)%></td>
<td style="padding-left:5px; color:#000000"><%=(GenAtt.Fields.Item("TheAttribute").Value)%></td>
</tr>
<tr> 
<td height="5"></td>
<td height="5"></td>
</tr>
<%
End If
%>
<% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  GenAtt.MoveNext()
Wend
%>
<tr> 
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr> 
<td> 
<%
Response.Write "[<a href='view.asp?asset=" &  Rs("AssetID") & "&bedrooms=" & bedrooms & "&forsale=" & forsale & "&tolet=" &tolet & "&postcode=" & postcode & "&Development="  & nh & "' class='iagManagerSmallBlue'>Arrange to View</a>]"&"<BR>"			
%>
</td>
<td>&nbsp;</td>
</tr>
<tr> 
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr> 
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr align="right"> 
<td colspan="2"> 
<input type=button class="iagButton" value=" Back to Results " onClick="javascript:history.back()" name="button2">
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td height=370 bgcolor="#FFFFFF" valign="top">&nbsp; </td>
</tr>
<tr> 
<td> <img src="images/_gif_41.gif" width=6 height=8 alt=""></td>
<td bgcolor="#2E3192"> <img src="images/_gif_42.gif" width=686 height=8 alt=""></td>
<td width="13"> <img src="images/_gif_43.gif" width=13 height=8 alt=""></td>
</tr>
</form>
</table>
</td>
<td width="88">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr> 
<td height="57" bgcolor="#00A651" class="ThinLineL">&nbsp;</td>
</tr>
<tr> 
<td> 
<table cellspacing="0" cellpadding="0" border="0">
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
<td> <img src="images/rr_broadland_44.gif" width=690 height=24 alt=""></td>
<td> <img src="images/rr_broadland_45.gif" width=97 height=24 alt=""></td>
</table>
</td>
</tr>
<tr> 
<td><img name="rr_broadland_47" src="images/rr_broadland_47.gif" width=884 height=30 alt=""></td>
</tr>
</table>
</td>
</tr>
</table>
</BODY>
</HTML>
<%
GenAtt.Close()
%>
