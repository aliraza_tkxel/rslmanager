<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="Connections/ContactManager.asp" -->
<%
set rsNews = Server.CreateObject("ADODB.Recordset")
rsNews.ActiveConnection = RSL_CONNECTION_STRING
rsNews.Source = "SELECT *  FROM tbl_News_And_Story_Content, tbl_Page_Headings  WHERE ((tbl_Page_Headings.SSHeadingID = tbl_News_And_Story_Content.SectionHeadingID) AND (tbl_News_And_Story_Content.SectionHeadingID = 5) ) ORDER BY StartDate DESC"
rsNews.CursorType = 0
rsNews.CursorLocation = 2
rsNews.LockType = 3
rsNews.Open()
rsNews_numRows = 0
%>
<%
Dim Repeat2__numRows
Repeat2__numRows = 2
Dim Repeat2__index
Repeat2__index = 0
rsNews_numRows = rsNews_numRows + Repeat2__numRows
%>
<%
'  *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

' set the record count
rsNews_total = rsNews.RecordCount

' set the number of rows displayed on this page
If (rsNews_numRows < 0) Then
  rsNews_numRows = rsNews_total
Elseif (rsNews_numRows = 0) Then
  rsNews_numRows = 1
End If

' set the first and last displayed record
rsNews_first = 1
rsNews_last  = rsNews_first + rsNews_numRows - 1

' if we have the correct record count, check the other stats
If (rsNews_total <> -1) Then
  If (rsNews_first > rsNews_total) Then rsNews_first = rsNews_total
  If (rsNews_last > rsNews_total) Then rsNews_last = rsNews_total
  If (rsNews_numRows > rsNews_total) Then rsNews_numRows = rsNews_total
End If
%>
<%
' *** Move To Record and Go To Record: declare variables

Set MM_rs    = rsNews
MM_rsCount   = rsNews_total
MM_size      = rsNews_numRows
MM_uniqueCol = ""
MM_paramName = ""
MM_offset = 0
MM_atTotal = false
MM_paramIsDefined = false
If (MM_paramName <> "") Then
  MM_paramIsDefined = (Request.QueryString(MM_paramName) <> "")
End If
%>
<%
' *** Move To Record: handle 'index' or 'offset' parameter

if (Not MM_paramIsDefined And MM_rsCount <> 0) then

  ' use index parameter if defined, otherwise use offset parameter
  r = Request.QueryString("index")
  If r = "" Then r = Request.QueryString("offset")
  If r <> "" Then MM_offset = Int(r)

  ' if we have a record count, check if we are past the end of the recordset
  If (MM_rsCount <> -1) Then
    If (MM_offset >= MM_rsCount Or MM_offset = -1) Then  ' past end or move last
      If ((MM_rsCount Mod MM_size) > 0) Then         ' last page not a full repeat region
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' move the cursor to the selected record
  i = 0
  While ((Not MM_rs.EOF) And (i < MM_offset Or MM_offset = -1))
    MM_rs.MoveNext
    i = i + 1
  Wend
  If (MM_rs.EOF) Then MM_offset = i  ' set MM_offset to the last possible record

End If
%>
<%
' *** Move To Record: if we dont know the record count, check the display range

If (MM_rsCount = -1) Then

  ' walk to the end of the display range for this page
  i = MM_offset
  While (Not MM_rs.EOF And (MM_size < 0 Or i < MM_offset + MM_size))
    MM_rs.MoveNext
    i = i + 1
  Wend

  ' if we walked off the end of the recordset, set MM_rsCount and MM_size
  If (MM_rs.EOF) Then
    MM_rsCount = i
    If (MM_size < 0 Or MM_size > MM_rsCount) Then MM_size = MM_rsCount
  End If

  ' if we walked off the end, set the offset based on page size
  If (MM_rs.EOF And Not MM_paramIsDefined) Then
    If (MM_offset > MM_rsCount - MM_size Or MM_offset = -1) Then
      If ((MM_rsCount Mod MM_size) > 0) Then
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' reset the cursor to the beginning
  If (MM_rs.CursorType > 0) Then
    MM_rs.MoveFirst
  Else
    MM_rs.Requery
  End If

  ' move the cursor to the selected record
  i = 0
  While (Not MM_rs.EOF And i < MM_offset)
    MM_rs.MoveNext
    i = i + 1
  Wend
End If
%>
<%
' *** Move To Record: update recordset stats

' set the first and last displayed record
rsNews_first = MM_offset + 1
rsNews_last  = MM_offset + MM_size
If (MM_rsCount <> -1) Then
  If (rsNews_first > MM_rsCount) Then rsNews_first = MM_rsCount
  If (rsNews_last > MM_rsCount) Then rsNews_last = MM_rsCount
End If

' set the boolean used by hide region to check if we are on the last record
MM_atTotal = (MM_rsCount <> -1 And MM_offset + MM_size >= MM_rsCount)
%>
<%
' *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

' create the list of parameters which should not be maintained
MM_removeList = "&index="
If (MM_paramName <> "") Then MM_removeList = MM_removeList & "&" & MM_paramName & "="
MM_keepURL="":MM_keepForm="":MM_keepBoth="":MM_keepNone=""

' add the URL parameters to the MM_keepURL string
For Each Item In Request.QueryString
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepURL = MM_keepURL & NextItem & Server.URLencode(Request.QueryString(Item))
  End If
Next

' add the Form variables to the MM_keepForm string
For Each Item In Request.Form
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepForm = MM_keepForm & NextItem & Server.URLencode(Request.Form(Item))
  End If
Next

' create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL & MM_keepForm
if (MM_keepBoth <> "") Then MM_keepBoth = Right(MM_keepBoth, Len(MM_keepBoth) - 1)
if (MM_keepURL <> "")  Then MM_keepURL  = Right(MM_keepURL, Len(MM_keepURL) - 1)
if (MM_keepForm <> "") Then MM_keepForm = Right(MM_keepForm, Len(MM_keepForm) - 1)

' a utility function used for adding additional parameters to these strings
Function MM_joinChar(firstItem)
  If (firstItem <> "") Then
    MM_joinChar = "&"
  Else
    MM_joinChar = ""
  End If
End Function
%>
<%
' *** Move To Record: set the strings for the first, last, next, and previous links

MM_keepMove = MM_keepBoth
MM_moveParam = "index"

' if the page has a repeated region, remove 'offset' from the maintained parameters
If (MM_size > 0) Then
  MM_moveParam = "offset"
  If (MM_keepMove <> "") Then
    params = Split(MM_keepMove, "&")
    MM_keepMove = ""
    For i = 0 To UBound(params)
      nextItem = Left(params(i), InStr(params(i),"=") - 1)
      If (StrComp(nextItem,MM_moveParam,1) <> 0) Then
        MM_keepMove = MM_keepMove & "&" & params(i)
      End If
    Next
    If (MM_keepMove <> "") Then
      MM_keepMove = Right(MM_keepMove, Len(MM_keepMove) - 1)
    End If
  End If
End If

' set the strings for the move to links
If (MM_keepMove <> "") Then MM_keepMove = MM_keepMove & "&"
urlStr = Request.ServerVariables("URL") & "?" & MM_keepMove & MM_moveParam & "="
MM_moveFirst = urlStr & "0"
MM_moveLast  = urlStr & "-1"
MM_moveNext  = urlStr & Cstr(MM_offset + MM_size)
prev = MM_offset - MM_size
If (prev < 0) Then prev = 0
MM_movePrev  = urlStr & Cstr(prev)
%>
<html>
<head>
<title>Repair Reporter</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<STYLE>
    .TA {scrollbar-3dlight-color:;
           scrollbar-arrow-color:;
           scrollbar-base-color:;
           scrollbar-darkshadow-color:;
           scrollbar-face-color:moccasin;
           scrollbar-highlight-color:mistyrose;
           scrollbar-shadow-color:}
  </STYLE>
</head>
<script language="javascript">

	function goIn(){
	
		divFront.style.display = 'none';
		divIn.style.display = 'block';
	
	}
	
</script>
<body bgcolor="#FFFFFF" text="#000000">
<br>
<form name='thisForm' method='post' target='frmRR'>
  <table width=820px height=500px align=center border=0 style='border:2px solid black'>
    <tr> 
      <td colspan=2 width=100%> 
        <!--#include file="banner.asp" -->
      </td>
    </tr>
    <tr> 
      <td align=left valign=top width=20%> 
        <div align="center"><img src="Images/screengrab_04.gif" width="153" height="133"><br>
          <span class="rrTop"><b>IMAGE NEEDED</b></span></div>
      </td>
      <td align=left valign=top> 
        <div id='divFront' named='divFront' width=100% style='overflow:auto;height:400px' class='TA'> 
          <table width=100%>
            <tr> 
              <td bgcolor='#912d45' width=200px class='rrTop' style='color:white'>&nbsp;</td>
            </tr>
            <tr style='height:4px'> 
              <td width=40px></td>
            </tr>
            <tr> 
              <td class='rrTop' valign="top"> Welcome to the Reid news area, where 
                you can read all about the latest news and events in the world 
                of Reid Housing and its subsidiaries'.</td>
            </tr>
            <tr> 
              <td>&nbsp; </td>
            </tr>
            <tr> 
              <td valign="top"> 
                <table width="100%">
                  <% If rsNews.EOF Or rsNews.BOF Then %>
                  <tr> 
                    <td colspan="2">There are currently no News Items. Please 
                      check back later.</td>
                  </tr>
                  <% End If %>
                  <% If Not rsNews.EOF Or Not rsNews.BOF Then %>
                  <%' End If ' end Not rsNews.EOF Or NOT rsNews.BOF %>
                  <% While ((Repeat2__numRows <> 0) AND (NOT rsNews.EOF)) %>
                  <tr> 
                    <td rowspan="2"> 
                      <%IF (rsNews.Fields.Item("upload_1").Value) <> "" then %>
                      <img src="http://194.129.129.248/downloads/rsldemo/<%=(rsNews.Fields.Item("upload_1").Value)%>" width="153" height="133"> 
                      <%Else %>
                      <img src="Images/news_default.gif"> 
                      <% End If %>
                    </td>
                    <td class="rrTop" width="525"><b><a href="News_Body.asp?NewsItem=<%=(rsNews.Fields.Item("ContentID").Value)%>" title="<%=(rsNews.Fields.Item("Headline").Value)%>"><%=(rsNews.Fields.Item("Headline").Value)%></a></b>&nbsp;(<%=(rsNews.Fields.Item("StartDate").Value)%>)</td>
                  </tr>
                  <tr> 
                    <td class="rrTop" width="525"> 
                      <% 
Dim SB 
SB = rsNews.Fields.Item("Summary_or_Body").Value
SB = Replace (SB,chr(13)," ")
If Len(SB) >= 101 Then
SB = Left((SB), 100) & " ..." 
Else
SB = SB
End If 
%>
                      <%= SB %> </td>
                  </tr>
                  <tr> 
                    <td width="115" height="10"></td>
                    <td height="10" width="525"></td>
                  </tr>
                  <% 
  Repeat2__index=Repeat2__index+1
  Repeat2__numRows=Repeat2__numRows-1
  rsNews.MoveNext()
Wend
%>
                  <tr> 
                    <td colspan="2" height="10"><A HREF="<%=MM_movePrev%>">&lt;&lt; 
                      Previous</A> | <A HREF="<%=MM_moveNext%>">Next 
                      &gt;&gt;</A></td>
                  </tr>
                </table>
                <% End If
				  %>
              </td>
            </tr>
          </table>
        </div>
        <div id='divIn' named='divIn' width=100% style='display:none;overflow:auto;height:400px' class='TA'> 
        </div>
      </td>
    </tr>
    <tr> 
      <td colspan=2> 
        <!--#include file="footer.asp" -->
      </td>
    </tr>
  </table>
</form>
<BR>
<BR>
<IFRAME width=500px height=300px name='frmRR' style='display:none'></IFRAME> 
</body>
</html>
<%
rsNews.Close()
%>
