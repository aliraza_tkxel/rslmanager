<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="Connections/ContactManager.asp" -->
<%
Dim rsPublications
Dim rsPublications_numRows
set rsPublications = Server.CreateObject("ADODB.Recordset")
	rsPublications.ActiveConnection = RSL_CONNECTION_STRING
	rsPublications.Source = "SELECT *  FROM tbl_News_And_Story_Content, tbl_Page_Headings  WHERE ((tbl_Page_Headings.SSHeadingID = tbl_News_And_Story_Content.SectionHeadingID) AND (tbl_News_And_Story_Content.SectionHeadingID = 4) AND (EndDate >= getDate()))ORDER BY tbl_News_And_Story_Content.StartDate DESC" 
	rsPublications.CursorType = 0
	rsPublications.CursorLocation = 2
	rsPublications.LockType = 3
	rsPublications.Open()
	rsPublications_numRows = 0
%>
<%
Dim Repeat1__numRows
Repeat1__numRows = 2
Dim Repeat1__index
Repeat1__index = 0
rsPublications_numRows = rsPublications_numRows + Repeat1__numRows
%>
<%
'  *** Recordset Stats, Move To Record, and Go To Record: declare stats variables

' set the record count
rsPublications_total = rsPublications.RecordCount

' set the number of rows displayed on this page
If (rsPublications_numRows < 0) Then
  rsPublications_numRows = rsPublications_total
Elseif (rsPublications_numRows = 0) Then
  rsPublications_numRows = 1
End If

' set the first and last displayed record
rsPublications_first = 1
rsPublications_last  = rsPublications_first + rsPublications_numRows - 1

' if we have the correct record count, check the other stats
If (rsPublications_total <> -1) Then
  If (rsPublications_first > rsPublications_total) Then rsPublications_first = rsPublications_total
  If (rsPublications_last > rsPublications_total) Then rsPublications_last = rsPublications_total
  If (rsPublications_numRows > rsPublications_total) Then rsPublications_numRows = rsPublications_total
End If
%>
<%
' *** Move To Record and Go To Record: declare variables

Set MM_rs    = rsPublications
MM_rsCount   = rsPublications_total
MM_size      = rsPublications_numRows
MM_uniqueCol = ""
MM_paramName = ""
MM_offset = 0
MM_atTotal = false
MM_paramIsDefined = false
If (MM_paramName <> "") Then
  MM_paramIsDefined = (Request.QueryString(MM_paramName) <> "")
End If
%>
<%
' *** Move To Record: handle 'index' or 'offset' parameter

if (Not MM_paramIsDefined And MM_rsCount <> 0) then

  ' use index parameter if defined, otherwise use offset parameter
  r = Request.QueryString("index")
  If r = "" Then r = Request.QueryString("offset")
  If r <> "" Then MM_offset = Int(r)

  ' if we have a record count, check if we are past the end of the recordset
  If (MM_rsCount <> -1) Then
    If (MM_offset >= MM_rsCount Or MM_offset = -1) Then  ' past end or move last
      If ((MM_rsCount Mod MM_size) > 0) Then         ' last page not a full repeat region
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' move the cursor to the selected record
  i = 0
  While ((Not MM_rs.EOF) And (i < MM_offset Or MM_offset = -1))
    MM_rs.MoveNext
    i = i + 1
  Wend
  If (MM_rs.EOF) Then MM_offset = i  ' set MM_offset to the last possible record

End If
%>
<%
' *** Move To Record: if we dont know the record count, check the display range

If (MM_rsCount = -1) Then

  ' walk to the end of the display range for this page
  i = MM_offset
  While (Not MM_rs.EOF And (MM_size < 0 Or i < MM_offset + MM_size))
    MM_rs.MoveNext
    i = i + 1
  Wend

  ' if we walked off the end of the recordset, set MM_rsCount and MM_size
  If (MM_rs.EOF) Then
    MM_rsCount = i
    If (MM_size < 0 Or MM_size > MM_rsCount) Then MM_size = MM_rsCount
  End If

  ' if we walked off the end, set the offset based on page size
  If (MM_rs.EOF And Not MM_paramIsDefined) Then
    If (MM_offset > MM_rsCount - MM_size Or MM_offset = -1) Then
      If ((MM_rsCount Mod MM_size) > 0) Then
        MM_offset = MM_rsCount - (MM_rsCount Mod MM_size)
      Else
        MM_offset = MM_rsCount - MM_size
      End If
    End If
  End If

  ' reset the cursor to the beginning
  If (MM_rs.CursorType > 0) Then
    MM_rs.MoveFirst
  Else
    MM_rs.Requery
  End If

  ' move the cursor to the selected record
  i = 0
  While (Not MM_rs.EOF And i < MM_offset)
    MM_rs.MoveNext
    i = i + 1
  Wend
End If
%>
<%
' *** Move To Record: update recordset stats

' set the first and last displayed record
rsPublications_first = MM_offset + 1
rsPublications_last  = MM_offset + MM_size
If (MM_rsCount <> -1) Then
  If (rsPublications_first > MM_rsCount) Then rsPublications_first = MM_rsCount
  If (rsPublications_last > MM_rsCount) Then rsPublications_last = MM_rsCount
End If

' set the boolean used by hide region to check if we are on the last record
MM_atTotal = (MM_rsCount <> -1 And MM_offset + MM_size >= MM_rsCount)
%>
<%
' *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters

' create the list of parameters which should not be maintained
MM_removeList = "&index="
If (MM_paramName <> "") Then MM_removeList = MM_removeList & "&" & MM_paramName & "="
MM_keepURL="":MM_keepForm="":MM_keepBoth="":MM_keepNone=""

' add the URL parameters to the MM_keepURL string
For Each Item In Request.QueryString
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepURL = MM_keepURL & NextItem & Server.URLencode(Request.QueryString(Item))
  End If
Next

' add the Form variables to the MM_keepForm string
For Each Item In Request.Form
  NextItem = "&" & Item & "="
  If (InStr(1,MM_removeList,NextItem,1) = 0) Then
    MM_keepForm = MM_keepForm & NextItem & Server.URLencode(Request.Form(Item))
  End If
Next

' create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL & MM_keepForm
if (MM_keepBoth <> "") Then MM_keepBoth = Right(MM_keepBoth, Len(MM_keepBoth) - 1)
if (MM_keepURL <> "")  Then MM_keepURL  = Right(MM_keepURL, Len(MM_keepURL) - 1)
if (MM_keepForm <> "") Then MM_keepForm = Right(MM_keepForm, Len(MM_keepForm) - 1)

' a utility function used for adding additional parameters to these strings
Function MM_joinChar(firstItem)
  If (firstItem <> "") Then
    MM_joinChar = "&"
  Else
    MM_joinChar = ""
  End If
End Function
%>
<%
' *** Move To Record: set the strings for the first, last, next, and previous links

MM_keepMove = MM_keepBoth
MM_moveParam = "index"

' if the page has a repeated region, remove 'offset' from the maintained parameters
If (MM_size > 0) Then
  MM_moveParam = "offset"
  If (MM_keepMove <> "") Then
    params = Split(MM_keepMove, "&")
    MM_keepMove = ""
    For i = 0 To UBound(params)
      nextItem = Left(params(i), InStr(params(i),"=") - 1)
      If (StrComp(nextItem,MM_moveParam,1) <> 0) Then
        MM_keepMove = MM_keepMove & "&" & params(i)
      End If
    Next
    If (MM_keepMove <> "") Then
      MM_keepMove = Right(MM_keepMove, Len(MM_keepMove) - 1)
    End If
  End If
End If

' set the strings for the move to links
If (MM_keepMove <> "") Then MM_keepMove = MM_keepMove & "&"
urlStr = Request.ServerVariables("URL") & "?" & MM_keepMove & MM_moveParam & "="
MM_moveFirst = urlStr & "0"
MM_moveLast  = urlStr & "-1"
MM_moveNext  = urlStr & Cstr(MM_offset + MM_size)
prev = MM_offset - MM_size
If (prev < 0) Then prev = 0
MM_movePrev  = urlStr & Cstr(prev)
%>
<%
' *** Recordset Stats: if we don't know the record count, manually count them

If (rsLevel1_total = -1) Then

  ' count the total records by iterating through the recordset
  rsLevel1_total=0
  While (Not rsLevel1.EOF)
    rsLevel1_total = rsLevel1_total + 1
    rsLevel1.MoveNext
  Wend

  ' reset the cursor to the beginning
  If (rsLevel1.CursorType > 0) Then
    rsLevel1.MoveFirst
  Else
    rsLevel1.Requery
  End If

  ' set the number of rows displayed on this page
  If (rsLevel1_numRows < 0 Or rsLevel1_numRows > rsLevel1_total) Then
    rsLevel1_numRows = rsLevel1_total
  End If

  ' set the first and last displayed record
  rsLevel1_first = 1
  rsLevel1_last = rsLevel1_first + rsLevel1_numRows - 1
  If (rsLevel1_first > rsLevel1_total) Then rsLevel1_first = rsLevel1_total
  If (rsLevel1_last > rsLevel1_total) Then rsLevel1_last = rsLevel1_total

End If %>
<html>
<head>
<title>Repair Reporter</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">
<STYLE>
    .TA {scrollbar-3dlight-color:;
           scrollbar-arrow-color:;
           scrollbar-base-color:;
           scrollbar-darkshadow-color:;
           scrollbar-face-color:moccasin;
           scrollbar-highlight-color:mistyrose;
           scrollbar-shadow-color:}
  </STYLE>
</head>
<script language="javascript">

	function goIn(){
	
		divFront.style.display = 'none';
		divIn.style.display = 'block';
	
	}
	
</script>
<SCRIPT TYPE="text/javascript" Language="JavaScript">
<!--
FormFields = new Array("Name|Name|TEXT|Y", "Address|Address|TEXT|Y");
function SendEmail(){
	
		Publicationsfrm.target = "ServerFrame";
		Publicationsfrm.method="POST";
		Publicationsfrm.action="ServerSide/SendPublication.asp";	
		Publicationsfrm.submit();
		EmailSent.style.display = "block";
	
}
//-->
</SCRIPT>
<body bgcolor="#FFFFFF" text="#000000">
<br>
<table width=820px height=500px align=center border=0 style='border:2px solid black'>
  <tr> 
    <td colspan=2 width=100%> 
      <!--#include file="banner.asp" -->
    </td>
  </tr>
  <tr> 
    <td align=left valign=top width=20%> 
      <div align="center"><img src="Images/screengrab_04.gif" width="153" height="133"><br>
        <span class="rrTop"><b>IMAGE NEEDED</b></span></div>
    </td>
    <td align=left valign=top> 
      <div id='divFront' named='divFront' width=100% style='overflow:auto;height:400px' class='TA'> 
        <table width=100% height="100%">
          <tr> 
            <td bgcolor='#912d45' width=485 class='rrTop' style='color:white'>&nbsp;</td>
            <td bgcolor='#912d45' width=163 class='rrTop' style='color:white'><font size="1"><b><span class="rrTop"><font size="2"> 
              &nbsp;Request a Publication:</font></span></b></font></td>
          </tr>
          <tr style='height:4px'> 
            <td width=485></td>
            <td width=163></td>
          </tr>
          <tr> 
            <td class='rrTop' valign="top" width="485">Welcome to the Reid news 
              area. We always keeps this area of the site up to date so you have 
              all the latest insight.</td>
            <td class='rrTop' valign="top" rowspan="3" width="163"> 
              <table width="163" border="0" cellspacing="0" cellpadding="0" height="100%">
                <tr> 
                  <td height="4" width="4" bgcolor="#FAEDED"></td>
                  <td width="120" height="4" bgcolor="#FAEDED"></td>
                  <td height="4" width="4"></td>
                </tr>
                <tr> 
                  <td height="4" bgcolor="#FAEDED"> </td>
                  <td height="4" colspan="2" bgcolor="#FAEDED" valign="top"><span class="rrTop"></span> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <form name="Publicationsfrm">
                        <tr> 
                          <td> <span class="rrTop"><font size="1" class="rrSmall">If 
                            you would like a printed copy of any of these publications 
                            just tick the box next to its title, complete and 
                            send the form below </font></span></td>
                        </tr>
                        <tr> 
                          <td height="5"></td>
                        </tr>
                        <tr> 
                          <td><b><span class="rrSmall">Name</span></b> <span class="rrSmall"><font color="#FF0000">*</font></span></td>
                        </tr>
                        <tr> 
                          <td> 
                            <div align="center"><span class="rrTop"><font size="1" class="rrSmall"> 
                              <input type="text" name="Name" class="PubTextField" style="width:150">
                              </font></span></div>
                          </td>
                        </tr>
                        <tr> 
                          <td height="5"></td>
                        </tr>
                        <tr> 
                          <td><b><span class="rrSmall">Address</span></b> <span class="rrSmall"><font color="#FF0000">*</font></span></td>
                        </tr>
                        <tr> 
                          <td height="5"></td>
                        </tr>
                        <tr> 
                          <td> 
                            <div align="center"> 
                              <textarea name="Address" class="PubTextField" rows="3" style="width:150"></textarea>
                            </div>
                          </td>
                        </tr>
                        <tr> 
                          <td>&nbsp;</td>
                        </tr>
                        <tr> 
                          <td> 
                            <input type="button" name="Button" value="Request" class="FeedbackSubmit" onClick="SendEmail()">
                            <iframe name=ServerFrame style='display:none'></iframe></td>
                        </tr>
                        <tr> 
                          <td><iframe name=ServerFrame style='display:none'>&nbsp;</iframe><span class="rrSmall"><font color="#FF0000">* 
                            Required Fields&nbsp;</font> </span></td>
                        </tr>
                        <tr> 
                          <td>&nbsp;</td>
                        </tr>
                        <tr> 
                          <td> 
                            <div id="Response" name="Response"> <span class="rrTop"></span></div>
                          </td>
                        </tr>
                      </form>
                    </table>
                    <span class="rrTop"></span></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class='rrTop' valign="top" width="485"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <% 
While ((Repeat1__numRows <> 0) AND (NOT rsPublications.EOF)) 
%>
                <tr> 
                  <td width="100"> 
                    <% If isNull(rsPublications.Fields.Item("Upload_2").Value) OR (rsPublications.Fields.Item("Upload_2").Value = "") Then %>
                    <img src="http://194.129.129.248/downloads/RSLDemo/" width="100" height="100" alt="default publications image"> 
                    <% Else %>
                    <img src="http://194.129.129.248/downloads/RSLDemo/<%=rsPublications.Fields.Item("Upload_2").Value %>" width="100" height="100"> 
                    <% End If %>
                  </td>
                  <td width="10">&nbsp;</td>
                  <td width="10" valign="top"> 
                    <input type="radio" name="Publication<%'=Counter%>"  value="<%=(rsPublications.Fields.Item("Title").Value)%>" >
                  </td>
                  <td width="10">&nbsp;</td>
                  <td valign="top"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td><b><%=(rsPublications.Fields.Item("Title").Value)%></b> (<%=(rsPublications.Fields.Item("StartDate").Value)%>)</td>
                      </tr>
                      <tr> 
                        <td><%=(rsPublications.Fields.Item("Summary_or_Body").Value)%></td>
                      </tr>
                      <% If isNull(rsPublications.Fields.Item("Upload_1").Value) OR (rsPublications.Fields.Item("Upload_1").Value = "") Then %>
                      <% Else %>
                      <tr> 
                        <td><a href="http://194.129.129.248/downloads/LHT/<%=(rsPublications.Fields.Item("Upload_1").Value)%>" target="_blank">Click 
                          here to view</a></td>
                        <% End If %>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  rsPublications.MoveNext()
Wend
%>
                <tr> 
                  <td colspan="5"><A HREF="<%=MM_movePrev%>">&lt; 
                    Previous</A> | <A HREF="<%=MM_moveNext%>">Next 
                    &gt;</A></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr> 
            <td class='rrTop' valign="top" width="485">&nbsp;</td>
          </tr>
        </table>
      </div>
      <div id='divIn' named='divIn' width=100% style='display:none;overflow:auto;height:400px' class='TA'> 
      </div>
    </td>
  </tr>
  <tr> 
    <td colspan=2> 
      <!--#include file="footer.asp" -->
    </td>
  </tr>
</table>
<BR>
<BR>
<IFRAME width=500px height=300px name='frmRR' style='display:none'></IFRAME> 
</body>
</html>
<%
rsPublications.Close()
%>

