<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="../Connections/db_connection.asp" -->

<% Session.LCID = 2057 %>
<%
	' Implemented for sake of demo at property show
	' Must eventually come from valid tenancy record upon provision of valid Property Reference No
	
	Session("TenancyID") = 22
	Session("RSLID") = 4
	

%>

<html>
<head>
<title>Repair Reporter</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/reporter.css" type="text/css">

<STYLE>
    .TA {scrollbar-3dlight-color:;
           scrollbar-arrow-color:;
           scrollbar-base-color:;
           scrollbar-darkshadow-color:;
           scrollbar-face-color:moccasin;
           scrollbar-highlight-color:mistyrose;
           scrollbar-shadow-color:;
		   font-family: "Arial";
		   font-size: 14px;
		   font-style: normal;
		   line-height: normal;
		   text-transform: none 
		   }
  </STYLE>

</head>

<script language="javascript">

	function loadPage(intID){
		
		reColour(intID);
		thisForm.action = "main.asp?theID=" + intID;
		thisForm.submit();	
	}
	
	
	function reColour(intID){
	
		var coll = document.all.tags("TD");
			if (coll!=null)
				for (i=0; i<coll.length; i++) 
			    	if (coll[i].style.backgroundColor == "beige")
				  		coll[i].style.backgroundColor = "#c8c7c7"
						
		document.all.item("TD" + intID).style.backgroundColor = "beige";
		
	
	}
	
	function drillDown(intID){
	
		thisForm.action = "drilldown.asp?drillID=" + intID;
		thisForm.submit();
	
	}
	
	function drillSubmit(intAction){
	
		reColour(10);
		thisForm.action = "basket.asp?action=" + intAction;
		thisForm.submit();
	
	}
	
	function loadApp(){
	
		thisForm.action = "app.asp";
		thisForm.submit();
			
	}
	
	function submitApp(){
	
		thisForm.action = "saveApp.asp";
		thisForm.submit();
		
	}
	
</script>

<body bgcolor="#FFFFFF" text="#000000">
<br>
<form name='thisForm' method='post' target='frmRR'>
<table width=820px height=500px align=center border=0 style='border:2px solid black'>
	<tr>
		<td colspan=2 width=100%><!--#include file="banner.asp" --></td>
	</tr>
	<tr>
		<td align=left valign=top width=20%><!--#include file="menu.asp" --></td>
		<td align=left valign=top>
			<div id='divMain' named='divMain' width=100% style='overflow:auto;height:400px' class='TA'>
			<table height=100%' style='border-collapse:collapse'>
					<tr style='height:2px'><td></td></tr>
    				<tr>
						<td colspan=5 width=655px style='height:15px' bgcolor='#912d45'></td>
					</tr>	
					<tr>
						<td  style='background-image:url(Images/Image1.gif)'>&nbsp;</td>
						
					</tr>
			</table>
			</div>
			</td>
		</tr>
			<tr>
		<td colspan=2><!--#include file="footer.asp" --></td>
	</tr>
	</table> 
</form>
<BR><BR>	
<IFRAME width=500px height=300px name='frmRR' style='display:none'></IFRAME>
</body>
</html>
