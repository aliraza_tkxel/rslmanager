<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="Connections/WM_Which.asp" -->
<% On Error Resume Next %>
<%
Dim theFrm_Task 
	theFrm_Task = Request("theFrm_Task")
%>
<%
Dim rsiSelectQuery__VarG
	rsiSelectQuery__VarG = "1"
if (Request("theG") <> "") then rsiSelectQuery__VarG = Request("theG")  
%>
<%
Dim rsiSelectQuery__VarS
	rsiSelectQuery__VarS = "1"
if (Request("theS") <> "") then rsiSelectQuery__VarS = Request("theS")    
%>
<%
Dim rsiSelectQuery__VarSS
	rsiSelectQuery__VarSS = "1"
if (Request("theSS") <> "") then rsiSelectQuery__VarSS = Request("theSS")  
%>
<%
Dim rsiSelectQuery__VarP
	rsiSelectQuery__VarP = "1"
if (Request("theP") <> "") then rsiSelectQuery__VarP = Request("theP")  
%>
<%
Dim SQL
If Request("theP") <> "" Then
	SQL = "SELECT *  FROM tbl_Content_Ref, tbl_Page_Headings  WHERE (tbl_Content_Ref.TopStructureID = " + Replace(rsiSelectQuery__VarG, "'", "''") + ") AND (tbl_Content_Ref.SectionHeadingID = " + Replace(rsiSelectQuery__VarS, "'", "''") + ") AND (tbl_Content_Ref.PageHeadingID = " + Replace(rsiSelectQuery__VarSS, "'", "''") + ")  AND (tbl_Content_Ref.PageID = " + Replace(rsiSelectQuery__VarP, "'", "''") + ")"
Else
	SQL = "SELECT * FROM tbl_Content_Ref, tbl_Page_Headings  WHERE (tbl_Content_Ref.TopStructureID =" + Replace(rsiSelectQuery__VarG, "'", "''") + ") AND (tbl_Content_Ref.SectionHeadingID = " + Replace(rsiSelectQuery__VarS, "'", "''") + ") AND (tbl_Content_Ref.PageHeadingID = " + Replace(rsiSelectQuery__VarSS, "'", "''") + ")  AND (tbl_Content_Ref.PageHeadingID = tbl_Page_Headings.SSHeadingID)"
End If
%>
<%
Dim rsiSelectQuery
Dim rsiSelectQuery_numRows
set rsiSelectQuery = Server.CreateObject("ADODB.Recordset")
	rsiSelectQuery.ActiveConnection = RSL_CONNECTION_STRING
	rsiSelectQuery.Source = SQL
	rsiSelectQuery.CursorType = 0
	rsiSelectQuery.CursorLocation = 2
	rsiSelectQuery.LockType = 3
	rsiSelectQuery.Open()
	rsiSelectQuery_numRows = 0
	'Response.Write rsiSelectQuery.Source & "<br>"
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
<!--
function AddLink() {
var sText = document.selection.createRange();
if (!sText==""){
     document.execCommand("CreateLink");
	 sText.parentElement().tagName == "A";
     //if (sText.parentElement().tagName == "A"){
       //sText.parentElement().innerText=sText.parentElement().href;
       //document.execCommand("ForeColor","false","#FF0033");
    // }    
  }
else{
    alert("Please select some blue text!");
  }   
}

function displayHTML(form) {
frm.theContent2.value = theContent.innerHTML; 
var HTML = frm.theContent2.value;
	win = window.open("Preview", 'popup', 'toolbar=0, status=0, scrollbars=yes, width=400, height=300');
	win.document.write("<html><title>Preview</title><link rel='stylesheet' href='css/WebManager.css' type='text/css'><body>" + HTML + "</body></html>");
}

function SubmitFrm() {
frm.theContent2.value = theContent.innerHTML;
frm.action = "/webmanager03/EditForms/ServerSide/serv_Standard_Text_1.asp" ;
frm.target = "_self" ;
frm.submit();
}

//-->
</script>
</head>
<body bgcolor="#F2F2EE" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<% If theFrm_Task = "Add" then %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
		<td width="10" height="10"></td>
		<td height="10"></td>
		<td width="10" height="10"></td>
	</tr>
	<tr>
		<td width="10"></td>
		<td>You are unable to add new content as the content already exists, you are allowed to amend, or delete this content only </td>
		<td width="10" height="10"></td>
	</tr>
</table>
<% Else %>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100">
<form name="frm" method="post" action="">
<tr> 
<td width="10" height="10"></td>
<td width="10" height="10"></td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10">&nbsp;</td>
<td width="100">Content</td>
<td width="10">&nbsp;</td>
</tr>
<div unselectable="on"> 
<tr> 
<td width="10">&nbsp;</td>
<td> 
<div id="theContent" name="theContent" contenteditable style="width:405; height:170; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;"><%=(rsiSelectQuery.Fields.Item("Content").Value)%></div>
</td>
<td width="10">&nbsp;</td>
</tr>
<tr>
<td width="10">&nbsp;</td>
<td>&nbsp;</td>
<td width="10">&nbsp;</td>
</tr>
<tr> 
<td width="10">&nbsp;</td>
<td> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td align="left"> 
<input type="button" name="Action" value="<%=Request("theFrm_Task")%>" class="iagButton" onClick="SubmitFrm()">
<input type="button" name="Preview" value="Preview" onClick="displayHTML(this.form)" class="iagButton">
</td>
<td width="10">&nbsp;</td>
<td align="right">
<% If theFrm_Task = "Delete" OR IsNull(theFrm_Task) Then %>
<% Else %>
<button unselectable="On" onClick='document.execCommand("Bold");theContent.focus();' class="iagButton"><b>B</b></button> 
<button unselectable="On" onClick='document.execCommand("Italic");theContent.focus();' class="iagButton"><i><b>I</b></i></button> 
<button unselectable="On" onClick='document.execCommand("Underline");theContent.focus();' class="iagButton"><u><b>U</b></u></button> 
<button unselectable="On" onClick="AddLink()" class="iagButton">Hyperlink</button> 
<% End If %>
</td>
</tr>
</table>
</td>
<td width="10">&nbsp;</td>
</tr>
</div>
<tr> 
<td width="10">&nbsp;</td>
<td> 
<input type="hidden" name="UniqueContentID" value="<%=(rsiSelectQuery.Fields.Item("ID").Value)%>">
<input type="hidden" name="theG" value="<%=(rsiSelectQuery.Fields.Item("TopStructureID").Value)%>">
<input type="hidden" name="theS" value="<%=(rsiSelectQuery.Fields.Item("SectionHeadingID").Value)%>">
<input type="hidden" name="theSS" value="<%=(rsiSelectQuery.Fields.Item("PageHeadingID").Value)%>">
<input type="hidden" name="theFrm_Type" value="<%=(rsiSelectQuery.Fields.Item("frm_Type").Value)%>">
<input type="hidden" name="theTbl_Name" value="<%=(rsiSelectQuery.Fields.Item("tbl_Name").Value)%>">
<input type="hidden" name="theTask" value="Amend">

<input type="hidden" name="theContent2" value="">
</td>
<td width="10">&nbsp;</td>
</tr>
</form>
</table>
<% End If %>
</body>
</html>