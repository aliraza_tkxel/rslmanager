<%@LANGUAGE="VBSCRIPT"%>
<% ' Option Explicit %>
<% ' On Error Resume Next %>
<!--#include file="Connections/WM_Which.asp" --> 
<!--#include file="class/JimClassLib.asp" -->
<!--#include file="LibFunctions.asp" -->
<%
Dim objDB1
Dim rsDeleteRecord1	' Declare Recordsets
Dim objDB2
Dim rsDeleteRecord2	' Declare Recordsets

DNS_STRING = RSL_CONNECTION_STRING
Dim IntRsID 
Dim PageState
Dim tbl_Name
Dim tbl_ID_Name

	tbl_Name 	 = Request("thetbl_Name")
	tbl_ID_Name	 = Request("thetbl_ID_Name")
	frmStatus    = Request.Form("frmStatus")
	IntRsID      = Request("RsID")
	'response.write Request("thess")
	If frmStatus = "TRUE" then
	
		theG = Request("theG")
		thes= Request("thes")
		theSS = Request("thess")
		IF  NOT theG <> "" then  theG= 0 End if
		IF  NOT theSS <> "" then  theSS = 0 End if
		IF  NOT theS <> "" then  theS = 0 End if

		IntRsID  = request("rsID")
		' Below Deletes record		 
		SQL = "DELETE  FROM " & tbl_Name & " WHERE " & tbl_ID_Name & "=" & IntRsID 			 
		OpenRecordSet rsDeleteRecord2
		CloseRecordSet rsDeleteRecord2
		
		'Below records deletion
		SQL = "INSERT INTO dbo.tbl_Actions("		&_ 
							"IntRsID, "		&_
							"IntG, " 		&_ 
							"IntS, " 		&_ 
							"IntSS, " 		&_
							"strTbl_Name, "	&_
							"IntTaskID, " &_
							"IntUserID) " 	&_
							"VALUES (" 		&_
							"   "  & IntRsID 			&_
							" , "  & theG 				&_
							" , "  & thes 				&_
							" , "  & thess 				&_
							" ,'"  & tbl_Name 			&_
							"', 3"   				&_
							" , "  & Session("svUserID") 	&_					
							")"
							'response.write SQL
 		OpenRecordSet Com_Delete
		CloseRecordSet Com_Delete 
		
		 PageState = "Deletion Complete"
%>

	 <script language="JavaScript">
	<!--
		parent.gotoPage("Delete");	
	//-->
	</script>
	
<%
	Else

		SQL = "SELECT * FROM " & tbl_Name & "  WHERE " & tbl_ID_Name & " = " & IntRsID & ""		
		OpenRecordSet rsDeleteRecord1
		
		'Response.Write SQL
		
			If rsDeleteRecord1.EOF OR rsDeleteRecord1.BOF Then
				PageState = "To Delete"
			End If
		CloseRecordSet rsDeleteRecord1 
	End If
%>
<html>
<head>
<title>WebManager :: Delete Title</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<script language="JavaScript">
<!--
	function gotoPage(){	
		Deletefrm.action = "DeleteTitle.asp";
		Deletefrm.target = "_self";
		Deletefrm.method = "POST";
		Deletefrm.submit();	
	}
//-->
</script>
<link rel="stylesheet" href="css/WebManager.css" type="text/css">
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<form name="Deletefrm">  
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
	<% If Pagestate = "Deletion Complete" Then %>
	<tr> 
		<td width="2%" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000">&nbsp;</td>
		<td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000">You have Successfully deleted this title. </td>
	</tr>
	<% Else %>
	<tr> 
		<td width="2%" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000">&nbsp;</td>
		<td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000">Are you sure you want to delete this title. <input type="hidden" name="hiddenField" value="<%request("id")%>"></td>
	</tr>
	<tr> 
		<td width="2%">&nbsp;</td>
		<td colspan="2" class="iagManagerSmallBlk">&nbsp;</td>
	</tr>
	<tr> 
		<td width="2%">&nbsp;</td>
		<td width="50" class="iagManagerSmallBlk"><input type="button" name="Submit" value="Yes" class="iagButton" style="width:50px" onClick="gotoPage()">
	</td>
		<td class="iagManagerSmallBlk">I would like to delete this title.</td>
	</tr>
	<tr> 
		<td width="2%">&nbsp;</td>
		<td colspan="2" class="iagManagerSmallBlk">&nbsp;</td>
	</tr>
	<tr> 
		<td width="2%">&nbsp;</td>
		<td class="iagManagerSmallBlk"><input type="button" name="No" value="No" class="iagButton"  style="width:50px"></td>
		<td class="iagManagerSmallBlk">Cancel this request.</td>
	</tr>
	<tr> 
		<td width="2%">&nbsp;</td>
		<td colspan="2" class="iagManagerSmallBlk">&nbsp;</td>
	</tr>
<% End If %>
</table>
  <% If NOT Pagestate = "Deletion Complete" Then  %>
	  <input type="hidden" name="frmStatus" value="TRUE">
	  <input type="hidden" name="RsID" value="<%=IntRsID%>">
	  <input type="hidden" name="theG" value="<%=Request("theG")%>">
	  <input type="hidden" name="thes" value="<%=Request("thes")%>">
	  <input type="hidden" name="thess" value="<%=Request("thess")%>">
	  <input type="hidden" name="thetbl_Name" value="<%=tbl_Name%>">
	  <input type="hidden" name="thetbl_ID_Name" value="<%=tbl_ID_Name%>">
  <% End If %>
</form>
</body>
</html>