<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="Connections/WM_Which.asp" -->
<!--#include file="LibFunctions.asp" -->
<% 'On Error Resume Next %>

<%
Dim theTbl_Name
	theTbl_Name = Request("theTbl_Name")

 Dim theFrm_Task
	 theFrm_Task = Request("theFrm_Task") 
	 
 Dim IntSSID
 	 IntSSID = Request("theSS") 
	 
	 If NOT IntSSID <> "" Then 
	 	IntSSID = 0 
	 End If

 Dim IntSID
	 IntSID = "0"
	
 If (Request("theS") <> "") Then 
	 IntSID = Request("theS")  
 End If
  
 	DNS_STRING = RSL_CONNECTION_STRING

	IF IntSSID = "0" Then	
		SQL = "SELECT NSC.ContentID AS [ID], NSC.Title AS [Title],  SH.SectionHeading AS [Heading]  FROM tbl_News_And_Story_Content NSC, tbl_Section_Headings SH   WHERE NSC.SectionHeadingID = " & IntSID & " AND SH.SectionHeadingID = " & IntSID & " AND NSC.SectionHeadingID = SH.SectionHeadingID" & " ORDER BY NSC.ContentID DESC"
	Else
		SQL = "SELECT NSC.ContentID AS [ID], NSC.Title AS [Title],  PH.SSHeading AS [Heading],  NSC.SubSectionID AS [SubSection]" &_
		   		" FROM tbl_News_And_Story_Content NSC, tbl_Section_Headings SH, tbl_Page_Headings PH" &_   
		   		" WHERE NSC.SectionHeadingID = " & IntSID &_
		   		" AND SH.SectionHeadingID = " & IntSID &_
		   		" AND NSC.SectionHeadingID = SH.SectionHeadingID" & " AND NSC.SubSectionID  = " & IntSSID &_
				" AND NSC.SubSectionID = PH.SSHeadingID" &_
		   		" ORDER BY NSC.ContentID DESC"
	End If
	
	'Response.Write("News" & "<br><br>" & SQL)
	
	OpenRecordSet Recordset1
	Recordset1_numRows = 0
%>
<%
Dim Repeat1__numRows
	Repeat1__numRows = -1
Dim Repeat1__index
	Repeat1__index = 0
	Recordset1_numRows = Recordset1_numRows + Repeat1__numRows
%>
<html>
<head>
<title>Web Manager :: Select...?</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/WebManager.css" type="text/css">
<script language="JavaScript">
<!--
	function PerformAction (GetListid , action) {
		if (action == 1) {
		TitleFrm.theFrm_Task.value = action;
		TitleFrm.action = "EditTitle.asp";
		TitleFrm.method = "POST";
		//TitleFrm.target = "_blank";
		TitleFrm.target = "TaskfrmServerFrame";
		TitleFrm.submit();
		}
		if (action == 2) { 
		TitleFrm.theFrm_Task.value = action;
		TitleFrm.action = "DeleteTitle.asp";
		TitleFrm.method = "POST";
		TitleFrm.target = "TaskfrmServerFrame";
	    //TitleFrm.target = "TaskfrmServerFrame";		
		TitleFrm.submit();
		}
	}
	
	function AddAction () {
		TitleFrm.theFrm_Task.value = "0";
		TitleFrm.action = "EditTitle.asp";
		TitleFrm.target = "TaskfrmServerFrame";
		TitleFrm.submit();
	}
	function ClearAction () {
		//TitleFrm.theFrm_Task.value = "";
		//TitleFrm.action = "EditTitle.asp";
		//TitleFrm.target = "TaskfrmServerFrame";
		parent.document.getElementById('TaskfrmServerFrame').src = "Waiting.asp";
		//TitleFrm.submit();
	}
//-->	
</script>
</head>
<body bgcolor="#F2F2EE" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <% If theFrm_Task = "Add" Then Response.Write("onload=AddAction();") Else Response.Write("onload=ClearAction();") End If %> >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<Form name="TitleFrm">
<tr <% If theFrm_Task <> "Add" Then Response.Write "bgcolor=""#637670""" End If %>> 
<td width="10" height="15"></td>
<td height="15" class="iagManagerSmallWht">
 <% If theFrm_Task <> "Add" Then %>
        <% If Not Recordset1.EOF Or Not Recordset1.BOF Then %>
        	<%=(Recordset1.Fields.Item("Heading").Value)%> : Select to <%= theFrm_Task %>
 	<% End If ' end Not Recordset1.EOF Or NOT Recordset1.BOF %>
 <% End If %>
</td>
<td width="10" height="15"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10"></td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10">
<div style="width:280px; height:200px; overflow: auto">
<table cellpadding="0" cellspacing="0" border="0">
<% 
Dim rsID
Dim rsTitle
While ((Repeat1__numRows <> 0) AND (NOT Recordset1.EOF)) 
	rsID = Recordset1.Fields.Item("ID").Value
	rsTitle = Recordset1.Fields.Item("Title").Value
%>
<tr> 
<td valign="middle"> 
<%  If theFrm_Task = "Amend" Then %>
<input type="radio" name="rsID" value="<%= rsID %>"  onClick="PerformAction(<%= rsID %>,1)"><%=(rsTitle)%> 
<%  ElseIf theFrm_Task = "Delete" Then %>
<input type="radio" name="rsID" value="<%= rsID %>" onClick="PerformAction(<%= rsID %>,2)"><%=(rsTitle)%> 

<% 
 ElseIf theFrm_Task = "" OR IsNull(theFrm_Task) Then 
	Response.Write "Error.asp"
 End If 
%>
</td>
</tr>
<% 
  	Repeat1__index=Repeat1__index+1
  	Repeat1__numRows=Repeat1__numRows-1
 	Recordset1.MoveNext()
	Wend
	CloseRecordSet Recordset1
%>
</Table>
</div>
</td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10">&nbsp;</td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10"> 
<input type="hidden" name="thetbl_Name" 	value="<%= Request("theTbl_Name") %>">
<input type="hidden" name="thetbl_ID_Name" 	value="ContentID">
<input type="hidden" name="theFrm_Task" 	value="<%= Request("theFrm_Task") %>">
<input type="hidden" name="theFrm_Type" 	value="<%= Request("theFrm_Type") %>">
<input type="hidden" name="theG" 			value="<%= Request("theG") %>">
<input type="hidden" name="theS" 			value="<%= Request("theS") %>">
<input type="hidden" name="theSS" 			value="<%= Request("theSS") %>">
<input type="hidden" name="formStatus" 		value="TRUE">
</td>
<td width="10" height="10"></td>
</tr>
</form>
</table>
</body>
</html>