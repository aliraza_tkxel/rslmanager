<%@LANGUAGE="VBSCRIPT"%>
<!--#include virtual="/Connections/WM_Which.asp" --> 
<!--#include file="../Class/ClassLib.asp" -->
<%
 Session.LCID = 2057
 Dim StartDate, EndDate, Summary, Upload1, Title, ContentID , User
 Dim objDB, objDB2
 Dim LastID
 Dim theSecID, theTaskIDNum, theTitleID, doneTask

 doneTask = Request("doneTask")
 if doneTask <> "" then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
// End -->
</script>
<%
 End if

 theSecID = Request("xNum")
 theTaskIDNum = Request("theTaskIDNum")
 
 if (Request("TitleIDNum") <> "") then theTitleID = Request("TitleIDNum")

 if theTaskIDNum = "1" then
	formState = "1"
	else if theTaskIDNum = "Add" then
		formState = "2"
	End if
 End If

 if formState = "1" then
	set rsLink = Server.CreateObject("ADODB.Recordset")
	rsLink.ActiveConnection = RSL_CONNECTION_STRING 'MM_WebManager_STRING
	rsLink.Source = "SELECT * FROM tbl_Links WHERE LinkID = " + Replace(theTitleID, "'", "''") + ""
	rsLink.CursorType = 0
	rsLink.CursorLocation = 2
	rsLink.LockType = 3
	rsLink.Open()
	rsLink_numRows = 0  
 End If
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ValidateForm(){
 thisForm.action = "serverSide/serv_Standard_Links_1.asp";
 thisForm.submit();
}
</SCRIPT></head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
    <td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> 
      <p> 
        <% if doneTask = "1" then %>
        Amend Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif"> 
        <% ELSE %>
        <% if doneTask = "2" then %>
        Addition Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif">&nbsp;&nbsp;You 
        can now add another... 
        <% Else %>
        Complete the form below to 
        <% if theTaskIDNum = "1" then %>
        amend 
        <% else if theTaskIDNum = "Add" then %>
        add a 
        <%End If 
	  End If %>
        Link. </p>
    </td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
  <form name="thisForm" method ="POST" action="ser" target ="_self">
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Title</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% if formState = "1" then %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="<% if rsLink("Title") <> "" then%><%=(rsLink.Fields.Item("Title").Value)%><% End If %>">
        <% else %>
        <input type="Text" name="Title" size="30" class="webManagerBox" value="">
        <% End If ' end Not rsLink.EOF Or NOT rsLink.BOF %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Address</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% if formState = "1" then %>
        <input type="text" name="LinkURL" size="30" class="webManagerBox" value="<% if rsLink("LinkURL") <> "" then%><%=(rsLink.Fields.Item("LinkURL").Value)%><% End If %>">
        <% else %>
        <input type="Text" name="LinkURL" size="30" class="webManagerBox" value="http://" >
        <% End If ' end Not rsLink.EOF Or NOT rsLink.BOF %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Description</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% if formState = "1" then %>
        <textarea name="LinkDescription" cols="30" class="webManagerBox" style="height: 60"><% if rsLink("LinkDescription") <> "" then%><%=(rsLink.Fields.Item("LinkDescription").Value)%><% End If %></textarea>
        <% else %>
        <textarea name="LinkDescription" cols="30" class="webManagerBox" style="height: 60" ></textarea>
        <% End If ' end Not rsLink.EOF Or NOT rsLink.BOF %>
      </td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% 
		if theTaskIDNum = "1" then %>
        <input type="button" name="go" value="Amend Link" class="iagButton" onClick="ValidateForm()">
        <% else if theTaskIDNum = "Add" then %>
        <input type="button" name="go" value="Add Link" class="iagButton" onClick="ValidateForm()">
        <input type="hidden" name="SectionLink" value="<%=theSecID%>">
        <% End If
		   End If   %>
        <input type="hidden" name="WhatToDo" value="<% if theTaskIDNum= "1" then %>AmendtheRecord<% else if theTaskIDNum= "Add" then %>AddNew<% End If End If%>">
        <input type="hidden" name="TitletoUpdate" value="<%=theTitleID%>">
        <% if formState = "1" then %>
        <input type="hidden" name="UniqueContentID" value="<%= rsLink("LinkID")%>">
        <input type="hidden" name="SectionLink" value="<%=theSecID%>">
        <% End If ' end Not rsLink.EOF Or NOT rsLink.BOF
		 %>
        <input type="hidden" name="SectionCount" value="<%=SectionCount%>">
        <% End If %>
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
  </form>
</table>
</body>
</html>
<%
if theTaakID = "1" then
rsLink.Close()
end if
%>
