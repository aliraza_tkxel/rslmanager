<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="Class/ClassLib.asp" -->
<%
Dim objDB1, objDB2, objDB3
Dim toDeleteRecord 
Dim DeleteRecord__theID
Dim SubSectionIdToDelete, PageState
Dim theTableName, theColName
theTableName = Session("WhatTable")
theColName = Session("TheIdColName")
formStatus = Request("formStatus")
If formStatus = "TRUE" then
 // get the record ID for the indvid title
 toDeleteRecord  = request("toDeleteRecord")
  
 // Delete record from the content table
 sql1 = "DELETE  FROM " & theTableName & " WHERE " & theColName & "=" & toDeleteRecord
 set objDB2 = new rsClass
 set rsDeleteRecord2  = objDB2.GetRecordset(sql1)
  
 PageState = "Deletion Complete"
 %>
 <script language="JavaScript">
<!--
	parent.gotoPage("Delete");	
	//parent.TitleFrame.window.location.reload();
    //document.refresh = true;
// End -->
</script>
 <%
Else

SQLstr = "SELECT * FROM " & theTableName & "  WHERE " & theColName & "='" + request("DeleteRecord") + "'"
SQLmode = "1"
set objDB1 = new rsClass
set rsDeleteRecord1  = objDB1.GetRecordset(SQLstr) 
//Get the SubSectionID from the content table for delete on submit
if NOT rsDeleteRecord1.EOF or NOT rsDeleteRecord1.BOF then
SubSectionIdToDelete = rsDeleteRecord1("SectionHeadingID")
End If
if rsDeleteRecord1.EOF or rsDeleteRecord1.BOF then
PageState = "To Delete"
End If
End If
%>
<html>
<head>
<title>WebManager :: Delete Title</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<script language="JavaScript">
<!--

	function gotoPage(){	
	form1.action = "DeleteTitle.asp";
	form1.target = "_self";
	form1.submit();	
	}

// End -->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<form name="form1" method="POST" action="<%=MM_editAction%>">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
    <tr> 
      <td colspan="4" height="5"></td>
    </tr>
	<% if Pagestate = "Deletion Complete" then %>
	<tr> 
      <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> You have Successfully deleted this title.
      </td>
    </tr>
	<% Else %>
    <tr> 
      <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> Are you sure 
        you want to delete this title. 
        <input type="hidden" name="hiddenField" value="<%request("id")%>">
      </td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk"> 
        <input type="submit" name="Submit" value=" Yes " class="iagButton">
        I would like to delete this title.</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk"> 
        <input type="button" name="No" value=" No " class="iagButton" onClick="gotoPage()">
        , cancel this request.</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td colspan="3" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
	<% End If %>
  </table>
  <%  if NOT Pagestate = "Deletion Complete" then  %>
  <input type="hidden" name="FormStatus" value="TRUE">
  <%'<input type="hidden" name="SubSectionIdToDelete" value="= SubSectionIdToDelete">%>
  <input type="hidden" name="toDeleteRecord" value=" <% if rsDeleteRecord1(theColName) <> "" then %><%= rsDeleteRecord1(theColName)%><% End If %>">
  <%End If %>
</form>
</body>
</html>