<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/WM_Which.asp" -->
<!--#include file="Class/ClassLib.asp" -->
<%
'response.buffer
'on error resume next
Dim SectionID
Dim Task


SectionID = Request("Sections")
Task = Request.Form("TaskToPerform")


Dim objDB1
 set objDB1 = new rsClass
 set rsTableName  = objDB1.GetRecordset("SELECT theDbTableName FROM dbo.tbl_Section_Headings WHERE SectionHeadingID = '" & SectionID & "'")
whatTable = rsTableName("theDbTableName")
Session("WhatTable") = whatTable
if sectionID <> "" then

 	if Task = "Amend" then
			Task = "CanAmend"
	else if Task = "Delete" then 
	   		Task = "CanDelete"
 	End If
		
    End If
 
 Dim objDB2
 set objDB2 = new rsClass
 set rsOptionList  = objDB2.GetRecordset("SELECT * FROM dbo." & whatTable & " WHERE SectionHeadingID = '" & SectionID & "' and " & Task & "= '1'") 
 Session("TheIdColName") = rsOptionList(0).Name
 Dim objDB3
 set objDB3 = new rsClass
 set rsSection = objDB3.GetRecordset("Select * from dbo.tbl_Section_Headings where SectionHeadingID = '" & SectionID & "'" )

 Dim Repeat1__numRows
 Dim Repeat1__index
 
 Repeat1__numRows = -1
 Repeat1__index = 0
 rsOptionList_numRows = rsOptionList_numRows + Repeat1__numRows 

End If
%>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<script language="JavaScript">
<!--
function Refresher(){

	document.refresh();

}

function PerformAction (GetListid , action){

		if (action == 1) {
		TitleForm.Task.value = action;
		TitleForm.action = "EditTitle.asp";  //jim=" + GetListid;
		 TitleForm.target = "serverFrame";
		//TitleForm.target = "_blank";
		TitleForm.submit();
		}
		if (action == 2) { 
		TitleForm.Task.value = action;
		TitleForm.action = "DeleteTitle.asp";
	    TitleForm.target = "serverFrame";
		
		TitleForm.submit();
		}
	}
// End -->	
</script>
</head>
<body text="#000000" bgcolor="#FFFFFF" topmargin="0" leftmargin="0">
<%'onload="returnData()"
			 If SectionID <> "" then
						  %>
<table bgcolor="#FFFFFF" width="99%" border="0" cellspacing="0" cellpadding="0">
  <% 
		if NOT rsOptionList.EOF then
					 %>
  <tr> 
    <td height="15" width="4%" bgcolor="#637670">&nbsp;</td>
    <td height="15" width="91%" bgcolor="#637670"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><b><font size="2" class="iagManagerSmallWht"><%= rsSection("SectionHeading")%> : Select 
       to 
      <% if Task = "CanAmend" then
	  		Response.write "Amend" 
	 	 else 
			Response.write "Delete"
	 	 End If 
	  %>
      </font></b></font></td>
    <td height="15" width="5%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="4%" bgcolor="#F9FAF8">&nbsp;</td>
    <td width="91%" bgcolor="#F9FAF8" height="150" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <Form name="TitleForm">
          <% 
			Dim theID            	
			while ((NOT rsOptionList.EOF)) 
				'  Response.Write ":" & objDB2.GetRecordset & ":" 
						%>
          <tr> 
            <td width="11%"> 
              <% 
				ListID =  rsOptionList(0)
				If task = "CanAmend" then  %>
              <input type="radio" name="AmendRecord" value="<%= ListID %>"  onClick="PerformAction(<%=ListID%>,1)">
              <% else if task = "CanDelete" then  %>
              <input  type="radio" name="DeleteRecord" value="<%= ListID %>" onClick="PerformAction(<%= ListID %>,2)">
              <% 
				End if  
				End if 
							%>
            </td>
            <td width="89%" class="iagManagerSmallBlk" valign="middle"><%= rsOptionList("Title")%></td>
          </tr>
          <% 
            rsOptionList.MoveNext()
			Wend
						  %>
          <input type="hidden" name="Section" value="<%= sectionID%>">
          <input type="hidden" name="Task">
        </form>
      </table>
    </td>
    <td width="5%">&nbsp;</td>
  </tr>
  <tr> 
    <td width="4%" bgcolor="#F9FAF8">&nbsp;</td>
    <td width="91%" class="iagManagerSmallBlk" align="right" bgcolor="#F9FAF8"><b>more</b> 
      &gt;&nbsp;&nbsp;</td>
    <td width="5%">&nbsp;</td>
  </tr>
  <% 
	 Else %>
  <tr> 
    <td height="15" width="4%" bgcolor="#637670">&nbsp;</td>
    <td height="15" width="91%" bgcolor="#637670"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><b><font size="2" class="iagManagerSmallWht">No 
      title to Administer</font></b></font></td>
    <td height="15" width="5%">&nbsp;</td>
  </tr>
  <% 
				 End if %>
</table>
<%
				 End if
				  %>
</body>
</html>
<%
rsOptionList.Close()
%>