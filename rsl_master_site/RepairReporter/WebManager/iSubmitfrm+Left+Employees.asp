<%@LANGUAGE="VBSCRIPT"%>
<% 'On Error Resume Next %>
<!--#include file="Connections/WM_Which.asp" -->
<!--#include file="LibFunctions.asp" -->
<%
Dim theTbl_Name
	 theTbl_Name = Request("theTbl_Name")

 Dim theFrm_Task
	 theFrm_Task = Request("theFrm_Task") 
	 
 If (Request("theG") <> "") Then 
	varIntGID = Request("theG")  
 End If 

 If (Request("theS") <> "") Then 
	varIntSHID = Request("theS")  
 End If 
 
 If (Request("theSS") <> "") Then 
	varIntSSID = Request("theSS")  
 End If 
 
 	DNS_STRING = RSL_CONNECTION_STRING
	
	' The below SQL is for 2 DD only	
	'SQL =   "SELECT * " &_
			'"FROM tbl_EmployeeGroup, tbl_Employee_Ref, tbl_Employee, tbl_Section_Headings " &_
			'"WHERE TopStructureID = " & varIntGID & " " &_
			'"AND tbl_EmployeeGroup.SectionHeadingID = " & varIntSHID & " " &_
			'"AND tbl_EmployeeGroup.EmployeeGroupID = tbl_Employee_Ref.EmployeeGroupID " &_
			'"AND tbl_Employee_Ref.EmployeeID = tbl_Employee.EmployeeID " &_
			'"AND tbl_EmployeeGroup.SectionHeadingID = tbl_Section_Headings.SectionHeadingID "
	
	' The below SQL is for 3 DD only, one additional line only!	
	SQL =   "SELECT  tbl_Employee.EmployeeID, EmployeeFName, EmployeeSName, SSHeading AS [Heading] FROM tbl_EmployeeGroup, tbl_Employee_Ref, tbl_Employee, tbl_Section_Headings, tbl_Page_Headings " &_
			"WHERE TopStructureID = " & varIntGID & " " &_
			"AND tbl_EmployeeGroup.SectionHeadingID = " & varIntSHID & " " &_
			"AND tbl_EmployeeGroup.EmployeeGroupID = tbl_Employee_Ref.EmployeeGroupID " &_ 
			"AND tbl_Employee_Ref.EmployeeID = tbl_Employee.EmployeeID " &_
			"AND tbl_EmployeeGroup.SectionHeadingID = tbl_Section_Headings.SectionHeadingID " &_
			"AND tbl_Page_Headings.SSHeadingID = " & varIntSSID & " " &_
			"ORDER BY tbl_Employee.EmployeeSName, tbl_Employee.EmployeeFName ASC"
			
			'Response.Write SQL
	
	OpenRecordSet RsEmployees
	RsEmployees_numRows = 0
%>
<%
Dim Repeat1__numRows
	Repeat1__numRows = -1
Dim Repeat1__index
	Repeat1__index = 0
	RsEmployees_numRows = RsEmployees_numRows + Repeat1__numRows
%>
<html>
<head>
<title>Web Manager :: Select...?</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/WebManager.css" type="text/css">
<script language="JavaScript">
<!--
	function PerformAction (GetListid , action) {
		if (action == 1) {
		TitleFrm.TaskID.value = action;
		TitleFrm.action = "EditTitle.asp";
		TitleFrm.method = "POST";
		//TitleFrm.target = "_blank";
		TitleFrm.target = "TaskfrmServerFrame";
		TitleFrm.submit();
		}
		if (action == 2) { 
		TitleFrm.TaskID.value = action;
		TitleFrm.action = "DeleteTitle.asp";
		TitleFrm.method = "POST";
		//TitleFrm.target = "_blank";
	    TitleFrm.target = "TaskfrmServerFrame";		
		TitleFrm.submit();
		}
	}
	
	function AddAction () {
		TitleFrm.TaskID.value = "0";
		TitleFrm.action = "EditTitle.asp";
		TitleFrm.target = "TaskfrmServerFrame";
		TitleFrm.submit();
	}
	function ClearAction () {
		TitleFrm.TaskID.value = "";
		TitleFrm.action = "EditTitle.asp";
		TitleFrm.target = "TaskfrmServerFrame";
		TitleFrm.submit();
	}
//-->	
</script>
</head>
<body bgcolor="#F2F2EE" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" <% If theFrm_Task = "Add" Then Response.Write("onload=AddAction();") Else Response.Write("onload=ClearAction();") End If %> >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<Form name="TitleFrm">
<tr <% If theFrm_Task <> "Add" Then Response.Write "bgcolor=""#637670""" End If %>> 
<td width="10" height="15"></td>
<td height="15" class="iagManagerSmallWht">
 <% If theFrm_Task <> "Add" Then %>
        <% If Not RsEmployees.EOF Or Not RsEmployees.BOF Then %>
        	<%=(RsEmployees.Fields.Item("Heading").Value)%> : Select to <%= theFrm_Task %>
 	<% End If ' end Not RsEmployees.EOF Or NOT RsEmployees.BOF %>
 <% End If %>
</td>
<td width="10" height="15"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10"></td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10">
<div style="width:280px; height:200px; overflow: auto">
<table cellpadding="0" cellspacing="0" border="0">
<% 
Dim rsID
Dim rsTitle
While ((Repeat1__numRows <> 0) AND (NOT RsEmployees.EOF)) 
	rsID = RsEmployees.Fields.Item("EmployeeID").Value
	rsTitle = RsEmployees.Fields.Item("EmployeeFName").Value & "&nbsp;" & RsEmployees.Fields.Item("EmployeeSName").Value
%>
<tr> 
<td valign="middle"> 
<%  If theFrm_Task = "Amend" Then %>
<input type="radio" name="rsID" value="<%= rsID %>"  onClick="PerformAction(<%= rsID %>,1)"><%=(rsTitle)%> 
<%  ElseIf theFrm_Task = "Delete" Then %>
<input type="radio" name="rsID" value="<%= rsID %>" onClick="PerformAction(<%= rsID %>,2)"><%=(rsTitle)%> 
<% 
 ElseIf theFrm_Task = "" OR IsNull(theFrm_Task) Then 
	Response.Write "Error.asp"
 End If 
%>
</td>
</tr>
<% 
  	Repeat1__index=Repeat1__index+1
  	Repeat1__numRows=Repeat1__numRows-1
 	RsEmployees.MoveNext()
	Wend
	CloseRecordSet RsEmployees
%>
</table>
</div>
</td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10">&nbsp;</td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10" height="10"></td>
<td height="10"> 
<input type="hidden" name="tbl_Name" value="<%= Request("theTbl_Name") %>">
<input type="hidden" name="tbl_ID_Name" value="EmployeeID">
<input type="hidden" name="TaskID" value="<%= Request("theFrm_Task") %>">
<input type="hidden" name="frmType" value="<%= Request("frmType") %>">
<input type="hidden" name="G" value="<%= Request("theG") %>">
<input type="hidden" name="S" value="<%= Request("theS") %>">
</td>
<td width="10" height="10"></td>
</tr>
</form>
</table>
</body>
</html>