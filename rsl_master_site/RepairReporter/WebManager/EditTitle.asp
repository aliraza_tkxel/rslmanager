<%@LANGUAGE="VBSCRIPT"%>
<% '	On Error Resume Next %>
<!--#include File="LibFunctions.asp" -->
<!--#include File="Connections/WM_Which.asp" -->
<%
Dim rsFormType 	' Declare Record Set
Dim rsFormNum 	' Declare Record Set
Dim rsFile 		' Declare Record Set

Dim Tbl_Name
Dim Tbl_ID_Name
Dim IntRsID
Dim IntGID
Dim IntSID 
Dim IntSSID
Dim IntPID
Dim IntTaskID
Dim IntFrmID

	Tbl_Name 	= Request("theTbl_Name")
	Tbl_ID_Name = Request("theTbl_ID_Name")	
	IntRsID 	= Request("rsID")	
	IntGID 		= Request("theG")
	IntSID 		= Request("theS")
	IntSSID		= Request("theSS")
	IntPID 		= Request("theP")	
	IntTaskID 	= Request("theFrm_Task")	
	IntFrmID 	= Request("theFrm_Type")

	if IntTaskID = "Add" then	

		DNS_STRING = RSL_CONNECTION_STRING		' Set connection str for function recordsets
			
			' If 2 dropdowns are selected then get "Form Type" ID
			if  (IntSSID = "") AND (IntPID = "") then
				SQL  = "SELECT * FROM tbl_Standard_Forms, tbl_Section_Headings WHERE tbl_Standard_Forms.FormID = tbl_Section_Headings.FormType AND tbl_Section_Headings.SectionHeadingID = " & IntSID	
				Response.Write "1" & SQL & "<BR>"
			' If 3 dropdowns get the "form type" id from lower tbl
			Else
				SQL = "SELECT DISTINCT Form_Type, PageHeadingID, PH.SSHeading, Tbl_Name  FROM tbl_Content_Ref CR, tbl_Page_Headings PH  WHERE CR.SectionHeadingID = " & IntSID & " AND CR.PageHeadingID = PH.SSHeadingID AND PageHeadingID = " & IntSSID & " ORDER BY PageHeadingID, PH.SSHeading "
				Response.Write "2" & SQL  & "<BR>"
			End If
			
		OpenRecordSet rsFormType
		IntFrmID = rsFormType("FormType")
		CloseRecordSet rsFormType
		 
			 DNS_STRING = RSL_CONNECTION_STRING
			 		
			 SQL = "SELECT FileName AS Frm_File FROM tbl_Standard_Forms WHERE tbl_Standard_Forms.FormID = " & IntFrmID
			 OpenRecordSet rsFormNum
			 
			 Dim Frm_File
				 Frm_File = rsFormNum.Fields.Item("Frm_File").value		
			 
			 CloseRecordSet rsFormNum		
	    
		
		' Redirection to correct Frm for ADD or AMEND
		
		If IntSSID <> "" then 
			LineToAdd = "&theSS=" & IntSSID
		End IF
		
		Response.Redirect "EditForms/" & (Frm_File) & "?theG=" & IntGID &"&theS=" & IntSID & "&theFrm_Task=" & IntTaskID & LineToAdd & "&theFrm_Type=" & IntFrmID & "&thetbl_Name=" & Tbl_Name

	
	Else	
    
		' Below Requires a Recordset (Amend)
		
		DNS_STRING = RSL_CONNECTION_STRING
		SQL = "SELECT * FROM " & Tbl_Name & ", tbl_Standard_Forms WHERE " & Tbl_Name & "." & Tbl_ID_Name & " = " & IntRsID & " AND " & Tbl_Name & ".FormType = tbl_Standard_Forms.FormID" 
		Response.write SQL
		OpenRecordSet rsFile
	
		 If NOT rsFile.EOF Or NOT rsFile.BOF Then
			If Not theSS <> "" Then
				Response.Redirect "EditForms/" & rsFile("FileName") & "?theG=" & IntGID & "&theS=" & IntSID & "&rsID=" & IntRsID & "&theFrm_Task=" & IntTaskID & "&theFrm_Type=" & IntFrmID & "&thetbl_Name=" & Tbl_Name
			Else
				Response.Redirect "EditForms/" & rsFile("FileName") & "?theG=" & IntGID & "&theS=" & IntSID & "&rsID=" & IntRsID & "&theFrm_Task=" & IntTaskID & "&theSS=" & IntSS & "&theFrm_Type=" & IntFrmID & "&thetbl_Name=" & Tbl_Name
			End If
		 Else
			'Response.Redirect "Error.asp"
			'Response.Write SQL
		 End If
	 
		CloseRecordSet rsFile
	
	End If
	
%>