<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!--#include file="../Connections/WM_Which.asp" -->
<% 'On Error Resume Next %>
<%
Dim SQL
Dim RsID
Dim JsToggle
Dim Summary
Dim ContentID

	RsID = Request("ContentID")
	SQL = "SELECT *  FROM tbl_News_And_Story_Content NSC WHERE NSC.ContentID = " + RsID

Dim rsiSelectQuery
Dim rsiSelectQuery_numRows
set rsiSelectQuery = Server.CreateObject("ADODB.Recordset")
	rsiSelectQuery.ActiveConnection = RSL_CONNECTION_STRING
	rsiSelectQuery.Source = SQL
	rsiSelectQuery.CursorType = 0
	rsiSelectQuery.CursorLocation = 2
	rsiSelectQuery.LockType = 3
	rsiSelectQuery.Open()
	rsiSelectQuery_numRows = 0
		
	If RsID <> "" AND NOT rsiSelectQuery.EOF Then
		JsToggle = 1
		Summary = rsiSelectQuery.Fields.Item("Summary_or_Body").Value
		ContentID = rsiSelectQuery.Fields.Item("ContentID").Value
	Else
		JsToggle = 0
		Summary = ""
		ContentID = ""
	End If
%><html>
<head>
<title>News : Story Summary</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
<!--
function AddLink() {
var sText = document.selection.createRange();
if (!sText==""){
     document.execCommand("CreateLink");
	 sText.parentElement().tagName == "A";
     //if (sText.parentElement().tagName == "A"){
       //sText.parentElement().innerText=sText.parentElement().href;
       //document.execCommand("ForeColor","false","#FF0033");
    // }    
  }
else{
    alert("Please select some blue text!");
  }   
}

function displayHTML(form) {
frm.theContent2.value = theContent.innerHTML; 
var HTML = frm.theContent2.value;
	win = window.open("Preview", 'popup', 'toolbar=0, status=0, scrollbars=yes, width=400, height=300');
	win.document.write("<html><title>Preview</title><link rel='stylesheet' href='css/WebManager.css' type='text/css'><body>" + HTML + "</body></html>");
}

<% 'If JsToggle = 1 Then %>
//function SubmitFrm() {
	//frm.theContent2.value = theContent.innerHTML;
	//frm.action = "/webmanager/EditForms/ServerSide/serv_Standard_News_Body_1.asp" ;
	//frm.target = "FrmServerFrame";
	//frm.method = "post";
	//frm.submit();
//}
<%' Else %>
//function SubmitFrm() {
    //opener.document.frm.Summary.value = theContent.innerHTML;
    //return false;
//}
//function GetFrm() {
   // theContent.innerHTML = opener.document.frm.Summary.value;
   // return false;
//}
//window.onload = GetFrm();
<% 'End If %>
//-->
</script>
<script language=javascript defer>
	function returnData(){
		opener.frm.Summary.value = theContent.innerHTML;
		SubmitFrm();
		savedtable.style.display = "block";		
		}
		
	function hideButtons(){
		opener.frm.Summary.value = theContent.innerHTML;
		if (savedtable.style.display = "block")
		savedtable.style.display = "none";
		}
</script>
</head>
<body bgcolor="#F2F2EE" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<% If theFrm_Task = "Add" then %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
		<td width="10" height="10"></td>
		<td height="10"></td>
		<td width="10" height="10"></td>
	</tr>
	<tr>
		<td width="10"></td>
		<td>You are unable to add new content as the content already exists, you are allowed to amend, or delete this content only </td>
		<td width="10" height="10"></td>
	</tr>
</table>
<% Else %>
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100">
<form name="frm" method="post">
<tr> 
<td width="10" height="10"></td>
<td width="10" height="10"></td>
<td width="10" height="10"></td>
</tr>
<tr> 
<td width="10">&nbsp;</td>
<td width="100">Story Body</td>
<td width="10">&nbsp;</td>
</tr>
<div unselectable="on"> 
<tr> 
<td width="10">&nbsp;</td>
<td> 
<div id="theContent" name="theContent" contenteditable style="width:280; height:300; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;" onClick="hideButtons()"><%=(Summary)%></div>
</td>
<td width="10">&nbsp;</td>
</tr>
<tr>
<td width="10">&nbsp;</td>
<td>&nbsp;</td>
<td width="10">&nbsp;</td>
</tr>
<tr> 
<td width="10">&nbsp;</td>
<td> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td align="left"> 
                <input type="button" name="Preview" value="Save Story" onClick="returnData()" class="iagButton">
              </td>
              <td width="10">&nbsp;</td>
              <td align="right"> 
                <% If theFrm_Task = "Delete" OR IsNull(theFrm_Task) Then %>
                <% Else %>
                <button unselectable="On" onClick='document.execCommand("Bold");theContent.focus();' class="iagButton"><b>B</b></button> 
                <button unselectable="On" onClick='document.execCommand("Italic");theContent.focus();' class="iagButton"><i><b>I</b></i></button> 
                <button unselectable="On" onClick='document.execCommand("Underline");theContent.focus();' class="iagButton"><u><b>U</b></u></button> 
                <button unselectable="On" onClick="AddLink()" class="iagButton">Hyperlink</button> 
                <% End If %>
              </td>
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>
            </tr>
            <tr> 
              <td align="left" colspan="3"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="0" name="savedtable" id="savedtable" style="display: none">
                  <tr > 
                    <td>Story Saved </td>
                    <td> 
                      <div align="right">
                        <input type="button" name="Close" value="Close Window" onClick="JavaScript:window.close()" class="iagButton">
                      </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
</td>
<td width="10">&nbsp;</td>
</tr>
</div>
<tr> 
<td width="10">&nbsp;</td>
<td> 
<% 'If JsToggle = 1 Then %>
<input type="hidden" name="ContentID" value="<%=(ContentID)%>">
<input type="hidden" name="theG" value="<%'=(rsiSelectQuery.Fields.Item("TopStructureID").Value)%>">
<input type="hidden" name="theS" value="<%'=(rsiSelectQuery.Fields.Item("SectionHeadingID").Value)%>">
<input type="hidden" name="theSS" value="<%'=(rsiSelectQuery.Fields.Item("PageHeadingID").Value)%>">
<input type="hidden" name="theFrm_Type" value="<%'=(rsiSelectQuery.Fields.Item("frm_Type").Value)%>">
<input type="hidden" name="theTbl_Name" value="<%'=(rsiSelectQuery.Fields.Item("tbl_Name").Value)%>">
<input type="hidden" name="theTask" value="Amend">
<% 'End If %>
<input type="hidden" name="theContent2" value="">
</td>
<td width="10">&nbsp;</td>
</tr>
</form>
</table>
<% End If %>

<SCRIPT LANGUAGE="JavaScript">
<!--
<% If JsToggle = 1 Then %>
function SubmitFrm() {
	frm.theContent2.value = theContent.innerHTML;
	frm.action = "/webmanager/EditForms/ServerSide/serv_Standard_News_Body_1.asp" ;
	frm.target = "FrmServerFrame";
	frm.method = "post";
	frm.submit();
}
<% Else %>
function SubmitFrm() {
    opener.document.frm.Summary.value = theContent.innerHTML;
    //window.close();
    return false;
}
function GetFrm() {
    theContent.innerHTML = opener.document.frm.Summary.value;
    return false;
}
window.onload = GetFrm;
<% End If %>
//-->
</script>
<iframe name="FrmServerFrame" id="FrmServerFrame" frameborder="0" style='display:none'>Frm Server Frame</iframe> 
</body>
</html>