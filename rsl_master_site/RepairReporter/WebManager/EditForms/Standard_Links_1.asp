<%@LANGUAGE="VBSCRIPT"%>
<!--#include File="../LibFunctions.asp" -->
<!--#include File="../Connections/WM_Which.asp" -->
<%
 Session.LCID = 2057
 
 Dim doneTask 						' The Status Before and after an operation has been implemented 
 	 doneTask = Request("doneTask") ' Below Start redirect 
 If doneTask <> "" Then
%>
	<script language="JavaScript">
	<!--
		parent.gotoPage("Amend");	
	//-->
	</script>
<%
 End If 							' End Redirect Code

 
 Dim IntTaskID
 Dim IntRsID 	' Auto Number (Record Number)
 Dim IntGID
 Dim IntSID
 Dim IntSSID
 Dim IntFrmID
 Dim frmState
 
	 IntGID 	= Request("GID")
	 IntSID 	= Request("SID")
	 IntSSID 	= Request("theSS")
	 IntFrmID 	= Request("theFrm_Type")  
	 IntTaskID 	= Request("TaskID") 
 
 If (Request("rsID") <> "") Then 
 	IntRsID = Request("rsID")
 End If

 If IntTaskID = "1" Then
	frmState = "1"
	Else If IntTaskID = "Add" Then
		frmState = "2"
	End if
 End If

 If frmState = "1" then
 	Dim rsLink												' Declare Recordset
 	Dim rsLink_numRows
	set rsLink = Server.CreateObject("ADODB.Recordset")
		rsLink.ActiveConnection = RSL_CONNECTION_STRING
		rsLink.Source = "SELECT tbl_Links.LinkID AS [ID], tbl_Links.LinkTitle AS [Title], tbl_Links.LinkUrl As [Url], tbl_Links.LinkDescription As [Description] FROM tbl_Links WHERE LinkID = " + Replace(IntRsID, "'", "''") + ""
		rsLink.CursorType = 0
		rsLink.CursorLocation = 2
		rsLink.LockType = 3
		rsLink.Open()
		rsLink_numRows = 0  
 End If
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/WebManager.css" rel="stylesheet" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
<!--
function ValidateForm(){
 	Linkfrm.action = "serverSide/serv_Standard_Links_1.asp";
	Linkfrm.method = "POST"
	Linkfrm.target = "_self"	
 	Linkfrm.submit();
}
//-->
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr> 
    <td width="2%" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000">&nbsp;</td>
    <td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE" style="border-bottom: 1px solid #000000"> 
      <p> 
        <% If doneTask = "1" Then %>Amend Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif"> 
        <% Else %>
        	<% If doneTask = "2" Then %>Addition Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif">&nbsp;&nbsp;You can now add another... 
        	<% Else %>Complete the form below to 
        		<% If IntTaskID = "1" Then %>amend 
        			<% Else If IntTaskID = "Add" Then %>add a 
        			<% End If %>
	    		<% End If %>Link. 
	</p>
    </td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
  <form name="Linkfrm">
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Title</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% if frmState = "1" then %>
        <input type="text" name="LinkTitle" size="30" class="webManagerBox" value="<%=(rsLink.Fields.Item("Title").Value)%>">
        <% else %>
        <input type="Text" name="LinkTitle" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Address</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="LinkURL" size="30" class="webManagerBox" value="<%=(rsLink.Fields.Item("Url").Value)%>">
        <% Else %>
        <input type="Text" name="LinkURL" size="30" class="webManagerBox" value="http://" >
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk" valign="top">Link Description</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <textarea name="LinkDescription" cols="30" class="webManagerBox" style="height: 60"><%=(rsLink.Fields.Item("Description").Value)%></textarea>
        <% Else %>
        <textarea name="LinkDescription" cols="30" class="webManagerBox" style="height: 60"></textarea>
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="76%" class="iagManagerSmallBlk"> 
        <% If IntTaskID = "1" Then %>
			<input type="button" name="go" value="Amend Link" class="iagButton" onClick="ValidateForm()">
		<% Else If IntTaskID = "Add" Then %>
			<input type="button" name="go" value="Add Link" class="iagButton" onClick="ValidateForm()">
			<input type="hidden" name="SubSectionLink" value="<%=IntSSID%>">
			<input type="hidden" name="SectionLink" value="<%=IntSID%>">
			<input type="hidden" name="GroupLink" value="<%=IntGID%>">
			<input type="hidden" name="theFrmID" value="<%=IntFrmID%>">
		<% End If %>
		<% End If %>
		
			<input type="hidden" name="WhatToDo" value="<% if IntTaskID= "1" then %>AmendtheRecord<% else if IntTaskID= "Add" then %>AddNew<% End If End If%>">
			<input type="hidden" name="TitletoUpdate" value="<%=LinkID%>">
			
		<% If frmState = "1" Then %>
			<input type="hidden" name="UniqueContentID" value="<%= (rsLink.Fields.Item("ID").Value)%>">
			<input type="hidden" name="SectionLink" value="<%=IntSID%>">
		<% End If %>
		
			<input type="hidden" name="SectionCount" value="<%=SectionCount%>">		
      </td>
    </tr>
	</form>	
		<% End If %>	
    <% End If %>
	<% If doneTask = "1" Then Response.Write "</p></td></tr>" End If %>
</table>
</body>
</html><%
' Whats theTaakID ??
' if theTaakID = "1" then
	' rsLink.Close()
' end if
%>