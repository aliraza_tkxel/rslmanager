<%@LANGUAGE="VBSCRIPT"%>
<% 'Option Explicit %>
<% 'On Error Resume Next %>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../LibFunctions.asp" -->

<%
 Session.LCID = 2057 ' British English
 
 ' Dimmed in LibFunctions
 ' Declared Globally
 DNS_STRING = RSL_CONNECTION_STRING
 
 Dim DNS_STRING ' Connection String
 Dim rsContent  ' Declare Recordset
 Dim rsUser  	' Declare Recordset
 Dim rsSH  		' Declare Recordset
 
 Dim IntTaskID  ' Action to be carried out
 Dim frmState   ' i.e. Amend = 1 , Delete = 2, Add = ?
 Dim IntGID	    ' Top Structure
 Dim IntSID	    ' Section
 Dim IntSSID    ' Sub Section
 Dim IntPID		' Page
 Dim IntFrmID	' Form Type
 
 Dim doneTask
 Dim rsID		' Recordset Identity
 
 Dim checkYes 	' Radio Check Boxes
 Dim checkNo 	' Radio Check Boxes

	IntSID 		= Request("theS")
	IntGID 		= Request("theG")
	IntSSID		= Request("theSS")
	IntPID 		= Request("theP")
	IntTaskID 	= Request("theFrm_Task")
	IntFrmID 	= Request("theFrm_Type")
	strtbl_Name = Request("thetbl_Name")
	
	'Response.Write "G" 		& IntGID 		& "<BR><BR>"
	'Response.Write "S" 		& IntSID  		& "<BR><BR>"
	'Response.Write "SS" 	& IntSSID  		& "<BR><BR>"
	'Response.Write "P" 		& IntPID  		& "<BR><BR>"
	'Response.Write "Task" 	& IntTaskID  	& "<BR><BR>"
	'Response.Write "Form" 	& IntFrmID 		& "<BR><BR>"
	'Response.Write "Table" 	& strtbl_Name 	& "<BR><BR>"

%>

<%
   doneTask = Request("doneTask")
if doneTask <> "" then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
// End -->
</script>
<%
End if

If IntTaskID = "1" Then
	frmState = "1"
	Else If IntTaskID = "Add" Then
		frmState = "2"
	End if
End If

'If  IntTaskID >= "1" Then  'NEW... DID NOT WORK

  If frmState = "1" Then  
  
  	If (Request("rsID") <> "") Then 
		rsID = Request("rsID")
	End If
	
		SQL = "SELECT *  FROM tbl_News_And_Story_Content NSC WHERE NSC.ContentID = " + Replace(rsID, "'", "''") + ""
		OpenRecordSet rsContent		
  	
    Dim rsIntSSID
		rsIntSSID = rsContent("SectionHeadingID").value		
		SQL = "SELECT *  FROM tbl_Section_Headings WHERE SectionHeadingID = " & rsIntSSID
		OpenRecordSet rsSH
			
   If NOT rsContent.EOF Or NOT rsContent.BOF Then   
   	If rsContent("UserID") <> "" Then
		SQL = "SELECT *  FROM UserData WHERE WUserID = " & rsContent("UserID") & ""
		OpenRecordSet rsUser
	End If
   End If
   
  End If ' End Of Form State
 'End If
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="calander/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function ValidateForm(){

 var box  = document.frm.upload1;
 var box2 = document.frm.upload2;
 if (box.value == ""){
 	alert("Please select a pdf,doc or xls to upload");
	box.focus();
	return false;
	}
 if (box2.value == ""){
 	alert("Please select a cover page to upload");
	box2.focus();
 	return false;
	}

  frm.action = "serverSide/serv_Standard_Publication_1.asp";
  frm.submit();
}

function AmendTheForm(){

	if ((frm.upload1.value == "") && (frm.upload2.value != "")){
		 frm.FileSent.value = "secondfile";
	}
	if ((frm.upload2.value == "") && (frm.upload1.value != "")){
		 frm.FileSent.value = "firstfile";
	
	}
	if ((frm.upload2.value == "") && (frm.upload1.value == "")){
		 frm.FileSent.value = "nofile";
	
	}
	  frm.action = "serverSide/serv_Standard_Publication_1.asp";
	  frm.submit();
	
	}
</SCRIPT>
</head>
<body bgcolor="#F2F2EE" text="#000000" topmargin="0" leftmargin="0">
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
		
      <table width="405" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
		<td width="10" height="15" <% 
		If IntTaskID <> "" Then 
			Response.Write "bgcolor='#FFFFFF' style='border-bottom: 1px solid #000000'" 
		Else 
			Response.Write "bgcolor=""F2F2EE"""
		End IF
		%>>&nbsp;</td>
		<td height="15" class="iagManagerSmallBlk" 
		<% 
		If IntTaskID <> "" Then 
			Response.Write "bgcolor=""#FFFFFF"" style=""border-bottom: 1px solid #000000""" 
		Else 
			Response.Write "bgcolor=""F2F2EE"""
		End IF %>
		>
     <% If doneTask = "1" Then %>
	  	Amend Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif"> 
      	<a href="<%= Session("svDomain")%>/<%=rsSH("SectionHeading")%>/<%=rsSH("SectionHeading")%>.asp" target="_blank">Click here</a> to view web page 
      <% Else %>   
	  		<% If doneTask = "2" Then %>
	  			Addition Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif">&nbsp;&nbsp;You can now add another...
	  		<% Else %> 	  			
      			<% If IntTaskID = "1" Then %>
      				Complete the form below to amend a publication.
      			<% Else If IntTaskID = "Add" OR IntTaskID = "0" Then %>
      				Complete the form below to add a publication.
				<% ElseIf IntTaskID = "" Then %>
      				<!--Please make a selection from the publications to the left.//-->
      		<%
	  		  End If 
	  		  End If 
	  		%>
	  </td>
  </tr>
	</table>
	</td>
  </tr>
  <tr>
    <td>
	<% 
'If doneTask = "" OR IsNull(doneTask) Then  
If IntTaskID = "" OR IsNull(IntTaskID) Then 
Response.Write "empty" 
%>
<% Else %>
	  <div id="thePublications" name="thePublications" style="position:absolute; left:9px; top:30px; width:390; height:190; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;"> 
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
  <form name="frm" method = "POST" target = "_self" enctype = "multipart/form-data">
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Start Date </td>
      <td width="72%" valign="middle"> 
        <% If frmState = "1" Then %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" readonly style="Width:75px" value="<%=(rsContent.Fields.Item("StartDate").Value)%>">
        <% Else %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" readonly style="Width:75px" value="<%= Date()%>">
        <% End If %>
        <a href="javascript:;" onClick="YY_Calendar('StartDate',113,50,'de','#FFFFFF','#637670','YY_calendar1')"><FONT color="#9287AE"> 
        <font color="#003333"><b> <span class="calText">[Choose Date]</span></b></font></font></a> 
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">End Date</td>
      <td width="72%" class="iagManagerSmallBlk" valign="middle"> 
        <% If frmState = "1" Then %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" readonly style="Width:75px" value="<% if rsContent("StartDate") <> "" then%><%=(rsContent.Fields.Item("EndDate").Value)%><%End If %>">
        <% Else %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" readonly style="Width:75px" value="<%= Date()+ 256 %>">
        <% End If %>
        <a href="javascript:;" onClick="YY_Calendar('EndDate',113,80,'de','#FFFFFF','#637670','YY_calendar1')"><FONT class="calText" color="#9287AE"> 
        <font color="#003333"><b> [Choose Date]</b></font></FONT></a> </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Publication Name</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Title").Value)%>">
        <% Else %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Summary</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"><%=(rsContent.Fields.Item("Summary_or_Body").Value)%></textarea>
        <% Else %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"></textarea>
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Upload <br>
        (.pdf/.doc.xls)</td>
      <td width="72%" valign="top"> 
        <input type="file" name="upload1" value="Browse" class="iagButton">
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr>
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Front Cover Image<br>
        <font size="1"> h : 100 pxl<br>
        w : 100 pxl </font></td>
      <td width="72%" valign="top"> 
        <input type="file" name="upload2" value="Browse" class="iagButton">
      </td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">&nbsp;</td>
      <td width="72%">&nbsp; </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Published by</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        	<% 
			  ' Problem with ASP Code id no Session variable exists, excemption Error occurs!
			  if NOT rsUser.EOF then
			  If rsContent("UserID") <> "" then 
		         
				  If Not rsUser("WUserID") = Session("svUserID") Then 
				 %>
        			Previously :: <%= rsUser("UserFName")%>&nbsp;<%= rsUser("UserSName")%><br>
        			Update to :: 
        		<% Else %>
        			<%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%>
        		<% End If 
				End If %>
        	<% Else %>
        		<input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
       			 <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        	<% End If %>
        <% Else %>
        	<input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        	<%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If IntTaskID = "1" Then %>
        <input type="button" name="go" value="Amend publication" class="iagButton" onClick="AmendTheForm()">
        <% Else If IntTaskID = "Add" OR  IntTaskID = "0" Then %> 
        <input type="button" name="go" value="Add a publication" class="iagButton" onClick="ValidateForm()">
		<input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
        <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
		<input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
		<input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
		<input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
		<input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
		<input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">				
        <% 
		End If
		End If   
		%>
        <input type="hidden" name="WhatToDo" value="<% If IntTaskID= "1" Then Response.Write("Amend") Else Response.Write("Add") End If %>">
        <input type="hidden" name="TitletoUpdate" value="<%=IntRsID%>">
        <% If frmState = "1" Then %>	
		<input type="hidden"	name="ContentID" 	value="<%= rsContent("ContentID")%>">
		<input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
        <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
		<input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
		<input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
		<input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
		<input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
		<input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">
		<input type="hidden" 	name="FileSent" 	value="">
        <% End If %>
        <% End If %>
        <% End If %>
      </td>
    </tr>
  </form>
</table>
</div>
<% End If %>
	</td>
  </tr>
</table>
</body>
</html>