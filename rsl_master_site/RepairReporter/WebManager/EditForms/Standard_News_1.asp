<%@LANGUAGE="VBSCRIPT"%>
<% 'Option Explicit %>
<% 'On Error Resume Next %>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../LibFunctions.asp" -->

<%
 Session.LCID = 2057 ' British English
 
 ' Dimmed in LibFunctions
 ' Declared Globally
 DNS_STRING = RSL_CONNECTION_STRING
 
 Dim DNS_STRING ' Connection String
 Dim rsContent  ' Declare Recordset
 Dim rsUser  	' Declare Recordset
 Dim rsSH  		' Declare Recordset
 
 Dim IntTaskID  ' Action to be carried out
 Dim frmState   ' i.e. Amend = 1 , Delete = 2, Add = ?
 Dim IntGID	    ' Top Structure
 Dim IntSID	    ' Section
 Dim IntSSID    ' Sub Section
 Dim IntPID		' Page
 Dim IntFrmID	' Form Type
 
 Dim doneTask
 Dim rsID		' Recordset Identity
 
 Dim checkYes 	' Radio Check Boxes
 Dim checkNo 	' Radio Check Boxes

	IntSID 		= Request("theS")
	IntGID 		= Request("theG")
	IntSSID		= Request("theSS")
	IntPID 		= Request("theP")
	IntTaskID 	= Request("theFrm_Task")
	IntFrmID 	= Request("theFrm_Type")
	strtbl_Name = Request("thetbl_Name")
	
	'Response.Write "G" 		& IntGID 		& "<BR><BR>"
	'Response.Write "S" 		& IntSID  		& "<BR><BR>"
	'Response.Write "SS" 	& IntSSID  		& "<BR><BR>"
	'Response.Write "P" 		& IntPID  		& "<BR><BR>"
	'Response.Write "Task" 	& IntTaskID  	& "<BR><BR>"
	'Response.Write "Form" 	& IntFrmID 		& "<BR><BR>"
	'Response.Write "Table" 	& strtbl_Name 	& "<BR><BR>"

%>

<%
   doneTask = Request("doneTask")
if doneTask <> "" then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
// End -->
</script>
<%
End if

If IntTaskID = "1" Then
	frmState = "1"
	Else If IntTaskID = "Add" Then
		frmState = "2"
	End if
End If

If  IntTaskID >= "1" Then  'NEW... DID NOT WORK

  If frmState = "1" Then  
  
  	If (Request("rsID") <> "") Then 
		rsID = Request("rsID")
	End If
	
  		SQL = "SELECT *  FROM tbl_News_And_Story_Content NSC WHERE NSC.ContentID = " + Replace(rsID, "'", "''") + ""
		OpenRecordSet rsContent		
  	
    	Dim rsIntSSID
			rsIntSSID = rsContent("SectionHeadingID").value		
		SQL = "SELECT *  FROM tbl_Section_Headings WHERE SectionHeadingID = " & rsIntSSID
		OpenRecordSet rsSH
			
   If NOT rsContent.EOF Or NOT rsContent.BOF Then   
   	If rsContent("UserID") <> "" Then
		SQL = "SELECT *  FROM UserData WHERE WUserID = " & rsContent("UserID") & ""
		OpenRecordSet rsUser
	End If
   End If
   
  End If ' End Of Form State
 End If
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="calander/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
<!--
function AddLink() {
var sText = document.selection.createRange();
if (!sText==""){
     document.execCommand("CreateLink");
	 sText.parentElement().tagName == "A";
       //if (sText.parentElement().tagName == "A"){
       //sText.parentElement().innerText=sText.parentElement().href;
       //document.execCommand("ForeColor","false","#FF0033");
       //}    
  }
else{
    alert("Please select some blue text!");
  }   
}

function displayHTML(form) {
		//frm.theContent2.value = theContent.innerHTML; 
var HTML = '<b>' + frm.Title.value + '</b>' + '<br><br>' + frm.Headline.value + '<br><br>' + frm.Summary.value + '<br><br>' + '<a href="frm.Links.value">' + frm.Links.value + '</a>';
	win = window.open("Preview", 'popup', 'toolbar=0, status=0, scrollbars=yes, width=400, height=300');
	win.document.write("<html><title>Preview</title><link rel='stylesheet' href='css/WebManager.css' type='text/css'><body>" + HTML + "</body></html>");
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


//-->
</script>
</head>
<body bgcolor="#F2F2EE" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <table width="405" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
    <td width="10" height="15" <% 
	If IntTaskID <> "" Then 
		Response.Write "bgcolor='#FFFFFF' style='border-bottom: 1px solid #000000'" 
	Else 
		Response.Write "bgcolor=""F2F2EE"""
	End IF
	%>>&nbsp;</td>
    <td height="15" class="iagManagerSmallBlk" 
	<% 
	If IntTaskID <> "" Then 
		Response.Write "bgcolor=""#FFFFFF"" style=""border-bottom: 1px solid #000000""" 
	Else 
		Response.Write "bgcolor=""F2F2EE"""
	End IF %>
	>
	  <% If doneTask = "1" Then %>
	  	Amend Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif"> 
      	<a href="<%= Session("svDomain")%>/<%=rsSH("SectionHeading")%>/<%=rsSH("SectionHeading")%>.asp" target="_blank">Click here</a> to view web page 
      <% Else %>   
	  		<% If doneTask = "2" Then %>
	  			Addition Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif">&nbsp;&nbsp;You can now add another...
	  		<% Else %> 	  			
      			<% If IntTaskID = "1" Then %>
      				Complete the form below to amend news article.
      			<% Else If IntTaskID = "Add" OR IntTaskID = "0" Then %>
      				Complete the form below to add a news article.
				<% ElseIf IntTaskID = "" Then %>
      				<!--Please make a selection from the news stories to the left.//-->
      		<%
	  		  End If 
	  End If 
	  %> 
	</td>
  </tr>
</table>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<% 
'If doneTask = "" OR IsNull(doneTask) Then  
If IntTaskID = "" OR IsNull(IntTaskID) Then
Response.Write "empty"  
%>
<% Else %>
<div id="theNews" name="theNews" style="position:absolute; left:9px; top:30px; width:390; height:190; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
    <tr> 
      <td colspan="3" height="5"></td>
    </tr>
    <tr> 
      <td width="2%" height="5"></td>
      <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
    </tr>
    <form name="frm" method="POST" action="serverSide/serv_Standard_News_1.asp" enctype="multipart/form-data">
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Start Date </td>
        <td width="72%"> 
          <% If frmState = "1" Then %>
          <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<% if rsContent("StartDate") <> "" then%><%= rsContent("StartDate")%><% End If %>">
          <% Else %>
          <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<%= Date()%>">
          <% End If %>
          <a href="javascript:;" onClick="YY_Calendar('StartDate',113,80,'de','#FFFFFF','#637670','YY_calendar1')"> 
          <font class="calText" color="#003333"><b> [Choose Date]</b></font></a> 
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">End Date</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
          <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<% if rsContent("EndDate") <> "" then%><%=rsContent("EndDate")%><% End IF%>">
          <% Else %>
          <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<%= DateAdd("yyyy", 1, Date()) %>">
          <% End If %>
          <a href="javascript:;" onClick="YY_Calendar('EndDate',113,80,'de','#FFFFFF','#637670','YY_calendar1')"><font class="calText" color="#003333"><b> 
          [Choose Date]</b></font></a> </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Title</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
          <input type="text" name="Title" size="30" class="webManagerBox" value="<% if rsContent("Title") <> "" Then %><%=(rsContent.Fields.Item("Title").Value)%><% End IF %>">
          <% Else %>
          <input type="text" name="Title" size="30" class="webManagerBox" value="">
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Headline</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
          <input type="text" name="Headline" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Headline").Value)%>">
          <% Else %>
          <input type="text" name="Headline" size="30" class="webManagerBox" value="">
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Story</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
          <input type="hidden" name="Summary" rows="4" class="webManagerBox" style="height:75px" value="<%=rsContent.Fields.Item("Summary_or_Body").Value%>">
          <input type="button" name="AddAddress" value="Edit Story" class="iagButton" onClick="MM_openBrWindow('Standard_News_Body.asp?ContentID=<%= rsContent("ContentID")%>','News','width=300,height=400,top=200,left=200')">
          <% Else %>
          <input type="hidden" name="Summary" rows="4" class="webManagerBox" style="height:75px">
          <input type="button" name="AddAddress" value="Edit Story" class="iagButton" onClick="MM_openBrWindow('Standard_News_Body.asp?ContentID=0','News','width=300,height=400,top=200,left=200')">
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Hyperlink</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
          <input type="text" name="Links" size="30" class="webManagerBox" value="<% if rsContent("Links") <> "" then %><%=(rsContent.Fields.Item("Links").Value)%><% End IF %>">
          <% Else %>
          <input type="text" name="Links" size="30" class="webManagerBox">
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Image <br>
          <font size="1">(size: 109pxl x 109pxl)</font></td>
        <td width="72%" valign="top">
          <input type="file" name="upload1" size="30" class="webManagerBox" value="Browse">
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Published by</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <%
	  ' Bug 
	  ' If not logged on an insert is carried out
	  ' Character of unknown type is inserted into DB 
	  ' Update completes and exception error message is given
	  
	  ' Fix, but does it??
	  If Session("svUserID") = "" Then
	  	Session("svUserID") = "0"
	  End If
	  %>
          <% If frmState = "1" Then %>
          <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
          <% If rsContent("UserID") <> "" then 
		         If Not rsUser("WUserID") = Session("svUserID") then %>
          Previously :: <%= rsUser("UserFName")%>&nbsp;<%= rsUser("UserSName")%><br>
          Update to :: 
          <% else %>
          <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%>
          <% End If %>
          <% Else %>
          <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
          <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
          <% End If %>
          <% Else %>
          <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
          <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
          <% End If ' end Not rsContent.EOF Or NOT rsContent.BOF %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Headline News</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% 
		If frmState = "1" Then 
	  		If rsContent.Fields.Item("Importance").Value = "1" Then 
				checkYes = "checked" 
	  		Else If rsContent.Fields.Item("Importance").Value = "0" Then
	  			checkNo = "checked" 
	  		End If 
	  		End If 
		%>
          <input type="radio" name="Importance" value="1" <%= checkYes %>>
          Yes 
          <input type="radio" name="Importance" value="0" <%= checkNo %>>
          No 
          <% 
	  	End If
		%>
          <%
	  	'If frmState = "2" Then 
		If frmState = "2" Then 
	  	%>
          <input type="radio" name="Importance" value="1">
          Yes 
          <input type="radio" name="Importance" value="0" checked>
          No 
          <% 
		End If 
		%>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="72%" class="iagManagerSmallBlk">
          <input type="button" name="Preview" value="Preview" onClick="displayHTML(this.form)" class="iagButton">
          <%If IntTaskID = "1" Then %>
          <input type="submit" name="Submit" value="Amend News" class="iagButton">
          <% Else If IntTaskID = "Add" OR IntTaskID = "0" Then %>
          <input type="submit" 	name="Submit" 		value="Add News" class="iagButton">
          <input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
          <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
          <input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
          <input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
          <input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
          <input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
          <input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">
          <% 
		End If
		End If   
		%>
          <input type="hidden" name="WhatToDo" value="<% If IntTaskID= "1" Then Response.Write("Amend") Else Response.Write("Add") End If %>">
          <% If frmState = "1" Then %>
          <input type="hidden"	name="ContentID" 	value="<%= rsContent("ContentID")%>">
          <input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
          <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
          <input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
          <input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
          <input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
          <input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
          <input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">
          <% 
		End If 		 
		End If 
		End If 
	  	%>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
    </form>
  </table>
</div>
<% End If %>
</body>
</html>