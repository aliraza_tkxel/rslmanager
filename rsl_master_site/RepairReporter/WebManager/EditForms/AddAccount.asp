<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../Connections/WhichWebManager.asp" -->
<!--#include file="../LibFunctions.asp" -->
<%

 Session.LCID = 2057
 Dim StartDate, EndDate, Summary, Upload1, Title, ContentID , User
 Dim objDB, objDB2
 Dim LastID
 Dim theSecID, theTaskIDNum, theTitleID, doneTask

IntTaskID = Request("ActionType")

if (Request("rsID") <> "") then theTitleID = Request("rsID")

if IntTaskID = "Amend" then
	formState = "1"
	else if IntTaskID = "add" then
		formState = "2"
	End if
End If

if formState = "1" then
	set rsUser = Server.CreateObject("ADODB.Recordset")
	rsUser.ActiveConnection = RSL_CONNECTION_STRING 'MM_WebManager_STRING
	rsUser.Source = "SELECT * FROM UserData WHERE WUserID = " + Replace(theTitleID, "'", "''") + ""
	rsUser.CursorType = 0
	rsUser.CursorLocation = 2
	rsUser.LockType = 3
	rsUser.Open()
	rsUser_numRows = 0
	'response.write rsUser.Source & "<BR><BR>"
	
	set rsUser_WHICH = Server.CreateObject("ADODB.Recordset")
	rsUser_WHICH.ActiveConnection = MM_WhichWebManager_STRING 'MM_WebManager_STRING
	rsUser_WHICH.Source = "SELECT * FROM UserData WHERE UserEmail ='" + rsUser("UserEmail") + "'"
	rsUser_WHICH.CursorType = 0
	rsUser_WHICH.CursorLocation = 2
	rsUser_WHICH.LockType = 3
	rsUser_WHICH.Open()
	'response.write rsUser_WHICH.Source & "<BR><BR>"
    	
End If

DNS_STRING = RSL_CONNECTION_STRING

SQL = "SELECT * FROM tbl_Top_Structures where ToAdmin = '1' ORDER By TopStructure ASC" 
OpenRecordSet rsAreaSelection

%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="/calander/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function ValidateForm(){
 thisForm.action = "serverSide/serv_Account_Admin.asp";
 thisForm.submit();
}

function AmendForm(){

 thisForm.action = "serverSide/serv_Account_Admin.asp";
 thisForm.submit();
}

function GetName(){
var fname = thisForm.UserFName.value;
var sname = thisForm.UserSName.value;
var fullname = fname + " " + sname;
thisForm.Fullname.value = fullname;
}

function genLogin(){
var email = thisForm.email.value;
thisForm.Username.value = email;
}
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="480" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
    <td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> 
      <p> 
        <% if doneTask = "1" then %>
        Amend Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif"> 
        <% ELSE %>
        <% if doneTask = "2" then %>
        Addition Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif">&nbsp;&nbsp;You 
        can now add another... 
        <% Else %>
        Complete the form below to 
        <% if IntTaskID = "Amend" then %>
        amend 
        <% else if IntTaskID = "add" then %>
        add a 
        <%End If 
	  End If %>
        User. </p>
    </td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
</table>
<div id="theNews" name="theNews" style="position:absolute; left:9px; top:30px; width:480px; height:250px; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;"> 
  <table width=400>
    <form name="thisForm" method ="POST" target ="_self">
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">First Name</td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle" height="22"> 
          <% if formState = "1" then %>
          <input type="text" tabIndex="1" name="UserFName" size="30" class="webManagerBox" value="<% if rsUser("UserFName") <> "" then%><%=(rsUser.Fields.Item("UserFName").Value)%><% End If %>">
          <% else %>
          <input type="Text" tabIndex="1" name="UserFName" size="30" class="webManagerBox" value="">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle" height="22"><img src="../images/help_1.gif" width="22" height="22" onClick="javascript:parent.loadHelp('EditForms/HelpPages/Help_User_Account.asp','Firstname');"></td>
      </tr>
      <tr> 
        <td colspan="4" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">Surname</td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle"> 
          <% if formState = "1" then %>
          <input type="text" tabIndex="2" name="UserSName" size="30" class="webManagerBox" value="<% if rsUser("UserSName") <> "" then%><%=(rsUser.Fields.Item("UserSName").Value)%><% End If %>">
          <% else %>
          <input type="Text" tabIndex="2" name="UserSName" size="30" class="webManagerBox" value="" onBlur="GetName()">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle"><img src="../images/help_1.gif" width="22" height="22" onClick="javascript:parent.loadHelp('EditForms/HelpPages/Help_User_Account.asp','Lastname');"></td>
      </tr>
      <tr> 
        <td colspan="4" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">Email</td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle"> 
          <% if formState = "1" then %>
          <input type="text" tabIndex="3" name="email" size="30" class="webManagerBox" value="<% if rsUser("UserEmail") <> "" then%><%=(rsUser.Fields.Item("UserEmail").Value)%><% End If %>">
          <% else %>
          <input type="text" tabIndex="3" name="email" size="30" class="webManagerBox" value="" onBlur="genLogin()">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
        <td width="38%" class="iagManagerSmallBlk" valign="middle"><img src="../images/help_1.gif" width="22" height="22" onClick="javascript:parent.loadHelp('EditForms/HelpPages/Help_User_Account.asp','Email');"></td>
      </tr>
      <tr> 
        <td colspan="4" height="4"></td>
      </tr>
      <tr> 
        <td height="2" width="2%"></td>
        <td height="2"  width="22%"></td>
        <td height="2"  width="76%" colspan="2"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="3" class="iagManagerSmallBlk" valign="top" bgcolor="#F2F2EE"> 
          <table width="427" border="0" cellspacing="0" cellpadding="0" height="17">
            <tr> 
              <td colspan="7" class="iagManagerSmallBlk" bgcolor="#637670" height="2"></td>
            </tr>
            <tr> 
              <td width="2" class="iagManagerSmallBlk" bgcolor="#637670"></td>
              <td width="3" class="iagManagerSmallBlk"></td>
              <td width="100" class="iagManagerSmallBlk" valign="middle">Username</td>
              <td width="125" valign="middle"> 
                <% if formState = "1" then %>
                <input type="text" tabIndex="4" name="Username" size="15" class="webManagerBox" style="width:100" value="<% if rsUser("UserLogin") <> "" then%><%=(rsUser.Fields.Item("UserLogin").Value)%><% End If %>">
                <% else %>
                <input type="text" tabIndex="4" name="Username" size="15" class="webManagerBox" style="width:100" value="">
                <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
              </td>
              <td width="12">&nbsp;</td>
              <td valign="middle"><span class="iagManagerSmallBlk">tick to freeze 
                access</span> 
                <input type="checkbox" tabIndex="5" name="Active" value="0">
              </td>
              <td width="4" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td bgcolor="#637670" height="5" width="2"></td>
              <td height="5" width="3"></td>
              <td height="5" width="100" valign="middle"></td>
              <td height="5" valign="middle"></td>
              <td height="5" width="12"></td>
              <td height="5"></td>
              <td height="5" width="4" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td width="2" class="iagManagerSmallBlk" bgcolor="#637670"></td>
              <td width="3" class="iagManagerSmallBlk">&nbsp;</td>
              <td width="100" class="iagManagerSmallBlk" valign="middle">Password</td>
              <td valign="middle"> 
                <% if formState = "1" then %>
                <input type="password" tabIndex="5" name="Password" size="20" class="webManagerBox" style="width:100" value="<% if rsUser("UserPwd") <> "" then%><%=(rsUser.Fields.Item("UserPwd").Value)%><% End If %>">
                <% else %>
                <input type="password" tabIndex="5" name="Password" size="20" class="webManagerBox" style="width:100" value="">
                <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
              </td>
              <td width="12">&nbsp;</td>
              <td valign="bottom" align="right"><img src="../images/help_1.gif" width="22" height="22" onClick="javascript:parent.loadHelp('EditForms/HelpPages/Help_User_Account.asp','LogInfo');">&nbsp;&nbsp;<BR>
              </td>
              <td width="4" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td colspan="7" class="iagManagerSmallBlk" bgcolor="#637670" height="2"></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr> 
        <td height="2" width="2%"></td>
        <td height="2" width="22%"></td>
        <td height="2" width="76%" colspan="2"></td>
      </tr>
      <tr> 
        <td colspan="4" height="4"></td>
      </tr>
      <tr> 
        <td colspan="4" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="3" class="iagManagerSmallBlk"> 
          <p> 
            <input type="text" class="iagManagerSmallBlk" name="Fullname" style="width: 75px; border: 0; background-color: #ffffff;padding: 0;font-family: verdana; font-size: 8pt" value="the new user" wrap readonly>
            will be capable of administering the following sections when ticked.<br>
            <img src="../images/help_1.gif" width="22" height="22"></p>
        </td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk" colspan="2">&nbsp;</td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="3" class="iagManagerSmallBlk"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td width="50"> 
                <input type="checkbox"  name="Area1" value="1" >
                <input type="hidden" name="HiddenArea1" value="1">
              </td>
              <td class="iagManagerSmallBlk">Reid Housing Admin</td>
            </tr>
			 <tr> 
              <td width="50"> 
                <input type="checkbox"  name="Area2" value="2" >
                <input type="hidden" name="HiddenArea2" value="2">
              </td>
              <td class="iagManagerSmallBlk">Web Manager Admin</td>
            </tr>
		</table>
        </td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk" colspan="2">&nbsp;</td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="3" class="iagManagerSmallBlk"> 
          <% 
		response.write theTaskIDNum
		if formState = "1" then %>
          <input type="button" name="go" value="Amend User Details" class="iagButton" onClick="AmendForm()">
          <input type="hidden" 	name="WhichID" 	value="<% If rsUser_WHICH("UserID") <> "" Then Response.Write rsUser_WHICH("UserID") Else Response.Write "0" End If %>">

		  <% else if IntTaskID = "add" then %>
          <input type="button" name="go" value="Add a User" class="iagButton" onClick="ValidateForm()">
          <% End If
		   End If   %>
          <input type="hidden" name="WhatToDo" value="<% if IntTaskID= "Amend" then %>AmendtheRecord<% else if IntTaskID= "add" then %>AddNew<% End If End If%>">
          <input type="hidden" name="TitletoUpdate" value="<%=theTitleID%>">
          <% if formState = "1" then %>
          <input type="hidden" name="UniqueContentID" value="<%= rsUser("WUserID")%>">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF
		 %>
          <input type="hidden" name="SectionCount" value="<%=SectionCount%>">
          <% End If %>
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk" colspan="2">&nbsp;</td>
      </tr>
    </form>
  </table>
</div>
</body>
</html>
<%
CloseRecordSet rsAreaSelection
if IntTaskID = "1" then

rsUser.Close()
end if
%>
