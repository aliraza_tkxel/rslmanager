<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../../css/WebManager.css" type="text/css">
</head>

<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin=0>
<table width="140" border="0" cellspacing="0" cellpadding="0" height="21">
  <tr> 
    <td height="23" valign="middle" align="center" colspan="2"><b>HELP</b></td>
  </tr>
  <tr> 
    <td height="8" bgcolor="#F2F2EE" colspan="2"></td>
  </tr>
  <tr> 
    <td height="8" colspan="2"></td>
  </tr>
  <%
  Dim TheHelpID 
  TheHelpID = Request("rsID") 
  if TheHelpID = "Firstname" then %>
  <tr> 
    <td width="4">&nbsp;</td>
    <td> 
      <p>Enter you Firstname(s) in the box provided. This will be used to personlise 
        the site for the user and also to record the users activity around the 
        site.<br>
        i.e. <i>John<br>
        <br>
        </i></p>
    </td>
  </tr>
  <% Else IF TheHelpID = "Lastname" then %>
  <tr> 
    <td width="4">&nbsp;</td>
    <td> 
      <p>Enter you Surnname(s) in the box provided. This will be used to personlise 
        the site for the user and also to record the users activity around the 
        site.<br>
        i.e. <i>Smith<br>
        <br>
        </i></p>
    </td>
  </tr>
  <% Else IF TheHelpID = "Email" then %>
  <tr> 
    <td width="4">&nbsp;</td>
    <td> 
      <p>Enter you Email Address in the box provided. </p>
      <p>This will be used send the users any notification and is mandatory.<br>
        <br>
        i.e. <br>
        <i><font size="1">jSmith@webname.com</font><br>
        <br>
        </i></p>
    </td>
  </tr>
  <tr>
    <td width="4">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <% Else IF TheHelpID = "LogInfo" then %>
  <tr> 
    <td width="4">&nbsp;</td>
    <td> 
      <p>These details will be used to log the user into the Webmanager<i><font size="1"></font>.<br>
        <br>
        <b>Username</b><br>
        </i><i> If you click on the username, webmanager generates a username 
        for you. Change it if you wish.</i></p>
      <p><i><b>Password</b><br>
        Enter a password that has letters and numbers in, which is 6 or more characters 
        long.<br>
        <br>
        <b>Account Freezing</b><br>
        If you wish to stop a user logging in for any reason, check this option 
        and the user will be unable to login in. If the user is already logged 
        in it will automatically log them out.<br>
        <br>
        </i></p>
      </td>
  </tr>
  <tr>
    <td width="4">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <% End If 
  End If 
  End If 
  
  End If %>
</table>
</body>
</html>
