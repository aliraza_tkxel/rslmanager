<!--#include file="../../Connections/WM_Which.asp" -->
<!--#include file="../../LibFunctions.asp" -->
<!--#include file="upload.asp" -->
<%
'On Error Resume Next

 Session.LCID = 2057 'British English
 
 Dim Uploader, File
 Set Uploader = New FileUploader
	 Uploader.Upload()
	 	
 Dim StartDate 	' Article Start Date (live from Date)
 Dim EndDate   	' Article End Date (live to Date)
 
 	StartDate = FormatDateTime(Uploader.Form("StartDate"),1) ' Insert correct Date Format. Local ID did not appear to work.
	EndDate = FormatDateTime(Uploader.Form("EndDate"),1)	 ' Insert correct Date Format. Local ID did not appear to work.
	
 If StartDate = "" Then 
 	StartDate = FormatDateTime(date(),1)					 ' Insert correct Date Format. Local ID did not appear to work.
 End If 
	
 If EndDate = "" Then 
 	EndDate = FormatDateTime(date(),1) 						 ' Insert correct Date Format. Local ID did not appear to work.
 End If
   
 Dim Summary	' frm TEXTAREA
 Dim Title		' frm INPUT	
 Dim ContentID	' frm Record ID
 Dim User		' frm HIDDEN Session Variable
 Dim FileSent 	' frm JavaScript Value
 Dim SID		' frm HIDDEN Variable : Section
 Dim SSID		' frm HIDDEN Variable : SubSection
 Dim theFrmID   ' frm HIDDEN Variable : Form Type e.g. Publications_2.asp

 	Summary 	= Replace(Uploader.Form("Summary"), "'", "''")	'ALL frm requests must be made through the Uploader Class	
 	Title 		= Replace(Uploader.Form("Title"), "'", "''")
 	ContentID 	= Uploader.Form("ContentID")
 	User 		= Uploader.Form("User")
 	FileSent 	= Uploader.Form("FileSent")
 	SID		 	= Uploader.Form("theSID")
	SSID	 	= Uploader.Form("theSS")
	theFrmID 	= Uploader.Form("theFrmID")
	
	If User = "" Then
	   User = "0"
	End If
  
 Dim Upload(4) 
 Dim j

	If Uploader.Form("WhatToDo") = "Add" then 
 		
  		Response.Write "<b>Thank you for your upload " & Uploader.Form("upload1") & "</b><br>"
	
		If Uploader.Files.Count = 0 Then
  			 Response.Write "File(s) not uploaded."
		Else	   
			j = 0			
			For Each File In Uploader.Files.Items				
				upload(j) = File.FileName				
				File.SaveToDisk "C:\Inetpub\wwwroot\WebManager\Downloads\" & Session("dwnldFolder")	
				j = j + 1
			Next
	
		End If
		
		If NOT Upload(0) <> "" Then
			Upload(0) = NULL
		End If
		If NOT Upload(1) <> "" Then
			Upload(1) = "download_icon.gif"
		End If
    
	DNS_STRING 	= RSL_CONNECTION_STRING
	SQL = "INSERT INTO tbl_News_And_Story_Content(StartDate, EndDate, Title, Upload_1, Upload_2, UserID, SectionHeadingID, SubSectionID, FormType) Values('" & StartDate & "', '" & EndDate & "', '" & Title & "', '" & Upload(0) & "', '" & Upload(1) & "', " & User & ", " & SID & ", " & SSID & ", " & theFrmID & ")"
	OpenRecordSet rsOptionList
	CloseRecordSet rsOptionList
	
	'Response.Write(SQL)
	
 	doneTask = "2"  
%> 
 <script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
//-->
</script>
<%
	End If

if Uploader.Form("WhatToDo") = "Amend" then		'Uploading section for amend

	If Uploader.Files.Count = 0 Then
  			 Response.Write "File(s) not uploaded."
		Else
			j = 0
			For Each File In Uploader.Files.Items				
				upload(j) = File.FileName		
				File.SaveToDisk "C:\Inetpub\wwwroot\WebManager\Downloads\" & Session("dwnldFolder")			
				j = j + 1
			Next	
	End If
	
	'End of Uploading section for amend
	
	If FileSent = "nofile" Then	
			SQL = "UPDATE tbl_News_And_Story_Content " & _
			"SET tbl_News_And_Story_Content.StartDate = '" & StartDate & "', " & _
			"tbl_News_And_Story_Content.EndDate = '" & EndDate & "', " & _
			"tbl_News_And_Story_Content.Title = '" & Title & "' " & _
			"WHERE ((tbl_News_And_Story_Content.ContentID=" & ContentID & "))"	
	Response.write "<BR>" & "1..." & "<BR>"	
	End If
	
	If FileSent = "firstfile"  Then	
			SQL = "UPDATE tbl_News_And_Story_Content " & _
				"SET tbl_News_And_Story_Content.StartDate = '" & StartDate & "', " & _
				"tbl_News_And_Story_Content.EndDate = '" & EndDate & "', " & _
				"tbl_News_And_Story_Content.Upload_1 = '" & Upload(0) & "', " & _
				"tbl_News_And_Story_Content.Title = '" & Title & "' " & _
				"WHERE ((tbl_News_And_Story_Content.ContentID=" & ContentID & "))"				
	Response.write  "<BR>" &"2..." & "<BR>"	
	End If
	
	If FileSent = "secondfile" Then	
	SQL = "UPDATE tbl_News_And_Story_Content " & _
		"SET tbl_News_And_Story_Content.StartDate = '" & StartDate & "', " & _
		"tbl_News_And_Story_Content.EndDate = '" & EndDate & "', " & _
		"tbl_News_And_Story_Content.Upload_2 = '" & Upload(0) & "', " & _
		"tbl_News_And_Story_Content.Title = '" & Title & "' " & _
		"WHERE ((tbl_News_And_Story_Content.ContentID=" & ContentID & "))"
	End If
	
	If NOT FileSent <> "" Then	
		SQL = "UPDATE tbl_News_And_Story_Content " & _
			"SET tbl_News_And_Story_Content.StartDate = '" & StartDate & "', " & _
			"tbl_News_And_Story_Content.EndDate = '" & EndDate & "', " & _
			"tbl_News_And_Story_Content.Upload_1 = '" & Upload(0) & "', " & _
			"tbl_News_And_Story_Content.Title = '" & Title & "' " & _
			"WHERE ((tbl_News_And_Story_Content.ContentID=" & ContentID & "))"			
	End If
	
	Response.Write FileSent
	
	DNS_STRING 	= RSL_CONNECTION_STRING
	OpenRecordSet rsUpdateContent
	CloseRecordSet rsUpdateContent
	
	Response.Write(SQL)
	
	doneTask = "1"
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
//-->
</script>
<%
 End If
%>
<%
	Dim theSID
	Dim thersID
	Dim theGID
	Dim theTask
	
		theSID = Uploader.Form("theSID")
		thersID = Uploader.Form("ContentID")
		theGID = Uploader.Form("theGID")
		'theTask = Uploader.Form("TaskID")

	Response.Redirect "../Standard_Publication_2.asp?GID=" & theGID&"SID=" & theSID & "&rsID=" &thersID& "&DoneTask=" & doneTask '"&TaskID=" &theTask&
%>