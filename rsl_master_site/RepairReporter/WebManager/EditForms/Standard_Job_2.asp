<%@LANGUAGE="VBSCRIPT"%>
<% 'Option Explicit %>
<% 'On Error Resume Next %>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../LibFunctions.asp" -->
<%
 Session.LCID = 2057 ' British English

 Dim DNS_STRING ' Connection to Database
 Dim ContentID	' Record	
 Dim rsContent 	' Declare Recordset
 Dim rsUser		' Declare Recordset
 Dim rsOffice	' Declare Recordset
 Dim rsID 		' Record  Requested from left iframe
 Dim IntGID	    ' Top Structure
 Dim IntSID		' Request frm value  Section
 Dim IntTaskID	' Request frm value  Task
 Dim doneTask 	' What task has been completed?, value comes from processing page to this frm
 Dim frmState  ' Toggle value Between Add (clear) & Amend (rs values)

 	IntGID		= Request("GID")
	IntSID 		= Request("SID")
 	IntTaskID 	= Request("TaskID")
 
 'Dim StartDate
 'Dim EndDate
 'Dim Summary
 'Dim Upload1
 'Dim Title
 'Dim theTitleID	' Request frm value 
 'If (Request("TitleIDNum") <> "") then 
 'theTitleID = Request("TitleIDNum")
 'End If
%>

<%
    doneTask = Request("doneTask")
 If doneTask <> "" Then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
	//parent.TitleFrame.window.location.reload();
    //document.refresh = true;
// End -->
</script>
<%
 End if
%>
<%
 If IntTaskID = "1" then
	frmState = "1"
	
	'Else If IntTaskID = "Add" then
		'frmState = "2"
	'End if
	
 End If
 
  
 Dim DDDefault
 	 DDDefault = "ApplicationRow','CVRow"
	 DDProcess = "CV"
 'Additional Code 'frmState=""' as Add dos not have a value!!??!!
 If frmState = "1" Then  'OR frmState = ""

  	If (Request("rsID") <> "") Then 
		rsID = Request("rsID")
	Else
	 rsID = -1
	End If
	
  		DNS_STRING = RSL_CONNECTION_STRING
		SQL = "SELECT *  FROM tbl_News_And_Story_Content NSC WHERE NSC.ContentID = " + Replace(rsID, "'", "''") + ""
		OpenRecordSet rsContent

  Dim DDOffice
  Dim DDProcess	
  If NOT rsContent.EOF or NOT rsContent.BOF Then 
  	DDDefault = rsContent.Fields.Item("Ref").value 
	DDProcess = rsContent.Fields.Item("Process").value
	DDOffice = rsContent.Fields.Item("Office").value
    If rsContent("UserID") <> "" Then 	 
  		DNS_STRING = RSL_CONNECTION_STRING
		SQL = "SELECT *  FROM UserData where WUserID = " & rsContent("UserID") & ""
		OpenRecordSet rsUser
     End If 	 
   Else
   'Do nothing  
   End If
   
 End If

	DNS_STRING = RSL_CONNECTION_STRING
	SQL = "SELECT DISTINCT Title, Address  FROM tbl_Top_Structure_Adresses"
	OpenRecordSet rsOffice
	
%><html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="calander/calendarFunctions.js"></SCRIPT>
<script>

var globeCV;

function WhatRow(row) {

   var val1;
       val1 = frm.Process.value ;
   var val2;
   
   if (val1 == "CV"){
   		globeCV = "CV";
   		val1 = "CVRow";
   		val2 = "ApplicationRow";
		}
   		else {
		globeCV = "App";
		val1 = "ApplicationRow";
		val2 = "CVRow";
		}			   

    var temp, temp2;
	temp = document.all(val1);
	temp2 = document.getElementsByName(val2);	
	
    for (i = 0; i <temp.length; i ++){		
       temp[i].style.display = "block";
	   }
	for (j = 0; j<temp2.length; j ++){
	   temp2[j].style.display = "none";
	   }   
}

function HideRows(Rows1,Rows2){
    
	
	globeCV = "CV";
   temp=document.all(Rows1);
    for (i = 0; i<temp.length; i ++){
       temp[i].style.display = "none";}
	
	temp2=document.getElementsByName(Rows2);
    for (j = 0; j<temp2.length; j ++){
       temp2[j].style.display = "block";}
   

}

function ValidateForm(){

	 var box  = frm.upload1.value;
	 var box2  = frm.upload2.value;
	 var box3  = frm.upload3.value;
	 
	 if (globeCV == "CV"){
		 if (box == ""){
			 alert("You have not selected a job advert to upload.");
			 return false;
		 }
	 }
	 if (globeCV == "App"){
		 if (box == ""){
			 alert("You have not selected a job advert to upload.");
			 return false;
		 }
		 if (box2 == ""){
			 alert("You have not selected the Application Form to upload.");
			 return false;
		 }
		 if (box3 == ""){
			 alert("You have not selected the Application Notes to upload.");
			 return false;
		 }
	 }
	 //if ((box == "") && (frm.upload1.style.display == "block")){
	 //alert("no1");
	//}
 	frm.action = "serverSide/serv_Standard_Job_1.asp";
 	frm.target = "_self";
	frm.method = "POST";
 	document.frm.encoding = "multipart/form-data";
 	frm.submit();
}

function GetDetails(){
	frm.action = "serverside/servAddress.asp";
	frm.target = "AddressFrame";
	frm.method = "POST";
	frm.submit();
}

function AmendForm(){

	if ((frm.upload1.value != "") && (frm.upload2.value == "")){
		frm.FileSent.value = "AdvertOnly";
		alert("1");
	}
	if ((frm.upload1.value != "") && (frm.upload2.value != "") && (frm.upload3.value == "")){
		frm.FileSent.value = "AdvertAndApp";
		alert("2");
	}
	if ((frm.upload1.value != "") && (frm.upload2.value != "") && (frm.upload3.value != "")){
		frm.FileSent.value = "All";
		alert("3");
	}	
	if ((frm.upload1.value == "") && (frm.upload2.value != "") && (frm.upload3.value == "")){
		frm.FileSent.value = "AppOnly";
		alert("4");
	}
	if ((frm.upload1.value == "") && (frm.upload2.value != "") && (frm.upload3.value != "")){
		frm.FileSent.value = "AppAndNotes";
		alert("5");
	}
	if ((frm.upload1.value == "") && (frm.upload2.value == "") && (frm.upload3.value != "")){
		alert("In order to upload Application Notes, you have to upload an Application form.");
		return false 
	}
	frm.action = "serverSide/serv_Standard_Job_1.asp";
	frm.target= "_self";
	frm.method="POST";
	document.frm.encoding = "multipart/form-data";
	frm.submit();
}
</script>
  
</head>
<body bgcolor="#F2F2EE" text="#000000" topmargin="0" leftmargin="0" onLoad="
<% If IntTaskID = "" Then 
 Response.Write "" 
 	Else 
		If frmState = "1" Then 
			Response.Write "WhatRow('ApplicationRow','CVRow')" 
		Else Response.Write "HideRows('" & DDDefault & "')" 
		End If  
	End If 
%>">
<% If IntTaskID = "" Then %>
<% Else %>
<%= IntTaskID %>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F2F2EE">
  <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
    <td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> 
      <% 'if doneTask = "1" then %>
      Amend Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif"> 
      <% 'ELSE %>
      <% 'if doneTask = "2" then %>
      Addition Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif">&nbsp;&nbsp;You 
      can now add another... 
      <% 'Else %>
      Complete the form below to 
      <% 'if IntTaskID = "1" then %>
      amend a 
      <% 'else if IntTaskID = "Add" then %>
      add a 
      <%'End If 
	  'End If %>
      job vacancy. </td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
  <form name="frm">    
    <tr> 
      <td width="2%"></td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Start Date </td>
      <td width="72%"> 
        <% If frmState = "1" Then %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<%=(rsContent.Fields.Item("StartDate").Value)%>">
        <% Else %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<%= Date()%>">
        <% End If %>
<a href="javascript:;" onClick="YY_Calendar('StartDate',113,50,'de','#FFFFFF','#637670','YY_calendar1')"><FONT color="#9287AE"> 
<font color="#003333"><b> <span class="calText">[Choose Date]</span></b></font></FONT></a></td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Closing Date</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<%=(rsContent.Fields.Item("EndDate").Value)%>">
        <% Else %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<%= Date()+ 30%>">
        <% End If %>
<a href="javascript:;" onClick="YY_Calendar('EndDate',113,80,'de','#FFFFFF','#637670','YY_calendar1')"><FONT class="calText" color="#9287AE"> 
<font color="#003333"><b>[Choose Date]</b></font></FONT></a> </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Ref</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Ref" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Ref").Value)%>">
        <% Else %>
        <input type="text" name="Ref" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Title</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Title").Value)%>">
        <% Else %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Salary</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Salary" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Salary").Value)%>">
        <% Else %>
        <input type="text" name="Salary" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Summary</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"><%=(rsContent.Fields.Item("Summary_or_Body").Value)%></textarea>
        <% Else %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"></textarea>
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Application Process</td>
      <td width="72%"> 
        <% 	
		Dim a
		Dim b
		
	   If DDProcess = "CV" Then    
	   a = "selected" 	
	   Else 
	   b = "selected" 
	   End If 
	   %>
        <select name="Process" onChange="WhatRow('ApplicationRow')">
          <option value="CV" <%= a %>>CV</option>
          <option value="Application Form" <%= b %>>Application Form</option>
        </select>
        <% 'End If ' end Not rsContent.EOF Or NOT rsContent.BOF %>
      </td>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Advert (.pdf)</td>
      <td width="72%"> 
        <input type="file" id="uploadID1" name="upload1" size="30" class="webManagerBox" value="Browse">
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr id="CVRow"> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Office</td>
      <td width="72%" class="iagManagerSmallBlk"> 	 
	 <%
	  function GetSelect(sSQL, selectedItem)
    	Dim sTemp
    	Dim sSelected
    	Dim rsDD
		Set rsDD = Server.CreateObject("ADODB.Recordset")
    		rsDD.Open sSQL, "dsn=WebManager;uid=WebManagerUser;pwd=WebUSer;"
    	While Not rsDD.EOF
		
		If selectedItem <> 0 Then
    		If Trim(rsDD(0)) = Trim(selectedItem) Then 
				sSelected = " SELECTED "
			End If
		End If
    		sTemp = sTemp & "<OPTION " & _
    			 sSelected & " value=""" & _
    			 rsDD(0) & """>" & _
    			 rsDD(1) & "</OPTION>" & vbCrLf
    		sSelected = ""
    	rsDD.MoveNext
    	Wend
    
    	GetSelect = sTemp
    	Set rsDD = Nothing
    End function
	%>	 
	<Select Name="Office" onChange='GetDetails()' style="width:180px; font-family:arial; font-size:8pt; background-color:#F2F2EE; color:#637670">
	<option style="background-color:#637670; color:#ffffff"" value='0'>Please select an office</option>
    <%=GetSelect("SELECT ID, Title FROM tbl_Top_Structure_Adresses", DDOffice)%>
    </Select>
      </td>
    </tr>
    <tr id="CVRow"> 
      <td height="4" class="iagManagerSmallBlk">&nbsp;</td>
      <td height="4" class="iagManagerSmallBlk">&nbsp;</td>
      <td height="4" class="iagManagerSmallBlk"><div id="Address">&nbsp;</div>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr id="ApplicationRow"> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Application Form (.doc)</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <input type="file" name="upload2" size="30" class="webManagerBox" value="Browse">
      </td>
    </tr>
    <tr id="ApplicationRow"> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr id="ApplicationRow"> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Application Notes (.doc)</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <input type="file" name="upload3" size="30" class="webManagerBox" value="Browse">
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Published by</td>
      <td width="72%" class="iagManagerSmallBlk"> 
       <% 
	   If frmState = "1" Then 
	   %>
        <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        <% 
		' Problem with ASP Code id no Session variable exists, excemption Error occurs!
		 If rsContent("UserID") <> "" then 
		    If Not rsUser("WUserID") = Session("svUserID") Then 
		%>
        Previously :: <%= rsUser("UserFName")%>&nbsp;<%= rsUser("UserSName")%><br>
        Update to :: 
        <% 
			Else 
		%>
        <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        <% 
			End If 
		%>
        <% 
		 Else 
		%>
        <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        <% End If %>
        <% Else %>
        <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="72%" class="iagManagerSmallBlk"> 
	    <%
		If IntTaskID = "1" Then 
		%>        
		<input type="button" name="Button" value="Amend a Job" class="iagButton" onClick="AmendForm()">
        <% 
		Else If IntTaskID = "Add" OR IntTaskID = "0" Then 
		%>        
		<input type="button" name="Button" value="Add a Job" class="iagButton" onClick="ValidateForm()">
		<input type="hidden" name="theGID" value="<%= IntGID %>">
        <input type="hidden" name="theSID" value="<%= IntSID %>">
		<input type="hidden" name="theTaskID" value="<%= IntTaskID %>">
        <% 
		End If
		End If   
		%>
        <input type="hidden" name="WhatToDo" value="<% If IntTaskID= "1" Then Response.Write("Amend") Else Response.Write("Add") End If %>">
        <% 
		If frmState = "1" Then 
		%>
		<input type="hidden" name="theGID" value="<%= IntGID %>">
        <input type="hidden" name="theSID" value="<%= IntSID %>">
        <input type="hidden" name="ContentID" value="<%= rsContent("ContentID")%>">
		<input type="hidden" name="theTaskID" value="<%= IntTaskID %>">
        <% 
		End If 		 
		'End If 
		'End If 
	  	%> 
      </td>
    </tr>
  </form>
</table>
<% End If %>
<iframe name="AddressFrame" id="AddressFrame" frameborder="0" width="1" height="1" style='display:none'>&nbsp;</iframe>
</body>
</html>