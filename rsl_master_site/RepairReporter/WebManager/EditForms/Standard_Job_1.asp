<%@LANGUAGE="VBSCRIPT"%>
<% 'Option Explicit %>
<% 'On Error Resume Next %>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../LibFunctions.asp" -->
<%
 Session.LCID = 2057 ' British English

 Dim DNS_STRING ' Connection to Database
 Dim ContentID	' Record	
 Dim rsContent 	' Declare Recordset
 Dim rsUser		' Declare Recordset
 Dim rsOffice	' Declare Recordset
 Dim rsID 		' Record  Requested from left iframe
 Dim doneTask 	' What task has been completed?, value comes from processing page to this frm
 Dim frmState  ' Toggle value Between Add (clear) & Amend (rs values)
 
 Dim IntTaskID  ' Action to be carried out
 Dim IntGID	    ' Top Structure
 Dim IntSID	    ' Section
 Dim IntSSID    ' Sub Section
 Dim IntPID		' Page
 Dim IntFrmID	' Form Type
 
	' Dimmed in LibFunctions
 	' Declared Globally
	
 	DNS_STRING = RSL_CONNECTION_STRING
 	IntGID 		= Request("theG")
	IntSID 		= Request("theS")	
	IntSSID		= Request("theSS")
	IntPID 		= Request("theP")
	IntTaskID 	= Request("theFrm_Task")
	IntFrmID 	= Request("theFrm_Type")
	strtbl_Name = Request("thetbl_Name")
	
	'Response.Write "G" 		& IntGID 		& "<BR><BR>"
	'Response.Write "S" 		& IntSID  		& "<BR><BR>"
	'Response.Write "SS" 	& IntSSID  		& "<BR><BR>"
	'Response.Write "P" 		& IntPID  		& "<BR><BR>"
	'Response.Write "Task" 	& IntTaskID  	& "<BR><BR>"
	'Response.Write "Form" 	& IntFrmID 		& "<BR><BR>"
	'Response.Write "Table" 	& strtbl_Name 	& "<BR><BR>"
%>
<%
    doneTask = Request("doneTask")
 If doneTask <> "" Then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
	//parent.TitleFrame.window.location.reload();
    //document.refresh = true;
// End -->
</script>
<%
 End if
%>
<%
 If IntTaskID = "1" then
	frmState = "1"
	
	'Else If IntTaskID = "Add" then
		'frmState = "2"
	'End if
	
 End If
 
  
 Dim DDDefault
 	 DDDefault = "ApplicationRow','CVRow"
	 DDProcess = "CV"
 'Additional Code 'frmState=""' as Add dos not have a value!!??!!
 If frmState = "1" Then  'OR frmState = ""

  	If (Request("rsID") <> "") Then 
		rsID = Request("rsID")
	Else
	 rsID = -1
	End If
	
		SQL = "SELECT *  FROM tbl_News_And_Story_Content NSC WHERE NSC.ContentID = " + Replace(rsID, "'", "''") + ""
		OpenRecordSet rsContent

  Dim DDOffice
  Dim DDProcess	
  If NOT rsContent.EOF or NOT rsContent.BOF Then 
  
  	DDDefault 	= rsContent.Fields.Item("Ref").value 
	DDProcess 	= rsContent.Fields.Item("Process").value
	DDOffice 	= rsContent.Fields.Item("Office").value
	
    If rsContent("UserID") <> "" Then  	
		SQL = "SELECT *  FROM UserData where WUserID = " & rsContent("UserID") & ""
		OpenRecordSet rsUser
     End If
	  	 
   Else
   'Do nothing  
   End If
   
 End If

	DNS_STRING = RSL_CONNECTION_STRING
	SQL = "SELECT DISTINCT Title, Address  FROM tbl_Top_Structure_Adresses"
	OpenRecordSet rsOffice
	
%><html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="calander/calendarFunctions.js"></SCRIPT>
<script>
function ValidateForm(){
 	frm.action = "serverSide/serv_Standard_Job_1.asp";
 	frm.target = "_self";
	frm.method = "POST";
 	document.frm.encoding = "multipart/form-data";
 	frm.submit();	
}

//function AmendForm(){
	//frm.action = "serverSide/serv_Standard_Job_1.asp";
	//frm.target = "_self";
	//frm.method = "POST";
	//document.frm.encoding = "multipart/form-data";
	//frm.submit();
//}
</script>  
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<% If IntTaskID = "" Then %>
<% Else %>
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <table width="405" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
    <td width="10" height="15" <% 
	If IntTaskID <> "" Then 
		Response.Write "bgcolor='#FFFFFF' style='border-bottom: 1px solid #000000'" 
	Else 
		Response.Write "bgcolor=""F2F2EE"""
	End IF
	%>>&nbsp;</td>
          <td height="15" class="iagManagerSmallBlk" 
	<% 
	If IntTaskID <> "" Then 
		Response.Write "bgcolor=""#FFFFFF"" style=""border-bottom: 1px solid #000000""" 
	Else 
		Response.Write "bgcolor=""F2F2EE"""
	End IF %> width="414"
	> 
            <% If doneTask = "1" Then %>
            Amend Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif"> 
            <a href="<%= Session("svDomain")%>/<%=rsSH("SectionHeading")%>/<%=rsSH("SectionHeading")%>.asp" target="_blank">Click 
            here</a> to view web page 
            <% Else %>   
	  		<% If doneTask = "2" Then %>
	  			Addition Completed Successfully&nbsp;&nbsp;<img src="../images/tick.gif">&nbsp;&nbsp;You can now add another...
	  		<% Else %> 	  			
      			<% If IntTaskID = "1" Then %>
      				Complete the form below to amend a job vacancy.
      			<% Else If IntTaskID = "Add" OR IntTaskID = "0" Then %>
      				Complete the form below to add a job vacancy.
				<% ElseIf IntTaskID = "" Then %>
      				<!--Please make a selection from the news stories to the left.//-->
      		<%
	  		  End If 
	  End If 
	  %> 
	</td>
  </tr>
</table>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<% 
'If doneTask = "" OR IsNull(doneTask) Then  
If IntTaskID = "" OR IsNull(IntTaskID) Then
Response.Write "empty"  
%>
<% Else %>
<div id="theNews" name="theNews" style="position:absolute; left:9px; top:30px; width:390px; height:190px; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;"> 
  <form name="frm">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
      <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
    <tr> 
      <td width="2%"></td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Start Date </td>
      <td width="72%"> 
        <% If frmState = "1" Then %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<%=(rsContent.Fields.Item("StartDate").Value)%>">
        <% Else %>
        <input type="text" name="StartDate" size="30" class="webManagerBox" style="Width:75px" value="<%= Date()%>">
        <% End If %>
        <a href="javascript:;" onClick="YY_Calendar('StartDate',113,50,'de','#FFFFFF','#637670','YY_calendar1')"><FONT color="#9287AE"> 
        <font color="#003333"><b> <span class="calText">[Choose Date]</span></b></font></FONT></a></td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Closing Date</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<%=(rsContent.Fields.Item("EndDate").Value)%>">
        <% Else %>
        <input type="text" name="EndDate" size="30" class="webManagerBox" style="Width:75px" value="<%= Date()+ 30%>">
        <% End If %>
        <a href="javascript:;" onClick="YY_Calendar('EndDate',113,80,'de','#FFFFFF','#637670','YY_calendar1')"><FONT class="calText" color="#9287AE"> 
        <font color="#003333"><b>[Choose Date]</b></font></FONT></a> </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Ref</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Ref" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Ref").Value)%>">
        <% Else %>
        <input type="text" name="Ref" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Title</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Title").Value)%>">
        <% Else %>
        <input type="text" name="Title" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Salary</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        <input type="text" name="Salary" size="30" class="webManagerBox" value="<%=(rsContent.Fields.Item("Salary").Value)%>">
        <% Else %>
        <input type="text" name="Salary" size="30" class="webManagerBox" value="">
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="26%" class="iagManagerSmallBlk" valign="top">Summary</td>
        <td width="72%" class="iagManagerSmallBlk"> 
          <% If frmState = "1" Then %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"><%=(rsContent.Fields.Item("Summary_or_Body").Value)%></textarea>
        <% Else %>
        <textarea name="Summary" rows="4" class="webManagerBox" style="height:75px"></textarea>
        <% End If %>
      </td>
    </tr>
    <tr> 
      
<td colspan="3" height="3"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Job Advert (.pdf)</td>
      <td width="72%"> 
        <input type="file" id="uploadID1" name="upload1" size="30" class="webManagerBox" value="Browse">
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk" valign="top">Published by</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If frmState = "1" Then %>
        	<input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
				<% 
				 If rsContent("UserID") <> "" then 
					If Not rsUser("WUserID") = Session("svUserID") Then 
				%>
					Previously :: <%= rsUser("UserFName")%>&nbsp;<%= rsUser("UserSName")%><br>
					Update to :: 
					<% Else %>
					<%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
					<% End If %>
       			<% Else %>
				<input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
				<%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
				<% End If %>
        <% Else %>
        <input type="hidden" name="User" size="30" class="webManagerBox" value="<%= Session("svUserID")%>">
        <%= Session("svFirstName")%>&nbsp;<%= Session("svLastName")%> 
        <% End If %>
      </td>
    </tr>
    <tr> 
      <td colspan="3" height="4"></td>
    </tr>
    <tr> 
      <td width="2%">&nbsp;</td>
      <td width="26%" class="iagManagerSmallBlk">&nbsp;</td>
      <td width="72%" class="iagManagerSmallBlk"> 
        <% If IntTaskID = "1" Then %>
        <input type="button" name="Button" value="Amend a Job" class="iagButton" onClick="ValidateForm()">
        <% Else If IntTaskID = "Add" OR IntTaskID = "0" Then %>
        <input type="button" name="Button" value="Add a Job" class="iagButton" onClick="ValidateForm()">	
		<input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
        <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
		<input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
		<input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">		
		<input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
		<input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
		<input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">		
        <% 
		End If
		End If   
		%>
        <input type="hidden" name="WhatToDo" value="<% If IntTaskID= "1" Then Response.Write("Amend") Else Response.Write("Add") End If %>">
        <% If frmState = "1" Then %>		
		<input type="hidden"	name="ContentID" 	value="<%= rsContent("ContentID")%>">
		<input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
        <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
		<input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
		<input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
		<input type="hidden" 	name="theTaskID" 	value="<%= IntTaskID %>">
		<input type="hidden" 	name="theFrm_Type" 	value="<%= IntFrmID %>">
		<input type="hidden" 	name="theTbl_Name" 	value="<%= strTbl_Name %>">		
        <% 
		End If 		 
		End If 
		End If 
	  	%>
      </td>
    </tr>
</table>
</form>
</div>
<% 
End If 
End If
%>
<iframe name="AddressFrame" id="AddressFrame" frameborder="0" width="1" height="1" style='display:none'>&nbsp;</iframe>
</body>
</html>