<%@LANGUAGE="VBSCRIPT"%>
<!--#include file="../Connections/WM_Which.asp" --> 
<!--#include file="../Class/ClassLib.asp" -->
<!--#include file="../../Connections/WhichWebManager.asp" -->
<%

 Session.LCID = 2057
 Dim StartDate, EndDate, Summary, Upload1, Title, ContentID , User
 Dim objDB, objDB2
 Dim LastID
 Dim theSecID, theTaskIDNum, theTitleID, doneTask
 
 IntSID 		= Request("theS")
 IntGID 		= Request("theG")
 IntSSID		= Request("theSS")
 IntPID 		= Request("theP")

doneTask = Request("doneTask")
if doneTask <> "" then
%>
<script language="JavaScript">
<!--
	parent.gotoPage("Amend");	
	//parent.TitleFrame.window.location.reload();
    //document.refresh = true;
// End -->
</script>
<%
End if

theSecID = Request("xNum")
IntTaskID = Request("theFrm_Task")
if (Request("rsID") <> "") then theTitleID = Request("rsID")

if IntTaskID = "1" then
	formState = "1"
	else if IntTaskID = "Add" then
		formState = "2"
	End if
End If

if formState = "1" then
	set rsUser = Server.CreateObject("ADODB.Recordset")
	rsUser.ActiveConnection = RSL_CONNECTION_STRING 'MM_WebManager_STRING
	rsUser.Source = "SELECT * FROM UserData WHERE WUserID = " + Replace(theTitleID, "'", "''") + ""
	rsUser.CursorType = 0
	rsUser.CursorLocation = 2
	rsUser.LockType = 3
	rsUser.Open()
	rsUser_numRows = 0
	
	set rsUser_WHICH = Server.CreateObject("ADODB.Recordset")
	rsUser_WHICH.ActiveConnection = MM_WhichWebManager_STRING 'MM_WebManager_STRING
	rsUser_WHICH.Source = "SELECT * FROM UserData WHERE UserEmail = " + rsUser("UserEmail") + ""
	rsUser_WHICH.CursorType = 0
	rsUser_WHICH.CursorLocation = 2
	rsUser_WHICH.LockType = 3
	rsUser.Open()
    
End If

set objDB   = new rsClass
set rsAreaSelection  = objDB.GetRecordset("SELECT * FROM tbl_Top_Structures where ToAdmin = '1'ORDER By TopStructure ASC" )
%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/WebManager.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="/calander/calendarFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function ValidateForm(){
 thisForm.action = "serverSide/serv_Standard_Users_1.asp";
 thisForm.submit();
}

function AmendForm(){

 thisForm.action = "serverSide/serv_Standard_Users_1.asp";
 thisForm.submit();
}

function GetName(){
var fname = thisForm.UserFName.value;
var sname = thisForm.UserSName.value;
var fullname = fname + " " + sname;
thisForm.Fullname.value = fullname;
}

function genLogin(){
var email = thisForm.email.value;
thisForm.Username.value = email;
}
</SCRIPT>
</head>
<body bgcolor="#FFFFFF" text="#000000" topmargin="0" leftmargin="0">
<div id='Calendar1' style='background-color:white;position:absolute; left:1px; top:1px; width:200px; height:115px; z-index:20; visibility: hidden'></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
  <tr> 
    <td colspan="3" height="5"></td>
  </tr>
  <tr> 
    <td width="2%" bgcolor="#F2F2EE">&nbsp;</td>
    <td colspan="2" class="iagManagerSmallBlk" bgcolor="#F2F2EE"> 
      <p> 
        <% if doneTask = "1" then %>
        Amend Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif"> 
        <% ELSE %>
        <% if doneTask = "2" then %>
        Addition Completed Successfully&nbsp;&nbsp;<img src="../../images/tick.gif">&nbsp;&nbsp;You 
        can now add another... 
        <% Else %>
        Complete the form below to 
        <% if IntTaskID = "1" then %>
        amend 
        <% else if IntTaskID = "Add" then %>
        add a 
        <%End If 
	  End If %>
        User. </p>
    </td>
  </tr>
  <tr> 
    <td width="2%" height="5"></td>
    <td colspan="2" class="iagManagerSmallBlk" height="5"> </td>
  </tr>
</table>
<div id="theNews" name="theNews" style="position:absolute; left:9px; top:30px; width:405; height:190; background-color:white; font-face:Arial; padding:3; border: 1px solid #000000; overflow=auto;">
  <table width=100%>
    <form name="thisForm" method ="POST" target ="_self">
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">First Name</td>
        <td width="76%" class="iagManagerSmallBlk"> 
          <% if formState = "1" then %>
          <input type="text" name="UserFName" size="30" class="webManagerBox" value="<% if rsUser("UserFName") <> "" then%><%=(rsUser.Fields.Item("UserFName").Value)%><% End If %>">
          <% else %>
          <input type="Text" name="UserFName" size="30" class="webManagerBox" value="">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">Surname</td>
        <td width="76%" class="iagManagerSmallBlk"> 
          <% if formState = "1" then %>
          <input type="text" name="UserSName" size="30" class="webManagerBox" value="<% if rsUser("UserSName") <> "" then%><%=(rsUser.Fields.Item("UserSName").Value)%><% End If %>">
          <% else %>
          <input type="Text" name="UserSName" size="30" class="webManagerBox" value="" onBlur="GetName()">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk" valign="top">Email</td>
        <td width="76%" class="iagManagerSmallBlk"> 
          <% if formState = "1" then %>
          <input type="text" name="email" size="30" class="webManagerBox" value="<% if rsUser("UserEmail") <> "" then%><%=(rsUser.Fields.Item("UserEmail").Value)%><% End If %>">
          <% else %>
          <input type="text" name="email" size="30" class="webManagerBox" value="" onBlur="genLogin()">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
        </td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td height="2" width="2%"></td>
        <td height="2"  width="22%"></td>
        <td height="2"  width="76%"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="2" class="iagManagerSmallBlk" valign="top" bgcolor="#F2F2EE"> 
          <table width="365" border="0" cellspacing="0" cellpadding="0" height="17">
            <tr> 
              <td colspan="7" class="iagManagerSmallBlk" bgcolor="#637670" height="2"></td>
            </tr>
            <tr> 
              <td width="2" class="iagManagerSmallBlk" bgcolor="#637670"></td>
              <td width="3" class="iagManagerSmallBlk"></td>
              <td width="88" class="iagManagerSmallBlk" valign="middle">Username</td>
              <td width="110" valign="middle"> 
                <% if formState = "1" then %>
                <input type="text" name="Username" size="15" class="webManagerBox" style="width:100" value="<% if rsUser("UserLogin") <> "" then%><%=(rsUser.Fields.Item("UserLogin").Value)%><% End If %>">
                <% else %>
                <input type="text" name="Username" size="15" class="webManagerBox" style="width:100" value="">
                <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
              </td>
              <td width="11">&nbsp;</td>
              <td width="197"><span class="iagManagerSmallBlk">tick to freeze 
                access</span> 
                <input type="checkbox" name="Active" value="0">
              </td>
              <td width="2" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td bgcolor="#637670" height="5" width="2"></td>
              <td height="5" width="3"></td>
              <td height="5" width="88" valign="middle"></td>
              <td height="5" width="110" valign="middle"></td>
              <td height="5" width="11"></td>
              <td height="5" width="197"></td>
              <td height="5" width="2" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td width="2" class="iagManagerSmallBlk" bgcolor="#637670"></td>
              <td width="3" class="iagManagerSmallBlk">&nbsp;</td>
              <td width="88" class="iagManagerSmallBlk" valign="middle">Password</td>
              <td width="110" valign="middle"> 
                <% if formState = "1" then %>
                <input type="password" name="Password" size="20" class="webManagerBox" style="width:100" value="<% if rsUser("UserPwd") <> "" then%><%=(rsUser.Fields.Item("UserPwd").Value)%><% End If %>">
                <% else %>
                <input type="password" name="Password" size="20" class="webManagerBox" style="width:100" value="">
                <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF %>
              </td>
              <td width="11">&nbsp;</td>
              <td width="197">&nbsp;</td>
              <td width="2" bgcolor="#637670"></td>
            </tr>
            <tr> 
              <td colspan="7" class="iagManagerSmallBlk" bgcolor="#637670" height="2"></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr> 
        <td height="2" width="2%"></td>
        <td height="2" width="22%"></td>
        <td height="2" width="76%"></td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td colspan="3" height="4"></td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="2" class="iagManagerSmallBlk"> 
          <p> 
            <input type="text" class="iagManagerSmallBlk" name="Fullname" style="width: 75px; border: 0; background-color: #ffffff;padding: 0;font-family: verdana; font-size: 8pt" value="the new user" wrap readonly>
            will be capable of administering the following sections when ticked.</p>
        </td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
      </tr>
      <%      
	
		Dim SectionCount
			SectionCount = 0       	
			while ((NOT rsAreaSelection.EOF)) 
				'  Response.Write ":" & objDB2.GetRecordset & ":"
			SectionCount = SectionCount + 1
	'If formstate = "1" then
			'set objDB2   = new rsClass
			'set rsCheckIt  = objDB2.GetRecordset("SELECT * FROM tbl_AreaAccess where UserID =  " & rsUser("WUserID")  & " AND AreaID=" & rsAreaSelection("TopStructureID") & "")
'	End If		
	%>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="2" class="iagManagerSmallBlk"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td width="50"> 
                <input type="checkbox" name="Area<%= SectionCount %>" value="<%= rsAreaSelection("TopStructureID") %>" >
                <input type="hidden" name="HiddenArea<%= SectionCount %>" value="<%= rsAreaSelection("TopStructureID") %>">
              </td>
              <td class="iagManagerSmallBlk"><%=rsAreaSelection("TopStructure")%></td>
            </tr>
          </table>
        </td>
      </tr>
      <% 
	'If formstate = "1" then
        '    rsCheckIt = nothing
		'	rsCheckIt.close()
	'End If
		    rsAreaSelection.MoveNext()
			Wend
							  %>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td colspan="2" class="iagManagerSmallBlk"> 
          <% 
		response.write theTaskIDNum
		if IntTaskID = "1" then %>
          <input type="button" name="go" value="Amend User Details" class="iagButton" onClick="AmendForm()">
          <% else if IntTaskID = "Add" then %>
          <input type="button" name="go" value="Add a User" class="iagButton" onClick="ValidateForm()">
          <input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
          <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
          <input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
          <input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">

          <% End If
		   End If   %>
          <input type="hidden" name="WhatToDo" value="<% if IntTaskID= "1" then %>AmendtheRecord<% else if IntTaskID= "Add" then %>AddNew<% End If End If%>">
          <input type="hidden" name="TitletoUpdate" value="<%=theTitleID%>">
          <% if formState = "1" then %>
          <input type="hidden" 	name="theGID" 		value="<% If IntGID 	= "" OR IsNull(IntGID) 	Then Response.Write "0" Else Response.Write IntGID 	End If %>">
          <input type="hidden" 	name="theSID" 		value="<% If IntSID 	= "" OR IsNull(IntSID) 	Then Response.Write "0" Else Response.Write IntSID 	End If %>">
          <input type="hidden" 	name="theSSID" 		value="<% If IntSSID 	= "" OR IsNull(IntSSID) Then Response.Write "0" Else Response.Write IntSSID End If %>">
          <input type="hidden" 	name="thePID" 		value="<% If IntPID 	= "" OR IsNull(IntPID) 	Then Response.Write "0" Else Response.Write IntPID 	End If %>">
          <input type="hidden" 	name="WhichID" 		value="<% If IntPID 	= "" OR IsNull(rsUser_WHICH("UserID")) 	Then Response.Write "0" Else Response.Write rsUser_WHICH("UserID") 	End If %>">
          <input type="hidden" name="UniqueContentID" value="<%= rsUser("WUserID")%>">
          <% End If ' end Not rsUser.EOF Or NOT rsUser.BOF
		 %>
          <input type="hidden" name="SectionCount" value="<%=SectionCount%>">
          <% End If %>
          <% End If %>
        </td>
      </tr>
      <tr> 
        <td width="2%">&nbsp;</td>
        <td width="22%" class="iagManagerSmallBlk">&nbsp;</td>
        <td width="76%" class="iagManagerSmallBlk">&nbsp;</td>
      </tr>
    </form>
  </table>
</div>
</body>
</html>
<%
if IntTaskID = "1" then
rsUser.Close()
end if
%>
