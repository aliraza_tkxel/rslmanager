<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%> 
<!-- #include virtual="Connections/db_connection.asp" -->
<%

	dim Session
	set Session = server.CreateObject("SessionMgr.Session2")

	Dim Rs, Conn, SQL, strSQL
	
	Function OpenDB()
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.ConnectionTimeout = 90
		Conn.Open RSL_CONNECTION_STRING 
	End Function
	 
	Function CloseDB()
		Conn.Close
		Set Conn = Nothing
	End Function
	 
	Function OpenRs(RecSet, iSQL)
		Set RecSet = Server.CreateObject("ADODB.Recordset")
		RecSet.Open iSQL, Conn, 0, 1
	End Function
	
	Function CloseRs(RecSet)
		RecSet.Close
		Set RecSet = Nothing
	End Function
	
	Dim Username, Password
	PassCode = "REFUSE"
	
	Username = REPLACE(Request.Form("UserID") , "'", "''")
	Password =  REPLACE(Request.Form("Password"), "'", "''")   
	if (Username <> "" AND Password <> "") then

		Call OpenDB()
		Dim rsLogin
		SQL = "SELECT FIRSTNAME, LASTNAME, E.EMPLOYEEID, PASSWORD, L.ACTIVE, UPPER(ISNULL(T.TEAMNAME, 'UNKNOWN')) AS TEAMNAME, ISNULL(TE.TEAMCODE, 'UNK') AS TEAMCODE, E.ORGID FROM AC_LOGINS L " &_
			"LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = L.EMPLOYEEID " &_
				"LEFT JOIN E_JOBDETAILS J ON J.EMPLOYEEID = E.EMPLOYEEID " &_
					"LEFT JOIN E_TEAM T ON J.TEAM = T.TEAMID " &_
						"LEFT JOIN G_TEAMCODES TE ON T.TEAMID = TE.TEAMID " &_
							"WHERE (LOGIN = '" & Username & "' AND J.ACTIVE = 1)"
							'"WHERE (LOGIN = '" & Username & "' AND J.ACTIVE = 1 AND EXPIRES > getdate())"
						
		Call OpenRs(rsLogin, SQL)
		if (NOT rsLogin.EOF) then
			DatabasePassword = rsLogin("PASSWORD")
			Active = rsLogin("ACTIVE")

			strname = rsLogin("FIRSTNAME") & " " & rsLogin("LASTNAME") & "(" & rsLogin("EMPLOYEEID") & ")"
			strlogsql = "INSERT INTO L_ERRORS (ASPDESCRIPTION) VALUES ('USER LOGGED IN " & strname &  " ')"
			conn.execute(strlogsql)
			if (Active = 1) then
				if (isNULL(DatabasePassword)) then
					DatabasePassword = -1
				else
					COMPAREPASS = StrComp(Password, DatabasePassword)
					if (COMPAREPASS = 0) then
						Session("FirstName") = rsLogin("FIRSTNAME")
						Session("LastName") = rsLogin("LASTNAME")		
						Session("USERID") = rsLogin("EMPLOYEEID")
						Session("TeamCode") = rsLogin("TeamCode")						
						Session("TEAMNAME") = rsLogin("TEAMNAME")
						isContractor = rslogin("ORGID")
						if (isContractor <> "" AND NOT isNull(isContractor)) then
							Session("ORGID") = isContractor
							RedirectPage = "MyWeb/MyWhiteboard.asp"
						else
							if rsLogin("TeamCode") = "PMA" then
								RedirectPage = "BoardMembers/BM.asp"
							else
								RedirectPage = "MyWeb/MyWhiteboard.asp"
							end if
						end if		
						PassCode = "AccessAllowed"
						PassCode = "PASS"
					end if
				end if
			else
				PassCode = "DISABLED"
			end if
		end if
		
		Call CloseRs(rsLogin)
		Call CloseDB()
	end if		
%>
<HTML>
<HEAD>
<script language=javascript>
 if (top.location == location) {
    top.location.href = "default.asp";
  }
</script>
<SCRIPT language="javascript">
<% if PassCode = "REFUSE" then %>
	parent.errorDiv.innerHTML = "<b><font color='red'>Your Login and/or Password do not match, please try again.<br>If you have forgot your password you can <a href='misplaced.asp'><font color=blue>request your password</font></a> and then try again.</font></b>"; 
	parent.thisForm.Password.value = "";
	parent.thisForm.Password.focus();
<% Elseif PassCode = "DISABLED" then %>
	parent.errorDiv.innerHTML = "<b><font color='#FF9900'>Your access rights to the RSL Manager system have been disabled.<br>Please contact Reidmark or your administrator for details.</font></b>"; 
	parent.thisForm.Password.value = "";
	parent.thisForm.Password.focus();
<% Elseif PassCode = "PASS" then %>
	parent.errorDiv.innerHTML = "<b>&nbsp;<br><font color='#FF9900'>Please wait you are entering RSL Manager...</font></b>"; 
	top.location = "/<%=RedirectPage%>";
<% End if %>
</SCRIPT>
</HEAD>
<BODY>
</BODY>
</HTML>

