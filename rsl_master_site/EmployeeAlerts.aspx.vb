
Partial Class EmployeeAlerts
    Inherits System.Web.UI.Page

    Private empID As Integer = 0

    Protected Sub btnSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each cbrow As ListItem In DirectCast(fvEmployee.FindControl("cbEAlerts"), CheckBoxList).Items
            cbrow.Selected = True
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("empid") Is Nothing Then
            empID = Integer.Parse(Request.QueryString("empid"))
            If Not IsPostBack Then
                Dim ealertsAdapter As New dalEAlertsTableAdapters.E_EMPLOYEEALERTSTableAdapter
                Dim ealertsTable As dalEAlerts.E_EMPLOYEEALERTSDataTable = ealertsAdapter.GetDataByEID(Request.QueryString("empid"))
                For Each cbrow As ListItem In DirectCast(fvEmployee.FindControl("cbEAlerts"), CheckBoxList).Items
                    For Each row As dalEAlerts.E_EMPLOYEEALERTSRow In ealertsTable
                        If cbrow.Value = row.ALERTID.ToString Then
                            cbrow.Selected = True
                            Exit for
                        Else
                            cbrow.Selected = False
                        End If
                    Next
                Next
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ealertsAdapter As New dalEAlertsTableAdapters.E_EMPLOYEEALERTSTableAdapter
        'Dim ealertsTable As dalEAlerts.E_EMPLOYEEALERTSDataTable = ealertsAdapter.GetDataByEID(empID)
        Try
            ealertsAdapter.DeleteEmpAlerts(empID)
            For Each cbrow As ListItem In DirectCast(fvEmployee.FindControl("cbEAlerts"), CheckBoxList).Items
                If cbrow.Selected Then
                    ealertsAdapter.Insert(empID, Integer.Parse(cbrow.Value))
                End If
            Next
            Dim tealertsTable As dalEAlerts.E_EMPLOYEEALERTSDataTable = ealertsAdapter.GetDataByEID(Request.QueryString("empid"))
            For Each cbrow As ListItem In DirectCast(fvEmployee.FindControl("cbEAlerts"), CheckBoxList).Items
                For Each row As dalEAlerts.E_EMPLOYEEALERTSRow In tealertsTable
                    If cbrow.Value = row.ALERTID.ToString Then
                        cbrow.Selected = True
                        Exit For
                    Else
                        cbrow.Selected = False
                    End If
                Next
            Next
        Catch ex As Exception
            'ealertsAdapter.Update(ealertsTable)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Response.Redirect("/BusinessManager/employees.asp?alert=true")
    End Sub
End Class
