<% Response.CacheControl = "no-cache" %>
<% Response.Buffer = true %>
<% 
dim Session
set Session = server.CreateObject("SessionMgr.Session2")
%>
<!--#include virtual="Includes/Functions/LibFunctions.asp" -->
<%
	StartTimer(1)
	
	Dim rsAccess
	
	UserID_193 = Session("UserID")
	if (UserID_193 = "") then
		Response.Redirect "/TimedOut.asp"
	end if
	
	ModuleIdentifier = 0

	PageName = Request.ServerVariables("SCRIPT_NAME")
	
	OpenDB()
	
	if (ByPassSecurityAccess) then
		SQLAccess = "EXECUTE AC_BYPASS_SECURITY @PAGENAME = '" & PageName & "'"					
	else
		SQLAccess = "EXECUTE AC_ALLOWACCESS @UserID = " & UserID_193 & ", @PAGENAME = '" & PageName & "'"					
	end if
	
	Set rsAccess = Conn.Execute (SQLAccess)
	
	if (NOT rsAccess.EOF) then
		ModuleIdentifier = rsAccess("MODULEID")
	else
		if (NOT ByPassSecurityAccess) then
			CloseRs(rsAccess)
			CloseDB()		
			Response.Redirect "/Logout/AccessDenied_Popup.asp"
		end if
	end if
	
	CloseRs(rsAccess)
	CloseDB()

	ZZ_Time_1 = StopTimer(1)	
	StartTimer(2)
%>