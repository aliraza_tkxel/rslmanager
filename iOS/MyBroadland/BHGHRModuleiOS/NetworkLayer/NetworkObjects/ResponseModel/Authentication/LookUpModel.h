//
//  LookUpModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface LookUpModel : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString *lookUpDescription;
@property (nonatomic) int lookUpId;

@end
