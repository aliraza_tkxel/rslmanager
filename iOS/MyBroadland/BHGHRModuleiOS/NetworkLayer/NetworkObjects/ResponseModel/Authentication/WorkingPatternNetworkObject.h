//
//  WorkingPatternNetworkObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 14/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface WorkingPatternNetworkObject : MTLModel <MTLJSONSerializing>

@property (copy,nonatomic) NSNumber *employeeId;
@property (copy,nonatomic) NSNumber *mon;
@property (copy,nonatomic) NSNumber *tue;
@property (copy,nonatomic) NSNumber *wed;
@property (copy,nonatomic) NSNumber *thu;
@property (copy,nonatomic) NSNumber *fri;
@property (copy,nonatomic) NSNumber *sat;
@property (copy,nonatomic) NSNumber *sun;
@property (copy,nonatomic) NSNumber *weekNumber;
@end
