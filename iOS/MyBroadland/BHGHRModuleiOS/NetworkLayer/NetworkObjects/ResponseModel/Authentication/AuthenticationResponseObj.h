//
//  AuthenticationResponseObj.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import "LookUpResponseObject.h"

@interface AuthenticationResponseObj : MTLModel <MTLJSONSerializing>

@property (copy,nonatomic) UserObject * userInfo;
@property (copy,nonatomic) LookUpResponseObject * lookUps;

-(void) insertUserInfo;
@end
