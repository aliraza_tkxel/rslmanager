//
//  LookUpResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LookUpResponseObject.h"

@implementation LookUpResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"absenceNature"      : @"absenceNature",
             @"absenceReason"      : @"absenceReason",
             @"ethnicity"          : @"ethnicity",
             @"sexualOrientation"  : @"sexualOrientation",
             @"maritalStatus"      : @"maritalStatus",
             @"religion"           : @"religion",
             @"leaveActions"       : @"leaveActions",
             @"myStaff"            : @"myStaff"
             };
}

+ (NSValueTransformer *)myStaffJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)absenceNatureJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)absenceReasonJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)ethnicityJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)sexualOrientationJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)maritalStatusJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)religionJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *) leaveActionsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}


-(void) insertLookUps{
    [[LookUpsDataManager sharedInstance] deleteAllLookUps];
    
    [[LookUpsDataManager sharedInstance] insertReligions:_religion];
    [[LookUpsDataManager sharedInstance] insertEthnicities:_ethnicity];
    [[LookUpsDataManager sharedInstance] insertLeaveNatures:_absenceNature];
    [[LookUpsDataManager sharedInstance] insertLeaveReasons:_absenceReason];
    [[LookUpsDataManager sharedInstance] insertSexualOrientations:_sexualOrientation];
    [[LookUpsDataManager sharedInstance] insertMaritalStatuses:_maritalStatus];
    [[LookUpsDataManager sharedInstance] insertLeaveActions:_leaveActions];
    [[LookUpsDataManager sharedInstance] insertMyStaff:_myStaff];
   
    LookUpModel *modelMale = [LookUpModel new];
    modelMale.lookUpDescription = @"Male";
    modelMale.lookUpId = 1;
    
    LookUpModel *modelFemale = [LookUpModel new];
    modelFemale.lookUpDescription = @"Female";
    modelFemale.lookUpId = 2;
    
    LookUpModel *modelTrans = [LookUpModel new];
    modelTrans.lookUpDescription = @"Prefer not to say";
    modelTrans.lookUpId = 3;
    
     NSMutableArray *gender = [[NSMutableArray alloc] initWithObjects:modelFemale,modelMale,modelTrans, nil];
    
    [[LookUpsDataManager sharedInstance] insertGenders:gender];
}

@end
