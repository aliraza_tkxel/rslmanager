//
//  LookUpModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LookUpModel.h"

@implementation LookUpModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"lookUpDescription" : @"lookUpDescription",
             @"lookUpId"          : @"lookUpId"
             };
}
@end
