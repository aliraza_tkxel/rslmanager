//
//  UserObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import "WorkingPatternNetworkObject.h"
@interface UserObject : MTLModel <MTLJSONSerializing>
@property (copy, nonatomic) NSString * employeeFullName;
@property (nonatomic) NSNumber* isActive;
@property (nonatomic) NSNumber* userId;
@property (copy, nonatomic) NSString * userName;
@property (nonatomic) NSNumber* teamId;
@property (copy,nonatomic) NSString *employmentNature;
@property (copy,nonatomic) NSString *jobRole;
@property (copy,nonatomic) NSString *gender;
@property (copy,nonatomic) NSDate *dob;
@property (copy,nonatomic) NSDate *leaveYearStart;
@property (copy,nonatomic) NSDate *leaveYearEnd;
@property (copy,nonatomic) NSDate *alStartDate;

@end
