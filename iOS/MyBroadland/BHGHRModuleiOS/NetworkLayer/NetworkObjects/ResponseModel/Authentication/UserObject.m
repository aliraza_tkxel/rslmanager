//
//  UserObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserObject.h"

@implementation UserObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"employeeFullName" : @"employeeFullName",
             @"isActive"         : @"isActive",
             @"gender"           : @"gender",
             @"jobRole"          : @"jobRole",
             @"userId"           : @"userId",
             @"userName"         : @"userName",
             @"teamId"           : @"teamId",
             @"dob"      : @"dob",
             @"employmentNature": @"employmentNature",
             @"leaveYearStart"      : @"leaveYearStart",
             @"leaveYearEnd": @"leaveYearEnd",
             @"alStartDate": @"alStartDate"
             };
}

+ (NSValueTransformer *)alStartDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)dobJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
            reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)leaveYearStartJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatDefaultStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)leaveYearEndJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatDefaultStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}


@end
