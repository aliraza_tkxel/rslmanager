//
//  AuthenticationResponseObj.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AuthenticationResponseObj.h"

@implementation AuthenticationResponseObj
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"userInfo" : @"userInfo",
             @"lookUps"  : @"lookUps"};
}

+ (NSValueTransformer *)userInfoJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UserObject class]];
}

+ (NSValueTransformer *)lookUpsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[LookUpResponseObject class]];
}

-(void) insertUserInfo{
    [[UserDataManager sharedInstance] insertUserObject:_userInfo];
}

@end
