//
//  WorkingPatternNetworkObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 14/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "WorkingPatternNetworkObject.h"

@implementation WorkingPatternNetworkObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"employeeId" : @"employeeId",
             @"mon"         : @"mon",
             @"tue"         : @"tue",
             @"wed"         : @"wed",
             @"thu"         : @"thu",
             @"fri"         : @"fri",
             @"sat"         : @"sat",
             @"sun"         : @"sun",
             @"weekNumber"         : @"weekNumber"
             };
}
@end
