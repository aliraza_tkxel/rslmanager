//
//  LookUpResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "LookUpModel.h"
@interface LookUpResponseObject : MTLModel <MTLJSONSerializing>
@property (copy,nonatomic) NSArray * absenceNature;
@property (copy,nonatomic) NSArray * absenceReason;
@property (copy,nonatomic) NSArray* ethnicity;
@property (copy,nonatomic) NSArray * sexualOrientation;
@property (copy,nonatomic) NSArray * maritalStatus;
@property (copy,nonatomic) NSArray * religion;
@property (copy,nonatomic) NSArray * leaveActions;
@property (copy,nonatomic) NSArray * myStaff;
-(void) insertLookUps;

@end
