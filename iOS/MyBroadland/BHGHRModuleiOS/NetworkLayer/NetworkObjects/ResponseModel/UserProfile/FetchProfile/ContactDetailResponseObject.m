//
//  ContactDetailResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "ContactDetailResponseObject.h"


@implementation ContactDetailResponseObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"workPhone"               : @"workPhone",
             @"workMobile"              : @"workMobile",
             @"workEmail"               : @"workEmail",
             @"mobile"                  : @"mobile",
             @"homePhone"               : @"homePhone",
             @"homeEmail"               : @"homeEmail",
             @"addressLine1"            : @"addressLine1",
             @"addressLine2"            : @"addressLine2",
             @"addressLine3"            : @"addressLine3",
             @"postalTown"              : @"postalTown",
             @"county"                  : @"county",
             @"postalCode"              : @"postalCode",
             @"emergencyContactNumber"  : @"emergencyContactNumber",
             @"emergencyContactPerson"  : @"emergencyContactPerson",
             @"emergencyContactRelationship"  : @"emergencyContactRelationship",
             @"mobilePersonal" : @"mobilePersonal"
             };
}

@end
