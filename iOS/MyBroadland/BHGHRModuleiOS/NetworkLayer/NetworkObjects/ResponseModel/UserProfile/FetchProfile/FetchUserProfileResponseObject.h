//
//  FetchUserProfileResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "PersonalDetailResponseObject.h"
#import "ContactDetailResponseObject.h"
@interface FetchUserProfileResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) PersonalDetailResponseObject * personalDetails;
@property (copy,nonatomic) ContactDetailResponseObject * contactDetails;
@end
