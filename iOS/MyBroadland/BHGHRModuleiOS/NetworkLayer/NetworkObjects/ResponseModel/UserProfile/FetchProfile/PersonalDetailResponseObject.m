//
//  PersonalDetailResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PersonalDetailResponseObject.h"

@implementation PersonalDetailResponseObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"firstName"            : @"firstName",
             @"lastName"             : @"lastName",
             @"dateOfBirth"          : @"dob",
             @"gender"               : @"gender",
             @"maritalStatusId"      : @"maritalStatus",
             @"ethnicityId"          : @"ethnicity",
             @"religionId"           : @"religion",
             @"sexualOrientationId"  : @"sexualOrientation"
             };
}

+ (NSValueTransformer *)dateOfBirthJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatDefaultStamp];//kDateFormatStyleServerStamp
            }
            reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}





@end
