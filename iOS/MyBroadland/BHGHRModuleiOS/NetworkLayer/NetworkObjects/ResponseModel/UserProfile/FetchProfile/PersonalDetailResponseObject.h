//
//  PersonalDetailResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface PersonalDetailResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString * firstName;
@property (copy,nonatomic) NSString * lastName;
@property (copy,nonatomic) NSDate * dateOfBirth;
@property (copy,nonatomic) NSString * gender;
@property (copy, nonatomic) NSNumber* maritalStatusId;
@property (copy, nonatomic) NSNumber* ethnicityId;
@property (copy, nonatomic) NSNumber* religionId;
@property (copy,nonatomic) NSNumber * sexualOrientationId;
@end
