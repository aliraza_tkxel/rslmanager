//
//  FetchUserProfileResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchUserProfileResponseObject.h"

@implementation FetchUserProfileResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"contactDetails"       : @"contactDetail",
             @"personalDetails"       : @"personalDetail"
             };
}

+ (NSValueTransformer *)contactDetailsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[ContactDetailResponseObject class]];
}

+ (NSValueTransformer *)personalDetailsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PersonalDetailResponseObject class]];
}

@end
