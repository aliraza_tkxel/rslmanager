//
//  UpdatedUserDetailResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UpdatedUserDetailResponseObject.h"

@implementation UpdatedUserDetailResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"contactDetails"       : @"contactDetail",
             @"personalDetails"      : @"personalDetail"
             };
}
+ (NSValueTransformer *)contactDetailsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[ContactDetailResponseObject class]];
}

+ (NSValueTransformer *)personalDetailsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PersonalDetailResponseObject class]];
}
@end
