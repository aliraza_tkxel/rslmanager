//
//  UpdateUserProfileResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "UpdatedUserDetailResponseObject.h"
@interface UpdateUserProfileResponseObject : MTLModel<MTLJSONSerializing>
@property (nonatomic) BOOL isSuccessFul;
@property (copy,nonatomic) NSString *message;
@end
