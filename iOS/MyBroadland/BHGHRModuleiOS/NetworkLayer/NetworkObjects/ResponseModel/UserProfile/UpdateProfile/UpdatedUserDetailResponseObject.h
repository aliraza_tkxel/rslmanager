//
//  UpdatedUserDetailResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface UpdatedUserDetailResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) PersonalDetailResponseObject * personalDetails;
@property (copy,nonatomic) ContactDetailResponseObject * contactDetails;
@end
