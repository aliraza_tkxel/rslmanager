//
//  UpdateUserProfileResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UpdateUserProfileResponseObject.h"

@implementation UpdateUserProfileResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             
             @"message"              : @"message",
             @"isSuccessFul"         : @"isSuccessFul"
             };
}

+ (NSValueTransformer *)userDetailJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UpdatedUserDetailResponseObject class]];
}


@end
