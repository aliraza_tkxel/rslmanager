//
//  FetchSicknessListResponse.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchSicknessListResponse.h"
#import "LookUpResponseObject.h"
#import "SicknessLeaveRecord.h"
#import "StaffMemberSicknessSummaryResponse.h"
@implementation FetchSicknessListResponse
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"bankHolidays" : @"bankHolidays",
             @"sicknessAbsences" : @"sicknessAbsences",
             @"absenceReasons"  : @"absenceReasons",
             @"staffMembers": @"staffMembers"};
}

+ (NSValueTransformer *)sicknessAbsencesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[SicknessLeaveRecord class]];
}

+ (NSValueTransformer *)absenceReasonsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}

+ (NSValueTransformer *)staffMembersJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[StaffMemberSicknessSummaryResponse class]];
}

@end
