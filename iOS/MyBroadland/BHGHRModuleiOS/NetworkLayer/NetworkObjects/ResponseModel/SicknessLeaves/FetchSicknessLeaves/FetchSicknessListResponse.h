//
//  FetchSicknessListResponse.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchSicknessListResponse : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * sicknessAbsences;
@property (copy,nonatomic) NSArray * absenceReasons;
@property (copy,nonatomic) NSArray * bankHolidays;
@property (copy, nonatomic) NSArray *staffMembers;
@end
