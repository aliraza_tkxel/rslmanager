//
//  SicknessLeaveRecord.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SicknessLeaveRecord : MTLModel<MTLJSONSerializing>

@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSDate * endDate;
@property (copy, nonatomic) NSString * nature;
@property (copy, nonatomic) NSString * reason;
@property (copy, nonatomic) NSString * status;
@property (copy,nonatomic) NSNumber *statusId;
@property (copy,nonatomic) NSDate *anticipatedReturnDate;
@property (copy, nonatomic) NSNumber *absenceHistoryId;
@property (copy,nonatomic) NSString *notes;
@property (copy,nonatomic) NSNumber *natureId;
@property (copy,nonatomic) NSNumber *reasonId;
@property (copy,nonatomic) NSNumber *absentDays;

@end
