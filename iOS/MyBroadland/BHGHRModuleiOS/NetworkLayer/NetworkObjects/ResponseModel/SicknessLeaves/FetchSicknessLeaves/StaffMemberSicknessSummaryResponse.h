//
//  StaffMemberSicknessSummaryResponse.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface StaffMemberSicknessSummaryResponse : MTLModel<MTLJSONSerializing>
@property (copy, nonatomic) NSDate * currentleaveStartDate;
@property (copy, nonatomic) NSString * staffMemberLName;
@property (copy, nonatomic) NSString * staffMemberFName;
@property (copy,nonatomic) NSNumber * staffMemberId;
@property (copy,nonatomic) NSNumber * lineManagerId;
@property (copy, nonatomic) NSNumber * absentDays;
@property (copy, nonatomic) NSNumber * reasonId ;
@property (copy, nonatomic) NSDate * endDate ;
@property (copy, nonatomic) NSString * nature ;
@property (copy, nonatomic) NSString * holidayType ;
@property (copy, nonatomic) NSString * reason ;
@property (copy, nonatomic) NSNumber * statusId ;
@property (copy, nonatomic) NSDate * anticipatedReturnDate ;
@property (copy, nonatomic) NSString * status ;
@property (copy, nonatomic) NSNumber * natureId ;
@end
