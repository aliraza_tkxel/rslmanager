//
//  StaffMemberSicknessSummaryResponse.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "StaffMemberSicknessSummaryResponse.h"

@implementation StaffMemberSicknessSummaryResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"currentleaveStartDate" : @"startDate",
             @"staffMemberLName"  : @"staffMemberLName",
             @"staffMemberFName"    : @"staffMemberFName",
             @"staffMemberId"     : @"staffMemberId",
             @"lineManagerId"     : @"lineManagerId",
             @"absentDays" : @"absentDays",
             @"reasonId"  : @"reasonId",
             @"endDate"    : @"endDate",
             @"nature"     : @"nature",
             @"holidayType"     : @"holidayType",
             @"reason"     : @"reason",
             @"statusId"     : @"statusId",
             @"anticipatedReturnDate"     : @"anticipatedReturnDate",
             @"status"     : @"status",
             @"natureId"     : @"natureId"
             };
}

+ (NSValueTransformer *)currentleaveStartDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)endDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)anticipatedReturnDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}
@end
