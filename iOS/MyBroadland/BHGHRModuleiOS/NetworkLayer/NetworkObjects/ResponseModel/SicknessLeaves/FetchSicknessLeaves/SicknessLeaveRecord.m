//
//  SicknessLeaveRecord.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SicknessLeaveRecord.h"

@implementation SicknessLeaveRecord
/*
 @property (copy, nonatomic) NSNumber *absenceHistoryId;
 @property (copy,nonatomic) NSString *notes;
 @property (copy,nonatomic) NSNumber *natureId;
 @property (copy,nonatomic) NSNumber *reasonId;
 */
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"absentDays" : @"absentDays",
             @"startDate"  : @"startDate",
             @"endDate"    : @"endDate",
             @"nature"     : @"nature",
             @"reason"     : @"reason",
             @"status"     : @"status",
             @"statusId"     : @"statusId",
             @"anticipatedReturnDate" : @"anticipatedReturnDate",
             @"absenceHistoryId" : @"absenceHistoryId",
             @"notes" : @"notes",
             @"natureId" : @"natureId",
             @"reasonId" : @"reasonId"
             };
}

+ (NSValueTransformer *)anticipatedReturnDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}
+ (NSValueTransformer *)endDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}


@end
