//
//  FetchAbsenceLeavesListResponse.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchAbsenceLeavesListResponse : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * absentees;
@property (copy,nonatomic) NSArray * leavesNatures;
@property (copy,nonatomic) NSArray * bankHolidays;
@property (copy, nonatomic) NSNumber * toilsOwed;
@end
