//
//  FetchAbsenceLeavesListResponse.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchAbsenceLeavesListResponse.h"
#import "LookUpModel.h"
#import "AbsenceLeaveRecord.h"
@implementation FetchAbsenceLeavesListResponse
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"bankHolidays" : @"bankHolidays",
             @"absentees" : @"absentees",
             @"leavesNatures"  : @"leavesNatures",
             @"toilsOwed" : @"toilsOwed"};
}

+ (NSValueTransformer *)absenteesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[AbsenceLeaveRecord class]];
}

+ (NSValueTransformer *)leavesNaturesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LookUpModel class]];
}
@end
