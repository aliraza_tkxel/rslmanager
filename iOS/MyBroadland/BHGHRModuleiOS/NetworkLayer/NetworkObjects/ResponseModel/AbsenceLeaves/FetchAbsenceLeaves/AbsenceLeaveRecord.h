//
//  AbsenceLeaveRecord.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AbsenceLeaveRecord : MTLModel<MTLJSONSerializing>

@property (nonatomic) NSNumber* absentDays;
@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSDate * endDate;
@property (copy, nonatomic) NSString * nature;
@property (copy, nonatomic) NSString * status;
@property (copy, nonatomic) NSString * reason;
@property (copy, nonatomic) NSString * unit;
@property (copy,nonatomic) NSString * holidayType;
@property (copy, nonatomic) NSNumber *statusId;
@property (copy, nonatomic) NSNumber *natureId;
@property (copy,nonatomic)  NSNumber* absenceHistoryId;
@end
