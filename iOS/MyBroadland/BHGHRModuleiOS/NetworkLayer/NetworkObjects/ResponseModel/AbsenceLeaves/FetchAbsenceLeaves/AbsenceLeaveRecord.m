//
//  AbsenceLeaveRecord.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbsenceLeaveRecord.h"

@implementation AbsenceLeaveRecord
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"unit" : @"unit",
             @"absentDays" : @"absentDays",
             @"startDate"  : @"startDate",
             @"endDate"    : @"endDate",
             @"nature"     : @"nature",
             @"status"     : @"status",
             @"reason"     : @"reason",
             @"holidayType"     : @"holidayType",
             @"statusId"     : @"statusId",
             @"natureId"     : @"natureId",
             @"absenceHistoryId": @"absenceHistoryId"
             };
}
+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}
+ (NSValueTransformer *)endDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

@end
