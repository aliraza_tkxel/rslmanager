//
//  GenericLeaveResponse.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "GenericLeaveResponse.h"

@implementation GenericLeaveResponse
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             
             @"message"              : @"message",
             @"isSuccessFul"         : @"isSuccessFul"
             };
}
@end
