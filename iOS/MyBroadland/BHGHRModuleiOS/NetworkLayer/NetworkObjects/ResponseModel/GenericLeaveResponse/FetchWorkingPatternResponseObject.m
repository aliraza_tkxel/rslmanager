//
//  FetchWorkingPatternResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 27/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "FetchWorkingPatternResponseObject.h"

@implementation FetchWorkingPatternResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"workingHours":@"workingHours"
             };
}
+ (NSValueTransformer *)workingHoursJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[WorkingPatternNetworkObject class]];
}
@end
