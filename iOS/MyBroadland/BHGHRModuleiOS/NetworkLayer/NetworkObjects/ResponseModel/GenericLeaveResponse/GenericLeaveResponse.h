//
//  GenericLeaveResponse.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GenericLeaveResponse : MTLModel<MTLJSONSerializing>
@property (nonatomic) BOOL isSuccessFul;
@property (copy,nonatomic) NSString *message;
@end
