//
//  FetchWorkingPatternResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 27/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchWorkingPatternResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * workingHours;
@end
