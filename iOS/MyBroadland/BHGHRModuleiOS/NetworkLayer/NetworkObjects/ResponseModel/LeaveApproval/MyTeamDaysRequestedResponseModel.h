//
//  MyTeamDaysRequestedResponseModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MyTeamDaysRequestedResponseModel : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * myTeamLeavesPending;
@end
