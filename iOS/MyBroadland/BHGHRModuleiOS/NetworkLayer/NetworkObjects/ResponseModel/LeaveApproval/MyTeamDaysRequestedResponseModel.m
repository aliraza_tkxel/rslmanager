//
//  MyTeamDaysRequestedResponseModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MyTeamDaysRequestedResponseModel.h"
#import "LeaveTakenNetworkObject.h"
@implementation MyTeamDaysRequestedResponseModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"myTeamLeavesPending"      : @"myTeamLeavesPending"
             };
}
+ (NSValueTransformer *)myTeamLeavesPendingJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LeaveTakenNetworkObject class]];
}
@end
