//
//  MainDashboardTrainingModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MainDashboardTrainingModel.h"

@implementation MainDashboardTrainingModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"trainingId"           : @"trainingId",
             @"employeeId"             : @"employeeId",
             @"employeeName"            : @"employeeName",
             @"justification"            : @"justification",
             @"course"            : @"course",
             @"startDate"    : @"startDate",
             @"endDate"                : @"endDate",
             @"totalNumberOfDays"              : @"totalNumberOfDays",
             @"providerName"              : @"providerName",
             @"providerWebsite"              : @"providerWebsite",
             @"location"              : @"location",
             @"venue"              : @"venue",
             @"totalCost"    : @"totalCost",
             @"isSubmittedBy"                : @"isSubmittedBy",
             @"isMandatoryTraining"              : @"isMandatoryTraining",
             @"expiry"              : @"expiry",
             @"additionalNotes"              : @"additionalNotes",
             @"status"              : @"status",
             @"createdby"              : @"createdby",
             @"approvedTraining"              : @"approvedTraining",
             };
}
+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatDefaultStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)endDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatDefaultStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}
@end
