//
//  MainDashboardTrainingResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MainDashboardTrainingResponseObject.h"
#import "MainDashboardTrainingModel.h"
@implementation MainDashboardTrainingResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"trainingList" : @"trainingList",
             @"pagination"   : @"pagination"
             };
}
+ (NSValueTransformer *)trainingListJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[MainDashboardTrainingModel class]];
}

@end
