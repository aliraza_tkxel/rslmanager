//
//  MainDashboardStatsModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MainDashboardStatsModel.h"

@implementation MainDashboardStatsModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"itemName" : @"itemName",
             @"itemValue"          : @"itemValue"
             };
}

@end
