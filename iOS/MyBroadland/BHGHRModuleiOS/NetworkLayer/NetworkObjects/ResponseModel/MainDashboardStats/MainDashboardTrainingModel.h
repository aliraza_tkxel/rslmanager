//
//  MainDashboardTrainingModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MainDashboardTrainingModel : MTLModel<MTLJSONSerializing>

@property (copy,nonatomic) NSNumber *trainingId;
@property (copy,nonatomic) NSNumber *employeeId;
@property (copy,nonatomic) NSString *employeeName;
@property (copy,nonatomic) NSString *justification;
@property (copy,nonatomic) NSString *course;
@property (copy,nonatomic) NSDate *startDate;
@property (copy,nonatomic) NSDate *endDate;
@property (copy,nonatomic) NSNumber *totalNumberOfDays;
@property (copy,nonatomic) NSString *providerName;
@property (copy,nonatomic) NSString *providerWebsite;
@property (copy,nonatomic) NSString *location;
@property (copy,nonatomic) NSString *venue;
@property (copy,nonatomic) NSNumber *totalCost;
@property (copy,nonatomic) NSString *isSubmittedBy;
@property (copy,nonatomic) NSNumber *isMandatoryTraining;
@property (copy,nonatomic) NSString *expiry;
@property (copy,nonatomic) NSString *additionalNotes;
@property (copy,nonatomic) NSString *status;
@property (copy,nonatomic) NSString *createdby;
@property (copy,nonatomic) NSString *approvedTraining;
@end
