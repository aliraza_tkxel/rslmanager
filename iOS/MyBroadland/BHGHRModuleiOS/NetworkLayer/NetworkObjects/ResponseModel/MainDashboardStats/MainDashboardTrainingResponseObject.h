//
//  MainDashboardTrainingResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MainDashboardTrainingResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * trainingList;
@property (copy,nonatomic) NSNumber * pagination;
@end
