//
//  MainDashboardStatsResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MainDashboardStatsResponseObject.h"
#import "MainDashboardStatsModel.h"
@implementation MainDashboardStatsResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"approvalStats" : @"ApprovalStats"
             };
}
+ (NSValueTransformer *)approvalStatsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[MainDashboardStatsModel class]];
}
@end
