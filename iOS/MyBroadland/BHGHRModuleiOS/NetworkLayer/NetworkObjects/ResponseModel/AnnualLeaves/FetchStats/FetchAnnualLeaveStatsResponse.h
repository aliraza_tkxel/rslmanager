//
//  FetchAnnualLeaveStatsResponse.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchAnnualLeaveStatsResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic) NSNumber * totalAllowance ;
@property (nonatomic) NSNumber * leavesBooked ;
@property (nonatomic) NSNumber * leavesRequested ;
@property (nonatomic) NSNumber * leavesTaken ;
@property (nonatomic) NSNumber * leavesRemaining ;
@property (nonatomic) NSNumber * holidayEntitlementHours;
@property (nonatomic) NSNumber * holidayEntitlementDays;
@property (nonatomic) NSNumber * timeOffInLieuOwed;
@property (nonatomic) NSNumber * carryForward;
@property (nonatomic) NSNumber * bankHoliday;
@property (copy, nonatomic) NSDate* startDate ;
@property (copy, nonatomic) NSString * unit ;

@end
