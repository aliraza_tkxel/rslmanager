//
//  FetchAnnualLeaveStatsResponse.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchAnnualLeaveStatsResponse.h"

@implementation FetchAnnualLeaveStatsResponse

/*
 @property (nonatomic) double holidayEntitlementDays;
 @property (nonatomic) double timeOffInLieuOwed;
 @property (nonatomic) double carryForward;
 @property (nonatomic) double bankHoliday;
 */
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"totalAllowance"    : @"totalAllowance",
             @"leavesBooked"      : @"leavesBooked",
             @"leavesRequested"   : @"leavesRequested",
             @"leavesTaken"       : @"leavesTaken",
             @"leavesRemaining"   : @"leavesRemaining",
             @"startDate"         : @"startDate",
             @"unit"              : @"unit",
             @"holidayEntitlementDays": @"holidayEntitlementDays",
             @"holidayEntitlementHours":@"holidayEntitlementHours",
             @"timeOffInLieuOwed" : @"timeOfInLieuOwed",
             @"carryForward"      : @"carryForward",
             @"bankHoliday"       : @"bankHoliday"
             };
}


+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

@end
