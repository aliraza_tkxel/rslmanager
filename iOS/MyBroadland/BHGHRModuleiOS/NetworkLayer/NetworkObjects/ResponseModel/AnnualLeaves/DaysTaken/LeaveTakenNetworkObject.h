//
//  LeaveTakenNetworkObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface LeaveTakenNetworkObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSDate * startDate;
@property (copy,nonatomic) NSDate * endDate;
@property (nonatomic) NSNumber*  duration;

@property (copy,nonatomic) NSString * unit;
@property (copy,nonatomic) NSNumber* natureId;
@property (copy,nonatomic) NSString * leaveDescription;
@property (copy, nonatomic) NSString * status;

@property (copy,nonatomic)  NSNumber* statusId;
@property (copy,nonatomic)  NSNumber* absenceHistoryId;
@property (copy,nonatomic)  NSNumber* applicantId;
@property (copy, nonatomic) NSString * applicantName;
@end
