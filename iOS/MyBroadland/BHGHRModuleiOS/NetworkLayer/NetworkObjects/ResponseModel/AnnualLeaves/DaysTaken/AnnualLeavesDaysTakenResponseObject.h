//
//  AnnualLeavesDaysTakenResponseObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AnnualLeavesDaysTakenResponseObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSArray * leaveTakenList;
@end
