//
//  AnnualLeavesDaysTakenResponseObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeavesDaysTakenResponseObject.h"
#import "LeaveTakenNetworkObject.h"
@implementation AnnualLeavesDaysTakenResponseObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName      : json_key
              */
             @"leaveTakenList"      : @"leaveTakenList"
             };
}
+ (NSValueTransformer *)leaveTakenListJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[LeaveTakenNetworkObject class]];
}

@end
