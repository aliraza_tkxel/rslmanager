//
//  LeaveTakenNetworkObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveTakenNetworkObject.h"

@implementation LeaveTakenNetworkObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"startDate"           : @"startDate",
             @"endDate"             : @"endDate",
             @"duration"            : @"duration",
             @"duration"            : @"duration",
             @"natureId"            : @"natureId",
             @"leaveDescription"    : @"leaveDescription",
             @"unit"                : @"unit",
             @"status"              : @"status",
             @"statusId"              : @"statusId",
             @"absenceHistoryId"              : @"absenceHistoryId",
             @"applicantId"              : @"applicantId",
             @"applicantName"              : @"applicantName",
             };
}
+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
                
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}

+ (NSValueTransformer *)endDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *dateString, BOOL *success, NSError **error)
            {
                return [DateUtilities getDateFromString:dateString
                                             withFormat:kDateFormatStyleServerStamp];
            }
                                                reverseBlock:^(NSDate *date, BOOL *success, NSError **error)
            {
               
                return [DateUtilities getDateStringFromDate:date
                                                 withFormat:kDateFormatDefaultStamp];
            }];
}


@end
