//
//  AmendSicknessRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/8/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AmendSicknessRequestObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSNumber* actionBy;
@property (copy,nonatomic) NSNumber* absenceHistoryId;
@property (copy,nonatomic) NSNumber* actionId;
@property (copy,nonatomic) NSString* certNo;
@property (copy,nonatomic) NSString* drName;
@property (copy,nonatomic) NSString* notes;
@property (copy,nonatomic) NSNumber* reasonId;
@property (copy,nonatomic) NSString* startDate;
@property (copy,nonatomic) NSString* endDate;
@property (copy,nonatomic) NSString* anticipatedReturnDate;
@property (copy,nonatomic) NSNumber* duration;

@property (copy,nonatomic) NSDate* startDateRaw;
@property (copy,nonatomic) NSDate* endDateRaw;
@property (copy,nonatomic) NSDate* anticipatedReturnDateRaw;
@end
