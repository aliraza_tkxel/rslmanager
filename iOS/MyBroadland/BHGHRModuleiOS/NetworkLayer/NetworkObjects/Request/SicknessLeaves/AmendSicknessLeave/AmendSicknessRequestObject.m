//
//  AmendSicknessRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/8/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AmendSicknessRequestObject.h"

@implementation AmendSicknessRequestObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"actionBy"       : @"actionBy",
             @"absenceHistoryId"       : @"absenceHistoryId",
             @"actionId"       : @"actionId",
             @"certNo"       : @"certNo",
             @"drName"       : @"drName",
             @"notes"       : @"notes",
             @"reasonId"       : @"reasonId",
             @"startDate"       : @"startDate",
             @"endDate"       : @"endDate",
             @"anticipatedReturnDate"       : @"anticipatedReturnDate",
             @"duration"       : @"duration"
             };
}
@end
