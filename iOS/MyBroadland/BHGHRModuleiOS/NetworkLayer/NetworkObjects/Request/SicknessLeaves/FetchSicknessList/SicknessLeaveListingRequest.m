//
//  SicknessLeaveListingRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SicknessLeaveListingRequest.h"

@implementation SicknessLeaveListingRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"employeeId"       : @"employeeId"
             };
}
@end
