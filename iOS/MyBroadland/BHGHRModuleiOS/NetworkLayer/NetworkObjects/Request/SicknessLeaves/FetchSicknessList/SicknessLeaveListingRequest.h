//
//  SicknessLeaveListingRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SicknessLeaveListingRequest : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@end
