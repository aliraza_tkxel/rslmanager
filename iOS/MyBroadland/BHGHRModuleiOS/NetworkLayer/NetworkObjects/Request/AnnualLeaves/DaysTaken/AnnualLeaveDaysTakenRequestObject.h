//
//  AnnualLeaveDaysTakenRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AnnualLeaveDaysTakenRequestObject : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@end
