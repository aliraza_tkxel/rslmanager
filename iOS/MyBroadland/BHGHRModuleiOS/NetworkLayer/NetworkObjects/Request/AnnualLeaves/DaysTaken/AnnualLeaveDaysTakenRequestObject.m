//
//  AnnualLeaveDaysTakenRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeaveDaysTakenRequestObject.h"

@implementation AnnualLeaveDaysTakenRequestObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"employeeId"       : @"employeeId"
             };
}
@end
