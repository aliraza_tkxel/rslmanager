//
//  FetchAnnualLeaveStatsRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchAnnualLeaveStatsRequest : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@end
