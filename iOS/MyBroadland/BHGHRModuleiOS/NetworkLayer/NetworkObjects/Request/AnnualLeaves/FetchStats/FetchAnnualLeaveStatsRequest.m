//
//  FetchAnnualLeaveStatsRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchAnnualLeaveStatsRequest.h"

@implementation FetchAnnualLeaveStatsRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"employeeId"       : @"employeeId"
             };
}
@end
