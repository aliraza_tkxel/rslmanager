//
//  GeneralUserOperationRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 21/02/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GeneralUserOperationRequest : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@property (copy,nonatomic) NSString * fiscalYearDate;
@end
