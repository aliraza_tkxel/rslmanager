//
//  GeneralUserOperationRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 21/02/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "GeneralUserOperationRequest.h"

@implementation GeneralUserOperationRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"employeeId"           : @"employeeId",
             @"fiscalYearDate"       : @"fiscalYearDate"
             };
}

@end
