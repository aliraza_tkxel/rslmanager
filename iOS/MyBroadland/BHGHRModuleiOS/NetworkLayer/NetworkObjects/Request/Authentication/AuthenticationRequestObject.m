//
//  AuthenticationRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AuthenticationRequestObject.h"

@implementation AuthenticationRequestObject

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"userName"       : @"userName",
             @"password"       : @"password"
             };
}
@end
