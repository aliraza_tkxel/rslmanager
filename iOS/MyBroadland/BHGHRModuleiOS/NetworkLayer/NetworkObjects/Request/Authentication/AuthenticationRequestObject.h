//
//  AuthenticationRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
@interface AuthenticationRequestObject : MTLModel<MTLJSONSerializing>
@property (copy, nonatomic) NSString * userName;
@property (copy, nonatomic) NSString * password;
@end
