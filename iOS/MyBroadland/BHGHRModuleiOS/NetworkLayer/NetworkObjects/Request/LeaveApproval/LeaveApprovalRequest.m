//
//  LeaveApprovalRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveApprovalRequest.h"

@implementation LeaveApprovalRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"actionBy"       : @"actionBy",
             @"absenceHistoryId"       : @"absenceHistoryId",
             @"actionId"       : @"actionId",
             @"notes"       : @"notes"
             };
}
@end
