//
//  LeaveApprovalRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface LeaveApprovalRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic) int actionBy;
@property (copy,nonatomic) NSNumber* absenceHistoryId;
@property (copy,nonatomic) NSNumber* actionId;
@property (copy,nonatomic) NSString* notes;

@end
