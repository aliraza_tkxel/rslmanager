//
//  GenericLeaveRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GenericLeaveRequest : MTLModel<MTLJSONSerializing>

@property (nonatomic) int employeeId;
@property (copy,nonatomic) NSString* anticipatedReturnDate;/*For Sickness*/
@property (copy,nonatomic) NSNumber* absenceHistoryId;
@property (copy,nonatomic) NSNumber* reasonId;
@property (copy,nonatomic) NSString* startDate;
@property (copy,nonatomic) NSString* endDate;
@property (nonatomic) double duration;
@property (nonatomic) int recordedBy;
@property (copy,nonatomic) NSString* notes;
@property (copy,nonatomic) NSString* holType;
@property (copy,nonatomic) NSString* reason;
@property (copy,nonatomic) NSNumber* natureId;
@end
