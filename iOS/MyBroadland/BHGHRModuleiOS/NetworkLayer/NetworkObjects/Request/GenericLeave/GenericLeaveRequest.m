//
//  GenericLeaveRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "GenericLeaveRequest.h"

@implementation GenericLeaveRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"employeeId"            : @"employeeId",
             @"absenceHistoryId"           : @"absenceHistoryId",
             @"reasonId"             : @"reasonId",
             @"startDate"                  : @"startDate",
             @"endDate"                  : @"endDate",
             @"duration"               : @"duration",
             @"recordedBy"        : @"recordedBy",
             @"notes"           : @"notes",
             @"holType"             : @"holType",
             @"reason"    : @"reason",
             @"natureId"    : @"natureId",
             @"anticipatedReturnDate": @"anticipatedReturnDate"
             };
}

@end
