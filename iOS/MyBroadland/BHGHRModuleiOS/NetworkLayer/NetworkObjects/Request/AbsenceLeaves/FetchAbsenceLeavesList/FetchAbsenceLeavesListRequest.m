//
//  FetchAbsenceLeavesListRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchAbsenceLeavesListRequest.h"

@implementation FetchAbsenceLeavesListRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"employeeId"       : @"employeeId"
             };
}
@end
