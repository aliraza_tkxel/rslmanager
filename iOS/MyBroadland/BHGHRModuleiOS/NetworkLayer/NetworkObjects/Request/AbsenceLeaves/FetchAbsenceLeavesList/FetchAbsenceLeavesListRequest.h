//
//  FetchAbsenceLeavesListRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchAbsenceLeavesListRequest : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@end
