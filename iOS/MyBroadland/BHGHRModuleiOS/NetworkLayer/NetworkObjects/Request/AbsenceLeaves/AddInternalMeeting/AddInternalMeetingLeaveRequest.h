//
//  AddInternalMeetingLeaveRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 23/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
@interface AddInternalMeetingLeaveRequest : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString* startDate;
@property (copy,nonatomic) NSDate *startDateRaw;
@property (copy,nonatomic) NSString* from;
@property (copy,nonatomic) NSDate *startTimeRaw;
@property (copy,nonatomic) NSString* to;
@property (copy,nonatomic) NSDate *endTimeRaw;
@property (copy,nonatomic) NSNumber* duration;
@property (copy,nonatomic) NSNumber* recordedBy;
@property (copy,nonatomic) NSNumber* employeeId;
@property (copy,nonatomic) NSString *employeeNameRaw;
@property (copy,nonatomic) NSString *recorderNameRaw;
@property (copy,nonatomic) NSString* notes;
@property (copy,nonatomic) NSString* leaveType;
@end
