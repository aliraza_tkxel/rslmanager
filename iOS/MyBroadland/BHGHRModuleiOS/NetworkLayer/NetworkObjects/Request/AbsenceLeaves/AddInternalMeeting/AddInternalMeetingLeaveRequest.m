//
//  AddInternalMeetingLeaveRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 23/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AddInternalMeetingLeaveRequest.h"

@implementation AddInternalMeetingLeaveRequest
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"startDate"       : @"startDate",
             @"from"       : @"from",
             @"to"       : @"to",
             @"duration"       : @"duration",
             @"recordedBy"       : @"recordedBy",
             @"employeeId"       : @"employeeId",
             @"notes"       : @"notes",
             @"leaveType"       : @"leaveType"
             };
}

+ (NSValueTransformer*)durationJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *jsonValue, BOOL *success, NSError **error)
            {
                NSNumber *number = @(jsonValue.floatValue);
                return number;
            }
                                                reverseBlock:^(NSNumber *numberValue, BOOL *success, NSError **error)
            {
                
                return [NSString stringWithFormat:@"%.2f", numberValue.floatValue];
            }];
}
@end
