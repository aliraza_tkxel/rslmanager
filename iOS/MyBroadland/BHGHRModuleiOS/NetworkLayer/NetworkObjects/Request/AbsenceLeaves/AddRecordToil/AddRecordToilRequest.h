//
//  AddRecordToilRequest.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface AddRecordToilRequest : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) NSString* startDate;
@property (copy,nonatomic) NSDate *startDateRaw;
@property (copy,nonatomic) NSString* from;
@property (copy,nonatomic) NSDate *startTimeRaw;
@property (copy,nonatomic) NSString* to;
@property (copy,nonatomic) NSDate *endTimeRaw;
@property (copy,nonatomic) NSNumber* duration;
@property (copy,nonatomic) NSNumber* recordedBy;
@property (copy,nonatomic) NSNumber* employeeId;
@property (copy,nonatomic) NSString *employeeNameRaw;
@property (copy,nonatomic) NSString *recorderNameRaw;
@property (copy,nonatomic) NSString* notes;
@property (copy,nonatomic) NSString* leaveType;
@end
