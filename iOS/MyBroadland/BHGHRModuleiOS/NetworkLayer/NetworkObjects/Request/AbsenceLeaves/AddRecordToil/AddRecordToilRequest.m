//
//  AddRecordToilRequest.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AddRecordToilRequest.h"

@implementation AddRecordToilRequest

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"startDate"       : @"startDate",
             @"from"       : @"from",
             @"to"       : @"to",
             @"duration"       : @"duration",
             @"recordedBy"       : @"recordedBy",
             @"employeeId"       : @"employeeId",
             @"notes"       : @"notes",
             @"leaveType"       : @"leaveType"
             };
}

+ (NSValueTransformer*)durationJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *jsonValue, BOOL *success, NSError **error)
            {
                NSNumber *number = @(jsonValue.floatValue);
                return number;
            }
            reverseBlock:^(NSNumber *numberValue, BOOL *success, NSError **error)
            {
                
                return [NSString stringWithFormat:@"%.2f", numberValue.floatValue];
            }];
}

@end
