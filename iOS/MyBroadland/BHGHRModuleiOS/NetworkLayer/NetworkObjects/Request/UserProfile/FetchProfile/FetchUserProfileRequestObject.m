//
//  FetchUserProfileRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FetchUserProfileRequestObject.h"

@implementation FetchUserProfileRequestObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"employeeId"       : @"employeeId"
             };
}
@end
