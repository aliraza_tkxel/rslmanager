//
//  FetchUserProfileRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface FetchUserProfileRequestObject : MTLModel<MTLJSONSerializing>
@property (nonatomic) int employeeId;
@end
