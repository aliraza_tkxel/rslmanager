//
//  UpdatePersonalInfoRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface UpdatePersonalInfoRequestObject : MTLModel <MTLJSONSerializing>
@property (copy,nonatomic) NSString * firstName;
@property (copy,nonatomic) NSString * lastName;
@property (copy,nonatomic) NSString * dob;
@property (copy,nonatomic) NSString * gender;
@property (copy, nonatomic) NSNumber* maritalStatus;
@property (copy, nonatomic) NSNumber* ethnicity;
@property (copy, nonatomic) NSNumber* religion;
@property (copy,nonatomic)  NSNumber * sexualOrientation;
@property (copy,nonatomic) NSNumber* employeeId;
@end
