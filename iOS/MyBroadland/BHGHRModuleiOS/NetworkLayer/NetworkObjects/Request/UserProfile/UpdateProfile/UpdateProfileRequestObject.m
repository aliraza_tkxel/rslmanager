//
//  UpdateProfileRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UpdateProfileRequestObject.h"

@implementation UpdateProfileRequestObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             /*
              propertyName       : json_key
              */
             @"contactDetail"       : @"contactDetail",
             @"personalDetail"       : @"personalDetail"
             };
}

+ (NSValueTransformer *)contactDetailJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UpdateContactInfoRequestObject class]];
}

+ (NSValueTransformer *)personalDetailJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UpdatePersonalInfoRequestObject class]];
}


@end
