//
//  UpdateProfileRequestObject.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "UpdateContactInfoRequestObject.h"
#import "UpdatePersonalInfoRequestObject.h"

@interface UpdateProfileRequestObject : MTLModel<MTLJSONSerializing>
@property (copy,nonatomic) UpdatePersonalInfoRequestObject * personalDetail;
@property (copy,nonatomic) UpdateContactInfoRequestObject * contactDetail;

@end
