//
//  UpdatePersonalInfoRequestObject.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UpdatePersonalInfoRequestObject.h"
@implementation UpdatePersonalInfoRequestObject
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    
    return @{
             /*
              propertyName      : json_key
              */
             @"firstName"            : @"firstName",
             @"employeeId"           : @"employeeId",
             @"lastName"             : @"lastName",
             @"dob"                  : @"dob",
             @"gender"               : @"gender",
             @"maritalStatus"        : @"maritalStatus",
             @"ethnicity"           : @"ethnicity",
             @"religion"             : @"religion",
             @"sexualOrientation"    : @"sexualOrientation"
             };
}




@end
