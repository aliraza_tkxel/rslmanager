//
//  AbsenceLeaveManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "GenericLeaveModel.h"
#import "GenericLeaveRequest.h"
#import "GenericLeaveResponse.h"
#import "AddRecordToilRequest.h"
#import "AddInternalMeetingLeaveRequest.h"
@interface AbsenceLeaveManager : AbstractBaseManager
-(void) fetchAbsenceLeavesListForUserId:(int)userId
                                forYear: (NSString *) year
                      withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) fetchTOILLeavesListForUserId:(int)userId
                   withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordAbsenceLeaveModel:(GenericLeaveModel *) model
              withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordAddToilForRequest:(AddRecordToilRequest *) requestObj
              withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordInternalMeetingLeave:(AddInternalMeetingLeaveRequest *) requestObj
                 withResponseBlock:(ManagerResponseBlock)responseBlock;
@end
