//
//  AbsenceLeaveManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbsenceLeaveManager.h"
#import "FetchAbsenceLeavesListRequest.h"
#import "FetchAbsenceLeavesListResponse.h"
#import "AbsenceLeaveRecord.h"
#import "GenericLeaveManager.h"
@implementation AbsenceLeaveManager
-(void) fetchAbsenceLeavesListForUserId:(int)userId
                                forYear: (NSString *) year
                       withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_ABSENCE_LIST
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         FetchAbsenceLeavesListResponse * respObj = (FetchAbsenceLeavesListResponse *) [self.networkManager serializeObject:[FetchAbsenceLeavesListResponse class] FromJson:response];
         [self insertLookUpsForResponse:respObj];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
    
}

-(void) fetchTOILLeavesListForUserId:(int)userId
                      withResponseBlock:(ManagerResponseBlock)responseBlock{
    FetchAbsenceLeavesListRequest *requestObj = [FetchAbsenceLeavesListRequest new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_TOIL_LIST
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         FetchAbsenceLeavesListResponse * respObj = (FetchAbsenceLeavesListResponse *) [self.networkManager serializeObject:[FetchAbsenceLeavesListResponse class] FromJson:response];
         [self insertLookUpsForResponse:respObj];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
    
}



-(void) insertLookUpsForResponse:(FetchAbsenceLeavesListResponse *) response{
    [[LookUpsDataManager sharedInstance] insertBankHolidays:response.bankHolidays];
    [[LookUpsDataManager sharedInstance] insertAbsenceLeaveReasons:response.leavesNatures];
}

-(void) recordAbsenceLeaveModel:(GenericLeaveModel *) model
             withResponseBlock:(ManagerResponseBlock)responseBlock{
    
    GenericLeaveRequest *request = [GenericLeaveRequest new];
    
    request.employeeId = LoggedInUserId;
    request.startDate = [DateUtilities getDateStringFromDate:model.startDate
                                                withFormat:kDateFormatDefaultStamp];
    request.natureId = [NSNumber numberWithInt:model.reasonId];
    request.endDate = [DateUtilities getDateStringFromDate:model.endDate
                                                withFormat:kDateFormatDefaultStamp];
    request.duration = model.duration;
    request.recordedBy = LoggedInUserId;
    request.notes = model.notes;
    request.holType = [model getHolidayType];
    if([request.holType isEqualToString:kHolTypeBothFullDay] || [request.holType isEqualToString:kHolTypeStartFullEndHalf]||[request.holType isEqualToString:kHolTypeStartHalfEndFull]||[request.holType isEqualToString:kHolTypeSingleFull]
       ||[request.holType isEqualToString:kHolTypeSingleMorning]||[request.holType isEqualToString:kHolTypeSingleAfternoon]){
        NSDictionary * leaveRequest = [self.networkManager deserializeDictionaryFromObject:request];
        
        [self.networkManager sendPOST:SERVICE_RECORD_ABSENCE parameter:leaveRequest response:^(BOOL success, id response, NSError *error) {
            GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
            respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
            responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
        }];
    }
    else{
        responseBlock(NO,[NSNull null], kValidationInvalidHolTypeCombination);
    }
    

}

-(void) recordAddToilForRequest:(AddRecordToilRequest *) requestObj
              withResponseBlock:(ManagerResponseBlock)responseBlock{
    NSDictionary * leaveRequest = [self.networkManager deserializeDictionaryFromObject:requestObj];
    [self.networkManager sendPOST:SERVICE_ADD_RECORD_TOIL parameter:leaveRequest response:^(BOOL success, id response, NSError *error) {
        GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
        respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
}

-(void) recordInternalMeetingLeave:(AddInternalMeetingLeaveRequest *) requestObj
                 withResponseBlock:(ManagerResponseBlock)responseBlock
{
    NSDictionary * leaveRequest = [self.networkManager deserializeDictionaryFromObject:requestObj];
    [self.networkManager sendPOST:SERVICE_RECORD_INTERNAL_MEETING parameter:leaveRequest response:^(BOOL success, id response, NSError *error) {
        GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
        respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
    
}

@end
