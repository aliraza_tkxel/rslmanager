//
//  UserProfileManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserProfileManager.h"
#import "FetchUserProfileRequestObject.h"
#import "UpdateProfileRequestObject.h"
#import "UpdateUserProfileResponseObject.h"
@implementation UserProfileManager

-(void) fetchUserProfileForId:(int) userId
            withResponseBlock:(ManagerResponseBlock) responseBlock
{
    FetchUserProfileRequestObject *requestObj = [FetchUserProfileRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_PROFILE_URL parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
    {
        FetchUserProfileResponseObject * respObj = (FetchUserProfileResponseObject *) [self.networkManager serializeObject:[FetchUserProfileResponseObject class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
}

-(void) updateUserProfileWithPersonalInformation:(UserPersonalInfoModel *) personalInfo
                                  andContactInfo:(UserContactInfoModel *) contactInfo
                               withResponseBlock:(ManagerResponseBlock) responseBlock
{
    UpdateProfileRequestObject *requestObj = [UpdateProfileRequestObject new];
    requestObj.contactDetail = [self createContactRequestWithInfoModel:contactInfo];
    requestObj.personalDetail = [self createPersonalInfoReqWithInfoModel:personalInfo];
    NSDictionary * profile = [self.networkManager deserializeDictionaryFromObject:requestObj];
    
    [self.networkManager sendPOST:SERVICE_UPDATE_PROFILE_URL parameter:profile response:^(BOOL success, id response, NSError *error) {
        UpdateUserProfileResponseObject * respObj = (UpdateUserProfileResponseObject *) [self.networkManager serializeObject:[UpdateUserProfileResponseObject class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
}


#pragma mark -Data Population

-(UpdateContactInfoRequestObject *) createContactRequestWithInfoModel:(UserContactInfoModel *) infoModel{
    UpdateContactInfoRequestObject *reqObj =  [UpdateContactInfoRequestObject new];
    reqObj.workPhone = infoModel.workPhone;
    reqObj.workMobile = infoModel.workMobile;
    reqObj.workEmail = infoModel.workEmail;
    reqObj.mobile = infoModel.mobile;
    reqObj.homePhone = infoModel.homePhone;
    reqObj.homeEmail = infoModel.homeEmail;
    reqObj.addressLine1 = infoModel.addressLine1;
    reqObj.addressLine2 = infoModel.addressLine2;
    reqObj.addressLine3 = infoModel.addressLine3;
    reqObj.postalTown = infoModel.postalTown;
    reqObj.county = infoModel.county;
    reqObj.emergencyContactPerson = infoModel.emergencyContactPerson;
    reqObj.emergencyContactNumber = infoModel.emergencyContactNumber;
    reqObj.emergencyContactRelationship = infoModel.emergencyContactRelationship;
    reqObj.mobilePersonal = infoModel.mobilePersonal;
    reqObj.postalCode = infoModel.postalCode;
    return reqObj;
    
}

-(UpdatePersonalInfoRequestObject *) createPersonalInfoReqWithInfoModel:(UserPersonalInfoModel *) infoModel{
    UpdatePersonalInfoRequestObject *reqObj =  [UpdatePersonalInfoRequestObject new];
    reqObj.firstName = infoModel.firstName;
    reqObj.lastName = infoModel.lastName;
    reqObj.employeeId = [NSNumber numberWithInt:LoggedInUserId];
    reqObj.dob = [DateUtilities getDateStringFromDate:infoModel.dateOfBirth
                                         withFormat:kDateFormatDefaultStamp];
    reqObj.gender = infoModel.gender;
    reqObj.maritalStatus = infoModel.maritalStatusId;
    reqObj.ethnicity = infoModel.ethnicityId;
    reqObj.religion = infoModel.religionId;
    reqObj.sexualOrientation = infoModel.sexualOrientationId;
    return reqObj;
    
}

@end
