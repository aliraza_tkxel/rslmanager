//
//  UserProfileManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractBaseManager.h"
@interface UserProfileManager : AbstractBaseManager

-(void) fetchUserProfileForId:(int) userId
            withResponseBlock:(ManagerResponseBlock) responseBlock;

-(void) updateUserProfileWithPersonalInformation:(UserPersonalInfoModel *) personalInfo
                                  andContactInfo:(UserContactInfoModel *) contactInfo
                               withResponseBlock:(ManagerResponseBlock) responseBlock;

@end
