//
//  AbstractBaseManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkManager.h"
typedef void (^ManagerResponseBlock) (BOOL success, id response, NSString *error);
@interface AbstractBaseManager : NSObject
@property (copy, nonatomic) NetworkManager * networkManager;
@end
