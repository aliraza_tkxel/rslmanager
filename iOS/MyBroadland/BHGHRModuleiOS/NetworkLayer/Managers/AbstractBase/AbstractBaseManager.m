//
//  AbstractBaseManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"

@implementation AbstractBaseManager
- (id)init
{
    self = [super init];
    if (self)
    {
        _networkManager = [NetworkManager sharedInstance];
    }
    return self;
}

@end
