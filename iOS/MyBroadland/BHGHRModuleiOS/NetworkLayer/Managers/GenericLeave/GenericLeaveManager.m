//
//  GenericLeaveManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "GenericLeaveManager.h"
#import "GenericLeaveRequest.h"
#import "GenericLeaveResponse.h"
#import "FetchUserProfileRequestObject.h"
#import "FetchWorkingPatternResponseObject.h"
@implementation GenericLeaveManager

-(void) recordLeaveForModel:(GenericLeaveModel *) model
                    andType:(NSString *) leaveType
          withResponseBlock:(ManagerResponseBlock)responseBlock{
    
    GenericLeaveRequest *request = [GenericLeaveRequest new];
    request = [self configureRequestForModel:model withType:leaveType];
    if([request.holType isEqualToString:kHolTypeBothFullDay] || [request.holType isEqualToString:kHolTypeStartFullEndHalf]||[request.holType isEqualToString:kHolTypeStartHalfEndFull]||[request.holType isEqualToString:kHolTypeSingleFull]
       ||[request.holType isEqualToString:kHolTypeSingleMorning]||[request.holType isEqualToString:kHolTypeSingleAfternoon]){
        NSDictionary * leaveRequest = [self.networkManager deserializeDictionaryFromObject:request];
        
        [self.networkManager sendPOST:SERVICE_RECORD_GENERIC_LEAVE parameter:leaveRequest response:^(BOOL success, id response, NSError *error) {
            GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
            respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
            responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
        }];
    }
    else{
        responseBlock(NO,[NSNull null], kValidationInvalidHolTypeCombination);
    }
}

-(GenericLeaveRequest *) configureRequestForModel:(GenericLeaveModel *) model
                                         withType:(NSString *) leaveType{
    GenericLeaveRequest *request = [GenericLeaveRequest new];
    request.employeeId = LoggedInUserId;
    request.holType = [model getHolidayType];
    if([leaveType isEqualToString:kLeaveNatureBirthday]){
        request.natureId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:leaveType
                                                                            forLookUpType:LookUpTypeLeaveNature];
        request.duration = model.duration;
        request.startDate = [DateUtilities getDateStringFromDate:model.startDate
                                                      withFormat:kDateFormatDefaultStamp];
        request.endDate = [DateUtilities getDateStringFromDate:model.endDate
                                                    withFormat:kDateFormatDefaultStamp];
        return  request;
    }
    
    
    request.startDate = [DateUtilities getDateStringFromDate:model.startDate
                                                  withFormat:kDateFormatDefaultStamp];
    if([leaveType isEqualToString:kLeaveNatureAnnual] || [leaveType isEqualToString:kLeaveNatureSickness] || [leaveType isEqualToString:kLeaveNaturePersonal]){
        request.endDate = [DateUtilities getDateStringFromDate:model.endDate
                                                    withFormat:kDateFormatDefaultStamp];
        request.duration = model.duration;
    }
    
    if([leaveType isEqualToString:kLeaveNatureSickness]){
        request.reason = model.reasonDescription;
        request.reasonId = [NSNumber numberWithInt:model.reasonId];
    }
    
    request.recordedBy = LoggedInUserId;
    request.notes = model.notes;
    
    if([leaveType isEqualToString:kLeaveNatureAbsence]){
        request.natureId = [NSNumber numberWithInt:model.reasonId];
    }
    else{
        request.natureId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:leaveType
                                                                            forLookUpType:LookUpTypeLeaveNature];
    }
    if([leaveType isEqualToString:kLeaveNatureSickness]){
        request.employeeId = [model.selectedStaffMemberId intValue];
        if(!isEmpty(model.anticipatedReturnDate)){
            request.anticipatedReturnDate = [DateUtilities getDateStringFromDate:model.anticipatedReturnDate
                                                                      withFormat:kDateFormatDefaultStamp];
        }
    }
    return request;
}

-(void) fetchUserWorkingPatternForId:(int)userId
                   withResponseBlock:(ManagerResponseBlock)responseBlock{
    FetchUserProfileRequestObject *requestObj = [FetchUserProfileRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_WORKING_PATTERN parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
          FetchWorkingPatternResponseObject* respObj = (FetchWorkingPatternResponseObject *) [self.networkManager serializeObject:[FetchWorkingPatternResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

@end
