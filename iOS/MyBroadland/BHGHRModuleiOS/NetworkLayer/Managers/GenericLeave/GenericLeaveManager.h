//
//  GenericLeaveManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "GenericLeaveModel.h"
@interface GenericLeaveManager : AbstractBaseManager
-(void) recordLeaveForModel:(GenericLeaveModel *) model
                    andType:(NSString *) leaveType
          withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) fetchUserWorkingPatternForId:(int) userId
            withResponseBlock:(ManagerResponseBlock) responseBlock;
@end
