//
//  AuthenticationManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractBaseManager.h"

@interface AuthenticationManager : AbstractBaseManager
-(void) loginWithUserName:(NSString *) userName
              andPassword:(NSString *)password
              andResponse: (ManagerResponseBlock)responseBlock;
@end
