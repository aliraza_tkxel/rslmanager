//
//  AuthenticationManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AuthenticationManager.h"
#import "AuthenticationRequestObject.h"
#import "AuthenticationResponseObj.h"
@implementation AuthenticationManager

-(void) loginWithUserName:(NSString *) userName
              andPassword:(NSString *)password
              andResponse: (ManagerResponseBlock)responseBlock
{
    if(userName.length<3 || password.length<6){
        responseBlock(NO,[NSNull null], [GeneralUtilities createErrorWithDomain:kDomainAuthentication andMessage:kValidationInvalidParamsBodyLogin].localizedDescription);
        return;
    }
    AuthenticationRequestObject *obj = [AuthenticationRequestObject new];
    obj.userName = userName;
    obj.password = [GeneralUtilities base64EncodeString:password];
    [self.networkManager sendPOST:SERVICE_LOGIN_URL
                        parameter:[obj dictionaryValue]
                         response:^(BOOL success, id response, NSError *error) {
        AuthenticationResponseObj * authResp = (AuthenticationResponseObj *) [self.networkManager serializeObject:[AuthenticationResponseObj class] FromJson:response];
        responseBlock(success,authResp, [GeneralUtilities getErrorDescriptionForError:error]);
        
    }];
}

@end
