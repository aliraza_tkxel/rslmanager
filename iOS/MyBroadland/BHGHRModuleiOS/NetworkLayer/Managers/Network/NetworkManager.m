//
//  NetworkManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "NetworkManager.h"
#import <Mantle.h>
#import <Crashlytics/Crashlytics.h>
@implementation NetworkManager


+ (id)sharedInstance
{
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}


#pragma mark - Init

- (id)init
{
    self = [super init];
    if (self)
    {
        _operationManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        _operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        _operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

#pragma mark - Network Requests

- (void)sendPOST:(NSString *)url parameter:(NSDictionary *)param response:(NetworkResponseBlock)block
{
    NSError *jsonError;
    CLS_LOG(@"POST: %@\nparam: %@", url, [GeneralUtilities collectionToString:param error:&jsonError]);
    
    //check internet is available or not
    if (IS_NETWORK_AVAILABLE)
    {
        //if internet is available then call api
        NSURL *urlPath = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,url]];
        [_operationManager POST:urlPath.absoluteString parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSError *error;
            CLS_LOG(@"response: %@", [GeneralUtilities collectionToString:responseObject
                                                                 error:&error]);
            block (YES, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            DError(@"response: %@", error);
            block (NO, nil, error);
        }];
        
        
    }
    else
    {
        block (NO, nil, [GeneralUtilities createErrorWithDomain:kDomainInternetConnection andMessage:kInternetNotAvailable]);
    }
}

- (void)sendGET:(NSString *)url parameter:(NSDictionary *)param response:(NetworkResponseBlock)block
{
    NSError *jsonError;
    CLS_LOG(@"GET: %@\nparam: %@", url, [GeneralUtilities collectionToString:param error:&jsonError]);
    
    //check internet is available or not
    if (IS_NETWORK_AVAILABLE)
    {
        //if internet is available then call api
        NSURL *urlPath = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,url]];
        [_operationManager GET:urlPath.absoluteString parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSError *error;
            CLS_LOG(@"response: %@", [GeneralUtilities collectionToString:responseObject
                                                                 error:&error]);
            block (YES, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            DError(@"response: %@", error);
            block (NO, nil, error);
        }];
    }
    else
    {
        block (NO, nil, [GeneralUtilities createErrorWithDomain:kDomainInternetConnection andMessage:kInternetNotAvailable]);
    }
}



#pragma mark - Response

// Get Object array from Json Array
- (NSArray *)serializeObject:(Class)class FromJsonArray:(NSArray *)json
{
    NSError *error;
    NSArray *array = [MTLJSONAdapter modelsOfClass:class fromJSONArray:json error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    return array;
}

// Get Object from Json Dictionary
- (id)serializeObject:(Class)class FromJson:(NSDictionary *)json
{
    NSError *error;
    id typeObject = [MTLJSONAdapter modelOfClass:class fromJSONDictionary:json error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    return typeObject;
}



// Get Collection from Object
- (NSDictionary *)deserializeDictionaryFromObject:(id)object
{
    NSError *error;
    NSDictionary *dic = [MTLJSONAdapter JSONDictionaryFromModel:object error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    return dic;
}

// Get Json from Object
- (NSString *)deserializeJsonFromObject:(id)object
{
    NSError *error;
    NSDictionary *dic = [MTLJSONAdapter JSONDictionaryFromModel:object error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    NSString * json = [GeneralUtilities collectionToString:dic error:&error];
    return json;
}

// Get Dictionary array from object array
- (NSArray*)deserializeCollectionFromClasses:(NSArray *)objectArray
{
    NSError *error;
    NSArray *arr = [MTLJSONAdapter JSONArrayFromModels:objectArray error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    return arr;
}

// Get Json from object array
- (NSString *)deserializeJsonFromClasses:(NSArray *)objectArray
{
    NSError *error;
    NSArray *arr = [MTLJSONAdapter JSONArrayFromModels:objectArray error:&error];
    if (error)
    {
        DError(@"Couldn't convert app infos JSON to  models: %@", error);
        return nil;
    }
    NSString * json = [GeneralUtilities collectionToString:arr error:&error];
    return json;
}
@end
