//
//  NetworkManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
typedef void (^NetworkResponseBlock) (BOOL success, id response, NSError *error);
@interface NetworkManager : NSObject
@property (strong, nonatomic) AFHTTPSessionManager *operationManager;
+ (id)sharedInstance;
- (NSArray *)serializeObject:(Class)class FromJsonArray:(NSArray *)json;
- (id)serializeObject:(Class)class FromJson:(NSDictionary *)json;
- (NSDictionary *)deserializeDictionaryFromObject:(id)object;
- (NSString *)deserializeJsonFromObject:(id)object;
- (NSArray*)deserializeCollectionFromClasses:(NSArray *)objectArray;
- (NSString *)deserializeJsonFromClasses:(NSArray *)objectArray;

- (void)sendPOST:(NSString *)url parameter:(NSDictionary *)param response:(NetworkResponseBlock)block;
- (void)sendGET:(NSString *)url parameter:(NSDictionary *)param response:(NetworkResponseBlock)block;


@end
