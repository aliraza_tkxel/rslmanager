//
//  LeavesApprovalManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeavesApprovalManager.h"
#import "AnnualLeaveDaysTakenRequestObject.h"
#import "AnnualLeavesDaysTakenResponseObject.h"
#import "MyTeamDaysRequestedResponseModel.h"
#import "LeaveApprovalRequest.h"
#import "GenericLeaveResponse.h"
#import "AbsenceLeaveManager.h"
#import "FetchAbsenceLeavesListResponse.h"

@implementation LeavesApprovalManager

#pragma mark - Visible Methods
-(void) fetchPendingLeavesForUser:(int) userId
                          forYear: (NSString *) year
                     andLeaveType:(InAppLeaveType) leaveTypeConst
                     andIsForTeam:(BOOL) teamList
                withResponseBlock:(ManagerResponseBlock)responseBlock{
    if(!teamList){
        if(leaveTypeConst == InAppLeaveTypeAnnual){
            [self fetchAnnualLeavesDaysRequestedForUserId:userId forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];

        }
        else if (leaveTypeConst == InAppLeaveTypeAbsence){
            [self fetchAbsenceRequestsForUserId:userId forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if (leaveTypeConst == InAppLeaveTypePersonalDay){
            [self fetchMyPersonalLeaves:userId forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypeBirthday){
            [self fetchBirthDaysRequestedForUserId:userId forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypeTOIL){
            [self fetchTOILRequestsForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
    }
    else{
        if(leaveTypeConst == InAppLeaveTypeAnnual){
            [self fetchTeamAnnualLeavesDaysRequestedForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypeAbsence){
            [self fetchTeamAbsenceDaysRequestedForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypePersonalDay){
            [self fetchTeamPersonalLeavesDaysRequestedForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypeBirthday){
            [self fetchTeamBirthDaysRequestedForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
        else if(leaveTypeConst == InAppLeaveTypeTOIL){
            [self fetchTeamTOILRequestedForUserId:userId withResponseBlock:^(BOOL success, id response, NSString *error) {
                responseBlock(success,response, error);
            }];
        }
    }
    
}


-(void) takeActionForLeave:(NSNumber *) absenceHistoryId
              withActionId:(NSNumber *) actionId
                  andNotes:(NSString *)notes
          andResponseBlock:(ManagerResponseBlock)responseBlock{
    
    LeaveApprovalRequest *reqObj = [LeaveApprovalRequest new];
    reqObj.actionBy = LoggedInUserId;
    reqObj.notes = notes;
    reqObj.actionId = actionId;
    reqObj.absenceHistoryId = absenceHistoryId;
    NSDictionary * reqObjDict = [self.networkManager deserializeDictionaryFromObject:reqObj];
    
    [self.networkManager sendPOST:SERVICE_LEAVE_APPROVAL parameter:reqObjDict response:^(BOOL success, id response, NSError *error) {
        GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
        respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
}

#pragma mark - Internal Methods

-(void) fetchAnnualLeavesDaysRequestedForUserId:(int) userId
                                        forYear: (NSString *) year
                              withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_DAYS_REQUESTED_ANNUAL
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         AnnualLeavesDaysTakenResponseObject * respObj = (AnnualLeavesDaysTakenResponseObject *) [self.networkManager serializeObject:[AnnualLeavesDaysTakenResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchTeamAnnualLeavesDaysRequestedForUserId:(int) userId
                                  withResponseBlock:(ManagerResponseBlock)responseBlock{
    AnnualLeaveDaysTakenRequestObject *requestObj = [AnnualLeaveDaysTakenRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_DAYS_REQUESTED_ANNUAL_TEAM
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MyTeamDaysRequestedResponseModel * respObj = (MyTeamDaysRequestedResponseModel *) [self.networkManager serializeObject:[MyTeamDaysRequestedResponseModel class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchAbsenceRequestsForUserId:(int) userId forYear: (NSString *) year withResponseBlock:(ManagerResponseBlock) responseBlock{
    AbsenceLeaveManager *manager = [AbsenceLeaveManager new];
    [manager fetchAbsenceLeavesListForUserId:LoggedInUserId
                                     forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
        FetchAbsenceLeavesListResponse * respObj = (FetchAbsenceLeavesListResponse *) response;
        if(success){
            if(!isEmpty(respObj.absentees)){
                NSPredicate *aToZPredicate = [NSPredicate predicateWithFormat:@"status == %@",kLeaveStatusPending];
                NSArray *filteredDataArray = [respObj.absentees filteredArrayUsingPredicate:aToZPredicate];
                respObj.absentees = [[NSArray alloc] initWithArray:filteredDataArray];
            }
        }
        responseBlock (success,respObj,error);
    }];

}

-(void) fetchTOILRequestsForUserId:(int) userId withResponseBlock:(ManagerResponseBlock) responseBlock{
    AbsenceLeaveManager *manager = [AbsenceLeaveManager new];
    [manager fetchTOILLeavesListForUserId:LoggedInUserId withResponseBlock:^(BOOL success, id response, NSString *error) {
        FetchAbsenceLeavesListResponse * respObj = (FetchAbsenceLeavesListResponse *) response;
        if(success){
            if(!isEmpty(respObj.absentees)){
                NSPredicate *aToZPredicate = [NSPredicate predicateWithFormat:@"status == %@",kLeaveStatusPending];
                NSArray *filteredDataArray = [respObj.absentees filteredArrayUsingPredicate:aToZPredicate];
                respObj.absentees = [[NSArray alloc] initWithArray:filteredDataArray];
            }
        }
        responseBlock (success,respObj,error);
    }];
    
}



-(void) fetchTeamAbsenceDaysRequestedForUserId:(int) userId
                                  withResponseBlock:(ManagerResponseBlock)responseBlock{
    AnnualLeaveDaysTakenRequestObject *requestObj = [AnnualLeaveDaysTakenRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_TEAM_ABSENCE_LIST
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MyTeamDaysRequestedResponseModel * respObj = (MyTeamDaysRequestedResponseModel *) [self.networkManager serializeObject:[MyTeamDaysRequestedResponseModel class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchTeamTOILRequestedForUserId:(int) userId
                             withResponseBlock:(ManagerResponseBlock)responseBlock{
    AnnualLeaveDaysTakenRequestObject *requestObj = [AnnualLeaveDaysTakenRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_TEAM_TOIL_LIST
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MyTeamDaysRequestedResponseModel * respObj = (MyTeamDaysRequestedResponseModel *) [self.networkManager serializeObject:[MyTeamDaysRequestedResponseModel class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchMyPersonalLeaves:(int) userId forYear: (NSString *) year
                       withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_PERSONAL_LEAVE
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         AnnualLeavesDaysTakenResponseObject * respObj = (AnnualLeavesDaysTakenResponseObject *) [self.networkManager serializeObject:[AnnualLeavesDaysTakenResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
    
}

-(void) fetchTeamPersonalLeavesDaysRequestedForUserId:(int) userId
                                  withResponseBlock:(ManagerResponseBlock)responseBlock{
    AnnualLeaveDaysTakenRequestObject *requestObj = [AnnualLeaveDaysTakenRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_PERSONAL_LEAVE_TEAM
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MyTeamDaysRequestedResponseModel * respObj = (MyTeamDaysRequestedResponseModel *) [self.networkManager serializeObject:[MyTeamDaysRequestedResponseModel class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchBirthDaysRequestedForUserId:(int) userId
                                 forYear: (NSString *) year
                             withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_BDAY_LEAVE
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         AnnualLeavesDaysTakenResponseObject * respObj = (AnnualLeavesDaysTakenResponseObject *) [self.networkManager serializeObject:[AnnualLeavesDaysTakenResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchTeamBirthDaysRequestedForUserId:(int) userId
                             withResponseBlock:(ManagerResponseBlock)responseBlock{
    AnnualLeaveDaysTakenRequestObject *requestObj = [AnnualLeaveDaysTakenRequestObject new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_BDAY_LEAVE_TEAM
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MyTeamDaysRequestedResponseModel * respObj = (MyTeamDaysRequestedResponseModel *) [self.networkManager serializeObject:[MyTeamDaysRequestedResponseModel class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

@end
