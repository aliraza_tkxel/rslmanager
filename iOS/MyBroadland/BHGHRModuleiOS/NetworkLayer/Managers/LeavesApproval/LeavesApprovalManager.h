//
//  LeavesApprovalManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "DaysTakenModel.h"
@interface LeavesApprovalManager : AbstractBaseManager
-(void) fetchPendingLeavesForUser:(int) userId
                          forYear: (NSString *) year
                     andLeaveType:(InAppLeaveType) leaveTypeConst
                     andIsForTeam:(BOOL) teamList
                withResponseBlock:(ManagerResponseBlock)responseBlock;

-(void) takeActionForLeave:(NSNumber *) absenceHistoryId
              withActionId:(NSNumber *) actionId
                  andNotes:(NSString *)notes
          andResponseBlock:(ManagerResponseBlock)responseBlock;
@end
