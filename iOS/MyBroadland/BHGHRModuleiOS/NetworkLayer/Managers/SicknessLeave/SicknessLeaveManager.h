//
//  SicknessLeaveManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "GenericLeaveModel.h"
#import "AmendSicknessRequestObject.h"
@interface SicknessLeaveManager : AbstractBaseManager
-(void) recordSickLeaveWithModel:(GenericLeaveModel *) model
             withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) fetchSicknessLeavesListForUserId:(int) userId
                          withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) amendExistingSicknessObjectWith:(AmendSicknessRequestObject*)reqObj
                      withResponseBlock:(ManagerResponseBlock)responseBlock;
@end
