//
//  SicknessLeaveManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SicknessLeaveManager.h"
#import "GenericLeaveManager.h"
#import "FetchSicknessListResponse.h"
#import "SicknessLeaveListingRequest.h"
#import "GenericLeaveResponse.h"
@implementation SicknessLeaveManager
-(void) recordSickLeaveWithModel:(GenericLeaveModel *) model
             withResponseBlock:(ManagerResponseBlock)responseBlock{
    GenericLeaveManager *mgr = [GenericLeaveManager new];
    [mgr recordLeaveForModel:model
                     andType:kLeaveNatureSickness
           withResponseBlock:^(BOOL success, id response, NSString *error)
     {
         responseBlock(success,response,error);
     }];
}

-(void) fetchSicknessLeavesListForUserId:(int)userId
                        withResponseBlock:(ManagerResponseBlock)responseBlock{
    SicknessLeaveListingRequest *requestObj = [SicknessLeaveListingRequest new];
    requestObj.employeeId = userId;
    
    [self.networkManager sendGET:SERVICE_FETCH_SICKNESS_LIST
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         FetchSicknessListResponse * respObj = (FetchSicknessListResponse *) [self.networkManager serializeObject:[FetchSicknessListResponse class] FromJson:response];
         [self insertLookUpsForResponse:respObj];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];

}


-(void) amendExistingSicknessObjectWith:(AmendSicknessRequestObject*)reqObj
                       withResponseBlock:(ManagerResponseBlock)responseBlock{
    NSDictionary * leaveRequest = [self.networkManager deserializeDictionaryFromObject:reqObj];
    
    [self.networkManager sendPOST:SERVICE_AMEND_SICKNESS parameter:leaveRequest response:^(BOOL success, id response, NSError *error) {
        GenericLeaveResponse * respObj = [[GenericLeaveResponse alloc] init];
        respObj = (GenericLeaveResponse *) [self.networkManager serializeObject:[GenericLeaveResponse class] FromJson:response];
        responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
    }];
}

-(void) insertLookUpsForResponse:(FetchSicknessListResponse *) response{
    [[LookUpsDataManager sharedInstance] insertBankHolidays:response.bankHolidays];
    [[LookUpsDataManager sharedInstance] insertSickLeaveReasons:response.absenceReasons];
}


@end
