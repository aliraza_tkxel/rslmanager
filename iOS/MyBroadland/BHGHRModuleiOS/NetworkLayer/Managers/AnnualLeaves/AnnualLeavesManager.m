//
//  AnnualLeavesManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeavesManager.h"
#import "GenericLeaveRequest.h"
#import "FetchAnnualLeaveStatsRequest.h"
#import "FetchAnnualLeaveStatsResponse.h"
#import "AnnualLeaveDaysTakenRequestObject.h"
#import "AnnualLeavesDaysTakenResponseObject.h"
#import "GenericLeaveManager.h"
#import "GenericLeaveResponse.h"
@implementation AnnualLeavesManager

-(void) fetchAnnualLeaveStatsForUserId:(int)userId
                               forYear: (NSString *) yearString
                     withResponseBlock:(ManagerResponseBlock)responseBlock
{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(yearString)) {
        requestObj.fiscalYearDate = yearString;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    [self.networkManager sendPOST:SERVICE_FETCH_ANNUAL_LEAVE_STATS
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         FetchAnnualLeaveStatsResponse * respObj = (FetchAnnualLeaveStatsResponse *) [self.networkManager serializeObject:[FetchAnnualLeaveStatsResponse class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchAnnualLeavesDaysTakenForUserId:(int) userId
                                    forYear: (NSString *) year
                          withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_DAYS_TAKEN
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         AnnualLeavesDaysTakenResponseObject * respObj = (AnnualLeavesDaysTakenResponseObject *) [self.networkManager serializeObject:[AnnualLeavesDaysTakenResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchAnnualLeavesDaysRequestedForUserId:(int) userId
                                        forYear: (NSString *) year
                          withResponseBlock:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_DAYS_REQUESTED_ANNUAL
                       parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         AnnualLeavesDaysTakenResponseObject * respObj = (AnnualLeavesDaysTakenResponseObject *) [self.networkManager serializeObject:[AnnualLeavesDaysTakenResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}


-(void) recordAnnualLeaveModel:(GenericLeaveModel *) model
                  withResponseBlock:(ManagerResponseBlock)responseBlock{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [mgr recordLeaveForModel:model
                     andType:kLeaveNatureAnnual
           withResponseBlock:^(BOOL success, id response, NSString *error)
     {
         responseBlock(success,response,error);
     }];
}


-(void) recordBirthdayLeaveForUserWithModel:(GenericLeaveModel *) model
                           andResponseBlock:(ManagerResponseBlock)responseBlock{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [mgr recordLeaveForModel:model
                     andType:kLeaveNatureBirthday
           withResponseBlock:^(BOOL success, id response, NSString *error)
     {
         responseBlock(success,response,error);
     }];
}
-(void) recordPersonalLeaveWithModel:(GenericLeaveModel *) model
             withResponseBlock:(ManagerResponseBlock)responseBlock{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [mgr recordLeaveForModel:model
                     andType:kLeaveNaturePersonal
           withResponseBlock:^(BOOL success, id response, NSString *error)
     {
         responseBlock(success,response,error);
     }];
}


@end
