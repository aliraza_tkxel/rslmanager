//
//  AnnualLeavesManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "GenericLeaveModel.h"

@interface AnnualLeavesManager : AbstractBaseManager
-(void) fetchAnnualLeaveStatsForUserId:(int)userId
                               forYear: (NSString *) yearString
                     withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) fetchAnnualLeavesDaysTakenForUserId:(int) userId
                                    forYear: (NSString *) year
                          withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordAnnualLeaveModel:(GenericLeaveModel *) model
             withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) fetchAnnualLeavesDaysRequestedForUserId:(int) userId
                                        forYear: (NSString *) year
                              withResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordBirthdayLeaveForUserWithModel:(GenericLeaveModel *) model
                           andResponseBlock:(ManagerResponseBlock)responseBlock;
-(void) recordPersonalLeaveWithModel:(GenericLeaveModel *) model
                   withResponseBlock:(ManagerResponseBlock)responseBlock;
@end
