//
//  MainDashboardNetworkManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MainDashboardNetworkManager.h"

@implementation MainDashboardNetworkManager

-(void) fetchMainDashboardStatsForUserId:(int)userId
                                 forYear: (NSString *) year
                            withResponse:(ManagerResponseBlock)responseBlock{
    GeneralUserOperationRequest *requestObj = [GeneralUserOperationRequest new];
    requestObj.employeeId = userId;
    if (!isEmpty(year)) {
        requestObj.fiscalYearDate = year;
    }
    else {
        requestObj.fiscalYearDate = [[UserModel sharedInstance] userObject].leaveYearStartString;
    }
    
    [self.networkManager sendPOST:SERVICE_FETCH_MAIN_DASHBOARD_STATS parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
         MainDashboardStatsResponseObject * respObj = (MainDashboardStatsResponseObject *) [self.networkManager serializeObject:[MainDashboardStatsResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];
}

-(void) fetchMainDashboardTrainingDataForUserId:(int)userId
                                   withResponse:(ManagerResponseBlock)responseBlock{
    FetchUserProfileRequestObject *requestObj = [FetchUserProfileRequestObject new];
    requestObj.employeeId = userId;
    [self.networkManager sendGET:SERVICE_FETCH_MAIN_DASHBOARD_TRAININGS parameter:[requestObj dictionaryValue]
                        response:^(BOOL success, id response, NSError *error)
     {
        MainDashboardTrainingResponseObject* respObj = (MainDashboardTrainingResponseObject *) [self.networkManager serializeObject:[MainDashboardTrainingResponseObject class] FromJson:response];
         responseBlock(success,respObj, [GeneralUtilities getErrorDescriptionForError:error]);
     }];

    
}

@end
