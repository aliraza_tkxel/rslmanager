//
//  MainDashboardNetworkManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbstractBaseManager.h"
#import "FetchUserProfileRequestObject.h"
#import "MainDashboardStatsResponseObject.h"
#import "MainDashboardStatsModel.h"
#import "MainDashboardTrainingResponseObject.h"
#import "MainDashboardTrainingModel.h"
@interface MainDashboardNetworkManager : AbstractBaseManager
-(void) fetchMainDashboardStatsForUserId:(int)userId
                                 forYear: (NSString *) year
                            withResponse:(ManagerResponseBlock)responseBlock;
-(void) fetchMainDashboardTrainingDataForUserId:(int) userId
                                   withResponse:(ManagerResponseBlock) responseBlock;
@end
