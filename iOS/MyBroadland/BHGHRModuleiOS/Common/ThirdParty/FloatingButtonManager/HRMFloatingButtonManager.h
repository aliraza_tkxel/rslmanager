//
//  HRMFloatingButtonManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGPlusButtonsView.h"
typedef void (^HRMFloatingButtonManagerResponseBlock) (NSUInteger selectedIndex, BOOL success, NSString *error);
@interface HRMFloatingButtonManager : NSObject
@property (strong, nonatomic) LGPlusButtonsView *plusButtonsViewMain;
+ (id)sharedInstance;
-(void) addFloatingButtonsWithSize:(int)buttonSize
             andButtonDescriptions:(NSArray *)descriptions
                   andButtonImages:(NSArray *) buttonImages
                 forViewController:(UIViewController*) controller
                 withResponseBlock:(HRMFloatingButtonManagerResponseBlock) responseBlock;
-(void) hideButtons;
@end
