//
//  HRMFloatingButtonManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "HRMFloatingButtonManager.h"

@implementation HRMFloatingButtonManager

#pragma mark - Init
+ (id)sharedInstance
{
    static HRMFloatingButtonManager *sharedButtonMgr = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedButtonMgr = [[self alloc] init];
    });
    return sharedButtonMgr;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
        
    }
    return self;
}

#pragma mark - Methods

-(void) hideButtons {
    if(!isEmpty(_plusButtonsViewMain)){
        [_plusButtonsViewMain hideButtonsAnimated:YES completionHandler:nil];
    }
}

-(void) addFloatingButtonsWithSize:(int)buttonSize
             andButtonDescriptions:(NSArray *)descriptions
                   andButtonImages:(NSArray *) buttonImages
                 forViewController:(UIViewController*) controller
                 withResponseBlock:(HRMFloatingButtonManagerResponseBlock) responseBlock{
    
    @try {
        _plusButtonsViewMain = [LGPlusButtonsView plusButtonsViewWithNumberOfButtons:buttonSize+1
                                                             firstButtonIsPlusButton:YES
                                                                       showAfterInit:YES
                                                                       actionHandler:^(LGPlusButtonsView *plusButtonView, NSString *title, NSString *description, NSUInteger index)
                                {
                                    NSLog(@"actionHandler | title: %@, description: %@, index: %lu", title, description, (long unsigned)index);
                                    responseBlock(index,YES,description);
                                    
                                }];
        NSMutableArray *descriptionsArr = [[NSMutableArray alloc] initWithObjects:@"", nil];
        [descriptionsArr addObjectsFromArray:descriptions];
        [_plusButtonsViewMain setDescriptionsTexts:descriptionsArr];
        
        NSMutableArray *titlesArr = [[NSMutableArray alloc] initWithObjects:@"+", nil];
        for(int i = 0 ; i <buttonSize;i++){
            [titlesArr addObject:@""];
        }
        
        [_plusButtonsViewMain setButtonsTitles:titlesArr forState:UIControlStateNormal];
        
        NSMutableArray *bgImgsArr = [[NSMutableArray alloc] initWithObjects:[NSNull new], nil];
        [bgImgsArr addObjectsFromArray:buttonImages];
        [_plusButtonsViewMain setButtonsImages:bgImgsArr
                                      forState:UIControlStateNormal
                                forOrientation:LGPlusButtonsViewOrientationAll];
        
        [_plusButtonsViewMain setButtonsAdjustsImageWhenHighlighted:NO];
        
        
        _plusButtonsViewMain.coverColor = [UIColor clearColor];
        _plusButtonsViewMain.position = LGPlusButtonsViewPositionBottomRight;
        _plusButtonsViewMain.plusButtonAnimationType = LGPlusButtonAnimationTypeRotate;
        
        
        [_plusButtonsViewMain setButtonsAdjustsImageWhenHighlighted:NO];
        [_plusButtonsViewMain setButtonsBackgroundColor:AppThemeColorRed forState:UIControlStateNormal];
        [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted];
        [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted|UIControlStateSelected];
        [_plusButtonsViewMain setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
        [_plusButtonsViewMain setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
        [_plusButtonsViewMain setButtonsTitleFont:[UIFont boldSystemFontOfSize:24.f] forOrientation:LGPlusButtonsViewOrientationAll];
        [_plusButtonsViewMain setButtonsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
        [_plusButtonsViewMain setButtonsLayerShadowOpacity:0.5];
        [_plusButtonsViewMain setButtonsLayerShadowRadius:3.f];
        [_plusButtonsViewMain setButtonsLayerShadowOffset:CGSizeMake(0.f, 2.f)];
        
        [_plusButtonsViewMain setButtonAtIndex:0
                               backgroundColor:AppThemeColorRed
                                      forState:UIControlStateNormal];
        [_plusButtonsViewMain setButtonAtIndex:0
                               backgroundColor:AppThemeColorRed
                                      forState:UIControlStateHighlighted];
        [_plusButtonsViewMain setButtonAtIndex:0
                               backgroundColor:AppThemeColorRed
                                      forState:UIControlStateHighlighted|UIControlStateSelected];
        [_plusButtonsViewMain setButtonAtIndex:0 size:CGSizeMake(56.f, 56.f)
                                forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
        [_plusButtonsViewMain setButtonAtIndex:0 layerCornerRadius:56.f/2.f
                                forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
        [_plusButtonsViewMain setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:40.f]
                                forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
        [_plusButtonsViewMain setButtonAtIndex:0
                                   titleOffset:CGPointMake(0.f, -3.f)
                                forOrientation:LGPlusButtonsViewOrientationAll];
        
        
        for(int i = 1;i<=buttonSize;i++){
            //[_plusButtonsViewMain setButtonAtIndex:i backgroundColor:[UIColor colorWithRed:0.f green:0.7 blue:0.f alpha:1.f] forState:UIControlStateNormal];
            [_plusButtonsViewMain setButtonAtIndex:i backgroundColor:[UIColor colorWithRed:0.f green:0.8 blue:0.f alpha:1.f] forState:UIControlStateHighlighted];
        }
        
        
        [_plusButtonsViewMain setDescriptionsBackgroundColor:[UIColor whiteColor]];
        [_plusButtonsViewMain setDescriptionsTextColor:[UIColor blackColor]];
        [_plusButtonsViewMain setDescriptionsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
        [_plusButtonsViewMain setDescriptionsLayerShadowOpacity:0.25];
        [_plusButtonsViewMain setDescriptionsLayerShadowRadius:1.f];
        [_plusButtonsViewMain setDescriptionsLayerShadowOffset:CGSizeMake(0.f, 1.f)];
        [_plusButtonsViewMain setDescriptionsLayerCornerRadius:6.f forOrientation:LGPlusButtonsViewOrientationAll];
        [_plusButtonsViewMain setDescriptionsContentEdgeInsets:UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f) forOrientation:LGPlusButtonsViewOrientationAll];
        
        for (NSUInteger i=1; i<=buttonSize; i++)
            [_plusButtonsViewMain setButtonAtIndex:i offset:CGPointMake(-6.f, 0.f)
                                    forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            [_plusButtonsViewMain setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -2.f) forOrientation:LGPlusButtonsViewOrientationLandscape];
            [_plusButtonsViewMain setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:32.f] forOrientation:LGPlusButtonsViewOrientationLandscape];
        }
        
        [controller.view addSubview:_plusButtonsViewMain];
    } @catch (NSException *exception) {
        responseBlock(0,NO,exception.reason);
    } @finally {
        
    }
    
    
}

@end
