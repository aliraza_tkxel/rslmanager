//
//  DynamicTableViewCellProtocol.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicCellsConfiguration.h"
@protocol DynamicTableViewCellProtocol <NSObject>

@required
-(void) didChangeValueFor :(DynamicCellsConfiguration *) config;
@optional



@end
