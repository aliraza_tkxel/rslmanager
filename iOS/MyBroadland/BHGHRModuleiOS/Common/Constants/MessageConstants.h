//
//  MessageConstants.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef MessageConstants_h
#define MessageConstants_h

/*Actions*/

#define kAlertTitleConfirmAction            @"Confirm Action"
#define kAlertActionOk                      @"OK"
#define kAlertActionCancel                  @"Cancel"
#define kAlertActionYes                     @"YES"
#define kAlertActionNO                      @"NO"

/*Approval Actions*/

#define kAlertTitlePickAction               @"Select Action"
#define kAlertActionApprove                 @"Approve"
#define kAlertActionDecline                 @"Decline"
#define kAlertActionNoAction                @"No Action"
#define kAlertCancelLeave                   @"Cancel Leave"
#define kAlertMessageApproval               @"Please review the following information and take the appropriate action."
#define kAlertMessageApproveLeave           @"Are you sure you want to approve this leave?"
#define kAlertMessageDeclineLeave           @"Are you sure you want to decline this leave?"
#define kAlertMessageCancelLeave            @"Are you sure you want to cancel this leave?"

/*Response*/

#define kResponseStatusSuccess              @"Success"
#define kResponseStatusError                @"Error"

/*Validation*/

#define kValidationBankHolidayCancelling    @"You cannot modify bank holidays."
#define kValidationInvalidParamsTitle       @"Invalid Parameters"
#define kValidationInvalidParamsBodyLogin   @"Please enter a valid username and password."
#define kValidationInvalidStartDateBody     @"Start date must be earlier than end date."
#define kValidationInvalidEndDateBody       @"End date must be later than start date."
#define kValidationInvalidReturnDateBody       @"Return date must be later than start date."
#define kValidationInvalidStartDatePast     @"Start date must be later than or same as today."
#define kValidationInvalidStartDatePastEarlier     @"Start date must be earlier than or same as today."
#define kValidationSelectStartDateBody      @"Please select start date first."
#define kValidationSelectReason             @"Please select a reason first."
#define kValidationSelectEndDateBody        @"Please select end date."
#define kValidationObjectNotFound           @"Operation failed: No object found."
#define kInternetNotAvailable               @"Failed to connect. Please check your internet connection and try again."
#define kValidationLeaveQuotaExceeded       @"Your requested leave duration exceeds your remaining leave quota of"
#define kValidationDiscussWithManager       @"If you have any concerns, please discuss them with your manager."
#define kValidationLeaveQuotaUnavailable    @"Your remaining leave quota doesn't allow you to perform this operation."
#define kValidationStartDateWeekend         @"Start Date cannot fall on a weekend."
#define kValidationEndDateWeekend           @"End Date cannot fall on a weekend."
#define kValidationReturnDateWeekend           @"Return Date cannot fall on a weekend."
#define kValidationStaffMemberSelection     @"Please select a Staff member first."
#define kValidationCorruptedAbsenceHistory  @"Server couldn't fetch the absence history record for this leave. Please contact the administration."
#define kValidationInvalidDuration          @"Please select valid Start and End time."

#define kValidationBankHolidayStartDate     @"You selected start date is a Bank holiday. Please select another date."
#define kValidationBankHolidayEndDate       @"You selected start date is a Bank holiday. Please select another date."
#define kValidationNotesLimitExceeded       @"Notes can only be 1000 characters long or fewer."

#define kValidationStartTime                @"Please select start time"
#define kValidationEndTime                  @"Please Select end time"
#define kValidationInvalidStartTimePast     @"Start time must be earlier than end time"

#define kValidationNoReportingStaff         @"You don't have any employees reporting to you."
#define kValidationCancelPastLeaves         @"You cannot cancel a leave after the start date has passed."
#define kValidationCancelNonPendingLeave    @"You can cancel only pending leave."
#define kValidationInvalidHolTypeCombination    @"Invalid Combination. Start date must be full day or afternoon AND End date must be morning or full day."

/*Profile Info Validation*/

#define kProfValidationInvalidFirstName        @"Please enter a valid first Name"
#define kProfValidationInvalidLastName         @"Please enter a valid last Name"
#define kProfValidationInvalidDoB              @"Please select your date of birth"
#define kProfValidationInvalidGender           @"Please select your gender"
#define kProfValidationInvalidMaritalStatus    @"Please select your marital status"
#define kProfValidationInvalidEthnicity        @"Please select your ethnicity"
#define kProfValidationInvalidReligion         @"Please select your religion"
#define kProfValidationInvalidSexOrientation   @"Please select your sexual orientation"

#define kProfValidationInvalidWorkTel          @"Please enter a valid telephone number for work"
#define kProfValidationInvalidPersonalMobile          @"Please enter a valid number for personal mobile"
#define kProfValidationInvalidWorkMobile       @"Please enter a valid mobile number for work"
#define kProfValidationInvalidWorkEmail        @"Please enter a valid email for work"
#define kProfValidationInvalidMobile           @"Please enter a valid mobile number"
#define kProfValidationInvalidTelHome          @"Please enter a valid telephone number for home"
#define kProfValidationInvalidEmailHome        @"Please enter a valid email for home"
#define kProfValidationInvalidPostalCode        @"Please enter a valid post code"
#define kProfValidationInvalidAddressLine1     @"Please enter valid data for address line 1"
#define kProfValidationInvalidAddressLine2     @"Please enter valid data for address line 2"
#define kProfValidationInvalidAddressLine3     @"Please enter valid data for address line 3"
#define kProfValidationInvalidCounty           @"Please enter a valid county name"
#define kProfValidationInvalidPostalTown       @"Please enter a valid postal town Name"
#define kProfValidationInvalidEmergencyNumber  @"Please enter a valid emergency number"
#define kProfValidationInvalidEmergencyContact @"Please enter a valid name for emergency contact"
#define kProfValidationInvalidEmergencyContactRel @"Please enter a valid relation for emergency contact"
#define kAddToilInsufficientQuota                 @"Your current TOIL balance does not allow this operation."
/*Proress Ring*/

#define kProgressGeneral                    @"Please Wait..."
#define kProgressValidatingData             @"Validating Data..."
#define kProgressLoggingIn                  @"Logging In......"
#define kProgressFetchingProfile            @"Fetching Profile Info...."
#define kProgressUpdatingProfileInfo        @"Updating Profile Info...."
#define kProgressInsertingData              @"Inserting Data...."
#define kProgressFetchingData               @"Fetching Data...."
#define kProgressUpdatingData               @"Updating Data...."
#define kProgressDeletingData               @"Deleting Data...."
#define kProgressFetchingLeavesStats        @"Fetching Leave Stats..."
#define kProgressFetchingDaysTaken          @"Fetching Days Taken...."
#define kProgressRecordingAnnualLeave       @"Recording Annual Leave...."
#define kProgressRecordingSickLeave         @"Recording Sick Leave...."
#define kProgressAmendingSickLeave          @"Amending Sick Leave...."
#define kProgressFetchingSicknessLeaves     @"Fetching Sickness Leave...."
#define kProgressFetchingAbsenceLeaves      @"Fetching Other Leave...."
#define kProgressRecordingAbsenceLeave      @"Recording Absence Request...."
#define kProgressRecordingBdayLeave         @"Recording Birthday Leave...."
#define kProgressRecordingPersonalDay         @"Recording Personal Day...."
#define kProgressRecordingToil              @"Recording TOIL........"
#define kProgressRecordingInternalMeeting   @"Recording Internal Meeting...."
#define kProgressFetchingDashboardStats     @"Updating Dashboard Stats....."
#define kProgressFetchingTrainingStats      @"Updating Training Stats......"
#define kProgressApplyingAction             @"Applying action...."
#define kProgressFetchingWorkingPattern     @"Fetching Working Pattern...."

/*Sheet Picker Titles*/

#define kSheetPickerTitleDOB                @"Date of Birth"
#define kSheetPickerTitleGender             @"Select Gender"
#define kSheetPickterTitleMaritalStatus     @"Select Marital Status"
#define kSheetPickerTitleEthnicity          @"Select Ethnicity"
#define kSheetPickerTitleReligion           @"Select Religion"
#define kSheetPickerTitleSexuality          @"Select Sexuality"
#define kSheetPickerTitleStartDate          @"Select Start Date"
#define kSheetPickerTitleReturnDate         @"Select Return Date"
#define kSheetPickerTitleAntiReturnDate     @"Select Anticipated Return Date"
#define kSheetPickerTitleEndDate            @"Select End Date"
#define kSheetPickerTitleReason             @"Select Reason"
#define kSheetPickerStaffMember             @"Select Staff Member"

/*Domains*/

#define kDomainAuthentication               @"Authentication"
#define kDomainDashboard                    @"Dashboard"
#define kDomainAnnualLeaves                 @"Annual Leave"
#define kDomainAbsence                      @"Other Leave"
#define kDomainRecordAbsence                @"Record Other Leave"
#define kDomainSickLeaves                   @"Sick Leave"
#define kDomainRecordAnnual                 @"Record Annual Leave"
#define kDomainRecordSickness               @"Record Sickness"
#define kDomainLogout                       @"Logout"
#define kDomainDataOperations               @"Data Operations"
#define kDomainInternetConnection           @"Internet Connection"
#define kDomainDashboard                    @"Dashboard"

/*Confirmation Messages*/

#define kConfirmationBookBdayLeaveMsg       @"Are you sure you want to apply for your birthday leave?"
#define kConfirmationBookPersonalDayMsg     @"Are you sure you want to apply for your personal day?"
#define kConfirmationCancelEditingProfile   @"Are you sure you want to cancel updating profile?"


/*Future build message*/

#define kFeatureUnavailable                 @"This feature will be implemented in the forthcoming builds."

/*User intimation messages*/

#define kNoLeavesFound                      @"No leave found!"
#define kNoWorkingPatternReturned           @"The server did not return any working pattern for this user. Please contact the developers."

#endif /* MessageConstants_h */
