//
//  Macros.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef Macros_h
#define Macros_h
#pragma mark - UIColor Utility Macros
/*!
 @discussion
 Macro to Create an UIColor Object From RGB Values.
 */
#define AppThemeColorGrey [UIColor colorWithRed:((float)122)/255.0 green:((float)124)/255.0 blue:((float)127)/255.0 alpha:1.0]

#define AppThemeColorRed [UIColor colorWithRed:((float)226)/255.0 green:((float)30)/255.0 blue:((float)60)/255.0 alpha:1.0]

#define UIColorFromRGB(rVal,gVal,bVal) [UIColor colorWithRed:((float)rVal)/255.0 green:((float)gVal)/255.0 blue:((float)bVal)/255.0 alpha:1.0]

#pragma mark - Font

#define AppThemeFontWithSize(ptSize)    [UIFont fontWithName:@"HelveticaNeue" size:(float)ptSize]

#pragma mark - NSObjcet Emptiness
/*!
 @discussion
 isEmpty Method Checks Data Integrity. Can be used for any NSObject type instance.
 Particularly usefull for NSString, NSData, NSDictionary, NSArray, NSSet, etc.
 */
#ifdef __OBJC__
static inline BOOL isEmpty(id thing) {
    return thing == nil || thing == [NSNull null]
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSDictionary *)thing count] == 0);
}
#endif

/*In line If Else*/

#ifdef __OBJC__
static inline id conditionallyAssign(id testValue,id fallBackValue) {
    return (!isEmpty(testValue))?testValue:fallBackValue;
}
#endif

#pragma mark - Exception Handling Macros
/*!
 @discussion
 Exception Handling Macros.
 */
#define STARTEXCEPTION @try{
#define ENDEXCEPTION }@catch (NSException * e) {NSLog(@"%s[L:%d] Exception in %@,\n Stacktrace: %@", __PRETTY_FUNCTION__,__LINE__, [e description], [NSThread callStackSymbols]);}

#pragma mark -  System version checker Macros

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])

#pragma mark - Reachability

#define IS_NETWORK_AVAILABLE [Reachability reachabilityWithHostname:@"www.google.com"].isReachable

#pragma mark - Logging
#define kLogsEnabled @"LogsEnabled"
#define DLog(...) NSLog(@"[%d]%s[L:%d] %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);
#define DInfo(...) NSLog(@"[%d] = %s [L:%d]--- %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);
#define DError(...) NSLog(@"[%d] = %s [L:%d]--- %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);

#pragma mark - Singletons
#define LoggedInUser                [UserModel sharedInstance]
#define LoggedInUser_UserObject     [UserModel sharedInstance].userObject
#define LoggedInUserId              [[UserModel sharedInstance] userObject].userId

#endif /* Macros_h */
