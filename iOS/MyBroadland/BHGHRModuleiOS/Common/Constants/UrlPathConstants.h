//
//  UrlPathConstants.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef UrlPathConstants_h
#define UrlPathConstants_h

#if LIVE
#define BASE_URL                            @"https://crm.broadlandhousinggroup.org/RSLHRModuleApi/"
#elif DEV
#define BASE_URL                            @"https://devcrm.broadlandhousinggroup.org/RSLHRModuleApi/"
#elif TEST
#define BASE_URL                            @"https://testcrm.broadlandhousinggroup.org/RSLHRModuleApi/"
#elif UAT
#define BASE_URL                            @"https://uatcrm.broadlandhousinggroup.org/RSLHRModuleApi/"
#elif LIVECLONE
#define BASE_URL                            @"https://livecopy.broadlandhousinggroup.org/RSLHRModuleApi/"
#endif

/*Sign in*/

#define SERVICE_LOGIN_URL                   @"api/iOS/authentication/AuthenticateUser"

/*Dashboard Stats*/

#define SERVICE_FETCH_MAIN_DASHBOARD_STATS  @"api/iOS/Dashboard/GetDashboardStats"
#define SERVICE_FETCH_MAIN_DASHBOARD_TRAININGS  @"api/ios/Trainings/GetEmployeesTrainings"

/*User Profile*/

#define SERVICE_FETCH_PROFILE_URL           @"api/ios/MyDetail/GetMyDetail"
#define SERVICE_UPDATE_PROFILE_URL          @"api/iOS/MyDetail/AmendMyDetail"

/*Annual Leave*/

#define SERVICE_FETCH_ANNUAL_LEAVE_STATS    @"api/iOS/MyDetail/GetLeaveStats"
#define SERVICE_FETCH_DAYS_TAKEN            @"api/ios/Absence/GetLeavesTaken"
#define SERVICE_FETCH_DAYS_REQUESTED_ANNUAL @"api/ios/Absence/GetLeavesRequested"
#define SERVICE_FETCH_DAYS_REQUESTED_ANNUAL_TEAM    @"api/iOS/Absence/GetMyTeamLeavesRequested"
#define SERVICE_FETCH_BDAY_LEAVE           @"api/iOS/Absence/GetBirthDayLeaveRequested"
#define SERVICE_FETCH_BDAY_LEAVE_TEAM      @"api/iOS/Absence/GetMyTeamBirthdayLeavesRequested"
#define SERVICE_FETCH_PERSONAL_LEAVE        @"api/iOS/Absence/GetPersonalDayLeaveRequested"
#define SERVICE_FETCH_PERSONAL_LEAVE_TEAM   @"api/iOS/Absence/GetMyTeamPersonalDayLeavesRequested"

/*Generic Leave*/
#define SERVICE_RECORD_GENERIC_LEAVE        @"api/ios/Absence/RecordAnnualSickLeave"
#define SERVICE_FETCH_WORKING_PATTERN       @"api/iOS/Absence/GetWorkingPattern"

/*Sickness Leave*/

#define SERVICE_FETCH_SICKNESS_LIST         @"api/ios/Absence/GetSicknessAbsence"
#define SERVICE_AMEND_SICKNESS              @"api/ios/Absence/ChangeSicknessLeaveStatus"

/*Absence Leave*/

#define SERVICE_FETCH_TOIL_LIST             @"api/iOS/Absence/GetLeaveOfToil"
#define SERVICE_FETCH_ABSENCE_LIST          @"api/ios/Absence/GetLeaveOfAbsence"
#define SERVICE_RECORD_ABSENCE              @"api/ios/Absence/RecordLeaveOfAbsence"
#define SERVICE_FETCH_TEAM_ABSENCE_LIST     @"api/iOS/Absence/GetMyTeamLeavesOfAbsenceRequested"
#define SERVICE_FETCH_TEAM_TOIL_LIST        @"api/iOS/Absence/GetMyTeamLeavesOfToilRequested"
#define SERVICE_ADD_RECORD_TOIL             @"api/ios/Absence/AddToilRecord"
#define SERVICE_RECORD_INTERNAL_MEETING     @"api/iOS/Absence/AddInternalMeetingRecord_IOS"
/*Leave Approval*/

#define SERVICE_LEAVE_APPROVAL              @"api/iOS/Absence/ChangeLeaveStatus"

#endif /* UrlPathConstants_h */
