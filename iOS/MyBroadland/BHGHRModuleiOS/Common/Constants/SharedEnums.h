//
//  SharedEnums.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef SharedEnums_h
#define SharedEnums_h

typedef NS_ENUM(NSInteger, AnnualLeavesListControllerType) {
    AnnualLeavesListControllerTypeTakenBooked,
    AnnualLeavesListControllerTypePendingApproval
};

typedef NS_ENUM(NSInteger, AnnualSubLeaveType) {
    AnnualSubLeaveTypePersonal = 0 ,
    AnnualSubLeaveTypeBirthday = 1
};
typedef NS_ENUM(NSInteger, SicknessRecordingType) {
    SicknessRecordingTypeNew,
    SicknessRecordingTypeAmendment
};
typedef NS_ENUM(NSInteger, InAppLeaveType) {
    InAppLeaveTypeAnnual,
    InAppLeaveTypeSickness,
    InAppLeaveTypeAbsence,
    InAppLeaveTypeBirthday,
    InAppLeaveTypePersonalDay,
    InAppLeaveTypeTOIL
};
typedef NS_ENUM(NSInteger, UserEmploymentNature) {
    UserEmploymentNatureDaily = 0,
    UserEmploymentNatureHourly = 1
};
/*Look ups*/

typedef NS_ENUM(NSInteger, LookUpType)
{
    LookUpTypeReligion = 0,
    LookUpTypeEthnicity = 1,
    LookUpTypeGender = 2,
    LookUpTypeMaritalStatus = 3,
    LookUpTypeSexualOrientation = 4,
    LookUpTypeLeaveNature = 5,
    LookUpTypeLeaveReason = 6,
    LookUpTypeBankHoliday = 7,
    LookUpTypeSickLeaveReasons = 8,
    LookUpTypeAbsenceLeaveReasons = 9,
    LookUpTypeLeaveActions = 10,
    LookUpTypeMyStaff = 11
};

/*Annual Leave*/

typedef NS_ENUM(NSInteger, LeaveAtomicity)
{
    LeaveAtomicitySingle = 0,
    LeaveAtomicityMultiple = 1
};

typedef NS_ENUM(NSInteger, LeaveDuration)
{
    LeaveDurationFullDay = 0,
    LeaveDurationMorning = 1,
    LeaveDurationEvening = 2
};

/*My Details*/

typedef NS_ENUM(NSInteger, MyDetailsTab) {
    MyDetailsTabPersonal,
    MyDetailsTabContact,
    MyDetailsTabEmergency
};

typedef NS_ENUM(NSInteger, PersonalDayFormField) {
    PersonalDayFormFieldEmployeeName,
    PersonalDayFormFieldStartDate,
    PersonalDayFormFieldDuration,
    PersonalDayFormFieldNotes
};

typedef NS_ENUM(NSInteger, MyDetailsPersonalInfoField) {
    MyDetailsPersonalInfoFieldFirstName,
    MyDetailsPersonalInfoFieldLastName,
    MyDetailsPersonalInfoFieldDOB,
    MyDetailsPersonalInfoFieldGender,
    MyDetailsPersonalInfoFieldMaritalStatus,
    MyDetailsPersonalInfoFieldEthnicity,
    MyDetailsPersonalInfoFieldReligion,
    MyDetailsPersonalInfoFieldSexualOrientation
};

typedef NS_ENUM(NSInteger, MyDetailsContactInfoField) {
    MyDetailsContactInfoFieldTelWork,
    MyDetailsContactInfoFieldMobileWork,
    MyDetailsContactInfoFieldEmailWork,
    MyDetailsContactInfoFieldMobileBCP,
    MyDetailsContactInfoFieldTelHome,
    MyDetailsContactInfoFieldMobilePersonal,
    MyDetailsContactInfoFieldEmailHome,
    MyDetailsContactInfoFieldAddress1,
    MyDetailsContactInfoFieldAddress2,
    MyDetailsContactInfoFieldAddress3,
    MyDetailsContactInfoFieldPostalTown,
    MyDetailsContactInfoFieldPostalCode,
    MyDetailsContactInfoFieldCounty
};

typedef NS_ENUM(NSInteger, MyDetailsEmergencyInfoField) {
    MyDetailsEmergencyInfoFieldContactName,
    MyDetailsEmergencyInfoFieldContactTel,
    MyDetailsEmergencyInfoFieldContactRelation
};

typedef NS_ENUM(NSInteger, DynamicCellsBaseType) {
    DynamicCellsBaseTypeDropDown,
    DynamicCellsBaseTypeDate,
    DynamicCellsBaseTypeFreeText,
    DynamicCellsBaseTypeTime
};

typedef NS_ENUM(NSInteger, DynamicCellsFreeTextFieldType) {
    DynamicCellsFreeTextFieldTypeText,
    DynamicCellsFreeTextFieldTelephone,
    DynamicCellsFreeTextFieldEmail,
    DynamicCellsFreeTextFieldNumeric,
    DynamicCellsFreeTextFieldTypeDecimal
};

/*Record Internal Meeting*/

typedef NS_ENUM (NSInteger , RecordInternalMeetingField){
    RecordInternalMeetingFieldStartDate,
    RecordInternalMeetingFieldStartTime,
    RecordInternalMeetingFieldEndTime,
    RecordInternalMeetingFieldDuration,
    RecordInternalMeetingFieldNotes
};

/*Record Toil*/

typedef NS_ENUM(NSInteger, AddRecordToilScreenType) {
    AddRecordToilScreenTypeTopUp,
    AddRecordToilScreenTypeRecording
};

typedef NS_ENUM(NSInteger, AbsenceAddToilField) {
    AbsenceAddToilFieldStaffMember,
    AbsenceAddToilFieldStartDate,
    AbsenceAddToilFieldStartTime,
    AbsenceAddToilFieldEndTime,
    AbsenceAddToilFieldDuration,
    AbsenceAddToilFieldRecordedBy
};

typedef NS_ENUM(NSInteger, AbsenceRecordToilField) {
    AbsenceRecordToilFieldStartDate,
    AbsenceRecordToilFieldStartTime,
    AbsenceRecordToilFieldDuration,
    AbsenceRecordToilFieldRecordedBy
};

#endif /* SharedEnums_h */
