//
//  UIConstants.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef UIConstants_h
#define UIConstants_h

//RearView Constants

#define kRearViewRevealWidth                        300

//Cell Reuse identifiers

#define kSideMenuCellReuseIdentifier                    @"kSideMenuCellReuseIdentifier"
#define kDashboardGridCellReuseIdentifier               @"kDashboardGridCellReuseIdentifier"
#define kDashboardStatsCellReuseIdentifier              @"kDashboardStatsCellReuseIdentifier"
#define kMyDetailsCellReuseIdentifier                   @"kMyDetailsCellReuseIdentifier"
#define kLeaveListingDetailCellReuseIdentifier          @"kLeaveListingCellReuseIdentifierDetail"
#define kLeaveListingCompactCellReuseIdentifier         @"kLeaveListingCellReuseIdentifierCompact"
#define kSickLeaveTableViewCellReuseIdentifier          @"kSickLeaveTableViewCellReuseIdentifier"
#define kSickLeaveStaffTableViewCellReuseIdentifier     @"kMyStaffSummaryTableViewCellReuseIdentifier"
#define kAbsentLeaveTableViewCellReuseIdentifier        @"kAbsenceListingTableViewCellReuseIdentifier"
#define kLeaveApprovalTableViewCellReuseIdentifier      @"kLeaveApprovalTableViewCellReuseIdentifier"
#define kDynamicDropDownCellReuseIdentifier             @"kDropDownListTableViewCellReuseIdentifier"
#define kDynamicDatePickerCellReuseIdentifier             @"kDatePickerInputTableViewCellReuseIdentifier"
#define kDynamicFreeTextCellReuseIdentifier             @"kFreeTextInputTableViewCellReuseIdentifier"

//Segue Identifiers

//Segue format - SEG_FROM_TO_DESTINATION


#define kSegueFromLoginToDashboard                      @"SEG_LOGIN_TO_DASHBOARD"
#define kSegueFromSideMenuToMyDetails                   @"SEG_SIDEMENU_TO_MYDETAILS"
#define kSegueFromSideMenuToDashboard                   @"SEG_SIDEMENU_TO_DASHBOARD"
#define kSegueFromDashboardToMyDetails                  @"SEG_DASHBOARD_TO_MYDETAILS"
#define kSegueFromSideMenuToAnnualLeavesDashboard       @"SEG_SIDEMENU_TO_ANNUALLEAVEDASHBOARD"
#define kSegueFromDashboardToAnnualLeavesDashboard      @"SEG_DASHBOARD_TO_ANNUALLEAVEDASHBOARD"
#define kSegueFromAnnualDashboardToRecordAnnualLeave    @"SEG_ANNUALLEAVESDASHBOARD_TO_RECORDANNUALLEAVES"
#define kSegueFromSideMenuToRecordSickness              @"SEG_SIDEMENU_TO_RECORD_SICKNESS"
#define kSegueFromDashboardToRecordSickness             @"SEG_DASHBOARD_TO_RECORDSICKNESS"
#define kSegueFromAnnualLeavesDashboardToListing        @"SEG_ANNUAL_LEAVES_DASHBOARD_TO_ANNUAL_LEAVES_LIST"
#define kSegueFromDashboardToAbsenceList                @"SEG_DASHBOARD_TO_ABSENCE_LISTING"
#define kSegueFromSideMenuToAbsenceList                 @"SEG_SIDEMENU_TO_ABSENCE_LIST"
#define kSegueLogout                                    @"SEG_UNWIND_TO_LOGIN"
#define kSegueFromDashboardToApproval                   @"SEG_DASHBOARD_TO_APPROVAL_LIST"
#define kSegueFromDashboardToNewSickness                @"SEG_DASHBOARD_TO_NEW_SICKNESS_MODULE"
#define kSegueFromNewSicknessToRecordSickness           @"SEG_NEW_SICKNESS_MODULE_TO_RECORD_SICKNESS"
#define kSegueFromSicknessToStaffHistory                @"SEG_SICKNESS_HISTORY_TO_MY_STAFF_HISTORY"
#define kSegueFromStaffSicknessToAmendSickness          @"SEG_STAFFHISTORY_TO_AMEND_SICKNESS"
#define kSegueFromAbsenceToRecordAbsence                @"SEG_ABSENCE_TO_RECORD_ABSENCE"
#define kSegueFromAbsenceToAddRecordToil                @"SEG_ABSENCE_TO_ADD_RECORD_TOIL"
#define kSegueFromAnnualDashboardToRecordPersonalDayBirthday           @"SEG_AL_DASHBOARD_TO_RECORD_PERSONAL_DAY_BIRTHDAY"
#define kSegueFromAbsenceToRecordInternalMeeting        @"SEG_ABSENCE_TO_RECORD_INTERNAL_MEETING"

/*Sickness*/

#define kStaffSicknessInWork                            @"In Work"
#define kStaffSicknessOnLeave                           @"Sickness Absence"


/*Toil*/

#define kToilLeaveTypeTopUp                             @"BRS TOIL Recorded"
#define kToilLeaveTypeRequest                           @"BRS Time Off in Lieu"

#endif /* UIConstants_h */
