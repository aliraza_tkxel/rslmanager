//
//  GeneralConstants.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef GeneralConstants_h
#define GeneralConstants_h




//DateFormats
#define kDateFormatComponentMonthNameFull       @"MMMM"
#define kDateFormatComponentMonthNameCompact    @"MMM"
#define kDateFormatComponentMonthNameNumeric    @"MM"
#define kDateFormatComponentYear                @"yyyy"
#define kDateFormatComponentDayNumeric          @"dd"
#define kDateFormatComponentDayName             @"EEEE"
#define kDateFormatComponentTime12Hours         @"hh:mm a"
#define kDateFormatComponentTime24Hours         @"HH:mm"
#define kDateFormatStyleServerStamp             @"yyyy-MM-dd'T'HH:mm:ss"
#define kDateFormatDefaultStamp                 @"dd/MM/yyyy"

//Network

#define kRESPONSE_DATA_KEY @"Data"

//CD Value

#define kValueNotApplicable                     @"N/A"

/*Leave Nature Names*/

#define kLeaveNatureSickness                    @"Sickness"
#define kLeaveNatureAnnual                      @"Annual Leave"
#define kLeaveNatureAbsence                     @"Absence"
#define kLeaveNaturePersonal                    @"Personal Day"
#define kLeaveNatureBirthday                    @"Birthday Leave"
#define kDaysTakenModelNatureKey                @"natureDescription"

/*Leave Status*/

#define kLeaveStatusApproved                    @"Approved"
#define kLeaveStatusRejected                    @"Rejected"
#define kLeaveStatusPending                     @"Pending"
#define kLeaveStatusCancelled                   @"Cancelled"
#define kLeaveStatusDeclined                    @"Declined"
#define kLeaveStatusSicknessAbsent              @"Absent"
#define kLeaveStatusBHApproved                  @"BH-Approved"

/*Dashboard Stats*/

#define kDashboardStatsPropertyName             @"itemName"
#define kDashboardStatsPropertyValue            @"itemValue"
#define kDashboardStatsAnnualLeaves             @"Annual Leave"
#define kDashboardStatsOtherLeaves              @"Other Leave"
#define kDashboardStatsBirthday                 @"Birthday Leave"
#define kDashboardStatsPersonal                 @"Personal day Leave"
#define kDashboardStatsTOIL                     @"Toil Leave"
#define kDashboardSickness                     @"Sickness"
/*Holtype combos*/

#define kHolTypeBothFullDay                     @"F-F"
#define kHolTypeStartFullEndHalf                @"F-M"
#define kHolTypeStartHalfEndFull                @"A-F"
#define kHolTypeSingleFull                      @"F"
#define kHolTypeSingleMorning                   @"M"
#define kHolTypeSingleAfternoon                 @"A"


#endif /* GeneralConstants_h */
