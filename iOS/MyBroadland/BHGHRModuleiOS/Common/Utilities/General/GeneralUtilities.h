//
//  GeneralUtilities.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneralUtilities : NSObject


#pragma mark - Errors
+(NSError *) createErrorWithDomain:(NSString *) domain
                        andMessage:(NSString *) message;
+(NSString *) getErrorDescriptionForError:(NSError *)error;

#pragma mark - App Version

+(NSString *)getAPPVersionString;
+(NSString *) getCopyRightNoticeString;

#pragma mark - JSON

+ (NSString *)collectionToString:(id)collection
                           error:(NSError **)error;

#pragma mark - Base64 Encoding

+(NSString *) base64EncodeString:(NSString *)simpleStr;
+(NSString *) decodeBase64String:(NSString *)base64Str;

#pragma mark - Core Data

+ (NSURL *)applicationDocumentsDirectory;

@end
