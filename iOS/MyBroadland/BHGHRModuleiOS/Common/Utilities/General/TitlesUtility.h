//
//  TitlesUtility.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 8/1/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TitlesUtility : NSObject
#pragma mark - Side Menu
+(NSMutableArray *) GetSideMenuTitles;
+(NSMutableArray *) GetSideMenuIcons;

#pragma mark - Dashboard

+(NSMutableArray *) GetDashboardMenuTitles;
+(NSMutableArray *) GetDashboardMenuIcons;

+(NSMutableArray *) GetDashboardStatsSectionTitles;

@end
