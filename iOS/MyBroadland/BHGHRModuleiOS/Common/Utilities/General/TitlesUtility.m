//
//  TitlesUtility.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 8/1/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "TitlesUtility.h"

@implementation TitlesUtility
#pragma mark - Side menu

+(NSMutableArray *) GetSideMenuTitles{
    return [[NSMutableArray alloc] initWithObjects:@"Dashboard",@"My Details",@"Annual Leave", @"Sickness",@"Other Leave",@"Logout", nil];
}

+(NSMutableArray *) GetSideMenuIcons{
    return [[NSMutableArray alloc] initWithObjects:@"dashboard_icon.png",@"my_details_icon.png",@"annual_leaves.png", @"sick_leave_icon.png",@"absence_request_icon.png",@"logout_icon.png", nil];
}


#pragma mark - Dashboard

+(NSMutableArray *) GetDashboardMenuTitles{
    return [[NSMutableArray alloc] initWithObjects:@"My Details",@"Annual Leave",  @"Other Leave",nil];//@"Sickness", @"Expenses",@"Slips & Trips"
}

+(NSMutableArray *) GetDashboardMenuIcons{
    return [[NSMutableArray alloc] initWithObjects:@"my_details_grid_icon_grey.png",@"annual_leaves_grid_icon.png", @"absence_request_grid_icon_grey.png", nil];//@"sick_leave_grid_icon_grey.png",@"expenses_grid_icon_grey.png",@"slips_and_trips_grid_icon_grey.png"
}

+(NSMutableArray *) GetDashboardStatsSectionTitles{
    return [[NSMutableArray alloc] initWithObjects:@"Requests & Approvals",@"My Training", nil];
}

@end
