//
//  GeneralUtilities.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/15/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "GeneralUtilities.h"
#import <AFNetworking.h>
@implementation GeneralUtilities
#pragma mark -  Error

+(NSError *) createErrorWithDomain:(NSString *) domain
                        andMessage:(NSString *) message{
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:message forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:domain code:500 userInfo:details];
    return error;
}

+(NSString *) getErrorDescriptionForError:(NSError *)error{
   
    NSDictionary *userinfo1 = [[NSDictionary alloc] initWithDictionary:error.userInfo];
    NSString *errorDesc= kResponseStatusError;
    if(userinfo1) {
        NSError *innerError = [userinfo1 valueForKey:NSUnderlyingErrorKey];
        if(innerError) {
            NSDictionary *innerUserInfo = [[NSDictionary alloc] initWithDictionary:innerError.userInfo];
            if(innerUserInfo)
            {
                if([innerUserInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey])
                {
                    errorDesc = [[NSString alloc] initWithData:[innerUserInfo objectForKey:AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                    
                }
            }
        }
        else
        {
            errorDesc = [[NSString alloc] initWithData:[userinfo1 valueForKey:@"AFNetworkingOperationFailingURLResponseDataErrorKey"] encoding:NSUTF8StringEncoding];
            
            if([errorDesc length]==0){
                errorDesc = error.localizedDescription;
            }
            
            
        }
    }
    return errorDesc;
}

#pragma mark - App Version

+(NSString *)getAPPVersionString
{
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *prodName = [NSString stringWithFormat:@"v%@(%@)",[infoDict objectForKey:@"CFBundleShortVersionString"],[infoDict objectForKey:@"CFBundleVersion"]];
    
    return prodName;
}

+(NSString *) getCopyRightNoticeString{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:[NSDate date]];
    NSString *copyRightNotice = [NSString stringWithFormat:@"Copyright © %li by Broadland Housing Group",[components year]];
    return copyRightNotice;
}

#pragma mark - JSON


+ (NSString *)collectionToString:(id)collection
                           error:(NSError **)error
{
    NSString * myString = @"";
    if(collection != nil)
    {
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:collection options:0 error:error];
        myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return myString;
}




#pragma mark - Base64 Encode

+(NSString *) base64EncodeString:(NSString *)simpleStr{
    NSData *nsdata = [simpleStr
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    return base64Encoded;
}

+(NSString *) decodeBase64String:(NSString *)base64Str{
    NSData *nsdataFromBase64String = [[NSData alloc]
                                      initWithBase64EncodedString:base64Str options:0];
    
    // Decoded NSString from the NSData
    NSString *base64Decoded = [[NSString alloc]
                               initWithData:nsdataFromBase64String encoding:NSUTF8StringEncoding];
    return base64Decoded;
}

#pragma mark - CoreData

+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
