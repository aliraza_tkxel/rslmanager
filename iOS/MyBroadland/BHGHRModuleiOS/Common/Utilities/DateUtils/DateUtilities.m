//
//  DateUtilities.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DateUtilities.h"

@implementation DateUtilities

+ (NSDateFormatter *) getDateFormatter:(NSString *)format andTimeZone:(NSString *) timeZone
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    if(![timeZone isEqualToString:@""]){
        dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone];
    }
    else{
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    }
    dateFormatter.dateFormat = format;
    return dateFormatter;
}


+(NSComparisonResult) compareDate:(NSDate*) date1 toDate:(NSDate *) date2{
    NSComparisonResult result = [date1 compare:date2];
    return result;
}

#pragma mark - Bank Holidays

+(BOOL) isDateABankHoliday:(NSDate *) date
{
    NSArray *bankHolidays = [[LookUpsDataManager sharedInstance] fetchAllBankHolidays];
    for(LookUpModel *model in bankHolidays){
        NSDate *holiday = [DateUtilities getDateFromString:model.lookUpDescription withFormat:kDateFormatStyleServerStamp];
        if([DateUtilities isDate:holiday SameAs:date]){
            return YES;
        }
    }
    return NO;
}


+(NSDate *) getTimeStrippedDateFor:(NSDate *) originalDate{
    NSString *dateTimelessStr = [DateUtilities getDateStringFromDate:originalDate withFormat:kDateFormatDefaultStamp];
    NSDate *dateTimeLess = [DateUtilities getDateFromString:dateTimelessStr withFormat:kDateFormatDefaultStamp];
    return dateTimeLess;
}

+(BOOL) checkIfDateLiesOnWeekend:(NSDate *) date{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    return [gregorianCalendar isDateInWeekend:date];
}

+(BOOL) checkIfDateLiesOnWeekendForAnnualLeave:(NSDate *)date{
    BOOL result = NO;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitWeekOfMonth | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday) fromDate:date];
    NSInteger weekNumber = [dateComponent weekOfMonth];
    NSMutableDictionary *map = [DateUtilities getWorkingPatternMap];
    if(!isEmpty(map)){
        NSString *key = [NSString stringWithFormat:@"%lu",weekNumber];
        WorkingPattern *appliedPattern = [map objectForKey:key];
        switch ([dateComponent weekday]) {
            case 1:
                result = (appliedPattern.sun==0)?YES:NO;
                break;
            case 2:
                result = (appliedPattern.mon==0)?YES:NO;
                break;
            case 3:
                result = (appliedPattern.tue==0)?YES:NO;
                break;
            case 4:
                result = (appliedPattern.wed==0)?YES:NO;
                break;
            case 5:
                result = (appliedPattern.thu==0)?YES:NO;
                break;
            case 6:
                result = (appliedPattern.fri==0)?YES:NO;
                break;
            case 7:
                result = (appliedPattern.sat==0)?YES:NO;
                break;
            default:
                break;
        }
    }
    
    
    return result;
}


+(NSMutableDictionary *) getWorkingPatternMap{
    NSArray *workingPattern = [[UserDataManager sharedInstance] fetchWorkingPatternForCurrentUser];
    NSMutableDictionary *workingPatternMap = [[NSMutableDictionary alloc] init];
    if(!isEmpty(workingPattern)){
        int y = 0;
        for(int i=0;i<5;i++){
            if([workingPattern count]>i){
                [workingPatternMap setObject:[workingPattern objectAtIndex:i] forKey:[NSString stringWithFormat:@"%i",i+1]];
            }
            else{
                if(y>=[workingPattern count]){
                    y=0;
                }
                [workingPatternMap setObject:[workingPattern objectAtIndex:y] forKey:[NSString stringWithFormat:@"%i",i+1]];
                y=y+1;
            }
        }
    }
    
    return workingPatternMap;
}

#pragma mark - Conversions

+(NSDate*) getDateFromString:(NSString *)dateString withFormat:(NSString*)format
{
    NSDateFormatter *formatter = [DateUtilities getDateFormatter:format andTimeZone:@"UTC"];
    NSDate *date = [formatter dateFromString:dateString];
    return date;
}

+(NSString *) getDateStringFromDate:(NSDate*) date withFormat:(NSString*) format{
    NSDateFormatter *df = [DateUtilities getDateFormatter:format andTimeZone:@""];
    NSString *stringFromDate = [df stringFromDate:date];
    return stringFromDate;
}

#pragma mark - Comparison
/*date 1 is in future*/
+(BOOL) isDate:(NSDate *)date1 laterThan:(NSDate *)date2{
    NSComparisonResult result = [DateUtilities compareDate:[DateUtilities getTimeStrippedDateFor:date1]
                                                    toDate:[DateUtilities getTimeStrippedDateFor:date2]];
    if(result==NSOrderedDescending){
        return YES;
    }
    return NO;
}

+(BOOL) isDate:(NSDate *)date1 SameAs:(NSDate *)date2{
    NSComparisonResult result = [DateUtilities compareDate:[DateUtilities getTimeStrippedDateFor:date1]
                                                    toDate:[DateUtilities getTimeStrippedDateFor:date2]];
    if(result==NSOrderedSame){
        return YES;
    }
    return NO;
}
/*date 1 is in past*/
+(BOOL) isDate:(NSDate *)date1 EarlierThan:(NSDate *)date2{
    NSComparisonResult result = [DateUtilities compareDate:[DateUtilities getTimeStrippedDateFor:date1]
                                                    toDate:[DateUtilities getTimeStrippedDateFor:date2]];
    if(result==NSOrderedAscending){
        return YES;
    }
    return NO;
}

#pragma mark - Difference

+ (NSNumber *)calculateHoursBetween:(NSDate *)firstDate and:(NSDate *)secondDate {
    NSUInteger unitFlags = NSCalendarUnitSecond;
    NSNumber* result = [NSNumber numberWithFloat:0.00];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:unitFlags fromDate:firstDate toDate:secondDate options:0];
    NSInteger totalSeconds = [components second];
    double hours = totalSeconds / 3600.00;
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    
    NSString *stringo = [fmt stringFromNumber:[NSNumber numberWithDouble:hours]];
    
    NSNumber *numero = [fmt numberFromString:stringo];
    result = (!isEmpty(numero))?numero:result;
    return result;
}

+(double) calculateNumberOfDaysBetween:(NSDate *)startDate andEndDate:(NSDate *) endDate{
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSNumber *num = [NSNumber numberWithInteger:components.day];
    return [num doubleValue];
}
+(double) calculateWorkingDaysForSicknessBetween:(NSDate *) startDate andEndDate:(NSDate *) endDate{
    startDate = [DateUtilities getTimeStrippedDateFor:startDate];
    endDate = [DateUtilities getTimeStrippedDateFor:endDate];
    NSDate* dateTag = startDate;
    double workingDays = 0;
    if([DateUtilities isDate:startDate EarlierThan:endDate]){
        workingDays = [DateUtilities calculateNumberOfDaysBetween:startDate andEndDate:endDate];
        while (![DateUtilities isDate:dateTag laterThan:endDate]) {
            if([DateUtilities checkIfDateLiesOnWeekend:dateTag]){
                workingDays = workingDays-1;
            }
            dateTag = [DateUtilities addNumberOfDays:1 toDate:dateTag];
        }
        
    }
    return (workingDays>0)?workingDays:0;
}
+(double) calculateWorkingDaysBetween:(NSDate *) startDate andEndDate:(NSDate *) endDate forEmploymentNature:(NSInteger)nature andLeaveStartDuration:(NSInteger) leaveStartDuration andLeaveEndDuration:(NSInteger) leaveEndDuration andAtomicity:(NSInteger) leaveAtomicity{
    startDate = [DateUtilities getTimeStrippedDateFor:startDate];
    endDate = [DateUtilities getTimeStrippedDateFor:endDate];
    NSDate* dateTag = startDate;
    double workingDays = 0;
    double coefficient = 0.00;
    NSMutableDictionary *map = [DateUtilities getWorkingPatternMap];
    if(!isEmpty(map))
    {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        while (![DateUtilities isDate:dateTag laterThan:endDate]) {
            if(![DateUtilities isDateABankHoliday:dateTag]){
                NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:dateTag];
                NSInteger weekNumber = [dateComponent weekOfMonth];
                NSInteger weekDay = [dateComponent weekday];
                NSString *key = [NSString stringWithFormat:@"%lu",weekNumber];
                WorkingPattern *appliedPattern = [map objectForKey:key];
                coefficient = 0.00;
                switch (weekDay) {
                    case 1:
                        coefficient = appliedPattern.sun;
                        break;
                    case 2:
                        coefficient = appliedPattern.mon;
                        break;
                    case 3:
                        coefficient = appliedPattern.tue;
                        break;
                    case 4:
                        coefficient = appliedPattern.wed;
                        break;
                    case 5:
                        coefficient = appliedPattern.thu;
                        break;
                    case 6:
                        coefficient = appliedPattern.fri;
                        break;
                    case 7:
                        coefficient = appliedPattern.sat;
                        break;
                    default:
                        break;
                }
                if(nature==UserEmploymentNatureDaily){
                    coefficient = (coefficient>0)?1.00:0.00;
                }
                if(leaveAtomicity==LeaveAtomicitySingle){
                    if(leaveStartDuration!=LeaveDurationFullDay || leaveEndDuration!=LeaveDurationFullDay){
                        if(coefficient>0){
                            coefficient=coefficient/2;
                        }
                    }
                }
                else{
                    if([DateUtilities isDate:dateTag SameAs:startDate]){
                        if(leaveStartDuration!=LeaveDurationFullDay){
                            coefficient=coefficient/2;
                        }
                    }
                    if([DateUtilities isDate:dateTag SameAs:endDate] && ![DateUtilities isDate:startDate SameAs:endDate]){
                        if(leaveEndDuration!=LeaveDurationFullDay){
                            coefficient=coefficient/2;
                        }
                    }
                }
            }
            workingDays+=coefficient;
            
            dateTag = [DateUtilities addNumberOfDays:1 toDate:dateTag];
        }
        
    }
    
    return workingDays;
}

+(NSDate *) addNumberOfDays:(NSInteger) days toDate:(NSDate *) date{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = days;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *nextDate = [gregorianCalendar dateByAddingComponents:dayComponent toDate:date options:0];
    return nextDate;

}

+(NSDate *) addYears:(NSInteger) years toDate:(NSDate *) date{
    NSDateComponents *yearComp = [[NSDateComponents alloc] init];
    yearComp.year = years;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *nextDate = [gregorianCalendar dateByAddingComponents:yearComp toDate:date options:0];
    return nextDate;
    
}

+(NSInteger)getYear:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    return [components year];
}

#pragma mark - Age Calculation

+(NSString *) calculateAgeForPersonBornOn:(NSDate *)date{
    double ageDiff = [DateUtilities calculateNumberOfDaysBetween:date andEndDate:[NSDate date]];
    double ageInYears = ageDiff/365.0;
    return [NSString stringWithFormat:@"%.f years old",ageInYears];
}


#pragma mark - Fiscal Year

+(NSInteger) getCurrentYear{
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate];
    return [components year];
}

+(NSDate*) getFiscalYearStartDate{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:4];
    if([DateUtilities isCurrentDateAfterCurrentFiscalYearEnd]){
        [comps setYear:[DateUtilities getCurrentYear]];
    }
    else{
        [comps setYear:[DateUtilities getCurrentYear]-1];
    }
    
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    return date;
}
+(BOOL) isCurrentDateAfterCurrentFiscalYearEnd{
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:4];
    [comps setYear:[DateUtilities getCurrentYear]];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    if([DateUtilities isDate:currentDate laterThan:date]){
        return YES;
    }
    return NO;
}
+(NSDate*) getFiscalYearEndDate{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:31];
    [comps setMonth:3];
    if([DateUtilities isCurrentDateAfterCurrentFiscalYearEnd]){
        [comps setYear:[DateUtilities getCurrentYear]+1];
    }
    else{
        [comps setYear:[DateUtilities getCurrentYear]];
    }
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    return date;
}

@end
