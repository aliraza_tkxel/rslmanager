//
//  DateUtilities.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtilities : NSObject
+(NSDate*) getDateFromString:(NSString *)dateString withFormat:(NSString*)format;
+(NSString *) getDateStringFromDate:(NSDate*) date withFormat:(NSString*) format;
+(BOOL) isDate:(NSDate *)date1 laterThan:(NSDate *)date2;
+(BOOL) isDate:(NSDate *)date1 SameAs:(NSDate *)date2;
+(BOOL) isDate:(NSDate *)date1 EarlierThan:(NSDate *)date2;
+(BOOL) isDateABankHoliday:(NSDate *) date;
+(BOOL) checkIfDateLiesOnWeekendForAnnualLeave:(NSDate *)date;
+(NSString *) calculateAgeForPersonBornOn:(NSDate *)date;
+ (NSNumber*)calculateHoursBetween:(NSDate *)firstDate and:(NSDate *)secondDate;
+(BOOL) checkIfDateLiesOnWeekend:(NSDate *) date;
+(double) calculateWorkingDaysBetween:(NSDate *) startDate andEndDate:(NSDate *) endDate forEmploymentNature:(NSInteger)nature andLeaveStartDuration:(NSInteger) leaveStartDuration andLeaveEndDuration:(NSInteger) leaveEndDuration andAtomicity:(NSInteger) leaveAtomicity;
+(double) calculateWorkingDaysForSicknessBetween:(NSDate *) startDate andEndDate:(NSDate *) endDate;
+(NSDate*) getFiscalYearStartDate;
+(NSDate*) getFiscalYearEndDate;
+(NSInteger)getYear:(NSDate*)date;
+(NSDate *) addYears:(NSInteger) years toDate:(NSDate *) date;
@end
