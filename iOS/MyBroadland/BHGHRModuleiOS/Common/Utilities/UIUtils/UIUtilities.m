//
//  UIUtilities.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UIUtilities.h"
#import <QuartzCore/QuartzCore.h>
@implementation UIUtilities

#pragma mark - ImageView

+(void) makeImageView:(UIImageView *)imageView roundedWithBorderColor:(UIColor *) borderColor
       andBorderWidth:(CGFloat) borderWidth {
    
    imageView.layer.backgroundColor=[[UIColor clearColor] CGColor];
    imageView.layer.cornerRadius=imageView.frame.size.width / 2;
    imageView.clipsToBounds = YES;
    imageView.layer.borderWidth=borderWidth;
    imageView.layer.borderColor=[borderColor CGColor];
}


#pragma mark - UIViewController

+ (UIViewController *) getVisibleViewController{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    topController = [UIUtilities getVisibleViewControllerFrom:topController];
    
    return topController;
}

+ (UIViewController *) getVisibleViewControllerFrom:(UIViewController *) vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [UIUtilities getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [UIUtilities getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [UIUtilities getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

#pragma mark - UIButton

+(void) makeUIButton:(UIButton *) btn curvedWithBorderWidth:(CGFloat) width andBorderColor:(UIColor*) borderColor{
    btn.layer.cornerRadius = 5;
    btn.clipsToBounds = YES;
    
    btn.layer.borderColor=borderColor.CGColor;
    btn.layer.borderWidth=width;//2.5f;
}





@end
