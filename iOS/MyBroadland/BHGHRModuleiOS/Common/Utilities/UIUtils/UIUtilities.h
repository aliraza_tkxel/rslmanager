//
//  UIUtilities.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIUtilities : NSObject
#pragma mark  - Imgview
+(void) makeImageView:(UIImageView *)imageView roundedWithBorderColor:(UIColor *) borderColor andBorderWidth:(CGFloat) borderWidth;

#pragma mark - UIViewController

+ (UIViewController *) getVisibleViewController;

#pragma mark - UIButton

+(void) makeUIButton:(UIButton *) btn curvedWithBorderWidth:(CGFloat) width andBorderColor:(UIColor*) borderColor;



@end
