//
//  ViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/5/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthenticationManager.h"
#import "BaseViewController.h"
@interface LoginViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *lblCopyRightNotice;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBg;
@property (weak, nonatomic) IBOutlet UILabel *lblAppVersion;

@end

