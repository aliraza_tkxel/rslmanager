//
//  ViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/5/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LoginViewController.h"
#import "UIUtilities.h"
#import <Crashlytics/Crashlytics.h>
#import "AuthenticationResponseObj.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self initModule];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [_btnLogin setBackgroundImage:[UIImage imageNamed:@"btn_login.png"] forState:UIControlStateNormal];
    [_btnLogin setBackgroundImage:[UIImage imageNamed:@"btn_login_selected.png"] forState:UIControlStateHighlighted];
    [_btnLogin setBackgroundImage:[UIImage imageNamed:@"btn_login_selected.png"] forState:UIControlStateFocused];
    [self configureTextFields];
    _lblAppVersion.text = [GeneralUtilities getAPPVersionString];
    _lblCopyRightNotice.text = [GeneralUtilities getCopyRightNoticeString];
    
}

-(void) configureTextFields{
    UIColor *color = [UIColor whiteColor];
    _txtFieldUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    _txtFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
}

#pragma mark - IBActions
- (IBAction)btnLoginTapped:(id)sender {
    
    [self showProgressRingWithMessage:kProgressLoggingIn];
    
    AuthenticationManager *manager = [AuthenticationManager new];
    
    //_txtFieldUserName.text = @"823.823";
    //_txtFieldPassword.text = @"Broadland823@!";
    
//    _txtFieldUserName.text = @"858.858";
//    _txtFieldPassword.text = @"Broadland858@!";
    
    //_txtFieldUserName.text = @"113.113";
    //_txtFieldPassword.text = @"Broadland113@!";
    
    //_txtFieldUserName.text = @"1226.1226";
    //_txtFieldPassword.text = @"Broadland1226@!";
    
    [manager loginWithUserName:_txtFieldUserName.text
                   andPassword:_txtFieldPassword.text
                   andResponse:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            [self showProgressRingWithMessage:kProgressInsertingData];
            AuthenticationResponseObj *obj = (AuthenticationResponseObj *)response;
            [obj.lookUps insertLookUps];
            [obj insertUserInfo] ;
            [self hideProgressRing];
            [LoggedInUser setUserObjectInfo:obj.userInfo];
            [CrashlyticsKit setUserName:obj.userInfo.userName];
            [self performSegueWithIdentifier:kSegueFromLoginToDashboard sender:self];
        }
        else{
            [self showMessageWithHeader:kDomainAuthentication
                                       andBody:error];
        }
    }];
}
#pragma mark - Navigation

- (IBAction)unWindToLoginScreen:(UIStoryboardSegue*)sender
{
    _txtFieldPassword.text = @"";
    _txtFieldUserName.text = @"";
}

@end
