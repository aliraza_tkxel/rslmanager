//
//  SideMenuViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SideMenuViewController.h"
#import "RearViewMenuTableViewCell.h"
#import "UIUtilities.h"
#import "DashboardViewController.h"
#import "AnnualLeavesDashboardViewController.h"
#import "SicknessHistoryViewController.h"
#import "MyDetailsViewController.h"
#import "AbsenceListingViewController.h"
@interface SideMenuViewController ()
typedef NS_ENUM(NSInteger, SideMenuOption) {
    SideMenuOptionDashboard,
    SideMenuOptionMyDetails,
    SideMenuOptionAnnualLeaves,
    SideMenuOptionRecordSickness,
    SideMenuOptionAbsence,
    SideMenuOptionLogout
};

@end

@implementation SideMenuViewController

-(void) viewWillAppear:(BOOL)animated{
    [self setUserInformation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init Methods

-(void) loadData{
    [_personalInfo addBottomBorderWithColor:[UIColor whiteColor]
                                   andWidth:1.0];
    _titleArray = [TitlesUtility GetSideMenuTitles];
    _iconImagesArray = [TitlesUtility GetSideMenuIcons];
    _lblVersionInfo.text = [GeneralUtilities getAPPVersionString];
    _tblView.tableFooterView = [UIView new];
    [_tblView reloadData];
}

-(void) setUserInformation{
    if(!isEmpty([[UserModel sharedInstance] userPersonalInfo])){
        _lblUserName.text = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedInstance] userPersonalInfo].firstName,[[UserModel sharedInstance] userPersonalInfo].lastName];
        _lblUserTitle.text = [[UserModel sharedInstance] userObject].jobRole;
        
    }
    else{
        _lblUserName.text = [[UserModel sharedInstance] userObject].employeeFullName;
        _lblUserTitle.text = [[UserModel sharedInstance] userObject].jobRole;
       
    }
}

#pragma mark - Operations

-(BOOL) isVisibleViewControllerOfClass:(Class) vcClass{

    if([self.revealViewController.frontViewController isKindOfClass:vcClass]){
        [self.revealViewController revealToggleAnimated:YES];
        return YES;
    }
    return NO;
}

#pragma mark - TableViewDelegates & Datasource

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_titleArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RearViewMenuTableViewCell *customCell = (RearViewMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSideMenuCellReuseIdentifier];
    [customCell configureCellWithIcon:[_iconImagesArray objectAtIndex:indexPath.row] andTitle:[_titleArray objectAtIndex:indexPath.row]];
    return customCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case SideMenuOptionDashboard:
            if(![self isVisibleViewControllerOfClass:[DashboardViewController class]]){
                [self performSegueWithIdentifier:kSegueFromSideMenuToDashboard sender:self];
            }
            break;
        case SideMenuOptionMyDetails:
            if(![self isVisibleViewControllerOfClass:[MyDetailsViewController class]]){
                
                [self performSegueWithIdentifier:kSegueFromSideMenuToMyDetails sender:self];
            }
            break;
        case SideMenuOptionAnnualLeaves:
            if(![self isVisibleViewControllerOfClass:[AnnualLeavesDashboardViewController class]]){
                [self performSegueWithIdentifier:kSegueFromSideMenuToAnnualLeavesDashboard sender:self];
                
            }
            break;
        case SideMenuOptionRecordSickness:
            if(![self isVisibleViewControllerOfClass:[SicknessHistoryViewController class]]){
                [self performSegueWithIdentifier:kSegueFromSideMenuToRecordSickness sender:self];
            }
            break;
        case SideMenuOptionAbsence:
            if(![self isVisibleViewControllerOfClass:[AbsenceListingViewController class]]){
                [self performSegueWithIdentifier:kSegueFromSideMenuToAbsenceList sender:self];
            }
            break;
        case SideMenuOptionLogout:
            [LoggedInUser clearData];
            [self performSegueWithIdentifier:kSegueLogout sender:self];
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
