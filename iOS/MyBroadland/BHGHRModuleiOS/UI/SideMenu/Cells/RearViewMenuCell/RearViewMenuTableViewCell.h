//
//  RearViewMenuTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearViewMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
-(void) configureCellWithIcon:(NSString *) iconName andTitle:(NSString*) title;
@end
