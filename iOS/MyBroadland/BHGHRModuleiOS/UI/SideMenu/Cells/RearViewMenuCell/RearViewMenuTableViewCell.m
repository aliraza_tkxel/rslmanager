//
//  RearViewMenuTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "RearViewMenuTableViewCell.h"

@implementation RearViewMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCellWithIcon:(NSString *) iconName andTitle:(NSString*) title{
    _imgViewIcon.image = [UIImage imageNamed:iconName];
    _lblTitle.text = title;
}

@end
