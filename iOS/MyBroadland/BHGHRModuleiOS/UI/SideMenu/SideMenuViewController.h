//
//  SideMenuViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface SideMenuViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property NSMutableArray * titleArray;
@property NSMutableArray *iconImagesArray;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserTitle;
@property (weak, nonatomic) IBOutlet UIView *personalInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblVersionInfo;

@end
