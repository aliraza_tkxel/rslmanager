//
//  DashboardGridCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardGridCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
-(void) configureCellWithImageName:(NSString *) imageName andTitle:(NSString *) title;
@end
