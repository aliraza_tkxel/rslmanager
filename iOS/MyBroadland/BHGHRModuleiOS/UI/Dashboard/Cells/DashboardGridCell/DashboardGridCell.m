//
//  DashboardGridCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DashboardGridCell.h"

@implementation DashboardGridCell

-(void) configureCellWithImageName:(NSString *) imageName andTitle:(NSString *) title{
    _lblTitle.text = title;
    _imgViewIcon.image = [UIImage imageNamed:imageName];
}

@end
