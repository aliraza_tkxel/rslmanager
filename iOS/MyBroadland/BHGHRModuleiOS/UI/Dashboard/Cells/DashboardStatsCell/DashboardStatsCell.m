//
//  DashboardStatsCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DashboardStatsCell.h"

@implementation DashboardStatsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCellWithTitle:(NSString *) title andStats:(NSString *) stats
{
    _lblTitle.text = title;
    _lblStats.text = stats;
}

-(void) configureCellWithMainDashboardStatsModel:(MainDashboardStatsModel *) model{
    _lblTitle.text = model.itemName;
    _lblStats.text = model.itemValue;
}

-(void) configureCellWithMainDashboardTrainingModel:(DashboardTrainingPresentationModel *) model{
    _lblTitle.text = model.trainingName;
    _lblStats.text = model.startDateDescription;
}

@end
