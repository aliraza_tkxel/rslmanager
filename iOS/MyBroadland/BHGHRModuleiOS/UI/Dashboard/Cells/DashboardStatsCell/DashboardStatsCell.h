//
//  DashboardStatsCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainDashboardStatsModel.h"
#import "DashboardTrainingPresentationModel.h"
@interface DashboardStatsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStats;
-(void) configureCellWithTitle:(NSString *) title andStats:(NSString *) stats;
-(void) configureCellWithMainDashboardStatsModel:(MainDashboardStatsModel *) model;
-(void) configureCellWithMainDashboardTrainingModel:(DashboardTrainingPresentationModel *) model;
@end
