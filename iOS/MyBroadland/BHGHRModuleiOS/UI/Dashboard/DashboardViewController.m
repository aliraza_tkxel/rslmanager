//
//  DashboardViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DashboardViewController.h"
#import "UIUtilities.h"
#import "DashboardGridCell.h"
#import "DashboardStatsCell.h"
#import "MainDashboardStatsModel.h"
#import "MainDashboardStatsResponseObject.h"
#import "MainDashboardNetworkManager.h"
#import "MainDashboardTrainingModel.h"
#import "MainDashboardTrainingResponseObject.h"
#import "LeaveApprovalViewController.h"
#import "DashboardTrainingPresentationModel.h"
#import "LeaveDateFilterPicker.h"
#import "AnnualLeavesDashboardViewController.h"
#import "AbsenceListingViewController.h"
@interface DashboardViewController ()

@end

@implementation DashboardViewController

-(void) viewWillAppear:(BOOL)animated{
    if (isEmpty([[UserModel sharedInstance] userObject].selectedYearRange)) {
        [self updateDashboardStats:nil];
    }
    else {
        NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
        [self updateDashboardStats:selectedYear];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  - Init

-(void) initModule{
    [self configureRevealMenu];
    [_navigationBarView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1];
    [self SetUpDashboardGrid];
    [self SetUpDashboardStats];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _selectedLeaveType = InAppLeaveTypeAnnual;
}

-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void) SetUpDashboardGrid{
    _dashboardGridIcons = [[NSMutableArray alloc] initWithArray:[TitlesUtility GetDashboardMenuIcons]];
    _dashboardGridTitles = [[NSMutableArray alloc] initWithArray:[TitlesUtility GetDashboardMenuTitles]];
    [_dashboardCollectionView reloadData];
}

-(void) SetUpDashboardStats{
    _dashboardApprovalTabTitles = [[NSMutableArray alloc] init];
    _dashboardStatsSectionTitles = [[NSMutableArray alloc] initWithArray:[TitlesUtility GetDashboardStatsSectionTitles]];
    _dashboardTrainingData =[[NSMutableArray alloc] init];
    
    _tblView.tableFooterView = [UIView new];
    [_tblView reloadData];
}

#pragma mark - Network Operations

-(void)updateDashboardStats: (NSString *) year{
    [self showProgressRingWithMessage:kProgressFetchingDashboardStats];
    MainDashboardNetworkManager *leavesMgr = [MainDashboardNetworkManager new];
    [leavesMgr fetchMainDashboardStatsForUserId:LoggedInUserId
                                        forYear: year
                                   withResponse:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            MainDashboardStatsResponseObject *responseObj = (MainDashboardStatsResponseObject *)response;
            _dashboardApprovalTabTitles = [[NSMutableArray alloc] initWithArray:responseObj.approvalStats];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kDashboardStatsPropertyName
                                                                           ascending:YES];
            _dashboardApprovalTabTitles = [[NSMutableArray alloc] initWithArray:[_dashboardApprovalTabTitles sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [_tblView reloadData];
            [self updateTrainingStats];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];

}

-(void) updateTrainingStats{
    [self showProgressRingWithMessage:kProgressFetchingTrainingStats];
    MainDashboardNetworkManager *leavesMgr = [MainDashboardNetworkManager new];
    [leavesMgr fetchMainDashboardTrainingDataForUserId:LoggedInUserId withResponse:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            MainDashboardTrainingResponseObject *responseObj = (MainDashboardTrainingResponseObject *)response;
            _dashboardTrainingData =[[NSMutableArray alloc] initWithArray:responseObj.trainingList];
            
            _dashboardTrainingData = [self sortAndParseTrainingData:_dashboardTrainingData];
            [_tblView reloadData];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}

-(NSMutableArray *) sortAndParseTrainingData:(NSArray *) responseArray {
    NSMutableArray *sortedArr = [NSMutableArray new];
    for(MainDashboardTrainingModel *respObj in responseArray){
        DashboardTrainingPresentationModel *model = [[DashboardTrainingPresentationModel alloc] init];
        [model initWithTrainingRecord:respObj];
        [sortedArr addObject:model];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate"
                                                                   ascending:YES];
    sortedArr = [[NSMutableArray alloc] initWithArray:[sortedArr sortedArrayUsingDescriptors:@[sortDescriptor]]];
    return sortedArr;
}

#pragma mark - Collection View Datasource and Delegates

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return _dashboardGridTitles.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cellBase;
    DashboardGridCell *cell = [cv dequeueReusableCellWithReuseIdentifier:kDashboardGridCellReuseIdentifier forIndexPath:indexPath];
    [cell configureCellWithImageName:[_dashboardGridIcons objectAtIndex:indexPath.row]
                            andTitle:[_dashboardGridTitles objectAtIndex:indexPath.row]];
    cellBase = cell;
    return cellBase;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:kSegueFromDashboardToMyDetails
                                      sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:kSegueFromDashboardToAnnualLeavesDashboard
                                      sender:self];
            break;
        /*case 2:
            [self performSegueWithIdentifier:kSegueFromDashboardToRecordSickness
                                      sender:self]; //Old one
            [self performSegueWithIdentifier:kSegueFromDashboardToNewSickness
                                      sender:self];
            break;*/
        case 2:
            [self performSegueWithIdentifier:kSegueFromDashboardToAbsenceList
                                      sender:self];
            break;
        default:
            [self showMessageWithHeader:kDomainDashboard
                                andBody:kFeatureUnavailable];
            break;
    }
}

#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dashboardStatsSectionTitles.count;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger sectionsCount = 0;
    switch (section) {
        case 0:
            sectionsCount= _dashboardApprovalTabTitles.count;
            break;
        case 1:
            sectionsCount = _dashboardTrainingData.count;
            break;
        default:
            sectionsCount = 0;
            break;
    }
    return sectionsCount;

}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DashboardStatsCell *customCell = (DashboardStatsCell *)[tableView dequeueReusableCellWithIdentifier:kDashboardStatsCellReuseIdentifier];
    if(indexPath.section==0){
        MainDashboardStatsModel *modelObj = [_dashboardApprovalTabTitles objectAtIndex:indexPath.row];
        [customCell configureCellWithMainDashboardStatsModel:modelObj];
    }
    else{
        [customCell configureCellWithMainDashboardTrainingModel:[_dashboardTrainingData objectAtIndex:indexPath.row]];
    }
    
    return customCell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        return 41;
    }
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        MainDashboardStatsModel *modelObj = [_dashboardApprovalTabTitles objectAtIndex:indexPath.row];
        if([modelObj.itemName containsString:kDashboardSickness]){
            [self performSegueWithIdentifier:kSegueFromDashboardToNewSickness
                                      sender:self];
            return;
        }
        if([modelObj.itemName containsString:kDashboardStatsAnnualLeaves]){
            _selectedLeaveType = InAppLeaveTypeAnnual;
        }
        else if([modelObj.itemName containsString:kDashboardStatsOtherLeaves]){
            _selectedLeaveType = InAppLeaveTypeAbsence;
        }
        else if([modelObj.itemName containsString:kDashboardStatsBirthday]){
            _selectedLeaveType = InAppLeaveTypeBirthday;
        }
        else if([modelObj.itemName containsString:kDashboardStatsPersonal]){
            _selectedLeaveType = InAppLeaveTypePersonalDay;
        }
        else if([modelObj.itemName containsString:kDashboardStatsTOIL]){
            _selectedLeaveType = InAppLeaveTypeTOIL;
        }
        if([modelObj.itemValue intValue]>0){
            [self performSegueWithIdentifier:kSegueFromDashboardToApproval
                                      sender:self];
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0){
        return (_dashboardApprovalTabTitles.count>0)?72:0;
    }
    else if(section ==1){
        return (_dashboardTrainingData.count>0)?30:0;
    }
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 72)];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [label setFont:AppThemeFontWithSize(17)];
        [label setTextColor:[UIColor whiteColor]];
        label.text = [_dashboardStatsSectionTitles objectAtIndex:section];
        label.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label];
        [view setBackgroundColor:AppThemeColorRed];
        [parentView addSubview:view];
        LeaveDateFilterPicker *picker = [[LeaveDateFilterPicker alloc] initWithFrame:CGRectMake(0, 30, tableView.frame.size.width, 38)];
        picker.lblSelectedDate.text = !isEmpty([[UserModel sharedInstance] userObject].selectedYearRange)? [[UserModel sharedInstance] userObject].selectedYearRange : @"N/A";
        picker.delegate = self;
        [parentView addSubview:picker];
        return parentView;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    
    label.text = [_dashboardStatsSectionTitles objectAtIndex:section];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:AppThemeColorRed];
    return view;
}

#pragma mark - Delegate

-(void) didSelectYearRange:(NSString *)rangeValue {
    [[UserModel sharedInstance] userObject].selectedYearRange = rangeValue;
    [_tblView reloadData];
    NSString *selectedYear = [[rangeValue componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self updateDashboardStats:selectedYear];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kSegueFromDashboardToApproval])
    {
        LeaveApprovalViewController *vc = [[[segue destinationViewController] childViewControllers] objectAtIndex:0];
        vc.controllerLeaveType = _selectedLeaveType;
    }
}

@end
