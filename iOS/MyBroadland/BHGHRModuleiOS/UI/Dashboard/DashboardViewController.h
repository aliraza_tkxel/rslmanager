//
//  DashboardViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface DashboardViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource, LeaveDateFilterPickerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *navBarTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UICollectionView *dashboardCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@property NSMutableArray * dashboardStatsSectionTitles;

@property NSMutableArray * dashboardGridTitles;
@property NSMutableArray *dashboardGridIcons;

@property NSMutableArray * dashboardApprovalTabTitles;
@property NSMutableArray *dashboardTrainingData;
@property InAppLeaveType selectedLeaveType;
@end
