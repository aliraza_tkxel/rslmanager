//
//  BaseViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/22/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Progress Methods

-(void) showProgressRingWithMessage:(NSString *) message{
    [self.view endEditing:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = kProgressGeneral;
    hud.detailsLabel.text = message;
}

-(void) hideProgressRing{
    [MBProgressHUD hideHUDForView:self.view
                         animated:YES];
}

#pragma mark - Custom Views

-(void) showNoResultsFoundView{
    @try {
        if(isEmpty(_noResultsFoundView)){
            _noResultsFoundView = [[[NSBundle mainBundle] loadNibNamed:@"NoResultsFoundView" owner:nil options:nil] objectAtIndex:0];
            
            
        }
        [_noResultsFoundView removeFromSuperview];
        _noResultsFoundView.center = self.view.center;
        [self.view addSubview:_noResultsFoundView];
        [self.view bringSubviewToFront:_noResultsFoundView];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    
}

-(void) hideNoResultsFoundView{
    @try {
        if(!isEmpty(_noResultsFoundView)){
            [_noResultsFoundView removeFromSuperview];
            _noResultsFoundView = nil;
        }
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
}

#pragma mark - Alert Methods
-(void) showMessageWithHeader:(NSString *)header
                      andBody:(NSString *)bodyMessage{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:header message:bodyMessage preferredStyle:UIAlertControllerStyleAlert];//UIAlertControllerStyleActionSheet
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) showMessageAndReturnWithHeader:(NSString *)header
                      andBody:(NSString *)bodyMessage{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:header message:bodyMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
