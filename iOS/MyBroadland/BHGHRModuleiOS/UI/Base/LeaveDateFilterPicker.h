//
//  LeaveDateFilterPicker.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 21/02/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ActionSheetStringPicker.h>
@protocol LeaveDateFilterPickerDelegate <NSObject>
@required
-(void) didSelectYearRange: (NSString *) rangeValue;
@end

//IB_DESIGNABLE
@interface LeaveDateFilterPicker : UIView
@property NSMutableArray * yearRangeOptions;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDate;
@property (weak, nonatomic) id<LeaveDateFilterPickerDelegate> delegate;
@end
