//
//  LeaveDateFilterPicker.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 21/02/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "LeaveDateFilterPicker.h"
#import <Crashlytics/Crashlytics.h>
@implementation LeaveDateFilterPicker

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype) initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self initializeView];
    }
    return self;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self initializeView];
    }
    return self;
}

-(void) initializeView {
    [[NSBundle mainBundle] loadNibNamed:@"LeaveDateFilterPicker"
                                  owner:self
                                options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    _yearRangeOptions = [[NSMutableArray alloc] init];
    [self populateYearRangeOptions];
    [_btnSelectDate addTarget:self action:@selector(btnSelectDateTapped) forControlEvents:UIControlEventTouchUpInside];
}

-(void) populateYearRangeOptions {
    [self incrementYearsByFactor:2];
    [self decrementYearsTillDateOfJoining];
}

-(void) incrementYearsByFactor: (NSInteger) years {
    NSDate * startDate = [[UserModel sharedInstance] userObject].leaveYearStart;
    NSDate * endDate = [[UserModel sharedInstance] userObject].leaveYearEnd;
    NSInteger loopCounter = 0;
    while (loopCounter < years) {
        startDate = [DateUtilities addYears:1 toDate:startDate];
        endDate = [DateUtilities addYears:1 toDate:endDate];
        NSString *stDate = [DateUtilities getDateStringFromDate:startDate
                                                     withFormat:kDateFormatDefaultStamp];
        NSString *enDate = [DateUtilities getDateStringFromDate:endDate
                                                     withFormat:kDateFormatDefaultStamp];
        [_yearRangeOptions addObject:[NSString stringWithFormat:@"%@ - %@", stDate, enDate]];
        loopCounter = loopCounter+1;
    }
    _yearRangeOptions = [[NSMutableArray alloc] initWithArray:[[_yearRangeOptions reverseObjectEnumerator] allObjects]];
}

-(void) decrementYearsTillDateOfJoining {
    NSDate *alStartDare = [[UserModel sharedInstance] userObject].alStartDate;
    NSDate * startDate = [[UserModel sharedInstance] userObject].leaveYearStart;
    NSDate * endDate = [[UserModel sharedInstance] userObject].leaveYearEnd;
    do {
        NSString *stDate = [DateUtilities getDateStringFromDate:startDate
                                                     withFormat:kDateFormatDefaultStamp];
        NSString *enDate = [DateUtilities getDateStringFromDate:endDate
                                                     withFormat:kDateFormatDefaultStamp];
        [_yearRangeOptions addObject:[NSString stringWithFormat:@"%@ - %@", stDate, enDate]];
        startDate = [DateUtilities addYears:-1 toDate:startDate];
        endDate = [DateUtilities addYears:-1 toDate:endDate];
        
    } while ([DateUtilities getYear:endDate]>=[DateUtilities getYear:alStartDare]);
}


- (void)btnSelectDateTapped {
    /*UIViewController *vc = [UIUtilities getVisibleViewController];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Year:"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    for (NSString *year in _yearRangeOptions) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:year
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action) {
                                                           //NSString *title = action.title;
                                                           //you can check here on what button is pressed using title
                                                           if (isEmpty(_delegate) == false) {
                                                               if ([_delegate respondsToSelector:@selector(didSelectYearRange:)]) {
                                                                   [_delegate didSelectYearRange:action.title];
                                                               }
                                                           }
                                                           
                                                       }];
        [alertController addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                         }];
    [alertController addAction:cancelAction];
    [vc presentViewController:alertController animated:YES completion:nil];*/
    NSString *selectedOption = [[UserModel sharedInstance] userObject].selectedYearRange;
    if (!isEmpty(_yearRangeOptions) && !isEmpty(selectedOption)) {
        @try {
            NSInteger index = [_yearRangeOptions indexOfObject:selectedOption];
            [ActionSheetStringPicker showPickerWithTitle:@"Select Year"
                                                    rows:_yearRangeOptions
                                        initialSelection:index
                                               doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                                   if (isEmpty(_delegate) == false) {
                                                       if ([_delegate respondsToSelector:@selector(didSelectYearRange:)]) {
                                                           [_delegate didSelectYearRange:selectedValue];
                                                       }
                                                   }
                                               }
                                             cancelBlock:^(ActionSheetStringPicker *picker) {
                                                 
                                             }
                                                  origin:_btnSelectDate];
        } @catch (NSException *exception) {
            CLS_LOG(@"Error occurred in Filter opening: %@", exception.reason);
        } @finally {
            
        }
    }
    
    
}

@end
