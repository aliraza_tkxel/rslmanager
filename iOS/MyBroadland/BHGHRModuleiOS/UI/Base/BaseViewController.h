//
//  BaseViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/22/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TPKeyboardAvoidingTableView.h>
#import <TPKeyboardAvoidingScrollView.h>
#import "HRMFloatingButtonManager.h"
#import "NoResultsFoundView.h"
@interface BaseViewController : UIViewController

-(void) showMessageWithHeader:(NSString *)header
                      andBody:(NSString *)bodyMessage;

-(void) hideProgressRing;
-(void) showProgressRingWithMessage:(NSString *) message;
-(void) showMessageAndReturnWithHeader:(NSString *)header
                               andBody:(NSString *)bodyMessage;

-(void) showNoResultsFoundView;
-(void) hideNoResultsFoundView;

@property NoResultsFoundView *noResultsFoundView;

@end
