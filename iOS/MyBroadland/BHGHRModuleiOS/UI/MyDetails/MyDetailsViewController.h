//
//  MyDetailsViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DynamicCellsConfiguration.h"
#import "DynamicTableViewCellProtocol.h"
#import "DynamicCellsConfigurationFactoryUtility.h"
@interface MyDetailsViewController : BaseViewController <DynamicTableViewCellProtocol,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *navBarIcon;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UISegmentedControl *detailsSegmentView;
@property (weak, nonatomic) IBOutlet UIView *editingControlsView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property MyDetailsTab selectedSection;
@property BOOL isEditing;
@property (copy,nonatomic) NSMutableDictionary *sourceDict;
@property UserContactInfoModel *emergencyDetails;
@property UserContactInfoModel *contactDetails;
@property UserPersonalInfoModel *personalDetails;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editControlsHeight;
@end
