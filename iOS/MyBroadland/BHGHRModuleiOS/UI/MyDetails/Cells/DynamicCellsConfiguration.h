//
//  DynamicCellsConfiguration.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DynamicCellsConfiguration : NSObject
@property NSArray *sourceArray; /*For Dropdown Field*/
@property NSString *headerTitle; /*For Dropdown/DateTime Field*/
@property DynamicCellsFreeTextFieldType appliedCellFieldType; /*For Text Field*/
@property NSDate *startingDate; /*For DateTime Field*/
@property NSInteger cellRowId;
@property NSInteger cellSectionId;
@property NSString *selectedValue;
@property NSString *inputFieldTitle;
@property BOOL isNonMutableField;
@property BOOL isRangeSpecific;
@property NSDate *maxDate;
@property NSDate *minDate;
@property DynamicCellsBaseType baseTypeForConfig;
@end
