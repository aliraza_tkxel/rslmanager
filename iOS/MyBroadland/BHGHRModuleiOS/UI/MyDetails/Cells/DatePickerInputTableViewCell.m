//
//  DatePickerInputTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DatePickerInputTableViewCell.h"
#import <ActionSheetDatePicker.h>
@implementation DatePickerInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) initializeCellWithConfiguration:(DynamicCellsConfiguration *)configObj{
    _config = configObj;
    _txtFieldSelectedDate.text = configObj.selectedValue;
    _lblTitle.text = configObj.inputFieldTitle;
    [_txtFieldSelectedDate addBottomBorderWithColor:AppThemeColorGrey
                                          andWidth:1.0];
}

- (IBAction)btnSelectDateTapped:(id)sender {
    UIDatePickerMode selectedMode = UIDatePickerModeDate;
    if(_config.baseTypeForConfig == DynamicCellsBaseTypeTime){
        selectedMode = UIDatePickerModeTime;
    }
    
   if(!_config.isNonMutableField){
        NSDate *startDate = [NSDate date];
        if(!isEmpty(_config.startingDate)){
            startDate = _config.startingDate;
        }
       if(!_config.isRangeSpecific){
           [ActionSheetDatePicker showPickerWithTitle:_config.headerTitle datePickerMode:selectedMode selectedDate:startDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
               if(selectedMode==UIDatePickerModeDate){
                   _txtFieldSelectedDate.text = [DateUtilities getDateStringFromDate:selectedDate
                                                                          withFormat:kDateFormatDefaultStamp];
               }
               else{
                   _txtFieldSelectedDate.text = [DateUtilities getDateStringFromDate:selectedDate
                                                                          withFormat:kDateFormatComponentTime24Hours];
               }
               
               _config.startingDate = selectedDate;
               _config.selectedValue = _txtFieldSelectedDate.text;
               if(!isEmpty(_delegate)){
                   [_delegate didChangeValueFor:_config];
               }
           } cancelBlock:^(ActionSheetDatePicker *picker) {
               
           } origin:sender];
       }
       else{
           [ActionSheetDatePicker showPickerWithTitle:_config.headerTitle datePickerMode:selectedMode selectedDate:startDate minimumDate:_config.minDate maximumDate:_config.maxDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
               if(selectedMode==UIDatePickerModeDate){
                   _txtFieldSelectedDate.text = [DateUtilities getDateStringFromDate:selectedDate
                                                                          withFormat:kDateFormatDefaultStamp];
               }
               else{
                   _txtFieldSelectedDate.text = [DateUtilities getDateStringFromDate:selectedDate
                                                                          withFormat:kDateFormatComponentTime24Hours];
               }
               
               _config.startingDate = selectedDate;
               _config.selectedValue = _txtFieldSelectedDate.text;
               if(!isEmpty(_delegate)){
                   [_delegate didChangeValueFor:_config];
               }
               
           } cancelBlock:^(ActionSheetDatePicker *picker) {
               
           } origin:sender];
       }
       
    }
    
}

@end
