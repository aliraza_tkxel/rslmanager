//
//  DropDownListTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicTableViewCellProtocol.h"
#import "DynamicCellsConfiguration.h"
@interface DropDownListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSelectedVal;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectValue;
@property (weak, nonatomic) id<DynamicTableViewCellProtocol> delegate;

@property DynamicCellsConfiguration *config;

-(void) initializeCellWithConfiguration:(DynamicCellsConfiguration *) configObj;

@end
