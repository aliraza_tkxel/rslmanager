//
//  FreeTextInputTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "FreeTextInputTableViewCell.h"

@implementation FreeTextInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) initializeCellWithConfiguration:(DynamicCellsConfiguration *)configObj{
    _config = configObj;
    _txtFieldValue.text = configObj.selectedValue;
    _txtFieldValue.delegate = self;
    _lblTitle.text = configObj.inputFieldTitle;
    [_txtFieldValue addBottomBorderWithColor:AppThemeColorGrey
                                          andWidth:1.0];
    if(configObj.appliedCellFieldType == DynamicCellsFreeTextFieldEmail){
        _txtFieldValue.keyboardType = UIKeyboardTypeEmailAddress;
        _txtFieldValue.returnKeyType = UIReturnKeyDone;
    }
    else if(configObj.appliedCellFieldType == DynamicCellsFreeTextFieldNumeric){
        _txtFieldValue.keyboardType = UIKeyboardTypeNumberPad;
    }
    else if(configObj.appliedCellFieldType == DynamicCellsFreeTextFieldTypeDecimal){
        _txtFieldValue.keyboardType = UIKeyboardTypeDecimalPad;
    }
    else if(configObj.appliedCellFieldType == DynamicCellsFreeTextFieldTelephone){
        _txtFieldValue.keyboardType = UIKeyboardTypePhonePad;
    }
    else if(configObj.appliedCellFieldType == DynamicCellsFreeTextFieldTypeText){
        _txtFieldValue.keyboardType = UIKeyboardTypeDefault;
        _txtFieldValue.returnKeyType = UIReturnKeyDone;
    }
    if(_config.isNonMutableField){
        _txtFieldValue.enabled = NO;
    }
}


-(void) textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    _config.selectedValue = textField.text;
    if(!isEmpty(_delegate)){
        [_delegate didChangeValueFor:_config];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    _config.selectedValue = textField.text;
    if(!isEmpty(_delegate)){
        [_delegate didChangeValueFor:_config];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
