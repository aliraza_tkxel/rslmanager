//
//  DropDownListTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DropDownListTableViewCell.h"
#import <ActionSheetStringPicker.h>
@implementation DropDownListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) initializeCellWithConfiguration:(DynamicCellsConfiguration *)configObj{
    _config = configObj;
    _txtFieldSelectedVal.text = configObj.selectedValue;
    _lblTitle.text = configObj.inputFieldTitle;
    [_txtFieldSelectedVal addBottomBorderWithColor:AppThemeColorGrey
                                  andWidth:1.0];
    [self sortSourceArray];
}

-(void) sortSourceArray{
    if(!isEmpty(_config.sourceArray)){
        NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:kEntityLookUpVarLookUpDescription ascending:YES comparator:^(NSString *obj1, NSString *obj2) {
            
            return [obj1 compare:obj2 options:NSNumericSearch | NSCaseInsensitiveSearch];
            
        }];
        _config.sourceArray =  [_config.sourceArray sortedArrayUsingDescriptors:@[sd]];
    }
   
}

- (IBAction)btnSelectValueTapped:(id)sender {
    if(!_config.isNonMutableField){
        if(!isEmpty(_config.sourceArray)){
            [ActionSheetStringPicker showPickerWithTitle:_config.headerTitle
                                                    rows:[_config.sourceArray valueForKeyPath:kEntityLookUpVarLookUpDescription]
                                        initialSelection:0
                                               doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                                   _txtFieldSelectedVal.text = selectedValue;
                                                   _config.selectedValue = selectedValue;
                                                   if(!isEmpty(_delegate)){
                                                       [_delegate didChangeValueFor:_config];
                                                   }
                                               }
                                             cancelBlock:^(ActionSheetStringPicker *picker) {
                                                 
                                             }
                                                  origin:sender];
        }
    }
    
}

@end
