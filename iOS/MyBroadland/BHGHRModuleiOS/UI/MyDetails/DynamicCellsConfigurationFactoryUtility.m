//
//  DynamicCellsConfigurationFactoryUtility.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DynamicCellsConfigurationFactoryUtility.h"

@implementation DynamicCellsConfigurationFactoryUtility
#pragma mark - My Details

+(NSMutableArray *) parseEmergencyDetailsDataWithContactData:(UserContactInfoModel *)_contactDetails{
    NSMutableArray *mutArr = [NSMutableArray new];
    DynamicCellsConfiguration *emergencyContactName = [[DynamicCellsConfiguration alloc] init];
    emergencyContactName.inputFieldTitle = @"Emergency Contact (Name):";
    emergencyContactName.selectedValue = _contactDetails.emergencyContactPerson;
    emergencyContactName.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    emergencyContactName.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:emergencyContactName];
    
    DynamicCellsConfiguration *emergencyContactTel = [[DynamicCellsConfiguration alloc] init];
    emergencyContactTel.inputFieldTitle = @"Emergency Contact (Tel):";
    emergencyContactTel.selectedValue = _contactDetails.emergencyContactNumber;
    emergencyContactTel.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    emergencyContactTel.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:emergencyContactTel];
    
    DynamicCellsConfiguration *emergencyContactRelation = [[DynamicCellsConfiguration alloc] init];
    emergencyContactRelation.inputFieldTitle = @"Emergency Contact (Relationship):";
    emergencyContactRelation.selectedValue = _contactDetails.emergencyContactRelationship;
    emergencyContactRelation.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    emergencyContactRelation.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:emergencyContactRelation];
    
    return mutArr;
}

+(NSMutableArray *) parsePersonalDetailsDataWithPersonalData:(UserPersonalInfoModel *)_personalDetails{
    NSMutableArray *mutArr = [NSMutableArray new];
    DynamicCellsConfiguration *firstName = [[DynamicCellsConfiguration alloc] init];
    firstName.inputFieldTitle = @"First Name:";
    firstName.selectedValue = _personalDetails.firstName;
    firstName.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    firstName.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:firstName];
    
    DynamicCellsConfiguration *lastName = [[DynamicCellsConfiguration alloc] init];
    lastName.inputFieldTitle = @"Last Name:";
    lastName.selectedValue = _personalDetails.lastName;
    lastName.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    lastName.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:lastName];
    
    DynamicCellsConfiguration *dob = [[DynamicCellsConfiguration alloc] init];
    dob.inputFieldTitle = @"Date of Birth:";
    dob.selectedValue = _personalDetails.dateOfBirthString;
    dob.startingDate = _personalDetails.dateOfBirth;
    dob.baseTypeForConfig = DynamicCellsBaseTypeDate;
    dob.isNonMutableField = YES;
    dob.headerTitle = kSheetPickerTitleDOB;
    [mutArr addObject:dob];
    
    DynamicCellsConfiguration *gender = [[DynamicCellsConfiguration alloc] init];
    gender.inputFieldTitle = @"Gender:";
    gender.selectedValue = _personalDetails.gender;
    gender.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllGenders];
    gender.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    gender.headerTitle = kSheetPickerTitleGender;
    [mutArr addObject:gender];
    
    DynamicCellsConfiguration *maritalStatus = [[DynamicCellsConfiguration alloc] init];
    maritalStatus.inputFieldTitle = @"Marital Status:";
    maritalStatus.selectedValue = _personalDetails.maritalStatusDescription;
    maritalStatus.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllMaritalStatuses];
    maritalStatus.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    maritalStatus.headerTitle = kSheetPickterTitleMaritalStatus;
    [mutArr addObject:maritalStatus];
    
    DynamicCellsConfiguration *ethnicity = [[DynamicCellsConfiguration alloc] init];
    ethnicity.inputFieldTitle = @"Ethnicity:";
    ethnicity.selectedValue = _personalDetails.ethnicityDescription;
    ethnicity.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllEthnicities];
    ethnicity.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    ethnicity.headerTitle = kSheetPickerTitleEthnicity;
    [mutArr addObject:ethnicity];
    
    DynamicCellsConfiguration *religion = [[DynamicCellsConfiguration alloc] init];
    religion.inputFieldTitle = @"Religion:";
    religion.selectedValue = _personalDetails.religionDescription;
    religion.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllReligions];
    religion.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    religion.headerTitle = kSheetPickerTitleReligion;
    [mutArr addObject:religion];
    
    DynamicCellsConfiguration *sexualOrientation = [[DynamicCellsConfiguration alloc] init];
    sexualOrientation.inputFieldTitle = @"Sexual Orientation:";
    sexualOrientation.selectedValue = _personalDetails.sexualOrientationDescription;
    sexualOrientation.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllSexualOrientations];
    sexualOrientation.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    sexualOrientation.headerTitle = kSheetPickerTitleSexuality;
    [mutArr addObject:sexualOrientation];
    
    return mutArr;
}


+(NSMutableArray *) parseContactDetailsDataWithContactData:(UserContactInfoModel *)_contactDetails{
    NSMutableArray *mutArr = [NSMutableArray new];
    DynamicCellsConfiguration *telWork = [[DynamicCellsConfiguration alloc] init];
    telWork.inputFieldTitle = @"Tel (Work):";
    telWork.selectedValue = _contactDetails.workPhone;
    telWork.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    telWork.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:telWork];
    
    DynamicCellsConfiguration *mobileWork = [[DynamicCellsConfiguration alloc] init];
    mobileWork.inputFieldTitle = @"Mobile (Work):";
    mobileWork.selectedValue = _contactDetails.workMobile;
    mobileWork.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    mobileWork.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:mobileWork];
    
    DynamicCellsConfiguration *emailWork = [[DynamicCellsConfiguration alloc] init];
    emailWork.inputFieldTitle = @"Email (Work):";
    emailWork.selectedValue = _contactDetails.workEmail;
    emailWork.appliedCellFieldType = DynamicCellsFreeTextFieldEmail;
    emailWork.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:emailWork];

    DynamicCellsConfiguration *mobileBCP = [[DynamicCellsConfiguration alloc] init];
    mobileBCP.inputFieldTitle = @"Mobile (BCP):";
    mobileBCP.selectedValue = _contactDetails.mobile;
    mobileBCP.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    mobileBCP.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:mobileBCP];
    
    DynamicCellsConfiguration *telHome = [[DynamicCellsConfiguration alloc] init];
    telHome.inputFieldTitle = @"Tel (Home):";
    telHome.selectedValue = _contactDetails.homePhone;
    telHome.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    telHome.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:telHome];
    
    DynamicCellsConfiguration *mobileHome = [[DynamicCellsConfiguration alloc] init];
    mobileHome.inputFieldTitle = @"Mobile (Personal):";
    mobileHome.selectedValue = _contactDetails.mobilePersonal;
    mobileHome.appliedCellFieldType = DynamicCellsFreeTextFieldTelephone;
    mobileHome.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:mobileHome];
    
    DynamicCellsConfiguration *emailhome = [[DynamicCellsConfiguration alloc] init];
    emailhome.inputFieldTitle = @"Email (Home):";
    emailhome.selectedValue = _contactDetails.homeEmail;
    emailhome.appliedCellFieldType = DynamicCellsFreeTextFieldEmail;
    emailhome.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [mutArr addObject:emailhome];
    
    DynamicCellsConfiguration *address1 = [[DynamicCellsConfiguration alloc] init];
    address1.inputFieldTitle = @"Address 1:";
    address1.selectedValue = _contactDetails.addressLine1;
    address1.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    address1.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:address1];
    
    DynamicCellsConfiguration *address2 = [[DynamicCellsConfiguration alloc] init];
    address2.inputFieldTitle = @"Address 2:";
    address2.selectedValue = _contactDetails.addressLine2;
    address2.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    address2.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:address2];
    
    DynamicCellsConfiguration *address3 = [[DynamicCellsConfiguration alloc] init];
    address3.inputFieldTitle = @"Address 3:";
    address3.selectedValue = _contactDetails.addressLine3;
    address3.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    address3.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:address3];
    
    DynamicCellsConfiguration *postalTown = [[DynamicCellsConfiguration alloc] init];
    postalTown.inputFieldTitle = @"Postal Town:";
    postalTown.selectedValue = _contactDetails.postalTown;
    postalTown.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    postalTown.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:postalTown];
    
    DynamicCellsConfiguration *postalCode = [[DynamicCellsConfiguration alloc] init];
    postalCode.inputFieldTitle = @"Post Code:";
    postalCode.selectedValue = _contactDetails.postalCode;
    postalCode.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    postalCode.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:postalCode];
    
    DynamicCellsConfiguration *county = [[DynamicCellsConfiguration alloc] init];
    county.inputFieldTitle = @"County:";
    county.selectedValue = _contactDetails.county;
    county.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    county.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    [mutArr addObject:county];

    return mutArr;
}


+(void) validatePersonalInfo:(UserPersonalInfoModel*) personalInfo withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock{
    NSString *errorString = @"";
    if(![personalInfo.firstName isValidName]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidFirstName];
        
    }
    if(![personalInfo.lastName isValidName]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidLastName];
        
    }
    if([personalInfo.dateOfBirthString isEmptyOrWhiteSpaceString]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidDoB];
        
    }
    if([personalInfo.gender isEmptyOrWhiteSpaceString]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidGender];
        
    }
    if([personalInfo.maritalStatusDescription isEmptyOrWhiteSpaceString]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidMaritalStatus];
        
    }
    if([personalInfo.ethnicityDescription isEmptyOrWhiteSpaceString]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidEthnicity];
        
    }
    
    if([personalInfo.religionDescription isEmptyOrWhiteSpaceString]){
        
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidReligion];
        
    }
    if([personalInfo.sexualOrientationDescription isEmptyOrWhiteSpaceString]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidSexOrientation];
        
    }

    BOOL isValid = ([errorString isEmptyOrWhiteSpaceString]) ? YES : NO;
    responseBlock(isValid,errorString);
}

+(void) validateContactData:(UserContactInfoModel *) contactInfo withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock{
    NSString *errorString = @"";
    
    if(![contactInfo.workPhone isValidPhone]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidWorkTel];
        
    }
    if(![contactInfo.workMobile isValidPhone]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidWorkMobile];
        
    }
    if(![contactInfo.workEmail isValidEmail]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidWorkEmail];
        
    }
    if(![contactInfo.homePhone isEmptyOrWhiteSpaceString])
    {
        if(![contactInfo.homePhone isValidPhone]){
            
            errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidTelHome];
            
        }
    }
    if(![contactInfo.mobilePersonal isEmptyOrWhiteSpaceString])
    {
        if(![contactInfo.mobilePersonal isValidPhone]){
            
            errorString = [NSString stringWithFormat:@"%@%@\r",errorString,kProfValidationInvalidPersonalMobile];
            
        }
    }
    if(![contactInfo.homeEmail isEmptyOrWhiteSpaceString])
    {
        if(![contactInfo.homeEmail isValidEmail]){
            
            errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidEmailHome];
            
        }
    }
    if(![contactInfo.postalCode isEmptyOrWhiteSpaceString])
    {
        if(![contactInfo.postalCode isValidPostalCode]){
            
            errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidPostalCode];
            
        }
    }
    BOOL isValid = ([errorString isEmptyOrWhiteSpaceString]) ? YES : NO;
    responseBlock(isValid,errorString);
}

+(void) validateEmergencyData:(UserContactInfoModel*)emergencyData withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock{
    NSString *errorString = @"";
    
    
    if(![emergencyData.emergencyContactNumber isValidPhone]){
        
        errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidEmergencyNumber];
        
    }
    if([emergencyData.emergencyContactPerson isEmptyOrWhiteSpaceString]){
        errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidEmergencyContact];
        
    }
    if([emergencyData.emergencyContactRelationship isEmptyOrWhiteSpaceString]){
        errorString = [NSString stringWithFormat:@"%@%@\r\r",errorString,kProfValidationInvalidEmergencyContactRel];
        
    }
    
    BOOL isValid = ([errorString isEmptyOrWhiteSpaceString]) ? YES : NO;
    responseBlock(isValid,errorString);

}

@end
