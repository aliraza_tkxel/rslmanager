//
//  DynamicCellsConfigurationFactoryUtility.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DynamicCellsConfiguration.h"
@interface DynamicCellsConfigurationFactoryUtility : NSObject
//My details

+(NSMutableArray *) parsePersonalDetailsDataWithPersonalData:(UserPersonalInfoModel *)_personalDetails;
+(NSMutableArray *) parseEmergencyDetailsDataWithContactData:(UserContactInfoModel *)_contactDetails;
+(NSMutableArray *) parseContactDetailsDataWithContactData:(UserContactInfoModel *)_contactDetails;

+(void) validateEmergencyData:(UserContactInfoModel*)emergencyData withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock;
+(void) validateContactData:(UserContactInfoModel *) contactInfo withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock;
+(void) validatePersonalInfo:(UserPersonalInfoModel*) personalInfo withResponse:(void(^)(bool isValidInput, NSString * errorMessage)) responseBlock;
@end
