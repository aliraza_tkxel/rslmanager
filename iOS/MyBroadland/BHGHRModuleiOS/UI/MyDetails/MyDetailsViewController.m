//
//  MyDetailsViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MyDetailsViewController.h"
#import "UserProfileManager.h"
#import "UpdateUserProfileResponseObject.h"
#import "DropDownListTableViewCell.h"
#import "DatePickerInputTableViewCell.h"
#import "FreeTextInputTableViewCell.h"
@interface MyDetailsViewController ()


@end

@implementation MyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"DropDownListTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDropDownCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"DatePickerInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDatePickerCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"FreeTextInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicFreeTextCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [_navigationBarView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1];
    [self configureRevealMenu];
    [_navBarIcon addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0];
    [self fetchProfileData];
    [self exitEditingMode];
    
    
    [[HRMFloatingButtonManager sharedInstance] addFloatingButtonsWithSize:1
                                                    andButtonDescriptions:@[@"Edit"]
                                                          andButtonImages:@[[UIImage imageNamed:@"icon_edit_profile.png"]]
                                                        forViewController:self
                                                        withResponseBlock:^(NSUInteger selectedIndex, BOOL success, NSString *error)
     {
         if(success){
             if(selectedIndex==1){
                 if(!_isEditing){
                     _isEditing = YES;
                     [self enterEditingMode];
                 }
                 else{
                     [[HRMFloatingButtonManager sharedInstance] hideButtons];
                     [self showMessageWithHeader:@"" andBody:@"You are already in edit-mode. Please finish your current session first."];
                 }
                 
             }
             
             
         }
         
         
     }];
    
}
-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

#pragma mark - Editing methods

-(void) enterEditingMode{
    [[HRMFloatingButtonManager sharedInstance] hideButtons];
    [self toggleButtons];
}

-(void) exitEditingMode{
    _isEditing = NO;
    [self toggleButtons];
}

-(void) toggleButtons{
    if(_isEditing){
        _editControlsHeight.constant = 60;
        _editingControlsView.hidden = NO;
        [UIView animateWithDuration:0.5
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];

    }
    else{
        _editControlsHeight.constant = 0;
        _editingControlsView.hidden = YES;
        [self disableEditing];
    }
    [self detailsSegmentSelectionChanged:_detailsSegmentView];
    [self.view bringSubviewToFront:[[HRMFloatingButtonManager sharedInstance] plusButtonsViewMain]];
    [_tblView reloadData];

}
-(void) disableEditing{
    [self.view endEditing:YES];
}


#pragma mark - Network Operations

-(void) fetchProfileData{
    UserProfileManager *manager = [UserProfileManager new];
    [self showProgressRingWithMessage:kProgressFetchingProfile];
    [manager fetchUserProfileForId:LoggedInUserId withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        [self exitEditingMode];
        if(success){
            
            [self resetLoggedInUserDetailsWith:response];
            
        }
        else{
            
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}



-(void) updateProfileDataForPersonalInfo:(UserPersonalInfoModel *) personalInfo
                          andContactInfo:(UserContactInfoModel *)contctInfo
                 andEmergencyContactInfo:(UserContactInfoModel *) emergencyContactInfo{
    [self showProgressRingWithMessage:kProgressUpdatingProfileInfo];
    contctInfo.emergencyContactNumber = emergencyContactInfo.emergencyContactNumber;
    contctInfo.emergencyContactPerson = emergencyContactInfo.emergencyContactPerson;
    contctInfo.emergencyContactRelationship = emergencyContactInfo.emergencyContactRelationship;
    UserProfileManager *manager = [UserProfileManager new];
    [manager updateUserProfileWithPersonalInformation:personalInfo andContactInfo:contctInfo withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        [self exitEditingMode];
        if(success){
            UpdateUserProfileResponseObject *respObj = (UpdateUserProfileResponseObject*)response;
            if(respObj.isSuccessFul){
                [LoggedInUser setUserPersonalInfo:personalInfo];
                [LoggedInUser setUserContactInfo:contctInfo];
                [self resetLoggedInUserDetailsWith:nil];
                [self showMessageWithHeader:kResponseStatusSuccess
                                    andBody:respObj.message];
            }
            
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                 andBody:error];
        }
    }];

}

#pragma mark - IBActions

- (IBAction)detailsSegmentSelectionChanged:(id)sender {
    [self.view endEditing:YES];
    _selectedSection = _detailsSegmentView.selectedSegmentIndex;
    [self parseAndFilterProfileData];
    [_tblView reloadData];
}
- (IBAction)btnSaveTapped:(id)sender {
    
    [self.view endEditing:YES];
    [self fetchProfileDataParams];
}
- (IBAction)btnCancelTapped:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kConfirmationCancelEditingProfile preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //[self resetLoggedInUserDetailsWith:nil];
        [self fetchProfileData];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];

    [alertController addAction:okAction];
    [alertController addAction:noAction];
    [self presentViewController:alertController animated: YES completion: nil];
    
}

#pragma mark - Functional

-(void) fetchProfileDataParams{
   
    [DynamicCellsConfigurationFactoryUtility validatePersonalInfo:_personalDetails
                                                     withResponse:^(bool isValidInput, NSString *errorMessage)
    {
        if(isValidInput){
            [DynamicCellsConfigurationFactoryUtility validateContactData:_contactDetails withResponse:^(bool isValidInput2, NSString *errorMessage2)
            {
                if(isValidInput2){
                    [DynamicCellsConfigurationFactoryUtility validateEmergencyData:_emergencyDetails withResponse:^(bool isValidInput3, NSString *errorMessage3) {
                        if(isValidInput3){
                            [self updateProfileDataForPersonalInfo:_personalDetails
                                                    andContactInfo:_contactDetails
                                           andEmergencyContactInfo:_emergencyDetails];
                        }
                        else{
                            [self showMessageWithHeader:kResponseStatusError
                                                andBody:errorMessage3];
                        }
                    }];
                }
                else{
                    [self showMessageWithHeader:kResponseStatusError
                                        andBody:errorMessage2];
                }
            }];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:errorMessage];
        }
    }];
}

-(void) resetLoggedInUserDetailsWith:(id)respObj{
    if(respObj!=nil){
        [LoggedInUser setUserProfile:respObj];
    }
    
    [self exitEditingMode];
    _personalDetails = [[UserModel sharedInstance] userPersonalInfo];
    _contactDetails = [[UserModel sharedInstance] userContactInfo];
    _emergencyDetails = [[UserModel sharedInstance] userContactInfo];
    [self parseAndFilterProfileData];
    [_tblView reloadData];
}

#pragma mark - Datasource

-(void) parseAndFilterProfileData{
    _sourceDict = [NSMutableDictionary new];
    NSMutableArray *personalData = [DynamicCellsConfigurationFactoryUtility parsePersonalDetailsDataWithPersonalData:_personalDetails];
    [_sourceDict setObject:personalData forKey:@"0"];
    NSMutableArray *contactData = [DynamicCellsConfigurationFactoryUtility parseContactDetailsDataWithContactData:_contactDetails];
    [_sourceDict setObject:contactData forKey:@"1"];
    NSMutableArray *emergencyData = [DynamicCellsConfigurationFactoryUtility parseEmergencyDetailsDataWithContactData:_contactDetails];
    [_sourceDict setObject:emergencyData forKey:@"2"];
}


#pragma mark  - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sourceArr = [NSArray new];
    if(_selectedSection == MyDetailsTabPersonal){
        sourceArr = [_sourceDict objectForKey:@"0"];
    }
    else if(_selectedSection == MyDetailsTabContact){
        sourceArr = [_sourceDict objectForKey:@"1"];
    }
    else if(_selectedSection == MyDetailsTabEmergency){
        sourceArr = [_sourceDict objectForKey:@"2"];
    }
    return [sourceArr count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sourceArr = [NSArray new];
    UITableViewCell *cell;
    if(_selectedSection == MyDetailsTabPersonal){
        sourceArr = [_sourceDict objectForKey:@"0"];
    }
    else if(_selectedSection == MyDetailsTabContact){
        sourceArr = [_sourceDict objectForKey:@"1"];
    }
    else if(_selectedSection == MyDetailsTabEmergency){
        sourceArr = [_sourceDict objectForKey:@"2"];
    }
    
    if(!isEmpty(sourceArr)){
        DynamicCellsConfiguration *config = [sourceArr objectAtIndex:indexPath.row];
        config.cellRowId = indexPath.row;
        config.cellSectionId = _selectedSection;
        if(config.baseTypeForConfig == DynamicCellsBaseTypeDate){
            DatePickerInputTableViewCell *customCell = (DatePickerInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDatePickerCellReuseIdentifier];
            customCell.delegate = self;
            config.isNonMutableField = NO;
            [customCell initializeCellWithConfiguration:config];
            customCell.btnSelectDate.userInteractionEnabled = _isEditing;
            customCell.txtFieldSelectedDate.userInteractionEnabled = NO;
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeDropDown){
            DropDownListTableViewCell *customCell = (DropDownListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDropDownCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            customCell.btnSelectValue.userInteractionEnabled = _isEditing;
            customCell.txtFieldSelectedVal.userInteractionEnabled = NO;
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeFreeText){
            FreeTextInputTableViewCell *customCell = (FreeTextInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicFreeTextCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            customCell.txtFieldValue.userInteractionEnabled = _isEditing;
            cell = customCell;
            
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}


#pragma mark  - Delegates

-(void) didChangeValueFor:(DynamicCellsConfiguration *)config{
    if(config.cellSectionId == MyDetailsTabPersonal){
        switch (config.cellRowId) {
            case MyDetailsPersonalInfoFieldFirstName:
                _personalDetails.firstName = config.selectedValue;
                break;
            case MyDetailsPersonalInfoFieldLastName:
                _personalDetails.lastName = config.selectedValue;
                break;
            case MyDetailsPersonalInfoFieldDOB:
                _personalDetails.dateOfBirth = config.startingDate;
                _personalDetails.dateOfBirthString = config.selectedValue;
                break;
            case MyDetailsPersonalInfoFieldGender:
                _personalDetails.gender = config.selectedValue;
                break;
            case MyDetailsPersonalInfoFieldMaritalStatus:
                _personalDetails.maritalStatusDescription = config.selectedValue;
                _personalDetails.maritalStatusId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:config.selectedValue forLookUpType:LookUpTypeMaritalStatus];
                break;
            case MyDetailsPersonalInfoFieldEthnicity:
                _personalDetails.ethnicityDescription = config.selectedValue;
                _personalDetails.ethnicityId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:config.selectedValue forLookUpType:LookUpTypeEthnicity];
                break;
            case MyDetailsPersonalInfoFieldReligion:
                _personalDetails.religionDescription = config.selectedValue;
                _personalDetails.religionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:config.selectedValue forLookUpType:LookUpTypeReligion];
                break;
            case MyDetailsPersonalInfoFieldSexualOrientation:
                _personalDetails.sexualOrientationDescription = config.selectedValue;
                _personalDetails.sexualOrientationId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:config.selectedValue forLookUpType:LookUpTypeSexualOrientation];
                break;
                
            default:
                break;
        }
    }
    else if(config.cellSectionId == MyDetailsTabContact){
        switch (config.cellRowId) {
            case MyDetailsContactInfoFieldTelWork:
                _contactDetails.workPhone = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldMobileWork:
                _contactDetails.workMobile = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldEmailWork:
                _contactDetails.workEmail = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldMobileBCP:
                _contactDetails.mobile = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldTelHome:
                _contactDetails.homePhone = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldMobilePersonal:
                _contactDetails.mobilePersonal = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldEmailHome:
                _contactDetails.homeEmail = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldAddress1:
                _contactDetails.addressLine1 = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldAddress2:
                _contactDetails.addressLine2 = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldAddress3:
                _contactDetails.addressLine3 = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldPostalTown:
                _contactDetails.postalTown = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldPostalCode:
                _contactDetails.postalCode = config.selectedValue;
                break;
            case MyDetailsContactInfoFieldCounty:
                _contactDetails.county = config.selectedValue;
                break;
                
            default:
                break;
        }
        
    }
    else if(config.cellSectionId == MyDetailsTabEmergency){
        switch (config.cellRowId) {
            case MyDetailsEmergencyInfoFieldContactName:
                _contactDetails.emergencyContactPerson = config.selectedValue;
                break;
            case MyDetailsEmergencyInfoFieldContactTel:
                _contactDetails.emergencyContactNumber = config.selectedValue;
                break;
            case MyDetailsEmergencyInfoFieldContactRelation:
                _contactDetails.emergencyContactRelationship = config.selectedValue;
                break;
            default:
                break;
        }
    }
    
    [self parseAndFilterProfileData];
    [_tblView reloadData];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
