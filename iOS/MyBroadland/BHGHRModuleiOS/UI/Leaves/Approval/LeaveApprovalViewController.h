//
//  LeaveApprovalViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"

@interface LeaveApprovalViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource, LeaveDateFilterPickerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPageIcon;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrl;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerFilterHeight;
@property (weak, nonatomic) IBOutlet LeaveDateFilterPicker *dateFilterPickerView;
@property (copy,nonatomic) NSMutableDictionary *sourceDict;
@property (copy, nonatomic) NSArray *sectionsArray;
@property InAppLeaveType controllerLeaveType;
@property BOOL isForTeam;
@end
