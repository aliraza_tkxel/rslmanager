//
//  LeaveApprovalViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveApprovalViewController.h"
#import "LeaveApprovalTableViewCell.h"
#import "LeavesApprovalManager.h"
#import "AnnualLeavesDaysTakenResponseObject.h"
#import "MyTeamDaysRequestedResponseModel.h"
#import "FetchAbsenceLeavesListResponse.h"
#import "GenericLeaveResponse.h"
@interface LeaveApprovalViewController ()

@end

@implementation LeaveApprovalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [self configureRevealMenu];
    [_navigationBarView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1];
    [_imgViewPageIcon addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0];
    _tblView.tableFooterView = [UIView new];
    [_tblView registerNib:[UINib nibWithNibName:@"LeaveApprovalTableViewCell" bundle:nil] forCellReuseIdentifier:kLeaveApprovalTableViewCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.delegate = self;
    _tblView.dataSource = self;
    [_tblView reloadData];
    _isForTeam = NO;
    _dateFilterPickerView.lblSelectedDate.text = [[UserModel sharedInstance] userObject].selectedYearRange;
    _dateFilterPickerView.delegate = self;
    NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self fetchLeaveLists: selectedYear];
    
}
-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark - IBActions
- (IBAction)segmentCtrlFlipped:(id)sender {
    if(_segmentCtrl.selectedSegmentIndex==0){
        _isForTeam = NO;
        _datePickerFilterHeight.constant = 42;
        _dateFilterPickerView.hidden = NO;
        
    }
    else{
        _isForTeam = YES;
        _datePickerFilterHeight.constant = 0;
        _dateFilterPickerView.hidden = YES;
    }
    NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self fetchLeaveLists: selectedYear];
}
#pragma mark - Network Operations

-(void) fetchLeaveLists: (NSString *) year{
    _sourceDict = [NSMutableDictionary new];
    _sectionsArray = [[NSArray alloc] init];
    [_tblView reloadData];
    [self showProgressRingWithMessage:kProgressFetchingDaysTaken];
    LeavesApprovalManager *leavesMgr = [LeavesApprovalManager new];
    [leavesMgr fetchPendingLeavesForUser:LoggedInUserId
                                 forYear: year
                            andLeaveType:_controllerLeaveType
                            andIsForTeam:_isForTeam
                       withResponseBlock:^(BOOL success, id response, NSString *error) {
                           [self hideProgressRing];
                           if(success){
                               [self parseAndFilterDataSource:response];
                           }
                           else{
                               [self showMessageWithHeader:kResponseStatusError
                                                   andBody:error];
                           }
                       }];
}

-(void) applyAction:(NSNumber *)actionType
            onLeave:(DaysTakenModel *) leave
          withNotes:(NSString *) notes{
    [self showProgressRingWithMessage:kProgressApplyingAction];
    LeavesApprovalManager *mgr = [LeavesApprovalManager new];
    [mgr takeActionForLeave:leave.absenceHistoryId
               withActionId:actionType
                   andNotes:notes
           andResponseBlock:^(BOOL success, id response, NSString *error) {
               [self hideProgressRing];
               if(success){
                   GenericLeaveResponse *respObj = (GenericLeaveResponse*) response;
                   if(respObj.isSuccessFul){
                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kResponseStatusSuccess message:respObj.message preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                           NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
                           [self fetchLeaveLists:selectedYear];
                       }];
                       [alertController addAction:okAction];
                       [self presentViewController:alertController animated: YES completion: nil];
                   }
                   else{
                       [self showMessageWithHeader:kResponseStatusError
                                           andBody:respObj.message];
                   }
               }
               else{
                   [self showMessageWithHeader:kResponseStatusError
                                       andBody:error];
               }
               
           }];
}

#pragma mark - General Operations

-(void) parseAndFilterDataSource:(id) responseObj{
    NSArray * dataSource = [NSArray new];
    if([responseObj isKindOfClass:[AnnualLeavesDaysTakenResponseObject class]]){
        AnnualLeavesDaysTakenResponseObject *myTaken = (AnnualLeavesDaysTakenResponseObject*) responseObj;
        dataSource = myTaken.leaveTakenList;
    }
    else if([responseObj isKindOfClass:[MyTeamDaysRequestedResponseModel class]]){
        MyTeamDaysRequestedResponseModel *myTaken = (MyTeamDaysRequestedResponseModel*) responseObj;
        dataSource = myTaken.myTeamLeavesPending;
    }
    else if([responseObj isKindOfClass:[FetchAbsenceLeavesListResponse class]]){
        FetchAbsenceLeavesListResponse *myTaken = (FetchAbsenceLeavesListResponse*) responseObj;
        dataSource = myTaken.absentees;
    }
    if([dataSource count]==0){
        [self showNoResultsFoundView];
        return;
    }
    else{
        [self hideNoResultsFoundView];
    }
    _sourceDict = [NSMutableDictionary new];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"startDate"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [dataSource
                                 sortedArrayUsingDescriptors:sortDescriptors];
    dataSource = [NSArray arrayWithArray:sortedEventArray];
    
    for(id record in dataSource){
        
        DaysTakenModel *model = [DaysTakenModel new];
        if([record isKindOfClass:[LeaveTakenNetworkObject class]]){
            LeaveTakenNetworkObject *netObj = (LeaveTakenNetworkObject *) record;
            [model initWithResponseObject:netObj];
        }
        else if([record isKindOfClass:[AbsenceLeaveRecord class]]){
            AbsenceLeaveRecord *netObj = (AbsenceLeaveRecord *) record;
            [model initWithAbsenceResponseObject:netObj];
        }
        NSMutableArray *arr;
        if([_sourceDict objectForKey:model.groupKey]){
            arr = [_sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [_sourceDict setObject:arr forKey:model.groupKey];
    }
    _sectionsArray = [[NSArray alloc] init];
    _sectionsArray = [_sourceDict allKeys];
    [self sortSectionsArray];
    [_tblView reloadData];
    
}

-(void) sortSectionsArray {
    if(isEmpty(_sectionsArray)){
        return;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for(NSString * str in _sectionsArray){
        NSString *dateStr = [NSString stringWithFormat:@"1/%@",str];
        NSDate *dateObj = [DateUtilities getDateFromString:dateStr withFormat:kDateFormatDefaultStamp];
        [arr addObject:dateObj];
    }
    NSArray *reverseOrderUsingComparator = [arr sortedArrayUsingComparator:
                                            ^(id obj1, id obj2) {
                                                return [obj2 compare:obj1];
                                            }];
    [arr removeAllObjects];
    for(NSDate * date in reverseOrderUsingComparator){
        NSString *str = [DateUtilities getDateStringFromDate:date withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
        [arr addObject:str];
    }
    _sectionsArray = [NSArray arrayWithArray:arr];
}

#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sectionsArray count];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return [arr count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    static NSString *cellId = kLeaveApprovalTableViewCellReuseIdentifier;
    LeaveApprovalTableViewCell *customCell = (LeaveApprovalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    [customCell configureWithDataObject:[sourceArray objectAtIndex:indexPath.row]];
    cell = customCell;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return (arr.count>0)?30:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    
    label.text = [_sectionsArray objectAtIndex:section];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor blackColor]];
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*Allow approval only for my team*/
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    DaysTakenModel *leaveObj = [sourceArray objectAtIndex:indexPath.row];
    if(_segmentCtrl.selectedSegmentIndex==1){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitlePickAction
                                                                                 message:[self leaveInfoDescriptionFor:leaveObj]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNoAction style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        UIAlertAction * approveAct = [UIAlertAction actionWithTitle:kAlertActionApprove style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UITextField *textField = alertController.textFields[0];
            [self approveLeave:leaveObj withNotes:textField.text];
        }];
        UIAlertAction * declineAct = [UIAlertAction actionWithTitle:kAlertActionDecline style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            UITextField *textField = alertController.textFields[0];
            [self declineLeave:leaveObj withNotes:textField.text];
        }];
        
        UIAlertAction * cancelAct = [UIAlertAction actionWithTitle:kAlertCancelLeave style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UITextField *textField = alertController.textFields[0];
            [self cancelLeave:leaveObj withNotes:textField.text];
        }];
        
        [alertController addAction:noAction];
        [alertController addAction:cancelAct];
        [alertController addAction:approveAct];
        [alertController addAction:declineAct];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Add comments here...";
            textField.keyboardType = UIKeyboardTypeDefault;
        }];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else if(_segmentCtrl.selectedSegmentIndex==0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitlePickAction
                                                                                 message:[self leaveInfoDescriptionForCancellation:leaveObj]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNoAction style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
        }];
        UIAlertAction * cancelAct = [UIAlertAction actionWithTitle:kAlertCancelLeave style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UITextField *textField = alertController.textFields[0];
            [self cancelLeave:leaveObj withNotes:textField.text];
            
        }];
        
        [alertController addAction:cancelAct];
        [alertController addAction:noAction];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Add comments here...";
            textField.keyboardType = UIKeyboardTypeDefault;
        }];
        [self presentViewController:alertController animated: YES completion: nil];
    }
}

#pragma mark - Leave Approval

-(void) cancelLeave:(DaysTakenModel *) leaveObj withNotes:(NSString *) notes{
    if([leaveObj.status isEqualToString:kLeaveStatusPending]){
        if([DateUtilities isDate:leaveObj.startDate laterThan:[NSDate date]] || [DateUtilities isDate:leaveObj.startDate SameAs:[NSDate date]]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kAlertMessageCancelLeave preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                NSNumber *actionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kAlertActionCancel
                                                                                      forLookUpType:LookUpTypeLeaveActions];
                [self applyAction:actionId
                          onLeave:leaveObj
                        withNotes:notes];
            }];
            UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                
            }];
            
            [alertController addAction:okAction];
            [alertController addAction:noAction];
            [self presentViewController:alertController animated: YES completion: nil];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationCancelPastLeaves];
        }
    }
    else{
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationCancelNonPendingLeave];
    }
}

-(NSString *) leaveInfoDescriptionForCancellation:(DaysTakenModel*) leaveObj{
    NSString *descriptionStr =  [NSString stringWithFormat:@"\r%@\r\rStart: %@\rEnd: %@\rDuration:%@\rNature: %@\r", kAlertMessageApproval,leaveObj.startDateDescription,leaveObj.endDateDescription,leaveObj.durationDescription, leaveObj.leaveDescription];
    return descriptionStr;
}

-(NSString *) leaveInfoDescriptionFor:(DaysTakenModel*) leaveObj{
   NSString *descriptionStr =  [NSString stringWithFormat:@"\r%@\r\rApplicant: %@\rStart: %@\rEnd: %@\rDuration:%@\rNature: %@\r", kAlertMessageApproval,leaveObj.applicantName,leaveObj.startDateDescription,leaveObj.endDateDescription,leaveObj.durationDescription, leaveObj.leaveDescription];
    return descriptionStr;
}

-(void) approveLeave:(DaysTakenModel *) leaveObj withNotes:(NSString *) notes{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kAlertMessageApproveLeave preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSNumber *actionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kAlertActionApprove
                                                                              forLookUpType:LookUpTypeLeaveActions];
        [self applyAction:actionId
                  onLeave:leaveObj
                withNotes:notes];
        
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:noAction];
    
    [self presentViewController:alertController animated: YES completion: nil];
}
-(void) declineLeave:(DaysTakenModel *) leaveObj withNotes:(NSString *) notes{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kAlertMessageDeclineLeave preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSNumber *actionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kAlertActionDecline
                                                                              forLookUpType:LookUpTypeLeaveActions];
        [self applyAction:actionId
                  onLeave:leaveObj
                withNotes:notes];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:noAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

#pragma mark - Delegate

-(void) didSelectYearRange:(NSString *)rangeValue {
    [[UserModel sharedInstance] userObject].selectedYearRange = rangeValue;
    _dateFilterPickerView.lblSelectedDate.text = rangeValue;
    NSString *selectedYear = [[rangeValue componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self fetchLeaveLists:selectedYear];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
