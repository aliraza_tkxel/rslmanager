//
//  LeaveApprovalTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveApprovalTableViewCell.h"

@implementation LeaveApprovalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) configureWithDataObject:(DaysTakenModel *) obj{
    _lblDuration.text = obj.durationDescription;
    _lblStartDate.text = obj.startDateDescription;
    _lblEndDate.text = obj.endDateDescription;
    _lblStatus.text = obj.status;
    _lblReason.text = obj.leaveDescription;
    if([obj.status isEqualToString:kLeaveStatusPending]){
        [_lblStatus setTextColor:UIColorFromRGB(221, 138, 47)];
    }
    else if([obj.status isEqualToString:kLeaveStatusApproved]){
        [_lblStatus setTextColor:UIColorFromRGB(139, 188, 79)];
    }
    else if([obj.status isEqualToString:kLeaveStatusRejected] || [obj.status isEqualToString:kLeaveStatusCancelled] || [obj.status isEqualToString:kLeaveStatusDeclined]){
        [_lblStatus setTextColor:UIColorFromRGB(203, 38, 40)];
    }
    else{
        [_lblStatus setTextColor:[UIColor blackColor]];
    }
    _lblApplicant.text = obj.applicantName;
    _applicantInfoView.hidden = isEmpty(obj.applicantName);
    
}
@end
