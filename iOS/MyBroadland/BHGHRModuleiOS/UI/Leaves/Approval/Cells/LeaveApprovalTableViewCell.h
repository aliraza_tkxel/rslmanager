//
//  LeaveApprovalTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DaysTakenModel.h"
@interface LeaveApprovalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblApplicant;
@property (weak, nonatomic) IBOutlet UIView *applicantInfoView;
@property (weak, nonatomic) IBOutlet UIView *startDateView;
@property (weak, nonatomic) IBOutlet UIView *reasonView;
@property (weak, nonatomic) IBOutlet UIView *endDateView;
@property (weak, nonatomic) IBOutlet UIView *durationView;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIView *applicantNameView;

-(void) configureWithDataObject:(DaysTakenModel *) obj;
@end
