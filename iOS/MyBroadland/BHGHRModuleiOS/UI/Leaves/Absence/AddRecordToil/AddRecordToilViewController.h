//
//  AddRecordToilViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
#import "DynamicCellsConfiguration.h"
#import "DynamicTableViewCellProtocol.h"
#import "DropDownListTableViewCell.h"
#import "DatePickerInputTableViewCell.h"
#import "FreeTextInputTableViewCell.h"
#import "AddRecordToilRequest.h"
@interface AddRecordToilViewController : BaseViewController<DynamicTableViewCellProtocol,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UILabel *lblPageTitle;
@property (copy,nonatomic) NSMutableArray *sourceArray;
@property AddRecordToilScreenType selectedScreenType;
@property AddRecordToilRequest* requestObj;
@property NSNumber *toilOwed;
@end
