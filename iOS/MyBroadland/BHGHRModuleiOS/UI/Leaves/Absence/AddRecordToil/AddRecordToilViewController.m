//
//  AddRecordToilViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AddRecordToilViewController.h"
#import "AbsenceLeaveManager.h"
@interface AddRecordToilViewController ()

@end

@implementation AddRecordToilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey andWidth:1];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"DropDownListTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDropDownCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"DatePickerInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDatePickerCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"FreeTextInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicFreeTextCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _requestObj = [AddRecordToilRequest new];
    _requestObj.recordedBy = [NSNumber numberWithInt:LoggedInUserId];
    if(_selectedScreenType == AddRecordToilScreenTypeRecording){
        _requestObj.leaveType = kToilLeaveTypeRequest;
        _requestObj.employeeId = _requestObj.recordedBy;
        _lblPageTitle.text = @"Request TOIL";
    }
    else{
        _requestObj.leaveType = kToilLeaveTypeTopUp;
        _lblPageTitle.text = @"Add TOIL";
    }
    
    
    [self configureFormFields];
}

-(void) configureFormFields{
    _sourceArray = [NSMutableArray new];
    if(_selectedScreenType==AddRecordToilScreenTypeTopUp){
        DynamicCellsConfiguration *staffMember = [[DynamicCellsConfiguration alloc] init];
        staffMember.inputFieldTitle = @"Staff Member:";
        staffMember.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllStaffMembers];
        staffMember.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
        staffMember.headerTitle = kSheetPickerStaffMember;
        staffMember.selectedValue = _requestObj.employeeNameRaw;
        [_sourceArray addObject:staffMember];
    }
    
    
    DynamicCellsConfiguration *startDate = [[DynamicCellsConfiguration alloc] init];
    startDate.inputFieldTitle = @"Start Date:";
    startDate.selectedValue = _requestObj.startDate;
    startDate.baseTypeForConfig = DynamicCellsBaseTypeDate;
    [_sourceArray addObject:startDate];
    
    
    DynamicCellsConfiguration *fromTime = [[DynamicCellsConfiguration alloc] init];
    fromTime.inputFieldTitle = @"From:";
    fromTime.selectedValue = _requestObj.from;
    fromTime.baseTypeForConfig = DynamicCellsBaseTypeTime;
    [_sourceArray addObject:fromTime];
    
    if(_selectedScreenType==AddRecordToilScreenTypeTopUp){
        DynamicCellsConfiguration *startTime = [[DynamicCellsConfiguration alloc] init];
        startTime.inputFieldTitle = @"To:";
        startTime.selectedValue = _requestObj.to;
        startTime.baseTypeForConfig = DynamicCellsBaseTypeTime;
        [_sourceArray addObject:startTime];
    }
    
    
    DynamicCellsConfiguration *duration = [[DynamicCellsConfiguration alloc] init];
    duration.inputFieldTitle = @"Duration:";
    duration.selectedValue = [[_requestObj duration] stringValue];
    duration.appliedCellFieldType = DynamicCellsFreeTextFieldTypeDecimal;
    duration.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    if(_selectedScreenType==AddRecordToilScreenTypeTopUp){
        duration.isNonMutableField = YES;
    }
    [_sourceArray addObject:duration];
    
    DynamicCellsConfiguration *recorder = [[DynamicCellsConfiguration alloc] init];
    recorder.inputFieldTitle = @"Recorded By:";
    recorder.isNonMutableField = YES;
    if(!isEmpty([[UserModel sharedInstance] userPersonalInfo])){
        recorder.selectedValue = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedInstance] userPersonalInfo].firstName,[[UserModel sharedInstance] userPersonalInfo].lastName];
        
    }
    else{
        recorder.selectedValue = [[UserModel sharedInstance] userObject].employeeFullName;
        
    }
    recorder.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllStaffMembers];
    recorder.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    [_sourceArray addObject:recorder];
    
    [_tblView reloadData];
    
    
}



#pragma mark  - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    return [_sourceArray count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if(!isEmpty(_sourceArray)){
        DynamicCellsConfiguration *config = [_sourceArray objectAtIndex:indexPath.row];
        config.cellRowId = indexPath.row;
        if(config.baseTypeForConfig == DynamicCellsBaseTypeDate || config.baseTypeForConfig == DynamicCellsBaseTypeTime){
            DatePickerInputTableViewCell *customCell = (DatePickerInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDatePickerCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeDropDown){
            DropDownListTableViewCell *customCell = (DropDownListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDropDownCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeFreeText){
            FreeTextInputTableViewCell *customCell = (FreeTextInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicFreeTextCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

#pragma mark  - Operations

-(void) calculateLeaveDuration{
    if(!isEmpty(_requestObj.startTimeRaw) && !isEmpty(_requestObj.endTimeRaw)){
        _requestObj.duration = [DateUtilities calculateHoursBetween:_requestObj.startTimeRaw and:_requestObj.endTimeRaw];
    }
    
}

#pragma mark  - IBActions

- (IBAction)btnSaveTapped:(id)sender {
    [self.view endEditing:YES];
    if(_selectedScreenType==AddRecordToilScreenTypeTopUp){
        if(isEmpty(_requestObj.employeeId)){
            [self showMessageWithHeader:kResponseStatusError andBody:kValidationStaffMemberSelection];
            return;
        }
    }
    if(isEmpty(_requestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError andBody:kValidationSelectStartDateBody];
        return;
    }
    if([DateUtilities checkIfDateLiesOnWeekend:_requestObj.startDateRaw]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStartDateWeekend];
        return;
    }
    if(isEmpty(_requestObj.from)){
        [self showMessageWithHeader:kResponseStatusError andBody:kValidationStartTime];
        return;
    }
    if(_selectedScreenType==AddRecordToilScreenTypeRecording)
    {
        if([_toilOwed doubleValue]<[_requestObj.duration doubleValue]){
            [self showMessageWithHeader:kResponseStatusError andBody:kAddToilInsufficientQuota];
            return;
        }
    }
    
    
    if(_selectedScreenType==AddRecordToilScreenTypeTopUp){
        if(isEmpty(_requestObj.to)){
            [self showMessageWithHeader:kResponseStatusError andBody:kValidationEndTime];
            return;
        }
        
        if([DateUtilities isDate:_requestObj.startTimeRaw
                     EarlierThan:_requestObj.endTimeRaw]){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationInvalidStartTimePast];
            return;
        }
    }
    [self showProgressRingWithMessage:kProgressRecordingToil];
    AbsenceLeaveManager *mgr = [[AbsenceLeaveManager alloc] init];
    [mgr recordAddToilForRequest:_requestObj withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
        if(success){
            if(respObj.isSuccessFul){
                [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                             andBody:respObj.message];
            }
            else{
                [self showMessageWithHeader:kResponseStatusError
                                    andBody:respObj.message];
            }
        }
        else{
            [self showMessageWithHeader:kDomainRecordAbsence
                                andBody:error];
        }
    }];
    
    
}
- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  - Delegates

-(void) didChangeValueFor:(DynamicCellsConfiguration *)config{
    if(_selectedScreenType == AddRecordToilScreenTypeRecording){
        switch (config.cellRowId) {
            case AbsenceRecordToilFieldStartDate:
                _requestObj.startDate = config.selectedValue;
                _requestObj.startDateRaw = config.startingDate;
                break;
            case AbsenceRecordToilFieldStartTime:
                _requestObj.from = config.selectedValue;
                _requestObj.startTimeRaw = config.startingDate;
                break;
            case AbsenceRecordToilFieldDuration:
                _requestObj.duration = [NSNumber numberWithDouble:[config.selectedValue doubleValue]];
                break;
            case AbsenceRecordToilFieldRecordedBy:
                _requestObj.recordedBy = [NSNumber numberWithDouble:[config.selectedValue doubleValue]];
                break;
            default:
                break;
        }
    }
    else{
        switch (config.cellRowId) {
            case AbsenceAddToilFieldStaffMember:
                _requestObj.employeeId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:config.selectedValue forLookUpType:LookUpTypeMyStaff];
                _requestObj.employeeNameRaw = config.selectedValue;
                break;
            case  AbsenceAddToilFieldStartDate:
                _requestObj.startDate = config.selectedValue;
                break;
            case AbsenceAddToilFieldStartTime:
                _requestObj.from = config.selectedValue;
                _requestObj.startTimeRaw = config.startingDate;
                [self calculateLeaveDuration];
                break;
            case AbsenceAddToilFieldEndTime:
                _requestObj.to = config.selectedValue;
                _requestObj.endTimeRaw = config.startingDate;
                [self calculateLeaveDuration];
                break;
            case AbsenceAddToilFieldDuration:
                _requestObj.duration =[NSNumber numberWithDouble:[config.selectedValue doubleValue]];
                break;
            case AbsenceAddToilFieldRecordedBy:
                _requestObj.recordedBy = [NSNumber numberWithDouble:[config.selectedValue doubleValue]];
                break;
            default:
                break;
        }
    }
    [self configureFormFields];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
