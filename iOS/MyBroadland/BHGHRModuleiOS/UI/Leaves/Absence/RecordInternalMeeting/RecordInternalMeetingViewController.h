//
//  RecordInternalMeetingViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 23/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseViewController.h"
#import "DynamicCellsConfiguration.h"
#import "DynamicTableViewCellProtocol.h"
#import "DropDownListTableViewCell.h"
#import "DatePickerInputTableViewCell.h"
#import "FreeTextInputTableViewCell.h"
#import "AddInternalMeetingLeaveRequest.h"
#import "AbsenceLeaveManager.h"
#import "GenericLeaveResponse.h"
@interface RecordInternalMeetingViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,DynamicTableViewCellProtocol>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (copy,nonatomic) NSMutableArray *sourceArray;
@property AddInternalMeetingLeaveRequest* requestObj;
@end
