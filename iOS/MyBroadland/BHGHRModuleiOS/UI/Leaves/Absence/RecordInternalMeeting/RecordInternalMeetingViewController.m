//
//  RecordInternalMeetingViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 23/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "RecordInternalMeetingViewController.h"

@interface RecordInternalMeetingViewController ()

@end

@implementation RecordInternalMeetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey andWidth:1];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"DropDownListTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDropDownCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"DatePickerInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDatePickerCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"FreeTextInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicFreeTextCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    
    [self configureFormFields];
}
-(void) configureFormFields{
    _sourceArray = [NSMutableArray new];
    
    DynamicCellsConfiguration *startDate = [[DynamicCellsConfiguration alloc] init];
    startDate.inputFieldTitle = @"Start Date:";
    startDate.selectedValue = _requestObj.startDate;
    startDate.baseTypeForConfig = DynamicCellsBaseTypeDate;
    [_sourceArray addObject:startDate];
    
    
    DynamicCellsConfiguration *fromTime = [[DynamicCellsConfiguration alloc] init];
    fromTime.inputFieldTitle = @"From:";
    fromTime.selectedValue = _requestObj.from;
    fromTime.baseTypeForConfig = DynamicCellsBaseTypeTime;
    [_sourceArray addObject:fromTime];
    
    DynamicCellsConfiguration *startTime = [[DynamicCellsConfiguration alloc] init];
    startTime.inputFieldTitle = @"To:";
    startTime.selectedValue = _requestObj.to;
    startTime.baseTypeForConfig = DynamicCellsBaseTypeTime;
    [_sourceArray addObject:startTime];
    
    
    DynamicCellsConfiguration *duration = [[DynamicCellsConfiguration alloc] init];
    duration.inputFieldTitle = @"Duration:";
    duration.selectedValue = [[_requestObj duration] stringValue];
    duration.appliedCellFieldType = DynamicCellsFreeTextFieldNumeric;
    duration.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    duration.isNonMutableField = YES;
    [_sourceArray addObject:duration];
    
    DynamicCellsConfiguration *notes = [[DynamicCellsConfiguration alloc] init];
    notes.inputFieldTitle = @"Notes:";
    notes.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    notes.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    notes.cellRowId = RecordInternalMeetingFieldNotes;
    [_sourceArray addObject:notes];
    
    DynamicCellsConfiguration *recorder = [[DynamicCellsConfiguration alloc] init];
    recorder.inputFieldTitle = @"Recorded By:";
    recorder.isNonMutableField = YES;
    if(!isEmpty([[UserModel sharedInstance] userPersonalInfo])){
        recorder.selectedValue = [NSString stringWithFormat:@"%@ %@",[[UserModel sharedInstance] userPersonalInfo].firstName,[[UserModel sharedInstance] userPersonalInfo].lastName];
        
    }
    else{
        recorder.selectedValue = [[UserModel sharedInstance] userObject].employeeFullName;
        
    }
    recorder.sourceArray = [[LookUpsDataManager sharedInstance] fetchAllStaffMembers];
    recorder.baseTypeForConfig = DynamicCellsBaseTypeDropDown;
    
    _requestObj = [AddInternalMeetingLeaveRequest new];
    _requestObj.duration = [NSNumber numberWithFloat:0.00];
    _requestObj.recordedBy = [NSNumber numberWithInt:LoggedInUserId];
    _requestObj.employeeId = [NSNumber numberWithInt:LoggedInUserId];
    _requestObj.recorderNameRaw = recorder.selectedValue;
    _requestObj.employeeNameRaw = recorder.selectedValue;
    _requestObj.startDateRaw = [NSDate date];
    _requestObj.leaveType = @"F";
    
    [_tblView reloadData];
    
    
}
-(void) calculateLeaveDuration{
    if(!isEmpty(_requestObj.startTimeRaw) && !isEmpty(_requestObj.endTimeRaw)){
        _requestObj.duration = [DateUtilities calculateHoursBetween:_requestObj.startTimeRaw and:_requestObj.endTimeRaw];
        
        DynamicCellsConfiguration *config = [_sourceArray objectAtIndex:RecordInternalMeetingFieldDuration];
        config.selectedValue = [NSString stringWithFormat:@"%.2f",[_requestObj.duration floatValue]];
        
        [_sourceArray replaceObjectAtIndex:RecordInternalMeetingFieldDuration withObject:config];
        [_tblView reloadData];
    }
    
}
#pragma mark  - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_sourceArray count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    if(!isEmpty(_sourceArray)){
        DynamicCellsConfiguration *config = [_sourceArray objectAtIndex:indexPath.row];
        config.cellRowId = indexPath.row;
        if(config.baseTypeForConfig == DynamicCellsBaseTypeDate || config.baseTypeForConfig == DynamicCellsBaseTypeTime){
            DatePickerInputTableViewCell *customCell = (DatePickerInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDatePickerCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeDropDown){
            DropDownListTableViewCell *customCell = (DropDownListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDropDownCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
        else if(config.baseTypeForConfig == DynamicCellsBaseTypeFreeText){
            FreeTextInputTableViewCell *customCell = (FreeTextInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicFreeTextCellReuseIdentifier];
            customCell.delegate = self;
            [customCell initializeCellWithConfiguration:config];
            cell = customCell;
            
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - Dyno Cell Delegate
-(void) didChangeValueFor:(DynamicCellsConfiguration *)config{
    switch (config.cellRowId) {
        case RecordInternalMeetingFieldStartDate:
            _requestObj.startDate = config.selectedValue;
            _requestObj.startDateRaw = config.startingDate;
            [self calculateLeaveDuration];
            break;
        case RecordInternalMeetingFieldStartTime:
            _requestObj.from = config.selectedValue;
            _requestObj.startTimeRaw = config.startingDate;
            [self calculateLeaveDuration];
            break;
        case RecordInternalMeetingFieldEndTime:
            _requestObj.to = config.selectedValue;
            _requestObj.endTimeRaw = config.startingDate;
            [self calculateLeaveDuration];
            break;
        case RecordInternalMeetingFieldNotes:
            _requestObj.notes = config.selectedValue;
            break;
        default:
            break;
    }
}
#pragma mark - IBActions

- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSaveTapped:(id)sender {
    if(isEmpty(_requestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError andBody:kValidationSelectStartDateBody];
        return;
    }
    if([_requestObj.duration doubleValue]<0.50){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationInvalidDuration];
        return;
    }
    
    [self showProgressRingWithMessage:kProgressRecordingInternalMeeting];
    AbsenceLeaveManager *mgr = [[AbsenceLeaveManager alloc] init];
    [mgr recordInternalMeetingLeave:_requestObj withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
        if(success){
            if(respObj.isSuccessFul){
                [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                             andBody:respObj.message];
            }
            else{
                [self showMessageWithHeader:kResponseStatusError
                                    andBody:respObj.message];
            }
        }
        else{
            [self showMessageWithHeader:kDomainRecordAbsence
                                andBody:error];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
