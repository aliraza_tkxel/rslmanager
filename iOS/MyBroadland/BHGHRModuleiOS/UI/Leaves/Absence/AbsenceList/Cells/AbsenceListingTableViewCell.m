//
//  AbsenceListingTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbsenceListingTableViewCell.h"

@implementation AbsenceListingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) configureCellForRecord:(AbsenceLeaveRecordModel *)model{
    _lblReason.text = model.leaveNature;
    _lblEndDate.text = model.endDateDescription;
    _lblDuration.text = model.durationDescription;
    _lblStartDate.text = model.startDateDescription;
    _lblStatus.text = model.leaveStatus;
    
    if([model.leaveStatus isEqualToString:kLeaveStatusPending]){
        [_lblStatus setTextColor:UIColorFromRGB(221, 138, 47)];
    }
    else if([model.leaveStatus isEqualToString:kLeaveStatusApproved]){
        [_lblStatus setTextColor:UIColorFromRGB(139, 188, 79)];
    }
    else if([model.leaveStatus isEqualToString:kLeaveStatusRejected] || [model.leaveStatus isEqualToString:kLeaveStatusCancelled] || [model.leaveStatus isEqualToString:kLeaveStatusDeclined]){
        [_lblStatus setTextColor:UIColorFromRGB(203, 38, 40)];
    }
    else{
        [_lblStatus setTextColor:[UIColor blackColor]];
    }
}


@end
