//
//  AbsenceListingTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbsenceLeaveRecordModel.h"
@interface AbsenceListingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
-(void) configureCellForRecord:(AbsenceLeaveRecordModel*) model;
@end
