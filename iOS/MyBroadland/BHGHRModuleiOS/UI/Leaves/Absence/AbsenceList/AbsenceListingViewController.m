//
//  AbsenceListingViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbsenceListingViewController.h"
#import "AbsenceListingTableViewCell.h"
#import "AbsenceLeaveManager.h"
#import "LeaveApprovalTableViewCell.h"
#import "FetchAbsenceLeavesListResponse.h"
#import "AbsenceLeaveRecord.h"
#import "AddRecordToilViewController.h"
#import "LeavesApprovalManager.h"
#import "GenericLeaveResponse.h"
@interface AbsenceListingViewController ()

@end

@implementation AbsenceListingViewController

-(void) viewWillAppear:(BOOL)animated{
    if (isEmpty([[UserModel sharedInstance] userObject].selectedYearRange)) {
        [self fetchAbsenceList: nil];
    }
    else {
        NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
        [self fetchAbsenceList: selectedYear];
    }
    
    _willTopUpToil = NO;
    [[HRMFloatingButtonManager sharedInstance] hideButtons];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init

-(void) initModule{
    [self configureRevealMenu];
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1];
    [_imgViewPageIcon addCompleteBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0];
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = YES;
    [_tblView registerNib:[UINib nibWithNibName:@"LeaveApprovalTableViewCell" bundle:nil] forCellReuseIdentifier:kLeaveApprovalTableViewCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.delegate = self;
    _tblView.dataSource = self;
    [_tblView reloadData];
    
    _dateFilterPicker.lblSelectedDate.text = [[UserModel sharedInstance] userObject].selectedYearRange;
    _dateFilterPicker.delegate = self;
    
}

-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnSideMenu addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void) configureFloatingButton{
    
    NSArray * teamMembers = [[LookUpsDataManager sharedInstance] fetchAllStaffMembers];
    _isLineManager = ([teamMembers count]>0);
    NSString *recordToilTitle = [NSString stringWithFormat:@"Request Toil (%.2f Hours)",[_toilOwed doubleValue]];
    if(!_isLineManager){
        [[HRMFloatingButtonManager sharedInstance] addFloatingButtonsWithSize:3
                                                        andButtonDescriptions:@[recordToilTitle,@"Request Absence",@"Record Internal Meeting"]
                                                              andButtonImages:@[[UIImage imageNamed:@"icon_toil_generic.png"],[UIImage imageNamed:@"icon_request_absence.png"],[UIImage imageNamed:@"plus_icon.png"]]
                                                            forViewController:self
                                                            withResponseBlock:^(NSUInteger selectedIndex, BOOL success, NSString *error)
         {
             if(success){
                 if(selectedIndex==3){
                     [self performSegueWithIdentifier:kSegueFromAbsenceToRecordInternalMeeting sender:self];
                 }
                 if(selectedIndex==2){
                     [self performSegueWithIdentifier:kSegueFromAbsenceToRecordAbsence sender:self];
                 }
                 else if(selectedIndex==1){
                     if([_toilOwed integerValue]>0){
                         [self performSegueWithIdentifier:kSegueFromAbsenceToAddRecordToil sender:self];
                     }
                     else{
                         [self showMessageWithHeader:kResponseStatusError andBody:kAddToilInsufficientQuota];
                     }
                 }
                 
                 
             }
             
         }];
    }
    else{
        [[HRMFloatingButtonManager sharedInstance] addFloatingButtonsWithSize:4
                                                        andButtonDescriptions:@[@"Add Toil",recordToilTitle,@"Request Absence",@"Record Internal Meeting"]
                                                              andButtonImages:@[[UIImage imageNamed:@"plus_icon.png"],[UIImage imageNamed:@"icon_toil_generic.png"],[UIImage imageNamed:@"icon_request_absence.png"],[UIImage imageNamed:@"plus_icon.png"]]
                                                            forViewController:self
                                                            withResponseBlock:^(NSUInteger selectedIndex, BOOL success, NSString *error)
         {
             if(success){
                 if(selectedIndex==4){
                     [self performSegueWithIdentifier:kSegueFromAbsenceToRecordInternalMeeting sender:self];
                 }
                 if(selectedIndex==3){
                     [self performSegueWithIdentifier:kSegueFromAbsenceToRecordAbsence sender:self];
                 }
                 else if(selectedIndex==2){
                     if([_toilOwed integerValue]>0){
                         [self performSegueWithIdentifier:kSegueFromAbsenceToAddRecordToil sender:self];
                     }
                     else{
                         [self showMessageWithHeader:kResponseStatusError andBody:kAddToilInsufficientQuota];
                     }
                     
                 }
                 else if(selectedIndex==1){
                     _willTopUpToil = YES;
                     [self performSegueWithIdentifier:kSegueFromAbsenceToAddRecordToil sender:self];
                 }
                 
                 
             }
             
             
         }];
    }
    

}

#pragma mark - Network Operations and DataSource

-(void) fetchAbsenceList: (NSString *) year{
    AbsenceLeaveManager *manager = [AbsenceLeaveManager new];
    [self showProgressRingWithMessage:kProgressFetchingAbsenceLeaves];
    [manager fetchAbsenceLeavesListForUserId:LoggedInUserId forYear: year withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            FetchAbsenceLeavesListResponse * respObj = (FetchAbsenceLeavesListResponse *) response;
            _toilOwed = respObj.toilsOwed;
            [self configureFloatingButton];
            if(isEmpty(respObj.absentees)){
                [self showMessageWithHeader:kDomainAbsence
                                    andBody:kNoLeavesFound];
                _sourceDict = [NSMutableDictionary new];
                _sectionsArray = [[NSArray alloc] init];
                [_tblView reloadData];
            }
            else{
                [self parseAndFilterDataSource:respObj.absentees];
                
            }
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}

-(void) parseAndFilterDataSource:(NSArray *) dataSource{
    _sourceDict = [NSMutableDictionary new];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"startDate"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [dataSource
                                 sortedArrayUsingDescriptors:sortDescriptors];
    dataSource = [NSArray arrayWithArray:sortedEventArray];
    
    for(AbsenceLeaveRecord *record in dataSource){
        DaysTakenModel *model = [DaysTakenModel new];
        [model initWithAbsenceResponseObject:record];
        
        
        NSMutableArray *arr;
        if([_sourceDict objectForKey:model.groupKey]){
            arr = [_sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [_sourceDict setObject:arr forKey:model.groupKey];
    }
    _sectionsArray = [[NSArray alloc] init];
    _sectionsArray = [_sourceDict allKeys];
    [self sortSectionsArray];
    [_tblView reloadData];
    
}

-(void) sortSectionsArray {
    if(isEmpty(_sectionsArray)){
        return;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for(NSString * str in _sectionsArray){
        NSString *dateStr = [NSString stringWithFormat:@"1/%@",str];
        NSDate *dateObj = [DateUtilities getDateFromString:dateStr withFormat:kDateFormatDefaultStamp];
        [arr addObject:dateObj];
    }
    NSArray *reverseOrderUsingComparator = [arr sortedArrayUsingComparator:
                                            ^(id obj1, id obj2) {
                                                return [obj2 compare:obj1]; // note reversed comparison here
                                            }];
    [arr removeAllObjects];
    for(NSDate * date in reverseOrderUsingComparator){
        NSString *str = [DateUtilities getDateStringFromDate:date withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
        [arr addObject:str];
    }
    _sectionsArray = [NSArray arrayWithArray:arr];
}



#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sectionsArray count];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return [arr count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    static NSString *cellId = kLeaveApprovalTableViewCellReuseIdentifier;
    LeaveApprovalTableViewCell *customCell = (LeaveApprovalTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    [customCell configureWithDataObject:[sourceArray objectAtIndex:indexPath.row]];
    cell = customCell;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return (arr.count>0)?30:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    
    label.text = [_sectionsArray objectAtIndex:section];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor blackColor]];
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    DaysTakenModel *model = [sourceArray objectAtIndex:indexPath.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitlePickAction
                                                                             message:[self leaveInfoDescriptionFor:model]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNoAction style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        
    }];
    UIAlertAction * cancelAct = [UIAlertAction actionWithTitle:kAlertCancelLeave style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UITextField *textField = alertController.textFields[0];
        [self cancelLeave:model withNotes:textField.text];
        
    }];
    
    [alertController addAction:cancelAct];
    [alertController addAction:noAction];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Add comments here...";
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [self presentViewController:alertController animated: YES completion: nil];
    
}

#pragma mark - Leave Status Changing

-(NSString *) leaveInfoDescriptionFor:(DaysTakenModel*) leaveObj{
    NSString *descriptionStr =  [NSString stringWithFormat:@"\r%@\r\rStart: %@\rEnd: %@\rDuration:%@\rNature: %@\r", kAlertMessageApproval,leaveObj.startDateDescription,leaveObj.endDateDescription,leaveObj.durationDescription, leaveObj.natureDescription];
    return descriptionStr;
}

-(void) cancelLeave:(DaysTakenModel *) leaveObj withNotes:(NSString *) notes{
    if([DateUtilities isDate:leaveObj.startDate laterThan:[NSDate date]] || [DateUtilities isDate:leaveObj.startDate SameAs:[NSDate date]]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kAlertMessageCancelLeave preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSNumber *actionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kAlertActionCancel
                                                                                  forLookUpType:LookUpTypeLeaveActions];
            [self applyAction:actionId
                      onLeave:leaveObj
                    withNotes:notes];
        }];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:noAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationCancelPastLeaves];
    }
    
}

-(void) applyAction:(NSNumber *)actionType
            onLeave:(DaysTakenModel *) leave
          withNotes:(NSString *) notes{
    [self showProgressRingWithMessage:kProgressApplyingAction];
    LeavesApprovalManager *mgr = [LeavesApprovalManager new];
    [mgr takeActionForLeave:leave.absenceHistoryId
               withActionId:actionType
                   andNotes:notes
           andResponseBlock:^(BOOL success, id response, NSString *error) {
               [self hideProgressRing];
               if(success){
                   GenericLeaveResponse *respObj = (GenericLeaveResponse*) response;
                   if(respObj.isSuccessFul){
                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kResponseStatusSuccess message:respObj.message preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                           NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
                           [self fetchAbsenceList: selectedYear];
                       }];
                       [alertController addAction:okAction];
                       [self presentViewController:alertController animated: YES completion: nil];
                   }
                   else{
                       [self showMessageWithHeader:kResponseStatusError
                                           andBody:respObj.message];
                   }
               }
               else{
                   [self showMessageWithHeader:kResponseStatusError
                                       andBody:error];
               }
               
           }];
}

#pragma mark - Delegate

-(void) didSelectYearRange:(NSString *)rangeValue {
    [[UserModel sharedInstance] userObject].selectedYearRange = rangeValue;
    _dateFilterPicker.lblSelectedDate.text = rangeValue;
    NSString *selectedYear = [[rangeValue componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self fetchAbsenceList:selectedYear];
}


#pragma mark - Navigation

 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:kSegueFromAbsenceToAddRecordToil]){
        AddRecordToilViewController *addVC = (AddRecordToilViewController *)[segue destinationViewController];
        if(_willTopUpToil){
            addVC.selectedScreenType = AddRecordToilScreenTypeTopUp;
        }
        else{
            addVC.selectedScreenType = AddRecordToilScreenTypeRecording;
        }
        addVC.toilOwed = _toilOwed;
    }
}


@end
