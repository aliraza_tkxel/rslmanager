//
//  AbsenceListingViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
@interface AbsenceListingViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource, LeaveDateFilterPickerDelegate>
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet LeaveDateFilterPicker *dateFilterPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnSideMenu;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPageIcon;
@property (copy,nonatomic) NSMutableDictionary *sourceDict;
@property (copy, nonatomic) NSArray *sectionsArray;
@property (copy, nonatomic) NSNumber *toilOwed;
@property BOOL isLineManager;
@property BOOL willTopUpToil;
@end
