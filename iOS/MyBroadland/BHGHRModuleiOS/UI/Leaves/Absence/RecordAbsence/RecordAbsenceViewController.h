//
//  RecordAbsenceViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
#import "GenericLeaveModel.h"
#import "GenericLeaveResponse.h"
#import "GenericLeaveManager.h"
#import "FetchWorkingPatternResponseObject.h"
@interface RecordAbsenceViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlLeaveAtomicity;
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *reasonPickerView;
@property (weak, nonatomic) IBOutlet UIView *endDatePickerView;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldReason;
@property (weak, nonatomic) IBOutlet UIView *startDatePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectReason;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *endDateHeightConst;
@property (weak, nonatomic) IBOutlet UIView *endDatePanel;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldStartDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectEndDate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlEndDateDuration;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectStartDate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlDuration;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes;
@property GenericLeaveModel* leaveRequestObj;
@property UserEmploymentNature employmentNature;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldDuration;
@end
