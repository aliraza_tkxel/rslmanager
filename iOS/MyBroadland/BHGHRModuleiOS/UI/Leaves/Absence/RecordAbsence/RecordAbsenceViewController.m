//
//  RecordAbsenceViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "RecordAbsenceViewController.h"
#import <ActionSheetDatePicker.h>
#import <ActionSheetStringPicker.h>
#import "AbsenceLeaveManager.h"
@interface RecordAbsenceViewController ()

@end

@implementation RecordAbsenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init Module

-(void) initModule{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey andWidth:1];
    
    
    [_txtViewNotes addCompleteBorderWithColor:AppThemeColorGrey
                                     andWidth:1.0f];
    _employmentNature= ([[[UserModel sharedInstance] userObject].employmentNature isEqualToString:@"Days"])?UserEmploymentNatureDaily:UserEmploymentNatureHourly;
    _leaveRequestObj = [GenericLeaveModel new];
    _leaveRequestObj.leaveAtomicity = LeaveAtomicitySingle;
    _leaveRequestObj.leaveDurationStart = LeaveDurationFullDay;
    _leaveRequestObj.leaveDurationEnd = LeaveDurationFullDay;
    _endDatePanel.userInteractionEnabled = NO;
    [self configureTextFields];
    [self fetchWorkingPattern];
    
}

-(void) fetchWorkingPattern{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [self showProgressRingWithMessage:kProgressFetchingWorkingPattern];
    [mgr fetchUserWorkingPatternForId:LoggedInUserId
                    withResponseBlock:^(BOOL success, id response, NSString *error) {
                        [self hideProgressRing];
                        if(success)
                        {
                            FetchWorkingPatternResponseObject *respObj = (FetchWorkingPatternResponseObject *) response;
                            if(!isEmpty(respObj)){
                                if(!isEmpty(respObj.workingHours)){
                                    [[UserDataManager sharedInstance] insertWorkingPattern:respObj.workingHours];
                                }
                                else{
                                    [self showMessageWithHeader:kResponseStatusError
                                                        andBody:kNoWorkingPatternReturned];
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                            }
                            else{
                                [self showMessageWithHeader:kResponseStatusError
                                                    andBody:kNoWorkingPatternReturned];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                        else{
                            [self showMessageWithHeader:kResponseStatusError
                                                andBody:error];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
}
-(void) configureTextFields{
    [_reasonPickerView addBottomBorderWithColor:AppThemeColorGrey
                                     andWidth:1.0];
    [_startDatePickerView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0];
    [_endDatePickerView addBottomBorderWithColor:AppThemeColorGrey
                                          andWidth:1.0];
    [_txtFieldDuration addBottomBorderWithColor:AppThemeColorGrey
                                       andWidth:1.0];
    _txtFieldDuration.enabled = NO;
    _txtFieldDuration.userInteractionEnabled = NO;
    
}

#pragma mark - Operations

-(void) calculateLeaveDuration{
    if(!isEmpty(_leaveRequestObj.startDate) && !isEmpty(_leaveRequestObj.endDate)){
        double days = [DateUtilities calculateWorkingDaysBetween:_leaveRequestObj.startDate
                                                      andEndDate:_leaveRequestObj.endDate
                                             forEmploymentNature:_employmentNature andLeaveStartDuration:_leaveRequestObj.leaveDurationStart andLeaveEndDuration:_leaveRequestObj.leaveDurationEnd andAtomicity:_leaveRequestObj.leaveAtomicity];
        _leaveRequestObj.duration = days;
        _txtFieldDuration.text = [NSString stringWithFormat:@"%.1f %@",days, [[UserModel sharedInstance] userObject].employmentNature];
    }
    
}



#pragma mark - IBActions
- (IBAction)btnSelectReasonTapped:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:kSheetPickerTitleReason
                                            rows:[[[LookUpsDataManager sharedInstance] fetchAllAbsenceLeaveReasons] valueForKeyPath:kEntityLookUpVarLookUpDescription]
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           _txtFieldReason.text = selectedValue;
                                           _leaveRequestObj.reasonDescription = selectedValue;
                                           _leaveRequestObj.reasonId = [[[LookUpsDataManager sharedInstance] getLookUpIdForDescription:selectedValue forLookUpType:LookUpTypeAbsenceLeaveReasons] intValue];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         
                                     }
                                          origin:sender];
}
- (IBAction)btnStartDateTapped:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleStartDate datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
     {
         _leaveRequestObj.startDate = selectedDate;
         _txtFieldStartDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
         
         if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle)
         {
             _leaveRequestObj.endDate = selectedDate;
             _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
         }
         [self calculateLeaveDuration];
     }
     
                                   cancelBlock:^(ActionSheetDatePicker *picker)
     {
         [self calculateLeaveDuration];
     } origin:sender];

}
- (IBAction)btnSelectEndDateTapped:(id)sender {
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicityMultiple){
        if(_leaveRequestObj.startDate!=NULL){
            [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleEndDate datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
             {
                 if([DateUtilities isDate:selectedDate laterThan:_leaveRequestObj.startDate]){
                     _leaveRequestObj.endDate = selectedDate;
                     _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
                     [self calculateLeaveDuration];
                     
                 }
                 else{
                     [self showMessageWithHeader:kResponseStatusError andBody:kValidationInvalidEndDateBody];
                     _txtFieldEndDate.text = @"";
                     _leaveRequestObj.endDate = NULL;
                     [self calculateLeaveDuration];
                 }
             }
             
                                           cancelBlock:^(ActionSheetDatePicker *picker)
             {
                 [self calculateLeaveDuration];
             } origin:sender];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationSelectStartDateBody];
        }
        
        
    }
}
- (IBAction)segmentCtrlLeaveAtomicityChanged:(id)sender {
    _leaveRequestObj.leaveAtomicity =  _segmentCtrlLeaveAtomicity.selectedSegmentIndex;
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle){
        _leaveRequestObj.endDate = _leaveRequestObj.startDate;
        if(!isEmpty(_leaveRequestObj.startDate))
        {
            _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:_leaveRequestObj.startDate withFormat:kDateFormatDefaultStamp];
        }
        _endDatePanel.userInteractionEnabled = NO;
        _leaveRequestObj.leaveDurationEnd = _segmentCtrlDuration.selectedSegmentIndex;
        [_segmentCtrlEndDateDuration setSelectedSegmentIndex:_segmentCtrlDuration.selectedSegmentIndex];

        
    }
    else
    {
        _endDatePanel.userInteractionEnabled = YES;
    }
    [self calculateLeaveDuration];
}
- (IBAction)segmentCtrlDurationChanged:(id)sender {
    _leaveRequestObj.leaveDurationStart =  _segmentCtrlDuration.selectedSegmentIndex;
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle){
        _leaveRequestObj.leaveDurationEnd = _segmentCtrlDuration.selectedSegmentIndex;
        [_segmentCtrlEndDateDuration setSelectedSegmentIndex:_segmentCtrlDuration.selectedSegmentIndex];
    }
    [self calculateLeaveDuration];
}
- (IBAction)segmentCtrlEndDateDurationChanged:(id)sender {
    _leaveRequestObj.leaveDurationEnd =  _segmentCtrlEndDateDuration.selectedSegmentIndex;
    [self calculateLeaveDuration];
}
- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSaveTapped:(id)sender {
    _leaveRequestObj.notes = _txtViewNotes.text;
    if(isEmpty(_leaveRequestObj.reasonDescription)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectReason];
        return;
    }
    if(isEmpty(_leaveRequestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectStartDateBody];
        return;
    }
    if([DateUtilities checkIfDateLiesOnWeekend:_leaveRequestObj.startDate]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStartDateWeekend];
        return;
    }
    if([DateUtilities isDate:_leaveRequestObj.startDate
                 EarlierThan:[NSDate date]]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationInvalidStartDatePast];
        return;
    }
    if(isEmpty(_leaveRequestObj.endDate)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectEndDateBody];
        return;
    }
    
    if([DateUtilities isDateABankHoliday:_leaveRequestObj.startDate]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationBankHolidayStartDate];
        return;
    }
    if(!isEmpty(_leaveRequestObj.notes)){
        if([_leaveRequestObj.notes length]>100){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationNotesLimitExceeded];
            return;
        }
    }
    [self showProgressRingWithMessage:kProgressRecordingAbsenceLeave ];
    AbsenceLeaveManager *mgr = [[AbsenceLeaveManager alloc] init];
    [mgr recordAbsenceLeaveModel:_leaveRequestObj
               withResponseBlock:^(BOOL success, id response, NSString *error) {
                   [self hideProgressRing];
                   GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
                   if(success){
                       if(respObj.isSuccessFul){
                           [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                                        andBody:respObj.message];
                       }
                       else{
                           [self showMessageWithHeader:kResponseStatusError
                                                        andBody:respObj.message];
                       }
                   }
                   else{
                       [self showMessageWithHeader:kDomainRecordAbsence
                                           andBody:error];
                   }
               }];

    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
