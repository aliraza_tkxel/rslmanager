//
//  RecordPersonalDayViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
// 1st APR to 31st Mar Fiscal Year

#import "RecordPersonalDayViewController.h"
#import "GenericLeaveManager.h"
#import "FetchWorkingPatternResponseObject.h"
@interface RecordPersonalDayViewController ()

@end

@implementation RecordPersonalDayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) configureView{
    if(_screenType==AnnualSubLeaveTypePersonal){
        _lblScreenTitle.text = @"Book Personal Day";
    }
    else{
        _lblScreenTitle.text = @"Book Birthday";
    }
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"DropDownListTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDropDownCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"DatePickerInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicDatePickerCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"FreeTextInputTableViewCell" bundle:nil] forCellReuseIdentifier:kDynamicFreeTextCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _tblView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1];
    
    [self createDynamicCells];
    
    [_tblView reloadData];
    [self fetchWorkingPattern];
}
-(void) fetchWorkingPattern{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [self showProgressRingWithMessage:kProgressFetchingWorkingPattern];
    [mgr fetchUserWorkingPatternForId:LoggedInUserId
                    withResponseBlock:^(BOOL success, id response, NSString *error) {
                        [self hideProgressRing];
                        if(success)
                        {
                            FetchWorkingPatternResponseObject *respObj = (FetchWorkingPatternResponseObject *) response;
                            if(!isEmpty(respObj)){
                                if(!isEmpty(respObj.workingHours)){
                                    [[UserDataManager sharedInstance] insertWorkingPattern:respObj.workingHours];
                                }
                                else{
                                    [self showMessageWithHeader:kResponseStatusError
                                                        andBody:kNoWorkingPatternReturned];
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                            }
                            else{
                                [self showMessageWithHeader:kResponseStatusError
                                                    andBody:kNoWorkingPatternReturned];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                        else{
                            [self showMessageWithHeader:kResponseStatusError
                                                andBody:error];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }];
}
-(void) createDynamicCells{
    
    _employmentNature= ([[[UserModel sharedInstance] userObject].employmentNature isEqualToString:@"Days"])?UserEmploymentNatureDaily:UserEmploymentNatureHourly;
    
    _leaveRequestObj = [GenericLeaveModel new];
    _leaveRequestObj.employeeId = LoggedInUserId;
    _leaveRequestObj.leaveDurationStart = LeaveDurationFullDay;
    _leaveRequestObj.leaveAtomicity = LeaveAtomicitySingle;
    _leaveRequestObj.leaveDurationEnd = LeaveDurationFullDay;
    _leaveRequestObj.duration = 0.00;
    
    
    _sourceArr = [NSMutableArray new];
    DynamicCellsConfiguration *employeeName = [[DynamicCellsConfiguration alloc] init];
    employeeName.inputFieldTitle = @"Employee Name:";
    employeeName.selectedValue = [[[UserModel sharedInstance] userObject] employeeFullName];
    employeeName.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    employeeName.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    employeeName.isNonMutableField = YES;
    employeeName.cellRowId = PersonalDayFormFieldEmployeeName;
    [_sourceArr addObject:employeeName];
    
    
    DynamicCellsConfiguration *startDate = [[DynamicCellsConfiguration alloc] init];
    startDate.inputFieldTitle = @"Start Date:";
    startDate.startingDate = [NSDate date];
    startDate.baseTypeForConfig = DynamicCellsBaseTypeDate;
    startDate.headerTitle = kSheetPickerTitleStartDate;
    startDate.minDate = [NSDate date];//[DateUtilities getFiscalYearStartDate];
    startDate.maxDate = nil;//[DateUtilities getFiscalYearEndDate];
    startDate.isRangeSpecific = YES;
    startDate.cellRowId = PersonalDayFormFieldStartDate;
    [_sourceArr addObject:startDate];
    
    DynamicCellsConfiguration *duration = [[DynamicCellsConfiguration alloc] init];
    duration.inputFieldTitle = @"Duration";
    duration.selectedValue=[NSString stringWithFormat:@"%.1f %@",_leaveRequestObj.duration, [[UserModel sharedInstance] userObject].employmentNature];
    duration.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    duration.isNonMutableField = YES;
    duration.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    [_sourceArr addObject:duration];
    
    DynamicCellsConfiguration *notes = [[DynamicCellsConfiguration alloc] init];
    notes.inputFieldTitle = @"Notes:";
    notes.baseTypeForConfig = DynamicCellsBaseTypeFreeText;
    notes.appliedCellFieldType = DynamicCellsFreeTextFieldTypeText;
    notes.cellRowId = PersonalDayFormFieldNotes;
    [_sourceArr addObject:notes];
}

#pragma mark - IBActions

- (IBAction)btnRequestTapped:(id)sender {
    if(isEmpty(_leaveRequestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError andBody:kValidationSelectStartDateBody];
        return;
    }
    if(_leaveRequestObj.duration<=0.00){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStartDateWeekend];
        return;
    }
    AnnualLeavesManager *mgr = [[AnnualLeavesManager alloc] init];
    if(_screenType == AnnualSubLeaveTypeBirthday){
        [self showProgressRingWithMessage:kProgressRecordingBdayLeave];
        [mgr recordBirthdayLeaveForUserWithModel:_leaveRequestObj
                                andResponseBlock:^(BOOL success, id response, NSString *error) {
            [self hideProgressRing];
            GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
            if(success){
                if(respObj.isSuccessFul){
                    [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                                 andBody:respObj.message];
                }
                else
                {
                    [self showMessageWithHeader:kResponseStatusError
                                        andBody:respObj.message];
                }
            }
            else{
                [self showMessageWithHeader:kDomainAnnualLeaves
                                    andBody:error];
            }
        }];
    }
    else{
        [self showProgressRingWithMessage:kProgressRecordingPersonalDay];
        
        [mgr recordPersonalLeaveWithModel:_leaveRequestObj
                        withResponseBlock:^(BOOL success, id response, NSString *error) {
            [self hideProgressRing];
            GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
            if(success){
                if(respObj.isSuccessFul){
                    [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                                 andBody:respObj.message];
                }
                else
                {
                    [self showMessageWithHeader:kResponseStatusError
                                        andBody:respObj.message];
                }
            }
            else{
                [self showMessageWithHeader:kDomainAnnualLeaves
                                    andBody:error];
            }
        }];
    }
    
}
- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Delegates

-(void) didChangeValueFor:(DynamicCellsConfiguration *)config{
    switch (config.cellRowId) {
        case PersonalDayFormFieldEmployeeName:
            break;
        case PersonalDayFormFieldStartDate:
            _leaveRequestObj.startDate = config.startingDate;
            _leaveRequestObj.endDate = config.startingDate;
           _leaveRequestObj.duration = [DateUtilities calculateWorkingDaysBetween:config.startingDate andEndDate:config.startingDate forEmploymentNature:_employmentNature andLeaveStartDuration:LeaveDurationFullDay andLeaveEndDuration:LeaveDurationFullDay andAtomicity:LeaveAtomicitySingle];
            [self updateDurationInTblView];
            break;
        case PersonalDayFormFieldDuration:
            _leaveRequestObj.duration = [config.selectedValue doubleValue];
            break;
        case PersonalDayFormFieldNotes:
            _leaveRequestObj.notes = config.selectedValue;
            break;
        default:
            break;
    }
}

-(void) updateDurationInTblView{
    DynamicCellsConfiguration *durT = [_sourceArr objectAtIndex:PersonalDayFormFieldDuration];
    durT.selectedValue = [NSString stringWithFormat:@"%.1f %@",_leaveRequestObj.duration, [[UserModel sharedInstance] userObject].employmentNature];
    [_sourceArr replaceObjectAtIndex:PersonalDayFormFieldDuration withObject:durT];
    [_tblView reloadData];
}

#pragma mark - TableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_sourceArr count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    DynamicCellsConfiguration *config = [_sourceArr objectAtIndex:indexPath.row];
    config.cellRowId = indexPath.row;
    config.cellSectionId = indexPath.section;
    if(config.baseTypeForConfig == DynamicCellsBaseTypeDate){
        DatePickerInputTableViewCell *customCell = (DatePickerInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDatePickerCellReuseIdentifier];
        customCell.delegate = self;
        [customCell initializeCellWithConfiguration:config];
        cell = customCell;
        
    }
    else if(config.baseTypeForConfig == DynamicCellsBaseTypeDropDown){
        DropDownListTableViewCell *customCell = (DropDownListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicDropDownCellReuseIdentifier];
        customCell.delegate = self;
        [customCell initializeCellWithConfiguration:config];
        cell = customCell;
        
    }
    else if(config.baseTypeForConfig == DynamicCellsBaseTypeFreeText){
        FreeTextInputTableViewCell *customCell = (FreeTextInputTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kDynamicFreeTextCellReuseIdentifier];
        customCell.delegate = self;
        [customCell initializeCellWithConfiguration:config];
        cell = customCell;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
