//
//  RecordPersonalDayViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
#import "DropDownListTableViewCell.h"
#import "DatePickerInputTableViewCell.h"
#import "FreeTextInputTableViewCell.h"
#import "DynamicCellsConfiguration.h"
#import "DynamicTableViewCellProtocol.h"
#import "GenericLeaveResponse.h"
#import "GenericLeaveModel.h"
#import "AnnualLeavesManager.h"
@interface RecordPersonalDayViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,DynamicTableViewCellProtocol>
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (copy,nonatomic) NSMutableArray *sourceArr;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitle;
@property (copy,nonatomic)GenericLeaveModel* leaveRequestObj;
@property AnnualSubLeaveType screenType;
@property UserEmploymentNature employmentNature;
@end
