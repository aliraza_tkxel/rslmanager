//
//  AnnualLeavesDashboardViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AnnualLeaveStatsModel.h"
@interface AnnualLeavesDashboardViewController : BaseViewController <LeaveDateFilterPickerDelegate>
@property (weak, nonatomic) IBOutlet LeaveDateFilterPicker *dateFilterPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnBookBdayLeave;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestLeave;
@property (weak, nonatomic) IBOutlet UIButton *btnBookPersonalDay;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestLeave;
@property (weak, nonatomic) IBOutlet UILabel *lblBookPersonalDay;
@property (weak, nonatomic) IBOutlet UILabel *lblBookBdayLeave;
@property (weak, nonatomic) IBOutlet UIImageView *navBarIcon;
@property (weak, nonatomic) IBOutlet UIView *topStatsView;
@property (weak, nonatomic) IBOutlet UIView *daysBookedView;
@property (weak, nonatomic) IBOutlet UIView *daysRequestedView;
@property (weak, nonatomic) IBOutlet UIView *daysRemainingView;
@property (copy, nonatomic) AnnualLeaveStatsModel *statsModel;

@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAllowance;
@property (weak, nonatomic) IBOutlet UILabel *lblBookedDays;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestedDays;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingDays;
@property AnnualSubLeaveType personalDayBirthdayTypeIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewShowAllowence;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealAllowence;
@property (weak, nonatomic) IBOutlet UIView *allowenceBreakDownView;
@property (weak, nonatomic) IBOutlet UILabel *lblHolidayEntitlements;
@property (weak, nonatomic) IBOutlet UILabel *lblBankHolidays;
@property (weak, nonatomic) IBOutlet UILabel *lblCarryForward;
@property (weak, nonatomic) IBOutlet UILabel *lblInLieu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topStatsViewHeightConstraint;
@property AnnualLeavesListControllerType selectedListingType;
@end
