//
//  AnnualLeavesDashboardViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeavesDashboardViewController.h"
#import "AnnualLeavesManager.h"
#import "FetchAnnualLeaveStatsResponse.h"
#import "RecordAnnualLeaveViewController.h"
#import "AnnualLeavesListViewController.h"
#import "GenericLeaveResponse.h"
#import "RecordPersonalDayViewController.h"
@interface AnnualLeavesDashboardViewController (){
    BOOL isAllowanceBreakDownRevealed;
}

@end

@implementation AnnualLeavesDashboardViewController

-(void) viewWillAppear:(BOOL)animated{
    if (isEmpty([[UserModel sharedInstance] userObject].selectedYearRange)) {
        [self fetchAnnualLeavesStats: nil];
    }
    else {
        NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
        [self fetchAnnualLeavesStats: selectedYear];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Init

-(void) initModule{
    isAllowanceBreakDownRevealed = NO;
    _selectedListingType = AnnualLeavesListControllerTypeTakenBooked;
    [self configureRevealMenu];
    [_navigationBarView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1];
    [_navBarIcon addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0];
    [_topStatsView addBottomBorderWithColor:AppThemeColorGrey
                                    andWidth:1.0f];
    [_daysBookedView addBottomBorderWithColor:AppThemeColorGrey
                                    andWidth:1.0f];
    [_daysRemainingView addBottomBorderWithColor:AppThemeColorGrey
                                    andWidth:1.0f];
    [_daysRequestedView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0f];
    _dateFilterPicker.lblSelectedDate.text = [[UserModel sharedInstance] userObject].selectedYearRange;
    _dateFilterPicker.delegate = self;
    
}

-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void) configureLabels{
    _lblStartDate.text = !(isEmpty(_statsModel.startDateString))?_statsModel.startDateString:kValueNotApplicable;
    
    _lblTotalAllowance.text = !(isEmpty(_statsModel.totalAllowanceDesc))?_statsModel.totalAllowanceDesc:kValueNotApplicable;
    
    _lblBookedDays.text = !(isEmpty(_statsModel.leavesBookedDesc))?_statsModel.leavesBookedDesc:kValueNotApplicable;
    
    _lblRequestedDays.text = !(isEmpty(_statsModel.leavesRequestedDesc))?_statsModel.leavesRequestedDesc:kValueNotApplicable;
    
    _lblRemainingDays.text = !(isEmpty(_statsModel.leavesRemainingDesc))?_statsModel.leavesRemainingDesc:kValueNotApplicable;
    
    if([_statsModel.leavesUnit isEqualToString:@"hrs"]){
        _lblHolidayEntitlements.text = !(isEmpty(_statsModel.holidayEntitlementHours))?_statsModel.holidayEntitlementHours:kValueNotApplicable;
    }
    else{
        _lblHolidayEntitlements.text = !(isEmpty(_statsModel.entitlementDays))?_statsModel.entitlementDays:kValueNotApplicable;
    }
    
    _lblCarryForward.text = !(isEmpty(_statsModel.carryForward))?_statsModel.carryForward:kValueNotApplicable;
    _lblBankHolidays.text = !(isEmpty(_statsModel.bankHolidays))?_statsModel.bankHolidays:kValueNotApplicable;
    _lblInLieu.text = !(isEmpty(_statsModel.inLieu))?_statsModel.inLieu:kValueNotApplicable;
    
}

#pragma mark - Network Operations

-(void) fetchAnnualLeavesStats: (NSString *) year{
    [self showProgressRingWithMessage:kProgressFetchingLeavesStats];
    AnnualLeavesManager *leavesMgr = [AnnualLeavesManager new];
    [leavesMgr fetchAnnualLeaveStatsForUserId:LoggedInUserId
                                      forYear: year
                            withResponseBlock:^(BOOL success, id response, NSString *error)
    {
        [self hideProgressRing];
        if(success){
            FetchAnnualLeaveStatsResponse *responseObj = (FetchAnnualLeaveStatsResponse *)response;
            _statsModel = [AnnualLeaveStatsModel new];
            [_statsModel initWithLeaveStatsResponse:responseObj];
            [self configureLabels];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}

#pragma mark - IBActions

- (IBAction)bookPersonalDayTapped:(id)sender {
    //kSegueFromAnnualDashboardToRecordPersonalDayBirthday
    _personalDayBirthdayTypeIndicator = AnnualSubLeaveTypePersonal;
    [self performSegueWithIdentifier:kSegueFromAnnualDashboardToRecordPersonalDayBirthday
                              sender:self];
}
- (IBAction)btnBookBirthdayLeaveTapped:(id)sender {
    _personalDayBirthdayTypeIndicator = AnnualSubLeaveTypeBirthday;
    [self performSegueWithIdentifier:kSegueFromAnnualDashboardToRecordPersonalDayBirthday
                              sender:self];
}
- (IBAction)btnRequestLeaveTapped:(id)sender {
    if(_statsModel.leavesRemaining>0){
        [self performSegueWithIdentifier:kSegueFromAnnualDashboardToRecordAnnualLeave
                                  sender:self];
    }
    else{
        [self showMessageWithHeader:kDomainAnnualLeaves
                            andBody:kValidationLeaveQuotaUnavailable];
    }
    
}
- (IBAction)btnDaysBookedTapped:(id)sender {
    _selectedListingType = AnnualLeavesListControllerTypeTakenBooked;
    [self performSegueWithIdentifier:kSegueFromAnnualLeavesDashboardToListing
                              sender:self];
}
- (IBAction)btnDaysRequestedTapped:(id)sender {
    _selectedListingType = AnnualLeavesListControllerTypePendingApproval;
    [self performSegueWithIdentifier:kSegueFromAnnualLeavesDashboardToListing
                              sender:self];
}

- (IBAction)btnRevealAllowanceTapped:(id)sender {
    if(isAllowanceBreakDownRevealed){
        _imgViewShowAllowence.image = [UIImage imageNamed:@"icon_arrow_white_east.png"];
        _topStatsViewHeightConstraint.constant = 128;
        [UIView animateWithDuration:0.5
                         animations:^{
                             [self.view layoutIfNeeded];
        }];
        isAllowanceBreakDownRevealed = NO;
    }
    else{
        _imgViewShowAllowence.image = [UIImage imageNamed:@"icon_arrow_white_south.png"];
        _topStatsViewHeightConstraint.constant = 250;
        [UIView animateWithDuration:0.5
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
        isAllowanceBreakDownRevealed = YES;
    }
    
    
}


- (IBAction)btnDaysRemainingTapped:(id)sender {
   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kSegueFromAnnualDashboardToRecordAnnualLeave])
    {
        RecordAnnualLeaveViewController *vc = [segue destinationViewController];
        vc.statsModel = _statsModel;
        
    }
    
    else if ([[segue identifier] isEqualToString:kSegueFromAnnualLeavesDashboardToListing])
    {
        AnnualLeavesListViewController *vc = [segue destinationViewController];
        vc.controllerType = _selectedListingType;
        
    }
    else if([[segue identifier] isEqualToString:kSegueFromAnnualDashboardToRecordPersonalDayBirthday]){
        RecordPersonalDayViewController *vc = [segue destinationViewController];
        vc.screenType = _personalDayBirthdayTypeIndicator;
    }
}

-(void) didSelectYearRange:(NSString *)rangeValue {
    [[UserModel sharedInstance] userObject].selectedYearRange = rangeValue;
    _dateFilterPicker.lblSelectedDate.text = rangeValue;
    NSString *selectedYear = [[rangeValue componentsSeparatedByString:@" - "] objectAtIndex:0];
    [self fetchAnnualLeavesStats:selectedYear];
}

@end
