//
//  AnnualLeavesListViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeavesListViewController.h"
#import "LeaveListingTableViewCell.h"
#import "LeaveTakenNetworkObject.h"
#import "AnnualLeavesDaysTakenResponseObject.h"
#import "AnnualLeavesManager.h"
#import "LeaveTakenNetworkObject.h"
#import "LeaveListingCompactTableViewCell.h"
#import "LeavesApprovalManager.h"
#import "GenericLeaveResponse.h"

@interface AnnualLeavesListViewController ()

@end

@implementation AnnualLeavesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init

-(void) initModule{
    [self configureRevealMenu];
    [_navigationBarView addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1];
    [_imgViewPageIcon addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0];
    //_sectionHeaders = [[NSMutableArray alloc] initWithObjects:kLeaveNatureSickness,kLeaveNatureAnnual,kLeaveNatureBirthday,kLeaveNaturePersonal,nil];
    _tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = YES;
    [_tblView reloadData];
    NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
    if(_controllerType == AnnualLeavesListControllerTypeTakenBooked){
        [self getTakenLeaveObjectsForCurrentUser: selectedYear];
    }
    else if(_controllerType == AnnualLeavesListControllerTypePendingApproval){
        [self getRequestedLeaveObjectsForCurrentUser: selectedYear];
    }
    _filterHostView.lblSelectedDate.text = [[UserModel sharedInstance] userObject].selectedYearRange;
    _filterHostView.delegate = self;
    
    
}
-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark - Network Operations

-(void) getTakenLeaveObjectsForCurrentUser: (NSString *) year{
    
    [self showProgressRingWithMessage:kProgressFetchingDaysTaken];
    AnnualLeavesManager *leavesMgr = [AnnualLeavesManager new];
    [leavesMgr fetchAnnualLeavesDaysTakenForUserId:LoggedInUserId
                                           forYear: year
                            withResponseBlock:^(BOOL success, id response, NSString *error)
    {
        [self hideProgressRing];
        if(success){
            AnnualLeavesDaysTakenResponseObject *responseObj = (AnnualLeavesDaysTakenResponseObject *)response;
            [self parseAndFilterDataSource:responseObj];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}

-(void) getRequestedLeaveObjectsForCurrentUser: (NSString *) year{
    
    [self showProgressRingWithMessage:kProgressFetchingDaysTaken];
    AnnualLeavesManager *leavesMgr = [AnnualLeavesManager new];
    [leavesMgr fetchAnnualLeavesDaysRequestedForUserId:LoggedInUserId
                                               forYear: year
                                 withResponseBlock:^(BOOL success, id response, NSString *error)
     {
         [self hideProgressRing];
         if(success){
             AnnualLeavesDaysTakenResponseObject *responseObj = (AnnualLeavesDaysTakenResponseObject *)response;
             [self parseAndFilterDataSource:responseObj];
         }
         else{
             [self showMessageWithHeader:kResponseStatusError
                                 andBody:error];
         }
     }];
}

#pragma mark - General Operations

-(void) parseAndFilterDataSource:(AnnualLeavesDaysTakenResponseObject *) responseObj{
    NSArray * dataSource = responseObj.leaveTakenList;
    if([dataSource count]==0){
        [self showMessageWithHeader:kDomainAnnualLeaves
                            andBody:kNoLeavesFound];
        _sectionsArray = [[NSArray alloc] init];
        _sourceDict = [NSMutableDictionary new];
        [_tblView reloadData];
        return;
    }
    _sourceDict = [NSMutableDictionary new];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"startDate"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [dataSource
                                 sortedArrayUsingDescriptors:sortDescriptors];
    dataSource = [NSArray arrayWithArray:sortedEventArray];
    
    for(LeaveTakenNetworkObject *record in dataSource){
        DaysTakenModel *model = [DaysTakenModel new];
        [model initWithResponseObject:record];
        NSMutableArray *arr;
        if([_sourceDict objectForKey:model.groupKey]){
            arr = [_sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [_sourceDict setObject:arr forKey:model.groupKey];
    }
    _sectionsArray = [[NSArray alloc] init];
    _sectionsArray = [_sourceDict allKeys];
    [self sortSectionsArray];
    [_tblView reloadData];
    
}

-(void) sortSectionsArray {
    if(isEmpty(_sectionsArray)){
        return;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for(NSString * str in _sectionsArray){
        NSString *dateStr = [NSString stringWithFormat:@"1/%@",str];
        NSDate *dateObj = [DateUtilities getDateFromString:dateStr withFormat:kDateFormatDefaultStamp];
        [arr addObject:dateObj];
    }
    NSArray *reverseOrderUsingComparator = [arr sortedArrayUsingComparator:
                                            ^(id obj1, id obj2) {
                                                return [obj2 compare:obj1];
                                            }];
    [arr removeAllObjects];
    for(NSDate * date in reverseOrderUsingComparator){
        NSString *str = [DateUtilities getDateStringFromDate:date withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
        [arr addObject:str];
    }
    _sectionsArray = [NSArray arrayWithArray:arr];
}

#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sectionsArray count];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isEmpty(_sectionsArray) || isEmpty(_sourceDict)){
        return 0;
    }
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return [arr count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    LeaveListingTableViewCell *customCell = (LeaveListingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kLeaveListingDetailCellReuseIdentifier];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    [customCell configureWithDataObject:[sourceArray objectAtIndex:indexPath.row]];
    cell = customCell;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(isEmpty(_sectionsArray) || isEmpty(_sourceDict)){
        return 0;
    }
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return (arr.count>0)?30:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    
    label.text = !isEmpty(_sectionsArray)?[_sectionsArray objectAtIndex:section]:@"";
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor blackColor]];
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    DaysTakenModel *model = [sourceArray objectAtIndex:indexPath.row];
    
    //Dont let the user take action on bank holidays
    if([model.absenceHistoryId integerValue]!=0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitlePickAction
                                                                                 message:[self leaveInfoDescriptionFor:model]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNoAction style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
        }];
        UIAlertAction * cancelAct = [UIAlertAction actionWithTitle:kAlertCancelLeave style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UITextField *textField = alertController.textFields[0];
            [self cancelLeave:model withNotes:textField.text];
            
        }];
        
        [alertController addAction:cancelAct];
        [alertController addAction:noAction];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Add comments here...";
            textField.keyboardType = UIKeyboardTypeDefault;
        }];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:kResponseStatusError andBody:kValidationBankHolidayCancelling];
    }
    
    
    
}

#pragma mark - Leave Status Changing

-(NSString *) leaveInfoDescriptionFor:(DaysTakenModel*) leaveObj{
    NSString *descriptionStr =  [NSString stringWithFormat:@"\r%@\r\rStart: %@\rEnd: %@\rDuration:%@\rNature: %@\r", kAlertMessageApproval,leaveObj.startDateDescription,leaveObj.endDateDescription,leaveObj.durationDescription, leaveObj.leaveDescription];
    return descriptionStr;
}

-(void) cancelLeave:(DaysTakenModel *) leaveObj withNotes:(NSString *) notes{
    if([DateUtilities isDate:leaveObj.startDate laterThan:[NSDate date]] || [DateUtilities isDate:leaveObj.startDate SameAs:[NSDate date]]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitleConfirmAction message:kAlertMessageCancelLeave preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSNumber *actionId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kAlertActionCancel
                                                                                  forLookUpType:LookUpTypeLeaveActions];
            [self applyAction:actionId
                      onLeave:leaveObj
                    withNotes:notes];
        }];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:kAlertActionNO style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:noAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationCancelPastLeaves];
    }
    /*if([leaveObj.status isEqualToString:kLeaveStatusPending]){
        
        
    }
    else{
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationCancelNonPendingLeave];
    }*/
    
}

-(void) applyAction:(NSNumber *)actionType
            onLeave:(DaysTakenModel *) leave
          withNotes:(NSString *) notes{
    [self showProgressRingWithMessage:kProgressApplyingAction];
    LeavesApprovalManager *mgr = [LeavesApprovalManager new];
    [mgr takeActionForLeave:leave.absenceHistoryId
               withActionId:actionType
                   andNotes:notes
           andResponseBlock:^(BOOL success, id response, NSString *error) {
               [self hideProgressRing];
               if(success){
                   GenericLeaveResponse *respObj = (GenericLeaveResponse*) response;
                   if(respObj.isSuccessFul){
                       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kResponseStatusSuccess message:respObj.message preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction * okAction = [UIAlertAction actionWithTitle:kAlertActionOk style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                           NSString *selectedYear = [[[[UserModel sharedInstance] userObject].selectedYearRange componentsSeparatedByString:@" - "] objectAtIndex:0];
                           if(_controllerType == AnnualLeavesListControllerTypeTakenBooked){
                               [self getTakenLeaveObjectsForCurrentUser: selectedYear];
                           }
                           else if(_controllerType == AnnualLeavesListControllerTypePendingApproval){
                               [self getRequestedLeaveObjectsForCurrentUser: selectedYear];
                           }
                       }];
                       [alertController addAction:okAction];
                       [self presentViewController:alertController animated: YES completion: nil];
                   }
                   else{
                       [self showMessageWithHeader:kResponseStatusError
                                           andBody:respObj.message];
                   }
               }
               else{
                   [self showMessageWithHeader:kResponseStatusError
                                       andBody:error];
               }
               
           }];
}

-(void) didSelectYearRange:(NSString *)rangeValue {
    [[UserModel sharedInstance] userObject].selectedYearRange = rangeValue;
    _filterHostView.lblSelectedDate.text = rangeValue;
    NSString *selectedYear = [[rangeValue componentsSeparatedByString:@" - "] objectAtIndex:0];
    if(_controllerType == AnnualLeavesListControllerTypeTakenBooked){
        [self getTakenLeaveObjectsForCurrentUser: selectedYear];
    }
    else if(_controllerType == AnnualLeavesListControllerTypePendingApproval){
        [self getRequestedLeaveObjectsForCurrentUser: selectedYear];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
