//
//  AnnualLeavesListViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface AnnualLeavesListViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource, LeaveDateFilterPickerDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPageIcon;
@property (copy,nonatomic) NSMutableDictionary *sourceDict;
@property (copy, nonatomic) NSArray *sectionsArray;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property AnnualLeavesListControllerType controllerType;
@property (weak, nonatomic) IBOutlet LeaveDateFilterPicker *filterHostView;
@end
