//
//  LeaveListingTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveListingTableViewCell.h"

@implementation LeaveListingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureWithDataObject:(DaysTakenModel *) obj{
    _lblDuration.text = obj.durationDescription;
    _lblStartDate.text = obj.startDateDescription;
    _lblEndDate.text = obj.endDateDescription;
    _lblLeaveStatus.text = obj.status;
    _lblLeaveType.text = obj.leaveDescription;
    if([obj.status isEqualToString:kLeaveStatusPending]){
        [_lblLeaveStatus setTextColor:UIColorFromRGB(221, 138, 47)];
    }
    else if([obj.status isEqualToString:kLeaveStatusApproved] || [obj.status isEqualToString:kLeaveStatusBHApproved]){
        [_lblLeaveStatus setTextColor:UIColorFromRGB(139, 188, 79)];
    }
    else if([obj.status isEqualToString:kLeaveStatusRejected] || [obj.status isEqualToString:kLeaveStatusCancelled] || [obj.status isEqualToString:kLeaveStatusDeclined]){
        [_lblLeaveStatus setTextColor:UIColorFromRGB(203, 38, 40)];
    }
    else{
        [_lblLeaveStatus setTextColor:[UIColor blackColor]];
    }
    
}

@end
