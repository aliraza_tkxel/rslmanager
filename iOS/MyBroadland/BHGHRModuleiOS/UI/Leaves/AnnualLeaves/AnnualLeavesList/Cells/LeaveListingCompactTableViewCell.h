//
//  LeaveListingCompactTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DaysTakenModel.h"
@interface LeaveListingCompactTableViewCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel*lblDate;
-(void) configureWithDataObject:(DaysTakenModel *) obj;
@end
