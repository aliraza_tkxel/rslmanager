//
//  LeaveListingTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DaysTakenModel.h"
@interface LeaveListingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UILabel *lblLeaveType;
@property (weak, nonatomic) IBOutlet UILabel *lblLeaveStatus;
-(void) configureWithDataObject:(DaysTakenModel *) obj;
@end
