//
//  LeaveListingCompactTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LeaveListingCompactTableViewCell.h"

@implementation LeaveListingCompactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureWithDataObject:(DaysTakenModel *) obj{
    _lblDate.text = obj.startDateDescription;
    
}

@end
