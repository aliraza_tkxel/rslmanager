//
//  RecordAnnualLeaveViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "GenericLeaveModel.h"
#import "AnnualLeaveStatsModel.h"
#import "GenericLeaveManager.h"
#import "FetchWorkingPatternResponseObject.h"
@interface RecordAnnualLeaveViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnRequest;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlLeaveType;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectStartDate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlLeaveDurationStart;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlLeaveDurationEnd;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes;
@property (weak, nonatomic) IBOutlet UIView *endDatePanel;
@property (copy,nonatomic)GenericLeaveModel* leaveRequestObj;
@property AnnualLeaveStatsModel *statsModel;
@property UserEmploymentNature employmentNature;
@end
