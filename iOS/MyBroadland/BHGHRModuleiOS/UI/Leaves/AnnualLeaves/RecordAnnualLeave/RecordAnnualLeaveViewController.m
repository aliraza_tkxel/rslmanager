//
//  RecordAnnualLeaveViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//




#import "RecordAnnualLeaveViewController.h"
#import <ActionSheetDatePicker.h>
#import "AnnualLeavesManager.h"
#import "LoginViewController.h"
#import "GenericLeaveResponse.h"
@interface RecordAnnualLeaveViewController ()

@end

@implementation RecordAnnualLeaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1];
    
    [_txtViewNotes addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0f];
    
    [_lblDuration addBottomBorderWithColor:AppThemeColorGrey
                                  andWidth:1.0];
    
    _leaveRequestObj = [GenericLeaveModel new];
    _leaveRequestObj.leaveAtomicity = LeaveAtomicitySingle;
    _leaveRequestObj.leaveDurationStart = LeaveDurationFullDay;
    _leaveRequestObj.leaveDurationEnd = LeaveDurationFullDay;
    _endDatePanel.userInteractionEnabled = NO;
    _employmentNature= ([[[UserModel sharedInstance] userObject].employmentNature isEqualToString:@"Days"])?UserEmploymentNatureDaily:UserEmploymentNatureHourly;
    [self configureTextFields];
    [self fetchWorkingPattern];
}


-(void) configureTextFields{
    [_txtFieldEndDate addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1.0];
    [_txtFieldStartDate addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1.0];
    
}


-(void) fetchWorkingPattern{
    GenericLeaveManager *mgr = [[GenericLeaveManager alloc] init];
    [self showProgressRingWithMessage:kProgressFetchingWorkingPattern];
    [mgr fetchUserWorkingPatternForId:LoggedInUserId
                    withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success)
        {
            FetchWorkingPatternResponseObject *respObj = (FetchWorkingPatternResponseObject *) response;
            if(!isEmpty(respObj)){
                if(!isEmpty(respObj.workingHours)){
                    [[UserDataManager sharedInstance] insertWorkingPattern:respObj.workingHours];
                }
                else{
                    [self showMessageWithHeader:kResponseStatusError
                                        andBody:kNoWorkingPatternReturned];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else{
                [self showMessageWithHeader:kResponseStatusError
                                    andBody:kNoWorkingPatternReturned];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - Operations

-(void) calculateLeaveDuration{
    if(!isEmpty(_leaveRequestObj.startDate) && !isEmpty(_leaveRequestObj.endDate)){
        double days = [DateUtilities calculateWorkingDaysBetween:_leaveRequestObj.startDate
                                                          andEndDate:_leaveRequestObj.endDate
                                                        forEmploymentNature:_employmentNature andLeaveStartDuration:_leaveRequestObj.leaveDurationStart andLeaveEndDuration:_leaveRequestObj.leaveDurationEnd andAtomicity:_leaveRequestObj.leaveAtomicity];
        
        
        _leaveRequestObj.duration = days;
        _lblDuration.text = [NSString stringWithFormat:@"%.1f %@",days, [[UserModel sharedInstance] userObject].employmentNature];
    }
    else{
        _lblDuration.text = @"";
    }
}

#pragma mark - IBActions

- (IBAction)SegmentCtrlLeaveTypeChanged:(id)sender {
    _leaveRequestObj.leaveAtomicity =  _segmentCtrlLeaveType.selectedSegmentIndex;
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle){
        _leaveRequestObj.endDate = _leaveRequestObj.startDate;
        
        _leaveRequestObj.leaveDurationStart =  LeaveDurationFullDay;
        _leaveRequestObj.leaveDurationEnd =  LeaveDurationFullDay;
        _segmentCtrlLeaveDurationEnd.selectedSegmentIndex = LeaveDurationFullDay;
        _segmentCtrlLeaveDurationStart.selectedSegmentIndex = LeaveDurationFullDay;
        
        if(!isEmpty(_leaveRequestObj.startDate))
        {
             _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:_leaveRequestObj.startDate withFormat:kDateFormatDefaultStamp];
        }
        _endDatePanel.userInteractionEnabled = NO;
       
    }
    else
    {
        _endDatePanel.userInteractionEnabled = YES;
    }
    [self calculateLeaveDuration];
    
}
- (IBAction)segmentCtrlLeaveDurationStartChanged:(id)sender {
    _leaveRequestObj.leaveDurationStart =  _segmentCtrlLeaveDurationStart.selectedSegmentIndex;
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle){
        _leaveRequestObj.leaveDurationEnd = _segmentCtrlLeaveDurationStart.selectedSegmentIndex;
        _segmentCtrlLeaveDurationEnd.selectedSegmentIndex = _segmentCtrlLeaveDurationStart.selectedSegmentIndex;
    }
    [self calculateLeaveDuration];
}
- (IBAction)segmentCtrlLeaveDurationEndChanged:(id)sender {
    _leaveRequestObj.leaveDurationEnd =  _segmentCtrlLeaveDurationEnd.selectedSegmentIndex;
    [self calculateLeaveDuration];
}
- (IBAction)btnSelectStartDateTapped:(id)sender {
   
    [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleStartDate datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
    {
        _leaveRequestObj.startDate = selectedDate;
        _txtFieldStartDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
        
        if(_leaveRequestObj.leaveAtomicity==LeaveAtomicitySingle)
        {
            _leaveRequestObj.endDate = selectedDate;
            _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
        }
        [self calculateLeaveDuration];
    }
     
    cancelBlock:^(ActionSheetDatePicker *picker)
    {
        [self calculateLeaveDuration];
    } origin:sender];
    
}
- (IBAction)btnSelectEndDateTapped:(id)sender {
    if(_leaveRequestObj.leaveAtomicity==LeaveAtomicityMultiple){
        if(_leaveRequestObj.startDate!=NULL){
            [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleEndDate datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin)
             {
                 if([DateUtilities isDate:selectedDate laterThan:_leaveRequestObj.startDate]){
                     _leaveRequestObj.endDate = selectedDate;
                     _txtFieldEndDate.text = [DateUtilities getDateStringFromDate:selectedDate withFormat:kDateFormatDefaultStamp];
                    [self calculateLeaveDuration];
                     
                 }
                 else{
                     [self showMessageWithHeader:kResponseStatusError andBody:kValidationInvalidEndDateBody];
                     _txtFieldEndDate.text = @"";
                     _leaveRequestObj.endDate = NULL;
                     [self calculateLeaveDuration];
                 }
             }
             
            cancelBlock:^(ActionSheetDatePicker *picker)
             {
                [self calculateLeaveDuration];
             } origin:sender];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationSelectStartDateBody];
        }
        
        
    }
    
    
}
- (IBAction)btnSaveTapped:(id)sender {
    [self sendAnnualLeaveRequest];
}

-(IBAction)btnCancelTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Validation

-(BOOL) isDataValid{
    _leaveRequestObj.notes = _txtViewNotes.text;
    
    if(isEmpty(_leaveRequestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectStartDateBody];
        return NO;
    }
    if(_leaveRequestObj.duration <=0){
        if([DateUtilities checkIfDateLiesOnWeekendForAnnualLeave:_leaveRequestObj.startDate]){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationStartDateWeekend];
            return NO;
        }
    }
    
    
    if([DateUtilities isDate:_leaveRequestObj.startDate
                    EarlierThan:[NSDate date]]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationInvalidStartDatePast];
        return NO;
    }
    if(_leaveRequestObj.leaveAtomicity == LeaveAtomicityMultiple){
        if(_leaveRequestObj.duration <=0){
            if([DateUtilities checkIfDateLiesOnWeekendForAnnualLeave:_leaveRequestObj.endDate]){
                [self showMessageWithHeader:kResponseStatusError
                                    andBody:kValidationEndDateWeekend];
                return NO;
            }
        }
        
        if(isEmpty(_leaveRequestObj.endDate)){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationSelectEndDateBody];
            return NO;
        }
        if(![DateUtilities isDate:_leaveRequestObj.endDate
                        laterThan:_leaveRequestObj.startDate]){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationInvalidEndDateBody];
            return NO;
        }
    }
    if(_leaveRequestObj.duration > _statsModel.leavesRemaining){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:[NSString stringWithFormat:@"%@ %.2f %@.",kValidationLeaveQuotaExceeded,_statsModel.leavesRemaining,[[UserModel sharedInstance] userObject].employmentNature]];
        return NO;
    }
    if(!isEmpty(_leaveRequestObj.notes)){
        if([_leaveRequestObj.notes length]>100){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationNotesLimitExceeded];
            return NO;
        }
    }

    return YES;
}

#pragma mark - Network Operations

-(void) sendAnnualLeaveRequest{
    BOOL isValid = [self isDataValid];
    if(isValid){
        [self showProgressRingWithMessage:kProgressRecordingAnnualLeave];
        AnnualLeavesManager *mgr = [[AnnualLeavesManager alloc] init];
        [mgr recordAnnualLeaveModel:_leaveRequestObj
                  withResponseBlock:^(BOOL success, id response, NSString *error) {
            [self hideProgressRing];
            GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
            if(success){
                if(respObj.isSuccessFul){
                    [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                                 andBody:respObj.message];
                }
                else
                {
                    [self showMessageWithHeader:kResponseStatusError
                                                 andBody:respObj.message];
                }
            }
            else{
                [self showMessageWithHeader:kDomainAnnualLeaves
                                    andBody:error];
            }
        }];
    
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
