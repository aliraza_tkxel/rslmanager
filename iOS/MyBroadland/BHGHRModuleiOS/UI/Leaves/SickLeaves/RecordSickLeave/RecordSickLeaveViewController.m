//
//  RecordSickLeaveViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "RecordSickLeaveViewController.h"
#import <ActionSheetDatePicker.h>
#import <ActionSheetStringPicker.h>
#import "SicknessLeaveManager.h"
#import "GenericLeaveResponse.h"
@interface RecordSickLeaveViewController ()

@end

@implementation RecordSickLeaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    _leaveRequestObj = [GenericLeaveModel new];
    _amendSicknessRequest = [AmendSicknessRequestObject new];
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        [self setUpViewForNewLeave];
    }
    else{
        [self setUpViewForAmendingLeave];
    }
    _staffMembers = [[LookUpsDataManager sharedInstance] fetchAllStaffMembers];
    [self configureTextFields];
    
}


-(void) configureTextFields{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey andWidth:1];
    
    [_txtViewNotes addCompleteBorderWithColor:AppThemeColorGrey
                                     andWidth:1.0f];
    [_txtFieldReason addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1.0];
    [_txtFieldStartDate addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1.0];
    [_txtFieldMyStaff addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0];
    [_txtFieldAnticipatedReturnDate addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0];
    [_txtFieldReturnDate addBottomBorderWithColor:AppThemeColorGrey
                                                    andWidth:1.0];
    
}

-(void) setUpViewForNewLeave{
    _leaveRequestObj.leaveAtomicity = LeaveAtomicitySingle;
    _leaveRequestObj.leaveDurationStart = LeaveDurationFullDay;
    [_btnRequestLeave setTitle:@"Record" forState:UIControlStateNormal];
    _lblScreenTitle.text = @"Record Sickness";
    _btnSicknessHistory.hidden = YES;
    _imgViewMyStaffDropDownIcon.hidden = NO;
    _returnDateViewHeight.constant=0;
    _returnDateView.hidden = YES;
}
-(void) setUpViewForAmendingLeave{
    _leaveAtomicityView.hidden = YES;
    _btnSicknessHistory.hidden = NO;
    _leaveAtomicityViewHeight.constant = 0;
    [_btnRequestLeave setTitle:@"Save" forState:UIControlStateNormal];
    _lblScreenTitle.text = @"Amend Sickness";
    _imgViewMyStaffDropDownIcon.hidden = YES;
    [self loadDataForLeaveAmendment];
}

-(void) loadDataForLeaveAmendment{
    if(!isEmpty(_selectedMemberName)){
        _txtFieldMyStaff.text = _selectedMemberName;
    }
    if(!isEmpty(_selectedLeave)){
        _txtFieldReason.text = _selectedLeave.leaveReason;
        _amendSicknessRequest.reasonId = _selectedLeave.reasonId;
        
        _txtFieldStartDate.text = _selectedLeave.startDateDescription;
        _amendSicknessRequest.startDateRaw = _selectedLeave.startDate;
        
        if(!isEmpty(_selectedLeave.anticipatedReturnDate)){
            _txtFieldAnticipatedReturnDate.text = _selectedLeave.anticipatedReturnDateDescription;
            _amendSicknessRequest.anticipatedReturnDateRaw = _selectedLeave.anticipatedReturnDate;
        }
        if(!isEmpty(_selectedLeave.endDate)){
            _txtFieldAnticipatedReturnDate.text = _selectedLeave.endDateDescription;
            _amendSicknessRequest.endDateRaw = _selectedLeave.endDate;
        }
        
        _txtViewNotes.text = _selectedLeave.notes;
        _amendSicknessRequest.notes = _selectedLeave.notes;
        
        _amendSicknessRequest.absenceHistoryId = _selectedLeave.absenceHistoryId;
    }
}

#pragma mark - IBActions

- (IBAction)btnViewSicknessHistoryTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSelectAnticipatedReturnDateTapped:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleAntiReturnDate
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin){
                                         [self setAnticipatedEndDateForChosenRecordingType:selectedDate];
                                     } cancelBlock:^(ActionSheetDatePicker *picker){
                                         
                                     } origin:sender];
}

- (IBAction)btnSelectMyStaffTapped:(id)sender {
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        [ActionSheetStringPicker showPickerWithTitle:kSheetPickerStaffMember
                                                rows:[_staffMembers valueForKeyPath:kEntityLookUpVarLookUpDescription]
                                    initialSelection:0
                                           doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                               _txtFieldMyStaff.text = selectedValue;
                                               [self setSelectedStaffMemberForVal:selectedValue];
                                           }
                                         cancelBlock:^(ActionSheetStringPicker *picker) {
                                             
                                         }
                                              origin:sender];
    }
    
}
- (IBAction)btnSelectReturnDateTapped:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleReturnDate
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin){
                                         [self setEndDateForChosenRecordingType:selectedDate];
                                     } cancelBlock:^(ActionSheetDatePicker *picker){
                                         
                                     } origin:sender];
}
-(void) setSelectedStaffMemberForVal:(id)selectedVal{
    
    NSNumber *lookUpId = [[LookUpsDataManager sharedInstance] getLookUpIdForDescription:selectedVal forLookUpType:LookUpTypeMyStaff];
    _leaveRequestObj.selectedStaffMemberId = lookUpId;
    _selectedMemberId = lookUpId;
    
}


- (IBAction)btnSelectReasonTapped:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:kSheetPickerTitleReason
                                            rows:[[[LookUpsDataManager sharedInstance] fetchAllLeaveReasons] valueForKeyPath:kEntityLookUpVarLookUpDescription]
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           [self setReasonForChosenRecordingType:selectedIndex andDescription:selectedValue];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         
                                     }
                                          origin:sender];
}
- (IBAction)btnSelectStartDateTapped:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:kSheetPickerTitleStartDate datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin){
        [self setStartDateForChosenRecordingType:selectedDate];
     } cancelBlock:^(ActionSheetDatePicker *picker){
        
     } origin:sender];
    
}
- (IBAction)segmentCtrlDurationChanged:(id)sender {
    _leaveRequestObj.leaveDurationStart =  _segmentCtrlDuration.selectedSegmentIndex;
}
- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSaveTapped:(id)sender {
    [self.view endEditing:YES];
    BOOL isValid = NO;
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        _leaveRequestObj.notes = _txtViewNotes.text;
        isValid = [self validateRequestObjForNewLeave];
        if(isValid){
            [self applyNewLeave];
        }
    }
    else{
        _amendSicknessRequest.notes = _txtViewNotes.text;
        isValid = [self validateRequestObjForExistingLeave];
        if(isValid){
            [self amendExistingLeave];

        }
    }
    
}

#pragma mark - Set Data

-(void) setStartDateForChosenRecordingType:(NSDate*) startDate{
    
    _txtFieldStartDate.text = [DateUtilities getDateStringFromDate:startDate withFormat:kDateFormatDefaultStamp];
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        _leaveRequestObj.startDate = startDate;
    }
    else{
        _amendSicknessRequest.startDateRaw = startDate;
    }
}

-(void) setEndDateForChosenRecordingType:(NSDate*) endDate{
    
    _txtFieldReturnDate.text = [DateUtilities getDateStringFromDate:endDate withFormat:kDateFormatDefaultStamp];
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        _leaveRequestObj.endDate = endDate;
    }
    else{
        _amendSicknessRequest.endDateRaw = endDate;
    }
}

-(void) setAnticipatedEndDateForChosenRecordingType:(NSDate*) antiEndDate{
    _txtFieldAnticipatedReturnDate.text = [DateUtilities getDateStringFromDate:antiEndDate withFormat:kDateFormatDefaultStamp];
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        _leaveRequestObj.anticipatedReturnDate = antiEndDate;
    }
    else{
        _amendSicknessRequest.anticipatedReturnDateRaw = antiEndDate;
    }
}

-(void) setReasonForChosenRecordingType:(NSInteger) reasonId andDescription:(NSString *) descriptionStr{
    _txtFieldReason.text = descriptionStr;
    
    if(_sicknessRecordingType == SicknessRecordingTypeNew){
        _leaveRequestObj.reasonDescription = descriptionStr;
        _leaveRequestObj.reasonId = [[[LookUpsDataManager sharedInstance] getLookUpIdForDescription:descriptionStr forLookUpType:LookUpTypeLeaveReason] intValue];
    }
    else{
        _amendSicknessRequest.reasonId =[[LookUpsDataManager sharedInstance] getLookUpIdForDescription:descriptionStr forLookUpType:LookUpTypeLeaveReason];
    }
}

#pragma mark - Network Operations

-(void) amendExistingLeave{
    if(!isEmpty(_amendSicknessRequest.endDateRaw)){
        _amendSicknessRequest.duration = [NSNumber numberWithFloat:[DateUtilities
                                                                    calculateWorkingDaysForSicknessBetween:_amendSicknessRequest.startDateRaw
                                                                    andEndDate:_amendSicknessRequest.endDateRaw]];
        _amendSicknessRequest.endDate = [DateUtilities getDateStringFromDate:_amendSicknessRequest.endDateRaw
                                                                  withFormat:kDateFormatDefaultStamp];
    }
    else{
        _amendSicknessRequest.duration = [NSNumber numberWithFloat:0.00];
    }
    if(!isEmpty(_amendSicknessRequest.anticipatedReturnDateRaw)){
        _amendSicknessRequest.anticipatedReturnDate = [DateUtilities getDateStringFromDate:_amendSicknessRequest.anticipatedReturnDateRaw withFormat:kDateFormatDefaultStamp];
    }
    _amendSicknessRequest.startDate = [DateUtilities getDateStringFromDate:_amendSicknessRequest.startDateRaw
                                                                withFormat:kDateFormatDefaultStamp];
    _amendSicknessRequest.actionBy = [NSNumber numberWithInt:LoggedInUserId];
    _amendSicknessRequest.notes = _txtViewNotes.text;
    [self showProgressRingWithMessage:kProgressAmendingSickLeave];
    SicknessLeaveManager *mgr = [[SicknessLeaveManager alloc] init];
    [mgr amendExistingSicknessObjectWith:_amendSicknessRequest withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
        if(success){
            if(respObj.isSuccessFul){
                [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                             andBody:respObj.message];
            }
            else{
                [self showMessageWithHeader:kResponseStatusError
                                    andBody:respObj.message];
            }
        }
        else{
            [self showMessageWithHeader:kDomainSickLeaves
                                andBody:error];
        }
    }];
    
}

-(void) applyNewLeave{
    _leaveRequestObj.notes = _txtViewNotes.text;
    _leaveRequestObj.endDate = _leaveRequestObj.startDate;/*Sending start date as end date because of API crew's requirements*/
    [self showProgressRingWithMessage:kProgressRecordingSickLeave];
    SicknessLeaveManager *mgr = [[SicknessLeaveManager alloc] init];
    [mgr recordSickLeaveWithModel:_leaveRequestObj
                withResponseBlock:^(BOOL success, id response, NSString *error) {
                    [self hideProgressRing];
                    GenericLeaveResponse *respObj = (GenericLeaveResponse *) response;
                    if(success){
                        if(respObj.isSuccessFul){
                            [self showMessageAndReturnWithHeader:kResponseStatusSuccess
                                                         andBody:respObj.message];
                        }
                        else{
                            [self showMessageWithHeader:kResponseStatusError
                                                andBody:respObj.message];
                        }
                    }
                    else{
                        [self showMessageWithHeader:kDomainSickLeaves
                                            andBody:error];
                    }
                }];
}

#pragma mark - Validation

-(BOOL) validateRequestObjForNewLeave{
    if(isEmpty(_leaveRequestObj.selectedStaffMemberId)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStaffMemberSelection];
        return NO;
    }
    if(isEmpty(_leaveRequestObj.reasonDescription)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectReason];
        return NO;
    }
    if(isEmpty(_leaveRequestObj.startDate)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectStartDateBody];
        return NO;
    }
    if([DateUtilities checkIfDateLiesOnWeekend:_leaveRequestObj.startDate]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStartDateWeekend];
        return NO;
    }
    if(![DateUtilities isDate:_leaveRequestObj.startDate
                       SameAs:[NSDate date]] && ![DateUtilities isDate:_leaveRequestObj.startDate
                                                                EarlierThan:[NSDate date]]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationInvalidStartDatePastEarlier];
        return NO;
    }
    if([DateUtilities isDateABankHoliday:_leaveRequestObj.startDate]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationBankHolidayStartDate];
        return NO;
    }
    if(!isEmpty(_leaveRequestObj.notes)){
        if([_leaveRequestObj.notes length]>1000){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationNotesLimitExceeded];
            return NO;
        }
    }
    return YES;
}

-(BOOL) validateRequestObjForExistingLeave{
    if(isEmpty(_amendSicknessRequest.absenceHistoryId)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationCorruptedAbsenceHistory];
        return NO;
    }
    if(isEmpty(_amendSicknessRequest.reasonId)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectReason];
        return NO;
    }
    if(isEmpty(_amendSicknessRequest.startDateRaw)){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationSelectStartDateBody];
        return NO;
    }
    if([DateUtilities checkIfDateLiesOnWeekend:_amendSicknessRequest.startDateRaw]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationStartDateWeekend];
        return NO;
    }
    if([DateUtilities isDateABankHoliday:_amendSicknessRequest.startDateRaw]){
        [self showMessageWithHeader:kResponseStatusError
                            andBody:kValidationBankHolidayStartDate];
        return NO;
    }
    if(!isEmpty(_amendSicknessRequest.endDateRaw)){
        if(![DateUtilities isDate:_amendSicknessRequest.endDateRaw
                        laterThan:_amendSicknessRequest.startDateRaw]){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationInvalidReturnDateBody];
            return NO;
        }
        if([DateUtilities checkIfDateLiesOnWeekend:_amendSicknessRequest.endDateRaw]){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationReturnDateWeekend];
            return NO;
        }
    }
    
    if(!isEmpty(_amendSicknessRequest.notes)){
        if([_amendSicknessRequest.notes length]>1000){
            [self showMessageWithHeader:kResponseStatusError
                                andBody:kValidationNotesLimitExceeded];
            return NO;
        }
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
