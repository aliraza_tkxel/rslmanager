//
//  RecordSickLeaveViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericLeaveModel.h"
#import "BaseViewController.h"
#import "StaffMemberSicknessSummaryModel.h"
#import "SicknessLeaveRecordModel.h"
#import "AmendSicknessRequestObject.h"
@interface RecordSickLeaveViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnRequestLeave;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnSicknessHistory;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMyStaffDropDownIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblScreenTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectReason;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldReason;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectStartDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldReturnDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnDateViewHeight;
@property (weak, nonatomic) IBOutlet UIView *leaveAtomicityView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leaveAtomicityViewHeight;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlDuration;
@property (weak, nonatomic) IBOutlet UIView *returnDateView;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMyStaff;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAnticipatedReturnDate;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes;
@property GenericLeaveModel* leaveRequestObj;
@property AmendSicknessRequestObject *amendSicknessRequest;
@property (copy,nonatomic) NSArray *staffMembers;
@property NSNumber *selectedMemberId;
@property NSString *selectedMemberName;
@property SicknessRecordingType sicknessRecordingType;
@property SicknessLeaveRecordModel *selectedLeave;
@end
