//
//  SicknessHistoryViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/30/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, SicknessLeaveHistoryType) {
    SicknessLeaveHistoryTypeMy,
    SicknessLeaveHistoryTypeStaff
};
@interface SicknessHistoryViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnRevealMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPageIcon;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCtrlSicknessHistory;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (copy,nonatomic) NSMutableDictionary *sourceDictForMyLeave;
@property (copy, nonatomic) NSArray *sectionsArrayForMyLeave;

@property (copy,nonatomic) NSMutableDictionary *sourceDictForStaffLeave;
@property (copy, nonatomic) NSArray *sectionsArrayForStaffLeave;
@property (copy,nonatomic) NSArray * rawStaffMembersList;
@property NSInteger selectedObjectIndex;
@property NSInteger selectedObjectSectionIndex;

@property SicknessLeaveHistoryType selectedHistoryViewingType;
@end
