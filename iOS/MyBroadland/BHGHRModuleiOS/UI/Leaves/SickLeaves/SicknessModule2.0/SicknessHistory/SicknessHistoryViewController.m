//
//  SicknessHistoryViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 11/30/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SicknessHistoryViewController.h"
#import "SicknessCompleteHistoryTableViewCell.h"
#import "MyStaffSummaryTableViewCell.h"
#import "SicknessLeaveRecordModel.h"
#import "SicknessLeaveRecord.h"
#import "FetchSicknessListResponse.h"
#import "SicknessLeaveManager.h"
#import "StaffMemberSicknessSummaryModel.h"
#import "MyStaffSicknessHistoryViewController.h"
#import "RecordSickLeaveViewController.h"
@interface SicknessHistoryViewController ()

@end

@implementation SicknessHistoryViewController
-(void) viewWillAppear:(BOOL)animated{
    
    [self fetchSickLeaveListing];
    [[HRMFloatingButtonManager sharedInstance] hideButtons];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init

-(void) initModule{
    [self configureRevealMenu];
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1];
    [_imgViewPageIcon addCompleteBorderWithColor:AppThemeColorGrey
                                   andWidth:1.0];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    _tblView.allowsSelection = NO;
    [_tblView registerNib:[UINib nibWithNibName:@"SicknessCompleteHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:kSickLeaveTableViewCellReuseIdentifier];
    [_tblView registerNib:[UINib nibWithNibName:@"MyStaffSummaryTableViewCell" bundle:nil] forCellReuseIdentifier:kSickLeaveStaffTableViewCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _selectedHistoryViewingType = SicknessLeaveHistoryTypeMy;
    [[HRMFloatingButtonManager sharedInstance] addFloatingButtonsWithSize:2
                                                    andButtonDescriptions:@[@"Sickness History",@"Record Sickness"]
                                                          andButtonImages:@[[UIImage imageNamed:@"sickness_firstaid_icon.png"],[UIImage imageNamed:@"plus_icon.png"]]
                                                        forViewController:self
                                                        withResponseBlock:^(NSUInteger selectedIndex, BOOL success, NSString *error)
    {
        if(success){
            if(selectedIndex==2){
                if(!isEmpty(_rawStaffMembersList)){
                    [self performSegueWithIdentifier:kSegueFromNewSicknessToRecordSickness sender:self];
                }
                else{
                    [self showMessageWithHeader:kResponseStatusError andBody:kValidationNoReportingStaff];
                }
            }
            
            
        }
                                                            
     
     }];
}

-(void) configureRevealMenu{
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.rearViewRevealWidth = kRearViewRevealWidth;
    if (revealViewController) {
        [self.btnRevealMenu addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

#pragma mark  - IBActions
- (IBAction)segmentCtrlSicknessHistoryTypeChanged:(id)sender {
    if(_segmentCtrlSicknessHistory.selectedSegmentIndex==0){
        _selectedHistoryViewingType = SicknessLeaveHistoryTypeMy;
    }
    else{
        _selectedHistoryViewingType = SicknessLeaveHistoryTypeStaff;
    }
    [_tblView reloadData];
    @try {
        [self setUpNoResultsView];
        [_tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                        atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } @catch (NSException *exception) {
        NSLog(@"Can't scroll to the top dummy, the secion 0 is empty");
    } @finally {
        
    }
    
    
    //[_tblView setContentOffset:CGPointZero animated:YES];
}

-(void) setUpNoResultsView{
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        if(isEmpty(_sectionsArrayForMyLeave)){
            [self showNoResultsFoundView];
        }
        else{
            [self hideNoResultsFoundView];
        }
    }
    else{
        [self hideNoResultsFoundView];
    }
}

#pragma mark - Network Operations and DataSource

-(void) fetchSickLeaveListing{
    SicknessLeaveManager *manager = [SicknessLeaveManager new];
    [self showProgressRingWithMessage:kProgressFetchingSicknessLeaves];
    [manager fetchSicknessLeavesListForUserId:LoggedInUserId withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            FetchSicknessListResponse * respObj = (FetchSicknessListResponse *) response;
            _sourceDictForMyLeave = [self parseAndFilterDataSourceForMyLeaves:respObj.sicknessAbsences];
            _sectionsArrayForMyLeave = [[NSArray alloc] initWithArray:[_sourceDictForMyLeave allKeys]];
            _sourceDictForStaffLeave = [self parseAndFilterDataSourceForMyStaffLeaves:respObj.staffMembers];
            _sectionsArrayForStaffLeave = [[NSArray alloc] initWithObjects:kStaffSicknessOnLeave,kStaffSicknessInWork, nil];
            [self sortSectionsArray];
            [self setUpNoResultsView];
            [_tblView reloadData];
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}

-(NSMutableDictionary *) parseAndFilterDataSourceForMyStaffLeaves:(NSArray *) dataSource{
     NSMutableDictionary *sourceDict = [NSMutableDictionary new];
     NSMutableArray *rawDataDump = [NSMutableArray new];
    for(id record in dataSource){
        StaffMemberSicknessSummaryModel *model = [StaffMemberSicknessSummaryModel new];
        [model initWithResponseObj:record];
        NSMutableArray *arr;
        if([sourceDict objectForKey:model.groupKey]){
            arr = [sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [rawDataDump addObject:model];
        [sourceDict setObject:arr forKey:model.groupKey];
    }
    _rawStaffMembersList = [[NSArray alloc] initWithArray:rawDataDump];
    return sourceDict;
}

-(NSMutableDictionary *) parseAndFilterDataSourceForMyLeaves:(NSArray *) dataSource{
    NSMutableDictionary *sourceDict = [NSMutableDictionary new];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"startDate"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [dataSource
                                 sortedArrayUsingDescriptors:sortDescriptors];
    dataSource = [NSArray arrayWithArray:sortedEventArray];
    
    for(id record in dataSource){
        SicknessLeaveRecordModel *model = [SicknessLeaveRecordModel new];
        [model initWithSicknessRecord:record];
        NSMutableArray *arr;
        if([sourceDict objectForKey:model.groupKey]){
            arr = [sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [sourceDict setObject:arr forKey:model.groupKey];
        
    }
    return sourceDict;
}

-(void) sortSectionsArray {
    if(!isEmpty(_sectionsArrayForMyLeave)){
        NSMutableArray *arr = [NSMutableArray new];
        for(NSString * str in _sectionsArrayForMyLeave){
            NSString *dateStr = [NSString stringWithFormat:@"1/%@",str];
            NSDate *dateObj = [DateUtilities getDateFromString:dateStr withFormat:kDateFormatDefaultStamp];
            [arr addObject:dateObj];
        }
        NSArray *reverseOrderUsingComparator = [arr sortedArrayUsingComparator:
                                                ^(id obj1, id obj2) {
                                                    return [obj2 compare:obj1]; // note reversed comparison here
                                                }];
        [arr removeAllObjects];
        for(NSDate * date in reverseOrderUsingComparator){
            NSString *str = [DateUtilities getDateStringFromDate:date withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
            [arr addObject:str];
        }
        _sectionsArrayForMyLeave = [NSArray arrayWithArray:arr];
    }
    if(!isEmpty(_sectionsArrayForStaffLeave)){
        _sectionsArrayForStaffLeave = [[NSMutableArray alloc] initWithObjects:kStaffSicknessOnLeave,kStaffSicknessInWork, nil];
    }
}

#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        return [_sectionsArrayForMyLeave count];
    }
    
    return [_sectionsArrayForStaffLeave count];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        NSArray *arr = [_sourceDictForMyLeave objectForKey:[_sectionsArrayForMyLeave objectAtIndex:section]];
        return [arr count];
    }
    NSArray *arr = [_sourceDictForStaffLeave objectForKey:[_sectionsArrayForStaffLeave objectAtIndex:section]];
    return [arr count];
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        SicknessCompleteHistoryTableViewCell *customCell = (SicknessCompleteHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSickLeaveTableViewCellReuseIdentifier];
        customCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *sourceArray = [_sourceDictForMyLeave objectForKey:[_sectionsArrayForMyLeave objectAtIndex:indexPath.section]];
        [customCell configureCellForRecord:[sourceArray objectAtIndex:indexPath.row]];
        cell = customCell;
    }
    else{
        MyStaffSummaryTableViewCell *customCell = (MyStaffSummaryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSickLeaveStaffTableViewCellReuseIdentifier];
        customCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *sourceArray = [_sourceDictForStaffLeave objectForKey:[_sectionsArrayForStaffLeave objectAtIndex:indexPath.section]];
        [customCell configureCellWithStaffMemberSummary:[sourceArray objectAtIndex:indexPath.row]];
        cell = customCell;
    }
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        return 120;
    }
    
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSArray *arr = [NSArray new];
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
        arr = [_sourceDictForMyLeave objectForKey:[_sectionsArrayForMyLeave objectAtIndex:section]];
    }
    else{
        arr = [_sourceDictForStaffLeave objectForKey:[_sectionsArrayForStaffLeave objectAtIndex:section]];
    }
    return (arr.count>0)?30:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeMy){
       label.text = [_sectionsArrayForMyLeave objectAtIndex:section];
    }
    else{
        label.text = [_sectionsArrayForStaffLeave objectAtIndex:section];
    }
    
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor blackColor]];
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_selectedHistoryViewingType == SicknessLeaveHistoryTypeStaff){
        _selectedObjectIndex = indexPath.row;
        _selectedObjectSectionIndex = indexPath.section;
        [self performSegueWithIdentifier:kSegueFromSicknessToStaffHistory sender:self];
        
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:kSegueFromSicknessToStaffHistory]){
        NSString *sectionKey = (_selectedObjectSectionIndex>0)?kStaffSicknessInWork:kStaffSicknessOnLeave;
        StaffMemberSicknessSummaryModel *selectedStaffMember = [[_sourceDictForStaffLeave objectForKey:sectionKey] objectAtIndex:_selectedObjectIndex];
        MyStaffSicknessHistoryViewController *destVC = [segue destinationViewController];
        destVC.selectedStaffMember = selectedStaffMember;
        destVC.staffMembers = _rawStaffMembersList;
    }
    else if([[segue identifier] isEqualToString:kSegueFromNewSicknessToRecordSickness]){
        RecordSickLeaveViewController *destVC = [segue destinationViewController];
        destVC.staffMembers = _rawStaffMembersList;
        destVC.sicknessRecordingType = SicknessRecordingTypeNew;
    }
}


@end
