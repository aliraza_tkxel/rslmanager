//
//  MyStaffSicknessHistoryViewController.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/4/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseViewController.h"
#import "StaffMemberSicknessSummaryModel.h"
@interface MyStaffSicknessHistoryViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldStaffMember;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectStaffMember;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;

@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (copy,nonatomic) NSMutableDictionary *sourceDict;
@property (copy, nonatomic) NSArray *sectionsArray;
@property NSInteger selectedObjectIndex;
@property NSInteger selectedObjectSectionIndex;
@property BOOL isRecordingNewLeave;

@property (copy,nonatomic) NSArray *staffMembers;
@property StaffMemberSicknessSummaryModel *selectedStaffMember;
@end
