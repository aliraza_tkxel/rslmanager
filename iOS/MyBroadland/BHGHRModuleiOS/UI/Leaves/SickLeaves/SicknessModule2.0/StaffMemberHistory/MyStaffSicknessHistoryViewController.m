//
//  MyStaffSicknessHistoryViewController.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/4/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MyStaffSicknessHistoryViewController.h"
#import "SicknessCompleteHistoryTableViewCell.h"
#import "SicknessLeaveRecordModel.h"
#import "SicknessLeaveRecord.h"
#import "FetchSicknessListResponse.h"
#import "SicknessLeaveManager.h"
#import "RecordSickLeaveViewController.h"
#import <ActionSheetPicker.h>
@interface MyStaffSicknessHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation MyStaffSicknessHistoryViewController
-(void) viewWillAppear:(BOOL)animated{
    
    [self fetchSickLeaveListing];
     [[HRMFloatingButtonManager sharedInstance] hideButtons];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

-(void) initModule{
    [_navBarView addBottomBorderWithColor:AppThemeColorGrey
                                 andWidth:1];
    [_txtFieldStaffMember addBottomBorderWithColor:AppThemeColorGrey
                                        andWidth:1.0];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    self.tblView.tableFooterView = [UIView new];
    [_tblView registerNib:[UINib nibWithNibName:@"SicknessCompleteHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:kSickLeaveTableViewCellReuseIdentifier];
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 120.0;
    _tblView.allowsSelection = YES;
    _txtFieldStaffMember.text = _selectedStaffMember.fullName;
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES comparator:^(NSString *obj1, NSString *obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch | NSCaseInsensitiveSearch];
        
    }];
    
   _staffMembers =  [_staffMembers sortedArrayUsingDescriptors:@[sd]];
    
    
    [[HRMFloatingButtonManager sharedInstance] addFloatingButtonsWithSize:2
                                                    andButtonDescriptions:@[@"Sickness History",@"Record Sickness"]
                                                          andButtonImages:@[[UIImage imageNamed:@"sickness_firstaid_icon.png"],[UIImage imageNamed:@"plus_icon.png"]]
                                                        forViewController:self
                                                        withResponseBlock:^(NSUInteger selectedIndex, BOOL success, NSString *error)
     {
         if(success){
             if(selectedIndex==2){
                 if(!isEmpty(_staffMembers)){
                     _isRecordingNewLeave = YES;
                     [self performSegueWithIdentifier:kSegueFromStaffSicknessToAmendSickness sender:self];
                 }
                 else{
                     [self showMessageWithHeader:kResponseStatusError andBody:kValidationNoReportingStaff];
                 }
                 
             }
             
             
         }
         
         
     }];
    
    [_tblView reloadData];
    
}

#pragma mark - IBActions

- (IBAction)btnSelectStaffTapped:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:kSheetPickerStaffMember
                                            rows:[_staffMembers valueForKeyPath:@"fullName"]
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           _txtFieldStaffMember.text = selectedValue;
                                           [self setSelectedStaffMemberForVal:selectedValue];
                                           [self fetchSickLeaveListing];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         
                                     }
                                          origin:sender];
    
}
- (IBAction)btnBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) setSelectedStaffMemberForVal:(id)selectedVal{
    NSArray *entities = [[NSArray alloc] initWithArray:_staffMembers];
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName == %@",selectedVal];
    entities = [entities filteredArrayUsingPredicate:filterPredicate];
    if([entities count]>0){
        _selectedStaffMember = [entities objectAtIndex:0];
    }

}

#pragma mark - Network Operations and DataSource

-(void) fetchSickLeaveListing{
    SicknessLeaveManager *manager = [SicknessLeaveManager new];
    [self showProgressRingWithMessage:kProgressFetchingSicknessLeaves];
    [manager fetchSicknessLeavesListForUserId:[_selectedStaffMember.staffMemberId intValue] withResponseBlock:^(BOOL success, id response, NSString *error) {
        [self hideProgressRing];
        if(success){
            FetchSicknessListResponse * respObj = (FetchSicknessListResponse *) response;
            if(isEmpty(respObj.sicknessAbsences)){
                [self showMessageWithHeader:kDomainSickLeaves
                                    andBody:kNoLeavesFound];
                _sourceDict = [NSMutableDictionary new];
                _sectionsArray = [NSArray new];
                [_tblView reloadData];
            }
            else{
                _sourceDict = [self parseAndFilterDataSourceForMyLeaves:respObj.sicknessAbsences];
                _sectionsArray = [[NSArray alloc] init];
                _sectionsArray = [_sourceDict allKeys];
                [self sortSectionsArray];
                [_tblView reloadData];
            }
        }
        else{
            [self showMessageWithHeader:kResponseStatusError
                                andBody:error];
        }
    }];
}


-(NSMutableDictionary *) parseAndFilterDataSourceForMyLeaves:(NSArray *) dataSource{
    NSMutableDictionary *sourceDict = [NSMutableDictionary new];
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"startDate"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [dataSource
                                 sortedArrayUsingDescriptors:sortDescriptors];
    dataSource = [NSArray arrayWithArray:sortedEventArray];
    
    for(id record in dataSource){
        SicknessLeaveRecordModel *model = [SicknessLeaveRecordModel new];
        [model initWithSicknessRecord:record];
        NSMutableArray *arr;
        if([sourceDict objectForKey:model.groupKey]){
            arr = [sourceDict objectForKey:model.groupKey];
        }
        else{
            arr = [[NSMutableArray alloc] init];
        }
        
        [arr addObject:model];
        [sourceDict setObject:arr forKey:model.groupKey];
        
    }
    return sourceDict;
}

-(void) sortSectionsArray {
    if(!isEmpty(_sectionsArray)){
        NSMutableArray *arr = [NSMutableArray new];
        for(NSString * str in _sectionsArray){
            NSString *dateStr = [NSString stringWithFormat:@"1/%@",str];
            NSDate *dateObj = [DateUtilities getDateFromString:dateStr withFormat:kDateFormatDefaultStamp];
            [arr addObject:dateObj];
        }
        NSArray *reverseOrderUsingComparator = [arr sortedArrayUsingComparator:
                                                ^(id obj1, id obj2) {
                                                    return [obj2 compare:obj1]; // note reversed comparison here
                                                }];
        [arr removeAllObjects];
        for(NSDate * date in reverseOrderUsingComparator){
            NSString *str = [DateUtilities getDateStringFromDate:date withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
            [arr addObject:str];
        }
        _sectionsArray = [NSArray arrayWithArray:arr];
    }
}

#pragma mark - TableView Data source and Delegates



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    SicknessLeaveRecordModel *model = [sourceArray objectAtIndex:indexPath.row];
    _selectedObjectIndex = indexPath.row;
    _selectedObjectSectionIndex = indexPath.section;
    if(!model.isOnWork){
        _isRecordingNewLeave = NO;
        [self performSegueWithIdentifier:kSegueFromStaffSicknessToAmendSickness sender:self];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sectionsArray count];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return [arr count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    SicknessCompleteHistoryTableViewCell *customCell = (SicknessCompleteHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:kSickLeaveTableViewCellReuseIdentifier];
    customCell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:indexPath.section]];
    [customCell configureCellForRecord:[sourceArray objectAtIndex:indexPath.row]];
    cell = customCell;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedheightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSArray *arr = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:section]];
    return (arr.count>0)?30:0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    [label setFont:AppThemeFontWithSize(17)];
    [label setTextColor:[UIColor whiteColor]];
    
    label.text = [_sectionsArray objectAtIndex:section];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor blackColor]];
    return view;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:kSegueFromStaffSicknessToAmendSickness]){
        RecordSickLeaveViewController *destVC = [segue destinationViewController];
        if(!_isRecordingNewLeave){
            NSArray *sourceArray = [_sourceDict objectForKey:[_sectionsArray objectAtIndex:_selectedObjectSectionIndex]];
            SicknessLeaveRecordModel *model = [sourceArray objectAtIndex:_selectedObjectIndex];
            destVC.sicknessRecordingType = SicknessRecordingTypeAmendment;
            destVC.selectedLeave = model;
            destVC.selectedMemberId = _selectedStaffMember.staffMemberId;
            destVC.selectedMemberName = _selectedStaffMember.fullName;
        }
        else{
            destVC.sicknessRecordingType = SicknessRecordingTypeNew;
        }
        destVC.staffMembers = _staffMembers;
        
    }
}

@end
