//
//  MyStaffSummaryTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "MyStaffSummaryTableViewCell.h"

@implementation MyStaffSummaryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCellWithStaffMemberSummary:(StaffMemberSicknessSummaryModel *) model{
    _lblStaffMemberName.text = model.fullName;
    _lblLeaveStartDate.text = model.startDateDescription;
    _lblLeaveStartDate.hidden = isEmpty(model.startDate);
}

@end
