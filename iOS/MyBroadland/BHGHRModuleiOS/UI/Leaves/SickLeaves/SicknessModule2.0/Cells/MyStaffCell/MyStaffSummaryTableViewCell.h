//
//  MyStaffSummaryTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaffMemberSicknessSummaryModel.h"
@interface MyStaffSummaryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblStaffMemberName;
@property (weak, nonatomic) IBOutlet UILabel *lblLeaveStartDate;
-(void) configureCellWithStaffMemberSummary:(StaffMemberSicknessSummaryModel *) model;
@end
