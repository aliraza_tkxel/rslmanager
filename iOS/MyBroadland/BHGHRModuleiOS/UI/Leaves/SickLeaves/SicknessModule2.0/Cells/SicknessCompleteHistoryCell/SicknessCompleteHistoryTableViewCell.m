//
//  SicknessCompleteHistoryTableViewCell.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/4/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SicknessCompleteHistoryTableViewCell.h"

@implementation SicknessCompleteHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) configureCellForRecord:(SicknessLeaveRecordModel *)model{
    _lblReason.text = model.leaveReason;
    _lblEndDate.text = model.endDateDescription;
    _lblDuration.text = model.durationDescription;
    _lblStartDate.text = model.startDateDescription;
    _lblAnticipatedReturnDate.text = model.anticipatedReturnDateDescription;
    if(!model.isOnWork){
        _anticipatedDateView.hidden = NO;
        _revealDetailIcon.hidden = NO;
        _endDateView.hidden = YES;
        _durationView.hidden = YES;
    }
    else{
        _anticipatedDateView.hidden = YES;
        _revealDetailIcon.hidden = YES;
        _endDateView.hidden = NO;
        _durationView.hidden = NO;
    }
}
@end
