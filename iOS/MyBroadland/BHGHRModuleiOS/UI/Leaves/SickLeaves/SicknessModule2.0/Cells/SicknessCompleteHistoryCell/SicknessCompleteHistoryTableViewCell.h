//
//  SicknessCompleteHistoryTableViewCell.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/4/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SicknessLeaveRecordModel.h"
@interface SicknessCompleteHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *durationView;
@property (weak, nonatomic) IBOutlet UIView *anticipatedDateView;
@property (weak, nonatomic) IBOutlet UIView *endDateView;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UIImageView *revealDetailIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblAnticipatedReturnDate;
-(void) configureCellForRecord:(SicknessLeaveRecordModel*) model;
@end
