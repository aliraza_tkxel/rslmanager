//
//  UserBasicInfoModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserBasicInfoModel.h"

@implementation UserBasicInfoModel

-(void) initWithUserObject:(UserObject *)object{
    self.employeeFullName = (!isEmpty(object.employeeFullName)) ? object.employeeFullName : @"";
    self.isActive = [conditionallyAssign(object.isActive, @0) intValue];
    self.userId = [conditionallyAssign(object.userId, @0) intValue];
    self.userName = (!isEmpty(object.userName)) ? object.userName : kValueNotApplicable;
    self.teamId = [conditionallyAssign(object.teamId, @0) intValue];
    self.jobRole = (!isEmpty(object.jobRole)) ? object.jobRole : kValueNotApplicable;
    self.gender = (!isEmpty(object.gender)) ? object.gender : kValueNotApplicable;
    self.dob = object.dob;
    self.dateOfBirthString = (!isEmpty(object.dob)) ? [DateUtilities getDateStringFromDate:object.dob
                                                                                withFormat:kDateFormatDefaultStamp] : kValueNotApplicable;
    self.age = (!isEmpty(object.dob)) ? [DateUtilities calculateAgeForPersonBornOn:object.dob] : kValueNotApplicable;
    self.employmentNature = object.employmentNature;
    
    self.leaveYearStart = object.leaveYearStart;
    self.leaveYearStartString = (!isEmpty(object.leaveYearStart)) ? [DateUtilities getDateStringFromDate:object.leaveYearStart
                                                                                withFormat:kDateFormatDefaultStamp] : kValueNotApplicable;
    
    self.leaveYearEnd = object.leaveYearEnd;
    self.leaveYearEndString = (!isEmpty(object.leaveYearEnd)) ? [DateUtilities getDateStringFromDate:object.leaveYearEnd
                                                                                              withFormat:kDateFormatDefaultStamp] : kValueNotApplicable;
    
    self.alStartDate = object.alStartDate;
    self.alStartDateString = (!isEmpty(object.alStartDate)) ? [DateUtilities getDateStringFromDate:object.alStartDate
                                                                                          withFormat:kDateFormatDefaultStamp] : kValueNotApplicable;
    
    self.selectedYearRange = [NSString stringWithFormat:@"%@ - %@",self.leaveYearStartString,self.leaveYearEndString];
    
}



@end
