//
//  UserContactInfoModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserContactInfoModel.h"

@implementation UserContactInfoModel

-(void) initWithContactDetails:(ContactDetailResponseObject *)respObj{
    self.workPhone = (!isEmpty(respObj.workPhone))  ? respObj.workPhone :   @"";
    self.workMobile = (!isEmpty(respObj.workMobile))  ? respObj.workMobile :   @"";
    self.workEmail = (!isEmpty(respObj.workEmail))  ? respObj.workEmail :   @"";
    
    
    self.mobilePersonal = (!isEmpty(respObj.mobilePersonal))  ? respObj.mobilePersonal :   @"";
    self.mobile = (!isEmpty(respObj.mobile))  ? respObj.mobile :   @"";
    self.homePhone = (!isEmpty(respObj.homePhone))  ? respObj.homePhone :   @"";
    self.homeEmail = (!isEmpty(respObj.homeEmail))  ? respObj.homeEmail :   @"";
    
    self.addressLine1 = (!isEmpty(respObj.addressLine1))  ? respObj.addressLine1 :   @"";
    self.addressLine2 = (!isEmpty(respObj.addressLine2))  ? respObj.addressLine2 :   @"";
    self.addressLine3 = (!isEmpty(respObj.addressLine3))  ? respObj.addressLine3 :   @"";
    
    self.postalTown = (!isEmpty(respObj.postalTown))  ? respObj.postalTown :   @"";
    self.county = (!isEmpty(respObj.county))  ? respObj.county :   @"";
    self.postalCode = (!isEmpty(respObj.postalCode))  ? respObj.postalCode :   @"";
    self.emergencyContactNumber = (!isEmpty(respObj.emergencyContactNumber))  ? respObj.emergencyContactNumber :   @"";
    self.emergencyContactPerson = (!isEmpty(respObj.emergencyContactPerson))  ? respObj.emergencyContactPerson :   @"";
    self.emergencyContactRelationship = (!isEmpty(respObj.emergencyContactRelationship))  ? respObj.emergencyContactRelationship :   @"";
    
    
}

@end
