//
//  UserPersonalInfoModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersonalDetailResponseObject.h"

@interface UserPersonalInfoModel : NSObject

@property (copy,nonatomic) NSString * firstName;
@property (copy,nonatomic) NSString * lastName;
@property (copy,nonatomic) NSDate * dateOfBirth;
@property (copy,nonatomic) NSString *age;
@property (copy,nonatomic) NSString *dateOfBirthString;
@property (copy,nonatomic) NSString * gender;
@property (copy, nonatomic) NSNumber* maritalStatusId;
@property (copy, nonatomic) NSString* maritalStatusDescription;
@property (copy, nonatomic) NSNumber* ethnicityId;
@property (copy, nonatomic) NSString* ethnicityDescription;
@property (copy, nonatomic) NSNumber* religionId;
@property (copy, nonatomic) NSString* religionDescription;
@property (copy,nonatomic) NSNumber * sexualOrientationId;
@property (copy, nonatomic) NSString* sexualOrientationDescription;


-(void) initWithPersonalDetails:(PersonalDetailResponseObject *)respObj;


@end
