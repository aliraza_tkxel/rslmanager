//
//  UserModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel
+ (id)sharedInstance
{
    static UserModel *sharedUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [[self alloc] init];
    });
    return sharedUser;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
        
    }
    return self;
}

-(void) setUserObjectInfo:(UserObject *)userObj{
    _userObject = [UserBasicInfoModel new];
    [_userObject initWithUserObject:userObj];
}

-(void) setUserProfile:(id)responseObejct{
    _userContactInfo = [UserContactInfoModel new];
    _userPersonalInfo = [UserPersonalInfoModel new];
    if([responseObejct isKindOfClass:[FetchUserProfileResponseObject class]]){
        FetchUserProfileResponseObject *fetchResp = (FetchUserProfileResponseObject *)responseObejct;
        [_userContactInfo initWithContactDetails:fetchResp.contactDetails];
        [_userPersonalInfo initWithPersonalDetails:fetchResp.personalDetails];
    }
    
}

-(void) setUserContactInfo:(UserContactInfoModel *)userContactInfo{
    _userContactInfo = [UserContactInfoModel new];
    _userContactInfo = userContactInfo;
    
}

-(void) setUserPersonalInfo:(UserPersonalInfoModel *)userPersonalInfo{
    _userPersonalInfo = [UserPersonalInfoModel new];
    _userPersonalInfo = userPersonalInfo;
}


-(void) clearData{
    _userObject = nil;
    _userContactInfo = nil;
    _userPersonalInfo = nil;
}

@end
