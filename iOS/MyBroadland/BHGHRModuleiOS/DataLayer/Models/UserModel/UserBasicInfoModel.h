//
//  UserBasicInfoModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
@interface UserBasicInfoModel : NSObject
@property (copy, nonatomic) NSString * employeeFullName;
@property (nonatomic) int isActive;
@property (nonatomic) int userId;
@property (copy, nonatomic) NSString * userName;
@property (nonatomic) int teamId;
@property (copy,nonatomic) NSString *jobRole;
@property (copy,nonatomic) NSString *gender;
@property (copy,nonatomic) NSDate *dob;
@property (copy,nonatomic) NSString *dateOfBirthString;
@property (copy,nonatomic) NSString *age;
@property (copy,nonatomic) NSString *employmentNature;
@property (copy,nonatomic) NSDate *leaveYearStart;
@property (copy,nonatomic) NSString *leaveYearStartString;
@property (copy,nonatomic) NSDate *leaveYearEnd;
@property (copy,nonatomic) NSString *leaveYearEndString;
@property (copy,nonatomic) NSDate *alStartDate;
@property (copy,nonatomic) NSString *alStartDateString;
@property (copy, nonatomic) NSString *selectedYearRange;

-(void) initWithUserObject:(UserObject *)object;
@end
