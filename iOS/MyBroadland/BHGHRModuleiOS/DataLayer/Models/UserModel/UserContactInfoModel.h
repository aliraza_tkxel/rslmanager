//
//  UserContactInfoModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactDetailResponseObject.h"
@interface UserContactInfoModel : NSObject
@property (copy,nonatomic) NSString * workPhone;
@property (copy,nonatomic) NSString * workMobile;
@property (copy,nonatomic) NSString * workEmail;

@property (copy,nonatomic) NSString * mobile;
@property (copy,nonatomic) NSString * homePhone;
@property (copy,nonatomic) NSString * homeEmail;
@property (copy, nonatomic) NSString * mobilePersonal;

@property (copy,nonatomic) NSString * addressLine1;
@property (copy,nonatomic) NSString * addressLine2;
@property (copy,nonatomic) NSString * addressLine3;

@property (copy,nonatomic) NSString * postalTown;
@property (copy,nonatomic) NSString * county;
@property (copy,nonatomic) NSString *postalCode;
@property (copy,nonatomic) NSString * emergencyContactNumber;
@property (copy,nonatomic) NSString * emergencyContactPerson;
@property (copy,nonatomic) NSString * emergencyContactRelationship;

-(void) initWithContactDetails:(ContactDetailResponseObject *) respObj;
@end
