//
//  UserModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserBasicInfoModel.h"
#import "FetchUserProfileResponseObject.h"
#import "UserContactInfoModel.h"
#import "UserPersonalInfoModel.h"
#import "UpdateUserProfileResponseObject.h"

@interface UserModel : NSObject
@property (copy,nonatomic) UserBasicInfoModel *userObject;
@property (copy,nonatomic) UserContactInfoModel *userContactInfo;
@property (copy,nonatomic) UserPersonalInfoModel *userPersonalInfo;
+ (id)sharedInstance;
-(void) setUserObjectInfo:(UserObject *)userObj;
-(void) setUserPersonalInfo:(UserPersonalInfoModel *)userPersonalInfo;
-(void) setUserContactInfo:(UserContactInfoModel *)userContactInfo;
-(void) setUserProfile:(id)responseObejct;
-(void) clearData;
@end
