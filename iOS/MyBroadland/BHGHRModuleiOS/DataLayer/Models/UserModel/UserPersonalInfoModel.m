//
//  UserPersonalInfoModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserPersonalInfoModel.h"
#import "MaritalStatus+CoreDataClass.h"

@implementation UserPersonalInfoModel

-(void) initWithPersonalDetails:(PersonalDetailResponseObject *)respObj{
    self.firstName = (!isEmpty(respObj.firstName)) ? respObj.firstName : @"";
    self.lastName = (!isEmpty(respObj.lastName)) ? respObj.lastName : @"";
    
    self.dateOfBirth = respObj.dateOfBirth;
    self.dateOfBirthString = (!isEmpty(respObj.dateOfBirth)) ? [DateUtilities
                                                                getDateStringFromDate:respObj.dateOfBirth
                                                                withFormat:kDateFormatDefaultStamp] : kValueNotApplicable;
    self.age = (!isEmpty(self.dateOfBirth))? [DateUtilities calculateAgeForPersonBornOn:respObj.dateOfBirth] : kValueNotApplicable;
    
    self.gender = (!isEmpty(respObj.gender)) ? respObj.gender : kValueNotApplicable;
    
    self.maritalStatusId = respObj.maritalStatusId;
    self.maritalStatusDescription = (!isEmpty(respObj.maritalStatusId)) ? [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:respObj.maritalStatusId forLookUpType:LookUpTypeMaritalStatus] : kValueNotApplicable;
    
    self.ethnicityId = respObj.ethnicityId;
    self.ethnicityDescription = (!isEmpty(respObj.ethnicityId)) ? [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:respObj.ethnicityId forLookUpType:LookUpTypeEthnicity] : kValueNotApplicable;
    
    self.religionId = respObj.religionId;
    self.religionDescription = (!isEmpty(respObj.religionId)) ? [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:respObj.religionId forLookUpType:LookUpTypeReligion] : kValueNotApplicable;
    
    self.sexualOrientationId = respObj.sexualOrientationId;
    self.sexualOrientationDescription = (!isEmpty(respObj.sexualOrientationId)) ? [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:respObj.sexualOrientationId forLookUpType:LookUpTypeSexualOrientation] : kValueNotApplicable;
    
}



@end
