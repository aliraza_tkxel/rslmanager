//
//  AbsenceLeaveRecordModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsenceLeaveRecord.h"
@interface AbsenceLeaveRecordModel : NSObject

@property (nonatomic) double duration;
@property (copy,nonatomic) NSString * unit;
@property (copy,nonatomic) NSString *durationDescription;

@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSString * startDateDescription;

@property (copy, nonatomic) NSDate * endDate;
@property (copy, nonatomic) NSString * endDateDescription;

@property (copy, nonatomic) NSString * leaveNature;

@property (copy, nonatomic) NSString * leaveStatus;

@property (copy, nonatomic) NSString * groupKey;

@property (copy,nonatomic) NSNumber *absenceHistoryId;

-(void) initWithAbsenceRecord:(AbsenceLeaveRecord *) obj;

@end
