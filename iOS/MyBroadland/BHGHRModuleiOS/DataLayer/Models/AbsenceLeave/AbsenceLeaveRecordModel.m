//
//  AbsenceLeaveRecordModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/12/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbsenceLeaveRecordModel.h"

@implementation AbsenceLeaveRecordModel
-(void) initWithAbsenceRecord:(AbsenceLeaveRecord *)obj{
    _duration = (!isEmpty(obj.absentDays))?[obj.absentDays doubleValue]:0.00;;
    _unit = (isEmpty(obj.unit))?@"day(s)":obj.unit;
    //HOTFIX: Should be done on API instead
    if(_duration<2){
        _unit = ([[_unit lowercaseString] containsString:@"day"])?@"day(s)":@"hour(s)";
    }
    //END HOTFIX
    _durationDescription = [NSString stringWithFormat:@"%.2f %@",_duration,_unit];
    
    _startDate = obj.startDate;
    _startDateDescription = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                                       getDateStringFromDate:_startDate
                                                                       withFormat:kDateFormatDefaultStamp];
    
    _endDate = obj.endDate;
    _endDateDescription = (isEmpty(_endDate))?kValueNotApplicable:[DateUtilities
                                                                   getDateStringFromDate:_endDate withFormat:kDateFormatDefaultStamp];
    
    _leaveNature = (isEmpty(obj.nature))?kValueNotApplicable:obj.nature;
    
    _leaveStatus = (isEmpty(obj.status))?kValueNotApplicable:obj.status;
    
    _absenceHistoryId = obj.absenceHistoryId;
    
    _groupKey = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                           getDateStringFromDate:_startDate
                                                           withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
}
@end
