//
//  GenericLeaveModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenericLeaveModel : NSObject
@property (nonatomic) int employeeId;
@property (copy,nonatomic) NSDate* startDate;
@property (copy,nonatomic) NSDate* endDate;
@property (copy,nonatomic) NSDate* anticipatedReturnDate;/*For Sickness*/
@property NSNumber *selectedStaffMemberId;/*For Sickness*/
@property (nonatomic) double duration;
@property (copy,nonatomic) NSString* notes;
@property (nonatomic) int reasonId;
@property (copy,nonatomic) NSString *reasonDescription;
@property (nonatomic) LeaveDuration leaveDurationStart;
@property (nonatomic) LeaveDuration leaveDurationEnd;
@property (nonatomic) LeaveAtomicity leaveAtomicity;
-(NSString *) getHolidayType;
@end
