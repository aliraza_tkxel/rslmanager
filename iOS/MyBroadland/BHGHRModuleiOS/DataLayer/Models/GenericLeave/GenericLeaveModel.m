//
//  GenericLeaveModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/6/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "GenericLeaveModel.h"

@implementation GenericLeaveModel
/*
 
 Valid combinations for single and Multiple days leave
 M : morning - 08:00 AM - 12:00 PM
 A : mean after noon - 01:00 PM - 05:00 PM
 F : Single full day
 F-F : Multiple full days
 F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
 A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days
 
 */
-(NSString *) getHolidayType{
    if(_leaveAtomicity == LeaveAtomicitySingle){
        if(_leaveDurationStart == LeaveDurationFullDay){
            return @"F";
        }
        else if(_leaveDurationStart == LeaveDurationMorning){
            return @"M";
        }
        else if(_leaveDurationStart == LeaveDurationEvening){
            return @"A";
        }
    }
    else{
        NSString *firstStr = @"";
        NSString *secondStr = @"";
        
        if(_leaveDurationStart == LeaveDurationFullDay){
            firstStr = @"F";
        }
        else if(_leaveDurationStart == LeaveDurationMorning){
            firstStr = @"M";
        }
        else if(_leaveDurationStart == LeaveDurationEvening){
            firstStr = @"A";
        }
        
        if(_leaveDurationEnd == LeaveDurationFullDay){
            secondStr = @"F";
        }
        else if(_leaveDurationEnd == LeaveDurationMorning){
            secondStr = @"M";
        }
        else if(_leaveDurationEnd == LeaveDurationEvening){
            secondStr = @"A";
        }
        return [NSString stringWithFormat:@"%@-%@",firstStr,secondStr];
    }
    return @"";
}

@end
