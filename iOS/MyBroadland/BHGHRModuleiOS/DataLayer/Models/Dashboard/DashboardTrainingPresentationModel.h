//
//  DashboardTrainingPresentationModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainDashboardTrainingModel.h"
@interface DashboardTrainingPresentationModel : NSObject
@property (copy,nonatomic) NSString *trainingName;

@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSString * startDateDescription;

-(void) initWithTrainingRecord:(MainDashboardTrainingModel *) obj;
@end
