//
//  DashboardTrainingPresentationModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/11/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DashboardTrainingPresentationModel.h"

@implementation DashboardTrainingPresentationModel

-(void) initWithTrainingRecord:(MainDashboardTrainingModel *)obj{
    _trainingName = (!isEmpty(obj.course))?obj.course:kValueNotApplicable;
    _startDate = obj.startDate;
    if(!isEmpty(obj.startDate)){
        _startDateDescription = (!isEmpty(obj.startDate))?[DateUtilities getDateStringFromDate:obj.startDate
                                                                             withFormat:kDateFormatDefaultStamp]:kValueNotApplicable;
    }
}

@end
