//
//  DaysTakenModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DaysTakenModel.h"
@implementation DaysTakenModel

-(void) initWithResponseObject:(LeaveTakenNetworkObject *) obj{
    _startDate = obj.startDate;
    _startDateDescription = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                                       getDateStringFromDate:_startDate withFormat:kDateFormatDefaultStamp];
    
    _endDate = obj.endDate;
    _endDateDescription = (isEmpty(_endDate))?kValueNotApplicable:[DateUtilities
                                                                   getDateStringFromDate:_endDate withFormat:kDateFormatDefaultStamp];
    
    _duration = (!isEmpty(obj.duration))?[obj.duration doubleValue]:0;
    _unit = (isEmpty(obj.unit))?@"day(s)":obj.unit;
    //HOTFIX: Should be done on API instead
    if(_duration<2){
        _unit = ([[_unit lowercaseString] containsString:@"day"])?@"day(s)":@"hour(s)";
    }
    //END HOTFIX
    _durationDescription = [NSString stringWithFormat:@"%.2f %@",_duration,_unit];
    
    _natureId = obj.natureId;
    _natureDescription = [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:_natureId
                                                                          forLookUpType:LookUpTypeLeaveNature];
    _leaveDescription = obj.leaveDescription;
    _status = obj.status;
    _groupKey = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                           getDateStringFromDate:_startDate
                                                           withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
    
    _applicantName = obj.applicantName;
    _statusId = obj.statusId;
    _applicantId = obj.applicantId;
    _absenceHistoryId = obj.absenceHistoryId;
}

-(void) initWithAbsenceResponseObject:(AbsenceLeaveRecord *) obj{
    _startDate = obj.startDate;
    _startDateDescription = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                                       getDateStringFromDate:_startDate withFormat:kDateFormatDefaultStamp];
    
    _endDate = obj.endDate;
    _endDateDescription = (isEmpty(_endDate))?kValueNotApplicable:[DateUtilities
                                                                   getDateStringFromDate:_endDate withFormat:kDateFormatDefaultStamp];
    
    _duration = (!isEmpty(obj.absentDays))?[obj.absentDays doubleValue]:0.00;
    _unit = (isEmpty(obj.unit))?@"day(s)":obj.unit;
    //HOTFIX: Should be done on API instead
    if(_duration<2){
        _unit = ([[_unit lowercaseString] containsString:@"day"])?@"day(s)":@"hour(s)";
    }
    //END HOTFIX
    _durationDescription = [NSString stringWithFormat:@"%.2f %@",_duration,_unit];
    
    _natureId = obj.natureId;
    _natureDescription = [[LookUpsDataManager sharedInstance] getLookUpDescriptionForId:_natureId
                                                                          forLookUpType:LookUpTypeLeaveNature];
    _leaveDescription = obj.nature;
    _status = obj.status;
    _groupKey = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                           getDateStringFromDate:_startDate
                                                           withFormat:[NSString stringWithFormat:@"%@ %@",kDateFormatComponentMonthNameFull,kDateFormatComponentYear]];
    _absenceHistoryId = obj.absenceHistoryId;
    
    _statusId = obj.statusId;
}


@end
