//
//  DaysTakenModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 6/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnnualLeavesDaysTakenResponseObject.h"
#import "LeaveTakenNetworkObject.h"
#import "AbsenceLeaveRecord.h"
@interface DaysTakenModel : NSObject

@property (copy,nonatomic) NSDate * startDate;
@property (copy,nonatomic) NSString * startDateDescription;

@property (copy,nonatomic) NSDate * endDate;
@property (copy,nonatomic) NSString * endDateDescription;

@property (nonatomic) double  duration;
@property (copy,nonatomic) NSString * unit;
@property (copy,nonatomic) NSString *durationDescription;


@property (copy,nonatomic) NSNumber * natureId;
@property (copy,nonatomic) NSString *natureDescription;
@property (copy,nonatomic) NSString * leaveDescription;

@property (copy,nonatomic) NSString *status;
@property (copy, nonatomic) NSString * groupKey;

@property (copy,nonatomic) NSNumber *statusId;
@property (copy,nonatomic) NSNumber *absenceHistoryId;
@property (copy,nonatomic) NSNumber *applicantId;
@property (copy,nonatomic) NSString * applicantName;
-(void) initWithResponseObject:(LeaveTakenNetworkObject *) obj;
-(void) initWithAbsenceResponseObject:(AbsenceLeaveRecord *) obj;
@end
