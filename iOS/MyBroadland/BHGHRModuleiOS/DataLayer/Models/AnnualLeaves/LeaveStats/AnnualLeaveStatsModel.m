//
//  AnnualLeaveStatsModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AnnualLeaveStatsModel.h"

@implementation AnnualLeaveStatsModel

-(void) initWithLeaveStatsResponse:(FetchAnnualLeaveStatsResponse *) response{
    
    self.totalAllowanceDesc = (!isEmpty(response.totalAllowance))?[NSString stringWithFormat:@"%.2f %@",[response.totalAllowance doubleValue],response.unit]:[NSString stringWithFormat:@"N/A %@",response.unit];
    self.totalAllowance = [conditionallyAssign(response.totalAllowance, @0.00) doubleValue];
    
    
    self.leavesBookedDesc = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.leavesBooked, @0.00) doubleValue]+[conditionallyAssign(response.leavesTaken, @0.00) doubleValue],response.unit];
    self.leavesBooked = [conditionallyAssign(response.leavesBooked, @0.00) doubleValue];
    
    
    self.leavesRequestedDesc = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.leavesRequested, @0.00) doubleValue],response.unit];
    self.leavesRequested = [conditionallyAssign(response.leavesRequested, @0.00) doubleValue];
    
    
    self.leavesRemainingDesc = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.leavesRemaining, @0.00) doubleValue],response.unit];
    self.leavesRemaining = [conditionallyAssign(response.leavesRemaining, @0.00) doubleValue];
    
    _holidayEntitlementHours =[NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.holidayEntitlementHours, @0.00) doubleValue],response.unit];
    
    
    self.startDate = response.startDate;
    self.startDateString = [DateUtilities getDateStringFromDate:response.startDate withFormat:kDateFormatDefaultStamp];
    
    self.bankHolidays = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.bankHoliday, @0.00) doubleValue],response.unit];
    self.entitlementDays = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.holidayEntitlementDays, @0.00) doubleValue],response.unit];
    self.inLieu = [NSString stringWithFormat:@"%.2f Hrs.",[conditionallyAssign(response.timeOffInLieuOwed, @0.00) doubleValue]];
    self.carryForward = [NSString stringWithFormat:@"%.2f %@",[conditionallyAssign(response.carryForward, @0.00) doubleValue],response.unit];
    
    self.leavesUnit = response.unit;
}

@end
