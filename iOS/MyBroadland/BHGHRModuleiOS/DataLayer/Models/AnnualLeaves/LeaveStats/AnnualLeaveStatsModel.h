//
//  AnnualLeaveStatsModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/29/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FetchAnnualLeaveStatsResponse.h"
@interface AnnualLeaveStatsModel : NSObject
@property (nonatomic) double totalAllowance ;
@property (nonatomic) double leavesBooked ;
@property (nonatomic) double leavesRequested ;
@property (nonatomic) double leavesRemaining ;
@property (copy,nonatomic) NSString *holidayEntitlementHours;
@property (copy,nonatomic) NSString * totalAllowanceDesc ;
@property (copy,nonatomic) NSString * leavesBookedDesc ;
@property (copy,nonatomic) NSString * leavesRequestedDesc ;
@property (copy,nonatomic) NSString * leavesRemainingDesc ;
@property (copy, nonatomic) NSDate* startDate ;
@property (copy,nonatomic) NSString *startDateString;
@property (copy, nonatomic) NSString * leavesUnit ;
@property (copy, nonatomic) NSString * entitlementDays;
@property (copy, nonatomic) NSString * inLieu;
@property (copy, nonatomic) NSString * bankHolidays;
@property (copy, nonatomic) NSString * carryForward;
-(void) initWithLeaveStatsResponse:(FetchAnnualLeaveStatsResponse *) response;
@end
