//
//  SicknessLeaveRecordModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 7/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SicknessLeaveRecord.h"
@interface SicknessLeaveRecordModel : NSObject

@property (nonatomic) double duration;
@property (copy,nonatomic) NSString * unit;
@property (copy,nonatomic) NSString *durationDescription;

@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSString * startDateDescription;

@property (copy, nonatomic) NSDate * anticipatedReturnDate;
@property (copy, nonatomic) NSString * anticipatedReturnDateDescription;

@property (copy, nonatomic) NSDate * endDate;
@property (copy, nonatomic) NSString * endDateDescription;

@property (copy, nonatomic) NSString * leaveNature;
@property (copy,nonatomic) NSNumber *natureId;

@property (copy, nonatomic) NSString * leaveReason;
@property (copy,nonatomic) NSNumber *reasonId;

@property (copy, nonatomic) NSString * leaveStatus;
@property (copy, nonatomic) NSNumber * statusId;

@property (copy, nonatomic) NSString * groupKey;

@property (copy, nonatomic) NSNumber *absenceHistoryId;
@property (copy,nonatomic) NSString *notes;

@property BOOL isOnWork;
-(void) initWithSicknessRecord:(SicknessLeaveRecord *) obj;
@end
