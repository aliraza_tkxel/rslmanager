//
//  StaffMemberSicknessSummaryModel.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StaffMemberSicknessSummaryResponse.h"
@interface StaffMemberSicknessSummaryModel : NSObject
@property (copy, nonatomic) NSDate * startDate;
@property (copy, nonatomic) NSString * startDateDescription;
@property (copy, nonatomic) NSString * groupKey;
@property (copy, nonatomic) NSString * firstName;
@property (copy,nonatomic) NSString * lastName;
@property (copy,nonatomic) NSString * fullName;
@property (copy,nonatomic) NSNumber * lineManagerId;
@property (copy,nonatomic) NSNumber * staffMemberId;

@property (copy, nonatomic) NSDate * anticipatedReturnDate;
@property (copy, nonatomic) NSString * anticipatedReturnDateDescription;

@property (copy, nonatomic) NSDate * endDate;
@property (copy, nonatomic) NSString * endDateDescription;

@property (copy, nonatomic) NSString * leaveNature;
@property (copy,nonatomic) NSNumber * natureId;

@property (copy, nonatomic) NSString * leaveReason;
@property (copy,nonatomic) NSNumber * reasonId;

@property (copy, nonatomic) NSString * leaveStatus;
@property (copy,nonatomic) NSNumber * statusId;

@property BOOL isOnWork;
-(void) initWithResponseObj:(StaffMemberSicknessSummaryResponse *) obj;
@end
