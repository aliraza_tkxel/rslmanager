//
//  StaffMemberSicknessSummaryModel.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 12/7/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "StaffMemberSicknessSummaryModel.h"

@implementation StaffMemberSicknessSummaryModel

-(void) initWithResponseObj:(StaffMemberSicknessSummaryResponse *) obj{
    
    _startDate = obj.currentleaveStartDate;
    _startDateDescription = (isEmpty(_startDate))?kValueNotApplicable:[DateUtilities
                                                                       getDateStringFromDate:_startDate
                                                                       withFormat:kDateFormatDefaultStamp];
    
    _anticipatedReturnDate = obj.anticipatedReturnDate;
    _anticipatedReturnDateDescription = (isEmpty(_anticipatedReturnDate))?kValueNotApplicable:[DateUtilities
                                                                                               getDateStringFromDate:_anticipatedReturnDate
                                                                                               withFormat:kDateFormatDefaultStamp];
    
    
    _endDate = obj.endDate;
    _endDateDescription = (isEmpty(_endDate))?kValueNotApplicable:[DateUtilities
                                                                   getDateStringFromDate:_endDate withFormat:kDateFormatDefaultStamp];
    
    
    
    
    _firstName = (isEmpty(obj.staffMemberFName))?@"":obj.staffMemberFName;
    
    _lastName = (isEmpty(obj.staffMemberLName))?@"":obj.staffMemberLName;
    
    _fullName = [NSString stringWithFormat:@"%@ %@", obj.staffMemberFName, obj.staffMemberLName];
    
    
    _leaveNature = (isEmpty(obj.nature))?kValueNotApplicable:obj.nature;
    _natureId = obj.natureId;
    
    _leaveReason = (isEmpty(obj.reason))?kValueNotApplicable:obj.reason;
    _reasonId = obj.reasonId;
    
    _leaveStatus = (isEmpty(obj.status))?kValueNotApplicable:obj.status;
    _statusId = obj.statusId;
    
    //TODO:Hard Coding Absent Status Id because of some mapping issue which we have no time to look into right now. Will be fixed in future releases*/
    
    NSNumber *absentId = [NSNumber numberWithInt:1]; /*[[LookUpsDataManager sharedInstance] getLookUpIdForDescription:kLeaveStatusSicknessAbsent
                                                      forLookUpType:LookUpTypeLeaveActions];*/
    if(!isEmpty(_statusId)){
        _isOnWork = ([absentId isEqualToNumber:_statusId])?NO:YES;
    }
    else{
        _isOnWork = YES;
    }
    
    _lineManagerId = obj.lineManagerId;
    _staffMemberId = obj.staffMemberId;
    
    _groupKey = (_isOnWork)?kStaffSicknessInWork:kStaffSicknessOnLeave;
    
    
    
}

@end
