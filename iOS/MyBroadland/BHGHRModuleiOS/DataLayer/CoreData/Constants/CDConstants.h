//
//  CDConstants.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/16/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#ifndef CDConstants_h
#define CDConstants_h


#define COREDATA_MODEL_NAME @"BHGHRModuleiOS"

/*Entities Names*/

#define kEntityLeaveReasons                   @"LeaveReasons"
#define kEntityGender                         @"Gender"
#define kEntityReligion                       @"Religion"
#define kEntityUser                           @"User"
#define kEntitySexualOrientation              @"SexualOrientation"
#define kEntityMaritalStatus                  @"MaritalStatus"
#define kEntityLeaveNature                    @"LeaveNature"
#define kEntityEthnicity                      @"Ethnicity"
#define kEntityLeaveActions                   @"LeaveAction"
#define kEntityWorkingPattern                 @"WorkingPattern"

/*User Entity*/

#define kEntityUserVarDob                       @"dob"
#define kEntityUserVarEmployeeFullName          @"employeeFullName"
#define kEntityUserVarGender                    @"gender"
#define kEntityUserVarIsActive                  @"isActive"
#define kEntityUserVarJobRole                   @"jobRole"
#define kEntityUserVarTeamId                    @"teamId"
#define kEntityUserVarUserId                    @"userId"
#define kEntityUserVarUserName                  @"userName"

/*WorkingPattern Entity*/

#define kEntityWorkingPatternDaySun            @"sun"
#define kEntityWorkingPatternDayMon            @"mon"
#define kEntityWorkingPatternDayTue            @"tue"
#define kEntityWorkingPatternDayWed            @"wed"
#define kEntityWorkingPatternDayThu            @"thu"
#define kEntityWorkingPatternDayFri            @"fri"
#define kEntityWorkingPatternDaySat            @"sat"
#define kEntityWorkingPatternWeekNumber        @"weekNumber"
#define kEntityWorkingPatternEmployeeId        @"employeeId"
/*Leave Actions*/

#define kEntityLeaveActionsVarActionApprove     @"Approve"
#define kEntityLeaveActionsVarActionDecline     @"Decline"
#define kEntityLeaveActionsVarActionCancel      @"Cancel"

/*Lookups Entities*/

#define kEntityLookUpVarLookUpDescription       @"lookUpDescription"
#define kEntityLookUpVarLookUpId                @"lookUpId"


#endif /* CDConstants_h */
