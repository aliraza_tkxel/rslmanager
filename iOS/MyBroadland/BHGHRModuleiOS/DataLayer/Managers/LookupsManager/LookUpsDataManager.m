//
//  LookUpsDataManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "LookUpsDataManager.h"

@implementation LookUpsDataManager

#pragma mark - Init 

+ (id)sharedInstance
{
    static LookUpsDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

#pragma mark insertion

-(void) insertReligions:(NSArray *) religion{
    STARTEXCEPTION
    if(isEmpty(religion)){
        return;
    }
    for(LookUpModel *model in religion){
        Religion *religionEntity = [Religion MR_createEntity];
        religionEntity.lookUpId = model.lookUpId;
        religionEntity.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertGenders:(NSArray *) genders{
    STARTEXCEPTION
    if(isEmpty(genders)){
        return;
    }
    for(LookUpModel *model in genders){
        Gender *genderEntity = [Gender MR_createEntity];
        genderEntity.lookUpId = model.lookUpId;
        genderEntity.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertSexualOrientations:(NSArray *) sexOrientations{
    STARTEXCEPTION
    if(isEmpty(sexOrientations)){
        return;
    }
    for(LookUpModel *model in sexOrientations){
        SexualOrientation *sexOrEnt = [SexualOrientation MR_createEntity];
        sexOrEnt.lookUpId = model.lookUpId;
        sexOrEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertEthnicities:(NSArray *) ethnicities{
    STARTEXCEPTION
    if(isEmpty(ethnicities)){
        return;
    }
    for(LookUpModel *model in ethnicities){
        Ethnicity *ethniEnt = [Ethnicity MR_createEntity];
        ethniEnt.lookUpId = model.lookUpId;
        ethniEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertMaritalStatuses:(NSArray *) maritStats{
    STARTEXCEPTION
    if(isEmpty(maritStats)){
        return;
    }
    for(LookUpModel *model in maritStats){
        MaritalStatus *marriStatus = [MaritalStatus MR_createEntity];
        marriStatus.lookUpId = model.lookUpId;
        marriStatus.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertLeaveNatures:(NSArray *) leaveNats{
    STARTEXCEPTION
    if(isEmpty(leaveNats)){
        return;
    }
    for(LookUpModel *model in leaveNats){
        LeaveNature *leaveEnt = [LeaveNature MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertLeaveActions:(NSArray *) leaveActions{
    STARTEXCEPTION
    if(isEmpty(leaveActions)){
        return;
    }
    for(LookUpModel *model in leaveActions){
        LeaveAction *leaveEnt = [LeaveAction MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertMyStaff:(NSArray *) myStaff{
    STARTEXCEPTION
    if(isEmpty(myStaff)){
        return;
    }
    for(LookUpModel *model in myStaff){
        MyStaff *leaveEnt = [MyStaff MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertLeaveReasons:(NSArray *) leaveReasons{
    STARTEXCEPTION
    if(isEmpty(leaveReasons)){
        return;
    }
    for(LookUpModel *model in leaveReasons){
        LeaveReasons *leaveEnt = [LeaveReasons MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertSickLeaveReasons:(NSArray *) sickLeaveReasons{
    STARTEXCEPTION
    if(isEmpty(sickLeaveReasons)){
        return;
    }
    [SickLeaveReasons MR_truncateAll];
    for(LookUpModel *model in sickLeaveReasons){
        SickLeaveReasons *leaveEnt = [SickLeaveReasons MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertBankHolidays:(NSArray *) bankHolidays{
    STARTEXCEPTION
    if(isEmpty(bankHolidays)){
        return;
    }
    [BankHolidays MR_truncateAll];
    int i =0;
    for(NSString *model in bankHolidays){
        BankHolidays *leaveEnt = [BankHolidays MR_createEntity];
        leaveEnt.lookUpId = i; /*Assigning auto lookup id as Holidays don't have it*/
        leaveEnt.lookUpDescription = model;
        i=i+1;
    }
    [self saveContext];
    ENDEXCEPTION
}

-(void) insertAbsenceLeaveReasons:(NSArray *)absenceLeaveReasons{
    STARTEXCEPTION
    if(isEmpty(absenceLeaveReasons)){
        return;
    }
    [AbsenceLeaveNature MR_truncateAll];
    for(LookUpModel *model in absenceLeaveReasons){
        AbsenceLeaveNature *leaveEnt = [AbsenceLeaveNature MR_createEntity];
        leaveEnt.lookUpId = model.lookUpId;
        leaveEnt.lookUpDescription = model.lookUpDescription;
    }
    [self saveContext];
    ENDEXCEPTION
}


#pragma mark - Fetching

-(NSArray *) fetchAllReligions{
    NSArray *sortedRecords = [Religion MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllGenders{
    NSArray *sortedRecords = [Gender MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllEthnicities{
    NSArray *sortedRecords = [Ethnicity MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllMaritalStatuses{
    NSArray *sortedRecords = [MaritalStatus MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllSexualOrientations{
    NSArray *sortedRecords = [SexualOrientation MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllLeaveReasons{
    NSArray *sortedRecords = [LeaveReasons MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllLeaveNatures{
    NSArray *sortedRecords = [LeaveNature MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllSicknessLeaveReasons{
    NSArray *sortedRecords = [SickLeaveReasons MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllBankHolidays{
    NSArray *sortedRecords = [BankHolidays MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllAbsenceLeaveReasons{
    NSArray *sortedRecords = [AbsenceLeaveNature MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray *) fetchAllLeaveActions{
    NSArray *sortedRecords = [LeaveAction MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

-(NSArray*) fetchAllStaffMembers{
    NSArray *sortedRecords = [MyStaff MR_findAllSortedBy:kEntityLookUpVarLookUpDescription                                             ascending:YES];
    return sortedRecords;
}

#pragma mark - Searching

-(NSString *)getLookUpDescriptionForId:(NSNumber *)lookUpId forLookUpType:(LookUpType)type{
    NSArray *entities = [self getSourceArrayForLookUpType:type];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ == %i", kEntityLookUpVarLookUpId,[lookUpId intValue]];
    entities = [entities filteredArrayUsingPredicate:filterPredicate];
    if([entities count]>0){
        entities = [entities valueForKeyPath:kEntityLookUpVarLookUpDescription];
        return [entities objectAtIndex:0];
    }
    return kValueNotApplicable;
}

-(NSNumber *)getLookUpIdForDescription:(NSString *)lookUpDescription forLookUpType:(LookUpType)type{
    NSArray *entities = [self getSourceArrayForLookUpType:type];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ == %@", kEntityLookUpVarLookUpDescription,lookUpDescription];
    entities = [entities filteredArrayUsingPredicate:filterPredicate];
    if([entities count]>0){
        entities = [entities valueForKeyPath:kEntityLookUpVarLookUpId];
        NSNumber *lookUpId = (NSNumber *)[entities objectAtIndex:0];
        return lookUpId;
    }
    return [NSNumber numberWithInt:0];
}


-(NSArray *) getSourceArrayForLookUpType:(LookUpType) type{
    NSArray *entities = [[NSArray alloc] init];
    switch (type) {
        case LookUpTypeGender:
            entities = [self fetchAllGenders];
            break;
        case LookUpTypeReligion:
            entities = [self fetchAllReligions];
            break;
        case LookUpTypeEthnicity:
            entities = [self fetchAllEthnicities];
            break;
        case LookUpTypeSexualOrientation:
            entities = [self fetchAllSexualOrientations];
            break;
        case LookUpTypeLeaveNature:
            entities = [self fetchAllLeaveNatures];
            break;
        case LookUpTypeLeaveReason:
            entities = [self fetchAllLeaveReasons];
            break;
        case LookUpTypeMaritalStatus:
            entities = [self fetchAllMaritalStatuses];
            break;
        case LookUpTypeSickLeaveReasons:
            entities = [self fetchAllSicknessLeaveReasons];
            break;
        case LookUpTypeBankHoliday:
            entities = [self fetchAllBankHolidays];
            break;
        case LookUpTypeAbsenceLeaveReasons:
            entities = [self fetchAllAbsenceLeaveReasons];
            break;
        case LookUpTypeLeaveActions:
            entities = [self fetchAllLeaveActions];
            break;
        case LookUpTypeMyStaff:
            entities = [self fetchAllStaffMembers];
            break;
        default:
            break;
    }
   
    return entities;
}


#pragma mark - Delete All

-(void) deleteAllLookUps{
    STARTEXCEPTION
    [Religion MR_truncateAll];
    [Ethnicity MR_truncateAll];
    [Gender MR_truncateAll];
    [MaritalStatus MR_truncateAll];
    [SexualOrientation MR_truncateAll];
    [LeaveReasons MR_truncateAll];
    [LeaveNature MR_truncateAll];
    [SickLeaveReasons MR_truncateAll];
    [BankHolidays MR_truncateAll];
    [AbsenceLeaveNature MR_truncateAll];
    [LeaveAction MR_truncateAll];
    [MyStaff MR_truncateAll];
    ENDEXCEPTION
}


@end
