//
//  LookUpsDataManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDataManager.h"
#import "Gender+CoreDataClass.h"
#import "Religion+CoreDataClass.h"
#import "Ethnicity+CoreDataClass.h"
#import "SexualOrientation+CoreDataClass.h"
#import "MaritalStatus+CoreDataClass.h"
#import "LeaveNature+CoreDataClass.h"
#import "LeaveReasons+CoreDataClass.h"
#import "SickLeaveReasons+CoreDataClass.h"
#import "AbsenceLeaveNature+CoreDataClass.h"
#import "BankHolidays+CoreDataClass.h"
#import "LeaveAction+CoreDataClass.h"
#import "MyStaff+CoreDataClass.h"
#import "CDConstants.h"
#import "LookUpModel.h"
@interface LookUpsDataManager : BaseDataManager

+ (id)sharedInstance;

-(void) insertReligions:(NSArray *) religion;
-(void) insertGenders:(NSArray *) genders;
-(void) insertSexualOrientations:(NSArray *) sexOrientations;
-(void) insertEthnicities:(NSArray *) ethnicities;
-(void) insertMaritalStatuses:(NSArray *) maritStats;
-(void) insertLeaveNatures:(NSArray *) leaveNats;
-(void) insertLeaveReasons:(NSArray *) leaveReasons;
-(void) insertSickLeaveReasons:(NSArray *) sickLeaveReasons;
-(void) insertBankHolidays:(NSArray *) bankHolidays;
-(void) insertAbsenceLeaveReasons:(NSArray *) absenceLeaveReasons;
-(void) insertLeaveActions:(NSArray *) leaveActions;
-(void) insertMyStaff:(NSArray *) myStaff;

-(NSArray *) fetchAllReligions;
-(NSArray *) fetchAllGenders;
-(NSArray *) fetchAllEthnicities;
-(NSArray *) fetchAllMaritalStatuses;
-(NSArray *) fetchAllSexualOrientations;
-(NSArray *) fetchAllLeaveReasons;
-(NSArray *) fetchAllLeaveNatures;
-(NSArray *) fetchAllSicknessLeaveReasons;
-(NSArray *) fetchAllBankHolidays;
-(NSArray *) fetchAllAbsenceLeaveReasons;
-(NSArray *) fetchAllLeaveActions;
-(NSArray *) fetchAllStaffMembers;


-(NSNumber *)getLookUpIdForDescription:(NSString *)lookUpDescription
                         forLookUpType:(LookUpType)type;
-(NSString *)getLookUpDescriptionForId:(NSNumber *)lookUpId
                         forLookUpType:(LookUpType)type;

-(void) deleteAllLookUps;

@end
