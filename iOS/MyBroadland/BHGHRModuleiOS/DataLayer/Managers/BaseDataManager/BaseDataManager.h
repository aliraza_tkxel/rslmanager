//
//  BaseDataManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>
#import "CDConstants.h"

typedef void (^DataOperationBlock) (BOOL success, id response, NSError *error);
@interface BaseDataManager : NSObject
- (void)saveContext;
@end
