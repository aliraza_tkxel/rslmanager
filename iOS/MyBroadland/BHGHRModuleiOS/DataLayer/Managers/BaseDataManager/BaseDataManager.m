//
//  BaseDataManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "BaseDataManager.h"

@implementation BaseDataManager

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)saveContext
{
    // Save ManagedObjectContext using MagicalRecord
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}
@end
