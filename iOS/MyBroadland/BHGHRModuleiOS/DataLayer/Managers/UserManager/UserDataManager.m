//
//  UserDataManager.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UserDataManager.h"

@implementation UserDataManager

#pragma mark - Init

+ (id)sharedInstance
{
    static UserDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

#pragma mark - Data Exploitation

-(void) insertUserObject:(UserObject *) userNetworkObj{
    STARTEXCEPTION
    [self deleteAllUsers];
    User * record = [User MR_createEntity];
    record.employeeFullName = userNetworkObj.employeeFullName;
    record.userName = userNetworkObj.userName;
    record.userId = [conditionallyAssign(userNetworkObj.userId, @0) intValue];
    record.isActive = [conditionallyAssign(userNetworkObj.isActive, @0) intValue];
    record.teamId = [conditionallyAssign(userNetworkObj.teamId, @0) intValue];
    record.jobRole = userNetworkObj.jobRole;
    record.gender = userNetworkObj.gender;
    record.dob = userNetworkObj.dob;
    record.alStartDate = userNetworkObj.alStartDate;
    record.leaveYearStart = userNetworkObj.leaveYearStart;
    record.leaveYearEnd = userNetworkObj.leaveYearEnd;
    [self saveContext];
    ENDEXCEPTION
    
}

-(void) insertWorkingPattern:(NSArray *) workingPattern{
    STARTEXCEPTION
   
    if(!isEmpty(workingPattern)){
        [WorkingPattern MR_truncateAll];
        for(WorkingPatternNetworkObject *wpObj in workingPattern){
            WorkingPattern * wpEnt = [WorkingPattern MR_createEntity];
            wpEnt.sun = [conditionallyAssign(wpObj.sun, @0.00) doubleValue];
            wpEnt.mon = [conditionallyAssign(wpObj.mon, @0.00) doubleValue];
            wpEnt.tue = [conditionallyAssign(wpObj.tue, @0.00) doubleValue];
            wpEnt.wed = [conditionallyAssign(wpObj.wed, @0.00) doubleValue];
            wpEnt.thu = [conditionallyAssign(wpObj.thu, @0.00) doubleValue];
            wpEnt.fri = [conditionallyAssign(wpObj.fri, @0.00) doubleValue];
            wpEnt.sat = [conditionallyAssign(wpObj.sat, @0.00) doubleValue];
            wpEnt.weekNumber = [conditionallyAssign(wpObj.weekNumber, @1) intValue];
            wpEnt.employeeId = [conditionallyAssign(wpObj.employeeId, [NSNumber numberWithInt:LoggedInUserId]) intValue];
            [self saveContext];
        }
        
        
    }
    ENDEXCEPTION
}

-(NSArray *) fetchWorkingPatternForCurrentUser{
    NSArray *sortedRecords = [WorkingPattern MR_findAllSortedBy:kEntityWorkingPatternWeekNumber
                                                      ascending:YES];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ == %i", kEntityWorkingPatternEmployeeId,LoggedInUserId];
    NSArray*entities = [sortedRecords filteredArrayUsingPredicate:filterPredicate];
    NSArray *workingPattern =(!isEmpty(entities))?entities:[NSArray new];
    return workingPattern;
}

-(void) updateUserObject:(UserObject *) userObj{
    STARTEXCEPTION
    User *record = [User MR_findFirstByAttribute:kEntityUserVarUserName
                                       withValue:userObj.userName];
    if(!isEmpty(record)){
        record.employeeFullName = userObj.employeeFullName;
        record.userName = userObj.userName;
        record.userId = [conditionallyAssign(userObj.userId, @0) intValue];
        record.isActive = [conditionallyAssign(userObj.isActive, @0) intValue];
        record.teamId = [conditionallyAssign(userObj.teamId, @0) intValue];;
        record.jobRole = userObj.jobRole;
        record.gender = userObj.gender;
        record.dob = userObj.dob;
        [self saveContext];
    }
    
    ENDEXCEPTION
}

-(void) deleteAllUsers{
    STARTEXCEPTION
    [User MR_truncateAll];
    [WorkingPattern MR_truncateAll];
    ENDEXCEPTION
}




@end
