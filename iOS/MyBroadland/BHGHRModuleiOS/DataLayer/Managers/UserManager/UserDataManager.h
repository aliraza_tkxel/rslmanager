//
//  UserDataManager.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDataManager.h"
#import "User+CoreDataClass.h"
#import "WorkingPattern+CoreDataClass.h"
@interface UserDataManager : BaseDataManager

+ (id)sharedInstance;
-(NSArray *) fetchWorkingPatternForCurrentUser;
-(void) insertWorkingPattern:(NSArray *) workingPattern;
-(void) insertUserObject:(UserObject *) userNetworkObj;
-(void) updateUserObject:(UserObject *) userObj;
-(void) deleteAllUsers;
@end
