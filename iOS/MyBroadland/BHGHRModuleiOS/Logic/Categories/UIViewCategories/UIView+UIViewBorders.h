//
//  UIView+UIViewBorders.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/22/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewBorders)
-(void) addCompleteBorderWithColor:(UIColor *) color
                          andWidth:(CGFloat) borderWidth;
- (void)addTopBorderWithColor:(UIColor *)color
                     andWidth:(CGFloat) borderWidth;
- (void)addBottomBorderWithColor:(UIColor *)color
                        andWidth:(CGFloat) borderWidth;
- (void)addLeftBorderWithColor:(UIColor *)color
                      andWidth:(CGFloat) borderWidth;
- (void)addRightBorderWithColor:(UIColor *)color
                       andWidth:(CGFloat) borderWidth;
@end
