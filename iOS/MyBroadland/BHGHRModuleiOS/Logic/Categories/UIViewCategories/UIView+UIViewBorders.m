//
//  UIView+UIViewBorders.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/22/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "UIView+UIViewBorders.h"

@implementation UIView (UIViewBorders)
-(void) addCompleteBorderWithColor:(UIColor *) color
                          andWidth:(CGFloat) borderWidth{
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = borderWidth;
}
- (void)addTopBorderWithColor:(UIColor *)color
                     andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin];
    border.frame = CGRectMake(0, 0, self.frame.size.width, borderWidth);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    [self addSubview:border];
}

- (void)addBottomBorderWithColor:(UIColor *)color
                        andWidth:(CGFloat) borderWidth{
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, borderWidth);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    [self addSubview:border];
}

- (void)addLeftBorderWithColor:(UIColor *)color
                      andWidth:(CGFloat) borderWidth{
    UIView *border = [UIView new];
    border.backgroundColor = color;
    border.frame = CGRectMake(0, 0, borderWidth, self.frame.size.height);
    [border setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin];
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    [self addSubview:border];
}

- (void)addRightBorderWithColor:(UIColor *)color
                       andWidth:(CGFloat) borderWidth{
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin];
    border.frame = CGRectMake(self.frame.size.width - borderWidth, 0, borderWidth, self.frame.size.height);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    [self addSubview:border];
}
@end
