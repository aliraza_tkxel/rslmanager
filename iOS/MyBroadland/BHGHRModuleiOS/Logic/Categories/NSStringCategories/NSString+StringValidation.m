//
//  NSString+StringValidation.m
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "NSString+StringValidation.h"
@implementation NSString (StringValidation)
#pragma mark - Validation
- (BOOL)isValidEmail
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"%@ MATCHES %@", self,emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL) isValidPostalCode{
    NSString *regexFilter = @"^([A-PR-UWYZa-pr-uwyz](([0-9](([0-9]|[A-HJKSTUWa-hjkstuw])?)?)|([A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRVWXYabehmnprvwxy])?)) [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2})";
    NSPredicate *postCodeTest = [NSPredicate predicateWithFormat:@"%@ MATCHES %@", self,regexFilter];
    return [postCodeTest evaluateWithObject:self];
}

- (BOOL)isValidPhone
{
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSRange inputRange = NSMakeRange(0, [self length]);
    NSArray *matches = [detector matchesInString:self options:0 range:inputRange];
    
    if ([matches count] == 0) {
        return NO;
    }
    
    NSTextCheckingResult *result = (NSTextCheckingResult *)[matches objectAtIndex:0];
    
    if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
        
        return YES;
    }
    else {
        
        return NO;
    }
}

- (BOOL) isValidName{
    if(![self isEmptyOrWhiteSpaceString]){
        NSMutableCharacterSet *validationCharSet = [NSMutableCharacterSet characterSetWithCharactersInString:@" ÄäÀàÁáÂâÃãÅåǍǎĄąĂăÆæĀāÇçĆćĈĉČčĎđĐďðÈèÉéÊêËëĚěĘęĖėĒēĜĝĢģĞğĤĥÌìÍíÎîÏïıĪīĮįĴĵĶķĹĺĻļŁłĽľÑñŃńŇňŅņÖöÒòÓóÔôÕõŐőØøŒœŔŕŘřẞßŚśŜŝŞşŠšȘșŤťŢţÞþȚțÜüÙùÚúÛûŰűŨũŲųŮůŪūŴŵÝýŸÿŶŷŹźŽžŻż"];
        [validationCharSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
        NSString *trimmedString = [self stringByTrimmingCharactersInSet:validationCharSet];
        return ([trimmedString isEqualToString:@""]) ? YES : NO;
    }
    return NO;
}

-(BOOL) isEmptyOrWhiteSpaceString{
    NSMutableCharacterSet *validationCharSet = [NSMutableCharacterSet characterSetWithCharactersInString:@"/"];
    [validationCharSet formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:charSet];
    return ([trimmedString isEqualToString:@""]) ? YES : NO;
}


@end
