//
//  NSString+StringValidation.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringValidation)
#pragma mark - Validation
- (BOOL)isValidPhone;
- (BOOL)isValidEmail;
- (BOOL) isValidName;
- (BOOL) isEmptyOrWhiteSpaceString;
-(BOOL) isValidPostalCode;
@end
