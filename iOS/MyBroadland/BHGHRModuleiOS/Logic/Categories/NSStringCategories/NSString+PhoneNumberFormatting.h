//
//  NSString+PhoneNumberFormatting.h
//  BHGHRModuleiOS
//
//  Created by Afaq Hussain on 5/19/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PhoneNumberFormatting)
-(NSString *) getFormattedPhoneNumber;
-(NSString *) getPlainPhoneNumber;
@end
