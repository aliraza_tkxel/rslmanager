
@class FBRImageCache;

@protocol FBRImageCacheDelegate <NSObject>

@optional
- (void)imageCache:(FBRImageCache *)imageCache didFindImage:(UIImage *)image forKey:(NSString *)key userInfo:(NSDictionary *)info;
- (void)imageCache:(FBRImageCache *)imageCache didNotFindImageForKey:(NSString *)key userInfo:(NSDictionary *)info;

@end
