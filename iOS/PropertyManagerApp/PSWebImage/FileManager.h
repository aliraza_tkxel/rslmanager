
#import <Foundation/Foundation.h>

@interface FileManager : NSObject {

}


- (NSString *)checkForFile:(NSString *)fileName;
- (NSString *)createFile:(NSString *)fileName  data:(NSData*)fileData;
- (void)removeFile:(NSString *)fileName;
- (void)clear;


@end
