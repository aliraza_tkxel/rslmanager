#import "FileManager.h"
#import <sys/xattr.h>

@interface FileManager(Private)

- (long long)freeSpace;
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end

@implementation FileManager

#pragma mark FileManager(Private)

- (long long)freeSpace {
    NSFileManager *manager = [NSFileManager defaultManager];
    
    // get the file system attributes for the temporary directory
    NSDictionary *attributes = [manager attributesOfFileSystemForPath:NSTemporaryDirectory() error:NULL];
    
    // ask for the available space
    NSNumber *freeSize = [attributes objectForKey:NSFileSystemFreeSize];
    
    // and return to the caller
    return [freeSize unsignedLongLongValue];
}



#pragma mark FileManager

- (NSString *)checkForFile:(NSString *)fileName {
    // get the full path to the file
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
    
    // return YES/NO depending on if the file is there
    return [[NSFileManager defaultManager] fileExistsAtPath:fullPath] ? fullPath : nil;
}
- (NSString *)createFile:(NSString *)fileName  data:(NSData*)fileData {
    
    // get the full path to the file
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
    
    // create an empty file
    [[NSFileManager defaultManager] createFileAtPath:fullPath 
                                            contents:fileData
                                            attributes:nil];
    
    // and return the path
    return fullPath;
}
- (void)clear {
    NSFileManager *manager = [NSFileManager defaultManager];
    for (NSString *fileName in [manager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:nil]) {
        [manager removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName] error:NULL];
    }
}
//Remove the file from local cache
- (void)removeFile:(NSString *)fileName {
    NSFileManager *manager = [NSFileManager defaultManager];
    [manager removeItemAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:fileName] error:NULL];
}

@end
