

#import "UIImageView+WebCache.h"
#import "FileManager.h"
#import "FBRImageCache.h"
#import <QuartzCore/QuartzCore.h>

#define kIndicatorTag 10002
#define kIndicatorViewTag 10001
#define kFABRedColor [UIColor colorWithRed:194.0f/256 green:38.0f/256 blue:63.0f/256 alpha:1]

#define kFABLightGrayColor [UIColor colorWithRed:235.0f/256 green:235.0f/256 blue:235.0f/256 alpha:1]


@implementation FBRImageView (WebCache)

- (void)setImageWithURL:(NSURL *)url andAddBorder:(BOOL)yesOrNo
{
    [self setImageWithURL:url placeholderImage:nil andAddBorder:yesOrNo];
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder andAddBorder:(BOOL)yesOrNo
{
    [self setImageWithURL:url placeholderImage:placeholder options:0 andAddBorder:yesOrNo];
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(FBRWebImageOptions)options andAddBorder:(BOOL)yesOrNo
{
    FBRImageCache* imageCache = [FBRImageCache sharedImageCache];
    if (url) {
        NSString *keyPath = [[imageCache cachePathForKey:[url absoluteString]] lastPathComponent];
        [self setThumbnailKey:keyPath];
    }
    
    
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
   // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    
    UIView* indicatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [indicatorView setBackgroundColor:[UIColor grayColor]];
    [indicatorView setTag:kIndicatorViewTag];
    [indicatorView setAlpha:0.6];
    indicatorView.layer.cornerRadius = 5.0f;
    [indicatorView setHidden:NO];
    indicatorView.center = self.center;
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator setFrame:CGRectMake(0, 0, 50, 50)];
    //self.indicator.center = self.view.center;
    [indicator setTag:kIndicatorTag];
    indicator.center = self.center;
    [indicator setColor:kFABRedColor];
    [indicator setUserInteractionEnabled:NO];
    [indicator setBackgroundColor:[UIColor clearColor]];
    [indicator setAlpha:1.0];
    [indicator startAnimating];
    [self addSubview:indicatorView];
    [self addSubview:indicator];
    
    [indicatorView setFrame:CGRectMake((self.frame.size.width-indicatorView.frame.size.width)/2, (self.frame.size.height-indicatorView.frame.size.height)/2, indicatorView.frame.size.width, indicatorView.frame.size.height)];
    [indicator setFrame:CGRectMake((self.frame.size.width-indicator.frame.size.width)/2, (self.frame.size.height-indicator.frame.size.height)/2, indicator.frame.size.width, indicator.frame.size.height)];
    
    [indicatorView release];
    [indicator release];
    
    
    if (yesOrNo && url) {
        [self.layer setBorderWidth:1];
        [self.layer setBorderColor:[UIColor whiteColor].CGColor];
        self.layer.masksToBounds = NO;
        //self.layer.cornerRadius = 9; // if you like rounded corners
        self.layer.shadowOffset = CGSizeMake(-2, 2);
        self.layer.shadowRadius = 2;
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shouldRasterize = YES;
        
    }
   // [self.layer setShadowOffset:CGSizeMake(20.0f, 0.0f)];
   // [self.layer setShadowColor:[UIColor redColor].CGColor];

    self.image = placeholder;
    if (url)
    {
        [manager downloadWithURL:url delegate:self options:options];
    } else {
        //self.contentMode = UIViewContentModeScaleAspectFill;
        [self webImageManager:nil didFailWithError:nil];
    }
}

- (void)cancelCurrentImageLoad
{
    [[FBRWebImageManager sharedManager] cancelForDelegate:self];
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFinishWithImage:(UIImage *)image {
   
    if (!self.thumbnail) {
        self.image = image;
        [[self viewWithTag:10001] removeFromSuperview];
        [[self viewWithTag:10002] removeFromSuperview];
    } else {
        FileManager *fileManager = [[FileManager alloc] init];
        if ([fileManager checkForFile:self.thumbnailKey]) {
            //[UIImage imageWithData:[NSData ] pathForResource:@"mainScreenBackground" ofType:@"png"]]]];
            
            self.image = [[[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:[fileManager checkForFile:self.thumbnailKey]]]autorelease];
            //self.image = [[UIImage imageWithContentsOfFile:[fileManager checkForFile:self.thumbnailKey]];
            [[self viewWithTag:10001] removeFromSuperview];
            [[self viewWithTag:10002] removeFromSuperview];
        } else {
            
            dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                float hfactor = image.size.width / 145;
                float vfactor = image.size.height / 100;
                float factor = fmax(hfactor, vfactor);
                float newWidth = image.size.width / factor;
                float newHeight = image.size.height / factor;
                CGSize destinationSize = CGSizeMake(newWidth, newHeight);
            
            UIGraphicsBeginImageContext(destinationSize);
                [image drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                UIImage *thumb = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
           
                [fileManager createFile:self.thumbnailKey data: UIImageJPEGRepresentation(thumb, 1)];
       
            self.image = thumb;
            [[self viewWithTag:10001] removeFromSuperview];
            [[self viewWithTag:10002] removeFromSuperview];
            });
        }
        
        [fileManager release];
        

    }
    
        
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFailWithError:(NSError *)error {
    self.image = [UIImage imageNamed:@"dummyImage.png"];
    [[self viewWithTag:10001] removeFromSuperview];
    [[self viewWithTag:10002] removeFromSuperview];
    [self.layer setBorderWidth:0];
    [self.layer setBorderColor:[UIColor clearColor].CGColor];
    self.layer.masksToBounds = NO;
    self.layer.shadowOffset = CGSizeMake(-2, 2);
    self.layer.shadowRadius = 0;
    self.layer.shadowOpacity = 0.0;
    self.layer.shadowColor = [UIColor clearColor].CGColor;
}
@end
