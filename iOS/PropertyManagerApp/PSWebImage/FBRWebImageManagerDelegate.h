/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

@class FBRWebImageManager;
@class UIImage;

@protocol FBRWebImageManagerDelegate <NSObject>

@optional

- (void)webImageManager:(FBRWebImageManager *)imageManager didFinishWithImage:(UIImage *)image;
- (void)webImageManager:(FBRWebImageManager *)imageManager didFailWithError:(NSError *)error;

@end
