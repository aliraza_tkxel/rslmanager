//
//  JobDataList.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, FaultRepairData, JobPauseData;

@interface JobDataList : NSManagedObject

@property (nonatomic, retain) NSDate * completionDate;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * faultLogID;
@property (nonatomic, retain) NSString * followOnNotes;
@property (nonatomic, retain) NSString * jobStatus;
@property (nonatomic, retain) NSString * jsnDescription;
@property (nonatomic, retain) NSString * jsnLocation;
@property (nonatomic, retain) NSString * jsnNotes;
@property (nonatomic, retain) NSString * jsNumber;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * repairNotes;
@property (nonatomic, retain) NSDate * reportedDate;
@property (nonatomic, retain) NSString * responseTime;
@property (nonatomic, retain) NSSet *faultRepairDataList;
@property (nonatomic, retain) Appointment *jobDataListToAppointment;
@property (nonatomic, retain) NSSet *jobPauseData;
@end

@interface JobDataList (CoreDataGeneratedAccessors)

- (void)addFaultRepairDataListObject:(FaultRepairData *)value;
- (void)removeFaultRepairDataListObject:(FaultRepairData *)value;
- (void)addFaultRepairDataList:(NSSet *)values;
- (void)removeFaultRepairDataList:(NSSet *)values;

- (void)addJobPauseDataObject:(JobPauseData *)value;
- (void)removeJobPauseDataObject:(JobPauseData *)value;
- (void)addJobPauseData:(NSSet *)values;
- (void)removeJobPauseData:(NSSet *)values;

@end
