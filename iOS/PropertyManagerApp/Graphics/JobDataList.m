//
//  JobDataList.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobDataList.h"
#import "Appointment.h"
#import "FaultRepairData.h"
#import "JobPauseData.h"


@implementation JobDataList

@dynamic completionDate;
@dynamic duration;
@dynamic faultLogID;
@dynamic followOnNotes;
@dynamic jobStatus;
@dynamic jsnDescription;
@dynamic jsnLocation;
@dynamic jsnNotes;
@dynamic jsNumber;
@dynamic priority;
@dynamic repairNotes;
@dynamic reportedDate;
@dynamic responseTime;
@dynamic faultRepairDataList;
@dynamic jobDataListToAppointment;
@dynamic jobPauseData;

@end
