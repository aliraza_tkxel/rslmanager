//
//  PSAppDelegate.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSAppDelegate+Window.h"
#import "PSAppDelegate+Version.h"
#import "PSAppointmentsViewController.h"
#import "PSLoginViewController.h"
#import "PSCustomNavigationController.h"
#import "PSForgotPasswordViewController.h"
#import "PSChangePasswordViewController.h"

@implementation PSAppDelegate
@synthesize userLoggedInToApplication,postingDataObjectAppointments;

+ (PSAppDelegate *) delegate
{
    return (PSAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    [Fabric with:@[[Crashlytics class]]];
    //[[Fabric sharedSDK] setDebug: YES];
    self.postingDataObjectAppointments = 0;
    application.applicationIconBadgeNumber = 0;
    //Register For Notifications
    [self registerNotifications];
    [self registerForAPNS];
    [self setUserLoggedInToApplication:FALSE];
    [self loadAppVersionInfo];
    [self registringSettingBundleFields];
    //Start Reachability Monitoring
    
    
    
    //Create Read MOC ..Dont Remove Below 2 lines . These are for coredata movement + storage
    [[PSDatabaseContext sharedContext] persistentStoreCoordinator];
    [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [[SettingsClass sharedObject] loadSettings];
    
    CLS_LOG(@"%@", [SettingsClass sharedObject].loggedInUser);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if([UtilityClass isUserLoggedIn] && [SettingsClass sharedObject].autoLogin)
    {
        [CrashlyticsKit setUserName:[SettingsClass sharedObject].loggedInUser.userName];
        [[PSAuthenticator sharedAuthenticator] authenticateUser];
        self.appointmentsViewController = [[PSAppointmentsViewController alloc] initWithNibName:@"PSAppointmentsViewController" bundle:nil];
        PSCustomNavigationController *navigationController = [[PSCustomNavigationController alloc] initWithRootViewController:self.appointmentsViewController];
        self.window.rootViewController = navigationController;
    }
    else
    {
        self.loginViewController = [[PSLoginViewController alloc] initWithNibName:@"PSLoginViewController" bundle:nil];
        PSCustomNavigationController *navigationController = [[PSCustomNavigationController alloc] initWithRootViewController:self.self.loginViewController];
        [navigationController setNavigationBarHidden:YES];
        self.window.rootViewController = navigationController;
    }
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    return YES;
}

-(void)registringSettingBundleFields
{
#if TEST
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:5]
                                                            forKey:@"firetime_preference"];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
#endif
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[SettingsClass sharedObject] saveSettings];
    
    //Stop Reachability Monitoring
    /*[[ReachabilityManager sharedManager] stopMonintoring];
     [[PSOutBox sharedOutbox]stopMonintoring];*/
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if([UtilityClass isUserLoggedIn] && [SettingsClass sharedObject].autoLogin)
    {
        [[PSAuthenticator sharedAuthenticator] authenticateUser];
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.postingDataObjectAppointments = 0;
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    self.postingDataObjectAppointments =0;
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[SettingsClass sharedObject] saveSettings];
    [[PSReachibilityManager sharedManager] stopMonitoringNetworkChanges];
    [self deRegisterNotifications];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}


#pragma mark Notifications
- (void) registerNotifications {
    
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSignOutNotification) name:kUserSignOutNotification object:nil];
    CLS_LOG(@"-------------------------->registerNotifications------------------------");
    

}

- (void) deRegisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kUserSignOutNotification object:nil];
    CLS_LOG(@"*************************deRegisterNotifications************************");
}

- (void) onReceiveSignOutNotification
{
    [[PSReachibilityManager sharedManager] stopMonitoringNetworkChanges];
    [self registerForAPNS];
    self.postingDataObjectAppointments = 0;
    [[PSAppDelegate delegate] showLoginView];
}

- (void)reachabilityChangedNotReceived:(NSNotification *)notification {
    if(IS_NETWORK_AVAILABLE) {
        if([SettingsClass sharedObject].isSyncingProgress==NO){
            [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        }
        
    }
    
}

#pragma mark - Push Notifications

//Called when a notification is delivered to a foreground app - iOS 10+.
/*-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler;
{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}

//Open notification settings screen in app
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
   openSettingsForNotification:(UNNotification *)notification{
    
}*/

- (void) registerForAPNS
{
    
    /*if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        // Code for old versions
        if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            CLS_LOG(@"Requesting permission for push notifications...iOS8+");
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound categories:nil];
            [UIApplication.sharedApplication registerUserNotificationSettings:settings];
        } else {
            NSLog(@"Registering device for push notifications...iOS7 and below");
            [UIApplication.sharedApplication registerForRemoteNotificationTypes:
             UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |
             UIRemoteNotificationTypeSound];
            
        }
    }*/
    // Code for old versions
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        CLS_LOG(@"Requesting permission for push notifications...iOS8+");
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |
                                                UIUserNotificationTypeSound categories:nil];
        [UIApplication.sharedApplication registerUserNotificationSettings:settings];
    } else {
        NSLog(@"Registering device for push notifications...iOS7 and below");
        [UIApplication.sharedApplication registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeSound];
        
    }
    
    
    
}
- (void) unregisterForAPNS
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void) handlePushNotificationOnAppLaunch:(NSDictionary*)userInfo {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSDictionary* payload = [userInfo objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(payload != nil) {
        [self handlePushNotificationWithPayload:payload notificationMode:APNSModeLaunch];
    }
    return;
}

- (void) handlePushNotificationWithPayload:(NSDictionary*)payload notificationMode:(APNSMode)mode {
    CLS_LOG(@"didReceiveRemoteNotification %@",payload);
    
    [[APNSHandler sharedHandler] handlePushNotificationWithPayload:payload notificationMode:mode];
}


- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    application.applicationIconBadgeNumber = 0;
    NSString *deviceTokenStr = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" "
                                withString:@""];
    NSLog(@"APNS DTK: %@",deviceTokenStr);
    if(!isEmpty(deviceTokenStr))
    {
        [[SettingsClass sharedObject] setDeviceToken:deviceTokenStr];
    }
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    application.applicationIconBadgeNumber = 0;
    CLS_LOG(@"payload %@",notification.alertBody);
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:APP_NAME_STRING message:notification.alertBody delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}


- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    CLS_LOG(@"%@",[error description]);
    if ([error code] != 3010) // 3010 is for the iPhone Simulator
    {
        [[SettingsClass sharedObject] setDeviceToken:@""];
    }
    
}



- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if(state == UIApplicationStateActive){
        CLS_LOG(@"UIApplicationStateActive");
        [self handlePushNotificationWithPayload:userInfo notificationMode:APNSModeRemote];
    }
    else if(state == UIApplicationStateInactive){
        CLS_LOG(@"UIApplicationStateInactive");
        [self handlePushNotificationWithPayload:userInfo notificationMode:APNSModeLaunch];
    } else if(state == UIApplicationStateBackground){
        CLS_LOG(@"UIApplicationStateBackground");
    }
}


- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings *)settings
{
    NSLog(@"Registering device for push notifications..."); // iOS 8
    [application registerForRemoteNotifications];
}

@end
