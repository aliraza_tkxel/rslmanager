//
//  PSAppDelegate+Window.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate+Window.h"
#import "PSLoginViewController.h"
#import "PSCustomNavigationController.h"
#import "PSAppointmentsViewController.h"

@implementation PSAppDelegate (Window)

- (void) showLoginView
{
    [self setLoginViewController:[[PSLoginViewController alloc] initWithNibName:@"PSLoginViewController" bundle:nil]];
    [self setNavigationController:[[PSCustomNavigationController alloc] initWithRootViewController:self.loginViewController]];
    [self.navigationController.navigationBar setHidden:YES];
    [UIView transitionWithView:self.window duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:
     ^(void) {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         self.window.rootViewController = self.navigationController;
         [UIView setAnimationsEnabled:oldState];
     }
                    completion:nil];    
}

- (void) showAppointmentsView
{
    [self setAppointmentsViewController:[[PSAppointmentsViewController alloc] initWithNibName:@"PSAppointmentsViewController" bundle:nil]];
    [self setNavigationController:[[PSCustomNavigationController alloc] initWithRootViewController:self.appointmentsViewController]];
    [UIView transitionWithView:self.window duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:
     ^(void) {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         self.window.rootViewController = self.navigationController;
         [UIView setAnimationsEnabled:oldState];
     }
     completion:nil];
}

@end
