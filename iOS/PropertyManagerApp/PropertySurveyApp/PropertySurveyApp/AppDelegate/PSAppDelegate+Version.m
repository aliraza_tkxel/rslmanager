//
//  PSAppDelegate+Version.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate+Version.h"

@implementation PSAppDelegate (Version)

#define LAST_VERSION_RUN @"Settings.LastVersionRun"
- (void) loadAppVersionInfo
{
	// get the current version
	NSString *currentVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
	// get the last version run
	NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
	NSString *lastVersion = [preferences objectForKey:LAST_VERSION_RUN];
    
	// is this a new version being run?
	if (![currentVersion isEqualToString:lastVersion]) {
        
		// yes, record this as the last version run now
		[preferences setObject:currentVersion forKey:LAST_VERSION_RUN];
		[preferences synchronize];
        [self deleteCacheOfOldApp];
        self.isFirstRunForVersion = YES;
        
        
	} else {
        self.isFirstRunForVersion = NO;
	}
}

-(void)deleteCacheOfOldApp
{
        NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSArray *directoryContent = [fileManager contentsOfDirectoryAtPath:cachePath error:NULL];
        
        NSError* error;
        NSString* fullFilePath = nil;
        
        for(NSString* fileName in directoryContent)
        {
                fullFilePath = [cachePath stringByAppendingPathComponent:fileName];
                [fileManager removeItemAtPath:fullFilePath error:&error];
                if(error)
                {
                    CLS_LOG(@"Error: %@",error);
                }
        }
}

@end
