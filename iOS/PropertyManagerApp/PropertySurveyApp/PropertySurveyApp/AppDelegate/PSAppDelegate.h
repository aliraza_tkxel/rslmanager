//
//  PSAppDelegate.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <UserNotifications/UserNotifications.h>
#import <Firebase/Firebase.h>
@class PSAppointmentsViewController;
@class PSLoginViewController;
@class PSCustomNavigationController;
static BOOL _allowRelaodingTableView_ = YES;
@interface PSAppDelegate : UIResponder <UIApplicationDelegate, UIAppearanceContainer>
//@interface PSAppDelegate : UIResponder <UIApplicationDelegate, UIAppearanceContainer,UNUserNotificationCenterDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PSCustomNavigationController *navigationController;
@property (strong, nonatomic) PSAppointmentsViewController *appointmentsViewController;
@property (strong, nonatomic) PSLoginViewController *loginViewController;

@property (readwrite, nonatomic) BOOL isFirstRunForVersion;
@property (readwrite, nonatomic) BOOL userLoggedInToApplication;
@property (readwrite, nonatomic) BOOL postingStartedByOutbox;
@property (readwrite,nonatomic) int postingDataObjectAppointments;

+ (PSAppDelegate *) delegate;

@end
