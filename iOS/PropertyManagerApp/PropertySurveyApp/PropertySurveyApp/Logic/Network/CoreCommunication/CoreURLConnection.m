/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import "CoreURLConnection.h"

static NSMutableDictionary *globalCache;

@interface CoreURLConnection()
{
    float       _downloadProgress;
    long long   _expectedContentLength;
    long long   _downloadedSoFar;
    BOOL        _downloadIsIndeterminate;
}

- (void)logRequest:(NSString *)method content:(NSData *)content;
- (void)logResponse;

@end

static NSString *completeAppointmentsServiceURL = @"CompleteAppointment";

@implementation CoreURLConnection

@synthesize delegate;
@synthesize url;
@synthesize bufferedContentTypes;
@synthesize connection;
@synthesize connectionActive;
@synthesize responseData;
@synthesize responseBuffered;
@synthesize filePath;
@synthesize file;
@synthesize cacheMode;
@synthesize enableCompression;
@synthesize method;
@synthesize contentType;

#pragma mark Initialization

+ (void)initialize
{
	globalCache = [[NSMutableDictionary alloc] init];
}
- (id)initWithDelegate:(id<CoreURLConnectionDelegate>)pointer url:(NSString *)strURL
{
	if ((self = [super init]) != nil)
	{
		[self setCacheMode:RFURLConnectionCacheModeNone];
		[self setDelegate:pointer];
		[self setUrl:strURL];
		[self setConnection:nil];
		[self setResponseData:nil];
		_expectedContentLength = -1;
	}
	return (self);
}
- (void)dealloc
{
	[self setDelegate:nil];
	[self setBufferedContentTypes:nil];
	[self setUrl:nil];
	[self setConnection:nil];
	[self setResponseData:nil];
	[self setFilePath:nil];
	[self setFile:nil];
	[super dealloc];
}

#pragma mark CoreURLConnection

- (NSMutableData *)cachedResponse:(NSData *)content
{
	switch (cacheMode)
	{
		case RFURLConnectionCacheModeNone:
			// if we aren't caching anything, return nil
			return (nil);
		case RFURLConnectionCacheModeApplication:
			// get the service cache from the global cache
			if ([globalCache objectForKey:NSStringFromClass([self class])] == nil)
			{
					[globalCache setObject:[NSMutableDictionary dictionary] forKey:NSStringFromClass([self class])];
			}
			NSMutableDictionary *globalServiceCache = [globalCache objectForKey:NSStringFromClass([self class])];
			// create the cache key
			NSString *cacheKey = [[[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding] autorelease];
			// see if we have a response cached for this request
			NSMutableData *cachedResponse = [globalServiceCache objectForKey:cacheKey];
			
			if (cachedResponse != nil)
			{
					// if we have a response, return it
					return (cachedResponse);
			}
			else
			{
					// if not, setup the current response object to cache
					[globalServiceCache setObject:responseData forKey:cacheKey];
					
					// tell the caller we need to hit the server
					return (nil);
			}
		default:
			return (nil);
	}
}
- (void)purgeCacheForCurrentRequest
{
	switch (cacheMode)
	{
		case RFURLConnectionCacheModeNone:
			// if we aren't caching anything, don't do anything
			break;
		case RFURLConnectionCacheModeApplication:
			// get the service cache from the global cache
			if ([globalCache objectForKey:NSStringFromClass([self class])] == nil)
			{
					[globalCache setObject:[NSMutableDictionary dictionary] forKey:NSStringFromClass([self class])];
			}
			NSMutableDictionary *connectionCache = [globalCache objectForKey:NSStringFromClass([self class])];
			
			// get rid of the current response data object from the cache
			id keyToPurge = nil;
			for (id key in [connectionCache allKeys])
			{
					if ([connectionCache objectForKey:key] == responseData)
					{
							keyToPurge = key;
							break;
					}
			}
			if (keyToPurge != nil)
			{
					[connectionCache removeObjectForKey:keyToPurge];
			}
			break;
	}
}

//- (void)invoke:(NSString *)query content:(NSData *)content contentType:(NSString *)contentType streamLocation:(NSString *)streamLocation {
- (void)invoke:(NSString *)query content:(NSData *)content streamLocation:(NSString *)streamLocation
{
    // clear out our data cache, which will be as we receive data from the server.  in addition, if
    // we need to cache this particular request, then this will be the object that holds the 
    // response.
    [self setResponseData:[NSMutableData data]];
    
    // concatenate the query and URL
    NSString *urlAndQuery = self.url;
    if ([method isEqualToString:@"GET"] || [contentType isEqualToString:@"application/x-www-form-urlencoded"])
		{
        urlAndQuery = [NSString stringWithFormat:@"%@?%@", self.url, query];
    }
    //NSData * content= [NSData dataWithContentsOfFile:streamLocation];
    
    // create the URL request that we will post to the server -- set a ridiculously high timeout
    // on it, as some of our services can take a looong time to come back.
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:urlAndQuery]];
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [urlRequest setHTTPMethod:method];
    [urlRequest setTimeoutInterval:600];
	
    if (!content && streamLocation)
		{
        content=[NSData dataWithContentsOfFile:streamLocation];
    }
    
    // add content if we were given any
    if (content != nil && contentType != nil)
		{
        [urlRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
        //            [urlRequest setValue:@"text/plain; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
        [urlRequest setValue:@"utf-8,gzip,deflate" forHTTPHeaderField:@"Accept-Encoding"];
        
        [urlRequest setValue:[NSString stringWithFormat:@"%ld", [content length]] forHTTPHeaderField:@"Content-Length"];
        
        CLS_LOG(@"ContentLength Before Compression %ld",[content length]);
        
        // compress the data (if required)
        if(self.enableCompression)
        {
            content = [content gzipDeflate];
            CLS_LOG(@"ContentLength After Compression %ld",[content length]);
        }
        [urlRequest setHTTPBody:content];
    }
    
    // log the request
    [self logRequest:method content:content];
    
    // begin loading the data
    [self setConnection:[NSURLConnection connectionWithRequest:urlRequest delegate:self]];
    
    // tell the connection to begin
    [self.connection start];
    
    // mark the connection as active
    [self setConnectionActive:YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    NSString *body = nil;
    if ([urlRequest HTTPBody]) {
        body = [[NSString alloc] initWithData:[urlRequest HTTPBody] encoding:NSUTF8StringEncoding];
    }
    
     CLS_LOG(@"%@ '%@': %@ %@", [urlRequest HTTPMethod], [[urlRequest URL] absoluteString], [urlRequest allHTTPHeaderFields], body);
    
}

- (NSData *) invokeSync:(NSString *)query content:(NSData *)content
{
    // clear out our data cache, which will be as we receive data from the server.  in addition, if
    // we need to cache this particular request, then this will be the object that holds the
    // response.
    [self setResponseData:[NSMutableData data]];
    
    // concatenate the query and URL
    NSString *urlAndQuery = self.url;
    if ([method isEqualToString:@"GET"]) {
        urlAndQuery = [NSString stringWithFormat:@"%@?%@", self.url, query];
    }
    
    // create the URL request that we will post to the server -- set a ridiculously high timeout
    // on it, as some of our services can take a looong time to come back.
    NSMutableURLRequest *urlRequest = [[[NSMutableURLRequest alloc] init] autorelease];
    [urlRequest setURL:[NSURL URLWithString:urlAndQuery]];
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [urlRequest setHTTPMethod:method];
    [urlRequest setTimeoutInterval:300];
    
    
    
    // add content if we were given any
    if (content != nil && contentType != nil) {
        
        [urlRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
        //            [urlRequest setValue:@"text/plain; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setValue:@"utf-8" forHTTPHeaderField:@"Accept-Charset"];
        [urlRequest setValue:@"utf-8,gzip,deflate" forHTTPHeaderField:@"Accept-Encoding"];
        
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[content length]] forHTTPHeaderField:@"Content-Length"];
        
        CLS_LOG(@"ContentLength Before Compression %lu",(unsigned long)[content length]);
        
        // compress the data (if required)
        if(self.enableCompression)
        {
            content = [content gzipDeflate];
            CLS_LOG(@"ContentLength After Compression %lu",[content length]);
        }
        
        [urlRequest setHTTPBody:content];
    }
    
    // log the request
    [self logRequest:method content:content];
    
    // Send Synchrnous Request
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    NSData *syncResponseData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    if(error != nil)
    {
        CLS_LOG(@"Sync Request Error: %@", [error description]);
        syncResponseData = nil;
    }
    return syncResponseData;
}

- (BOOL)active {
    return connectionActive;
}
- (void)cancel {
    [self setConnectionActive:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    // if the connection is active and we're sreaming...
    if (connectionActive && file != nil) {
        // close up the file
        [self.file closeFile];
        
        // and delete it -- since the connection is still active we didn't get the entire thing
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    // if this service is participating in any kind of caching, make sure to clear out the current
    // response cache since we got cancelled
    if (connectionActive) {
        [self purgeCacheForCurrentRequest];
    }
    
    [self.connection cancel];
    [self setResponseData:nil];
    [self setConnection:nil];
}

#pragma mark NSURLConnection




- (void)connection:(NSURLConnection *)c didReceiveResponse:(NSURLResponse *)response {
    // we seem to be having hanging connections in the background -- if we get any notifications 
    // from an old connection ignore them
    if (self.connection != c || !connectionActive) {
        CLS_LOG(@"Connection Cancelled");
        return;
    }

    // by default buffer the response if we have no buffered content types
    [self setResponseBuffered:([bufferedContentTypes count] == 0)];
    
    // loop over all the content types to determine if we should buffer this response
    for (NSString *contentMIMEType in bufferedContentTypes)
		{
        if ([contentMIMEType isEqualToString:[response MIMEType]])
				{
            [self setResponseBuffered:YES];
            break;
        }
    }
    
    int statusCode = (int)[((NSHTTPURLResponse *)response) statusCode];
    CLS_LOG(@"StatusCode Received: %d", statusCode);
    //NSInteger contentLength = [[[response allHeaderFields] objectForKey:@"Content-Length"] intValue];
    if (statusCode == 200)
		{
        _expectedContentLength = [response expectedContentLength];
        if (_expectedContentLength > 0.0)
        {
            _downloadIsIndeterminate = NO;
            _downloadedSoFar = 0;
        }
    }
  /*  else
    {
        NSURL *currentURL = [[c currentRequest] URL];
        NSArray *URLcomponents = [currentURL pathComponents];
        
        NSString *URLString = [URLcomponents objectAtIndex:[URLcomponents count] - 1];//[[currentURL baseURL] path];
        if ([URLString isEqualToString:completeAppointmentsServiceURL])
        {
            CLS_LOG(@"fetch all appointment service");
  //          [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kRefreshAppointmentsExceptionReceiveNotification object:nil];
        }
    }*/
    // extract the headers and forward to whom registered
    NSDictionary *header = [(NSHTTPURLResponse *) response allHeaderFields];
    NSMutableDictionary *headerDic = [NSMutableDictionary dictionaryWithDictionary:header];
    [headerDic setObject:[NSNumber numberWithInteger:statusCode] forKey:@"statusCode"];
    
    if ([self.delegate respondsToSelector:@selector(connection:didReceiveResponseHeader:)]) {
        [self.delegate connection:self didReceiveResponseHeader:headerDic];
    }
    
    
}

- (void)connection:(NSURLConnection *)c didReceiveData:(NSData *)data
{
	// we seem to be having hanging connections in the background -- if we get any notifications
	// from an old connection ignore them
	if (self.connection != c || !connectionActive)
	{
		CLS_LOG(@"Connection Cancelled");
		return;
	}

	// buffer the data, if we're running in buffered zone
	[self.responseData appendData:data];
	NSUInteger length = [data length];
	CLS_LOG(@"Received Data Length: %lul", length);
	_downloadedSoFar += length;
	CLS_LOG(@"Download So Far: %lld, Expected: %lld", _downloadedSoFar, _expectedContentLength);
	
	if (_downloadedSoFar >= _expectedContentLength)
	{
		// the expected content length was wrong as we downloaded more than expected
		// make the progress indeterminate
		_downloadIsIndeterminate = YES;
	}
	else
	{
		//_downloadProgress = ((float) length / (float) _expectedContentLength);
		_downloadProgress = (float)_downloadedSoFar / (float)_expectedContentLength;
		CLS_LOG(@"Download Progress: %f", _downloadProgress);
	}
	// notify the retriever about download progress
	if ([self.delegate respondsToSelector:@selector(connection:hasDownloaded:expectedContentLength:)])
	{
		[self.delegate connection:self hasDownloaded:(NSInteger)_downloadedSoFar expectedContentLength:(NSInteger)_expectedContentLength];
	}
}
- (void)connectionDidFinishLoading:(NSURLConnection *)c {
    // we seem to be having hanging connections in the background -- if we get any notifications 
    // from an old connection ignore them
    if (self.connection != c || !connectionActive) {
        CLS_LOG(@"Connection Cancelled");
        return;
    }
    
    [self setConnectionActive:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    // log the response if configured for it
    [self logResponse];
    [self.connection cancel];
    self.connection = nil;
        // notify the retriever that we've retrieved our data
    if ([self.delegate respondsToSelector:@selector(connection:didReceiveData:)])
        [self.delegate connection:self didReceiveData:[self responseData]];
}

- (void)connection:(NSURLConnection *)c didFailWithError:(NSError *)error {
    // we seem to be having hanging connections in the background -- if we get any notifications 
    // from an old connection ignore them
    if (self.connection != c || !connectionActive) {
        CLS_LOG(@"Connection Cancelled");
        return;
    }

	if ([self.delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
		[self.delegate connection:self didFailWithError:error];
	}
    
    [self setConnectionActive:NO];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    CLS_LOG(@"Connection failed with error %@ %d", [error domain], [error code]);
}

#pragma mark CoreURLConnection(Private)

- (void)logRequest:(NSString *)requestMethod content:(NSData *)content {
    CLS_LOG(@"====================================================================================");
    CLS_LOG(@"Executing request for URL: %@ and method: %@", url, requestMethod);
    if (content != nil) {
        CLS_LOG(@"Content: %@", [[[NSString alloc] initWithData:content encoding:NSUTF8StringEncoding] autorelease]);
    }
    CLS_LOG(@"====================================================================================");
}
- (void)logResponse {
    CLS_LOG(@"====================================================================================");
    if ([responseData length] > 10240) {
        CLS_LOG(@"Connection returned over 10K of data\n %@\n\n", [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
    } else {
        CLS_LOG(@"Connection returned data: %@", [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease]);
    }
    CLS_LOG(@"====================================================================================");
}


@end
