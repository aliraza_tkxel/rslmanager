/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import "CoreRESTService.h"
#import <UIKit/UIDevice.h>


@implementation CoreRESTService

@synthesize rootURL;
@synthesize delegate;
@synthesize connection;
@synthesize method;
@synthesize contentType;
@synthesize responseClass;

#pragma mark Initialization

- (id)initWithRootUrl:(NSString *)rootUrl serviceUrl:(NSString *)serviceUrl
{
	if ((self = [super init]))
	{
		// calculate the fully qualified REST URL
		NSString *fullyQualifiedUrl = [NSString stringWithFormat:@"%@%@", rootUrl, serviceUrl];
		
		// create and initialize the connection
		[self setConnection:[[[CoreFormURLConnection alloc] initWithDelegate:self url:fullyQualifiedUrl] autorelease]];
		
		// set the content type to buffer
		[self.connection setBufferedContentTypes:[NSArray arrayWithObject:@"application/json"]];
	}
	return (self);
}

- (void)setUrl:(NSString *)rootUrl serviceUrl:(NSString *)serviceUrl
{
	// calculate the fully qualified REST URL
	NSString *fullyQualifiedUrl = [NSString stringWithFormat:@"%@%@", rootUrl, serviceUrl];
	
	// create and initialize the connection
	[self setConnection:[[[CoreFormURLConnection alloc] initWithDelegate:self url:fullyQualifiedUrl] autorelease]];
	
	// set the content type to buffer
	[self.connection setBufferedContentTypes:[NSArray arrayWithObject:@"application/json"]];
}

- (void)dealloc
{
	[self setRootURL:nil];
	[self setDelegate:nil];
	[self setConnection:nil];
	[self setMethod:nil];
	[self setResponseClass:nil];
	[super dealloc];
}

#pragma mark CoreRESTService(Private)

- (void)invoke:(NSDictionary *)parameters responseClass:(Class)clazz streamLocation:(NSString *)streamLocation enableCompression:(BOOL)enableCompression
{
	// save the method and response class
	[self setResponseClass:clazz];
	
	//Set the HTTP Method
	[connection setMethod:method];

	//Set the HTTP Method
	[connection setContentType:contentType];

	// set the flag to compress data
	[self.connection setEnableCompression:enableCompression];
	[connection invoke:parameters streamLocation:streamLocation];
}

- (NSData *)invokeSync:(NSDictionary *)parameters enableCompression:(BOOL)enableCompression
{
    //Set the HTTP Method
    [connection setMethod:method];
    
    //Set the HTTP Method
    [connection setContentType:contentType];
    
    // set the flag to compress data
    [self.connection setEnableCompression:enableCompression];
    return [connection invokeSync:parameters];
}

#pragma mark CoreRESTService

- (BOOL)active
{
    return [self.connection active];
}
- (void)cancel
{
    [self.connection cancel];
}
- (BOOL)isErrorResponse:(id)jsonObject
{
    return (NO);
}
- (void)didReceiveError:(id)jsonObject
{
    //    if ([[status code] isEqual:@"403"]) {
    //        // post a notification that the user's session timed out
    //        [[NSNotificationCenter defaultCenter] postNotificationName:kCommonSessionTimeout
    //                                                            object:nil];
    //    } else if (([[status code] isEqual:@"400"] && [[status subCode] isEqual:@"3-1"]) || [[status code] isEqual:@"401"] ) {
    //        // post a notification that requested entuty not found
    //        [[NSNotificationCenter defaultCenter] postNotificationName:kAuthorizationFailureNotification
    //                                                            object:nil];
    //    }
    //    // all other errors are concerened unhandled and unexpected
    //    else {
    //        // post a notification that the server returned an error
    //        [[NSNotificationCenter defaultCenter] postNotificationName:kCommonUnexpectedServerError
    //                                                            object:nil];
    //    }

}

- (void) executeRequest:(NSDictionary *)parameters responseClass:(Class)clazz streamLocation:streamLocation enableCompression:(BOOL)enableCompression
{
    [self invoke:parameters responseClass:clazz streamLocation:streamLocation enableCompression:enableCompression];
}

- (NSData *) executeSyncRequest:(NSDictionary *)parameters enableCompression:(BOOL)enableCompression
{
    return [self invokeSync:parameters enableCompression:enableCompression];
}

#pragma mark CoreURLConnectionDelegate

- (void)connection:(CoreURLConnection *)c didReceiveData:(NSData *)data
{
	id jsonObject = nil;
	if (data != nil && [data length] > 0)
	{
		jsonObject = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
		#warning PSA, JSON Data Parsing Issue Hot Fix, Remove the line, after fix on server side.
		jsonObject = [jsonObject stringByReplacingOccurrencesOfString:@", ," withString:@","];
	}

	// if this json response represents an error
	if ([self isErrorResponse:jsonObject])
	{
		// if the delegate responds to the handling selector, if it is handled by sub class. If not, handle in same class.
		BOOL errorHandled = NO;
		if ([self.delegate respondsToSelector:@selector(restService:didReceiveError:)])
		{
			CLS_LOG(@"Subclass has implemented didReceiveError");
			errorHandled = [self.delegate restService:self didReceiveError:jsonObject];
		}
		
		// if the error hasn't been handled, allow the subclass to handle it
		if (!errorHandled)
		{
			[self didReceiveError:jsonObject];
		}
	}
	// otherwise, we got a normal response
	else
	{
		if ([self.delegate respondsToSelector:@selector(restService:didReceiveResponse:)])
		{
			[self.delegate restService:self didReceiveResponse:jsonObject];
		}
	}
}

- (void)connection:(CoreURLConnection *)connection hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
	if ([self.delegate respondsToSelector:@selector(restService:hasDownloaded:expectedContentLength:)])
	{
		[self.delegate restService:self hasDownloaded:downloadedBytes expectedContentLength:expectedContentLength];
	}
}
- (void)connection:(CoreURLConnection *)connection didFinishStreaming:(NSString *)filePath
{
	if ([self.delegate respondsToSelector:@selector(restService:didFinishStreaming:)])
	{
		[self.delegate restService:self didFinishStreaming:filePath];
	}
}

- (void)connection:(CoreURLConnection *)con didReceiveResponseHeader:(NSDictionary *)responseHeader
{
	// For handling the Page not found or redirection error we have to get the status code from responce header.
	int statusCode = [[responseHeader objectForKey:@"statusCode"] intValue];
	if (statusCode == 500 || statusCode == 404)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:kCommonUnableToAccessServices
																												object:nil];
		if(self == [PSCompleteAppointmentService class] || [PSGetAllAppointmentsService class])
		{
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kRefreshAppointmentsExceptionReceiveNotification object:nil];
        }
        //Handle 500 error for Progress Status Syncing service
        if([self class]== [PSSyncAppointmentStatusService class]){
            [self.delegate restService:self didReceiveError:[NSNull null]];
        }
		[con cancel];
		//SET TARGET TO THREAD THAT IS POSTING OFFLINE IMAGES IN CASE ITS RUNNING ELSE NOTTHING IS LISTNING
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentOfflineImageSaveNotificationFail object:nil];
	}
	else if ([self.self.delegate respondsToSelector:@selector(restService:didReceiveResponseHeader:)])
	{
		[self.delegate restService:self didReceiveResponseHeader:responseHeader];
	}
}

@end
