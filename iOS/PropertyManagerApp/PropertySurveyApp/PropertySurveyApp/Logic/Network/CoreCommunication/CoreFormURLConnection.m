/*    Copyright (c) 2013 Tkxel. All rights reserved. */

#import "CoreFormURLConnection.h"
#import <UIKit/UIDevice.h>

@implementation CoreFormURLConnection

#pragma mark form-post

- (BOOL)checkForMultipart:(NSDictionary *)parameters
{
	for (NSString *pKey in [parameters allKeys])
	{
		id value = [parameters objectForKey:pKey];
		
		if ([value isKindOfClass:[NSURL class]])
		{
			NSURL *uValue = (NSURL *)value;
			if (![uValue isFileURL])
			{
				NSException *fileOnly = [NSException exceptionWithName:@"FileSchemeOnlyException"
																												reason:@"Can only post NSURL objects with file:// scheme"
																											userInfo:nil];
				@throw fileOnly;
			}
			return (YES);
		}
	}
	return (false);
}

- (NSString *)encodeParameters:(NSDictionary *)parameters excludeLarge:(BOOL)exclude
{
	NSString *postParameters = @"";
	NSArray * paramArray=nil;
	paramArray=[parameters allKeys];
	
	for (NSString *pKey in paramArray)
	{
		// get the value out
		id value = [parameters objectForKey:pKey];
		
		if ([value isKindOfClass:[NSNumber class]])
		{
			value=((NSNumber*)value).stringValue;
		}
		// can't encode NSURL objects (file references)
		if (![value isKindOfClass:[NSString class]])
		{
			continue;
		}
		// exclude large parameters if we've been instructed to
		if ([value length] > 256 && exclude == TRUE)
		{
			continue;
		}
		// escape the form paramter
		value = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)value, NULL, (CFStringRef)@"!*'();:@+$,/?%#[]&", kCFStringEncodingUTF8);
		// create the parameter to send to the server
		NSString *parameter = @"";
		if([postParameters length] > 0)
		{
			parameter = [parameter stringByAppendingString:@"&"];
		}
		parameter=[parameter stringByAppendingString:[NSString stringWithFormat:@"%@=%@", pKey, value]];
		// append this parameters
		postParameters = [postParameters stringByAppendingString:parameter];
		// release the value (CFURLCreateStringByAddingPercentEscapes added a retain)
		[value release];
	}
	
	return postParameters;
}

- (void)invokeFormEncoded:(NSDictionary *)parameters streamLocation:(NSString *)streamLocation
{
	if([method isEqualToString:@"POST"])
	{
		// create an empty POST, to be filled with our post parameters
		NSMutableData *postData = [NSMutableData dataWithLength:0];
		NSString *parametersStr=nil;
		// get the parameters to post
		if([contentType isEqualToString:@"application/json"])
		{
			//Create JSON Data
			NSString *jsonString = [(NSObject *)parameters JSONRepresentation];
			[postData appendData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
			
		}
		else  if([contentType isEqualToString:@"application/x-www-form-urlencoded"])
		{
			//Create JSON Data
			parametersStr = [self encodeParameters:parameters excludeLarge:NO];
			// add them to the post data
			//self.url=[NSString stringWithFormat:@"%@?%@",self.url,parametersString];
			//[postData appendData:[parametersString dataUsingEncoding:NSUTF8StringEncoding]];
			postData=nil;
			//append here stream binary
		}
		else
		{
			NSString *parametersString = [self encodeParameters:parameters excludeLarge:NO];
			// add them to the post data
			[postData appendData:[parametersString dataUsingEncoding:NSUTF8StringEncoding]];
		}
		[self invoke:parametersStr content:postData streamLocation:streamLocation];
	}
	else if([method isEqualToString:@"GET"])
	{
		// create query string from get parameters.
		NSString *queryString = [self encodeParameters:parameters excludeLarge:NO];
		[self invoke:queryString content:nil streamLocation:streamLocation];
	}
}

#pragma mark multi-part
- (NSString *)generateBoundary
{
	// the boundary always starts with 10 dashes to make readability easier
	NSString *boundary = @"----------";
	
	// now we generate a probabalistic boundary string.  we choose a random 30 character string,
	// each position made from the numbers 0-9, a-z or A-Z (62 possibilities).  the chances of this
	// appearing in any of the posted text are 1 in 62^30 or about 1 in 1x10^53 (astronomically
	// small)
	for (int i = 0; i < 30; i++)
	{
		char c;
		// decide if we're going to do a number, capital or lower case letter randomly
		int r = rand();
		if (r % 3 == 0)
		{ // '0' to '9'
			c = (r % 10) + '0';
		}
		else if (r % 3 == 1)
		{ // 'A' to 'Z'
			c = (r % 26) + 'A';
		}
		else
		{ // 'a' to 'z'
			c = (r % 26) + 'a';
		}
		boundary = [boundary stringByAppendingFormat:@"%c", c];
	}
	return boundary;
}

- (void)invokeMultiPartEncoded:(NSDictionary *)parameters streamLocation:(NSString *)streamLocation
{
	// create an empty POST, to be filled with our post parameters
	NSMutableData *postData = [NSMutableData dataWithLength:0];
	
	// generate a new boundary token for this request
	NSString *boundary = [self generateBoundary];
	
	// loop over each parameter we're sending
	for (NSString *pKey in [parameters allKeys])
	{
		// get the value out
		id value = [parameters objectForKey:pKey];
		
		// every parameter we output the boundary
		[postData appendData:[@"\r\n--" dataUsingEncoding:NSASCIIStringEncoding]];
		[postData appendData:[boundary dataUsingEncoding:NSASCIIStringEncoding]];
		[postData appendData:[@"\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
		
		// every parameter we need to generate a content-disposition
		NSString *disposition = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"", pKey];
		
		// if we're posting a file we also need to add the filename to the disposition
		if ([value isKindOfClass:[NSURL class]])
		{
			// parse out the file name from the URL
			NSString *fileName = [[value relativeString] lastPathComponent];
			
			// and add it to the disposion
			disposition = [disposition stringByAppendingFormat:@"; filename=\"%@\"", fileName];
		}
		
		// append the CRLF to the disposition
		disposition = [disposition stringByAppendingString:@"\r\n"];
		
		// send out whatever the disposition is
		[postData appendData:[disposition dataUsingEncoding:NSASCIIStringEncoding]];
		
		// check to see what kind of value we're posting (normal string or file)
		if ([value isKindOfClass:[NSURL class]])
		{
			// in the case of a file, we tell the server we're dealing with a binary octet
			[postData appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
			
			// now load the file's data into the output stream.  this isn't the most ideal way of
			// doing this.  what we REALLY should do is have some method of streaming the file out,
			// but for now hopefully this will do for most of our files (it should be fine for files
			// into the low-MB range, say around 10MB which i think is the majority).
			
			[postData appendData:[NSData dataWithContentsOfURL:value]];
			
			
		}
		else
		{
			// add one final CRLF before the value
			[postData appendData:[@"\r\n" dataUsingEncoding:NSASCIIStringEncoding]];
			// then we can write out the value directly -- ne need for encoding
			[postData appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
		}
		// add a final CRLF to the value
		[postData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	}
	
	// finalize the post with the boundary again
	NSString *lastBoundary = [NSString stringWithFormat:@"--%@--", boundary];
	[postData appendData:[lastBoundary dataUsingEncoding:NSASCIIStringEncoding]];
	// create a query string
	NSString *queryString = [self encodeParameters:parameters excludeLarge:YES];
	// create the content-type header to send
	contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
	// invoke the server
	[self invoke:queryString content:postData streamLocation:streamLocation];
}

#pragma mark CoreLFormURLConnection

- (void)invoke:(NSDictionary *)parameters streamLocation:(NSString *)streamLocation
{
	if ([self checkForMultipart:parameters])
	{
		[self invokeMultiPartEncoded:parameters streamLocation:streamLocation];
	}
	else
	{
		[self invokeFormEncoded:parameters streamLocation:streamLocation];
	}
}

- (NSData *)invokeSync:(NSDictionary *)parameters
{
	NSData *syncResponseData = nil;
	if([method isEqualToString:@"POST"])
	{
		// create an empty POST, to be filled with our post parameters
		NSMutableData *postData = [NSMutableData dataWithLength:0];
		// get the parameters to post
		if([contentType isEqualToString:@"application/json"])
		{
			//Create JSON Data
			NSString *jsonString = [(NSObject *)parameters JSONRepresentation];
			[postData appendData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
		}
		else
		{
			NSString *parametersString = [self encodeParameters:parameters excludeLarge:NO];
			// add them to the post data
			[postData appendData:[parametersString dataUsingEncoding:NSUTF8StringEncoding]];
		}
		syncResponseData = [self invokeSync:nil content:postData];
	}
	else if([method isEqualToString:@"GET"])
	{
		// create query string from get parameters.
		NSString *queryString = [self encodeParameters:parameters excludeLarge:NO];
		syncResponseData = [self invokeSync:queryString content:nil];
	}
	return syncResponseData;
}

@end
