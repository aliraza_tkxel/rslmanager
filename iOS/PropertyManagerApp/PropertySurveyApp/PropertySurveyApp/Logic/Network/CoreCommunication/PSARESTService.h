/*    Copyright (c) 2013 Tkxel. All rights reserved. */


#import "CoreRESTService.h"
//#import "ApplicationConstants.h"

@interface PSARESTService : CoreRESTService {
  
    NSString *serviceUrl;
}

@property (nonatomic, retain) NSString *serviceUrl;

- (id)initWithServiceUrl:(NSString *)_serviceUrl;
 
@end
