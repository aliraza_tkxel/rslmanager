//
//  PSReachibilityManager.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
@interface PSReachibilityManager : NSObject
@property (nonatomic, strong) Reachability *reachabilityInstance;
+(PSReachibilityManager*) sharedManager;
-(BOOL) isConnectedToInternet;
-(void) startMonitoringNetworkChanges;
-(void) stopMonitoringNetworkChanges;
@end
