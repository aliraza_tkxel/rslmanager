//
//  PSReachibilityManager.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/23/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PSReachibilityManager.h"

@implementation PSReachibilityManager
#pragma mark - Singleton method
static PSReachibilityManager *sharedReachabilityManager = nil;
+(PSReachibilityManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedReachabilityManager = [[PSReachibilityManager alloc] init];
        sharedReachabilityManager.reachabilityInstance = [Reachability reachabilityWithHostname:@"www.google.com"];
        
    });
    return sharedReachabilityManager;
}

-(BOOL) isConnectedToInternet{
    return _reachabilityInstance.isReachable;
}

-(void) startMonitoringNetworkChanges{
    [_reachabilityInstance startNotifier];
}

-(void) stopMonitoringNetworkChanges{
    [_reachabilityInstance stopNotifier];
}

@end
