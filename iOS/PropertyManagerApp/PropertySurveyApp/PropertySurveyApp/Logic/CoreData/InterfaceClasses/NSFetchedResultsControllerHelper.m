//
//  NSFetchedResultsControllerHelper.m
//  TkXel_iOS
//
//  Created by Ansari on 3/28/13.
//
//

#import "NSFetchedResultsControllerHelper.h"

@implementation NSFetchedResultsControllerHelper

#pragma mark - Public Methods -


+ (NSSortDescriptor *) getSortDescriptersForOption:(PSFilterOption)filterOption
{
	NSSortDescriptor *sortDescriptor = nil;
	if(filterOption == PSFilterOptionSortByName)
	{
		sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:kAppointmentTitle ascending:YES selector:@selector(caseInsensitiveCompare:)];
	}
	else if(filterOption == PSFilterOptionSortByDate)
	{
		sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:kAppointmentDate ascending:YES];
	}
	return sortDescriptor;
}

+ (void) filterAppointmentsWithOptions:(NSArray *)filterOptions controller:(NSFetchedResultsController *)_fetchedResultsController
{
	STARTEXCEPTION
	NSPredicate *defaultPredicate = [NSPredicate predicateWithFormat:@"(SELF.appointmentToUser.userName LIKE[cd] SELF.surveyorUserName) AND (appointmentToUser.userId == %@)", [SettingsClass sharedObject].loggedInUser.userId];
	if(!isEmpty(filterOptions))
	{
		NSMutableArray *sortDescriptors = [NSMutableArray array];
		NSMutableArray *viewPredicates = [NSMutableArray array];
		NSMutableArray *typePredicates = [NSMutableArray array];
		NSMutableArray *statusPredicates = [NSMutableArray array];
		
		for(NSNumber *filterOption in filterOptions)
		{
			switch ([filterOption integerValue])
			{
				case PSFilterOptionViewAll:
				{
					//[viewPredicates addObject:[NSPredicate predicateWithValue:YES]];
				}
					break;
				case PSFilterOptionViewToday:
				{
					NSPredicate *firstPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ > %@", kAppointmentStartDateTime, [[NSDate date] dateAtStartOfDay]]; //12:00:00 am current date
					NSPredicate *secondPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ < %@", kAppointmentStartDateTime, [[[[NSDate date] dateByAddingDays:1] dateAtStartOfDay] dateByAddingTimeInterval:-1]]; //11:59:59 pm current date
					NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[firstPredicate, secondPredicate]];
					CLS_LOG(@"%@",predicate);
					[viewPredicates addObject:predicate];
				}
					break;
				case PSFilterOptionViewNextFiveDays:
				{
					NSPredicate *firstPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ > %@", kAppointmentStartDateTime, [[NSDate date] dateAtStartOfDay]]; //12:00:00 am current date
					NSPredicate *secondPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ < %@", kAppointmentStartDateTime, [[[NSDate date] dateByAddingDays:5] dateAtStartOfDay]]; //11:59:59 PM 5th day next to today
					NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[firstPredicate, secondPredicate]];
					[viewPredicates addObject:predicate];
				}
					break;
				case PSFilterOptionSortByName:
				{
					NSSortDescriptor *dateSortDescriptor = [NSFetchedResultsControllerHelper getSortDescriptersForOption:PSFilterOptionSortByDate];
					NSSortDescriptor *nameSortDescriptor = [NSFetchedResultsControllerHelper getSortDescriptersForOption:PSFilterOptionSortByName];
					if(dateSortDescriptor && nameSortDescriptor)
					{
						[sortDescriptors addObjectsFromArray:@[dateSortDescriptor, nameSortDescriptor]];
					}
				}
					break;
				case PSFilterOptionSortByDate:
				{
					NSSortDescriptor *sortDescriptor = [NSFetchedResultsControllerHelper getSortDescriptersForOption:PSFilterOptionSortByDate];
					if(sortDescriptor)
					{
						[sortDescriptors addObject:sortDescriptor];
					}
				}
					break;
				case PSFilterOptionShowOnlyGas:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeGas];
					[typePredicates addObject:predicate];
                    //Added Gas Void appointment to Show only gas type
                    predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeGasVoid];
                    [typePredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyVoid:
				{
					NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypePreVoid];
					[typePredicates addObject:predicate];
					predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypePostVoid];
					[typePredicates addObject:predicate];
					predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeGasCheck];
					[typePredicates addObject:predicate];
					predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeElectricCheck];
					[typePredicates addObject:predicate];
					predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeVoidWorkRequired];
					[typePredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyFault:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeFault];
					[typePredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyPlanned:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypePlanned];
					[typePredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyAdaptations:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeAdaptations];
					[typePredicates addObject:predicate];
				}
					break;
                
				case PSFilterOptionShowOnlyMiscellaneous:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeMiscellaneous];
					[typePredicates addObject:predicate];
				}
					break;
					case PSFilterOptionShowOnlyDefect:
					{
						NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeDefect];
						[typePredicates addObject:predicate];
					}
						break;
                case PSFilterOptionShowOnlyOil:
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeOil];
                    [typePredicates addObject:predicate];
                }
                    break;
                case PSFilterOptionShowOnlyAlternative:
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeAlternativeFuel];
                    [typePredicates addObject:predicate];
                }
                    break;
				case PSFilterOptionShowOnlyConditionRatingTool:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentType, kAppointmentTypeCondition];
					[typePredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyInProgress:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusInProgress];
					[statusPredicates addObject:predicate];
				}
					break;
				case PSFilterOptionShowOnlyArranged:
				{
					NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusNotStarted];
					[statusPredicates addObject:predicate];
                    predicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusAccepted];
                    [statusPredicates addObject:predicate];
				}
					break;
				default:
					break;
			}
		}
		
			NSPredicate *dontShowStatus = [NSPredicate predicateWithFormat:@"(NOT (SELF.%@ LIKE %@) AND NOT (SELF.%@ LIKE %@) AND NOT (SELF.%@ LIKE %@))",
																		 kAppointmentStatus, kAppointmentStatusComplete,
																		 kAppointmentStatus, kAppointmentStatusNoEntry,
																		 kAppointmentStatus, kAppointmentStatusFinished];
			[_fetchedResultsController.fetchRequest setPredicate:dontShowStatus];
		if(!isEmpty(sortDescriptors))
		{
			[_fetchedResultsController.fetchRequest setSortDescriptors:sortDescriptors];
		}
		
		NSCompoundPredicate *predicate = nil;
		NSCompoundPredicate *viewPredicate = nil;
		NSCompoundPredicate *typePredicate = nil;
		NSCompoundPredicate *statusPredicate = nil;
		
		NSMutableArray *predicates = [NSMutableArray array];
		BOOL isViewPredicateEmpty = isEmpty(viewPredicates);
		BOOL isTypePredicateEmpty = isEmpty(typePredicates);
		BOOL isStatusPredicateEmpty = isEmpty(statusPredicates);
		
		NSPredicate *inProgressPredicate = nil;
		NSPredicate *notStartedPredicate = nil;
		
		if(!isStatusPredicateEmpty)
		{
			statusPredicate = (NSCompoundPredicate *)[NSCompoundPredicate orPredicateWithSubpredicates:statusPredicates];
			[predicates addObject:statusPredicate];
		}
		else
		{ // if no status is selected than show InProgress and Arranged
			NSPredicate *inProgressPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusInProgress];
			NSPredicate *notStartedPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusNotStarted];
            NSPredicate *acceptedPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusAccepted];
			//warning adding this abort predicate to show the abort appointments for development purpose. will be deleted after wards.
            //NSPredicate *abortedPredicate = [NSPredicate predicateWithFormat:@"SELF.%@ LIKE[cd] %@", kAppointmentStatus, kAppointmentStatusAborted];
			//[statusPredicates addObject:abortedPredicate];
			[statusPredicates addObject:inProgressPredicate];
			[statusPredicates addObject:notStartedPredicate];
            [statusPredicates addObject:acceptedPredicate];
			statusPredicate = (NSCompoundPredicate *)[NSCompoundPredicate orPredicateWithSubpredicates:statusPredicates];
			[predicates addObject:statusPredicate];
		}
		
		if(!isViewPredicateEmpty)
		{
			viewPredicate = (NSCompoundPredicate *)[NSCompoundPredicate orPredicateWithSubpredicates:viewPredicates];
			[predicates addObject:viewPredicate];
		}
		
		if(!isTypePredicateEmpty)
		{
			typePredicate = (NSCompoundPredicate *)[NSCompoundPredicate orPredicateWithSubpredicates:typePredicates];
			[predicates addObject:typePredicate];
		}
		predicate = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:predicates];
		predicate = (NSCompoundPredicate *)[NSCompoundPredicate andPredicateWithSubpredicates:@[defaultPredicate, predicate]];
		
		[_fetchedResultsController.fetchRequest setPredicate:predicate];
		
		NSError *error = nil;
		if (![_fetchedResultsController performFetch:&error])
		{
			// Handle error
			CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
	else
	{
		NSPredicate *defaultPredicate = [NSPredicate predicateWithFormat:@"(SELF.appointmentStatus LIKE[cd] 'NotStarted' OR SELF.appointmentStatus LIKE[cd] 'InProgress'  OR SELF.appointmentStatus LIKE[cd] 'Accepted') AND (SELF.appointmentToUser.userName LIKE[cd] SELF.surveyorUserName) AND (appointmentToUser.userId == %@)", [SettingsClass sharedObject].loggedInUser.userId];
		NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
		[_fetchedResultsController.fetchRequest setPredicate:defaultPredicate];
		NSError *error = nil;
		if (![_fetchedResultsController performFetch:&error])
		{
			// Handle error
			CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
		}
	}
	ENDEXCEPTION
}

@end
