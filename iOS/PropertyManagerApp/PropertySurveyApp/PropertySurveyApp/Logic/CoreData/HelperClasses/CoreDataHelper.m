//
//  Created by Björn Sållarp on 2009-06-14.
//  NO Copyright 2009 MightyLittle Industries. NO rights reserved.
// 
//  Use this code any way you like. If you do like it, please
//  link to my blog and/or write a friendly comment. Thank you!
//
//  Read my blog @ http://blog.sallarp.com
//

#import "CoreDataHelper.h"
#import <Foundation/Foundation.h>

@implementation CoreDataHelper

+(NSArray *) getObjectsFromEntity:(NSString*)entityName
													predicate:(NSPredicate *)predicate
														sortKey:(NSString*)sortKey
											sortAscending:(BOOL)sortAscending
										 context:(NSManagedObjectContext *)dbContext
{
	__block NSArray *fetchResults = nil;
	STARTEXCEPTION
	@autoreleasepool
	{
		[dbContext performBlockAndWait:^{
			NSFetchRequest *request = [[NSFetchRequest alloc] init];
			[request setReturnsObjectsAsFaults:NO];
			[request setIncludesPropertyValues:YES];
			
			
			NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
																								inManagedObjectContext:dbContext];
			[request setEntity:entity];
			
			// If a predicate was passed, pass it to the query
			if(predicate != nil)
			{
					[request setPredicate:predicate];
			}
			
			// If a sort key was passed, use it for sorting.
			if(sortKey != nil)
			{
					NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey
																																				 ascending:sortAscending];
					NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
					[request setSortDescriptors:sortDescriptors];
			}
			
			NSError *error;
			fetchResults = [dbContext executeFetchRequest:request error:&error];
		}];
	}
	ENDEXCEPTION
	return fetchResults;
}


+(NSArray *) getObjectsFromEntity:(NSString*)entityName
													 sortKey:(NSString*)sortKey
										 sortAscending:(BOOL)sortAscending
													 context:(NSManagedObjectContext *)dbContext
{
    return [self getObjectsFromEntity:entityName
														 predicate:nil
															 sortKey:sortKey
												 sortAscending:sortAscending
												context:dbContext];
}

+ (void)deleteAllObjects:(NSString *)entityName
								 context:(NSManagedObjectContext *)dbContext
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
	[fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
	
	NSError *error;
	NSArray *fetchedObjects = [dbContext executeFetchRequest:fetchRequest error:&error];
	for (NSManagedObject *object in fetchedObjects)
	{
		[dbContext deleteObject:object];
	}
	
	error = nil;
	[dbContext save:&error];
	CLS_LOG(@"delete all Objects: Error = %@", error);
}
@end
