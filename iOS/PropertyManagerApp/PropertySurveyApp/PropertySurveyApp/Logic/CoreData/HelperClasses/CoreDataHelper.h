//
//  Created by Björn Sållarp on 2009-06-14.
//  NO Copyright 2009 MightyLittle Industries. NO rights reserved.
// 
//  Use this code any way you like. If you do like it, please
//  link to my blog and/or write a friendly comment. Thank you!
//
//  Read my blog @ http://blog.sallarp.com
//
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CoreDataHelper : NSObject

+(NSArray *) getObjectsFromEntity:(NSString *)entityName
												 predicate:(NSPredicate *)predicate
													 sortKey:(NSString*)sortKey
										 sortAscending:(BOOL)sortAscending
										context:(NSManagedObjectContext *)managedObjectContext;

+(NSArray *) getObjectsFromEntity:(NSString *)entityName
													 sortKey:(NSString*)sortKey
										 sortAscending:(BOOL)sortAscending
													 context:(NSManagedObjectContext *)managedObjectContext;

+ (void)deleteAllObjects:(NSString *)entityName
								 context:(NSManagedObjectContext *)dbContext;

@end
