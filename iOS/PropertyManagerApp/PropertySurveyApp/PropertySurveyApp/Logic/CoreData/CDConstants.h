//
//  CDConstants.h
//  VLogic
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

/*!
 @discussion
 Void Tenancy StoryBoard names
 */
#define VT_SB_PreInspection          @"PreInspection"
#define VT_SB_WorksRequired          @"WorksRequired"
#define VT_SB_PostInspection         @"PostInspection"
#define VT_SB_GasElectricInspection  @"GasElectricInspection"
#define VT_SB_AddRequiredWork        @"AddRequiredWork"

#define AFS_SB_MAIN                     @"AlternativeFuelServicingStoryboard"
#define OIL_SERVICING_SB_MAIN   @"OilServicingStoryboard"

/*!
 @discussion
 Repair Module StoryBoard names
 */
#define RM_SB_AddFaultProcess        @"AddFaultProcess"

/*!
 @discussion
 JSON type property
 */

#define kJsonTypeDefect              @"ApplianceDefectAppointmentData:#PropSurvey.Contracts.Data"

/*!
 @discussion
 Defect StoryBoard names
 */
#define D_SB_Defect          @"DefectAppointment"

/*!
 @discussion
 Unique identifier that will represent an image saving.
 */
#define kUniqueImageIdentifier @"imageIdentifier"

#pragma mark - Core Data Store Constants

/*!
 @discussion
 XCDataModel File Name.
 */
#define kXCDataModelFile @"PSAOfflineStock"

/*!
 @discussion
 XCDataModel type. 
 momd describes as versioned Model.
 */
#define kXCDataModelType @"momd"

/*!
 @discussion
 Persistence Data Store Type.
 */
#define kXCDataStoreType @"sqlite"

/*!
 @discussion
 Persistence Data File Name with extension.
 */
#define kXCDataSQLiteFile [NSString stringWithFormat:@"%@.sqlite", kXCDataModelFile]





#pragma mark - Core Data Entities

/*!
 @discussion
 Accomodation Entity Name.
 */
#define kAccomodation @"Accomodation"

/*!
 @discussion
 Address Entity Name.
 */
#define kAddress @"Address"

/*!
 @discussion
 AppInfoData Entity Name.
 */
#define kAppInfoData @"AppInfoData"

/*!
 @discussion
 Appointment Entity Name.
 */
#define kAppointment @"Appointment"

/*!
 @discussion
 CP12Info Entity Name.
 */
#define kCP12Info @"CP12Info"

/*!
 @discussion
 Customer Entity Name.
 */
#define kCustomer @"Customer"

/*!
 @discussion
 CustomerRiskData Entity Name.
 */
#define kCustomerRiskData @"CustomerRiskData"

/*!
 @discussion
 CustomerVulnerabilityData Entity Name.
 */
#define kCustomerVulnerabilityData @"CustomerVulnerabilityData"

/*!
 @discussion
 FaultRepairData Entity Name.
 */
#define kFaultRepairData @"FaultRepairData"

/*!
 @discussion
 FaultRepairListHistory Entity Name.
 */
#define kFaultRepairListHistory @"FaultRepairListHistory"

/*!
 @discussion
 FormData Entity Name.
 */
#define kFormData @"FormData"

/*!
 @discussion
 JobDataList Entity Name.
 */
#define kTradeComponent @"PlannedTradeComponent"

/*!
 @discussion
 PropertyAttributesNotes Entity Name.
 */
#define kPropertyAttributesNotes @"PropertyAttributesNotes"


/*!
 @discussion
 Journal Entity Name.
 */
#define kJournal @"Journal"

/*!
 @discussion
 Outbox Entity Name.
 */
#define kOutbox @"Outbox"

/*!
 @discussion
 Property Entity Name.
 */
#define kProperty @"Property"

/*!
 @discussion
 Property Entity Name.
 */
#define kScheme @"Scheme"

/*!
 @discussion
 PropertyAsbestosData Entity Name.
 */
#define kPropertyAsbestosData @"PropertyAsbestosData"

/*!
 @discussion
 SchemeAsbestosData Entity Name.
 */
#define kSchemeAsbestosData @"SchemeAsbestosData"

/*!
 @discussion
 PropertyPicture Entity Name.
 */
#define kPropertyPicture @"PropertyPicture"
#define kAlternativeFuelPicture @"AlternativeFuelPicture"
/*!
 @discussion
 SurveyData Entity Name.
 */
#define kSurveyData @"SurveyData"

/*!
 @discussion
 User Entity Name.
 */
#define kUser @"User"

/*!
 @discussion
 Surveyor Entity Name.
 */
#define kSurveyor @"Surveyor"

/*!
 @discussion
 SProperty Entity Name.
 */
#define kSProperty @"SProperty"

/*!
 @discussion
 Survey Entity Name.
 */
#define kSurvey @"survey"

/*!
 @discussion
 RoomData Entity Name.
 */
#define kRoomData @"RoomData"

/*!
 @discussion
 PauseReason Entity Name.
 */
#define kPauseReason @"PauseReason"

/*!
 @discussion
 Appliance Entity Name.
 */
#define kAppliance @"Appliance"

/*!
 @discussion
 Boiler Entity Name.
 */
#define kBoiler @"Boiler"
#define kAlternativeHeating @"AlternativeHeating"

/*!
 @discussion
 BoilerManufacturer Entity Name.
 */
#define kBoilerManufacturer @"BoilerManufacturer"
#define kSolarType          @"SolarType"
#define kBurnerTypeEntity         @"BurnerType"

/*!
 @discussion
 BoilerModel Entity Name.
 */
#define kBoilerModel @"BoilerModel"

/*!
 @discussion
 BoilerType Entity Name.
 */
#define kBoilerType @"BoilerType"

/*!
 @discussion
 FlueType Entity Name.
 */
#define kFlueType @"FlueType"
#define kOilFlueTypeEntity  @"OilFlueType"
#define kOilFuelTypeEntity  @"OilFuelType"
#define kTankTypeEntity @"TankType"

/*!
 @discussion
 AbortReason Entity Name.
 */
#define kAbortReasonType @"AbortReason"


/*!
 @discussion
 ApplianceLocation Entity Name.
 */
#define kApplianceLocation @"ApplianceLocation"

/*!
 @discussion
 ApplianceManufacturer  Entity Name.
 */
#define kApplianceManufacturer @"ApplianceManufacturer"

/*!
 @discussion
 ApplianceModel Entity Name.
 */
#define kApplianceModel @"ApplianceModel"

/*!
 @discussion
 ApplianceType Entity Name.
 */
#define kApplianceType @"ApplianceType"

/*!
 @discussion
 ApplianceType Entity Name.
 */
#define kDetectorType @"DetectorType"

/*!
 @discussion
 ApplianceInspection Entity Name.
 */
#define kApplianceInspection @"ApplianceInspection"

/*!
 @discussion
 BoilerInspection Entity Name.
 */
#define kBoilerInspection @"BoilerInspection"

/*!
 @discussion
 kApplianceDefects Entity Name.
 */
#define kBoilerDefects @"BoilerDefects"

/*!
 @discussion
 kApplianceDefects Entity Name.
 */
#define kApplianceDefects @"ApplianceDefects"


/*!
 @discussion
 Property Pipework Installation Entity Name.
 */
#define kInstallationPipework @"InstallationPipework"

/*!
 @discussion
 Appliance Defect Entity Name.
 */
#define kDefect @"Defect"

/*!
 @discussion
 Appliance DefectPicture Entity Name.
 */
#define kDefectPicture @"DefectPicture"

/*!
 @discussion
 Defect Category Entity Name.
 */
#define kDefectCategory @"DefectCategory"

/*!
 @discussion
 Defect Category Entity Name.
 */
#define kDefectPriority @"DefectPriority"

/*!
 @discussion
 Defect Trade Entity Name.
 */
#define kTrade @"Trade"


/*!
 @discussion
 Pause Reason Entity Name.
 */
#define kJobPauseReason @"JobPauseReason"

/*!
 @discussion
Job Pause Data Entity Name.
 */
#define kJobPauseData @"JobPauseData"

/*!
 @discussion
 Job Detector Entity Name.
 */
#define kDetector     @"Detector"
  //#define kDetectorId   @"ID"

/*!
 @discussion
 Job Meter Entity Name.
 */
#define kMeter     @"Meter"

#define kAppointmentHistory     @"AppointmentHistory"

/*!
 @discussion
 Power Type Entity Name.
 */
#define kPowerType    @"PowerType"

/*!
 @discussion
 Job Detector Inspection Entity Name.
 */
#define kDetectorInspection @"DetectorInspection"

/*!
 @discussion
 Repair pictures Entity Name.
 */
#define kRepairPictures @"RepairPictures"

#pragma mark - Property Address Styles
/*!
 @discussion
 Property Address Styles
 */
typedef NS_ENUM(NSInteger, PropertyAddressStyle)
{
    PropertyAddressStyleShort,
    PropertyAddressStyleLong,
    PropertyAddressStyleFull,
    PropertyAddressStyle1,
    PropertyAddressStyleAddress2Onwards
};


#pragma mark - Appointment Date Style
/*!
 @discussion
 Appointment Date Style. 
 @result
 Appointment String Date formed using appointment start time and end time, or appointmentData in case of long style.
 */
typedef NS_ENUM(NSInteger, AppointmentDateStyle)
{
    AppointmentDateStyleStartTime,
    AppointmentDateStyleEndTime,
    AppointmentDateStyleStartToEnd,
    AppointmentDateStyleComplete,
};

/*!
 @discussion
 AppointmentStatus represents the appointment's completion status.
 */
typedef NS_ENUM(NSInteger, AppointmentStatus) {
    AppointmentStatusPaused,
    AppointmentStatusInProgress,
    AppointmentStatusComplete,
    AppointmentStatusNotStarted,
    AppointmentStatusFinished,
    AppointmentStatusNoEntry,
    AppointmentStatusAbort,
    AppointmentStatusAccepted,
};


/*!
 @discussion
 AppointmentType represents the appointment's type like Stock, Gas, Fault.
 */
typedef NS_ENUM(NSInteger, AppointmentType)
{
	AppointmentTypeGas,
	AppointmentTypePreVoid,
	AppointmentTypePostVoid,
	AppointmentTypeVoidWorkRequired,
	AppointmentTypeGasCheck,
	AppointmentTypeElectricCheck,
	AppointmentTypeFault,
	AppointmentTypePlanned,
	AppointmentTypeAdaptations,
	AppointmentTypeMiscellaneous,
	AppointmentTypeCondition,
    AppointmentTypeDefect,
    AppointmentTypeGasVoid,
    AppointmentTypeAlternativeFuels,
    AppointmentTypeOilServicing,
	AppointmentTypeNone
};

/*!
 Appointment sync status*/
typedef NS_ENUM(NSInteger, AppointmentSyncStatus)
{
    AppointmentNotSynced,
    AppointmentDefectSynced,
    AppointmentImageSynced,
    AppointmentSynced,
};

/*!
 @discussion
 JobStatus represents the job completion status.
 */
typedef NS_ENUM(NSInteger, JobStatus) {
    JobStatusPaused,
    JobStatusInProgress,
    JobStatusComplete,
    JobStatusNotStarted,
    JobStatusFinished,
    JobStatusNoEntry,
};

#pragma mark - MeterType Entity Attributes
/*!
 @discussion
 MeterType Entity Attributes.
 */
#define kMeterId                @"meterId"
#define kMeterType              @"meterType"
#define kDeviceType             @"deviceType"
#define kDeviceTypeId           @"deviceTypeId"

#pragma mark - Meter Entity Attributes
/*!
 @discussion
 Meter Entity Attributes.
 */
#define kMeterInspectedBy        @"inspectedBy"
#define kMeterInspectionDate     @"inspectionDate"
#define kMeterInstalledDate      @"installedDate"
#define kMeterIsCapped           @"isCapped"
#define kMeterIsInspected        @"isInspected"
#define kMeterIsPassed           @"isPassed"
#define kMeterLastReading        @"lastReading"
#define kMeterLastReadingDate    @"lastReadingDate"
#define kMeterLocation           @"location"
#define kMeterManufacturer       @"manufacturer"
#define kMeterTypeId             @"meterTypeId"
#define kMeterNotes              @"notes"
#define kMeterPropertyId         @"propertyId"
#define kMeterReading            @"reading"
#define kMeterReadingDate        @"readingDate"
#define kMeterSerialNumber       @"serialNumber"

#define kMeterObjectId           @"meterObjectId"
#define kMeterRequestKey          @"Meters"

#define kCoreDateObjectId       @"objectID"

#pragma mark - PauseReason Entity Attributes
/*!
 @discussion
 PauseReason Entity Attributes.
 */
#define kPauseId                @"pauseId"


#pragma mark - MeterType Entity Attribute Values
/*!
 @discussion
 MeterType Entity Attributes.
 */
#define kGasDeviceType          @"Gas"
#define kElectricDeviceType     @"Electric"

#pragma mark - RoomData Entity Attributes
/*!
 @discussion
 RoomData Entity Attributes.
 */
#define kRoomId                 @"roomId"

#pragma mark - Employee Entity Attributes
/*!
 @discussion
 Employee Entity Attributes.
 */
#define kEmployee                   @"Employee"
#define kEmployeeId                 @"employeeId"
#define kEmployeeName               @"employeeName"

#pragma mark - Appointment History Entity Attributes

#define kAppointmentHistoryActionBy       @"actionBy"
#define kAppointmentHistoryActionType     @"actionType"
#define kAppointmentHistoryActionDate     @"actionDate"
#define kAppointmentHistoryJSONEntry      @"appointmentHistory"

#pragma mark - Appointment Entity Attributes
/*!
 @discussion
 Appointment Entity Attributes.
 */


#define kAppointmentAbortReason             @"appointmentAbortReason"
#define kAppointmentAbortNotes              @"appointmentAbortNotes"
#define kAppointmentActualStartTime         @"appointmentActualStartTime"
#define kAppointmentActualEndTime           @"appointmentActualEndTime"
#define kAppointmentCalendar                @"appointmentCalendar"
#define kAppointmentDate                    @"appointmentDate"
#define kAppointmentStartTime               @"appointmentStartDateTime"
#define kAppointmentEndTime                 @"appointmentEndDateTime"
#define kAppointmentStartTimeString         @"appointmentStartTimeString"
#define kAppointmentEndTimeString           @"appointmentEndTimeString"
#define kAppointmentId                      @"appointmentId"
#define kAppointmentLocation                @"appointmentLocation"
#define kAppointmentNotes                   @"appointmentNotes"
#define kAppointmentOverdue                 @"appointmentOverdue"
#define kAppointmentShift                   @"appointmentShift"
#define kAppointmentStatus                  @"appointmentStatus"
#define kAppointmentTitle                   @"appointmentTitle"
#define kAppointmentType                    @"appointmentType"
#define kAssignedTo                         @"assignedTo"
#define kCreatedBy                          @"createdBy"
#define kDefaultCustomerId                  @"defaultCustomerId"
#define kDefaultCustomerIndex               @"defaultCustomerIndex"
#define kJournalHistoryId                   @"journalHistoryId"
#define kJournalId                          @"journalId"
#define kJsgNumber                          @"jsgNumber"
#define kLoggedDate                         @"loggedDate"
#define kNoEntryNotes                       @"noEntryNotes"
#define kRepairCompletionDateTime           @"repairCompletionDateTime"
#define kRepairNotes                        @"repairNotes"
#define kSurveyFormJSON                     @"surveyFormJSON"
#define kSurveyorAlert                      @"surveyorAlert"
#define kSurveyorUserName                   @"surveyorUserName"
#define kSurveyourAvailability              @"surveyourAvailability"
#define kSurveyType                         @"surveyType"
#define kTenancyId                          @"tenancyId"
#define kAppointmentToCP12Info              @"cp12Info"
#define kAppointmentToAppInfoData           @"appInfoData"
#define kAppointmentToCustomer              @"customerList"
#define kAppointmentToJobDataList           @"jobDataList"
#define kAppointmentToComponentTrade        @"componentTrade"
#define kAppointmentToJournal               @"journal"
#define kAppointmentToProperty              @"property"
#define kAppointmentToScheme                @"scheme"
#define kAppointmentEventIdentifier         @"appointmentEventIdentifier"
#define kAppointmentDateSectionIdentifier   @"appointmentDateSectionIdentifier"
#define kAppointmentIsModified              @"isModified"
#define kAppointmentAddtoCalendar           @"addToCalendar"
#define kAppointmentStartDateTime           @"appointmentStartTime"
#define kCreatedByPerson                    @"createdByPerson"
#define kIsMiscAppointment                  @"isMiscAppointment"
#define kAppointmentFailedReason            @"failureReason"
#define kAppointmentToPropertyAttributeNotes @"propertyAttributeNotes"
#define kAppointmentCreationDate            @"creationDate"
#define kAptCompletedAppVersionInfo         @"appointmentCompletedAppVersion"
#define kAptCurrentAppVersionInfo           @"appointmentCurrentAppVersion"
#define kAppointmentSyncStatus              @"syncStatus"
#define kAppointmentHeatingFuel             @"heatingFuel"

#pragma mark - Void Data Attributes
#define kJsvNumber                          @"jsvNumber"
#define kReletData                          @"reletDate"
#define kTerminationDate                    @"terminationDate"
#define kIsWorksRequired                    @"isWorksRequired"
#define kIsGasCheckRequired                 @"isGasCheckRequired"
#define kIsEPCCheckRequired                 @"isEPCCheckRequired"
#define kIsElectricCheckRequired            @"isElectricCheckRequired"
#define kIsAsbestosCheckRequired            @"isAsbestosCheckRequired"

#pragma mark -

#pragma mark - User Entity Attributes
/*!
 @discussion
 User Entity Attributes.
 */
#define kFullName                   @"fullName"
#define kIsActive                   @"isActive"
#define kLastLoggedInDate           @"lastLoggedInDate"
#define kPassword                   @"password"
#define kSalt                       @"salt"
#define kUserId                     @"userId"
#define kUserName                   @"userName"

/*!
 @discussion
 Surveyor Entity Attributes. 
 (Other the "SurveyorType" All Entity Attributes are similar to User's Entity Attributes)
 */
#define kSurveyorType                       @"surveyorType"
#define kSurveyorNameSectionIdentifier      @"surveyorNameSectionIdentifier"
#define kSurveyorName                       @"fullName"

#pragma mark - JobSheet Entity Attributes
/*!
 @discussion
 JobSheet Entity Attributes.
 */
#define kIsLegionella                   @"isLegionella"
#define kDuration                       @"duration"
#define kDurationUnit                   @"durationUnit"
#define kFaultLogID                     @"faultLogID"
#define kFollowOnNotes                  @"followOnNotes"
#define kJobStatus                      @"jobStatus"
#define kJSNDescription                 @"JSNDescription"
#define kJSNLocation                    @"JSNLocation"
#define kJSNNotes                       @"JSNNotes"
#define kJSNumber                       @"JSNumber"
#define kJSRepairId                     @"repairId"
#define kJSType                         @"jsType"
#define kPriority                       @"priority"
#define kJobRepairNotes                 @"repairNotes"
#define kReportedDate                   @"reportedDate"
#define kResponseTime                   @"responseTime"
#define kJSCompletedAppVersionInfo      @"jsCompletedAppVersion"
#define kJSCurrentAppVersionInfo        @"jsCurrentAppVersion"
#define kJSActualStartTime              @"jsActualStartTime"
#define kJSActualEndTime                @"jsActualEndTime"


#pragma mark - Accomodation Entity Attributes
/*!
 @discussion
 Accomodation Entity Attributes.
 */
#define kRoomHeight                 @"roomHeight"
#define kRoomLength                 @"roomLength"
#define kRoomName                   @"roomName"
#define kRoomWidth                  @"roomWidth"
#define kUpdatedBy                  @"updatedBy"
#define kUpdatedOn                  @"updatedOn"

#pragma mark - Property Attribute Notes
/*!
 @discussion
 Property Attributes Notes.
 */
#define kAttributePath                 @"attributePath"
#define kAttributeNote                 @"attributeNotes"

#pragma mark - Tenant Entity Attributes
/*!
 @discussion
 Tenant(Customer) Entity Attributes.
 */
#define kCustomerId                 @"customerId"
#define kCustomerAddress            @"address"
#define kCustomerEmail              @"email"
#define kCustomerFirstName          @"firstName"
#define kCustomerLastName           @"lastName"
#define kCustomerMiddleName         @"middleName"
#define kCustomerMobile             @"mobile"
#define kCustomerFax                @"fax"
#define kCustomerProperty           @"property"
#define kCustomerTelephone          @"telephone"
#define kCustomerTitle              @"title"


#pragma mark - SProperty Entity Attributes
/*!
 @discussion
 SProperty Entity Attributes.
 */
#define kSPropertyNameSectionIdentifier     @"sPropertyNameSectionIdentifier"

#pragma mark - Property Entity Attributes
/*!
 @discussion
 Property Entity Attributes.
 */
#define kAddress1                   @"address1"
#define kAddress2                   @"address2"
#define kAddress3                   @"address3"
#define kCertificateExpiry          @"certificateExpiry"
#define kCounty                     @"county"
#define kFlatNumber                 @"flatNumber"
#define kHouseNumber                @"houseNumber"
#define kLastSurveyDate             @"lastSurveyDate"
#define kPostCode                   @"postCode"
#define kPropertyId                 @"propertyId"
#define kTenancyId                  @"tenancyId"
#define kTownCity                   @"townCity"

#pragma mark - Scheme Entity Attributes
/*!
 @discussion
 Scheme Entity Attributes.
 */
#define kSchemeBlockID      @"blockId"
#define kSchemeID           @"schemeId"
#define kSchemeName         @"schemeName"
#define kSchemeBlockName    @"blockName"
#define kSchemCityTown      @"towncity"
#define kSchemePostCode     @"postcode"
#define kSchemeCounty       @"county"

#pragma mark - PropertyPicture Entity Attributes
/*!
 @discussion
 PropertyPicture Entity Attributes.
 */
#define kDefectPictureId                @"defectPictureId"

#define kPropertyPictureId              @"propertyPictureId"
#define kItemId                         @"itemId"
#define kPropertyPicturePath            @"imagePath"
#define kPropertyPictureName            @"propertyPictureName"
#define kPropertyPictureIsDefault       @"isDefault"
#define kPropertyPicturesSynchStatus    @"synchStatus"
#define kPropertyImage                  @"propertyImage"
#define kFileExtension                  @"fileExt"
#define kIsDefault                      @"isDefault"

#pragma mark - Appliances Entity(ies) Attributes
/*!
 @discussion
 Appliance Entity Attributes.
 */
#define kApplianceId                       @"ApplianceID"
#define kApplianceFlueType                 @"FluType"
#define kApplianceInstalledDate            @"InstalledDate"
#define kApplianceReplacementDate          @"ReplacementDate"
#define kApplianceSerialNumber             @"SerialNumber"
#define kApplianceGCNumber                 @"GCNumber"
#define kApplianceIsLandlordAppliance      @"isLandlordAppliance"
#define kApplianceIsInspected              @"isInspected"
#define kApplianceIsActive                 @"isActive"

/*!
 @discussion
 Boiler Entity Attributes.
 */
#define kBoilerFlueTypeId                  @"flueTypeId"
#define kBoilerGcNumber                    @"gcNumber"
#define kBoilerIsLandlordAppliance         @"isLandlordAppliance"
#define kBoilerLastReplaced                @"lastReplaced"
#define kBoilerLocation                    @"location"
#define kBoilerManufacturerId              @"manufacturerId"
#define kBoilerModel                       @"model"
#define kBoilerPropertyId                  @"propertyId"
#define kBoilerReplacementDue              @"replacementDue"
#define kBoilerSerialNumber                @"serialNumber"
#define kBoilerTypeId                      @"boilerTypeId"
#define kBoilerIsInspected                 @"isInspected"
#define kBoilerGasketReplacementDate       @"gasketReplacementDate"
#define kHeatingId                        @"heatingId"
#define kBoilerName                         @"boilerName"


#define kLookupId                       @"lookupId"
#define kLookupValue                    @"value"

#define kHeatingFuel                        @"heatingFuel"
#define kHeatingType                        @"heatingType"
#define kHeatingName                        @"heatingName"
#define kInstalledDate                     @"installedDate"
#define kSolarTypeId                        @"solarTypeId"
#define kCertificateName                    @"certificateName"
#define kCertiticateIssued                   @"certificateIssued"
#define kCertificateNumber                    @"certificateNumber"
#define kCertificateRenewal                   @"certificateRenewal"
#define kMVHRInspectionForm         @"mvhrInspection"
#define kSolarInspectionForm         @"solarInspection"
#define kAirsourceInspectionForm     @"airsourceInspection"



#define kBoilerFlueTypeDescription         @"flueTypeDescription"
#define kBoilerLocationDescription         @"locationDescription"
#define kBoilerManufacturerDescription     @"manufacturerDescription"
#define kBoilerModelDescription            @"modelDescription"
#define kBoilerTypeDescription             @"boilerTypeDescription"
#define kIsSchemeBlock                      @"isSchemeBlock"
#define kIsAlternativeHeating                  @"isAlternativeHeating"

#pragma mark - RepairPicture Entity Attributes
/*!
 @discussion
 RepairPicture Entity Attributes.
 */
#define kIsBeforeImage                  @"isBeforeImage"
#define kFaultRepairImageId             @"FaultRepairImageId"
#define kRepairImagePath                @"imagePath"
#define kRepairImage                    @"image"
#define kPropertyPictureIsDefault       @"isDefault"
#define kRepairImageSyncStatus          @"syncStatus"
#define kRepairJSNumber                 @"jsNumber"
#define kTemporaryImageFileName         @"temporaryImageFileToUpload.png"


/*!
 @discussion
 ApplianceLocation Entity Attributes.
 */
#define kApplianceLocationName      @"Location"
#define kApplianceLocationID        @"LocationID"

/*!
 @discussion
 ApplianceManufacturer Entity Attributes.
 */
#define kApplianceManufacturerName      @"Manufacturer"
#define kApplianceManufacturerID        @"ManufacturerID"

/*!
 @discussion
 ApplianceModel Entity Attributes.
 */
#define kApplianceModelName      @"ApplianceModel"
#define kApplianceModelID    @"ApplianceModelID"

/*!
 @discussion
 ApplianceType Entity Attributes.
 */
#define kApplianceTypeName      @"ApplianceType"
#define kApplianceTypeID        @"ApplianceTypeID"

/*!
 @discussion
 DefectPriority Entity Attributes.
 */
#define kPriorityDesc      @"priority"
#define kPriorityId        @"priorityId"

/*!
 @discussion
 PowerType Entity Attributes.
 */
#define kPowerTypeDesc            @"powerType"

/*!
 @discussion
 Detector Entity Attributes.
*/

#define kDetectorsType          @"detectorType"
#define kDetectorTypeId         @"detectorTypeId"
#define kInstalledDate          @"installedDate"
#define kIsDetectorInspected    @"isInspected"
#define kIsLandlordsDetector    @"isLandlordsDetector"
#define kLocation               @"location"
#define kManufacturer           @"manufacturer"
#define kDetectorPropertyID     @"propertyID"
#define kSerialNumber           @"serialNumber"
#define kPowerTypeId            @"powerTypeId"
#define kDetectorId             @"detectorId"
#define kBatteryReplaced        @"batteryReplaced"
#define kDetectorInspectedBy    @"inspectedBy"
#define kDetectorInspectionDate @"inspectionDate"
#define kIsDetectorPassed       @"isPassed"
#define kDetectorLastTestedDate @"lastTestedDate"
#define kDetectorNotes          @"notes"

#define kDetectorDefects        @"detectorDefects"
#define kDetectorToPowerType    @"detectorToPowerType"
#define kDetectorToProperty     @"detectorToProperty"

#pragma mark - Survey Entity Attributes
/*!
 @discussion
 Property Survey Attributes.
 */
#define kSurveyJSON                 @"surveyJSON"
#define kIsVisited                  @"isVisited"

#pragma mark - New Appointment Entity Attributes
/*!
 @discussion
 New Appointment Attributes.
 */


#pragma mark - Appliance Inspection Entity Attributes
/*!
 @discussion
 Appliance Inspection Attributes.
 */

#define kIsInspected                  @"isInspected"
#define kCombustionReading            @"combustionReading"
#define kOperatingPressure            @"operatingPressure"
#define kSafetyDeviceOperational      @"safetyDeviceOperational"
#define kSmokePellet                  @"smokePellet"
#define kAdequateVentilation          @"adequateVentilation"
#define kFlueVisualCondition          @"flueVisualCondition"
#define kSatisfactoryTermination      @"satisfactoryTermination"
#define kFluePerformanceChecks        @"fluePerformanceChecks"
#define kApplianceServiced            @"applianceServiced"
#define kApplianceSafeTouse           @"applianceSafeToUse"
#define kInspectionId                 @"inspectionID"
#define kInspectedBy                  @"inspectedBy"
#define kInspectionDate               @"inspectionDate"
#define kSpillageTest                 @"spillageTest"
#define kOperatingPressureUnit        @"operatingPressureUnit"

/*!
 @discussion
 Boiler Inspection Attributes.
 */
#define kBoilerServiced               @"boilerServiced"
#define kBoilerSafeTouse              @"boilerSafeToUse"
#define kBoilerInspectionId           @"inspectionId"

#pragma mark - Appliance Detector Entity Attributes
/*!
 @discussion
 Appliance Detector Attributes.
 */
#define kDetectorTest                  @"detectorTest"


#pragma mark - Property Installation Pipework Entity Attributes
/*!
 @discussion
Property Installation PipeworkA ttributes.
 */

#define kEmergencyControl         @"emergencyControl"
#define kEquipotentialBonding     @"equipotentialBonding"
#define kGasTightnessTest          @"gasTightnessTest"
#define KVisualInspection          @"visualInspection"

#pragma mark - Defect Entity Attributes
/*!
 @discussion
 Defect Entity Attributes.
 */
#define kDefectObjectId             @"defectObjectId"
#define kDefectDate                 @"defectDate"
#define kDefectNotes                @"defectNotes"
#define kDefectID                   @"ID"
#define kDefectCategoryType         @"faultCategory"
#define kIsRemedialActionTaken      @"isRemedialActionTaken"
#define kisAdviceNoteIssued         @"isAdviceNoteIssued"
#define kIsDefectIdentified         @"isDefectIdentified"
#define kRemedialActionNotes        @"remedialActionNotes"
#define kWarningNoteSerialNo        @"warningNoteSerialNo"
#define kSerialNo                   @"serialNumber"
#define kWarningTagFixed            @"warningTagFixed"
#define kDefectType                 @"defectType"
#define kApplianceObjectId          @"applianceObjectId"
#define kBoilerObjectId             @"boilerObjectId"
#define kHeatingObjectId            @"kHeatingObjectId"

#define kGasCouncilNumber           @"gasCouncilNumber"
#define kIsDisconnected             @"isDisconnected"
#define kIsPartsRequired            @"isPartsRequired"
#define kIsPartsOrdered             @"isPartsOrdered"
#define kPartsOrderedBy             @"partsOrderedBy"
#define kPartsDueDate               @"partsDueDate"
#define kPartsDescription           @"partsDescription"
#define kPartsLocation              @"partsLocation"
#define kIsTwoPersonsJob            @"isTwoPersonsJob"
#define kReasonForTwoPerson         @"reasonForTwoPerson"
#define kDefectDuration             @"duration"
#define kDefectPriorityId           @"priorityId"
#define kDefectTradeId              @"tradeId"
#define kDefectApplianceID          @"applianceID"
#define kDefectBoilerTypeId         @"boilerTypeId"
#define kDefectCompletionNotes      @"defectCompletionNotes"

#pragma mark - Defect Jobdata List Entity Attributes
/*!
 @discussion
 Defect Jobdata List Entity Attributes.
 */
#define kApplianceName              @"appliance"
#define kDefectApplianceId          @"applianceId"
#define kDefectJobdataBoilerTypeId  @"boilerTypeId"
#define kDefectId                   @"defectId"
#define kCompletionDate             @"completionDate"
#define kDefectRecordedBy           @"defectRecordedBy"
#define kInspectionRef              @"inspectionRef"
#define kApplianceMake              @"make"
#define kModel                      @"model"
#define kIsCustomerHaveHeating      @"isCustomerHaveHeating"
#define kIsHeatersLeft              @"isHeatersLeft"
#define kNumberOfHeatersLeft        @"numberOfHeatersLeft"
#define kIsCustomerHaveHotWater     @"isCustomerHaveHotWater"
#define kDefectCompletionNotes      @"defectCompletionNotes"

#pragma mark - Defect Category Entity Attributes
/*!
 @discussion
 Defect Category Entity Attributes.
 */
#define kDefectCategoryDescription        @"CatDescription"
#define kDefectCategoryID                 @"CatId"
#define kCategoryDescription              @"categoryDescription"
#define kCategoryId                       @"categoryID"

#define kDefectCategoryId                 @"defectCategoryId"

#pragma mark - Fault Repair Entity Attributes
/*!
 @discussion
 Fault Repair Entity Attributes.
 */
#define kFaultRepairDescription             @"description"
#define kFaultRepairID                      @"faultRepairId"
#define kRepairSectionIdentifier            @"repairSectionIdentifier"
#define kIsAssociated                       @"isAssociated"
#define kFaultRepairGross                   @"gross"

#pragma mark - Job Pause Data Entity Attributes
/*!
 @discussion
 Job Pause Data Entity Attributes.
 */
#define kPauseDate                  @"pauseDate"
#define kPauseNote                  @"pauseNote"
#define kPausedBy                   @"pausedBy"
#define kActionType                 @"actionType"
#define kPauseReasonTitle        @"pauseReason"


#pragma mark - JobSheet Entity Attributes
/*!
 @discussion
 PlannedTradeComponent Entity Attributes.
 */
#define kTradeDescription           @"trade"
#define kTradeId                    @"tradeId"
#define kSortingOrder               @"sorder"
#define kComponentId                @"componentId"
#define kComponentName              @"componentName"
#define kComponentTradeId           @"componentTradeId"
#define kPMODescription             @"PMO"
#define kCompletionDate             @"completionDate"
#define kLocationId                 @"locationId"
#define kAdaptation                 @"adaptation"
#define kAdaptationId               @"adaptationId"

#pragma mark - MVHR Inspection Entity Attributes
/*!
 */

#define kGeneralHeatingChecksEntity @"GeneralHeatingChecks"
#define kRadiatorCondition  @"radiatorCondition"
#define kHeatingControl @"heatingControl"
#define kSystemUsage    @"systemUsage"
#define kGeneralHeating @"generalHeating"
#define kAccessIssues   @"accessIssues"
#define kAccessIssueNotes   @"accessIssueNotes"
#define kConfirmation   @"confirmation"
#define kCheckedDate    @"checkedDate"


#define kMVHRInspectionEntity       @"MVHRInspection"
#define kCheckAirFlow              @"checkAirFlow"
#define kAirFlowDetail              @"airFlowDetail"
#define kDuctingInspection          @"ductingInspection"
#define kDuctingDetail              @"ductingDetail"
#define kCheckFilters               @"checkFilters"
#define kFilterDetail               @"filterDetail"

#define kOilHeatingEntity   @"OilHeating"
#define kOilHeatingApplianceLocation  @"applianceLocation"
#define kOilHeatingApplianceMake    @"applianceMake"
#define kOilHeatingApplianceModel   @"applianceModel"
#define kOilHeatingApplianceSerialNumber    @"applianceSerialNumber"
#define kBurnerMake @"burnerMake"
#define kBurnerModel    @"burnerModel"
#define kBurnerTypeId    @"burnerTypeId"
#define kFlueTypeId @"flueTypeId"
#define kFuelTypeId @"fuelTypeId"
#define kTankTypeId @"tankTypeId"
#define kOriginalInstallDate    @"originalInstallDate"
#define kOilFiringAndServiceForm    @"oilFiringAndService"


#define kOilFireServiceInspectionEntity @"OilFiringServiceInspection"
#define kAirSupply  @"airSupply"
#define kAirSupplyDetail    @"airSupplyDetail"
#define kApplianceSafety    @"applianceSafety"
#define kApplianceSafetyDetail  @"applianceSafetyDetail"
#define kChimneyFlue    @"chimneyFlue"
#define kChimneyFlueDetail  @"chimneyFlueDetail"
#define kCombustionChamber  @"combustionChamber"
#define kCombustionChamberDetail    @"combustionChamberDetail"
#define kControlCheck   @"controlCheck"
#define kControlCheckDetail @"controlCheckDetail"
#define kElectricalSafety   @"electricalSafety"
#define kElectricalSafetyDetail @"electricalSafetyDetail"
#define kHeatExchanger  @"heatExchanger"
#define kHeatExchangerDetail    @"heatExchangerDetail"
#define kHotWaterType   @"hotWaterType"
#define kHotWaterTypeDetail @"hotWaterTypeDetail"
#define kOilStorage @"oilStorage"
#define kOilStorageDetail   @"oilStorageDetail"
#define kOilSupplySystem    @"oilSupplySystem"
#define kOilSupplySystemDetail  @"oilSupplySystemDetail"
#define kPressureJet    @"pressureJet"
#define kPressureJetDetail  @"pressureJetDetail"
#define kVaporisingBurner   @"vaporisingBurner"
#define kVaporisingBurnerDetail @"vaporisingBurnerDetail"
#define kWallflameBurner @"wallflameBurner"
#define kWallflameBurnerDetail  @"wallflameBurnerDetail"
#define kWarmAirType    @"warmAirType"
#define kWarmAirTypeDetail  @"warmAirTypeDetail"


#pragma mark - Airsource Inspection Entity Attributes
/*!
 */

#define kAirsourceInspectionEntity              @"AirSourceInspection"
#define kCheckWaterSupplyTurnedOff              @"checkWaterSupplyTurnedOff"
#define kTurnedOffDetail              @"turnedOffDetail"
#define kCheckWaterSupplyTurnedOn          @"checkWaterSupplyTurnedOn"
#define kTurnedOnDetail              @"turnedOnDetail"
#define kCheckValves                @"checkValves"
#define kValveDetail              @"valveDetail"
#define kCheckSupplementaryBonding               @"checkSupplementaryBonding"
#define kSupplementaryBondingDetail              @"supplementaryBondingDetail"
#define kCheckFuse              @"checkFuse"
#define kFuseDetail          @"fuseDetail"
#define kCheckThermostat              @"checkThermostat"
#define kThermostatDetail                @"thermostatDetail"
#define kCheckOilLeak              @"checkOilLeak"
#define kOilLeakDetail               @"oilLeakDetail"
#define kCheckWaterPipework              @"checkWaterPipework"
#define kPipeworkDetail                @"pipeworkDetail"
#define kCheckElectricConnection              @"checkElectricConnection"
#define kConnectionDetail               @"connectionDetail"

#pragma mark - Solar Inspection Entity Attributes
/*!
 */
#define kSolarInspectionEntity              @"SolarInspection"
#define kVesselProtectionInstalled              @"vesselProtectionInstalled"
#define kVesselCapacity              @"vesselCapacity"
#define kMinPressure          @"minPressure"
#define kFreezingTemp              @"freezingTemp"
#define kSystemPressure                @"systemPressure"
#define kBackPressure              @"backPressure"
#define kDeltaOn               @"deltaOn"
#define kDeltaOff              @"deltaOff"
#define kMaxTemperature              @"maxTemperature"
#define kCalculationRate          @"calculationRate"
#define kThermostatTemperature              @"thermostatTemperature"
#define kAntiScaldingControl                @"antiScaldingControl"
#define kAntiScaldingControlDetail              @"antiScaldingControlDetail"
#define kCheckDirection               @"checkDirection"
#define kDirectionDetail              @"directionDetail"
#define kCheckElectricalControl                @"checkElectricalControl"
#define kElectricalControlDetail              @"electricalControlDetail"



