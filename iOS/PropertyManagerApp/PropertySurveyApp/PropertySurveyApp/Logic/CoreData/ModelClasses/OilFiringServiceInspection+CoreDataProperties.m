//
//  OilFiringServiceInspection+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilFiringServiceInspection+CoreDataProperties.h"

@implementation OilFiringServiceInspection (CoreDataProperties)

+ (NSFetchRequest<OilFiringServiceInspection *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"OilFiringServiceInspection"];
}

@dynamic airSupply;
@dynamic airSupplyDetail;
@dynamic applianceSafety;
@dynamic applianceSafetyDetail;
@dynamic appointmentId;
@dynamic chimneyFlue;
@dynamic chimneyFlueDetail;
@dynamic combustionChamber;
@dynamic combustionChamberDetail;
@dynamic controlCheck;
@dynamic controlCheckDetail;
@dynamic electricalSafety;
@dynamic electricalSafetyDetail;
@dynamic heatExchanger;
@dynamic heatExchangerDetail;
@dynamic heatingId;
@dynamic hotWaterType;
@dynamic hotWaterTypeDetail;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionId;
@dynamic isInspected;
@dynamic journalId;
@dynamic oilStorage;
@dynamic oilStorageDetail;
@dynamic oilSupplySystem;
@dynamic oilSupplySystemDetail;
@dynamic pressureJet;
@dynamic pressureJetDetail;
@dynamic vaporisingBurner;
@dynamic vaporisingBurnerDetail;
@dynamic wallflameBurner;
@dynamic wallflameBurnerDetail;
@dynamic warmAirType;
@dynamic warmAirTypeDetail;
@dynamic firingServiceInspectionToOilHeating;
@dynamic fireServicingInspectionToPictures;

@end
