//
//  CP12Info+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "CP12Info+CoreDataProperties.h"

@implementation CP12Info (CoreDataProperties)

+ (NSFetchRequest<CP12Info *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CP12Info"];
}

@dynamic cp12Document;
@dynamic cp12Number;
@dynamic cp12Passed;
@dynamic dateTimeStamp;
@dynamic documentType;
@dynamic inspectionCarried;
@dynamic issuedBy;
@dynamic issuedDate;
@dynamic issuedDateString;
@dynamic journalId;
@dynamic jsgNumber;
@dynamic lsgrid;
@dynamic notes;
@dynamic propertyID;
@dynamic receivedDate;
@dynamic receivedDateString;
@dynamic receivedOnBehalfOf;
@dynamic tenantHanded;
@dynamic heatingId;
@dynamic schemeId;
@dynamic blockId;
@dynamic cp12InfoToAppointment;

@end
