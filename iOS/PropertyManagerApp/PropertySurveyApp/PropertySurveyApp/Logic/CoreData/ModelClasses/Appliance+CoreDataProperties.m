//
//  Appliance+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appliance+CoreDataProperties.h"

@implementation Appliance (CoreDataProperties)

+ (NSFetchRequest<Appliance *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Appliance"];
}

@dynamic applianceID;
@dynamic fluType;
@dynamic gcNumber;
@dynamic installedDate;
@dynamic isActive;
@dynamic isInspected;
@dynamic isLandlordAppliance;
@dynamic model;
@dynamic propertyID;
@dynamic replacementDate;
@dynamic serialNumber;
@dynamic schemeId;
@dynamic blockId;
@dynamic applianceDefects;
@dynamic applianceInspection;
@dynamic applianceLocation;
@dynamic applianceManufacturer;
@dynamic applianceModel;
@dynamic applianceToProperty;
@dynamic applianceType;

@end
