//
//  ApplianceLocation.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "ApplianceLocation.h"


@implementation ApplianceLocation

@dynamic locationID;
@dynamic locationName;

@end
