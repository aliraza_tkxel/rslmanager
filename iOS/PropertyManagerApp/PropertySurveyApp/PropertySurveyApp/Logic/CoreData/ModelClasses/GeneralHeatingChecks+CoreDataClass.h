//
//  GeneralHeatingChecks+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

NS_ASSUME_NONNULL_BEGIN

@interface GeneralHeatingChecks : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GeneralHeatingChecks+CoreDataProperties.h"
