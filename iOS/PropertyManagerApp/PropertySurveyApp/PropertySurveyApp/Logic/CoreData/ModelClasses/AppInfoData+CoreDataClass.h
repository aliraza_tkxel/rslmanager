//
//  AppInfoData+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 7/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

NS_ASSUME_NONNULL_BEGIN

@interface AppInfoData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AppInfoData+CoreDataProperties.h"
