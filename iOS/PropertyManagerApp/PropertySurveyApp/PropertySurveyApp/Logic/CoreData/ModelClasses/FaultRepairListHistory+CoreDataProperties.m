//
//  FaultRepairListHistory+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "FaultRepairListHistory+CoreDataProperties.h"

@implementation FaultRepairListHistory (CoreDataProperties)

+ (NSFetchRequest<FaultRepairListHistory *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FaultRepairListHistory"];
}

@dynamic faultRepairDescription;
@dynamic faultRepairID;
@dynamic faultRepairListHistoryToFaultJobSheet;

@end
