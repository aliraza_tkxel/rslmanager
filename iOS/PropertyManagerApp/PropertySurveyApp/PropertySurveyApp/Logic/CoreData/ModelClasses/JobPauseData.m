//
//  JobPauseData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "JobPauseData.h"
#import "JobPauseReason.h"
#import "JobSheet+CoreDataClass.h"


@implementation JobPauseData

@dynamic actionType;
@dynamic pauseDate;
@dynamic pausedBy;
@dynamic pauseNote;
@dynamic jobSheet;
@dynamic pauseReason;

@end
