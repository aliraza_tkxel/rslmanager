//
//  MVHRInspection+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "MVHRInspection+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MVHRInspection (CoreDataProperties)

+ (NSFetchRequest<MVHRInspection *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *airFlowDetail;
@property (nullable, nonatomic, copy) NSNumber *checkAirFlow;
@property (nullable, nonatomic, copy) NSNumber *checkFilters;
@property (nullable, nonatomic, copy) NSString *ductingDetail;
@property (nullable, nonatomic, copy) NSNumber *ductingInspection;
@property (nullable, nonatomic, copy) NSString *filterDetail;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, retain) AlternativeHeating *mvhrInspectionToHeating;
@property (nullable, nonatomic, retain) NSSet<AlternativeFuelPicture *> *mvhrInspectionToPictures;

@end

@interface MVHRInspection (CoreDataGeneratedAccessors)

- (void)addMvhrInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)removeMvhrInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)addMvhrInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;
- (void)removeMvhrInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
