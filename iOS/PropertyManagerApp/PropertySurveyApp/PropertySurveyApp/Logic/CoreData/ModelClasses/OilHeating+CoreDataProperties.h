//
//  OilHeating+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilHeating+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface OilHeating (CoreDataProperties)

+ (NSFetchRequest<OilHeating *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *applianceLocation;
@property (nullable, nonatomic, copy) NSString *applianceMake;
@property (nullable, nonatomic, copy) NSString *applianceModel;
@property (nullable, nonatomic, copy) NSString *applianceSerialNumber;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSString *burnerMake;
@property (nullable, nonatomic, copy) NSString *burnerModel;
@property (nullable, nonatomic, copy) NSNumber *burnerTypeId;
@property (nullable, nonatomic, copy) NSDate *certificateIssued;
@property (nullable, nonatomic, copy) NSString *certificateName;
@property (nullable, nonatomic, copy) NSString *certificateNumber;
@property (nullable, nonatomic, copy) NSDate *certificateRenewel;
@property (nullable, nonatomic, copy) NSNumber *flueTypeId;
@property (nullable, nonatomic, copy) NSNumber *fuelTypeId;
@property (nullable, nonatomic, copy) NSString *heatingFuel;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSString *heatingName;
@property (nullable, nonatomic, copy) NSString *heatingType;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSDate *originalInstallDate;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *tankTypeId;
@property (nullable, nonatomic, retain) BurnerType *oilHeatingToBurnerType;
@property (nullable, nonatomic, retain) OilFiringServiceInspection *oilHeatingToFireServiceInspection;
@property (nullable, nonatomic, retain) OilFlueType *oilHeatingToOilFlueType;
@property (nullable, nonatomic, retain) OilFuelType *oilHeatingToOilFuelType;
@property (nullable, nonatomic, retain) Property *oilHeatingToProperty;
@property (nullable, nonatomic, retain) TankType *oilHeatingToTankType;
@property (nullable, nonatomic, retain) NSSet<Defect *> *oilHeatingToDefect;

@end

@interface OilHeating (CoreDataGeneratedAccessors)

- (void)addOilHeatingToDefectObject:(Defect *)value;
- (void)removeOilHeatingToDefectObject:(Defect *)value;
- (void)addOilHeatingToDefect:(NSSet<Defect *> *)values;
- (void)removeOilHeatingToDefect:(NSSet<Defect *> *)values;

@end

NS_ASSUME_NONNULL_END
