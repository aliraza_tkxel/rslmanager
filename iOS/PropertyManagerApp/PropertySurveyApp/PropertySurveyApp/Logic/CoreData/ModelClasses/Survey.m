//
//  Survey.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Survey.h"
#import "Appointment+CoreDataClass.h"
#import "SurveyData.h"


@implementation Survey

@dynamic surveyJSON;
@dynamic surveyToAppointment;
@dynamic surveyToSurveyData;

@end
