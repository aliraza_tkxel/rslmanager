//
//  Appliance+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ApplianceInspection, ApplianceLocation, ApplianceManufacturer, ApplianceModel, ApplianceType, Defect, Property;

NS_ASSUME_NONNULL_BEGIN

@interface Appliance : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Appliance+CoreDataProperties.h"
