//
//  VoidWorks.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-11.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "VoidWorks.h"
#import "Appointment+CoreDataClass.h"


@implementation VoidWorks

@dynamic terminationDate;
@dynamic reletDate;
@dynamic jsvNumber;
@dynamic voidWorksToAppointment;

@end
