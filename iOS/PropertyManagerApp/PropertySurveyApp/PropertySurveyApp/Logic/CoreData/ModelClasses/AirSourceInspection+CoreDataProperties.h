//
//  AirSourceInspection+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AirSourceInspection+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AirSourceInspection (CoreDataProperties)

+ (NSFetchRequest<AirSourceInspection *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *checkElectricConnection;
@property (nullable, nonatomic, copy) NSNumber *checkFuse;
@property (nullable, nonatomic, copy) NSNumber *checkOilLeak;
@property (nullable, nonatomic, copy) NSNumber *checkSupplementaryBonding;
@property (nullable, nonatomic, copy) NSNumber *checkThermostat;
@property (nullable, nonatomic, copy) NSNumber *checkValves;
@property (nullable, nonatomic, copy) NSNumber *checkWaterPipework;
@property (nullable, nonatomic, copy) NSNumber *checkWaterSupplyTurnedOff;
@property (nullable, nonatomic, copy) NSNumber *checkWaterSupplyTurnedOn;
@property (nullable, nonatomic, copy) NSString *connectionDetail;
@property (nullable, nonatomic, copy) NSString *fuseDetail;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSString *oilLeakDetail;
@property (nullable, nonatomic, copy) NSString *pipeworkDetail;
@property (nullable, nonatomic, copy) NSString *supplementaryBondingDetail;
@property (nullable, nonatomic, copy) NSString *thermostatDetail;
@property (nullable, nonatomic, copy) NSString *turnedOffDetail;
@property (nullable, nonatomic, copy) NSString *turnedOnDetail;
@property (nullable, nonatomic, copy) NSString *valveDetail;
@property (nullable, nonatomic, retain) AlternativeHeating *airsourceInspectionToHeating;
@property (nullable, nonatomic, retain) NSSet<AlternativeFuelPicture *> *airsourceInspectionToPictures;

@end

@interface AirSourceInspection (CoreDataGeneratedAccessors)

- (void)addAirsourceInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)removeAirsourceInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)addAirsourceInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;
- (void)removeAirsourceInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
