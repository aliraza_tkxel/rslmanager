//
//  DefectPriority.h
//  PropertySurveyApp
//
//  Created by aqib javed on 05/08/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DefectPriority : NSManagedObject

@property (nonatomic, retain) NSNumber * priorityId;
@property (nonatomic, retain) NSString * priority;

@end
