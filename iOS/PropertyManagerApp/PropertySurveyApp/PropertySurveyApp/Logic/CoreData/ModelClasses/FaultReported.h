//
//  FaultReported.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-29.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, FaultDetailList, FaultAreaList;

@interface FaultReported : NSManagedObject

@property (nonatomic, retain) NSNumber * isRecurring;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSNumber * problemDays;
@property (nonatomic, retain) Appointment * faultReportedToAppointment;
@property (nonatomic, retain) FaultDetailList * faultReportedToFaultDetail;
@property (nonatomic, retain) FaultAreaList * faultReportedToFaultArea;

@end