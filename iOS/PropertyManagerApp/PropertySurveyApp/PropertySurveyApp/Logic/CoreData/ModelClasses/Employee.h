//
//  Employee.h
//  PropertySurveyApp
//
//  Created by aqib javed on 13/08/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Employee : NSManagedObject

@property (nonatomic, retain) NSNumber * employeeId;
@property (nonatomic, retain) NSString * employeeName;

@end
