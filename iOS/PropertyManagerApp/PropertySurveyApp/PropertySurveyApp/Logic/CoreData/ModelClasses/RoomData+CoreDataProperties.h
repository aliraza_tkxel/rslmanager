//
//  RoomData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/29/16.
//  Copyright © 2016 TkXel. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RoomData.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *roomId;
@property (nullable, nonatomic, retain) NSString *roomName;

@end

NS_ASSUME_NONNULL_END
