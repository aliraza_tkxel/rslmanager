//
//  SolarInspection+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AlternativeFuelPicture, AlternativeHeating;

NS_ASSUME_NONNULL_BEGIN

@interface SolarInspection : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SolarInspection+CoreDataProperties.h"
