//
//  User.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "User.h"
#import "Appointment+CoreDataClass.h"


@implementation User

@dynamic fullName;
@dynamic isActive;
@dynamic lastLoggedInDate;
@dynamic password;
@dynamic salt;
@dynamic userId;
@dynamic userName;
@dynamic userToAppointments;

- (void) removeAllAppointments
{
    if(!isEmpty(self.userToAppointments))
    {
        for(Appointment *appointment in self.userToAppointments)
        {
            AppointmentType appointmentType = [appointment getType];
            if (appointmentType == AppointmentTypeGas || appointmentType == AppointmentTypeGasVoid)
            {
                if ([appointment getStatus] == AppointmentStatusNoEntry) {
                    [[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
                }
                else if ([appointment.isOfflinePrepared boolValue] == TRUE && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
                {
                    continue;
                }
                else
                {
                    [[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
                }
            }
            else
            {
               [[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
            }
        }
    }
}

- (NSArray *) completedAppointments
{
    NSArray *completedAppointments = nil;
    if(!isEmpty(self.userToAppointments))
    {
    
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'Complete'"];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'Finished'"];
        NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'No Entry'"];
        NSPredicate *predicate4 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'Aborted'"];
        NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2,predicate3,predicate4]];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
        NSSet *completedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:predicate];
        if(!isEmpty(completedAppointmentsSet))
        {
            completedAppointments = [completedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
        }
    }
    return completedAppointments;
}

-(NSArray *) inProgressAppointments
{
    NSArray *inProgressAppointments = [NSArray new];
    if(!isEmpty(self.userToAppointments))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == %@",kAppointmentStatusInProgress];
        NSPredicate *p2 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatusSynced = %@", [NSNumber numberWithBool:NO]];
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates: @[predicate, p2]];
        
        
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
        NSSet *modifiedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:p];
        if(!isEmpty(modifiedAppointmentsSet))
        {
            inProgressAppointments = [modifiedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
        }
    }
    return inProgressAppointments;
}

-(NSArray *) acceptedAppointments
{
    NSArray *inProgressAppointments = [NSArray new];
    if(!isEmpty(self.userToAppointments))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == %@",kAppointmentStatusAccepted];
        NSPredicate *p2 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatusSynced = %@", [NSNumber numberWithBool:NO]];
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates: @[predicate, p2]];
        
        
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
        NSSet *modifiedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:p];
        if(!isEmpty(modifiedAppointmentsSet))
        {
            inProgressAppointments = [modifiedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
        }
    }
    return inProgressAppointments;
}


-(NSArray *) pausedAppointments
{
    NSArray *inProgressAppointments = [NSArray new];
    if(!isEmpty(self.userToAppointments))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == %@",kAppointmentStatusPaused];
        NSPredicate *p2 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatusSynced = %@", [NSNumber numberWithBool:NO]];
        NSPredicate *p = [NSCompoundPredicate andPredicateWithSubpredicates: @[predicate, p2]];
        
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
        NSSet *modifiedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:p];
        if(!isEmpty(modifiedAppointmentsSet))
        {
            inProgressAppointments = [modifiedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
        }
    }
    return inProgressAppointments;
}

- (NSArray *) modifiedAppointments
{
    NSArray *modifiedAppointments = nil;
    if(!isEmpty(self.userToAppointments))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isModified == 1"];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
        NSSet *modifiedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:predicate];
        if(!isEmpty(modifiedAppointmentsSet))
        {
            modifiedAppointments = [modifiedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
        }
    }
    return modifiedAppointments;
}

- (void) deleteAppointmentWithInfoDict:(NSDictionary *) dict
{
    if(!isEmpty(dict)){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.appointmentId == %@) AND (SELF.appointmentType == %@)", [dict objectForKey:@"appointmentId"], [dict objectForKey:@"appointmentType"]];
        NSSet *filteredSet = [self.userToAppointments filteredSetUsingPredicate:predicate];
        if(filteredSet!=nil){
            if(!isEmpty([filteredSet allObjects])){
                Appointment *appointment = [[filteredSet allObjects] objectAtIndex:0];
                [[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
            }
        }
        
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"User: [ %@, %@, %@ ]",
            self.userName,
            self.password,
            self.salt];
}
@end
