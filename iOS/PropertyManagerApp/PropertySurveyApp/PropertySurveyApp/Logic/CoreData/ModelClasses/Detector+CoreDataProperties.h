//
//  Detector+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Detector+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Detector (CoreDataProperties)

+ (NSFetchRequest<Detector *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *batteryReplaced;
@property (nullable, nonatomic, copy) NSNumber *detectorId;
@property (nullable, nonatomic, copy) NSString *detectorType;
@property (nullable, nonatomic, copy) NSNumber *detectorTypeId;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSDate *installedDate;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *isLandlordsDetector;
@property (nullable, nonatomic, copy) NSNumber *isPassed;
@property (nullable, nonatomic, copy) NSDate *lastTestedDate;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *manufacturer;
@property (nullable, nonatomic, copy) NSString *notes;
@property (nullable, nonatomic, copy) NSNumber *powerTypeId;
@property (nullable, nonatomic, copy) NSString *propertyID;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, retain) NSSet<Defect *> *detectorDefects;
@property (nullable, nonatomic, retain) PowerType *detectorToPowerType;
@property (nullable, nonatomic, retain) Property *detectorToProperty;

@end

@interface Detector (CoreDataGeneratedAccessors)

- (void)addDetectorDefectsObject:(Defect *)value;
- (void)removeDetectorDefectsObject:(Defect *)value;
- (void)addDetectorDefects:(NSSet<Defect *> *)values;
- (void)removeDetectorDefects:(NSSet<Defect *> *)values;

@end

NS_ASSUME_NONNULL_END
