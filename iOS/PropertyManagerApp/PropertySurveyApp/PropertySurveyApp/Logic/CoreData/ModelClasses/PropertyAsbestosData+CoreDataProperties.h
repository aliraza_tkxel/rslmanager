//
//  PropertyAsbestosData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 21/12/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PropertyAsbestosData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PropertyAsbestosData (CoreDataProperties)

+ (NSFetchRequest<PropertyAsbestosData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *asbestosId;
@property (nullable, nonatomic, copy) NSString *asbRiskLevelDesc;
@property (nullable, nonatomic, copy) NSString *riskDesc;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *riskLevel;
@property (nullable, nonatomic, retain) Property *propertyAsbestosDataToProperty;

@end

NS_ASSUME_NONNULL_END
