//
//  PaintPack.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-30.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VoidData;

@interface PaintPack : NSManagedObject

@property (nonatomic, retain) NSNumber * roomId;
@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) VoidData *paintPackToVoidData;

@end
