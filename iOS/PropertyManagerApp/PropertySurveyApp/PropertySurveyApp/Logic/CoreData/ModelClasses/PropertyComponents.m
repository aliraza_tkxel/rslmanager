//
//  PropertyComponents.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-25.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PropertyComponents.h"
#import "VoidData.h"


@implementation PropertyComponents

@dynamic component;
@dynamic condition;
@dynamic replacementDue;
@dynamic componentId;
@dynamic propertyComponentsToVoidData;

@end
