//
//  Journal+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

NS_ASSUME_NONNULL_BEGIN

@interface Journal : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Journal+CoreDataProperties.h"
