//
//  Defect+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appliance, Boiler, DefectCategory, DefectPicture, DefectPriority, Detector, OilHeating, Trade;

NS_ASSUME_NONNULL_BEGIN

@interface Defect : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Defect+CoreDataProperties.h"
