//
//  GeneralHeatingChecks+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "GeneralHeatingChecks+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GeneralHeatingChecks (CoreDataProperties)

+ (NSFetchRequest<GeneralHeatingChecks *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *radiatorCondition;
@property (nullable, nonatomic, copy) NSString *heatingControl;
@property (nullable, nonatomic, copy) NSString *systemUsage;
@property (nullable, nonatomic, copy) NSString *generalHeating;
@property (nullable, nonatomic, copy) NSString *accessIssues;
@property (nullable, nonatomic, copy) NSString *accessIssueNotes;
@property (nullable, nonatomic, copy) NSString *confirmation;
@property (nullable, nonatomic, copy) NSDate *checkedDate;
@property (nullable, nonatomic, retain) Property *generalHeatingChecksToProperty;

@end

NS_ASSUME_NONNULL_END
