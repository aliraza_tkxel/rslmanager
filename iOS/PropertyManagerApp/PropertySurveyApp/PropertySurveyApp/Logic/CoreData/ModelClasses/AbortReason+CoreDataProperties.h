//
//  AbortReason+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Abdul Rehman on 23/02/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbortReason+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AbortReason (CoreDataProperties)

+ (NSFetchRequest<AbortReason *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *reasonId;
@property (nullable, nonatomic, copy) NSString *reason;

@end

NS_ASSUME_NONNULL_END
