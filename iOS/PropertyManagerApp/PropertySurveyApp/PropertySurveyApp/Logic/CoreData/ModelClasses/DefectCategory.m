//
//  DefectCategory.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DefectCategory.h"


@implementation DefectCategory

@dynamic categoryDescription;
@dynamic categoryID;

@end
