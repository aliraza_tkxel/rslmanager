//
//  DetectorInspection.m
//  PropertySurveyApp
//
//  Created by aqib javed on 29/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DetectorInspection.h"
#import "Detector+CoreDataClass.h"


@implementation DetectorInspection

@dynamic detectorsTested;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionID;
@dynamic lastTested;
@dynamic batteryReplaced;
@dynamic isPassed;
@dynamic notes;
@dynamic detector;

@end
