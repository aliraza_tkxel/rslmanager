//
//  PropertyPicture+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "PropertyPicture+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PropertyPicture (CoreDataProperties)

+ (NSFetchRequest<PropertyPicture *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSNumber *createdBy;
@property (nullable, nonatomic, copy) NSDate *createdOn;
@property (nullable, nonatomic, copy) NSNumber *deletedPicture;
@property (nullable, nonatomic, copy) NSString *imageIdentifier;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nullable, nonatomic, copy) NSNumber *isDefault;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *propertyPictureId;
@property (nullable, nonatomic, copy) NSString *propertyPictureName;
@property (nullable, nonatomic, copy) NSNumber *surveyId;
@property (nullable, nonatomic, copy) NSNumber *synchStatus;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, retain) Property *propertyPictureToProperty;

@end

NS_ASSUME_NONNULL_END
