//
//  FaultRepairListHistory+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "FaultRepairListHistory+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FaultRepairListHistory (CoreDataProperties)

+ (NSFetchRequest<FaultRepairListHistory *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *faultRepairDescription;
@property (nullable, nonatomic, copy) NSNumber *faultRepairID;
@property (nullable, nonatomic, retain) FaultJobSheet *faultRepairListHistoryToFaultJobSheet;

@end

NS_ASSUME_NONNULL_END
