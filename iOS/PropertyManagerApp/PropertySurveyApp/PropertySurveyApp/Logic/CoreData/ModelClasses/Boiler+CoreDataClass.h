//
//  Boiler+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BoilerInspection, BoilerManufacturer, BoilerType, Defect, FlueType, InstallationPipework, Property;

NS_ASSUME_NONNULL_BEGIN

@interface Boiler : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Boiler+CoreDataProperties.h"
