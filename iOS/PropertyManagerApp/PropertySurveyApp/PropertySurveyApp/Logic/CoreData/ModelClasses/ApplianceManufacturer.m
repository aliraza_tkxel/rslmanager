//
//  ApplianceManufacturer.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "ApplianceManufacturer.h"


@implementation ApplianceManufacturer

@dynamic manufacturerID;
@dynamic manufacturerName;

@end
