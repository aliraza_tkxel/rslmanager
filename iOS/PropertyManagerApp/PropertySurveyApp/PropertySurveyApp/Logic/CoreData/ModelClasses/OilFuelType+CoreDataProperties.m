//
//  OilFuelType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilFuelType+CoreDataProperties.h"

@implementation OilFuelType (CoreDataProperties)

+ (NSFetchRequest<OilFuelType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"OilFuelType"];
}

@dynamic value;
@dynamic lookupId;

@end
