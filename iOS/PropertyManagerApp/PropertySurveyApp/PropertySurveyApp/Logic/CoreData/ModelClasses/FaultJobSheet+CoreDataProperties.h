//
//  FaultJobSheet+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "FaultJobSheet+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FaultJobSheet (CoreDataProperties)

+ (NSFetchRequest<FaultJobSheet *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSString *durationUnit;
@property (nullable, nonatomic, copy) NSNumber *faultLogID;
@property (nullable, nonatomic, copy) NSString *followOnNotes;
@property (nullable, nonatomic, copy) NSString *jsnDescription;
@property (nullable, nonatomic, copy) NSString *jsnLocation;
@property (nullable, nonatomic, copy) NSString *jsnNotes;
@property (nullable, nonatomic, copy) NSString *priority;
@property (nullable, nonatomic, copy) NSNumber *repairId;
@property (nullable, nonatomic, copy) NSString *repairNotes;
@property (nullable, nonatomic, copy) NSDate *reportedDate;
@property (nullable, nonatomic, copy) NSString *responseTime;
@property (nullable, nonatomic, retain) NSSet<FaultRepairData *> *faultRepairDataList;
@property (nullable, nonatomic, retain) NSSet<FaultRepairListHistory *> *faultRepairHistoryList;

@end

@interface FaultJobSheet (CoreDataGeneratedAccessors)

- (void)addFaultRepairDataListObject:(FaultRepairData *)value;
- (void)removeFaultRepairDataListObject:(FaultRepairData *)value;
- (void)addFaultRepairDataList:(NSSet<FaultRepairData *> *)values;
- (void)removeFaultRepairDataList:(NSSet<FaultRepairData *> *)values;

- (void)addFaultRepairHistoryListObject:(FaultRepairListHistory *)value;
- (void)removeFaultRepairHistoryListObject:(FaultRepairListHistory *)value;
- (void)addFaultRepairHistoryList:(NSSet<FaultRepairListHistory *> *)values;
- (void)removeFaultRepairHistoryList:(NSSet<FaultRepairListHistory *> *)values;

@end

NS_ASSUME_NONNULL_END
