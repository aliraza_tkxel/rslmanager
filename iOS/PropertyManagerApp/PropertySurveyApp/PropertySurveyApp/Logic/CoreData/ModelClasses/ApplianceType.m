//
//  ApplianceType.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "ApplianceType.h"


@implementation ApplianceType

@dynamic typeID;
@dynamic typeName;

@end
