//
//  PropertyAttributesNotes+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/1/16.
//  Copyright © 2016 TkXel. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PropertyAttributesNotes+CoreDataProperties.h"

@implementation PropertyAttributesNotes (CoreDataProperties)

@dynamic attributeNotes;
@dynamic attributePath;
@dynamic propertyAttributesNotesToAppointment;

@end
