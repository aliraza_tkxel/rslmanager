//
//  RepairPictures.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "RepairPictures.h"
#import "JobSheet+CoreDataClass.h"


@implementation RepairPictures

@dynamic createdBy;
@dynamic createdOn;
@dynamic image;
@dynamic imageId;
@dynamic imageIdentifier;
@dynamic imagePath;
@dynamic isBeforeImage;
@dynamic syncStatus;
@dynamic repairImagesToJobSheet;
@synthesize repairImage;

@end
