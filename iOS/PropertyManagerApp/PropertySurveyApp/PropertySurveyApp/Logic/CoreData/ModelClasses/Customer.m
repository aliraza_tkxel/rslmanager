//
//  Customer.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Customer.h"
#import "Address.h"
#import "Appointment+CoreDataClass.h"
#import "CustomerRiskData.h"
#import "CustomerVulnerabilityData.h"


@implementation Customer

@dynamic address;
@dynamic customerId;
@dynamic email;
@dynamic fax;
@dynamic firstName;
@dynamic lastName;
@dynamic middleName;
@dynamic mobile;
@dynamic property;
@dynamic telephone;
@dynamic title;
@dynamic customerToAddress;
@dynamic customerToAppointment;
@dynamic customerToCustomerRiskData;
@dynamic customerToCustomerVulunarityData;

- (NSString *)  fullName
{
    NSMutableString *customerName =[NSMutableString stringWithString:@""];
    if(!isEmpty(self.title)){
        [customerName appendString:self.title];
        [customerName appendString:@"."];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.firstName)){
        [customerName appendString:self.firstName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.middleName)){
        [customerName appendString:self.middleName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.lastName))
        [customerName appendString:self.lastName];
    return customerName;
}
@end
