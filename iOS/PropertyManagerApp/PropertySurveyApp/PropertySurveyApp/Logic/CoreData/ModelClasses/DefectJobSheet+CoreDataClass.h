//
//  DefectJobSheet+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 21/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "JobSheet+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface DefectJobSheet : JobSheet

@end

NS_ASSUME_NONNULL_END

#import "DefectJobSheet+CoreDataProperties.h"
