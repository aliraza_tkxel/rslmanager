//
//  BoilerManufacturer+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerManufacturer+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BoilerManufacturer (CoreDataProperties)

+ (NSFetchRequest<BoilerManufacturer *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *isAlternativeHeating;
@property (nullable, nonatomic, copy) NSNumber *isSchemeBlock;
@property (nullable, nonatomic, copy) NSString *manufacturerDescription;
@property (nullable, nonatomic, copy) NSNumber *manufacturerId;

@end

NS_ASSUME_NONNULL_END
