//
//  Accomodation.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Accomodation.h"
#import "Property+CoreDataClass.h"


@implementation Accomodation

@dynamic propertyId;
@dynamic roomHeight;
@dynamic roomLength;
@dynamic roomName;
@dynamic roomWidth;
@dynamic updatedBy;
@dynamic updatedOn;
@dynamic accomodationToProperty;

@end
