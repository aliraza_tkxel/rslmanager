//
//  SchemeAsbestosData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 02/01/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "SchemeAsbestosData+CoreDataProperties.h"

@implementation SchemeAsbestosData (CoreDataProperties)

+ (NSFetchRequest<SchemeAsbestosData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SchemeAsbestosData"];
}

@dynamic asbestosId;
@dynamic asbRiskLevelDesc;
@dynamic riskDesc;
@dynamic riskLevel;
@dynamic type;
@dynamic schemeAsbestosDataToScheme;

@end
