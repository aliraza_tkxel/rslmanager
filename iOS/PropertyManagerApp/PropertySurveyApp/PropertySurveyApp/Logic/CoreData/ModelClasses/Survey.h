//
//  Survey.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, SurveyData;

@interface Survey : NSManagedObject

@property (nonatomic, retain) NSString * surveyJSON;
@property (nonatomic, retain) Appointment *surveyToAppointment;
@property (nonatomic, retain) NSSet *surveyToSurveyData;
@end

@interface Survey (CoreDataGeneratedAccessors)

- (void)addSurveyToSurveyDataObject:(SurveyData *)value;
- (void)removeSurveyToSurveyDataObject:(SurveyData *)value;
- (void)addSurveyToSurveyData:(NSSet *)values;
- (void)removeSurveyToSurveyData:(NSSet *)values;

@end
