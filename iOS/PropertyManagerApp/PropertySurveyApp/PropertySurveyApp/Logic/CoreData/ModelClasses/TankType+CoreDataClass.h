//
//  TankType+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface TankType : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TankType+CoreDataProperties.h"
