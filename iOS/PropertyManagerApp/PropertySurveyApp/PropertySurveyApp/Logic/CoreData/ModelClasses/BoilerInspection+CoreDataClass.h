//
//  BoilerInspection+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 17/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Boiler;

NS_ASSUME_NONNULL_BEGIN

@interface BoilerInspection : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BoilerInspection+CoreDataProperties.h"
