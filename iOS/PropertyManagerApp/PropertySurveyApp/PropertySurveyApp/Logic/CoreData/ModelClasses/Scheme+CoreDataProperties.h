//
//  Scheme+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 02/01/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "Scheme+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Scheme (CoreDataProperties)

+ (NSFetchRequest<Scheme *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSString *blockName;
@property (nullable, nonatomic, copy) NSString *county;
@property (nullable, nonatomic, copy) NSString *postCode;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSString *schemeName;
@property (nullable, nonatomic, copy) NSString *townCity;
@property (nullable, nonatomic, retain) Appointment *schemeToAppointment;
@property (nullable, nonatomic, retain) NSSet<SchemeAsbestosData *> *schemeToSchemeAsbestosData;

@end

@interface Scheme (CoreDataGeneratedAccessors)

- (void)addSchemeToSchemeAsbestosDataObject:(SchemeAsbestosData *)value;
- (void)removeSchemeToSchemeAsbestosDataObject:(SchemeAsbestosData *)value;
- (void)addSchemeToSchemeAsbestosData:(NSSet<SchemeAsbestosData *> *)values;
- (void)removeSchemeToSchemeAsbestosData:(NSSet<SchemeAsbestosData *> *)values;

@end

NS_ASSUME_NONNULL_END
