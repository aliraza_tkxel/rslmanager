//
//  SolarInspection+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 29/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SolarInspection+CoreDataProperties.h"

@implementation SolarInspection (CoreDataProperties)

+ (NSFetchRequest<SolarInspection *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"SolarInspection"];
}

@dynamic antiScaldingControl;
@dynamic antiScaldingControlDetail;
@dynamic backPressure;
@dynamic calculationRate;
@dynamic checkDirection;
@dynamic checkElectricalControl;
@dynamic deltaOff;
@dynamic deltaOn;
@dynamic directionDetail;
@dynamic electricalControlDetail;
@dynamic freezingTemp;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionId;
@dynamic isInspected;
@dynamic maxTemperature;
@dynamic minPressure;
@dynamic systemPressure;
@dynamic thermostatTemperature;
@dynamic vesselCapacity;
@dynamic vesselProtectionInstalled;
@dynamic solarInspectionToHeating;
@dynamic solarInspectionToPictures;

@end
