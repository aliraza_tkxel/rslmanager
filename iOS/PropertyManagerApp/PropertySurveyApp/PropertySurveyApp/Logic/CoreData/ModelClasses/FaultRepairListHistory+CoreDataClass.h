//
//  FaultRepairListHistory+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FaultJobSheet;

NS_ASSUME_NONNULL_BEGIN

@interface FaultRepairListHistory : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FaultRepairListHistory+CoreDataProperties.h"
