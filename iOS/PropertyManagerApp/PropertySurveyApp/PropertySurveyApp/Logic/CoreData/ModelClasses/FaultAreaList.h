//
//  FaultAreaList.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2016-03-17.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface FaultAreaList : NSManagedObject

@property (nullable, nonatomic, retain) NSNumber *faultAreaId;
@property (nullable, nonatomic, retain) NSString *faultAreaName;

@end
