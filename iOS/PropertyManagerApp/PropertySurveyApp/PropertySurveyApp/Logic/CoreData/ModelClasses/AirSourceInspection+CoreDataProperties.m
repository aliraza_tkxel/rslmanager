//
//  AirSourceInspection+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AirSourceInspection+CoreDataProperties.h"

@implementation AirSourceInspection (CoreDataProperties)

+ (NSFetchRequest<AirSourceInspection *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"AirSourceInspection"];
}

@dynamic checkElectricConnection;
@dynamic checkFuse;
@dynamic checkOilLeak;
@dynamic checkSupplementaryBonding;
@dynamic checkThermostat;
@dynamic checkValves;
@dynamic checkWaterPipework;
@dynamic checkWaterSupplyTurnedOff;
@dynamic checkWaterSupplyTurnedOn;
@dynamic connectionDetail;
@dynamic fuseDetail;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionId;
@dynamic isInspected;
@dynamic oilLeakDetail;
@dynamic pipeworkDetail;
@dynamic supplementaryBondingDetail;
@dynamic thermostatDetail;
@dynamic turnedOffDetail;
@dynamic turnedOnDetail;
@dynamic valveDetail;
@dynamic airsourceInspectionToHeating;
@dynamic airsourceInspectionToPictures;

@end
