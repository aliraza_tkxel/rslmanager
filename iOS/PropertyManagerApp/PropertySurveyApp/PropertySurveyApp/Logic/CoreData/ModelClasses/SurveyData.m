//
//  SurveyData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "SurveyData.h"
#import "FormData.h"
#import "Survey.h"


@implementation SurveyData

@dynamic appointmentId;
@dynamic customerId;
@dynamic itemId;
@dynamic propertyId;
@dynamic surveyFormName;
@dynamic surveyNotesDate;
@dynamic surveyNotesDetail;
@dynamic surveyDataToFormData;
@dynamic surveyDataToSurvey;

@end
