//
//  BoilerManufacturer+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerManufacturer+CoreDataProperties.h"

@implementation BoilerManufacturer (CoreDataProperties)

+ (NSFetchRequest<BoilerManufacturer *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BoilerManufacturer"];
}

@dynamic isAlternativeHeating;
@dynamic isSchemeBlock;
@dynamic manufacturerDescription;
@dynamic manufacturerId;

@end
