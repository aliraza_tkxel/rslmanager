//
//  PropertyAttributesNotes+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/1/16.
//  Copyright © 2016 TkXel. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PropertyAttributesNotes.h"

NS_ASSUME_NONNULL_BEGIN

@interface PropertyAttributesNotes (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *attributeNotes;
@property (nullable, nonatomic, retain) NSString *attributePath;
@property (nullable, nonatomic, retain) Appointment *propertyAttributesNotesToAppointment;

@end

NS_ASSUME_NONNULL_END
