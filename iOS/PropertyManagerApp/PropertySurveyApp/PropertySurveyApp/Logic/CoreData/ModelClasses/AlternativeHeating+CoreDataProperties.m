//
//  AlternativeHeating+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AlternativeHeating+CoreDataProperties.h"

@implementation AlternativeHeating (CoreDataProperties)

+ (NSFetchRequest<AlternativeHeating *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"AlternativeHeating"];
}

@dynamic blockId;
@dynamic certificateIssued;
@dynamic certificateName;
@dynamic certificateNumber;
@dynamic certificateRenewal;
@dynamic heatingFuel;
@dynamic heatingId;
@dynamic heatingName;
@dynamic heatingType;
@dynamic installedDate;
@dynamic isInspected;
@dynamic itemId;
@dynamic location;
@dynamic manufacturerId;
@dynamic model;
@dynamic propertyId;
@dynamic schemeId;
@dynamic serialNumber;
@dynamic solarTypeId;
@dynamic alternativeHeatingToAirsourceInspection;
@dynamic alternativeHeatingToManufacturer;
@dynamic alternativeHeatingToMVHRInspection;
@dynamic alternativeHeatingToProperty;
@dynamic alternativeHeatingToSolarInspection;
@dynamic alternativeHeatingToSolarType;

@end
