//
//  DefectJobSheet+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 21/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "DefectJobSheet+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DefectJobSheet (CoreDataProperties)

+ (NSFetchRequest<DefectJobSheet *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *appliance;
@property (nullable, nonatomic, copy) NSNumber *applianceId;
@property (nullable, nonatomic, copy) NSNumber *boilerTypeId;
@property (nullable, nonatomic, copy) NSNumber *defectCategoryId;
@property (nullable, nonatomic, copy) NSString *defectCompletionNotes;
@property (nullable, nonatomic, copy) NSDate *defectDate;
@property (nullable, nonatomic, copy) NSNumber *defectId;
@property (nullable, nonatomic, copy) NSString *defectNotes;
@property (nullable, nonatomic, copy) NSNumber *defectRecordedBy;
@property (nullable, nonatomic, copy) NSString *gasCouncilNumber;
@property (nullable, nonatomic, copy) NSString *inspectionRef;
@property (nullable, nonatomic, copy) NSNumber *isAdviceNoteIssued;
@property (nullable, nonatomic, copy) NSNumber *isCustomerHaveHeating;
@property (nullable, nonatomic, copy) NSNumber *isCustomerHaveHotWater;
@property (nullable, nonatomic, copy) NSNumber *isDisconnected;
@property (nullable, nonatomic, copy) NSNumber *isHeatersLeft;
@property (nullable, nonatomic, copy) NSNumber *isRemedialActionTaken;
@property (nullable, nonatomic, copy) NSNumber *isTwoPersonsJob;
@property (nullable, nonatomic, copy) NSString *make;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSNumber *numberOfHeatersLeft;
@property (nullable, nonatomic, copy) NSDate *partsDueDate;
@property (nullable, nonatomic, copy) NSString *partsLocation;
@property (nullable, nonatomic, copy) NSNumber *partsOrderedBy;
@property (nullable, nonatomic, copy) NSString *remedialActionNotes;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSString *trade;
@property (nullable, nonatomic, copy) NSNumber *warningTagFixed;
@property (nullable, nonatomic, copy) NSNumber *heatingId;

@end

NS_ASSUME_NONNULL_END
