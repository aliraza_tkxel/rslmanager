//
//  Boiler+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Boiler+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Boiler (CoreDataProperties)

+ (NSFetchRequest<Boiler *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSString *boilerName;
@property (nullable, nonatomic, copy) NSNumber *boilerTypeId;
@property (nullable, nonatomic, copy) NSNumber *flueTypeId;
@property (nullable, nonatomic, copy) NSDate *gasketReplacementDate;
@property (nullable, nonatomic, copy) NSString *gcNumber;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *isLandlordAppliance;
@property (nullable, nonatomic, copy) NSDate *lastReplaced;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSNumber *manufacturerId;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSDate *replacementDue;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, retain) BoilerInspection *boilerToBoilerInspection;
@property (nullable, nonatomic, retain) BoilerManufacturer *boilerToBoilerManufacturer;
@property (nullable, nonatomic, retain) BoilerType *boilerToBoilerType;
@property (nullable, nonatomic, retain) NSSet<Defect *> *boilerToDefect;
@property (nullable, nonatomic, retain) FlueType *boilerToFlueType;
@property (nullable, nonatomic, retain) InstallationPipework *boilerToGasInstallationPipeWork;
@property (nullable, nonatomic, retain) Property *boilerToProperty;

@end

@interface Boiler (CoreDataGeneratedAccessors)

- (void)addBoilerToDefectObject:(Defect *)value;
- (void)removeBoilerToDefectObject:(Defect *)value;
- (void)addBoilerToDefect:(NSSet<Defect *> *)values;
- (void)removeBoilerToDefect:(NSSet<Defect *> *)values;

@end

NS_ASSUME_NONNULL_END
