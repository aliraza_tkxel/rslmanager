//
//  AbortReason+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Abdul Rehman on 23/02/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AbortReason+CoreDataProperties.h"

@implementation AbortReason (CoreDataProperties)

+ (NSFetchRequest<AbortReason *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AbortReason"];
}

@dynamic reasonId;
@dynamic reason;

@end
