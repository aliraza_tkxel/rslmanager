//
//  JobSheet+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "JobSheet+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface JobSheet (CoreDataProperties)

+ (NSFetchRequest<JobSheet *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *actualEndTime;
@property (nullable, nonatomic, copy) NSDate *actualStartTime;
@property (nullable, nonatomic, copy) NSDate *completionDate;
@property (nullable, nonatomic, copy) NSString *jobStatus;
@property (nullable, nonatomic, copy) NSString *jsCompletedAppVersionInfo;
@property (nullable, nonatomic, copy) NSString *jsNumber;
@property (nullable, nonatomic, copy) NSString *jsType;
@property (nullable, nonatomic, copy) NSNumber *isLegionella;
@property (nullable, nonatomic, retain) NSSet<JobPauseData *> *jobPauseData;
@property (nullable, nonatomic, retain) Appointment *jobSheetToAppointment;
@property (nullable, nonatomic, retain) NSSet<RepairPictures *> *jobSheetToRepairImages;

@end

@interface JobSheet (CoreDataGeneratedAccessors)

- (void)addJobPauseDataObject:(JobPauseData *)value;
- (void)removeJobPauseDataObject:(JobPauseData *)value;
- (void)addJobPauseData:(NSSet<JobPauseData *> *)values;
- (void)removeJobPauseData:(NSSet<JobPauseData *> *)values;

- (void)addJobSheetToRepairImagesObject:(RepairPictures *)value;
- (void)removeJobSheetToRepairImagesObject:(RepairPictures *)value;
- (void)addJobSheetToRepairImages:(NSSet<RepairPictures *> *)values;
- (void)removeJobSheetToRepairImages:(NSSet<RepairPictures *> *)values;

@end

NS_ASSUME_NONNULL_END
