//
//  JobPauseData.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobPauseReason, JobSheet;

@interface JobPauseData : NSManagedObject

@property (nonatomic, retain) NSString * actionType;
@property (nonatomic, retain) NSDate * pauseDate;
@property (nonatomic, retain) NSNumber * pausedBy;
@property (nonatomic, retain) NSString * pauseNote;
@property (nonatomic, retain) JobSheet *jobSheet;
@property (nonatomic, retain) JobPauseReason *pauseReason;

@end
