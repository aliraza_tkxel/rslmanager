//
//  AlternativeFuelPicture+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AlternativeFuelPicture+CoreDataProperties.h"

@implementation AlternativeFuelPicture (CoreDataProperties)

+ (NSFetchRequest<AlternativeFuelPicture *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"AlternativeFuelPicture"];
}

@dynamic appointmentId;
@dynamic blockId;
@dynamic createdBy;
@dynamic createdOn;
@dynamic deletedPicture;
@dynamic imageIdentifier;
@dynamic imagePath;
@dynamic isDefault;
@dynamic itemId;
@dynamic pictureId;
@dynamic propertyId;
@dynamic propertyPictureId;
@dynamic propertyPictureName;
@dynamic schemeId;
@dynamic surveyId;
@dynamic syncStatus;
@dynamic heatingId;
@dynamic alternateFuelPictureToFireServiceInspection;
@dynamic alternateFuelPictureToProperty;
@dynamic alternativeFuelPictureToAirSourceInspection;
@dynamic alternativeFuelPictureToMVHRInspection;
@dynamic alternativeFuelPictureToSolarInspection;

@end
