//
//  Detector+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Detector+CoreDataProperties.h"

@implementation Detector (CoreDataProperties)

+ (NSFetchRequest<Detector *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Detector"];
}

@dynamic batteryReplaced;
@dynamic detectorId;
@dynamic detectorType;
@dynamic detectorTypeId;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic installedDate;
@dynamic isInspected;
@dynamic isLandlordsDetector;
@dynamic isPassed;
@dynamic lastTestedDate;
@dynamic location;
@dynamic manufacturer;
@dynamic notes;
@dynamic powerTypeId;
@dynamic propertyID;
@dynamic serialNumber;
@dynamic schemeId;
@dynamic blockId;
@dynamic detectorDefects;
@dynamic detectorToPowerType;
@dynamic detectorToProperty;

@end
