//
//  SolarType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SolarType+CoreDataProperties.h"

@implementation SolarType (CoreDataProperties)

+ (NSFetchRequest<SolarType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"SolarType"];
}

@dynamic value;
@dynamic lookupId;

@end
