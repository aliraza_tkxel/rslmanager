//
//  MeterType.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "MeterType.h"


@implementation MeterType

@dynamic deviceType;
@dynamic meterId;
@dynamic meterType;

@end
