//
//  MeterData.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, MeterType;

@interface MeterData : NSManagedObject

@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSString * meterLocation;
@property (nonatomic, retain) NSNumber * meterReading;
@property (nonatomic, retain) NSDate * reletDate;
@property (nonatomic, retain) NSString * tenantType;
@property (nonatomic, retain) NSDate * terminationDate;
@property (nonatomic, retain) Appointment *meterDataToAppointment;
@property (nonatomic, retain) MeterType *meterDataToMeterType;

@end
