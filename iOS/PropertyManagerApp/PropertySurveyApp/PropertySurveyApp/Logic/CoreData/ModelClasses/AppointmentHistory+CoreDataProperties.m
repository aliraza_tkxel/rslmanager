//
//  AppointmentHistory+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AppointmentHistory+CoreDataProperties.h"

@implementation AppointmentHistory (CoreDataProperties)

+ (NSFetchRequest<AppointmentHistory *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"AppointmentHistory"];
}

@dynamic actionBy;
@dynamic actionDate;
@dynamic actionType;

@end
