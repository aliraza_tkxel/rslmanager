//
//  OilFlueType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilFlueType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface OilFlueType (CoreDataProperties)

+ (NSFetchRequest<OilFlueType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *value;
@property (nullable, nonatomic, copy) NSNumber *lookupId;

@end

NS_ASSUME_NONNULL_END
