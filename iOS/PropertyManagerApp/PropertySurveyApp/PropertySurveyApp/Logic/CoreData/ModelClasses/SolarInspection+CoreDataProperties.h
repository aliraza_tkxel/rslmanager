//
//  SolarInspection+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 29/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SolarInspection+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SolarInspection (CoreDataProperties)

+ (NSFetchRequest<SolarInspection *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *antiScaldingControl;
@property (nullable, nonatomic, copy) NSString *antiScaldingControlDetail;
@property (nullable, nonatomic, copy) NSNumber *backPressure;
@property (nullable, nonatomic, copy) NSNumber *calculationRate;
@property (nullable, nonatomic, copy) NSNumber *checkDirection;
@property (nullable, nonatomic, copy) NSNumber *checkElectricalControl;
@property (nullable, nonatomic, copy) NSNumber *deltaOff;
@property (nullable, nonatomic, copy) NSNumber *deltaOn;
@property (nullable, nonatomic, copy) NSString *directionDetail;
@property (nullable, nonatomic, copy) NSString *electricalControlDetail;
@property (nullable, nonatomic, copy) NSNumber *freezingTemp;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *maxTemperature;
@property (nullable, nonatomic, copy) NSNumber *minPressure;
@property (nullable, nonatomic, copy) NSNumber *systemPressure;
@property (nullable, nonatomic, copy) NSString *thermostatTemperature;
@property (nullable, nonatomic, copy) NSNumber *vesselCapacity;
@property (nullable, nonatomic, copy) NSString *vesselProtectionInstalled;
@property (nullable, nonatomic, retain) AlternativeHeating *solarInspectionToHeating;
@property (nullable, nonatomic, retain) NSSet<AlternativeFuelPicture *> *solarInspectionToPictures;

@end

@interface SolarInspection (CoreDataGeneratedAccessors)

- (void)addSolarInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)removeSolarInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)addSolarInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;
- (void)removeSolarInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
