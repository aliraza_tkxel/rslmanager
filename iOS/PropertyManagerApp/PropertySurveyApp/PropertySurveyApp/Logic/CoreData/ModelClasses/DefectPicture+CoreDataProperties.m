//
//  DefectPicture+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "DefectPicture+CoreDataProperties.h"

@implementation DefectPicture (CoreDataProperties)

+ (NSFetchRequest<DefectPicture *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"DefectPicture"];
}

@dynamic createdBy;
@dynamic createdOn;
@dynamic defectPictureId;
@dynamic imageIdentifier;
@dynamic imagePath;
@dynamic synchStatus;
@dynamic heatingId;
@dynamic itemId;
@dynamic defectPictureToDefect;

@end
