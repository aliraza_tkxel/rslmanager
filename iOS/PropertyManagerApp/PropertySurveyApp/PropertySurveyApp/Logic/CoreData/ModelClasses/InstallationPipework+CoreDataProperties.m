//
//  InstallationPipework+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "InstallationPipework+CoreDataProperties.h"

@implementation InstallationPipework (CoreDataProperties)

+ (NSFetchRequest<InstallationPipework *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstallationPipework"];
}

@dynamic emergencyControl;
@dynamic equipotentialBonding;
@dynamic gasTightnessTest;
@dynamic inspectionDate;
@dynamic inspectionID;
@dynamic isInspected;
@dynamic visualInspection;
@dynamic gasInstallationPipeworkToBoiler;

@end
