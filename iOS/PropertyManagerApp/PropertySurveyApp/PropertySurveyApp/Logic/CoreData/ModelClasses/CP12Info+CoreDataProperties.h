//
//  CP12Info+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "CP12Info+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CP12Info (CoreDataProperties)

+ (NSFetchRequest<CP12Info *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cp12Document;
@property (nullable, nonatomic, copy) NSString *cp12Number;
@property (nullable, nonatomic, copy) NSNumber *cp12Passed;
@property (nullable, nonatomic, copy) NSDate *dateTimeStamp;
@property (nullable, nonatomic, copy) NSString *documentType;
@property (nullable, nonatomic, copy) NSNumber *inspectionCarried;
@property (nullable, nonatomic, copy) NSNumber *issuedBy;
@property (nullable, nonatomic, copy) NSDate *issuedDate;
@property (nullable, nonatomic, copy) NSString *issuedDateString;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSNumber *jsgNumber;
@property (nullable, nonatomic, copy) NSNumber *lsgrid;
@property (nullable, nonatomic, copy) NSString *notes;
@property (nullable, nonatomic, copy) NSString *propertyID;
@property (nullable, nonatomic, copy) NSDate *receivedDate;
@property (nullable, nonatomic, copy) NSString *receivedDateString;
@property (nullable, nonatomic, copy) NSString *receivedOnBehalfOf;
@property (nullable, nonatomic, copy) NSNumber *tenantHanded;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, retain) Appointment *cp12InfoToAppointment;

@end

NS_ASSUME_NONNULL_END
