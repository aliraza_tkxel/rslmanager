//
//  Trade.h
//  PropertySurveyApp
//
//  Created by aqib javed on 05/08/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Trade : NSManagedObject

@property (nonatomic, retain) NSNumber * tradeId;
@property (nonatomic, retain) NSString * trade;

@end
