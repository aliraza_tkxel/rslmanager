//
//  FlueType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "FlueType+CoreDataProperties.h"

@implementation FlueType (CoreDataProperties)

+ (NSFetchRequest<FlueType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"FlueType"];
}

@dynamic flueTypeDescription;
@dynamic flueTypeId;

@end
