//
//  JobSheet+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "JobSheet+CoreDataProperties.h"

@implementation JobSheet (CoreDataProperties)

+ (NSFetchRequest<JobSheet *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"JobSheet"];
}

@dynamic actualEndTime;
@dynamic actualStartTime;
@dynamic completionDate;
@dynamic jobStatus;
@dynamic jsCompletedAppVersionInfo;
@dynamic jsNumber;
@dynamic jsType;
@dynamic isLegionella;
@dynamic jobPauseData;
@dynamic jobSheetToAppointment;
@dynamic jobSheetToRepairImages;

@end
