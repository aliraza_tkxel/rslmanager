//
//  Address.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface Address : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * addressID;
@property (nonatomic, retain) NSString * houseNo;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * townCity;
@property (nonatomic, retain) Customer *addressToCustomer;

@end
