//
//  MeterData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "MeterData.h"
#import "Appointment+CoreDataClass.h"
#import "MeterType.h"


@implementation MeterData

@dynamic deviceType;
@dynamic meterLocation;
@dynamic meterReading;
@dynamic reletDate;
@dynamic tenantType;
@dynamic terminationDate;
@dynamic meterDataToAppointment;
@dynamic meterDataToMeterType;

@end
