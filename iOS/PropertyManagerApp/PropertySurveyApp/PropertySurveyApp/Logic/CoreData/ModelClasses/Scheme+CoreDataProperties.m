//
//  Scheme+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 02/01/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "Scheme+CoreDataProperties.h"

@implementation Scheme (CoreDataProperties)

+ (NSFetchRequest<Scheme *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Scheme"];
}

@dynamic blockId;
@dynamic blockName;
@dynamic county;
@dynamic postCode;
@dynamic schemeId;
@dynamic schemeName;
@dynamic townCity;
@dynamic schemeToAppointment;
@dynamic schemeToSchemeAsbestosData;

@end
