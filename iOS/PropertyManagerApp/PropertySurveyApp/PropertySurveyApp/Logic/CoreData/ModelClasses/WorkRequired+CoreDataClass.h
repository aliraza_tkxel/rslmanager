//
//  WorkRequired+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/24/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FaultAreaList, VoidData;

NS_ASSUME_NONNULL_BEGIN

@interface WorkRequired : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "WorkRequired+CoreDataProperties.h"
