//
//  VoidWorks.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-11.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface VoidWorks : NSManagedObject

@property (nonatomic, retain) NSDate * terminationDate;
@property (nonatomic, retain) NSDate * reletDate;
@property (nonatomic, retain) NSString * jsvNumber;
@property (nonatomic, retain) Appointment *voidWorksToAppointment;

@end
