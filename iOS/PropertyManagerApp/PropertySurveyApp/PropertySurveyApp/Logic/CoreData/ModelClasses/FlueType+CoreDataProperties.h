//
//  FlueType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "FlueType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FlueType (CoreDataProperties)

+ (NSFetchRequest<FlueType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *flueTypeDescription;
@property (nullable, nonatomic, copy) NSNumber *flueTypeId;

@end

NS_ASSUME_NONNULL_END
