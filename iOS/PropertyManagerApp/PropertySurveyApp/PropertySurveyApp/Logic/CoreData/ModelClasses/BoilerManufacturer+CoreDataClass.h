//
//  BoilerManufacturer+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BoilerManufacturer : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BoilerManufacturer+CoreDataProperties.h"
