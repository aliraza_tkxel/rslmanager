//
//  AppInfoData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 7/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AppInfoData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AppInfoData (CoreDataProperties)

+ (NSFetchRequest<AppInfoData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *isCardLeft;
@property (nullable, nonatomic, copy) NSNumber *totalAppointments;
@property (nullable, nonatomic, copy) NSNumber *totalNoEntries;
@property (nullable, nonatomic, retain) Appointment *appInfoDataToAppointment;

@end

NS_ASSUME_NONNULL_END
