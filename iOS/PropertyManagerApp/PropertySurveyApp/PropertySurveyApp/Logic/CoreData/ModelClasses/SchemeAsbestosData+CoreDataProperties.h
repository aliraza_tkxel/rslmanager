//
//  SchemeAsbestosData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by aqib javed on 02/01/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "SchemeAsbestosData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SchemeAsbestosData (CoreDataProperties)

+ (NSFetchRequest<SchemeAsbestosData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *asbestosId;
@property (nullable, nonatomic, copy) NSString *asbRiskLevelDesc;
@property (nullable, nonatomic, copy) NSString *riskDesc;
@property (nullable, nonatomic, copy) NSString *riskLevel;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, retain) Scheme *schemeAsbestosDataToScheme;

@end

NS_ASSUME_NONNULL_END
