//
//  MVHRInspection+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "MVHRInspection+CoreDataProperties.h"

@implementation MVHRInspection (CoreDataProperties)

+ (NSFetchRequest<MVHRInspection *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"MVHRInspection"];
}

@dynamic airFlowDetail;
@dynamic checkAirFlow;
@dynamic checkFilters;
@dynamic ductingDetail;
@dynamic ductingInspection;
@dynamic filterDetail;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionId;
@dynamic isInspected;
@dynamic mvhrInspectionToHeating;
@dynamic mvhrInspectionToPictures;

@end
