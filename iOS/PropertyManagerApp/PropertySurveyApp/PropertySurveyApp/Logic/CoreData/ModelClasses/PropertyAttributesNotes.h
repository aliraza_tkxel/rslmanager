//
//  PropertyAttributesNotes.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/1/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

NS_ASSUME_NONNULL_BEGIN

@interface PropertyAttributesNotes : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "PropertyAttributesNotes+CoreDataProperties.h"
