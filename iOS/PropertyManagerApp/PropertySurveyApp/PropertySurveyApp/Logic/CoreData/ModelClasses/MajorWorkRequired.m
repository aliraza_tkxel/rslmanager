//
//  MajorWorkRequired.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-25.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "MajorWorkRequired.h"
#import "VoidData.h"


@implementation MajorWorkRequired

@dynamic component;
@dynamic componentId;
@dynamic condition;
@dynamic replacementDue;
@dynamic notes;
@dynamic majorWorkRequiredToVoidData;

@end
