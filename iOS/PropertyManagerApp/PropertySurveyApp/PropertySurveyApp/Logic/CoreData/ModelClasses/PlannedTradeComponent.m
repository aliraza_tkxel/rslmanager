//
//  PlannedTradeComponent.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PlannedTradeComponent.h"
#import "Appointment+CoreDataClass.h"
#import "JobPauseData.h"


@implementation PlannedTradeComponent

@dynamic adaptation;
@dynamic adaptationId;
@dynamic completionDate;
@dynamic componentId;
@dynamic componentName;
@dynamic componentTradeId;
@dynamic duration;
@dynamic durationUnit;
@dynamic jobStatus;
@dynamic jsnNotes;
@dynamic jsNumber;
@dynamic location;
@dynamic locationId;
@dynamic pmoDescription;
@dynamic reportedDate;
@dynamic sortingOrder;
@dynamic tradeDescription;
@dynamic tradeId;
@dynamic jobPauseData;
@dynamic tradeComponentToAppointment;

@end
