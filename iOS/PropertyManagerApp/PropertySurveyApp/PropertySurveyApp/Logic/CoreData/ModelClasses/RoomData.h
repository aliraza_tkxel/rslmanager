//
//  RoomData.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/29/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoomData : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "RoomData+CoreDataProperties.h"
