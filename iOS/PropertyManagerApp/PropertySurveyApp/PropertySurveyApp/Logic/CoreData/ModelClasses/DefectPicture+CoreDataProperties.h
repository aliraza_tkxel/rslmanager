//
//  DefectPicture+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "DefectPicture+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DefectPicture (CoreDataProperties)

+ (NSFetchRequest<DefectPicture *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *createdBy;
@property (nullable, nonatomic, copy) NSDate *createdOn;
@property (nullable, nonatomic, copy) NSNumber *defectPictureId;
@property (nullable, nonatomic, copy) NSString *imageIdentifier;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nullable, nonatomic, copy) NSNumber *synchStatus;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, retain) Defect *defectPictureToDefect;

@end

NS_ASSUME_NONNULL_END
