//
//  Meter+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Meter+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Meter (CoreDataProperties)

+ (NSFetchRequest<Meter *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *deviceType;
@property (nullable, nonatomic, copy) NSNumber *deviceTypeId;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSDate *installedDate;
@property (nullable, nonatomic, copy) NSNumber *isCapped;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *isPassed;
@property (nullable, nonatomic, copy) NSString *lastReading;
@property (nullable, nonatomic, copy) NSDate *lastReadingDate;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSString *manufacturer;
@property (nullable, nonatomic, copy) NSNumber *meterTypeId;
@property (nullable, nonatomic, copy) NSString *notes;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSString *reading;
@property (nullable, nonatomic, copy) NSDate *readingDate;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, retain) Property *meterToProperty;

@end

NS_ASSUME_NONNULL_END
