//
//  PauseReason.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PauseReason.h"


@implementation PauseReason

@dynamic pauseId;
@dynamic reason;

@end
