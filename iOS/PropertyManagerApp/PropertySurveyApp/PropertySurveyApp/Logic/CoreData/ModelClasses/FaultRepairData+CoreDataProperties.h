//
//  FaultRepairData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/24/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "FaultRepairData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FaultRepairData (CoreDataProperties)

+ (NSFetchRequest<FaultRepairData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *faultRepairDescription;
@property (nullable, nonatomic, copy) NSNumber *faultRepairID;
@property (nullable, nonatomic, copy) NSNumber *gross;
@property (nullable, nonatomic, copy) NSNumber *isAssociated;
@property (nullable, nonatomic, copy) NSString *repairSectionIdentifier;
@property (nullable, nonatomic, retain) FaultJobSheet *faultRepairDataToFaultJobSheet;

@end

NS_ASSUME_NONNULL_END
