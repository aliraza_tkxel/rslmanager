//
//  FaultDetailList.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-17.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FaultDetailList : NSManagedObject

@property (nonatomic, retain) NSString * faultDescription;
@property (nonatomic, retain) NSNumber * faultId;
@property (nonatomic, retain) NSNumber * gross;
@property (nonatomic, retain) NSNumber * isRecharge;
@property (nonatomic, retain) NSString * priorityName;
@property (nonatomic, retain) NSString * responseTime;

@end
