//
//  PropertyAsbestosData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 21/12/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PropertyAsbestosData+CoreDataProperties.h"

@implementation PropertyAsbestosData (CoreDataProperties)

+ (NSFetchRequest<PropertyAsbestosData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PropertyAsbestosData"];
}

@dynamic asbestosId;
@dynamic asbRiskLevelDesc;
@dynamic riskDesc;
@dynamic type;
@dynamic riskLevel;
@dynamic propertyAsbestosDataToProperty;

@end
