//
//  Outbox.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Outbox.h"


@implementation Outbox

@dynamic encodingType;
@dynamic queryString;
@dynamic requestDateTime;
@dynamic requestHeader;
@dynamic requestType;
@dynamic requestURL;

@end
