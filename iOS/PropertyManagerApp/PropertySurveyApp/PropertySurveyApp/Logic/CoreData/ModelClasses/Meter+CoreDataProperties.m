//
//  Meter+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Meter+CoreDataProperties.h"

@implementation Meter (CoreDataProperties)

+ (NSFetchRequest<Meter *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Meter"];
}

@dynamic deviceType;
@dynamic deviceTypeId;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic installedDate;
@dynamic isCapped;
@dynamic isInspected;
@dynamic isPassed;
@dynamic lastReading;
@dynamic lastReadingDate;
@dynamic location;
@dynamic manufacturer;
@dynamic meterTypeId;
@dynamic notes;
@dynamic propertyId;
@dynamic reading;
@dynamic readingDate;
@dynamic serialNumber;
@dynamic blockId;
@dynamic schemeId;
@dynamic meterToProperty;

@end
