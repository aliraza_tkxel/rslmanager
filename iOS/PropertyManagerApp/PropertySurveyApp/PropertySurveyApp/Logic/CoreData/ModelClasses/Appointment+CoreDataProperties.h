//
//  Appointment+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appointment+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Appointment (CoreDataProperties)

+ (NSFetchRequest<Appointment *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *addToCalendar;
@property (nullable, nonatomic, copy) NSString *appointmentAbortNotes;
@property (nullable, nonatomic, copy) NSString *appointmentAbortReason;
@property (nullable, nonatomic, copy) NSDate *appointmentActualEndTime;
@property (nullable, nonatomic, copy) NSDate *appointmentActualStartTime;
@property (nullable, nonatomic, copy) NSString *appointmentCalendar;
@property (nullable, nonatomic, copy) NSDate *appointmentDate;
@property (nullable, nonatomic, copy) NSString *appointmentDateSectionIdentifier;
@property (nullable, nonatomic, copy) NSDate *appointmentEndTime;
@property (nullable, nonatomic, copy) NSString *appointmentEndTimeString;
@property (nullable, nonatomic, copy) NSString *appointmentEventIdentifier;
@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSString *appointmentLocation;
@property (nullable, nonatomic, copy) NSString *appointmentNotes;
@property (nullable, nonatomic, copy) NSNumber *appointmentOverdue;
@property (nullable, nonatomic, copy) NSString *appointmentShift;
@property (nullable, nonatomic, copy) NSDate *appointmentStartTime;
@property (nullable, nonatomic, copy) NSString *appointmentStartTimeString;
@property (nullable, nonatomic, copy) NSString *appointmentStatus;
@property (nullable, nonatomic, copy) NSNumber *appointmentStatusSynced;
@property (nullable, nonatomic, copy) NSString *appointmentTitle;
@property (nullable, nonatomic, copy) NSString *appointmentType;
@property (nullable, nonatomic, copy) NSString *aptCompletedAppVersionInfo;
@property (nullable, nonatomic, copy) NSNumber *assignedTo;
@property (nullable, nonatomic, copy) NSNumber *createdBy;
@property (nullable, nonatomic, copy) NSString *createdByPerson;
@property (nullable, nonatomic, copy) NSDate *creationDate;
@property (nullable, nonatomic, copy) NSNumber *defaultCustomerId;
@property (nullable, nonatomic, copy) NSNumber *defaultCustomerIndex;
@property (nullable, nonatomic, copy) NSString *failedReason;
@property (nullable, nonatomic, copy) NSNumber *isMiscAppointment;
@property (nullable, nonatomic, copy) NSNumber *isModified;
@property (nullable, nonatomic, copy) NSNumber *isOfflinePrepared;
@property (nullable, nonatomic, copy) NSNumber *isSurveyChanged;
@property (nullable, nonatomic, copy) NSNumber *journalHistoryId;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSNumber *jsgNumber;
@property (nullable, nonatomic, copy) NSDate *loggedDate;
@property (nullable, nonatomic, copy) NSString *noEntryNotes;
@property (nullable, nonatomic, copy) NSDate *repairCompletionDateTime;
@property (nullable, nonatomic, copy) NSString *repairNotes;
@property (nullable, nonatomic, retain) NSObject *surveyCheckListForm;
@property (nullable, nonatomic, copy) NSString *surveyorAlert;
@property (nullable, nonatomic, copy) NSString *surveyorUserName;
@property (nullable, nonatomic, copy) NSString *surveyourAvailability;
@property (nullable, nonatomic, copy) NSString *surveyType;
@property (nullable, nonatomic, copy) NSNumber *syncStatus;
@property (nullable, nonatomic, copy) NSNumber *tenancyId;
@property (nullable, nonatomic, copy) NSString *heatingFuel;
@property (nullable, nonatomic, retain) AppInfoData *appointmentToAppInfoData;
@property (nullable, nonatomic, retain) NSSet<AppointmentHistory *> *appointmentToAppointmentHistory;
@property (nullable, nonatomic, retain) CP12Info *appointmentToCP12Info;
@property (nullable, nonatomic, retain) NSSet<Customer *> *appointmentToCustomer;
@property (nullable, nonatomic, retain) NSSet<FaultReported *> *appointmentToFaultReported;
@property (nullable, nonatomic, retain) NSSet<JobSheet *> *appointmentToJobSheet;
@property (nullable, nonatomic, retain) Journal *appointmentToJournal;
@property (nullable, nonatomic, retain) MeterData *appointmentToMeterData;
@property (nullable, nonatomic, retain) PlannedTradeComponent *appointmentToPlannedComponent;
@property (nullable, nonatomic, retain) Property *appointmentToProperty;
@property (nullable, nonatomic, retain) NSSet<PropertyAttributesNotes *> *appointmentToPropertyAttributesNotes;
@property (nullable, nonatomic, retain) Scheme *appointmentToScheme;
@property (nullable, nonatomic, retain) Survey *appointmentToSurvey;
@property (nullable, nonatomic, retain) User *appointmentToUser;
@property (nullable, nonatomic, retain) VoidData *appointmentToVoidData;
@property (nullable, nonatomic, retain) VoidWorks *appointmentToVoidWorks;

@end

@interface Appointment (CoreDataGeneratedAccessors)

- (void)addAppointmentToAppointmentHistoryObject:(AppointmentHistory *)value;
- (void)removeAppointmentToAppointmentHistoryObject:(AppointmentHistory *)value;
- (void)addAppointmentToAppointmentHistory:(NSSet<AppointmentHistory *> *)values;
- (void)removeAppointmentToAppointmentHistory:(NSSet<AppointmentHistory *> *)values;

- (void)addAppointmentToCustomerObject:(Customer *)value;
- (void)removeAppointmentToCustomerObject:(Customer *)value;
- (void)addAppointmentToCustomer:(NSSet<Customer *> *)values;
- (void)removeAppointmentToCustomer:(NSSet<Customer *> *)values;

- (void)addAppointmentToFaultReportedObject:(FaultReported *)value;
- (void)removeAppointmentToFaultReportedObject:(FaultReported *)value;
- (void)addAppointmentToFaultReported:(NSSet<FaultReported *> *)values;
- (void)removeAppointmentToFaultReported:(NSSet<FaultReported *> *)values;

- (void)addAppointmentToJobSheetObject:(JobSheet *)value;
- (void)removeAppointmentToJobSheetObject:(JobSheet *)value;
- (void)addAppointmentToJobSheet:(NSSet<JobSheet *> *)values;
- (void)removeAppointmentToJobSheet:(NSSet<JobSheet *> *)values;

- (void)addAppointmentToPropertyAttributesNotesObject:(PropertyAttributesNotes *)value;
- (void)removeAppointmentToPropertyAttributesNotesObject:(PropertyAttributesNotes *)value;
- (void)addAppointmentToPropertyAttributesNotes:(NSSet<PropertyAttributesNotes *> *)values;
- (void)removeAppointmentToPropertyAttributesNotes:(NSSet<PropertyAttributesNotes *> *)values;

@end

NS_ASSUME_NONNULL_END
