//
//  SProperty.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SProperty : NSManagedObject

@property (nonatomic, retain) NSString * address1;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSString * address3;
@property (nonatomic, retain) NSDate * certificateExpiry;
@property (nonatomic, retain) NSString * county;
@property (nonatomic, retain) NSString * flatNumber;
@property (nonatomic, retain) NSString * houseNumber;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSString * townCity;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * sPropertyNameSectionIdentifier;
@property (nonatomic, retain) NSString * sPropertyJSON;
@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString *primitiveSPropertyNameSectionIdentifier;

- (NSString *) fullName;
- (NSString *) fullNameWithoutTitle;
- (NSString *) SPropertyAddress;
- (NSString *) propertyShortAddress;

@end
