//
//  Customer.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Address, Appointment, CustomerRiskData, CustomerVulnerabilityData;

@interface Customer : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * customerId;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fax;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) NSString * property;
@property (nonatomic, retain) NSString * telephone;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Address *customerToAddress;
@property (nonatomic, retain) Appointment *customerToAppointment;
@property (nonatomic, retain) NSSet *customerToCustomerRiskData;
@property (nonatomic, retain) NSSet *customerToCustomerVulunarityData;

- (NSString *)  fullName;

@end

@interface Customer (CoreDataGeneratedAccessors)

- (void)addCustomerToCustomerRiskDataObject:(CustomerRiskData *)value;
- (void)removeCustomerToCustomerRiskDataObject:(CustomerRiskData *)value;
- (void)addCustomerToCustomerRiskData:(NSSet *)values;
- (void)removeCustomerToCustomerRiskData:(NSSet *)values;

- (void)addCustomerToCustomerVulunarityDataObject:(CustomerVulnerabilityData *)value;
- (void)removeCustomerToCustomerVulunarityDataObject:(CustomerVulnerabilityData *)value;
- (void)addCustomerToCustomerVulunarityData:(NSSet *)values;
- (void)removeCustomerToCustomerVulunarityData:(NSSet *)values;

@end
