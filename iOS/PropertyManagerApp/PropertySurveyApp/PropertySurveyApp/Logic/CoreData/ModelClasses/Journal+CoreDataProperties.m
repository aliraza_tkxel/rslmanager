//
//  Journal+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Journal+CoreDataProperties.h"

@implementation Journal (CoreDataProperties)

+ (NSFetchRequest<Journal *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Journal"];
}

@dynamic actionId;
@dynamic creationBy;
@dynamic creationDate;
@dynamic inspectionTypeId;
@dynamic isCurrent;
@dynamic journalHistoryId;
@dynamic journalId;
@dynamic propertyId;
@dynamic statusId;
@dynamic schemeId;
@dynamic blockId;
@dynamic journalToAppointment;

@end
