//
//  Defect+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Defect+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Defect (CoreDataProperties)

+ (NSFetchRequest<Defect *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *applianceID;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSNumber *boilerTypeId;
@property (nullable, nonatomic, copy) NSDate *defectDate;
@property (nullable, nonatomic, copy) NSNumber *defectID;
@property (nullable, nonatomic, copy) NSString *defectNotes;
@property (nullable, nonatomic, copy) NSString *defectType;
@property (nullable, nonatomic, copy) NSNumber *detectorTypeId;
@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSNumber *faultCategory;
@property (nullable, nonatomic, copy) NSString *gasCouncilNumber;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSNumber *isAdviceNoteIssued;
@property (nullable, nonatomic, copy) NSNumber *isCustomerHaveHeating;
@property (nullable, nonatomic, copy) NSNumber *isCustomerHaveHotWater;
@property (nullable, nonatomic, copy) NSNumber *isDefectIdentified;
@property (nullable, nonatomic, copy) NSNumber *isDisconnected;
@property (nullable, nonatomic, copy) NSNumber *isHeatersLeft;
@property (nullable, nonatomic, copy) NSNumber *isPartsOrdered;
@property (nullable, nonatomic, copy) NSNumber *isPartsRequired;
@property (nullable, nonatomic, copy) NSNumber *isRemedialActionTaken;
@property (nullable, nonatomic, copy) NSNumber *isTwoPersonsJob;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSNumber *numberOfHeatersLeft;
@property (nullable, nonatomic, copy) NSString *partsDescription;
@property (nullable, nonatomic, copy) NSDate *partsDueDate;
@property (nullable, nonatomic, copy) NSString *partsLocation;
@property (nullable, nonatomic, copy) NSNumber *partsOrderedBy;
@property (nullable, nonatomic, copy) NSNumber *priorityId;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSString *reasonForTwoPerson;
@property (nullable, nonatomic, copy) NSString *remedialActionNotes;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *tradeId;
@property (nullable, nonatomic, copy) NSString *warningNoteSerialNo;
@property (nullable, nonatomic, copy) NSNumber *warningTagFixed;
@property (nullable, nonatomic, retain) DefectCategory *defectCategory;
@property (nullable, nonatomic, retain) NSSet<DefectPicture *> *defectPictures;
@property (nullable, nonatomic, retain) Appliance *defectToAppliance;
@property (nullable, nonatomic, retain) Boiler *defectToBoiler;
@property (nullable, nonatomic, retain) DefectPriority *defectToDefectPriority;
@property (nullable, nonatomic, retain) Detector *defectToDetector;
@property (nullable, nonatomic, retain) Trade *defectToTrade;
@property (nullable, nonatomic, retain) OilHeating *defectToOilHeating;

@end

@interface Defect (CoreDataGeneratedAccessors)

- (void)addDefectPicturesObject:(DefectPicture *)value;
- (void)removeDefectPicturesObject:(DefectPicture *)value;
- (void)addDefectPictures:(NSSet<DefectPicture *> *)values;
- (void)removeDefectPictures:(NSSet<DefectPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
