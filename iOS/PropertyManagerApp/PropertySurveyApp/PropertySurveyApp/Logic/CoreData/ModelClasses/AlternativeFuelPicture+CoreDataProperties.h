//
//  AlternativeFuelPicture+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AlternativeFuelPicture+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AlternativeFuelPicture (CoreDataProperties)

+ (NSFetchRequest<AlternativeFuelPicture *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSNumber *createdBy;
@property (nullable, nonatomic, copy) NSDate *createdOn;
@property (nullable, nonatomic, copy) NSNumber *deletedPicture;
@property (nullable, nonatomic, copy) NSString *imageIdentifier;
@property (nullable, nonatomic, copy) NSString *imagePath;
@property (nullable, nonatomic, copy) NSNumber *isDefault;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSNumber *pictureId;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *propertyPictureId;
@property (nullable, nonatomic, copy) NSString *propertyPictureName;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *surveyId;
@property (nullable, nonatomic, copy) NSNumber *syncStatus;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, retain) OilFiringServiceInspection *alternateFuelPictureToFireServiceInspection;
@property (nullable, nonatomic, retain) Property *alternateFuelPictureToProperty;
@property (nullable, nonatomic, retain) AirSourceInspection *alternativeFuelPictureToAirSourceInspection;
@property (nullable, nonatomic, retain) MVHRInspection *alternativeFuelPictureToMVHRInspection;
@property (nullable, nonatomic, retain) SolarInspection *alternativeFuelPictureToSolarInspection;

@end

NS_ASSUME_NONNULL_END
