//
//  InstallationPipework+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "InstallationPipework+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstallationPipework (CoreDataProperties)

+ (NSFetchRequest<InstallationPipework *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *emergencyControl;
@property (nullable, nonatomic, copy) NSString *equipotentialBonding;
@property (nullable, nonatomic, copy) NSString *gasTightnessTest;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionID;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSString *visualInspection;
@property (nullable, nonatomic, retain) Boiler *gasInstallationPipeworkToBoiler;

@end

NS_ASSUME_NONNULL_END
