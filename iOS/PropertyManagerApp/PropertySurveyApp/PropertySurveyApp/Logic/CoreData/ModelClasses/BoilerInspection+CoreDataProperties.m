//
//  BoilerInspection+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 17/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerInspection+CoreDataProperties.h"

@implementation BoilerInspection (CoreDataProperties)

+ (NSFetchRequest<BoilerInspection *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BoilerInspection"];
}

@dynamic adequateVentilation;
@dynamic boilerSafeToUse;
@dynamic boilerServiced;
@dynamic combustionReading;
@dynamic fluePerformanceChecks;
@dynamic flueVisualCondition;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionId;
@dynamic isInspected;
@dynamic operatingPressure;
@dynamic operatingPressureUnit;
@dynamic safetyDeviceOperational;
@dynamic satisfactoryTermination;
@dynamic smokePellet;
@dynamic spillageTest;
@dynamic heatingId;
@dynamic boilerInspectionToBoiler;

@end
