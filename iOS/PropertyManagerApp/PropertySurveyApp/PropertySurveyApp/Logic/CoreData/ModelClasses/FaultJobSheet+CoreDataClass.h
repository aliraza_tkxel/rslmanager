//
//  FaultJobSheet+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobSheet+CoreDataClass.h"

@class FaultRepairData, FaultRepairListHistory;

NS_ASSUME_NONNULL_BEGIN

@interface FaultJobSheet : JobSheet

@end

NS_ASSUME_NONNULL_END

#import "FaultJobSheet+CoreDataProperties.h"
