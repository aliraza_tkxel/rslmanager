//
//  OilFlueType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilFlueType+CoreDataProperties.h"

@implementation OilFlueType (CoreDataProperties)

+ (NSFetchRequest<OilFlueType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"OilFlueType"];
}

@dynamic value;
@dynamic lookupId;

@end
