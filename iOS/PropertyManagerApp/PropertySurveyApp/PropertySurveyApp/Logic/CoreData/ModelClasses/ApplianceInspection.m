//
//  ApplianceInspection.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "ApplianceInspection.h"
#import "Appliance+CoreDataClass.h"


@implementation ApplianceInspection

@dynamic adequateVentilation;
@dynamic applianceSafeToUse;
@dynamic applianceServiced;
@dynamic combustionReading;
@dynamic fluePerformanceChecks;
@dynamic flueVisualCondition;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionID;
@dynamic isInspected;
@dynamic operatingPressure;
@dynamic operatingPressureUnit;
@dynamic safetyDeviceOperational;
@dynamic satisfactoryTermination;
@dynamic smokePellet;
@dynamic spillageTest;
@dynamic appliance;

@end
