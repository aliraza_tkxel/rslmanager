//
//  RepairPictures.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobSheet;

@interface RepairPictures : NSManagedObject

@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSNumber * imageId;
@property (nonatomic, retain) NSString * imageIdentifier;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * isBeforeImage;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) JobSheet *repairImagesToJobSheet;

@property (nonatomic, retain) UIImage *repairImage;

@end
