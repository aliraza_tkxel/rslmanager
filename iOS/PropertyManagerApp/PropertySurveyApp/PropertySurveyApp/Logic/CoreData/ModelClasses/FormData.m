//
//  FormData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FormData.h"
#import "SurveyData.h"


@implementation FormData

@dynamic controlType;
@dynamic isSelected;
@dynamic surveyItemParamId;
@dynamic surveyParameterId;
@dynamic surveyParamItemFieldValue;
@dynamic surveyParamName;
@dynamic surveyPramItemFieldId;
@dynamic formDataToSurveyData;

@end
