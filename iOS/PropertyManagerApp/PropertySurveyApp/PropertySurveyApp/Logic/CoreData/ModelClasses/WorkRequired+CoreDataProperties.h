//
//  WorkRequired+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/24/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "WorkRequired+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WorkRequired (CoreDataProperties)

+ (NSFetchRequest<WorkRequired *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *gross;
@property (nullable, nonatomic, copy) NSNumber *isBRSWork;
@property (nullable, nonatomic, copy) NSNumber *isTenantWork;
@property (nullable, nonatomic, copy) NSNumber *isVerified;
@property (nullable, nonatomic, copy) NSString *repairDescription;
@property (nullable, nonatomic, copy) NSNumber *repairId;
@property (nullable, nonatomic, copy) NSString *repairNotes;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSNumber *worksId;
@property (nullable, nonatomic, retain) FaultAreaList *workRequiredToFaultArea;
@property (nullable, nonatomic, retain) VoidData *workRequiredToVoidData;

@end

NS_ASSUME_NONNULL_END
