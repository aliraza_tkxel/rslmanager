//
//  Journal+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Journal+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Journal (CoreDataProperties)

+ (NSFetchRequest<Journal *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *actionId;
@property (nullable, nonatomic, copy) NSNumber *creationBy;
@property (nullable, nonatomic, copy) NSDate *creationDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionTypeId;
@property (nullable, nonatomic, copy) NSNumber *isCurrent;
@property (nullable, nonatomic, copy) NSNumber *journalHistoryId;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *statusId;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, retain) Appointment *journalToAppointment;

@end

NS_ASSUME_NONNULL_END
