//
//  FaultRepairData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/24/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "FaultRepairData+CoreDataProperties.h"

@implementation FaultRepairData (CoreDataProperties)

+ (NSFetchRequest<FaultRepairData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FaultRepairData"];
}

@dynamic faultRepairDescription;
@dynamic faultRepairID;
@dynamic gross;
@dynamic isAssociated;
@dynamic repairSectionIdentifier;
@dynamic faultRepairDataToFaultJobSheet;

@end
