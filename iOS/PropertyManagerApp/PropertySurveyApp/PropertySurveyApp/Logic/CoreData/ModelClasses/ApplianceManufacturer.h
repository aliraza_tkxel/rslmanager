//
//  ApplianceManufacturer.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ApplianceManufacturer : NSManagedObject

@property (nonatomic, retain) NSNumber * manufacturerID;
@property (nonatomic, retain) NSString * manufacturerName;

@end
