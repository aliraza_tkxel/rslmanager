//
//  WorkRequired+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/24/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "WorkRequired+CoreDataProperties.h"

@implementation WorkRequired (CoreDataProperties)

+ (NSFetchRequest<WorkRequired *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WorkRequired"];
}

@dynamic gross;
@dynamic isBRSWork;
@dynamic isTenantWork;
@dynamic isVerified;
@dynamic repairDescription;
@dynamic repairId;
@dynamic repairNotes;
@dynamic status;
@dynamic worksId;
@dynamic workRequiredToFaultArea;
@dynamic workRequiredToVoidData;

@end
