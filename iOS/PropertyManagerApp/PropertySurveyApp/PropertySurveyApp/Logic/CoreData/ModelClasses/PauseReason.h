//
//  PauseReason.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PauseReason : NSManagedObject

@property (nonatomic, retain) NSNumber * pauseId;
@property (nonatomic, retain) NSString * reason;

@end
