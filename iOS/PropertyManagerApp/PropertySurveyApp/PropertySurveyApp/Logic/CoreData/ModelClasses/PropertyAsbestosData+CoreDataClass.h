//
//  PropertyAsbestosData+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by aqib javed on 21/12/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

NS_ASSUME_NONNULL_BEGIN

@interface PropertyAsbestosData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PropertyAsbestosData+CoreDataProperties.h"
