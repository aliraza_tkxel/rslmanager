//
//  CustomerRiskData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "CustomerRiskData.h"
#import "Customer.h"


@implementation CustomerRiskData

@dynamic riskCatDesc;
@dynamic riskSubCatDesc;
@dynamic customerRiskDataToCustomer;

@end
