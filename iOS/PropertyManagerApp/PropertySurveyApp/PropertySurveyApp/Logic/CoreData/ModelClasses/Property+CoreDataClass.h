//
//  Property+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Accomodation, AlternativeHeating, Appliance, Appointment, Boiler, Detector, GeneralHeatingChecks, Meter, OilHeating, PropertyAsbestosData, PropertyPicture;

NS_ASSUME_NONNULL_BEGIN

@interface Property : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Property+CoreDataProperties.h"
