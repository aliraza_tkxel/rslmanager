//
//  Address.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Address.h"
#import "Customer.h"


@implementation Address

@dynamic address;
@dynamic addressID;
@dynamic houseNo;
@dynamic postCode;
@dynamic townCity;
@dynamic addressToCustomer;

@end
