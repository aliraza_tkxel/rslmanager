//
//  PropertyComponents.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-25.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VoidData;

@interface PropertyComponents : NSManagedObject

@property (nonatomic, retain) NSString * component;
@property (nonatomic, retain) NSString * condition;
@property (nonatomic, retain) NSDate * replacementDue;
@property (nonatomic, retain) NSNumber * componentId;
@property (nonatomic, retain) VoidData *propertyComponentsToVoidData;

@end
