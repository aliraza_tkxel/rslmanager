//
//  BurnerType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BurnerType+CoreDataProperties.h"

@implementation BurnerType (CoreDataProperties)

+ (NSFetchRequest<BurnerType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BurnerType"];
}

@dynamic lookupId;
@dynamic value;

@end
