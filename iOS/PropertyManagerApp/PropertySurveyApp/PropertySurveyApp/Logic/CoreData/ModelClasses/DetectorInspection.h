//
//  DetectorInspection.h
//  PropertySurveyApp
//
//  Created by aqib javed on 29/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Detector;

@interface DetectorInspection : NSManagedObject

@property (nonatomic, retain) NSString * detectorsTested;
@property (nonatomic, retain) NSNumber * inspectedBy;
@property (nonatomic, retain) NSDate * inspectionDate;
@property (nonatomic, retain) NSNumber * inspectionID;
@property (nonatomic, retain) NSDate * lastTested;
@property (nonatomic, retain) NSDate * batteryReplaced;
@property (nonatomic, retain) NSNumber * isPassed;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) Detector *detector;

@end
