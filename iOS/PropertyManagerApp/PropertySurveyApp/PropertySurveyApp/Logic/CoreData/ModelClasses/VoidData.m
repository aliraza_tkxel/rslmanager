//
//  VoidData.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-19.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "VoidData.h"
#import "Appointment+CoreDataClass.h"
#import "MajorWorkRequired.h"
#import "PaintPack.h"
#import "PropertyComponents.h"
#import "WorkRequired+CoreDataClass.h"


@implementation VoidData

@dynamic isAsbestosCheckRequired;
@dynamic isElectricCheckRequired;
@dynamic isEPCCheckRequired;
@dynamic isGasCheckRequired;
@dynamic isMajorWorkRequired;
@dynamic isPaintPackRequired;
@dynamic isWorksRequired;
@dynamic jsvNumber;
@dynamic reletDate;
@dynamic tenantEstimate;
@dynamic terminationDate;
@dynamic gasCheckStatus;
@dynamic gasCheckDate;
@dynamic electricCheckStatus;
@dynamic electricCheckDate;
@dynamic voidDataToAppointment;
@dynamic voidDataToMajorWorkRequired;
@dynamic voidDataToPaintPack;
@dynamic voidDataToPropertyComponents;
@dynamic voidDataToWorkRequired;

@end
