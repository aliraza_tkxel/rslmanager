//
//  MajorWorkRequired.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-25.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VoidData;

@interface MajorWorkRequired : NSManagedObject

@property (nullable, nonatomic, retain) NSString *component;
@property (nullable, nonatomic, retain) NSNumber *componentId;
@property (nullable, nonatomic, retain) NSString *condition;
@property (nullable, nonatomic, retain) NSDate *replacementDue;
@property (nullable, nonatomic, retain) NSString *notes;
@property (nullable, nonatomic, retain) VoidData *majorWorkRequiredToVoidData;

@end
