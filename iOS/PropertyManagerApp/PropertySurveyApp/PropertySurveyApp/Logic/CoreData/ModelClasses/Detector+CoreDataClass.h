//
//  Detector+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Defect, PowerType, Property;

NS_ASSUME_NONNULL_BEGIN

@interface Detector : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Detector+CoreDataProperties.h"
