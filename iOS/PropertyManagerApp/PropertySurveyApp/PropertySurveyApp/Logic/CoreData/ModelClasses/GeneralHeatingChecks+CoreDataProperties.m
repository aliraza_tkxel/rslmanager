//
//  GeneralHeatingChecks+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "GeneralHeatingChecks+CoreDataProperties.h"

@implementation GeneralHeatingChecks (CoreDataProperties)

+ (NSFetchRequest<GeneralHeatingChecks *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"GeneralHeatingChecks"];
}

@dynamic radiatorCondition;
@dynamic heatingControl;
@dynamic systemUsage;
@dynamic generalHeating;
@dynamic accessIssues;
@dynamic accessIssueNotes;
@dynamic confirmation;
@dynamic checkedDate;
@dynamic generalHeatingChecksToProperty;

@end
