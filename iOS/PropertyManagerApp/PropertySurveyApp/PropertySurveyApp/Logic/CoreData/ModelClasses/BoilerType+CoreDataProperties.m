//
//  BoilerType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerType+CoreDataProperties.h"

@implementation BoilerType (CoreDataProperties)

+ (NSFetchRequest<BoilerType *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BoilerType"];
}

@dynamic boilerTypeDescription;
@dynamic boilerTypeId;
@dynamic isSchemeBlock;

@end
