//
//  FaultAreaList.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2016-03-17.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "FaultAreaList.h"

@implementation FaultAreaList

@dynamic faultAreaId;
@dynamic faultAreaName;

@end
