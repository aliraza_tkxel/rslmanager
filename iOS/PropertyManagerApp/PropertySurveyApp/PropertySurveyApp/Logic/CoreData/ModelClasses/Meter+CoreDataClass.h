//
//  Meter+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

NS_ASSUME_NONNULL_BEGIN

@interface Meter : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Meter+CoreDataProperties.h"
