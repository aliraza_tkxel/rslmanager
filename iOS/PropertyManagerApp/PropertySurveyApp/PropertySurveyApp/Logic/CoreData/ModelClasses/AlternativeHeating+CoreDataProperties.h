//
//  AlternativeHeating+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AlternativeHeating+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AlternativeHeating (CoreDataProperties)

+ (NSFetchRequest<AlternativeHeating *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, copy) NSDate *certificateIssued;
@property (nullable, nonatomic, copy) NSString *certificateName;
@property (nullable, nonatomic, copy) NSNumber *certificateNumber;
@property (nullable, nonatomic, copy) NSDate *certificateRenewal;
@property (nullable, nonatomic, copy) NSString *heatingFuel;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSString *heatingName;
@property (nullable, nonatomic, copy) NSString *heatingType;
@property (nullable, nonatomic, copy) NSDate *installedDate;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSString *location;
@property (nullable, nonatomic, copy) NSNumber *manufacturerId;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *solarTypeId;
@property (nullable, nonatomic, retain) AirSourceInspection *alternativeHeatingToAirsourceInspection;
@property (nullable, nonatomic, retain) BoilerManufacturer *alternativeHeatingToManufacturer;
@property (nullable, nonatomic, retain) MVHRInspection *alternativeHeatingToMVHRInspection;
@property (nullable, nonatomic, retain) Property *alternativeHeatingToProperty;
@property (nullable, nonatomic, retain) SolarInspection *alternativeHeatingToSolarInspection;
@property (nullable, nonatomic, retain) SolarType *alternativeHeatingToSolarType;

@end

NS_ASSUME_NONNULL_END
