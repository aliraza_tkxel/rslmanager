//
//  Appointment+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appointment+CoreDataProperties.h"

@implementation Appointment (CoreDataProperties)

+ (NSFetchRequest<Appointment *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Appointment"];
}

@dynamic addToCalendar;
@dynamic appointmentAbortNotes;
@dynamic appointmentAbortReason;
@dynamic appointmentActualEndTime;
@dynamic appointmentActualStartTime;
@dynamic appointmentCalendar;
@dynamic appointmentDate;
@dynamic appointmentDateSectionIdentifier;
@dynamic appointmentEndTime;
@dynamic appointmentEndTimeString;
@dynamic appointmentEventIdentifier;
@dynamic appointmentId;
@dynamic appointmentLocation;
@dynamic appointmentNotes;
@dynamic appointmentOverdue;
@dynamic appointmentShift;
@dynamic appointmentStartTime;
@dynamic appointmentStartTimeString;
@dynamic appointmentStatus;
@dynamic appointmentStatusSynced;
@dynamic appointmentTitle;
@dynamic appointmentType;
@dynamic aptCompletedAppVersionInfo;
@dynamic assignedTo;
@dynamic createdBy;
@dynamic createdByPerson;
@dynamic creationDate;
@dynamic defaultCustomerId;
@dynamic defaultCustomerIndex;
@dynamic failedReason;
@dynamic isMiscAppointment;
@dynamic isModified;
@dynamic isOfflinePrepared;
@dynamic isSurveyChanged;
@dynamic journalHistoryId;
@dynamic journalId;
@dynamic jsgNumber;
@dynamic loggedDate;
@dynamic noEntryNotes;
@dynamic repairCompletionDateTime;
@dynamic repairNotes;
@dynamic surveyCheckListForm;
@dynamic surveyorAlert;
@dynamic surveyorUserName;
@dynamic surveyourAvailability;
@dynamic surveyType;
@dynamic syncStatus;
@dynamic tenancyId;
@dynamic heatingFuel;
@dynamic appointmentToAppInfoData;
@dynamic appointmentToAppointmentHistory;
@dynamic appointmentToCP12Info;
@dynamic appointmentToCustomer;
@dynamic appointmentToFaultReported;
@dynamic appointmentToJobSheet;
@dynamic appointmentToJournal;
@dynamic appointmentToMeterData;
@dynamic appointmentToPlannedComponent;
@dynamic appointmentToProperty;
@dynamic appointmentToPropertyAttributesNotes;
@dynamic appointmentToScheme;
@dynamic appointmentToSurvey;
@dynamic appointmentToUser;
@dynamic appointmentToVoidData;
@dynamic appointmentToVoidWorks;

@end
