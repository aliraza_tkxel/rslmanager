//
//  BoilerInspection+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 17/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerInspection+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BoilerInspection (CoreDataProperties)

+ (NSFetchRequest<BoilerInspection *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *adequateVentilation;
@property (nullable, nonatomic, copy) NSString *boilerSafeToUse;
@property (nullable, nonatomic, copy) NSString *boilerServiced;
@property (nullable, nonatomic, copy) NSString *combustionReading;
@property (nullable, nonatomic, copy) NSString *fluePerformanceChecks;
@property (nullable, nonatomic, copy) NSString *flueVisualCondition;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSString *operatingPressure;
@property (nullable, nonatomic, copy) NSString *operatingPressureUnit;
@property (nullable, nonatomic, copy) NSString *safetyDeviceOperational;
@property (nullable, nonatomic, copy) NSString *satisfactoryTermination;
@property (nullable, nonatomic, copy) NSString *smokePellet;
@property (nullable, nonatomic, copy) NSString *spillageTest;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, retain) Boiler *boilerInspectionToBoiler;

@end

NS_ASSUME_NONNULL_END
