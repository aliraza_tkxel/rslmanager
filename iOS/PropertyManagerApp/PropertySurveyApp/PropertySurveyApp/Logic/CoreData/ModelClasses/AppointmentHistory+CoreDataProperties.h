//
//  AppointmentHistory+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "AppointmentHistory+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AppointmentHistory (CoreDataProperties)

+ (NSFetchRequest<AppointmentHistory *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *actionBy;
@property (nullable, nonatomic, copy) NSDate *actionDate;
@property (nullable, nonatomic, copy) NSString *actionType;

@end

NS_ASSUME_NONNULL_END
