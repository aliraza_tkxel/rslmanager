//
//  Defect+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Defect+CoreDataProperties.h"

@implementation Defect (CoreDataProperties)

+ (NSFetchRequest<Defect *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Defect"];
}

@dynamic applianceID;
@dynamic blockId;
@dynamic boilerTypeId;
@dynamic defectDate;
@dynamic defectID;
@dynamic defectNotes;
@dynamic defectType;
@dynamic detectorTypeId;
@dynamic duration;
@dynamic faultCategory;
@dynamic gasCouncilNumber;
@dynamic heatingId;
@dynamic isAdviceNoteIssued;
@dynamic isCustomerHaveHeating;
@dynamic isCustomerHaveHotWater;
@dynamic isDefectIdentified;
@dynamic isDisconnected;
@dynamic isHeatersLeft;
@dynamic isPartsOrdered;
@dynamic isPartsRequired;
@dynamic isRemedialActionTaken;
@dynamic isTwoPersonsJob;
@dynamic journalId;
@dynamic numberOfHeatersLeft;
@dynamic partsDescription;
@dynamic partsDueDate;
@dynamic partsLocation;
@dynamic partsOrderedBy;
@dynamic priorityId;
@dynamic propertyId;
@dynamic reasonForTwoPerson;
@dynamic remedialActionNotes;
@dynamic schemeId;
@dynamic serialNumber;
@dynamic tradeId;
@dynamic warningNoteSerialNo;
@dynamic warningTagFixed;
@dynamic defectCategory;
@dynamic defectPictures;
@dynamic defectToAppliance;
@dynamic defectToBoiler;
@dynamic defectToDefectPriority;
@dynamic defectToDetector;
@dynamic defectToTrade;
@dynamic defectToOilHeating;

@end
