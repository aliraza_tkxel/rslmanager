//
//  MeterType.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MeterType : NSManagedObject

@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSNumber * meterId;
@property (nonatomic, retain) NSString * meterType;

@end
