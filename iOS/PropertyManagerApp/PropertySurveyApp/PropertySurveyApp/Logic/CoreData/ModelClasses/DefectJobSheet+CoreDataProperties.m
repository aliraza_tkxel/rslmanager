//
//  DefectJobSheet+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 21/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "DefectJobSheet+CoreDataProperties.h"

@implementation DefectJobSheet (CoreDataProperties)

+ (NSFetchRequest<DefectJobSheet *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"DefectJobSheet"];
}

@dynamic appliance;
@dynamic applianceId;
@dynamic boilerTypeId;
@dynamic defectCategoryId;
@dynamic defectCompletionNotes;
@dynamic defectDate;
@dynamic defectId;
@dynamic defectNotes;
@dynamic defectRecordedBy;
@dynamic gasCouncilNumber;
@dynamic inspectionRef;
@dynamic isAdviceNoteIssued;
@dynamic isCustomerHaveHeating;
@dynamic isCustomerHaveHotWater;
@dynamic isDisconnected;
@dynamic isHeatersLeft;
@dynamic isRemedialActionTaken;
@dynamic isTwoPersonsJob;
@dynamic make;
@dynamic model;
@dynamic numberOfHeatersLeft;
@dynamic partsDueDate;
@dynamic partsLocation;
@dynamic partsOrderedBy;
@dynamic remedialActionNotes;
@dynamic serialNumber;
@dynamic trade;
@dynamic warningTagFixed;
@dynamic heatingId;

@end
