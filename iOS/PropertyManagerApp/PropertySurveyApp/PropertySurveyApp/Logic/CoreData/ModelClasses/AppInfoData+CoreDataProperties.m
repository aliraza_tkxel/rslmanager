//
//  AppInfoData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 7/21/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AppInfoData+CoreDataProperties.h"

@implementation AppInfoData (CoreDataProperties)

+ (NSFetchRequest<AppInfoData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AppInfoData"];
}

@dynamic isCardLeft;
@dynamic totalAppointments;
@dynamic totalNoEntries;
@dynamic appInfoDataToAppointment;

@end
