//
//  BoilerType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BoilerType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BoilerType (CoreDataProperties)

+ (NSFetchRequest<BoilerType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *boilerTypeDescription;
@property (nullable, nonatomic, copy) NSNumber *boilerTypeId;
@property (nullable, nonatomic, copy) NSNumber *isSchemeBlock;

@end

NS_ASSUME_NONNULL_END
