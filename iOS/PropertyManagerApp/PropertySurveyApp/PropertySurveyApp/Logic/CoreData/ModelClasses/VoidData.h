//
//  VoidData.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-19.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, MajorWorkRequired, PaintPack, PropertyComponents, WorkRequired;

@interface VoidData : NSManagedObject

@property (nonatomic, retain) NSNumber * isAsbestosCheckRequired;
@property (nonatomic, retain) NSNumber * isElectricCheckRequired;
@property (nonatomic, retain) NSNumber * isEPCCheckRequired;
@property (nonatomic, retain) NSNumber * isGasCheckRequired;
@property (nonatomic, retain) NSNumber * isMajorWorkRequired;
@property (nonatomic, retain) NSNumber * isPaintPackRequired;
@property (nonatomic, retain) NSNumber * isWorksRequired;
@property (nonatomic, retain) NSString * jsvNumber;
@property (nonatomic, retain) NSDate * reletDate;
@property (nonatomic, retain) NSNumber * tenantEstimate;
@property (nonatomic, retain) NSDate * terminationDate;
@property (nonatomic, retain) NSString * gasCheckStatus;
@property (nonatomic, retain) NSDate * gasCheckDate;
@property (nonatomic, retain) NSString * electricCheckStatus;
@property (nonatomic, retain) NSDate * electricCheckDate;
@property (nonatomic, retain) Appointment *voidDataToAppointment;
@property (nonatomic, retain) NSSet *voidDataToMajorWorkRequired;
@property (nonatomic, retain) NSSet *voidDataToPaintPack;
@property (nonatomic, retain) NSSet *voidDataToPropertyComponents;
@property (nonatomic, retain) NSSet *voidDataToWorkRequired;
@end

@interface VoidData (CoreDataGeneratedAccessors)

- (void)addVoidDataToMajorWorkRequiredObject:(MajorWorkRequired *)value;
- (void)removeVoidDataToMajorWorkRequiredObject:(MajorWorkRequired *)value;
- (void)addVoidDataToMajorWorkRequired:(NSSet *)values;
- (void)removeVoidDataToMajorWorkRequired:(NSSet *)values;

- (void)addVoidDataToPaintPackObject:(PaintPack *)value;
- (void)removeVoidDataToPaintPackObject:(PaintPack *)value;
- (void)addVoidDataToPaintPack:(NSSet *)values;
- (void)removeVoidDataToPaintPack:(NSSet *)values;

- (void)addVoidDataToPropertyComponentsObject:(PropertyComponents *)value;
- (void)removeVoidDataToPropertyComponentsObject:(PropertyComponents *)value;
- (void)addVoidDataToPropertyComponents:(NSSet *)values;
- (void)removeVoidDataToPropertyComponents:(NSSet *)values;

- (void)addVoidDataToWorkRequiredObject:(WorkRequired *)value;
- (void)removeVoidDataToWorkRequiredObject:(WorkRequired *)value;
- (void)addVoidDataToWorkRequired:(NSSet *)values;
- (void)removeVoidDataToWorkRequired:(NSSet *)values;

@end
