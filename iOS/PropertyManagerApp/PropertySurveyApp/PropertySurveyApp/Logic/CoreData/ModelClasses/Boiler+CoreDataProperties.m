//
//  Boiler+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Boiler+CoreDataProperties.h"

@implementation Boiler (CoreDataProperties)

+ (NSFetchRequest<Boiler *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Boiler"];
}

@dynamic blockId;
@dynamic boilerName;
@dynamic boilerTypeId;
@dynamic flueTypeId;
@dynamic gasketReplacementDate;
@dynamic gcNumber;
@dynamic heatingId;
@dynamic isInspected;
@dynamic isLandlordAppliance;
@dynamic lastReplaced;
@dynamic location;
@dynamic manufacturerId;
@dynamic model;
@dynamic propertyId;
@dynamic replacementDue;
@dynamic schemeId;
@dynamic serialNumber;
@dynamic itemId;
@dynamic boilerToBoilerInspection;
@dynamic boilerToBoilerManufacturer;
@dynamic boilerToBoilerType;
@dynamic boilerToDefect;
@dynamic boilerToFlueType;
@dynamic boilerToGasInstallationPipeWork;
@dynamic boilerToProperty;

@end
