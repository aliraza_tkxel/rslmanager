//
//  BurnerType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "BurnerType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BurnerType (CoreDataProperties)

+ (NSFetchRequest<BurnerType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *lookupId;
@property (nullable, nonatomic, copy) NSString *value;

@end

NS_ASSUME_NONNULL_END
