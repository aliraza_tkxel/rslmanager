//
//  SchemeAsbestosData+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by aqib javed on 02/01/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Scheme;

NS_ASSUME_NONNULL_BEGIN

@interface SchemeAsbestosData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SchemeAsbestosData+CoreDataProperties.h"
