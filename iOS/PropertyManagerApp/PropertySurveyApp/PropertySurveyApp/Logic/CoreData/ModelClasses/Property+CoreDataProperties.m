//
//  Property+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Property+CoreDataProperties.h"

@implementation Property (CoreDataProperties)

+ (NSFetchRequest<Property *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Property"];
}

@dynamic address1;
@dynamic address2;
@dynamic address3;
@dynamic blockId;
@dynamic blockName;
@dynamic certificateExpiry;
@dynamic county;
@dynamic flatNumber;
@dynamic houseNumber;
@dynamic lastSurveyDate;
@dynamic postCode;
@dynamic propertyId;
@dynamic schemeId;
@dynamic schemeName;
@dynamic tenancyId;
@dynamic townCity;
@dynamic defaultPicture;
@dynamic propertyToAccomodation;
@dynamic propertyToAlternativeHeating;
@dynamic propertyToAppliances;
@dynamic propertyToAppointment;
@dynamic propertyToBoiler;
@dynamic propertyToDetector;
@dynamic propertyToGeneralHeatingChecks;
@dynamic propertyToMeter;
@dynamic propertyToPropertyAsbestosData;
@dynamic propertyToPropertyPicture;
@dynamic propertyToOilHeatings;

@end
