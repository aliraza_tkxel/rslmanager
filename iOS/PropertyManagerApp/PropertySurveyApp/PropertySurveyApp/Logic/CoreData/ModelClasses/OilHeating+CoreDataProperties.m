//
//  OilHeating+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilHeating+CoreDataProperties.h"

@implementation OilHeating (CoreDataProperties)

+ (NSFetchRequest<OilHeating *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"OilHeating"];
}

@dynamic applianceLocation;
@dynamic applianceMake;
@dynamic applianceModel;
@dynamic applianceSerialNumber;
@dynamic blockId;
@dynamic burnerMake;
@dynamic burnerModel;
@dynamic burnerTypeId;
@dynamic certificateIssued;
@dynamic certificateName;
@dynamic certificateNumber;
@dynamic certificateRenewel;
@dynamic flueTypeId;
@dynamic fuelTypeId;
@dynamic heatingFuel;
@dynamic heatingId;
@dynamic heatingName;
@dynamic heatingType;
@dynamic isInspected;
@dynamic itemId;
@dynamic originalInstallDate;
@dynamic propertyId;
@dynamic schemeId;
@dynamic tankTypeId;
@dynamic oilHeatingToBurnerType;
@dynamic oilHeatingToFireServiceInspection;
@dynamic oilHeatingToOilFlueType;
@dynamic oilHeatingToOilFuelType;
@dynamic oilHeatingToProperty;
@dynamic oilHeatingToTankType;
@dynamic oilHeatingToDefect;

@end
