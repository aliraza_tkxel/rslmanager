//
//  JobSheet+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/6/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, JobPauseData, RepairPictures;

NS_ASSUME_NONNULL_BEGIN

@interface JobSheet : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "JobSheet+CoreDataProperties.h"
