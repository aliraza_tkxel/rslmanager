//
//  OilFiringServiceInspection+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "OilFiringServiceInspection+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface OilFiringServiceInspection (CoreDataProperties)

+ (NSFetchRequest<OilFiringServiceInspection *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *airSupply;
@property (nullable, nonatomic, copy) NSString *airSupplyDetail;
@property (nullable, nonatomic, copy) NSNumber *applianceSafety;
@property (nullable, nonatomic, copy) NSString *applianceSafetyDetail;
@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSNumber *chimneyFlue;
@property (nullable, nonatomic, copy) NSString *chimneyFlueDetail;
@property (nullable, nonatomic, copy) NSNumber *combustionChamber;
@property (nullable, nonatomic, copy) NSString *combustionChamberDetail;
@property (nullable, nonatomic, copy) NSNumber *controlCheck;
@property (nullable, nonatomic, copy) NSString *controlCheckDetail;
@property (nullable, nonatomic, copy) NSNumber *electricalSafety;
@property (nullable, nonatomic, copy) NSString *electricalSafetyDetail;
@property (nullable, nonatomic, copy) NSNumber *heatExchanger;
@property (nullable, nonatomic, copy) NSString *heatExchangerDetail;
@property (nullable, nonatomic, copy) NSNumber *heatingId;
@property (nullable, nonatomic, copy) NSNumber *hotWaterType;
@property (nullable, nonatomic, copy) NSString *hotWaterTypeDetail;
@property (nullable, nonatomic, copy) NSNumber *inspectedBy;
@property (nullable, nonatomic, copy) NSDate *inspectionDate;
@property (nullable, nonatomic, copy) NSNumber *inspectionId;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSNumber *oilStorage;
@property (nullable, nonatomic, copy) NSString *oilStorageDetail;
@property (nullable, nonatomic, copy) NSNumber *oilSupplySystem;
@property (nullable, nonatomic, copy) NSString *oilSupplySystemDetail;
@property (nullable, nonatomic, copy) NSNumber *pressureJet;
@property (nullable, nonatomic, copy) NSString *pressureJetDetail;
@property (nullable, nonatomic, copy) NSNumber *vaporisingBurner;
@property (nullable, nonatomic, copy) NSString *vaporisingBurnerDetail;
@property (nullable, nonatomic, copy) NSNumber *wallflameBurner;
@property (nullable, nonatomic, copy) NSString *wallflameBurnerDetail;
@property (nullable, nonatomic, copy) NSNumber *warmAirType;
@property (nullable, nonatomic, copy) NSString *warmAirTypeDetail;
@property (nullable, nonatomic, retain) OilHeating *firingServiceInspectionToOilHeating;
@property (nullable, nonatomic, retain) NSSet<AlternativeFuelPicture *> *fireServicingInspectionToPictures;

@end

@interface OilFiringServiceInspection (CoreDataGeneratedAccessors)

- (void)addFireServicingInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)removeFireServicingInspectionToPicturesObject:(AlternativeFuelPicture *)value;
- (void)addFireServicingInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;
- (void)removeFireServicingInspectionToPictures:(NSSet<AlternativeFuelPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
