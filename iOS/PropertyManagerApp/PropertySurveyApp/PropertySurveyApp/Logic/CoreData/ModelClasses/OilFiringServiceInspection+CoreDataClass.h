//
//  OilFiringServiceInspection+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AlternativeFuelPicture, OilHeating;

NS_ASSUME_NONNULL_BEGIN

@interface OilFiringServiceInspection : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "OilFiringServiceInspection+CoreDataProperties.h"
