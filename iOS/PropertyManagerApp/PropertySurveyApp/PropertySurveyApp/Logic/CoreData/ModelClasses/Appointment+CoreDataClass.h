//
//  Appointment+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 19/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AppInfoData, AppointmentHistory, CP12Info, Customer, FaultReported, JobSheet, Journal, MeterData, NSObject, PlannedTradeComponent, Property, PropertyAttributesNotes, Scheme, Survey, User, VoidData, VoidWorks;

NS_ASSUME_NONNULL_BEGIN

@interface Appointment : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Appointment+CoreDataProperties.h"
