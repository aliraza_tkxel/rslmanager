//
//  FaultReported.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-29.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import "FaultReported.h"
#import "Appointment+CoreDataClass.h"
#import "FaultDetailList.h"

@implementation FaultReported

@dynamic isRecurring;
@dynamic notes;
@dynamic problemDays;
@dynamic faultReportedToAppointment;
@dynamic faultReportedToFaultDetail;
@dynamic faultReportedToFaultArea;

@end
