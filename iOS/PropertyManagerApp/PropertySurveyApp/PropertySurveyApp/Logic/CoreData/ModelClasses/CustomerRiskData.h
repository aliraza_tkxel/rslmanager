//
//  CustomerRiskData.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface CustomerRiskData : NSManagedObject

@property (nonatomic, retain) NSString * riskCatDesc;
@property (nonatomic, retain) NSString * riskSubCatDesc;
@property (nonatomic, retain) Customer *customerRiskDataToCustomer;

@end
