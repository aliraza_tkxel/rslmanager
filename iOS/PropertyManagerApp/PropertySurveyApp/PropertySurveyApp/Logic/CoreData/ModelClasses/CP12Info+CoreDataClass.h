//
//  CP12Info+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

NS_ASSUME_NONNULL_BEGIN

@interface CP12Info : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CP12Info+CoreDataProperties.h"
