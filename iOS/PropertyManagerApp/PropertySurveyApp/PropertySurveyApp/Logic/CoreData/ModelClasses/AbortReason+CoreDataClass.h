//
//  AbortReason+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Abdul Rehman on 23/02/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AbortReason : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AbortReason+CoreDataProperties.h"
