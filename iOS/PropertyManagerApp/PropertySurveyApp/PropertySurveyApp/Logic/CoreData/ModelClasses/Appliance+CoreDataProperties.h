//
//  Appliance+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appliance+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Appliance (CoreDataProperties)

+ (NSFetchRequest<Appliance *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *applianceID;
@property (nullable, nonatomic, copy) NSString *fluType;
@property (nullable, nonatomic, copy) NSString *gcNumber;
@property (nullable, nonatomic, copy) NSDate *installedDate;
@property (nullable, nonatomic, copy) NSNumber *isActive;
@property (nullable, nonatomic, copy) NSNumber *isInspected;
@property (nullable, nonatomic, copy) NSNumber *isLandlordAppliance;
@property (nullable, nonatomic, copy) NSString *model;
@property (nullable, nonatomic, copy) NSString *propertyID;
@property (nullable, nonatomic, copy) NSDate *replacementDate;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSNumber *schemeId;
@property (nullable, nonatomic, copy) NSNumber *blockId;
@property (nullable, nonatomic, retain) NSSet<Defect *> *applianceDefects;
@property (nullable, nonatomic, retain) ApplianceInspection *applianceInspection;
@property (nullable, nonatomic, retain) ApplianceLocation *applianceLocation;
@property (nullable, nonatomic, retain) ApplianceManufacturer *applianceManufacturer;
@property (nullable, nonatomic, retain) ApplianceModel *applianceModel;
@property (nullable, nonatomic, retain) Property *applianceToProperty;
@property (nullable, nonatomic, retain) ApplianceType *applianceType;

@end

@interface Appliance (CoreDataGeneratedAccessors)

- (void)addApplianceDefectsObject:(Defect *)value;
- (void)removeApplianceDefectsObject:(Defect *)value;
- (void)addApplianceDefects:(NSSet<Defect *> *)values;
- (void)removeApplianceDefects:(NSSet<Defect *> *)values;

@end

NS_ASSUME_NONNULL_END
