//
//  DetectorType.h
//  PropertySurveyApp
//
//  Created by aqib javed on 29/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DetectorType : NSManagedObject

@property (nonatomic, retain) NSString * detectorType;
@property (nonatomic, retain) NSNumber * detectorTypeId;

@end
