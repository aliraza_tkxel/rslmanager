//
//  SurveyData.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FormData, Survey;

@interface SurveyData : NSManagedObject

@property (nonatomic, retain) NSNumber * appointmentId;
@property (nonatomic, retain) NSNumber * customerId;
@property (nonatomic, retain) NSNumber * itemId;
@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSString * surveyFormName;
@property (nonatomic, retain) NSDate * surveyNotesDate;
@property (nonatomic, retain) NSString * surveyNotesDetail;
@property (nonatomic, retain) NSSet *surveyDataToFormData;
@property (nonatomic, retain) Survey *surveyDataToSurvey;
@end

@interface SurveyData (CoreDataGeneratedAccessors)

- (void)addSurveyDataToFormDataObject:(FormData *)value;
- (void)removeSurveyDataToFormDataObject:(FormData *)value;
- (void)addSurveyDataToFormData:(NSSet *)values;
- (void)removeSurveyDataToFormData:(NSSet *)values;

@end
