//
//  FaultDetailList.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-17.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FaultDetailList.h"


@implementation FaultDetailList

@dynamic faultDescription;
@dynamic faultId;
@dynamic gross;
@dynamic isRecharge;
@dynamic priorityName;
@dynamic responseTime;

@end
