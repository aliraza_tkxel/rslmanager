//
//  Surveyor.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-09.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Surveyor : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * surveyorType;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userName;

@property (nonatomic, retain) NSString *surveyorNameSectionIdentifier;
@property (nonatomic, retain) NSString *primitiveSurveyorNameSectionIdentifier;

@end
