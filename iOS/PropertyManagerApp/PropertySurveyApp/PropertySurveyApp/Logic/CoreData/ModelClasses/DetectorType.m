//
//  DetectorType.m
//  PropertySurveyApp
//
//  Created by aqib javed on 29/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DetectorType.h"


@implementation DetectorType

@dynamic detectorType;
@dynamic detectorTypeId;

@end
