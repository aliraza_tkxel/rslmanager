//
//  FaultJobSheet+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by aqib javed on 07/03/2019.
//  Copyright © 2019 TkXel. All rights reserved.
//

#import "FaultJobSheet+CoreDataProperties.h"

@implementation FaultJobSheet (CoreDataProperties)

+ (NSFetchRequest<FaultJobSheet *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FaultJobSheet"];
}

@dynamic duration;
@dynamic durationUnit;
@dynamic faultLogID;
@dynamic followOnNotes;
@dynamic jsnDescription;
@dynamic jsnLocation;
@dynamic jsnNotes;
@dynamic priority;
@dynamic repairId;
@dynamic repairNotes;
@dynamic reportedDate;
@dynamic responseTime;
@dynamic faultRepairDataList;
@dynamic faultRepairHistoryList;

@end
