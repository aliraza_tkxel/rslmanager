//
//  PowerType.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/08/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PowerType : NSManagedObject

@property (nonatomic, retain) NSNumber * powerTypeId;
@property (nonatomic, retain) NSString * powerType;

@end
