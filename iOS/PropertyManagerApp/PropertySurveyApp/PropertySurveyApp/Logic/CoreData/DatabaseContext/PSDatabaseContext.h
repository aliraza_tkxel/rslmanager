//
//  PSDatabaseContext.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDatabaseContext : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSString *)applicationDocumentsDirectory;
- (void) resetCoreDataStack;
+ (PSDatabaseContext *) sharedContext;
+ (void) turnObjectIntoFault:(NSManagedObjectID *) objectID;
+ (void) turnObjectIntoFault:(NSManagedObjectID *) objectID context:(NSManagedObjectContext *)managedObjectContext;
+ (void) deleteManagedObjectFromCoreDataWithID:(NSManagedObjectID *) objectID;
@end
