//
//  PSDataPersistenceManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager : NSObject

+ (PSDataPersistenceManager *)sharedManager;
- (void) saveUserInfo:(NSDictionary *)userInfo;
- (void) saveAppointments:(NSArray *)appointmentsArray;
- (void) changeAppointmentModificationStatus:(NSArray *)appointmentsArray savedAppointments:(NSArray*)savedAppointments failedAppointments:(NSArray*)failedAppointments postNotification:(BOOL)flag;
- (void) createNewAppointment: (NSDictionary *)appointmentDictionary;
- (void) savePropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Property*) property;
- (void) saveAppointmentSurveyData:(NSDictionary *)surveyData;
- (void) saveSurveyors:(NSArray *)surveyorsArray;
- (void) saveProperties:(NSArray *)propertyArray;

- (void) saveAppliances:(NSDictionary *)appliancesData;
- (void) saveApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline;

- (void) saveFetchedApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline;
- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *)appliance;
- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forBoiler:(Boiler *)boiler;
- (void) deleteApplianceData:(NSDictionary *) appliancesData forProperty:(NSManagedObjectID *)property;
- (void) deleteAppliance:(NSDictionary *) appliancesData property:(Property *)property managedObjectContext:(NSManagedObjectContext *) managedObjectContext;
- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray;
- (void) saveFetchedFaultRepairData:(NSArray *)faultRepairData;
- (void) savefetchedJobPauseReasons:(NSArray *)jobPauseReasons;
- (void) saveFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(FaultJobSheet *)jobData;
- (void) updateModificationStatusOfAppointments:(NSArray *)appointmentsArray  toModificationStatus:(NSNumber*)modificationStatus;

- (void) saveRepairPictureInfo:(NSDictionary *)pictureDictionary;
- (void) loadAppointmentSchemeData:(NSDictionary *)SchemeData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (BOOL) saveDataInDB:(NSManagedObjectContext *)dbContext;
-(void) addDefaultCP12InfoForNonInspectedBoilersWithAppointment: (Appointment *) appointment managedObjectContext:(NSManagedObjectContext *) managedObjectContext;
-(NSNumber*) generateIdentifier;
@end
