//
//  PSDataPersistenceManager (CommonData).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(CommonData).h"


@implementation PSDataPersistenceManager (CommonData)

- (void) saveCommonData:(NSArray *)commonDataArray
{
	CLS_LOG(@"save CommonData");
	[self saveVoidCommonData:commonDataArray];
	[self saveRepairCommonData:commonDataArray];	
	[self saveGasSurveyCommonData:commonDataArray];
	[self saveFaultAreaCommonData:commonDataArray];
    [self saveRepairListCommonData:commonDataArray];
    [self saveAlternativeAndOilLookups:commonDataArray];
}

@end
