//
//  PSDataPersistenceManager+OilServicing.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
@interface PSDataPersistenceManager (OilServicing)
-(void) saveFetchedOilHeatingData:(NSDictionary *) jsonData;
- (void) saveFireServicingInspectionForm:(NSDictionary *) inspectionFormDictionary forOilHeating:(OilHeating *)altHeating;
@end
