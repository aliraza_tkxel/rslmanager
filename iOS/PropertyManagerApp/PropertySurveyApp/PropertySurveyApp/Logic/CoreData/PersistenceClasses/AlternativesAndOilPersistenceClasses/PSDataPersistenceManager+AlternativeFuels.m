//
//  PSDataPersistenceManager+AlternativeFuels.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+AlternativeFuels.h"
#import "AlternativeFuelPicture+JSON.h"
@implementation PSDataPersistenceManager (AlternativeFuels)
-(void) saveFetchedAlternativeHeatingData:(NSDictionary *) jsonData{
    if([UtilityClass isUserLoggedIn])
    {
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        if(!isEmpty(jsonData)){
            __block NSArray * heatingDataBlock = [jsonData objectForKey:kHeatings];
            if(!isEmpty(heatingDataBlock)){
                __block NSDictionary *heating = [heatingDataBlock objectAtIndex:0];
                __block NSString *propertyId = [heating objectForKey:kPropertyId];
                PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
                __block Property * property = [coreDataMgr findRecordFrom:kProperty
                                                                    Field:kPropertyId
                                                                WithValue:propertyId
                                                                  context:managedObjectContext];
                if(!isEmpty(property)){
                    [managedObjectContext performBlock:^{
                        [[PSDataPersistenceManager sharedManager] loadAltFuelsData:heatingDataBlock property:property managedObjectContext:managedObjectContext isAddedOffline:NO];
                        property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
                        [self saveDataInDB:managedObjectContext];
                    }];
                }
            }
        }
    }
}
#pragma mark - Alt heating Loading

- (void) loadAltFuelsData:(NSArray *)boilerData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline
{
    if(!isEmpty(boilerData))
    {
        
        for (NSDictionary * boilerDictionary in boilerData)
        {
            NSNumber *heatingId = [boilerDictionary valueForKey:kHeatingId];
            NSString *heatingName = [boilerDictionary valueForKey:kHeatingName];
            NSString *heatingFuel = [boilerDictionary valueForKey:kHeatingFuel];
            NSString *heatingType = [boilerDictionary valueForKey:kHeatingType];
            NSNumber * manufacturerId = [boilerDictionary valueForKey:kBoilerManufacturerId];
            NSString * model = [boilerDictionary valueForKey:kBoilerModel];
            NSString * serialNumber = [boilerDictionary valueForKey:kBoilerSerialNumber];
            NSString * location = [boilerDictionary valueForKey:kBoilerLocation];
            NSDate *   installedDate = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kInstalledDate]];
            NSString *certificateName = [boilerDictionary valueForKey:kCertificateName];
            NSDate *   certificateIssued = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kCertiticateIssued]];
            NSNumber *certificateNumber = [boilerDictionary valueForKey:kCertificateNumber];
            NSDate *certificateRenewal = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kCertificateRenewal]];
            NSNumber *solarTypeId = [boilerDictionary valueForKey:kSolarTypeId];
            NSString * propertyId = [boilerDictionary valueForKey:kBoilerPropertyId];
            NSNumber *schemeId = [boilerDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [boilerDictionary valueForKey:kSchemeBlockID];
            NSNumber * isInspected = [boilerDictionary valueForKey:kBoilerIsInspected];
            NSNumber * itemID = [boilerDictionary valueForKey:kItemId];
            
            NSManagedObjectID *heatingObjectId = [boilerDictionary valueForKey:kHeatingObjectId];
            
            NSDictionary *mvHRInspectionDict = [boilerDictionary valueForKey:kMVHRInspectionForm];
            NSDictionary *airSourceInspectionDict = [boilerDictionary valueForKey:kAirsourceInspectionForm];
            NSDictionary *solarInspectionForm = [boilerDictionary valueForKey:kSolarInspectionForm];
            AlternativeHeating * altHeat = nil;
            
            if (!isEmpty(heatingObjectId))
            {
                altHeat = (AlternativeHeating *)[managedObjectContext objectWithID:heatingObjectId];
            }
            else{
                altHeat = [NSEntityDescription insertNewObjectForEntityForName:kAlternativeHeating inManagedObjectContext:managedObjectContext];
            }
            altHeat.heatingId = isEmpty(heatingId)?nil:heatingId;
            altHeat.heatingName = isEmpty(heatingName)?nil:heatingName;
            altHeat.heatingFuel = isEmpty(heatingFuel)?nil:heatingFuel;
            altHeat.heatingType = isEmpty(heatingType)?nil:heatingType;
            altHeat.manufacturerId = isEmpty(manufacturerId)?nil:manufacturerId;
            altHeat.model = isEmpty(model)?nil:model;
            altHeat.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
            altHeat.location = isEmpty(location)?nil:location;
            altHeat.installedDate = isEmpty(installedDate)?nil:installedDate;
            altHeat.certificateName = isEmpty(certificateName)?nil:certificateName;
            altHeat.certificateIssued = isEmpty(certificateIssued)?nil:certificateIssued;
            altHeat.certificateNumber = isEmpty(certificateNumber)?nil:certificateNumber;
            altHeat.certificateRenewal = isEmpty(certificateRenewal)?nil:certificateRenewal;
            altHeat.solarTypeId = isEmpty(solarTypeId)?nil:solarTypeId;
            altHeat.propertyId = isEmpty(propertyId)?nil:propertyId;
            altHeat.schemeId = isEmpty(schemeId)?nil:schemeId;
            altHeat.blockId = isEmpty(blockId)?nil:blockId;
            altHeat.isInspected = isEmpty(isInspected)?nil:isInspected;
            altHeat.itemId = isEmpty(itemID)?nil:itemID;
            
            if(!isEmpty(manufacturerId)){
                [self setBoilerManufacturer:manufacturerId ForAlHeat:altHeat managedObjectContext:managedObjectContext];
            }
            if(!isEmpty(solarTypeId)){
                [self setSolarType:solarTypeId ForAlHeat:altHeat managedObjectContext:managedObjectContext];
            }
            
            
            // Set Boiler Property
            
            
            if ([altHeat.isInspected boolValue])
            {
                if(!isEmpty(mvHRInspectionDict)){
                    [[PSDataPersistenceManager sharedManager] loadMVHRInspectionForm:mvHRInspectionDict forAltHeating:altHeat managedObjectContext:managedObjectContext];
                }
                else {
                    altHeat.alternativeHeatingToMVHRInspection = nil;
                }
                
                if(!isEmpty(solarInspectionForm)){
                    [[PSDataPersistenceManager sharedManager] loadSolarInspectionForm:solarInspectionForm forAltHeating:altHeat managedObjectContext:managedObjectContext];
                }
                else {
                    altHeat.alternativeHeatingToSolarInspection = nil;
                }
                
                if(!isEmpty(airSourceInspectionDict)){
                    [[PSDataPersistenceManager sharedManager] loadAirSourceInspectionForm:solarInspectionForm forAltHeating:altHeat managedObjectContext:managedObjectContext];
                }
                else {
                    altHeat.alternativeHeatingToAirsourceInspection = nil;
                }
                
            }
            
            
            if (!isEmpty(property))
            {
                altHeat.alternativeHeatingToProperty = property;
                [property addPropertyToAlternativeHeatingObject:altHeat];
            }
            
            
           
            [self saveDataInDB:managedObjectContext];
        }
    }
}
#pragma mark - Load & Save Air Source Inspection

- (void) saveAirSourceInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating
{
    if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        AlternativeHeating *existingAltHeating = (AlternativeHeating *)[managedObjectContext objectWithID:altHeating.objectID];
        [[PSDataPersistenceManager sharedManager] loadAirSourceInspectionForm:inspectionFormDictionary forAltHeating:existingAltHeating managedObjectContext:managedObjectContext];
        existingAltHeating.isInspected = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
    }
}

- (void) loadAirSourceInspectionForm:(NSDictionary *)inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    if(!isEmpty(inspectionFormDictionary) && !isEmpty(altHeating)){
        NSNumber *checkWaterSupplyTurnedOff = [inspectionFormDictionary valueForKey:kCheckWaterSupplyTurnedOff];
        NSString *turnedOffDetail = [inspectionFormDictionary valueForKey:kTurnedOffDetail];
        NSNumber *checkWaterSupplyTurnedOn = [inspectionFormDictionary valueForKey:kCheckWaterSupplyTurnedOn];
        NSString *turnedOnDetail = [inspectionFormDictionary valueForKey:kTurnedOnDetail];
        NSNumber *checkValves = [inspectionFormDictionary valueForKey:kCheckValves];
        NSString *valveDetail = [inspectionFormDictionary valueForKey:kValveDetail];
        NSNumber *checkSupplementaryBonding   =   [inspectionFormDictionary valueForKey:kCheckSupplementaryBonding];
        NSString *supplementaryBondingDetail = [inspectionFormDictionary valueForKey:kSupplementaryBondingDetail];
        NSNumber *checkFuse    = [inspectionFormDictionary valueForKey:kCheckFuse];
        NSString *fuseDetail   = [inspectionFormDictionary valueForKey:kFuseDetail];
        NSNumber *checkThermostat = [inspectionFormDictionary valueForKey:kCheckThermostat];
        NSString *thermostatDetail = [inspectionFormDictionary valueForKey:kThermostatDetail];
        NSNumber *checkOilLeak = [inspectionFormDictionary valueForKey:kCheckOilLeak];
        NSString *oilLeakDetail = [inspectionFormDictionary valueForKey:kOilLeakDetail];
        NSNumber *checkWaterPipework = [inspectionFormDictionary valueForKey:kCheckWaterPipework];
        NSString *pipeworkDetail = [inspectionFormDictionary valueForKey:kPipeworkDetail];
        NSNumber *checkElectricConnection = [inspectionFormDictionary valueForKey:kCheckElectricConnection];
        NSString *connectionDetail = [inspectionFormDictionary valueForKey:kConnectionDetail];
        
        NSNumber *inspectionId = [inspectionFormDictionary valueForKey:kBoilerInspectionId];
        NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
        NSDate   *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
        NSNumber *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
        
        
        AirSourceInspection *heatingInspection = [[PSCoreDataManager sharedManager] findRecordFrom:kAirsourceInspectionEntity Field:kBoilerInspectionId WithValue:inspectionId context:managedObjectContext];
        if ( heatingInspection != nil)
        {
            CLS_LOG(@"Air Inspection updated");
        }
        else
        {
            heatingInspection = [NSEntityDescription insertNewObjectForEntityForName:kSolarInspectionEntity inManagedObjectContext:managedObjectContext];
            CLS_LOG(@"New Air Inspection added");
        }
        
        
        heatingInspection.checkWaterSupplyTurnedOff = isEmpty(checkWaterSupplyTurnedOff)?nil:checkWaterSupplyTurnedOff;
        heatingInspection.turnedOffDetail = isEmpty(turnedOffDetail)?nil:turnedOffDetail;
        heatingInspection.checkWaterSupplyTurnedOn = isEmpty(checkWaterSupplyTurnedOn)?nil:checkWaterSupplyTurnedOn;
        heatingInspection.turnedOnDetail = isEmpty(turnedOnDetail)?nil:turnedOnDetail;
        heatingInspection.checkValves = isEmpty(checkValves)?nil:checkValves;
        heatingInspection.valveDetail = isEmpty(valveDetail)?nil:valveDetail;
        heatingInspection.checkSupplementaryBonding = isEmpty(checkSupplementaryBonding)?nil:checkSupplementaryBonding;
        heatingInspection.supplementaryBondingDetail = isEmpty(supplementaryBondingDetail)?nil:supplementaryBondingDetail;
        heatingInspection.checkFuse = isEmpty(checkFuse)?nil:checkFuse;
        heatingInspection.fuseDetail = isEmpty(fuseDetail)?nil:fuseDetail;
        heatingInspection.checkThermostat = isEmpty(checkThermostat)?nil:checkThermostat;
        heatingInspection.thermostatDetail = isEmpty(thermostatDetail)?nil:thermostatDetail;
        heatingInspection.checkOilLeak = isEmpty(checkOilLeak)?nil:checkOilLeak;
        heatingInspection.oilLeakDetail = isEmpty(oilLeakDetail)?nil:oilLeakDetail;
        heatingInspection.checkWaterPipework = isEmpty(checkWaterPipework)?nil:checkWaterPipework;
        heatingInspection.pipeworkDetail = isEmpty(pipeworkDetail)?nil:pipeworkDetail;
        heatingInspection.checkElectricConnection = isEmpty(checkElectricConnection)?nil:checkElectricConnection;
        heatingInspection.connectionDetail = isEmpty(connectionDetail)?nil:connectionDetail;
        
        heatingInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
        heatingInspection.inspectionId = isEmpty(inspectionId)?nil:inspectionId;
        heatingInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
        heatingInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        
        altHeating.alternativeHeatingToAirsourceInspection = heatingInspection;
        heatingInspection.airsourceInspectionToHeating = altHeating;
    }
}

#pragma mark - Load & Save Solar Inspection

- (void) saveSolarInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating
{
    if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        AlternativeHeating *existingAltHeating = (AlternativeHeating *)[managedObjectContext objectWithID:altHeating.objectID];
        [[PSDataPersistenceManager sharedManager] loadSolarInspectionForm:inspectionFormDictionary forAltHeating:existingAltHeating managedObjectContext:managedObjectContext];
        existingAltHeating.isInspected = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
    }
}


- (void) loadSolarInspectionForm:(NSDictionary *)inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    if(!isEmpty(inspectionFormDictionary) && !isEmpty(altHeating)){
        NSNumber *vesselProtectionInstalled = [inspectionFormDictionary valueForKey:kVesselProtectionInstalled];
        NSNumber *vesselCapacity = [inspectionFormDictionary valueForKey:kVesselCapacity];
        NSNumber *minPressure = [inspectionFormDictionary valueForKey:kMinPressure];
        NSNumber *freezingTemp = [inspectionFormDictionary valueForKey:kFreezingTemp];
        NSNumber *systemPressure = [inspectionFormDictionary valueForKey:kSystemPressure];
        NSNumber *backPressure = [inspectionFormDictionary valueForKey:kBackPressure];
        NSNumber *deltaOn   =   [inspectionFormDictionary valueForKey:kDeltaOn];
        NSNumber *deltaOff = [inspectionFormDictionary valueForKey:kDeltaOff];
        NSNumber *maxTemperature    = [inspectionFormDictionary valueForKey:kMaxTemperature];
        NSNumber *calculationRate   = [inspectionFormDictionary valueForKey:kCalculationRate];
        NSString *thermostatTemperature = [inspectionFormDictionary valueForKey:kThermostatTemperature];
        NSNumber *antiScaldingControl = [inspectionFormDictionary valueForKey:kAntiScaldingControl];
        NSString *antiScaldingControlDetail = [inspectionFormDictionary valueForKey:kAntiScaldingControlDetail];
        NSNumber *checkDirection = [inspectionFormDictionary valueForKey:kCheckDirection];
        NSString *directionDetail = [inspectionFormDictionary valueForKey:kDirectionDetail];
        NSNumber *checkElectricalControl = [inspectionFormDictionary valueForKey:kCheckElectricalControl];
        NSString *electricalControlDetail = [inspectionFormDictionary valueForKey:kElectricalControlDetail];
        
        NSNumber *inspectionId = [inspectionFormDictionary valueForKey:kBoilerInspectionId];
        NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
        NSDate   *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
        NSNumber *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
        
        
        SolarInspection *heatingInspection = [[PSCoreDataManager sharedManager] findRecordFrom:kSolarInspectionEntity Field:kBoilerInspectionId WithValue:inspectionId context:managedObjectContext];
        if ( heatingInspection != nil)
        {
            CLS_LOG(@"Solar Inspection updated");
        }
        else
        {
            heatingInspection = [NSEntityDescription insertNewObjectForEntityForName:kSolarInspectionEntity inManagedObjectContext:managedObjectContext];
            CLS_LOG(@"New Solar Inspection added");
        }
        
        
        heatingInspection.vesselProtectionInstalled = isEmpty(vesselProtectionInstalled)?nil:vesselProtectionInstalled;
        heatingInspection.vesselCapacity = isEmpty(vesselCapacity)?nil:vesselCapacity;
        heatingInspection.minPressure = isEmpty(minPressure)?nil:minPressure;
        heatingInspection.freezingTemp = isEmpty(freezingTemp)?nil:freezingTemp;
        heatingInspection.systemPressure = isEmpty(systemPressure)?nil:systemPressure;
        heatingInspection.backPressure = isEmpty(backPressure)?nil:backPressure;
        heatingInspection.deltaOn = isEmpty(deltaOn)?nil:deltaOn;
        heatingInspection.deltaOff = isEmpty(deltaOff)?nil:deltaOff;
        heatingInspection.maxTemperature = isEmpty(maxTemperature)?nil:maxTemperature;
        heatingInspection.calculationRate = isEmpty(calculationRate)?nil:calculationRate;
        heatingInspection.thermostatTemperature = isEmpty(thermostatTemperature)?nil:thermostatTemperature;
        heatingInspection.antiScaldingControl = isEmpty(antiScaldingControl)?nil:antiScaldingControl;
        heatingInspection.antiScaldingControlDetail = isEmpty(antiScaldingControlDetail)?nil:antiScaldingControlDetail;
        heatingInspection.checkDirection = isEmpty(checkDirection)?nil:checkDirection;
        heatingInspection.directionDetail = isEmpty(directionDetail)?nil:directionDetail;
        heatingInspection.checkElectricalControl = isEmpty(checkElectricalControl)?nil:checkElectricalControl;
        heatingInspection.electricalControlDetail = isEmpty(electricalControlDetail)?nil:electricalControlDetail;
        
        heatingInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
        heatingInspection.inspectionId = isEmpty(inspectionId)?nil:inspectionId;
        heatingInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
        heatingInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        
        altHeating.alternativeHeatingToSolarInspection = heatingInspection;
        heatingInspection.solarInspectionToHeating = altHeating;
    }
}

#pragma mark - Load & Save MVHR Inspection

- (void) saveMVHRInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating
{
    if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        AlternativeHeating *existingAltHeating = (AlternativeHeating *)[managedObjectContext objectWithID:altHeating.objectID];
        [[PSDataPersistenceManager sharedManager] loadMVHRInspectionForm:inspectionFormDictionary forAltHeating:existingAltHeating managedObjectContext:managedObjectContext];
        existingAltHeating.isInspected = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingAltHeating.alternativeHeatingToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
    }
}

- (void) loadMVHRInspectionForm:(NSDictionary *)inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    if(!isEmpty(inspectionFormDictionary) && !isEmpty(altHeating)){
        NSNumber *checkAirFlow = [inspectionFormDictionary valueForKey:kCheckAirFlow];
        NSString *airFlowDetail = [inspectionFormDictionary valueForKey:kAirFlowDetail];
        NSNumber *ductingInspection = [inspectionFormDictionary valueForKey:kDuctingInspection];
        NSString *ductingDetail = [inspectionFormDictionary valueForKey:kDuctingDetail];
        NSNumber *checkFilters = [inspectionFormDictionary valueForKey:kCheckFilters];
        NSString *filterDetail = [inspectionFormDictionary valueForKey:kFilterDetail];
        
        NSNumber *inspectionId = [inspectionFormDictionary valueForKey:kBoilerInspectionId];
        NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
        NSDate   *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
        NSNumber *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
        
        
        MVHRInspection *heatingInspection = [[PSCoreDataManager sharedManager] findRecordFrom:kMVHRInspectionEntity Field:kBoilerInspectionId WithValue:inspectionId context:managedObjectContext];
        if ( heatingInspection != nil)
        {
            CLS_LOG(@"MVHR Inspection updated");
        }
        else
        {
            heatingInspection = [NSEntityDescription insertNewObjectForEntityForName:kMVHRInspectionEntity inManagedObjectContext:managedObjectContext];
            CLS_LOG(@"New MVHR Inspection added");
        }
        
        heatingInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
        heatingInspection.inspectionId = isEmpty(inspectionId)?nil:inspectionId;
        heatingInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
        heatingInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        
        heatingInspection.checkAirFlow = isEmpty(checkAirFlow)?nil:checkAirFlow;
        heatingInspection.airFlowDetail = isEmpty(airFlowDetail)?nil:airFlowDetail;
        heatingInspection.ductingInspection = isEmpty(ductingInspection)?nil:ductingInspection;
        heatingInspection.ductingDetail = isEmpty(ductingDetail)?nil:ductingDetail;
        heatingInspection.checkFilters = isEmpty(checkFilters)?nil:checkFilters;
        heatingInspection.filterDetail = isEmpty(filterDetail)?nil:filterDetail;
        
        altHeating.alternativeHeatingToMVHRInspection = heatingInspection;
        heatingInspection.mvhrInspectionToHeating = altHeating;
    }
}


#pragma mark - Misc for Look ups

- (void) setBoilerManufacturer:(NSNumber *) manufacturerId ForAlHeat:(AlternativeHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    BoilerManufacturer * boilerManufacturer  = [[PSCoreDataManager sharedManager] findRecordFrom:kBoilerManufacturer Field:kBoilerManufacturerId WithValue:manufacturerId context:managedObjectContext];
    if (!isEmpty(boilerManufacturer)) {
        altHeat.alternativeHeatingToManufacturer = boilerManufacturer;
    }else{
        altHeat.alternativeHeatingToManufacturer = nil;
    }
    
}

- (void) setSolarType:(NSNumber *) solarTypeId ForAlHeat:(AlternativeHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    SolarType * slrTyp  = [[PSCoreDataManager sharedManager] findRecordFrom:kSolarType Field:kLookupId WithValue:solarTypeId context:managedObjectContext];
    if (!isEmpty(slrTyp)) {
        altHeat.alternativeHeatingToSolarType = slrTyp;
    }else{
        altHeat.alternativeHeatingToSolarType = nil;
    }
    
}

#pragma mark - Alt Fuel PropertyPicture Persistence
- (void) saveAltFuelPropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(AlternativeHeating*) propertyParam forType:(AFSPhotoGridRoot) root
{
    if(!isEmpty(pictureDictionary))
    {
        __block AlternativeFuelPicture *existingPicture = [[PSCoreDataManager sharedManager] altFuelPropertyPictureWithIdentifier:[pictureDictionary valueForKey:kPropertyPictureId]];
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            AlternativeHeating* property = (AlternativeHeating*)[managedObjectContext objectWithID:propertyParam.objectID];
            if (!property)
            {
                property = propertyParam;
            }
            AlternativeFuelPicture *propertyPicture = nil;
            
            if(existingPicture != nil)
            {
                //User already exists in database, so just update the existing object.
                propertyPicture = (AlternativeFuelPicture *)[managedObjectContext objectWithID:existingPicture.objectID];
            }
            else
            {
                //User does not exists in database, so create new user entity.
                propertyPicture = (AlternativeFuelPicture *)[NSEntityDescription insertNewObjectForEntityForName:kAlternativeFuelPicture inManagedObjectContext:managedObjectContext];
            }
            
            NSNumber *propertyPictureId = [pictureDictionary valueForKey:kPropertyPictureId];
            NSString * propertyId = [pictureDictionary valueForKey:kPropertyId];
            NSNumber *itemId = [pictureDictionary valueForKey:kItemId];
            NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
            NSString *imagePath = [pictureDictionary valueForKey:kPropertyPictureId];
            NSNumber *createdBy = [pictureDictionary valueForKey:kCreatedBy];
            NSString * isDefault=[pictureDictionary valueForKey:kPropertyPictureIsDefault];
            UIImage * propertyImage = [pictureDictionary valueForKey:kPropertyImage];
            NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
            NSNumber *schemeId = [pictureDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [pictureDictionary valueForKey:kSchemeBlockID];
            
            propertyPicture.propertyPictureId = isEmpty(propertyPictureId)?nil:propertyPictureId;
            propertyPicture.propertyId = isEmpty(propertyId)?nil:propertyId;
            propertyPicture.schemeId = isEmpty(schemeId)?nil:schemeId;
            propertyPicture.blockId = isEmpty(blockId)?nil:blockId;
            propertyPicture.itemId = itemId;
            propertyPicture.appointmentId = appointmentId;
            
            //propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
            if(root==AFSPhotoGridRootMVHRInspection){
                propertyPicture.alternativeFuelPictureToMVHRInspection = property.alternativeHeatingToMVHRInspection;
            }
            else if(root==AFSPhotoGridRootSolarInspection){
                propertyPicture.alternativeFuelPictureToSolarInspection = property.alternativeHeatingToSolarInspection;
            }
            if(root==AFSPhotoGridRootAirsourceInspection){
                propertyPicture.alternativeFuelPictureToAirSourceInspection = property.alternativeHeatingToAirsourceInspection;
            }
            
            
            propertyPicture.createdOn = [NSDate date];
            propertyPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
            propertyPicture.imageIdentifier = uniqueIdentifier;
            propertyPicture.imagePath = [propertyPicture getPicturePath];
            propertyPicture.syncStatus = @0;
            propertyPicture.isDefault = [NSNumber numberWithBool:NO];
            propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
            
            if([self saveDataInDB:managedObjectContext] == TRUE)
            {
                // if property picture is saved then need to update it on server
                AlternativeFuelPicture *propertyPicture1 = (AlternativeFuelPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
                [propertyPicture1 uploadPropertyPicture:pictureDictionary withImage:propertyImage];
            }
        }]; // parent
    }
}

@end
