//
//  PSDataPersistenceManager+OilServicing.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+OilServicing.h"

@implementation PSDataPersistenceManager (OilServicing)

#pragma mark - Save Fetched Oil Heating

-(void) saveFetchedOilHeatingData:(NSDictionary *) jsonData{
    if([UtilityClass isUserLoggedIn])
    {
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        if(!isEmpty(jsonData)){
            __block NSArray * heatingDataBlock = [jsonData objectForKey:kOilHeatings];
            if(!isEmpty(heatingDataBlock)){
                __block NSDictionary *heating = [heatingDataBlock objectAtIndex:0];
                __block NSString *propertyId = [heating objectForKey:kPropertyId];
                PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
                __block Property * property = [coreDataMgr findRecordFrom:kProperty
                                                                    Field:kPropertyId
                                                                WithValue:propertyId
                                                                  context:managedObjectContext];
                if(!isEmpty(property)){
                    [managedObjectContext performBlock:^{
                        [[PSDataPersistenceManager sharedManager] loadOilHeatingData:heatingDataBlock property:property managedObjectContext:managedObjectContext isAddedOffline:NO];
                        property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
                        [self saveDataInDB:managedObjectContext];
                    }];
                }
            }
        }
    }
}

- (void) loadOilHeatingData:(NSArray *)boilerData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline{
    if(!isEmpty(boilerData)){
        for(NSDictionary *boilerDictionary in boilerData){
            NSString *applianceLocation = [boilerDictionary valueForKey:kOilHeatingApplianceLocation];
            NSString *applianceMake = [boilerDictionary valueForKey:kOilHeatingApplianceMake];
            NSString *applianceModel = [boilerDictionary valueForKey:kOilHeatingApplianceModel];
            NSString *applianceSerialNumber = [boilerDictionary valueForKey:kOilHeatingApplianceSerialNumber];
            NSNumber *blockId = [boilerDictionary valueForKey:kSchemeBlockID];
            NSString *burnerMake = [boilerDictionary valueForKey:kBurnerMake];
            NSString *burnerModel = [boilerDictionary valueForKey:kBurnerModel];
            NSNumber *burnerTypeId = [boilerDictionary valueForKey:kBurnerTypeId];
            NSDate *certificateIssued = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kCertiticateIssued]];
            NSString *certificateName = [boilerDictionary valueForKey:kCertificateName];
            NSString *certificateNumber = [boilerDictionary valueForKey:kCertificateNumber];
            NSDate *certificateRenewal = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kCertificateRenewal]];
            NSNumber *flueTypeId = [boilerDictionary valueForKey:kFlueTypeId];
            NSNumber *fuelTypeId = [boilerDictionary valueForKey:kFuelTypeId];
            NSString *heatingFuel = [boilerDictionary valueForKey:kHeatingFuel];
            NSNumber *heatingId = [boilerDictionary valueForKey:kHeatingId];
            NSString *heatingName = [boilerDictionary valueForKey:kHeatingName];
            NSString *heatingType = [boilerDictionary valueForKey:kHeatingType];
            NSNumber *isInspected = [boilerDictionary valueForKey:kIsInspected];
            NSNumber *itemId= [boilerDictionary valueForKey:kItemId];
            NSDate *originalInstallDate = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kOriginalInstallDate]];
            NSString *propertyId = [boilerDictionary valueForKey:kPropertyId];
            NSNumber *schemeId = [boilerDictionary valueForKey:kSchemeID];
            NSNumber *tankTypeId = [boilerDictionary valueForKey:kTankTypeId];
            
            NSManagedObjectID *heatingObjectId = [boilerDictionary valueForKey:kHeatingObjectId];
            NSDictionary *fireServicingInspection = [boilerDictionary valueForKey:kOilFiringAndServiceForm];
            OilHeating * altHeat = nil;
            
            if (!isEmpty(heatingObjectId))
            {
                altHeat = (OilHeating *)[managedObjectContext objectWithID:heatingObjectId];
            }
            else{
                altHeat = [NSEntityDescription insertNewObjectForEntityForName:kOilHeatingEntity inManagedObjectContext:managedObjectContext];
            }
            altHeat.applianceLocation = isEmpty(applianceLocation)?nil:applianceLocation;
            altHeat.applianceMake = isEmpty(applianceMake)?nil:applianceMake;
            altHeat.applianceModel = isEmpty(applianceModel)?nil:applianceModel;
            altHeat.applianceSerialNumber = isEmpty(applianceSerialNumber)?nil:applianceSerialNumber;
            altHeat.blockId = isEmpty(blockId)?nil:blockId;
            altHeat.burnerMake = isEmpty(burnerMake)?nil:burnerMake;
            altHeat.burnerModel = isEmpty(burnerModel)?nil:burnerModel;
            altHeat.burnerTypeId = isEmpty(burnerTypeId)?nil:burnerTypeId;
            altHeat.heatingType = isEmpty(heatingType)?nil:heatingType;
            altHeat.certificateName = isEmpty(certificateName)?nil:certificateName;
            altHeat.certificateIssued = isEmpty(certificateIssued)?nil:certificateIssued;
            altHeat.certificateNumber = isEmpty(certificateNumber)?nil:certificateNumber;
            altHeat.certificateRenewel = isEmpty(certificateRenewal)?nil:certificateRenewal;
            altHeat.flueTypeId = isEmpty(flueTypeId)?nil:flueTypeId;
            altHeat.fuelTypeId = isEmpty(fuelTypeId)?nil:fuelTypeId;
            altHeat.heatingFuel = isEmpty(heatingFuel)?nil:heatingFuel;
            altHeat.heatingId = isEmpty(heatingId)?nil:heatingId;
            altHeat.heatingName = isEmpty(heatingName)?nil:heatingName;
            altHeat.heatingType = isEmpty(heatingType)?nil:heatingType;
            altHeat.propertyId = isEmpty(propertyId)?nil:propertyId;
            altHeat.schemeId = isEmpty(schemeId)?nil:schemeId;
            altHeat.isInspected = isEmpty(isInspected)?nil:isInspected;
            altHeat.itemId = isEmpty(itemId)?nil:itemId;
            altHeat.tankTypeId = isEmpty(tankTypeId)?nil:tankTypeId;
            altHeat.originalInstallDate = isEmpty(originalInstallDate)?nil:originalInstallDate;
            
            if(!isEmpty(burnerTypeId)){
                [self setBurnerType:burnerTypeId ForOilHeat:altHeat managedObjectContext:managedObjectContext];
            }
            if(!isEmpty(flueTypeId)){
                [self setOilFlueType:flueTypeId ForOilHeat:altHeat managedObjectContext:managedObjectContext];
            }
            if(!isEmpty(fuelTypeId)){
                [self setOilFuelType:fuelTypeId ForOilHeat:altHeat managedObjectContext:managedObjectContext];
            }
            if(!isEmpty(tankTypeId)){
                [self setOilTankType:tankTypeId ForOilHeat:altHeat managedObjectContext:managedObjectContext];
            }
            if ([altHeat.isInspected boolValue]){
                if(!isEmpty(fireServicingInspection)){
                    [[PSDataPersistenceManager sharedManager] loadFireServiceInspectionForm:fireServicingInspection forOilHeating:altHeat managedObjectContext:managedObjectContext];
                }
                else {
                    altHeat.oilHeatingToFireServiceInspection = nil;
                }
            }
            
            if (!isEmpty(property))
            {
                altHeat.oilHeatingToProperty = property;
                [property addPropertyToOilHeatingsObject:altHeat];
            }
            [self saveDataInDB:managedObjectContext];
        }
    }
}


#pragma mark - Load & Save Fire-Servicing Inspection

- (void) saveFireServicingInspectionForm:(NSDictionary *) inspectionFormDictionary forOilHeating:(OilHeating *)altHeating
{
    if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        OilHeating *existingAltHeating = (OilHeating *)[managedObjectContext objectWithID:altHeating.objectID];
        [[PSDataPersistenceManager sharedManager] loadFireServiceInspectionForm:inspectionFormDictionary forOilHeating:existingAltHeating managedObjectContext:managedObjectContext];
        existingAltHeating.isInspected = [NSNumber numberWithBool:YES];
        existingAltHeating.oilHeatingToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingAltHeating.oilHeatingToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
    }
}


- (void) loadFireServiceInspectionForm:(NSDictionary *)inspectionFormDictionary forOilHeating:(OilHeating *)altHeating managedObjectContext:(NSManagedObjectContext *)managedObjectContext{
    if(!isEmpty(inspectionFormDictionary) && !isEmpty(altHeating)){
        NSNumber *airSupply = [inspectionFormDictionary valueForKey:kAirSupply];
        NSString *airSupplyDetail = [inspectionFormDictionary valueForKey:kAirSupplyDetail];
        NSNumber *applianceSafety = [inspectionFormDictionary valueForKey:kApplianceSafety];
        NSString *applianceSafetyDetail = [inspectionFormDictionary valueForKey:kApplianceSafetyDetail];
        NSNumber *appointmentId = [inspectionFormDictionary valueForKey:kAppointmentId];
        NSNumber *chimneyFlue = [inspectionFormDictionary valueForKey:kChimneyFlue];
        NSString *chimneyFlueDetail = [inspectionFormDictionary valueForKey:kChimneyFlueDetail];
        NSNumber *combustionChamber = [inspectionFormDictionary valueForKey:kCombustionChamber];
        NSString *combustionChamberDetail = [inspectionFormDictionary valueForKey:kCombustionChamberDetail];
        NSNumber *controlCheck = [inspectionFormDictionary valueForKey:kControlCheck];
        NSString *controlCheckDetail = [inspectionFormDictionary valueForKey:kControlCheckDetail];
        NSNumber *electricalSafety = [inspectionFormDictionary valueForKey:kElectricalSafety];
        NSString *electricalSafetyDetail = [inspectionFormDictionary valueForKey:kElectricalSafetyDetail];
        NSNumber *heatExchanger = [inspectionFormDictionary valueForKey:kHeatExchanger];
        NSString *heatExchangerDetail = [inspectionFormDictionary valueForKey:kHeatExchangerDetail];
        NSNumber *heatingId = [inspectionFormDictionary valueForKey:kHeatingId];
        NSNumber *hotWaterType = [inspectionFormDictionary valueForKey:kHotWaterType];
        NSString *hotWaterTypeDetail = [inspectionFormDictionary valueForKey:kHotWaterTypeDetail];
        NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
        NSDate   *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
        NSNumber *inspectionId = [inspectionFormDictionary valueForKey:kInspectionId];
        NSNumber *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
        NSNumber *journalId = [inspectionFormDictionary valueForKey:kJournalId];
        NSNumber *oilStorage = [inspectionFormDictionary valueForKey:kOilStorage];
        NSString *oilStorageDetail = [inspectionFormDictionary valueForKey:kOilStorageDetail];
        NSNumber *oilSupplySystem = [inspectionFormDictionary valueForKey:kOilSupplySystem];
        NSString *oilSupplySystemDetail = [inspectionFormDictionary valueForKey:kOilSupplySystemDetail];
        NSNumber *pressureJet = [inspectionFormDictionary valueForKey:kPressureJet];
        NSString *pressureJetDetail = [inspectionFormDictionary valueForKey:kPressureJetDetail];
        NSNumber *vaporisingBurner = [inspectionFormDictionary valueForKey:kVaporisingBurner];
        NSString *vaporisingBurnerDetail = [inspectionFormDictionary valueForKey:kVaporisingBurnerDetail];
        NSNumber *wallflameBurner = [inspectionFormDictionary valueForKey:kWallflameBurner];
        NSString *wallflameBurnerDetail = [inspectionFormDictionary valueForKey:kWallflameBurnerDetail];
        NSNumber *warmAirType = [inspectionFormDictionary valueForKey:kWarmAirType];
        NSString *warmAirTypeDetail = [inspectionFormDictionary valueForKey:kWarmAirTypeDetail];
        
        OilFiringServiceInspection *heatingInspection = [[PSCoreDataManager sharedManager] findRecordFrom:kOilFireServiceInspectionEntity Field:kBoilerInspectionId WithValue:inspectionId context:managedObjectContext];
        if ( heatingInspection != nil)
        {
            CLS_LOG(@"Fire Servicing Inspection updated");
        }
        else
        {
            heatingInspection = [NSEntityDescription insertNewObjectForEntityForName:kOilFireServiceInspectionEntity inManagedObjectContext:managedObjectContext];
            CLS_LOG(@"New Fire Servicing Inspection added");
        }
        
        heatingInspection.airSupply = isEmpty(airSupply)?nil:airSupply;
        heatingInspection.airSupplyDetail = isEmpty(airSupplyDetail)?nil:airSupplyDetail;
        heatingInspection.applianceSafety = isEmpty(applianceSafety)?nil:applianceSafety;
        heatingInspection.applianceSafetyDetail = isEmpty(applianceSafetyDetail)?nil:applianceSafetyDetail;
        heatingInspection.appointmentId = isEmpty(appointmentId)?nil:appointmentId;
        heatingInspection.chimneyFlue = isEmpty(chimneyFlue)?nil:chimneyFlue;
        heatingInspection.chimneyFlueDetail = isEmpty(chimneyFlueDetail)?nil:chimneyFlueDetail;
        heatingInspection.combustionChamber = isEmpty(combustionChamber)?nil:combustionChamber;
        heatingInspection.combustionChamberDetail = isEmpty(combustionChamberDetail)?nil:combustionChamberDetail;
        heatingInspection.controlCheck = isEmpty(controlCheck)?nil:controlCheck;
        heatingInspection.controlCheckDetail = isEmpty(controlCheckDetail)?nil:controlCheckDetail;
        heatingInspection.electricalSafety = isEmpty(electricalSafety)?nil:electricalSafety;
        heatingInspection.electricalSafetyDetail = isEmpty(electricalSafetyDetail)?nil:electricalSafetyDetail;
        heatingInspection.heatExchanger = isEmpty(heatExchanger)?nil:heatExchanger;
        heatingInspection.heatExchangerDetail = isEmpty(heatExchangerDetail)?nil:heatExchangerDetail;
        heatingInspection.heatingId = isEmpty(heatingId)?nil:heatingId;
        heatingInspection.hotWaterType = isEmpty(hotWaterType)?nil:hotWaterType;
        heatingInspection.hotWaterTypeDetail = isEmpty(hotWaterTypeDetail)?nil:hotWaterTypeDetail;
        heatingInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
        heatingInspection.inspectionId = isEmpty(inspectionId)?nil:inspectionId;
        heatingInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
        heatingInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        heatingInspection.journalId = isEmpty(journalId)?nil:journalId;
        heatingInspection.oilStorage = isEmpty(oilStorage)?nil:oilStorage;
        heatingInspection.oilStorageDetail = isEmpty(oilStorageDetail)?nil:oilStorageDetail;
        heatingInspection.oilSupplySystem = isEmpty(oilSupplySystem)?nil:oilSupplySystem;
        heatingInspection.oilSupplySystemDetail = isEmpty(oilSupplySystemDetail)?nil:oilSupplySystemDetail;
        heatingInspection.pressureJet = isEmpty(pressureJet)?nil:pressureJet;
        heatingInspection.pressureJetDetail = isEmpty(pressureJetDetail)?nil:pressureJetDetail;
        heatingInspection.vaporisingBurner = isEmpty(vaporisingBurner)?nil:vaporisingBurner;
        heatingInspection.vaporisingBurnerDetail = isEmpty(vaporisingBurnerDetail)?nil:vaporisingBurnerDetail;
        heatingInspection.warmAirType = isEmpty(warmAirType)?nil:warmAirType;
        heatingInspection.warmAirTypeDetail = isEmpty(warmAirTypeDetail)?nil:warmAirTypeDetail;
        heatingInspection.wallflameBurner = isEmpty(wallflameBurner)?nil:wallflameBurner;
        heatingInspection.wallflameBurnerDetail = isEmpty(wallflameBurnerDetail)?nil:wallflameBurnerDetail;
        altHeating.oilHeatingToFireServiceInspection = heatingInspection;
        heatingInspection.firingServiceInspectionToOilHeating = altHeating;
        
    }
}

#pragma mark - Misc for Look ups

- (void) setBurnerType:(NSNumber *) burnerTypeId ForOilHeat:(OilHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    BurnerType * brnTyp  = [[PSCoreDataManager sharedManager] findRecordFrom:kBurnerTypeEntity Field:kLookupId WithValue:burnerTypeId context:managedObjectContext];
    if (!isEmpty(brnTyp)) {
        altHeat.oilHeatingToBurnerType = brnTyp;
    }else{
        altHeat.oilHeatingToBurnerType = nil;
    }
    
}

- (void) setOilFlueType:(NSNumber *) flueTypeId ForOilHeat:(OilHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    OilFlueType * flueType  = [[PSCoreDataManager sharedManager] findRecordFrom:kOilFlueTypeEntity Field:kLookupId WithValue:flueTypeId context:managedObjectContext];
    if (!isEmpty(flueType)) {
        altHeat.oilHeatingToOilFlueType = flueType;
    }else{
        altHeat.oilHeatingToOilFlueType = nil;
    }
    
}

- (void) setOilFuelType:(NSNumber *) fuelTypeId ForOilHeat:(OilHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    OilFuelType * fuelType  = [[PSCoreDataManager sharedManager] findRecordFrom:kOilFuelTypeEntity Field:kLookupId WithValue:fuelTypeId context:managedObjectContext];
    if (!isEmpty(fuelType)) {
        altHeat.oilHeatingToOilFuelType = fuelType;
    }else{
        altHeat.oilHeatingToOilFuelType = nil;
    }
    
}

- (void) setOilTankType:(NSNumber *) tankTypeId ForOilHeat:(OilHeating *)altHeat managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    TankType * fuelType  = [[PSCoreDataManager sharedManager] findRecordFrom:kTankTypeEntity Field:kLookupId WithValue:tankTypeId context:managedObjectContext];
    if (!isEmpty(fuelType)) {
        altHeat.oilHeatingToTankType = fuelType;
    }else{
        altHeat.oilHeatingToTankType = nil;
    }
    
}


@end
