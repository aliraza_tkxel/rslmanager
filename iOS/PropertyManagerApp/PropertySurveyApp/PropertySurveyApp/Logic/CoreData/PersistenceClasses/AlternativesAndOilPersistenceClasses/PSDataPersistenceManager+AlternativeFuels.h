//
//  PSDataPersistenceManager+AlternativeFuels.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
typedef void (^CompletionStandardResponse) (BOOL success, id response, NSString *error);
@interface PSDataPersistenceManager (AlternativeFuels)
-(void) saveFetchedAlternativeHeatingData:(NSDictionary *) jsonData;
- (void) saveAirSourceInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating;
- (void) saveSolarInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating;
- (void) saveMVHRInspectionForm:(NSDictionary *) inspectionFormDictionary forAltHeating:(AlternativeHeating *)altHeating;
- (void) saveAltFuelPropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(AlternativeHeating*) propertyParam forType:(AFSPhotoGridRoot) root;
@end
