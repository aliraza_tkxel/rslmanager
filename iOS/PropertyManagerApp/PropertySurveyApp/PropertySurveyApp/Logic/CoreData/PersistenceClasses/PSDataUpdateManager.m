//
//  PSDataUpdateManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 06/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDataUpdateManager.h"
#import "PSEventsManager.h"
#import "PropertyPicture+JSON.h"
#import "PSSynchronizationManager.h"
#import "AlternativeFuelPicture+MWPhoto.h"
#import "AlternativeFuelPicture+JSON.h"
@implementation PSDataUpdateManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDataUpdateManager *sharedManagerObject = nil;
+ (PSDataUpdateManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManagerObject = [[PSDataUpdateManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedManagerObject;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManagerObject = [super allocWithZone:zone];
    });
    return sharedManagerObject;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Remove Methods

- (void) deleteAppointment:(Appointment *)appointment
{
    if(appointment != nil)
    {
        if ([appointment.addToCalendar boolValue])
        {
            [[PSAppointmentsManager sharedManager]deleteCalendarEventForAppointment:appointment];
        }
        [self deleteAppInfoData:appointment.appointmentToAppInfoData];
        [self deleteCP12Info:appointment.appointmentToCP12Info];
        [self deleteJournal:appointment.appointmentToJournal];
        [self deleteProperty:appointment.appointmentToProperty];
			  [self deleteScheme:appointment.appointmentToScheme];
        [self deleteSurvey:appointment.appointmentToSurvey];
        
        for(Customer *customer in appointment.appointmentToCustomer)
        {
            [self deleteCustomer:customer];
        }
        
        for(JobSheet *jobData in appointment.appointmentToJobSheet)
        {
            [self deleteJobDataList:jobData];
        }
        
        [self deletePlannedTradeComponent:appointment.appointmentToPlannedComponent];
        
        [appointment removeAppointmentToCustomer:appointment.appointmentToCustomer];
        
        [appointment removeAppointmentToJobSheet:appointment.appointmentToJobSheet];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appointment objectID]];
        //[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveSuccessNotification object:nil];
    }
}

- (void) deleteSurvey:(Survey *)appointmentSurvey
{
    if (!isEmpty(appointmentSurvey))
    {
        for (SurveyData *surveyData in appointmentSurvey.surveyToSurveyData)
        {
            [self deleteSurveyData:surveyData];
        }
        [appointmentSurvey removeSurveyToSurveyData:appointmentSurvey.surveyToSurveyData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:appointmentSurvey.objectID];
    }
}

- (void) deleteSurveyData:(SurveyData *)surveyData
{
    if (surveyData != nil)
    {
        for (FormData *formData in surveyData.surveyDataToFormData)
        {
            [self deleteFormData:formData];
        }
        [surveyData removeSurveyDataToFormData:surveyData.surveyDataToFormData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:surveyData.objectID];
    }
}

- (void) deleteFormData:(FormData *)formData
{
    if (formData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:formData.objectID];
    }
}
- (void) deleteCustomer:(Customer *)customer
{
    if(customer != nil)
    {
        [self deleteAddress:customer.customerToAddress];
        
        for (CustomerVulnerabilityData * vulunarityData in customer.customerToCustomerVulunarityData) {
            [self deleteCustomerVulnerabilityData:vulunarityData];
        }
        [customer removeCustomerToCustomerRiskData:customer.customerToCustomerVulunarityData];
        //[self deleteCustomerVulnerabilityData:customer.customerToCustomerVulunarityData];
        for(CustomerRiskData *customerRiskData in customer.customerToCustomerRiskData)
        {
            [self deleteCustomerRiskData:customerRiskData];
        }
        [customer removeCustomerToCustomerVulunarityData:customer.customerToCustomerVulunarityData];
        [customer removeCustomerToCustomerRiskData:customer.customerToCustomerRiskData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customer objectID]];
    }
}

- (void) deleteAddress:(Address *)address
{
    if(address != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[address objectID]];
    }
}

- (void) deleteAppInfoData:(AppInfoData *)appInfoData
{
    if(appInfoData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appInfoData objectID]];
    }
}

- (void) deleteCP12Info:(CP12Info *)cp12Info
{
    if(cp12Info != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[cp12Info objectID]];
    }
}

- (void) deleteCustomerVulnerabilityData:(CustomerVulnerabilityData *)customerVulnerabilityData
{
    if(customerVulnerabilityData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customerVulnerabilityData objectID]];
    }
}

- (void) deleteCustomerRiskData:(CustomerRiskData *)customerRiskData
{
    if(customerRiskData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customerRiskData objectID]];
    }
}

- (void) deleteFaultRepairData:(FaultRepairData *)faultRepairData
{
    if(faultRepairData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[faultRepairData objectID]];
    }
}

- (void) deleteFaultRepairHistoryData:(FaultRepairListHistory *)faultRepairHistoryData
{
    if(faultRepairHistoryData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[faultRepairHistoryData objectID]];
    }
}

- (void) deleteJobPauseData:(JobPauseData *)jobPauseData
{
    if(jobPauseData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[jobPauseData objectID]];
    }
}


- (void) deletePlannedTradeComponent:(PlannedTradeComponent *)plannedTradeComponent
{
    if(plannedTradeComponent != nil)
    {
        //        for (JobPauseData *jobPauseData in jobDataList.jobPauseData)
        //        {
        //            [self deleteJobPauseData:jobPauseData];
        //        }
        //
        [plannedTradeComponent removeJobPauseData:plannedTradeComponent.jobPauseData];
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[plannedTradeComponent objectID]];
    }
}

- (void) deleteJobDataList:(JobSheet *)jobSheet
{
    if(jobSheet != nil)
    {
			for (JobPauseData *jobPauseData in jobSheet.jobPauseData)
			{
				[self deleteJobPauseData:jobPauseData];
			}
			for (RepairPictures *repairImage in jobSheet.jobSheetToRepairImages)
			{
				[self deleteRepairImage:repairImage];
			}
			
			[jobSheet removeJobSheetToRepairImages:jobSheet.jobSheetToRepairImages];
			[jobSheet removeJobPauseData:jobSheet.jobPauseData];
			
			if([jobSheet isKindOfClass:[FaultJobSheet class]] == TRUE)
			{
				FaultJobSheet * faultJobSheet = (FaultJobSheet *)jobSheet;
        for(FaultRepairData *faultRepairData in faultJobSheet.faultRepairDataList)
        {
            [self deleteFaultRepairData:faultRepairData];
        }
        for (FaultRepairListHistory *faultRepairHistory in faultJobSheet.faultRepairHistoryList)
        {
            [self deleteFaultRepairHistoryData:faultRepairHistory];
        }
        [faultJobSheet removeFaultRepairHistoryList:faultJobSheet.faultRepairHistoryList];
        [faultJobSheet removeFaultRepairDataList:faultJobSheet.faultRepairDataList];
			}
			[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[jobSheet objectID]];
    }
}

- (void) deleteRepairImage:(RepairPictures *)repairPicture
{
    if(repairPicture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[repairPicture objectID]];
    }
}


- (void) deleteJournal:(Journal *)journal
{
    if(journal != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[journal objectID]];
    }
}

- (void) deleteProperty:(Property *)property
{
	if(property != nil)
	{
		for(Accomodation *accommodation in property.propertyToAccomodation)
		{
			[self deleteAccommodation:accommodation];
		}
		for(PropertyAsbestosData *propertyAsbestosData in property.propertyToPropertyAsbestosData)
		{
			[self deletePropertyAsbestosData:propertyAsbestosData];
		}
		for(PropertyPicture *propertyPicture in property.propertyToPropertyPicture)
		{
			[self deletePropertyPicture:propertyPicture];
		}
		for (Appliance *appliance in property.propertyToAppliances)
		{
			[self deleteAppliance:appliance];
		}
		for (Detector *detector in property.propertyToDetector)
		{
			[self deleteDetector:detector];
		}
		
		for (Meter *meter in property.propertyToMeter)
		{
			[self deleteMeter:meter];
		}
        
		for (Boiler *boiler in property.propertyToBoiler)
		{
            
            [self deleteInstallationPipework:boiler.boilerToGasInstallationPipeWork];
			[self deleteBoiler:boiler];
		}
        
		[self deletePicture:property.defaultPicture];
		[property removePropertyToDetector:property.propertyToDetector];
		[property removePropertyToMeter:property.propertyToMeter];
		[property removePropertyToAppliances:property.propertyToAppliances];
		[property removePropertyToAccomodation:property.propertyToAccomodation];
		[property removePropertyToPropertyAsbestosData:property.propertyToPropertyAsbestosData];
		[property removePropertyToPropertyPicture:property.propertyToPropertyPicture];
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[property objectID]];
	}
}

- (void) deleteScheme:(Scheme *)scheme
{
    if(scheme != nil)
    {
        for(SchemeAsbestosData *schemeAsbestosData in scheme.schemeToSchemeAsbestosData)
        {
            [self deleteSchemeAsbestosData:schemeAsbestosData];
        }
        [scheme removeSchemeToSchemeAsbestosData:scheme.schemeToSchemeAsbestosData];

        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[scheme objectID]];
    }
}

- (void) deleteInstallationPipework:(InstallationPipework *)installationPipework
{
    if(installationPipework != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[installationPipework objectID]];
    }
}

- (void) deletePicture:(PropertyPicture *)picture
{
    if(picture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[picture objectID]];
    }
}

- (void) deleteAccommodation:(Accomodation *)accommodation
{
    if(accommodation != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[accommodation objectID]];
    }
}

- (void) deleteSchemeAsbestosData:(SchemeAsbestosData *)schemeAsbestosData
{
	if(schemeAsbestosData != nil)
	{
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[schemeAsbestosData objectID]];
	}
}

- (void) deletePropertyAsbestosData:(PropertyAsbestosData *)propertyAsbestosData
{
    if(propertyAsbestosData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyAsbestosData objectID]];
    }
}

- (void) deletePropertyPicture:(PropertyPicture *)propertyPicture
{
    if(propertyPicture != nil)
    {
        if (propertyPicture.propertyPictureToProperty.defaultPicture.objectID==propertyPicture.objectID) {
            
            propertyPicture.propertyPictureToProperty.defaultPicture=nil;
            
        }
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyPicture objectID]];
    }
}

- (void) markDeletePropertyPicture:(PropertyPicture *)propertyPicture
{
    if(propertyPicture != nil)
    {
        if (propertyPicture.propertyPictureToProperty.defaultPicture.objectID==propertyPicture.objectID) {
            
            propertyPicture.propertyPictureToProperty.defaultPicture=nil;
            
        }
        propertyPicture.deletedPicture = [NSNumber numberWithBool:YES];
        propertyPicture.propertyPictureToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        NSError * error = nil;
        [[PSDataUpdateManager sharedManager] persistPropertyPicture:propertyPicture];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyPicture objectID]];
    }
}

- (void) deleteDefectPicture:(DefectPicture *)defectPicture
{
    if(defectPicture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defectPicture objectID]];
    }
}


- (void) deleteAppliance:(Appliance *)appliance
{
    if(appliance != nil)
    {//todo
      
        for (Defect *defect in appliance.applianceDefects)
        {
            [self deleteDefect:defect];
        }
        //   [self deleteApplianceLocation:appliance.applianceLocation];
        //  [self deleteApplianceManufacturer:appliance.applianceManufacturer];
        // [self deleteApplianceModel:appliance.applianceModel];
        //[self deleteApplianceType:appliance.applianceType];
        [self deleteApplianceInspection:appliance.applianceInspection];
        [appliance removeApplianceDefects:appliance.applianceDefects];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appliance objectID]];
      
    }
}

- (void) deleteBoiler:(Boiler *)boiler
{
	if(boiler != nil)
	{
		
		for (Defect *defect in boiler.boilerToDefect)
		{
			[self deleteDefect:defect];
		}

		[self deleteBoilerInspection:boiler.boilerToBoilerInspection];
		[boiler removeBoilerToDefect:boiler.boilerToDefect];
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[boiler objectID]];
		
	}

}

- (void) deleteMeter:(Meter *)meter
{
	if(meter != nil)
	{
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[meter objectID]];
	}
}

- (void) deleteDetector:(Detector *)detector
{
    if(detector != nil)
    {
      
        for (Defect *defect in detector.detectorDefects)
        {
            [self deleteDefect:defect];
        }
        [detector removeDetectorDefects:detector.detectorDefects];
        //[self deleteDetectorInspection:detector.detectorInspection];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[detector objectID]];
      
    }
}

- (void) deleteBoilerInspection:(BoilerInspection *)boilerInspection
{
	if(boilerInspection != nil)
	{
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[boilerInspection objectID]];
	}
}

- (void) deleteApplianceInspection:(ApplianceInspection *)applianceInspection
{
    if(applianceInspection != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceInspection objectID]];
    }
}


- (void) deleteDetectorInspection:(DetectorInspection *)detectorInspection
{
    if(detectorInspection != nil)
    {
      //     [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[detectorInspection objectID]];
    }
}
- (void) deleteDefect:(Defect *)defect
{
    if(defect != nil)
    {
        for (DefectPicture *defectPicture in defect.defectPictures)
        {
            [self deleteDefectPicture:defectPicture];
        }
        [self deleteDefectCategory:defect.defectCategory];
        
        [defect removeDefectPictures:defect.defectPictures];
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defect objectID]];
    }
}

- (void) deleteDefectCategory:(DefectCategory *)defectCategory
{
    if(defectCategory != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defectCategory objectID]];
    }
}

- (void) deleteApplianceLocation:(ApplianceLocation *)applianceLocation
{
    if(applianceLocation != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceLocation objectID]];
    }
}

- (void) deleteApplianceManufacturer:(ApplianceManufacturer *)applianceManufacturer
{
    if(applianceManufacturer != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceManufacturer objectID]];
    }
}

- (void) deleteApplianceModel:(ApplianceModel *)applianceModel
{
    if(applianceModel != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceModel objectID]];
    }
}

- (void) deleteApplianceType:(ApplianceType *)applianceType
{
    if(applianceType != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceType objectID]];
    }
}

#pragma mark - Update Methods

- (void) updateAppointmentSurvey:(NSDictionary *)surveyData forAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(surveyData != nil && appointment != nil)
    {
        __block NSManagedObjectID* appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            
            if(existingAppointment != nil)
            {
                existingAppointment.appointmentToSurvey.surveyJSON = [surveyData JSONRepresentation];
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // mainobject:nil];
        }]; // parent
    }
    ENDEXCEPTION
    
}

- (void) updateAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary
{
	STARTEXCEPTION
	if(noEntryDictionary != nil)
	{
		__block NSManagedObjectID* appointmentId = [noEntryDictionary valueForKey:@"objectId"];
		// Creating managed objects
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
		[managedObjectContext performBlock:^{
			Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
			if(existingAppointment != nil)
			{
				AppInfoData *appInfo = existingAppointment.appointmentToAppInfoData;
				if (!isEmpty(appInfo))
				{
					NSNumber *isCardLeft = [noEntryDictionary valueForKey:@"isCardLeft"];
					appInfo.totalNoEntries = [NSNumber numberWithInt:[appInfo.totalNoEntries intValue] + 1];
					appInfo.isCardLeft = isEmpty(isCardLeft) ? nil : isCardLeft;
					existingAppointment.appointmentStatus = kAppointmentStatusNoEntry;
					//appInfo.appInfoDataToAppointment.appointmentEndTime = [NSDate date];
					existingAppointment.isModified=[NSNumber numberWithBool:YES];
					existingAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
                    existingAppointment.loggedDate = [NSDate date];
                    existingAppointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
				}
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentWithNoEntryFailureNotification object:nil];
				return;
			}
			
			[managedObjectContext performBlock:^{
				// Save the context.
				NSError *error = nil;
				if (![managedObjectContext save:&error])
				{
						CLS_LOG(@"Error in Saving MOC: %@",[error description]);
				}
			}]; // main
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentWithNoEntrySuccessNotification object:nil];
		}]; // parent
	}
	ENDEXCEPTION
}


- (void) updateFaultAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary
{
    STARTEXCEPTION
    if(noEntryDictionary != nil)
    {
        __block NSManagedObjectID* appointmentId = [noEntryDictionary valueForKey:@"objectId"];
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            
            if(existingAppointment != nil)
            {
                NSString *noEntryNotes = [noEntryDictionary valueForKey:kNoEntryNotes];
                existingAppointment.noEntryNotes = isEmpty(noEntryNotes)?nil:noEntryNotes;
                //   existingAppointment.appointmentEndTime = [NSDate date];
                existingAppointment.appointmentStatus = kAppointmentStatusNoEntry;
                existingAppointment.loggedDate = [NSDate date];
                existingAppointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
                
                if (existingAppointment.appointmentToPlannedComponent) {
                    
                    existingAppointment.appointmentToPlannedComponent.jobStatus=kJobStatusComplete;
                    
                }
                else
                {
                    NSArray *jobs = [existingAppointment.appointmentToJobSheet allObjects];
                    [self updateFaultJobWithNoEntry:jobs inManagedObjectContext:managedObjectContext];
                }
                
                existingAppointment.isModified=[NSNumber numberWithBool:YES];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateFaultAppointmentWithNoEntryFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateFaultAppointmentWithNoEntrySuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updateFaultJobWithNoEntry:(NSArray *)JobDataListArray inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (!isEmpty(JobDataListArray))
    {
        for (FaultJobSheet *jobData in JobDataListArray)
        {
            FaultJobSheet *existingJobData = (FaultJobSheet *)[managedObjectContext objectWithID:jobData.objectID];
            if (!isEmpty(existingJobData))
            {
                existingJobData.jobStatus = kJobStatusNoEntry;
                existingJobData.completionDate = [NSDate date];
            }
        }
    }
}

- (void) updateAccomodations:(NSDictionary *)accomodationsInfo
{
    STARTEXCEPTION
    if(!isEmpty(accomodationsInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            NSArray *accomodationIDs = [accomodationsInfo allKeys];
            for(NSManagedObjectID *accomodationID in accomodationIDs)
            {
                NSDictionary *accomodationValues = (NSDictionary *)[accomodationsInfo objectForKey:accomodationID];
                Accomodation *accomodation;
                if (!isEmpty(accomodationID)) {
                    accomodation = (Accomodation *)[managedObjectContext objectWithID:accomodationID];
                }
                
                if(accomodation != nil)
                {
                    accomodation.roomWidth = [accomodationValues objectForKey:kRoomWidth];
                    accomodation.roomLength = [accomodationValues objectForKey:kRoomLength];
                    accomodation.accomodationToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
                    accomodation.updatedOn = [NSDate date];
                    accomodation.updatedBy = [[SettingsClass sharedObject]loggedInUser].userId;
                }
            }
					
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAccomodationInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) turnAccomodationsIntoFault:(NSDictionary *)accomodationsInfo
{
    STARTEXCEPTION
    if(!isEmpty(accomodationsInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            NSArray *accomodationIDs = [accomodationsInfo allKeys];
            for(NSManagedObjectID *accomodationID in accomodationIDs)
            {
                // NSDictionary *accomodationValues = (NSDictionary *)[accomodationsInfo objectForKey:accomodationID];
                Accomodation *accomodation;
                if (!isEmpty(accomodationID)) {
                    accomodation = (Accomodation *)[managedObjectContext objectWithID:accomodationID];
                }
                
                [PSDatabaseContext turnObjectIntoFault:accomodation.objectID];
                [PSDatabaseContext turnObjectIntoFault:accomodation.accomodationToProperty.propertyToAppointment.objectID];
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAccomodationInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
    
}
- (void) updateTenant:(NSDictionary *)tenantInfo
{
    STARTEXCEPTION
    if(!isEmpty(tenantInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            NSArray *tenantIDs = [tenantInfo allKeys];
            for(NSManagedObjectID *tenantID in tenantIDs)
            {
                NSDictionary *tenantDetails = (NSDictionary *)[tenantInfo objectForKey:tenantID];
                Customer *customer = nil;
                if (!isEmpty(tenantID)) {
                    customer = (Customer *)[managedObjectContext objectWithID:tenantID];
                }
                
                if(customer != nil)
                {
                    customer.customerToAppointment.isModified = [NSNumber numberWithBool:YES];
                    NSString *email = [tenantDetails objectForKey:kCustomerEmail];
                    NSString *mobileNumber = [tenantDetails objectForKey:kCustomerMobile];
                    NSString *telephoneNumber = [tenantDetails objectForKey:kCustomerTelephone];
                    customer.email = isEmpty(email)?nil:email;
                    customer.telephone = isEmpty(telephoneNumber)?nil:telephoneNumber;
                    customer.mobile = isEmpty(mobileNumber)?nil:mobileNumber;
                }
                [PSDatabaseContext turnObjectIntoFault:customer.customerToAppointment.objectID];
            }
					
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kTenantInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) startAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        // Creating managed objects
         NSManagedObjectID *appointmentId = appointment.objectID;
         NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        if (!isEmpty(appointmentId))
        {
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            if(existingAppointment != nil)
            {
                existingAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
                existingAppointment.appointmentStatus = kAppointmentStatusInProgress;
                existingAppointment.isModified = [NSNumber numberWithBool:YES];
                existingAppointment.appointmentActualStartTime = [NSDate date];
                NSDate * date = [NSDate date];
                [[existingAppointment managedObjectContext]save:nil];
                [[existingAppointment managedObjectContext]save:nil];
                [self addAppointmentHistoryLogWithAction:kAppointmentStatusInProgress
                                          forAppointment:appointment];
            }
        }
        
        NSError *error = nil;
        if (![managedObjectContext save:&error])
        {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentStartNotification object:nil];
    }
    ENDEXCEPTION
}

- (void) acceptAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        // Creating managed objects
         NSManagedObjectID *appointmentId = appointment.objectID;
         NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        if (!isEmpty(appointmentId))
        {
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            if(existingAppointment != nil)
            {
                existingAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
                existingAppointment.appointmentStatus = kAppointmentStatusAccepted;
                existingAppointment.isModified = [NSNumber numberWithBool:YES];
                [self addAppointmentHistoryLogWithAction:kAppointmentStatusAccepted
                                          forAppointment:appointment];
            }
        }
        
        NSError *error = nil;
        
        if (![managedObjectContext save:&error])
        {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentStartNotification object:nil];
    }
    ENDEXCEPTION
}

/*!
 @discussion
 Update Repair Picture.
 */
- (void) updateRepairPicture:(RepairPictures*)repairPicture forDictionary:(NSDictionary *) repairPictureDetails
{
    STARTEXCEPTION
    if(repairPicture != nil)
    {
        
        __block NSManagedObjectID* repairPictureId = repairPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            RepairPictures *existingRepairPicture = (RepairPictures *)[managedObjectContext objectWithID:repairPictureId];
            
            if(existingRepairPicture != nil)
            {
                if ([repairPictureDetails objectForKey:kRepairImagePath])
                {
                    existingRepairPicture.imagePath = [repairPictureDetails objectForKey:kRepairImagePath];
                }
                
                if ([repairPictureDetails objectForKey:kFaultRepairImageId]) {
                    existingRepairPicture.imageId = [repairPictureDetails objectForKey:kFaultRepairImageId];
                }
                
                if ([repairPictureDetails objectForKey:kRepairImageSyncStatus]) {
                    existingRepairPicture.syncStatus = [repairPictureDetails objectForKey:kRepairImageSyncStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kRepairPictureSaveFailureNotification object:nil];
                return;
            }
					
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
        }]; // parent
    }
    ENDEXCEPTION
}

/*!
 @discussion
 Update property picture
 */
- (void) updatePropertyPicture:(PropertyPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

        [managedObjectContext performBlock:^{
            
            PropertyPicture *existingPropertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                
                if ([[propertyPictureDetails objectForKey:kPropertyPictureIsDefault] isEqualToString:@"true"]) {
                    
                    existingPropertyPicture.propertyPictureToProperty.defaultPicture=existingPropertyPicture;
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPictureId]) {
                    existingPropertyPicture.propertyPictureId = [propertyPictureDetails objectForKey:kPropertyPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
								else
                {
                    PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
                    
                    if (propertyPicture. propertyPictureToProperty.defaultPicture==propertyPicture)
										{
                        
                        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureDefaultNotification
                                                                                            object:propertyPicture];
                        
                        // if property picture is saved then need to update it on server
                        
                        if([propertyPicture.synchStatus integerValue]!=1)
                        {
                            
                            PropertyPicture *propertyPicture1 = (PropertyPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
                            
                            [propertyPicture1 uploadPicture];
                            
                        }
                        
                        
                    }
										else if([propertyPicture.synchStatus integerValue]!=1)
                    {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                                            object:propertyPicture];
                    }
                }
            }]; // main
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updateAltFuelPicture:(AlternativeFuelPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            AlternativeFuelPicture *existingPropertyPicture = (AlternativeFuelPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:@"HeatingInspectionImageId"]) {
                    existingPropertyPicture.propertyPictureId = [propertyPictureDetails objectForKey:@"HeatingInspectionImageId"];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.syncStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
                else
                {
                    AlternativeFuelPicture *propertyPicture = (AlternativeFuelPicture *)[managedObjectContext objectWithID:propertyPictureId];
                    
                    if([propertyPicture.syncStatus integerValue]!=1)
                    {
                        
                        AlternativeFuelPicture *propertyPicture1 = (AlternativeFuelPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
                        
                        [propertyPicture1 uploadPicture];
                        
                    }
                    else if([propertyPicture.syncStatus integerValue]!=1)
                    {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAltFuelPictureSaveNotification
                                                                                            object:propertyPicture];
                    }
                }
            }]; // main
        }]; // parent
    }
    ENDEXCEPTION
}



/*!
 @discussion
 persist  property picture Data
 */
- (void) persistPropertyPicture:(PropertyPicture*)propertyPicture
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

                [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error])
                {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                    
                }
            }]; // main
        
    }
    ENDEXCEPTION
}

/*!
 @discussion
   save Updated records in core data entity
 */
- (void) updatePropertyProfilePicture:(PropertyPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails

{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            PropertyPicture *existingPropertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                existingPropertyPicture.propertyPictureToProperty.defaultPicture=existingPropertyPicture;
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPictureId]) {
                    existingPropertyPicture.propertyPictureId = [propertyPictureDetails objectForKey:kPropertyPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            
            PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if (propertyPicture. propertyPictureToProperty.defaultPicture==propertyPicture) {
                
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureDefaultNotification
                                                                                    object:propertyPicture];
                
                [[PSPropertyPictureManager sharedManager] updateDefaultPhotograph:[propertyPicture JSONValue] withImage:nil];
                
            }
						else if([propertyPicture.synchStatus integerValue]!=1)
            {
                
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                                    object:propertyPicture];
            }
            
        }]; // parent
        
    }
    
    ENDEXCEPTION
}


/*!
 @discussion
 Update property picture
 */
- (void) updateDefectPicture:(DefectPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            DefectPicture *existingPropertyPicture = (DefectPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kDefectPictureId]) {
                    existingPropertyPicture.defectPictureId = [propertyPictureDetails objectForKey:kDefectPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }

            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            
            PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if([propertyPicture.synchStatus integerValue]!=1)
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                                    object:propertyPicture];
            }
            
            
        }]; // parent
        
    }
    ENDEXCEPTION
}


/*!
 @discussion
 Mark Appointment as Complete.
 */
- (void) updateAppointmentStatus:(AppointmentStatus)status appointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
            
            if(existingAppointment != nil)
            {
                /* NSString *statusString = [UtilityClass statusStringForAppointmentStatus:status];
                 if(!isEmpty(statusString))
                 {
                 existingAppointment.appointmentStatus = statusString;
                 }*/
                
                // existingAppointment.appointmentEndTime = [NSDate date];
                existingAppointment.isModified = [NSNumber numberWithBool:NO];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentSuccessNotification object:nil];
        }]; // parent
        
    }
    ENDEXCEPTION
}

- (void) updateAppointmentStatusOnly:(AppointmentStatus)status appointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

				[managedObjectContext performBlock:^{
            if (!isEmpty(_appointmentId))
            {
                Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
                
                if(existingAppointment != nil)
                {
                    NSString *statusString = [UtilityClass statusStringForAppointmentStatus:status];
                    if(!isEmpty(statusString))
                    {
                        existingAppointment.appointmentStatus = statusString;
                        existingAppointment.isModified = [NSNumber numberWithBool:YES];
                        if(existingAppointment.appointmentActualStartTime == nil)
                        {
                            existingAppointment.appointmentActualStartTime = [NSDate date];
                        }
                        
                    }
                    // existingAppointment.appointmentEndTime = [NSDate date];
                    [[PSAppointmentsManager sharedManager]deleteCalendarEventForAppointment:existingAppointment];
                    if(status == AppointmentStatusNoEntry || status == AppointmentStatusComplete || status == AppointmentStatusFinished){
                        existingAppointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
                        existingAppointment.loggedDate = [NSDate date];
                        existingAppointment.appointmentActualEndTime = [NSDate date];
                    }

                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentStatusUpdateSuccessNotification object:nil];
        }]; // parent
        
    }
    
    ENDEXCEPTION
}
- (void) updateCP12Info:(NSDictionary *)cp12InfoDictionary forAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(cp12InfoDictionary != nil)
    {
        __block NSManagedObjectID* appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            Appointment *existingAppointment = (Appointment *) [managedObjectContext objectWithID:appointmentId];
            if(existingAppointment != nil)
            {
                NSDate *issuedDate = [cp12InfoDictionary valueForKey:@"IssuedDate"];
                NSDate *receivedDate = [cp12InfoDictionary valueForKey:@"ReceivedDate"];
                
                NSString *receivedOnBehalfOf = [cp12InfoDictionary valueForKey:@"ReceivedOnBehalfOf"];
                NSNumber *issuedBy = [cp12InfoDictionary valueForKey:@"IssuedBy"];
                NSNumber *inspectionCarried = [cp12InfoDictionary valueForKey:@"InspectionCarried"];
                NSNumber *schemeId = [cp12InfoDictionary valueForKey:kSchemeID];
                NSNumber *blockId = [cp12InfoDictionary valueForKey:kSchemeBlockID];
                NSNumber *heatingId = [cp12InfoDictionary valueForKey:kHeatingId];
                NSString *propertyId = [cp12InfoDictionary valueForKey:kPropertyId];
                existingAppointment.appointmentToCP12Info.issuedDate = isEmpty(issuedDate)?nil:issuedDate;
                existingAppointment.appointmentToCP12Info.receivedDate = isEmpty(receivedDate)?nil:receivedDate;
                existingAppointment.appointmentToCP12Info.receivedOnBehalfOf = isEmpty(receivedOnBehalfOf)?nil:receivedOnBehalfOf;
                existingAppointment.appointmentToCP12Info.issuedDateString = isEmpty([UtilityClass stringFromDate:issuedDate dateFormat:kDateTimeStyle17])?nil:[UtilityClass stringFromDate:issuedDate dateFormat:kDateTimeStyle17];
                existingAppointment.appointmentToCP12Info.receivedDateString = isEmpty([UtilityClass stringFromDate:receivedDate dateFormat:kDateTimeStyle17])?nil:[UtilityClass stringFromDate:receivedDate dateFormat:kDateTimeStyle17];
                existingAppointment.appointmentToCP12Info.inspectionCarried = isEmpty(inspectionCarried)?nil:inspectionCarried;
                existingAppointment.appointmentToCP12Info.issuedBy = isEmpty(issuedBy)?nil:issuedBy;
                existingAppointment.appointmentToCP12Info.schemeId = isEmpty(schemeId)?nil:schemeId;
                existingAppointment.appointmentToCP12Info.blockId = isEmpty(blockId)?nil:blockId;
                existingAppointment.appointmentToCP12Info.heatingId = isEmpty(heatingId)?nil:heatingId;
                existingAppointment.appointmentToCP12Info.propertyID = isEmpty(propertyId)?nil:propertyId;
                existingAppointment.isModified = [NSNumber numberWithBool:YES];
                existingAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateCP12InfoFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateCP12InfoSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

#pragma Planned Jobs

- (void) startPlannedJob:(PlannedTradeComponent *)jobData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
         NSManagedObjectID* jobDataId = jobData.objectID;
         PlannedTradeComponent * currentJob = jobData;
         NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

       
        PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
        if(existingJob != nil)
        {
            existingJob.jobStatus = kJobStatusInProgress;
            existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
            existingJob.tradeComponentToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
        }
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        if([jobData.jobStatus isEqualToString:kJobStatusInProgress] || [jobData.jobStatus isEqualToString:kJobStatusPaused]){
            [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        }
        
        [self performSelector:@selector(addPlannedStartResumeforJob:) withObject:currentJob];
        [[PSLocalNotificationManager sharedManager] scheduleInProgressFor12HourNotificationForAppointmentID:currentJob.tradeComponentToAppointment.appointmentId];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStartNotification object:nil];
    }
    ENDEXCEPTION
}

- (void) updatePlannedJobStatus:(PlannedTradeComponent *)jobData
                       jobStaus:(NSString *)status
                   andPauseData:(NSDictionary *)pauseData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        NSString * jobstatus = status;
        PlannedTradeComponent * currentJobData = jobData;
        NSManagedObjectID* jobDataId = jobData.objectID;
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        PlannedTradeComponent *existingJob;
        if (!isEmpty(jobDataId))
        {
             existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
            if(existingJob != nil)
            {
                existingJob.jobStatus = status;
                existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
                existingJob.tradeComponentToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
            }
        }
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        if(!isEmpty(pauseData) && !isEmpty(existingJob)){
            [[PSDataUpdateManager sharedManager] addPlannedJobPauseData:pauseData forJob:existingJob];
        }
        if([jobstatus isEqualToString:kJobStatusInProgress] || [jobstatus isEqualToString:kJobStatusPaused]){
            [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        }
        
        if([jobstatus isEqualToString:kJobStatusInProgress])
        {
            [self performSelector:@selector(addPlannedStartResumeforJob:) withObject:currentJobData];
            //Scheduling Local Notification
            [[PSLocalNotificationManager sharedManager] scheduleInProgressFor12HourNotificationForAppointmentID:currentJobData.tradeComponentToAppointment.appointmentId];
        }
        else
        {
            //unschedule Local Notification
            [[PSLocalNotificationManager sharedManager] cancelInProgressFor12HourNotificationForAppointmentID:currentJobData.tradeComponentToAppointment.appointmentId];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStatusUpdateNotification object:nil];
    }
    ENDEXCEPTION
}


- (void)addPlannedJobPauseData:(NSDictionary *)jobPauseData forJob:(PlannedTradeComponent *)jobData
{
    
    STARTEXCEPTION
    if(!isEmpty(jobPauseData))
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
        PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            NSString *pauseNotes = [jobPauseData valueForKey:kPauseNote];
            NSNumber *pausedBy = [jobPauseData valueForKey:@"pausedBy"];
            JobPauseReason *jobPauseReason = [jobPauseData valueForKey:kJobPauseReason];
            // JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext existingObjectWithID:jobPauseReason.objectID error:nil];
            JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"Paused";
            jobPauseData.pauseNote = isEmpty(pauseNotes)?nil:pauseNotes;
            jobPauseData.pausedBy = isEmpty(pausedBy)?nil:pausedBy;
            if (!isEmpty(jobPauseReason1))
            {
                jobPauseData.pauseReason = isEmpty(jobPauseReason1)?nil:jobPauseReason1;
            }
            [existingJob1 addJobPauseDataObject:jobPauseData];
            
            existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
            existingJob1.tradeComponentToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataSuccessNotification object:nil];
            
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataFailureNotification object:nil];
        }
        
        
    }
    ENDEXCEPTION
}

- (void)addPlannedStartResumeforJob:(PlannedTradeComponent *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
     NSManagedObjectID* jobDataId = jobData.objectID;
     NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
    PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
    if(existingJob1 != nil)
    {
        JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
        jobPauseData.pauseDate = [NSDate date];
        jobPauseData.actionType = @"InProgress";
        
        jobPauseData.pauseNote = nil;
        jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
        jobPauseData.pauseReason = nil;
        [existingJob1 addJobPauseDataObject:jobPauseData];
        existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingJob1.tradeComponentToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
    }
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        CLS_LOG(@"Error in Saving MOC: %@",[error description]);
    }
    
    
    ENDEXCEPTION
}

- (void)addPlannedJobComplete:(NSManagedObjectID *)jobDataID
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobDataID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
    [managedObjectContext performBlock:^{
        PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
        PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = kJobStatusComplete;
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
        }
        
        [managedObjectContext performBlock:^{
            // Save the context.
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
        }]; // main
    }]; // parent
    
    ENDEXCEPTION
}

- (void) markPlannedJobComplete:(FaultJobSheet *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary
{
    //
    STARTEXCEPTION
    if(jobData != nil && !isEmpty(jobDataListDictionary))
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
                    NSString *repairNotes = [jobDataListDictionary valueForKey:kJobRepairNotes];
                    existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
                    existingJob.tradeComponentToAppointment.appointmentStatus = kAppointmentStatusComplete;
                    existingJob.jsnNotes = repairNotes;
                    existingJob.completionDate = [NSDate date];
                    existingJob.jobStatus = kJobStatusComplete;
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [self performSelector:@selector(addPlannedJobComplete:) withObject:jobDataId];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
    }
    ENDEXCEPTION
}

#pragma Fault Job

- (void) startJob:(JobSheet *)jobData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
         NSManagedObjectID* jobDataId = jobData.objectID;
         JobSheet * jobList = jobData;
         NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        JobSheet *existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
        if(existingJob != nil)
        {
            existingJob.jobStatus = kJobStatusInProgress;
            existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
            existingJob.jobSheetToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
            existingJob.actualStartTime = [NSDate date];
        }
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        if([jobData.jobStatus isEqualToString:kJobStatusInProgress] || [jobData.jobStatus isEqualToString:kJobStatusPaused]){
            [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        }
        [self performSelector:@selector(addJobStartResumeDataForJob:) withObject:jobList];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStartNotification object:nil];    }
    ENDEXCEPTION
}

- (void) updateLegionella:(JobSheet *)jobData
{
	STARTEXCEPTION
	if(jobData != nil)
	{
		// Creating managed objects
		NSManagedObjectID* jobDataId = jobData.objectID;
		JobSheet * jobList = jobData;
		NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		JobSheet *existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
		if(existingJob != nil)
		{
			existingJob.isLegionella = jobData.isLegionella;
			existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
			existingJob.jobSheetToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
		}
		
		NSError *error = nil;
		if (![managedObjectContext save:&error]) {
			CLS_LOG(@"Error in Saving MOC: %@",[error description]);
		}

	}
	ENDEXCEPTION
}

- (void) updateJobStatus:(JobSheet *)jobData
                jobStaus:(NSString *)status
            andPauseData:(NSDictionary *) pauseData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block JobSheet * dataList = jobData;
        __block NSString * jobStatus = status;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        JobSheet *existingJob;
        if (!isEmpty(jobDataId))
        {
            existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
            if(existingJob != nil)
            {
                existingJob.jobStatus = status;
                existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
                existingJob.jobSheetToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
                if(existingJob.actualStartTime == nil)
                {
                    existingJob.actualStartTime = [NSDate date];
                }
                
                if([jobStatus isEqualToString:kJobStatusNoEntry] || [jobStatus isEqualToString:kJobStatusComplete] || [jobStatus isEqualToString:kJobStatusFinished]){
                    existingJob.jsCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
                    existingJob.completionDate = [NSDate date];
                    existingJob.actualEndTime = [NSDate date];
                }
            }
        }
        
        // Save the context.
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        if(!isEmpty(pauseData) && !isEmpty(existingJob)){
            [[PSDataUpdateManager sharedManager] addJobPauseData:pauseData forJob:existingJob];
        }
        if([jobStatus isEqualToString:kJobStatusInProgress])
        {
            [self performSelector:@selector(addJobStartResumeDataForJob:) withObject:dataList];
        }
        else //Always Pause
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStatusUpdateNotification object:nil];
        }
        if([jobStatus isEqualToString:kJobStatusInProgress] || [jobStatus isEqualToString:kJobStatusPaused]){
            [[PSSynchronizationManager sharedManager] syncAppointmentProgressStatus];
        }
    }
    ENDEXCEPTION
}


- (void)addJobPauseData:(NSDictionary *)jobPauseData forJob:(JobSheet *)jobData
{
	
    STARTEXCEPTION
    if(!isEmpty(jobPauseData))
    {
        // Creating managed objects
        NSManagedObjectID* jobDataId = jobData.objectID;
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        JobSheet *existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
        JobSheet *existingJob1 = (JobSheet *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            NSString *pauseNotes = [jobPauseData valueForKey:kPauseNote];
            NSNumber *pausedBy = [jobPauseData valueForKey:@"pausedBy"];
            JobPauseReason *jobPauseReason = [jobPauseData valueForKey:kJobPauseReason];
            // JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext existingObjectWithID:jobPauseReason.objectID error:nil];
            JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"Paused";
            jobPauseData.pauseNote = isEmpty(pauseNotes)?nil:pauseNotes;
            jobPauseData.pausedBy = isEmpty(pausedBy)?nil:pausedBy;
            if (!isEmpty(jobPauseReason1))
            {
                jobPauseData.pauseReason = isEmpty(jobPauseReason1)?nil:jobPauseReason1;
            }
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
            existingJob1.jobSheetToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataSuccessNotification object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataFailureNotification object:nil];
        }
        
    }
    ENDEXCEPTION
}

- (void)addJobStartResumeDataForJob:(FaultJobSheet *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
    NSManagedObjectID* jobDataId = jobData.objectID;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    FaultJobSheet *existingJob = (FaultJobSheet *)[managedObjectContext objectWithID:jobDataId];
    FaultJobSheet *existingJob1 = (FaultJobSheet *)[managedObjectContext objectWithID:existingJob.objectID];
    if(existingJob1 != nil)
    {
        JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
        jobPauseData.pauseDate = [NSDate date];
        jobPauseData.actionType = @"InProgress";
        jobPauseData.pauseNote = nil;
        jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
        jobPauseData.pauseReason = nil;
        
        [existingJob1 addJobPauseDataObject:jobPauseData];
        existingJob1.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingJob1.jobSheetToAppointment.appointmentStatusSynced = [NSNumber numberWithBool:NO];
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        }
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobResumeDataSuccessNotification object:nil];
        
    }
    else
     {
         [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobResumeDataFailureNotification object:nil];
     }
    
    
    ENDEXCEPTION
}

- (void)addJobDataForComplete:(JobSheet *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobData.objectID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
    [managedObjectContext performBlock:^{
        JobSheet *existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
        JobSheet *existingJob1 = (JobSheet *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = kJobStatusComplete;
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
            
        }
        
        [managedObjectContext performBlock:^{
            // Save the context.
            NSError *error = nil;
            if (![managedObjectContext save:&error])
						{
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
        }]; // main
        
    }]; // parent
    ENDEXCEPTION
}

- (void) markJobComplete:(JobSheet *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary
{
    //
    STARTEXCEPTION
    if(jobData != nil/* && !isEmpty(jobDataListDictionary)*/)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block JobSheet * jobDataList = jobData;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                JobSheet * existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
									NSString *entityName = [[existingJob entity] name];
									if ([entityName compare:NSStringFromClass([DefectJobSheet class])] == NSOrderedSame)
									{
										DefectJobSheet *defectJobDataList = (DefectJobSheet *) existingJob;
										[self markDefectJobdatalistComplete:jobDataListDictionary :defectJobDataList];
									}
									else
									{
										FaultJobSheet * faultJobDataList = (FaultJobSheet *) jobDataList;
									 [self markJobdatalistComplete:jobDataListDictionary :faultJobDataList];
									}
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [self performSelector:@selector(addJobDataForComplete:) withObject:jobDataList];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
    }
    ENDEXCEPTION
}

-(void) markDefectJobdatalistComplete:(NSDictionary *)jobDataListDictionary : (DefectJobSheet*)existingJob
{
	
	NSNumber *defectCategory  = [jobDataListDictionary valueForKey:kDefectCategoryType];
	NSNumber *isRemedialActionTaken = [jobDataListDictionary valueForKey:kIsRemedialActionTaken];
	NSString *defectNotes = [jobDataListDictionary valueForKey:kDefectNotes];
	NSString *remedialActionNotes = [jobDataListDictionary valueForKey:kRemedialActionNotes];
	NSNumber *isAdviceNoteIssued = [jobDataListDictionary valueForKey:kisAdviceNoteIssued];
	NSString *serialNumber = [jobDataListDictionary valueForKey:kSerialNo];
	NSString *gasCouncilNumber = [jobDataListDictionary valueForKey:kGasCouncilNumber];
	NSNumber *warningTagFixed = [jobDataListDictionary valueForKey:kWarningTagFixed];
	NSNumber *isDisconnected = [jobDataListDictionary valueForKey:kIsDisconnected];
	NSNumber *isCustomerHaveHeating = [jobDataListDictionary valueForKey:kIsCustomerHaveHeating];
	NSNumber *isHeatersLeft = [jobDataListDictionary valueForKey:kIsHeatersLeft];
	NSNumber *numberOfHeatersLeft = [jobDataListDictionary valueForKey:kNumberOfHeatersLeft];
	NSNumber *isCustomerHaveHotWater = [jobDataListDictionary valueForKey:kIsCustomerHaveHotWater];
    NSString *completionNotes = [jobDataListDictionary valueForKey:kDefectCompletionNotes];
    
	
	existingJob.defectCategoryId = isEmpty(defectCategory)?nil:defectCategory;
	existingJob.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
	existingJob.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
	existingJob.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
	existingJob.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
	existingJob.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
	existingJob.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
	existingJob.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
	existingJob.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
	existingJob.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
	existingJob.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
	existingJob.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
	existingJob.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
    existingJob.actualEndTime = [NSDate date];
	existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
	existingJob.completionDate = [NSDate date];
    existingJob.defectCompletionNotes = isEmpty(completionNotes)?nil:completionNotes;
	existingJob.jobStatus = kJobStatusComplete;

}

-(void) markJobdatalistComplete:(NSDictionary *)jobDataListDictionary : (FaultJobSheet *)existingJob
{
	//Void work Job Sheet/ Fault repair worksheet marking complete
	NSString *repairNotes = [jobDataListDictionary valueForKey:kJobRepairNotes];
	NSString *followOnNotes = [jobDataListDictionary valueForKey:kFollowOnNotes];
    existingJob.actualEndTime = [NSDate date];
	existingJob.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
	existingJob.followOnNotes = isEmpty(followOnNotes)?nil:followOnNotes;
	existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
	existingJob.completionDate = [NSDate date];
	existingJob.jobStatus = kJobStatusComplete;
	existingJob.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
    existingJob.jsCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
	
}

#pragma Detect

- (void) updateDetector:(Detector *)detector withDetectorCount:(NSNumber *)detectorCount
{
    STARTEXCEPTION
    if(detector != nil && !isEmpty(detectorCount))
    {
        // Creating managed objects
        __block NSManagedObjectID *detectorId = detector.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            if (!isEmpty(detectorId))
            {
                Detector *existingDetector = (Detector *)[managedObjectContext objectWithID:detectorId];
                if(existingDetector != nil)
                {
                  // existingDetector.detectorCount = detectorCount;
                    [PSDatabaseContext turnObjectIntoFault:existingDetector.detectorToProperty.propertyToAppointment.objectID];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorFailureNotification object:nil];
    }
    ENDEXCEPTION
    
}

#pragma mark - Syncing
-(void) updateAppointmentSyncStatusForAppointment:(Appointment *) appointment andStatus:(AppointmentSyncStatus) status andSuccessBlock:(void (^)(BOOL isSuccess))successBlock failure:(void (^)(NSError * error))failureBlock
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(_appointmentId))
            {
                Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
                
                if(existingAppointment != nil)
                {
                    NSNumber *statusString = [UtilityClass statusNumberForAppointmentSyncStatus:status];
                    if(!isEmpty(statusString))
                    {
                        existingAppointment.syncStatus = statusString;
                        
                    }
                    
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                CLS_LOG(@"Saving to PSC");
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                    failureBlock(error);
                }
                else{
                    successBlock(YES);
                }
            }]; // main
        }]; // parent
        
    }
    
    ENDEXCEPTION
}

#pragma mark - "Appointment History"

-(void) addAppointmentHistoryLogWithAction:(NSString *) status
                            forAppointment:(Appointment *) appointment{
    STARTEXCEPTION
     __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    [managedObjectContext performBlock:^{
        CLS_LOG(@"Adding History Log for Action: %@ andAppointmentId: %@", status, appointment.appointmentId);
        AppointmentHistory * appointmentHistoryLog = [NSEntityDescription insertNewObjectForEntityForName:kAppointmentHistory inManagedObjectContext:managedObjectContext];
        appointmentHistoryLog.actionType = status;
        appointmentHistoryLog.actionDate = [NSDate date];
        appointmentHistoryLog.actionBy = [[SettingsClass sharedObject]loggedInUser].userId;
        [appointment addAppointmentToAppointmentHistory:[NSSet setWithArray:@[appointmentHistoryLog]]];
        [managedObjectContext performBlock:^{
            NSError *error = nil;
            if (![managedObjectContext save:&error])
            {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
            if([status isEqualToString:kJobStatusAccepted]){
                [self addHistoryLogForAcceptedInJobSheetForAppointment:appointment];
            }
        }];

    }];
    ENDEXCEPTION
    
}

-(void) addHistoryLogForAcceptedInJobSheetForAppointment:(Appointment*) appointment{
    if(!isEmpty(appointment.appointmentToJobSheet)){
        NSArray *jobsheets = [NSArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
        for(JobSheet * sheet in jobsheets){
            [self addHistoryLogInJobsForAccept:sheet];
        }
    }
    else if(!isEmpty(appointment.appointmentToPlannedComponent)){
        [self addHistoryLogInPlannedComponentForAccept:appointment.appointmentToPlannedComponent];
    }
}

- (void)addHistoryLogInJobsForAccept:(JobSheet *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobData.objectID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        JobSheet *existingJob = (JobSheet *)[managedObjectContext objectWithID:jobDataId];
        JobSheet *existingJob1 = (JobSheet *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = kJobStatusAccepted;
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            [existingJob1 addJobPauseDataObject:jobPauseData];
        }
        
        [managedObjectContext performBlock:^{
            // Save the context.
            NSError *error = nil;
            if (![managedObjectContext save:&error])
            {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
        }]; // main
        
    }]; // parent
    ENDEXCEPTION
}


-(void) addHistoryLogInPlannedComponentForAccept:(PlannedTradeComponent *) existingJob1{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = kJobStatusAccepted;
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            [existingJob1 addJobPauseDataObject:jobPauseData];
        }
        
        [managedObjectContext performBlock:^{
            // Save the context.
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
        }]; // main
    }]; // parent
    
    ENDEXCEPTION
}


@end
