//
//  PSDataPersistenceManager (CommonData).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager (CommonData)

- (void) saveCommonData:(NSArray *)commonDataArray;

@end
