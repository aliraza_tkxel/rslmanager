//
//  PSDataPersistenceManager+(CommonData).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSDataPersistenceManager.h"

@interface PSDataPersistenceManager (Detectors)

- (void) saveFetchedDetectorData:(NSArray *) detectorData forProperty:(NSManagedObjectID *)propertyId;
- (BOOL) saveDetector:(NSDictionary *)detector forProperty:(NSManagedObjectID *)propertyId;
- (BOOL) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *)detector;

@end
