//
//  PSDataPersistenceManager+(GasElectricCheck).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSDataPersistenceManager.h"

@interface PSDataPersistenceManager (Defects)

- (void) saveFetchedDefects:(NSDictionary *) defectsDictionary;
- (void) saveNewDefect:(NSDictionary *)defect;
- (void) saveFetchedDefectCategories:(NSArray *)defectCategories defects:(NSDictionary *)defectsDictionary;
- (void) updateDefectObject:(Defect *)defectObj  withDictionary:(NSDictionary*)defectDictionary;
- (void) saveDefectPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Defect*) propertyParam;
- (void) loadApplianceDefectsData:(NSArray *)applianceDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadDefectCategoryData:(NSArray *)defectCategoryList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadDetectorDefectsData:(NSArray *)detectorDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
@end
