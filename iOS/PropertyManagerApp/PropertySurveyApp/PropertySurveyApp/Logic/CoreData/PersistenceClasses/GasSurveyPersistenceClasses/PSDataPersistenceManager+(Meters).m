//
//  PSDataPersistenceManager+(GasElectricCheck).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(Meters).h"


@implementation PSDataPersistenceManager (Meters)

#pragma mark - Meters Persistence

- (void) saveFetchedMeterData:(NSArray *) meterData forProperty:(NSManagedObjectID *)propertyId
{
    if([UtilityClass isUserLoggedIn])
    {
            // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        [managedObjectContext performBlock:^{
            Property *property;
            if(!isEmpty(meterData) && !isEmpty(propertyId))
            {
                Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
                [[PSDataPersistenceManager sharedManager]loadMetersData:meterData property:property managedObjectContext:managedObjectContext];
            }
            if (property != nil)
            {
                property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
            }
            
            [self saveDataInDB:managedObjectContext];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesSuccessNotification object:nil];
        }];
    }
}

- (BOOL) saveMeter:(NSDictionary *)meter forProperty:(NSManagedObjectID *)propertyId
{
    BOOL isSaved = NO;
    
    if([UtilityClass isUserLoggedIn] && !isEmpty(meter))
    {
            // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
				Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
        [[PSDataPersistenceManager sharedManager] loadMeterData:meter property:property managedObjectContext:managedObjectContext];
        property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
        isSaved = YES;
    }
    return isSaved;
    
}


#pragma mark - Meters Data

- (void) loadMetersData:(NSArray *)metersData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if(!isEmpty(metersData))
    {
        for (NSDictionary * meterDictionary in metersData)
        {
            
            [self loadMeterData:meterDictionary property:property managedObjectContext:managedObjectContext];
            
        }
    }
    
}

- (void) loadMeterData:(NSDictionary *)meterDictionary property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if(!isEmpty(meterDictionary))
    {
        NSNumber * inspectedBy = [meterDictionary valueForKey:kMeterInspectedBy];
        NSDate * inspectionDate = [UtilityClass convertServerDateToNSDate:[meterDictionary valueForKey:kMeterInspectionDate]];
        NSDate * installedDate = [UtilityClass convertServerDateToNSDate:[meterDictionary valueForKey:kMeterInstalledDate]];
        NSNumber * isCapped = [meterDictionary valueForKey:kMeterIsCapped];
        NSNumber * isInspected = [meterDictionary valueForKey:kMeterIsInspected];
        NSNumber * isPassed = [meterDictionary valueForKey:kMeterIsPassed];
        NSString * lastReading = [meterDictionary valueForKey:kMeterLastReading];
        NSDate * lastReadingDate = [UtilityClass convertServerDateToNSDate:[meterDictionary valueForKey:kMeterLastReadingDate]];
        NSString * location = [meterDictionary valueForKey:kMeterLocation];
        NSString * manufacturer = [meterDictionary valueForKey:kMeterManufacturer];
        NSNumber * meterTypeId = [meterDictionary valueForKey:kMeterTypeId];
        NSString * notes = [meterDictionary valueForKey:kMeterNotes];
        NSString * propertyId = [meterDictionary valueForKey:kMeterPropertyId];
        NSString * reading = [meterDictionary valueForKey:kMeterReading];
        NSDate * readingDate = [UtilityClass convertServerDateToNSDate:[meterDictionary valueForKey:kMeterReadingDate]];
        NSString * serialNumber = [meterDictionary valueForKey:kMeterSerialNumber];
			  NSString * deviceType = [meterDictionary valueForKey:kDeviceType];
			  NSNumber * deviceTypeId = [meterDictionary valueForKey:kDeviceTypeId];
        NSNumber *schemeId = [meterDictionary valueForKey:kSchemeID];
        NSNumber *blockId = [meterDictionary valueForKey:kSchemeBlockID];
			
        Meter *meter = [[PSCoreDataManager sharedManager] meterWithIdentifier:deviceTypeId forProperty:property context:managedObjectContext];
        if(meter != nil)
        {
            meter = (Meter *)[managedObjectContext objectWithID:meter.objectID];
        }
        else
        {
            meter = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Meter class]) inManagedObjectContext:managedObjectContext];
        }
        meter.schemeId =isEmpty(schemeId)?nil:schemeId;
        meter.blockId = isEmpty(blockId)?nil:blockId;
        meter.inspectedBy =isEmpty(inspectedBy)?nil:inspectedBy;
        meter.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        meter.installedDate =isEmpty(installedDate)?nil:installedDate;
        meter.isCapped = isEmpty(isCapped)?nil:isCapped;
        meter.isInspected = isEmpty(isInspected)?nil:isInspected;
        meter.isPassed = isEmpty(isPassed)?nil:isPassed;
        meter.lastReading = isEmpty(lastReading)?nil:lastReading;
        meter.lastReadingDate = isEmpty(lastReadingDate)?nil:lastReadingDate;
        meter.location =isEmpty(location)?nil:location;
        meter.manufacturer = isEmpty(manufacturer)?nil:manufacturer;
        meter.meterTypeId = isEmpty(meterTypeId)?nil:meterTypeId;
        meter.notes = isEmpty(notes)?nil:notes;
        meter.propertyId = isEmpty(propertyId)?nil:propertyId;
        meter.reading = isEmpty(reading)?nil:reading;
        meter.readingDate = isEmpty(readingDate)?nil:readingDate;
        meter.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
			  meter.deviceType = isEmpty(deviceType)?nil:deviceType;
			  meter.deviceTypeId = isEmpty(deviceTypeId)?nil:deviceTypeId;
        
            //Set the Inverse Relationship
        
            // Property
        if (!isEmpty(property))
        {
            meter.meterToProperty = property;
            [property addPropertyToMeterObject:meter];
        }
        
    }
}

- (void) loadMeterInspectionForm:(NSDictionary *)inspectionFormDictionary forMeter:(Meter *)meter managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (!isEmpty(inspectionFormDictionary) && !isEmpty(meter))
    {
        NSString *reading = [inspectionFormDictionary valueForKey:kMeterReading];
        NSDate *readingDate = ([[inspectionFormDictionary valueForKey:kMeterReadingDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kMeterReadingDate]]:[inspectionFormDictionary valueForKey:kMeterReadingDate];
        NSNumber *isPassed = [inspectionFormDictionary valueForKey:kMeterIsPassed];
				NSNumber *isCapped = [inspectionFormDictionary valueForKey:kMeterIsCapped];
        NSString *notes = [inspectionFormDictionary valueForKey:kMeterNotes];
        NSDate *inspectionDate = ([[inspectionFormDictionary valueForKey:kMeterInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kMeterInspectionDate]]:[inspectionFormDictionary valueForKey:kMeterInspectionDate];
        NSNumber *meterInspectedBy = [inspectionFormDictionary valueForKey:kMeterInspectedBy];
        
        NSManagedObjectID* _meterId = meter.objectID;
        Meter *existingMeter = (Meter *)[managedObjectContext objectWithID:_meterId];
        existingMeter.reading = isEmpty(reading)?nil: reading;
        existingMeter.readingDate = isEmpty(readingDate)?nil: readingDate;
        existingMeter.isPassed = isEmpty(isPassed)?nil:isPassed;
        existingMeter.notes = isEmpty(notes)?nil:notes ;
        existingMeter.isCapped =isEmpty(isCapped)?nil:isCapped ;
        existingMeter.inspectionDate =isEmpty(inspectionDate)?nil:inspectionDate;
        existingMeter.inspectedBy =isEmpty(meterInspectedBy)?nil:meterInspectedBy;
        existingMeter.isInspected = [NSNumber numberWithBool:YES];
        existingMeter.meterToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        existingMeter.meterToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        
    }
    
}


- (BOOL) saveMeterInspectionForm:(NSDictionary *) inspectionFormDictionary forMeter:(Meter *)meter
{
    BOOL isSaved = NO;
    
    if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
        CLS_LOG(@"Creating managed objects");
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        [[PSDataPersistenceManager sharedManager]loadMeterInspectionForm:inspectionFormDictionary forMeter:meter managedObjectContext:managedObjectContext];
        [self saveDataInDB:managedObjectContext];
        isSaved = YES;
    }
    
    return isSaved;
}




@end
