//
//  PSDataPersistenceManager+(CommonData).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
#import "PSDataPersistenceManager+(GasSurveyCommonData).h"
#import "CoreDataHelper.h"
#import "AbortReason+CoreDataClass.h"

@implementation PSDataPersistenceManager (GasSurveyCommonData)

- (void) saveGasSurveyCommonData:(NSArray *)commonDataArray
{
	CLS_LOG(@"save GasSurveyCommonData");
	if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
	{
		__block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		NSArray * priorityList = [commonDataArray valueForKey:@"defectsPriority"];
		[self loadDefectPriorities:priorityList dbContext:dbContext];
		NSArray * tradesList = [commonDataArray valueForKey:@"trades"];
		[self loadTrades:tradesList dbContext:dbContext];
		NSArray * employeesList = [commonDataArray valueForKey:@"partsOrderedBy"];
		[self loadEmployees:employeesList dbContext:dbContext];
		NSArray * detectorTypesList = [commonDataArray valueForKey:@"detectorTypes"];
		[self loadDetectorTypes:detectorTypesList dbContext:dbContext];
		NSArray * powerTypesList = [commonDataArray valueForKey:@"powerTypes"];
		[self loadPowerTypes:powerTypesList dbContext:dbContext];
		NSArray * defectCategoryList = [commonDataArray valueForKey:@"defectCategories"];
		[self loadDefectCategoryData:defectCategoryList managedObjectContext:dbContext];
		NSArray * boilerTypesList = [commonDataArray valueForKey:@"boilerTypes"];
		[self loadBoilerTypes:boilerTypesList dbContext:dbContext];
		NSArray * boilerManufacturersList = [commonDataArray valueForKey:@"boilerManufacturers"];
		[self loadBoilerManufacturers:boilerManufacturersList dbContext:dbContext];
		NSArray * boilerFlueTypeList = [commonDataArray valueForKey:@"mainsGasFlueTypes"];
		[self loadBoilerFlueTypes:boilerFlueTypeList dbContext:dbContext];
        
        NSArray *abortReasons = [commonDataArray valueForKey:@"abortReasons"];
        [self loadAbortReasons:abortReasons dbContext:dbContext];
        
	}
}

-(void) loadBoilerFlueTypes:(NSArray *)flueList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(flueList) == NO)
	{
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * boilerFlue in flueList)
		{
			NSNumber * flueId = [boilerFlue valueForKey:kLookupId];
			NSString * flueDesc =[boilerFlue valueForKey:kLookupValue];
			FlueType * flueData = [coreDataMgr findRecordFrom:kFlueType
																														Field:kBoilerFlueTypeId
																												WithValue:flueId
																													context:dbContext];
			if(flueData != nil)
			{
				flueData = (FlueType *)[dbContext objectWithID:flueData.objectID];
			}else{
				
				flueData = [NSEntityDescription insertNewObjectForEntityForName:kFlueType
																												inManagedObjectContext:dbContext];
			}
			flueData.flueTypeId = flueId;
			flueData.flueTypeDescription = flueDesc;
			[self saveDataInDB:dbContext];
		}
	}
}


-(void) loadAbortReasons:(NSArray *)abortReasonList dbContext:(NSManagedObjectContext *)dbContext
{
    if(isEmpty(abortReasonList) == NO)
    {
        PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
        for(NSDictionary * abortReason in abortReasonList)
        {
            NSNumber * reasonId = [abortReason valueForKey:@"AbortReasonId"];
            NSString * reason = [abortReason valueForKey:@"AbortReason"];
            
            
            
            AbortReason * abortReason = [coreDataMgr findRecordFrom:kAbortReasonType
                                                        Field:@"reasonId"
                                                    WithValue:reasonId
                                                      context:dbContext];
            if(abortReason != nil)
            {
                abortReason = (AbortReason*)[dbContext objectWithID:abortReason.objectID];
            }else{
                
                abortReason = [NSEntityDescription insertNewObjectForEntityForName:kAbortReasonType
                                                         inManagedObjectContext:dbContext];
            }
            
            abortReason.reasonId = reasonId;
            abortReason.reason = reason;
            [self saveDataInDB:dbContext];
        }
    }
}




-(void) loadBoilerManufacturers:(NSArray *)manufacturerList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(manufacturerList) == NO)
	{
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * boilerManufacturer in manufacturerList)
		{
			NSNumber * manufacturerid = [boilerManufacturer valueForKey:kBoilerManufacturerId];
			NSString * manufacturerDesc =[boilerManufacturer valueForKey:kBoilerManufacturerDescription];
			NSNumber * schemeBlockFlag = [boilerManufacturer valueForKey:kIsSchemeBlock];
            NSNumber *isAlternativeHeatingFlag = [boilerManufacturer valueForKey:kIsAlternativeHeating];
			BOOL isSchemeBlock = isEmpty(schemeBlockFlag) || [schemeBlockFlag isEqualToNumber:[NSNumber numberWithBool:NO]] ? NO : YES;
            BOOL isAlternativeHeating = isEmpty(isAlternativeHeatingFlag) || [isAlternativeHeatingFlag isEqualToNumber:[NSNumber numberWithBool:NO]] ? NO : YES;

            
			BoilerManufacturer * boilerManufacturer = [coreDataMgr findRecordFrom:kBoilerManufacturer
																													Field:kBoilerManufacturerId
																											WithValue:manufacturerid
																												context:dbContext];
			if(boilerManufacturer != nil)
			{
				boilerManufacturer = (BoilerManufacturer *)[dbContext objectWithID:boilerManufacturer.objectID];
			}else{
				
				boilerManufacturer = [NSEntityDescription insertNewObjectForEntityForName:kBoilerManufacturer
																											 inManagedObjectContext:dbContext];
			}
			
			boilerManufacturer.isSchemeBlock = [NSNumber numberWithBool:isSchemeBlock];
            boilerManufacturer.isAlternativeHeating = [NSNumber numberWithBool:isAlternativeHeating];
			boilerManufacturer.manufacturerDescription = manufacturerDesc;
			boilerManufacturer.manufacturerId = manufacturerid;
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadBoilerTypes:(NSArray *)boilerTypeList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(boilerTypeList) == NO)
	{
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * boilerType in boilerTypeList)
		{
			NSNumber * boilerTypeId = [boilerType valueForKey:kBoilerTypeId];
			NSString * boilerTypeDesc =[boilerType valueForKey:kBoilerTypeDescription];
			NSNumber * schemeBlockFlag = [boilerType valueForKey:kIsSchemeBlock];
			BOOL isSchemeBlock = isEmpty(schemeBlockFlag) || [schemeBlockFlag isEqualToNumber:[NSNumber numberWithBool:NO]] ? NO : YES;
			BoilerType * boilerTypeData = [coreDataMgr findRecordFrom:kBoilerType
																												Field:kBoilerTypeId
																										WithValue:boilerTypeId
																											context:dbContext];
			if(boilerTypeData != nil)
			{
				boilerTypeData = (BoilerType *)[dbContext objectWithID:boilerTypeData.objectID];
			}else{
				
				boilerTypeData = [NSEntityDescription insertNewObjectForEntityForName:kBoilerType
																											inManagedObjectContext:dbContext];
			}
			
			boilerTypeData.boilerTypeId = boilerTypeId;
			boilerTypeData.boilerTypeDescription = boilerTypeDesc;
            boilerTypeData.isSchemeBlock = [NSNumber numberWithBool:isSchemeBlock];
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadEmployees:(NSArray *)employeeList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(employeeList) == NO)
	{
		int counter = 0;
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * employee in employeeList)
		{
			NSNumber * employeeId = [employee valueForKey:kEmployeeId];
			Employee * employeeData = [coreDataMgr findRecordFrom:kEmployee
																											Field:kEmployeeId
																									WithValue:employeeId
																										context:dbContext];
			if(employeeData == NULL)
			{
				employeeData = [NSEntityDescription insertNewObjectForEntityForName:kEmployee
																										 inManagedObjectContext:dbContext];
				employeeData.employeeId = employeeId;
				employeeData.employeeName = [employee valueForKey:kEmployeeName];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadDefectPriorities:(NSArray *)priorityList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(priorityList) == NO)
	{
		int counter = 0;
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * priority in priorityList)
		{
			NSNumber * priorityId = [priority valueForKey:kPriorityId];
			DefectPriority * priorityData = [coreDataMgr findRecordFrom:kDefectPriority
																														Field:kPriorityId
																												WithValue:priorityId
																													context:dbContext];
			if(priorityData == NULL)
			{
				priorityData = [NSEntityDescription insertNewObjectForEntityForName:kDefectPriority
																										 inManagedObjectContext:dbContext];
				priorityData.priorityId = priorityId;
				priorityData.priority = [priority valueForKey:kPriorityDesc];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadTrades:(NSArray *)tradeList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(tradeList) == NO)
	{
		int counter = 0;
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * trade in tradeList)
		{
			NSNumber * tradeId = [trade valueForKey:kTradeId];
			Trade * tradeData = [coreDataMgr findRecordFrom:kTrade
																								Field:kTradeId
																						WithValue:tradeId
																							context:dbContext];
			if(tradeData == NULL)
			{
				tradeData = [NSEntityDescription insertNewObjectForEntityForName:kTrade
																									inManagedObjectContext:dbContext];
				tradeData.tradeId = tradeId;
				tradeData.trade = [trade valueForKey:kTradeDescription];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadDetectorTypes:(NSArray *)detectorTypeList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(detectorTypeList) == NO)
	{
		int counter = 0;
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * detectorType in detectorTypeList)
		{
			NSNumber * detectorTypeId = [detectorType valueForKey:kDetectorTypeId];
			DetectorType * detectorTypeData = [coreDataMgr findRecordFrom:kDetectorType
																															Field:kDetectorTypeId
																													WithValue:detectorTypeId
																														context:dbContext];
			if(detectorTypeData == NULL)
			{
				detectorTypeData = [NSEntityDescription insertNewObjectForEntityForName:kDetectorType
																												 inManagedObjectContext:dbContext];
				detectorTypeData.detectorTypeId = detectorTypeId;
				detectorTypeData.detectorType = [detectorType valueForKey:kDetectorsType];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadPowerTypes:(NSArray *)powerTypeList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(powerTypeList) == NO)
	{
		PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
		for(NSDictionary * powerType in powerTypeList)
		{
			NSNumber * powerTypeId = [powerType valueForKey:kPowerTypeId];
			NSString * powerTypeDesc =[powerType valueForKey:kPowerTypeDesc];
			
			PowerType * powerTypeData = [coreDataMgr findRecordFrom:kPowerType
																												Field:kPowerTypeId
																										WithValue:powerTypeId
																											context:dbContext];
			if(powerTypeData != nil)
			{
				powerTypeData = (PowerType *)[dbContext objectWithID:powerTypeData.objectID];
			}else{
				
				powerTypeData = [NSEntityDescription insertNewObjectForEntityForName:kPowerType
																											inManagedObjectContext:dbContext];
			}
			
			powerTypeData.powerTypeId = powerTypeId;
			powerTypeData.powerType = powerTypeDesc;
			[self saveDataInDB:dbContext];
		}
	}
}

- (void) loadDefectCategoryData:(NSArray *)defectCategoryList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(defectCategoryList))
	{
		for (NSDictionary *categoryDictionary in defectCategoryList)
		{
			NSString *categoryDescription = [categoryDictionary valueForKey:kDefectCategoryDescription];
			NSNumber *categoryId  = [categoryDictionary valueForKey:kDefectCategoryID];
			
			DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:categoryId];
			if (defectCategory != nil)
			{
				defectCategory = (DefectCategory *)[managedObjectContext objectWithID:defectCategory.objectID];
			}
			else
			{
				defectCategory = [NSEntityDescription insertNewObjectForEntityForName:kDefectCategory inManagedObjectContext:managedObjectContext];
			}
			
			defectCategory.categoryDescription = isEmpty(categoryDescription)?nil:categoryDescription;
			defectCategory.categoryID = isEmpty(categoryId)?nil:categoryId;
		}
	}
}


#pragma mark - Alternate and OIL

-(void) saveAlternativeAndOilLookups:(NSArray *) common{
    [self saveBurnerTypesCommonData:common];
    [self saveSolarTypesCommonData:common];
    [self saveTankTypesCommonData:common];
    [self saveOilFlueTypesCommonData:common];
    [self saveOilFuelTypesCommonData:common];
}


-(void) saveBurnerTypesCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveBurnerTanksCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * burnerTanks = [commonDataArray valueForKey:@"burnerTypes"];
        [self loadBurnerTypesData:burnerTanks withDbContext:dbContext];
    }
}


-(void) loadBurnerTypesData:(NSArray *) data withDbContext:(NSManagedObjectContext *) dbContext{
    NSString * entityName = NSStringFromClass([BurnerType class]);
    NSArray * dbList = [CoreDataHelper getObjectsFromEntity:entityName
                                                    sortKey:NULL
                                              sortAscending:FALSE
                                                    context:dbContext];
    if(isEmpty(data) == NO)
    {
        int counter = 0;
        for(NSDictionary * entityDict in data)
        {
            NSNumber * lookupId = [entityDict valueForKey:@"lookupId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lookupId == %@", lookupId];
            NSArray * result = [dbList filteredArrayUsingPredicate:filter];
            BurnerType *entityObj;
            if([result count] == 0)
            {
                entityObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                          inManagedObjectContext:dbContext];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                counter++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else{
                entityObj = [result objectAtIndex:0];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                [self saveDataInDB:dbContext];
            }
        }
    }
}

-(void) saveSolarTypesCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveSolarTypesCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * info = [commonDataArray valueForKey:@"solarTypes"];
        [self loadSolarTypesData:info withDbContext:dbContext];
    }
}

-(void) loadSolarTypesData:(NSArray *) data withDbContext:(NSManagedObjectContext *) dbContext{
    NSString * entityName = NSStringFromClass([SolarType class]);
    NSArray * dbList = [CoreDataHelper getObjectsFromEntity:entityName
                                                    sortKey:NULL
                                              sortAscending:FALSE
                                                    context:dbContext];
    if(isEmpty(data) == NO)
    {
        int counter = 0;
        for(NSDictionary * entityDict in data)
        {
            NSNumber * lookupId = [entityDict valueForKey:@"lookupId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lookupId == %@", lookupId];
            NSArray * result = [dbList filteredArrayUsingPredicate:filter];
            SolarType *entityObj;
            if([result count] == 0)
            {
                entityObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                          inManagedObjectContext:dbContext];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                counter++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else{
                entityObj = [result objectAtIndex:0];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                [self saveDataInDB:dbContext];
            }
        }
    }
}


-(void) saveTankTypesCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveTankTypesCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * info = [commonDataArray valueForKey:@"tankTypes"];
        [self loadTankTypesData:info withDbContext:dbContext];
    }
}

-(void) loadTankTypesData:(NSArray *) data withDbContext:(NSManagedObjectContext *) dbContext{
    NSString * entityName = NSStringFromClass([TankType class]);
    NSArray * dbList = [CoreDataHelper getObjectsFromEntity:entityName
                                                    sortKey:NULL
                                              sortAscending:FALSE
                                                    context:dbContext];
    if(isEmpty(data) == NO)
    {
        int counter = 0;
        for(NSDictionary * entityDict in data)
        {
            NSNumber * lookupId = [entityDict valueForKey:@"lookupId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lookupId == %@", lookupId];
            NSArray * result = [dbList filteredArrayUsingPredicate:filter];
            TankType *entityObj;
            if([result count] == 0)
            {
                entityObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                          inManagedObjectContext:dbContext];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                counter++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else{
                entityObj = [result objectAtIndex:0];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                [self saveDataInDB:dbContext];
            }
        }
    }
}


-(void) saveOilFlueTypesCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveOilFlueTypesCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * info = [commonDataArray valueForKey:@"oilFlueTypes"];
        [self loadOilFlueTypesData:info withDbContext:dbContext];
    }
}

-(void) loadOilFlueTypesData:(NSArray *) data withDbContext:(NSManagedObjectContext *) dbContext{
    NSString * entityName = NSStringFromClass([OilFlueType class]);
    NSArray * dbList = [CoreDataHelper getObjectsFromEntity:entityName
                                                    sortKey:NULL
                                              sortAscending:FALSE
                                                    context:dbContext];
    if(isEmpty(data) == NO)
    {
        int counter = 0;
        for(NSDictionary * entityDict in data)
        {
            NSNumber * lookupId = [entityDict valueForKey:@"lookupId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lookupId == %@", lookupId];
            NSArray * result = [dbList filteredArrayUsingPredicate:filter];
            OilFlueType *entityObj;
            if([result count] == 0)
            {
                entityObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                          inManagedObjectContext:dbContext];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                counter++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else{
                entityObj = [result objectAtIndex:0];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                [self saveDataInDB:dbContext];
            }
        }
    }
}

-(void) saveOilFuelTypesCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveOilFlueTypesCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * info = [commonDataArray valueForKey:@"oilFuelTypes"];
        [self loadOilFuelTypesData:info withDbContext:dbContext];
    }
}

-(void) loadOilFuelTypesData:(NSArray *) data withDbContext:(NSManagedObjectContext *) dbContext{
    NSString * entityName = NSStringFromClass([OilFuelType class]);
    NSArray * dbList = [CoreDataHelper getObjectsFromEntity:entityName
                                                    sortKey:NULL
                                              sortAscending:FALSE
                                                    context:dbContext];
    if(isEmpty(data) == NO)
    {
        int counter = 0;
        for(NSDictionary * entityDict in data)
        {
            NSNumber * lookupId = [entityDict valueForKey:@"lookupId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.lookupId == %@", lookupId];
            NSArray * result = [dbList filteredArrayUsingPredicate:filter];
            OilFuelType *entityObj;
            if([result count] == 0)
            {
                entityObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                          inManagedObjectContext:dbContext];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                counter++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else{
                entityObj = [result objectAtIndex:0];
                entityObj.lookupId = lookupId;
                entityObj.value = [entityDict objectForKey:@"value"];
                [self saveDataInDB:dbContext];
            }
        }
    }
}


@end
