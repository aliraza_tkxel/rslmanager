//
//  PSDataPersistenceManager+(GasElectricCheck).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(Defects).h"


@implementation PSDataPersistenceManager (Defects)


#pragma mark - Defect Picture Persistence
- (void) saveDefectPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Defect*) propertyParam
{
	if(!isEmpty(pictureDictionary))
	{
		__block DefectPicture *existingPicture = [[PSCoreDataManager sharedManager] defectPictureWithIdentifier:[pictureDictionary valueForKey:kPropertyPictureId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		[managedObjectContext performBlock:^{
			
			Defect* property = nil;
			if(propertyParam)
			{
				property = (Defect*)[managedObjectContext objectWithID:propertyParam.objectID];
			}
			
			DefectPicture *propertyPicture = nil;
			if(existingPicture != nil)
			{
				//User already exists in database, so just update the existing object.
				propertyPicture = (DefectPicture *)[managedObjectContext existingObjectWithID:existingPicture.objectID error:nil];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				propertyPicture = (DefectPicture *)[NSEntityDescription insertNewObjectForEntityForName:kDefectPicture inManagedObjectContext:managedObjectContext];
			}
			
			UIImage * propertyImage = [pictureDictionary valueForKey:kPropertyImage];
			NSString *imagePath = [pictureDictionary valueForKey:kPropertyPicturePath];
			NSNumber *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
            NSNumber *itemId = [pictureDictionary valueForKey:kItemId];
            NSNumber *heatingId = [pictureDictionary valueForKey:kHeatingId];
			propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
			
			if(!isEmpty(property))
			{
				propertyPicture.defectPictureToDefect=property;
			}
			
			propertyPicture.createdOn=[NSDate date];
			propertyPicture.imagePath= isEmpty(imagePath)?nil:imagePath;
			propertyPicture.synchStatus=[self generateIdentifier];
			propertyPicture.imageIdentifier = uniqueIdentifier;
            propertyPicture.heatingId = heatingId;
            propertyPicture.itemId = itemId;
			
			if([self saveDataInDB:managedObjectContext] == TRUE)
			{
				dispatch_async(dispatch_get_main_queue(), ^{
					DefectPicture *propertyPicture1 = (DefectPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
					[propertyPicture1 uploadPropertyPicture:pictureDictionary withImage:propertyImage];
				});
			}
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUserInfoSaveCompleted object:nil];
		}]; // parent
	}
}

#pragma mark - Defects Persistence
- (void) saveFetchedDefects:(NSDictionary *)defectsDictionary
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defectsDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			NSArray *applianceDefects = [defectsDictionary valueForKey:@"defects"];
			[[PSDataPersistenceManager sharedManager] loadApplianceDefectsData:applianceDefects managedObjectContext:managedObjectContext];
			[self saveDataInDB:managedObjectContext];
			
			NSArray *detectorDefects = [defectsDictionary valueForKey:@"detectorDefects"];
			[[PSDataPersistenceManager sharedManager] saveFetchedDetectorDefects:detectorDefects];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification object:nil];
	}
}

- (void) saveFetchedDetectorDefects:(NSArray *)defects
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defects))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadDetectorDefectsData:defects managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveSuccessNotification object:nil];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification object:nil];
	}
}
- (void) saveNewDefect:(NSDictionary *)defect
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defect))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadNewDefectData:defect managedObjectContext:managedObjectContext];
			Property *property = [[PSCoreDataManager sharedManager]propertyWithIdentifier:[defect valueForKey:kPropertyId] context:managedObjectContext];
			property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveDefectSuccessNotification object:nil];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveDefectFailureNotification object:nil];
	}
}

#pragma mark - Defect Categories Persistence
- (void) saveFetchedDefectCategories:(NSArray *)defectCategories defects:(NSDictionary *)defectsDictionary
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defectCategories))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadDefectCategoryData:defectCategories managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			[[PSDataPersistenceManager sharedManager]saveFetchedDefects:defectsDictionary];
		}]; // parent
	}
}


- (void) updateDefectObject:(Defect *)defectObj  withDictionary:(NSDictionary*)defectDictionary
{
	if(!isEmpty(defectDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSManagedObjectID * defectObjID = defectObj.objectID;
		__block NSDictionary * defectDict = defectDictionary;
		[managedObjectContext performBlock:^{
			
			Defect * defect = (Defect *)[managedObjectContext objectWithID:defectObjID];
			NSNumber * defectID = [defectDict valueForKey:kDefectID];
			
			//NSNumber *applianceId = [defectDict valueForKey:kApplianceId];
			NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDict valueForKey:kDefectDate]];
			NSString * defectNotes = [defectDict valueForKey:kDefectNotes];
			NSNumber *defectCategory  = [defectDict valueForKey:kDefectCategoryType];
			NSNumber *isRemedialActionTaken = [defectDict valueForKey:kIsRemedialActionTaken];
			NSNumber *isAdviceNoteIssued = [defectDict valueForKey:kisAdviceNoteIssued];
			NSNumber *isDefectidentified = [defectDict valueForKey:kIsDefectIdentified];
			NSString *remedialActionNotes = [defectDict valueForKey:kRemedialActionNotes];
			NSString *serialNumber = [defectDict valueForKey:kSerialNo];
			NSNumber *warningTagFixed = [defectDict valueForKey:kWarningTagFixed];
			//NSNumber *detectorId = [defectDict valueForKey:kDetectorTypeId];
			
			NSString *gasCouncilNumber = [defectDictionary valueForKey:kGasCouncilNumber];
			NSNumber *isDisconnected = [defectDictionary valueForKey:kIsDisconnected];
			NSNumber *isPartsRequired = [defectDictionary valueForKey:kIsPartsRequired];
			NSNumber *isPartsOrdered = [defectDictionary valueForKey:kIsPartsOrdered];
			NSNumber *partsOrderedBy = [defectDictionary valueForKey:kPartsOrderedBy];
			NSDate   *partsDueDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kPartsDueDate]];
			NSString *partsDescription = [defectDictionary valueForKey:kPartsDescription];
			NSString *partsLocation = [defectDictionary valueForKey:kPartsLocation];
			NSNumber *isTwoPersonsJob = [defectDictionary valueForKey:kIsTwoPersonsJob];
			NSString *reasonForTwoPerson = [defectDictionary valueForKey:kReasonForTwoPerson];
			NSNumber *duration = [defectDictionary valueForKey:kDuration];
			NSNumber *defectPriorityId = [defectDictionary valueForKey:kDefectPriorityId];
			NSNumber *defectTradeId = [defectDictionary valueForKey:kDefectTradeId];
			NSNumber *isCustomerHaveHeating = [defectDictionary valueForKey:kIsCustomerHaveHeating];
			NSNumber *isHeatersLeft = [defectDictionary valueForKey:kIsHeatersLeft];
			NSNumber *numberOfHeatersLeft = [defectDictionary valueForKey:kNumberOfHeatersLeft];
			NSNumber *isCustomerHaveHotWater = [defectDictionary valueForKey:kIsCustomerHaveHotWater];
			
			defect.defectDate = isEmpty(defectDate)?nil:defectDate;
			defect.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
			defect.faultCategory = isEmpty(defectCategory)?nil:defectCategory;
			defect.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
			defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
			defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
			defect.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
			defect.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
			defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
			defect.defectID = isEmpty(defectID)?nil:defectID;
			
			defect.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
			defect.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
			defect.isPartsRequired = isEmpty(isPartsRequired)?nil:isPartsRequired;
			defect.isPartsOrdered = isEmpty(isPartsOrdered)?nil:isPartsOrdered;
			defect.partsOrderedBy = isEmpty(partsOrderedBy)?nil:partsOrderedBy;
			defect.partsDueDate = isEmpty(partsDueDate)?nil:partsDueDate;
			defect.partsDescription = isEmpty(partsDescription)?nil:partsDescription;
			defect.partsLocation = isEmpty(partsLocation)?nil:partsLocation;
			defect.isTwoPersonsJob = isEmpty(isTwoPersonsJob)?nil:isTwoPersonsJob;
			defect.reasonForTwoPerson = isEmpty(reasonForTwoPerson)?nil:reasonForTwoPerson;
			defect.duration = isEmpty(duration)?nil:duration;
			defect.priorityId = isEmpty(defectPriorityId)?nil:defectPriorityId;
			defect.tradeId = isEmpty(defectTradeId)?nil:defectTradeId;
			defect.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
			defect.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
			defect.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
			defect.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
			
			[self saveDataInDB:managedObjectContext];
			
		}]; // parent
	}
}

- (void) loadDefectCategoryData:(NSArray *)defectCategoryList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(defectCategoryList))
	{
		for (NSDictionary *categoryDictionary in defectCategoryList)
		{
			NSString *categoryDescription = [categoryDictionary valueForKey:kDefectCategoryDescription];
			NSNumber *categoryId  = [categoryDictionary valueForKey:kDefectCategoryID];
			
			DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:categoryId];
			if (defectCategory != nil)
			{
				defectCategory = (DefectCategory *)[managedObjectContext objectWithID:defectCategory.objectID];
			}
			else
			{
				defectCategory = [NSEntityDescription insertNewObjectForEntityForName:kDefectCategory inManagedObjectContext:managedObjectContext];
			}
			
			defectCategory.categoryDescription = isEmpty(categoryDescription)?nil:categoryDescription;
			defectCategory.categoryID = isEmpty(categoryId)?nil:categoryId;
		}
	}
}

#pragma mark Defects Data
- (void) loadApplianceDefectsData:(NSArray *)applianceDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceDefectsList))
	{
		for(NSDictionary *defectDictionary in applianceDefectsList)
		{
			if(!isEmpty(defectDictionary))
			{
				NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
				Defect *defect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
				
				if (defect != nil)
				{
					//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
				}
				else
				{
					defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
				}
				
				NSNumber *applianceId = [defectDictionary valueForKey:kApplianceId];
				NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
				NSString *defectNotes = [defectDictionary valueForKey:kDefectNotes];
				NSNumber *defectCategory  = [defectDictionary valueForKey:kDefectCategoryType];
				NSNumber *isRemedialActionTaken = [defectDictionary valueForKey:kIsRemedialActionTaken];
				NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
				NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
				NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
				NSString *remedialActionNotes = [defectDictionary valueForKey:kRemedialActionNotes];
				NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
				NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
				
				NSString *gasCouncilNumber = [defectDictionary valueForKey:kGasCouncilNumber];
				NSNumber *isDisconnected = [defectDictionary valueForKey:kIsDisconnected];
				NSNumber *isPartsRequired = [defectDictionary valueForKey:kIsPartsRequired];
				NSNumber *isPartsOrdered = [defectDictionary valueForKey:kIsPartsOrdered];
				NSNumber *partsOrderedBy = [defectDictionary valueForKey:kPartsOrderedBy];
				NSDate   *partsDueDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kPartsDueDate]];
				NSString *partsDescription = [defectDictionary valueForKey:kPartsDescription];
				NSString *partsLocation = [defectDictionary valueForKey:kPartsLocation];
				NSNumber *isTwoPersonsJob = [defectDictionary valueForKey:kIsTwoPersonsJob];
				NSString *reasonForTwoPerson = [defectDictionary valueForKey:kReasonForTwoPerson];
				NSNumber *duration = [defectDictionary valueForKey:kDuration];
				NSNumber *defectPriorityId = [defectDictionary valueForKey:kDefectPriorityId];
				NSNumber *defectTradeId = [defectDictionary valueForKey:kDefectTradeId];
				NSNumber *isCustomerHaveHeating = [defectDictionary valueForKey:kIsCustomerHaveHeating];
				NSNumber *isHeatersLeft = [defectDictionary valueForKey:kIsHeatersLeft];
				NSNumber *numberOfHeatersLeft = [defectDictionary valueForKey:kNumberOfHeatersLeft];
				NSNumber *isCustomerHaveHotWater = [defectDictionary valueForKey:kIsCustomerHaveHotWater];
				
				defect.applianceID = isEmpty(applianceId)?nil:applianceId;
				defect.defectDate = isEmpty(defectDate)?nil:defectDate;
				defect.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
				defect.faultCategory = isEmpty(defectCategory)?nil:defectCategory;
				defect.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
				defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
				defect.journalId = isEmpty(journalId)?nil:journalId;
				defect.propertyId = isEmpty(propertyId)?nil:propertyId;
				defect.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
				defect.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
				defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				defect.defectID = isEmpty(defectId)?nil:defectId;
				defect.defectType = kDefectTypeAppliance;
				
				defect.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
				defect.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
				defect.isPartsRequired = isEmpty(isPartsRequired)?nil:isPartsRequired;
				defect.isPartsOrdered = isEmpty(isPartsOrdered)?nil:isPartsOrdered;
				defect.partsOrderedBy = isEmpty(partsOrderedBy)?nil:partsOrderedBy;
				defect.partsDueDate = isEmpty(partsDueDate)?nil:partsDueDate;
				defect.partsDescription = isEmpty(partsDescription)?nil:partsDescription;
				defect.partsLocation = isEmpty(partsLocation)?nil:partsLocation;
				defect.isTwoPersonsJob = isEmpty(isTwoPersonsJob)?nil:isTwoPersonsJob;
				defect.reasonForTwoPerson = isEmpty(reasonForTwoPerson)?nil:reasonForTwoPerson;
				defect.duration = isEmpty(duration)?nil:duration;
				defect.priorityId = isEmpty(defectPriorityId)?nil:defectPriorityId;
				defect.tradeId = isEmpty(defectTradeId)?nil:defectTradeId;
				defect.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
				defect.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
				defect.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
				defect.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
				
				//Setting Inverse relations
				
				// Defect Category
				DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:defectCategory];
				if (!isEmpty(objDefectCategory))
				{
					DefectCategory *objExistingDefectCategory = (DefectCategory *)[managedObjectContext objectWithID:objDefectCategory.objectID];
					if (!isEmpty(objExistingDefectCategory))
					{
						defect.defectCategory = objExistingDefectCategory;
					}
				}
				
				//Defect Appliance
				Appliance *appliance = [[PSCoreDataManager sharedManager] findRecordFrom:kAppliance Field:@"applianceID" WithValue:applianceId context:managedObjectContext];
				if (!isEmpty(appliance))
				{
					Appliance *objAppliance = (Appliance *)[managedObjectContext objectWithID:appliance.objectID];
					if (!isEmpty(objAppliance))
					{
						[objAppliance addApplianceDefectsObject:defect];
					}
				}
				
				//Defect Priority
				DefectPriority *defectPriority = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([DefectPriority class]) Field:kDefectPriorityId WithValue:defectPriorityId context:managedObjectContext];
				if (!isEmpty(defectPriority))
				{
					DefectPriority *objDefectPriority = (DefectPriority *)[managedObjectContext objectWithID:defectPriority.objectID];
					if (!isEmpty(objDefectPriority))
					{
						defect.defectToDefectPriority = objDefectPriority;
					}
				}
				
				//Defect Trade
				Trade *trade = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([Trade class]) Field:kTradeId WithValue:defectTradeId context:managedObjectContext];
				if (!isEmpty(trade))
				{
					Trade *objTrade = (Trade *)[managedObjectContext objectWithID:trade.objectID];
					if (!isEmpty(objTrade))
					{
						defect.defectToTrade = objTrade;
					}
				}
				
			}
		}
	}
}


- (void) loadDetectorDefectsData:(NSArray *)detectorDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(detectorDefectsList))
	{
		for(NSDictionary *defectDictionary in detectorDefectsList)
		{
			if(!isEmpty(defectDictionary))
			{
				NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
				Defect *defect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
				
				if (defect != nil)
				{
					//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
				}
				else
				{
					defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
				}
				
				NSNumber *applianceId = [defectDictionary valueForKey:kApplianceId];
				NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
				NSString *defectNotes = [defectDictionary valueForKey:kDefectNotes];
				NSNumber *defectCategory  = [defectDictionary valueForKey:kDefectCategoryType];
				NSNumber *isRemedialActionTaken = [defectDictionary valueForKey:kIsRemedialActionTaken];
				NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
				NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
				NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
				NSString *remedialActionNotes = [defectDictionary valueForKey:kRemedialActionNotes];
				NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
				NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
				NSNumber *detectorId = [defectDictionary valueForKey:kDetectorTypeId];
				
				defect.applianceID = isEmpty(applianceId)?nil:applianceId;
				defect.defectDate = isEmpty(defectDate)?nil:defectDate;
				defect.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
				defect.faultCategory = isEmpty(defectCategory)?nil:defectCategory;
				defect.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
				defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
				defect.journalId = isEmpty(journalId)?nil:journalId;
				defect.propertyId = isEmpty(propertyId)?nil:propertyId;
				defect.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
				defect.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
				defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				defect.defectID = isEmpty(defectId)?nil:defectId;
				defect.defectType = kDefectTypeDetector;
				
				DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:defectCategory];
				if (!isEmpty(objDefectCategory))
				{
					DefectCategory *objExistingDefectCategory = (DefectCategory *)[managedObjectContext objectWithID:objDefectCategory.objectID];
					if (!isEmpty(objExistingDefectCategory))
					{
						defect.defectCategory = objExistingDefectCategory;
					}
				}
				
				defect.detectorTypeId = isEmpty(detectorId)?nil:detectorId;
				Detector *detector = [[PSCoreDataManager sharedManager] detectorWithIdentifier:detectorId forProperty:propertyId context:managedObjectContext];
				if (!isEmpty(detector))
				{
					[detector addDetectorDefectsObject:defect];
				}
				
			}
		}
	}
}

- (void) loadNewDefectData:(NSDictionary *)defectDictionary managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(defectDictionary))
	{
		NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
		NSManagedObjectID *defectObjectId = [defectDictionary valueForKey:@"defectObjectId"];
		Defect *defect = nil;
		if (!isEmpty(defectObjectId))
		{
			defect = (Defect *)[managedObjectContext objectWithID:defectObjectId];
		}
		
		
		if (defect != nil)
		{
			//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
		}
		else
		{
			defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
		}
		
		NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
		NSString *defectNotes = [defectDictionary valueForKey:kDefectNotes];
		NSNumber *defectCategory  = [defectDictionary valueForKey:kDefectCategoryType];
		NSNumber *isRemedialActionTaken = [defectDictionary valueForKey:kIsRemedialActionTaken];
		NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
		NSString *warningNoteSerialNo = [defectDictionary valueForKey:kWarningNoteSerialNo];
		NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
		NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
		NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
		NSString *remedialActionNotes = [defectDictionary valueForKey:kRemedialActionNotes];
		NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
		NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
		NSString *defectType = [defectDictionary valueForKey:kDefectType];
		NSNumber *detectorId = [defectDictionary valueForKey:kDetectorTypeId];
		NSString *gasCouncilNumber = [defectDictionary valueForKey:kGasCouncilNumber];
		NSNumber *isDisconnected = [defectDictionary valueForKey:kIsDisconnected];
		NSNumber *isPartsRequired = [defectDictionary valueForKey:kIsPartsRequired];
		NSNumber *isPartsOrdered = [defectDictionary valueForKey:kIsPartsOrdered];
		NSNumber *partsOrderedBy = [defectDictionary valueForKey:kPartsOrderedBy];
		NSDate   *partsDueDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kPartsDueDate]];
		NSString *partsDescription = [defectDictionary valueForKey:kPartsDescription];
		NSString *partsLocation = [defectDictionary valueForKey:kPartsLocation];
		NSNumber *isTwoPersonsJob = [defectDictionary valueForKey:kIsTwoPersonsJob];
		NSString *reasonForTwoPerson = [defectDictionary valueForKey:kReasonForTwoPerson];
		NSNumber *duration = [defectDictionary valueForKey:kDuration];
		NSNumber *defectPriorityId = [defectDictionary valueForKey:kDefectPriorityId];
		NSNumber *defectTradeId = [defectDictionary valueForKey:kDefectTradeId];
		NSNumber *applianceId = [defectDictionary valueForKey:kDefectApplianceID];
		NSNumber *boilerTypeId = [defectDictionary valueForKey:kDefectBoilerTypeId];
		
		NSNumber *isCustomerHaveHeating = [defectDictionary valueForKey:kIsCustomerHaveHeating];
		NSNumber *isHeatersLeft = [defectDictionary valueForKey:kIsHeatersLeft];
		NSNumber *numberOfHeatersLeft = [defectDictionary valueForKey:kNumberOfHeatersLeft];
		NSNumber *isCustomerHaveHotWater = [defectDictionary valueForKey:kIsCustomerHaveHotWater];
        
        NSNumber *schemeId = [defectDictionary valueForKey:kSchemeID];
        NSNumber *blockId = [defectDictionary valueForKey:kSchemeBlockID];
        NSNumber *heatingId = [defectDictionary valueForKey:kHeatingId];
		
		defect.defectDate = isEmpty(defectDate)?nil:defectDate;
		defect.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
		defect.faultCategory = isEmpty(defectCategory)?nil:defectCategory;
		defect.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
		defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued) ? nil : isAdviceNoteIssued;
		defect.warningNoteSerialNo = isEmpty(warningNoteSerialNo) ? nil : warningNoteSerialNo;
		defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
		defect.journalId = isEmpty(journalId)?nil:journalId;
		defect.propertyId = isEmpty(propertyId)?nil:propertyId;
		defect.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
		defect.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
		defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
		defect.defectID = isEmpty(defectId)?nil:defectId;
		defect.defectType = defectType;
		defect.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
		defect.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
		defect.isPartsRequired = isEmpty(isPartsRequired)?nil:isPartsRequired;
		defect.isPartsOrdered = isEmpty(isPartsOrdered)?nil:isPartsOrdered;
		defect.partsOrderedBy = isEmpty(partsOrderedBy)?nil:partsOrderedBy;
		defect.partsDueDate = isEmpty(partsDueDate)?nil:partsDueDate;
		defect.partsDescription = isEmpty(partsDescription)?nil:partsDescription;
		defect.partsLocation = isEmpty(partsLocation)?nil:partsLocation;
		defect.isTwoPersonsJob = isEmpty(isTwoPersonsJob)?nil:isTwoPersonsJob;
		defect.reasonForTwoPerson = isEmpty(reasonForTwoPerson)?nil:reasonForTwoPerson;
		defect.duration = isEmpty(duration)?nil:duration;
		defect.priorityId = isEmpty(defectPriorityId)?nil:defectPriorityId;
		defect.tradeId = isEmpty(defectTradeId)?nil:defectTradeId;
		defect.applianceID = isEmpty(applianceId)?nil:applianceId;
		defect.boilerTypeId = isEmpty(boilerTypeId)?nil:boilerTypeId;
		defect.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
		defect.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
		defect.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
		defect.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
		defect.schemeId = isEmpty(schemeId)?nil:schemeId;
        defect.blockId = isEmpty(blockId)?nil:blockId;
        defect.heatingId = isEmpty(heatingId)?nil:heatingId;
		//Defect Inverse relation
		
		// Defect Category
		DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:defectCategory context:managedObjectContext];
		if (!isEmpty(objDefectCategory))
		{
			defect.defectCategory = objDefectCategory;
		}
		
		//Defect Priority
		if (isEmpty(defectPriorityId)==NO) {
			DefectPriority *defectPriority = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([DefectPriority class]) Field:kDefectPriorityId WithValue:defectPriorityId context:managedObjectContext];
			if (!isEmpty(defectPriority))
			{
				DefectPriority *objDefectPriority = (DefectPriority *)[managedObjectContext objectWithID:defectPriority.objectID];
				if (!isEmpty(objDefectPriority))
				{
					defect.defectToDefectPriority = objDefectPriority;
				}
			}
		}
		
		//Defect Trade
		if (isEmpty(defectTradeId)==NO) {
			Trade *trade = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([Trade class]) Field:kTradeId WithValue:defectTradeId context:managedObjectContext];
			if (!isEmpty(trade))
			{
				Trade *objTrade = (Trade *)[managedObjectContext objectWithID:trade.objectID];
				if (!isEmpty(objTrade))
				{
					defect.defectToTrade = objTrade;
				}
			}
		}
		
		
		if ([defectType isEqualToString:kDefectTypeAppliance]) // Appliance
		{
			NSManagedObjectID *applianceObjectId = [defectDictionary valueForKey:kApplianceObjectId];
			Appliance *appliance = nil;
			if (!isEmpty(applianceObjectId))
			{
				appliance = (Appliance *)[managedObjectContext objectWithID:applianceObjectId];
			}
			
			if (!isEmpty(appliance))
			{
				[appliance addApplianceDefectsObject:defect];
			}
			
		}	else if ([defectType isEqualToString:kDefectTypeDetector]) // Detector
		{
			defect.detectorTypeId = isEmpty(detectorId)?nil:detectorId;
			Detector *detector = [[PSCoreDataManager sharedManager] detectorWithIdentifier:detectorId forProperty:propertyId context:managedObjectContext];
			if (!isEmpty(detector))
			{
				[detector addDetectorDefectsObject:defect];
			}
		} else if ([defectType isEqualToString:kDefectTypeBoiler]) //Boiler
		{
			Boiler *boiler = nil;
			NSManagedObjectID *boilerObjectId = [defectDictionary valueForKey:kBoilerObjectId];
			if (!isEmpty(boilerObjectId))
			{
				boiler = (Boiler *)[managedObjectContext objectWithID:boilerObjectId];
			}
			
			if (!isEmpty(boiler))
			{
				[boiler addBoilerToDefectObject:defect];
			}
		}
		
	}
}

@end
