//
//  PSDataPersistenceManager+(Boiler).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(Boiler).h"


@implementation PSDataPersistenceManager (Boiler)

#pragma mark - Boiler Persistence


- (void) saveFetchedBoilerData:(NSArray *) boilerData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSArray * boilerDataBlock = boilerData;
		__block Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
		[managedObjectContext performBlock:^{
			
			if(!isEmpty(boilerDataBlock))
			{
				[[PSDataPersistenceManager sharedManager] loadBoilerData:boilerDataBlock property:property managedObjectContext:managedObjectContext isAddedOffline:offline];
			}
			property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
			[self saveDataInDB:managedObjectContext];
		}];
	}
}

- (void) loadBoilerData:(NSArray *)boilerData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline
{
	if(!isEmpty(boilerData))
	{
		
		for (NSDictionary * boilerDictionary in boilerData)
		{
            NSNumber * itemId = [boilerDictionary valueForKey:kItemId];
			NSNumber * fluTypeId = [boilerDictionary valueForKey:kBoilerFlueTypeId];
			NSString * gcNumber = [boilerDictionary valueForKey:kBoilerGcNumber];
			NSNumber * isLandlordAppliance = [boilerDictionary valueForKey:kBoilerIsLandlordAppliance];
			NSDate *   lastReplaced = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kBoilerLastReplaced]];
			NSDate *   replacementDue = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kBoilerReplacementDue]];
			NSString * location = [boilerDictionary valueForKey:kBoilerLocation];
			NSNumber * manufacturerId = [boilerDictionary valueForKey:kBoilerManufacturerId];
			NSString * model = [boilerDictionary valueForKey:kBoilerModel];
			NSNumber * boilerTypeId = [boilerDictionary valueForKey:kBoilerTypeId];
			NSNumber * isInspected = [boilerDictionary valueForKey:kBoilerIsInspected];
			NSString * propertyId = [boilerDictionary valueForKey:kBoilerPropertyId];
			NSString * serialNumber = [boilerDictionary valueForKey:kBoilerSerialNumber];
            NSDate *gasketReplacementDate = [UtilityClass convertServerDateToNSDate:[boilerDictionary valueForKey:kBoilerGasketReplacementDate]];
            NSNumber *schemeId = [boilerDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [boilerDictionary valueForKey:kSchemeBlockID];
            NSNumber *heatingId = [boilerDictionary valueForKey:kHeatingId];
            NSString *boilerName = [boilerDictionary valueForKey:kBoilerName];
            
            
            
            
			NSDictionary *boilerInspectionDictionary = [boilerDictionary valueForKey:kBoilerInspection];
			NSArray* boilerDefectsArray = [boilerDictionary valueForKey:kBoilerDefects];
			
			NSManagedObjectID *boilerObjectId = [boilerDictionary valueForKey:kBoilerObjectId];
			
			Boiler * boiler = nil;
            if (!isEmpty(boilerObjectId))
            {
                boiler = (Boiler *)[managedObjectContext objectWithID:boilerObjectId];
            }
            else{
                boiler = [NSEntityDescription insertNewObjectForEntityForName:kBoiler inManagedObjectContext:managedObjectContext];
            }
            boiler.schemeId = isEmpty(schemeId)?nil:schemeId;
            boiler.blockId = isEmpty(blockId)?nil:blockId;
            boiler.heatingId = isEmpty(heatingId)?nil:heatingId;
            boiler.boilerName = isEmpty(boilerName)?nil:boilerName;
            boiler.itemId = isEmpty(itemId)?nil:itemId;
            
			boiler.flueTypeId = isEmpty(fluTypeId)?nil:fluTypeId;
			boiler.gcNumber = isEmpty(gcNumber)?nil:gcNumber;
			boiler.isLandlordAppliance = isEmpty(isLandlordAppliance)?nil:isLandlordAppliance;
			boiler.lastReplaced = isEmpty(lastReplaced)?nil:lastReplaced;
			boiler.replacementDue = isEmpty(replacementDue)?nil:replacementDue;
			boiler.location = isEmpty(location)?nil:location;
			boiler.manufacturerId = isEmpty(manufacturerId)?nil:manufacturerId;
			boiler.model = isEmpty(model)?nil:model;
			boiler.boilerTypeId = isEmpty(boilerTypeId)?nil:boilerTypeId;
			boiler.isInspected = isEmpty(isInspected)?nil:isInspected;
			boiler.propertyId = isEmpty(propertyId)?nil:propertyId;
			boiler.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
            boiler.gasketReplacementDate = isEmpty(gasketReplacementDate)?nil:gasketReplacementDate;

			
			// Set Boiler Manufacturer
			[self setBoilerManufacturer:manufacturerId :boiler managedObjectContext:managedObjectContext];
			
			// Set Boiler Type
			[self setBoilerType:boilerTypeId :boiler managedObjectContext:managedObjectContext];
			
			// Set Boiler Flue Type
			[self setBoilerFlueType:fluTypeId :boiler managedObjectContext:managedObjectContext];
			
			// Set Boiler Inspection
			if ([boiler.isInspected boolValue])
			{
					[self loadBoilerInspectionForm:boilerInspectionDictionary forBoiler:boiler managedObjectContext:managedObjectContext];
			}
			else {
				  boiler.boilerToBoilerInspection = nil;
			}
			
			// Set Boiler Defect
			[[PSDataPersistenceManager sharedManager]loadBoilerDefectsData:boilerDefectsArray forBoiler:boiler managedObjectContext:managedObjectContext ];
			
			
			// Set Boiler Property
			if (!isEmpty(property))
			{
				boiler.boilerToProperty = property;
				[property addPropertyToBoilerObject:boiler];
			}
			[self saveDataInDB:managedObjectContext];
		}
	}
}

- (Boiler *) getPropertyBoiler: (Property *)property
{
	NSArray *boilerArray = [property.propertyToBoiler allObjects];
	Boiler * objBoiler = nil;
	if (!isEmpty(boilerArray)) {
		objBoiler =[boilerArray objectAtIndex:0];
	}
	return objBoiler;
}


- (void) setBoilerManufacturer:(NSNumber *) manufacturerId :(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	BoilerManufacturer * boilerManufacturer  = [[PSCoreDataManager sharedManager] findRecordFrom:kBoilerManufacturer Field:kBoilerManufacturerId WithValue:manufacturerId context:managedObjectContext];
	if (!isEmpty(boilerManufacturer)) {
		boiler.boilerToBoilerManufacturer = boilerManufacturer;
	}else{
		boiler.boilerToBoilerManufacturer = nil;
	}
	
}

- (void) setBoilerType:(NSNumber *) typeId :(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	BoilerType * boilerType  = [[PSCoreDataManager sharedManager] findRecordFrom:kBoilerType Field:kBoilerTypeId WithValue:typeId context:managedObjectContext];
	if (!isEmpty(boilerType)) {
		boiler.boilerToBoilerType = boilerType;
	}else{
		boiler.boilerToBoilerType = nil;
	}
	
}

- (void) setBoilerFlueType:(NSNumber *) flueTypeId :(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	FlueType * flueType  = [[PSCoreDataManager sharedManager] findRecordFrom:kFlueType Field:kBoilerFlueTypeId WithValue:flueTypeId context:managedObjectContext];
	if (!isEmpty(flueType)) {
		boiler.boilerToFlueType = flueType;
	}else{
		boiler.boilerToFlueType = nil;
	}
	
}


#pragma mark - Boiler Inspection Form Data Persistence

- (void) saveBoilerInspectionForm:(NSDictionary *) inspectionFormDictionary forBoiler:(Boiler *)boiler
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
	{
		NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		Boiler *existingBoiler = (Boiler *)[managedObjectContext objectWithID:boiler.objectID];
		[[PSDataPersistenceManager sharedManager] loadBoilerInspectionForm:inspectionFormDictionary forBoiler:existingBoiler managedObjectContext:managedObjectContext];
		existingBoiler.isInspected = [NSNumber numberWithBool:YES];
		existingBoiler.boilerToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
		existingBoiler.boilerToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
		[self saveDataInDB:managedObjectContext];
	}
}


- (void) loadBoilerInspectionForm:(NSDictionary *)inspectionFormDictionary forBoiler:(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(inspectionFormDictionary) && !isEmpty(boiler))
	{
		NSNumber *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
		NSString *combustionReading = [inspectionFormDictionary valueForKey:kCombustionReading];
		NSString *operatingPressure = [inspectionFormDictionary valueForKey:kOperatingPressure];
		NSString *safetyDeviceOperational = [inspectionFormDictionary valueForKey:kSafetyDeviceOperational];
		NSString *smokePellet = [inspectionFormDictionary valueForKey:kSmokePellet];
		NSString *adequateVentilation = [inspectionFormDictionary valueForKey:kAdequateVentilation];
		NSString *flueVisualCondition = [inspectionFormDictionary valueForKey:kFlueVisualCondition];
		NSString *satisfactoryTermination = [inspectionFormDictionary valueForKey:kSatisfactoryTermination];
		NSString *fluePerformanceChecks = [inspectionFormDictionary valueForKey:kFluePerformanceChecks];
		NSString *boilerServiced = [inspectionFormDictionary valueForKey:kBoilerServiced];
		NSString *boilerSafeToUse = [inspectionFormDictionary valueForKey:kBoilerSafeTouse];
		NSNumber *inspectionId = [inspectionFormDictionary valueForKey:kBoilerInspectionId];
		NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
		NSDate   *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
		NSString *spillageTest = [inspectionFormDictionary valueForKey:kSpillageTest];
		NSString *operatingPressureUnit = [inspectionFormDictionary valueForKey:kOperatingPressureUnit];
		
		BoilerInspection *boilerInspection = [[PSCoreDataManager sharedManager] findRecordFrom:kBoilerInspection Field:kBoilerInspectionId WithValue:inspectionId context:managedObjectContext];
		
		if ( boilerInspection != nil)
		{
			CLS_LOG(@"Boiler Inspection updated");
		}
		else
		{
			boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kBoilerInspection inManagedObjectContext:managedObjectContext];
			CLS_LOG(@"New Boiler Inspection added");
		}
		
		boilerInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
		boilerInspection.combustionReading = isEmpty(combustionReading)?nil:combustionReading;
		boilerInspection.operatingPressure = isEmpty(operatingPressure)?nil:operatingPressure;
		boilerInspection.safetyDeviceOperational = isEmpty(safetyDeviceOperational)?nil:safetyDeviceOperational;
		boilerInspection.smokePellet = isEmpty(smokePellet)?nil:smokePellet;
		boilerInspection.adequateVentilation = isEmpty(adequateVentilation)?nil:adequateVentilation;
		boilerInspection.flueVisualCondition = isEmpty(flueVisualCondition)?nil:flueVisualCondition;
		boilerInspection.satisfactoryTermination = isEmpty(satisfactoryTermination)?nil:satisfactoryTermination;
		boilerInspection.fluePerformanceChecks = isEmpty(fluePerformanceChecks)?nil:fluePerformanceChecks;
		boilerInspection.boilerServiced = isEmpty(boilerServiced)?nil:boilerServiced;
		boilerInspection.boilerSafeToUse = isEmpty(boilerSafeToUse)?nil:boilerSafeToUse;
		boilerInspection.inspectionId = isEmpty(inspectionId)?nil:inspectionId;
		boilerInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
		boilerInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
		boilerInspection.spillageTest = isEmpty(spillageTest)?nil:spillageTest;
		boilerInspection.operatingPressureUnit = isEmpty(operatingPressureUnit)?nil:operatingPressureUnit;
		
		boilerInspection.boilerInspectionToBoiler = boiler;
		boiler.boilerToBoilerInspection = boilerInspection;
	}
}


#pragma mark Defects Data
- (void) loadBoilerDefectsData:(NSArray *)boilerDefectsList forBoiler:(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(boilerDefectsList))
	{
		for(NSDictionary *defectDictionary in boilerDefectsList)
		{
			if(!isEmpty(defectDictionary))
			{
				NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
				Defect *boilerDefect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
				
				if (boilerDefect != nil)
				{
					//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
				}
				else
				{
					boilerDefect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
				}
				
				NSNumber *boilerTypeId = [defectDictionary valueForKey:kBoilerTypeId];
				NSDate   *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
				NSString *defectNotes = [defectDictionary valueForKey:kDefectNotes];
				NSNumber *defectCategory  = [defectDictionary valueForKey:kDefectCategoryType];
				NSNumber *isRemedialActionTaken = [defectDictionary valueForKey:kIsRemedialActionTaken];
				NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
				NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
				NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
				NSString *remedialActionNotes = [defectDictionary valueForKey:kRemedialActionNotes];
				NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
				NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
				
				NSString *gasCouncilNumber = [defectDictionary valueForKey:kGasCouncilNumber];
				NSNumber *isDisconnected = [defectDictionary valueForKey:kIsDisconnected];
				NSNumber *isPartsRequired = [defectDictionary valueForKey:kIsPartsRequired];
				NSNumber *isPartsOrdered = [defectDictionary valueForKey:kIsPartsOrdered];
				NSNumber *partsOrderedBy = [defectDictionary valueForKey:kPartsOrderedBy];
				NSDate   *partsDueDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kPartsDueDate]];
				NSString *partsDescription = [defectDictionary valueForKey:kPartsDescription];
				NSString *partsLocation = [defectDictionary valueForKey:kPartsLocation];
				NSNumber *isTwoPersonsJob = [defectDictionary valueForKey:kIsTwoPersonsJob];
				NSString *reasonForTwoPerson = [defectDictionary valueForKey:kReasonForTwoPerson];
				NSNumber *duration = [defectDictionary valueForKey:kDuration];
				NSNumber *defectPriorityId = [defectDictionary valueForKey:kDefectPriorityId];
				NSNumber *defectTradeId = [defectDictionary valueForKey:kDefectTradeId];
				NSNumber *isCustomerHaveHeating = [defectDictionary valueForKey:kIsCustomerHaveHeating];
				NSNumber *isHeatersLeft = [defectDictionary valueForKey:kIsHeatersLeft];
				NSNumber *numberOfHeatersLeft = [defectDictionary valueForKey:kNumberOfHeatersLeft];
				NSNumber *isCustomerHaveHotWater = [defectDictionary valueForKey:kIsCustomerHaveHotWater];
				
				boilerDefect.boilerTypeId = isEmpty(boilerTypeId)?nil:boilerTypeId;
				boilerDefect.defectDate = isEmpty(defectDate)?nil:defectDate;
				boilerDefect.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
				boilerDefect.faultCategory = isEmpty(defectCategory)?nil:defectCategory;
				boilerDefect.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
				boilerDefect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				boilerDefect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
				boilerDefect.journalId = isEmpty(journalId)?nil:journalId;
				boilerDefect.propertyId = isEmpty(propertyId)?nil:propertyId;
				boilerDefect.remedialActionNotes = isEmpty(remedialActionNotes)?nil:remedialActionNotes;
				boilerDefect.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
				boilerDefect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				boilerDefect.defectID = isEmpty(defectId)?nil:defectId;
				boilerDefect.defectType = kDefectTypeBoiler;
				
				boilerDefect.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
				boilerDefect.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
				boilerDefect.isPartsRequired = isEmpty(isPartsRequired)?nil:isPartsRequired;
				boilerDefect.isPartsOrdered = isEmpty(isPartsOrdered)?nil:isPartsOrdered;
				boilerDefect.partsOrderedBy = isEmpty(partsOrderedBy)?nil:partsOrderedBy;
				boilerDefect.partsDueDate = isEmpty(partsDueDate)?nil:partsDueDate;
				boilerDefect.partsDescription = isEmpty(partsDescription)?nil:partsDescription;
				boilerDefect.partsLocation = isEmpty(partsLocation)?nil:partsLocation;
				boilerDefect.isTwoPersonsJob = isEmpty(isTwoPersonsJob)?nil:isTwoPersonsJob;
				boilerDefect.reasonForTwoPerson = isEmpty(reasonForTwoPerson)?nil:reasonForTwoPerson;
				boilerDefect.duration = isEmpty(duration)?nil:duration;
				boilerDefect.priorityId = isEmpty(defectPriorityId)?nil:defectPriorityId;
				boilerDefect.tradeId = isEmpty(defectTradeId)?nil:defectTradeId;
				boilerDefect.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
				boilerDefect.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
				boilerDefect.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
				boilerDefect.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
				
				//Setting Inverse relations
				
				// Defect Category
				DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:defectCategory];
				if (!isEmpty(objDefectCategory))
				{
					DefectCategory *objExistingDefectCategory = (DefectCategory *)[managedObjectContext objectWithID:objDefectCategory.objectID];
					if (!isEmpty(objExistingDefectCategory))
					{
						boilerDefect.defectCategory = objExistingDefectCategory;
					}
				}
				
				//Defect Priority
				DefectPriority *defectPriority = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([DefectPriority class]) Field:kDefectPriorityId WithValue:defectPriorityId context:managedObjectContext];
				if (!isEmpty(defectPriority))
				{
					DefectPriority *objDefectPriority = (DefectPriority *)[managedObjectContext objectWithID:defectPriority.objectID];
					if (!isEmpty(objDefectPriority))
					{
						boilerDefect.defectToDefectPriority = objDefectPriority;
					}
				}
				
				//Defect Trade
				Trade *trade = [[PSCoreDataManager sharedManager]findRecordFrom:NSStringFromClass([Trade class]) Field:kTradeId WithValue:defectTradeId context:managedObjectContext];
				if (!isEmpty(trade))
				{
					Trade *objTrade = (Trade *)[managedObjectContext objectWithID:trade.objectID];
					if (!isEmpty(objTrade))
					{
						boilerDefect.defectToTrade = objTrade;
					}
				}
				
				boilerDefect.defectToBoiler = boiler;
				[boiler addBoilerToDefectObject:boilerDefect];
				
			}
		}
	}
}



@end
