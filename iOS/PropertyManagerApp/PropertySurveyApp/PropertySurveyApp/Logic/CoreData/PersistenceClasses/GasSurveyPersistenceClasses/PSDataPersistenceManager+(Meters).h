//
//  PSDataPersistenceManager+(GasElectricCheck).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSDataPersistenceManager.h"

@interface PSDataPersistenceManager (Meters)

- (void) saveFetchedMeterData:(NSArray *) meterData forProperty:(NSManagedObjectID *)propertyId;
- (BOOL) saveMeter:(NSDictionary *)meter forProperty:(NSManagedObjectID *)propertyId;
- (BOOL) saveMeterInspectionForm:(NSDictionary *) inspectionFormDictionary forMeter:(Meter *)meter;

@end
