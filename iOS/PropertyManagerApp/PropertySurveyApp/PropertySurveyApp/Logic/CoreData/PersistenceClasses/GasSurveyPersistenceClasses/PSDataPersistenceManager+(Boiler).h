//
//  PSDataPersistenceManager+(Boiler).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSDataPersistenceManager.h"

@interface PSDataPersistenceManager (Boiler)

- (void) saveFetchedBoilerData:(NSArray *) boilerData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline;
- (void) saveBoilerInspectionForm:(NSDictionary *) inspectionFormDictionary forBoiler:(Boiler *)boiler;
- (void) loadBoilerData:(NSArray *)boilerData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline;
- (void) loadBoilerInspectionForm:(NSDictionary *)inspectionFormDictionary forBoiler:(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end
