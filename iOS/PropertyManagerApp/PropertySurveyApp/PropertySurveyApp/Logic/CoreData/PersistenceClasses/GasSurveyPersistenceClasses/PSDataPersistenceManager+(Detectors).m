  //
  //  PSDataPersistenceManager+(CommonData).m
  //  PropertySurveyApp
  //
  //  Created by Omer Nasir on 2015-08-08.
  //  Copyright (c) 2015 TkXel. All rights reserved.
  //

#import "PSDataPersistenceManager+(Detectors).h"


@implementation PSDataPersistenceManager (Detectors)


#pragma mark - Detectors Persistence

- (void) saveFetchedDetectorData:(NSArray *) detectorData forProperty:(NSManagedObjectID *)propertyId
{
  if([UtilityClass isUserLoggedIn])
    {
      // Creating managed objects
    CLS_LOG(@"Creating managed objects");
    
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    [managedObjectContext performBlock:^{
      Property *property;
      if(!isEmpty(detectorData) && !isEmpty(propertyId))
        {
        Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
        [[PSDataPersistenceManager sharedManager]loadDetectorsData:detectorData property:property managedObjectContext:managedObjectContext];
        }
      if (property != nil)
        {
        property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
        }
      
      [self saveDataInDB:managedObjectContext];
        //[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesSuccessNotification object:nil];
    }]; // parent
    }
}

- (BOOL) saveDetector:(NSDictionary *)detector forProperty:(NSManagedObjectID *)propertyId
{
  BOOL isSaved = NO;
  
  if([UtilityClass isUserLoggedIn] && !isEmpty(detector))
    {
      // Creating managed objects
    CLS_LOG(@"Creating managed objects");
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
    [[PSDataPersistenceManager sharedManager] loadDetectorData:detector property:property managedObjectContext:managedObjectContext];
    property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
    property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [self saveDataInDB:managedObjectContext];
    isSaved = YES;
    }
  return isSaved;
  
}


#pragma mark - Detectors Data

- (void) loadDetectorsData:(NSArray *)detectorsData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
  if(!isEmpty(detectorsData))
    {
    for (NSDictionary * detectorDictionary in detectorsData)
      {
      
      [self loadDetectorData:detectorDictionary property:property managedObjectContext:managedObjectContext];
      
      }
    }
  
}

- (void) loadDetectorData:(NSDictionary *)detectorDictionary property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
  if(!isEmpty(detectorDictionary))
    {
    NSNumber * detectorId = [detectorDictionary valueForKey:kDetectorId];
    NSString * detectorType = [detectorDictionary valueForKey:kDetectorsType];
    NSNumber * detectorTypeId = [detectorDictionary valueForKey:kDetectorTypeId];
		NSDate   * installedDate = isEmpty([detectorDictionary valueForKey:kInstalledDate])?nil:[UtilityClass convertServerDateToNSDate:[detectorDictionary valueForKey:kInstalledDate]];
    NSNumber * isDetectorInspected = [detectorDictionary valueForKey:kIsDetectorInspected];
    NSNumber * isLandlordsDetector = isEmpty([detectorDictionary valueForKey:kIsLandlordsDetector])?nil:[detectorDictionary valueForKey:kIsLandlordsDetector];
    NSString * location = [detectorDictionary valueForKey:kLocation];
    NSString * manufacturer = [detectorDictionary valueForKey:kManufacturer];
    //NSString * propertyId = [detectorDictionary valueForKey:kDetectorPropertyID];
    NSString * serialNumber = [detectorDictionary valueForKey:kSerialNumber];
    NSNumber * powerTypeId = [detectorDictionary valueForKey:kPowerTypeId];
		NSDate   * lastTestedDate = [UtilityClass convertServerDateToNSDate:[detectorDictionary valueForKey:kDetectorLastTestedDate]];
        NSNumber *schemeId = [detectorDictionary valueForKey:kSchemeID];
        NSNumber *blockId = [detectorDictionary valueForKey:kSchemeBlockID];
    
		NSManagedObjectID *detectorObjectId = [detectorDictionary valueForKey:@"detectorObjectId"];
		Detector *detector = nil;
    if(detectorObjectId != nil)
      {
      detector = (Detector *)[managedObjectContext objectWithID:detectorObjectId];
      }
    else
      {
      detector = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Detector class]) inManagedObjectContext:managedObjectContext];
      }
        detector.schemeId =isEmpty(schemeId)?nil:schemeId;
        detector.blockId = isEmpty(blockId)?nil:blockId;
    detector.detectorId =isEmpty(detectorId)?nil:detectorId;
    detector.detectorType = isEmpty(detectorType)?nil:detectorType;
    detector.detectorTypeId =isEmpty(detectorTypeId)?nil:detectorTypeId;
    detector.installedDate = isEmpty(installedDate)?nil:installedDate;
    detector.isInspected = isEmpty(isDetectorInspected)?nil:isDetectorInspected;
    detector.isLandlordsDetector = isEmpty(isLandlordsDetector)?nil:isLandlordsDetector;
    detector.location = isEmpty(location)?nil:location;
    detector.manufacturer =isEmpty(manufacturer)?nil:manufacturer;
    detector.propertyID = isEmpty(property.propertyId)?nil:property.propertyId;
    detector.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
    detector.powerTypeId = isEmpty(powerTypeId)?nil:powerTypeId;
		if (!isEmpty(lastTestedDate)) {
     detector.lastTestedDate = lastTestedDate;
		}
		
      //Set the Inverse Relationship
      
    NSArray* detectorDefects = [detectorDictionary valueForKey:@"detectorDefects"];
    [[PSDataPersistenceManager sharedManager] loadDetectorDefectsData:detectorDefects managedObjectContext:managedObjectContext];
    
      // Power Type
    PowerType *powerType = [[PSCoreDataManager sharedManager] powerTypeWithIdentifier:powerTypeId context:managedObjectContext];
    detector.detectorToPowerType = powerType;
    
      // Property
    if (!isEmpty(property))
      {
      detector.detectorToProperty = property;
      [property addPropertyToDetectorObject:detector];
      }
    
    }
}


- (void) loadDetectorInspectionForm:(NSDictionary *)inspectionFormDictionary forDetector:(Detector *)detector managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
  if (!isEmpty(inspectionFormDictionary) && !isEmpty(detector))
    {
    NSDate *batteryReplaced = ([[inspectionFormDictionary valueForKey:kBatteryReplaced] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kBatteryReplaced]]:[inspectionFormDictionary valueForKey:kBatteryReplaced];
    NSNumber *detectorInspectedBy = [inspectionFormDictionary valueForKey:kDetectorInspectedBy];
    NSDate *detectorInspectionDate = ([[inspectionFormDictionary valueForKey:kDetectorInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kDetectorInspectionDate]]:[inspectionFormDictionary valueForKey:kDetectorInspectionDate];
    NSNumber *isDetectorPassed = [inspectionFormDictionary valueForKey:kIsDetectorPassed];
    NSDate *detectorLastTestedDate = ([[inspectionFormDictionary valueForKey:kDetectorLastTestedDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kDetectorLastTestedDate]]:[inspectionFormDictionary valueForKey:kDetectorLastTestedDate];
    NSString *detectorNotes = [inspectionFormDictionary valueForKey:kDetectorNotes];
    
    NSManagedObjectID* _detectorId = detector.objectID;
    Detector *existingDetector = (Detector *)[managedObjectContext objectWithID:_detectorId];
    existingDetector.batteryReplaced = isEmpty(batteryReplaced)?nil: batteryReplaced;
    existingDetector.inspectedBy = isEmpty(detectorInspectedBy)?nil: detectorInspectedBy;
    existingDetector.inspectionDate = isEmpty(detectorInspectionDate)?nil: detectorInspectionDate;
    existingDetector.isPassed = isEmpty(isDetectorPassed)?nil:isDetectorPassed;
    existingDetector.lastTestedDate = isEmpty(detectorLastTestedDate)?nil:detectorLastTestedDate ;
    existingDetector.notes = isEmpty(detectorNotes)?nil: detectorNotes;
    existingDetector.isInspected = [NSNumber numberWithBool:YES];
    existingDetector.detectorToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
    existingDetector.detectorToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    }
  
}

- (BOOL) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *)detector
{
  BOOL isSaved = NO;
  
  if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
    {
    CLS_LOG(@"Creating managed objects");
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    [[PSDataPersistenceManager sharedManager]loadDetectorInspectionForm:inspectionFormDictionary forDetector:detector managedObjectContext:managedObjectContext];
    [self saveDataInDB:managedObjectContext];
    isSaved = YES;
    }
  
  return isSaved;
}




@end
