//
//  PSDataPersistenceManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
#import "PSDataPersistenceManager+(CommonData).h"
#import "PSDataPersistenceManager+(PrePostInspection).h"

#import "PSDatabaseContext.h"
#import "PSCoreDataManager.h"
#import "PSPropertyPictureManager.h"
#import "PropertyPicture+JSON.h"
#import "RepairPictures+JSON.h"
#import "Scheme+JSON.h"
#import "Survey.h"
#import "VoidData.h"
#import "RoomData.h"
#import "PauseReason.h"
#import "MeterType.h"
#import "PropertyComponents.h"
#import "MajorWorkRequired.h"
#import "WorkRequired+CoreDataClass.h"
#import "CoreDataHelper.h"
#import "MeterData.h"
#import "PaintPack.h"
#import "Appointment+JSON.h"
#import "JobPauseData.h"
#import "PropertyAttributesNotes.h"
#import "FaultRepairData+CoreDataClass.h"


@interface PSDataPersistenceManager ()

@end

@implementation PSDataPersistenceManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDataPersistenceManager *sharedManagerObject = nil;
+ (PSDataPersistenceManager *) sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedManagerObject = [[PSDataPersistenceManager alloc] init];
		// Do any other initialisation stuff here
	});
	return sharedManagerObject;
}

+(id)allocWithZone:(NSZone *)zone {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedManagerObject = [super allocWithZone:zone];
	});
	return sharedManagerObject;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

#pragma mark - Methods


-(BOOL)saveDataInDB:(NSManagedObjectContext *)dbContext
{
    BOOL status = TRUE;
        // Save the context.
    NSError *error = nil;
    if (![dbContext save:&error])
    {
        CLS_LOG(@"Error in Saving MOC: %@",[error description]);
        status = FALSE;
    }
    return status;
}



#pragma mark - PropertyPicture Persistence
- (void) savePropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Property*) propertyParam
{
	if(!isEmpty(pictureDictionary))
	{
		__block PropertyPicture *existingPicture = [[PSCoreDataManager sharedManager] propertyPictureWithIdentifier:[pictureDictionary valueForKey:kPropertyPictureId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Property* property = (Property*)[managedObjectContext objectWithID:propertyParam.objectID];
			if (!property)
			{
				property = propertyParam;
			}
			PropertyPicture *propertyPicture = nil;
			
			if(existingPicture != nil)
			{
				//User already exists in database, so just update the existing object.
				propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:existingPicture.objectID];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				propertyPicture = (PropertyPicture *)[NSEntityDescription insertNewObjectForEntityForName:kPropertyPicture inManagedObjectContext:managedObjectContext];
			}
			
			NSNumber *propertyPictureId = [pictureDictionary valueForKey:kPropertyPictureId];
			NSString * propertyId = [pictureDictionary valueForKey:kPropertyId];
			NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
			NSString *imagePath = [pictureDictionary valueForKey:kPropertyPictureId];
			NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSString * isDefault=[pictureDictionary valueForKey:kPropertyPictureIsDefault];
			UIImage * propertyImage = [pictureDictionary valueForKey:kPropertyImage];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
            NSNumber *schemeId = [pictureDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [pictureDictionary valueForKey:kSchemeBlockID];
            NSNumber *itemId = [pictureDictionary valueForKey:kItemId];
            NSNumber *heatingId = [pictureDictionary valueForKey:kHeatingId];
			
			propertyPicture.propertyPictureId = isEmpty(propertyPictureId)?nil:propertyPictureId;
			propertyPicture.propertyId = isEmpty(propertyId)?nil:propertyId;
			
			propertyPicture.itemId = itemId;
			propertyPicture.appointmentId = appointmentId;
            propertyPicture.heatingId = heatingId;
			//propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
			
			propertyPicture.propertyPictureToProperty = property;
			
			propertyPicture.createdOn = [NSDate date];
			propertyPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
			propertyPicture.synchStatus=[self generateIdentifier];
			propertyPicture.imageIdentifier = uniqueIdentifier;
			
			if ([isDefault isEqualToString:@"true"])
			{
				propertyPicture.isDefault=[NSNumber numberWithInt:YES];
				property.defaultPicture=propertyPicture;
			}
			else
			{
				propertyPicture.isDefault = [NSNumber numberWithInt:NO];
			}
			
			if([self saveDataInDB:managedObjectContext] == TRUE)
			{
				// if property picture is saved then need to update it on server
				PropertyPicture *propertyPicture1 = (PropertyPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
				[propertyPicture1 uploadPropertyPicture:pictureDictionary withImage:propertyImage];
			}
		}]; // parent
	}
}

#pragma mark - RepairPicture Persistence
- (void) saveRepairPictureInfo:(NSDictionary *)pictureDictionary
{
	if(!isEmpty(pictureDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			NSString *jobSheetNo = [pictureDictionary valueForKey:kRepairJSNumber];
			JobSheet* jobDataListObject = (JobSheet*)[[PSCoreDataManager sharedManager] jobDataWithJSN:jobSheetNo inContext:managedObjectContext];
			
			//Repair pircure does not exists in database, so create new user entity.
			RepairPictures* repairPicture = (RepairPictures *)[NSEntityDescription insertNewObjectForEntityForName:kRepairPictures inManagedObjectContext:managedObjectContext];
			
			NSNumber *imageId = [pictureDictionary valueForKey:kFaultRepairImageId];
			NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
			NSString *imagePath = [pictureDictionary valueForKey:kRepairImagePath];
			NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSNumber *isBeforeImage=[pictureDictionary valueForKey:kIsBeforeImage];
			UIImage  *repairImage = [pictureDictionary valueForKey:kRepairImage];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
			
			[pictureDictionary setValue:(isBeforeImage == [NSNumber numberWithBool:YES])?@"TRUE":@"FALSE" forKey:kIsBeforeImage];
			
			repairPicture.imageId = isEmpty(imageId)?nil:imageId;
			repairPicture.createdOn = [NSDate date];
			repairPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
			repairPicture.isBeforeImage=isEmpty(isBeforeImage)?nil:isBeforeImage;
			repairPicture.repairImage = isEmpty(repairImage)?nil:repairImage;
			repairPicture.syncStatus=[self generateIdentifier];
			repairPicture.repairImagesToJobSheet = jobDataListObject;
			repairPicture.imageIdentifier = uniqueIdentifier;
			
			[jobDataListObject addJobSheetToRepairImagesObject:repairPicture];
			
			if([self saveDataInDB:managedObjectContext] == true)
			{
				// if property picture is saved then need to update it on server
				RepairPictures *repairPicture1 = (RepairPictures *)[managedObjectContext objectWithID:repairPicture.objectID];
				[repairPicture1 uploadRepairPicture:pictureDictionary withImage:repairImage];
			}
		}]; // parent
	}
}

- (void) loadRepairPictureData:(NSDictionary *)pictureDictionary inContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(pictureDictionary) && !isEmpty(managedObjectContext))
	{
		NSString *jobSheetNo = [pictureDictionary valueForKey:kRepairJSNumber];
		
		JobSheet* jobDataListObject = (JobSheet*)[[PSCoreDataManager sharedManager] jobDataWithJSN:jobSheetNo inContext:managedObjectContext];
		
		//Repair pircure does not exists in database, so create new user entity.
		RepairPictures* repairPicture = (RepairPictures *)[NSEntityDescription insertNewObjectForEntityForName:kRepairPictures inManagedObjectContext:managedObjectContext];
		
		NSNumber *imageId = [pictureDictionary valueForKey:kFaultRepairImageId];
		NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
		NSString *imagePath = [pictureDictionary valueForKey:kRepairImagePath];
		NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
		NSNumber *isBeforeImage=[pictureDictionary valueForKey:kIsBeforeImage];
		UIImage  *repairImage = [pictureDictionary valueForKey:kRepairImage];
		NSDate *createdOn = ([[pictureDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[pictureDictionary valueForKey:kInspectionDate]]:[pictureDictionary valueForKey:kInspectionDate];
		
		[pictureDictionary setValue:(isBeforeImage == [NSNumber numberWithBool:YES])?@"TRUE":@"FALSE" forKey:kIsBeforeImage];
		repairPicture.imageId = isEmpty(imageId)?nil:imageId;
		repairPicture.createdOn = isEmpty(createdOn)?nil:createdOn;;
		repairPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
		repairPicture.isBeforeImage=isEmpty(isBeforeImage)?nil:isBeforeImage;
		repairPicture.repairImage = isEmpty(repairImage)?nil:repairImage;
		repairPicture.syncStatus=[self generateIdentifier];
		
		repairPicture.repairImagesToJobSheet = jobDataListObject;
		[jobDataListObject addJobSheetToRepairImagesObject:repairPicture];
		
	}
}

- (void) loadJobActivityData:(NSDictionary *)activityDictionary forJobSheet:(JobSheet*)jobDataListObject inContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(activityDictionary) && !isEmpty(managedObjectContext))
	{
		JobPauseData* objJobActivity = (JobPauseData *)[NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
		
		if (objJobActivity !=nil) {
			NSString *actionType = [activityDictionary valueForKey:kActionType];
			NSDate *pauseDate = [UtilityClass convertServerDateToNSDate:[activityDictionary valueForKey:kPauseDate]];
			NSString *pauseNotes = [activityDictionary valueForKey:kPauseNote];
			NSString *pauseReason = [activityDictionary valueForKey:kPauseReasonTitle];
			NSNumber *pausedBy = [activityDictionary valueForKey:kPausedBy];
			
			objJobActivity.actionType = isEmpty(actionType)?nil:actionType;
			objJobActivity.pauseDate = isEmpty(pauseDate)?nil:pauseDate;
			objJobActivity.pauseNote = isEmpty(pauseNotes)?nil:pauseNotes;
			objJobActivity.pausedBy = isEmpty(pausedBy)?nil:pausedBy;
			
			if(!isEmpty(pauseReason))
			{
				JobPauseReason *jobPauseReason = [[PSCoreDataManager sharedManager] jobPauseReasonWithName:pauseReason];
				objJobActivity.pauseReason = isEmpty(jobPauseReason)?nil:jobPauseReason;
			}
			
			[jobDataListObject addJobPauseDataObject:objJobActivity];
			
		}
	}
}

-(NSNumber*) generateIdentifier
{
	int a=arc4random();
	if (!a) {
		
		while (a) {
			
			a=arc4random();
			
			break;
		}
		
	}
	
	NSNumber *temp=[NSNumber numberWithInt:a];
	
	return temp;
	
}
#pragma mark - UserInfo Persistence
- (void) saveUserInfo:(NSDictionary *)userInfo
{
	if(!isEmpty(userInfo))
	{
		__block User *existingUser = [[PSCoreDataManager sharedManager] userWithIdentifier:[userInfo valueForKey:kUserId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			User *user = nil;
			if(existingUser != nil)
			{
				//User already exists in database, so just update the existing object.
				user = (User *)[managedObjectContext existingObjectWithID:existingUser.objectID error:nil];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				user = (User *)[NSEntityDescription insertNewObjectForEntityForName:kUser inManagedObjectContext:managedObjectContext];
			}
			
			NSString *salt = [userInfo valueForKey:kSalt];
			NSNumber *userId = [userInfo valueForKey:kUserId];
			NSString *userName = [userInfo valueForKey:kUserName];
			NSString *password = [SettingsClass sharedObject].currentUserPassword;
			NSString *fullName = [userInfo valueForKey:kFullName];
			NSNumber *isActive = [userInfo valueForKey:kIsActive];
			NSDate *lastLoggedInDate = [UtilityClass convertServerDateToNSDate:[userInfo valueForKey:kLastLoggedInDate]];
			
			user.salt = isEmpty(salt)?nil:salt;
			user.userId = isEmpty(userId)?nil:userId;
			user.userName = isEmpty(userName)?nil:userName;
			user.password = isEmpty(password)?nil:password;
			user.fullName = isEmpty(fullName)?nil:fullName;
			user.isActive = isEmpty(isActive)?nil:isActive;
			user.lastLoggedInDate = isEmpty(lastLoggedInDate)?nil:lastLoggedInDate;
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUserInfoSaveCompleted object:nil];
		}]; // parent
	}
}
#pragma mark - Appliances Data Persistence
- (void) saveFetchedApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSArray * applinceLocationBlock = appliancesData;
		__block Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
		[managedObjectContext performBlock:^{
			
			if(!isEmpty(applinceLocationBlock))
			{
				[[PSDataPersistenceManager sharedManager] loadApplianceData:applinceLocationBlock property:property managedObjectContext:managedObjectContext isAddedOffline:offline];
			}
			property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}

- (void) saveApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appliancesData))
	{
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
			[[PSDataPersistenceManager sharedManager] loadApplianceData:appliancesData property:property managedObjectContext:managedObjectContext isAddedOffline:offline];
			
			property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneSuccessNotification object:nil];
		}]; // parent
	}
}



- (void) deleteApplianceData:(NSDictionary *) appliancesData forProperty:(NSManagedObjectID *)property
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appliancesData))
	{
		CLS_LOG(@"Deleting managed object");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Property * applianceProperty = (Property *)[managedObjectContext objectWithID:property];
			[[PSDataPersistenceManager sharedManager] deleteAppliance:appliancesData property:applianceProperty managedObjectContext:managedObjectContext];
			
			applianceProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			applianceProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kDeleteApplianceSuccessNotification object:nil];
		}];
	}
}

- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:locationArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:manufecturerArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:modelArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceTypeData:typeArray appliance:nil managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}

- (void) saveAppliances:(NSDictionary *)appliancesData
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appliancesData))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			NSArray *applianceLocations = [appliancesData valueForKey:@"ApplianceLocation"];
			NSArray *applianceManufacturers = [appliancesData valueForKey:@"ApplianceManufacturer"];
			NSArray *applianceModels = [appliancesData valueForKey:@"ApplianceModel"];
			NSArray *applianceTypes = [appliancesData valueForKey:@"ApplianceType"];
			NSArray *defectCategories = [appliancesData valueForKey:@"defectCategories"];
			
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:applianceLocations appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:applianceManufacturers appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:applianceModels appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceTypeData:applianceTypes appliance:nil managedObjectContext:managedObjectContext];
			
			[[PSDataPersistenceManager sharedManager] loadDefectCategoryData:defectCategories managedObjectContext:managedObjectContext];
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}
#pragma mark - Inspection Form Data Persistence

- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *)appliance
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID* _applianceId = appliance.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Appliance *existingAppliance = (Appliance *)[managedObjectContext objectWithID:_applianceId];
			[[PSDataPersistenceManager sharedManager] loadInspectionForm:inspectionFormDictionary forAppliance:existingAppliance managedObjectContext:managedObjectContext];
			
			
			existingAppliance.isInspected = [NSNumber numberWithBool:YES];
			existingAppliance.applianceToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			existingAppliance.applianceToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveApplianceInspectionFormSuccessNotification object:nil];
		}]; // parent
	}
}



#pragma mark - Pipework Installation Data Persistence

- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forBoiler:(Boiler *)boiler
{
    if([UtilityClass isUserLoggedIn] && !isEmpty(pipeworkDictionary))
    {
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectID* _boilerId = boiler.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            Boiler *_boiler = (Boiler *)[managedObjectContext objectWithID:_boilerId];
            [[PSDataPersistenceManager sharedManager] loadPipeworkForm:pipeworkDictionary forBoiler:_boiler managedObjectContext:managedObjectContext];
            _boiler.boilerToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
            _boiler.boilerToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
            
            [self saveDataInDB:managedObjectContext];
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSavePropertyPipeworkFormSuccessNotification object:nil];
        }]; // parent
    }
}


#pragma mark - Job Data Persistence
- (void) saveFetchedFaultRepairData:(NSArray *)faultRepairDataList
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(faultRepairDataList))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager]loadFaultRepairData:faultRepairDataList jobData:nil managedObjectContext:managedObjectContext];
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchFaultRepairDataSaveSuccessNotification object:nil];
		}]; // parent
	}
}

- (void) savefetchedJobPauseReasons:(NSArray *)jobPauseReasons
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(jobPauseReasons))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager]loadJobPauseReasonData:jobPauseReasons managedObjectContext:managedObjectContext];
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchJobPauseReasonsSaveSuccessNotification object:nil];
		}]; // parent
	}
}

- (void) saveFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(FaultJobSheet *)jobData
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(jobData))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID *jobDataId = jobData.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			FaultJobSheet *existingJobData = (FaultJobSheet *)[managedObjectContext objectWithID:jobDataId];
			if (!isEmpty(existingJobData))
			{
				[[PSDataPersistenceManager sharedManager]loadFaultRepairHistoryData:faultRepairHistory jobData:existingJobData managedObjectContext:managedObjectContext];
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveFaultRepairListFailureNotification object:nil];
			}
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveFaultRepairListSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - Surveyors Data Persistence
- (void) saveSurveyors:(NSArray *)surveyorsArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(surveyorsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			int counter = 1;
			for (NSDictionary *surveyorData in surveyorsArray)
			{
				[[PSDataPersistenceManager sharedManager] loadSurveyorData:surveyorData managedObjectContext:managedObjectContext];
				
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:managedObjectContext];
				}
				counter++;
			}
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - SProperties Data Persistence
- (void) saveProperties:(NSArray *)propertyArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(propertyArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		[managedObjectContext performBlock:^{
			int counter = 1;
			for (NSDictionary *propertyData in propertyArray)
			{
				[[PSDataPersistenceManager sharedManager] loadSPropertyData:propertyData managedObjectContext:managedObjectContext];
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:managedObjectContext];
				}
				counter++;
			}
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - New Appointment Persistence

- (void) createNewAppointment: (NSDictionary *)appointmentDictionary
{
	if ([UtilityClass isUserLoggedIn] && !isEmpty(appointmentDictionary)) {
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			[[PSDataPersistenceManager sharedManager] loadAppointmentData:appointmentDictionary managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentSuccessNotification object:nil];
		}]; // parent
	}
	
}

#pragma mark - Appointment Persistence

/*!
 @discussion
 This method is called to set appointment 'isModified' attribute false after modified appointments are posted
 to the server, so that same data is not posted again and again to server. Flag determines whether to post
 notifification or not.
 */
- (void) changeAppointmentModificationStatus:(NSArray *)appointmentsArray  savedAppointments:(NSArray*)savedAppointments failedAppointments:(NSArray*)failedAppointments postNotification:(BOOL)flag
{
	if(!isEmpty(appointmentsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			for (Appointment *appointment in appointmentsArray)
			{
				NSNumber *appointmentId = appointment.appointmentId;
				if (!isEmpty(appointmentId))
				{
					__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
					if (existingAppointment != nil)
					{
						// we have to only check if some appointments failed
						if(!isEmpty(failedAppointments))
						{
							for (NSDictionary* object in failedAppointments)
							{
								if(!isEmpty([object objectForKey:kAppointmentId]) && [appointment.appointmentId isEqualToNumber:[object objectForKey:kAppointmentId]])
								{
									existingAppointment.isModified = [NSNumber numberWithBool:YES];
									existingAppointment.failedReason = isEmpty([object objectForKey:kAppointmentFailedReason])?nil:[object objectForKey:kAppointmentFailedReason];
                                    
                                    if([existingAppointment.appointmentType isEqualToString:kAppointmentTypeGas] || [existingAppointment.appointmentType isEqualToString:kAppointmentTypeAlternativeFuel] || [existingAppointment.appointmentType isEqualToString:kAppointmentTypeOil] )
                                    {
                                        existingAppointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:AppointmentStatusInProgress];
                                    }
								}
							}
						}
						else
						{
							existingAppointment.isModified = [NSNumber numberWithBool:NO];
							existingAppointment.failedReason = nil;
						}
					}
				}
			}
			[self saveDataInDB:managedObjectContext];
			
			if (flag)
			{
				if(isEmpty(savedAppointments) == FALSE && [savedAppointments count] > 0)
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentModificationStatusChangeSuccessNotification object:nil];
				}
				if(isEmpty(failedAppointments) == FALSE && [failedAppointments count] > 0)
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentModificationStatusChangeFailureNotification object:failedAppointments];
				}
			}
			
		}]; // parent
	}
}

- (void) updateModificationStatusOfAppointments:(NSArray *)appointmentsArray  toModificationStatus:(NSNumber*)modificationStatus
{
	if(!isEmpty(appointmentsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSNumber * blockModficationStatus = modificationStatus;
		__block NSArray * appointmentsArrayForBlock = appointmentsArray;
		
		[managedObjectContext performBlock:^{
			
			for (Appointment *appointment in appointmentsArrayForBlock)
			{
				NSNumber *appointmentId = appointment.appointmentId;
				if (!isEmpty(appointmentId))
				{
					__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
					if (existingAppointment)
					{
						existingAppointment.isModified = blockModficationStatus;
					}
				}
			}
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}


- (void) saveAppointments:(NSArray *)appointmentsArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appointmentsArray))
	{
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			int counter = 1;
			for (NSDictionary *appointmentData in appointmentsArray)
			{
				[[PSDataPersistenceManager sharedManager] loadAppointmentData:appointmentData managedObjectContext:managedObjectContext];
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:managedObjectContext];
				}
				counter++;
			}
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - Appointment Survey Data Persistence
- (void) saveAppointmentSurveyData:(NSDictionary *)surveyData
{
	if(!isEmpty(surveyData))
	{
		NSDictionary *appointmentInfo = [surveyData objectForKey:@"appointmentinfo"];
		
		__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithPropertyIdentifier:[appointmentInfo valueForKey:kPropertyId]
																																																			appointmentId:[appointmentInfo valueForKey:kAppointmentId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			Appointment *appointment = nil;
			if(existingAppointment != nil)
			{
				appointment = (Appointment *)[managedObjectContext objectWithID:existingAppointment.objectID];
				
				Survey *survey = [NSEntityDescription insertNewObjectForEntityForName:kSurvey inManagedObjectContext:managedObjectContext];
				NSString *surveyJSON = [surveyData JSONRepresentation];
				survey.surveyJSON = isEmpty(surveyJSON)?nil:surveyJSON;
				appointment.appointmentToSurvey = survey;
				appointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
				survey.surveyToAppointment = appointment;
			}
			else
			{
				//Appointment Does Not Exists.
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveFailureNotification object:nil];
			}
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveSucessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - AppointmentData
- (void) loadAppointmentData:(NSDictionary *)appointmentData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(appointmentData))
	{
		NSNumber * appointmentId = [appointmentData valueForKey:kAppointmentId];
		__block Appointment *appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
		if (appointment == nil)
		{
			appointment = [NSEntityDescription insertNewObjectForEntityForName:kAppointment inManagedObjectContext:managedObjectContext];
			
			NSString * appointmentCalendar = [appointmentData valueForKey:kAppointmentCalendar];
			NSDate * appointmentDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentDate]];
			NSDate * appointmentStartTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentStartTime]];
			NSDate * appointmentEndTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentEndTime]];
			NSString * appointmentStartTimeString = [appointmentData valueForKey:kAppointmentStartTimeString];
			NSString * appointmentEndTimeString = [appointmentData valueForKey:kAppointmentEndTimeString];
			NSString * appointmentLocation = [appointmentData valueForKey:kAppointmentLocation];
			NSString * appointmentNotes = [appointmentData valueForKey:kAppointmentNotes];
			NSNumber * appointmentOverdue = [appointmentData valueForKey:kAppointmentOverdue];
			NSString * appointmentShift = [appointmentData valueForKey:kAppointmentShift];
			NSString * appointmentStatus = [appointmentData valueForKey:kAppointmentStatus];
			NSString * appointmentTitle = [appointmentData valueForKey:kAppointmentTitle];
			NSString * appointmentType = [appointmentData valueForKey:kAppointmentType];
			NSNumber * assignedTo = [appointmentData valueForKey:kAssignedTo];
			NSNumber * createdBy = [appointmentData valueForKey:kCreatedBy];
			NSNumber * defaultCustomerId = [appointmentData valueForKey:kDefaultCustomerId];
			NSNumber * defaultCustomerIndex = [appointmentData valueForKey:kDefaultCustomerIndex];
			NSNumber * journalHistoryId = [appointmentData valueForKey:kJournalHistoryId];
			NSNumber * journalId = [appointmentData valueForKey:kJournalId];
			NSNumber * jsgNumber = [appointmentData valueForKey:kJsgNumber];
			NSDate * loggedDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kLoggedDate]];
			NSString * noEntryNotes = [appointmentData valueForKey:kNoEntryNotes];
			NSDate * repairCompletionDateTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kRepairCompletionDateTime]];
			NSString * repairNotes = [appointmentData valueForKey:kRepairNotes];
			id surveyCheckListForm = nil;
			NSString * surveyJSON = [appointmentData valueForKey:kSurveyFormJSON];
			NSString * surveyorAlert = [appointmentData valueForKey:kSurveyorAlert];
			NSString * surveyorUserName = [appointmentData valueForKey:kSurveyorUserName];
			NSString * surveyourAvailability = [appointmentData valueForKey:kSurveyourAvailability];
			NSString * createdByPerson = [appointmentData valueForKey:kCreatedByPerson];
			NSString * surveyType = [appointmentData valueForKey:kSurveyType];
			NSNumber * isMiscAppointment = [appointmentData valueForKey:kIsMiscAppointment];
			NSNumber * tenancyId = [appointmentData valueForKey:kTenancyId];
			NSNumber *addToCalendar = [appointmentData valueForKey:kAppointmentAddtoCalendar];
			NSString * jsonTypeProperty = [appointmentData valueForKey:@"__type"];
            NSDate * creationDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentCreationDate]];
            NSString *heatingFuel = [appointmentData valueForKey:kAppointmentHeatingFuel];
            
            appointment.creationDate = isEmpty(creationDate)?nil: creationDate;
			appointment.appointmentCalendar = isEmpty(appointmentCalendar)?nil:appointmentCalendar;
			appointment.appointmentEndTime = isEmpty(appointmentEndTime)?nil:appointmentEndTime;
			appointment.appointmentId = isEmpty(appointmentId)?nil:appointmentId;
			appointment.appointmentLocation = isEmpty(appointmentLocation)?nil:appointmentLocation;
			appointment.appointmentNotes = isEmpty(appointmentNotes)?nil:appointmentNotes;
			appointment.appointmentOverdue = isEmpty(appointmentOverdue)?nil:appointmentOverdue;
			appointment.appointmentShift = isEmpty(appointmentShift)?nil:appointmentShift;
			appointment.appointmentStartTime = isEmpty(appointmentStartTime)?nil:appointmentStartTime;
			appointment.appointmentDate = isEmpty(appointmentStartTime)?nil: appointmentStartTime;
			appointment.appointmentStartTimeString = isEmpty(appointmentStartTimeString)?nil:appointmentStartTimeString;
			appointment.appointmentEndTimeString = isEmpty(appointmentEndTimeString)?nil: appointmentEndTimeString;
			appointment.appointmentStatus = isEmpty(appointmentStatus)?nil:appointmentStatus;
			appointment.appointmentTitle = isEmpty(appointmentTitle)?nil:appointmentTitle;
			appointment.appointmentType = isEmpty(appointmentType)?nil:appointmentType;
			appointment.assignedTo = isEmpty(assignedTo)?nil:assignedTo;
			appointment.createdBy = isEmpty(createdBy)?nil:createdBy;
			appointment.defaultCustomerId = isEmpty(defaultCustomerId)?nil:defaultCustomerId;
			appointment.defaultCustomerIndex = isEmpty(defaultCustomerIndex)?nil:defaultCustomerIndex;
			appointment.journalHistoryId = isEmpty(journalHistoryId)?nil:journalHistoryId;
			appointment.journalId = isEmpty(journalId)?nil:journalId;
			appointment.jsgNumber = isEmpty(jsgNumber)?nil:jsgNumber;
			appointment.loggedDate = isEmpty(loggedDate)?nil:loggedDate;
			appointment.noEntryNotes = isEmpty(noEntryNotes)?nil:noEntryNotes;
			appointment.repairCompletionDateTime = isEmpty(repairCompletionDateTime)?nil:repairCompletionDateTime;
			appointment.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
			appointment.surveyCheckListForm = isEmpty(surveyCheckListForm)?nil:surveyCheckListForm;
			appointment.appointmentToSurvey.surveyJSON = isEmpty(surveyJSON)?nil:surveyJSON;
			appointment.surveyorAlert = isEmpty(surveyorAlert)?nil:surveyorAlert;
			appointment.surveyorUserName = isEmpty(surveyorUserName)?nil:surveyorUserName;
			appointment.surveyourAvailability = isEmpty(surveyourAvailability)?nil:surveyourAvailability;
			appointment.surveyType = isEmpty(surveyType)?nil:surveyType;
			appointment.tenancyId = isEmpty(tenancyId)?nil:tenancyId;
            appointment.heatingFuel =isEmpty(heatingFuel)?nil:heatingFuel;
			appointment.createdByPerson = isEmpty(createdByPerson)?nil:createdByPerson;
			appointment.isModified = [NSNumber numberWithBool:NO];
			appointment.isSurveyChanged = [NSNumber numberWithBool:NO];
			appointment.addToCalendar = /*[NSNumber numberWithInt:1];*/ isEmpty(addToCalendar)?nil:addToCalendar;
			appointment.isMiscAppointment = isEmpty(isMiscAppointment)?[NSNumber numberWithBool:NO]:isMiscAppointment;
			appointment.failedReason = nil;
			
			NSDictionary *propertyData = [appointmentData valueForKey:kAppointmentToProperty];
			NSDictionary *schemeData = [appointmentData valueForKey:kAppointmentToScheme];
			
			if([propertyData isEqual:[NSNull null]])
			{
				propertyData = nil;
			}
			
			if(propertyData)
			{
				[self loadAppointmentPropertyData:propertyData appointment:appointment managedObjectContext:managedObjectContext];
                if(!isEmpty(appointment.appointmentToProperty.propertyId)){
                    appointment.appointmentTitle = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
                }
                else if(!isEmpty(appointment.appointmentToProperty.blockId)){
                    appointment.appointmentTitle = appointment.appointmentToProperty.blockName;
                }
                else{
                    appointment.appointmentTitle = appointment.appointmentToProperty.schemeName;
                }
				
			}
			else
			{
				NSDictionary *schemeData = [appointmentData valueForKey:kAppointmentToScheme];
				[self loadAppointmentSchemeData:schemeData appointment:appointment managedObjectContext:managedObjectContext];
				appointment.appointmentTitle = appointment.appointmentToScheme.schemeName;
			}
			
			//////
			// Void Appointments Data
			// [1,2] Pre & Post Inspection
			// [3]   Void Works Required
			// [4,5] Gas & Electric Check
			[self loadVoidAppointmentData:appointmentData appointment:appointment dbContext:managedObjectContext];
			
			//Appointment Releationsship Keys
			NSDictionary *appInfoData = [appointmentData valueForKey:kAppointmentToAppInfoData];
			[self loadAppointmentAppInfoData:appInfoData appointment:appointment managedObjectContext:managedObjectContext];
			
			// Server is sending CP12 and Component Trade structure with null data.
			// Legacy appointments have same UI for all.
			if([appointment getType] != AppointmentTypeFault)
			{
				NSArray *CP12Info = [appointmentData valueForKey:kAppointmentToCP12Info];
                if(!isEmpty(CP12Info)){
                    for(NSDictionary *dict in CP12Info){
                        [self loadAppointmentCP12Info:dict appointment:appointment managedObjectContext:managedObjectContext];
                    }
                }
                else{
                    [self addDefaultCP12InfoForNonInspectedBoilersWithAppointment:appointment managedObjectContext:managedObjectContext];
                }
				
				
				NSDictionary * tradeComponent = [appointmentData valueForKey:kAppointmentToComponentTrade];
				[self loadAppointmentTradeComponent:tradeComponent appointment:appointment managedObjectContext:managedObjectContext];
			}
			NSArray *customerList = [appointmentData valueForKey:kAppointmentToCustomer];
			[self loadAppointmentCustomerData:customerList appointment:appointment managedObjectContext:managedObjectContext];
            
            NSArray *propertyAttributesNotes = [appointmentData valueForKey:kAppointmentToPropertyAttributeNotes];
            [self loadAppointmentPropertyAttributesNotes:propertyAttributesNotes appointment:appointment managedObjectContext:managedObjectContext];
			
			NSArray *jobDataList = [appointmentData valueForKey:kAppointmentToJobDataList];
			if ([jsonTypeProperty isEqualToString:kJsonTypeDefect]) {
				[self loadDefectAppointmentJobDataList:jobDataList appointment:appointment managedObjectContext:managedObjectContext];
			}
			else{
				[self loadAppointmentJobSheet:jobDataList appointment:appointment managedObjectContext:managedObjectContext];
			}
			
			NSDictionary *journalData = [appointmentData valueForKey:kAppointmentToJournal];
			[self loadAppointmentJournalData:journalData appointment:appointment managedObjectContext:managedObjectContext];
			
			//Add Appointment into logged in user.
			User *loggedInUser = (User *)[managedObjectContext existingObjectWithID:[[SettingsClass sharedObject].loggedInUser objectID] error:nil];
			if(loggedInUser)
			{
				[loggedInUser addUserToAppointmentsObject:appointment];
				appointment.appointmentToUser = loggedInUser;
			}
			
			//Populate Aggregate Values
			__block NSString *appointmentEventIdentifier;
			if ([appointment.addToCalendar boolValue] && appointment.appointmentStatus !=[UtilityClass statusStringForAppointmentStatus: [UtilityClass completionStatusForAppointmentType:[appointment getType]]])
			{
				NSString *appointmentTitle = [NSString stringWithFormat:@"%@: %@ (%@)",appointment.appointmentType, appointment.appointmentTitle, appointment.appointmentId];
				[[PSAppointmentsManager sharedManager] deleteCalendarEventForAppointment:appointment];
				NSMutableDictionary *appointmentEventDictionary = [NSMutableDictionary dictionary];
				
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentEndTime)?[NSNull null]:appointment.appointmentEndTime forKey:kAppointmentEndTime];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentStartTime)?[NSNull null]:appointment.appointmentStartTime forKey:kAppointmentStartTime];
				[appointmentEventDictionary setObject:isEmpty(appointmentTitle)?[NSNull null]:appointmentTitle forKey:kAppointmentTitle];
				[appointmentEventDictionary setObject:isEmpty(appointment.surveyorAlert)?[NSNull null]:appointment.surveyorAlert forKey:kSurveyorAlert];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentNotes)?[NSNull null]:appointment.appointmentNotes forKey:kAppointmentNotes];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentLocation)?[NSNull null]:appointment.appointmentLocation forKey:kAppointmentLocation];
				[[PSEventsManager sharedManager] createNewEventWithDictionary:appointmentEventDictionary inCalendar:appointment.appointmentCalendar completion:^(NSString *eventIdentifer, NSError *error){}];
			}
		}
	}
}

- (NSString *) createAppointmentEvent:(NSDictionary *)calendarEventDictionary inCalendar:(NSString *)calendar
{
	__block NSString *appointmentEventIdentifier;
	[[PSEventsManager sharedManager] createNewEventWithDictionary:calendarEventDictionary inCalendar:calendar completion:^(NSString *eventIdentifer, NSError *error) {
		appointmentEventIdentifier = eventIdentifer;
	}];
	
	return appointmentEventIdentifier;
}



#pragma mark - SurveyorData
- (void) loadSurveyorData:(NSDictionary *)surveyorData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(surveyorData))
	{
		Surveyor *surveyor = [[PSCoreDataManager sharedManager] surveyorWithIdentifier:[surveyorData valueForKey:kUserId]];
		
		if(surveyor != nil)
		{
			//User already exists in database, so just update the existing object.
			surveyor = (Surveyor *)[managedObjectContext existingObjectWithID:surveyor.objectID error:nil];
		}
		else
		{
			//User does not exists in database, so create new user entity.
			surveyor = (Surveyor *)[NSEntityDescription insertNewObjectForEntityForName:kSurveyor inManagedObjectContext:managedObjectContext];
		}
		
		NSString * fullName = [surveyorData valueForKey:kFullName];
		NSNumber * userId = [surveyorData valueForKey:kUserId];
		NSString * userName = [surveyorData valueForKey:kUserName];
		NSString * surveyorType = [[SettingsClass sharedObject] applicationTypeName];
		
		surveyor.fullName = isEmpty(fullName)?nil:fullName;
		surveyor.userId = isEmpty(userId)?nil:userId;
		surveyor.userName = isEmpty(userName)?nil:userName;
		surveyor.surveyorType = isEmpty(surveyorType)?nil:surveyorType;
		surveyor.surveyorNameSectionIdentifier = [surveyor surveyorNameSectionIdentifier];
	}
}

#pragma mark - SurveyData
- (void) loadAppointmentSurvey:(NSDictionary *)surveyData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
}

#pragma mark - SPropertyData
- (void) loadSPropertyData:(NSDictionary *)propertyData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(propertyData))
	{
		NSDictionary *propertyDictionary = [NSDictionary dictionaryWithDictionary:[propertyData objectForKey:@"property"]];
		SProperty *property = [[PSCoreDataManager sharedManager] sPropertyWithIdentifier:[propertyDictionary valueForKey:kPropertyId]];
		
		if(property != nil)
		{
			//User already exists in database, so just update the existing object.
			property = (SProperty *)[managedObjectContext existingObjectWithID:property.objectID error:nil];
		}
		else
		{
			//User does not exists in database, so create new user entity.
			property = (SProperty *)[NSEntityDescription insertNewObjectForEntityForName:kSProperty inManagedObjectContext:managedObjectContext];
		}
		
		NSString *address1 = [propertyDictionary valueForKey:kAddress1];
		NSString *address2 = [propertyDictionary valueForKey:kAddress2];
		NSString *address3 = [propertyDictionary valueForKey:kAddress3];
		NSDate * certificateExpiry = [UtilityClass convertServerDateToNSDate:[propertyDictionary valueForKey:kCertificateExpiry]];
		NSString *county = [propertyDictionary valueForKey:kCounty];
		NSString *flatNumber = [propertyDictionary valueForKey:kFlatNumber];
		NSString *houseNumber = [propertyDictionary valueForKey:kHouseNumber];
		NSString *postCode = [propertyDictionary valueForKey:kPostCode];
		NSString *propertyId = [propertyDictionary valueForKey:kPropertyId];
		NSString *townCity = [propertyDictionary valueForKey:kTownCity];
		NSString *firstName = [propertyData valueForKey:kCustomerFirstName];
		NSString *lastName = [propertyData valueForKey:kCustomerLastName];
		NSString *middleName = [propertyData valueForKey:kCustomerMiddleName];
		NSString *title = [propertyData valueForKey:kCustomerTitle];
		NSString *sPropertyJSON = [propertyData JSONRepresentation];
		
		property.address1 = isEmpty(address1)?nil:address1;
		property.address2 = isEmpty(address2)?nil:address2;
		property.address3 = isEmpty(address3)?nil:address3;
		property.certificateExpiry = isEmpty(certificateExpiry)?nil:certificateExpiry;
		property.county = isEmpty(county)?nil:county;
		property.flatNumber = isEmpty(flatNumber)?nil:flatNumber;
		property.houseNumber = isEmpty(houseNumber)?nil:houseNumber;
		property.postCode = isEmpty(postCode)?nil:postCode;
		property.propertyId = isEmpty(propertyId)?nil:propertyId;
		property.townCity = isEmpty(townCity)?nil:townCity;
		property.firstName = isEmpty(firstName)?nil:firstName;
		property.middleName = isEmpty(middleName)?nil:middleName;
		property.lastName = isEmpty(lastName)?nil:lastName;
		property.title = isEmpty(title)?nil:[title stringByAppendingString:@"."];
		property.sPropertyJSON = isEmpty(sPropertyJSON)?nil:sPropertyJSON;
		property.sPropertyNameSectionIdentifier = [property sPropertyNameSectionIdentifier];
		property.fullName = [property fullNameWithoutTitle];
	}
}

#pragma mark - AppInfoData
- (void) loadAppointmentAppInfoData:(NSDictionary *)appInfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(appInfoDictionary))
	{
		AppInfoData *appInfoData = [NSEntityDescription insertNewObjectForEntityForName:kAppInfoData inManagedObjectContext:managedObjectContext];
		
		NSNumber * totalAppointments = [appInfoDictionary valueForKey:@"totalAppointments"];
		NSNumber * totalNoEntries = [appInfoDictionary valueForKey:@"totalNoEntries"];
		
		totalAppointments = isEmpty(totalAppointments)?nil:totalAppointments;
		totalNoEntries = isEmpty(totalNoEntries)?nil:totalNoEntries;
		
		appInfoData.totalAppointments = totalAppointments;
		appInfoData.totalNoEntries = totalNoEntries;
		
		//Set the Inverse Relationship
		appInfoData.appInfoDataToAppointment = appointment;
		appointment.appointmentToAppInfoData = appInfoData;
	}
}

#pragma mark - CP12Info

-(void) addDefaultCP12InfoForNonInspectedBoilersWithAppointment: (Appointment *) appointment managedObjectContext:(NSManagedObjectContext *) managedObjectContext{
    if(!isEmpty(appointment)){
        CP12Info *cp12Info = [NSEntityDescription insertNewObjectForEntityForName:kCP12Info inManagedObjectContext:managedObjectContext];
        cp12Info.cp12InfoToAppointment = appointment;
        appointment.appointmentToCP12Info = cp12Info;
    }
}

- (void) loadAppointmentCP12Info:(NSDictionary *)CP12InfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if(!isEmpty(appointment)){
        CP12Info *cp12Info = [NSEntityDescription insertNewObjectForEntityForName:kCP12Info inManagedObjectContext:managedObjectContext];
        if(!isEmpty(CP12InfoDictionary))
        {
            NSString * cp12Document = [CP12InfoDictionary valueForKey:@"CP12Document"];
            NSString * cp12Number = [CP12InfoDictionary valueForKey:@"CP12Number"];
            NSDate * dateTimeStamp = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"DTimeStamp"]];
            NSString * documentType = [CP12InfoDictionary valueForKey:@"DocumentType"];
            NSNumber * inspectionCarried = [CP12InfoDictionary valueForKey:@"InspectionCarried"];
            NSNumber * issuedBy = [CP12InfoDictionary valueForKey:@"IssuedBy"];
            NSDate * issuedDate = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"IssuedDate"]];
            NSString * issuedDateString = [CP12InfoDictionary valueForKey:@"IssuedDateString"];
            NSNumber * journalId = [CP12InfoDictionary valueForKey:@"JournalId"];
            NSNumber * jsgNumber = [CP12InfoDictionary valueForKey:@"jsgNumber"];
            NSNumber * lsgrid = [CP12InfoDictionary valueForKey:@"LGSRID"];
            NSString * notes = [CP12InfoDictionary valueForKey:@"Notes"];
            NSString * propertyID = [CP12InfoDictionary valueForKey:kPropertyId];
            NSDate * receivedDate = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"ReceivedDate"]];
            NSString * receivedDateString = [CP12InfoDictionary valueForKey:@"ReceivedDateString"];
            NSString * receivedOnBehalfOf = [CP12InfoDictionary valueForKey:@"ReceivedOnBehalfOf"];
            NSNumber * tenantHanded = [CP12InfoDictionary valueForKey:@"TenantHanded"];
            
            NSNumber *schemeId = [CP12InfoDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [CP12InfoDictionary valueForKey:kSchemeBlockID];
            NSNumber *heatingId = [CP12InfoDictionary valueForKey:kHeatingId];
            
            cp12Document = isEmpty(cp12Document)?nil:cp12Document;
            cp12Number = isEmpty(cp12Number)?nil:cp12Number;
            dateTimeStamp = isEmpty(dateTimeStamp)?nil:dateTimeStamp;
            documentType = isEmpty(documentType)?nil:documentType;
            inspectionCarried = isEmpty(inspectionCarried)?nil:inspectionCarried;
            issuedBy = isEmpty(issuedBy)?nil:issuedBy;
            issuedDate = isEmpty(issuedDate)?nil:issuedDate;
            issuedDateString = isEmpty(issuedDateString)?nil:issuedDateString;
            journalId = isEmpty(journalId)?nil:journalId;
            jsgNumber = isEmpty(jsgNumber)?nil:jsgNumber;
            lsgrid = isEmpty(lsgrid)?nil:lsgrid;
            notes = isEmpty(notes)?nil:notes;
            propertyID = isEmpty(propertyID)?nil:propertyID;
            receivedDate = isEmpty(receivedDate)?nil:receivedDate;
            receivedDateString = isEmpty(receivedDateString)?nil:receivedDateString;
            receivedOnBehalfOf = isEmpty(receivedOnBehalfOf)?nil:receivedOnBehalfOf;
            tenantHanded = isEmpty(tenantHanded)?nil:tenantHanded;
            schemeId = isEmpty(schemeId)?nil:schemeId;
            blockId = isEmpty(blockId)?nil:blockId;
            heatingId = isEmpty(heatingId)?nil:heatingId;
            
            cp12Info.cp12Document = cp12Document;
            cp12Info.cp12Number = cp12Number;
            cp12Info.dateTimeStamp = dateTimeStamp;
            cp12Info.documentType = documentType;
            cp12Info.inspectionCarried =inspectionCarried ;
            cp12Info.issuedBy = issuedBy;
            cp12Info.issuedDate = issuedDate;
            cp12Info.issuedDateString = issuedDateString;
            cp12Info.journalId = journalId;
            cp12Info.jsgNumber = jsgNumber;
            cp12Info.lsgrid = lsgrid;
            cp12Info.notes = notes;
            cp12Info.propertyID = propertyID;
            cp12Info.receivedDate = receivedDate;
            cp12Info.receivedDateString = receivedDateString;
            cp12Info.receivedOnBehalfOf = receivedOnBehalfOf;
            cp12Info.tenantHanded = tenantHanded;
            cp12Info.heatingId = heatingId;
            cp12Info.blockId = blockId;
            cp12Info.schemeId = schemeId;
            //Set the Inverse Relationship
            
        }
        cp12Info.cp12InfoToAppointment = appointment;
        appointment.appointmentToCP12Info = cp12Info;
    }
    
}

#pragma mark - PropertyAttributeNotes

- (void) loadAppointmentPropertyAttributesNotes:(NSArray *)attributeNotesArray appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if(appointment && !isEmpty(attributeNotesArray))
    {
        for(NSDictionary *propertyNoteDictionary in attributeNotesArray)
        {
            if(!isEmpty(propertyNoteDictionary))
            {
                PropertyAttributesNotes * propertyNotes= [NSEntityDescription insertNewObjectForEntityForName:kPropertyAttributesNotes inManagedObjectContext:managedObjectContext];
                
                NSString *attributePath = [propertyNoteDictionary valueForKey:kAttributePath];
                NSString *attributeNote = [propertyNoteDictionary valueForKey:kAttributeNote];
                
                attributeNote = isEmpty(attributeNote)?nil:attributeNote;
                attributePath = isEmpty(attributePath)?nil:attributePath;
                
                propertyNotes.attributeNotes = attributeNote;
                propertyNotes.attributePath = attributePath;
                [appointment addAppointmentToPropertyAttributesNotesObject:propertyNotes];
            }
        }
        
    }
}

#pragma mark - CustomerList
- (void) loadAppointmentCustomerData:(NSArray *)customerList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(customerList))
	{
		for(NSDictionary *customerDictionary in customerList)
		{
			if(!isEmpty(customerDictionary))
			{
				Customer *customer = [NSEntityDescription insertNewObjectForEntityForName:kCustomer inManagedObjectContext:managedObjectContext];
				
				NSNumber * customerId = [customerDictionary valueForKey:kCustomerId];
				NSString * address = [customerDictionary valueForKey:kCustomerAddress];
				NSString * email = [customerDictionary valueForKey:kCustomerEmail];
				NSString * fax = [customerDictionary valueForKey:kCustomerFax];
				NSString * firstName = [customerDictionary valueForKey:kCustomerFirstName];
				NSString * lastName = [customerDictionary valueForKey:kCustomerLastName];
				NSString * middleName = [customerDictionary valueForKey:kCustomerMiddleName];
				NSString * mobile = [customerDictionary valueForKey:kCustomerMobile];
				NSString * property = [customerDictionary valueForKey:kCustomerProperty];
				NSString * telephone = [customerDictionary valueForKey:kCustomerTelephone];
				NSString * title = [customerDictionary valueForKey:kCustomerTitle];
				
				address = isEmpty(address)?nil:address;
				customerId = isEmpty(customerId)?nil:customerId;
				email = isEmpty(email)?nil:email;
				fax = isEmpty(fax)?nil:fax;
				firstName = isEmpty(firstName)?nil:firstName;
				lastName = isEmpty(lastName)?nil:lastName;
				middleName = isEmpty(middleName)?nil:middleName;
				mobile = isEmpty(mobile)?nil:mobile;
				property = isEmpty(property)?nil:property;
				telephone = isEmpty(telephone)?nil:telephone;
				title = isEmpty(title)?nil:title;
				
				customer.address = address;
				customer.customerId = customerId;
				customer.email = email;
				customer.fax = fax;
				customer.firstName = firstName;
				customer.lastName = lastName;
				customer.middleName = middleName;
				customer.mobile = mobile;
				customer.property = property;
				customer.telephone = telephone;
				customer.title = title;
				
				//Customer Releationsship Keys
				NSDictionary *customerAddressData = [customerDictionary valueForKey:@"Address"];
				[[PSDataPersistenceManager sharedManager] loadCustomerAddressData:customerAddressData customer:customer managedObjectContext:managedObjectContext];
				
				NSArray *customerRiskData = [customerDictionary valueForKey:@"customerRiskData"];
				[[PSDataPersistenceManager sharedManager] loadCustomerRiskData:customerRiskData customer:customer managedObjectContext:managedObjectContext];
				
				NSArray *customerVulnerabilityData = [customerDictionary valueForKey:@"customerVulnerabilityData"];
				[[PSDataPersistenceManager sharedManager] loadCustomerVulunarityData:customerVulnerabilityData customer:customer managedObjectContext:managedObjectContext];
				
				//Set the Inverse Relationship
				//customer.customerToAppointment = appointment;
				[appointment addAppointmentToCustomerObject:customer];
			}
		}
		
	}
}

- (void) loadCustomerAddressData:(NSDictionary *)customerAddressDataDictionary customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerAddressDataDictionary))
	{
		Address *customerAddress = [NSEntityDescription insertNewObjectForEntityForName:kAddress inManagedObjectContext:managedObjectContext];
		
		NSString * address = [customerAddressDataDictionary valueForKey:@"address"];
		NSNumber * addressID = [customerAddressDataDictionary valueForKey:@"addressID"];
		NSString * houseNo = [customerAddressDataDictionary valueForKey:@"houseNo"];
		NSString * postCode = [customerAddressDataDictionary valueForKey:@"postCode"];
		NSString * townCity = [customerAddressDataDictionary valueForKey:@"townCity"];
		
		
		address = isEmpty(address)?nil:address;
		addressID = isEmpty(addressID)?nil:addressID;
		houseNo = isEmpty(houseNo)?nil:houseNo;
		postCode = isEmpty(postCode)?nil:postCode;
		townCity = isEmpty(townCity)?nil:townCity;
		
		customerAddress.address = address;
		customerAddress.addressID = addressID;
		customerAddress.houseNo = houseNo;
		customerAddress.postCode = postCode;
		customerAddress.townCity = townCity;
		
		//Set the Inverse Relationship
		customerAddress.addressToCustomer = customer;
		customer.customerToAddress = customerAddress;
	}
}

- (void) loadCustomerRiskData:(NSArray *)customerRiskDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerRiskDataList))
	{
		for(NSDictionary *customerRiskDataDictionary in customerRiskDataList)
		{
			if(!isEmpty(customerRiskDataDictionary))
			{
				CustomerRiskData *customerRiskData = [NSEntityDescription insertNewObjectForEntityForName:kCustomerRiskData inManagedObjectContext:managedObjectContext];
				
				NSString * riskCatDesc = [customerRiskDataDictionary valueForKey:@"riskCatDesc"];;
				NSString * riskSubCatDesc = [customerRiskDataDictionary valueForKey:@"riskSubCatDesc"];
				
				riskCatDesc = isEmpty(riskCatDesc)?nil:riskCatDesc;
				riskSubCatDesc = isEmpty(riskSubCatDesc)?nil:riskSubCatDesc;
				
				customerRiskData.riskCatDesc = riskCatDesc;
				customerRiskData.riskSubCatDesc = riskSubCatDesc;
				
				customerRiskData.customerRiskDataToCustomer = customer;
				[customer addCustomerToCustomerRiskDataObject:customerRiskData];
			}
		}
	}
}

- (void) loadCustomerVulunarityData:(NSArray *)customerVulnerabilityDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerVulnerabilityDataList))
	{
		for (NSDictionary *customerVulnerabilityDataDictionary in customerVulnerabilityDataList)
		{
			if (!isEmpty(customerVulnerabilityDataDictionary)) {
				CustomerVulnerabilityData *customerVulnerabilityData = [NSEntityDescription insertNewObjectForEntityForName:kCustomerVulnerabilityData inManagedObjectContext:managedObjectContext];
				
				NSString * vulCatDesc = [customerVulnerabilityDataDictionary valueForKey:@"vulCatDesc"];
				NSNumber * vulHistoryId = [customerVulnerabilityDataDictionary valueForKey:@"vulHistoryId"];
				NSString * vulSubCatDesc = [customerVulnerabilityDataDictionary valueForKey:@"vulSubCatDesc"];
				
				vulCatDesc = isEmpty(vulCatDesc)?nil:vulCatDesc;
				vulHistoryId = isEmpty(vulHistoryId)?nil:vulHistoryId;
				vulSubCatDesc = isEmpty(vulSubCatDesc)?nil:vulSubCatDesc;
				
				customerVulnerabilityData.vulCatDesc = vulCatDesc;
				customerVulnerabilityData.vulHistoryId = vulHistoryId;
				customerVulnerabilityData.vulSubCatDesc = vulSubCatDesc;
				
				//Set the Inverse Relationship
				customerVulnerabilityData.customerVulunarityDataToCustomer = customer;
				[customer addCustomerToCustomerVulunarityDataObject:customerVulnerabilityData];
			}
		}
	}
}

#pragma mark - JobSheet
- (void) loadAppointmentJobSheet:(NSArray *)jobDataList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(jobDataList))
	{
		for(NSDictionary *jobDataDictionary in jobDataList)
		{
			if(!isEmpty(jobDataDictionary))
			{
				FaultJobSheet *jobData = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([FaultJobSheet class])
																															 inManagedObjectContext:managedObjectContext];
				NSDate * completionDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:@"completionDate"]];
				NSNumber * duration = [jobDataDictionary valueForKey:kDuration];
				NSString * durationUnit = [jobDataDictionary valueForKey:kDurationUnit];
				NSNumber * faultLogID = [jobDataDictionary valueForKey:kFaultLogID];
				NSString * followOnNotes = [jobDataDictionary valueForKey:kFollowOnNotes];
				NSString * jobStatus = [jobDataDictionary valueForKey:kJobStatus];
				
				NSString * jsnDescription = [jobDataDictionary valueForKey:kJSNDescription];
				NSString * jsnLocation = [jobDataDictionary valueForKey:kJSNLocation];
				NSString * jsnNotes = [jobDataDictionary valueForKey:kJSNNotes];
				NSString * jsNumber = [jobDataDictionary valueForKey:kJSNumber];
				NSString * jsType = [jobDataDictionary valueForKey:kJSType];
				NSString * priority = [jobDataDictionary valueForKey:kPriority];
				NSString * repairNotes = [jobDataDictionary valueForKey:kJobRepairNotes];
				NSDate * reportedDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kReportedDate]];
				NSString * responseTime = [jobDataDictionary valueForKey:kResponseTime];
                NSNumber *repairId = [jobDataDictionary valueForKey:kJSRepairId];
                
                
				jobData.completionDate = isEmpty(completionDate)?nil:completionDate;
				jobData.duration = isEmpty(duration)?nil:duration;
				jobData.faultLogID = isEmpty(faultLogID)?nil:faultLogID;
				jobData.followOnNotes = isEmpty(followOnNotes)?nil:followOnNotes;
				jobData.jobStatus = isEmpty(jobStatus)?nil:jobStatus;
				jobData.jsnDescription = isEmpty(jsnDescription)?nil:jsnDescription;
				jobData.jsnLocation = isEmpty(jsnLocation)?nil:jsnLocation;
				jobData.jsnNotes = isEmpty(jsnNotes)?nil:jsnNotes;
				jobData.jsNumber = isEmpty(jsNumber)?nil:jsNumber;
				jobData.jsType = isEmpty(jsType) ? nil : jsType;
				jobData.priority = isEmpty(priority)?nil:priority;
				jobData.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
				jobData.reportedDate = isEmpty(reportedDate)?nil:reportedDate;
				jobData.responseTime = isEmpty(responseTime)?nil:responseTime;
				jobData.durationUnit = isEmpty(durationUnit)?nil:durationUnit;
				jobData.repairId = isEmpty(repairId)?nil:repairId;
				//Job Data Relationship Keys
				NSArray *faultRepairDataList = [jobDataDictionary valueForKey:@"faultRepairList"];
				[self loadFaultRepairDataList:faultRepairDataList jobData:jobData managedObjectContext:managedObjectContext];
				NSArray *repairImages = [jobDataDictionary valueForKey:@"repairImageList"];
				if (!isEmpty(repairImages) && [repairImages count] > 0)
				{
					for (NSDictionary *repairImageDictionary in repairImages)
					{
						[self loadRepairPictureData:repairImageDictionary inContext:managedObjectContext];
					}
				}
				
				//Set the Inverse Relationship
				jobData.jobSheetToAppointment = appointment;
				[appointment addAppointmentToJobSheetObject:jobData];
			}
		}
	}
}
- (void) loadDefectAppointmentJobDataList:(NSArray *)jobDataList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(jobDataList))
	{
		for(NSDictionary *jobDataDictionary in jobDataList)
		{
			if(!isEmpty(jobDataDictionary))
			{
				DefectJobSheet * jobData = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([DefectJobSheet class])
																																 inManagedObjectContext:managedObjectContext];
				NSDate * completionDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kCompletionDate]];
				NSString * applianceName = [jobDataDictionary valueForKey:kApplianceName];
				NSNumber  *applianceId = [jobDataDictionary valueForKey:kDefectApplianceId];
				NSNumber  *boilerTypeId = [jobDataDictionary valueForKey:kDefectJobdataBoilerTypeId];
				NSNumber * defectCategoryId = [jobDataDictionary valueForKey:kDefectCategoryId];
				NSDate * defectDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kDefectDate]];
				NSNumber * defectId = [jobDataDictionary valueForKey:kDefectId];
				NSString * defectNotes = [jobDataDictionary valueForKey:kDefectNotes];
				NSNumber * defectRecordedBy = [jobDataDictionary valueForKey:kDefectRecordedBy];
				NSString * gasCouncilNumber = [jobDataDictionary valueForKey:kGasCouncilNumber];
				NSString * inspectionRef = [jobDataDictionary valueForKey:kInspectionRef];
				NSNumber * isAdviceNoteIssued = [jobDataDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber * isCustomerHaveHeating = [jobDataDictionary valueForKey:kIsCustomerHaveHeating];
				NSNumber * isCustomerHaveHotWater = [jobDataDictionary valueForKey:kIsCustomerHaveHotWater];
				NSNumber * isDisconnected = [jobDataDictionary valueForKey:kIsDisconnected];
				NSNumber * isHeatersLeft = [jobDataDictionary valueForKey:kIsHeatersLeft];
				NSNumber * isRemedialActionTaken = [jobDataDictionary valueForKey:kIsRemedialActionTaken];
				NSNumber * isTwoPersonsJob = [jobDataDictionary valueForKey:kIsTwoPersonsJob];
				NSString * make = [jobDataDictionary valueForKey:kApplianceMake];
				NSString * model = [jobDataDictionary valueForKey:kModel];
				NSNumber * numberOfHeatersLeft = [jobDataDictionary valueForKey:kNumberOfHeatersLeft];
				NSDate * partsDueDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kPartsDueDate]];
        NSString * partsLocation = [jobDataDictionary valueForKey:kPartsLocation];
				NSNumber * partsOrderedBy = [jobDataDictionary valueForKey:kPartsOrderedBy];
                NSNumber * heatingId = [jobDataDictionary valueForKey:kHeatingId];
				NSString * remedialNotes = [jobDataDictionary valueForKey:kRemedialActionNotes];
				NSString * serialNumber = [jobDataDictionary valueForKey:kSerialNumber];
				NSString * trade = [jobDataDictionary valueForKey:kTradeDescription];
				NSNumber * warningTagFixed = [jobDataDictionary valueForKey:kWarningTagFixed];
				NSString * jsNumber = [jobDataDictionary valueForKey:kJSNumber];
				NSString * jobStatus = [jobDataDictionary valueForKey:kJobStatus];
                NSString * defCompletionNotes = [jobDataDictionary valueForKey:kDefectCompletionNotes];
				jobData.jsNumber = isEmpty(jsNumber)?nil:jsNumber;
				jobData.jobStatus = isEmpty(jobStatus)?nil:jobStatus;
				jobData.appliance = isEmpty(applianceName)?nil:applianceName;
				jobData.applianceId = isEmpty(applianceId)?nil:applianceId;
				jobData.boilerTypeId = isEmpty(boilerTypeId)?nil:boilerTypeId;
                jobData.heatingId = isEmpty(heatingId)?nil:heatingId;
				jobData.defectCategoryId = isEmpty(defectCategoryId)?nil:defectCategoryId;
				jobData.defectDate = isEmpty(defectDate)?nil:defectDate;
				jobData.defectId = isEmpty(defectId)?nil:defectId;
				jobData.defectNotes = isEmpty(defectNotes)?nil:defectNotes;
				jobData.defectRecordedBy = isEmpty(defectRecordedBy)?nil:defectRecordedBy;
				jobData.gasCouncilNumber = isEmpty(gasCouncilNumber)?nil:gasCouncilNumber;
				jobData.inspectionRef = isEmpty(inspectionRef)?nil:inspectionRef;
				jobData.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				jobData.isCustomerHaveHeating = isEmpty(isCustomerHaveHeating)?nil:isCustomerHaveHeating;
				jobData.isCustomerHaveHotWater = isEmpty(isCustomerHaveHotWater)?nil:isCustomerHaveHotWater;
				jobData.isDisconnected = isEmpty(isDisconnected)?nil:isDisconnected;
				jobData.isHeatersLeft = isEmpty(isHeatersLeft)?nil:isHeatersLeft;
				jobData.isRemedialActionTaken = isEmpty(isRemedialActionTaken)?nil:isRemedialActionTaken;
				jobData.isTwoPersonsJob = isEmpty(isTwoPersonsJob)?nil:isTwoPersonsJob;
				jobData.make = isEmpty(make)?nil:make;
				jobData.model = isEmpty(model)?nil:model;
                jobData.defectCompletionNotes = isEmpty(defCompletionNotes)?nil:defCompletionNotes;
				jobData.numberOfHeatersLeft = isEmpty(numberOfHeatersLeft)?nil:numberOfHeatersLeft;
				jobData.partsDueDate = isEmpty(partsDueDate)?nil:partsDueDate;
				jobData.partsLocation = isEmpty(partsLocation)?nil:partsLocation;
				jobData.partsOrderedBy = isEmpty(partsOrderedBy)?nil:partsOrderedBy;
				jobData.remedialActionNotes = isEmpty(remedialNotes)?nil:remedialNotes;
				jobData.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
				jobData.trade = isEmpty(trade)?nil:trade;
				jobData.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				
				//Job Data Relationship Keys
				NSArray *jobsheetImages = [jobDataDictionary valueForKey:@"jobSheetImageList"];
				if (isEmpty(jobsheetImages)==NO && [jobsheetImages count] > 0)
				{
					for (NSDictionary *jobImageDictionary in jobsheetImages)
					{
						[self loadRepairPictureData:jobImageDictionary inContext:managedObjectContext];
					}
				}
				
				//Job Activity Data Relationship Keys
				NSArray *jobActivityHistory = [jobDataDictionary valueForKey:@"jobActivityHistory"];
				if (isEmpty(jobActivityHistory)==NO && [jobActivityHistory count] > 0)
				{
					for (NSDictionary *jobActivity in jobActivityHistory)
					{
						[self loadJobActivityData:jobActivity forJobSheet:jobData inContext:managedObjectContext];
					}
				}
				
				//Set the Inverse Relationship
				jobData.jobSheetToAppointment = appointment;
				[appointment addAppointmentToJobSheetObject:jobData];
			}
		}
	}
}


#pragma mark - PlannedTradeComponent
- (void) loadAppointmentTradeComponent:(NSDictionary *)jobDataDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(jobDataDictionary))
	{
		if(!isEmpty(jobDataDictionary))
		{
			PlannedTradeComponent *jobData = [NSEntityDescription insertNewObjectForEntityForName:kTradeComponent inManagedObjectContext:managedObjectContext];
			
			NSString * jsNumber = [jobDataDictionary valueForKey:kJSNumber];
			NSString * jsnNotes = [jobDataDictionary valueForKey:kJSNNotes];
			NSString * pmoDescription = [jobDataDictionary valueForKey:kPMODescription];
			NSNumber * componentTradeId = [jobDataDictionary valueForKey:kComponentTradeId];
			NSDate * completionDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kCompletionDate]];
			NSNumber * componentId = [jobDataDictionary valueForKey:kComponentId];
			NSString * componentName = [jobDataDictionary valueForKey:kComponentName];
			NSString * jobStatus = [jobDataDictionary valueForKey:kJobStatus];
			NSNumber * duration = [jobDataDictionary valueForKey:kDuration];
			NSString * durationUnit = [jobDataDictionary valueForKey:kDurationUnit];
			NSDate * reportedDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kReportedDate]];
			NSNumber * sortingOrder = [jobDataDictionary valueForKey:kSortingOrder];
			NSString * tradeDescription = [jobDataDictionary valueForKey:kTradeDescription];
			NSNumber * tradeId = [jobDataDictionary valueForKey:kTradeId];
			NSString * location = [jobDataDictionary valueForKey:kLocation];
			NSNumber * locationId = [jobDataDictionary valueForKey:kLocationId];
			NSString * adaptation = [jobDataDictionary valueForKey:kAdaptation];
			NSNumber * adaptationId = [jobDataDictionary valueForKey:kAdaptationId];
			
			jobData.jsNumber = isEmpty(jsNumber)?nil:jsNumber;
			jobData.jsnNotes = isEmpty(jsnNotes)?nil:jsnNotes;
			jobData.pmoDescription = isEmpty(pmoDescription)?nil:pmoDescription;
			jobData.componentTradeId = isEmpty(componentTradeId)?nil:componentTradeId;
			jobData.completionDate = isEmpty(completionDate)?nil:completionDate;
			jobData.componentId = isEmpty(componentId)?nil:componentId;
			jobData.componentName = isEmpty(componentName)?nil:componentName;
			jobData.duration = isEmpty(duration)?nil:duration;
			jobData.durationUnit = isEmpty(durationUnit)?nil:durationUnit;
			jobData.reportedDate = isEmpty(reportedDate)?nil:reportedDate;
			jobData.sortingOrder = isEmpty(sortingOrder)?nil:sortingOrder;
			jobData.tradeDescription = isEmpty(tradeDescription)?nil:tradeDescription;
			jobData.tradeId = isEmpty(tradeId)?nil:tradeId;
			jobData.location = isEmpty(location)?nil:location;
			jobData.locationId = isEmpty(locationId)?nil:locationId;
			jobData.adaptation = isEmpty(adaptation)?nil:adaptation;
			jobData.adaptationId = isEmpty(adaptationId)?nil:adaptationId;
			
			jobData.jobStatus= isEmpty(jobStatus)?nil:jobStatus;
			
			//Set the Inverse Relationship
			jobData.tradeComponentToAppointment = appointment;
			appointment.appointmentToPlannedComponent = jobData;
			
		}
	}
}


- (void) loadFaultRepairDataList:(NSArray *)faultRepairDataList jobData:(FaultJobSheet *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(jobData && !isEmpty(faultRepairDataList))
	{
		for(NSDictionary *faultRepairDictionary in faultRepairDataList)
		{
			if(!isEmpty(faultRepairDictionary))
			{
				/* FaultRepairData *faultRepairData = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];*/
				NSString * faultRepairDescription = [faultRepairDictionary valueForKey:kFaultRepairDescription];
				NSNumber * faultRepairID = [faultRepairDictionary valueForKey:kFaultRepairID];
				
				FaultRepairData *faultRepairData = [[PSCoreDataManager sharedManager]faultRepairDataWithID:faultRepairID inContext:managedObjectContext];
				if (faultRepairData != nil)
				{
					faultRepairData = (FaultRepairData *)[managedObjectContext objectWithID:faultRepairData.objectID];
				}
				else
				{
					faultRepairData = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];
				}
				
				
				
				
				faultRepairData.faultRepairDescription = isEmpty([faultRepairDescription trimWhitespace])?nil:[faultRepairDescription trimWhitespace];
				faultRepairData.faultRepairID = isEmpty(faultRepairID)?nil:faultRepairID;
				faultRepairData.repairSectionIdentifier = [faultRepairData repairSectionIdentifier];
				
				//Set the Inverse Relationship
				faultRepairData.faultRepairDataToFaultJobSheet = jobData;
				[jobData addFaultRepairDataListObject:faultRepairData];
			}
		}
	}
}

#pragma mark - JournalData
- (void) loadAppointmentJournalData:(NSDictionary *)journalData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(journalData))
	{
		Journal *journal = [NSEntityDescription insertNewObjectForEntityForName:kJournal inManagedObjectContext:managedObjectContext];
		
		NSNumber * actionId = [journalData valueForKey:@"actionId"];
		NSNumber * creationBy = [journalData valueForKey:@"creationBy"];
		NSDate * creationDate = [UtilityClass convertServerDateToNSDate:[journalData valueForKey:@"creationDate"]];
		NSNumber * inspectionTypeId = [journalData valueForKey:@"inspectionTypeId"];
		NSNumber * isCurrent = [journalData valueForKey:@"isCurrent"];
		NSNumber * journalId = [journalData valueForKey:@"journalId"];
		NSString * propertyId = [journalData valueForKey:kPropertyId];
		NSNumber * statusId = [journalData valueForKey:@"statusId"];
        NSNumber *schemeId = [journalData valueForKey:kSchemeID];
        NSNumber *blockId = [journalData valueForKey:kSchemeBlockID];
        NSNumber *journalHistoryId = [journalData valueForKey:kJournalHistoryId];
		
		journal.actionId = isEmpty(actionId)?nil:actionId;
		journal.creationBy = isEmpty(creationBy)?nil:creationBy;
		journal.creationDate = isEmpty(creationDate)?nil:creationDate;
		journal.inspectionTypeId = isEmpty(inspectionTypeId)?nil:inspectionTypeId;
		journal.isCurrent = isEmpty(isCurrent)?nil:isCurrent;
		journal.journalId = isEmpty(journalId)?nil:journalId;
		journal.propertyId = isEmpty(propertyId)?nil:propertyId;
		journal.statusId = isEmpty(statusId)?nil:statusId;
		journal.schemeId = isEmpty(schemeId)?nil:schemeId;
        journal.blockId = isEmpty(blockId)?nil:blockId;
        journal.journalHistoryId = isEmpty(journalHistoryId)?nil:journalHistoryId;
		//Set the Inverse Relationship
		journal.journalToAppointment = appointment;
		appointment.appointmentToJournal = journal;
	}
}
#pragma mark - SchemeData
- (void) loadAppointmentSchemeData:(NSDictionary *)schemeData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(schemeData))
	{
		Scheme *scheme = [NSEntityDescription insertNewObjectForEntityForName:kScheme inManagedObjectContext:managedObjectContext];
		
		NSNumber * schemeID = [schemeData valueForKey:kSchemeID];
		NSString * county = [schemeData valueForKey:kSchemeCounty];
		NSString * townCity = [schemeData valueForKey:kSchemCityTown];
		NSNumber * blockId = [schemeData valueForKey:kSchemeBlockID];
		NSString * postCode = [schemeData valueForKey:kSchemePostCode];
		NSString * schemeName = [schemeData valueForKey:kSchemeName];
		NSString * blockName = [schemeData valueForKey:kSchemeBlockName];
		
		scheme.schemeId = isEmpty(schemeID)?nil:schemeID;
		scheme.blockId = isEmpty(blockId)?nil:blockId;
		scheme.schemeName = isEmpty(schemeName)?nil:schemeName;
		scheme.postCode = isEmpty(postCode)?nil:postCode;
		scheme.blockName = isEmpty(blockName)?nil:blockName;
		scheme.county = isEmpty(county)?nil:county;
		scheme.townCity = isEmpty(townCity)?nil:townCity;
		
		NSArray *schemeAsbestosDataList = [schemeData valueForKey:@"schemeBlockAsbestos"];
		[[PSDataPersistenceManager sharedManager] loadSchemeAsbestosData:schemeAsbestosDataList scheme:scheme managedObjectContext:managedObjectContext];
		
		//Set the Inverse Relationship
		scheme.schemeToAppointment = appointment;
		appointment.appointmentToScheme = scheme;
	}
}
#pragma mark - PropertyData
- (void) loadAppointmentPropertyData:(NSDictionary *)propertyData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(propertyData))
	{
		Property *property = [NSEntityDescription insertNewObjectForEntityForName:kProperty inManagedObjectContext:managedObjectContext];
		
		NSString * propertyId = [propertyData valueForKey:kPropertyId];
		NSString * address1 = [propertyData valueForKey:kAddress1];
		NSString * address2 = [propertyData valueForKey:kAddress2];
		NSString * address3 = [propertyData valueForKey:kAddress3];
		NSDate * certificateExpiry = [UtilityClass convertServerDateToNSDate:[propertyData valueForKey:kCertificateExpiry]];
		NSString * county = [propertyData valueForKey:kCounty];
		NSString * flatNumber = [propertyData valueForKey:kFlatNumber];
		NSString * houseNumber = [propertyData valueForKey:kHouseNumber];
		NSDate * lastSurveyDate = [UtilityClass convertServerDateToNSDate:[propertyData valueForKey:kLastSurveyDate]];
		NSString * postCode = [propertyData valueForKey:kPostCode];
		
		NSNumber * tenancyId = [propertyData valueForKey:kTenancyId];
		NSString * townCity = [propertyData valueForKey:kTownCity];
        
        NSNumber *schemeId = [propertyData valueForKey:kSchemeID];
        NSNumber *blockId = [propertyData valueForKey:kSchemeBlockID];
        NSString *schemeName = [propertyData valueForKey:kSchemeName];
        NSString *blockName = [propertyData valueForKey:kSchemeBlockName];
		
		property.address1 = isEmpty(address1)?nil:address1;
		property.address2 = isEmpty(address2)?nil:address2;
		property.address3 = isEmpty(address3)?nil:address3;
		property.certificateExpiry = isEmpty(certificateExpiry)?nil:certificateExpiry;
		property.county = isEmpty(county)?nil:county;
		property.flatNumber = isEmpty(flatNumber)?nil:flatNumber;
		property.houseNumber = isEmpty(houseNumber)?nil:houseNumber;
		property.lastSurveyDate = isEmpty(lastSurveyDate)?nil:lastSurveyDate;
		property.postCode = isEmpty(postCode)?nil:postCode;
		property.propertyId = isEmpty(propertyId)?nil:propertyId;
		property.tenancyId = isEmpty(tenancyId)?nil:tenancyId;
		property.townCity = isEmpty(townCity)?nil:townCity;
		property.schemeId = isEmpty(schemeId)?nil:schemeId;
        property.blockId = isEmpty(blockId)?nil:blockId;
        property.schemeName = isEmpty(schemeName)?nil:schemeName;
        property.blockName = isEmpty(blockName)?nil:blockName;
		//Property Releationsship Keys
		NSArray *propertyAccomodationsList = [propertyData valueForKey:@"Accommodations"];
		[[PSDataPersistenceManager sharedManager] loadPropertyAccomodationsData:propertyAccomodationsList property:property managedObjectContext:managedObjectContext];
		
		NSArray *propertyAsbestosDataList = [propertyData valueForKey:@"propertyAsbestosData"];
		[[PSDataPersistenceManager sharedManager] loadPropertyAsbestosData:propertyAsbestosDataList property:property managedObjectContext:managedObjectContext];
		
		NSNumber * defaultPicId=[propertyData valueForKey:@"defaultPropertyPicId"];
		
		NSArray *propertyPicturesList = [propertyData valueForKey:@"propertyPicture"];
		[[PSDataPersistenceManager sharedManager] loadPropertyPicturesData:propertyPicturesList property:property defaultPictureId:defaultPicId managedObjectContext:managedObjectContext];
        //Set the Inverse Relationship
        property.propertyToAppointment = appointment;
        appointment.appointmentToProperty = property;
		/*
		 Setup default pic to property default pic
		 */
		
		
        
		
		/*NSMutableDictionary *installationPipework = [propertyData valueForKey:@"installationpipework"];
		
		NSString *dateString = [installationPipework valueForKey:kInspectionDate];
		if (!isEmpty(dateString))
		{
			NSDate *date = [UtilityClass convertServerDateToNSDate:dateString];
			[installationPipework setObject:date forKey:kInspectionDate];
		}
		[[PSDataPersistenceManager sharedManager]loadPipeworkForm:installationPipework forProperty:property managedObjectContext:managedObjectContext];
		*/
	}
}

- (void) loadPropertyAccomodationsData:(NSArray *)propertyAccomodationsList property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyAccomodationsList))
	{
		for(NSDictionary *propertyAccomodationsDictionary in propertyAccomodationsList)
		{
			if(!isEmpty(propertyAccomodationsDictionary))
			{
				Accomodation *accomodation = [NSEntityDescription insertNewObjectForEntityForName:kAccomodation inManagedObjectContext:managedObjectContext];
				
				NSString *propertyId = [propertyAccomodationsDictionary valueForKey:kPropertyId];
				NSString *roomName = [propertyAccomodationsDictionary valueForKey:kRoomName];
				NSNumber *roomHeight = [propertyAccomodationsDictionary valueForKey:kRoomHeight];
				NSNumber *roomWidth = [propertyAccomodationsDictionary valueForKey:kRoomWidth];
				NSNumber *roomLength = [propertyAccomodationsDictionary valueForKey:kRoomLength];
				NSNumber *updatedBy = [propertyAccomodationsDictionary valueForKey:kUpdatedBy];
				//NSDate *updatedOn = [propertyAccomodationsDictionary valueForKey:kUpdatedOn];
				NSDate *updatedOn = [UtilityClass convertServerDateToNSDate:[propertyAccomodationsDictionary valueForKey:kUpdatedOn]];
				accomodation.propertyId = isEmpty(propertyId)?nil:propertyId;
				accomodation.roomHeight = isEmpty(roomHeight)?nil:roomHeight;
				accomodation.roomLength = isEmpty(roomLength)?nil:roomLength;
				accomodation.roomName = isEmpty(roomName)?nil:roomName;
				accomodation.roomWidth = isEmpty(roomWidth)?nil:roomWidth;
				accomodation.updatedBy = isEmpty(updatedBy)?nil:updatedBy;
				accomodation.updatedOn = isEmpty(updatedOn)?nil:updatedOn;
				
				
				
				//Set the Inverse Relationship
				accomodation.accomodationToProperty = property;
				[property addPropertyToAccomodationObject:accomodation];
			}
		}
	}
}

- (void) loadPropertyAsbestosData:(NSArray *)propertyAsbestosDataList property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyAsbestosDataList))
	{
		for(NSDictionary *propertyAsbestosDataDictionary in propertyAsbestosDataList)
		{
			if(!isEmpty(propertyAsbestosDataDictionary))
			{
				PropertyAsbestosData *propertyAsbestosData = [NSEntityDescription insertNewObjectForEntityForName:kPropertyAsbestosData inManagedObjectContext:managedObjectContext];
				
				NSNumber * asbestosId = [propertyAsbestosDataDictionary valueForKey:@"asbestosId"];
				NSString * asbRiskLevelDesc = [propertyAsbestosDataDictionary valueForKey:@"asbRiskLevelDesc"];
				NSString * riskDesc = [propertyAsbestosDataDictionary valueForKey:@"riskDesc"];
				NSString * type = [propertyAsbestosDataDictionary valueForKey:@"type"];
				NSString * riskLevel = [propertyAsbestosDataDictionary valueForKey:@"riskLevel"];
				
				propertyAsbestosData.asbestosId = isEmpty(asbestosId)?nil:asbestosId;
				propertyAsbestosData.asbRiskLevelDesc = isEmpty(asbRiskLevelDesc)?nil:asbRiskLevelDesc;
				propertyAsbestosData.riskDesc = isEmpty(riskDesc)?nil:riskDesc;
				propertyAsbestosData.type = isEmpty(type)?nil:type;
				propertyAsbestosData.riskLevel = isEmpty(riskLevel)?nil:riskLevel;
				
				//Set the Inverse Relationship
				propertyAsbestosData.propertyAsbestosDataToProperty = property;
				[property addPropertyToPropertyAsbestosDataObject:propertyAsbestosData];
			}
		}
	}
}

- (void) loadSchemeAsbestosData:(NSArray *)schemeAsbestosDataList scheme:(Scheme *)scheme managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(scheme && !isEmpty(schemeAsbestosDataList))
	{
		for(NSDictionary *schemeAsbestosDataDictionary in schemeAsbestosDataList)
		{
			if(!isEmpty(schemeAsbestosDataDictionary))
			{
				SchemeAsbestosData *schemeAsbestosData = [NSEntityDescription insertNewObjectForEntityForName:kSchemeAsbestosData inManagedObjectContext:managedObjectContext];
				
				NSNumber * asbestosId = [schemeAsbestosDataDictionary valueForKey:@"asbestosId"];
				NSString * asbRiskLevelDesc = [schemeAsbestosDataDictionary valueForKey:@"asbRiskLevelDesc"];
				NSString * riskDesc = [schemeAsbestosDataDictionary valueForKey:@"riskDesc"];
				NSString * type = [schemeAsbestosDataDictionary valueForKey:@"type"];
				NSString * riskLevel = [schemeAsbestosDataDictionary valueForKey:@"riskLevel"];
				
				schemeAsbestosData.asbestosId = isEmpty(asbestosId)?nil:asbestosId;
				schemeAsbestosData.asbRiskLevelDesc = isEmpty(asbRiskLevelDesc)?nil:asbRiskLevelDesc;
				schemeAsbestosData.riskDesc = isEmpty(riskDesc)?nil:riskDesc;
				schemeAsbestosData.type = isEmpty(type)?nil:type;
				schemeAsbestosData.riskLevel = isEmpty(riskLevel)?nil:riskLevel;
				
				//Set the Inverse Relationship
				schemeAsbestosData.schemeAsbestosDataToScheme = scheme;
				[scheme addSchemeToSchemeAsbestosDataObject:schemeAsbestosData];
			}
		}
	}
}

- (void) loadPropertyPicturesData:(NSArray *)propertyPicturesList property:(Property *)property defaultPictureId:(NSNumber*) defaultPictureId managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyPicturesList))
	{
		
		for(NSDictionary *propertyPictureDictionary in propertyPicturesList)
		{
			if(!isEmpty(propertyPictureDictionary))
			{
				PropertyPicture *   propertyPicture = [NSEntityDescription insertNewObjectForEntityForName:kPropertyPicture inManagedObjectContext:managedObjectContext];
				
				NSNumber * propertyPictureId = [propertyPictureDictionary valueForKey:@"propertyPictureId"];
				NSNumber * appointmentId = [propertyPictureDictionary valueForKey:@"appointmentId"];
				NSNumber * createdBy = [propertyPictureDictionary valueForKey:@"createdBy"];
				NSDate *createdOn = [UtilityClass convertServerDateToNSDate:[propertyPictureDictionary valueForKey:@"createdOn"]];
				NSString * imagePath = [propertyPictureDictionary valueForKey:@"imagePath"];
				
				if (!isEmpty(defaultPictureId) && [defaultPictureId integerValue]==[propertyPictureId integerValue]) {
					
					property.defaultPicture=propertyPicture;
					propertyPicture.isDefault=[NSNumber numberWithBool:YES];
				}
				else
				{
					propertyPicture.isDefault = [NSNumber numberWithBool:NO];
				}
				NSNumber * itemId = [propertyPictureDictionary valueForKey:@"itemId"];
				NSString * propertyId = [propertyPictureDictionary valueForKey:kPropertyId];
				NSString * propertyPictureName = [propertyPictureDictionary valueForKey:@"propertyPictureName"];
				NSNumber * surveyId = [propertyPictureDictionary valueForKey:@"surveyId"];
				
				propertyPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
				
				propertyPicture.appointmentId = isEmpty(appointmentId)?nil:appointmentId;
				propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
				propertyPicture.createdOn = isEmpty(createdOn)?nil:createdOn;
				propertyPicture.itemId = isEmpty(itemId)?nil:itemId;
				propertyPicture.propertyId = isEmpty(propertyId)?nil:propertyId;
				propertyPicture.propertyPictureId = isEmpty(propertyPictureId)?nil:propertyPictureId;
				propertyPicture.propertyPictureName = isEmpty(propertyPictureName)?nil:propertyPictureName;
				propertyPicture.surveyId = isEmpty(surveyId)?nil:surveyId;
				
				//Set the Inverse Relationship
				propertyPicture.propertyPictureToProperty = property;
				[property addPropertyToPropertyPictureObject:propertyPicture];
			}
		}
	}
}

#pragma mark - Appliances Data

- (void) deleteAppliance:(NSDictionary *)appliancesData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(appliancesData))
	{
			NSNumber * applianceId = [appliancesData valueForKey:kApplianceId];
			NSNumber * isActive = [appliancesData valueForKey:kApplianceIsActive];
			NSManagedObjectID *applianceObjectId = [appliancesData valueForKey:@"applianceObjectId"];
		
			Appliance *appliance = nil;
			
		  if (!isEmpty(applianceObjectId))
		 {
			  appliance = (Appliance *)[managedObjectContext objectWithID:applianceObjectId];
		 }

			if(appliance != nil)
			{
				appliance.isActive = [NSNumber numberWithBool:NO];
			}
	}
	
}

- (void) loadApplianceData:(NSArray *)appliancesData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline
{
	if(!isEmpty(appliancesData))
	{
		
		for (NSDictionary * applianceDictionary in appliancesData)
		{
			
			NSDate * installedDate = [UtilityClass convertServerDateToNSDate:[applianceDictionary valueForKey:kApplianceInstalledDate]];
			
			NSDate * replacementDate = [UtilityClass convertServerDateToNSDate:[applianceDictionary valueForKey:kApplianceReplacementDate]];
			
			NSDictionary *locationDictionary = [applianceDictionary valueForKey:kApplianceLocation];
			NSDictionary *manufecturerDictionary = [applianceDictionary valueForKey:kApplianceManufacturer];
			NSDictionary *modelDictionary = [applianceDictionary valueForKey:kApplianceModel];
			NSDictionary *typeDictionary = [applianceDictionary valueForKey:kApplianceType];
			NSDictionary *applianceInspectionDictionary = [applianceDictionary valueForKey:kApplianceInspection];
			NSArray* applianceDefectsArray = [applianceDictionary valueForKey:kApplianceDefects];
			
			NSArray *applianceLocations = @[locationDictionary];
			NSArray *applianceManufacturers = @[manufecturerDictionary];
			NSArray *applianceModels = @[modelDictionary];
			NSArray *applianceTypes =@[typeDictionary];
			
			NSNumber * applianceId = [applianceDictionary valueForKey:kApplianceId];
			NSString * fluType = [applianceDictionary valueForKey:kApplianceFlueType];
			NSString * gcNumber = [applianceDictionary valueForKey:kApplianceGCNumber];
			NSString * propertyId = [applianceDictionary valueForKey:kPropertyId];
			NSNumber * isInspected = [applianceDictionary valueForKey:kApplianceIsInspected];
			NSNumber * isLandlordAppliance = [applianceDictionary valueForKey:kApplianceIsLandlordAppliance];
			NSNumber * isActive = [applianceDictionary valueForKey:kApplianceIsActive];
			NSString * serialNumber = [applianceDictionary valueForKey:kApplianceSerialNumber];
			NSManagedObjectID *applianceObjectId = [applianceDictionary valueForKey:@"applianceObjectId"];
            NSNumber *schemeId = [applianceDictionary valueForKey:kSchemeID];
            NSNumber *blockId = [applianceDictionary valueForKey:kSchemeBlockID];
            
			
			CLS_LOG(@"----------------------------------------------------------------");
			CLS_LOG(@"applianceObjectId %@",applianceObjectId);
			CLS_LOG(@"----------------------------------------------------------------");
			
			CLS_LOG(@"applianceId %@",applianceId);
			CLS_LOG(@"propertyId %@",propertyId);
			CLS_LOG(@"isInspected %@",isInspected);
			CLS_LOG(@"isLandlordAppliance %@",isLandlordAppliance);
			
			Appliance *appliance = nil;//[[PSCoreDataManager sharedManager] applianceWithIdentifier:applianceId inContext:managedObjectContext];
			
			if(offline) {
				if (!isEmpty(applianceObjectId))
				{
					appliance = (Appliance *)[managedObjectContext objectWithID:applianceObjectId];
				}
			}
			else
			{
							appliance = [[PSCoreDataManager sharedManager] findRecordFrom:kAppliance Field:@"applianceID" WithValue:applianceId context:managedObjectContext];
			}
			
			if(appliance != nil)
			{
				//appliance = (Appliance *)[managedObjectContext objectWithID:appliance.objectID];
				CLS_LOG(@"Appliance UPDATED");
			}
			else
			{
				appliance = [NSEntityDescription insertNewObjectForEntityForName:kAppliance inManagedObjectContext:managedObjectContext];
				CLS_LOG(@"New Appliance ADDED");
			}
			
			appliance.applianceID = isEmpty(applianceId)?nil:applianceId;
			appliance.fluType = isEmpty(fluType)?nil:fluType;
			appliance.gcNumber = isEmpty(gcNumber)?nil:gcNumber;
			appliance.isInspected = isEmpty(isInspected)?nil:isInspected;
			appliance.isLandlordAppliance = isEmpty(isLandlordAppliance)?nil:isLandlordAppliance;
			appliance.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
			appliance.propertyID = isEmpty(propertyId)?nil:propertyId;
			appliance.installedDate = isEmpty(installedDate)?nil:installedDate;
			appliance.replacementDate = isEmpty(replacementDate)?nil:replacementDate;
			appliance.isActive = isEmpty(isActive)?nil:isActive;
            appliance.schemeId = isEmpty(schemeId)?nil:schemeId;
            appliance.blockId = isEmpty(blockId)?nil:blockId;
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:applianceLocations appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:applianceManufacturers appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:applianceModels appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager]loadApplianceTypeData:applianceTypes appliance:appliance managedObjectContext:managedObjectContext];
			
			[[PSDataPersistenceManager sharedManager]loadApplianceDefectsData:applianceDefectsArray managedObjectContext:managedObjectContext];
			
			if ([appliance.isInspected boolValue])
			{
				CLS_LOG(@"appliance.isInspected TRUE");
				[[PSDataPersistenceManager sharedManager]loadInspectionForm:applianceInspectionDictionary forAppliance:appliance managedObjectContext:managedObjectContext];
			}
			else {
				appliance.applianceInspection = nil;
			}
			//Set the Inverse Relationship
			if (!isEmpty(property))
			{
				appliance.applianceToProperty = property;
				[property addPropertyToAppliancesObject:appliance];
			}
		}
	}
	
}

- (void) loadApplianceLocationData:(NSArray *)applianceLocationData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceLocationData))
	{
		for(NSDictionary *applianceLocationDictionary in applianceLocationData)
		{
			if(!isEmpty(applianceLocationDictionary))
			{
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceLocationDictionary);
				
				NSString * locationName = [applianceLocationDictionary valueForKey:kApplianceLocationName];
				
				if(!isEmpty(locationName)) {
					//CLS_LOG(@"locationName --------------(%@)-----------%d",locationName,[locationName length]);
					locationName = [locationName trimString];
					//CLS_LOG(@"locationName trim --------------(%@)-----------%d",locationName,[locationName length]);
				}
				
				NSNumber * locationID = [applianceLocationDictionary valueForKey:kApplianceLocationID];
				if (!isEmpty(locationName))
				{
					ApplianceLocation *applianceLocation = [[PSCoreDataManager sharedManager] applianceLocationWithName:locationName inContext:managedObjectContext] ;
					//                    if ([[locationName lowercaseString] isEqualToString:@"kitchen"]) {
					//                        CLS_LOG(@"--------------%@-----------%@",locationName,locationID);
					//                    }
					if(applianceLocation != nil)
					{
						//applianceLocation = (ApplianceLocation *)[managedObjectContext objectWithID:applianceLocation.objectID];
						//CLS_LOG(@"UPDATE --------------%@-----------%@",locationName,locationID);
						
					}
					else
					{
						applianceLocation = [NSEntityDescription insertNewObjectForEntityForName:kApplianceLocation inManagedObjectContext:managedObjectContext];
						//CLS_LOG(@"INSERT --------------%@-----------%@",locationName,locationID);
					}
					
					applianceLocation.locationName = isEmpty(locationName)?nil:locationName;
					applianceLocation.locationID = isEmpty(locationID)?nil:locationID;
					
					if (appliance != nil)
					{
						appliance.applianceLocation = applianceLocation;
					}
				}
			}
		}
		//CLS_LOG(@"loadApplianceLocationData --------------End ");
	}
}

- (void) loadApplianceManufacturerData:(NSArray *)applianceManufacturerData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceManufacturerData))
	{
		for(NSDictionary *applianceManufacturerDictionary in applianceManufacturerData)
		{
			if(!isEmpty(applianceManufacturerDictionary))
			{
				
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceManufacturerDictionary);
				
				NSString * manufacturerName = [applianceManufacturerDictionary valueForKey:kApplianceManufacturerName];
				
				if(!isEmpty(manufacturerName)) {
					//CLS_LOG(@"manufacturerName --------------(%@)-----------%d",manufacturerName,[manufacturerName length]);
					manufacturerName = [manufacturerName trimString];
					//CLS_LOG(@"manufacturerName trim --------------(%@)-----------%d",manufacturerName,[manufacturerName length]);
				}
				
				NSNumber * manufacturerID = [applianceManufacturerDictionary valueForKey:kApplianceManufacturerID];
				if (!isEmpty(manufacturerName))
				{
					ApplianceManufacturer *applianceManufacturer = [[PSCoreDataManager sharedManager] applianceManufacturerWithName:manufacturerName inContext:managedObjectContext];
					if(applianceManufacturer != nil)
					{
						//applianceManufacturer = (ApplianceManufacturer *)[managedObjectContext objectWithID:applianceManufacturer.objectID];
					}
					else
					{
						applianceManufacturer = [NSEntityDescription insertNewObjectForEntityForName:kApplianceManufacturer inManagedObjectContext:managedObjectContext];
					}
					
					applianceManufacturer.manufacturerName = isEmpty(manufacturerName)?nil:manufacturerName;
					applianceManufacturer.manufacturerID = isEmpty(manufacturerID)?nil:manufacturerID;
					if (appliance != Nil && applianceManufacturer!=Nil) {
						appliance.applianceManufacturer = applianceManufacturer;
					}
				}
			}
		}
	}
}

- (void) loadApplianceModelData:(NSArray *)applianceModelData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceModelData))
	{
		for(NSDictionary *applianceModelDictionary in applianceModelData)
		{
			if(!isEmpty(applianceModelDictionary))
			{
				
				
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceModelDictionary);
				
				NSString * modelName = [applianceModelDictionary valueForKey:kApplianceModelName];
				
				if(!isEmpty(modelName)) {
					//CLS_LOG(@"modelName --------------(%@)-----------%d",modelName,[modelName length]);
					modelName = [modelName trimString];
					//CLS_LOG(@"modelName trim --------------(%@)-----------%d",modelName,[modelName length]);
				}
				
				NSNumber * modelID = [applianceModelDictionary valueForKey:kApplianceModelID];
				if (!isEmpty(modelName))
				{
					ApplianceModel *applianceModel = [[PSCoreDataManager sharedManager] applianceModelWithName:modelName inContext:managedObjectContext];
					if(applianceModel != nil)
					{
						//applianceModel = (ApplianceModel *)[managedObjectContext objectWithID:applianceModel.objectID];
					}
					else
					{
						applianceModel = [NSEntityDescription insertNewObjectForEntityForName:kApplianceModel inManagedObjectContext:managedObjectContext];
					}
					
					applianceModel.modelName = isEmpty(modelName)?nil:modelName;
					applianceModel.modelID = isEmpty(modelID)?nil:modelID;
					if (appliance != nil) {
						appliance.applianceModel = applianceModel;
					}
				}
			}
		}
	}
}

- (void) loadApplianceTypeData:(NSArray *)applianceTypeData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceTypeData))
	{
		for(NSDictionary *applianceTypeDictionary in applianceTypeData)
		{
			if(!isEmpty(applianceTypeDictionary))
			{
				//CLS_LOG(@"applianceTypeDictionary --------------%@",applianceTypeDictionary);
				NSString * typeName = [applianceTypeDictionary valueForKey:kApplianceTypeName];
				
				if(!isEmpty(typeName)) {
					//CLS_LOG(@"typeName --------------(%@)-----------%d",typeName,[typeName length]);
					typeName = [typeName trimString];
					//CLS_LOG(@"typeName trim --------------(%@)-----------%d",typeName,[typeName length]);
				}
				
				NSNumber * typeID = [applianceTypeDictionary valueForKey:kApplianceTypeID];
				if (!isEmpty(typeName))
				{
					ApplianceType *applianceType = [[PSCoreDataManager sharedManager] applianceTypeWithName:typeName inContext:managedObjectContext];
					if(applianceType != nil)
					{
						//applianceType = (ApplianceType *)[managedObjectContext objectWithID:applianceType.objectID];
					}
					else
					{
						applianceType = [NSEntityDescription insertNewObjectForEntityForName:kApplianceType inManagedObjectContext:managedObjectContext];
					}
					
					applianceType.typeName = isEmpty(typeName)?nil:typeName;
					applianceType.typeID = isEmpty(typeID)?nil:typeID;
					if (appliance != nil) {
						appliance.applianceType = applianceType;
					}
				}
			}
		}
	}
}

- (void) loadInspectionForm:(NSDictionary *)inspectionFormDictionary forAppliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(inspectionFormDictionary) && !isEmpty(appliance))
	{
		NSNumber  *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
		NSString *combustionReading = [inspectionFormDictionary valueForKey:kCombustionReading];
		NSString *operatingPressure = [inspectionFormDictionary valueForKey:kOperatingPressure];
		NSString *safetyDeviceOperational = [inspectionFormDictionary valueForKey:kSafetyDeviceOperational];
		NSString *smokePellet = [inspectionFormDictionary valueForKey:kSmokePellet];
		NSString *adequateVentilation = [inspectionFormDictionary valueForKey:kAdequateVentilation];
		NSString *flueVisualCondition = [inspectionFormDictionary valueForKey:kFlueVisualCondition];
		NSString *satisfactoryTermination = [inspectionFormDictionary valueForKey:kSatisfactoryTermination];
		NSString *fluePerformanceChecks = [inspectionFormDictionary valueForKey:kFluePerformanceChecks];
		NSString *applianceServiced = [inspectionFormDictionary valueForKey:kApplianceServiced];
		NSString *applianceSafeToUse = [inspectionFormDictionary valueForKey:kApplianceSafeTouse];
		NSNumber *inspectionID = [inspectionFormDictionary valueForKey:kInspectionId];
		NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
		NSDate *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
		NSString *spillageTest = [inspectionFormDictionary valueForKey:kSpillageTest];
		NSString *operatingPressureUnit = [inspectionFormDictionary valueForKey:kOperatingPressureUnit];
		
		CLS_LOG(@"isInspected %@",isInspected);
		CLS_LOG(@"combustionReading %@",combustionReading);
		
		ApplianceInspection *applianceInspection = [[PSCoreDataManager sharedManager] applianceInspectionWithInspectionIdAndApplianceId:appliance.applianceID inspectionId:inspectionID managedObjectContext:managedObjectContext];
		if ( applianceInspection != nil)
		{
			CLS_LOG(@"Appliance Inspection UPDATED");
			//applianceInspection = (ApplianceInspection *)[managedObjectContext objectWithID:applianceInspection.objectID];
		}
		else
		{
			applianceInspection = [NSEntityDescription insertNewObjectForEntityForName:kApplianceInspection inManagedObjectContext:managedObjectContext];
			CLS_LOG(@"New Appliance Inspection ADDED");
		}
		
		
		applianceInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
		applianceInspection.combustionReading = isEmpty(combustionReading)?nil:combustionReading;
		applianceInspection.operatingPressure = isEmpty(operatingPressure)?nil:operatingPressure;
		applianceInspection.safetyDeviceOperational = isEmpty(safetyDeviceOperational)?nil:safetyDeviceOperational;
		applianceInspection.smokePellet = isEmpty(smokePellet)?nil:smokePellet;
		
		applianceInspection.adequateVentilation = isEmpty(adequateVentilation)?nil:adequateVentilation;
		
		applianceInspection.flueVisualCondition = isEmpty(flueVisualCondition)?nil:flueVisualCondition;
		applianceInspection.satisfactoryTermination = isEmpty(satisfactoryTermination)?nil:satisfactoryTermination;
		applianceInspection.fluePerformanceChecks = isEmpty(fluePerformanceChecks)?nil:fluePerformanceChecks;
		applianceInspection.applianceServiced = isEmpty(applianceServiced)?nil:applianceServiced;
		applianceInspection.applianceSafeToUse = isEmpty(applianceSafeToUse)?nil:applianceSafeToUse;
		applianceInspection.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
		applianceInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
		applianceInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
		applianceInspection.spillageTest = isEmpty(spillageTest)?nil:spillageTest;
		applianceInspection.operatingPressureUnit = isEmpty(operatingPressureUnit)?nil:operatingPressureUnit;
		
		applianceInspection.appliance = appliance;
		appliance.applianceInspection = applianceInspection;
	}
}

- (void) loadPipeworkForm:(NSDictionary *)pipeworkDictionary forBoiler:(Boiler *)boiler managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (!isEmpty(pipeworkDictionary) && !isEmpty(boiler))
    {
        NSString * emergencyControl = [pipeworkDictionary valueForKey:kEmergencyControl];
        NSString * equipotentialBonding = [pipeworkDictionary valueForKey:kEquipotentialBonding];
        NSString * gasTightnessTest = [pipeworkDictionary valueForKey:kGasTightnessTest];
        NSString * visualInspection = [pipeworkDictionary valueForKey:KVisualInspection];
        NSNumber * isInspected = [pipeworkDictionary valueForKey:kIsInspected];
        NSDate * inspectionDate = [pipeworkDictionary valueForKey:kInspectionDate];
        NSNumber * inspectionID = [pipeworkDictionary valueForKey:kInspectionId];
        
        InstallationPipework *installationPipework = boiler.boilerToGasInstallationPipeWork;
        if ( installationPipework != nil)
        {
            installationPipework = (InstallationPipework *)[managedObjectContext objectWithID:installationPipework.objectID];
        }
        else
        {
            installationPipework = [NSEntityDescription insertNewObjectForEntityForName:kInstallationPipework inManagedObjectContext:managedObjectContext];
        }
        
        installationPipework.emergencyControl = isEmpty(emergencyControl)?nil:emergencyControl;
        installationPipework.equipotentialBonding = isEmpty(equipotentialBonding)?nil:equipotentialBonding;
        installationPipework.gasTightnessTest = isEmpty(gasTightnessTest)?nil:gasTightnessTest;
        installationPipework.visualInspection = isEmpty(visualInspection)?nil:visualInspection;
        installationPipework.isInspected = isEmpty(isInspected)?nil:isInspected;
        installationPipework.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        installationPipework.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
        
        //  installationPipework.property = property;
        boiler.boilerToGasInstallationPipeWork = installationPipework;
    }
}



/*- (void) loadPipeworkForm:(NSDictionary *)pipeworkDictionary forProperty:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (!isEmpty(pipeworkDictionary) && !isEmpty(property))
    {
        NSString * emergencyControl = [pipeworkDictionary valueForKey:kEmergencyControl];
        NSString * equipotentialBonding = [pipeworkDictionary valueForKey:kEquipotentialBonding];
        NSString * gasTightnessTest = [pipeworkDictionary valueForKey:kGasTightnessTest];
        NSString * visualInspection = [pipeworkDictionary valueForKey:KVisualInspection];
        NSNumber * isInspected = [pipeworkDictionary valueForKey:kIsInspected];
        NSDate * inspectionDate = [pipeworkDictionary valueForKey:kInspectionDate];
        NSNumber * inspectionID = [pipeworkDictionary valueForKey:kInspectionId];
        
        InstallationPipework *installationPipework = property.installationPipework;
        if ( installationPipework != nil)
        {
            installationPipework = (InstallationPipework *)[managedObjectContext objectWithID:installationPipework.objectID];
        }
        else
        {
            installationPipework = [NSEntityDescription insertNewObjectForEntityForName:kInstallationPipework inManagedObjectContext:managedObjectContext];
        }
        
        installationPipework.emergencyControl = isEmpty(emergencyControl)?nil:emergencyControl;
        installationPipework.equipotentialBonding = isEmpty(equipotentialBonding)?nil:equipotentialBonding;
        installationPipework.gasTightnessTest = isEmpty(gasTightnessTest)?nil:gasTightnessTest;
        installationPipework.visualInspection = isEmpty(visualInspection)?nil:visualInspection;
        installationPipework.isInspected = isEmpty(isInspected)?nil:isInspected;
        installationPipework.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
        installationPipework.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
        
            //  installationPipework.property = property;
        property.installationPipework = installationPipework;
    }
}*/

#pragma mark - Fault Data
- (void) loadFaultRepairData:(NSArray *)faultRepairDataList jobData:(FaultJobSheet *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(faultRepairDataList))
	{
		for(NSDictionary *repairDataDictionary in faultRepairDataList)
		{
			if(!isEmpty(repairDataDictionary))
			{
				
				NSString * faultRepairDescription = [repairDataDictionary valueForKey:kFaultRepairDescription];
				NSNumber * faultRepairID = [repairDataDictionary valueForKey:kFaultRepairID];
				NSNumber * isAssociated = [repairDataDictionary valueForKey:kIsAssociated];
				FaultRepairData *faultRepair = [[PSCoreDataManager sharedManager] faultReapirWithID:faultRepairID];
				if(faultRepair != nil)
				{
					faultRepair = (FaultRepairData *)[managedObjectContext objectWithID:faultRepair.objectID];
				}
				else
				{
					faultRepair = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];
				}
				faultRepair.faultRepairID = isEmpty(faultRepairID)?nil:faultRepairID;
				faultRepair.faultRepairDescription = isEmpty([faultRepairDescription trimWhitespace])?nil:[faultRepairDescription trimWhitespace];
				faultRepair.repairSectionIdentifier = [faultRepair repairSectionIdentifier];
				faultRepair.isAssociated = isAssociated;
			}
		}
	}
}

- (void) loadJobPauseReasonData:(NSArray *)jobPauseReasonList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(jobPauseReasonList))
	{
		for (int pauseReason = 0; pauseReason < [jobPauseReasonList count]; ++pauseReason) {
			{
				NSString *reason = [jobPauseReasonList objectAtIndex:pauseReason];
				JobPauseReason *jobPauseReason = [[PSCoreDataManager sharedManager] jobPauseReasonWithName:reason];
				if (jobPauseReason != nil)
				{
					jobPauseReason = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
				}
				else
				{
					jobPauseReason = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseReason inManagedObjectContext:managedObjectContext];
				}
				jobPauseReason.pauseReason = reason;
			}
		}
	}
}

- (void) loadFaultRepairHistoryData:(NSArray *)faultRepairHistoryList jobData:(FaultJobSheet *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(jobData))
	{
		[jobData removeFaultRepairDataList:jobData.faultRepairDataList];
		
		if (!isEmpty(faultRepairHistoryList))
		{
			for (FaultRepairData *faultRepairData in faultRepairHistoryList)
			{
				if (!isEmpty(faultRepairData)) {
					FaultRepairListHistory *faultRepairHistory = [[PSCoreDataManager sharedManager]faultRepairHistoryWithID:faultRepairData.faultRepairID forJobData:jobData.jsNumber inContext:managedObjectContext];
					if (faultRepairHistory != nil)
					{
						faultRepairHistory = (FaultRepairListHistory *)[managedObjectContext objectWithID:faultRepairHistory.objectID];
					}
					else
					{
						faultRepairHistory = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairListHistory inManagedObjectContext:managedObjectContext];
					}
					
					faultRepairHistory.faultRepairID = faultRepairData.faultRepairID;
					faultRepairHistory.faultRepairDescription = faultRepairData.faultRepairDescription;
					FaultRepairData *existingFaultRepairData = (FaultRepairData *)[managedObjectContext objectWithID:faultRepairData.objectID];
					if (!isEmpty(jobData))
					{
						if (![jobData.faultRepairDataList containsObject:faultRepairData])
						{
							[jobData addFaultRepairDataListObject:existingFaultRepairData];
						}
						if (![jobData.faultRepairHistoryList containsObject:faultRepairHistory])
						{
							[jobData addFaultRepairHistoryListObject:faultRepairHistory];
						}
						jobData.jobSheetToAppointment.isModified = [NSNumber numberWithBool:YES];
					}
				}
			}
		}
	}
}
@end
