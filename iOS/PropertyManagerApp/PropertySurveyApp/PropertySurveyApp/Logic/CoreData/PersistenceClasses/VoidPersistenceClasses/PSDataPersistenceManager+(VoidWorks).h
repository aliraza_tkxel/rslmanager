//
//  PSDataPersistenceManager+(VoidWorks).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-11.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDataPersistenceManager (VoidWorks)

-(void)loadVoidWorksData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext;

@end
