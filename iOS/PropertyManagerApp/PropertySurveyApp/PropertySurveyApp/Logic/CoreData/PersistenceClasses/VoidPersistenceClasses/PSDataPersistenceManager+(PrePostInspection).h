//
//  PSDataPersistenceManager+(PreInspection).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager (PrePostInspection)

-(void)loadVoidAppointmentData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext;

@end
