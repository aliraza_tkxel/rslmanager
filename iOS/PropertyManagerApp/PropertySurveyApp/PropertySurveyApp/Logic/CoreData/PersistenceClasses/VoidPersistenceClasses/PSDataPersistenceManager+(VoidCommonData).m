//
//  PSDataPersistenceManager+(CommonData).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
#import "PSDataPersistenceManager+(VoidCommonData).h"
#import "MeterType.h"
#import "RoomData.h"
#import "PauseReason.h"
#import "CoreDataHelper.h"

@implementation PSDataPersistenceManager (VoidCommonData)

- (void) saveVoidCommonData:(NSArray *)commonDataArray
{
	CLS_LOG(@"save VoidCommonData");
	if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
	{
		__block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		NSArray * roomsList = [commonDataArray valueForKey:@"rooms"];
		[self loadRooms:roomsList dbContext:dbContext];
        /*
         TODO: jobPauseReasonList is the key, not pauseReasonList. Fix it in Ticket US-231
         */
		NSArray * pauseReasonList = [commonDataArray valueForKey:@"pauseReasonList"];
		[self loadPauseReason:pauseReasonList dbContext:dbContext];
		NSArray * gasMeterTypeList = [commonDataArray valueForKey:@"gasMeterType"];
		[self loadMeterType:gasMeterTypeList DeviceType:kGasDeviceType dbContext:dbContext];
		NSArray * electricMeterTypeList = [commonDataArray valueForKey:@"electricMeterType"];
		[self loadMeterType:electricMeterTypeList DeviceType:kElectricDeviceType dbContext:dbContext];

	}
}

-(void) loadPauseReason:(NSArray *)pauseReasonList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(pauseReasonList) == NO)
	{
		int counter = 0;
		NSArray * dbPauseList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([PauseReason class])
																													sortKey:NULL
																										sortAscending:FALSE
																													context:dbContext];
		for(NSDictionary * pauseReason in pauseReasonList)
		{
			NSNumber * pauseId = [pauseReason valueForKey:kPauseId];
			NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.pauseId == %@", pauseId];
			NSArray * result = [dbPauseList filteredArrayUsingPredicate:filter];
			if([result count] == 0)
			{
				PauseReason * pauseReasonObj = [NSEntityDescription insertNewObjectForEntityForName:kPauseReason
																																		 inManagedObjectContext:dbContext];
				pauseReasonObj.pauseId = pauseId;
				pauseReasonObj.reason  = [pauseReason valueForKey:@"reason"];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadRooms:(NSArray *)roomsList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(roomsList) == NO)
	{
		int counter = 0;
		NSArray * dbRoomsList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([RoomData class])
																													sortKey:@"roomId"
																										sortAscending:TRUE
																													context:dbContext];
		for(NSDictionary * room in roomsList)
		{
			NSNumber * roomId = [room valueForKey:kRoomId];
			NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.roomId == %@", roomId];
			NSArray * result = [dbRoomsList filteredArrayUsingPredicate:filter];
			if([result count] == 0)
			{
				RoomData * roomData = [NSEntityDescription insertNewObjectForEntityForName:kRoomData
																														inManagedObjectContext:dbContext];
				roomData.roomId = roomId;
				roomData.roomName = [room valueForKey:@"name"];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}


-(void) loadMeterType:(NSArray *)meterTypeList
					 DeviceType:(NSString *)deviceType
						dbContext:(NSManagedObjectContext *)dbContext
{
	if (isEmpty(meterTypeList) == NO)
	{
		int counter = 0;
		NSArray * dbMeterList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([MeterType class])
																													sortKey:@"meterId"
																										sortAscending:TRUE
																													context:dbContext];
		for(NSDictionary * meterType in meterTypeList)
		{
			NSNumber * meterId = [meterType valueForKey:@"meterId"];
			NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.meterId == %@", meterId];
			NSArray * result = [dbMeterList filteredArrayUsingPredicate:filter];
			if([result count] == 0)
			{
				MeterType * meterTypeObj = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MeterType class])
																																 inManagedObjectContext:dbContext];
				meterTypeObj.meterId = meterId;
				meterTypeObj.meterType = [meterType valueForKey:@"meterType"];
				meterTypeObj.deviceType = deviceType;
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}
@end
