//
//  PSDataPersistenceManager+(VoidWorks).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-11.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(VoidWorks).h"
#import "VoidWorks.h"

@implementation PSDataPersistenceManager (VoidWorks)

-(void)loadVoidWorksData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext
{
	VoidWorks * voidWorks = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([VoidWorks class]) inManagedObjectContext:dbContext];
	voidWorks.reletDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kReletData]];
	voidWorks.terminationDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kTerminationDate]];
	voidWorks.jsvNumber = [appointmentData valueForKey:kJsvNumber];
	
	CLS_LOG(@"Property: %@", [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards]);
	CLS_LOG(@"Void Works: Relet Date = %@", [UtilityClass stringFromDate:voidWorks.reletDate dateFormat:kDateTimeStyle9]);
	CLS_LOG(@"Void Works: Termination Date = %@", [UtilityClass stringFromDate:voidWorks.terminationDate dateFormat:kDateTimeStyle9]);
	
	voidWorks.voidWorksToAppointment = appointment;
	appointment.appointmentToVoidWorks = voidWorks;
}

@end
