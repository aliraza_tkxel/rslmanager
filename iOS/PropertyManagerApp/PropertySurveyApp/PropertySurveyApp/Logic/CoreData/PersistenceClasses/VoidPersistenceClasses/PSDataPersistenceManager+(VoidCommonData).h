//
//  PSDataPersistenceManager+(CommonData).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager (VoidCommonData)

- (void) saveVoidCommonData:(NSArray *)commonDataArray;

@end
