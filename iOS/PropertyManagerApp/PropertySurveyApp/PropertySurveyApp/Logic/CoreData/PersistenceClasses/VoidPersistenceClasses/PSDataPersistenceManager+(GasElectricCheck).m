//
//  PSDataPersistenceManager+(GasElectricCheck).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(GasElectricCheck).h"
#import "MeterData.h"

@implementation PSDataPersistenceManager (GasElectricCheck)

#pragma mark - Void Pre Inspection
-(void)loadGasElectricData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext
{
	AppointmentType type = [appointment getType];
	if(type == AppointmentTypeGasCheck ||
		 type == AppointmentTypeElectricCheck)
	{
		MeterData * meterData = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MeterData class]) inManagedObjectContext:dbContext];
		meterData.meterLocation = [appointmentData valueForKey:@"meterLocation"];
		meterData.meterReading = [appointmentData valueForKey:@"meterReading"];
		meterData.deviceType = [appointmentData valueForKey:@"deviceType"];
		meterData.tenantType = [appointmentData valueForKey:@"tenantType"];
		meterData.reletDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kReletData]];
		meterData.terminationDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kTerminationDate]];
		
		CLS_LOG(@"Property: %@", [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards]);
		CLS_LOG(@"Meter Data: Relet Date = %@", [UtilityClass stringFromDate:meterData.reletDate dateFormat:kDateTimeStyle9]);
		CLS_LOG(@"Meter Data: Termination Date = %@", [UtilityClass stringFromDate:meterData.terminationDate dateFormat:kDateTimeStyle9]);
		
		meterData.meterDataToAppointment = appointment;
		appointment.appointmentToMeterData = meterData;
	}
}

@end
