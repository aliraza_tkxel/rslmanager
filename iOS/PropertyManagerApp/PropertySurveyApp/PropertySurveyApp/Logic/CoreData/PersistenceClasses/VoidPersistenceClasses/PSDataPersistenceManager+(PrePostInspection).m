//
//  PSDataPersistenceManager+(PreInspection).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-08.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(PrePostInspection).h"
#import "PSDataPersistenceManager+(GasElectricCheck).h"
#import "PSDataPersistenceManager+(VoidWorks).h"

#import "PropertyComponents.h"
#import "MajorWorkRequired.h"
#import "CoreDataHelper.h"
#import "WorkRequired+CoreDataClass.h"
#import "MeterData.h"
#import "PaintPack.h"
#import "RoomData.h"
#import "VoidData.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"

@implementation PSDataPersistenceManager (PrePostInspection)

#pragma mark - Void Pre Inspection
-(void)loadVoidAppointmentData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext
{
	AppointmentType type = [appointment getType];
	switch (type)
	{
		case AppointmentTypePreVoid:
		case AppointmentTypePostVoid:
			{
				// Load Pre & Post Inspection Appointment Data
				[self loadPrePostInspectionData:appointmentData appointment:appointment dbContext:dbContext];
			}
			break;
		case AppointmentTypeGasCheck:
		case AppointmentTypeElectricCheck:
			{
				// Load Gas & Electric Check Appointment Data
				[self loadGasElectricData:appointmentData appointment:appointment dbContext:dbContext];
			}
			break;
		case AppointmentTypeVoidWorkRequired:
			{
				// Load Void Works Appointment Data
				[self loadVoidWorksData:appointmentData appointment:appointment dbContext:dbContext];
			}
			break;
		default:
			break;
	}
}

-(void)loadPrePostInspectionData:(NSDictionary *)appointmentData appointment:(Appointment *)appointment dbContext:(NSManagedObjectContext *)dbContext
{
	// Pre Inspection data
	VoidData * voidData = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([VoidData class]) inManagedObjectContext:dbContext];
	voidData.jsvNumber = [appointmentData valueForKey:kJsvNumber];
	voidData.reletDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kReletData]];
	voidData.terminationDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kTerminationDate]];
	voidData.isAsbestosCheckRequired = [appointmentData valueForKey:kIsAsbestosCheckRequired];
	voidData.isEPCCheckRequired = [appointmentData valueForKey:kIsEPCCheckRequired];
	voidData.isGasCheckRequired = [appointmentData valueForKey:kIsGasCheckRequired];
	voidData.gasCheckDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:@"gasCheckDate"]];
	voidData.gasCheckStatus = [appointmentData valueForKey:@"gasCheckStatus"];
	voidData.isElectricCheckRequired = [appointmentData valueForKey:kIsElectricCheckRequired];
	voidData.electricCheckDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:@"electricCheckDate"]];
	voidData.electricCheckStatus = [appointmentData valueForKey:@"electricCheckStatus"];
	
	// Is Major Work Required
	NSDictionary * majorWorkRequired = [appointmentData valueForKey:@"majorWorksRequired"];
	voidData.isMajorWorkRequired = [majorWorkRequired valueForKey:@"isMajorWorkRequired"];

	// Property Components -- Major Work Required
	NSArray * propertyComponents = [majorWorkRequired objectForKey:@"propertyComponents"];
	[self loadPropertyComponents:propertyComponents VoidData:voidData dbContext:dbContext];

	// Recorded Major Work -- Major Work Required
	NSArray * recordMajorWork = [majorWorkRequired objectForKey:@"recordMajorWork"];
	[self loadRecordMajorWork:recordMajorWork VoidData:voidData dbContext:dbContext];
	
	// Paint Pack
	NSDictionary * paintPacks = [appointmentData valueForKey:@"paintPacks"];
	voidData.isPaintPackRequired = [paintPacks valueForKey:@"isPaintPackRequired"];
	
	// Room List -- Paint Pack
	NSArray * roomList = [paintPacks objectForKey:@"roomList"];
	[self loadRecordPaintPack:roomList VoidData:voidData dbContext:dbContext];
	
	// Work Required
	NSDictionary * worksRequired = [appointmentData valueForKey:@"worksRequired"];
	voidData.isWorksRequired = [worksRequired valueForKey:kIsWorksRequired];
	
	// Recorded Work Required -- Work Required
	NSArray * recordWorks = [worksRequired objectForKey:@"recordWorks"];
	[self loadRecordWork:recordWorks VoidData:voidData dbContext:dbContext];
	
	CLS_LOG(@"Property: %@", [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards]);
	CLS_LOG(@"Pre Post: Relet Date = %@", [UtilityClass stringFromDate:voidData.reletDate dateFormat:kDateTimeStyle9]);
	CLS_LOG(@"Pre Post: Termination Date = %@", [UtilityClass stringFromDate:voidData.terminationDate dateFormat:kDateTimeStyle9]);
	
	// Object relationship on both side
	appointment.appointmentToVoidData = voidData;
	voidData.voidDataToAppointment = appointment;
}

-(void) loadRecordPaintPack:(NSArray *)recordPaintPack VoidData:(VoidData *)voidData dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(recordPaintPack) == FALSE)
	{
		// TODO: Need optimization
		NSArray * roomList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([RoomData class])
																											 sortKey:@"roomId"
																								 sortAscending:TRUE
																											 context:dbContext];
		for (NSNumber * paintPackRoomId in recordPaintPack)
		{
			PaintPack * paintPack = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PaintPack class]) inManagedObjectContext:dbContext];
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.roomId == %@", paintPackRoomId];
			RoomData * roomData = [[roomList filteredArrayUsingPredicate:predicate] objectAtIndex:0];
			paintPack.roomId = roomData.roomId;
			paintPack.roomName = roomData.roomName;
			
			paintPack.paintPackToVoidData = voidData;
			[voidData addVoidDataToPaintPackObject:paintPack];
		}
	}
}
-(void) loadRecordWork:(NSArray *)recordWorks VoidData:(VoidData *)voidData dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(recordWorks) == FALSE)
	{
		// TODO: Need optimization
        
        NSArray * faultAreaList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([FaultAreaList class])
                                                               sortKey:@"faultAreaId"
                                                         sortAscending:TRUE
                                                               context:dbContext];
		
		voidData.tenantEstimate = [NSNumber numberWithFloat:0];
		for (NSDictionary * workRequiredDic in recordWorks)
		{
			WorkRequired * workRequired = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([WorkRequired class]) inManagedObjectContext:dbContext];
			
			workRequired.isTenantWork = [workRequiredDic valueForKey:@"isTenantWork"];
			workRequired.isBRSWork = [workRequiredDic valueForKey:@"isBRSWork"];
			workRequired.isVerified = [workRequiredDic valueForKey:@"isVerified"];
			workRequired.worksId = [workRequiredDic valueForKey:@"worksId"];
			workRequired.status = [workRequiredDic valueForKey:@"status"];
            
            workRequired.repairDescription = [workRequiredDic valueForKey:@"workDescription"];
            if(isEmpty([workRequiredDic valueForKey:@"repairId"])==NO){
                workRequired.repairId = [workRequiredDic valueForKey:@"repairId"];
            }
            else{
                workRequired.repairId = [NSNumber numberWithInteger:0];
            }
            
            workRequired.gross = [workRequiredDic valueForKey:@"gross"];
            workRequired.repairNotes = [workRequiredDic valueForKey:@"repairNotes"];
            
            
			
			NSNumber * faultAreaId = [workRequiredDic valueForKey:@"faultAreaId"];
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.faultAreaId == %@", faultAreaId];
            NSArray *filteredArray = [[NSArray alloc] initWithArray:[faultAreaList filteredArrayUsingPredicate:predicate]];
            FaultAreaList * areaData;
            if([filteredArray count]>0){
               areaData = [filteredArray objectAtIndex:0];
            }
    
			workRequired.workRequiredToFaultArea = areaData;
			
			if([workRequired.isTenantWork boolValue] == TRUE)
			{
				voidData.tenantEstimate = [NSNumber numberWithFloat:([voidData.tenantEstimate floatValue] + [workRequired.gross floatValue])];
			}
			
			workRequired.workRequiredToVoidData = voidData;
			[voidData addVoidDataToWorkRequiredObject:workRequired];
		}
	}
}
-(void) loadRecordMajorWork:(NSArray *)recordMajorWork VoidData:(VoidData *)voidData dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(recordMajorWork) == FALSE)
	{
		for (NSDictionary * majorWorkDic in recordMajorWork)
		{
			MajorWorkRequired * majorWork = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MajorWorkRequired class]) inManagedObjectContext:dbContext];
			majorWork.componentId = [majorWorkDic valueForKey:@"componentId"];
			majorWork.component = [majorWorkDic valueForKey:@"component"];
			majorWork.condition = [majorWorkDic valueForKey:@"condition"];
			majorWork.notes = [majorWorkDic valueForKey:@"notes"];
			majorWork.replacementDue = [UtilityClass convertServerDateToNSDate:[majorWorkDic valueForKey:@"replacementDue"]];
			
			majorWork.majorWorkRequiredToVoidData = voidData;
			[voidData addVoidDataToMajorWorkRequiredObject:majorWork];
		}
	}
}
-(void) loadPropertyComponents:(NSArray *)propertyComponents VoidData:(VoidData *)voidData dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(propertyComponents) == FALSE)
	{
		for (NSDictionary * componentDic in propertyComponents)
		{
			PropertyComponents * propertyComponent = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PropertyComponents class]) inManagedObjectContext:dbContext];
			propertyComponent.componentId = [componentDic valueForKey:@"componentId"];
			propertyComponent.component = [componentDic valueForKey:@"component"];
			propertyComponent.condition = [componentDic valueForKey:@"condition"];
			
			NSString * replacementDue = [componentDic valueForKey:@"replacementDue"];
			if([replacementDue isKindOfClass:[NSNull class]] == TRUE)
			{
				propertyComponent.replacementDue = NULL;
			}
			else
			{
				propertyComponent.replacementDue = [UtilityClass convertServerDateToNSDate:replacementDue];
			}
			propertyComponent.propertyComponentsToVoidData = voidData;
			[voidData addVoidDataToPropertyComponentsObject:propertyComponent];
		}
	}
}

@end
