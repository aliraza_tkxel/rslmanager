//
//  PSDataPersistenceManager+(RepairCommonData).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-17.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager (RepairCommonData)

- (void) saveRepairCommonData:(NSArray *)commonDataArray;
- (void) saveFaultAreaCommonData:(NSArray *)commonDataArray;
-(void) saveRepairListCommonData:(NSArray *) commonDataArray;

@end
