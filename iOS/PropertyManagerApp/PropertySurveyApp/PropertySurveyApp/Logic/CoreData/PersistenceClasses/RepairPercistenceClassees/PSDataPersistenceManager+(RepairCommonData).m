//
//  PSDataPersistenceManager+(RepairCommonData).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-17.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager+(RepairCommonData).h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"
#import "FaultRepairData+CoreDataClass.h"

@implementation PSDataPersistenceManager (RepairCommonData)

- (void) saveRepairCommonData:(NSArray *)commonDataArray
{
	CLS_LOG(@"saveRepairCommonData");
	if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
	{
		__block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		NSArray * roomsList = [commonDataArray valueForKey:@"faultsDetailList"];
		[self loadFaultDetailList:roomsList dbContext:dbContext];
	}
}

- (void) saveFaultAreaCommonData:(NSArray *)commonDataArray
{
	CLS_LOG(@"saveRepairCommonData");
	if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
	{
		__block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		NSArray * faultAreaList = [commonDataArray valueForKey:@"faultAreaList"];
		[self loadFaultAreaList:faultAreaList dbContext:dbContext];
	}
}

-(void) loadFaultAreaList:(NSArray *)faultAreaList dbContext:(NSManagedObjectContext *)dbContext
{
	NSString * entityName = NSStringFromClass([FaultAreaList class]);
	NSArray * dbFaultAreaList = [CoreDataHelper getObjectsFromEntity:entityName
																													 sortKey:NULL
																										 sortAscending:FALSE
																													 context:dbContext];
	if(isEmpty(faultAreaList) == NO)
	{
		int counter = 0;

		for(NSDictionary * faultArea in faultAreaList)
		{
			NSNumber * faultAreaId = [faultArea valueForKey:@"faultAreaId"];
			NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.faultAreaId == %@", faultAreaId];
			NSArray * result = [dbFaultAreaList filteredArrayUsingPredicate:filter];
			if([result count] == 0)
			{
				FaultAreaList * faultAreaObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
																																		 inManagedObjectContext:dbContext];
				faultAreaObj.faultAreaId = faultAreaId;
				faultAreaObj.faultAreaName = [faultArea valueForKey:@"faultAreaName"];
				
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) loadFaultDetailList:(NSArray *)faultDetailList dbContext:(NSManagedObjectContext *)dbContext
{
	if(isEmpty(faultDetailList) == NO)
	{
		int counter = 0;
		NSString * entityName = NSStringFromClass([FaultDetailList class]);
		NSArray * dbFaultDetailList = [CoreDataHelper getObjectsFromEntity:entityName
																															 sortKey:NULL
																												 sortAscending:FALSE
																															 context:dbContext];

		for(NSDictionary * faultDetail in faultDetailList)
		{
			NSNumber * faultId = [faultDetail valueForKey:@"faultId"];
			NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.faultId == %@", faultId];
			NSArray * result = [dbFaultDetailList filteredArrayUsingPredicate:filter];
			if([result count] == 0)
			{
				FaultDetailList * faultDetailObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
																																				 inManagedObjectContext:dbContext];
				faultDetailObj.faultId = faultId;
				faultDetailObj.faultDescription = [faultDetail valueForKey:@"faultDescription"];
				faultDetailObj.gross = [faultDetail valueForKey:@"gross"];
				faultDetailObj.isRecharge = [faultDetail valueForKey:@"isRecharge"];
				faultDetailObj.priorityName = [faultDetail valueForKey:@"priorityName"];
				faultDetailObj.responseTime = [faultDetail valueForKey:@"responseTime"];
				
				counter ++;
				if(counter % 5 == 0)
				{
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:dbContext];
					counter = 0;
				}
			}
		}
		if(counter > 0)
		{
			[self saveDataInDB:dbContext];
		}
	}
}

-(void) saveRepairListCommonData:(NSArray *) commonDataArray{
    CLS_LOG(@"saveRepairListCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * faultRepairList = [commonDataArray valueForKey:@"repairList"];
        [self loadFaultRepairData:faultRepairList dbContext:dbContext];
    }
    
}

-(void) loadFaultRepairData:(NSArray * )faultRepairList dbContext:(NSManagedObjectContext *) dbContext{
    
    NSString * entityName = NSStringFromClass([FaultRepairData class]);
    NSArray * dbFaultRepairList = [CoreDataHelper getObjectsFromEntity:entityName
                                                               sortKey:NULL
                                                         sortAscending:FALSE
                                                               context:dbContext];
    if(isEmpty(faultRepairList) == NO)
    {
        int counter = 0;
        for(NSDictionary * faultRepair in faultRepairList)
        {
            NSNumber * faultRepairId = [faultRepair valueForKey:@"faultRepairId"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.faultRepairID == %@", faultRepairId];
            NSArray * result = [dbFaultRepairList filteredArrayUsingPredicate:filter];
            FaultRepairData *faultRepairObj;
            if([result count] == 0)
            {
                faultRepairObj = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                                 inManagedObjectContext:dbContext];
                faultRepairObj.faultRepairID = faultRepairId;
                faultRepairObj = [self populateFaultRepairDataForObject:faultRepairObj andData:faultRepair];
                counter ++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
            else
            {
                faultRepairObj = [result objectAtIndex:0];
                faultRepairObj = [self populateFaultRepairDataForObject:faultRepairObj andData:faultRepair];
                [self saveDataInDB:dbContext];
            }
        }
    }
    
}

-(FaultRepairData* ) populateFaultRepairDataForObject:(FaultRepairData *) faultRepairObj andData:(NSDictionary *) faultRepair{
    faultRepairObj.faultRepairDescription = [faultRepair valueForKey:@"description"];
    faultRepairObj.isAssociated = [faultRepair valueForKey:@"isAssociated"];
    faultRepairObj.gross = [faultRepair valueForKey:@"gross"];
    return faultRepairObj;
}

@end
