//
//  SurveyData+DataPersistance.h
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 11/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "SurveyData.h"

@interface SurveyData (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (FormData *)formDataWithItemId:(NSString *)itemId controlType:(NSString *)controlType;

@end
