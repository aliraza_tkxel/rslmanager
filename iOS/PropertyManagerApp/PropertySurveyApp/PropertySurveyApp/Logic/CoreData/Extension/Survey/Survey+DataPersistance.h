//
//  Survey+DataPersistance.h
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 09/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Survey.h"

@class SurveyData;

@interface Survey (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary;
- (void)saveSurveyNotes:(NSString *)notesString forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary;

- (SurveyData *)surveyDataWithItemId:(NSString *)itemId;

- (NSInteger)surveyFieldsFilled:(NSDictionary *)surveyDataDictionary;

@end
