//
//  SurveyData+DataPersistance.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 11/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "SurveyData+DataPersistance.h"
#define kFormDataItemId @"surveyItemParamId"
#define kControlType @"controlType"

@implementation SurveyData (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
//verify the keys in if condition below
    if(!isEmpty([formDataDictionary objectForKey:@"controlType"]) && !isEmpty([formDataDictionary objectForKey:@"surveyParameterId"]) && !isEmpty([formDataDictionary objectForKey:@"surveyParameterId"]) )
    {
    
	STARTEXCEPTION
	FormData *formData = [self formDataWithItemId:[formDataDictionary objectForKey:kFormDataItemId] controlType:[formDataDictionary objectForKey:kControlType] inManagedObjectContext:managedObjectContext];
    
	formData.controlType = [formDataDictionary objectForKey:@"controlType"];
	formData.isSelected = [NSNumber numberWithBool:[[formDataDictionary objectForKey:@"isSelected"] boolValue]];
	formData.surveyItemParamId = [formDataDictionary objectForKey:kFormDataItemId];
	formData.surveyParameterId = [formDataDictionary objectForKey:@"surveyParameterId"];
	formData.surveyParamItemFieldValue = isEmpty([formDataDictionary objectForKey:@"surveyParamItemFieldValue"])?nil:[formDataDictionary objectForKey:@"surveyParamItemFieldValue"];
	formData.surveyPramItemFieldId = [NSNumber numberWithInteger:[[formDataDictionary objectForKey:@"surveyPramItemFieldId"] integerValue]];
	formData.surveyParamName = [formDataDictionary objectForKey:@"surveyParamName"];
	ENDEXCEPTION
    }
}

- (FormData *)formDataWithItemId:(NSString *)itemId controlType:(NSString *)controlType{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@ AND SELF.%@ = %@", kFormDataItemId, itemId, kControlType, controlType];
	FormData *formData = nil;
    
	NSSet *filteredObjects = [[self surveyDataToFormData] filteredSetUsingPredicate:predicate];
    
	if ([filteredObjects count] > 0) {
		formData = [[filteredObjects allObjects] objectAtIndex:0];
	}
    return formData;
}

#pragma mark - Private Method
- (FormData *)formDataWithItemId:(NSString *)itemId controlType:(NSString *)controlType inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
	FormData *formData = nil;
	STARTEXCEPTION
    
    formData = [self formDataWithItemId:itemId controlType:controlType];
	if (!formData) { // object is not found so we need to add a new one.
		formData = [NSEntityDescription insertNewObjectForEntityForName:@"FormData" inManagedObjectContext:managedObjectContext];
		SurveyData *surveyData = (SurveyData *)[managedObjectContext objectWithID:self.objectID];
		[surveyData addSurveyDataToFormDataObject:formData];
	}
	ENDEXCEPTION
    
	return formData;
}

@end
