//
//  Appointment+SurveyDownload.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 24/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appointment+SurveyDownload.h"


#import <objc/runtime.h>
#import "PSDataPersistenceManager+(Boiler).h"
@implementation Appointment (SurveyDownload)
@dynamic fetchAlternativeSurveyFormService;
@dynamic fetchSurveyFormService;
//@dynamic fetchAppliancesService;
@dynamic isDownloadInProgress;
@dynamic progressBlock;

static char PSA_FETCH_SURVEY_FORM_SERVICE_KEY;
static char PSA_FETCH_APPLIANCES_SERVICE_KEY;
static char PSA_FETCH_ALTERNATIVE_FUEL_SURVEY_SERVICE_KEY;
static char PSA_IS_DOWNLOAD_IN_PROGRESS_KEY;
static char PSA_PROGRESS_BLOCK_KEY;
static char PSA_FETCH_OIL_SURVEY_SERVICE_KEY;

#pragma mark - Accessor Methods for Fetch Survey Form Service

- (void)setFetchOilSurveyFormService:(PSFetchOilServicingSurveyService *)fetchOilSurveyFormService{
    objc_setAssociatedObject(self, &PSA_FETCH_OIL_SURVEY_SERVICE_KEY, fetchOilSurveyFormService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (void)setFetchAlternativeSurveyFormService:(PSFetchAlternativeFuelSurveyService *)fetchAlternativeSurveyFormService{
    objc_setAssociatedObject(self, &PSA_FETCH_ALTERNATIVE_FUEL_SURVEY_SERVICE_KEY, fetchAlternativeSurveyFormService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setFetchSurveyFormService:(PSFetchSurveyFormService *)fetchSurveyFormService {
	objc_setAssociatedObject(self, &PSA_FETCH_SURVEY_FORM_SERVICE_KEY, fetchSurveyFormService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (PSFetchSurveyFormService *)fetchSurveyFormService {
	return objc_getAssociatedObject(self, &PSA_FETCH_SURVEY_FORM_SERVICE_KEY);
}

- (PSFetchAlternativeFuelSurveyService *)fetchAlternativeSurveyFormService {
    return objc_getAssociatedObject(self, &PSA_FETCH_ALTERNATIVE_FUEL_SURVEY_SERVICE_KEY);
}

- (PSFetchOilServicingSurveyService *)fetchOilSurveyFormService {
    return objc_getAssociatedObject(self, &PSA_FETCH_OIL_SURVEY_SERVICE_KEY);
}

#pragma mark - Accessor Methods for Get Appliacnes Service
- (void)setFetchAppliancesService:(PSFetchAppliancesService *)fetchAppliancesService {
	objc_setAssociatedObject(self, &PSA_FETCH_APPLIANCES_SERVICE_KEY, fetchAppliancesService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (PSFetchAppliancesService *)fetchAppliancesService {
	return objc_getAssociatedObject(self, &PSA_FETCH_APPLIANCES_SERVICE_KEY);
}

#pragma mark - Download Progress
- (void)setIsDownloadInProgress:(BOOL)isDownloadInProgress {
	objc_setAssociatedObject(self, &PSA_IS_DOWNLOAD_IN_PROGRESS_KEY, [NSNumber numberWithBool:isDownloadInProgress], OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)isDownloadInProgress {
	return [(NSNumber *)objc_getAssociatedObject(self, &PSA_IS_DOWNLOAD_IN_PROGRESS_KEY) boolValue];
}

- (void)setProgressBlock:(AppointmentSurveyDataProgressiveOperationProgressBlock)progressBlock {
	objc_setAssociatedObject(self, &PSA_PROGRESS_BLOCK_KEY, progressBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (AppointmentSurveyDataProgressiveOperationProgressBlock)progressBlock {
	return objc_getAssociatedObject(self, &PSA_PROGRESS_BLOCK_KEY);
}

#pragma mark - Methods

- (void)prepareForOffline
{
	[self setIsDownloadInProgress:YES];
	if ([UtilityClass isUserLoggedIn] && !isEmpty(self.appointmentToProperty))
	{
		@synchronized(self)
		{
			if([self getType] == AppointmentTypeGas || [self getType] == AppointmentTypeGasVoid || [self getType]==AppointmentTypeAlternativeFuels || [self getType]==AppointmentTypeOilServicing)
			{
				if (![self fetchAppliancesService])
				{
					[self setFetchAppliancesService:[[PSFetchAppliancesService alloc] init]];
					[self fetchAppliancesService].appliacneDelegate = self;
				}
				NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
				[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
				[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
				[[self fetchAppliancesService] fetchAllAppliacnes:requestParameters];
			}
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchPropertyApplianceData
{
	NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
	[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
	[requestParameters setObject:self.appointmentId forKey:@"appointmentid"];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.propertyId)?self.appointmentToProperty.propertyId:[NSNull null] forKey:kPropertyId];
	[requestParameters setObject:self.journalId forKey:kJournalId];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.schemeId)?self.appointmentToProperty.schemeId:[NSNull null] forKey:kSchemeID];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.blockId)?self.appointmentToProperty.blockId:[NSNull null] forKey:kSchemeBlockID];
	[[PSApplianceManager sharedManager] fetchPropertyAppliances:requestParameters forProperty:self.appointmentToProperty.objectID];
}


-(void) fetchAlternativeFuelSurvey{
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
    [requestParameters setObject:self.appointmentId forKey:@"appointmentid"];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.propertyId)?self.appointmentToProperty.propertyId:[NSNull null] forKey:kPropertyId];
    [requestParameters setObject:self.journalId forKey:kJournalId];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.schemeId)?self.appointmentToProperty.schemeId:[NSNull null] forKey:kSchemeID];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.blockId)?self.appointmentToProperty.blockId:[NSNull null] forKey:kSchemeBlockID];
    
    if (![self fetchAlternativeSurveyFormService])
    {
        [self setFetchAlternativeSurveyFormService:[[PSFetchAlternativeFuelSurveyService alloc] init]];
        [self fetchAlternativeSurveyFormService].alternateFuelServiceDelegate = self;
    }
    NSData *responseData = [[self fetchAlternativeSurveyFormService] fetchSurveyForAlternativeFuel:requestParameters];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self service:[self fetchAlternativeSurveyFormService] didFinishLoadingAlternativeSurvey:responseString];
    
}


-(void) fetchOilServicingSurvey{
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
    [requestParameters setObject:self.appointmentId forKey:@"appointmentid"];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.propertyId)?self.appointmentToProperty.propertyId:[NSNull null] forKey:kPropertyId];
    [requestParameters setObject:self.journalId forKey:kJournalId];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.schemeId)?self.appointmentToProperty.schemeId:[NSNull null] forKey:kSchemeID];
    [requestParameters setObject:!isEmpty(self.appointmentToProperty.blockId)?self.appointmentToProperty.blockId:[NSNull null] forKey:kSchemeBlockID];
    
    if (![self fetchOilSurveyFormService])
    {
        [self setFetchOilSurveyFormService:[[PSFetchOilServicingSurveyService alloc] init]];
        [self fetchOilSurveyFormService].oilServiceDelegate = self;
    }
    NSData *responseData = [[self fetchOilSurveyFormService] fetchSurveyForOilServicing:requestParameters];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [self service:[self fetchOilSurveyFormService] didFinishLoadingOilSurvey:responseString];
    
}


#pragma mark - Delegates Download Progress Method

- (void)service:(id)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
	if (downloadedBytes >= expectedContentLength)
	{
		[self setIsDownloadInProgress:NO];
	}
	if ([self progressBlock])
	{
		AppointmentSurveyDataProgressiveOperationProgressBlock block = [self progressBlock];
		block(downloadedBytes, expectedContentLength);
	}
	CLS_LOG(@"Progess downloaded: %ld, expected: %ld", (long)downloadedBytes, (long)expectedContentLength);
}

#pragma mark - PSFetchSurveyFormServiceDelegate
- (void)service:(PSFetchSurveyFormService *)service didFinishDownloadingSurveyForm:(id)surveyData
{
	NSDictionary *responseDictionary = [surveyData JSONValue];
	NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
	if(statusCode == kSuccessCode)
	{
			NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
			[[PSDataPersistenceManager sharedManager] saveAppointmentSurveyData:surveyDataDictionary];
	}
	else
	{
			 [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
	}
	//    if (self.progressBlock) {
	//		self.progressBlock(1, 1); // to indicate that download has completed!
	//	}
}

#pragma mark - PSFetchOilSurveyFormServiceDelegate

-(void) service:(PSFetchOilServicingSurveyService *)service didFinishLoadingOilSurvey:(id)oilSurvey{
    if(!isEmpty(oilSurvey)){
        NSDictionary *responseDictionary = [oilSurvey JSONValue];
        NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
        if(statusCode == kSuccessCode)
        {
            NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
            [[PSDataPersistenceManager sharedManager] saveFetchedOilHeatingData:surveyDataDictionary];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                object:nil];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                            object:nil];
    }
    
}

#pragma mark - PSFetchAlternativeSurveyFormServiceDelegate

-(void) service:(PSFetchAlternativeFuelSurveyService *)service didFinishLoadingAlternativeSurvey:(id)alternativeSurvey{
    if(!isEmpty(alternativeSurvey)){
        NSDictionary *responseDictionary = [alternativeSurvey JSONValue];
        NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
        if(statusCode == kSuccessCode)
        {
            NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
            [[PSDataPersistenceManager sharedManager] saveFetchedAlternativeHeatingData:surveyDataDictionary];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                object:nil];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                            object:nil];
    }
    
}

#pragma mark - PSFetchAppliancesServiceDelegate
- (void) service:(PSFetchAppliancesService *)service didFinishLoadingAppliances:(id)appliancesData
{
	NSDictionary *responseDictionary = [appliancesData JSONValue];
	NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
	if(statusCode == kSuccessCode)
	{
		NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
		[[PSDataPersistenceManager sharedManager] saveAppliances:surveyDataDictionary];
        
        if([self getType] == AppointmentTypeGas || [self getType] == AppointmentTypeGasVoid)
        {
            [self performSelector:@selector(fetchPropertyApplianceData) withObject:nil afterDelay:5.0];
        }
        
        else if([self getType] == AppointmentTypeAlternativeFuels)
        {
            [self performSelector:@selector(fetchAlternativeFuelSurvey) withObject:nil afterDelay:5.0];
        }
        else if([self getType] == AppointmentTypeOilServicing){
            [self performSelector:@selector(fetchOilServicingSurvey) withObject:nil afterDelay:5.0];
        }
        
		
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
	}
}

#pragma mark - Delegates Error Method
- (void) service:(id)service didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyDownloadServiceNetworkFailed
                                                                        object:nil];
	[self setIsDownloadInProgress:NO];
}

@end
