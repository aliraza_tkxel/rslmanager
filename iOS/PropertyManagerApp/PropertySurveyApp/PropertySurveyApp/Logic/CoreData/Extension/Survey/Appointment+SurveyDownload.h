//
//  Appointment+SurveyDownload.h
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 24/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appointment+CoreDataClass.h"
#import "PSFetchSurveyFormService.h"
#import "PSFetchAppliancesService.h"
#import "PSFetchPropertyAppliancesService.h"
#import "PSFetchAlternativeFuelSurveyService.h"
#import "PSFetchOilServicingSurveyService.h"
NS_ASSUME_NONNULL_BEGIN

@interface Appointment (SurveyDownload) <PSFetchSurveyFormServiceDelegate, PSFetchAppliancesServiceDelegate, PSFetchAlternativeFuelSurveyServiceDelegate,PSFetchOilServicingSurveyServiceDelegate>

typedef void (^AppointmentSurveyDataProgressiveOperationProgressBlock)(long long totalBytesRead, long long totalBytesExpected);


@property (nonatomic, strong) PSFetchAlternativeFuelSurveyService *fetchAlternativeSurveyFormService;
@property (nonatomic, strong) PSFetchOilServicingSurveyService *fetchOilSurveyFormService;
@property (nonatomic, strong) PSFetchSurveyFormService *fetchSurveyFormService;
/*@property (nonatomic, strong) PSFetchAppliancesService *fetchAppliancesService;
@property (nonatomic, strong) PSFetchPropertyAppliancesService *fetchPropertyAppliancesService;*/
@property (nonatomic, assign) BOOL isDownloadInProgress;
@property (nonatomic, copy) AppointmentSurveyDataProgressiveOperationProgressBlock progressBlock;

- (void)prepareForOffline;

@end

NS_ASSUME_NONNULL_END
