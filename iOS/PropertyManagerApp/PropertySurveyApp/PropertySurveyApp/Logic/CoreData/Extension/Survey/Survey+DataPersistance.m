//
//  Survey+DataPersistance.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 09/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Survey+DataPersistance.h"
#import "SurveyData.h"
#import "UtilityClass.h"
#import "SettingsClass.h"
#import "SurveyData+DataPersistance.h"

#define kSurveyDataItemId @"itemId"


@implementation Survey (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary {
	__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
	SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId] inManagedObjectContext:managedObjectContext];
	self.surveyToAppointment.isModified = [NSNumber numberWithBool:YES];
	self.surveyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
	surveyData.appointmentId = self.surveyToAppointment.appointmentId;
	surveyData.customerId = [[SettingsClass sharedObject] currentUserId];
	surveyData.itemId = [surveyDataDictionary objectForKey:kSurveyDataItemId];
	surveyData.propertyId = surveyData.surveyDataToSurvey.surveyToAppointment.appointmentToProperty.propertyId;
	surveyData.surveyFormName = [formDataDictionary objectForKey:@"surveyName"];
    
	[surveyData saveFormDataDictionary:formDataDictionary inManagedObjectContext:managedObjectContext];
    
	[managedObjectContext performBlock: ^{
		// Save the context.
		NSError *error = nil;
		if (![managedObjectContext save:&error])
		{
			CLS_LOG(@"Error in Saving MOC: %@", [error description]);
		}
	}]; // main
}

- (void)saveSurveyNotes:(NSString *)notesString forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary {
	__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
	SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId] inManagedObjectContext:managedObjectContext];
	self.surveyToAppointment.isModified = [NSNumber numberWithBool:YES];
	surveyData.appointmentId = self.surveyToAppointment.appointmentId;
	surveyData.customerId = [[SettingsClass sharedObject] currentUserId];
	surveyData.propertyId = surveyData.surveyDataToSurvey.surveyToAppointment.appointmentToProperty.propertyId;
	surveyData.itemId = [surveyDataDictionary objectForKey:kSurveyDataItemId];
	surveyData.surveyNotesDetail = notesString;
	surveyData.surveyNotesDate = [NSDate date];

	[managedObjectContext performBlock: ^{
		// Save the context.
		NSError *error = nil;
		if (![managedObjectContext save:&error])
		{
			CLS_LOG(@"Error in Saving MOC: %@", [error description]);
		}
	}]; // main
}

- (SurveyData *)surveyDataWithItemId:(NSString *)itemId
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@", kSurveyDataItemId, itemId];
	NSSet *filteredObjects = [[self surveyToSurveyData] filteredSetUsingPredicate:predicate];
	SurveyData *surveyData = nil;

	if ([filteredObjects count] > 0)
	{
		surveyData = [[filteredObjects allObjects] objectAtIndex:0];
	}
	return surveyData;
}

- (NSInteger)surveyFieldsFilled:(NSDictionary *)surveyDataDictionary
{
    
	SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId]];
	NSInteger filled = 0;
	NSArray *fields = [surveyDataDictionary objectForKey:@"Order"];
	for (NSString *field in fields)
	{
		NSDictionary *information = [[surveyDataDictionary objectForKey:@"Fields"] objectForKey:field];
		FormData * form=[surveyData formDataWithItemId:[information objectForKey:@"itemParamId"] controlType:[information objectForKey:@"Type"]];
		if (!isEmpty(form.surveyParamItemFieldValue) || [[information objectForKey:@"Selected"] count] > 0)
		{
			filled++;
		}
		else
		{
			if([self checkForSelectedKeyInDictionary:information])
			{
				filled++;
			}
		}
	}
	return filled;
}

- (BOOL)checkForSelectedKeyInDictionary:(NSDictionary *)dictionaryToCheck
{
	if ([[dictionaryToCheck objectForKey:@"Selected"] count] > 0)
	{
		return YES;
	}
	else
	{
		for (NSString *keyStrings in [dictionaryToCheck allKeys])
		{
			if ([[dictionaryToCheck objectForKey:keyStrings] isKindOfClass:[NSDictionary class]])
			{
				return [self checkForSelectedKeyInDictionary:[dictionaryToCheck objectForKey:keyStrings]];
			}
		}
		return NO;
	}
}

#pragma mark - Private Methods
- (SurveyData *)surveyDataWithItemId:(NSString *)itemId inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	SurveyData *surveyData = [self surveyDataWithItemId:itemId];
    
	if (!surveyData)
	{ // object is not found so we need to add a new one.
		surveyData = [NSEntityDescription insertNewObjectForEntityForName:@"SurveyData" inManagedObjectContext:managedObjectContext];
		Survey *survey = (Survey *)[managedObjectContext objectWithID:self.objectID];
		[survey addSurveyToSurveyDataObject:surveyData];
	}
    
	return surveyData;
}
@end
