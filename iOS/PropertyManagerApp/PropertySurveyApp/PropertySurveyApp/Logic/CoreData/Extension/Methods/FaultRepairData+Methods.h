//
//  FaultRepairData (Methods).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FaultRepairData+CoreDataClass.h"

@interface FaultRepairData (Methods)

@property (nonatomic, retain) NSString *primitiveRepairSectionIdentifier;

- (NSString *) repairSectionIdentifier;

@end
