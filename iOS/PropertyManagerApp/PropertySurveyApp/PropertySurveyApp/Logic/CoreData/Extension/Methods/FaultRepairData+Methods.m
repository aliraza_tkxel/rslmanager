//
//  FaultRepairData (Methods).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FaultRepairData+Methods.h"

@implementation FaultRepairData (Methods)

@dynamic primitiveRepairSectionIdentifier;

/*!
 @discussion
 Repair List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the repair list sections.
 */
- (NSString *) repairSectionIdentifier {
	
	// Create and cache the section identifier on demand.
	
	[self willAccessValueForKey:kRepairSectionIdentifier];
	NSString *sectionIdentifier = [self primitiveRepairSectionIdentifier];
	[self didAccessValueForKey:kRepairSectionIdentifier];
	
	if (!sectionIdentifier)
	{
		sectionIdentifier = [[[self.faultRepairDescription trimWhitespace] substringToIndex:1] uppercaseString];
		if(![sectionIdentifier isAlphabeticChar])
		{
			sectionIdentifier = @"#";
		}
		[self setPrimitiveRepairSectionIdentifier:sectionIdentifier];
	}
	return sectionIdentifier;
}

@end
