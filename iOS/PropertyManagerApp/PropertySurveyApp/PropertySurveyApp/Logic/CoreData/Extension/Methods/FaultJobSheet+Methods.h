//
//  FaultJobSheet (Methods).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FaultJobSheet+CoreDataProperties.h"
#import "JobSheet+Methods.h"

@interface FaultJobSheet (Methods)

- (NSString *) getFaultDescription;
- (NSString *) stringForJSNLabel;

@end
