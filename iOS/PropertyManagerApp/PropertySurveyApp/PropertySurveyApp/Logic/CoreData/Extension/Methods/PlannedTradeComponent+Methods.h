//
//  PlannedTradeComponent+Methods.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PlannedTradeComponent.h"

@interface PlannedTradeComponent (Methods)

- (void) startJob;
- (JobStatus) getStatus;
@end
