//
//  FaultJobSheet (Methods).m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "FaultJobSheet+Methods.h"
#import "JobSheet+Methods.h"

@implementation FaultJobSheet (Methods)

- (NSString *)getFaultDescription
{
	NSMutableString *faultDescription = [NSMutableString stringWithFormat:@""];
	if([self.jsType compare:kJSTYPE_SANI_CLEAN] == NSOrderedSame ||
		 [self.jsType compare:kJSTYPE_STANDARD_VOID_WORKS] == NSOrderedSame)
	{
		if (!isEmpty(self.jsType))
		{
			[faultDescription appendString:[NSString stringWithFormat:@"%@ > ",self.jsType]];
		}
	}
	else
	{
		if (!isEmpty(self.jsnLocation))
		{
			[faultDescription appendString:[NSString stringWithFormat:@"%@ > ",self.jsnLocation]];
		}
	}
	if (!isEmpty(self.jsnDescription))
	{
		[faultDescription appendString:self.jsnDescription];
	}
	return faultDescription;
}

- (NSString *) stringForJSNLabel
{
	NSString *JSNString = [NSString stringWithFormat:@"%@: %@",self.jsNumber,self.priority];
	return JSNString;
}

@end
