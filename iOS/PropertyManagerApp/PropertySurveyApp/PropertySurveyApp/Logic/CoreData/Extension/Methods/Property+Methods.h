//
//  Property+Methods.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Property+CoreDataClass.h"

@interface Property (Methods)

- (NSString *) addressWithStyle:(PropertyAddressStyle)addressStyle;

@end
