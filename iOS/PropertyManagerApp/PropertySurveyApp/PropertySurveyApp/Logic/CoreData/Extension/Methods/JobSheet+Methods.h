//
//  JobSheet+Methods.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobSheet+CoreDataClass.h"

#define kJSTYPE_SANI_CLEAN           @"Sani Clean"
#define kJSTYPE_STANDARD_VOID_WORKS  @"Standard Void Works"

@interface JobSheet (Methods)

- (void) startJob;
- (JobStatus) getStatus;

@end
