//
//  DefectPicture+MWPhoto.h
//  PropertySurveyApp
//
//  Created by TkXel on 03/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "DefectPicture+CoreDataClass.h"
#import "MWPhotoProtocol.h"

@interface DefectPicture (MWPhoto) <MWPhoto,FBRWebImageManagerDelegate>

@end
