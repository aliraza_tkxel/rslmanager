//
//  AlternativeFuelPicture+MWPhoto.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AlternativeFuelPicture+CoreDataClass.h"
#import "MWPhotoProtocol.h"
@interface AlternativeFuelPicture (MWPhoto)<MWPhoto,FBRWebImageManagerDelegate>

@end
