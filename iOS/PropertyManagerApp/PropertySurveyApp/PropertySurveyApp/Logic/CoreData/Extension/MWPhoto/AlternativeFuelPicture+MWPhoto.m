//
//  AlternativeFuelPicture+MWPhoto.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AlternativeFuelPicture+MWPhoto.h"
#import "AlternativeFuelPicture+JSON.h"
@implementation AlternativeFuelPicture (MWPhoto)
-(UIImage *) underlyingImage
{
    return [[FBRWebImageManager sharedManager] imageWithURL:[NSURL URLWithString:[self getPicturePath]] ];
}

-(void) loadUnderlyingImageAndNotify
{
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager downloadWithURL:[NSURL URLWithString:self.imagePath] delegate:self];
}

- (void)unloadUnderlyingImage
{
    
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                        object:self];
    //notify here with image
    
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                        object:nil];
}

-(void) deletePhotograph
{
    [[PSDataUpdateManager sharedManager] deletePropertyPicture:self];
}

@end
