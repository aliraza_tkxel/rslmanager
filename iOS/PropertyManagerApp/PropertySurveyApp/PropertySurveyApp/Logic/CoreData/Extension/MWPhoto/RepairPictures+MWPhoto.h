//
//  RepairPictures+MWPhoto.h
//  PropertySurveyApp
//
//  Created by TkXel on 10/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "RepairPictures.h"
#import "MWPhotoProtocol.h"

@interface RepairPictures (MWPhoto) <MWPhoto,FBRWebImageManagerDelegate>


@end
