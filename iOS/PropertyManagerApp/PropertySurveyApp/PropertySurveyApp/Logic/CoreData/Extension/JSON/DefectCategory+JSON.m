//
//  DefectCategory+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DefectCategory+JSON.h"

@implementation DefectCategory (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *defectCategoryDictionary = [NSMutableDictionary dictionary];
    [defectCategoryDictionary setObject:isEmpty(self.categoryDescription)?[NSNull null]:self.categoryDescription forKey:kDefectCategoryDescription];
    [defectCategoryDictionary setObject:isEmpty(self.categoryID)?[NSNull null]:self.categoryID forKey:kDefectCategoryID];
    
    return defectCategoryDictionary;
}
@end
