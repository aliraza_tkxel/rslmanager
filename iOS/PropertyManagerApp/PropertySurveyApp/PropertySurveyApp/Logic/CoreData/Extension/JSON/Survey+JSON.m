//
//  Survey+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Survey+JSON.h"
#import "SurveyData+JSON.h"

@implementation Survey (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *surveyDictionary = [NSMutableDictionary dictionary];
    NSMutableArray *surveys = [NSMutableArray array];
    if(!isEmpty(self.surveyToSurveyData))
    {
        for(SurveyData *surveyData in self.surveyToSurveyData)
        {
            [surveys addObject:[surveyData JSONValue]];
        }
    }
    [surveyDictionary setObject:isEmpty(surveys)?[NSNull null]:surveys forKey:@"surveyData"];
    return surveyDictionary;
}

@end
