//
//  AppointmentHistory+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AppointmentHistory+JSON.h"

@implementation AppointmentHistory (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    NSString *serverDate = isEmpty([UtilityClass convertNSDateToServerDate:self.actionDate])?nil:[UtilityClass convertNSDateToServerDate:self.actionDate];
    [mutDict setObject:serverDate forKey:kAppointmentHistoryActionDate];
    [mutDict setObject:self.actionBy forKey:kAppointmentHistoryActionBy];
    [mutDict setObject:self.actionType forKey:kAppointmentHistoryActionType];
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
