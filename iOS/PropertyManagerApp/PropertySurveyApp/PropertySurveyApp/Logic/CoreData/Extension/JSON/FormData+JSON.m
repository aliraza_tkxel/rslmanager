//
//  FormData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FormData+JSON.h"

@implementation FormData (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *formDataDictionary = [NSMutableDictionary dictionary];
    [formDataDictionary setObject:isEmpty(self.controlType)?[NSNull null]:self.controlType forKey:@"controlType"];
    [formDataDictionary setObject:isEmpty(self.surveyItemParamId)?[NSNull null]:self.surveyItemParamId forKey:@"surveyItemParamId"];
    [formDataDictionary setObject:isEmpty(self.surveyParameterId)?[NSNull null]:self.surveyParameterId forKey:@"surveyParameterId"];
    [formDataDictionary setObject:isEmpty(self.surveyParamName)?[NSNull null]:self.surveyParamName forKey:@"surveyParamName"];

    NSMutableArray *surveyParamItemField = nil;
    
    if ([self.surveyParamItemFieldValue isKindOfClass:[NSDictionary class]])
    {
        NSArray *allKeys = [(NSDictionary *)self.surveyParamItemFieldValue allKeys];
        surveyParamItemField = [NSMutableArray arrayWithCapacity:[allKeys count]];
        
        for (NSString *key in allKeys) {
            NSMutableDictionary *optionDictionary = [NSMutableDictionary dictionary];
            NSNumber *paramId = [NSNumber numberWithInteger:[[[self.surveyParamItemFieldValue valueForKey:key] valueForKey:@"paramId"] integerValue]];
            NSNumber *IsCheckBoxSelected = [NSNumber numberWithInteger:[[[self.surveyParamItemFieldValue valueForKey:key] valueForKey:@"IsCheckBoxSelected"] integerValue]];
            
            [optionDictionary setValue:key forKey:@"surveyParamItemFieldValue"];
            [optionDictionary setValue:isEmpty(paramId)?[NSNull null]:paramId forKey:@"surveyPramItemFieldId"];
            [optionDictionary setObject:isEmpty(IsCheckBoxSelected)?[NSNull null]:IsCheckBoxSelected forKey:@"isSelected"];
            
            [surveyParamItemField addObject:optionDictionary];
        }
    }
    else
    {
        
        surveyParamItemField = [NSMutableArray arrayWithCapacity:1];
        NSMutableDictionary *optionDictionary = [NSMutableDictionary dictionary];
        
        [optionDictionary setObject:isEmpty(self.surveyParamItemFieldValue)?[NSNull null]:self.surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
        [optionDictionary setValue:isEmpty(self.surveyPramItemFieldId)?[NSNull null]:self.surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
        [optionDictionary setObject:isEmpty(self.isSelected)?[NSNull null]:self.isSelected forKey:@"isSelected"];
        
        [surveyParamItemField addObject:optionDictionary];
    }
    
    [formDataDictionary setObject:isEmpty(surveyParamItemField)?[NSNull null]:surveyParamItemField forKey:@"surveyParamItemField"];
    
    
    return formDataDictionary;
}


@end
