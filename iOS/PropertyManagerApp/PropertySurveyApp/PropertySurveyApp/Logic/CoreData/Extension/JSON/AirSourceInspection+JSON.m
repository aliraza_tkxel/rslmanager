//
//  AirSourceInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AirSourceInspection+JSON.h"

@implementation AirSourceInspection (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    [mutDict setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [mutDict setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [mutDict setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [mutDict setObject:isEmpty(self.checkWaterSupplyTurnedOff)?[NSNull null]:self.checkWaterSupplyTurnedOff forKey:kCheckWaterSupplyTurnedOff];
    [mutDict setObject:isEmpty(self.turnedOffDetail)?[NSNull null]:self.turnedOffDetail forKey:kTurnedOffDetail];
    [mutDict setObject:isEmpty(self.checkWaterSupplyTurnedOn)?[NSNull null]:self.checkWaterSupplyTurnedOn forKey:kCheckWaterSupplyTurnedOn];
    [mutDict setObject:isEmpty(self.turnedOnDetail)?[NSNull null]:self.turnedOnDetail forKey:kTurnedOnDetail];
    [mutDict setObject:isEmpty(self.checkValves)?[NSNull null]:self.checkValves forKey:kCheckValves];
    [mutDict setObject:isEmpty(self.valveDetail)?[NSNull null]:self.valveDetail forKey:kValveDetail];
    [mutDict setObject:isEmpty(self.checkSupplementaryBonding)?[NSNull null]:self.checkSupplementaryBonding forKey:kCheckSupplementaryBonding];
    [mutDict setObject:isEmpty(self.supplementaryBondingDetail)?[NSNull null]:self.supplementaryBondingDetail forKey:kSupplementaryBondingDetail];
    [mutDict setObject:isEmpty(self.checkFuse)?[NSNull null]:self.checkFuse forKey:kCheckFuse];
    [mutDict setObject:isEmpty(self.fuseDetail)?[NSNull null]:self.fuseDetail forKey:kFuseDetail];
    [mutDict setObject:isEmpty(self.checkThermostat)?[NSNull null]:self.checkThermostat forKey:kCheckThermostat];
    [mutDict setObject:isEmpty(self.thermostatDetail)?[NSNull null]:self.thermostatDetail forKey:kThermostatDetail];
    [mutDict setObject:isEmpty(self.checkOilLeak)?[NSNull null]:self.checkOilLeak forKey:kCheckOilLeak];
    [mutDict setObject:isEmpty(self.oilLeakDetail)?[NSNull null]:self.oilLeakDetail forKey:kOilLeakDetail];
    [mutDict setObject:isEmpty(self.checkWaterPipework)?[NSNull null]:self.checkWaterPipework forKey:kCheckWaterPipework];
    [mutDict setObject:isEmpty(self.pipeworkDetail)?[NSNull null]:self.pipeworkDetail forKey:kPipeworkDetail];
    [mutDict setObject:isEmpty(self.checkElectricConnection)?[NSNull null]:self.checkElectricConnection forKey:kCheckElectricConnection];
    [mutDict setObject:isEmpty(self.connectionDetail)?[NSNull null]:self.connectionDetail forKey:kConnectionDetail];
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
