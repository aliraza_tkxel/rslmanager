//
//  Appointment+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appointment+JSON.h"
#import "Customer+JSON.h"
#import "Property+JSON.h"
#import "AppInfoData+JSON.h"
#import "CP12Info+JSON.h"
#import "JobSheet+JSON.h"
#import "FaultJobSheet+JSON.h"
#import "DefectJobSheet+JSON.h"
#import "Journal+JSON.h"
#import "Survey+JSON.h"
#import "Appointment+CoreDataClass.h"
#import "Scheme+JSON.h"
#import "VoidData.h"
#import "WorkRequired+CoreDataClass.h"
#import "RoomData.h"
#import "MajorWorkRequired.h"
#import "PaintPack.h"
#import "MeterData.h"
#import "MeterType.h"
#import "FaultReported+JSON.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"


@implementation Appointment (JSON)

- (NSDictionary *) JSONValue
{
	NSMutableDictionary *appointmentDictionary = [NSMutableDictionary dictionary];
	AppointmentType type = [self getType];
	switch (type)
	{
		case AppointmentTypePreVoid:
		case AppointmentTypePostVoid:
			[self createVoidInspectionAppointmentData:appointmentDictionary];
			break;
		case AppointmentTypeGasCheck:
		case AppointmentTypeElectricCheck:
			[self createVoidGasElectricAppointmentData:appointmentDictionary];
			break;
		case AppointmentTypeVoidWorkRequired:
			[self createVoidWorkRequiredAppointmentData:appointmentDictionary];
			break;
		case AppointmentTypeDefect:
			[self createDefectAppointmentData:appointmentDictionary];
			break;
		default:
			[self createAllAppointmentList:appointmentDictionary];
			break;
	}
    [self addBuildAndCompletionInfoToAppointmentDic:appointmentDictionary];
	return appointmentDictionary;
}

-(void)createVoidWorkRequiredAppointmentData:(NSMutableDictionary *)appointmentDictionary
{
	// Void Works Appointment Identifier
	[appointmentDictionary setObject:@"VoidWorksAppointmentData:#PropSurvey.Contracts.Data.Void" forKey:@"__type"];
    
    
    NSMutableArray *appointmentHistoryList = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToAppointmentHistory)){
        for(AppointmentHistory *hist in self.appointmentToAppointmentHistory){
            [appointmentHistoryList addObject:[hist JSONValue]];
        }
    }
    [appointmentDictionary setObject:appointmentHistoryList forKey:kAppointmentHistoryJSONEntry];
    
    
	NSMutableArray *jobDataListArray = [NSMutableArray array];
	for (FaultJobSheet *jobData in self.appointmentToJobSheet)
	{
		[jobDataListArray addObject:[jobData JSONValue]];
	}
	[appointmentDictionary setObject:jobDataListArray forKey:kAppointmentToJobDataList];
	
	[self createVoidCommonStructure:appointmentDictionary];
}

-(void)createVoidGasElectricAppointmentData:(NSMutableDictionary *)appointmentDictionary
{
	// Gas & Electric Check Appointment Identifier
	[appointmentDictionary setObject:@"VoidGasElectricData:#PropSurvey.Contracts.Data.Void" forKey:@"__type" ];
	
	MeterType * meterType = self.appointmentToMeterData.meterDataToMeterType;
	if(meterType == NULL)
	{
		[appointmentDictionary setObject:[NSNull null] forKey:@"meterLocation"];
		[appointmentDictionary setObject:[NSNull null] forKey:@"meterReading"];
		[appointmentDictionary setObject:self.appointmentToMeterData.deviceType forKey:@"deviceType"];
		[appointmentDictionary setObject:[NSNull null] forKey:@"tenantType"];
		[appointmentDictionary setObject:[NSNull null] forKey:@"meterType"];
		[appointmentDictionary setObject:[NSNull null] forKey:@"meterTypeId"];
	}
	else
	{
		[appointmentDictionary setObject:self.appointmentToMeterData.meterLocation forKey:@"meterLocation"];
		[appointmentDictionary setObject:self.appointmentToMeterData.meterReading forKey:@"meterReading"];
		[appointmentDictionary setObject:self.appointmentToMeterData.deviceType forKey:@"deviceType"];
		[appointmentDictionary setObject:self.appointmentToMeterData.tenantType forKey:@"tenantType"];
		[appointmentDictionary setObject:meterType.meterType forKey:@"meterType"];
		[appointmentDictionary setObject:meterType.meterId forKey:@"meterTypeId"];
	}
    
    NSMutableArray *appointmentHistoryList = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToAppointmentHistory)){
        for(AppointmentHistory *hist in self.appointmentToAppointmentHistory){
            [appointmentHistoryList addObject:[hist JSONValue]];
        }
    }
    [appointmentDictionary setObject:appointmentHistoryList forKey:kAppointmentHistoryJSONEntry];
    
	[self createVoidCommonStructure:appointmentDictionary];
}

-(void)createVoidInspectionAppointmentData:(NSMutableDictionary *)appointmentDictionary
{
	// Pre & Post Inspection Structure Identifier
	[appointmentDictionary setObject:@"VoidInspectionAppointmentData:#PropSurvey.Contracts.Data.Void" forKey:@"__type" ];
    
    NSMutableArray *appointmentHistoryList = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToAppointmentHistory)){
        for(AppointmentHistory *hist in self.appointmentToAppointmentHistory){
            [appointmentHistoryList addObject:[hist JSONValue]];
        }
    }
    [appointmentDictionary setObject:appointmentHistoryList forKey:kAppointmentHistoryJSONEntry];
    
	// Electric Check Fields
	[appointmentDictionary setObject:self.appointmentToVoidData.isElectricCheckRequired forKey:@"isElectricCheckRequired"];
	[appointmentDictionary setObject:self.appointmentToVoidData.electricCheckStatus forKey:@"electricCheckStatus"];
	if(self.appointmentToVoidData.electricCheckDate != NULL)
	{
		[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentToVoidData.electricCheckDate] forKey:@"electricCheckDate"];
	}
	// Gas Check Fields
	[appointmentDictionary setObject:self.appointmentToVoidData.isGasCheckRequired forKey:@"isGasCheckRequired"];
	[appointmentDictionary setObject:self.appointmentToVoidData.gasCheckStatus forKey:@"gasCheckStatus"];
	if(self.appointmentToVoidData.gasCheckDate != NULL)
	{
		[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentToVoidData.gasCheckDate] forKey:@"gasCheckDate"];
	}
	// Other Fields
	[appointmentDictionary setObject:self.appointmentToVoidData.isAsbestosCheckRequired forKey:@"isAsbestosCheckRequired"];
	[appointmentDictionary setObject:self.appointmentToVoidData.isEPCCheckRequired forKey:@"isEPCCheckRequired"];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentToVoidData.reletDate] forKey:@"reletDate"];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentToVoidData.terminationDate] forKey:@"terminationDate"];
	
	NSDictionary * worksRequired = [self getWorkRequiredDictionary];
	[appointmentDictionary setObject:worksRequired forKey:@"worksRequired"];
	
	NSDictionary * majorWorksRequired = [self getMajorWorkRequiredDictionary];
	[appointmentDictionary setObject:majorWorksRequired forKey:@"majorWorksRequired"];
	
	NSDictionary * paintPacks = [self getPaintPackDictionary];
	[appointmentDictionary setObject:paintPacks forKey:@"paintPacks"];
    
	
	[self createVoidCommonStructure:appointmentDictionary];
}

-(void)createAllAppointmentList:(NSMutableDictionary *)appointmentDictionary
{
    // Adding abort reason and notes
    
    if([self.appointmentStatus isEqualToString:kAppointmentStatusAborted])
    {
        if(!isEmpty(self.appointmentAbortReason))
        {
            [appointmentDictionary setObject:self.appointmentAbortReason forKey:kAppointmentAbortReason];
        }            
        
        if(!isEmpty(self.appointmentAbortNotes))
        {
            [appointmentDictionary setObject:self.appointmentAbortNotes forKey:kAppointmentAbortNotes];
        }
    }
    
	// All Previous (Before Void Tenancy Module) Structures Identifier
	[appointmentDictionary setObject:@"AllAppointmentsList:#PropSurvey.Contracts.Data" forKey:@"__type"];
	
	NSString *appointmentDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentDate])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentDate];
	NSString *appointmentStartTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentStartTime])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentActualStartTime];
	NSString *appointmentEndTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentEndTime])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentActualEndTime];
	NSString *loggedDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.loggedDate])?nil:[UtilityClass convertNSDateToServerDate:self.loggedDate];
	NSString *repairCompletionDateTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.repairCompletionDateTime])?nil:[UtilityClass convertNSDateToServerDate:self.repairCompletionDateTime];
	
	id surveyCheckListForm = nil;
    
   
    
	[appointmentDictionary setObject:isEmpty(appointmentDate)?[NSNull null]:appointmentDate forKey:kAppointmentDate];
	[appointmentDictionary setObject:isEmpty(appointmentStartTime)?[NSNull null]:appointmentStartTime forKey:kAppointmentStartTime];
	[appointmentDictionary setObject:isEmpty(appointmentEndTime)?[NSNull null]:appointmentEndTime forKey:kAppointmentEndTime];
	
	[appointmentDictionary setObject:isEmpty(self.appointmentId)?[NSNull null]:self.appointmentId forKey:kAppointmentId];
	[appointmentDictionary setObject:isEmpty(self.appointmentLocation)?[NSNull null]:self.appointmentLocation forKey:kAppointmentLocation];
	[appointmentDictionary setObject:isEmpty(self.appointmentCalendar)?[NSNull null]:self.appointmentCalendar forKey:kAppointmentCalendar];
	[appointmentDictionary setObject:isEmpty(self.appointmentNotes)?[NSNull null]:self.appointmentNotes forKey:kAppointmentNotes];
	[appointmentDictionary setObject:isEmpty(self.appointmentShift)?[NSNull null]:self.appointmentShift forKey:kAppointmentShift];
	[appointmentDictionary setObject:isEmpty(self.appointmentStatus)?[NSNull null]:self.appointmentStatus forKey:kAppointmentStatus];
	[appointmentDictionary setObject:isEmpty(self.appointmentTitle)?[NSNull null]:self.appointmentTitle forKey:kAppointmentTitle];
	[appointmentDictionary setObject:isEmpty(self.appointmentType)?[NSNull null]:self.appointmentType forKey:kAppointmentType];
	[appointmentDictionary setObject:isEmpty(self.assignedTo)?[NSNull null]:self.assignedTo forKey:kAssignedTo];
	[appointmentDictionary setObject:isEmpty(self.createdBy)?[NSNull null]:self.createdBy forKey:kCreatedBy];
	[appointmentDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerId)?[NSNull null]:self.defaultCustomerId forKey:kDefaultCustomerId];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerIndex)?[NSNull null]:self.defaultCustomerIndex forKey:kDefaultCustomerIndex];
	[appointmentDictionary setObject:isEmpty(self.journalHistoryId)?[NSNull null]:self.journalHistoryId forKey:kJournalHistoryId];
	[appointmentDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:kJournalId];
	[appointmentDictionary setObject:isEmpty(self.jsgNumber)?[NSNull null]:self.jsgNumber forKey:kJsgNumber];
	[appointmentDictionary setObject:isEmpty(loggedDate)?[NSNull null]:loggedDate forKey:kLoggedDate];
	[appointmentDictionary setObject:isEmpty(self.noEntryNotes)?[NSNull null]:self.noEntryNotes forKey:kNoEntryNotes];
	[appointmentDictionary setObject:isEmpty(repairCompletionDateTime)?[NSNull null]:repairCompletionDateTime forKey:kRepairCompletionDateTime];
	[appointmentDictionary setObject:isEmpty(self.surveyorAlert)?[NSNull null]:self.surveyorAlert forKey:kSurveyorAlert];
	[appointmentDictionary setObject:isEmpty(self.surveyorUserName)?[NSNull null]:self.surveyorUserName forKey:kSurveyorUserName];
	[appointmentDictionary setObject:isEmpty(self.surveyourAvailability)?[NSNull null]:self.surveyourAvailability forKey:kSurveyourAvailability];
	[appointmentDictionary setObject:isEmpty(self.surveyType)?[NSNull null]:self.surveyType forKey:kSurveyType];
	[appointmentDictionary setObject:isEmpty(self.tenancyId)?[NSNull null]:self.tenancyId forKey:kTenancyId];
	[appointmentDictionary setObject:isEmpty(self.isMiscAppointment)?[NSNull null]:self.isMiscAppointment forKey:kIsMiscAppointment];
	[appointmentDictionary setObject:isEmpty(self.addToCalendar)?[NSNull null]:self.addToCalendar forKey:kAppointmentAddtoCalendar];
	
	NSDictionary *appInfo = [NSDictionary dictionaryWithDictionary:[self.appointmentToAppInfoData JSONValue]];
    
	NSMutableDictionary *appCP12 = [NSMutableDictionary dictionaryWithDictionary:[self.appointmentToCP12Info JSONValue]];
    NSMutableArray *cp12List = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToProperty)){
        if(!isEmpty(self.appointmentToProperty.propertyToBoiler)){
            /*Repeat the same Certificate info for all Boilers and add heating Id*/
            NSArray *blrArr = [self.appointmentToProperty.propertyToBoiler allObjects];
            for(Boiler *boiler in blrArr){
                NSMutableDictionary *cp12Copy = [[NSMutableDictionary alloc] initWithDictionary:appCP12];
                [cp12Copy setObject:isEmpty(boiler.heatingId)?[NSNull null]:boiler.heatingId forKey:kHeatingId];
                [cp12List addObject:cp12Copy];
            }
        }
        else{
            [cp12List addObject:appCP12];
        }
        
    }
    else{
        [cp12List addObject:appCP12];
    }
	
    if([self getType]==AppointmentTypeAlternativeFuels){
        if(!isEmpty(self.appointmentToProperty)){
            if(!isEmpty(self.appointmentToProperty.propertyToAlternativeHeating)){
                [cp12List removeAllObjects];
                for(AlternativeHeating *heter in self.appointmentToProperty.propertyToAlternativeHeating){
                    NSMutableDictionary *cp12Copy = [[NSMutableDictionary alloc] initWithDictionary:appCP12];
                    [cp12Copy setObject:isEmpty(heter.heatingId)?[NSNull null]:heter.heatingId forKey:kHeatingId];
                    [cp12Copy setObject:isEmpty(self.jsgNumber)?[NSNull null]:self.jsgNumber forKey:kJsgNumber];
                    [cp12Copy setObject:isEmpty(self.appointmentToJournal.journalId)?[NSNull null]:self.appointmentToJournal.journalId forKey:kJournalId];
                    [cp12List addObject:cp12Copy];
                }
            }
        }
    }
    if([self getType]==AppointmentTypeOilServicing){
        if(!isEmpty(self.appointmentToProperty)){
            if(!isEmpty(self.appointmentToProperty.propertyToOilHeatings)){
                [cp12List removeAllObjects];
                for(OilHeating *heter in self.appointmentToProperty.propertyToOilHeatings){
                    NSMutableDictionary *cp12Copy = [[NSMutableDictionary alloc] initWithDictionary:appCP12];
                    [cp12Copy setObject:isEmpty(heter.heatingId)?[NSNull null]:heter.heatingId forKey:kHeatingId];
                    [cp12Copy setObject:isEmpty(self.jsgNumber)?[NSNull null]:self.jsgNumber forKey:kJsgNumber];
                    [cp12Copy setObject:isEmpty(self.appointmentToJournal.journalId)?[NSNull null]:self.appointmentToJournal.journalId forKey:kJournalId];
                    [cp12List addObject:cp12Copy];
                }
            }
        }
    }
    
    
	NSMutableArray *faultReportedList = [NSMutableArray array];
	for (FaultReported *faultReported in self.appointmentToFaultReported) {
		[faultReportedList addObject:[faultReported JSONValue]];
	}
	NSMutableArray *customerList = [NSMutableArray array];
	for (Customer *customer in self.appointmentToCustomer) {
		[customerList addObject:[customer JSONValue]];
	}
	
	if(self.appointmentToProperty)
		
	{
		NSDictionary *propertyData = [NSDictionary dictionaryWithDictionary:[self.appointmentToProperty JSONValue]];
		[appointmentDictionary setObject:propertyData forKey:kAppointmentToProperty];
	}
	else
	{
		NSDictionary * schemeData = [NSDictionary dictionaryWithDictionary:[self.appointmentToScheme JSONValue]];
		[appointmentDictionary setObject:schemeData forKey:kAppointmentToScheme];
	}
	NSMutableArray *jobDataListArray = [NSMutableArray array];
	for (JobSheet * jobData in self.appointmentToJobSheet)
	{
		[jobDataListArray addObject:[jobData JSONValue]];
	}
	
	NSDictionary *tradeComponent = [NSDictionary dictionaryWithDictionary:[self.appointmentToPlannedComponent JSONValue]];
	[appointmentDictionary setObject:tradeComponent forKey:kAppointmentToComponentTrade];
	
	NSDictionary *journalData = [NSDictionary dictionaryWithDictionary:[self.appointmentToJournal JSONValue]];
	NSDictionary *surveyDicitionary = [NSDictionary dictionaryWithDictionary:[self.appointmentToSurvey JSONValue]];
	[appointmentDictionary setObject:appInfo forKey:kAppointmentToAppInfoData];
	[appointmentDictionary setObject:cp12List forKey:kAppointmentToCP12Info];
	[appointmentDictionary setObject:customerList forKey:kAppointmentToCustomer];
	[appointmentDictionary setObject:faultReportedList forKey:@"faultReportedList"];
	[appointmentDictionary setObject:jobDataListArray forKey:kAppointmentToJobDataList];
	[appointmentDictionary setObject:journalData forKey:kAppointmentToJournal];
	
	[appointmentDictionary setObject:surveyDicitionary forKey:kSurvey];
    
    NSMutableArray *appointmentHistoryList = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToAppointmentHistory)){
        for(AppointmentHistory *hist in self.appointmentToAppointmentHistory){
            [appointmentHistoryList addObject:[hist JSONValue]];
        }
    }
    [appointmentDictionary setObject:appointmentHistoryList forKey:kAppointmentHistoryJSONEntry];
}

-(void)createVoidCommonStructure:(NSMutableDictionary *)appointmentDictionary
{
    
    if(self.appointmentActualStartTime != nil)
    {
        [appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentActualStartTime] forKey:kAppointmentActualStartTime];
    }
    else
    {
        self.appointmentActualStartTime = [NSDate date];
        [appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentActualStartTime] forKey:kAppointmentActualStartTime];
    }
    
    if(self.appointmentActualEndTime != nil)
    {
        [appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentActualEndTime] forKey:kAppointmentActualEndTime];
    }
    else
    {
        if(self.appointmentActualStartTime != nil)
        {
            self.appointmentActualEndTime = self.appointmentActualStartTime;
            [appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentActualEndTime] forKey:kAppointmentActualEndTime];
        }
    }
    
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentDate] forKey:kAppointmentDate];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentEndTime] forKey:kAppointmentEndTime];
	[appointmentDictionary setObject:self.appointmentId forKey:kAppointmentId];
	[appointmentDictionary setObject:isEmpty(self.appointmentNotes)?[NSNull null]:self.appointmentNotes forKey:kAppointmentNotes];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentStartTime] forKey:kAppointmentStartTime];
	[appointmentDictionary setObject:self.appointmentStatus forKey:kAppointmentStatus];
	[appointmentDictionary setObject:self.appointmentType forKey:kAppointmentType];
	[appointmentDictionary setObject:self.createdBy forKey:kCreatedBy];
	[appointmentDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerId)?[NSNull null]:self.defaultCustomerId forKey:kDefaultCustomerId];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerIndex)?[NSNull null]:self.defaultCustomerIndex forKey:kDefaultCustomerIndex];
	[appointmentDictionary setObject:isEmpty(self.journalHistoryId)?[NSNull null]:self.journalHistoryId forKey:kJournalHistoryId];
	[appointmentDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:kJournalId];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.loggedDate] forKey:kLoggedDate];
	[appointmentDictionary setObject:isEmpty(self.noEntryNotes)?[NSNull null]:self.noEntryNotes forKey:kNoEntryNotes];
	[appointmentDictionary setObject:isEmpty(self.surveyorUserName)?[NSNull null]:self.surveyorUserName forKey:kSurveyorUserName];
	[appointmentDictionary setObject:isEmpty(self.tenancyId)?[NSNull null]:self.tenancyId forKey:kTenancyId];
	NSArray *customerList = [self getCustomerList];
	[appointmentDictionary setObject:customerList forKey:kAppointmentToCustomer];
	NSDictionary *propertyData = [NSDictionary dictionaryWithDictionary:[self.appointmentToProperty JSONValue]];
	[appointmentDictionary setObject:propertyData forKey:kAppointmentToProperty];
}

-(void)createDefectAppointmentData:(NSMutableDictionary *)appointmentDictionary
{

	[appointmentDictionary setObject:@"ApplianceDefectAppointmentData:#PropSurvey.Contracts.Data" forKey:@"__type"];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentDate] forKey:kAppointmentDate];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentStartTime] forKey:kAppointmentStartTime];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.appointmentEndTime] forKey:kAppointmentEndTime];
	[appointmentDictionary setObject:self.appointmentId forKey:kAppointmentId];
	[appointmentDictionary setObject:isEmpty(self.appointmentNotes)?[NSNull null]:self.appointmentNotes forKey:kAppointmentNotes];
	[appointmentDictionary setObject:self.appointmentStatus forKey:kAppointmentStatus];
	[appointmentDictionary setObject:self.appointmentType forKey:kAppointmentType];
	[appointmentDictionary setObject:self.createdByPerson forKey:kCreatedByPerson];
	[appointmentDictionary setObject:self.createdBy forKey:kCreatedBy];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerId)?[NSNull null]:self.defaultCustomerId forKey:kDefaultCustomerId];
	[appointmentDictionary setObject:isEmpty(self.defaultCustomerIndex)?[NSNull null]:self.defaultCustomerIndex forKey:kDefaultCustomerIndex];
	[appointmentDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:kJournalId];
	[appointmentDictionary setObject:isEmpty(self.journalHistoryId)?[NSNull null]:self.journalHistoryId forKey:kJournalHistoryId];
	[appointmentDictionary setObject:[UtilityClass convertNSDateToServerDate:self.loggedDate] forKey:kLoggedDate];
	[appointmentDictionary setObject:isEmpty(self.noEntryNotes)?[NSNull null]:self.noEntryNotes forKey:kNoEntryNotes];
	[appointmentDictionary setObject:isEmpty(self.surveyorUserName)?[NSNull null]:self.surveyorUserName forKey:kSurveyorUserName];
	[appointmentDictionary setObject:isEmpty(self.tenancyId)?[NSNull null]:self.tenancyId forKey:kTenancyId];
	
	NSMutableArray *defectJobDataListArray = [NSMutableArray array];
	for (JobSheet *jobData in self.appointmentToJobSheet)
	{
		[defectJobDataListArray addObject:[jobData JSONValue]];
	}
	[appointmentDictionary setObject:defectJobDataListArray forKey:kAppointmentToJobDataList];
	
	NSArray *customerList = [self getCustomerList];
	[appointmentDictionary setObject:customerList forKey:kAppointmentToCustomer];
	
	NSDictionary *propertyData = [NSDictionary dictionaryWithDictionary:[self.appointmentToProperty JSONValue]];
	[appointmentDictionary setObject:propertyData forKey:kAppointmentToProperty];
    
    
    NSMutableArray *appointmentHistoryList = [[NSMutableArray alloc] init];
    if(!isEmpty(self.appointmentToAppointmentHistory)){
        for(AppointmentHistory *hist in self.appointmentToAppointmentHistory){
            [appointmentHistoryList addObject:[hist JSONValue]];
        }
    }
    [appointmentDictionary setObject:appointmentHistoryList forKey:kAppointmentHistoryJSONEntry];
    

}

-(NSArray *)getCustomerList
{
	NSMutableArray * customerList = [NSMutableArray array];
	for (Customer * customer in self.appointmentToCustomer)
	{
		[customerList addObject:[customer JSONValue]];
	}
	return customerList;
}

-(NSDictionary *)getPaintPackDictionary
{
	NSMutableDictionary * paintPacks = [NSMutableDictionary dictionary];
	[paintPacks setObject:self.appointmentToVoidData.isPaintPackRequired forKey:@"isPaintPackRequired"];
	
	NSMutableArray * roomList = [[NSMutableArray alloc]init];
	for(PaintPack * obj in self.appointmentToVoidData.voidDataToPaintPack)
	{
		[roomList addObject:obj.roomId];
	}
	[paintPacks setObject:roomList forKey:@"roomList"];
	return paintPacks;
}

-(NSDictionary *)getMajorWorkRequiredDictionary
{
	NSMutableDictionary * majorWorksRequired = [NSMutableDictionary dictionary];
	[majorWorksRequired setObject:self.appointmentToVoidData.isMajorWorkRequired forKey:@"isMajorWorkRequired"];
	[majorWorksRequired setObject:[[NSArray alloc]init] forKey:@"propertyComponents"];
	
	NSMutableArray * recordMajorWork = [[NSMutableArray alloc]init];
	for(MajorWorkRequired * obj in self.appointmentToVoidData.voidDataToMajorWorkRequired)
	{
		NSMutableDictionary * param = [NSMutableDictionary dictionary];
		[param setObject:obj.componentId forKey:@"componentId"];
		[param setObject:obj.component forKey:@"component"];
		[param setObject:obj.condition forKey:@"condition"];
		if(obj.notes == nil)
		{
			[param setObject:@"" forKey:@"notes"];
		}
		else
		{
			[param setObject:obj.notes forKey:@"notes"];
		}
		if(obj.replacementDue == nil)
		{
			[param setObject:[NSNull null] forKey:@"replacementDue"];
		}
		else
		{
			[param setObject:[UtilityClass convertNSDateToServerDate:obj.replacementDue] forKey:@"replacementDue"];
		}
		[recordMajorWork addObject:param];
	}
	[majorWorksRequired setObject:recordMajorWork forKey:@"recordMajorWork"];
	return majorWorksRequired;
}

-(NSDictionary *)getWorkRequiredDictionary
{
	NSMutableDictionary * workRequired = [NSMutableDictionary dictionary];
	[workRequired setObject:self.appointmentToVoidData.isWorksRequired forKey:@"isWorksRequired"];
	//	[workRequired setObject:self.appointmentToVoidData.tenantEstimate forKey:@"totalNeglectEstimate"];
	
	NSMutableArray * recordWorks = [[NSMutableArray alloc]init];
	for(WorkRequired * obj in self.appointmentToVoidData.voidDataToWorkRequired)
	{
		NSMutableDictionary * param = [NSMutableDictionary dictionary];
		//[param setObject:obj.gross forKey:@"estimate"];
        !isEmpty(obj.isTenantWork)?[param setObject:obj.isTenantWork forKey:@"isTenantWork"]:[param setObject:[NSNull null] forKey:@"isTenantWork"] ;
        !isEmpty(obj.isBRSWork)?[param setObject:obj.isBRSWork forKey:@"isBRSWork"]:[param setObject:[NSNull null] forKey:@"isBRSWork"];
		!isEmpty(obj.isVerified)?[param setObject:obj.isVerified forKey:@"isVerified"]:[param setObject:[NSNull null] forKey:@"isVerified"];
        !isEmpty(obj.worksId)?[param setObject:obj.worksId forKey:@"worksId"]:[param setObject:[NSNull null]  forKey:@"worksId"];
        if(!isEmpty(obj.workRequiredToFaultArea)){
            !isEmpty(obj.workRequiredToFaultArea.faultAreaId)?[param setObject:obj.workRequiredToFaultArea.faultAreaId forKey:@"faultAreaId"]:[param setObject:[NSNull null] forKey:@"faultAreaId"];
        }
        else{
            [param setObject:[NSNull null] forKey:@"faultAreaId"];
        }
        !isEmpty(obj.repairId)?[param setObject:obj.repairId forKey:@"repairId"]:[param setObject:[NSNull null]  forKey:@"repairId"];
        !isEmpty(obj.repairNotes)?[param setObject:obj.repairNotes forKey:@"repairNotes"]:[param setObject:[NSNull null]  forKey:@"repairNotes"];
        //[param setObject:obj.gross forKey:@"gross"]; //Not sending gross any longer
        
		NSString * status = @"";
		if(obj.status != nil)
		{
			status = obj.status;
		}
		[param setObject:status forKey:@"status"];
		[recordWorks addObject:param];
	}
	[workRequired setObject:recordWorks forKey:@"recordWorks"];
	return workRequired;
}

#pragma mark - Methods
- (void) startAppointment
{
	CLS_LOG(@"Start Appointment Here!");
	[[PSDataUpdateManager sharedManager] startAppointment:self];
}

- (void) acceptAppointment
{
    CLS_LOG(@"Accept Appointment Here!");
    [[PSDataUpdateManager sharedManager] acceptAppointment:self];
}

- (void) completeAppointment
{
	CLS_LOG(@"Complete Appointment Here");
	
}

-(void) addBuildAndCompletionInfoToAppointmentDic:(NSMutableDictionary *) dict{
    if(self.creationDate!=nil){
        [dict setObject:[UtilityClass convertNSDateToServerDate:self.creationDate] forKey:kAppointmentCreationDate];
    }
    else{
         [dict setObject:[NSNull null] forKey:kAppointmentCreationDate];
    }
    if(self.aptCompletedAppVersionInfo!=nil){
        [dict setObject:self.aptCompletedAppVersionInfo forKey:kAptCompletedAppVersionInfo];
        [dict setObject:[UtilityClass getAppVersionStringForCompletedAppt] forKey:kAptCurrentAppVersionInfo];
    }
    else{
        [dict setObject:[NSNull null] forKey:kAptCompletedAppVersionInfo];
        [dict setObject:[NSNull null] forKey:kAptCurrentAppVersionInfo];
    }
    
    if(self.loggedDate!=nil){
        [dict setObject:[UtilityClass convertNSDateToServerDate:self.loggedDate] forKey:kLoggedDate];
    }
    else{
        [dict setObject:[NSNull null] forKey:kLoggedDate];
    }
}

#pragma mark - Create New Appointment
+ (void) saveNewAppointment: (NSDictionary *) appointment
{
	CLS_LOG(@"Creating New Appointment");
	[[PSDataPersistenceManager sharedManager] createNewAppointment:appointment];
	
	if (!isEmpty([appointment valueForKey:kAppointmentEventIdentifier]))
	{
		CLS_LOG(@"event exists");
	}
	else
	{
		CLS_LOG(@"event doesnt exist");
	}
}

#pragma mark - Prepare For Offline

- (BOOL) isPreparedForOffline
{
	BOOL isPrepared = NO;
	if ([self.isOfflinePrepared boolValue])
	{
		isPrepared = YES;
	}
	return isPrepared;
}

#pragma mark - Methods

- (AppointmentStatus) getStatus
{
	AppointmentStatus status = AppointmentStatusComplete;
	if([self.appointmentStatus compare:kAppointmentStatusComplete] == NSOrderedSame)
	{
		status = AppointmentStatusComplete;
	}
	else if([self.appointmentStatus compare:kAppointmentStatusFinished] == NSOrderedSame)
	{
		status = AppointmentStatusFinished;
	}
	else if([self.appointmentStatus compare:kAppointmentStatusInProgress] == NSOrderedSame)
	{
		status = AppointmentStatusInProgress;
	}
	else if([self.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame)
	{
		status = AppointmentStatusNotStarted;
	}
	else if([self.appointmentStatus compare:kAppointmentStatusNoEntry] == NSOrderedSame)
	{
		status = AppointmentStatusNoEntry;
	}
    else if([self.appointmentStatus compare:kAppointmentStatusAborted] == NSOrderedSame)
    {
        status = AppointmentStatusAbort;
    }
    else if([self.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame)
    {
        status = AppointmentStatusAccepted;
    }

	return status;
}

- (AppointmentType) getType
{
	AppointmentType type = AppointmentTypeNone;
	if([self.appointmentType compare:kAppointmentTypePreVoid] == NSOrderedSame)
	{
		type = AppointmentTypePreVoid;
	}
	else if([self.appointmentType compare:kAppointmentTypePostVoid] == NSOrderedSame)
	{
		type = AppointmentTypePostVoid;
	}
	else if([self.appointmentType compare:kAppointmentTypeVoidWorkRequired] == NSOrderedSame)
	{
		type = AppointmentTypeVoidWorkRequired;
	}
	else if([self.appointmentType compare:kAppointmentTypeElectricCheck] == NSOrderedSame)
	{
		type = AppointmentTypeElectricCheck;
	}
	else if([self.appointmentType compare:kAppointmentTypeGasCheck] == NSOrderedSame)
	{
		type = AppointmentTypeGasCheck;
	}
	else if([self.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame)
	{
		type = AppointmentTypeGas;
	}
	else if([self.appointmentType compare:kAppointmentTypeFault] == NSOrderedSame)
	{
		type = AppointmentTypeFault;
	}
	else if([self.appointmentType compare:kAppointmentTypePlanned] == NSOrderedSame)
	{
		type = AppointmentTypePlanned;
	}
	else if([self.appointmentType compare:kAppointmentTypeMiscellaneous] == NSOrderedSame)
	{
		type = AppointmentTypeMiscellaneous;
	}
	else if([self.appointmentType compare:kAppointmentTypeAdaptations] == NSOrderedSame)
	{
		type = AppointmentTypeAdaptations;
	}
	else if([self.appointmentType compare:kAppointmentTypeCondition] == NSOrderedSame)
	{
		type = AppointmentTypeCondition;
	}
    else if([self.appointmentType compare:kAppointmentTypeGasVoid] == NSOrderedSame)
    {
        type = AppointmentTypeGasVoid;
    }
    
    else if([self.appointmentType compare:kAppointmentTypeOil] == NSOrderedSame)
    {
        type = AppointmentTypeOilServicing;
    }
    
    else if([self.appointmentType compare:kAppointmentTypeAlternativeFuel] == NSOrderedSame)
    {
        type = AppointmentTypeAlternativeFuels;
    }
    
  else if([self.appointmentType compare:kAppointmentTypeDefect] == NSOrderedSame)
  {
    type = AppointmentTypeDefect;
  }
		
	return type;
}

- (NSString *) stringWithAppointmentStatus:(AppointmentStatus)status
{
	NSString *statusString = @"";
	NSString *statusTitle = @"";
	NSString *dateFormat = @"";
    if(status == AppointmentStatusAccepted)
    {
        statusTitle = LOC(@"KEY_STRING_ACCEPTED");
    }
	else if(status == AppointmentStatusComplete)
	{
		statusTitle = LOC(@"KEY_STRING_COMPLETED");
	}
	else if(status == AppointmentStatusFinished)
	{
		statusTitle = LOC(@"KEY_STRING_COMPLETED");
	}
	else if(status == AppointmentStatusInProgress)
	{
		statusTitle = LOC(@"KEY_STRING_IN_PROGRESS");//KEY_STRING_STARTED
	}
	else if(status == AppointmentStatusPaused)
	{
		statusTitle = LOC(@"KEY_STRING_IN_PROGRESS");//KEY_STRING_STARTED
	}
	else if(status == AppointmentStatusNotStarted)
	{
		statusTitle = LOC(@"KEY_STRING_ARRANGED");
	}
	else if (status == AppointmentStatusNoEntry)
	{
		statusTitle = LOC(@"KEY_STRING_NO_ENTRY");
	}
	if (!isEmpty(self.loggedDate))
	{
		statusString = [NSString stringWithFormat:@"%@ %@",
										statusTitle,
										[UtilityClass stringFromDate:self.loggedDate dateFormat:kDateTimeStyle11]];
	}
	else
	{
		statusString = statusTitle;
	}
	return statusString;
}

- (NSString *) appointmentDateWithStyle:(AppointmentDateStyle)style dateFormat:(NSString *)dateFormat
{
	NSString *appointmentDateString = @"";
	switch (style)
	{
		case AppointmentDateStyleStartTime:
		{
			if(self.appointmentStartTime)
			{
				appointmentDateString = [UtilityClass stringFromDate:self.appointmentStartTime
																									dateFormat:dateFormat];
			}
		}
			break;
		case AppointmentDateStyleEndTime:
		{
			if(self.appointmentEndTime)
			{
				appointmentDateString = [UtilityClass stringFromDate:self.appointmentEndTime
																									dateFormat:dateFormat];
			}
		}
			break;
		case AppointmentDateStyleStartToEnd:
		{
			if(self.appointmentStartTime && self.appointmentEndTime)
			{
				NSString *startTimeString = [UtilityClass stringFromDate:self.appointmentStartTime
																											dateFormat:dateFormat];
				NSString *endTimeString = [UtilityClass stringFromDate:self.appointmentEndTime
																										dateFormat:dateFormat];
				if(!isEmpty(startTimeString) && !isEmpty(endTimeString))
				{
					appointmentDateString = [NSString stringWithFormat:@"%@ %@ %@",
																	 startTimeString,
																	 LOC(@"KEY_STRING_TO"),
																	 endTimeString];
				}
			}
		}
			break;
		case AppointmentDateStyleComplete:
		{
			if(self.appointmentDate)
			{
				appointmentDateString = [UtilityClass stringFromDate:/*self.appointmentDate*/self.appointmentStartTime
																									dateFormat:dateFormat];
			}
		}
			break;
		default:
		{
			appointmentDateString = @"";
		}
			break;
	}
	return appointmentDateString?appointmentDateString:@"";
}

- (Customer *) defaultTenant
{
	Customer *customer =nil;
	if(self && !isEmpty(self.appointmentToCustomer))
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.customerId == %d", [self.defaultCustomerId intValue]];
		NSSet *filteredTenantsSet = [self.appointmentToCustomer filteredSetUsingPredicate:predicate];
		if(!isEmpty(filteredTenantsSet))
		{
			customer = [[filteredTenantsSet allObjects] objectAtIndex:0];
		}
		else
		{
			customer = [[self.appointmentToCustomer allObjects] objectAtIndex:0];
		}
	}
	return customer;
}


/*!
 @discussion
 Appointment List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the appointment sections.
 */
- (NSString *) appointmentDateSectionIdentifier
{
	// Create and cache the section identifier on demand.
	[self willAccessValueForKey:kAppointmentDateSectionIdentifier];
	NSString *sectionIdentifier = nil;//[self primitiveAppointmentDateSectionIdentifier];
	[self didAccessValueForKey:kAppointmentDateSectionIdentifier];
	
	if (!sectionIdentifier)
	{
		sectionIdentifier = [UtilityClass stringFromDate:self.appointmentDate
																					dateFormat:kDateTimeStyle10];
		// [self setPrimitiveAppointmentDateSectionIdentifier:sectionIdentifier];
	}
	
	return sectionIdentifier;
}

#pragma mark -
#pragma mark Key path Dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier
{
	// If the value of timeStamp changes, the section identifier may change as well.
	return [NSSet setWithObject:kAppointmentDate];
}

@end
