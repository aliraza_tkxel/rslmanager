//
//  OilHeating+JSON.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "OilHeating+CoreDataClass.h"

@interface OilHeating (JSON)
- (NSDictionary *) JSONValue;
@end
