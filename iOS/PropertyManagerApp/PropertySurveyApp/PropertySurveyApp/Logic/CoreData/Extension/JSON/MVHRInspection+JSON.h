//
//  MVHRInspection+JSON.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "MVHRInspection+CoreDataClass.h"

@interface MVHRInspection (JSON)
- (NSDictionary *) JSONValue;
@end
