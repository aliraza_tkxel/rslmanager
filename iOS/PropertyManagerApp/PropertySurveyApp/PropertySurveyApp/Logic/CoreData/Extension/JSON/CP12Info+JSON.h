//
//  CP12Info+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "CP12Info+CoreDataClass.h"

@interface CP12Info (JSON)

- (NSDictionary *) JSONValue;
@end
