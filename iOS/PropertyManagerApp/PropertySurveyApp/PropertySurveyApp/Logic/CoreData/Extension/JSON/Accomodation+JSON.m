//
//  Accomodation+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Accomodation+JSON.h"

@implementation Accomodation (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *accomodationDictionary = [NSMutableDictionary dictionary];
    NSString *updatedOn = isEmpty([UtilityClass convertNSDateToServerDate:self.updatedOn])?nil:[UtilityClass convertNSDateToServerDate:self.updatedOn];
    
    [accomodationDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [accomodationDictionary setObject:isEmpty(self.roomName)?[NSNull null]:self.roomName forKey:kRoomName];
    [accomodationDictionary setObject:isEmpty(self.roomHeight)?[NSNull null]:self.roomHeight forKey:kRoomHeight];
    [accomodationDictionary setObject:isEmpty(self.roomWidth)?[NSNull null]:self.roomWidth forKey:kRoomWidth];
    [accomodationDictionary setObject:isEmpty(self.roomLength)?[NSNull null]:self.roomLength forKey:kRoomLength];
    [accomodationDictionary setObject:isEmpty(self.updatedBy)?[NSNull null]:self.updatedBy forKey:kUpdatedBy];
    [accomodationDictionary setObject:isEmpty(updatedOn)?[NSNull null]:updatedOn forKey:kUpdatedOn];

    return accomodationDictionary;
}
@end
