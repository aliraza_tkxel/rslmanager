//
//  DetectorInspection+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DetectorInspection.h"

@interface DetectorInspection (JSON)
- (NSDictionary *) JSONValue;
@end
