//
//  OilFiringServiceInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "OilFiringServiceInspection+JSON.h"

@implementation OilFiringServiceInspection (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    [mutDict setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [mutDict setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [mutDict setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [mutDict setObject:isEmpty(self.inspectionId)?[NSNull null]:self.inspectionId forKey:kInspectionId];
    
    [mutDict setObject:isEmpty(self.airSupply)?[NSNull null]:self.airSupply forKey:kAirSupply];
    [mutDict setObject:isEmpty(self.airSupplyDetail)?[NSNull null]:self.airSupplyDetail forKey:kAirSupplyDetail];
    [mutDict setObject:isEmpty(self.applianceSafety)?[NSNull null]:self.applianceSafety forKey:kApplianceSafety];
    [mutDict setObject:isEmpty(self.applianceSafetyDetail)?[NSNull null]:self.applianceSafetyDetail forKey:kApplianceSafetyDetail];
    [mutDict setObject:isEmpty(self.appointmentId)?[NSNull null]:self.appointmentId forKey:kAppointmentId];
    [mutDict setObject:isEmpty(self.chimneyFlue)?[NSNull null]:self.chimneyFlue forKey:kChimneyFlue];
    [mutDict setObject:isEmpty(self.chimneyFlueDetail)?[NSNull null]:self.chimneyFlueDetail forKey:kChimneyFlueDetail];
    [mutDict setObject:isEmpty(self.combustionChamber)?[NSNull null]:self.combustionChamber forKey:kCombustionChamber];
    [mutDict setObject:isEmpty(self.combustionChamberDetail)?[NSNull null]:self.combustionChamberDetail forKey:kCombustionChamberDetail];
    [mutDict setObject:isEmpty(self.controlCheck)?[NSNull null]:self.controlCheck forKey:kControlCheck];
    [mutDict setObject:isEmpty(self.controlCheckDetail)?[NSNull null]:self.controlCheckDetail forKey:kControlCheckDetail];
    [mutDict setObject:isEmpty(self.electricalSafety)?[NSNull null]:self.electricalSafety forKey:kElectricalSafety];
    [mutDict setObject:isEmpty(self.electricalSafetyDetail)?[NSNull null]:self.electricalSafetyDetail forKey:kElectricalSafetyDetail];
    [mutDict setObject:isEmpty(self.heatExchanger)?[NSNull null]:self.heatExchanger forKey:kHeatExchanger];
    [mutDict setObject:isEmpty(self.heatExchangerDetail)?[NSNull null]:self.heatExchangerDetail forKey:kHeatExchangerDetail];
    [mutDict setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [mutDict setObject:isEmpty(self.hotWaterType)?[NSNull null]:self.hotWaterType forKey:kHotWaterType];
    [mutDict setObject:isEmpty(self.hotWaterTypeDetail)?[NSNull null]:self.hotWaterTypeDetail forKey:kHotWaterTypeDetail];
    [mutDict setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:kJournalId];
    [mutDict setObject:isEmpty(self.oilStorage)?[NSNull null]:self.oilStorage forKey:kOilStorage];
    [mutDict setObject:isEmpty(self.oilStorageDetail)?[NSNull null]:self.oilStorageDetail forKey:kOilStorageDetail];
    [mutDict setObject:isEmpty(self.oilSupplySystem)?[NSNull null]:self.oilSupplySystem forKey:kOilSupplySystem];
    [mutDict setObject:isEmpty(self.oilSupplySystemDetail)?[NSNull null]:self.oilSupplySystemDetail forKey:kOilSupplySystemDetail];
    [mutDict setObject:isEmpty(self.pressureJet)?[NSNull null]:self.pressureJet forKey:kPressureJet];
    [mutDict setObject:isEmpty(self.pressureJetDetail)?[NSNull null]:self.pressureJetDetail forKey:kPressureJetDetail];
    [mutDict setObject:isEmpty(self.vaporisingBurner)?[NSNull null]:self.vaporisingBurner forKey:kVaporisingBurner];
    [mutDict setObject:isEmpty(self.vaporisingBurnerDetail)?[NSNull null]:self.vaporisingBurnerDetail forKey:kVaporisingBurnerDetail];
    [mutDict setObject:isEmpty(self.wallflameBurner)?[NSNull null]:self.wallflameBurner forKey:kWallflameBurner];
    [mutDict setObject:isEmpty(self.wallflameBurnerDetail)?[NSNull null]:self.wallflameBurnerDetail forKey:kWallflameBurnerDetail];
    [mutDict setObject:isEmpty(self.warmAirType)?[NSNull null]:self.warmAirType forKey:kWarmAirType];
    [mutDict setObject:isEmpty(self.warmAirTypeDetail)?[NSNull null]:self.warmAirTypeDetail forKey:kWarmAirTypeDetail];
    
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
