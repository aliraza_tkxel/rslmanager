//
//  MVHRInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "MVHRInspection+JSON.h"

@implementation MVHRInspection (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    [mutDict setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [mutDict setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [mutDict setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [mutDict setObject:isEmpty(self.checkAirFlow)?[NSNull null]:self.checkAirFlow forKey:kCheckAirFlow];
    [mutDict setObject:isEmpty(self.airFlowDetail)?[NSNull null]:self.airFlowDetail forKey:kAirFlowDetail];
    [mutDict setObject:isEmpty(self.ductingInspection)?[NSNull null]:self.ductingInspection forKey:kDuctingInspection];
    [mutDict setObject:isEmpty(self.ductingDetail)?[NSNull null]:self.ductingDetail forKey:kDuctingDetail];
    [mutDict setObject:isEmpty(self.checkFilters)?[NSNull null]:self.checkFilters forKey:kCheckFilters];
    [mutDict setObject:isEmpty(self.filterDetail)?[NSNull null]:self.filterDetail forKey:kFilterDetail];
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
