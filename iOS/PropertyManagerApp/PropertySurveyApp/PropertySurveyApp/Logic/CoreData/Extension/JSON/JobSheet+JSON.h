//
//  JobSheet (JSON).h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "JobSheet+CoreDataClass.h"

@interface JobSheet (JSON)

- (NSDictionary *) JSONValue;

@end
