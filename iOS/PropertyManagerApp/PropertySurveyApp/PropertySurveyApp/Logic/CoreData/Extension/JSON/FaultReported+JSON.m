//
//  FaultReported+JSON.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-22.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import "FaultReported+JSON.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"

@implementation FaultReported (JSON)

-(NSDictionary *)JSONValue
{
	NSMutableDictionary *schemeDict = [NSMutableDictionary dictionary];
	[schemeDict setObject:self.faultReportedToFaultArea.faultAreaId forKey:@"faultAreaId"];
	[schemeDict setObject:self.faultReportedToFaultDetail.faultId forKey:@"faultId"];
	[schemeDict setObject:self.isRecurring forKey:@"isRecurring"];
	[schemeDict setObject:self.notes forKey:@"notes"];
	[schemeDict setObject:self.problemDays forKey:@"problemDays"];
	return schemeDict;
}

@end
