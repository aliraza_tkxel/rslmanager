//
//  Detector+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Meter+CoreDataClass.h"

@interface Meter (JSON)
- (NSDictionary *) JSONValue;
@end
