//
//  CustomerRiskData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "CustomerRiskData+JSON.h"

@implementation CustomerRiskData (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *customerRiskDictionary = [NSMutableDictionary dictionary];
    [customerRiskDictionary setObject:isEmpty(self.riskCatDesc)?[NSNull null]:self.riskCatDesc forKey:@"riskCatDesc"];
    [customerRiskDictionary setObject:isEmpty(self.riskSubCatDesc)?[NSNull null]:self.riskSubCatDesc forKey:@"riskSubCatDesc"];
    
    return customerRiskDictionary;
}
@end
