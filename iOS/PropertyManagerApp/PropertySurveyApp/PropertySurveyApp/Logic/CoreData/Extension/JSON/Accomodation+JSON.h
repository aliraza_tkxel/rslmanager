//
//  Accomodation+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Accomodation.h"

@interface Accomodation (JSON)

- (NSDictionary *) JSONValue;
@end
