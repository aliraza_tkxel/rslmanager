//
//  FaultReported+JSON.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-22.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import "FaultReported.h"

@interface FaultReported (JSON)

- (NSDictionary *) JSONValue;

@end
