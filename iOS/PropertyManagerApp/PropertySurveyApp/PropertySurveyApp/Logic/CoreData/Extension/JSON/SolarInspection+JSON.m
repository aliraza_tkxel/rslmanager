//
//  SolarInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "SolarInspection+JSON.h"

@implementation SolarInspection (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    [mutDict setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [mutDict setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [mutDict setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [mutDict setObject:isEmpty(self.vesselProtectionInstalled)?[NSNull null]:self.vesselProtectionInstalled forKey:kVesselProtectionInstalled];
    [mutDict setObject:isEmpty(self.vesselCapacity)?[NSNull null]:self.vesselCapacity forKey:kVesselCapacity];
    [mutDict setObject:isEmpty(self.minPressure)?[NSNull null]:self.minPressure forKey:kMinPressure];
    [mutDict setObject:isEmpty(self.freezingTemp)?[NSNull null]:self.freezingTemp forKey:kFreezingTemp];
    [mutDict setObject:isEmpty(self.systemPressure)?[NSNull null]:self.systemPressure forKey:kSystemPressure];
    [mutDict setObject:isEmpty(self.backPressure)?[NSNull null]:self.backPressure forKey:kBackPressure];
    [mutDict setObject:isEmpty(self.deltaOn)?[NSNull null]:self.deltaOn forKey:kDeltaOn];
    [mutDict setObject:isEmpty(self.deltaOff)?[NSNull null]:self.deltaOff forKey:kDeltaOff];
    [mutDict setObject:isEmpty(self.maxTemperature)?[NSNull null]:self.maxTemperature forKey:kMaxTemperature];
    [mutDict setObject:isEmpty(self.calculationRate)?[NSNull null]:self.calculationRate forKey:kCalculationRate];
    [mutDict setObject:isEmpty(self.thermostatTemperature)?[NSNull null]:self.thermostatTemperature forKey:kThermostatTemperature];
    [mutDict setObject:isEmpty(self.antiScaldingControl)?[NSNull null]:self.antiScaldingControl forKey:kAntiScaldingControl];
    [mutDict setObject:isEmpty(self.antiScaldingControlDetail)?[NSNull null]:self.antiScaldingControlDetail forKey:kAntiScaldingControlDetail];
    [mutDict setObject:isEmpty(self.checkDirection)?[NSNull null]:self.checkDirection forKey:kCheckDirection];
    [mutDict setObject:isEmpty(self.directionDetail)?[NSNull null]:self.directionDetail forKey:kDirectionDetail];
    [mutDict setObject:isEmpty(self.checkElectricalControl)?[NSNull null]:self.checkElectricalControl forKey:kCheckElectricalControl];
    [mutDict setObject:isEmpty(self.electricalControlDetail)?[NSNull null]:self.electricalControlDetail forKey:kElectricalControlDetail];
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
