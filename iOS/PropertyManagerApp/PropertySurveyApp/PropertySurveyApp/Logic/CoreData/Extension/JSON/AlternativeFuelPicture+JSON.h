//
//  AlternativeFuelPicture+JSON.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 02/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AlternativeFuelPicture+CoreDataClass.h"
#import "PSUploadAlternateHeatingPhotographsService.h"
@interface AlternativeFuelPicture (JSON)<PSUploadAlternateHeatingPhotographsServiceDelegate>
- (NSDictionary *) JSONValue;
-(NSString *) getPicturePath;
-(NSMutableDictionary*) getJSON;
-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage;
-(void) uploadPicture;

@property (strong,nonatomic) UIImage *image;
@end
