//
//  PlannedTradeComponent+JSON.h
//  PropertySurveyApp
//
//  Created by TkXel on 02/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PlannedTradeComponent.h"

@interface PlannedTradeComponent (JSON)

- (NSDictionary *) JSONValue;

@end
