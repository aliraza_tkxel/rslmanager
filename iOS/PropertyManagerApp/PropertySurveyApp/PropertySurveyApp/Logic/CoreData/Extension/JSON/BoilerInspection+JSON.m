//
//  BoilerInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "BoilerInspection+JSON.h"

@implementation BoilerInspection (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    
    
    [inspectionFormDictionary setObject:isEmpty(self.boilerInspectionToBoiler.boilerTypeId)?[NSNull null]:self.boilerInspectionToBoiler.boilerTypeId forKey:kBoilerTypeId];
    [inspectionFormDictionary setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [inspectionFormDictionary setObject:isEmpty(self.isInspected)?[NSNumber numberWithBool:NO]:self.isInspected forKey:kIsInspected];
    [inspectionFormDictionary setObject:isEmpty(self.combustionReading)?[NSNull null]:self.combustionReading forKey:kCombustionReading];
    [inspectionFormDictionary setObject:isEmpty(self.operatingPressure)?[NSNull null]:self.operatingPressure forKey:kOperatingPressure];
    [inspectionFormDictionary setObject:isEmpty(self.safetyDeviceOperational)?[NSNull null]:self.safetyDeviceOperational forKey:kSafetyDeviceOperational];
    [inspectionFormDictionary setObject:isEmpty(self.smokePellet)?[NSNull null]:self.smokePellet forKey:kSmokePellet];
    [inspectionFormDictionary setObject:isEmpty(self.adequateVentilation)?[NSNull null]:self.adequateVentilation forKey:kAdequateVentilation];
    [inspectionFormDictionary setObject:isEmpty(self.flueVisualCondition)?[NSNull null]:self.flueVisualCondition forKey:kFlueVisualCondition];
    [inspectionFormDictionary setObject:isEmpty(self.satisfactoryTermination)?[NSNull null]:self.satisfactoryTermination forKey:kSatisfactoryTermination];
    [inspectionFormDictionary setObject:isEmpty(self.fluePerformanceChecks)?[NSNull null]:self.fluePerformanceChecks forKey:kFluePerformanceChecks];
    [inspectionFormDictionary setObject:isEmpty(self.boilerServiced)?[NSNull null]:self.boilerServiced forKey:kBoilerServiced];
    [inspectionFormDictionary setObject:isEmpty(self.boilerSafeToUse)?[NSNull null]:self.boilerSafeToUse forKey:kBoilerSafeTouse];
    [inspectionFormDictionary setObject:isEmpty(self.inspectionId)?[NSNull null]:self.inspectionId forKey:kBoilerInspectionId];
    [inspectionFormDictionary setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [inspectionFormDictionary setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [inspectionFormDictionary setObject:isEmpty(self.spillageTest)?[NSNull null]:self.spillageTest forKey:kSpillageTest];
    [inspectionFormDictionary setObject:isEmpty(self.operatingPressureUnit)?[NSNull null]:self.operatingPressureUnit forKey:kOperatingPressureUnit];
	
    return inspectionFormDictionary;
}
@end
