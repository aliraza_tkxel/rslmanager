//
//  Journal+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Journal+JSON.h"

@implementation Journal (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *journalDataDcitionary = [NSMutableDictionary dictionary];
    
    NSString *creationDate = isEmpty([UtilityClass convertNSDateToServerDate:self.creationDate])?nil:[UtilityClass convertNSDateToServerDate:self.creationDate];
    [journalDataDcitionary setObject:isEmpty(self.actionId)?[NSNull null]:self.actionId forKey:@"actionId"];
    [journalDataDcitionary setObject:isEmpty(self.creationBy)?[NSNull null]:self.creationBy forKey:@"creationBy"];
    [journalDataDcitionary setObject:isEmpty(creationDate)?[NSNull null]:creationDate forKey:@"creationDate"];
    [journalDataDcitionary setObject:isEmpty(self.inspectionTypeId)?[NSNull null]:self.inspectionTypeId forKey:@"inspectionTypeId"];
    [journalDataDcitionary setObject:isEmpty(self.isCurrent)?[NSNull null]:self.isCurrent forKey:@"isCurrent"];
    [journalDataDcitionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:@"journalId"];
    [journalDataDcitionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [journalDataDcitionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
     [journalDataDcitionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
     [journalDataDcitionary setObject:isEmpty(self.journalHistoryId)?[NSNull null]:self.journalHistoryId forKey:kJournalHistoryId];
    [journalDataDcitionary setObject:isEmpty(self.statusId)?[NSNull null]:self.statusId forKey:@"statusId"];
    return journalDataDcitionary;
}
@end
