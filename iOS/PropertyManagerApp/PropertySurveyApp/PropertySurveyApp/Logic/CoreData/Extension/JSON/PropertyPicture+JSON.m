//
//  PropertyPicture+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PropertyPicture+JSON.h"
#import "PropertyPicture+MWPhoto.h"
#import <objc/runtime.h>
static void * ImagePropertyKey = &ImagePropertyKey;
@implementation PropertyPicture (JSON)
- (UIImage *)image {
    return objc_getAssociatedObject(self, ImagePropertyKey);
}

- (void)setImage:(UIImage *)image {
    objc_setAssociatedObject(self, ImagePropertyKey, image, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *picturesDictionary = [NSMutableDictionary dictionary];
    NSString *createdOn = isEmpty([UtilityClass convertNSDateToServerDate:self.createdOn])?nil:[UtilityClass convertNSDateToServerDate:self.createdOn];
    
    [picturesDictionary setObject:isEmpty(self.appointmentId)?[NSNumber numberWithInt:0]:self.appointmentId forKey:@"appointmentId"];
    [picturesDictionary setObject:isEmpty(self.createdBy)?[NSNull null]:self.createdBy forKey:@"createdBy"];
    //[picturesDictionary setObject:isEmpty(createdOn)?[NSNull null]:createdOn forKey:@"createdOn"];
    //[picturesDictionary setObject:isEmpty(self.imagePath)?[NSNull null]:self.imagePath forKey:@"imagePath"];
    //[picturesDictionary setObject:isEmpty(self.isDefault)?@"false":@"true" forKey:@"isDefault"];
    [picturesDictionary setObject:isEmpty(self.itemId)?[NSNull null]:self.itemId forKey:@"itemId"];
    [picturesDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [picturesDictionary setObject:isEmpty(self.propertyPictureId)?[NSNumber numberWithInt:0]:self.propertyPictureId forKey:@"propertyPicId"];//kPropertyPictureId
    //[picturesDictionary setObject:isEmpty(self.propertyPictureName)?[NSNull null]:self.propertyPictureName forKey:@"propertyPictureName"];
    //[picturesDictionary setObject:isEmpty(self.surveyId)?[NSNull null]:self.surveyId forKey:@"surveyId"];
    return picturesDictionary;
    
}

-(NSString *) getPicturePath
{
    if (self.imagePath) {
        
        return self.imagePath;
    }
    NSString *idPAth = @"";
    if(!isEmpty(self.propertyPictureToProperty.propertyId)){
        idPAth = self.propertyPictureToProperty.propertyId;
    }
    else if(!isEmpty(self.propertyPictureToProperty.schemeId)){
        idPAth = [self.propertyPictureToProperty.schemeId stringValue];
    }
    else if(!isEmpty(self.propertyPictureToProperty.blockId)){
        idPAth = [self.propertyPictureToProperty.blockId stringValue];
    }
    return [NSString stringWithFormat:@"%@-%@-%@-%@",[self.appointmentId stringValue],[self.itemId stringValue],idPAth,[UtilityClass stringFromDate:self.createdOn dateFormat:@"HH-mm-ss"]];
}

-(NSMutableDictionary*) getJSON
{
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    if (self.image) {
        
        [requestParameters setObject:self.image forKey:kPropertyImage];
    }
    
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:self.propertyPictureToProperty.propertyToAppointment.appointmentType forKey:@"type"];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    
    [requestParameters setObject:!isEmpty(self.propertyId)?self.propertyId:[NSNull null] forKey:kPropertyId];
    if(!isEmpty(self.propertyPictureToProperty.schemeId)){
        [requestParameters setObject:self.propertyPictureToProperty.schemeId forKey:kSchemeID];
    }
    else if(!isEmpty(self.propertyPictureToProperty.blockId)){
        [requestParameters setObject:self.propertyPictureToProperty.blockId forKey:kSchemeBlockID];
    }
    
    [requestParameters setObject:self.itemId?:[NSNumber numberWithInt:0] forKey:kItemId];
    
    [requestParameters setObject:self.appointmentId?:[NSNumber numberWithInt:0] forKey:kAppointmentId];
    if(!isEmpty(self.heatingId)){
        [requestParameters setObject:self.heatingId forKey:kHeatingId];
    }
    BOOL isDefault= self.propertyPictureToProperty.defaultPicture==self;
    
    [requestParameters setObject:isDefault?@"true":@"false" forKey:@"isDefault"];
    
    [requestParameters setObject:self.propertyPictureId?:[NSNumber numberWithInt:0] forKey:@"propertyPicId"];
    
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    [requestParameters setObject:self.imageIdentifier forKey:kUniqueImageIdentifier];
    
    return requestParameters;
    
}

-(void)deletePictureService:(PropertyPicture*)pic
{
    // if you come here picture is dirty should be deleted this will only be called when posting data
    if(pic.imagePath) // needs to delete from server
    {
        NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
        
        [requestParameters setObject:pic.propertyPictureId forKey:kPropertyPictureId];
        [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
        [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
        NSDictionary * responseDict =  [[PSPropertyPictureManager sharedManager].deletePhotographService deleteSyncPhotograph:requestParameters];
        if(responseDict)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
            [[PSDataUpdateManager sharedManager] deletePropertyPicture:pic];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:nil];
        }
        
    }
    else
    {
        //here you get dirty image but unsyced with server . here just ignore this case..
        //data will refresh when data posted successfully on Server
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
        [[PSDataUpdateManager sharedManager] deletePropertyPicture:pic];
    }

}

-(void) uploadPicture
{
    if([self.deletedPicture isEqualToNumber:[NSNumber numberWithBool:YES]])
    {
        // if you come here picture is dirty should be deleted this will only be called when posting data
        [self performSelector:@selector(deletePictureService:) withObject:self afterDelay:0.5];
        //Giving little time for syncing of Coredata. Due to speedy updates (Sync call)
    }
    else
    {
        self.image=[[self underlyingImage] scaleToSize:CGSizeMake(500, 500)];
        [PSPropertyPictureManager sharedManager].uploadPhotographService.photographDelegate = self;
        [[PSPropertyPictureManager sharedManager] uploadPhotograph:[self getJSON] withImage:self.image];
    }
}

-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage
{
    
    self.image = [propertyImage scaleToSize:CGSizeMake(500, 500)];
    
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager storeImage:self.image forKey:[self getPicturePath]];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                        object:self];
#warning Check if network is available
    // temporarily disable this code to upload image in background
//    PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
//    if(delegate.postingDataObjectAppointments==0)
//    {
//        if([PSPropertyPictureManager sharedManager].isPostingImage==NO)
//        {
//            [PSPropertyPictureManager sharedManager].isPostingImage=YES;
//            [PSPropertyPictureManager sharedManager].uploadPhotographService.photographDelegate = self;
//            [[PSPropertyPictureManager sharedManager] uploadPhotograph:pictureDictionary withImage:self.image];
//        }
//    }
    
}



-(void) updatePropertyProfilePicture:(NSDictionary*) pictureDictionary
{
    
    
    self.image=[[self underlyingImage] scaleToSize:CGSizeMake(500, 500)];
    
    
    [PSPropertyPictureManager sharedManager].uploadPhotographService.photographDelegate = self;
    
    [[PSPropertyPictureManager sharedManager] uploadPhotograph:[self getJSON] withImage:self.image];
    
}



- (void) service:(PSUploadPhotographService *)service didReceivePhotographData:(id)surveyorData {
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    NSString * responseString=surveyorData;
    
    if(!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        
        //self.isDefault=[NSNumber numberWithBool:YES];
        
        [[PSDataUpdateManager sharedManager] updatePropertyPicture:self forDictionary:[JSONDictionary objectForKey:@"response"]];
        
        if (self.image) {
            
            FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
            
            [manager storeImage:self.image forKey:[[JSONDictionary objectForKey:@"response"] objectForKey:kPropertyPicturePath]];
        }
        
        
        NSDictionary * dict=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1] forKey:kPropertyPicturesSynchStatus];
        
        [[PSDataUpdateManager sharedManager] updatePropertyPicture:self forDictionary:dict];
        
        self.image=nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:nil];
    }
}

- (void) service:(PSUploadPhotographService *)service didFailWithError:(NSError *)error
{
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:error];
    self.image = nil;
}

@end
