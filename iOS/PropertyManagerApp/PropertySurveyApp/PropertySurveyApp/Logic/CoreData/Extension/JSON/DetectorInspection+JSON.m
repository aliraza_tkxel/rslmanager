//
//  DetectorInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DetectorInspection+JSON.h"

@implementation DetectorInspection (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    
    [inspectionFormDictionary setObject:isEmpty(self.inspectionID)?[NSNull null]:self.inspectionID forKey:kInspectionId];
    [inspectionFormDictionary setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [inspectionFormDictionary setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [inspectionFormDictionary setObject:isEmpty(self.detectorsTested)?[NSNull null]:self.detectorsTested forKey:kDetectorTest];
    
    return inspectionFormDictionary;
}
@end
