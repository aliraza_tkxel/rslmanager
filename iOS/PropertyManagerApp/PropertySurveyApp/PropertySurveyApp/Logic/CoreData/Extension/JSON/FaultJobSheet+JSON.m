//
//  FaultJobSheet+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultJobSheet+JSON.h"
#import "FaultRepairData+JSON.h"
#import "JobPauseData+JSON.h"

@implementation FaultJobSheet (JSON)

- (NSDictionary *) JSONValue
{
	
	NSMutableDictionary *jobDataDictionary = [NSMutableDictionary dictionary];
    
    
    NSString *actualStartDate = isEmpty([UtilityClass convertNSDateToServerDate:self.actualStartTime])?nil:[UtilityClass convertNSDateToServerDate:self.actualStartTime];
    
    NSString *actualEndDate = isEmpty([UtilityClass convertNSDateToServerDate:self.actualEndTime])?nil:[UtilityClass convertNSDateToServerDate:self.actualEndTime];
	
	NSString *completionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.completionDate])?nil:[UtilityClass convertNSDateToServerDate:self.completionDate];
	NSString *reportedDate = isEmpty([UtilityClass convertNSDateToServerDate:self.reportedDate])?nil:[UtilityClass convertNSDateToServerDate:self.reportedDate];
    NSString *currentAppVersion = [UtilityClass getAppVersionStringForCompletedAppt];
    
    
    // Mapping of actual start and end date time of job sheet
    [jobDataDictionary setObject:isEmpty(actualStartDate)?[NSNull null]:actualStartDate forKey:kJSActualStartTime];
    [jobDataDictionary setObject:isEmpty(actualEndDate)?[NSNull null]:actualEndDate forKey:kJSActualEndTime];
    
    
	[jobDataDictionary setObject:isEmpty(completionDate)?[NSNull null]:completionDate forKey:@"completionDate"];
	[jobDataDictionary setObject:isEmpty(self.duration)?[NSNull null]:self.duration forKey:kDuration];
	[jobDataDictionary setObject:isEmpty(self.isLegionella)?[NSNull null]:self.isLegionella forKey:kIsLegionella];
	[jobDataDictionary setObject:isEmpty(self.faultLogID)?[NSNull null]:self.faultLogID forKey:kFaultLogID];
	[jobDataDictionary setObject:isEmpty(self.followOnNotes)?[NSNull null]:self.followOnNotes forKey:kFollowOnNotes];
	[jobDataDictionary setObject:isEmpty(self.jobStatus)?[NSNull null]:self.jobStatus forKey:kJobStatus];
	[jobDataDictionary setObject:isEmpty(self.jsnDescription)?[NSNull null]:self.jsnDescription forKey:kJSNDescription];
	[jobDataDictionary setObject:isEmpty(self.jsnLocation)?[NSNull null]:self.jsnLocation forKey:kJSNLocation];
	[jobDataDictionary setObject:isEmpty(self.jsnNotes)?[NSNull null]:self.jsnNotes forKey:kJSNNotes];
	[jobDataDictionary setObject:isEmpty(self.jsNumber)?[NSNull null]:self.jsNumber forKey:kJSNumber];
	[jobDataDictionary setObject:isEmpty(self.jsType)?[NSNull null]:self.jsType forKey:kJSType];
	[jobDataDictionary setObject:isEmpty(self.priority)?[NSNull null]:self.priority forKey:kPriority];
	[jobDataDictionary setObject:isEmpty(self.repairNotes)?[NSNull null]:self.repairNotes forKey:kRepairNotes];
	[jobDataDictionary setObject:isEmpty(reportedDate)?[NSNull null]:reportedDate forKey:kReportedDate];
	[jobDataDictionary setObject:isEmpty(self.responseTime)?[NSNull null]:self.responseTime forKey:kResponseTime];
    [jobDataDictionary setObject:isEmpty(self.jsCompletedAppVersionInfo)?[NSNull null]:self.jsCompletedAppVersionInfo forKey:kJSCompletedAppVersionInfo];
    [jobDataDictionary setObject:isEmpty(currentAppVersion)?[NSNull null]:currentAppVersion forKey:kJSCurrentAppVersionInfo];
    [jobDataDictionary setObject:isEmpty(self.repairId)?[NSNull null]:self.repairId forKey:kJSRepairId];
	
	NSMutableArray *jobPauseHistoryArray = [NSMutableArray array];
	for (JobPauseData *jobPauseData in self.jobPauseData)
	{
		[jobPauseHistoryArray addObject:[jobPauseData JSONValue]];
	}
	
	NSMutableArray *faultRepairDataArray = [NSMutableArray array];
	for (FaultRepairData *faultRepairData in self.faultRepairDataList)
	{
		[faultRepairDataArray addObject:[faultRepairData JSONValue]];
	}
	[jobDataDictionary setObject:faultRepairDataArray forKey:@"faultRepairList"];
	[jobDataDictionary setObject:jobPauseHistoryArray forKey:@"jobPauseHistory"];
	return jobDataDictionary;
	
}



@end
