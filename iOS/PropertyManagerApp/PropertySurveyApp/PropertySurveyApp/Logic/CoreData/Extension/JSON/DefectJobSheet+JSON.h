//
//  DefectJobSheet+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DefectJobSheet+CoreDataClass.h"
#import "JobSheet+JSON.h"

@interface DefectJobSheet (JSON)

- (NSDictionary *) JSONValue;
@end
