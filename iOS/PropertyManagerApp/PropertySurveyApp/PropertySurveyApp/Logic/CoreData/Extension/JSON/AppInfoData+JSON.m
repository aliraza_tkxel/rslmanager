//
//  AppInfoData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "AppInfoData+JSON.h"

@implementation AppInfoData (JSON)

- (NSDictionary *) JSONValue
{
	NSMutableDictionary *appInfoDictionary = [NSMutableDictionary dictionary];
	[appInfoDictionary setObject:isEmpty(self.totalAppointments)?[NSNull null]:self.totalAppointments forKey:@"totalAppointments"];
	[appInfoDictionary setObject:isEmpty(self.totalNoEntries)?[NSNull null]:self.totalNoEntries forKey:@"totalNoEntries"];
	[appInfoDictionary setObject:isEmpty(self.isCardLeft)?[NSNull null]:self.isCardLeft forKey:@"isCardLeft"];
	return appInfoDictionary;
}
@end
