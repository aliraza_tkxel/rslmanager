//
//  PropertyAsbestosData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PropertyAsbestosData+JSON.h"

@implementation PropertyAsbestosData (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *asbestosDictionary = [NSMutableDictionary dictionary];
    
    [asbestosDictionary setObject:isEmpty(self.asbestosId)?[NSNull null]:self.asbestosId forKey:@"asbestosId"];
    [asbestosDictionary setObject:isEmpty(self.asbRiskLevelDesc)?[NSNull null]:self.asbRiskLevelDesc forKey:@"asbRiskLevelDesc"];
    [asbestosDictionary setObject:isEmpty(self.riskDesc)?[NSNull null]:self.riskDesc forKey:@"riskDesc"];
	  [asbestosDictionary setObject:isEmpty(self.type)?[NSNull null]:self.type forKey:@"type"];
	  [asbestosDictionary setObject:isEmpty(self.riskLevel)?[NSNull null]:self.riskLevel forKey:@"riskLevel"];
	
    return asbestosDictionary;
}
@end
