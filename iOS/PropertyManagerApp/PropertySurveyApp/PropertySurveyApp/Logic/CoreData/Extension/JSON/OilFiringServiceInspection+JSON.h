//
//  OilFiringServiceInspection+JSON.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "OilFiringServiceInspection+CoreDataClass.h"

@interface OilFiringServiceInspection (JSON)
- (NSDictionary *) JSONValue;
@end
