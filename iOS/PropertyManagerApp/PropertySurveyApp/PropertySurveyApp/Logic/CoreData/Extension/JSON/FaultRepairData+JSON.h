//
//  FaultRepairData+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultRepairData+CoreDataClass.h"

@interface FaultRepairData (JSON)
- (NSDictionary *) JSONValue;
@end
