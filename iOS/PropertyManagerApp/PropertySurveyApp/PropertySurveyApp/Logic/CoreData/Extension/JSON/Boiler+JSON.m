////
//  Appliance+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Boiler+JSON.h"
#import "BoilerInspection+JSON.h"
#import "Defect+JSON.h"
#import "InstallationPipework+JSON.h"

@implementation Boiler (JSON)


- (NSDictionary *) JSONValue
{
	NSMutableDictionary *boilerDictionary = [NSMutableDictionary dictionary];
	
	NSString *replacementDue = isEmpty([UtilityClass convertNSDateToServerDate:self.replacementDue])?nil:[UtilityClass convertNSDateToServerDate:self.replacementDue];
	NSString *lastReplaced = isEmpty([UtilityClass convertNSDateToServerDate:self.lastReplaced])?nil:[UtilityClass convertNSDateToServerDate:self.lastReplaced];
    NSString *gasketReplacementDate = isEmpty([UtilityClass convertNSDateToServerDate:self.gasketReplacementDate])?nil:[UtilityClass convertNSDateToServerDate:self.gasketReplacementDate];
	
	//Boiler
	[boilerDictionary setObject:isEmpty(self.flueTypeId)?[NSNull null]:self.flueTypeId forKey:kBoilerFlueTypeId];
	[boilerDictionary setObject:isEmpty(self.gcNumber)?[NSNull null]:self.gcNumber forKey:kBoilerGcNumber];
	[boilerDictionary setObject:isEmpty(self.isLandlordAppliance)?[NSNull null]:self.isLandlordAppliance forKey:kBoilerIsLandlordAppliance];
	[boilerDictionary setObject:isEmpty(lastReplaced)?[NSNull null]:lastReplaced forKey:kBoilerLastReplaced];
	[boilerDictionary setObject:isEmpty(replacementDue)?[NSNull null]:replacementDue forKey:kBoilerReplacementDue];
    [boilerDictionary setObject:isEmpty(gasketReplacementDate)?[NSNull null]:gasketReplacementDate forKey:kBoilerGasketReplacementDate];
	[boilerDictionary setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kBoilerLocation];
	[boilerDictionary setObject:isEmpty(self.manufacturerId)?[NSNull null]:self.manufacturerId forKey:kBoilerManufacturerId];
	[boilerDictionary setObject:isEmpty(self.model)?[NSNull null]:self.model forKey:kBoilerModel];
	[boilerDictionary setObject:isEmpty(self.boilerTypeId)?[NSNull null]:self.boilerTypeId forKey:kBoilerTypeId];
	[boilerDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kBoilerIsInspected];
	[boilerDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kBoilerPropertyId];
	[boilerDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kBoilerSerialNumber];
    
    [boilerDictionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [boilerDictionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
    [boilerDictionary setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [boilerDictionary setObject:isEmpty(self.itemId)?[NSNull null]:self.itemId forKey:kItemId];
    
    NSDictionary *installationPipeworkDictionary = [NSDictionary dictionaryWithDictionary:[self.boilerToGasInstallationPipeWork JSONValue]];
    [boilerDictionary setObject:installationPipeworkDictionary forKey:@"InstallationPipework"];
    //[boilerDictionary setObject:installationPipeworkDictionary forKey:@"installationpipework"];
    
	//Boiler Inspection
	NSDictionary *boilerInspectionDictionary = [NSDictionary dictionaryWithDictionary:[self.boilerToBoilerInspection JSONValue]];
	[boilerDictionary setObject:boilerInspectionDictionary forKey:kBoilerInspection];
	
	//Boiler Defect
	NSMutableArray *boilerDefectsArray = [NSMutableArray array];
	for (Defect *defect in self.boilerToDefect)
	{
		[boilerDefectsArray addObject:[defect JSONValue]];
	}
	[boilerDictionary setObject:boilerDefectsArray forKey:kBoilerDefects];
	
	return boilerDictionary;
}

@end
