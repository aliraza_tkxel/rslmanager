//
//  ApplianceLocation+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceLocation+JSON.h"

@implementation ApplianceLocation (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *applianceLocationDictionary = [NSMutableDictionary dictionary];
    [applianceLocationDictionary setObject:isEmpty(self.locationID)?[NSNull null]:self.locationID forKey:kApplianceLocationID];
    [applianceLocationDictionary setObject:isEmpty(self.locationName)?[NSNull null]:self.locationName forKey:kApplianceLocationName];
    
    return applianceLocationDictionary;
}
@end
