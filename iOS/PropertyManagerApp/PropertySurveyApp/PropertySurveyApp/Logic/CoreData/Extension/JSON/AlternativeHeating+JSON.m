//
//  AlternativeHeating+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AlternativeHeating+JSON.h"
#import "AirSourceInspection+JSON.h"
#import "SolarInspection+JSON.h"
#import "MVHRInspection+JSON.h"
@implementation AlternativeHeating (JSON)
-(NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    NSString *installedDate = isEmpty([UtilityClass convertNSDateToServerDate:self.installedDate])?nil:[UtilityClass convertNSDateToServerDate:self.installedDate];
    NSString *certificateIssued =isEmpty([UtilityClass convertNSDateToServerDate:self.certificateIssued])?nil:[UtilityClass convertNSDateToServerDate:self.certificateIssued];
    NSString *certificateRenewal = isEmpty([UtilityClass convertNSDateToServerDate:self.certificateRenewal])?nil:[UtilityClass convertNSDateToServerDate:self.certificateRenewal];
    
    
    [mutDict setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [mutDict setObject:isEmpty(self.heatingName)?[NSNull null]:self.heatingName forKey:kHeatingName];
    [mutDict setObject:isEmpty(self.heatingFuel)?[NSNull null]:self.heatingFuel forKey:kHeatingFuel];
    [mutDict setObject:isEmpty(self.heatingType)?[NSNull null]:self.heatingType forKey:kHeatingType];
    [mutDict setObject:isEmpty(self.manufacturerId)?[NSNull null]:self.manufacturerId forKey:kBoilerManufacturerId];
    [mutDict setObject:isEmpty(self.model)?[NSNull null]:self.model forKey:kBoilerModel];
    [mutDict setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kBoilerSerialNumber];
    [mutDict setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kLocation];
    [mutDict setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    
    [mutDict setObject:isEmpty(installedDate)?[NSNull null]:installedDate forKey:kInstalledDate];
    [mutDict setObject:isEmpty(self.certificateName)?[NSNull null]:self.certificateName forKey:kCertificateName];
    [mutDict setObject:isEmpty(certificateIssued)?[NSNull null]:certificateIssued forKey:kCertiticateIssued];
    [mutDict setObject:isEmpty(self.certificateNumber)?[NSNull null]:self.certificateNumber forKey:kCertificateNumber];
    [mutDict setObject:isEmpty(certificateRenewal)?[NSNull null]:certificateRenewal forKey:kCertificateRenewal];
    [mutDict setObject:isEmpty(self.solarTypeId)?[NSNull null]:self.solarTypeId forKey:kSolarTypeId];
    [mutDict setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [mutDict setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [mutDict setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
    
    NSDictionary *airsourceInspection = !isEmpty(self.alternativeHeatingToAirsourceInspection)?[self.alternativeHeatingToAirsourceInspection JSONValue]:nil;
    NSDictionary *mvhrInspection = !isEmpty(self.alternativeHeatingToMVHRInspection)?[self.alternativeHeatingToMVHRInspection JSONValue]:nil;
    NSDictionary *solarInspection = !isEmpty(self.alternativeHeatingToSolarInspection)?[self.alternativeHeatingToSolarInspection JSONValue]:nil;
    
    [mutDict setObject:isEmpty(airsourceInspection)?[NSNull null]:airsourceInspection forKey:kAirsourceInspectionForm];
    [mutDict setObject:isEmpty(mvhrInspection)?[NSNull null]:mvhrInspection forKey:kMVHRInspectionForm];
    [mutDict setObject:isEmpty(solarInspection)?[NSNull null]:solarInspection forKey:kSolarInspectionForm];
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
