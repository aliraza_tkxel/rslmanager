//
//  OilHeating+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "OilHeating+JSON.h"
#import "OilFiringServiceInspection+JSON.h"
#import "Defect+JSON.h"
@implementation OilHeating (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    
    NSString *certificateIssued =isEmpty([UtilityClass convertNSDateToServerDate:self.certificateIssued])?nil:[UtilityClass convertNSDateToServerDate:self.certificateIssued];
    NSString *certificateRenewal = isEmpty([UtilityClass convertNSDateToServerDate:self.certificateRenewel])?nil:[UtilityClass convertNSDateToServerDate:self.certificateRenewel];
    NSString *installedDate = isEmpty([UtilityClass convertNSDateToServerDate:self.originalInstallDate])?nil:[UtilityClass convertNSDateToServerDate:self.originalInstallDate];
    
    [mutDict setObject:isEmpty(self.applianceLocation)?[NSNull null]:self.applianceLocation forKey:kOilHeatingApplianceLocation];
    [mutDict setObject:isEmpty(self.applianceMake)?[NSNull null]:self.applianceMake forKey:kOilHeatingApplianceMake];
    [mutDict setObject:isEmpty(self.applianceModel)?[NSNull null]:self.applianceModel forKey:kOilHeatingApplianceModel];
    [mutDict setObject:isEmpty(self.applianceSerialNumber)?[NSNull null]:self.applianceSerialNumber forKey:kOilHeatingApplianceSerialNumber];
    [mutDict setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
    [mutDict setObject:isEmpty(self.burnerMake)?[NSNull null]:self.burnerMake forKey:kBurnerMake];
    [mutDict setObject:isEmpty(self.burnerModel)?[NSNull null]:self.burnerModel forKey:kBurnerModel];
    [mutDict setObject:isEmpty(self.burnerTypeId)?[NSNull null]:self.burnerTypeId forKey:kBurnerTypeId];
    [mutDict setObject:isEmpty(self.certificateName)?[NSNull null]:self.certificateName forKey:kCertificateName];
    [mutDict setObject:isEmpty(certificateIssued)?[NSNull null]:certificateIssued forKey:kCertiticateIssued];
    [mutDict setObject:isEmpty(self.certificateNumber)?[NSNull null]:self.certificateNumber forKey:kCertificateNumber];
    [mutDict setObject:isEmpty(certificateRenewal)?[NSNull null]:certificateRenewal forKey:kCertificateRenewal];
    [mutDict setObject:isEmpty(self.flueTypeId)?[NSNull null]:self.flueTypeId forKey:kFlueTypeId];
    [mutDict setObject:isEmpty(self.fuelTypeId)?[NSNull null]:self.fuelTypeId forKey:kFuelTypeId];
    [mutDict setObject:isEmpty(self.heatingFuel)?[NSNull null]:self.heatingFuel forKey:kHeatingFuel];
    [mutDict setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [mutDict setObject:isEmpty(self.heatingName)?[NSNull null]:self.heatingName forKey:kHeatingName];
    [mutDict setObject:isEmpty(self.heatingType)?[NSNull null]:self.heatingType forKey:kHeatingType];
    [mutDict setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [mutDict setObject:isEmpty(self.itemId)?[NSNull null]:self.itemId forKey:kItemId];
     [mutDict setObject:isEmpty(installedDate)?[NSNull null]:installedDate forKey:kOriginalInstallDate];
    [mutDict setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [mutDict setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [mutDict setObject:isEmpty(self.tankTypeId)?[NSNull null]:self.tankTypeId forKey:kTankTypeId];
    
    if(!isEmpty(self.oilHeatingToFireServiceInspection)){
        NSDictionary *dict1 = [self.oilHeatingToFireServiceInspection JSONValue];
        [mutDict setObject:dict1 forKey:@"oilFiringAndService"];
    }
    else{
        [mutDict setObject:[NSNull null] forKey:@"oilFiringAndService"];
    }
    
    NSMutableArray *defectsArr = [NSMutableArray array];
    for (Defect *defect in self.oilHeatingToDefect)
    {
        [defectsArr addObject:[defect JSONValue]];
    }
    [mutDict setObject:defectsArr forKey:@"oilDefects"];
    
    
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
