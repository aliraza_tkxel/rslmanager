//
//  ApplianceModel+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceModel+JSON.h"

@implementation ApplianceModel (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *applianceModelDictionary = [NSMutableDictionary dictionary];
    [applianceModelDictionary setObject:isEmpty(self.modelID)?[NSNull null]:self.modelID forKey:kApplianceModelID];
    [applianceModelDictionary setObject:isEmpty(self.modelName)?[NSNull null]:self.modelName forKey:kApplianceModelName];
    return applianceModelDictionary;
}
@end
