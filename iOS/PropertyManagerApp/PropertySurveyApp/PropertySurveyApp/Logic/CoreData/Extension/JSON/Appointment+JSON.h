//
//  Appointment+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appointment+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface Appointment (JSON)

- (NSDictionary *) JSONValue;


#pragma mark - Methods
- (void) startAppointment;
- (void) acceptAppointment;
- (void) completeAppointment;
- (BOOL) isPreparedForOffline;
- (AppointmentStatus) getStatus;
- (AppointmentType) getType;
- (Customer *) defaultTenant;
- (NSString *) stringWithAppointmentStatus:(AppointmentStatus)status;
- (NSString *) appointmentDateWithStyle:(AppointmentDateStyle)style dateFormat:(NSString *)dateFormat;

@end

NS_ASSUME_NONNULL_END
