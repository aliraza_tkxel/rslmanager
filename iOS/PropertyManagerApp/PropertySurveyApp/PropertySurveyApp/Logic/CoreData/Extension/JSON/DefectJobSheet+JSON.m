//
//  DefectJobSheet+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DefectJobSheet+JSON.h"
#import "JobPauseData+JSON.h"

@implementation DefectJobSheet (JSON)

- (NSDictionary *) JSONValue
{
	
	NSMutableDictionary *jobDataDictionary = [NSMutableDictionary dictionary];
	
	NSString *completionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.completionDate])?nil:[UtilityClass convertNSDateToServerDate:self.completionDate];
	NSString *partsDueDate = isEmpty([UtilityClass convertNSDateToServerDate:self.partsDueDate])?nil:[UtilityClass convertNSDateToServerDate:self.partsDueDate];
	NSString *defectDate = isEmpty([UtilityClass convertNSDateToServerDate:self.defectDate])?nil:[UtilityClass convertNSDateToServerDate:self.defectDate];
    NSString *actualStartDateTime = isEmpty([UtilityClass convertNSDateToServerDate:self.actualStartTime])?nil:[UtilityClass convertNSDateToServerDate:self.actualStartTime];
    NSString *actualEndDateTime = isEmpty([UtilityClass convertNSDateToServerDate:self.actualEndTime])?nil:[UtilityClass convertNSDateToServerDate:self.actualEndTime];
	
    // Adding jobsheet actual start and end date
    [jobDataDictionary setObject:isEmpty(self.actualStartTime)? [NSNull null] :actualStartDateTime  forKey:kJSActualStartTime];
    [jobDataDictionary setObject:isEmpty(self.actualEndTime)? [NSNull null] :actualEndDateTime  forKey:kJSActualEndTime];
    [jobDataDictionary setObject:isEmpty(self.defectCompletionNotes)?[NSNull null]:self.defectCompletionNotes forKey:kDefectCompletionNotes];
	[jobDataDictionary setObject:isEmpty(self.jsNumber)?[NSNull null]:self.jsNumber forKey:kJSNumber];
	[jobDataDictionary setObject:isEmpty(self.inspectionRef)?[NSNull null]:self.inspectionRef forKey:kInspectionRef];
	[jobDataDictionary setObject:isEmpty(self.jobStatus)?[NSNull null]:self.jobStatus forKey:kJobStatus];
	[jobDataDictionary setObject:isEmpty(self.applianceId)?[NSNull null]:self.applianceId forKey:kApplianceId];
	[jobDataDictionary setObject:isEmpty(self.boilerTypeId)?[NSNull null]:self.boilerTypeId forKey:kDefectJobdataBoilerTypeId];
	[jobDataDictionary setObject:isEmpty(self.defectId)?[NSNull null]:self.defectId forKey:kDefectId];
	[jobDataDictionary setObject:isEmpty(self.defectRecordedBy)?[NSNull null]:self.defectRecordedBy forKey:kDefectRecordedBy];
	[jobDataDictionary setObject:isEmpty(defectDate)?[NSNull null]:defectDate forKey:kDefectDate];
	[jobDataDictionary setObject:isEmpty(self.trade)?[NSNull null]:self.trade forKey:kTradeDescription];
	[jobDataDictionary setObject:isEmpty(self.appliance)?[NSNull null]:self.appliance forKey:kApplianceName];
	[jobDataDictionary setObject:isEmpty(self.make)?[NSNull null]:self.make forKey:kApplianceMake];
	[jobDataDictionary setObject:isEmpty(self.model)?[NSNull null]:self.model forKey:kModel];
	[jobDataDictionary setObject:isEmpty(self.isTwoPersonsJob)?[NSNull null]:self.isTwoPersonsJob forKey:kIsTwoPersonsJob];
	[jobDataDictionary setObject:isEmpty(self.defectCategoryId)?[NSNull null]:self.defectCategoryId forKey:kDefectCategoryId];
	[jobDataDictionary setObject:isEmpty(self.defectNotes)?[NSNull null]:self.defectNotes forKey:kDefectNotes];
	[jobDataDictionary setObject:isEmpty(self.isRemedialActionTaken)?[NSNull null]:self.isRemedialActionTaken forKey:kIsRemedialActionTaken];
	[jobDataDictionary setObject:isEmpty(self.remedialActionNotes)?[NSNull null]:self.remedialActionNotes forKey:kRemedialActionNotes];
	[jobDataDictionary setObject:isEmpty(self.isAdviceNoteIssued)?[NSNull null]:self.isAdviceNoteIssued forKey:kisAdviceNoteIssued];
	[jobDataDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kSerialNumber];
	[jobDataDictionary setObject:isEmpty(self.gasCouncilNumber)?[NSNull null]:self.gasCouncilNumber forKey:kGasCouncilNumber];
	[jobDataDictionary setObject:isEmpty(self.warningTagFixed)?[NSNull null]:self.warningTagFixed forKey:kWarningTagFixed];
	[jobDataDictionary setObject:isEmpty(self.isDisconnected)?[NSNull null]:self.isDisconnected forKey:kIsDisconnected];
	[jobDataDictionary setObject:isEmpty(self.partsOrderedBy)?[NSNull null]:self.partsOrderedBy forKey:kPartsOrderedBy];
	[jobDataDictionary setObject:isEmpty(partsDueDate)?[NSNull null]:partsDueDate forKey:kPartsDueDate];
	[jobDataDictionary setObject:isEmpty(self.partsLocation)?[NSNull null]:self.partsLocation forKey:kPartsLocation];
	[jobDataDictionary setObject:isEmpty(self.isCustomerHaveHeating)?[NSNull null]:self.isCustomerHaveHeating forKey:kIsCustomerHaveHeating];
	[jobDataDictionary setObject:isEmpty(self.isHeatersLeft)?[NSNull null]:self.isHeatersLeft forKey:kIsHeatersLeft];
	[jobDataDictionary setObject:isEmpty(self.numberOfHeatersLeft)?[NSNull null]:self.numberOfHeatersLeft forKey:kNumberOfHeatersLeft];
	[jobDataDictionary setObject:isEmpty(self.isCustomerHaveHotWater)?[NSNull null]:self.isCustomerHaveHotWater forKey:kIsCustomerHaveHotWater];
	[jobDataDictionary setObject:isEmpty(completionDate)?[NSNull null]:completionDate forKey:kCompletionDate];
    [jobDataDictionary setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
    [jobDataDictionary setObject:isEmpty(self.boilerTypeId)?[NSNull null]:self.boilerTypeId forKey:kBoilerTypeId];

	NSMutableArray *jobPauseHistoryArray = [NSMutableArray array];
	for (JobPauseData *jobPauseData in self.jobPauseData)
	{
		[jobPauseHistoryArray addObject:[jobPauseData JSONValue]];
	}

	[jobDataDictionary setObject:jobPauseHistoryArray forKey:@"jobActivityHistory"];
	return jobDataDictionary;
	
}
@end
