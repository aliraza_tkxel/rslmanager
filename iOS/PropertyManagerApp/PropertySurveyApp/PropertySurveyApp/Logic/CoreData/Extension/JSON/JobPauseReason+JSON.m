//
//  JobPauseReason+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobPauseReason+JSON.h"

@implementation JobPauseReason (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *jobPauseDictionary = [NSMutableDictionary dictionary];
    
    [jobPauseDictionary setObject:isEmpty(self.pauseReason)?[NSNull null]:self.pauseReason forKey:@"pauseReason"];
    return jobPauseDictionary;
}


@end
