  //
  //  Defect+JSON.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 20/11/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "Defect+JSON.h"
#import "DefectCategory+JSON.h"
#import "PSDataPersistenceManager+(Defects).h"

@implementation Defect (JSON)

- (NSDictionary *) JSONValue
{
  NSMutableDictionary *defectDictionary = [NSMutableDictionary dictionary];
  
  NSString *defectDate = isEmpty([UtilityClass convertNSDateToServerDate:self.defectDate])?nil:[UtilityClass convertNSDateToServerDate:self.defectDate];
	NSString *partsDueDate = isEmpty([UtilityClass convertNSDateToServerDate:self.partsDueDate])?nil:[UtilityClass convertNSDateToServerDate:self.partsDueDate];
	
  [defectDictionary setObject:isEmpty(self.applianceID)?[NSNull null]:self.applianceID forKey:kApplianceId];
	[defectDictionary setObject:isEmpty(self.boilerTypeId)?[NSNull null]:self.boilerTypeId forKey:kBoilerTypeId];
  [defectDictionary setObject:isEmpty(self.detectorTypeId)?[NSNull null]:self.detectorTypeId forKey:kDetectorTypeId];
  [defectDictionary setObject:isEmpty(defectDate)?[NSNull null]:defectDate forKey:kDefectDate];
  [defectDictionary setObject:isEmpty(self.defectNotes)?[NSNull null]:self.defectNotes forKey:kDefectNotes];
  [defectDictionary setObject:isEmpty(self.defectID)?[NSNull null]:self.defectID forKey:kDefectID];
  [defectDictionary setObject:isEmpty(self.faultCategory)?[NSNull null]:self.faultCategory forKey:kDefectCategoryType];
  [defectDictionary setObject:isEmpty(self.isRemedialActionTaken)?[NSNull null]:self.isRemedialActionTaken forKey:kIsRemedialActionTaken];
  [defectDictionary setObject:isEmpty(self.isAdviceNoteIssued)?[NSNull null]:self.isAdviceNoteIssued forKey:kisAdviceNoteIssued];
	[defectDictionary setObject:isEmpty(self.warningNoteSerialNo)?[NSNull null]:self.warningNoteSerialNo forKey:kWarningNoteSerialNo];
  [defectDictionary setObject:isEmpty(self.isDefectIdentified)?[NSNull null]:self.isDefectIdentified forKey:kIsDefectIdentified];
  [defectDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:@"JournalId"];
  [defectDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
  [defectDictionary setObject:isEmpty(self.remedialActionNotes)?[NSNull null]:self.remedialActionNotes forKey:kRemedialActionNotes];
  [defectDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kSerialNo];
  [defectDictionary setObject:isEmpty(self.warningTagFixed)?[NSNull null]:self.warningTagFixed forKey:kWarningTagFixed];
  
  [defectDictionary setObject:isEmpty(self.gasCouncilNumber)?[NSNull null]:self.gasCouncilNumber forKey:kGasCouncilNumber];
  [defectDictionary setObject:isEmpty(self.isDisconnected)?[NSNull null]:self.isDisconnected forKey:kIsDisconnected];
  [defectDictionary setObject:isEmpty(self.isPartsRequired)?[NSNull null]:self.isPartsRequired forKey:kIsPartsRequired];
  [defectDictionary setObject:isEmpty(self.isPartsOrdered)?[NSNull null]:self.isPartsOrdered forKey:kIsPartsOrdered];
  [defectDictionary setObject:isEmpty(self.partsOrderedBy)?[NSNull null]:self.partsOrderedBy forKey:kPartsOrderedBy];
  [defectDictionary setObject:isEmpty(partsDueDate)?[NSNull null]:partsDueDate forKey:kPartsDueDate];
  [defectDictionary setObject:isEmpty(self.partsDescription)?[NSNull null]:self.partsDescription forKey:kPartsDescription];
  [defectDictionary setObject:isEmpty(self.partsLocation)?[NSNull null]:self.partsLocation forKey:kPartsLocation];
  [defectDictionary setObject:isEmpty(self.isTwoPersonsJob)?[NSNull null]:self.isTwoPersonsJob forKey:kIsTwoPersonsJob];
  [defectDictionary setObject:isEmpty(self.reasonForTwoPerson)?[NSNull null]:self.reasonForTwoPerson forKey:kReasonForTwoPerson];
  [defectDictionary setObject:isEmpty(self.duration)?[NSNull null]:self.duration forKey:kDefectDuration];
  [defectDictionary setObject:isEmpty(self.priorityId)?[NSNull null]:self.priorityId forKey:kDefectPriorityId];
  [defectDictionary setObject:isEmpty(self.tradeId)?[NSNull null]:self.tradeId forKey:kDefectTradeId];
  [defectDictionary setObject:isEmpty(self.isCustomerHaveHeating)?[NSNull null]:self.isCustomerHaveHeating forKey:kIsCustomerHaveHeating];
  [defectDictionary setObject:isEmpty(self.isHeatersLeft)?[NSNull null]:self.isHeatersLeft forKey:kIsHeatersLeft];
  [defectDictionary setObject:isEmpty(self.numberOfHeatersLeft)?[NSNull null]:self.numberOfHeatersLeft forKey:kNumberOfHeatersLeft];
  [defectDictionary setObject:isEmpty(self.isCustomerHaveHotWater)?[NSNull null]:self.isCustomerHaveHotWater forKey:kIsCustomerHaveHotWater];
    [defectDictionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [defectDictionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
  [defectDictionary setObject:isEmpty(self.heatingId)?[NSNull null]:self.heatingId forKey:kHeatingId];
  /* NSDictionary *defectCategoryDictionary = [NSDictionary dictionaryWithDictionary:[self.defectCategory JSONValue]];
   [defectDictionary setObject:defectCategoryDictionary forKey:@"defectcategory"];*/
  return defectDictionary;
}

-(void)updateDefectValuesWithDictionary:(NSDictionary*)defectDictionary
{
  NSDictionary * parameterDictionary = [defectDictionary objectForKey:kResponseTag];
	
	NSLog(@"Response (Save Defect) : %@ ",parameterDictionary);
	
  [[PSDataPersistenceManager sharedManager] updateDefectObject:self withDictionary:parameterDictionary];
}

-(void)saveDefectOnServer
{
  NSMutableDictionary * parameterDict = [[NSMutableDictionary alloc] init];
  [parameterDict setObject:[self JSONValue] forKey:@"defect"];
  [parameterDict setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
  [parameterDict setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
  [PSApplianceManager sharedManager].saveDefectService.defectDelegate = self;
  [[PSApplianceManager sharedManager] saveApplianceDefect:parameterDict];
	
	NSLog(@"Request (Save Defect) : %@ ",parameterDict);
	
}

#pragma mark PSSaveDefectServiceDelegate
- (void) service:(PSSaveDefectService *)service didReceiveDefectDataAfterSuccess:(id)surveyorData {
  
  NSString * responseString=surveyorData;
  
  if(!isEmpty(responseString))
    {
    NSDictionary *JSONDictionary = [responseString JSONValue];
    [self updateDefectValuesWithDictionary:JSONDictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationSuccess object:nil];
    return;
    }
  else
    {
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
    return;
    }
}

- (void) service:(PSSaveDefectService *)service didFailWithError:(NSError *)error {
  
  [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationFail object:error];
}

@end
