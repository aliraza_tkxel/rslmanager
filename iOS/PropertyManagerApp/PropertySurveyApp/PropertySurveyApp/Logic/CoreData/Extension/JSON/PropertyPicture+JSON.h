//
//  PropertyPicture+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PropertyPicture+CoreDataClass.h"
#import "PSPropertyPictureManager.h"

@interface PropertyPicture (JSON)<PSUploadPhotographServiceDelegate>

- (NSDictionary *) JSONValue;

-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:propertyImage;

-(void) uploadPicture;

-(NSString *) getPicturePath;
@property (strong,nonatomic) UIImage *image;
-(void) updatePropertyProfilePicture:(NSDictionary*) pictureDictionary;
@end
