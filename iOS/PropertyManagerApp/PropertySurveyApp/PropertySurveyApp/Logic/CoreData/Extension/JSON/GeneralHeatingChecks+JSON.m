//
//  GeneralHeatingChecks+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "GeneralHeatingChecks+JSON.h"

@implementation GeneralHeatingChecks (JSON)
- (NSDictionary *) JSONValue{
    NSDictionary *dict = [NSDictionary new];
    NSMutableDictionary *mutDict = [[NSMutableDictionary alloc] init];
    [mutDict setObject:isEmpty(self.radiatorCondition)?[NSNull null]:self.radiatorCondition forKey:kRadiatorCondition];
    [mutDict setObject:isEmpty(self.heatingControl)?[NSNull null]:self.heatingControl forKey:kHeatingControl];
    [mutDict setObject:isEmpty(self.systemUsage)?[NSNull null]:self.systemUsage forKey:kSystemUsage];
    [mutDict setObject:isEmpty(self.generalHeating)?[NSNull null]:self.generalHeating forKey:kGeneralHeating];
    [mutDict setObject:isEmpty(self.accessIssues)?[NSNull null]:self.accessIssues forKey:kAccessIssues];
    [mutDict setObject:isEmpty(self.accessIssueNotes)?[NSNull null]:self.accessIssueNotes forKey:kAccessIssueNotes];
    [mutDict setObject:isEmpty(self.confirmation)?[NSNull null]:self.confirmation forKey:kConfirmation];
    NSString *checkoDate = isEmpty([UtilityClass convertNSDateToServerDate:self.checkedDate])?nil:[UtilityClass convertNSDateToServerDate:self.checkedDate];
     [mutDict setObject:isEmpty(checkoDate)?[NSNull null]:checkoDate forKey:kCheckedDate];
    dict = [[NSDictionary alloc] initWithDictionary:mutDict];
    return dict;
}
@end
