//
//  Property+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Property+JSON.h"
#import "Accomodation+JSON.h"
#import "PropertyAsbestosData+JSON.h"
#import "PropertyPicture+JSON.h"
#import "Appliance+JSON.h"
#import "InstallationPipework+JSON.h"
#import "Detector+JSON.h"
#import "Meter+JSON.h"
#import "Boiler+JSON.h"
#import "AlternativeHeating+JSON.h"
#import "GeneralHeatingChecks+JSON.h"
#import "OilHeating+JSON.h"
@implementation Property (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *propertyDictionary = [NSMutableDictionary dictionary];
    NSString *lastSurveyDate = isEmpty([UtilityClass convertNSDateToServerDate:self.lastSurveyDate])?nil:[UtilityClass convertNSDateToServerDate:self.lastSurveyDate];
    NSString *certificateExpiry = isEmpty([UtilityClass convertNSDateToServerDate:self.certificateExpiry])?nil:[UtilityClass convertNSDateToServerDate:self.certificateExpiry];
    
    [propertyDictionary setObject:isEmpty(self.address1)?[NSNull null]:self.address1 forKey:kAddress1];
    [propertyDictionary setObject:isEmpty(self.address2)?[NSNull null]:self.address2 forKey:kAddress2];
    [propertyDictionary setObject:isEmpty(self.address3)?[NSNull null]:self.address3 forKey:kAddress3];
    [propertyDictionary setObject:isEmpty(certificateExpiry)?[NSNull null]:certificateExpiry forKey:kCertificateExpiry];
    [propertyDictionary setObject:isEmpty(self.county)?[NSNull null]:self.county forKey:kCounty];
    [propertyDictionary setObject:isEmpty(self.flatNumber)?[NSNull null]:self.flatNumber forKey:kFlatNumber];
    [propertyDictionary setObject:isEmpty(self.houseNumber)?[NSNull null]:self.houseNumber forKey:kHouseNumber];
    [propertyDictionary setObject:isEmpty(lastSurveyDate)?[NSNull null]:lastSurveyDate forKey:kLastSurveyDate];
    [propertyDictionary setObject:isEmpty(self.postCode)?[NSNull null]:self.postCode forKey:kPostCode];
    [propertyDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [propertyDictionary setObject:isEmpty(self.tenancyId)?[NSNull null]:self.tenancyId forKey:kTenancyId];
    [propertyDictionary setObject:isEmpty(self.townCity)?[NSNull null]:self.townCity forKey:kTownCity];
    
    [propertyDictionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [propertyDictionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
    [propertyDictionary setObject:isEmpty(self.schemeName)?[NSNull null]:self.schemeName forKey:kSchemeName];
    [propertyDictionary setObject:isEmpty(self.blockName)?[NSNull null]:self.blockName forKey:kSchemeBlockName];
    
   [propertyDictionary setObject:isEmpty(self.propertyToGeneralHeatingChecks)?[NSNull null]:[self.propertyToGeneralHeatingChecks JSONValue] forKey:@"generalHeatingChecks"];
    
    NSMutableArray *propertyAccomodationsArray = [NSMutableArray array];
    for (Accomodation *accomodation in self.propertyToAccomodation)
    {
        [propertyAccomodationsArray addObject:[accomodation JSONValue]];
    }
    NSMutableArray *propertyAsbestosArray = [NSMutableArray array];
    for (PropertyAsbestosData *asbestosData in self.propertyToPropertyAsbestosData)
    {
        [propertyAsbestosArray addObject:[asbestosData JSONValue]];
    }
   /* NSMutableArray *propertyPicturesArray = [NSMutableArray array];
    for (PropertyPicture *propertyPicture in self.propertyToPropertyPicture)
    {
        [propertyPicturesArray addObject:[propertyPicture JSONValue]];
    }*/
    NSMutableArray *propertyAppliancesArray = [NSMutableArray array];
    for (Appliance *appliance in self.propertyToAppliances)
    {
        [propertyAppliancesArray addObject:[appliance JSONValue]];
    }
	
	  NSMutableArray *propertyBoilerArray = [NSMutableArray array];
	  for (Boiler *boiler in self.propertyToBoiler)
	  {
				[propertyBoilerArray addObject:[boiler JSONValue]];
	  }
	
    NSMutableArray *propertyDetectorsArray = [NSMutableArray array];
    for (Detector *detector in self.propertyToDetector)
    {
        [propertyDetectorsArray addObject:[detector JSONValue]];
    }
  
    NSMutableArray *propertyMetersArray = [NSMutableArray array];
    for (Meter *meter in self.propertyToMeter)
    {
        [propertyMetersArray addObject:[meter JSONValue]];
    }
  
    NSMutableArray *propertyAlternativeHeatingsArray = [NSMutableArray array];
    if(!isEmpty(self.propertyToAlternativeHeating)){
        for(AlternativeHeating *heato in self.propertyToAlternativeHeating){
            [propertyAlternativeHeatingsArray addObject:[heato JSONValue]];
        }
    }
    
    NSMutableArray *propertyOilHeatingsArray = [NSMutableArray array];
    if(!isEmpty(self.propertyToOilHeatings)){
        for(OilHeating *heato in self.propertyToOilHeatings){
            [propertyOilHeatingsArray addObject:[heato JSONValue]];
        }
    }
    
    [propertyDictionary setObject:propertyAccomodationsArray forKey:@"Accommodations"];
    [propertyDictionary setObject:propertyAsbestosArray forKey:@"propertyAsbestosData"];
   // [propertyDictionary setObject:propertyPicturesArray forKey:@"propertyPicture"];
    [propertyDictionary setObject:propertyAppliancesArray forKey:@"appliances"];
	  [propertyDictionary setObject:propertyBoilerArray forKey:@"boilers"];
    [propertyDictionary setObject:propertyDetectorsArray forKey:@"propertyDetectors"];
    [propertyDictionary setObject:propertyMetersArray forKey:@"meters"];
    [propertyDictionary setObject:propertyAlternativeHeatingsArray forKey:@"alternativeHeatings"];
    [propertyDictionary setObject:propertyOilHeatingsArray forKey:@"oilHeatings"];
    
    
    return propertyDictionary;
}

@end
