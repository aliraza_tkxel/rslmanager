  //
  //  Detector+JSON.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 26/12/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "Detector+JSON.h"
#import "Defect+JSON.h"
#import "DetectorInspection+JSON.h"

@implementation Detector (JSON)
- (NSDictionary *) JSONValue
{
	
	NSString *objInstalledDate = isEmpty([UtilityClass convertNSDateToServerDate:self.installedDate])?nil:[UtilityClass convertNSDateToServerDate:self.installedDate];
	NSString *objBatteryReplaced = isEmpty([UtilityClass convertNSDateToServerDate:self.batteryReplaced])?nil:[UtilityClass convertNSDateToServerDate:self.batteryReplaced];
	NSString *objInspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
	NSString *objLastTestedDate = isEmpty([UtilityClass convertNSDateToServerDate:self.lastTestedDate])?nil:[UtilityClass convertNSDateToServerDate:self.lastTestedDate];
	
  NSMutableDictionary *detectorDictionary = [NSMutableDictionary dictionary];
  [detectorDictionary setObject:isEmpty(self.detectorType)?[NSNull null]:self.detectorType forKey:kDetectorsType];
  [detectorDictionary setObject:isEmpty(self.detectorTypeId)?[NSNull null]:self.detectorTypeId forKey:kDetectorTypeId];
  [detectorDictionary setObject:isEmpty(objInstalledDate)?[NSNull null]:objInstalledDate forKey:kInstalledDate];
  [detectorDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsDetectorInspected];
  [detectorDictionary setObject:isEmpty(self.isLandlordsDetector)?[NSNull null]:self.isLandlordsDetector forKey:kIsLandlordsDetector];
  [detectorDictionary setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kLocation];
  [detectorDictionary setObject:isEmpty(self.manufacturer)?[NSNull null]:self.manufacturer forKey:kManufacturer];
  [detectorDictionary setObject:isEmpty(self.propertyID)?[NSNull null]:self.propertyID forKey:kPropertyId];
  [detectorDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kSerialNumber];
  [detectorDictionary setObject:isEmpty(self.powerTypeId)?[NSNull null]:self.powerTypeId forKey:kPowerTypeId];
  [detectorDictionary setObject:isEmpty(self.detectorId)?[NSNull null]:self.detectorId forKey:kDetectorId];
  
  [detectorDictionary setObject:isEmpty(objBatteryReplaced)?[NSNull null]:objBatteryReplaced forKey:kBatteryReplaced];
  [detectorDictionary setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kDetectorInspectedBy];
  [detectorDictionary setObject:isEmpty(objInspectionDate)?[NSNull null]:objInspectionDate forKey:kDetectorInspectionDate];
  [detectorDictionary setObject:isEmpty(self.isPassed)?[NSNull null]:self.isPassed forKey:kIsDetectorPassed];
  [detectorDictionary setObject:isEmpty(objLastTestedDate)?[NSNull null]:objLastTestedDate forKey:kDetectorLastTestedDate];
  [detectorDictionary setObject:isEmpty(self.notes)?[NSNull null]:self.notes forKey:kDetectorNotes];
    [detectorDictionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [detectorDictionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
  NSMutableArray *detectorDefectsArray = [NSMutableArray array];
  for (Defect *defect in self.detectorDefects)
    {
    [detectorDefectsArray addObject:[defect JSONValue]];
    }
   [detectorDictionary setObject:detectorDefectsArray forKey:@"detectorDefects"];
  
  
    //[detectorDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kApplianceIsInspected];
    //todo
    // NSDictionary *detectorInspectionDictionary = [NSDictionary dictionaryWithDictionary:[self.detectorInspection JSONValue]];
    //[detectorDictionary setObject:detectorInspectionDictionary forKey:@"detectorInspection"];
  
  return detectorDictionary;
}
@end
