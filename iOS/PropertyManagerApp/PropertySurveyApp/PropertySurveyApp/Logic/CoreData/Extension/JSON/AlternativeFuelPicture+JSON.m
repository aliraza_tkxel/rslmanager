//
//  AlternativeFuelPicture+JSON.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 02/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "AlternativeFuelPicture+JSON.h"
#import "PSAltFuelServicingPictureManager.h"
#import "AlternativeFuelPicture+MWPhoto.h"
#import <objc/runtime.h>
static void * ImagePropertyKey = &ImagePropertyKey;

@implementation AlternativeFuelPicture (JSON)

- (UIImage *)image {
    return objc_getAssociatedObject(self, ImagePropertyKey);
}

- (void)setImage:(UIImage *)image {
    objc_setAssociatedObject(self, ImagePropertyKey, image, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *picturesDictionary = [NSMutableDictionary dictionary];
    NSString *createdOn = isEmpty([UtilityClass convertNSDateToServerDate:self.createdOn])?nil:[UtilityClass convertNSDateToServerDate:self.createdOn];
    
    [picturesDictionary setObject:isEmpty(self.appointmentId)?[NSNumber numberWithInt:0]:self.appointmentId forKey:@"appointmentId"];
    [picturesDictionary setObject:isEmpty(self.createdBy)?[NSNull null]:self.createdBy forKey:@"createdBy"];
    [picturesDictionary setObject:isEmpty(self.isDefault)?@"false":@"true" forKey:@"isDefault"];
    [picturesDictionary setObject:isEmpty(self.itemId)?[NSNull null]:self.itemId forKey:@"itemId"];
    [picturesDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [picturesDictionary setObject:isEmpty(self.propertyPictureId)?[NSNumber numberWithInt:0]:self.propertyPictureId forKey:@"propertyPicId"];
    if(!isEmpty(self.itemId)){
        [picturesDictionary setObject:self.itemId forKey:kItemId];
    }
    
    
    [picturesDictionary setObject:self.appointmentId?:[NSNumber numberWithInt:0] forKey:kAppointmentId];
    Appointment *appt;
    if(!isEmpty(self.alternativeFuelPictureToMVHRInspection)){
        [picturesDictionary setObject:self.alternativeFuelPictureToMVHRInspection.mvhrInspectionToHeating.heatingId?:[NSNumber numberWithInt:0] forKey:kHeatingId];
        appt = self.alternativeFuelPictureToMVHRInspection.mvhrInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    else if(!isEmpty(self.alternativeFuelPictureToSolarInspection)){
        [picturesDictionary setObject:self.alternativeFuelPictureToSolarInspection.solarInspectionToHeating.heatingId?:[NSNumber numberWithInt:0] forKey:kHeatingId];
        appt = self.alternativeFuelPictureToSolarInspection.solarInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    else if(!isEmpty(self.alternativeFuelPictureToAirSourceInspection)){
        [picturesDictionary setObject:self.alternativeFuelPictureToAirSourceInspection.airsourceInspectionToHeating.heatingId?:[NSNumber numberWithInt:0] forKey:kHeatingId];
        appt = self.self.alternativeFuelPictureToAirSourceInspection.airsourceInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    
    [picturesDictionary setObject:appt.journalId?:[NSNull null] forKey:kJournalId];
    return picturesDictionary;
    
}
-(NSString *) getPicturePath
{
    if (self.imagePath) {
        
        return self.imagePath;
    }
    NSString *idPAth = @"";
    if(!isEmpty(self.alternateFuelPictureToProperty.propertyId)){
        idPAth = self.alternateFuelPictureToProperty.propertyId;
    }
    else if(!isEmpty(self.alternateFuelPictureToProperty.schemeId)){
        idPAth = [self.alternateFuelPictureToProperty.schemeId stringValue];
    }
    else if(!isEmpty(self.alternateFuelPictureToProperty.blockId)){
        idPAth = [self.alternateFuelPictureToProperty.blockId stringValue];
    }
    return [NSString stringWithFormat:@"%@-%@-%@-%@",[self.appointmentId stringValue],[self.itemId stringValue],idPAth,[UtilityClass stringFromDate:self.createdOn dateFormat:@"HH-mm-ss"]];
}

-(NSMutableDictionary*) getJSON
{
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    
    //[requestParameters setObject:self.appointmentId?:[NSNumber numberWithInt:0] forKey:kAppointmentId];
    Appointment *appt;
    if(!isEmpty(self.alternativeFuelPictureToMVHRInspection)){
        appt = self.alternativeFuelPictureToMVHRInspection.mvhrInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    else if(!isEmpty(self.alternativeFuelPictureToSolarInspection)){
        appt = self.alternativeFuelPictureToSolarInspection.solarInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    else if(!isEmpty(self.alternativeFuelPictureToAirSourceInspection)){
        appt = self.self.alternativeFuelPictureToAirSourceInspection.airsourceInspectionToHeating.alternativeHeatingToProperty.propertyToAppointment;
    }
    //[requestParameters setObject:appt.appointmentType forKey:@"type"];
    [requestParameters setObject:self.heatingId?:[NSNull null] forKey:kHeatingId];
    [requestParameters setObject:self.itemId?:[NSNull null] forKey:kItemId];
    [requestParameters setObject:appt.journalId?:[NSNull null] forKey:kJournalId];
    [requestParameters setObject:!isEmpty(self.propertyId)?self.propertyId:[NSNull null] forKey:kPropertyId];
    if(!isEmpty(appt.appointmentToProperty.schemeId)){
        [requestParameters setObject:appt.appointmentToProperty.schemeId forKey:kSchemeID];
    }
    else if(!isEmpty(appt.appointmentToProperty.blockId)){
        [requestParameters setObject:appt.appointmentToProperty.blockId forKey:kSchemeBlockID];
    }
    
    BOOL isDefault= false;
    
    //[requestParameters setObject:isDefault?@"true":@"false" forKey:@"isDefault"];
    
    //[requestParameters setObject:self.propertyPictureId?:[NSNumber numberWithInt:0] forKey:@"propertyPicId"];
    
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    [requestParameters setObject:self.imageIdentifier forKey:kUniqueImageIdentifier];
    
    return requestParameters;
    
}

-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage
{
    
    propertyImage = [propertyImage scaleToSize:CGSizeMake(500, 500)];
    
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager storeImage:propertyImage forKey:[self getPicturePath]];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                        object:self];
    
}

-(void) uploadPicture
{
    self.image=[[self underlyingImage] scaleToSize:CGSizeMake(500, 500)];
    [PSAltFuelServicingPictureManager sharedManager].uploadPhotographService.photographDelegate = self;
    [[PSAltFuelServicingPictureManager sharedManager] uploadPhotograph:[self getJSON] withImage:self.image];
}


- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didReceivePhotographData:(id)surveyorData {
    [PSAltFuelServicingPictureManager sharedManager].isPostingImage=NO;
    NSString * responseString=surveyorData;
    
    if(!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        NSMutableDictionary *dicter = [JSONDictionary objectForKey:@"response"];
        [dicter setObject:@1 forKey:kPropertyPicturesSynchStatus];
        NSMutableDictionary *mutableResponseDict = [[NSMutableDictionary alloc] initWithDictionary:JSONDictionary];
        [mutableResponseDict setObject:dicter forKey:@"response"];
        
        self.isDefault=[NSNumber numberWithBool:YES];
        
        [[PSDataUpdateManager sharedManager] updateAltFuelPicture:self forDictionary:[mutableResponseDict objectForKey:@"response"]];
        
        if (self.image) {
            
            FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
            
            [manager storeImage:self.image forKey:[[JSONDictionary objectForKey:@"response"] objectForKey:kPropertyPicturePath]];
        }
        
        
        NSDictionary * dict=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1] forKey:kPropertyPicturesSynchStatus];
        
        //[[PSDataUpdateManager sharedManager] updatePropertyPicture:self forDictionary:dict];
        
        self.image=nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:nil];
    }
}

- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didFailWithError:(NSError *)error
{
    [PSAltFuelServicingPictureManager sharedManager].isPostingImage=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:error];
    self.image = nil;
}




@end
