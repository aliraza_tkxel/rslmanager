//
//  ApplianceManufacturer+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceManufacturer+JSON.h"

@implementation ApplianceManufacturer (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *applianceManufecturerDictionary = [NSMutableDictionary dictionary];
    [applianceManufecturerDictionary setObject:isEmpty(self.manufacturerID)?[NSNull null]:self.manufacturerID forKey:kApplianceManufacturerID];
    [applianceManufecturerDictionary setObject:isEmpty(self.manufacturerName)?[NSNull null]:self.manufacturerName forKey:kApplianceManufacturerName];
    
    return applianceManufecturerDictionary;
}
@end
