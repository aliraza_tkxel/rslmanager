  //
  //  Meter+JSON.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 26/12/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "Meter+JSON.h"

@implementation Meter (JSON)
- (NSDictionary *) JSONValue
{
	
	NSString *objInstalledDate = isEmpty([UtilityClass convertNSDateToServerDate:self.installedDate])?nil:[UtilityClass convertNSDateToServerDate:self.installedDate];
	NSString *objLastReadingDate = isEmpty([UtilityClass convertNSDateToServerDate:self.lastReadingDate])?nil:[UtilityClass convertNSDateToServerDate:self.lastReadingDate];
	NSString *objInspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
	NSString *objReadingDate = isEmpty([UtilityClass convertNSDateToServerDate:self.readingDate])?nil:[UtilityClass convertNSDateToServerDate:self.readingDate];
	
  NSMutableDictionary *meterDictionary = [NSMutableDictionary dictionary];
  [meterDictionary setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kMeterInspectedBy];
  [meterDictionary setObject:isEmpty(objInspectionDate)?[NSNull null]:objInspectionDate forKey:kMeterInspectionDate];
  [meterDictionary setObject:isEmpty(objInstalledDate)?[NSNull null]:objInstalledDate forKey:kMeterInstalledDate];
  [meterDictionary setObject:isEmpty(self.isCapped)?[NSNull null]:self.isCapped forKey:kMeterIsCapped];
  [meterDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kMeterIsInspected];
  [meterDictionary setObject:isEmpty(self.isPassed)?[NSNull null]:self.isPassed forKey:kMeterIsPassed];
  [meterDictionary setObject:isEmpty(self.lastReading)?[NSNull null]:self.lastReading forKey:kMeterLastReading];
  [meterDictionary setObject:isEmpty(objLastReadingDate)?[NSNull null]:objLastReadingDate forKey:kMeterLastReadingDate];
  [meterDictionary setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kMeterLocation];
  [meterDictionary setObject:isEmpty(self.manufacturer)?[NSNull null]:self.manufacturer forKey:kMeterManufacturer];
  [meterDictionary setObject:isEmpty(self.meterTypeId)?[NSNull null]:self.meterTypeId forKey:kMeterTypeId];
  [meterDictionary setObject:isEmpty(self.notes)?[NSNull null]:self.notes forKey:kMeterNotes];
  [meterDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kMeterPropertyId];
  [meterDictionary setObject:isEmpty(self.reading)?[NSNull null]:self.reading forKey:kMeterReading];
  [meterDictionary setObject:isEmpty(objReadingDate)?[NSNull null]:objReadingDate forKey:kMeterReadingDate];
  [meterDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kMeterSerialNumber];
	[meterDictionary setObject:isEmpty(self.deviceType)?[NSNull null]:self.deviceType forKey:kDeviceType];
	[meterDictionary setObject:isEmpty(self.deviceTypeId)?[NSNull null]:self.deviceTypeId forKey:kDeviceTypeId];
    
    [meterDictionary setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [meterDictionary setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
  return meterDictionary;
}
@end
