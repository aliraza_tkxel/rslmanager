//
//  ApplianceInspection+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceInspection+JSON.h"

@implementation ApplianceInspection (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    
    [inspectionFormDictionary setObject:isEmpty(self.isInspected)?[NSNumber numberWithBool:NO]:self.isInspected forKey:kIsInspected];
    [inspectionFormDictionary setObject:isEmpty(self.combustionReading)?[NSNull null]:self.combustionReading forKey:kCombustionReading];
    [inspectionFormDictionary setObject:isEmpty(self.operatingPressure)?[NSNull null]:self.operatingPressure forKey:kOperatingPressure];
    [inspectionFormDictionary setObject:isEmpty(self.safetyDeviceOperational)?[NSNull null]:self.safetyDeviceOperational forKey:kSafetyDeviceOperational];
    [inspectionFormDictionary setObject:isEmpty(self.smokePellet)?[NSNull null]:self.smokePellet forKey:kSmokePellet];
    [inspectionFormDictionary setObject:isEmpty(self.adequateVentilation)?[NSNull null]:self.adequateVentilation forKey:kAdequateVentilation];
    [inspectionFormDictionary setObject:isEmpty(self.flueVisualCondition)?[NSNull null]:self.flueVisualCondition forKey:kFlueVisualCondition];
    [inspectionFormDictionary setObject:isEmpty(self.satisfactoryTermination)?[NSNull null]:self.satisfactoryTermination forKey:kSatisfactoryTermination];
    [inspectionFormDictionary setObject:isEmpty(self.fluePerformanceChecks)?[NSNull null]:self.fluePerformanceChecks forKey:kFluePerformanceChecks];
    [inspectionFormDictionary setObject:isEmpty(self.applianceServiced)?[NSNull null]:self.applianceServiced forKey:kApplianceServiced];
    [inspectionFormDictionary setObject:isEmpty(self.applianceSafeToUse)?[NSNull null]:self.applianceSafeToUse forKey:kApplianceSafeTouse];
    [inspectionFormDictionary setObject:isEmpty(self.inspectionID)?[NSNull null]:self.inspectionID forKey:kInspectionId];
    [inspectionFormDictionary setObject:isEmpty(self.inspectedBy)?[NSNull null]:self.inspectedBy forKey:kInspectedBy];
    [inspectionFormDictionary setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [inspectionFormDictionary setObject:isEmpty(self.spillageTest)?[NSNull null]:self.spillageTest forKey:kSpillageTest];
    [inspectionFormDictionary setObject:isEmpty(self.operatingPressureUnit)?[NSNull null]:self.operatingPressureUnit forKey:kOperatingPressureUnit];
    
    return inspectionFormDictionary;
}
@end
