//
//  AppointmentHistory+JSON.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/18/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "AppointmentHistory+CoreDataClass.h"

@interface AppointmentHistory (JSON)
- (NSDictionary *) JSONValue;
@end
