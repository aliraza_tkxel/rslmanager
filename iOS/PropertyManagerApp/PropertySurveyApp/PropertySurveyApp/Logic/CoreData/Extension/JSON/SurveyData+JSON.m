//
//  SurveyData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "SurveyData+JSON.h"
#import "FormData+JSON.h"

@implementation SurveyData (JSON)


- (NSDictionary *) JSONValue
{
    NSMutableDictionary *surveyDataDictionary = [NSMutableDictionary dictionary];
    [surveyDataDictionary setObject:isEmpty(self.appointmentId)?[NSNull null]:self.appointmentId forKey:@"appointmentId"];
    [surveyDataDictionary setObject:isEmpty(self.customerId)?[NSNull null]:self.customerId forKey:@"customerId"];
    [surveyDataDictionary setObject:isEmpty(self.itemId)?[NSNull null]:self.itemId forKey:@"itemId"];
    [surveyDataDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [surveyDataDictionary setObject:isEmpty(self.surveyFormName)?[NSNull null]:self.surveyFormName forKey:@"surveyFormName"];
    [surveyDataDictionary setObject:isEmpty(self.surveyNotesDate)?[NSNull null]:[UtilityClass convertNSDateToServerDate:self.surveyNotesDate] forKey:@"surveyNotesDate"];
    [surveyDataDictionary setObject:isEmpty(self.surveyNotesDetail)?[NSNull null]:self.surveyNotesDetail forKey:@"surveyNotesDetail"];
    //[surveyDataDictionary setObject:isEmpty(self.customerId)?[NSNull null]:self.customerId forKey:@"completedBy"];

    

    NSMutableArray *formDataArray = [NSMutableArray array];
    if(!isEmpty(self.surveyDataToFormData))
    {
        for(FormData *formData in self.surveyDataToFormData)
        {
            [formDataArray addObject:[formData JSONValue]];
        }
    }
    [surveyDataDictionary setObject:isEmpty(formDataArray)?[NSNull null]:formDataArray forKey:@"surveyFields"];
    return surveyDataDictionary;    
}
@end
