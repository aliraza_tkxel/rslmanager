//
//  Macros.h
//  Logic
//
//  Created by Ahmad Ansari on 9/27/10.
//


#pragma mark - Usefull Objective-C Macros
/* Usefull Objective-C Macros */
#define APP_NAME [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]
#define APP_BUILD [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
#define APP_VERSION [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]

#define OPEN_URL(urlString) [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:urlString]]
#define OS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
// Retrieving preference values
#define PREF_KEY_VALUE(x) [[[NSUserDefaultsController sharedUserDefaultsController] values] valueForKey:(x)]
#define PREF_KEY_BOOL(x) [(PREF_KEY_VALUE(x)) boolValue]
#define PREF_SET_KEY_VALUE(x, y) [[[NSUserDefaultsController sharedUserDefaultsController] values] setValue:(y) forKey:(x)]
#define PREF_OBSERVE_VALUE(x, y) [[NSUserDefaultsController sharedUserDefaultsController] addObserver:y forKeyPath:x options:NSKeyValueObservingOptionOld context:nil];

/* key, observer, object */
#define OB_OBSERVE_VALUE(key, observer, object) [(object) addObserver:observer forKeyPath:key options:NSKeyValueObservingOptionOld context:nil];

#define CFAutorelease(cf) ((__typeof(cf))[NSMakeCollectable(cf) autorelease])

#define degreesToRadian(x) (M_PI * (x) / 180.0)


#pragma mark - UIColor Utility Macros
/*!
 @discussion
 Macro to Create an UIColor Object From Hexadecimal Value.
 */
#define UIColorFromHex(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/*!
 @discussion
 Macro to Create an UIColor Object From RGB Values.
 */
#define UIColorFromRGB(rVal,gVal,bVal) [UIColor colorWithRed:((float)rVal)/255.0 green:((float)gVal)/255.0 blue:((float)bVal)/255.0 alpha:1.0]

#define CLEAR_COLOR [UIColor clearColor]
#define LIGHT_GRAY_COLOR [UIColor lightGrayColor]
#define DARK_GRAY_COLOR [UIColor darkGrayColor]
#define GRAY_COLOR [UIColor grayColor]
#define WHITE_COLOR [UIColor whiteColor]
#define RED_COLOR [UIColor redColor]
#define GREEN_COLOR [UIColor greenColor]
#define BLACK_COLOR [UIColor blackColor]
#define CYAN_COLOR [UIColor cyanColor]
#define ORANGE_COLOR [UIColor orangeColor]
#define MAGENTA_COLOR [UIColor magentaColor]

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#define AppThemeFontWithSize(ptSize)    [UIFont fontWithName:@"HelveticaNeue" size:(float)ptSize]

#pragma mark - NSObjcet Emptiness
/*!
 @discussion
 isEmpty Method Checks Data Integrity. Can be used for any NSObject type instance.
 Particularly usefull for NSString, NSData, NSDictionary, NSArray, NSSet, etc.
 */
#ifdef __OBJC__
static inline BOOL isEmpty(id thing) {
	return thing == nil || thing == [NSNull null]
	|| ([thing respondsToSelector:@selector(length)]
		&& [(NSData *)thing length] == 0)
	|| ([thing respondsToSelector:@selector(count)]
		&& [(NSArray *)thing count] == 0)
	|| ([thing respondsToSelector:@selector(count)]
		&& [(NSDictionary *)thing count] == 0);
}

#pragma mark - NSRange Emptiness
/*!
 @discussion
 isRangeEmpty Method Checks NSRange Emptiness / Validity.
 */
static inline BOOL isRangeEmpty(NSRange range) {
    return (range.location == NSNotFound)
    || NSEqualRanges(range, NSMakeRange(0, 0))
    || NSEqualRanges(range, NSMakeRange(NSNotFound, 0));
}
#endif

#pragma mark - iPhone/iPod, iPad & iPhone5 Detection Macros
/*!
 @discussion
 iPad Device Detection Check. UIUserInterfaceIdiomPad represents iPad touch style UI.
 */
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
/*!
 @discussion
 iPhone/iPod Device Detection Check. UIUserInterfaceIdiomPhone represents iPhone and iPod touch style UI.
 */
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
/*!
 @discussion
 iPhone5 Screen Size Check. Actual iPhone5 Height is 568.
 */
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)

#define IS_NETWORK_AVAILABLE [[PSReachibilityManager sharedManager] isConnectedToInternet]

#pragma mark - Logger Macros
/*!
 @discussion
 Logging Macros Definition. AppStore Distributions are identifier using CONFIGURATION_$(CONFIGURATION).
 CONFIGURATION_$(CONFIGURATION) is a Preprocessor Macro (Set in Target Build Settings).
 */
#ifdef CONFIGURATION_AppStoreDistribution
#define DLog(...)
#define DInfo(...)
#define DError(...)
#else
#define DLog(...) if ([[[NSBundle mainBundle] objectForInfoDictionaryKey:kLogsEnabled] boolValue]) NSLog(@"[%d]%s[L:%d] %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);
#define DInfo(...) if ([[[NSBundle mainBundle] objectForInfoDictionaryKey:kLogsEnabled] boolValue]) NSLog(@"[%d] = %s [L:%d]--- %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);
#define DError(...) NSLog(@"[%d] = %s [L:%d]--- %@",[NSThread isMainThread], __PRETTY_FUNCTION__,__LINE__, [NSString stringWithFormat:__VA_ARGS__]);
#endif

#pragma mark - Exception Handling Macros
/*!
 @discussion
 Exception Handling Macros.
 */
#define STARTEXCEPTION @try{
#define ENDEXCEPTION }@catch (NSException * e) {NSLog(@"%s[L:%d] Exception in %@,\n Stacktrace: %@", __PRETTY_FUNCTION__,__LINE__, [e description], [NSThread callStackSymbols]);}


#pragma mark -
#pragma mark Macro for Label Word Wrapping
#pragma mark -

#ifdef __IPHONE_6_0
    # define LINE_BREAK_WORD_WRAP NSLineBreakByWordWrapping
#else
    # define LINE_BREAK_WORD_WRAP UILineBreakModeWordWrap
#endif

#pragma mark -
#pragma mark System version checker Macros

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
