//
//  UtilityClass.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "UtilityClass.h"
#import "PSSynchronizationManager.h"
@implementation UtilityClass

+ (BOOL) isUserLoggedIn
{
    SettingsClass* settings = [SettingsClass sharedObject];
    if (!isEmpty(settings.currentUserId) &&
        !isEmpty(settings.currentUserPassword) &&
        !isEmpty(settings.loggedInUser)/* &&
                                        [PSAppDelegate delegate].userLoggedInToApplication*/)
    {
        return YES;
    }
    return NO;
}

+ (NSString *) userInfo
{
    /*
     General Format: Usename,Salt,ApplicationType
     Example UserInfo(1): PSA,v1.0,iPhone,iPhone OS,6.0
     */
    NSString *userAgentString = [NSString stringWithFormat:@"%@,%@,%d",
                                 @"Username",
                                 @"Salt",
                                 [SettingsClass sharedObject].applicationType];
    return userAgentString;
}

+ (NSString *) userAgent
{
    /*
     General Format: APP_NAME,APP_VERSION,Model,SystemName,SystemVersion,
     Example UserAgent(1): PSA,v1.0,iPhone,iPhone OS,6.0
     */
    NSString *userAgentString = [NSString stringWithFormat:@"%@,v%@,%@,%@,%@",
                                 APP_NAME,
                                 APP_VERSION,
                                 [UIDevice currentDevice].model,
                                 [UIDevice currentDevice].systemName,
                                 [UIDevice currentDevice].systemVersion];
    return userAgentString;
}


+ (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

+ (NSString *) statusStringForAppointmentStatus:(AppointmentStatus)status
{
    NSString *statusString = nil;
    switch (status) {
        case AppointmentStatusPaused:
            statusString = kAppointmentStatusPaused;
            break;
        case AppointmentStatusInProgress:
            statusString = kAppointmentStatusInProgress;
            break;
        case AppointmentStatusComplete:
            statusString = kAppointmentStatusComplete;
            break;
        case AppointmentStatusNotStarted:
            statusString = kAppointmentStatusNotStarted;
            break;
        case AppointmentStatusFinished:
            statusString = kAppointmentStatusFinished;
            break;
        case AppointmentStatusNoEntry:
            statusString = kAppointmentStatusNoEntry;
            break;
			 case AppointmentStatusAccepted:
				    statusString = kAppointmentStatusAccepted;
				    break;
			 default:
            statusString = nil;
            break;
    }
    return statusString;
}

+ (AppointmentStatus) completionStatusForAppointmentType:(AppointmentType)type
{
	AppointmentStatus status;
	switch (type)
	{
        case AppointmentTypeOilServicing:
        case AppointmentTypeAlternativeFuels:
        case AppointmentTypeGasVoid:
		case AppointmentTypeGas:
		{
			status = AppointmentStatusFinished;
			break;
		}
		case AppointmentTypePreVoid:
		case AppointmentTypeFault:
		case AppointmentTypePlanned:
		case AppointmentTypeVoidWorkRequired:
		{
			status = AppointmentStatusComplete;
			break;
		}
		case AppointmentTypeDefect:
		{
			status = AppointmentStatusComplete;
			break;
		}
		default:
		{
			status = -1;
			break;
		}
	}
	return status;
}

+ (NSDate *)convertServerDateToNSDate:(NSString *)serverDate
{
    //CLS_LOG(@"Server Date : %@", serverDate);
    NSDate *date = nil;
    if(!isEmpty(serverDate))
    {
        serverDate = [serverDate trimString];
        NSRange startRange = [serverDate rangeOfString:@"("];
        NSRange endRange = [serverDate rangeOfString:@"+"];
        if(!isRangeEmpty(startRange) && !isRangeEmpty(endRange))
        {
            NSInteger location = startRange.location + 1;
            NSInteger length = endRange.location - location;
            NSRange dateRange = NSMakeRange(location, length);
            if(!isRangeEmpty(dateRange))
            {
                serverDate = [serverDate substringWithRange:dateRange];
                double timeInterval = [serverDate doubleValue]/1000;
                date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
            }
            else
            {
                CLS_LOG(@"Invalid Server Date");
            }
        }
    }
    return date;
}

+ (NSString *)convertNSDateToServerDate:(NSDate *)localDate
{
    if (isEmpty(localDate) || localDate == nil)
    {
        return nil;
    }
    NSString *serverDate;
    NSTimeInterval timeInMiliSeconds = [localDate timeIntervalSince1970] * 1000;
    serverDate = [NSString stringWithFormat:@"/Date(%0.0f+0000)/", timeInMiliSeconds];
    return serverDate;
}

+ (NSString *) stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    NSString *dateString = nil;
    if(date && dateFormat)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        /*Warning: Begin Iffy patch for date integrity - RSUP-4395*/
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
         /*End Iffy patch for date integrity - RSUP-4395*/
        [formatter setDateFormat:dateFormat];
        dateString = [formatter stringFromDate:date];
    }
    return dateString;
}

+ (NSDate *) dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSDate *date = nil;
    if(!isEmpty(dateString) && !isEmpty(dateFormat))
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [formatter setDateFormat:dateFormat];
        date = [formatter dateFromString:dateString];
    }
    return date;
}


+ (BOOL) isEmailValid:(NSString *)email
{
    BOOL isValid = NO;
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    isValid = [emailTest evaluateWithObject:email];
    return isValid;
    
}


+(bool) isValidNumber:(NSString*)number
{
    //NSCharacterSet* set = [NSCharacterSet whitespaceCharacterSet];
    //	NSString * newNumberStr = [number stringByTrimmingCharactersInSet:set];
    NSString * newNumberStr = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([newNumberStr length] < 7 || [newNumberStr length] > 14 )
    {
        return false;
    }
    
    for (int i=0; i<[newNumberStr length]; i++)
    {
        NSRange range;
        range.length = 1;
        range.location = i;
        NSString* str2 = [newNumberStr substringWithRange:range];
        char ch = (char) [str2 characterAtIndex:(NSUInteger)0];
        
        if (ch >= (char)'0' && ch <= (char)'9')
        {
            continue;
        }
        else if (ch == '+' && i == 0)
        {
            continue;
        }
        else
            return false;
    }
    return true;
}

+(NSString *) getValidNumber:(NSString*) str
{
    NSString* newValidNumber = @"";
    if ((str != nil) && ([str length] > 0)) {
        NSCharacterSet* set = [NSCharacterSet whitespaceCharacterSet];
        str = [str stringByTrimmingCharactersInSet:set];
        for (int i=0; i<[str length]; i++)
        {
            NSRange range;
            range.length = 1;
            range.location = i;
            NSString* str2 = [str substringWithRange:range];
            char ch = (char) [str2 characterAtIndex:(NSUInteger)0];
            
            if (ch >= (char)'0' && ch <= (char)'9')
                newValidNumber = [newValidNumber stringByAppendingFormat:@"%@",str2];
            else if (ch == '+' && i == 0)
                newValidNumber = [newValidNumber stringByAppendingFormat:@"%@",@"00"];
        }
    }
    return newValidNumber;
}

+ (NSString *) removeWhiteSpacesFromString:(NSString*)str
{
    str =  [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    return str;
}

+(NSString *)getAPPVersionString
{
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* build = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString* targetName = [infoDict objectForKey:@"CFBundleExecutable"];
//    NSString* displayName = [infoDict objectForKey:@"CFBundleDisplayName"];
    targetName = [targetName stringByReplacingOccurrencesOfString:@"PropertySurvey" withString:@""];
    
    //NSMutableString *appVersion = [NSMutableString stringWithFormat:@"%@ Version: %@ (%@)",targetName,version, build];
    NSMutableString *appVersion = [NSMutableString stringWithString:@" "];
    if(![targetName isEqualToString:@"Live"])
    {
        appVersion = [NSMutableString stringWithFormat:@"v%@(%@)",version, build];
    }
    if([targetName isEqualToString:@"Live"])
    {
        appVersion = [NSMutableString stringWithFormat:@"v%@",version];
    }
    return appVersion;
}

+(NSString*) getAppVersionStringForCompletedAppt{
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* build = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *appVersion = [NSString stringWithFormat:@"v%@(%@)",version, build];
    return appVersion;
}

+(NSString *)getStringWithNewLineCharacters:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    string = [string stringByReplacingOccurrencesOfString:@"\n" withString:[NSString stringWithFormat:@"%c",'\x0a']];
    return string;
}

#pragma mark - Get UUID
//Return 47 to 64 Character Unique Identifier . (Globally unique)
+(NSString*) getUniqueIdentifierStringWithUserName:(NSString*)username
{
    NSString * resultString =nil;
    CFUUIDRef u = CFUUIDCreate(NULL);
    NSMutableString * muStr = [[NSMutableString alloc] init];
    //34 Character Key
    [muStr appendString:(__bridge_transfer NSString*)CFUUIDCreateString(NULL, u)]; // returns a
    CFRelease(u);
    // characters Will be get from time interval
    int timeInterval = ceil([[NSDate date] timeIntervalSinceReferenceDate]);
    
    // Appending time interval and Username to GUID String
    [muStr appendFormat:@"-%d-%@",timeInterval,username];
    if([muStr length]>64)
    {
        resultString = [muStr substringToIndex:64];
    }
    else
    {
        resultString = [NSString stringWithString:muStr];
    }
    return resultString;
}

+(void) showPopUpForErrorMessage:(NSString *)errorMessage forViewController:(UIViewController *) controller{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if(![PSPropertyPictureManager sharedManager].isPostingImage)
        {
            //[PSSynchronizationManager sharedManager].delegate = controller;
            [[PSSynchronizationManager sharedManager]  syncCompletedAppointments];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                           message:LOC(@"KEY_ALERT_DATA_POSTING_IMAGE_INPROGRESS")
                                                          delegate:nil
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            
            [alert show];
        }
    }];

    [alertController addAction:okAction];
    [alertController addAction:retryAction];
    [controller presentViewController:alertController animated: YES completion: nil];
}

+(NSString*) createErrorMessageStringForFailedAppointments:(NSArray *) failedAppointments{
    NSString * alertmsg = @"";
    for(NSDictionary *dict in failedAppointments){
        NSString *propertyAddress = [dict objectForKey:@"propertyAddress"];
        NSString *reasonStr = [dict objectForKey:@"failureReason"];
        
        NSString * errorAlert = [NSString stringWithFormat:@"\r%@:\r%@\r", propertyAddress, reasonStr];
        alertmsg = [NSString stringWithFormat:@"%@%@",alertmsg,errorAlert];
    }
    return alertmsg;
}

+(NSNumber*) statusNumberForAppointmentSyncStatus:(AppointmentSyncStatus) status{
    NSNumber *statusNumber;
    switch (status) {
        case AppointmentNotSynced:
            statusNumber = [NSNumber numberWithInteger:0];
            break;
        case AppointmentDefectSynced:
            statusNumber = [NSNumber numberWithInteger:1];
            break;
        case AppointmentImageSynced:
            statusNumber = [NSNumber numberWithInteger:2];
            break;
        case AppointmentSynced:
            statusNumber = [NSNumber numberWithInteger:3];
            break;
        default:
            statusNumber = nil;
            break;
    }
    return statusNumber;
}

+(void) showMessageWithHeader:(NSString *)header andBody:(NSString *)bodyMessage forViewController:(UIViewController *) controller{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:header message:bodyMessage preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [controller presentViewController:alertController animated: YES completion: nil];
}


+ (UIViewController *) getVisibleViewController{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    topController = [UtilityClass getVisibleViewControllerFrom:topController];
    
    return topController;
}

+ (UIViewController *) getVisibleViewControllerFrom:(UIViewController *) vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [UtilityClass getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [UtilityClass getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if (vc.presentedViewController) {
            return [UtilityClass getVisibleViewControllerFrom:vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

+ (BOOL) isEmpty:(NSString *)str
{
	if(str.length == 0
		 || [str isKindOfClass:[NSNull class]]
		 || [str isEqualToString:@""]
		 || [str isEqualToString:@"(null)"]
		 || str==nil
		 || [str isEqualToString:@"<null>"]){
		return YES;
	}
	return NO;
}

@end
