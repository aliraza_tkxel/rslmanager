//
//  PSPromoVideoPlayer.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 29/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSPromoVideoPlayer.h"

@implementation PSPromoVideoPlayer

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) playPromoVideoWithName:(NSString *) resourceName{
    NSString *filepath =[[NSBundle mainBundle]
                         pathForResource:resourceName ofType:nil];
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.avPlayer = [AVPlayer playerWithURL:fileURL];
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    videoLayer.frame = self.bounds;
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.layer addSublayer:videoLayer];
    
    /*Register for looping back when video ends*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[self.avPlayer currentItem]];
    
    [self.avPlayer play];

}

/*This Method loops the video*/

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero];
}


@end
