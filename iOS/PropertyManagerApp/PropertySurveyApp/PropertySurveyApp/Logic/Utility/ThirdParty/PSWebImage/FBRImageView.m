//
//  FBRFBRImageView.m
//  Faber
//
//  Created by Imran on 3/29/13.
//  Copyright (c) 2013 Hira Software Solutions. All rights reserved.
//

#import "FBRImageView.h"
#import "UIImageView+FABCategory.h"

@implementation FBRImageView

@synthesize thumbnail;
@synthesize thumbnailKey;

- (void)dealloc {
    [self setThumbnailKey:nil];
    [super dealloc];
}
- (BOOL)createThumbnails {
    return  self.thumbnail;
}

@end
