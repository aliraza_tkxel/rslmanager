//
//  MWPhotoBrowser.m
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SwipePhotoBrowser.h"
#import "PropertyPicture+JSON.h"


#define ArrowImageSize 15

@interface SwipePhotoBrowser ()
{
    int currentIndex;
    
    UIImageView * _imgView;
    UIImageView * _rightArrowImage;
    UIImageView * _leftArrowImage;
}

@end

@implementation SwipePhotoBrowser

@synthesize allPhotos=_allPhotos;
@synthesize progressHUD=_progressHUD;


-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        CGFloat width=320;
        
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, width-20, 225)];
       
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imgView];
        
        _leftArrowImage=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX([_imgView frame])+5, (CGRectGetHeight([_imgView frame]) - ArrowImageSize)/2, ArrowImageSize, ArrowImageSize)];
        [_leftArrowImage setImage:[UIImage imageNamed:@"whiteLeftArrow.png"]];
        [_leftArrowImage setBackgroundColor:[UIColor clearColor]];
        [_leftArrowImage setHidden:YES];
        [self addSubview:_leftArrowImage];

        _rightArrowImage=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX([_imgView frame])-ArrowImageSize-5, (CGRectGetHeight([_imgView frame]) - ArrowImageSize)/2, ArrowImageSize, ArrowImageSize)];
        [_rightArrowImage setImage:[UIImage imageNamed:@"whiteRightArrow.png"]];
        [_rightArrowImage setBackgroundColor:[UIColor clearColor]];
        [_rightArrowImage setHidden:YES];
        [self addSubview:_rightArrowImage];

        
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]         initWithTarget:self action:@selector(screenWasSwipedLeft)];
        swipeLeft.numberOfTouchesRequired = 1;
        swipeLeft.direction=UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeLeft];
        
        
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]         initWithTarget:self action:@selector(screenWasSwipedRight)];
        swipeRight.numberOfTouchesRequired = 1;
        swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:swipeRight];
        
        currentIndex=0;

        
        
        // Listen for MWPhoto notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleMWPhotoLoadingDidEndNotification:)
                                                     name:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                   object:nil];
        
        [self setSelectionStyle:(UITableViewCellSelectionStyleNone)];
        
        [_imgView setBackgroundColor:[UIColor grayColor]];
        [self setBackgroundColor:[UIColor grayColor]];
    }
    
    return self;
    
}

-(void) refreshImage
{
    if ([_allPhotos count]) {
        
        PropertyPicture * photo=[_allPhotos objectAtIndex:currentIndex];
            
            [_imgView setImage:nil];
            
            _imgView.alpha=0.5;
            
            [UIView animateWithDuration:0.5 animations:^{
                _imgView.alpha = 1.0;
                _imgView.transform = CGAffineTransformIdentity;
            }];
        

        [self updateArrows];
    }
}

-(void) screenWasSwipedLeft
{
    if (currentIndex>0) {
        
        currentIndex=currentIndex-1;
        
        PropertyPicture * photo=[_allPhotos objectAtIndex:currentIndex];
        
        UIImage * img=nil;
        
        img=[img scaleToSize:CGSizeMake(_imgView.frame.size.width, _imgView.frame.size.height)];
        
            
            [_imgView setImage:img];

            _imgView.alpha=0.5;
            
            [UIView animateWithDuration:0.5 animations:^{
                _imgView.alpha = 1.0;
                _imgView.transform = CGAffineTransformIdentity;
            }];
        [self updateArrows];
    }
    CLS_LOG(@"screenWasSwipedLeft");
}

-(void) screenWasSwipedRight
{
    if ([_allPhotos count]) {
        
        if (currentIndex < [_allPhotos count]-1) {
            
            currentIndex=currentIndex+1;
            
            
            PropertyPicture * photo=[_allPhotos objectAtIndex:currentIndex];
            
            UIImage * img=nil;
            
            img=[img scaleToSize:CGSizeMake(_imgView.frame.size.width, _imgView.frame.size.height)];
                
                [_imgView setImage:img];
                
                _imgView.alpha=0.5;
                
                [UIView animateWithDuration:0.5 animations:^{
                    _imgView.alpha = 1.0;
                    _imgView.transform = CGAffineTransformIdentity;
                }];
            
        }
        
        [self updateArrows];
    }
    
    CLS_LOG(@"screenWasSwipedRight");
}


#pragma mark - MWPhoto Loading Notification

- (void)handleMWPhotoLoadingDidEndNotification:(NSNotification *)notification {
    id <MWPhoto> photo = [notification object];
    
    [self hideProgressHUD:YES];
    
    UIImage * img=nil;
    
    img=[img scaleToSize:CGSizeMake(_imgView.frame.size.width, _imgView.frame.size.height)];
    
    if (img)
    {
        img=[img scaleToSize:CGSizeMake(_imgView.frame.size.width, _imgView.frame.size.height)];
        
        [_imgView setImage:img];
        
        _imgView.alpha=0.5;
        
        [UIView animateWithDuration:0.5 animations:^{
            _imgView.alpha = 1.0;
            _imgView.transform = CGAffineTransformIdentity;
        }];
    }
    
}

-(void) dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - MBProgressHUD

- (MBProgressHUD *)progressHUD {
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithView:self];
        _progressHUD.minSize = CGSizeMake(120, 120);
        _progressHUD.minShowTime = 1;

        [self addSubview:_progressHUD];
    }
    return _progressHUD;
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD.labelText = message;
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.progressHUD show:YES];
}

- (void)hideProgressHUD:(BOOL)animated {
    [self.progressHUD hide:animated];

}

- (void)showProgressHUDCompleteMessage:(NSString *)message {
    if (message) {
        if (self.progressHUD.isHidden) [self.progressHUD show:YES];
        self.progressHUD.labelText = message;
        self.progressHUD.mode = MBProgressHUDModeCustomView;
        [self.progressHUD hide:YES afterDelay:1.5];
    } else {
        [self.progressHUD hide:YES];
    }

}

+(CGFloat) getHeight
{
    return 235;
}


-(void)updateArrows {
    
    CLS_LOG(@"updateArrows-------------------------->>>>%d",[_allPhotos count]);
    if([_allPhotos count] <= 1) {
        [_rightArrowImage setHidden:YES];
        [_leftArrowImage setHidden:YES];
        return;
    }
    
    
    if(currentIndex == 0) {
        [_leftArrowImage setHidden:YES];
        [_rightArrowImage setHidden:NO];
    }
    else if(currentIndex == [_allPhotos count] -1) {
        [_rightArrowImage setHidden:YES];
        [_leftArrowImage setHidden:NO];
    }
    else {
        [_rightArrowImage setHidden:NO];
        [_leftArrowImage setHidden:NO];
    }
}

@end
