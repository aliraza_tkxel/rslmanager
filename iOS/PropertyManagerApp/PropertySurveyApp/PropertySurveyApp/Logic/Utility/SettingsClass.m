//
//  SettingsClass.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "SettingsClass.h"
#import "PSCoreDataManager.h"

@interface SettingsClass ()
-(void) _loadDefaultSettings;
-(void) _registerForNotifications;
@end


@implementation SettingsClass
@synthesize applicationType = _applicationType;
@synthesize autoLogin = _autoLogin;
@synthesize loggedInUser = _loggedInUser;
@synthesize currentUserId = _currentUserId;
@synthesize currentUserPassword = _currentUserPassword;
@synthesize deviceToken = _deviceToken;
@synthesize filterOptions = _filterOptions;

#pragma mark -
#pragma mark ARC Singleton Implementation

static SettingsClass *sharedSettingsObject = nil;
+ (SettingsClass *) sharedObject
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSettingsObject = [[SettingsClass alloc] init];
        // Do any other initialisation stuff here
        [sharedSettingsObject _registerForNotifications];
    });
    return sharedSettingsObject;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSettingsObject = [super allocWithZone:zone];
    });
    return sharedSettingsObject;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Private Methods
- (void) _registerForNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onReceiveUserInfoSaveNotification)
                                                 name:kUserInfoSaveCompleted
                                               object:nil];
}

- (void) _loadDefaultSettings
{
    CLS_LOG(@"loadDefaultSettings");
    
    
    /*
     * Load Application Type depending upon the Target Information.
     */
#ifdef PropertySurveyStockApp
    self.applicationType = ApplicationTypeStock;
#endif
    self.loggedInUser = nil;
    self.currentUserId = nil;
    self.currentUserPassword = nil;
    self.deviceToken = nil;
    self.autoLogin = NO;
    self.lastLoggedInDate = nil;
	  self.lastDisplayedDateForSafetyAlert = nil;
    // self.filterOptions = nil;
    if (isEmpty(self.filterOptions))
    {
        self.filterOptions = [NSMutableArray arrayWithObjects:
                              [NSNumber numberWithInt:PSFilterOptionViewAll],
                              [NSNumber numberWithInt: PSFilterOptionSortByDate],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyGas],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyVoid],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyFault],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyPlanned],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyAdaptations],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyMiscellaneous],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyInProgress],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyArranged],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyConditionRatingTool],
                              [NSNumber numberWithInt: PSFilterOptionShowOnlyDefect],
                              [NSNumber numberWithInt:PSFilterOptionShowOnlyOil],
                              [NSNumber numberWithInt:PSFilterOptionShowOnlyAlternative],
                              nil];
    }
}

#pragma mark - Public Methods

#pragma mark - Settings Cache
- (void) saveSettings
{
    CLS_LOG(@"saveSettings ");
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:self.autoLogin forKey:kAutoLogin];
    [defaults setObject:self.lastLoggedInDate forKey:kLastLoggedInDate];
    [defaults setObject:self.currentUserId forKey:kCurrentUserId];
    [defaults setObject:self.currentUserPassword forKey:kCurrentUserPassword];    
    [defaults setObject:self.deviceToken forKey:kAPNSDeviceToken];
    [defaults setObject:self.filterOptions forKey:kFilterOptions];
    [defaults setBool:YES forKey:kSettingsSaved];
    [defaults synchronize];
}

- (void) loadSettings
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey:kSettingsSaved])
    {
        [self _loadDefaultSettings];
        return;
    }
    else
    {
        self.autoLogin = [defaults boolForKey:kAutoLogin];
        self.lastLoggedInDate = [defaults objectForKey:kLastLoggedInDate];
        self.currentUserId = [defaults objectForKey:kCurrentUserId];
        self.currentUserPassword = [defaults objectForKey:kCurrentUserPassword];
        self.deviceToken = [defaults objectForKey:kAPNSDeviceToken];
        self.filterOptions = [defaults objectForKey:kFilterOptions];
        [self updateUserInfo];
    }
}

-(void) clearSettings
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [self _loadDefaultSettings];
    [self saveSettings];
    [defaults setBool:false forKey:kSettingsSaved];
    [defaults synchronize];
}

#pragma mark - Sign Out
- (void) signOut {
    [[SettingsClass sharedObject] clearSettings];
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUserSignOutNotification object:nil];
}

#pragma mark - Session Time Limit Passed
- (BOOL) isSessionTimeLimitPassed
{
    NSDate *lastLoggedInDate = self.lastLoggedInDate;
    NSDate *currentDate = [NSDate date];
    NSDate *sessionLimitDate = [lastLoggedInDate dateByAddingHours:48];
    BOOL isSessionValid;
    
    if (([currentDate compare:sessionLimitDate] == NSOrderedDescending))
    {
        isSessionValid = YES;
    }
    else
    {
        isSessionValid = NO;
    }
    
    return isSessionValid;
}

#pragma mark - Safety Alert

- (NSDate *) getCurrentUserLastAlertDisplayDate
{
	// Get Dictionary of users safety alert dates
	// Filter & return date
	
	NSMutableDictionary * usersSafetyAlertDatesDict = [[[NSUserDefaults standardUserDefaults] objectForKey:kUsersSafetyAlertDates] mutableCopy];
	NSDate *userLastAlertDisplayDate = nil;
	if (usersSafetyAlertDatesDict != nil) {
		userLastAlertDisplayDate = [usersSafetyAlertDatesDict valueForKey:[self loggedInUser].userName];
	}
	return userLastAlertDisplayDate;
}

- (void) setCurrentUserLastAlertDisplayDate:(NSDate *)alertDate
{
	NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
	
	// Get Dictionary from NSUserDefaults for SafetyAlertKey
	NSMutableDictionary * usersSafetyAlertDatesDict = [[defaults objectForKey:kUsersSafetyAlertDates] mutableCopy];
	
	// If dictionary is nil then create new dictionary else use existing
	if (usersSafetyAlertDatesDict == nil) {
		usersSafetyAlertDatesDict = [NSMutableDictionary dictionary];
	}

	@try {
		if(!isEmpty(alertDate)){
			// Add/Update alert date in dictionary against currently logged user
			[usersSafetyAlertDatesDict setObject:alertDate  forKey:[self loggedInUser].userName];
			
			// Set Dictionary in NSUserDefaults
			[defaults setObject:usersSafetyAlertDatesDict forKey:kUsersSafetyAlertDates];
			
			// Synchronize
			[defaults synchronize];
		}
	} @catch (NSException *exception) {
		
	} @finally {
		
	}
	
}


#pragma mark - Notifications

- (void) onReceiveUserInfoSaveNotification
{
    [[SettingsClass sharedObject] updateUserInfo];
    self.lastLoggedInDate = [NSDate date];
    [[SettingsClass sharedObject] saveSettings];
}


#pragma mark - UserInfo Methods
- (void) updateUserInfo
{
    if(!isEmpty(self.currentUserId))
    {
        self.loggedInUser = [[PSCoreDataManager sharedManager] userWithIdentifier:self.currentUserId];        
        if(self.loggedInUser == nil)
        {
            self.lastLoggedInDate = nil;
            self.loggedInUser = nil;
            self.currentUserPassword = nil;
            self.currentUserId = nil;
            self.autoLogin = NO;
        }
    }
    else
    {
        self.lastLoggedInDate = nil;
        self.loggedInUser = nil;
        self.currentUserPassword = nil;
        self.currentUserId = nil;
        self.autoLogin = NO;
    }
}

- (NSString *) userInfo
{
    NSString *userInfoString = nil;
    if(self.loggedInUser)
    {
        userInfoString = [NSString stringWithFormat:@"%@,%@,%d",
                                    self.loggedInUser.userName,
                                    self.loggedInUser.salt,
                                    self.applicationType];
    }
    return userInfoString;
}

- (NSString *) userFullName
{
    NSString *fullName = @"";
    if(self.loggedInUser && self.loggedInUser.fullName)
    {
        fullName = self.loggedInUser.fullName;
    }
    return fullName;
}

- (NSString *) applicationTypeName
{
    NSString *typeName = nil;
    switch (self.applicationType) {
        case ApplicationTypeGas:
            typeName = kApplicationTypeGas;
            break;
        case ApplicationTypeFault:
            typeName = kApplicationTypeFault;
            break;
        case ApplicationTypeMaintenance:
            typeName = kApplicationTypeMaintenance;
            break;            
        default:
            typeName = nil;
            break;
    }
    return typeName;
}

#pragma mark - PSFilterOption Method
- (BOOL) isFilterOptionSelected:(PSFilterOption)filterOption
{
    BOOL isSelected = NO;
    if(!isEmpty(self.filterOptions))
    {
        if([self.filterOptions containsObject:[NSNumber numberWithInteger:filterOption]])
        {
            isSelected = YES;
        }
    }
    return isSelected;
}

@end
