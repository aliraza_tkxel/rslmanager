//
//  SettingsClass.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSettingsSaved          @"settingsSaved" // Check for default settings
#define kAutoLogin              @"autoLogin"
#define kLastLoggedInDate       @"lastLoggedInDate"
#define kUsersSafetyAlertDates  @"UsersSafetyAlertDates"
#define kCurrentUserId          @"currentUserId"
#define kCurrentUserPassword    @"currentUserPassword"

@interface SettingsClass : NSObject

@property (nonatomic, assign) ApplicationType applicationType;
@property (nonatomic, strong) User *loggedInUser;
@property (nonatomic, strong) NSNumber *currentUserId;
@property (nonatomic, strong) NSString *currentUserPassword;
@property (nonatomic, strong) NSDate   *lastLoggedInDate;
@property (nonatomic, strong) NSDate   *lastDisplayedDateForSafetyAlert;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSArray *filterOptions;
@property (nonatomic, assign) BOOL autoLogin;
@property (atomic, assign) BOOL isSyncingProgress;

+ (SettingsClass *) sharedObject;


- (void) loadSettings;
- (void) saveSettings;
- (void) clearSettings;

- (void) signOut;
- (NSDate *) getCurrentUserLastAlertDisplayDate;
- (void) setCurrentUserLastAlertDisplayDate:(NSDate *)alertDate;

- (BOOL) isSessionTimeLimitPassed;

- (void) updateUserInfo;
- (NSString *) userInfo;
- (NSString *) userFullName;
- (NSString *) applicationTypeName;

- (BOOL) isFilterOptionSelected:(PSFilterOption)filterOption;
@end
