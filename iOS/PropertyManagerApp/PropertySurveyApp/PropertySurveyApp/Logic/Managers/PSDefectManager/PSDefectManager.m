//
//  PSDefectManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDefectManager.h"
#import "CoreDataHelper.h"

@implementation PSDefectManager


#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDefectManager *sharedDefectManager = nil;
+ (PSDefectManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDefectManager = [[PSDefectManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedDefectManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDefectManager = [super allocWithZone:zone];
    });
    return sharedDefectManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark - Methods

- (NSArray *) fetchAllDefects;
{
    NSArray *defects = nil;
    STARTEXCEPTION
    @autoreleasepool {
    if([UtilityClass isUserLoggedIn])
    {
        NSArray* fetchedDefects = [CoreDataHelper getObjectsFromEntity:kDefect
																															predicate:[NSPredicate predicateWithValue:YES]
																																sortKey:nil
																													sortAscending:YES
																												 context:[PSDatabaseContext sharedContext].managedObjectContext];
        if([fetchedDefects count] > 0)
        {
            defects = [NSArray arrayWithArray:fetchedDefects];
        }
    }
    }
    ENDEXCEPTION
    return defects;
}

@end
