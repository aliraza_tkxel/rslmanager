//
//  PSPreInspectionManager.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSVBaseManager.h"
#import "PropertyComponents.h"
#import "PaintPack.h"
#import "RoomData.h"

@interface PSVPreInspectionManager : PSVBaseManager

-(NSArray *)getRoomsListFromDB;
-(NSArray *)getConditionList;

-(void)addMajorWorkRequired:(PropertyComponents *)component
									Condition:(NSString *)condition
						 ReplacementDue:(NSString *)replacementDue
											Notes:(NSString *)notes;

-(PaintPack *) addPaintPack:(RoomData *)roomData;

@end
