//
//  PSPreInspectionManager.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPreInspectionManager.h"
#import "CoreDataHelper.h"
#import "PropertyComponents.h"
#import "VoidData.h"
#import "MajorWorkRequired.h"
#import "WorkRequired+CoreDataClass.h"

@interface PSVPreInspectionManager ()
{
}
@end

@implementation PSVPreInspectionManager

-(NSArray *)getRoomsListFromDB
{
	NSArray * roomList;
	NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
	roomList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([RoomData class])
																					 sortKey:@"roomName"
																		 sortAscending:TRUE
																					 context:dbContext];
	return roomList;
}

-(NSArray *)getConditionList
{
	NSMutableArray * conditionList = [[NSMutableArray alloc]init];
	[conditionList addObject:@"Potentially Unsatisfactory"];
	[conditionList addObject:@"Unsatisfactory"];
	return conditionList;
}

-(void)addMajorWorkRequired:(PropertyComponents *)component
									Condition:(NSString *)condition
						 ReplacementDue:(NSString *)replacementDue
											Notes:(NSString *)notes
{
	NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
	VoidData * voidData = self.appointment.appointmentToVoidData;
	MajorWorkRequired * majorRecord = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MajorWorkRequired class])
																																	inManagedObjectContext:dbContext];
	majorRecord.componentId = component.componentId;
	majorRecord.component = component.component;
	majorRecord.condition = condition;
	majorRecord.notes = notes;
	majorRecord.replacementDue = [UtilityClass dateFromString:replacementDue dateFormat:kDateTimeStyle8];
	majorRecord.majorWorkRequiredToVoidData = voidData;
	[voidData addVoidDataToMajorWorkRequiredObject:majorRecord];
	[self saveContext];
}


-(PaintPack *) addPaintPack:(RoomData *)roomData
{
	NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
	VoidData * voidData = self.appointment.appointmentToVoidData;
	PaintPack * paintPack = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([PaintPack class])
																												inManagedObjectContext:dbContext];
	paintPack.roomId = roomData.roomId;
	paintPack.roomName = roomData.roomName;
	paintPack.paintPackToVoidData = voidData;
	
	[voidData addVoidDataToPaintPackObject:paintPack];
	[self saveContext];
	
	return paintPack;
}

@end
