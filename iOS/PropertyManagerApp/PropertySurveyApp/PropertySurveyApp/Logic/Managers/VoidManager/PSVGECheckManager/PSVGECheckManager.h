//
//  PSVGECheckManager.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-07.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSVBaseManager.h"

@interface PSVGECheckManager : PSVBaseManager

-(NSArray *)getMeterTypeListFromDB:(NSString *)deviceType;
-(NSArray *)getTenantTypeList;

@end
