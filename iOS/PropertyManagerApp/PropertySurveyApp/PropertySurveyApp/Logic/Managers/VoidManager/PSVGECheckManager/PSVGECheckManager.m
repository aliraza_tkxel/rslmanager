//
//  PSVGECheckManager.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-07.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVGECheckManager.h"
#import "CoreDataHelper.h"
#import "MeterType.h"

@implementation PSVGECheckManager

-(NSArray *)getMeterTypeListFromDB:(NSString *)deviceType
{
	NSArray * meterTypeList;
	NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF.deviceType == %@", deviceType];
	meterTypeList = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([MeterType class])
																							predicate:predicate
																								sortKey:@"meterId"
																					sortAscending:TRUE
																				 context:dbContext];
	return meterTypeList;
}

-(NSArray *)getTenantTypeList
{
	NSMutableArray * conditionList = [[NSMutableArray alloc]init];
	[conditionList addObject:@"Vacating"];
	[conditionList addObject:@"New"];
	return conditionList;
}

@end
