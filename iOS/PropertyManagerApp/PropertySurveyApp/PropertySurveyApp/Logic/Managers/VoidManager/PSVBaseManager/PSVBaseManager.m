//
//  PSVBaseManager.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-07.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVBaseManager.h"

@interface PSVBaseManager ()
{
}
@end

@implementation PSVBaseManager

-(id)init:(Appointment *)appointment
{
	_appointment = appointment;
	_psDBContext = [PSDatabaseContext sharedContext];
	return self;
}

-(void)saveContext
{
	_appointment.loggedDate = [NSDate date];
	_appointment.isModified = [NSNumber numberWithBool:YES];
	[_psDBContext saveContext];
}


-(void)saveAppointmentAndMarkCompleted:(BOOL) shouldMarkComplete{
    if(shouldMarkComplete){
        _appointment.appointmentStatus = kAppointmentStatusComplete;
        _appointment.loggedDate = [NSDate date];
        _appointment.appointmentActualEndTime = [NSDate date];
        _appointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
        if(_appointment.appointmentActualStartTime == nil)
        {
            _appointment.appointmentActualStartTime = [NSDate date];
        }
        if(_appointment.appointmentActualEndTime == nil)
        {
            _appointment.appointmentActualEndTime = [NSDate date];
        }
    }
    [self saveContext];
}


-(void)markCompleteWithNoEntry:(NSString *)noEntryNote
{
	_appointment.noEntryNotes = noEntryNote;
	_appointment.appointmentStatus = kAppointmentStatusNoEntry;
    _appointment.loggedDate = [NSDate date];
    if(_appointment.appointmentActualStartTime == nil)
    {
        _appointment.appointmentActualStartTime = [NSDate date];
    }
    if(_appointment.appointmentActualEndTime == nil)
    {
        _appointment.appointmentActualEndTime = [NSDate date];
    }
    _appointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
	[self saveContext];
}

@end
