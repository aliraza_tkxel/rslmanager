//
//  PSVBaseManager.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-07.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSVBaseManager : NSObject

@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) PSDatabaseContext * psDBContext;

-(id)init:(Appointment *)appointment;
-(void)saveContext;
-(void)markCompleteWithNoEntry:(NSString *)noEntryNote;
-(void)saveAppointmentAndMarkCompleted:(BOOL) shouldMarkComplete;
@end
