//
//  PSSurveyorsManager.h
//  PropertySurveyApp
//
//  Created by Yawar on 14/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSGetAllSurveyorsService.h"

@interface PSSurveyorsManager : NSObject <CoreRESTServiceDelegate, PSGetAllSurveyorsServiceDelegate>

@property (nonatomic, strong) PSGetAllSurveyorsService *getSurveyorsService;

+ (PSSurveyorsManager *) sharedManager;
- (void) fetchAllSurveyors;
- (void) refreshAllSurveyors;
- (Surveyor *) fetchLoggedInSurveyor;

@end
