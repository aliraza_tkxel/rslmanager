//
//  PSSurveyorsManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 14/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyorsManager.h"
#import "PSCoreDataManager.h"

@implementation PSSurveyorsManager
@synthesize getSurveyorsService;

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSSurveyorsManager *sharedSurveyorManager = nil;
+(PSSurveyorsManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSurveyorManager = [[PSSurveyorsManager alloc] init];
        // Do any other initialisation stuff here
        sharedSurveyorManager.getSurveyorsService = [[PSGetAllSurveyorsService alloc] init];
        sharedSurveyorManager.getSurveyorsService.surveyorDelegate = sharedSurveyorManager;
    });
    return sharedSurveyorManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSurveyorManager = [super allocWithZone:zone];
    });
    return sharedSurveyorManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark - Methods
- (void) fetchAllSurveyors
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
            [requestParameters setObject:[NSNumber numberWithInteger:[SettingsClass sharedObject].applicationType] forKey:@"applicationtype"];
            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
            [self.getSurveyorsService getAllSurveyors:requestParameters];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
    }
}

- (Surveyor *) fetchLoggedInSurveyor
{
    Surveyor *surveyor = [[PSCoreDataManager sharedManager] fetchLoggedInSurveyor];
    return surveyor;
}

- (void) refreshAllSurveyors
{
    if([UtilityClass isUserLoggedIn] && [SettingsClass sharedObject].loggedInUser != nil)
    {
        //remove all appointments?
        [[SettingsClass sharedObject].loggedInUser removeAllAppointments];
        [[PSDatabaseContext sharedContext] saveContext];
        [self fetchAllSurveyors];
    }
}

#pragma mark - PSGetAllSurveyorsServiceDelegate

- (void) service:(PSGetAllSurveyorsService *)service didFinishLoadingSurveyors:(id)jsonObject
{

    NSDictionary *responseDictionary = [jsonObject JSONValue];
    NSArray *surveyorsArray = [responseDictionary objectForKey:kResponseTag];
    if(!isEmpty(surveyorsArray))
    {
        [[PSDataPersistenceManager sharedManager] saveSurveyors:surveyorsArray];
    }
}

- (void) service:(PSFetchSurveyFormService *)service didFailWithError:(NSError *)error
{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
}


@end
