//
//  PSAppointmentsManager.m
//  PropertySurveyApp
//
//  Created by TkXel on 08/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentsManager.h"
#import "PSDataPersistenceManager+(CommonData).h"


@implementation PSAppointmentsManager

@synthesize getAppointmentService;

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSAppointmentsManager *sharedAppointmentManager = nil;
+(PSAppointmentsManager*) sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedAppointmentManager = [[PSAppointmentsManager alloc] init];
		// Do any other initialisation stuff here
		sharedAppointmentManager.getAppointmentService = [[PSGetAllAppointmentsService alloc] init];
		sharedAppointmentManager.getAppointmentService.delegate = sharedAppointmentManager;
		
		sharedAppointmentManager.deleteAppointmentService = [[PSDeleteAppointmentService alloc] init];
		sharedAppointmentManager.deleteAppointmentService.delegate = sharedAppointmentManager.deleteAppointmentService;
		sharedAppointmentManager.deleteAppointmentService.deleteAppointmentDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.fetchSurveyFormService = [[PSFetchSurveyFormService alloc] init];
		sharedAppointmentManager.fetchSurveyFormService.surveyDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.createAppointmentService = [[PSCreateAppointmentService alloc] init];
		//sharedAppointmentManager.createAppointmentService.createAppointmentDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.completeAppointmentService = [[PSCompleteAppointmentService alloc] init];
		sharedAppointmentManager.completeAppointmentService.completeAppointmentDelegate = sharedAppointmentManager;
		
        sharedAppointmentManager.syncAppointmentStatusService = [[PSSyncAppointmentStatusService alloc] init];
        sharedAppointmentManager.syncAppointmentStatusService.syncStatusDelegate = sharedAppointmentManager;
	});
	return sharedAppointmentManager;
}

+(id)allocWithZone:(NSZone *)zone {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedAppointmentManager = [super allocWithZone:zone];
	});
	return sharedAppointmentManager;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}


#pragma mark - Methods

- (void) createAppointment:(NSDictionary *)appointment
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if([UtilityClass isUserLoggedIn])
		{
			NSMutableDictionary *appointmentDictionary = [appointment mutableCopy];
			[appointmentDictionary setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
			[appointmentDictionary setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
			
			NSData *responseData = [self.createAppointmentService createNewAppointment:appointmentDictionary];
			NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			if(!isEmpty(responseString))
			{
				NSDictionary *JSONDictionary = [responseString JSONValue];
				NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
				NSInteger errorCode = [[JSONDictionary objectForKey:kErrorCodeTag] integerValue];
				if(statusCode == kSuccessCode)
				{
					NSArray *responseArray = [JSONDictionary objectForKey:kResponseTag];
					if (!isEmpty(responseArray))
					{
						
						NSMutableDictionary *responseDictionary = [responseArray objectAtIndex:0];
                        [responseDictionary setValue:@0 forKey:@"syncStatus"];
						if(!isEmpty(responseDictionary))
						{
							/* [responseDictionary setObject:[appointmentDictionary valueForKey:kAppointmentEventIdentifier] forKey:kAppointmentEventIdentifier];*/
							[[PSDataPersistenceManager sharedManager] createNewAppointment:responseDictionary];
						}
					}
					else
					{
						[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																								object:nil];
					}
				}
				else if (errorCode == kAppointmentTimeUnavailableCode)
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentTimeNotAvailableNotification
																																							object:nil];
				}
				else
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																							object:nil];
				}
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																						object:nil];
			}
		}
	});
}

- (void) deleteAppointment:(Appointment *)appointment
{
	if([UtilityClass isUserLoggedIn])
	{
		NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
		[requestParameters setObject:appointment.appointmentId forKey:@"appointmentid"];
		[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
		[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
		[self.deleteAppointmentService deleteAppointment:requestParameters];
	}
}

- (void) fetchAllAppointsments
{
	if([UtilityClass isUserLoggedIn] /*&& [[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0*/)
	{
		@synchronized(self)
		{
			NSDate *startDate = [NSDate date];
			startDate = [startDate dateBySubtractingDays:kAppointmentDaysLimit];
			NSDate *endDate = [NSDate date];
			endDate = [endDate dateByAddingDays:kAppointmentDaysLimit];
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle16] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle16] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:0] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"fetchall"];
            [requestParameters setObject:[self getExistingAppointmentsInfoForFetchRequest] forKey:@"existingAppointments"];
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchAllAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	if([UtilityClass isUserLoggedIn] && [[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0)
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle5] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle5] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:0] forKey:@"fetchall"];
            [requestParameters setObject:[self getExistingAppointmentsInfoForFetchRequest] forKey:@"existingAppointments"];
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchAllInprogressAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	if([UtilityClass isUserLoggedIn] )
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle5] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle5] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:0] forKey:@"fetchall"];
            [requestParameters setObject:[self getExistingAppointmentsInfoForFetchRequest] forKey:@"existingAppointments"];
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) refreshAllAppointsments
{
	if([UtilityClass isUserLoggedIn])
	{
		//[[SettingsClass sharedObject].loggedInUser removeAllAppointments];
		//[[PSDatabaseContext sharedContext] saveContext];
		[self fetchAllAppointsments];
	}
}

- (NSArray *) modifiedAppointments
{
	if ([UtilityClass isUserLoggedIn]) {
		return [[SettingsClass sharedObject].loggedInUser modifiedAppointments];
	}
	return nil;
}

- (NSArray *) completedAppointments
{
    if ([UtilityClass isUserLoggedIn])
    {
        return [[SettingsClass sharedObject].loggedInUser completedAppointments];
    }
    return nil;
}

- (NSArray *) inProgressAppointments
{
    if ([UtilityClass isUserLoggedIn])
    {
        return [[SettingsClass sharedObject].loggedInUser inProgressAppointments];
    }
    return nil;
}

- (NSArray *) pausedAppointments
{
    if ([UtilityClass isUserLoggedIn])
    {
        return [[SettingsClass sharedObject].loggedInUser pausedAppointments];
    }
    return nil;
}
- (NSArray *) acceptedAppointments
{
    if ([UtilityClass isUserLoggedIn])
    {
        return [[SettingsClass sharedObject].loggedInUser acceptedAppointments];
    }
    return nil;
}

- (void) fetchSurveyFormForAppointment:(Appointment *)appointment
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appointment.appointmentToProperty))
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setObject:appointment.appointmentId forKey:@"appointmentid"];
			[requestParameters setObject:appointment.appointmentToProperty.propertyId forKey:kPropertyId];
			[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
			[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
			[self.fetchSurveyFormService fetchStockSurveyForm:requestParameters];
			
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) completeAppointment:(NSMutableDictionary *) requestParameters
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if([UtilityClass isUserLoggedIn])
		{
			NSData *responseData = [self.completeAppointmentService performSyncAppointmentUpdate:requestParameters];
			NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			CLS_LOG(@"Mark Complete Appointment Response %@",responseString);
			if(!isEmpty(responseString))
			{
				NSDictionary *JSONDictionary = [responseString JSONValue];
				NSDictionary *statusDictionary = [JSONDictionary objectForKey:kStatusTag];
				NSInteger statusCode = [[statusDictionary valueForKey:kCodeTag] intValue];
				if(statusCode == kSuccessCode)
				{
					NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
					// for (NSDictionary *responseDictionary in responseArray) {
					NSDictionary *appointment = [responseDictionary valueForKey:@"Appointment"];
					NSNumber *appointmentId = [appointment valueForKey:@"appointmentId"];
					if(!isEmpty(appointmentId))
					{
						Appointment *appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
						if(appointment)
						{
							appointment.isModified = [NSNumber numberWithBool:NO];
							appointment.isSurveyChanged = [NSNumber numberWithBool:NO];
							[self deleteCalendarEventForAppointment:appointment];
							AppointmentStatus completionStatus = [UtilityClass completionStatusForAppointmentType:[appointment getType]];
							[[PSDataUpdateManager sharedManager]
							 updateAppointmentStatus:completionStatus appointment:appointment];
						}
					}
					//}
				}
				else
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
				}
			}
			
		}
	});
}

- (void) updateAppointments:(NSMutableDictionary *) requestParameters
{
	if([UtilityClass isUserLoggedIn])
	{
		[self.completeAppointmentService updateAppointments:requestParameters];
	}
}

-(void) syncProgressStatusForAppointmentsWithParams:(NSMutableDictionary *) requestParameters{
    if([UtilityClass isUserLoggedIn]){
        [MBProgressHUD hideAllHUDsForView:[UtilityClass getVisibleViewController].view animated:YES];
        MBProgressHUD *spinner = [MBProgressHUD showHUDAddedTo:[UtilityClass getVisibleViewController].view animated:YES];
        [spinner setLabelText:@"Syncing Appointments Status..."];
        [spinner show:TRUE];
        [self.syncAppointmentStatusService syncAppointmentStatus:requestParameters];
    }
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
	if(service == self.getAppointmentService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveFailureNotification object:nil];
	}
	else if(service == self.fetchSurveyFormService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveFailureNotification object:nil];
	}
	
	else if (service == self.completeAppointmentService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
	}
    else if (service == self.syncAppointmentStatusService)
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kInProgressAppointmentSyncingFailed object:nil];
    }
	return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
	
	NSDictionary * responseObject = [jsonObject JSONValue];
	NSDictionary * responseDictionary = [responseObject valueForKey:kResponseTag];
	NSArray * appointmentsArray = [responseDictionary objectForKey:kAppointmentListTag];
    NSArray *deleteAppointmentsArray = [responseDictionary objectForKey:kDeleteAppointmentListTag];
    
    PSDataPersistenceManager * persistenceMgr = [PSDataPersistenceManager sharedManager];
    NSArray * commonDataArray = [responseDictionary objectForKey:kCommonDataTag];
    [persistenceMgr saveCommonData:commonDataArray];
    
    if(!isEmpty(deleteAppointmentsArray)){
        for(NSDictionary *dict in deleteAppointmentsArray){
            [[SettingsClass sharedObject].loggedInUser deleteAppointmentWithInfoDict:dict];
        }
    }
    if(!isEmpty(appointmentsArray))
    {
        [persistenceMgr saveAppointments:appointmentsArray];
        persistenceMgr = NULL;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsNoResultFoundNotification object:nil];
    }
    
}

#pragma mark - Rearranged Appointments handling
-(NSMutableArray *) getExistingAppointmentsInfoForFetchRequest{
    NSArray * appointments = [[SettingsClass sharedObject].loggedInUser.userToAppointments allObjects];
    NSMutableArray *existingAppointments = [[NSMutableArray alloc] init];
    for(Appointment *apt in appointments){
        NSMutableDictionary *aptDict = [[NSMutableDictionary alloc] init];
        [aptDict setValue:apt.appointmentType forKey:@"appointmentType"];
			
        //For legacy support, send NUll for empty creation date
        NSString *creationDate = [UtilityClass convertNSDateToServerDate:apt.creationDate];
        if(creationDate!=nil){
            [aptDict setValue:creationDate forKey:@"creationDate"];
        }
        else{
            [aptDict setValue:[NSNull null] forKey:@"creationDate"];
        }
			
			NSString *startDate = [UtilityClass convertNSDateToServerDate:apt.appointmentStartTime];
			if(startDate!=nil){
				[aptDict setValue:startDate forKey:@"startDate"];
			}
			else{
				[aptDict setValue:[NSNull null] forKey:@"startDate"];
			}
			
			NSString *endDate = [UtilityClass convertNSDateToServerDate:apt.appointmentEndTime];
			if(endDate!=nil){
				[aptDict setValue:endDate forKey:@"endDate"];
			}
			else{
				[aptDict setValue:[NSNull null] forKey:@"endDate"];
			}
			
			if(apt.appointmentStartTimeString!=nil){
				[aptDict setValue:apt.appointmentStartTimeString forKey:@"startTime"];
			}
			else{
				[aptDict setValue:[NSNull null] forKey:@"startTime"];
			}
			
			if(apt.appointmentEndTimeString!=nil){
				[aptDict setValue:apt.appointmentEndTimeString forKey:@"endTime"];
			}
			else{
				[aptDict setValue:[NSNull null] forKey:@"endTime"];
			}

			[aptDict setValue:[[SettingsClass sharedObject] currentUserId] forKey:@"operativeId"];
			
        [aptDict setValue:apt.appointmentId forKey:@"appointmentId"];
        [existingAppointments addObject:aptDict];
        
    }
    return existingAppointments;
    
}

#pragma mark - PSSyncAppointmentStatusServiceDelegate

-(void) service:(PSSyncAppointmentStatusService *)service didFailSyncingStatus:(NSError *)error{
    CLS_LOG(@"didFailWithError : %@", error);
    [UtilityClass showMessageWithHeader:@"Error" andBody:[NSString stringWithFormat:@"Appointments progress could not be logged. Please try later"] forViewController:[UtilityClass getVisibleViewController]];
    [MBProgressHUD hideAllHUDsForView:[UtilityClass getVisibleViewController].view animated:YES];
}

-(void) service:(PSSyncAppointmentStatusService *)service didSyncStatusSuccessfully:(id)result{
    NSError *jsonError;
    NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    [MBProgressHUD hideAllHUDsForView:[UtilityClass getVisibleViewController].view animated:YES];
    NSArray *failedAppt = (NSArray *)[response objectForKey:kAppointmentProgressSyncResponseFailedAppointments];
    if(isEmpty(failedAppt))
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kInProgressAppointmentSyncedSuccessfully object:nil];
        [UtilityClass showMessageWithHeader:@"Success" andBody:@"Appointment(s) status has been synced successfully."
                          forViewController:[UtilityClass getVisibleViewController]];
    }
    else
    {
        /*
         Type: Value
         Address: Value
         Reason: Value
         */
        NSString * alertmsg = @"";
        for(NSDictionary *dict in failedAppt){
            NSNumber *apptId = [dict objectForKey:kAppointmentProgressSyncResponseAppointmentId];
            Appointment *appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:apptId context:nil];
            NSString *appointmentType = [NSString stringWithFormat:@"Type: %@",appointment.appointmentType];
            NSString *appointmentAddress = [NSString stringWithFormat:@"Address: %@",[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong]];
            NSString *reasonStr = [NSString stringWithFormat:@"Reason: %@",[dict objectForKey:kAppointmentProgressSyncResponseFailureReasons]];
            NSString * errorAlert = [NSString stringWithFormat:@"\r%@\r%@\r%@\r", appointmentType, appointmentAddress,reasonStr];
            alertmsg = [NSString stringWithFormat:@"%@%@",alertmsg,errorAlert];
        }
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kInProgressAppointmentSyncingFailed object:nil];
        [UtilityClass showMessageWithHeader:@"Syncing Failure" andBody:alertmsg
                          forViewController:[UtilityClass getVisibleViewController]];
    }
    
}

#pragma mark - PSDeleteAppointmentServiceDelegate
- (void) service:(PSDeleteAppointmentService *)service didReceiveDeleteAppointmentResponse:(id)jsonObject
{
	NSDictionary *response = [jsonObject JSONValue];
	NSDictionary *statusDictionary = [response objectForKey:kStatusTag];
	
	if([[statusDictionary valueForKey:kCodeTag] intValue] == kSuccessCode)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveSuccessNotification object:nil];
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveFailureNotification object:nil];
	}
}

#pragma mark - PSFetchSurveyFormServiceDelegate
- (void) service:(PSFetchSurveyFormService *)service didReceivePercentData:(NSInteger)percentage
{
	CLS_LOG(@"didReceivePercentData : %lu", percentage);
}

- (void) service:(PSFetchSurveyFormService *)service didFinishDownloadingSurveyForm:(id)surveyData
{
	CLS_LOG(@"didFinishDownloadingSurveyForm : %@", surveyData);
	NSDictionary *responseDictionary = [surveyData JSONValue];
	NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
	[[PSDataPersistenceManager sharedManager] saveAppointmentSurveyData:surveyDataDictionary];
}

- (void) service:(PSFetchSurveyFormService *)service didFailWithError:(NSError *)error
{
	CLS_LOG(@"didFailWithError : %@", error);
}

#pragma mark - PSCompleteAppointmentServiceDelegate
- (void) service:(PSCompleteAppointmentService *)service didReceiveCompleteAppointmentResponse:(id)completeAppointmentData
{
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentsSuccessNotification object:completeAppointmentData];
}

- (void) service:(PSCompleteAppointmentService *)service didFailCompleteAppointmentWithError:(NSError *)error
{
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentsFailureNotification object:nil];
}

#pragma mark - Methods
- (void) deleteCalendarEventForAppointment:(Appointment *)appointment
{
	NSString *appointmentTitle = [NSString stringWithFormat:@"%@: %@ (%@)",appointment.appointmentType, appointment.appointmentTitle, appointment.appointmentId];
	NSString *existingEventIdentifier = [[PSEventsManager sharedManager] eventIdentifierForExistingEvent:appointmentTitle WithStartDate:appointment.appointmentStartTime AndEndDate:appointment.appointmentEndTime];
	if (existingEventIdentifier != nil)
	{
		CLS_LOG(@"Delete Calendar Event Identifier %@\n%@",appointmentTitle,existingEventIdentifier);
		[[PSEventsManager sharedManager] removeEventWithIdentifier:existingEventIdentifier completion:^(bool success, NSError *error) {
		}];
	}
}

- (BOOL) isFaultAppointmentComplete:(Appointment *) appointment
{
	BOOL isAppointmentComplete = NO;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.jobStatus LIKE[cd] %@",kJobStatusComplete];
	NSSet *completeAppointments = [appointment.appointmentToJobSheet filteredSetUsingPredicate:predicate];
	
	if ([completeAppointments count] == [appointment.appointmentToJobSheet count])
	{
		isAppointmentComplete = YES;
	}
	return isAppointmentComplete;
}
@end
