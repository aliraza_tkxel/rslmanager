//
//  PSAltFuelServicingPictureManager.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAltFuelServicingPictureManager.h"

@implementation PSAltFuelServicingPictureManager
static PSAltFuelServicingPictureManager *sharedPropertyManager = nil;
+(PSAltFuelServicingPictureManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [[PSAltFuelServicingPictureManager alloc] init];
        sharedPropertyManager.isPostingImage=NO;
        // Do any other initialisation stuff here
        sharedPropertyManager.uploadPhotographService = [[PSUploadAlternateHeatingPhotographsService alloc] init];
        sharedPropertyManager.uploadPhotographService.photographDelegate = sharedPropertyManager;
        
    });
    
    return sharedPropertyManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [super allocWithZone:zone];
    });
    return sharedPropertyManager;
}

- (void) uploadPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.uploadPhotographService uploadPhotograph:requestParameters streamLocation:image];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAltFuelPictureSaveFailureNotification object:nil];
    }
}


- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didReceivePhotographData:(id)surveyorData {
    
}

- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didFailWithError:(NSError *)error {
    
}


@end
