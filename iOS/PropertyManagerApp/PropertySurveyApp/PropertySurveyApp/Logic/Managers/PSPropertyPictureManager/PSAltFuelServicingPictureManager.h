//
//  PSAltFuelServicingPictureManager.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSUploadAlternateHeatingPhotographsService.h"
@interface PSAltFuelServicingPictureManager : NSObject<PSUploadAlternateHeatingPhotographsServiceDelegate>
@property (nonatomic, strong) PSUploadAlternateHeatingPhotographsService * uploadPhotographService;
@property (nonatomic, readwrite) BOOL isPostingImage;
+ (PSAltFuelServicingPictureManager *) sharedManager;
- (void) uploadPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image;
@end
