//
//  PSPropertyManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPropertyPictureManager.h"


@implementation PSPropertyPictureManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSPropertyPictureManager *sharedPropertyManager = nil;
+(PSPropertyPictureManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [[PSPropertyPictureManager alloc] init];
        sharedPropertyManager.isPostingImage=NO;
        // Do any other initialisation stuff here

        sharedPropertyManager.uploadPhotographService = [[PSUploadPhotographService alloc] init];
        sharedPropertyManager.uploadPhotographService.photographDelegate = sharedPropertyManager;
        
        sharedPropertyManager.uploadRepairPhotographService = [[PSUploadRepairPictureService alloc] init];
        
        sharedPropertyManager.uploadDefectPhotographService = [[PSUploadDefectPictureService alloc] init];
        
        sharedPropertyManager.uploadDefectPhotographService.photographDelegate = sharedPropertyManager;
        
        sharedPropertyManager.updatePhotographService = [[PSUpdatePhotographService alloc] init];
        sharedPropertyManager.updatePhotographService.photographDelegate = sharedPropertyManager;
        
        sharedPropertyManager.deletePhotographService = [[PSDeletePhotographService alloc] init];
        
        sharedPropertyManager.deletePhotographService.serviceDelegate = sharedPropertyManager;
        
        
        
    });
    
    return sharedPropertyManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [super allocWithZone:zone];
    });
    return sharedPropertyManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Methods

- (void) uploadPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.uploadPhotographService uploadPhotograph:requestParameters streamLocation:image];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }
}

- (void) updateDefaultPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.uploadPhotographService uploadPhotograph:requestParameters streamLocation:image];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }
}

- (void) uploadDefectPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.uploadDefectPhotographService uploadPhotograph:requestParameters streamLocation:image];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }
}

- (void) updatePhotograph:(NSDictionary *) requestParameters
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.updatePhotographService updatePhotograph:requestParameters streamLocation:nil];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }
}


- (void) deletePhotograph:(NSMutableDictionary *) requestParameters
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
            [self.deletePhotographService deletePhotograph:requestParameters];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }
}

#pragma mark - PSGetAllPropertiesServiceDelegate
- (void) service:(PSGetAllPropertiesService *)service didFinishLoadingProperties:(id)propertyData
{
    NSDictionary *propertyDictionary = [propertyData JSONValue];
    NSDictionary *statusDictionary = [propertyDictionary objectForKey:kStatusTag];
    NSDictionary *responseDictionary = [propertyDictionary objectForKey:kResponseTag];
    NSInteger statusCode = [[statusDictionary objectForKey:kCodeTag] integerValue];
    if (statusCode == kSuccessCode)
    {
        NSDictionary *pageInfo = [responseDictionary objectForKey:@"PageInfo"];
       // int pageNumber = [[pageInfo valueForKey:@"PageNumber"] integerValue];
        if (!isEmpty(pageInfo) /* && pageNumber == 1 */)
        {
            if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didReceivePageInformation:)])
            {
                [(PSPropertyListViewController *)self.delegate didReceivePageInformation:pageInfo];
            }
        }
        else
        {
            if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didReceivePageInformation:)])
            {
                [(PSPropertyListViewController *)self.delegate didReceivePageInformation:pageInfo];
            }
        }
        NSArray *propertyArray = [responseDictionary objectForKey:@"Properties"];
        if(!isEmpty(propertyArray))
        {
            // CLS_LOG(@"UserInfo : %@", propertyArray);
            [[PSDataPersistenceManager sharedManager] saveProperties:propertyArray];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveSuccessNotification object:nil];
        }
    }
}

- (void) service:(PSUploadPhotographService *)service didReceivePhotographData:(id)surveyorData {
    
}

- (void) service:(PSUploadPhotographService *)service didFailWithError:(NSError *)error {
    
}



@end
