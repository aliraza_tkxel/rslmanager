//
//  PSAuthenticator.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSUpdatePasswordService.h"
#import "PSForgotPasswordService.h"

@interface PSAuthenticator : NSObject <CoreRESTServiceDelegate>

@property (nonatomic, strong) PSLoginService* loginService;
@property (nonatomic, strong) PSUpdatePasswordService* updatePasswordService;
@property (nonatomic, strong) PSForgotPasswordService* forgotPasswordService;

+ (PSAuthenticator*) sharedAuthenticator;

- (void) authenticateUser;
- (void) authenticateUserWithName:(NSString *)userName password:(NSString *)password;
- (void) authenticateUserOfflineWithName:(NSString *)userName password:(NSString *)password;
- (BOOL) isUserAuthenticated;
- (void) updatePassword:(NSDictionary *)requestParameters;
- (void) forgotPassword:(NSDictionary *)requestParameters;
@end