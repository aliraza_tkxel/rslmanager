//
//  PSRFaultDetailsManager.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-21.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFaultDetailsManager.h"
#import "FaultReported.h"
#import "WorkRequired+CoreDataClass.h"
#import "VoidData.h"
@implementation PSRFaultDetailsManager

-(void) reportNewFaultDetail:(FaultDetailList *)faultDetail
								   faultArea:(FaultAreaList *)faultArea
								 isRecurring:(Boolean)isRecurring
								 problemDays:(NSString *)problemDays
											 notes:(NSString *)notes
{
	NSString * typeName = NSStringFromClass([FaultReported class]);
	NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
	FaultReported * faultReported = [NSEntityDescription insertNewObjectForEntityForName:typeName
																																inManagedObjectContext:dbContext];
	faultReported.faultReportedToFaultDetail = faultDetail;
	faultReported.faultReportedToFaultArea = faultArea;
	faultReported.isRecurring = [NSNumber numberWithBool:isRecurring];
	faultReported.problemDays = @([problemDays integerValue]);
	faultReported.notes = notes;
	
	[self.appointment addAppointmentToFaultReportedObject:faultReported];
	faultReported.faultReportedToAppointment = self.appointment;
	[self saveContext];
}


-(void) reportVoidWorkWith:(FaultRepairData *)faultDetail
                 faultArea:(FaultAreaList *)faultArea
                     notes:(NSString *)notes
{
    NSString * typeName = NSStringFromClass([WorkRequired class]);
    NSManagedObjectContext * dbContext = self.psDBContext.managedObjectContext;
    WorkRequired * workRequired = [NSEntityDescription insertNewObjectForEntityForName:typeName
                                                                inManagedObjectContext:dbContext];
    workRequired.workRequiredToFaultArea = faultArea;
    if(!isEmpty(notes)){
        workRequired.repairNotes = notes;
    }
    else{
        workRequired.repairNotes = @"";
    }
    
    
    //Setting umbrella value to TenantWork = YES no matter what
    workRequired.isTenantWork = [NSNumber numberWithBool:YES];
    workRequired.isBRSWork = [NSNumber numberWithBool:NO];
    
    workRequired.worksId = [NSNumber numberWithInt:-1]; // -1 indicate new record
    workRequired.status = @"To be Arranged";
    workRequired.isVerified = [NSNumber numberWithBool:FALSE];
    
    //Add fault detail data here to workRequired
    
    workRequired.repairDescription = faultDetail.faultRepairDescription;
    workRequired.repairId = faultDetail.faultRepairID;
    workRequired.gross = faultDetail.gross;
    
    [self.appointment.appointmentToVoidData addVoidDataToWorkRequiredObject:workRequired];
    workRequired.workRequiredToVoidData = self.appointment.appointmentToVoidData;
    [self saveContext];
}

@end
