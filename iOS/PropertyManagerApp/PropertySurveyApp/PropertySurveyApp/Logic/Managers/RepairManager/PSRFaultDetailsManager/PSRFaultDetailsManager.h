//
//  PSRFaultDetailsManager.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-21.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSRBaseManager.h"
#import "FaultAreaList.h"
#import "FaultRepairData+CoreDataClass.h"
#import "FaultDetailList.h"

@interface PSRFaultDetailsManager : PSRBaseManager

-(void) reportNewFaultDetail:(FaultDetailList *)faultDetail
									 faultArea:(FaultAreaList *)faultArea
								 isRecurring:(Boolean)isRecurring
								 problemDays:(NSString *)problemDays
											 notes:(NSString *)notes;

-(void) reportVoidWorkWith:(FaultRepairData *)faultDetail
                 faultArea:(FaultAreaList *)faultArea
                     notes:(NSString *)notes;

@end
