//
//  PSRBaseManager.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-22.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSRBaseManager : NSObject

@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) PSDatabaseContext * psDBContext;

-(id)init:(Appointment *)appointment;
-(void)saveContext;

@end
