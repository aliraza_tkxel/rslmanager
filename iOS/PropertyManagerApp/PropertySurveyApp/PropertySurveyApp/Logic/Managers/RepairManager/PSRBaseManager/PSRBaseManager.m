//
//  PSRBaseManager.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-22.
//  Copyright © 2015 TkXel. All rights reserved.
//

#import "PSRBaseManager.h"

@implementation PSRBaseManager

-(id)init:(Appointment *)appointment
{
	self.appointment = appointment;
	self.psDBContext = [PSDatabaseContext sharedContext];
	return self;
}

-(void)saveContext
{
	_appointment.loggedDate = [NSDate date];
	_appointment.isModified = [NSNumber numberWithBool:YES];
	[_psDBContext saveContext];
}

@end
