//
//  PSSynchronizationManager.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/14/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol PSSynchronizationManagerDelegate <NSObject>
@required
//UI effects
-(void) updateSpinnerWithLabel:(NSString *) progressLabel;
-(void) RollBackIfNoInternetAvailability;
//defects syncing
-(void) defectsSynchingFailedForAppointment:(Appointment *) appointment;
//image syncing
-(void) imagesSynchingFailedForAppointment:(Appointment *) appointment;
//data syncing
-(void) dataSynchingFailedForAppointment:(NSArray *) failedAppointments;
-(void) dataSyncingFailedOnNetworkLevel:(NSArray *)failedAppointments;
//Syncing successful
-(void) appointmentSyncingCompleted;
//Status syncing
-(void) appointmentStatusSyncingCompletedWithSuccess:(bool)success;
@end

@interface PSSynchronizationManager : NSObject
+ (PSSynchronizationManager*) sharedManager;
@property (nonatomic, strong) NSMutableArray *pendingAppointments;
@property (nonatomic, strong) NSMutableArray *sendingAppointments;
@property (nonatomic, strong) NSMutableArray *itemsUnderSyncing;//To cover different types of Items from defects to images;
@property  int totalObjects;//To count different types of Items from defects to images;
@property  BOOL isAnyObjectFailed;
@property (weak, nonatomic) id<PSSynchronizationManagerDelegate> delegate;

-(void) syncCompletedAppointments;
-(void) syncAppointmentProgressStatus;
@end
