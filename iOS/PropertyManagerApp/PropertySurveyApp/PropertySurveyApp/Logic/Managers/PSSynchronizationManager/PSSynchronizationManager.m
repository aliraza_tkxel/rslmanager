//
//  PSSynchronizationManager.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/14/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSSynchronizationManager.h"
#import "Defect+CoreDataClass.h"
#import "Defect+JSON.h"
#import "RepairPictures.h"
#import "RepairPictures+JSON.h"
#import "PropertyPicture+JSON.h"
#import "PropertyPicture+CoreDataClass.h"
#import "VoidWorks.h"
@implementation PSSynchronizationManager

#pragma mark - Progress Syncing Param Constants

NSString *const kProgressSyncingKeyAppointmentsList = @"appointments";
NSString *const kProgressSyncingKeyAppointmentId = @"appointmentId";
NSString *const kProgressSyncingKeyAppointmentType = @"appointmentType";
NSString *const kProgressSyncingKeyAppointmentStatus = @"status";
NSString *const kProgressSyncingKeyJobSheetsList = @"jobSheets";
NSString *const kProgressSyncingKeyJobSheetNumber = @"jobSheetNumber";
NSString *const kProgressSyncingKeyJobSheetStatus = @"status";
NSString *const kProgressSyncingKeyJobSheetNotes = @"notes";
NSString *const kProgressSyncingKeyJobSheetReason = @"reason";
NSString *const kProgressSyncingKeyLoggedInUserId = @"loggedInUserId";
NSString *const kProgressSyncingKeyAppointmentEndTime = @"appointmentEndDateTime";
NSString *const kProgressSyncingKeyAppointmentStartTime = @"appointmentStartDateTime";
#pragma mark - Singleton method
static PSSynchronizationManager *sharedsyncManager = nil;
+(PSSynchronizationManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedsyncManager = [[PSSynchronizationManager alloc] init];
        sharedsyncManager.pendingAppointments = [[NSMutableArray alloc] init];
        sharedsyncManager.sendingAppointments = [[NSMutableArray alloc] init];
        sharedsyncManager.itemsUnderSyncing = [[NSMutableArray alloc] init];
        sharedsyncManager.totalObjects = 0;
        sharedsyncManager.isAnyObjectFailed = NO;
        
    });
    return sharedsyncManager;
}

#pragma mark - Visible Methods

-(void) syncCompletedAppointments
{
    NSMutableArray *completedAppt = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
    if(!isEmpty(completedAppt)){
        if([completedAppt count]>0){
            _pendingAppointments = [NSMutableArray arrayWithArray:completedAppt];
            [_sendingAppointments removeAllObjects];
            [_sendingAppointments addObject:[_pendingAppointments objectAtIndex:0]];
            [_pendingAppointments removeObjectAtIndex:0];
            [self routeAppointmentForSyncing:[_sendingAppointments objectAtIndex:0]];
        }
        else{
            if(_delegate!=nil && [_delegate respondsToSelector:@selector(appointmentSyncingCompleted)]){
                [_delegate appointmentSyncingCompleted];
            }
        }
        
    }
    else{
        if(_delegate!=nil && [_delegate respondsToSelector:@selector(appointmentSyncingCompleted)]){
            [_delegate appointmentSyncingCompleted];
        }
    }
}

-(void) syncAppointmentProgressStatus
{
    NSMutableArray *inProgressAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] inProgressAppointments]];
    NSMutableArray *pausedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] pausedAppointments]];
    
    NSMutableArray *acceptedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] acceptedAppointments]];
    
    NSMutableArray * apptToBeSynced = [[NSMutableArray alloc] init];
    
    if(!isEmpty(inProgressAppointments)){
        [apptToBeSynced addObjectsFromArray:inProgressAppointments];
    }
    if(!isEmpty(pausedAppointments)){
        [apptToBeSynced addObjectsFromArray:pausedAppointments];
    }
    if(!isEmpty(acceptedAppointments)){
        [apptToBeSynced addObjectsFromArray:acceptedAppointments];
    }
    
    if(!isEmpty(apptToBeSynced)){
        
        if(IS_NETWORK_AVAILABLE){
            [[SettingsClass sharedObject] setIsSyncingProgress:YES];
            [self startSyncingProgressStatusForAppointments:apptToBeSynced];
            
        }
    }
    
}

#pragma mark - Roll back methods

-(void) rollBackManagerState{
    [_pendingAppointments removeAllObjects];
    [_sendingAppointments removeAllObjects];
    _totalObjects = 0;
    _isAnyObjectFailed  = NO;
    [_itemsUnderSyncing removeAllObjects];
}

#pragma mark - Routing Gateway

-(void) routeAppointmentForSyncing:(Appointment *) appointmentToBeSent
{
    [self launchPlaceholderHUDForAppointment:appointmentToBeSent];
    if([appointmentToBeSent.syncStatus integerValue] == AppointmentNotSynced){
        [self syncDefectsForAppointment:appointmentToBeSent];
    }
    else if([appointmentToBeSent.syncStatus integerValue] == AppointmentDefectSynced){
        [self syncImagesForAppointment:appointmentToBeSent];
        
    }
    else if ([appointmentToBeSent.syncStatus integerValue] == AppointmentImageSynced){
        [self syncDataForAppointment:appointmentToBeSent];
    }
    else if([appointmentToBeSent.syncStatus integerValue] == AppointmentSynced){
        [self completeSyncingForAppointment:appointmentToBeSent];
    }
}

#pragma mark - Placeholder HUD creator

-(void) launchPlaceholderHUDForAppointment:(Appointment *) appointment{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(_delegate!=nil && [_delegate respondsToSelector:@selector(updateSpinnerWithLabel:)])
        {
            if([appointment.syncStatus integerValue] == AppointmentNotSynced){
                [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Preparing defects for syncing"]];
            }
            else if([appointment.syncStatus integerValue] == AppointmentDefectSynced){
                [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Preparing images for syncing"]];
                
            }
            else if ([appointment.syncStatus integerValue] == AppointmentImageSynced){
                [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Preparing data for syncing"]];
            }
            else if([appointment.syncStatus integerValue] == AppointmentSynced){
                [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Completing appointment syncing"]];
            }
            
        }
    });
   
}

#pragma mark - Status Syncing

-(void) startSyncingProgressStatusForAppointments:(NSMutableArray *) inProgressAndPausedAppt{
    
    NSMutableDictionary * requestParams = [self createRequestParametersForProgressSyncingWithAppointments:inProgressAndPausedAppt];
    CLS_LOG(@"\nPost Data JSON \n%@",[requestParams JSONFragment]);
    [[PSAppointmentsManager sharedManager] syncProgressStatusForAppointmentsWithParams:requestParams];
    for(Appointment *appt in inProgressAndPausedAppt){
        appt.appointmentStatusSynced = [NSNumber numberWithBool:YES];
    }
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    NSError *error = nil;
    if (![managedObjectContext save:&error])
    {
        CLS_LOG(@"Error in Saving Syncing status: %@",[error description]);
    }

}

-(NSMutableDictionary *) createRequestParametersForProgressSyncingWithAppointments:(NSMutableArray*)inProgAppt{
    NSMutableDictionary * requestParams = [NSMutableDictionary new];
    NSMutableArray *apptData = [[NSMutableArray alloc] init];
    for(Appointment * appt in inProgAppt){
        NSMutableDictionary *dict =  [NSMutableDictionary dictionary];
        [dict setObject:appt.appointmentId forKey:kProgressSyncingKeyAppointmentId];
        [dict setObject:appt.appointmentType forKey:kProgressSyncingKeyAppointmentType];
        [dict setObject:appt.appointmentStatus forKey:kProgressSyncingKeyAppointmentStatus];
        [dict setObject:[UtilityClass convertNSDateToServerDate:appt.appointmentEndTime] forKey:kProgressSyncingKeyAppointmentEndTime];
        [dict setObject:[UtilityClass convertNSDateToServerDate:appt.appointmentStartTime] forKey:kProgressSyncingKeyAppointmentStartTime];
        [dict setObject:[self getJobSheetNumberForAppointment:appt] forKey:kProgressSyncingKeyJobSheetsList];
        [apptData addObject:dict];
    }
    [requestParams setObject:[[SettingsClass sharedObject] currentUserId] forKey:kProgressSyncingKeyLoggedInUserId];
    [requestParams setObject:apptData forKey:kProgressSyncingKeyAppointmentsList];
    return requestParams;
}

-(NSMutableArray*) getJobSheetNumberForAppointment:(Appointment *) appointment{
    NSMutableArray *paramArr = [[NSMutableArray alloc] init];
    AppointmentType type  = [appointment getType];
    /*FaultType is used for both Fault appointments and SBFault*/
    if(type== AppointmentTypeFault || type==AppointmentTypeVoidWorkRequired || type ==AppointmentTypeDefect){
        for (JobSheet *jobData in appointment.appointmentToJobSheet)
        {
            if([jobData.jobStatus isEqualToString:kJobStatusPaused] || [jobData.jobStatus isEqualToString:kJobStatusInProgress]){
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:jobData.jsNumber forKey:kProgressSyncingKeyJobSheetNumber];
                [dict setObject:jobData.jobStatus forKey:kProgressSyncingKeyJobSheetStatus];
                if([jobData.jobStatus isEqualToString:kJobStatusPaused]){
                    NSArray *jobPauseData = [jobData.jobPauseData allObjects];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"actionType = %@", kJobStatusPaused];
                    NSArray *filterArray = [jobPauseData filteredArrayUsingPredicate:predicate];
                    if(!isEmpty(filterArray)){
                        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pauseDate" ascending:NO];
                        NSArray *orderedArray = [filterArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                        JobPauseData *data = [orderedArray objectAtIndex:0];
                        if(!isEmpty(data.pauseReason)){
                            [dict setObject:data.pauseReason.pauseReason forKey:kProgressSyncingKeyJobSheetReason];
                            [dict setObject:data.pauseNote forKey:kProgressSyncingKeyJobSheetNotes];
                        }
                        else{
                            [dict setObject:[NSNull null] forKey:kProgressSyncingKeyJobSheetReason];
                            [dict setObject:[NSNull null] forKey:kProgressSyncingKeyJobSheetNotes];
                        }
                        
                    }
                }
                [paramArr addObject:dict];
            }
            
        }
    }
    else if(type == AppointmentTypePlanned || type == AppointmentTypeMiscellaneous || type == AppointmentTypeAdaptations || type==AppointmentTypeCondition){
        if(!isEmpty(appointment.appointmentToPlannedComponent)){
            if([appointment.appointmentToPlannedComponent.jobStatus isEqualToString:kJobStatusInProgress] ||[appointment.appointmentToPlannedComponent.jobStatus isEqualToString:kJobStatusPaused] )
            {
                NSMutableDictionary *dict = [NSMutableDictionary new];
                [dict setObject:appointment.appointmentToPlannedComponent.jsNumber forKey:kProgressSyncingKeyJobSheetNumber];
                [dict setObject:appointment.appointmentToPlannedComponent.jobStatus forKey:kProgressSyncingKeyJobSheetStatus];
                
                if([appointment.appointmentToPlannedComponent.jobStatus isEqualToString:kJobStatusPaused]){
                    NSArray *jobPauseData = [appointment.appointmentToPlannedComponent.jobPauseData allObjects];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"actionType = %@", kJobStatusPaused];
                    NSArray *filterArray = [jobPauseData filteredArrayUsingPredicate:predicate];
                    if(!isEmpty(filterArray)){
                        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pauseDate" ascending:NO];
                        NSArray *orderedArray = [filterArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                        JobPauseData *data = [orderedArray objectAtIndex:0];
                        if(!isEmpty(data.pauseReason)){
                            [dict setObject:data.pauseReason.pauseReason forKey:kProgressSyncingKeyJobSheetReason];
                            [dict setObject:data.pauseNote forKey:kProgressSyncingKeyJobSheetNotes];
                        }
                        else{
                            [dict setObject:[NSNull null] forKey:kProgressSyncingKeyJobSheetReason];
                            [dict setObject:[NSNull null] forKey:kProgressSyncingKeyJobSheetNotes];
                        }
                        
                    }
                }

                
                [paramArr addObject:dict];
            }
            
        }
        
    }
    
    return paramArr;
}


#pragma mark - Defects Syncing

-(void) syncDefectsForAppointment:(Appointment *) appointment{
    //Remove previous defects if any
    [_itemsUnderSyncing removeAllObjects];
    _isAnyObjectFailed = NO;
    AppointmentType type = [appointment getType];
    if(type == AppointmentTypeGas || type == AppointmentTypeGasVoid || type == AppointmentTypeOilServicing)
    {
        //Adding Appliances Defects
        for(Appliance * appl in [appointment.appointmentToProperty.propertyToAppliances allObjects])
        {
            for(Defect * defct in [appl.applianceDefects allObjects])
            {
                if([defct.defectID intValue]<0)
                {
                    [_itemsUnderSyncing addObject:defct];
                }
            }
        }
        
        for(OilHeating *oilHeat in appointment.appointmentToProperty.propertyToOilHeatings){
            for(Defect *defect in oilHeat.oilHeatingToDefect){
                if([defect.defectID intValue]<0){
                    [_itemsUnderSyncing addObject:defect];
                }
            }
        }
        
        //Adding Detector Defects
        for(Detector * dtr in [appointment.appointmentToProperty.propertyToDetector allObjects])
        {
            for(Defect * defct in [dtr.detectorDefects allObjects])
            {
                if([defct.defectID intValue]<0)
                {
                    [_itemsUnderSyncing addObject:defct];
                }
            }
        }
        
        //Adding Boiler Defects
        for(Boiler * blr in [appointment.appointmentToProperty.propertyToBoiler allObjects])
        {
            for(Defect * defct in [blr.boilerToDefect allObjects])
            {
                if([defct.defectID intValue]<0)
                {
                    [_itemsUnderSyncing addObject:defct];
                }
            }
        }
        
    }
    
    //Prepare to sync defects
    
    if(_itemsUnderSyncing.count>0)
    {
        //defect uploading notification
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defectDidFailToSync) name:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(defectSuccessfullySaved) name:kAppointmentOfflineDefectsSaveNotificationSuccess object:nil];
        _totalObjects = (int)_itemsUnderSyncing.count;
        [self uploadNextDefect];
    }
    else // no defects found, move on to image syncing
    {
        [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:appointment andStatus:AppointmentDefectSynced andSuccessBlock:^(BOOL isSuccess) {
            [self syncCompletedAppointments];
        } failure:^(NSError * error) {
            NSLog(@"Sync status update failed");
        }];
        
    }
    
}

-(void) uploadNextDefect{
    [((Defect*)[_itemsUnderSyncing objectAtIndex:0]) saveDefectOnServer];
    [_itemsUnderSyncing removeObjectAtIndex:0];
    int remaining = _totalObjects - (int)_itemsUnderSyncing.count;
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(updateSpinnerWithLabel:)])
    {
        [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Uploading %i of %i Defects..",remaining,_totalObjects]];
    }
}

-(BOOL)checkAllDefectsPostedSuccessFully
{
    if(_itemsUnderSyncing.count==0 && !_isAnyObjectFailed)
    {
        CLS_LOG(@"defect posting success");
        //Unregistring Success and failure notification only when going to post images
        [self removeNotificationsForDefect];
        return YES;
    }
    return NO;
}

-(void) removeNotificationsForDefect{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineDefectsSaveNotificationSuccess object:nil];
}

-(void)defectDidFailToSync
{
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(defectsSynchingFailedForAppointment:)]){
        [_delegate defectsSynchingFailedForAppointment:[_sendingAppointments objectAtIndex:0]];
        [self removeNotificationsForDefect];
        _isAnyObjectFailed = YES;
        [_delegate  RollBackIfNoInternetAvailability];
        [self rollBackManagerState];
    }
}



-(void)defectSuccessfullySaved
{
    if([self checkAllDefectsPostedSuccessFully])
    {
        if(_delegate!=nil && [_delegate respondsToSelector:@selector(updateSpinnerWithLabel:)])
        {
            [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Uploading Images..."]];
        }
        
        [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:0] andStatus:AppointmentDefectSynced andSuccessBlock:^(BOOL isSuccess) {
            [self syncCompletedAppointments];
        } failure:^(NSError *error) {
            NSLog(@"Sync status update failed");
        }];
        
        return;
    }
    if(_itemsUnderSyncing.count>0)
    {
        [self performSelector:@selector(uploadNextDefect)
                   withObject:self
                   afterDelay:1.0];
    }
    else
    {
        if(_delegate!=nil && [_delegate respondsToSelector:@selector(RollBackIfNoInternetAvailability)])
        {
            [_delegate  RollBackIfNoInternetAvailability];
        }
    }
}


#pragma mark - Images Syncing

-(void) syncImagesForAppointment:(Appointment *) appointment {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //================================================================ Upload in Background =================
        _isAnyObjectFailed = NO;
        CLS_LOG(@"sendOfflineImageThatsync");
        //Registring Success and failure notification only when going to post
        
        [_itemsUnderSyncing removeAllObjects];
        
        //getting defaultProperty pictures for syncing
        if(appointment.appointmentToProperty) //avoiding  Scheme object.
        {
            if(appointment.appointmentToProperty.defaultPicture)//Existance Verification
            {
                PropertyPicture * pic = appointment.appointmentToProperty.defaultPicture;
                
                // image path nil means image not synced and deletePic means dirty picture
                if(!pic.imagePath || [pic.deletedPicture isEqualToNumber:[NSNumber numberWithBool:YES]])
                {
                    [_itemsUnderSyncing addObject:pic];
                }
            }
        }
        //end
        
        AppointmentType type = [appointment getType];
        if(type == AppointmentTypeGas || type == AppointmentTypeGasVoid || type ==AppointmentTypeAlternativeFuels || type == AppointmentTypeOilServicing)
        {
            // only Stock and gas property images can be deleted
            for(PropertyPicture * ppic in [appointment.appointmentToProperty.propertyToPropertyPicture allObjects])
            {
                if(!ppic.imagePath || [ppic.deletedPicture isEqualToNumber:[NSNumber numberWithBool:YES]])  // Adding dirty images in too
                {
                    [_itemsUnderSyncing addObject:ppic];
                }
            }
            
            if(type==AppointmentTypeOilServicing){
                for(OilHeating *heating in appointment.appointmentToProperty.propertyToOilHeatings){
                    for(Defect *defect in heating.oilHeatingToDefect){
                        for(DefectPicture*pict in defect.defectPictures){
                            [_itemsUnderSyncing addObject:pict];
                        }
                    }
                    for(AlternativeFuelPicture *pict in heating.oilHeatingToFireServiceInspection.fireServicingInspectionToPictures){
                        [_itemsUnderSyncing addObject:pict];
                    }
                }
            }
            
            if(type==AppointmentTypeAlternativeFuels){
                if(!isEmpty(appointment.appointmentToProperty.propertyToAlternativeHeating)){
                    for(AlternativeHeating *heater in appointment.appointmentToProperty.propertyToAlternativeHeating){
                        for(AlternativeFuelPicture *pict in heater.alternativeHeatingToSolarInspection.solarInspectionToPictures){
                            [_itemsUnderSyncing addObject:pict];
                        }
                        for(AlternativeFuelPicture *pict in heater.alternativeHeatingToMVHRInspection.mvhrInspectionToPictures){
                            [_itemsUnderSyncing addObject:pict];
                        }
                        for(AlternativeFuelPicture *pict in heater.alternativeHeatingToAirsourceInspection.airsourceInspectionToPictures){
                            [_itemsUnderSyncing addObject:pict];
                        }
                    }
                }
            }
            
            //adding Defect of Appliance and detector images
            if(type == AppointmentTypeGas || type == AppointmentTypeGasVoid)
            {
                //Adding Appliances Defect images
                for(Appliance * appl in [appointment.appointmentToProperty.propertyToAppliances allObjects])
                {
                    for(Defect * defct in [appl.applianceDefects allObjects])
                    {
                        for(DefectPicture * dpic in [defct.defectPictures allObjects])
                        {
                            if(!dpic.imagePath)
                            {
                                [_itemsUnderSyncing addObject:dpic];
                            }
                        }
                    }
                }
                //Adding Boiler Defect images
                for(Boiler * blr in [appointment.appointmentToProperty.propertyToBoiler allObjects])
                {
                    for(Defect * defct in [blr.boilerToDefect allObjects])
                    {
                        for(DefectPicture * dpic in [defct.defectPictures allObjects])
                        {
                            if(!dpic.imagePath)
                            {
                                [_itemsUnderSyncing addObject:dpic];
                            }
                        }
                    }
                }
                
                //Adding Detector Defect images
                for(Detector * dtr in [appointment.appointmentToProperty.propertyToDetector allObjects])
                {
                    for(Defect * defct in [dtr.detectorDefects allObjects])
                    {
                        for(DefectPicture * dpic in [defct.defectPictures allObjects])
                        {
                            if(!dpic.imagePath)
                            {
                                [_itemsUnderSyncing addObject:dpic];
                            }
                        }
                    }
                }
                
            }
        }
        else if(type == AppointmentTypeFault
                || type == AppointmentTypeVoidWorkRequired
                || type == AppointmentTypeDefect)
        {
            NSArray * jobDataListArray = [appointment.appointmentToJobSheet allObjects];
            for(JobSheet * jobData in jobDataListArray)
            {
                NSArray * repairPicturesArray = [[PSCoreDataManager sharedManager] repairPictureAfterAndBeforeListWithJSNumber:jobData.jsNumber];
                //  NSArray * repairPicturesArray = [jobData.jobDataListToRepairImages allObjects];
                for(RepairPictures * rpic in repairPicturesArray)
                {
                    if(!rpic.imagePath)
                    {
                        [_itemsUnderSyncing addObject:rpic];
                    }
                }
            }
        }
        // Now we have send request for uploading of images that are not sent on server.
        //Remove object just as you send
        if(_itemsUnderSyncing.count>0)
        {
            //image uploading notification
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidFailToSync) name:kAppointmentOfflineImageSaveNotificationFail object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageSuccessfullySaved) name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
            _totalObjects = (int)_itemsUnderSyncing.count;
            [self uploadNextImage];
            
        }
        else // incase of all images sync
        {
            //[spinnerView setLabelText:@"Uploading Appointments..."];
            [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:appointment andStatus:AppointmentImageSynced andSuccessBlock:^(BOOL isSuccess) {
                [self syncCompletedAppointments];
            } failure:^(NSError *error) {
                NSLog(@"Sync status update failed");
            }];
            
        }
        //================================================================ End upload in background =============
        
    });
}

-(BOOL)checkAllImagesPostedSuccessFully
{
    if(_itemsUnderSyncing.count==0 && !_isAnyObjectFailed)
    {
        CLS_LOG(@"image posting success");
        //Unregistring Success and failure notification only when going to post
        [self removeNotificationForImages];
        return YES;
    }
    return NO;
}

-(void) removeNotificationForImages{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationFail object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
}

-(void)imageDidFailToSync
{
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(imagesSynchingFailedForAppointment:)]){
        [_delegate imagesSynchingFailedForAppointment:[_sendingAppointments objectAtIndex:0]];
        [_delegate  RollBackIfNoInternetAvailability];
        _isAnyObjectFailed = YES;
        [self removeNotificationForImages];
        [self rollBackManagerState];
    }
}
-(void)imageSuccessfullySaved
{
    if([self checkAllImagesPostedSuccessFully])
    {
        //[spinnerView setLabelText:@"Uploading Appointments..."];
        [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:0] andStatus:AppointmentImageSynced andSuccessBlock:^(BOOL isSuccess) {
            [self syncCompletedAppointments];
        } failure:^(NSError *error) {
            NSLog(@"Sync status update failed");
        }];
        
        return;
    }
    if(_itemsUnderSyncing)
    {
        if(_itemsUnderSyncing.count>0)
        {
            
            [self performSelector:@selector(uploadNextImage)
                       withObject:self
                       afterDelay:1.0];
        }
        else
        {
            if(_delegate!=nil && [_delegate respondsToSelector:@selector(RollBackIfNoInternetAvailability)])
            {
                [_delegate  RollBackIfNoInternetAvailability];
            }
        }
    }
}

-(void) uploadNextImage{
    if(!isEmpty(_sendingAppointments)){
        Appointment *currentApt = [_sendingAppointments objectAtIndex:0];
        NSString * appointmentType = currentApt.appointmentType;
        if(!isEmpty(_itemsUnderSyncing)){
            if([[_itemsUnderSyncing objectAtIndex:0] isKindOfClass:[PropertyPicture class]])
            {
                [((PropertyPicture*)[_itemsUnderSyncing objectAtIndex:0]) uploadPicture];
            }
            else if([[_itemsUnderSyncing objectAtIndex:0] isKindOfClass:[RepairPictures class]])
            {
                [((RepairPictures*)[_itemsUnderSyncing objectAtIndex:0]) uploadPicture:appointmentType];
            }
            else if([[_itemsUnderSyncing objectAtIndex:0] isKindOfClass:[DefectPicture class]])
            {
                [((DefectPicture*)[_itemsUnderSyncing objectAtIndex:0]) uploadPicture];
            }
            else if([[_itemsUnderSyncing objectAtIndex:0] isKindOfClass:[AlternativeFuelPicture class]])
            {
                [((AlternativeFuelPicture*)[_itemsUnderSyncing objectAtIndex:0]) uploadPicture];
            }
            [_itemsUnderSyncing removeObjectAtIndex:0];
            int remaining = _totalObjects - (int)_itemsUnderSyncing.count;
            if(_delegate!=nil && [_delegate respondsToSelector:@selector(updateSpinnerWithLabel:)])
            {
                [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Syncing %i of %i image",remaining,_totalObjects]];
            }
        }
        else{
            [self imageDidFailToSync];
        }
    }
    
    
}


#pragma mark - Data Syncing

-(void) syncDataForAppointment:(Appointment *) appointment{
    _isAnyObjectFailed = NO;
    [_itemsUnderSyncing removeAllObjects];
    CLS_LOG(@"data posting actual");
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(updateSpinnerWithLabel:)])
    {
        [_delegate updateSpinnerWithLabel:[NSString stringWithFormat:@"Uploading data..."]];
    }

    NSMutableArray * appointments = [NSMutableArray array];
    NSDictionary * appointmentDictionary = [appointment JSONValue];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentModificationChangeSuccessNotificationReceive:) name:kAppointmentModificationStatusChangeSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentModificationChangeFailureNotificationReceive:) name:kAppointmentModificationStatusChangeFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateAppointmentsSuccessNotificationReceive:) name:kUpdateAppointmentsSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateAppointmentsFailureNotificationReceive) name:kUpdateAppointmentsFailureNotification object:nil];
    if (!isEmpty(appointmentDictionary))
    {
        [appointments addObject:appointmentDictionary];
    }
    if (!isEmpty(appointments))
    {
        NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
        [requestParameters setObject:appointments forKey:@"appointments"];
        [requestParameters setObject:[NSNumber numberWithBool:NO] forKey:@"respondWithData"];
        [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
        [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
        [requestParameters setObject:[NSNumber numberWithBool:YES] forKey:@"responseWithIndividualStatus"];
        NSString * json = [requestParameters JSONFragment];
        CLS_LOG(@"\nPost Data JSON \n%@",[requestParameters JSONFragment]);
        [[PSAppointmentsManager sharedManager] updateAppointments:requestParameters];
    }
    PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
    delegate.postingDataObjectAppointments =0;
}

-(void) removeNotificationsForDataSyncing{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentModificationStatusChangeSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentModificationStatusChangeFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentsSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentsFailureNotification object:nil];
}

- (void) onUpdateAppointmentsSuccessNotificationReceive:(NSNotification*)notification
{
    NSDictionary* response = [notification.object JSONValue];
    NSString *respStr = [response JSONFragment];
    CLS_LOG(@"%@",respStr);
    PSAppDelegate * delgate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
    if(!delgate.postingStartedByOutbox)
    {
        
        NSDictionary* responseDictionary = [response objectForKey:kResponseTag];
        __block NSArray* savedAppointments = [responseDictionary objectForKey:@"savedAppointments"];
        __block NSArray* failedAppointments = [responseDictionary objectForKey:@"failedAppointments"];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[PSDataPersistenceManager sharedManager]changeAppointmentModificationStatus:_sendingAppointments savedAppointments:savedAppointments failedAppointments:failedAppointments postNotification:YES];
        });
    }
   
}

- (void) onUpdateAppointmentsFailureNotificationReceive
{
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(dataSyncingFailedOnNetworkLevel:)]){
        [_delegate dataSyncingFailedOnNetworkLevel:_sendingAppointments];
        [self removeNotificationsForDataSyncing];
    }
}


- (void) onAppointmentModificationChangeFailureNotificationReceive:(NSNotification*)notification
{
    
    NSArray* failedAppointments = (NSArray*)notification.object;
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(dataSynchingFailedForAppointment:)]){
        [_delegate dataSynchingFailedForAppointment:failedAppointments];
        [self removeNotificationsForDataSyncing];
    }
    [self removeNotificationsForDataSyncing];
    [self rollBackManagerState];
    
}


- (void) onAppointmentModificationChangeSuccessNotificationReceive:(NSNotification*)notification
{
    [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:0]  andStatus:AppointmentSynced andSuccessBlock:^(BOOL isSuccess) {
        [self syncCompletedAppointments];
    } failure:^(NSError *error) {
        NSLog(@"Sync status update failed");
    }];
    
}


#pragma mark - Finish Syncing

-(void) completeSyncingForAppointment:(Appointment *) appointment{
    [self removeNotificationsForDataSyncing];
    [[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
    [self performSelector:@selector(syncCompletedAppointments)
               withObject:self
               afterDelay:2];
    
}


@end
