//
//  APNSConstants.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#pragma mark -
#pragma mark Apple Push Notification Constants


#define kNotifPushNotificationSaved @"kNotifPushNotificationSaved"

#define NotificationEntity @"Notification"

#define kAPNSDeviceToken @"APNSDeviceToken"
#define kAppointmentOfflineImageSaveNotificationSuccess @"kAppointmentOfflineImageSaveNotificationSuccess"
#define kAppointmentOfflineImageSaveNotificationFail @"kAppointmentOfflineImageSaveNotificationFail"
#define kAppointmentOfflineDefectsSaveNotificationSuccess @"kAppointmentOfflineDefectsSaveNotificationSuccess"
#define kAppointmentOfflineDefectsSaveNotificationFail @"kAppointmentOfflineDefectsSaveNotificationFail"

//#define kAPNSSoundFile @"ringme.wav"
//#define kAPNSIMSoundFile @"im.wav"


#define kTagPromtForAPNS 100
#define kTagAskForVOIPService 101
#define kinitiateCallResponseReceived 104


typedef NS_ENUM(NSInteger, APNSDTKOperation)
{
	APNSDTKOperationInsert = 0,
	APNSDTKOperationRemove
};


typedef NS_ENUM(NSInteger, APNSId)
{
    APNSIdAppointment = 0
};

typedef NS_ENUM(NSInteger, APNSMode)
{
	APNSModeLaunch = 0,
	APNSModeRemote = 1
};


#pragma mark APNS Payload JSON Keys
static NSString * const kAPNSId = @"apnsid";
static NSString * const kAPS = @"aps";
static NSString * const kAPSBadge = @"badge";
static NSString * const kAPSSound = @"sound";
static NSString * const kAPSAlert = @"alert";
static NSString * const kAPSAlertActionLocKey =  @"action-loc-key";
static NSString * const kAPSAlertBody  = @"body";

#define kAPSViewButtonTitle = LOC(@"KEY_ALERT_VIEW");
#define kAPSCacnelButtonTitle = LOC(@"KEY_ALERT_CLOSE");


