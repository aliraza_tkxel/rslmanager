//
//  APNSHandler.h
//  PropertySurveyApp
//
//  Created by TkXel on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APNSHandler : NSObject
+ (APNSHandler *) sharedHandler;

- (void) handlePushNotificationWithPayload:(NSDictionary*)payload notificationMode:(APNSMode)mode;
- (void) performActionOnNotificationWithPayload:(NSDictionary*)payload;
- (void) promptForNotificationWithPayload:(NSDictionary*)payload;
@end
