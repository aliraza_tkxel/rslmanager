//
//  PSApplianceManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSApplianceManager.h"
#import "CoreDataHelper.h"
#import "PSDataPersistenceManager+(Detectors).h"
#import "PSDataPersistenceManager+(Meters).h"
#import "PSDataPersistenceManager+(Defects).h"
#import "PSDataPersistenceManager+(Boiler).h"

@implementation PSApplianceManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSApplianceManager *sharedAppliacnceManager = nil;
+ (PSApplianceManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAppliacnceManager = [[PSApplianceManager alloc] init];
        // Do any other initialisation stuff here
        sharedAppliacnceManager.saveApplianceService = [[PSSaveApplianceService alloc] init];
        sharedAppliacnceManager.fetchPropertyAppliancesService = [[PSFetchPropertyAppliancesService alloc] init];
        sharedAppliacnceManager.fetchApplianceDefectsService = [[PSFetchDefectsService alloc] init];
        sharedAppliacnceManager.saveDefectService = [[PSSaveDefectService alloc] init];
    });
    return sharedAppliacnceManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAppliacnceManager = [super allocWithZone:zone];
    });
    return sharedAppliacnceManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark - Methods
- (void) fetchPropertyAppliances:(NSDictionary *)requestParameters forProperty:(NSManagedObjectID *)propertyId
{
    if([UtilityClass isUserLoggedIn])
    {
        //GET Request to download Property Appliances
        //TODO: Convert to Async Call to avoid UI Freezing
        NSData *responseData = [self.fetchPropertyAppliancesService fetchPropertyAppliacnes:requestParameters];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *appliancesArray = [responseDictionary valueForKey:@"appliancesList"];
                    NSArray *detectorsArray = [responseDictionary valueForKey:@"propertyDetectors"];
                    NSArray *metersArray = [responseDictionary valueForKey:@"Meters"];
					NSArray *boilerArray = [responseDictionary valueForKey:@"boilers"];
									
									CLS_LOG(@"Property Appliance Data: %@ ",[responseString JSONValue]);
                    CLS_LOG(@"Appliances Count %lu---Detectors Count %lu ---Meters Count %lu",(unsigned long)[appliancesArray count],(unsigned long)[detectorsArray count], (unsigned long)[metersArray count]);
                    
                  [[PSDataPersistenceManager sharedManager] saveFetchedApplianceData:appliancesArray forProperty:propertyId isAddedOffline:NO];
                  [[PSDataPersistenceManager sharedManager] saveFetchedDetectorData:detectorsArray forProperty:propertyId];
                  [[PSDataPersistenceManager sharedManager] saveFetchedMeterData:metersArray forProperty:propertyId];
                  [[PSDataPersistenceManager sharedManager] saveFetchedBoilerData:boilerArray forProperty:propertyId isAddedOffline:NO];
                    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesDataDownloadedNotification
                                                                                        object:nil];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                    object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                object:nil];
        }
    }
}

- (void) fetchApplianceDefects:(NSDictionary *)requestParameters
{
	if ([UtilityClass isUserLoggedIn])
	{
		NSData *responseData = [self.fetchApplianceDefectsService fetchApplianceDefects:requestParameters];
		NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		
		// NSString *filePath = [[NSBundle mainBundle] pathForResource:@"defects" ofType:@"json"];
		// NSString *responseString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
		if(!isEmpty(responseString))
		{
			NSDictionary *JSONDictionary = [responseString JSONValue];
			NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
			if(statusCode == kSuccessCode)
			{
				NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
				if(!isEmpty(responseDictionary))
				{
					NSArray *applianceDefects = [responseDictionary valueForKey:@"defects"];
					NSArray *detectorDefects = [responseDictionary valueForKey:@"detectorDefects"];
					NSArray *defectCategories = [responseDictionary valueForKey:@"defectCategories"];
					NSMutableDictionary *defectsDictionary = [NSMutableDictionary dictionary];
					[defectsDictionary setObject:detectorDefects forKey:@"detectorDefects"];
					[defectsDictionary setObject:applianceDefects forKey:@"defects"];
					[[PSDataPersistenceManager sharedManager] saveFetchedDefectCategories:defectCategories defects:defectsDictionary];
				}
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification
																																						object:nil];
			}
		}
		else
		{
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification
																																					object:nil];
		}
	}
}

- (void) saveAppliance:(NSDictionary *)appliance forProperty:(NSManagedObjectID *)propertyId
{
    if([UtilityClass isUserLoggedIn])
    {
        NSMutableDictionary *applianceDictionary = [NSMutableDictionary dictionary];// [appliance mutableCopy];
			
        [applianceDictionary setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
        [applianceDictionary setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
        [applianceDictionary setObject:appliance forKey:@"ApplianceData"];
        
        //Synchronous Call
        NSData *responseData = [self.saveApplianceService saveAppliance:applianceDictionary];
        
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *applianceArray = @[responseDictionary];
                    [[PSDataPersistenceManager sharedManager] saveApplianceData:applianceArray forProperty:propertyId isAddedOffline:NO];
                    
                    
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneFailureNotification
                                                                                    object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneFailureNotification
                                                                                object:nil];
        }
    }
}

- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray
{
    [[PSDataPersistenceManager sharedManager] saveAppliancePropertiesOffline:locationArray typeArray:typeArray manufecturerArray:manufecturerArray modelArray:modelArray];
}

- (void) service:(PSSaveApplianceService *)service didFailWithError:(NSError *)error
{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
    
}

/**
 @brief Returns All Appliacnces (Fetched from Core Database)
 */
- (NSArray *) fetchAllAppliances
{
    NSArray *appliances = nil;
    STARTEXCEPTION
    if([UtilityClass isUserLoggedIn])
    {
        NSArray* fetchedAppliances = [CoreDataHelper getObjectsFromEntity:kAppliance
																																predicate:[NSPredicate predicateWithValue:YES]
																																	sortKey:nil
																														sortAscending:YES
																																	context:[PSDatabaseContext sharedContext].managedObjectContext];
        if([fetchedAppliances count] > 0)
        {
            appliances = [NSArray arrayWithArray:fetchedAppliances];
        }
    }
    ENDEXCEPTION
    return appliances;
}


- (NSArray *) fetchApplianceLoactions
{
    NSArray *applianceLoactions = nil;
    STARTEXCEPTION
    if([UtilityClass isUserLoggedIn])
    {
        NSArray* fetchedLocations = [CoreDataHelper getObjectsFromEntity:kApplianceLocation
																															 predicate:[NSPredicate predicateWithValue:YES]
																																 sortKey:nil
																													 sortAscending:YES
																																 context:[PSDatabaseContext sharedContext].managedObjectContext];
        if([fetchedLocations count] > 0)
        {
            applianceLoactions = [NSArray arrayWithArray:fetchedLocations];
        }
    }
    ENDEXCEPTION
    return applianceLoactions;
}

- (NSArray *) fetchApplianceManufacturers
{
    NSArray *applianceManufacturers = nil;
    STARTEXCEPTION
	NSArray* fetchedManufacturers = [CoreDataHelper getObjectsFromEntity:kApplianceManufacturer
																														 predicate:[NSPredicate predicateWithValue:YES]
																															 sortKey:nil
																												 sortAscending:YES
																															 context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedManufacturers count] > 0)
	{
		applianceManufacturers = [NSArray arrayWithArray:fetchedManufacturers];
	}
	ENDEXCEPTION
	return applianceManufacturers;
}

- (NSArray *) fetchApplianceModels
{
    NSArray *applianceModels = nil;
    STARTEXCEPTION
	NSArray* fetchedModels = [CoreDataHelper getObjectsFromEntity:kApplianceModel
																											predicate:[NSPredicate predicateWithValue:YES]
																												sortKey:nil
																									sortAscending:YES
																												context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedModels count] > 0)
	{
		applianceModels = [NSArray arrayWithArray:fetchedModels];
	}
	ENDEXCEPTION
	return applianceModels;
}

- (NSArray *) fetchApplianceTypes
{
    NSArray *applianceTypes = nil;
    STARTEXCEPTION
	NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kApplianceType
																										 predicate:[NSPredicate predicateWithValue:YES]
																											 sortKey:nil
																								 sortAscending:YES
																											 context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedTypes count] > 0)
	{
		applianceTypes = [NSArray arrayWithArray:fetchedTypes];
	}
	ENDEXCEPTION
	return applianceTypes;
}

- (NSArray *) fetchMeterTypes
{
  NSArray *meterTypes = nil;
  STARTEXCEPTION
	NSArray* fetchedMeterTypes = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([MeterType class])
																													predicate:[NSPredicate predicateWithValue:YES]
																														sortKey:nil
																											sortAscending:YES
																														context:[PSDatabaseContext sharedContext].managedObjectContext];
  if([fetchedMeterTypes count] > 0)
    {
    meterTypes = [NSArray arrayWithArray:fetchedMeterTypes];
    }
  ENDEXCEPTION
  return meterTypes;
}

- (NSMutableArray *)fetchDeviceMeterTypes:(NSString *)deviceType
{
  NSMutableArray* deviceMeterTypes = nil;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.deviceType =%@", deviceType];
  STARTEXCEPTION
	NSArray*  fetchedDeviceMeterTypes = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([MeterType class])
																																 predicate:predicate
																																	 sortKey:nil
																														 sortAscending:YES
																																	 context:[PSDatabaseContext sharedContext].managedObjectContext];
  
  if([fetchedDeviceMeterTypes count] > 0)
    {
     deviceMeterTypes = [NSMutableArray arrayWithArray:fetchedDeviceMeterTypes];
    }
  
  ENDEXCEPTION
  return deviceMeterTypes;
  
}

- (NSArray *) fetchDetectorTypes:(NSString*) sortColumn
{
  NSArray *detectorTypes = nil;
  
  STARTEXCEPTION
	NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kDetectorType
																										 predicate:[NSPredicate predicateWithValue:YES]
																											 sortKey:sortColumn
																								 sortAscending:YES
																											 context:[PSDatabaseContext sharedContext].managedObjectContext];
  if([fetchedTypes count] > 0)
    {
    detectorTypes = [NSArray arrayWithArray:fetchedTypes];
    }
  ENDEXCEPTION
  
  return detectorTypes;
}

- (NSArray *) fetchPowerTypes:(NSString*) sortColumn
{
  NSArray *powerTypes = nil;
  
  STARTEXCEPTION
	NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kPowerType
																										 predicate:[NSPredicate predicateWithValue:YES]
																											 sortKey:sortColumn
																								 sortAscending:YES
																											 context:[PSDatabaseContext sharedContext].managedObjectContext];
  if([fetchedTypes count] > 0)
    {
    powerTypes = [NSArray arrayWithArray:fetchedTypes];
    }
  ENDEXCEPTION
  
  return powerTypes;
}

- (BOOL) isApplianceInspected :(Property *)property
{
    BOOL isInspected = NO;
	
    NSPredicate *inspectedPredicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 1 AND (SELF.isActive = 1 OR SELF.isActive = nil )"];
    NSSet *inspectedAppliances = [property.propertyToAppliances filteredSetUsingPredicate:inspectedPredicate];
	
	  NSSet *activeAppliances = [self getActiveAppliances:property];

 if ([inspectedAppliances count] == [activeAppliances count] && [activeAppliances count] !=0)
		{
		  isInspected = YES;
	  }
	
    return isInspected;
}

- (NSSet *) getActiveAppliances:(Property *)property
{
	NSPredicate *activeAppliancePredicate = [NSPredicate predicateWithFormat:@"(SELF.isActive = 1 OR SELF.isActive = nil )"];
	NSSet *activeAppliances = [property.propertyToAppliances filteredSetUsingPredicate:activeAppliancePredicate];
	
	return activeAppliances;
}

- (BOOL) isBoilerInspected :(Property *)property
{
	BOOL isInspected = YES;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 0 OR SELF.isInspected == nil"];
	NSSet *unInspectedBoilers = [property.propertyToBoiler filteredSetUsingPredicate:predicate];
	
	if (property.propertyToBoiler.count == 0) {
		isInspected = NO;
	}else if ([unInspectedBoilers count] > 0)
	{
		isInspected = NO;
	}
	
	return isInspected;
}

- (BOOL) isDetectorInspected :(Property *)property
{
	BOOL isInspected = YES;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 0 OR SELF.isInspected == nil OR SELF.isPassed == nil"];
	NSSet *unInspectedDetectors = [property.propertyToDetector filteredSetUsingPredicate:predicate];
	
	if (property.propertyToDetector.count == 0) {
		isInspected = NO;
	}else if ([unInspectedDetectors count] > 0)
	{
		isInspected = NO;
	}

	return isInspected;
}

- (BOOL) isMeterInspected :(Property *)property
{
	BOOL isInspected = YES;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 0 OR SELF.isInspected == nil"];
	NSSet *unInspectedMeters = [property.propertyToMeter filteredSetUsingPredicate:predicate];
	
	if (property.propertyToMeter.count == 0) {
		isInspected = NO;
	}else if ([unInspectedMeters count] > 0)
	{
		isInspected = NO;
	}
	
	return isInspected;
}

- (NSNumber *) totalNumberOfAppliancesInspected :(Property *)property
{
    NSNumber *totalAppliancesInspected = [NSNumber numberWithInt:0];
    if (!isEmpty(property))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 1"];
        NSSet *inspectedAppliances = [property.propertyToAppliances filteredSetUsingPredicate:predicate];
        
        if (!isEmpty(inspectedAppliances))
        {
            totalAppliancesInspected = [NSNumber numberWithUnsignedInteger:[inspectedAppliances count]];
        }
    }
    return totalAppliancesInspected;
}


- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *) appliance
{
    [[PSDataPersistenceManager sharedManager]saveInspectionForm:inspectionFormDictionary forAppliance:appliance];
}

- (void) saveBoilerInspectionForm:(NSDictionary *) inspectionFormDictionary forBoiler:(Boiler *) boiler
{
	[[PSDataPersistenceManager sharedManager]saveBoilerInspectionForm:inspectionFormDictionary forBoiler:boiler];
}


- (BOOL) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *) detector
{
   return [[PSDataPersistenceManager sharedManager]saveDetectorInspectionForm:inspectionFormDictionary forDetector:detector];
}

- (BOOL) saveMeterInspectionForm:(NSDictionary *) inspectionFormDictionary forMeter:(Meter *)meter
{
  return [[PSDataPersistenceManager sharedManager]saveMeterInspectionForm:inspectionFormDictionary forMeter:meter];
}


- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forBoiler:(Boiler *)boiler
{
    [[PSDataPersistenceManager sharedManager] saveInstallationPipework:pipeworkDictionary forBoiler:boiler];
}


- (NSArray *)fetchAllPropertyDetectors:(NSString *)propertyId
{
  NSArray* detectors = nil;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.propertyID =%@", propertyId];
  STARTEXCEPTION
	detectors = [CoreDataHelper getObjectsFromEntity:kDetector
																				 predicate:predicate
																					 sortKey:nil
																		 sortAscending:YES
																					 context:[PSDatabaseContext sharedContext].managedObjectContext];
  ENDEXCEPTION
  return detectors;
  
}

- (NSArray *)fetchAllApplianceDefects:(NSNumber *)journalId
{
    NSArray* defects = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.journalId =%@ AND (SELF.defectToAppliance.isActive = 1 OR SELF.defectToAppliance.isActive = nil )", journalId];
    STARTEXCEPTION
	defects = [CoreDataHelper getObjectsFromEntity:kDefect
																				predicate:predicate
																					sortKey:nil
																		sortAscending:YES
																				 context:[PSDatabaseContext sharedContext].managedObjectContext];
	ENDEXCEPTION
	return defects;

}

- (Defect *)fetchApplianceDefectWithDefectID:(NSNumber *)defectID
{
	NSArray* defects = nil;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.defectID =%@", defectID];
	STARTEXCEPTION
	defects = [CoreDataHelper getObjectsFromEntity:kDefect
																			 predicate:predicate
																				 sortKey:nil
																	 sortAscending:YES
																				 context:[PSDatabaseContext sharedContext].managedObjectContext];
	ENDEXCEPTION
	if(defects.count>0)
	{
		return [defects firstObject];
	}
	return nil;
}

- (DetectorType *)fetchDetectorTypeWithDetectorTypeId:(NSNumber *)detectorTypeId
{
  NSArray* detectorTypes = nil;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.detectorTypeId =%@", detectorTypeId];
  STARTEXCEPTION
	detectorTypes = [CoreDataHelper getObjectsFromEntity:kDetectorType
																						 predicate:predicate
																							 sortKey:nil
																				 sortAscending:YES
																							 context:[PSDatabaseContext sharedContext].managedObjectContext];
  ENDEXCEPTION
	if(detectorTypes.count>0)
	{
		return [detectorTypes firstObject];
	}
  return nil;
}

- (MeterType *)fetchMeterTypeWithMeterTypeId:(NSNumber *)meterTypeId
{
  NSArray* meterTypes = nil;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.meterId =%@", meterTypeId];
  STARTEXCEPTION
	meterTypes = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([MeterType class])
																					predicate:predicate
																						sortKey:nil
																			sortAscending:YES
																						context:[PSDatabaseContext sharedContext].managedObjectContext];
  ENDEXCEPTION
	if(meterTypes.count>0)
	{
		return [meterTypes firstObject];
	}
  return nil;
}


- (PowerType *)fetchPowerTypeWithPowerTypeId:(NSNumber *)powerTypeId
{
  NSArray* powerTypes = nil;
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.powerTypeId =%@", powerTypeId];
  STARTEXCEPTION
	powerTypes = [CoreDataHelper getObjectsFromEntity:kPowerType
																					predicate:predicate
																						sortKey:nil
																			sortAscending:YES
																						context:[PSDatabaseContext sharedContext].managedObjectContext];
  ENDEXCEPTION
	if(powerTypes.count>0)
	{
		return [powerTypes firstObject];
	}
  return nil;
}

- (BOOL) isDefectAdded:(NSNumber *)journalId
{
    BOOL isDefect = NO;
    NSArray *defects = [self fetchAllApplianceDefects:journalId];
    if ([defects count] > 0)
    {
        isDefect = YES;
    }
    return isDefect;
}

- (NSArray *)fetchAllDefectCategories:(NSString*) sortKey
{
    NSArray *defectCategories = nil;
    STARTEXCEPTION
	NSArray* fetchedCategoriess = [CoreDataHelper getObjectsFromEntity:kDefectCategory
																													 predicate:[NSPredicate predicateWithValue:YES]
																														 sortKey:sortKey
																											 sortAscending:YES
																														 context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedCategoriess count] > 0)
	{
		defectCategories = [NSArray arrayWithArray:fetchedCategoriess];
	}
	ENDEXCEPTION
	return defectCategories;
}

- (NSArray *)fetchAllOperatives: (NSString*) sortKey
{
  NSArray *operatives = nil;
  STARTEXCEPTION
	NSArray* fetchedOperatives = [CoreDataHelper getObjectsFromEntity:kEmployee
																													predicate:[NSPredicate predicateWithValue:YES]
																														sortKey:sortKey
																											sortAscending:YES
																														context:[PSDatabaseContext sharedContext].managedObjectContext];
  if([fetchedOperatives count] > 0)
    {
    operatives = [NSArray arrayWithArray:fetchedOperatives];
    }
  ENDEXCEPTION
  return operatives;
}

- (NSArray *)fetchDefectPriorityList
{
	NSArray *priorityList = nil;
	STARTEXCEPTION
	NSArray* fetchedPriorities = [CoreDataHelper getObjectsFromEntity:kDefectPriority
																													predicate:[NSPredicate predicateWithValue:YES]
																														sortKey:nil
																											sortAscending:YES
																														context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedPriorities count] > 0)
	{
		priorityList = [NSArray arrayWithArray:fetchedPriorities];
	}
	ENDEXCEPTION
	return priorityList;
}

- (NSArray *)fetchTradeList
{
	NSArray *tradeList = nil;
	STARTEXCEPTION
	NSArray* fetchedTrades = [CoreDataHelper getObjectsFromEntity:kTrade
																											predicate:[NSPredicate predicateWithValue:YES]
																												sortKey:nil
																									sortAscending:YES
																												context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedTrades count] > 0)
	{
		tradeList = [NSArray arrayWithArray:fetchedTrades];
	}
	ENDEXCEPTION
	return tradeList;
}

- (void) saveApplianceDefect:(NSDictionary *)requestParameters
{
    [self.saveDefectService saveApplianceDefect:requestParameters];
}

@end
