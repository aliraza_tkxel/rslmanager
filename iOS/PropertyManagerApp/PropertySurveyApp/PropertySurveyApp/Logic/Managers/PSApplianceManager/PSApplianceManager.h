//
//  PSApplianceManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSSaveApplianceService.h"
#import "PSFetchPropertyAppliancesService.h"
#import "PSFetchDefectsService.h"
#import "PSSaveDefectService.h"

@interface PSApplianceManager : NSObject <PSSaveApplianceDelegate>

@property (nonatomic, strong) PSSaveApplianceService *saveApplianceService;
@property (nonatomic, strong) PSFetchPropertyAppliancesService *fetchPropertyAppliancesService;
@property (nonatomic, strong) PSFetchDefectsService *fetchApplianceDefectsService;
@property (nonatomic, strong) PSSaveDefectService *saveDefectService;

+ (PSApplianceManager *) sharedManager;

- (void) saveAppliance:(NSDictionary *)appliance forProperty:(NSManagedObjectID *)propertyId;

/**
 @brief saves data in offline
 */
- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray;
/**
 @brief Returns All Appliacnces (Fetched from Core Database)
 */
- (NSArray *) fetchAllAppliances;

/**
 @brief Returns All Appliacnce Locations (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceLoactions;
/**
 @brief Returns All Appliacnce Manufacturers (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceManufacturers;
/**
 @brief Returns All Appliacnce Models (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceModels;
/**
 @brief Returns All Appliacnce Types (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceTypes;

/**
 @brief Returns All Detector Types (Fetched from Core Database)
 */
- (NSArray *) fetchDetectorTypes:(NSString *) sortColumn;

/**
 @brief Returns All Power Types (Fetched from Core Database)
 */
- (NSArray *) fetchPowerTypes:(NSString *) sortColumn;;

/**
 @brief Returns YES if all appliances of a property have been inspected.
 */
- (BOOL) isApplianceInspected :(Property *)property;

/**
 @brief Returns YES if all boilers of a property have been inspected.
 */
- (BOOL) isBoilerInspected :(Property *)property;

/**
 @brief Returns count of inspected appliances of the property.
 */
- (NSNumber *) totalNumberOfAppliancesInspected :(Property *)property;

/**
 @brief Saves appliance's inspection form .
 */

- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *) appliance;

/**
 @brief Saves boiler's inspection form .
 */

- (void) saveBoilerInspectionForm:(NSDictionary *) inspectionFormDictionary forBoiler:(Boiler *) boiler;

/**
 @brief Saves property's pipework installation form .
 */

- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forBoiler:(Boiler *)boiler;

/**
 @brief Fetches all appliances in property.
 */
- (void) fetchPropertyAppliances:(NSDictionary *)requestParameters forProperty:(NSManagedObjectID *)propertyId;

/**
 @brief Fetches all appliances' defects in property.
 */
- (void) fetchApplianceDefects:(NSDictionary *)requestParameters;
/**
 @brief Fetches appliance defect in property with defect ID.
 */
- (Defect *)fetchApplianceDefectWithDefectID:(NSNumber *)defectID;
/**
 @brief Returns all appliances' defects in property (Fetched from Core Database).
 */
- (NSArray *)fetchAllApplianceDefects:(NSNumber *)journalId;

/**
 @brief Returns all defect categores (Fetched from Core Database).
 */
- (NSArray *)fetchAllDefectCategories:(NSString*)sortKey;

/**
 @brief saves new defect. responses will be catter in PSSavedewfect Service delegates
 */
- (void) saveApplianceDefect:(NSDictionary *)requestParameters;

/**
 @brief Saves detector's inspection form.
 */
- (BOOL) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *) detector;

/**
 @brief Fetch Detector type.
 */
- (DetectorType *)fetchDetectorTypeWithDetectorTypeId:(NSNumber *)detectorTypeId;

/**
 @brief Fetch Detector type.
 */
- (PowerType *)fetchPowerTypeWithPowerTypeId:(NSNumber *)powerTypeId;

/**
 @brief Returns true if defects exist against a property, no otherwise.
 */
- (BOOL) isDefectAdded:(NSNumber *)journalId;

/**
 @brief Returns true if detector exist against a property, no otherwise.
 */
- (NSArray *)fetchAllPropertyDetectors:(NSString *)propertyId;

/**
 @brief Returns surveyors list.
 */
- (NSArray *)fetchAllOperatives: (NSString*) sortKey;

/**
 @brief Fetch Meter type.
 */
- (MeterType *)fetchMeterTypeWithMeterTypeId:(NSNumber *)meterTypeId;

/**
 @brief Saves meter inspection form.
 */
- (BOOL) saveMeterInspectionForm:(NSDictionary *) inspectionFormDictionary forMeter:(Meter *)meter;

/**
 @brief Fetch meter types.
 */
- (NSArray *) fetchMeterTypes;

/**
 @brief Fetch device meter types.
 */
- (NSMutableArray *)fetchDeviceMeterTypes:(NSString *)deviceType;

/**
 @brief Fetch Defect Priority List.
 */
- (NSArray *)fetchDefectPriorityList;

/**
 @brief Fetch Trade List.
 */
- (NSArray *)fetchTradeList;

/**
 @brief Check all detectors of property are inspected
 */
- (BOOL) isDetectorInspected :(Property *)property;

/**
 @brief Check all detectors of property are inspected
 */
- (BOOL) isMeterInspected :(Property *)property;

/**
 @brief Get all active appliances
 */
- (NSSet *) getActiveAppliances:(Property *)property;

@end
