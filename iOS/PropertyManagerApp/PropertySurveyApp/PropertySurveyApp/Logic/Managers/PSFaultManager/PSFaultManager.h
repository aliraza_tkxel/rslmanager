//
//  PSFaultManager.h
//  PropertySurveyApp
//
//  Created by Yawar on 02/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSFetchJobRepairDataService.h"
#import "PSFetchJobPauseReasonsService.h"

@interface PSFaultManager : NSObject

@property (nonatomic, strong) PSFetchJobRepairDataService *fetchJobRepairDataService;
@property (nonatomic, strong) PSFetchJobPauseReasonsService *fetchJobPauseReasonService;

+(PSFaultManager *) sharedManager;

- (void) fetchFaultRepairList:(NSDictionary *)appliance;
- (void) fetchJobPauseReasonList:(NSDictionary *)requestParameters;

/**
 @brief Returns All Job Pause Reasons (Fetched from Core Database)
 */
- (NSArray *)fetchAllJobReasons;

/**
 @brief adds job Pause information in job data.
 */
- (void)addJobPauseData:(NSDictionary *)jobPauseData forJob:(FaultJobSheet *)jobData;

/**
 @brief adds job repairs in job data and also in job data repair history.
 */
- (void)addFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(FaultJobSheet *)jobData;
@end
