//
//  PSFaultManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 02/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDefectManager.h"
#import "CoreDataHelper.h"

@implementation PSFaultManager
#pragma mark -
#pragma mark ARC Singleton Implementation
static PSFaultManager *sharedDefectManager = nil;
+ (PSFaultManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDefectManager = [[PSFaultManager alloc] init];
        sharedDefectManager.fetchJobRepairDataService = [[PSFetchJobRepairDataService alloc] init];
        sharedDefectManager.fetchJobPauseReasonService = [[PSFetchJobPauseReasonsService alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedDefectManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDefectManager = [super allocWithZone:zone];
    });
    return sharedDefectManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (void) fetchFaultRepairList:(NSDictionary *)requestParameters
{
    if([UtilityClass isUserLoggedIn])
    {
        NSMutableDictionary *applianceDictionary = [NSMutableDictionary dictionary];// [appliance mutableCopy];
        
        NSData *responseData = [self.fetchJobRepairDataService fetchJobRepairDataList:requestParameters];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *repairDataArray = [responseDictionary valueForKey:@"faultRepairList"];
                    [[PSDataPersistenceManager sharedManager] saveFetchedFaultRepairData:repairDataArray];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchFaultRepairDataSaveFailureNotification                                                                                 object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchFaultRepairDataSaveFailureNotification
                                                                                object:nil];
        }
    }
}

- (void) fetchJobPauseReasonList:(NSDictionary *)requestParameters
{
    if([UtilityClass isUserLoggedIn])
    {
        NSMutableDictionary *applianceDictionary = [NSMutableDictionary dictionary];// [appliance mutableCopy];
        
        NSData *responseData = [self.fetchJobPauseReasonService fetchJobPauseReasons:requestParameters];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *pauseReasonsArray = [responseDictionary valueForKey:@"pauseReasonList"];
                    [[PSDataPersistenceManager sharedManager] savefetchedJobPauseReasons:pauseReasonsArray];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchJobPauseReasonsSaveFailureNotification                                                                                 object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchJobPauseReasonsSaveFailureNotification
                                                                                object:nil];
        }
    }

}

- (NSArray *)fetchAllJobReasons
{
    NSArray* pauseReasons = nil;
    STARTEXCEPTION
	pauseReasons = [CoreDataHelper getObjectsFromEntity:kJobPauseReason
                                           predicate:[NSPredicate predicateWithValue:YES]
                                             sortKey:nil
                                       sortAscending:YES
                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
	ENDEXCEPTION
	return pauseReasons;
    
}

- (void)addJobPauseData:(NSDictionary *)jobPauseData forJob:(FaultJobSheet *)jobData
{
    [[PSDataUpdateManager sharedManager]addJobPauseData:jobPauseData forJob:jobData];
}


- (void)addFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(FaultJobSheet *)jobData
{
    [[PSDataPersistenceManager sharedManager] saveFaultRepairHistory:faultRepairHistory forJob:jobData];
}
@end
