//
//  PSFetchDefectsService.m
//  PropertySurveyApp
//
//  Created by Yawar on 27/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchDefectsService.h"

@implementation PSFetchDefectsService
- (id)init {
    if ((self = [super initWithServiceUrl:kGetGasDefectsServiceURL]) != nil)
    {
        // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}
#pragma mark PSFetchAppliancesService

- (NSData *) fetchApplianceDefects:(NSDictionary *)parameters;
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification object:nil];
    return NO;
}

@end
