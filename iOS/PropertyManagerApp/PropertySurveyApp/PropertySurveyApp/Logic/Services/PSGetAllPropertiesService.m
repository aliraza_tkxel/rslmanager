//
//  PSGetAllPropertiesService.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSGetAllPropertiesService.h"

@implementation PSGetAllPropertiesService

- (id)init {
    if ((self = [super initWithServiceUrl:kGetAllPropertiesServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSgetPropertiesService

- (void)getAllProperties:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
    
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.propertyDelegate != nil && [self.propertyDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.propertyDelegate service:self didFailWithError:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    if(self.propertyDelegate != nil && [self.propertyDelegate respondsToSelector:@selector(service:didFinishLoadingProperties:)])
    {
        [self.propertyDelegate service:self didFinishLoadingProperties:jsonObject];
    }
}


@end
