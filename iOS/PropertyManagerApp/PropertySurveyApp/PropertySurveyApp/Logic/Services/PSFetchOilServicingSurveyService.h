//
//  PSFetchOilServicingSurveyService.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSARESTService.h"
@class PSFetchOilServicingSurveyService;
@protocol PSFetchOilServicingSurveyServiceDelegate <NSObject>
@optional
- (void) service:(PSFetchOilServicingSurveyService *)service didFinishLoadingOilSurvey:(id)oilSurvey;
- (void) service:(PSFetchOilServicingSurveyService *)service didFailWithError:(NSError *)error;
- (void) service:(PSFetchOilServicingSurveyService *)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;

@end
@interface PSFetchOilServicingSurveyService : PSARESTService <CoreRESTServiceDelegate>

@property (weak, nonatomic) id<PSFetchOilServicingSurveyServiceDelegate> oilServiceDelegate;

- (NSData*)fetchSurveyForOilServicing:(NSMutableDictionary *)parameters;


@end
