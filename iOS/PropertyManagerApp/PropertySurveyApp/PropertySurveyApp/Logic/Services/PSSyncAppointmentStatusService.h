//
//  PSSyncAppointmentStatusService.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 4/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSARESTService.h"

@class PSSyncAppointmentStatusService;

@protocol PSSyncAppointmentStatusServiceDelegate <NSObject>
@optional
- (void) service:(PSSyncAppointmentStatusService *)service didSyncStatusSuccessfully:(id)result;
- (void) service:(PSSyncAppointmentStatusService *)service didFailSyncingStatus:(NSError *)error;
@end

@interface PSSyncAppointmentStatusService : PSARESTService<CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSSyncAppointmentStatusServiceDelegate> syncStatusDelegate;

- (NSData *) performStatusSyncForAppointments:(NSMutableDictionary *)parameters;
- (void) syncAppointmentStatus:(NSMutableDictionary *)parameters;
@end
