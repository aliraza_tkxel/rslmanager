//
//  PSSaveDefectService.m
//  PropertySurveyApp
//
//  Created by Yawar on 28/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSaveDefectService.h"

@implementation PSSaveDefectService
- (id)init {
    if ((self = [super initWithServiceUrl:kSaveDefectsServiceURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}
#pragma mark PSFetchAppliancesService

- (void) saveApplianceDefect:(NSDictionary *)parameters;
{
     [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.defectDelegate != nil && [self.defectDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.defectDelegate service:self didFailWithError:nil];
    }
    return NO;
}

-(void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error
{
    if(self.defectDelegate != nil && [self.defectDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.defectDelegate service:self didFailWithError:nil];
        
    }
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.defectDelegate != nil && [self.defectDelegate respondsToSelector:@selector(service:didReceiveDefectDataAfterSuccess:)])
    {
        [self.defectDelegate service:self didReceiveDefectDataAfterSuccess:jsonObject];
    }
}

@end
