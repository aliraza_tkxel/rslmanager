//
//  PSUploadAlternateHeatingPhotographsService.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSARESTService.h"
@class PSUploadAlternateHeatingPhotographsService;
@protocol PSUploadAlternateHeatingPhotographsServiceDelegate <NSObject>
@optional

- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didReceivePhotographData:(id)surveyorData;

- (void) service:(PSUploadAlternateHeatingPhotographsService *)service didFailWithError:(NSError *)error;

@end
@interface PSUploadAlternateHeatingPhotographsService : PSARESTService
@property (strong, nonatomic) id<PSUploadAlternateHeatingPhotographsServiceDelegate> photographDelegate;

- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;
@end
