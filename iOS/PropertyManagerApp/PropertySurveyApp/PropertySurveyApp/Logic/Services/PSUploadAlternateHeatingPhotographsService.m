//
//  PSUploadAlternateHeatingPhotographsService.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 03/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSUploadAlternateHeatingPhotographsService.h"

@implementation PSUploadAlternateHeatingPhotographsService
- (id)init {
    if ((self = [super initWithServiceUrl:kUploadAlternateFuelPictureServiceURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/x-www-form-urlencoded"];
    }
    return (self);
}



- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph
{
    //return [super executeSyncRequest:parameters enableCompression:NO];
    
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    path = [paths objectAtIndex:0];
    
    path = [path stringByAppendingPathComponent:kTemporaryImageFileName];
    
    [[NSFileManager defaultManager] createFileAtPath:path
                                            contents:UIImageJPEGRepresentation(photograph, 1.0)
                                          attributes:nil];
    
    [super executeRequest:parameters responseClass:nil streamLocation:path enableCompression:NO];
    
}


#pragma mark - CoreRESTServiceDelegate

- (void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error {
    
    [super connection:connection didFailWithError:error];
    
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.photographDelegate service:self didFailWithError:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAltFuelPictureSaveFailureNotification object:nil];
    }
}

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.photographDelegate service:self didFailWithError:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAltFuelPictureSaveFailureNotification object:nil];
    }
    
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didReceivePhotographData:)])
    {
        [self.photographDelegate service:self didReceivePhotographData:jsonObject];
    }
    
    NSString *path;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [paths objectAtIndex:0];
    path = [path stringByAppendingPathComponent:kTemporaryImageFileName];
    NSError *error;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])        //Does file exist?
    {
        if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error])    //Delete it
        {
            CLS_LOG(@"Delete file error: %@", error);
        }
        
    }
    
    
}


@end
