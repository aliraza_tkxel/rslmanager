//
//  PSGetAllSurveyorsService.h
//  PropertySurveyApp
//
//  Created by Yawar on 14/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSGetAllSurveyorsService;
@protocol PSGetAllSurveyorsServiceDelegate <NSObject>
@optional
- (void) service:(PSGetAllSurveyorsService *)service didFinishLoadingSurveyors:(id)surveyorData;
- (void) service:(PSGetAllSurveyorsService *)service didFailWithError:(NSError *)error;
@end

@interface PSGetAllSurveyorsService : PSARESTService <CoreRESTServiceDelegate>

@property (weak, nonatomic) id<PSGetAllSurveyorsServiceDelegate> surveyorDelegate;

- (void)getAllSurveyors:(NSMutableDictionary *)parameters;

@end

