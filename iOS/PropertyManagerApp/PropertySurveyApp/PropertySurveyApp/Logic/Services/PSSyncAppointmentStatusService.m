//
//  PSSyncAppointmentStatusService.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 4/20/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PSSyncAppointmentStatusService.h"

@implementation PSSyncAppointmentStatusService

#pragma mark - "Init"
- (id)init {
    //change following url
    if ((self = [super initWithServiceUrl:kSyncProgressStatusServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    
    return (self);
}

#pragma mark - Service Methods
-(NSData*) performStatusSyncForAppointments:(NSMutableDictionary *)parameters{
    return [super executeSyncRequest:parameters enableCompression:NO];
}
-(void) syncAppointmentStatus:(NSMutableDictionary *)parameters{
    NSString * jsonString = [parameters JSONFragment];
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];

}

#pragma mark - RestServiceDelegates
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    [[SettingsClass sharedObject] setIsSyncingProgress:NO];
    if(self.syncStatusDelegate != nil && [self.syncStatusDelegate respondsToSelector:@selector(service:didFailSyncingStatus:)])
    {
        [self.syncStatusDelegate service:self didFailSyncingStatus:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    [[SettingsClass sharedObject] setIsSyncingProgress:NO];
    if(self.syncStatusDelegate != nil && [self.syncStatusDelegate respondsToSelector:@selector(service:didSyncStatusSuccessfully:)])
    {
        [self.syncStatusDelegate service:self didSyncStatusSuccessfully:jsonObject];
    }
}
@end
