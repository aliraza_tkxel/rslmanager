//
//  PSFetchJobRepairDataService.m
//  PropertySurveyApp
//
//  Created by Yawar on 02/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchJobRepairDataService.h"

@implementation PSFetchJobRepairDataService

- (id)init {
    if ((self = [super initWithServiceUrl:kFetchFaultDataServiceURL]) != nil)
    {
        // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}
#pragma mark PSFetchAppliancesService

- (NSData *) fetchJobRepairDataList:(NSDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveDefectFailureNotification object:nil];
    return NO;
}
@end
