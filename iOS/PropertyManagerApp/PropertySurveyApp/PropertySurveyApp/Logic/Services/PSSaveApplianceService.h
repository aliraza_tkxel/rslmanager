//
//  PSSaveApplianceService.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSSaveApplianceService;
@protocol PSSaveApplianceDelegate <NSObject>
@optional
- (void) service:(PSSaveApplianceService *)service didReceiveSaveApplianceResponse:(id)surveyorData;
- (void) service:(PSSaveApplianceService *)service didFailWithError:(NSError *)error;
@end

@interface PSSaveApplianceService : PSARESTService <CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSSaveApplianceDelegate> saveApplianceDelegate;
- (NSData *) saveAppliance:(NSDictionary *)parameters;
@end
