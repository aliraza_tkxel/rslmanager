//
//  PSCreateAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDeletePhotographService.h"

@implementation PSDeletePhotographService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kDeletePropertyPictureServiceURL]) != nil)
    {
     
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    
    return (self);
}

#pragma mark PSCreateAppointmentService

- (NSDictionary*) deleteSyncPhotograph:(NSDictionary *)parameters
{
   NSData * binaryData = [super executeSyncRequest:parameters enableCompression:NO];
    NSError *error = nil;
    NSDictionary*  jsonDict = [NSJSONSerialization
                     JSONObjectWithData:binaryData
                     options:NSJSONReadingAllowFragments error:&error];
    
    if (error)
    {
        jsonDict  = nil;
    }
    return jsonDict;
}

- (void) deletePhotograph:(NSDictionary *)parameters
{
    
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
    
}

#pragma mark - CoreRESTServiceDelegate

- (void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error {
    
    [super connection:connection didFailWithError:error];
    
    if(self.serviceDelegate != nil && [self.serviceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.serviceDelegate service:self didFailWithError:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPicturesRemoveNotification object:nil];
    }
}

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.serviceDelegate != nil && [self.serviceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.serviceDelegate service:self didFailWithError:nil];
//        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPicturesRemoveNotification object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPicturesRemoveNotification object:nil];

    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    if(self.serviceDelegate != nil && [self.serviceDelegate respondsToSelector:@selector(service:didReceiveCreateAppointmentResponse:)])
    {
        [self.serviceDelegate service:self didReceiveCreateAppointmentResponse:jsonObject];
    }
    
    NSString *path;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [paths objectAtIndex:0];
	path = [path stringByAppendingPathComponent:kTemporaryImageFileName];
	NSError *error;
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])		//Does file exist?
	{
		if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error])	//Delete it
		{
			CLS_LOG(@"Delete file error: %@", error);
		}
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPicturesRemoveNotification object:nil];
    
}


@end
