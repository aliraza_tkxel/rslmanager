//
//  PSCompleteAppointmentService.m
//  PropertySurveyApp
//
//  Created by Yawar on 02/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCompleteAppointmentService.h"

@implementation PSCompleteAppointmentService

- (id)init {
    //change following url
    if ((self = [super initWithServiceUrl:kUpdateCompleteAppointmentServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSUpdateCompleteAppointmentService
- (NSData *) performSyncAppointmentUpdate:(NSMutableDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

- (void) updateAppointments:(NSMutableDictionary *)parameters
{
    NSString * jsonString = [parameters JSONFragment];
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.completeAppointmentDelegate != nil && [self.completeAppointmentDelegate respondsToSelector:@selector(service:didFailCompleteAppointmentWithError:)])
    {
        [self.completeAppointmentDelegate service:self didFailCompleteAppointmentWithError:nil];
    }
    
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.completeAppointmentDelegate != nil && [self.completeAppointmentDelegate respondsToSelector:@selector(service:didReceiveCompleteAppointmentResponse:)])
    {
        [self.completeAppointmentDelegate service:self didReceiveCompleteAppointmentResponse:jsonObject];
    }
}

@end
