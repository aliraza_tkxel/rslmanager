//
//  PSFetchSurveyFormService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSFetchSurveyFormService;
@protocol PSFetchSurveyFormServiceDelegate <NSObject>
@optional
- (void)service:(PSFetchSurveyFormService *)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;
- (void) service:(PSFetchSurveyFormService *)service didFinishDownloadingSurveyForm:(id)surveyData;
- (void) service:(PSFetchSurveyFormService *)service didFailWithError:(NSError *)error;
@end

@interface PSFetchSurveyFormService : PSARESTService <CoreRESTServiceDelegate> {
    @private
    NSInteger _downloadPercentage;
}
@property (weak, nonatomic) id<PSFetchSurveyFormServiceDelegate> surveyDelegate;

- (NSInteger) getDownloadPercentage;
- (void) fetchStockSurveyForm:(NSMutableDictionary *)parameters;
@end
