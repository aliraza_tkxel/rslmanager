//
//  PSFetchAlternativeFuelSurveyService.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 02/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSFetchAlternativeFuelSurveyService.h"

@implementation PSFetchAlternativeFuelSurveyService
- (id)init
{
    if ((self = [super initWithServiceUrl:kDownloadAlternativeSurveyURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}


- (NSData*) fetchSurveyForAlternativeFuel:(NSMutableDictionary *)parameters{
    //[super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
     return [super executeSyncRequest:parameters enableCompression:NO];
}


#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.alternateFuelServiceDelegate != nil && [self.alternateFuelServiceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAlternativeSurveyFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.alternateFuelServiceDelegate != nil && [self.alternateFuelServiceDelegate respondsToSelector:@selector(service:didFinishLoadingAppliances:)])
    {
        [self.alternateFuelServiceDelegate service:self didFinishLoadingAlternativeSurvey:jsonObject];
    }
}

- (void)restService:(CoreRESTService *)service hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
    CLS_LOG(@"Appliance Download Progress: %ld, expectedContentLength: %ld", (long)downloadedBytes, (long)expectedContentLength);
    if (self.alternateFuelServiceDelegate && [self.alternateFuelServiceDelegate respondsToSelector:@selector(service:didReceiveDataBytes:expectedContentLength:)])
    {
        [self.alternateFuelServiceDelegate service:self didReceiveDataBytes:downloadedBytes expectedContentLength:expectedContentLength];
    }
}

#pragma mark CoreURLConnectionDelegate
- (void)connection:(CoreURLConnection *)pConnection didFailWithError:(NSError *)error
{
    [super connection:pConnection didFailWithError:error];
    if(self.alternateFuelServiceDelegate != nil && [self.alternateFuelServiceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.alternateFuelServiceDelegate service:self didFailWithError:nil];
    }
    // post a notification that we can't reach the service
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAlternativeSurveyFailureNotification object:nil];
}


@end
