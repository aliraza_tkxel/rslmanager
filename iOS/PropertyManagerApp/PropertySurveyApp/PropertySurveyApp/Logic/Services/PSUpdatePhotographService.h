//
//  PSCreateAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSUpdatePhotographService;
@protocol PSUploadPhotographServiceDelegate <NSObject>
@optional

- (void) service:(PSUpdatePhotographService *)service didReceivePhotographData:(id)surveyorData;

- (void) service:(PSUpdatePhotographService *)service didFailWithError:(NSError *)error;

@end

@interface PSUpdatePhotographService : PSARESTService <CoreRESTServiceDelegate> {
    
}

@property (strong, nonatomic) id<PSUploadPhotographServiceDelegate> photographDelegate;

- (void) updatePhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;

@end
