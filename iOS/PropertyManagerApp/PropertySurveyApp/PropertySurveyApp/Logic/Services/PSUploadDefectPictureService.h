//
//  PSCreateAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSUploadDefectPictureService;

@protocol PSUploadDefectPictureServiceDelegate <NSObject>
@optional

- (void) service:(PSUploadDefectPictureService *)service didReceivePhotographData:(id)surveyorData;

- (void) service:(PSUploadDefectPictureService *)service didFailWithError:(NSError *)error;

@end

@interface PSUploadDefectPictureService : PSARESTService <CoreRESTServiceDelegate> {
    
}

@property (strong, nonatomic) id<PSUploadDefectPictureServiceDelegate> photographDelegate;

- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;

@end
