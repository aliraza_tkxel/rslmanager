//
//  PSUpdatePasswordService.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSUpdatePasswordService.h"

@implementation PSUpdatePasswordService
- (id)init {
    if ((self = [super initWithServiceUrl:kUpdatePasswordServiceURL]) != nil)
    {
        // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSUpdatePasswordService

- (NSData *) updatePassword:(NSDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}
@end
