//
//  PSForgotPasswordService.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSForgotPasswordService.h"

@implementation PSForgotPasswordService
- (id)init {
    if ((self = [super initWithServiceUrl:kForgotPasswordServiceURL]) != nil)
    {
        // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSUpdatePasswordService

- (NSData *) forgotPassword:(NSDictionary *)requestParameters
{
    NSString *email = [requestParameters valueForKey:@"emailAddress"];
    NSString *URL = [NSString stringWithFormat:@"%@?emailAddress=%@",kForgotPasswordServiceURL,email];
    [self initWithServiceUrl:URL];
    return [super executeSyncRequest:nil enableCompression:NO];
}
@end
