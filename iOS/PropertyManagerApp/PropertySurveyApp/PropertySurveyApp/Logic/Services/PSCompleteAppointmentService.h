//
//  PSCompleteAppointmentService.h
//  PropertySurveyApp
//
//  Created by Yawar on 02/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSCompleteAppointmentService;

@protocol PSCompleteAppointmentServiceDelegate <NSObject>
@optional
- (void) service:(PSCompleteAppointmentService *)service didReceiveCompleteAppointmentResponse:(id)completeAppointmentData;
- (void) service:(PSCompleteAppointmentService *)service didFailCompleteAppointmentWithError:(NSError *)error;
@end

@interface PSCompleteAppointmentService : PSARESTService<CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSCompleteAppointmentServiceDelegate> completeAppointmentDelegate;

- (NSData *) performSyncAppointmentUpdate:(NSMutableDictionary *)parameters;
- (void) updateAppointments:(NSMutableDictionary *)parameters;
@end
