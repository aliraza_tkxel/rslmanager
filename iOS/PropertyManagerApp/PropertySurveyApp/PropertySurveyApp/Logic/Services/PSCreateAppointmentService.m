//
//  PSCreateAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCreateAppointmentService.h"

@implementation PSCreateAppointmentService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kCreateAppointmentServiceURL]) != nil)
    {
        //[self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSCreateAppointmentService


- (NSData *) createNewAppointment:(NSDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.createAppointmentDelegate != nil && [self.createAppointmentDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.createAppointmentDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    if(self.createAppointmentDelegate != nil && [self.createAppointmentDelegate respondsToSelector:@selector(service:didReceiveCreateAppointmentResponse:)])
    {
        [self.createAppointmentDelegate service:self didReceiveCreateAppointmentResponse:jsonObject];
    }
}


@end
