//
//  PSFetchOilServicingSurveyService.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSFetchOilServicingSurveyService.h"

@implementation PSFetchOilServicingSurveyService
- (id)init
{
    if ((self = [super initWithServiceUrl:kDownloadOilSurveyURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}


-(NSData *) fetchSurveyForOilServicing:(NSMutableDictionary *)parameters{
    return [super executeSyncRequest:parameters enableCompression:NO];
}


#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.oilServiceDelegate != nil && [self.oilServiceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAlternativeSurveyFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.oilServiceDelegate != nil && [self.oilServiceDelegate respondsToSelector:@selector(service:didFinishLoadingAppliances:)])
    {
        [self.oilServiceDelegate service:self didFinishLoadingOilSurvey:jsonObject];
    }
}

- (void)restService:(CoreRESTService *)service hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
    CLS_LOG(@"Appliance Download Progress: %ld, expectedContentLength: %ld", (long)downloadedBytes, (long)expectedContentLength);
    if (self.oilServiceDelegate && [self.oilServiceDelegate respondsToSelector:@selector(service:didReceiveDataBytes:expectedContentLength:)])
    {
        [self.oilServiceDelegate service:self didReceiveDataBytes:downloadedBytes expectedContentLength:expectedContentLength];
    }
}

#pragma mark CoreURLConnectionDelegate
- (void)connection:(CoreURLConnection *)pConnection didFailWithError:(NSError *)error
{
    [super connection:pConnection didFailWithError:error];
    if(self.oilServiceDelegate != nil && [self.oilServiceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.oilServiceDelegate service:self didFailWithError:nil];
    }
    // post a notification that we can't reach the service
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAlternativeSurveyFailureNotification object:nil];
}
@end
