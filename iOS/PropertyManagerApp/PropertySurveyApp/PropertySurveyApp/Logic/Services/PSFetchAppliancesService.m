//
//  PSFetchAppliancesService.m
//  PropertySurveyApp
//
//  Created by TkXel on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchAppliancesService.h"

@implementation PSFetchAppliancesService

- (id)init
{
	if ((self = [super initWithServiceUrl:kGetAppliacnesServiceURL]) != nil)
	{
		[self setDelegate:self];
		[self setMethod:@"POST"];
		[self setContentType:@"application/json"];
	}
	return (self);
}

#pragma mark PSFetchAppliancesService

- (void)fetchAllAppliacnes:(NSMutableDictionary *)parameters
{
	[super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}


#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
	if(self.appliacneDelegate != nil && [self.appliacneDelegate respondsToSelector:@selector(service:didFailWithError:)])
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
	}
	return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
	if(self.appliacneDelegate != nil && [self.appliacneDelegate respondsToSelector:@selector(service:didFinishLoadingAppliances:)])
	{
		[self.appliacneDelegate service:self didFinishLoadingAppliances:jsonObject];
	}
}

- (void)restService:(CoreRESTService *)service hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
	CLS_LOG(@"Appliance Download Progress: %ld, expectedContentLength: %ld", (long)downloadedBytes, (long)expectedContentLength);
	if (self.appliacneDelegate && [self.appliacneDelegate respondsToSelector:@selector(service:didReceiveDataBytes:expectedContentLength:)])
	{
		[self.appliacneDelegate service:self didReceiveDataBytes:downloadedBytes expectedContentLength:expectedContentLength];
	}
}


#pragma mark CoreURLConnectionDelegate
- (void)connection:(CoreURLConnection *)pConnection didFailWithError:(NSError *)error
{
	[super connection:pConnection didFailWithError:error];
	if(self.appliacneDelegate != nil && [self.appliacneDelegate respondsToSelector:@selector(service:didFailWithError:)])
	{
		[self.appliacneDelegate service:self didFailWithError:nil];
	}
	// post a notification that we can't reach the service
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
}

@end
