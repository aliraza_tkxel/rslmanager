//
//  PSUpdatePasswordService.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@interface PSUpdatePasswordService : PSARESTService
- (NSData *) updatePassword:(NSDictionary *)parameters;
@end
