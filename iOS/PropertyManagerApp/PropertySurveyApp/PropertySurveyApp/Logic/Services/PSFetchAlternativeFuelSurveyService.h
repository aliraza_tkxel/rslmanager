//
//  PSFetchAlternativeFuelSurveyService.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 02/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSARESTService.h"
@class PSFetchAlternativeFuelSurveyService;
@protocol PSFetchAlternativeFuelSurveyServiceDelegate <NSObject>
@optional
- (void) service:(PSFetchAlternativeFuelSurveyService *)service didFinishLoadingAlternativeSurvey:(id)alternativeSurvey;
- (void) service:(PSFetchAlternativeFuelSurveyService *)service didFailWithError:(NSError *)error;
- (void) service:(PSFetchAlternativeFuelSurveyService *)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;

@end
@interface PSFetchAlternativeFuelSurveyService : PSARESTService <CoreRESTServiceDelegate>

@property (weak, nonatomic) id<PSFetchAlternativeFuelSurveyServiceDelegate> alternateFuelServiceDelegate;

- (NSData*)fetchSurveyForAlternativeFuel:(NSMutableDictionary *)parameters;


@end
