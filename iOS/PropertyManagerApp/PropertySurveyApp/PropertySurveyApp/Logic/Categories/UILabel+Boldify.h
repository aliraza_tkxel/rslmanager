//
//  UILabel+Boldify.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-21.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)

- (void) boldRange: (NSRange) range;
- (void) boldSubstring: (NSString*) substring;
- (void) boldSubstring:(NSString*)substring options:(NSStringCompareOptions)mask;

@end
