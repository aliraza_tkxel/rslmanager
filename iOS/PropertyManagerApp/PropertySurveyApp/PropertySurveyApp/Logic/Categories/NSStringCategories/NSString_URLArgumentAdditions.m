//
//  NSString_URLArgumentAdditions.m
//

#import "NSString_URLArgumentAdditions.h"


@implementation NSString (NSString_URLArgumentAdditions)

- (NSString*) stringByEscapingForURLArgument {
	// Encode all the reserved characters, per RFC 3986
	
	CFStringRef escaped = 
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (CFStringRef)self,
                                            NULL,
                                            (CFStringRef)@";/?:@&+$,",  
											//(CFStringRef)@"!*'();:@&=+$,/?%#[]", //@"!*'();:@=+$,/#[]"
                                            kCFStringEncodingUTF8);
	return [(NSString*)escaped autorelease];
}

- (NSString *) stringByEscapingForURLArgument:(CFStringRef)ref {
	CFStringRef escaped = 
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (CFStringRef)self,
                                            NULL,
                                            ref,  
											kCFStringEncodingUTF8);
	return [(NSString*)escaped autorelease];
}
- (NSString*) stringByUnescapingFromURLArgument {
  NSMutableString *resultString = [NSMutableString stringWithString:self];
  [resultString replaceOccurrencesOfString:@"+"
                                withString:@" "
                                   options:NSLiteralSearch
                                     range:NSMakeRange(0, [resultString length])];
  return [resultString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}


- (NSString *) stringByAddingPercentEscapesUsingPriorityEncoding {

	NSString* result = nil;

	result = [self stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSISOLatin1StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSUTF8StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSASCIIStringEncoding :%s",result);
	return result;
	}



	result = [self stringByAddingPercentEscapesUsingEncoding:NSNEXTSTEPStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSNEXTSTEPStringEncoding :%s",result);
	return result;
	}



	result = [self stringByAddingPercentEscapesUsingEncoding:NSUTF16StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSUTF16StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSUTF32StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSUTF32StringEncoding :%s",result);
	return result;
	}




	result = [self stringByAddingPercentEscapesUsingEncoding:NSJapaneseEUCStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSJapaneseEUCStringEncoding :%s",result);
	return result;
	}


	result = [self stringByAddingPercentEscapesUsingEncoding:NSSymbolStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSSymbolStringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSNonLossyASCIIStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSNonLossyASCIIStringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSShiftJISStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSShiftJISStringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSISOLatin2StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSISOLatin2StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSUnicodeStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSUnicodeStringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSWindowsCP1251StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSWindowsCP1251StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSWindowsCP1252StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSWindowsCP1252StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSWindowsCP1253StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSWindowsCP1253StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSWindowsCP1254StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSWindowsCP1254StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSWindowsCP1250StringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSWindowsCP1250StringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSISO2022JPStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSISO2022JPStringEncoding :%s",result);
	return result;
	}

	result = [self stringByAddingPercentEscapesUsingEncoding:NSMacOSRomanStringEncoding];

	if (result != nil)
	{
	//CLS_LOG(@"NSMacOSRomanStringEncoding :%s",result);
	return result;
	}

	return result;
 }


-(BOOL) isEmpty {
	if(self == nil || [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
		return YES;
	return NO;
}
-(BOOL) isNumeric {
	NSScanner *sc = [NSScanner scannerWithString:self];
	if ( [sc scanFloat:NULL] )
	{
		return [sc isAtEnd];
	}
	return NO;	
}
-(BOOL) isAlphabetic {
	NSString *alphabetRegex = @"[A-Za-z]";
    NSPredicate *alphabetTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphabetRegex]; 	
    return [alphabetTest evaluateWithObject:self];
}

-(BOOL) isAlphabeticChar {
    if(self && [self length] > 0)
    {
        int chASCII = (int)[self characterAtIndex:(NSUInteger)0];
        if ((chASCII >= 65 && chASCII <= 90) || (chASCII >= 97 && chASCII <= 122))
        {
            return YES;
        }
    }
    return NO;
}



- (NSString*) trimString {
	NSString* input = self;
	if (input !=nil && [input length] > 0)
	{
		input = [input stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
		input = [input stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	
	return input;
}

@end
