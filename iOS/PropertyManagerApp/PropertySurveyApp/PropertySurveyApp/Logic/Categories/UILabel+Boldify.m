//
//  UILabel+Boldify.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-21.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "UILabel+Boldify.h"

@implementation UILabel (Boldify)

- (void) boldRange: (NSRange) range
{
	if (![self respondsToSelector:@selector(setAttributedText:)])
	{
		return;
	}
	NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
	[attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]} range:range];
	
	self.attributedText = attributedText;
}

- (void) boldSubstring:(NSString*) substring
{
	NSRange range = [self.text rangeOfString:substring];
	[self boldRange:range];
}

- (void) boldSubstring:(NSString*)substring options:(NSStringCompareOptions)mask
{
	NSRange range = [self.text rangeOfString:substring options:mask];
	[self boldRange:range];
}

@end
