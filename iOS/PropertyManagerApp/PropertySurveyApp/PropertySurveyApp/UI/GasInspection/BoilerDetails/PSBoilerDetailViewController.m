//
//  PSApplianceDetailViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSBoilerDetailViewController.h"
#import "PSBarButtonItem.h"
#import "PSBoilerDetailCell.h"
#import "PSInspectBoilerViewController.h"
#import "PSEditBoilerViewController.h"
#define kBoilerDetailCellHeight 492

@interface PSBoilerDetailViewController ()

@end

@implementation PSBoilerDetailViewController
{
    NSArray *_boilers;
    BOOL _noResultsFound;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
	  [self registerNibsForTableView];
    [self.tableView reloadData];

    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _boilers = [NSArray array];
    _boilers = [self.appointment.appointmentToProperty.propertyToBoiler allObjects];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];

    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Boiler Details"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    if (_noResultsFound)
        {
        rowHeight = 40;
        }
    else
        {
        rowHeight = kBoilerDetailCellHeight;
        }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
   
    if (!isEmpty(_boilers))
        {
        rows = [_boilers count];
        _noResultsFound = NO;
        }
    else
        {
        rows = 1;
        _noResultsFound = YES;
        }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;

    if(_noResultsFound)
        {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
            {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
            }
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_BOILER");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
        }
    
    else
        {
				PSBoilerDetailCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSBoilerDetailCell class])];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
        }
	
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
  
        PSBoilerDetailCell *_cell = (PSBoilerDetailCell *)*cell;
        dispatch_async(dispatch_get_main_queue(), ^{
					
					Boiler *boiler = nil;
					if (!isEmpty(_boilers))
					{
						boiler = [_boilers objectAtIndex:indexPath.row];
					}
					[_cell configureCell:boiler];
					
          [_cell.btnInspect addTarget:self action:@selector(onClickBoilerBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
          [_cell.btnInspect setTag:indexPath.row];
					
        *cell = _cell;
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_boilers count]>0){
        Boiler *boiler = [_boilers objectAtIndex:indexPath.row];
        PSEditBoilerViewController *editBoilerVC = [[PSEditBoilerViewController alloc] initWithNibName:NSStringFromClass([PSEditBoilerViewController class]) bundle:nil];
        editBoilerVC.boiler = boiler;
        [self.navigationController pushViewController:editBoilerVC animated:YES];
    }
    
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickBoilerBtnInspect:(id)sender
{
    CLS_LOG(@"onClickBoilerBtnInspect");
    UIButton *btn = (UIButton *)sender;
	  Boiler *boiler = [_boilers objectAtIndex:btn.tag];
    {
        PSInspectBoilerViewController *inspectBoilerViewController = [[PSInspectBoilerViewController alloc] initWithNibName:NSStringFromClass([PSInspectBoilerViewController class]) bundle:nil];
        [inspectBoilerViewController setBoiler:boiler];
        [self.navigationController pushViewController:inspectBoilerViewController animated:YES];
    }
	
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSBoilerDetailCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}


@end
