//
//  PSApplianceDetailViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSBoilerDetailViewController : PSCustomViewController
@property (weak,   nonatomic) Appointment *appointment;
@end
