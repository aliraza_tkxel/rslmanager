//
//  PSBoilerPickerTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSEditBoilerDelegate.h"
@interface PSBoilerPickerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldValue;
@property (weak, nonatomic) id<PSEditBoilerDelegate> delegate;
//@property BOOL isDateMode;
-(void) configureCellWithValue:(NSString *) value andHeader:(NSString *)header andTag:(NSInteger) tag;
@end
