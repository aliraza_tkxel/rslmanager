//
//  PSEditBoilerDelegate.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kRowType    0
#define kRowManufacturer    1
#define kRowModel   2
#define kRowSerialNumber  3
#define kRowGCNumber  4
#define kRowLocation    5
#define kRowFlue    6
//#define kRowInstalledDate   7
//#define kRowReplacementDate 8
#define kRowIsLandLord  7
@protocol PSEditBoilerDelegate <NSObject>
@optional
-(void) didEditBoilerWithDetail:(id)detail forRow:(NSInteger) row;
-(void) showPickerViewWithDate:(BOOL) isDateMode forRow:(NSInteger)row withSender:(id) sender;
@end
