//
//  PSBoilerPickerTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSBoilerPickerTableViewCell.h"

@implementation PSBoilerPickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnPickerTapped:(id)sender {
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(showPickerViewWithDate:forRow:withSender:)]){
        [_delegate showPickerViewWithDate:NO forRow:self.tag withSender:sender];
        
    }
}
- (IBAction)btnClearTapped:(id)sender {
    /*if(_isDateMode){
        if([_txtFieldValue.text length]>0){
            _txtFieldValue.text = @"";
            
            if(_delegate!=nil && [_delegate respondsToSelector:@selector(didEditBoilerWithDetail:forRow:)]){
                [_delegate didEditBoilerWithDetail:nil forRow:_indexPath.row];
                
            }
        }
    }*/
}

-(void) configureCellWithValue:(NSString *) value andHeader:(NSString *)header andTag:(NSInteger) tag
{
    
    self.tag = tag;
    _lblHeader.text = header;
    
    _txtFieldValue.placeholder = header;
    _txtFieldValue.text = value;
    //_isDateMode = [self isDateModeCell];
    
    _txtFieldValue.enabled = NO;
    _btnClear.hidden = YES;
    /*if(!_isDateMode || [_txtFieldValue.text length]==0){
        _btnClear.hidden = YES;
    }
    else{
        _btnClear.hidden = NO;
    }*/
}


#pragma mark - Data methods

-(BOOL) isDateModeCell{
    /*if(_indexPath.row == kRowInstalledDate || _indexPath.row == kRowReplacementDate){
        return YES;
    }*/
    return NO;
}






@end
