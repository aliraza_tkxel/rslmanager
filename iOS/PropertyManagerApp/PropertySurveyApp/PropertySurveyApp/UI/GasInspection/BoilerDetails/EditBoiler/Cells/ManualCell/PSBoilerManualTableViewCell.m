//
//  PSBoilerManualTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSBoilerManualTableViewCell.h"

@implementation PSBoilerManualTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnEditTapped:(id)sender {
    [_txtFieldValue becomeFirstResponder];
}

#pragma mark - TextField delegates

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    if(_delegate!=nil && [_delegate respondsToSelector:@selector(didEditBoilerWithDetail:forRow:)]){
        [_delegate didEditBoilerWithDetail:textField.text forRow:self.tag];
    }
}

-(void) configureCellWithValue:(NSString *) value andHeader:(NSString *)header andTag:(NSInteger) tag;
{
    self.tag = tag;
    _lblHeader.text = header;
    _txtFieldValue.text = value;
    [_txtFieldValue setPlaceholder:header];
    _txtFieldValue.delegate = self;
    
}

@end
