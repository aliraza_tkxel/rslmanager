//
//  PSEditBoilerViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBoilerManualTableViewCell.h"
#import "PSBoilerPickerTableViewCell.h"
#import "PSEditBoilerDelegate.h"
#import "TPKeyboardAvoidingTableView.h"
@interface PSEditBoilerViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSEditBoilerDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) Boiler *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;

//Sandbox Properties
@property NSString *sbModel;
@property NSString *sbSerialNumber;
@property NSString * sbGCNumber;
@property NSString * sbLocation;
@property BOOL sbIsLandLordAppliance;
//@property NSDate *sbInstalledDate;
//@property NSDate *sbReplacementDate;
@property BoilerManufacturer *sbBoilerManufacturer;
@property FlueType *sbFlueType;
@property BoilerType *sbBoilerType;
@property NSInteger pickerTag;
@property NSMutableArray *dataArray;
@property (nonatomic, retain) ActionSheetDatePicker * picker;


@end
