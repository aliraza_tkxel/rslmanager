//
//  PSEditBoilerViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSEditBoilerViewController.h"

@interface PSEditBoilerViewController ()


@end

@implementation PSEditBoilerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self registerCells];
    [self initSandBoxProperties];
    [self loadNavigationonBarItems];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init methods

-(void) initData{
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [_tblView reloadData];
}

-(void) initSandBoxProperties{
    _sbBoilerType = _boiler.boilerToBoilerType;
    _sbBoilerManufacturer = _boiler.boilerToBoilerManufacturer;
    _sbModel = _boiler.model;
    _sbSerialNumber = _boiler.serialNumber;
    _sbGCNumber = _boiler.gcNumber;
    _sbLocation = _boiler.location;
    _sbFlueType = _boiler.boilerToFlueType;
    //_sbInstalledDate = _boiler.lastReplaced;
    //_sbReplacementDate = _boiler.replacementDue;
    _sbIsLandLordAppliance = [_boiler.isLandlordAppliance boolValue];
    _dataArray = [[NSMutableArray alloc] init];
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSBoilerManualTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSBoilerManualTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSBoilerPickerTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSBoilerPickerTableViewCell"];
}
#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSaveBoiler = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(btnSaveTapped)];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.btnSaveBoiler, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Edit Boiler"];
}

- (void) btnBackTapped
{
   
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating this boiler? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

#pragma mark - Save Actions

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update the existing properties of this boiler?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveBoiler];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showErrorWithTitle:@"Error" andMessage:@"Please fill all the mandatory fields"];
    }
    
    
    
}

-(void) saveBoiler{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _boiler.boilerToBoilerType = _sbBoilerType;
    _boiler.boilerTypeId = _sbBoilerType.boilerTypeId;
    
    _boiler.boilerToBoilerManufacturer = _sbBoilerManufacturer;
    _boiler.manufacturerId = _sbBoilerManufacturer.manufacturerId;
    
    _boiler.model = _sbModel;
    _boiler.serialNumber = _sbSerialNumber;
    _boiler.gcNumber = _sbGCNumber;
    _boiler.location = _sbLocation;
    
    _boiler.boilerToFlueType = _sbFlueType;
    _boiler.flueTypeId = _sbFlueType.flueTypeId;
    
    //_boiler.lastReplaced = _sbInstalledDate;
    //_boiler.replacementDue = _sbReplacementDate;
    
    _boiler.isLandlordAppliance = [NSNumber numberWithBool:_sbIsLandLordAppliance];
    
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [self popOrCloseViewController];
    
    
}

#pragma mark - Edit boiler delegate

-(void) showPickerViewWithDate:(BOOL)isDateMode forRow:(NSInteger)row withSender:(id)sender{
    [self.view endEditing:YES];
    if(isDateMode){
        //[self showDatePickerForRow:indexPath.row withSender:sender];
    }
    else{
        [self showPickerForRow:row withSender:sender];
    }
}

-(void) didEditBoilerWithDetail:(id)detail forRow:(NSInteger)row{
    switch (row) {
        case kRowType:
            [self showMessageWithHeader:@"Alert" andBody:@"Heating type cannot be modified during this operation."];
            //_sbBoilerType = (BoilerType *) detail;
            break;
        case kRowManufacturer:
            _sbBoilerManufacturer = (BoilerManufacturer *) detail;
            break;
        case kRowModel:
            _sbModel = (NSString *) detail;
            break;
        case kRowSerialNumber:
            _sbSerialNumber = (NSString *) detail;
            break;
        case kRowGCNumber:
            _sbGCNumber = (NSString *) detail;
            break;
        case kRowLocation:
            _sbLocation = (NSString *) detail;
            break;
        case kRowFlue:
            _sbFlueType = (FlueType *) detail;
            break;
        /*case kRowInstalledDate:
            _sbInstalledDate = (NSDate *) detail;
            break;
        case kRowReplacementDate:
            _sbReplacementDate = (NSDate *) detail;
            break;*/
        case kRowIsLandLord:
            _sbIsLandLordAppliance = [(NSNumber *) detail boolValue];
            break;
        default:
            break;
    }
    [_tblView reloadData];
}


#pragma mark - TableViewDelegates

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(indexPath.row == kRowModel || indexPath.row == kRowSerialNumber || indexPath.row == kRowGCNumber || indexPath.row == kRowLocation){
        PSBoilerManualTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSBoilerManualTableViewCell class])];
        [manualCell configureCellWithValue:[self getValueForRow:indexPath.row] andHeader:[self getHeaderForRow:indexPath.row] andTag:indexPath.row];
        manualCell.delegate = self;
        cell = manualCell;
    }
    else{
        PSBoilerPickerTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSBoilerPickerTableViewCell class])];
        [pickerCell configureCellWithValue:[self getValueForRow:indexPath.row] andHeader:[self getHeaderForRow:indexPath.row] andTag:indexPath.row];
        if(indexPath.row==kRowType){
            pickerCell.btnPicker.enabled = NO;
        }
        pickerCell.delegate = self;
        cell = pickerCell;
    }
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Values Method

-(NSString *) getHeaderForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowType:
            header = @"TYPE";
            break;
        case kRowManufacturer:
            header = @"MANUFACTURER";
            break;
        case kRowModel:
            header = @"MODEL";
            break;
        case kRowSerialNumber:
            header = @"SERIAL NUMBER";
            break;
        case kRowGCNumber:
            header = @"GC NUMBER";
            break;
        case kRowLocation:
            header = @"LOCATION";
            break;
        case kRowFlue:
            header = @"FLUE TYPE";
            break;
        /*case kRowInstalledDate:
            header = @"INSTALLED DATE";
            break;
        case kRowReplacementDate:
            header = @"REPLACEMENT DATE";
            break;*/
        case kRowIsLandLord:
            header = @"LANDLORDS APPLIANCE";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getValueForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowType:
            header = _sbBoilerType.boilerTypeDescription;
            break;
        case kRowManufacturer:
            header = _sbBoilerManufacturer.manufacturerDescription;
            break;
        case kRowModel:
            header = _sbModel;
            break;
        case kRowSerialNumber:
            header = _sbSerialNumber;
            break;
        case kRowGCNumber:
            header = _sbGCNumber;
            break;
        case kRowLocation:
            header = _sbLocation;
            break;
        case kRowFlue:
            header = _sbFlueType.flueTypeDescription;
            break;
        /*case kRowInstalledDate:
            header = [UtilityClass stringFromDate:_sbInstalledDate dateFormat:kDateTimeStyle8];
            break;
        case kRowReplacementDate:
            header = [UtilityClass stringFromDate:_sbReplacementDate dateFormat:kDateTimeStyle8];
            break;*/
        case kRowIsLandLord:
            header = [self isBoilerLandLords];
            break;
        default:
            break;
    }
    if(isEmpty(header)==YES){
        header = @"";
    }
    return header;
}

-(NSString *) isBoilerLandLords{
    if(_sbIsLandLordAppliance){
        return @"YES";
    }
    else
        return @"NO";
}

#pragma mark - PickerRows

- (NSArray *) getPickerDataForRow:(NSInteger) row
{
    NSArray *rowsData = [NSArray array];
		BOOL isForSchemeBlock = isEmpty(_boiler.boilerToProperty.propertyId);
	  NSPredicate *sbPredicate = [NSPredicate predicateWithFormat:(isForSchemeBlock==YES) ? @"SELF.isSchemeBlock == 1" : @"SELF.isSchemeBlock == 0 OR SELF.isSchemeBlock == nil" ];


    STARTEXCEPTION
    NSArray* boilerData;
    switch (row) {
        case kRowType:
            boilerData = [CoreDataHelper getObjectsFromEntity:kBoilerType
																										predicate:sbPredicate
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
            break;
			case kRowManufacturer:

            boilerData = [CoreDataHelper getObjectsFromEntity:kBoilerManufacturer
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];

            break;
        case kRowFlue:
            boilerData = [CoreDataHelper getObjectsFromEntity:kFlueType
                                                    predicate:[NSPredicate predicateWithValue:YES]
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
            break;
        case kRowIsLandLord:
            boilerData = [NSArray arrayWithObjects:@"YES",@"NO", nil];
            break;
            
        default:
            break;
    }
	

	
    if([boilerData count] > 0)
    {
        rowsData = [NSArray arrayWithArray:boilerData];
    }
    ENDEXCEPTION
    return rowsData;
}

-(NSMutableArray *) createPresentablePickerDataForRow:(NSInteger) tag{
    NSArray *objectsArray = [self getPickerDataForRow:tag];
    NSMutableArray *presentableData = [[NSMutableArray alloc] init];
    if(tag==kRowType){
        for(BoilerType *type in objectsArray){
            [presentableData addObject:type.boilerTypeDescription];
        }
    }
    else if(tag == kRowManufacturer){
        for(BoilerManufacturer *type in objectsArray){
            [presentableData addObject:type.manufacturerDescription];
        }
    }
    else if(tag == kRowFlue){
        for(FlueType *type in objectsArray){
            [presentableData addObject:type.flueTypeDescription];
        }
    }
    else if(tag == kRowIsLandLord){
        presentableData = [[NSMutableArray alloc] initWithArray:objectsArray];
    }
    return presentableData;
}

#pragma mark - General Picker methods

-(void) showPickerForRow:(NSInteger) row withSender:(id) sender{
    _pickerTag = row;
    _dataArray = [self createPresentablePickerDataForRow:row];
    if(_dataArray.count > 0) {
        [ActionSheetStringPicker showPickerWithTitle:[self getHeaderForRow:row]
                                                rows:_dataArray
                                    initialSelection:[self getInitialIndexForRow:row withDataArray:_dataArray]
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
    else {
        [self showMessageWithHeader:@"Error" andBody:@"No data found for this attribute"];
    }
    
}

-(NSInteger) getInitialIndexForRow:(NSInteger) row withDataArray:(NSArray *) dataArray{
    NSInteger index = 0;
    switch (row) {
        case kRowType:
            index = [dataArray indexOfObject:_sbBoilerType.boilerTypeDescription];
            break;
        case kRowManufacturer:
            index = [dataArray indexOfObject:_sbBoilerManufacturer.manufacturerDescription];
            break;
        case kRowFlue:
            index = [dataArray indexOfObject:_sbFlueType.flueTypeDescription];
            break;
        case kRowIsLandLord:
            if(_sbIsLandLordAppliance){
                index = 0;
            }
            else{
                index = 1;
            }
            break;
            
        default:
            break;
    }
    if(index > [dataArray count] || index == [dataArray count]){
        index = 0;
    }
    return index;
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSString *value = [_dataArray objectAtIndex:[selectedIndex integerValue]];
    [_dataArray removeAllObjects];
    [self didEditBoilerWithDetail:[self getObjectForPickerTag:_pickerTag andValue:value] forRow:_pickerTag];
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

-(id) getObjectForPickerTag:(NSInteger)pickerTag andValue:(NSString *) value{
    NSArray *dataArray = [self getPickerDataForRow:pickerTag];
    if(pickerTag==kRowType){
        BoilerType *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.boilerTypeDescription = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(pickerTag==kRowManufacturer){
        BoilerManufacturer *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.manufacturerDescription = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(pickerTag==kRowFlue){
        FlueType *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.flueTypeDescription = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(pickerTag == kRowIsLandLord){
        if([value isEqualToString:@"YES"]){
            return [NSNumber numberWithBool:YES];
        }
        else{
            return [NSNumber numberWithBool:NO];
        }
    }
    return @"";
}
#pragma mark  - Validation

-(BOOL) isValidData{
    if(_sbBoilerType!=nil && _sbBoilerManufacturer!=nil && _sbFlueType!=nil && !isEmpty(_sbModel) && !isEmpty(_sbSerialNumber) && !isEmpty(_sbGCNumber) && !isEmpty(_sbLocation)){
        if([_sbBoilerType.boilerTypeDescription isEqualToString:@"Please Select"] || [_sbBoilerManufacturer.manufacturerDescription isEqualToString:@"Please Select"])
        {
            return NO;
        }
        return YES;
    }
    return NO;
}

#pragma mark - Utility

-(void) showErrorWithTitle:(NSString*) title andMessage:(NSString*) message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];
}
-(BOOL) isDate:(NSDate *) date1 EarlierThanOrEqualToDate:(NSDate*)date {
    return !([date1 compare:date] == NSOrderedDescending);
}

-(BOOL) isDate:(NSDate *) date1 LaterThanOrEqualTo:(NSDate*)date {
    return !([date1 compare:date] == NSOrderedAscending);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
