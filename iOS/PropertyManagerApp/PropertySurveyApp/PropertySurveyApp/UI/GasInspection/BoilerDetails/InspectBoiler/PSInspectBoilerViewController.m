//
//  PSInspectApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSInspectBoilerViewController.h"
#import "PSBarButtonItem.h"
#import "PSInspectBoilerPickerCell.h"
#import "PSInspectBoilerTextFieldCell.h"
#import "PSInspectBoilerPickerTFCell.h"

#define kBoilerNumberOfRows 12
#define kRowBoilerInspected 0
#define kRowCombustionReading 1
#define kRowOperatingPressure 2
#define kRowSafetyDeviceOperation 3
#define kRowSpillageTest 4
#define kRowSmokePelletFlue 5
#define kRowAdequateVentillation 6
#define kRowFlueCondition 7
#define kRowSatisfactoryTermination 8
#define kRowFluePerformanceChecks 9
#define kRowBoilerServices 10
#define kRowBoilerSafeToUse 11


@interface PSInspectBoilerViewController ()
{
    NSInteger _pickerTag;
    
    NSNumber  *_isBoilerInspected;
    NSString *_combustionReading;
    NSString *_operatingPressure;
    NSString *_safetyDeviceOperational;
		NSString *_spillageTest;
    NSString *_smokePellet;
    NSString *_adequateVentilation;
    NSString *_flueVisualCondition;
    NSString *_satisfactoryTermination;
    NSString *_fluePerformanceChecks;
    NSString *_boilerServiced;
    NSString *_boilerSafeToUse;
    NSNumber *_inspectionID;
    NSNumber *_inspectedBy;
    NSDate *_inspectionDate;
    NSString *_operatingPressureUnit;
}

@end

@implementation PSInspectBoilerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	  [self registerNibsForTableView];
    [self loadNavigationonBarItems];
		[self populateBoilerDetail];
	
}

-(void) populateBoilerDetail
{
	NSMutableString *boilerLabel =[NSMutableString stringWithString:@""];
	
	
	if (!isEmpty(self.boiler))
	{
		if (!isEmpty(self.boiler.boilerToBoilerType.boilerTypeDescription))
		{
			[boilerLabel appendString:self.boiler.boilerToBoilerType.boilerTypeDescription];
		}
		
		if (!isEmpty(self.boiler.boilerToBoilerManufacturer.manufacturerDescription))
		{
			[boilerLabel appendString:[NSString stringWithFormat:@" > %@",self.boiler.boilerToBoilerManufacturer.manufacturerDescription]];
		}
		
		
		if (!isEmpty(self.boiler.model))
		{
			[boilerLabel appendString:[NSString stringWithFormat:@" > %@",self.boiler.model]];
		}
		
		if (!isEmpty(self.boiler.boilerToBoilerInspection)) {
			[self initializeBoilerDetails];
		}
	}
	
	[self.lblBoilerDetail setText:boilerLabel];
	[self.lblBoilerDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
}

-(void) viewWillAppear:(BOOL)animated
{
	  [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	  [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Inspect Boiler"];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
	[self saveBoilerInspectionInfo];
	
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 43;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kBoilerNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;

        if (indexPath.row == kRowCombustionReading)
        {
					  PSInspectBoilerTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSInspectBoilerTextFieldCell class])];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
        else if (indexPath.row == kRowOperatingPressure)
        {
					  PSInspectBoilerPickerTFCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSInspectBoilerPickerTFCell class])];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
        else
        {
					  PSInspectBoilerPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSInspectBoilerPickerCell class])];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
 
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    
        if ([*cell isKindOfClass:[PSInspectBoilerTextFieldCell class]])
        {
            PSInspectBoilerTextFieldCell *_cell = (PSInspectBoilerTextFieldCell *)*cell;
            [_cell.txtTitle setDelegate:self];
					  [_cell configureCell];
            switch (indexPath.row)
            {
                case kRowCombustionReading:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_COMBUSTION_ANALYSER_READING")];
                    [_cell.txtTitle setTag:kRowCombustionReading];
                    if (!isEmpty(_combustionReading))
                    {
                        [_cell.txtTitle setText:_combustionReading];
                    }
                    break;
                default:
                    break;
            }
            *cell = _cell;
        }
        else if ([*cell isKindOfClass:[PSInspectBoilerPickerTFCell class]])
        {
            PSInspectBoilerPickerTFCell *_cell = (PSInspectBoilerPickerTFCell *)*cell;
					  [_cell configureCell];
            [_cell.txtTitle setDelegate:self];
            [_cell.lblHeader setText:LOC(@"KEY_STRING_OPERATING_PRESSURE_OR_HEAT_INPUT")];
            [_cell.txtTitle setTag:kRowOperatingPressure];
            [_cell setDelegate:self];
            
            if (!isEmpty(_operatingPressure))
            {
                [_cell.txtTitle setText:_operatingPressure];
            }
            if (!isEmpty(_operatingPressureUnit))
            {
                [_cell.txtUnit setText:_operatingPressureUnit];
            }
            
            _cell.pickerOptions = @[LOC(@"KEY_STRING_BTU"), LOC(@"KEY_STRING_KW"), LOC(@"KEY_STRING_MBAR")];
            [_cell.btnPicker addTarget:self action:@selector(onClickOperatingUnit:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else if ([*cell isKindOfClass:[PSInspectBoilerPickerCell class]])
        {
            PSInspectBoilerPickerCell *_cell = (PSInspectBoilerPickerCell *)*cell;
            [_cell setDelegate:self];
					  [_cell configureCell];
            switch (indexPath.row) {
                case kRowBoilerInspected:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_BOILER_INSPECTED")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    [_cell.lblTitle setText:isEmpty(_isBoilerInspected)?nil:([_isBoilerInspected boolValue] == YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO")];
                    [_cell.btnPicker addTarget:self action:@selector(onClickBoilerInspected:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSafetyDeviceOperation:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SAFETY_DEVICE_CORRECT_OPERATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_safetyDeviceOperational))
                    {
                        [_cell.lblTitle setText:_safetyDeviceOperational];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onclickSafetyDeviceOperation:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSpillageTest:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SPILLAGE_TEST")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_spillageTest))
                    {
                        [_cell.lblTitle setText:_spillageTest];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onClickSpillageTest:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSmokePelletFlue:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SMOKE_PELLET_FLUE_TEST")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_smokePellet))
                    {
                        [_cell.lblTitle setText:_smokePellet];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickSmokePelletFlue:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowAdequateVentillation:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_ADEQUATE_VENTILATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    if (!isEmpty(_adequateVentilation))
                    {
                        [_cell.lblTitle setText:_adequateVentilation];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickAdequateVentillation:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowFlueCondition:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_FLUE_VISUAL_CONDITION")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_flueVisualCondition))
                    {
                        [_cell.lblTitle setText:_flueVisualCondition];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickFlueCondition:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSatisfactoryTermination:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SATISFACTORY_TERMINATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_satisfactoryTermination))
                    {
                        [_cell.lblTitle setText:_satisfactoryTermination];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickSatisfactoryTermination:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowFluePerformanceChecks:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_FLUE_PERFORMANCE_CHECKS")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_fluePerformanceChecks))
                    {
                        [_cell.lblTitle setText:_fluePerformanceChecks];
                    }
                    
                    [_cell.btnPicker addTarget:self action:@selector(onCLickFluePerformance:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowBoilerServices:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_BOILER_SERVICED")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    
                    if (!isEmpty(_boilerServiced))
                    {
                        [_cell.lblTitle setText:_boilerServiced];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickBoilerServices:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowBoilerSafeToUse:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_BOILER_SAFE_TO_USE")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    
                    if (!isEmpty(_boilerSafeToUse))
                    {
                        [_cell.lblTitle setText:_boilerSafeToUse];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onClickBoilerSafeToUse:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                    
                default:
                    break;
            }
            *cell = _cell;
        }
   
}


- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickOperatingUnit:(id)sender
{
    CLS_LOG(@"onClickOperatingUnit");
    [self resignAllResponders];
    _pickerTag = kRowOperatingPressure;
}

- (IBAction)onClickBoilerInspected:(id)sender
{
    CLS_LOG(@"onClickBoilerInspected");
    [self resignAllResponders];
    _pickerTag = kRowBoilerInspected;
}

- (IBAction)onclickSafetyDeviceOperation:(id)sender
{
    CLS_LOG(@"onclickSafetyDeviceOperation");
    [self resignAllResponders];
    _pickerTag = kRowSafetyDeviceOperation;
}

- (IBAction)onClickSpillageTest:(id)sender
{
    CLS_LOG(@"onClickSpillageTest");
    [self resignAllResponders];
    _pickerTag = kRowSpillageTest;
}

- (IBAction)onCLickSmokePelletFlue:(id)sender
{
    CLS_LOG(@"onCLickSmokePelletFlue");
    [self resignAllResponders];
    _pickerTag = kRowSmokePelletFlue;
}

- (IBAction)onCLickAdequateVentillation:(id)sender
{
    CLS_LOG(@"onCLickAdequateVentillation");
    [self resignAllResponders];
    _pickerTag = kRowAdequateVentillation;
}

- (IBAction)onCLickFlueCondition:(id)sender
{
    CLS_LOG(@"onCLickFlueCondition");
    [self resignAllResponders];
    _pickerTag = kRowFlueCondition;
}

- (IBAction)onCLickSatisfactoryTermination:(id)sender
{
    CLS_LOG(@"onCLickSatisfactoryTermination");
    [self resignAllResponders];
    _pickerTag = kRowSatisfactoryTermination;
}

- (IBAction)onCLickFluePerformance:(id)sender
{
    CLS_LOG(@"onCLickFluePerformance");
    [self resignAllResponders];
    _pickerTag = kRowFluePerformanceChecks;
}

- (IBAction)onCLickBoilerServices:(id)sender
{
    CLS_LOG(@"onCLickBoilerServices");
    [self resignAllResponders];
    _pickerTag = kRowBoilerServices;
}

- (IBAction)onClickBoilerSafeToUse:(id)sender
{
    CLS_LOG(@"onClickBoilerSafeToUse");
    [self resignAllResponders];
    _pickerTag = kRowBoilerSafeToUse;
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowOperatingPressure: {
            self.selectedControl = textField;
        }
            break;
        default:
            self.selectedControl = textField;
            break;
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowCombustionReading:
            _combustionReading = textField.text;
            break;
        case kRowOperatingPressure:
                _operatingPressure = textField.text;
            break;
        default:
            break;
    }
    [textField resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL textFieldShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    
    if ([stringAfterReplacement length] > 15)
    {
        textFieldShouldReturn = NO;
        return textFieldShouldReturn;
    }

    switch (textField.tag) {
        case kRowCombustionReading:
            _combustionReading = stringAfterReplacement;
            break;
        case kRowOperatingPressure:
            _operatingPressure = stringAfterReplacement;
            break;
        default:
            break;
    }

    
    return textFieldShouldReturn;
    
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption
{

        switch (_pickerTag) {
            case kRowBoilerInspected:
                _isBoilerInspected = [NSNumber numberWithBool:[pickerOption boolValue]];
                break;
            case kRowSafetyDeviceOperation:
                _safetyDeviceOperational = pickerOption;
                break;
            case kRowSpillageTest:
                _spillageTest = pickerOption;
                break;
            case kRowSmokePelletFlue:
                _smokePellet = pickerOption;
                break;
            case kRowAdequateVentillation:
                _adequateVentilation = pickerOption;
                break;
            case kRowFlueCondition:
                _flueVisualCondition = pickerOption;
                break;
            case kRowSatisfactoryTermination:
                _satisfactoryTermination = pickerOption;
                break;
            case kRowFluePerformanceChecks:
                _fluePerformanceChecks = pickerOption;
                break;
            case kRowBoilerServices:
                _boilerServiced = pickerOption;
                break;
            case kRowBoilerSafeToUse:
                _boilerSafeToUse = pickerOption;
                break;
            case kRowOperatingPressure:
                _operatingPressureUnit = pickerOption;
                break;
            default:
                break;
        }

}

#pragma mark - Methods

- (void) saveBoilerInspectionInfo
{
    NSString *alertMessage = nil;
    NSString *emptyField;
    BOOL inValid = YES;
    
    if(_isBoilerInspected == nil)
    {
        alertMessage = @"Please set the inspection status first.";
    }
    else if ([_isBoilerInspected boolValue] == TRUE)
    {
        for (int cellNumber = kRowCombustionReading; cellNumber <= kRowBoilerSafeToUse; ++cellNumber)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellNumber inSection:0];
            UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
            emptyField = nil;
            alertMessage = nil;
            inValid = NO;
            
            if ([cell  isKindOfClass:[PSInspectBoilerTextFieldCell class]]) {
                PSInspectBoilerTextFieldCell *_cell = (PSInspectBoilerTextFieldCell *)cell;
                if (![_cell isValid])
                {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
            }
            
            else if ([cell isKindOfClass:[PSInspectBoilerPickerCell class]])
            {
                PSInspectBoilerPickerCell *_cell = (PSInspectBoilerPickerCell *)cell;
                if (![_cell isValid])
                {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
                
            }
            else if ([cell isKindOfClass:[PSInspectBoilerPickerTFCell class]])
            {
                PSInspectBoilerPickerTFCell *_cell = (PSInspectBoilerPickerTFCell *)cell;
                if (![_cell isValid]) {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
            }
            if (inValid)
            {
                alertMessage = [NSString stringWithFormat:@"Please enter %@.", emptyField];
                break;
            }
        }
    }
    else
    {
        inValid = NO;
    }
    
    
    if (inValid == YES)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:alertMessage delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
        [alert show];
    }
    else
    {
        [self saveInspectionForm];
    }
    
}

- (void) saveInspectionForm
{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    BoilerInspection *boilerInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
    _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    _inspectionID = (isEmpty(_inspectionID) || (_inspectionID == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionID;
    if(self.boiler.boilerToBoilerInspection!=nil){
        boilerInspection = self.boiler.boilerToBoilerInspection;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kBoilerInspection inManagedObjectContext:managedObjectContext];
    }
    
    boilerInspection.heatingId = isEmpty(_boiler.heatingId)?nil:_boiler.heatingId;
    boilerInspection.isInspected = isEmpty(_isBoilerInspected)?nil:_isBoilerInspected;
    boilerInspection.combustionReading = isEmpty(_combustionReading)?nil:_combustionReading;
    boilerInspection.operatingPressure = isEmpty(_operatingPressure)?nil:_operatingPressure;
    boilerInspection.safetyDeviceOperational = isEmpty(_safetyDeviceOperational)?nil:_safetyDeviceOperational;
    boilerInspection.smokePellet = isEmpty(_smokePellet)?nil:_smokePellet;
    boilerInspection.adequateVentilation = isEmpty(_adequateVentilation)?nil:_adequateVentilation;
    boilerInspection.flueVisualCondition = isEmpty(_flueVisualCondition)?nil:_flueVisualCondition;
    boilerInspection.satisfactoryTermination = isEmpty(_satisfactoryTermination)?nil:_satisfactoryTermination;
    boilerInspection.fluePerformanceChecks = isEmpty(_fluePerformanceChecks)?nil:_fluePerformanceChecks;
    boilerInspection.boilerServiced = isEmpty(_boilerServiced)?nil:_boilerServiced;
    boilerInspection.boilerSafeToUse = isEmpty(_boilerSafeToUse)?nil:_boilerSafeToUse;
    boilerInspection.inspectionId = isEmpty(_inspectionID)?nil:_inspectionID;
    boilerInspection.inspectedBy = isEmpty(_inspectedBy)?nil:_inspectedBy;
    boilerInspection.inspectionDate = isEmpty(_inspectionDate)?nil:_inspectionDate;
    boilerInspection.spillageTest = isEmpty(_spillageTest)?nil:_spillageTest;
    boilerInspection.operatingPressureUnit = isEmpty(_operatingPressureUnit)?nil:_operatingPressureUnit;
    
    boilerInspection.boilerInspectionToBoiler = self.boiler;
    self.boiler.boilerToBoilerInspection = boilerInspection;
    
    self.boiler.isInspected = [NSNumber numberWithBool:YES];
    self.boiler.boilerToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
    self.boiler.boilerToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [self popOrCloseViewController];

}

-(void) initializeBoilerDetails {

        _inspectionID = self.boiler.boilerToBoilerInspection.inspectionId;
        _inspectedBy = self.boiler.boilerToBoilerInspection.inspectedBy;
        _inspectionDate = self.boiler.boilerToBoilerInspection.inspectionDate;
        _isBoilerInspected = self.boiler.boilerToBoilerInspection.isInspected;
        _combustionReading = self.boiler.boilerToBoilerInspection.combustionReading;
        _operatingPressure = self.boiler.boilerToBoilerInspection.operatingPressure;
        _safetyDeviceOperational = self.boiler.boilerToBoilerInspection.safetyDeviceOperational;
        _smokePellet = self.boiler.boilerToBoilerInspection.smokePellet;
        _adequateVentilation = self.boiler.boilerToBoilerInspection.adequateVentilation;
        _flueVisualCondition = self.boiler.boilerToBoilerInspection.flueVisualCondition;
        _satisfactoryTermination = self.boiler.boilerToBoilerInspection.satisfactoryTermination;
        _fluePerformanceChecks = self.boiler.boilerToBoilerInspection.fluePerformanceChecks;
        _boilerServiced = self.boiler.boilerToBoilerInspection.boilerServiced;
        _boilerSafeToUse = self.boiler.boilerToBoilerInspection.boilerSafeToUse;
        _inspectedBy = self.boiler.boilerToBoilerInspection.inspectedBy;
        _spillageTest = self.boiler.boilerToBoilerInspection.spillageTest;
        _operatingPressureUnit = self.boiler.boilerToBoilerInspection.operatingPressureUnit;
	
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSInspectBoilerPickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSInspectBoilerPickerTFCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSInspectBoilerTextFieldCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

@end
