//
//  PSInspectApplianceTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSInspectBoilerTextFieldCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) NSArray *pickerOptions;
- (IBAction)resignFirstResponder:(id)sender;
- (BOOL) isValid;
- (IBAction)onClickEditBtn:(id)sender;
- (void) configureCell;
@end