//
//  PSInspectApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"

@protocol PSInspectBoilerProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;

@end

@interface PSInspectBoilerViewController : PSCustomViewController <PSInspectBoilerProtocol, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblBoilerDetail;
@property (weak,   nonatomic) Boiler *boiler;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@end
