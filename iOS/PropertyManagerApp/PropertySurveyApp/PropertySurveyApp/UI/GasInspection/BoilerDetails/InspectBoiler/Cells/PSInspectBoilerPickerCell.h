//
//  PSInspectAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSInspectBoilerViewController.h"
@interface PSInspectBoilerPickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSInspectBoilerProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickPickerBtn:(id)sender;
- (BOOL) isValid;
- (void) configureCell;
@end
