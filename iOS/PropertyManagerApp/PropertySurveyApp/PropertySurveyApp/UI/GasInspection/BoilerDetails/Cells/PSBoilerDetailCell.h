//
//  PSApplianceDetailCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSBoilerDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerName;
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerNameDesc;

@property (strong, nonatomic) IBOutlet UILabel *lblBoilerTypeTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblBoilerType;

@property (strong, nonatomic) IBOutlet UILabel *lblBoilerManufacturerTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblBoilerManufacturer;

@property (strong, nonatomic) IBOutlet UILabel *lblModelTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblModelType;

@property (strong, nonatomic) IBOutlet UILabel *lblSerialNumberTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSerialNumber;

@property (strong, nonatomic) IBOutlet UILabel *lbGcNumberTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbGcNumber;

@property (strong, nonatomic) IBOutlet UILabel *lblBoilerLocationTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblBoilerLocation;

@property (strong, nonatomic) IBOutlet UILabel *lblFlueTypeTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblFlueType;

@property (strong, nonatomic) IBOutlet UILabel *lblLastReplacedTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblLastReplaced;

@property (strong, nonatomic) IBOutlet UILabel *lblReplacementDueTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblReplacementDue;

@property (strong, nonatomic) IBOutlet UILabel *lblLandlordsApplianceTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblLandlordsAppliance;
@property (weak, nonatomic) IBOutlet UILabel *lblGasketReplacementDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGasketReplacementDateValue;

@property (strong, nonatomic) IBOutlet UIButton *btnInspect;

- (void) configureCell:(id) data;
@end
