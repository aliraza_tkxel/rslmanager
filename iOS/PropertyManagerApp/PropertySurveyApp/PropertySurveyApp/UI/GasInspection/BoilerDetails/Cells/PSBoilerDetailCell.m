//
//  PSApplianceDetailCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSBoilerDetailCell.h"

@implementation PSBoilerDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCell:(id) data
{
	[self.lblBoilerTypeTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblBoilerManufacturerTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblModelTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblSerialNumberTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lbGcNumberTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblBoilerLocationTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblFlueTypeTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblLastReplacedTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblReplacementDueTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblLandlordsApplianceTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblGasketReplacementDateTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblBoilerName setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	
    [self.lblBoilerNameDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblBoilerType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblBoilerManufacturer setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblModelType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblSerialNumber setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lbGcNumber setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblBoilerLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblFlueType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblLastReplaced setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblReplacementDue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblLandlordsAppliance setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblGasketReplacementDateValue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	
	Boiler * boiler = (Boiler*) data;
    
    [self.lblBoilerNameDesc setText:isEmpty(boiler.boilerName)?nil:boiler.boilerName];
    
	[self.lblBoilerType setText:isEmpty(boiler.boilerToBoilerType.boilerTypeDescription)?nil:boiler.boilerToBoilerType.boilerTypeDescription];
	[self.lblBoilerManufacturer setText:isEmpty(boiler.boilerToBoilerManufacturer.manufacturerDescription)?nil:boiler.boilerToBoilerManufacturer.manufacturerDescription];
	[self.lblModelType setText:isEmpty(boiler.model)?nil:boiler.model];
	[self.lblSerialNumber setText:isEmpty(boiler.serialNumber)?nil:boiler.serialNumber];
	[self.lbGcNumber setText:isEmpty(boiler.gcNumber)?nil:boiler.gcNumber];
	[self.lblBoilerLocation setText:isEmpty(boiler.location)?nil:boiler.location];
	[self.lblFlueType setText:isEmpty(boiler.boilerToFlueType.flueTypeDescription)?nil:boiler.boilerToFlueType.flueTypeDescription];
	[self.lblLastReplaced setText:[UtilityClass stringFromDate:boiler.lastReplaced dateFormat:kDateTimeStyle8]];
	[self.lblReplacementDue setText:[UtilityClass stringFromDate:boiler.replacementDue dateFormat:kDateTimeStyle8]];
	[self.lblLandlordsAppliance setText:[boiler.isLandlordAppliance boolValue]==YES?kBoolYes:kBoolNo];
    [self.lblGasketReplacementDateValue setText:[UtilityClass stringFromDate:boiler.gasketReplacementDate dateFormat:kDateTimeStyle8]];
    
	[self.btnInspect setBackgroundImage:[UIImage imageNamed:[boiler.isInspected boolValue]==YES?@"btn_Complete":@"checklistIcon"] forState:UIControlStateNormal];
}

@end
