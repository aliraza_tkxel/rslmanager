//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddApplianceTextFieldCell.h"

@implementation PSAddApplianceTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.txtTitle.text))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}
@end
