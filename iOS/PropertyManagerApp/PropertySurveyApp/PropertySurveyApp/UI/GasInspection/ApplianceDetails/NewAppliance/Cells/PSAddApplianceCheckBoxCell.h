//
//  PSAddApplianceCheckBoxCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddApplianceViewController.h"

@interface PSAddApplianceCheckBoxCell : UITableViewCell
@property (assign, nonatomic, setter=setCheckBoxSelected:) BOOL isCheckBoxSelected;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak,   nonatomic) id<PSAddApplianceProtocol> delegate;
- (IBAction)onClickCheckBoxBtn:(id)sender;
- (void) makeSelected;
- (void) makeDeselected;

@end
